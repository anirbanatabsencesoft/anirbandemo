﻿-- User: etlsync_secured
-- DROP USER etlsync_secured;

CREATE USER etlsync_secured WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  CREATEDB
  NOCREATEROLE
  NOREPLICATION;