﻿-- User: absencetracker_app
-- DROP USER absencetracker_app;

CREATE USER absencetracker_app WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;