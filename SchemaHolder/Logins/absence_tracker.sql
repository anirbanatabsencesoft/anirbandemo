﻿-- User: absence_tracker
-- DROP USER absence_tracker;

CREATE USER absence_tracker WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  VALID UNTIL 'infinity'

ALTER USER absence_tracker SET search_path TO public;