﻿-- User: etlsync
-- DROP USER etlsync;

CREATE USER etlsync WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;

ALTER USER etlsync SET search_path TO operational;