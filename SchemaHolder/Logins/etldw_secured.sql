﻿-- User: etldw_secured
-- DROP USER etldw_secured;

CREATE USER etldw_secured WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  CREATEDB
  NOCREATEROLE
  NOREPLICATION;