﻿CREATE OR REPLACE FUNCTION public.get_cur_count(relname TEXT) RETURNS BIGINT
AS
$body$
DECLARE
  RESULT BIGINT;
BEGIN
  EXECUTE ('select count(*) from ' || relname || ' where ver_is_current') INTO RESULT;
  RETURN RESULT;
END;
$body$
LANGUAGE plpgsql;

WITH table_counts AS (
	SELECT
		table_name,
		get_cur_count(table_name) AS records
	FROM information_schema.columns
	WHERE table_schema = 'public'
		AND column_name = 'ver_is_current'
		AND table_name NOT IN ('dim_ver', 'dbversion', 'dim_date', 'dim_lookup')
		AND table_name NOT LIKE 'dim_lookup%'
		AND table_name NOT LIKE 'view_%'
		AND table_name NOT LIKE 'm%'
)
SELECT *	
FROM table_counts
ORDER BY records DESC;

DROP FUNCTION IF EXISTS public.get_cur_count(relname TEXT);

/*
***********************************************************
***********************************************************

********************** MONGODB Query **********************
 
***********************************************************
***********************************************************

var collections = db.getCollectionNames();
print('Collections inside the db:');
for(var i = 0; i < collections.length; i++) {
	var name = collections[i];
	if(name.substr(0, 6) != 'system') {
		print(name + ' - ' + db[name].count() + ' records');
	}
}

***********************************************************
*/