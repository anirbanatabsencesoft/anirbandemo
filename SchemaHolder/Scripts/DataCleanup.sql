﻿DO
$do$
BEGIN

set schema 'operational';

TRUNCATE TABLE task_client;
TRUNCATE TABLE sync_log_error;
TRUNCATE TABLE sync_log_info;
TRUNCATE TABLE job_history;
TRUNCATE TABLE sync_failure;

UPDATE sync_manager SET 
	modified_date = utc(),
	last_sync_date_update = null,
	is_locked = false,
	is_dirty = false,
	is_suspended = false,
	lock_date = null,
	is_dirty_date = null,
	lock_task_client_id = 0,
	source_count = 0,
	initial_load = true, 
	max_failure_attempts = 1,
	default_failure_action = 0;

set schema 'public';
TRUNCATE TABLE dim_accommodation;
TRUNCATE TABLE dim_accommodation_usage;
TRUNCATE TABLE dim_case_policy;
TRUNCATE TABLE dim_case_policy_usage;
TRUNCATE TABLE dim_cases;
TRUNCATE TABLE dim_customer;
TRUNCATE TABLE dim_date;
TRUNCATE TABLE dim_demand;
TRUNCATE TABLE dim_demand_type;
TRUNCATE TABLE dim_employee;
TRUNCATE TABLE dim_employee_contact;
TRUNCATE TABLE dim_employee_organization;
TRUNCATE TABLE dim_employee_restriction;
TRUNCATE TABLE dim_employee_restriction_entry;
TRUNCATE TABLE dim_employer;
TRUNCATE TABLE dim_organization;
TRUNCATE TABLE dim_organization_annual_info;
TRUNCATE TABLE dim_todoitem;
TRUNCATE TABLE dim_users;

END
$do$