﻿-- SCHEMA: public

-- DROP SCHEMA public ;

CREATE SCHEMA IF NOT EXISTS audit;

COMMENT ON SCHEMA audit
    IS 'Audit tables stored under this schema';

DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etldw')
THEN
    CREATE ROLE etldw LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etlsync')
THEN
    CREATE ROLE etlsync LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'absence_tracker')
THEN
    CREATE ROLE absence_tracker LOGIN PASSWORD 'password_goes_here';
END IF;


END
$do$;


GRANT ALL ON SCHEMA audit TO postgres;

GRANT USAGE ON SCHEMA audit TO absence_tracker;

GRANT USAGE ON SCHEMA audit TO PUBLIC;

GRANT USAGE ON SCHEMA audit TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA audit
GRANT SELECT ON TABLES TO absence_tracker;

