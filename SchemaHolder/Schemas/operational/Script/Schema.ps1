﻿# This path argument is optional and should be absolute path up to SchemaHolder\Schemas\operational. 
# This script needs to work if its ran locally using relative paths. 
# And work using absolute paths when automated in TeamCity

param(
   [string]$Path
)

if($Path){
	$schema_folder = $Path
	$script_folder = $Path+'\'+'Script'
}

New-Item -ItemType file "$script_folder\Schema.sql" –force

Get-Content $schema_folder\operational.sql | Add-Content $script_folder\Schema.sql
Get-Content $script_folder\Template\file_beg.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Sequences\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Functions\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Tables\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Data\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\*.sql | Add-Content $script_folder\Schema.sql

Get-Content $script_folder\Template\file_end.sql | Add-Content $script_folder\Schema.sql