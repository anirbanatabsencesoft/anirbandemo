﻿/***********************************************************************************************************
** schema_update.sql
** NOTE: This file should be re-runnable all day long. Every single schema version and/or update goes into
**	     this single file, and gets wrapped with a proper if not already at this schema version so we
**       only apply upgrades that are necessary to any given target database we run it against.
**
** LAST VERSION: 29, ID: 29
************************************************************************************************************/
set schema 'operational';

DO
$do$
BEGIN

/***********************************************************************************************************
** TEMPLATE
************************************************************************************************************/
-- IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= DBVERSION_ID_VALUE) THEN
/***********************************************************************************************************/

-- Do stuff

/***********************************************************************************************************/
-- INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (DBVERSION_ID_VALUE, 1, DBVERSION_VALUE, 'Upgrade-VERSION');
-- END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** UNIVERSAL (every time)
************************************************************************************************************/
CREATE OR REPLACE FUNCTION operational.utc()
   RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'
   AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$;



/***********************************************************************************************************
** UPGRADE 14, dbversion_id = 15
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 15) THEN
/***********************************************************************************************************/

ALTER TABLE IF EXISTS operational.sync_manager DROP COLUMN IF EXISTS dest_count;

ALTER TABLE IF EXISTS operational.dimension ADD COLUMN IF NOT EXISTS table_name character varying(255);

UPDATE operational.dimension SET table_name = 'public.dim_employee' WHERE dimension_id = 1;
UPDATE operational.dimension SET table_name = 'public.dim_employer' WHERE dimension_id = 2;
UPDATE operational.dimension SET table_name = 'public.dim_customer' WHERE dimension_id = 3;
UPDATE operational.dimension SET table_name = 'public.dim_cases' WHERE dimension_id = 4;
UPDATE operational.dimension SET table_name = 'public.dim_todoitem' WHERE dimension_id = 5;
UPDATE operational.dimension SET table_name = 'public.dim_users' WHERE dimension_id = 6;
UPDATE operational.dimension SET table_name = 'public.dim_accommodation' WHERE dimension_id = 18;
UPDATE operational.dimension SET table_name = 'public.dim_organization' WHERE dimension_id = 19;
UPDATE operational.dimension SET table_name = 'public.dim_employee_organization' WHERE dimension_id = 20;
UPDATE operational.dimension SET table_name = 'public.dim_demand' WHERE dimension_id = 21;
UPDATE operational.dimension SET table_name = 'public.dim_demand_type' WHERE dimension_id = 22;
UPDATE operational.dimension SET table_name = 'public.dim_employee_restriction' WHERE dimension_id = 24;
UPDATE operational.dimension SET table_name = 'public.dim_employee_contact' WHERE dimension_id = 25;


CREATE SEQUENCE IF NOT EXISTS operational.job_history_job_history_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.job_history_job_history_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO postgres;


-- Table: operational.job

-- DROP TABLE operational.job;

CREATE TABLE IF NOT EXISTS operational.job
(
    job_id bigint NOT NULL,
    created_date timestamp with time zone NOT NULL DEFAULT now(),
	name text,
    frequency text,
    command text,
    first_run_date timestamp with time zone,
	last_run_date timestamp with time zone,
	next_run_date timestamp with time zone,
	connection_string_name text,
	is_deleted boolean DEFAULT false,
    CONSTRAINT job_pkey PRIMARY KEY (job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job
    OWNER to postgres;

GRANT ALL ON TABLE operational.job TO etlsync;
GRANT ALL ON TABLE operational.job TO postgres;



-- Table: operational.job_history

-- DROP TABLE operational.job_history;

CREATE TABLE IF NOT EXISTS operational.job_history
(
	job_history_id bigint NOT NULL DEFAULT nextval('operational.job_history_job_history_id_seq'::regclass),
    job_id bigint NOT NULL,
	name text,
    command text,
    start_date timestamp with time zone NOT NULL DEFAULT now(),
	end_date timestamp with time zone,
	status text,
	is_error boolean DEFAULT false,
	error text,
    CONSTRAINT job_history_pkey PRIMARY KEY (job_history_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job_history
    OWNER to postgres;

GRANT ALL ON TABLE operational.job_history TO etlsync;
GRANT ALL ON TABLE operational.job_history TO postgres;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (15, 1, 14, 'Upgrade-14');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 15, dbversion_id = 16
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 16) THEN
/***********************************************************************************************************/

ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS last_sync_records jsonb;
ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS lookback_period int DEFAULT 30000;
ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS initial_load boolean DEFAULT FALSE;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (16, 1, 15, 'Upgrade-15');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** UPGRADE 16, dbversion_id = 17
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 17) THEN
/***********************************************************************************************************/

/* Insert the new dimention for case_policy */
INSERT INTO operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) SELECT 26, '2017-04-10 00:00:00.000000', 'DIMENSION_CASE_POLICY', 'Dimension Case Policy', 'Dimension Case Policy', 'public.dim_case_policy' WHERE NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id=26);

/* insert record for sync manager */
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, lookback_period, initial_load) SELECT 26, NOW(), 26, 1000, NOW(), 30000, true WHERE NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id=26);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (17, 1, 16, 'Upgrade-16');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** UPGRADE 17, dbversion_id = 18
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 18) THEN
/***********************************************************************************************************/

DROP VIEW IF EXISTS operational.sync_manager_progress;

ALTER TABLE operational.sync_manager
	ALTER COLUMN last_sync_records
	SET DATA TYPE json
	USING last_sync_records::json;

TRUNCATE TABLE operational.sync_log_info;
ALTER TABLE operational.sync_log_info
	ALTER COLUMN created_date
	SET DATA TYPE timestamp with time zone
	USING created_date::timestamp with time zone;

create or replace view operational.sync_manager_progress as
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;


	create or replace view operational.sync_manager_progress as
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;

-- HACK: Add 2 default value columns for created_date to the new tables if they exist here.
-- There are too many other changes and versions going on in the public schema so I'm going to update
-- these here if the tables already exist, and if they don't, then they'll create created correctly
-- when that other stuff goes live anyway so no harm-no foul.
ALTER TABLE IF EXISTS public.dim_case_policy
	ADD COLUMN IF NOT EXISTS created_date timestamp with time zone DEFAULT utc();
ALTER TABLE IF EXISTS public.dim_case_policy_usage
	ADD COLUMN IF NOT EXISTS created_date timestamp with time zone DEFAULT utc();

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (18, 1, 17, 'Upgrade-17');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** UPGRADE 18, dbversion_id = 19
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 19) THEN
/***********************************************************************************************************/

DROP VIEW IF EXISTS operational.sync_manager_progress;

-- Fix column data types, especially dates without time zone and get that time zone in there
ALTER TABLE operational.dbversion ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.dimension ALTER COLUMN created_date SET DEFAULT utc();
TRUNCATE TABLE operational.sync_log_error;
ALTER TABLE operational.sync_log_error ALTER COLUMN created_date SET DEFAULT utc();
TRUNCATE TABLE operational.sync_log_info;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.task_client ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.task_client ALTER COLUMN modified_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_date_new;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_date_delete;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_new;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_delete;
ALTER TABLE operational.sync_manager ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager ALTER COLUMN modified_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager ALTER COLUMN lookback_period SET DEFAULT 0;
UPDATE operational.sync_manager SET lookback_period = 0 WHERE lookback_period != 0;
ALTER TABLE operational.sync_manager ALTER COLUMN initial_load SET DEFAULT TRUE;
ALTER TABLE operational.sync_manager ADD COLUMN IF NOT EXISTS max_failure_attempts int NOT NULL DEFAULT 1;
ALTER TABLE operational.sync_manager ADD COLUMN IF NOT EXISTS default_failure_action smallint NOT NULL DEFAULT 0;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date DROP DEFAULT;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DATA TYPE timestamp without time zone USING created_date at time zone 'UTC';
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.job ALTER COLUMN created_date DROP DEFAULT;
ALTER TABLE operational.job ALTER COLUMN created_date SET DATA TYPE timestamp without time zone USING created_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.job ALTER COLUMN first_run_date SET DATA TYPE timestamp without time zone USING first_run_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN last_run_date SET DATA TYPE timestamp without time zone USING last_run_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN next_run_date SET DATA TYPE timestamp without time zone USING next_run_date at time zone 'UTC';
ALTER TABLE operational.job_history ALTER COLUMN start_date DROP DEFAULT;
ALTER TABLE operational.job_history ALTER COLUMN start_date SET DATA TYPE timestamp without time zone USING start_date at time zone 'UTC';
ALTER TABLE operational.job_history ALTER COLUMN start_date SET DEFAULT utc();
ALTER TABLE operational.job_history ALTER COLUMN end_date SET DATA TYPE timestamp without time zone USING end_date at time zone 'UTC';

-- Sequence: operational.sync_failure_sync_failure_id_seq
CREATE SEQUENCE IF NOT EXISTS operational.sync_failure_sync_failure_id_seq
    INCREMENT 1
    START 1673
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_failure_sync_failure_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO postgres;

-- Table: operational.sync_failure
CREATE TABLE IF NOT EXISTS operational.sync_failure
(
    sync_failure_id bigint NOT NULL DEFAULT nextval('operational.sync_failure_sync_failure_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    ext_ref_id character varying(32),
	ext_modified_date bigint,
    errormsg character varying(255),
    errortrace text,
	entity json,
	do_action smallint NOT NULL DEFAULT 0,
	attempts int NOT NULL DEFAULT 1,
	resolved boolean NOT NULL DEFAULT FALSE,
    resolved_date timestamp without time zone,
    CONSTRAINT sync_failure_pkey PRIMARY KEY (sync_failure_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_failure
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_failure TO etlsync;
GRANT ALL ON TABLE operational.sync_failure TO postgres;

-- View: operational.sync_manager_progress
CREATE OR REPLACE VIEW operational.sync_manager_progress AS
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
), failures AS (
	select
		dimension_id,
		count(sync_failure_id) as total
	from
		operational.sync_failure
	where
		resolved = FALSE
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors,
	COALESCE(failures.total, 0) as failures
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
	left join failures on sync.dimension_id = failures.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;

-- FUNCTION: operational.getsyncisdirtyifnotexist(bigint)
CREATE OR REPLACE FUNCTION operational.getsyncisdirtyifnotexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=TRUE, is_dirty_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsyncisdirtyifnotexist(bigint)
    OWNER TO postgres;

-- FUNCTION: operational.releasesynclockifexist(bigint, bigint)
CREATE OR REPLACE FUNCTION operational.releasesynclockifexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = TRUE and sync_manager_id = SyncManagerID and lock_task_client_id = $2) THEN 
			UPDATE operational.sync_manager SET is_locked=FALSE, lock_task_client_id=0 WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesynclockifexist(bigint, bigint)
    OWNER TO postgres;

-- FUNCTION: operational.releasesyncisdirtyifexist(bigint)
CREATE OR REPLACE FUNCTION operational.releasesyncisdirtyifexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = TRUE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=FALSE WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesyncisdirtyifexist(bigint)
    OWNER TO postgres;

-- FUNCTION: operational.getsynclockifnotexist(bigint, bigint)
CREATE OR REPLACE FUNCTION operational.getsynclockifnotexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_locked=TRUE, lock_task_client_id=$2, lock_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsynclockifnotexist(bigint, bigint)
    OWNER TO postgres;
	
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = -1) THEN
	INSERT INTO operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) VALUES (-1, '2017-05-20 00:00:00.000000', 'RETRY_QUEUE', 'Retry Failure Queue', 'Retry Failure Queue', 'operational.sync_failure');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = -1) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, lookback_period, initial_load) VALUES (-1, utc(), -1, 0, utc(), 0, false);
END IF;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (19, 1, 18, 'Upgrade-18');
END IF;
/***********************************************************************************************************/





/***********************************************************************************************************
** UPGRADE 19, dbversion_id = 20
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 20) THEN
/***********************************************************************************************************/

ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_update;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS max_to_sync;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_records;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS lookback_period;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (20, 1, 19, 'Upgrade-19');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 20, dbversion_id = 21
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 21) THEN
/***********************************************************************************************************/

CREATE OR REPLACE FUNCTION operational.active_row_count(relname TEXT) RETURNS BIGINT
AS
$body$
DECLARE
  RESULT BIGINT;
BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE column_name = 'ver_is_current' AND table_name = relname) THEN
    EXECUTE ('select count(*) from ' || relname) INTO RESULT;
  ELSE
    EXECUTE ('select count(*) from ' || relname || ' where ver_is_current') INTO RESULT;
  END IF;

  RETURN RESULT;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION operational.active_row_count(text)
    OWNER TO postgres;

-- View: operational.sync_manager_progress
-- DROP VIEW operational.sync_manager_progress;

CREATE OR REPLACE VIEW operational.sync_manager_progress AS
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
), failures AS (
	select
		dimension_id,
		count(sync_failure_id) as total
	from
		operational.sync_failure
	where
		resolved = FALSE
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors,
	COALESCE(failures.total, 0) as failures,
	COALESCE(operational.active_row_count(dim."table_name"), 0) as active_rows
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
	left join failures on sync.dimension_id = failures.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (21, 1, 20, 'Upgrade-20');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** UPGRADE 21, dbversion_id = 22
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 22) THEN
/***********************************************************************************************************/

ALTER TABLE sync_manager
	ADD skip_buffer_count SMALLINT NOT NULL DEFAULT 0;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (22, 1, 21, 'Upgrade-21');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 21, dbversion_id = 22
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 22) THEN
/***********************************************************************************************************/

ALTER TABLE sync_manager
	ADD skip_buffer_count SMALLINT NOT NULL DEFAULT 0;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (22, 1, 21, 'Upgrade-21');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 24, dbversion_id = 24
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 24) THEN
/***********************************************************************************************************/
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (27, '2017-09-12 00:00:00.000000', 'DIMENSION_ORGANIZATION_ANNUAL_INFO', 'Dimension Organization Annual Info', 'Dimension Organization Annual Info', 'public.dim_organization_annual_info');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (27, utc(), 27, 1000, utc(), true);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (24, 1, 24, 'Upgrade-24');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 25, dbversion_id = 25
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 25) THEN
/***********************************************************************************************************/

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (28, '2017-11-03 00:00:00.000000', 'DIMENSION_CASE_AUTHORIZED_SUBMITTER', 'Dimension Case Authorized Submitter', 'Dimension Case Authorized Submitter', 'public.dim_case_authorized_submitter');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (28, utc(), 28, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (29, '2017-11-10 00:00:00.000000', 'DIM_CASE_EVENT_DATE', 'Dim Case Event Date', 'Dim Case Event Date', 'public.Dim_Case_Event_Date');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (29, utc(), 29, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (30, '2017-11-15 00:00:00.000000', 'DIM_CASE_PAY_PERIOD', 'Dim Case Pay Period', 'Dim Case Pay Period', 'public.Dim_Case_Pay_Period');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (30, utc(), 30, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (31, '2017-11-17 00:00:00.000000', 'DIMENSION_COMMUNICATION', 'Dimension Communication', 'Dimension Communication', 'public.Dim_Communication');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (31, utc(), 31, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (32, '2017-11-18 00:00:00.000000', 'DIMENSION_COMMUNICATION_PAPERWORK', 'Dimension Communication Paperwork', 'Dimension Communication Paperwork', 'public.Dim_Communication_Paperwork');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (32, utc(), 32, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (33, '2017-11-20 00:00:00.000000', 'DIMENSION_ATTACHMENT', 'Dimension Attachment', 'Dimension Attachment', 'public.dim_attachment');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (33, utc(), 33, 1000, utc(), true);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (25, 1, 25, 'Upgrade-25');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 26, dbversion_id = 26
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 26) THEN
/***********************************************************************************************************/

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (34, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_NOTE', 'Dimension Employee Note', 'Dimension Employee Note', 'public.dim_employee_note');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (34, utc(), 34, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (35, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_NECESSITY', 'Dimension Employee Necessity', 'Dimension Employee Necessity', 'public.dim_employee_necessity');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (35, utc(), 35, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (36, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_JOB', 'Dimension Employee Job', 'Dimension Employee Job', 'public.dim_employee_job');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (36, utc(), 36, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (37, '2017-11-29 00:00:00.000000', 'DIMENSION_EMPLOYER_SERVICE_OPTION', 'Dimension Employer Service Option', 'Dimension Employer Service Option', 'public.dim_employer_service_option');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (37, utc(), 37, 1000, utc(), true);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (26, 1, 26, 'Upgrade-26');
END IF;
/***********************************************************************************************************/
/***********************************************************************************************************
** UPGRADE 27, dbversion_id = 27
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 27) THEN
/***********************************************************************************************************/

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (38, '2017-12-01 00:00:00.000000', 'DIMENSION_CASE_NOTE', 'Dimension Case Note', 'Dimension Case Note', 'public.dim_case_note');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (39, '2017-12-01 00:00:00.000000', 'DIMENSION_WORKFLOW_INSTANCE', 'Dimension Workflow Instance', 'Dimension Workflow Instance', 'public.dim_workflow_instance');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (40, '2017-12-01 00:00:00.000000', 'DIMENSION_VARIABLE_SCHEDULE_TIME', 'Dimension Variable Schedule Time', 'Dimension Variable Schedule Time', 'public.dim_variable_schedule_time');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (38, utc(), 38, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (39, utc(), 39, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (40, utc(), 40, 1000, utc(), true);
/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (27, 1, 27, 'Upgrade-27');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 28, dbversion_id = 28
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 28) THEN
/***********************************************************************************************************/

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (41, '2017-11-20 00:00:00.000000', 'DIMENSION_ROLE', 'Dimension Role', 'Dimension Role', 'public.dim_role');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (41, utc(), 41, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (42, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT', 'Dimension Employer Contact', 'Dimension Employer Contact', 'public.dim_employer_contact');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (42, utc(), 42, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (43, '2017-11-20 00:00:00.000000', 'DIMENSION_TEAM', 'Dimension Team', 'Dimension Team', 'public.dim_team');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (43, utc(), 43, 1000, utc(), true);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (28, 1, 28, 'Upgrade-28');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 29, dbversion_id = 29
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 29) THEN
/***********************************************************************************************************/

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (44, '2017-11-20 00:00:00.000000', 'DIMENSION_CUSTOMER_SERVICE_OPTION', 'Dimension Customer Service Option', 'Dimension Customer Service Option', 'public.dim_customer_service_option');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (44, utc(), 44, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (45, '2017-11-20 00:00:00.000000', 'DIMENSION_TEAM_MEMBER', 'Dimension Team Member', 'Dimension Team Member', 'public.public.dim_team_member');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (45, utc(), 45, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (46, '2017-11-20 00:00:00.000000', 'DIMENSION_EVENT', 'Dimension Event', 'Dimension Event', 'public.dim_event');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (46, utc(), 46, 1000, utc(), true);

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (47, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT_TYPES', 'Dimension Employer Contact Types', 'Dimension Employer Contact Types', 'public.dim_employer_contact_types');
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (47, utc(), 47, 1000, utc(), true);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (29, 1, 29, 'Upgrade-29');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** END OF FILE
************************************************************************************************************/
END
$do$
