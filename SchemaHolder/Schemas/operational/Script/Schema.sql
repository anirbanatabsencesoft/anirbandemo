-- SCHEMA: operational

-- DROP SCHEMA operational ;

CREATE SCHEMA IF NOT EXISTS operational;

DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etldw')
THEN
    CREATE ROLE etldw LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etlsync')
THEN
    CREATE ROLE etlsync LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'absence_tracker')
THEN
    CREATE ROLE absence_tracker LOGIN PASSWORD 'password_goes_here';
END IF;


END
$do$;

GRANT ALL ON SCHEMA operational TO PUBLIC;

GRANT ALL ON SCHEMA operational TO etlsync;

ALTER DEFAULT PRIVILEGES IN SCHEMA operational
GRANT ALL ON TABLES TO etlsync;

ALTER DEFAULT PRIVILEGES IN SCHEMA operational
GRANT SELECT, USAGE ON SEQUENCES TO etlsync;
/******************************************************************************
**		File: Schema.sql
**		Desc: Schema implementation.  Creates complete <<SCHEMA_NAME>> schema for Postgresql db
**				Absencesoft ETL project.
**
**		Auth: <<AUTHOR>>
**		Date: <<DATE>>
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		<<DATE>>		<<AUTHOR_INITIALS>>		Created
**    
*******************************************************************************/

set schema 'operational';

DO
$do$
BEGIN
CREATE SEQUENCE IF NOT EXISTS operational.db_update_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.db_update_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.db_update_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.db_update_seq TO postgres;
CREATE SEQUENCE IF NOT EXISTS operational.job_history_job_history_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.job_history_job_history_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO postgres;
CREATE SEQUENCE IF NOT EXISTS operational.sync_failure_sync_failure_id_seq
    INCREMENT 1
    START 1673
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_failure_sync_failure_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO postgres;
CREATE SEQUENCE operational.sync_log_error_sync_log_error_id_seq
    INCREMENT 1
    START 1673
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_log_error_sync_log_error_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_log_error_sync_log_error_id_seq TO etlsync;

GRANT ALL ON SEQUENCE operational.sync_log_error_sync_log_error_id_seq TO postgres;
CREATE SEQUENCE operational.sync_log_info_sync_log_info_id_seq
    INCREMENT 1
    START 38348876
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_log_info_sync_log_info_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_log_info_sync_log_info_id_seq TO etlsync;

GRANT ALL ON SEQUENCE operational.sync_log_info_sync_log_info_id_seq TO postgres;
CREATE SEQUENCE operational.task_client_task_client_id_seq
    INCREMENT 1
    START 26
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.task_client_task_client_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.task_client_task_client_id_seq TO etlsync;

GRANT ALL ON SEQUENCE operational.task_client_task_client_id_seq TO postgres;
CREATE OR REPLACE FUNCTION operational.active_row_count(relname TEXT) RETURNS BIGINT
AS
$body$
DECLARE
  RESULT BIGINT;
BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE column_name = 'ver_is_current' AND table_name = relname) THEN
    EXECUTE ('select count(*) from ' || relname) INTO RESULT;
  ELSE
    EXECUTE ('select count(*) from ' || relname || ' where ver_is_current') INTO RESULT;
  END IF;

  RETURN RESULT;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION operational.active_row_count(text)
    OWNER TO postgres;
-- FUNCTION: operational.create_index_if_not_exists(text, text, text)

-- DROP FUNCTION operational.create_index_if_not_exists(text, text, text);

CREATE OR REPLACE FUNCTION operational.create_index_if_not_exists(
	t_name text,
	i_name text,
	index_sql text)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

DECLARE
  full_index_name varchar;
  schema_name varchar;
BEGIN

full_index_name = t_name || '_' || i_name;
schema_name = 'public';

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = full_index_name
    AND    n.nspname = schema_name
    ) THEN

    execute 'CREATE INDEX ' || full_index_name || ' ON ' || schema_name || '.' || t_name || ' ' || index_sql;
END IF;
END

$function$;

ALTER FUNCTION operational.create_index_if_not_exists(text, text, text)
    OWNER TO postgres;
-- FUNCTION: operational.eval_date(text)

-- DROP FUNCTION operational.eval_date(text);

create or replace function operational.eval_date(expression text) returns timestamp without time zone
as
$body$
declare
  result timestamp without time zone;
begin
  execute expression into result;
  return result;
end;
$body$
language plpgsql;
-- FUNCTION: operational.eval_interval(text)

-- DROP FUNCTION operational.eval_interval(text);

create or replace function operational.eval_interval(expression text) returns interval
as
$body$
declare
  result interval;
begin
  execute expression into result;
  return result;
end;
$body$
language plpgsql;
-- FUNCTION: operational.function_exists(text, text)

-- DROP FUNCTION operational.function_exists(text, text);

CREATE OR REPLACE FUNCTION operational.function_exists(
	sch text,
	fun text)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
	EXECUTE  'select pg_get_functiondef('''||sch||'.'||fun||'''::regprocedure)';
	RETURN true;
	exception when others then 
	RETURN false;
END;

$function$;

ALTER FUNCTION operational.function_exists(text, text)
    OWNER TO postgres;
-- FUNCTION: operational.getsyncisdirtyifnotexist(bigint)

-- DROP FUNCTION operational.getsyncisdirtyifnotexist(bigint);

CREATE OR REPLACE FUNCTION operational.getsyncisdirtyifnotexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=TRUE, is_dirty_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsyncisdirtyifnotexist(bigint)
    OWNER TO postgres;
-- FUNCTION: operational.getsynclockifnotexist(bigint, bigint)

-- DROP FUNCTION operational.getsynclockifnotexist(bigint, bigint);

CREATE OR REPLACE FUNCTION operational.getsynclockifnotexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_locked=TRUE, lock_task_client_id=$2, lock_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsynclockifnotexist(bigint, bigint)
    OWNER TO postgres;
-- FUNCTION: operational.releasesyncisdirtyifexist(bigint)

-- DROP FUNCTION operational.releasesyncisdirtyifexist(bigint);

CREATE OR REPLACE FUNCTION operational.releasesyncisdirtyifexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = TRUE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=FALSE WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesyncisdirtyifexist(bigint)
    OWNER TO postgres;
-- FUNCTION: operational.releasesynclockifexist(bigint, bigint)

-- DROP FUNCTION operational.releasesynclockifexist(bigint, bigint);

CREATE OR REPLACE FUNCTION operational.releasesynclockifexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = TRUE and sync_manager_id = SyncManagerID and lock_task_client_id = $2) THEN 
			UPDATE operational.sync_manager SET is_locked=FALSE, lock_task_client_id=0 WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesynclockifexist(bigint, bigint)
    OWNER TO postgres;
CREATE OR REPLACE FUNCTION operational.utc()
   RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'
   AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$;
-- Table: operational.dbversion

-- DROP TABLE operational.dbversion;

CREATE TABLE operational.dbversion
(
    dbversion_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    major_num integer DEFAULT 0,
    minur_num integer DEFAULT 0,
    notes text,
    CONSTRAINT dbversion_pkey PRIMARY KEY (dbversion_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.dbversion
    OWNER to postgres;

GRANT ALL ON TABLE operational.dbversion TO etlsync;
GRANT ALL ON TABLE operational.dbversion TO postgres;
-- Table: operational.db_update

-- DROP TABLE operational.db_update;

CREATE TABLE operational.db_update
(
    db_update_id bigint DEFAULT nextval('db_update_seq'::regclass) NOT NULL,
	db_update_record_type text NOT NULL,
	db_update_filename text,
	db_update_file_hash text,
	db_update_created_date timestamp with time zone DEFAULT now(),
	db_update_modified_date timestamp with time zone,
	db_update_ignore boolean,
	PRIMARY KEY(db_update_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.db_update
    OWNER to postgres;

GRANT ALL ON TABLE operational.db_update TO etlsync;
GRANT ALL ON TABLE operational.db_update TO postgres;
-- Table: operational.dimension

-- DROP TABLE operational.dimension;

CREATE TABLE operational.dimension
(
    dimension_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    code character varying(255),
    description character varying(255),
    visible_code character varying(255),
	table_name character varying(255),
    CONSTRAINT dimension_pkey PRIMARY KEY (dimension_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.dimension
    OWNER to postgres;

GRANT ALL ON TABLE operational.dimension TO etlsync;
GRANT ALL ON TABLE operational.dimension TO postgres;
-- Table: operational.job

-- DROP TABLE operational.job;

CREATE TABLE IF NOT EXISTS operational.job
(
    job_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	name text,
    frequency text,
    command text,
    first_run_date timestamp without time zone,
	last_run_date timestamp without time zone,
	next_run_date timestamp without time zone,
	connection_string_name text,
	is_deleted boolean DEFAULT false,
    CONSTRAINT job_pkey PRIMARY KEY (job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job
    OWNER to postgres;

GRANT ALL ON TABLE operational.job TO etlsync;
GRANT ALL ON TABLE operational.job TO postgres;
-- Table: operational.job_history

-- DROP TABLE operational.job_history;

CREATE TABLE IF NOT EXISTS operational.job_history
(
	job_history_id bigint NOT NULL DEFAULT nextval('operational.job_history_job_history_id_seq'::regclass),
    job_id bigint NOT NULL,
	name text,
    command text,
    start_date timestamp without time zone NOT NULL DEFAULT utc(),
	end_date timestamp without time zone,
	status text,
	is_error boolean DEFAULT false,
	error text,
    CONSTRAINT job_history_pkey PRIMARY KEY (job_history_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job_history
    OWNER to postgres;

GRANT ALL ON TABLE operational.job_history TO etlsync;
GRANT ALL ON TABLE operational.job_history TO postgres;
-- Table: operational.sync_failure

-- DROP TABLE operational.sync_failure;

CREATE TABLE IF NOT EXISTS operational.sync_failure
(
    sync_failure_id bigint NOT NULL DEFAULT nextval('operational.sync_failure_sync_failure_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    ext_ref_id character varying(32),
	ext_modified_date bigint,
    errormsg character varying(255),
    errortrace text,
	entity json,
	do_action smallint NOT NULL DEFAULT 0,
	attempts int NOT NULL DEFAULT 1,
	resolved boolean NOT NULL DEFAULT FALSE,
    resolved_date timestamp without time zone,
    CONSTRAINT sync_failure_pkey PRIMARY KEY (sync_failure_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_failure
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_failure TO etlsync;
GRANT ALL ON TABLE operational.sync_failure TO postgres;
-- Table: operational.sync_log_error

-- DROP TABLE operational.sync_log_error;

CREATE TABLE operational.sync_log_error
(
    sync_log_error_id bigint NOT NULL DEFAULT nextval('operational.sync_log_error_sync_log_error_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    msgsource character varying(255),
    errormsg character varying(255),
    errortrace text,
    CONSTRAINT sync_log_error_pkey PRIMARY KEY (sync_log_error_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_log_error
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_log_error TO etlsync;
GRANT ALL ON TABLE operational.sync_log_error TO postgres;
-- Table: operational.sync_log_info

-- DROP TABLE operational.sync_log_info;

CREATE TABLE operational.sync_log_info
(
    sync_log_info_id bigint NOT NULL DEFAULT nextval('operational.sync_log_info_sync_log_info_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    msgsource character varying(255),
    msgaction character varying(255),
    msgtxt character varying(255),
    CONSTRAINT sync_log_info_pkey PRIMARY KEY (sync_log_info_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_log_info
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_log_info TO etlsync;

GRANT ALL ON TABLE operational.sync_log_info TO postgres;
-- Table: operational.sync_manager

-- DROP TABLE operational.sync_manager;

CREATE TABLE operational.sync_manager
(
    sync_manager_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    last_sync_date_update timestamp without time zone,
    dimension_id bigint DEFAULT 0,
    batch_size integer DEFAULT 0,
    is_locked boolean DEFAULT false,
    lock_task_client_id bigint DEFAULT 0,
    is_suspended boolean DEFAULT false,
    suspended_master_index_id bigint DEFAULT 0,
    lock_date timestamp without time zone,
    suspend_task_client_id bigint DEFAULT 0,
    suspend_date timestamp without time zone,
    is_dirty boolean DEFAULT false,
    is_dirty_date timestamp without time zone,
    source_count bigint DEFAULT 0,
	initial_load boolean DEFAULT TRUE,
	max_failure_attempts int NOT NULL DEFAULT 1,
	default_failure_action smallint NOT NULL DEFAULT 0,
	skip_buffer_count smallint NOT NULL DEFAULT 0,
    CONSTRAINT sync_manager_pkey PRIMARY KEY (sync_manager_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_manager
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_manager TO etlsync;
GRANT ALL ON TABLE operational.sync_manager TO postgres;
-- Table: operational.task_client

-- DROP TABLE operational.task_client;

CREATE TABLE operational.task_client
(
    task_client_id bigint NOT NULL DEFAULT nextval('operational.task_client_task_client_id_seq'::regclass),
    sync_manager_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    modified_date timestamp without time zone,
    init_register_date timestamp without time zone,
    init_processing_date timestamp without time zone,
    last_process_date timestamp without time zone,
    last_heartbeat_date timestamp without time zone,
    process_id character varying(255),
    process_name character varying(255),
    connstring character varying(255),
    heartbeat_interval_in_ms integer DEFAULT 0,
    check_interval_in_ms integer DEFAULT 0,
    is_disabled boolean,
    api_settings character varying(255),
    notes text,
    CONSTRAINT task_client_pkey PRIMARY KEY (task_client_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.task_client
    OWNER to postgres;

GRANT ALL ON TABLE operational.task_client TO etlsync;
GRANT ALL ON TABLE operational.task_client TO postgres;
--dbversion Data
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (15, 1, 14, 'Upgrade-14');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (16, 1, 15, 'Upgrade-15');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (17, 1, 16, 'Upgrade-16');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (18, 1, 17, 'Upgrade-17');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (19, 1, 18, 'Upgrade-18');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (20, 1, 19, 'Upgrade-19');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (21, 1, 20, 'Upgrade-20');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (22, 1, 21, 'Upgrade-21');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (23, 1, 22, 'Upgrade-22');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (24, 1, 24, 'Upgrade-24');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (25, 1, 25, 'Upgrade-25');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (26, 1, 26, 'Upgrade-26');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (27, 1, 27, 'Upgrade-27');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (28, 1, 28, 'Upgrade-28');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (29, 1, 29, 'Upgrade-29');

-- Data: operational.dimension

insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (-1, '2017-05-20 00:00:00.000000', 'RETRY_QUEUE', 'Retry Failure Queue', 'Retry Failure Queue', 'operational.sync_failure');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (1, '2016-12-20 19:30:54.030085', 'DIMENSION_EMPLOYEE', 'Employee dimension', 'Employee dimension', 'public.dim_employee');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (2, '2016-12-20 19:30:54.030085', 'DIMENSION_EMPLOYER', 'Employer dimension', 'Employer dimension', 'public.dim_employer');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (3, '2016-12-20 19:30:54.030085', 'DIMENSION_CUSTOMER', 'Customer dimension', 'Customer dimension', 'public.dim_customer');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (4, '2016-12-20 19:33:04.27357', 'DIMENSION_CASES', 'Cases dimension', 'Cases dimension', 'public.dim_cases');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (5, '2016-12-20 19:33:04.27357', 'DIMENSION_TODOS', 'Todos dimension', 'Todos dimension', 'public.dim_todoitem');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (6, '2016-12-20 19:33:15.666968', 'DIMENSION_USERS', 'Users dimension', 'Users dimension', 'public.dim_users');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (18, '2016-12-30 23:25:49.91626', 'DIMENSION_ACCOMMODATION', 'Dimension Accommodation', 'Dimension Accommodation', 'public.dim_accommodation');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (19, '2017-01-13 22:06:25.574351', 'DIMENSION_ORGANIZATION', 'Dimension Organization', 'Dimension Organization', 'public.dim_organization');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (20, '2017-01-13 22:06:25.574351', 'DIMENSION_EMPLOYEE_ORGANIZATION', 'Dimension Employee Organization', 'Dimension Employee Organization', 'public.dim_employee_organization');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (21, '2017-01-13 22:06:25.574351', 'DIMENSION_DEMAND', 'Dimension Demand', 'Dimension Demand', 'public.dim_demand');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (22, '2017-01-13 22:06:25.574351', 'DIMENSION_DEMAND_TYPE', 'Dimension Demand Type', 'Dimension Demand Type', 'public.dim_demand_type');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (24, '2017-02-01 22:22:43.638797', 'DIMENSION_EMPLOYEE_RESTRICTION', 'Dimension Employee Restriction', 'Dimension Employee Restriction', 'public.dim_employee_restriction');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (25, '2017-02-09 00:00:00.000000', 'DIMENSION_EMPLOYEE_CONTACT', 'Dimension Employee Contact', 'Dimension Employee Contact', 'public.dim_employee_contact');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (26, '2017-04-10 00:00:00.000000', 'DIMENSION_CASE_POLICY', 'Dimension Case Policy', 'Dimension Case Policy', 'public.dim_case_policy');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (27, '2017-09-12 00:00:00.000000', 'DIMENSION_ORGANIZATION_ANNUAL_INFO', 'Dimension Organization Annual Info', 'Dimension Organization Annual Info', 'public.dim_organization_annual_info');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (28, '2017-11-03 00:00:00.000000', 'DIMENSION_CASE_AUTHORIZED_SUBMITTER', 'Dimension Case Authorized Submitter', 'Dimension Case Authorized Submitter', 'public.dim_case_authorized_submitter');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (29, '2017-11-10 00:00:00.000000', 'DIM_CASE_EVENT_DATE', 'Dim Case Event Date', 'Dim Case Event Date', 'public.Dim_Case_Event_Date');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (30, '2017-11-15 00:00:00.000000', 'DIM_CASE_PAY_PERIOD', 'Dim Case Pay Period', 'Dim Case Pay Period', 'public.Dim_Case_Pay_Period');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (31, '2017-11-17 00:00:00.000000', 'DIMENSION_COMMUNICATION', 'Dimension Communication', 'Dimension Communication', 'public.Dim_Communication');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (32, '2017-11-18 00:00:00.000000', 'DIMENSION_COMMUNICATION_PAPERWORK', 'Dimension Communication Paperwork', 'Dimension Communication Paperwork', 'public.Dim_Communication_Paperwork');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (33, '2017-11-20 00:00:00.000000', 'DIMENSION_ATTACHMENT', 'Dimension Attachment', 'Dimension Attachment', 'public.dim_attachment');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (34, '2017-09-12 00:00:00.000000', 'DIMENSION_EMPLOYEE_NOTE', 'Dimension Employee Note', 'Dimension Employee Note', 'public.dim_employee_note');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (35, '2017-09-12 00:00:00.000000', 'DIMENSION_EMPLOYEE_NECESSITY', 'Dimension Employee Necessity', 'Dimension Employee Necessity', 'public.dim_employee_necessity');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (36, '2017-09-12 00:00:00.000000', 'DIMENSION_EMPLOYEE_JOB', 'Dimension Employee Job', 'Dimension Employee Job', 'public.dim_employee_job');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (37, '2017-09-29 00:00:00.000000', 'DIMENSION_EMPLOYER_SERVICE_OPTION', 'Dimension Employer Service Option', 'Dimension Employer Service Option', 'public.dim_employer_service_option');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (38, '2017-12-01 00:00:00.000000', 'DIMENSION_CASE_NOTE', 'Dimension Case Note', 'Dimension Case Note', 'public.dim_case_note');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (39, '2017-12-01 00:00:00.000000', 'DIMENSION_WORKFLOW_INSTANCE', 'Dimension Workflow Instance', 'Dimension Workflow Instance', 'public.dim_workflow_instance');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (40, '2017-12-01 00:00:00.000000', 'DIMENSION_VARIABLE_SCHEDULE_TIME', 'Dimension Variable Schedule Time', 'Dimension Variable Schedule Time', 'public.dim_variable_schedule_time');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (41, '2017-09-29 00:00:00.000000', 'DIMENSION_ROLE', 'Dimension Role', 'Dimension Role', 'public.dim_role');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (42, '2017-09-29 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT', 'Dimension Employer Contact', 'Dimension Employer Contact', 'public.dim_employer_contact');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (43, '2017-09-29 00:00:00.000000', 'DIMENSION_TEAM', 'Dimension Team', 'Dimension Team', 'public.dim_team');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (44, '2017-09-29 00:00:00.000000', 'DIMENSION_CUSTOMER_SERVICE_OPTION', 'Dimension Customer Service Option', 'Dimension Customer Service Option', 'public.dim_customer_service_option');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (45, '2017-09-29 00:00:00.000000', 'DIMENSION_TEAM_MEMBER', 'Dimension Team Member', 'Dimension Team Member', 'public.dim_team_member');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (46, '2017-09-29 00:00:00.000000', 'DIMENSION_EVENT', 'Dimension Event', 'Dimension Event', 'public.dim_event');
insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (47, '2017-09-29 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT_TYPES', 'Dimension Employer Contact Types', 'Dimension Employer Contact Types', 'public.dim_employer_contact_types');
-- Data: operational.job

insert into operational.job (job_id, name, frequency, command, connection_string_name) values (1, 'Refresh Case Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_case', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (2, 'Refresh Customer Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_customer', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (3, 'Refresh Employee Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_employee', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (4, 'Refresh Employer Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_employer', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (5, 'Refresh Org Facts', '1h', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_organization', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (6, 'Refresh User Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_user', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (7, 'Refresh Employee Supervisors', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_supervisor', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (8, 'Refresh Employee HR', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_hr', 'DataWarehouseSettings');
-- Data: operational.sync_manager

INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (-1, utc(), -1, 0, utc(), false);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (1, utc(), 1, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (2, utc(), 2, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (3, utc(), 3, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (4, utc(), 4, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (5, utc(), 5, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (6, utc(), 6, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (18, utc(), 18, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (19, utc(), 19, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (20, utc(), 20, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (21, utc(), 21, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (22, utc(), 22, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (23, utc(), 23, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (24, utc(), 24, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (25, utc(), 25, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (26, utc(), 26, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (27, utc(), 27, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (28, utc(), 28, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (29, utc(), 29, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (30, utc(), 30, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (31, utc(), 31, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (32, utc(), 32, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (33, utc(), 33, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (34, utc(), 34, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (35, utc(), 35, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (36, utc(), 36, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (37, utc(), 37, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (38, utc(), 38, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (39, utc(), 39, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (40, utc(), 40, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (41, utc(), 41, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (42, utc(), 42, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (43, utc(), 43, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (44, utc(), 44, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (45, utc(), 45, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (46, utc(), 46, 1000, utc(), true);
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (47, utc(), 47, 1000, utc(), true);
-- View: operational.sync_manager_progress

-- DROP VIEW operational.sync_manager_progress;

CREATE OR REPLACE VIEW operational.sync_manager_progress AS
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
), failures AS (
	select
		dimension_id,
		count(sync_failure_id) as total
	from
		operational.sync_failure
	where
		resolved = FALSE
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors,
	COALESCE(failures.total, 0) as failures,
	COALESCE(operational.active_row_count(dim."table_name"), 0) as active_rows
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
	left join failures on sync.dimension_id = failures.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;


/* END SCHEMA */

END
$do$
