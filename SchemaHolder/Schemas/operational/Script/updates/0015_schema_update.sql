
ALTER TABLE IF EXISTS operational.sync_manager DROP COLUMN IF EXISTS dest_count;

ALTER TABLE IF EXISTS operational.dimension ADD COLUMN IF NOT EXISTS table_name character varying(255);

UPDATE operational.dimension SET table_name = 'public.dim_employee' WHERE dimension_id = 1;
UPDATE operational.dimension SET table_name = 'public.dim_employer' WHERE dimension_id = 2;
UPDATE operational.dimension SET table_name = 'public.dim_customer' WHERE dimension_id = 3;
UPDATE operational.dimension SET table_name = 'public.dim_cases' WHERE dimension_id = 4;
UPDATE operational.dimension SET table_name = 'public.dim_todoitem' WHERE dimension_id = 5;
UPDATE operational.dimension SET table_name = 'public.dim_users' WHERE dimension_id = 6;
UPDATE operational.dimension SET table_name = 'public.dim_accommodation' WHERE dimension_id = 18;
UPDATE operational.dimension SET table_name = 'public.dim_organization' WHERE dimension_id = 19;
UPDATE operational.dimension SET table_name = 'public.dim_employee_organization' WHERE dimension_id = 20;
UPDATE operational.dimension SET table_name = 'public.dim_demand' WHERE dimension_id = 21;
UPDATE operational.dimension SET table_name = 'public.dim_demand_type' WHERE dimension_id = 22;
UPDATE operational.dimension SET table_name = 'public.dim_employee_restriction' WHERE dimension_id = 24;
UPDATE operational.dimension SET table_name = 'public.dim_employee_contact' WHERE dimension_id = 25;


CREATE SEQUENCE IF NOT EXISTS operational.job_history_job_history_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.job_history_job_history_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO postgres;


-- Table: operational.job

-- DROP TABLE operational.job;

CREATE TABLE IF NOT EXISTS operational.job
(
    job_id bigint NOT NULL,
    created_date timestamp with time zone NOT NULL DEFAULT now(),
	name text,
    frequency text,
    command text,
    first_run_date timestamp with time zone,
	last_run_date timestamp with time zone,
	next_run_date timestamp with time zone,
	connection_string_name text,
	is_deleted boolean DEFAULT false,
    CONSTRAINT job_pkey PRIMARY KEY (job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job
    OWNER to postgres;

GRANT ALL ON TABLE operational.job TO etlsync;
GRANT ALL ON TABLE operational.job TO postgres;



-- Table: operational.job_history

-- DROP TABLE operational.job_history;

CREATE TABLE IF NOT EXISTS operational.job_history
(
	job_history_id bigint NOT NULL DEFAULT nextval('operational.job_history_job_history_id_seq'::regclass),
    job_id bigint NOT NULL,
	name text,
    command text,
    start_date timestamp with time zone NOT NULL DEFAULT now(),
	end_date timestamp with time zone,
	status text,
	is_error boolean DEFAULT false,
	error text,
    CONSTRAINT job_history_pkey PRIMARY KEY (job_history_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job_history
    OWNER to postgres;

GRANT ALL ON TABLE operational.job_history TO etlsync;
GRANT ALL ON TABLE operational.job_history TO postgres;

CREATE SEQUENCE IF NOT EXISTS test
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;