
UPDATE operational.dimension
SET TABLE_NAME = 'public.dim_communication'
where dimension_id = 31;

UPDATE operational.dimension
SET TABLE_NAME = 'public.dim_communication_paperwork'
where dimension_id = 32;

UPDATE operational.dimension
SET TABLE_NAME = 'public.dim_team_member'
where dimension_id = 45;