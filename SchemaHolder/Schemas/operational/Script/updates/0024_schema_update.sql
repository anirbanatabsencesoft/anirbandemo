DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 27) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) 
	values (27, '2017-09-12 00:00:00.000000', 'DIMENSION_ORGANIZATION_ANNUAL_INFO', 'Dimension Organization Annual Info', 
		'Dimension Organization Annual Info', 'public.dim_organization_annual_info');
END IF;

IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 27) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) 
VALUES (27, utc(), 27, 1000, utc(), true);
END IF;

END 
$do$