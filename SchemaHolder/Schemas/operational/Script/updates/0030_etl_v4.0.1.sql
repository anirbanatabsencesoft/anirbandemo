DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='operational' AND table_name='sync_manager' AND column_name='batch_look_back_minutes') THEN
	ALTER TABLE sync_manager
	ADD batch_look_back_minutes INT NOT NULL DEFAULT 0;
END IF;

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='operational' AND table_name='sync_manager' AND column_name='batch_look_ahead_minutes') THEN
	ALTER TABLE sync_manager
	ADD batch_look_ahead_minutes INT NOT NULL DEFAULT 0;
END IF;

update operational.sync_manager set batch_look_back_minutes = 3;
update operational.sync_manager set batch_look_ahead_minutes = 5;


END
$do$