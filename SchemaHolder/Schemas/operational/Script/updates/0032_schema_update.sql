﻿DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 48) THEN
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (48, utc(), 48, 1000, utc(), true);
END IF;

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 48) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (48, '2018-06-30 00:00:00.000000', 'DIMENSION_RELAPSE', 'Dimension Relapse', 'Dimension Relapse', 'public.dim_relapse');
END IF;
END

$do$
