DO
$do$
BEGIN

DROP VIEW IF EXISTS operational.sync_manager_progress;

-- Fix column data types, especially dates without time zone and get that time zone in there
ALTER TABLE operational.dbversion ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.dimension ALTER COLUMN created_date SET DEFAULT utc();
TRUNCATE TABLE operational.sync_log_error;
ALTER TABLE operational.sync_log_error ALTER COLUMN created_date SET DEFAULT utc();
TRUNCATE TABLE operational.sync_log_info;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.task_client ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.task_client ALTER COLUMN modified_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_date_new;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_date_delete;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_new;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_delete;
ALTER TABLE operational.sync_manager ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager ALTER COLUMN modified_date SET DEFAULT utc();
ALTER TABLE operational.sync_manager ALTER COLUMN lookback_period SET DEFAULT 0;
UPDATE operational.sync_manager SET lookback_period = 0 WHERE lookback_period != 0;
ALTER TABLE operational.sync_manager ALTER COLUMN initial_load SET DEFAULT TRUE;
ALTER TABLE operational.sync_manager ADD COLUMN IF NOT EXISTS max_failure_attempts int NOT NULL DEFAULT 1;
ALTER TABLE operational.sync_manager ADD COLUMN IF NOT EXISTS default_failure_action smallint NOT NULL DEFAULT 0;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date DROP DEFAULT;
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DATA TYPE timestamp without time zone USING created_date at time zone 'UTC';
ALTER TABLE operational.sync_log_info ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.job ALTER COLUMN created_date DROP DEFAULT;
ALTER TABLE operational.job ALTER COLUMN created_date SET DATA TYPE timestamp without time zone USING created_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE operational.job ALTER COLUMN first_run_date SET DATA TYPE timestamp without time zone USING first_run_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN last_run_date SET DATA TYPE timestamp without time zone USING last_run_date at time zone 'UTC';
ALTER TABLE operational.job ALTER COLUMN next_run_date SET DATA TYPE timestamp without time zone USING next_run_date at time zone 'UTC';
ALTER TABLE operational.job_history ALTER COLUMN start_date DROP DEFAULT;
ALTER TABLE operational.job_history ALTER COLUMN start_date SET DATA TYPE timestamp without time zone USING start_date at time zone 'UTC';
ALTER TABLE operational.job_history ALTER COLUMN start_date SET DEFAULT utc();
ALTER TABLE operational.job_history ALTER COLUMN end_date SET DATA TYPE timestamp without time zone USING end_date at time zone 'UTC';

-- Sequence: operational.sync_failure_sync_failure_id_seq
CREATE SEQUENCE IF NOT EXISTS operational.sync_failure_sync_failure_id_seq
    INCREMENT 1
    START 1673
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_failure_sync_failure_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.sync_failure_sync_failure_id_seq TO postgres;

-- Table: operational.sync_failure
CREATE TABLE IF NOT EXISTS operational.sync_failure
(
    sync_failure_id bigint NOT NULL DEFAULT nextval('operational.sync_failure_sync_failure_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    ext_ref_id character varying(32),
	ext_modified_date bigint,
    errormsg character varying(255),
    errortrace text,
	entity json,
	do_action smallint NOT NULL DEFAULT 0,
	attempts int NOT NULL DEFAULT 1,
	resolved boolean NOT NULL DEFAULT FALSE,
    resolved_date timestamp without time zone,
    CONSTRAINT sync_failure_pkey PRIMARY KEY (sync_failure_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_failure
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_failure TO etlsync;
GRANT ALL ON TABLE operational.sync_failure TO postgres;

-- View: operational.sync_manager_progress
CREATE OR REPLACE VIEW operational.sync_manager_progress AS
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
), failures AS (
	select
		dimension_id,
		count(sync_failure_id) as total
	from
		operational.sync_failure
	where
		resolved = FALSE
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors,
	COALESCE(failures.total, 0) as failures
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
	left join failures on sync.dimension_id = failures.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;

-- FUNCTION: operational.getsyncisdirtyifnotexist(bigint)
CREATE OR REPLACE FUNCTION operational.getsyncisdirtyifnotexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=TRUE, is_dirty_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsyncisdirtyifnotexist(bigint)
    OWNER TO postgres;

-- FUNCTION: operational.releasesynclockifexist(bigint, bigint)
CREATE OR REPLACE FUNCTION operational.releasesynclockifexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = TRUE and sync_manager_id = SyncManagerID and lock_task_client_id = $2) THEN 
			UPDATE operational.sync_manager SET is_locked=FALSE, lock_task_client_id=0 WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesynclockifexist(bigint, bigint)
    OWNER TO postgres;

-- FUNCTION: operational.releasesyncisdirtyifexist(bigint)
CREATE OR REPLACE FUNCTION operational.releasesyncisdirtyifexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = TRUE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=FALSE WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesyncisdirtyifexist(bigint)
    OWNER TO postgres;

-- FUNCTION: operational.getsynclockifnotexist(bigint, bigint)
CREATE OR REPLACE FUNCTION operational.getsynclockifnotexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_locked=TRUE, lock_task_client_id=$2, lock_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsynclockifnotexist(bigint, bigint)
    OWNER TO postgres;
	
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = -1) THEN
	INSERT INTO operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) VALUES (-1, '2017-05-20 00:00:00.000000', 'RETRY_QUEUE', 'Retry Failure Queue', 'Retry Failure Queue', 'operational.sync_failure');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = -1) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, lookback_period, initial_load) VALUES (-1, utc(), -1, 0, utc(), 0, false);
END IF;

END
$do$