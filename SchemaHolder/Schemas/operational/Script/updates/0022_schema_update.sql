DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='operational' AND table_name='sync_manager' AND column_name='skip_buffer_count') THEN
	ALTER TABLE sync_manager
	ADD skip_buffer_count SMALLINT NOT NULL DEFAULT 0;
END IF;

END
$do$