DO
$do$
BEGIN


IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 38) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (38, '2017-12-01 00:00:00.000000', 'DIMENSION_CASE_NOTE', 'Dimension Case 
		Note', 'Dimension Case Note', 'public.dim_case_note');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 39) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (39, '2017-12-01 00:00:00.000000', 'DIMENSION_WORKFLOW_INSTANCE', '
		Dimension Workflow Instance', 'Dimension Workflow Instance', 'public.dim_workflow_instance');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 40) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (40, '2017-12-01 00:00:00.000000', 'DIMENSION_VARIABLE_SCHEDULE_TIME', '
		Dimension Variable Schedule Time', 'Dimension Variable Schedule Time', 'public.dim_variable_schedule_time');
END IF;


IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 38) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (38, utc(), 38, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 39) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (39, utc(), 39, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 40) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (40, utc(), 40, 1000, utc(), true);
END IF;

END
$do$