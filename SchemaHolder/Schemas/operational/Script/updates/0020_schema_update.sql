
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_id_update;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS max_to_sync;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS last_sync_records;
ALTER TABLE operational.sync_manager DROP COLUMN IF EXISTS lookback_period;