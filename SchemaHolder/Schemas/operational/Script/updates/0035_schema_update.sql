﻿DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 49) THEN
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (49, utc(), 49, 1000, utc(), true);
END IF;

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 49) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (49, '2017-09-29 00:00:00.000000', 'DIMENSION_DENIAL_REASON', 'Dimension Denial Reason', 'Dimension Denial Reason', 'public.dim_denial_reason');
END IF;

END
$do$
