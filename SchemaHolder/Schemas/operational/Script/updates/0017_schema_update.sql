
INSERT INTO operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) 
SELECT 26, '2017-04-10 00:00:00.000000', 'DIMENSION_CASE_POLICY', 'Dimension Case Policy', 'Dimension Case Policy', 'public.dim_case_policy' 
WHERE NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id=26);

/* insert record for sync manager */
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, lookback_period, initial_load) 
SELECT 26, NOW(), 26, 1000, NOW(), 30000, true 
WHERE NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id=26);
