﻿DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   operational.job
      WHERE  command = 'SELECT ufn_changestream_archive();')
THEN
    INSERT INTO operational.job (job_id,
                             created_date,
                             name,
                             frequency,
                             command,
                             first_run_date,
                             last_run_date,
                             next_run_date,
                             connection_string_name,
                             is_deleted)
VALUES ((select max(job_id)+1 from operational.job),
        now(),
        'Archive Processed Change Stream Records',
        '10m',
        'SELECT ufn_changestream_archive();',
        null,
        null,
        null,
        'DataWarehouseSettings',
        false);
END IF;


END
$do$
