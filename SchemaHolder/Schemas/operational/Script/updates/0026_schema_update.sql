DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 34) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (34, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_NOTE', 'Dimension Employee Note', 'Dimension Employee Note', 'public.dim_employee_note');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 35) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (35, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_NECESSITY', 'Dimension Employee Necessity', 'Dimension Employee Necessity', 'public.dim_employee_necessity');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 36) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (36, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYEE_JOB', 'Dimension Employee Job', 'Dimension Employee Job', 'public.dim_employee_job');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 37) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (37, '2017-11-29 00:00:00.000000', 'DIMENSION_EMPLOYER_SERVICE_OPTION', 'Dimension Employer Service Option', 'Dimension Employer Service Option', 'public.dim_employer_service_option');
END IF;



IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 34) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (34, utc(), 34, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 35) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (35, utc(), 35, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 36) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (36, utc(), 36, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 37) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (37, utc(), 37, 1000, utc(), true);
END IF;

END
$do$