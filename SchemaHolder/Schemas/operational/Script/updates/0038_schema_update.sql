﻿DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 50) THEN
INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (50, utc(), 50, 1000, utc(), true);
END IF;

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 50) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (50, '2018-12-27 00:00:00.000000', 'DIMENSION_CASE_SEGMENT', 'Dimension Case Segment', 'Dimension Case Segment', 'public.dim_case_segment');
END IF;

END
$do$
