DO
$do$
BEGIN

DROP VIEW IF EXISTS operational.sync_manager_progress;

ALTER TABLE operational.sync_manager
	ALTER COLUMN last_sync_records
	SET DATA TYPE json
	USING last_sync_records::json;

TRUNCATE TABLE operational.sync_log_info;

ALTER TABLE operational.sync_log_info
	ALTER COLUMN created_date
	SET DATA TYPE timestamp with time zone
	USING created_date::timestamp with time zone;

create or replace view operational.sync_manager_progress as
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;


	create or replace view operational.sync_manager_progress as
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;

-- HACK: Add 2 default value columns for created_date to the new tables if they exist here.
-- There are too many other changes and versions going on in the public schema so I'm going to update
-- these here if the tables already exist, and if they don't, then they'll create created correctly
-- when that other stuff goes live anyway so no harm-no foul.
ALTER TABLE IF EXISTS public.dim_case_policy
	ADD COLUMN IF NOT EXISTS created_date timestamp with time zone DEFAULT utc();
ALTER TABLE IF EXISTS public.dim_case_policy_usage
	ADD COLUMN IF NOT EXISTS created_date timestamp with time zone DEFAULT utc();

END
$do$
