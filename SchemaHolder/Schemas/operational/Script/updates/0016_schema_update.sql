ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS last_sync_records jsonb;
ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS lookback_period int DEFAULT 30000;
ALTER TABLE sync_manager ADD COLUMN IF NOT EXISTS initial_load boolean DEFAULT FALSE;