DO
$do$
BEGIN


IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 44) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (44, '2017-11-20 00:00:00.000000', 'DIMENSION_CUSTOMER_SERVICE_OPTION', 'Dimension Customer Service Option', 'Dimension Customer Service Option', 'public.dim_customer_service_option');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 45) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (45, '2017-11-20 00:00:00.000000', 'DIMENSION_TEAM_MEMBER', 'Dimension Team Member', 'Dimension Team Member', 'public.public.dim_team_member');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 46) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (46, '2017-11-20 00:00:00.000000', 'DIMENSION_EVENT', 'Dimension Event', 'Dimension Event', 'public.dim_event');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 47) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (47, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT_TYPES', 'Dimension Employer Contact Types', 'Dimension Employer Contact Types', 'public.dim_employer_contact_types');
END IF;


IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 44) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (44, utc(), 44, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 45) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (45, utc(), 45, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 46) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (46, utc(), 46, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 47) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (47, utc(), 47, 1000, utc(), true);
END IF;

END
$do$