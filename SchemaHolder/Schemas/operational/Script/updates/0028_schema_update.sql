DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 41) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (41, '2017-11-20 00:00:00.000000', 'DIMENSION_ROLE', 'Dimension Role', 'Dimension Role', 'public.dim_role');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 42) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (42, '2017-11-20 00:00:00.000000', 'DIMENSION_EMPLOYER_CONTACT', 'Dimension Employer Contact', 'Dimension Employer Contact', 'public.dim_employer_contact');
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.dimension WHERE dimension_id = 43) THEN
	insert into operational.dimension (dimension_id, created_date, code, description, visible_code, table_name) values (43, '2017-11-20 00:00:00.000000', 'DIMENSION_TEAM', 'Dimension Team', 'Dimension Team', 'public.dim_team');
END IF;


IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 41) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (41, utc(), 41, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 42) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (42, utc(), 42, 1000, utc(), true);
END IF;
IF NOT EXISTS (SELECT 1 FROM operational.sync_manager WHERE sync_manager_id = 43) THEN
	INSERT INTO operational.sync_manager (sync_manager_id, created_date, dimension_id, batch_size, last_sync_date_update, initial_load) VALUES (43, utc(), 43, 1000, utc(), true);
END IF;

END
$do$