﻿DO
$do$
BEGIN

IF NOT EXISTS (SELECT 1 FROM operational.job WHERE upper(command) = upper('SELECT UFN_CHANGESTREAM_ARCHIVE()')) THEN
	INSERT
	INTO operational.job (job_id, created_date, name, frequency, command, first_run_date, last_run_date, next_run_date, connection_string_name, is_deleted)
	VALUES ((select (max(job_id) + 1)
			 from operational.job), now(), 'Archive mongo_changestream_log Records', '50m',
			'SELECT UFN_CHANGESTREAM_ARCHIVE()', null, null, null, 'DataWarehouseSettings', false);
END IF;

END
$do$