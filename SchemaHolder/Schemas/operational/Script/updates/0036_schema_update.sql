﻿DO
$do$
BEGIN

IF  EXISTS (SELECT 1 FROM operational.job WHERE upper(command) = upper('REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_organization')) THEN
	UPDATE operational.job
	set command = 'select ufn_tbl_mv_fact_org_basedata_refresh();REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_organization;'
	WHERE upper(command) = upper('REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_organization');
END IF;

END
$do$