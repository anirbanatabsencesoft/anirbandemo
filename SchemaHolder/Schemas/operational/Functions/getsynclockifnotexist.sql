﻿-- FUNCTION: operational.getsynclockifnotexist(bigint, bigint)

-- DROP FUNCTION operational.getsynclockifnotexist(bigint, bigint);

CREATE OR REPLACE FUNCTION operational.getsynclockifnotexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_locked=TRUE, lock_task_client_id=$2, lock_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsynclockifnotexist(bigint, bigint)
    OWNER TO postgres;
