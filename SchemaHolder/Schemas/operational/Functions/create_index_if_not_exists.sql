﻿-- FUNCTION: operational.create_index_if_not_exists(text, text, text)

-- DROP FUNCTION operational.create_index_if_not_exists(text, text, text);

CREATE OR REPLACE FUNCTION operational.create_index_if_not_exists(
	t_name text,
	i_name text,
	index_sql text)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

DECLARE
  full_index_name varchar;
  schema_name varchar;
BEGIN

full_index_name = t_name || '_' || i_name;
schema_name = 'public';

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = full_index_name
    AND    n.nspname = schema_name
    ) THEN

    execute 'CREATE INDEX ' || full_index_name || ' ON ' || schema_name || '.' || t_name || ' ' || index_sql;
END IF;
END

$function$;

ALTER FUNCTION operational.create_index_if_not_exists(text, text, text)
    OWNER TO postgres;
