﻿-- FUNCTION: operational.releasesynclockifexist(bigint, bigint)

-- DROP FUNCTION operational.releasesynclockifexist(bigint, bigint);

CREATE OR REPLACE FUNCTION operational.releasesynclockifexist(
	syncmanagerid bigint,
	locktaskclientid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_locked = TRUE and sync_manager_id = SyncManagerID and lock_task_client_id = $2) THEN 
			UPDATE operational.sync_manager SET is_locked=FALSE, lock_task_client_id=0 WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesynclockifexist(bigint, bigint)
    OWNER TO postgres;
