﻿CREATE OR REPLACE FUNCTION operational.active_row_count(relname TEXT) RETURNS BIGINT
AS
$body$
DECLARE
  RESULT BIGINT;
BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE column_name = 'ver_is_current' AND table_name = relname) THEN
    EXECUTE ('select count(*) from ' || relname) INTO RESULT;
  ELSE
    EXECUTE ('select count(*) from ' || relname || ' where ver_is_current') INTO RESULT;
  END IF;

  RETURN RESULT;
END;
$body$
LANGUAGE plpgsql;

ALTER FUNCTION operational.active_row_count(text)
    OWNER TO postgres;