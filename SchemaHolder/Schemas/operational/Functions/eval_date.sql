﻿-- FUNCTION: operational.eval_date(text)

-- DROP FUNCTION operational.eval_date(text);

create or replace function operational.eval_date(expression text) returns timestamp without time zone
as
$body$
declare
  result timestamp without time zone;
begin
  execute expression into result;
  return result;
end;
$body$
language plpgsql;
