﻿-- FUNCTION: operational.releasesyncisdirtyifexist(bigint)

-- DROP FUNCTION operational.releasesyncisdirtyifexist(bigint);

CREATE OR REPLACE FUNCTION operational.releasesyncisdirtyifexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = TRUE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=FALSE WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.releasesyncisdirtyifexist(bigint)
    OWNER TO postgres;
