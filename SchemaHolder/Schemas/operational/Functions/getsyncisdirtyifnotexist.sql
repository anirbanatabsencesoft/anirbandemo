﻿-- FUNCTION: operational.getsyncisdirtyifnotexist(bigint)

-- DROP FUNCTION operational.getsyncisdirtyifnotexist(bigint);

CREATE OR REPLACE FUNCTION operational.getsyncisdirtyifnotexist(
	syncmanagerid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
        IF EXISTS (SELECT 1 FROM operational.sync_manager WHERE is_dirty = FALSE and sync_manager_id = SyncManagerID) THEN 
			UPDATE operational.sync_manager SET is_dirty=TRUE, is_dirty_date=CURRENT_TIMESTAMP WHERE sync_manager_id = $1;
        END IF;
END;

$function$;

ALTER FUNCTION operational.getsyncisdirtyifnotexist(bigint)
    OWNER TO postgres;
