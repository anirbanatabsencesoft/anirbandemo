﻿-- FUNCTION: operational.eval_interval(text)

-- DROP FUNCTION operational.eval_interval(text);

create or replace function operational.eval_interval(expression text) returns interval
as
$body$
declare
  result interval;
begin
  execute expression into result;
  return result;
end;
$body$
language plpgsql;
