﻿CREATE OR REPLACE FUNCTION operational.utc()
   RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'
   AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$;