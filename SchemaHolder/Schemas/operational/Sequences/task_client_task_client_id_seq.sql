﻿CREATE SEQUENCE operational.task_client_task_client_id_seq
    INCREMENT 1
    START 26
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.task_client_task_client_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.task_client_task_client_id_seq TO etlsync;

GRANT ALL ON SEQUENCE operational.task_client_task_client_id_seq TO postgres;