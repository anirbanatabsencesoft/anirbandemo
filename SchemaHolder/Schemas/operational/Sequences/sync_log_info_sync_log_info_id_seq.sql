﻿CREATE SEQUENCE operational.sync_log_info_sync_log_info_id_seq
    INCREMENT 1
    START 38348876
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.sync_log_info_sync_log_info_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.sync_log_info_sync_log_info_id_seq TO etlsync;

GRANT ALL ON SEQUENCE operational.sync_log_info_sync_log_info_id_seq TO postgres;