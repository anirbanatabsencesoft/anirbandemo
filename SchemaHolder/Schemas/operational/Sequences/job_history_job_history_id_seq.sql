﻿CREATE SEQUENCE IF NOT EXISTS operational.job_history_job_history_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE operational.job_history_job_history_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO etlsync;
GRANT ALL ON SEQUENCE operational.job_history_job_history_id_seq TO postgres;