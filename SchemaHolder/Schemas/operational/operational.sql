﻿-- SCHEMA: operational

-- DROP SCHEMA operational ;

CREATE SCHEMA IF NOT EXISTS operational;

DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etldw')
THEN
    CREATE ROLE etldw LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etlsync')
THEN
    CREATE ROLE etlsync LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'absence_tracker')
THEN
    CREATE ROLE absence_tracker LOGIN PASSWORD 'password_goes_here';
END IF;


END
$do$;

GRANT ALL ON SCHEMA operational TO PUBLIC;

GRANT ALL ON SCHEMA operational TO etlsync;

ALTER DEFAULT PRIVILEGES IN SCHEMA operational
GRANT ALL ON TABLES TO etlsync;

ALTER DEFAULT PRIVILEGES IN SCHEMA operational
GRANT SELECT, USAGE ON SEQUENCES TO etlsync;