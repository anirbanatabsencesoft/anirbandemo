﻿-- View: operational.sync_manager_progress

-- DROP VIEW operational.sync_manager_progress;

CREATE OR REPLACE VIEW operational.sync_manager_progress AS
with errors AS (
	select
		dimension_id,
		count(sync_log_error_id) as total
	from
		operational.sync_log_error
	where
		errortrace not like '{Error calculating%'
	group by
		dimension_id
), infos AS (
	select
		dimension_id,
		to_char(utc() - max(created_date), 'HH24:MI:SS') as time_since_last_activity
	from
		operational.sync_log_info
	group by
		dimension_id
), failures AS (
	select
		dimension_id,
		count(sync_failure_id) as total
	from
		operational.sync_failure
	where
		resolved = FALSE
	group by
		dimension_id
)
select
	sync.dimension_id,
	dim.code, 
	sync.last_sync_date_update, 
	sync.source_count as "in_queue",
	sync.batch_size,
	operational.eval_date('select min(created_date) from ' || dim."table_name") as first_record,
	operational.eval_date('select max(created_date) from ' || dim."table_name") as last_record,
	to_char(operational.eval_interval('select max(created_date) - min(created_date) from ' || dim."table_name"), 'HH24:MI:SS') as elapsed,
	infos.time_since_last_activity,
	COALESCE(errors.total, 0) as errors,
	COALESCE(failures.total, 0) as failures,
	COALESCE(operational.active_row_count(dim."table_name"), 0) as active_rows
from
	operational.sync_manager as sync
	inner join operational.dimension as dim on sync.dimension_id = dim.dimension_id
	left join errors on sync.dimension_id = errors.dimension_id
	left join infos on sync.dimension_id = infos.dimension_id
	left join failures on sync.dimension_id = failures.dimension_id
order by
	sync.source_count desc;
	
ALTER TABLE operational.sync_manager_progress
    OWNER to postgres;