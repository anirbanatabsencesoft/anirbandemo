﻿-- Table: operational.dimension

-- DROP TABLE operational.dimension;

CREATE TABLE operational.dimension
(
    dimension_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    code character varying(255),
    description character varying(255),
    visible_code character varying(255),
	table_name character varying(255),
    CONSTRAINT dimension_pkey PRIMARY KEY (dimension_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.dimension
    OWNER to postgres;

GRANT ALL ON TABLE operational.dimension TO etlsync;
GRANT ALL ON TABLE operational.dimension TO postgres;