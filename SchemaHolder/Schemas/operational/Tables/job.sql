﻿-- Table: operational.job

-- DROP TABLE operational.job;

CREATE TABLE IF NOT EXISTS operational.job
(
    job_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	name text,
    frequency text,
    command text,
    first_run_date timestamp without time zone,
	last_run_date timestamp without time zone,
	next_run_date timestamp without time zone,
	connection_string_name text,
	is_deleted boolean DEFAULT false,
    CONSTRAINT job_pkey PRIMARY KEY (job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job
    OWNER to postgres;

GRANT ALL ON TABLE operational.job TO etlsync;
GRANT ALL ON TABLE operational.job TO postgres;