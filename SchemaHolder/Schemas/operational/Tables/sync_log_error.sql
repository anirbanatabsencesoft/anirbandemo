﻿-- Table: operational.sync_log_error

-- DROP TABLE operational.sync_log_error;

CREATE TABLE operational.sync_log_error
(
    sync_log_error_id bigint NOT NULL DEFAULT nextval('operational.sync_log_error_sync_log_error_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    msgsource character varying(255),
    errormsg character varying(255),
    errortrace text,
    CONSTRAINT sync_log_error_pkey PRIMARY KEY (sync_log_error_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_log_error
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_log_error TO etlsync;
GRANT ALL ON TABLE operational.sync_log_error TO postgres;