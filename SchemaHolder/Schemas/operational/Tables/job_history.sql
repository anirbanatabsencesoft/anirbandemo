﻿-- Table: operational.job_history

-- DROP TABLE operational.job_history;

CREATE TABLE IF NOT EXISTS operational.job_history
(
	job_history_id bigint NOT NULL DEFAULT nextval('operational.job_history_job_history_id_seq'::regclass),
    job_id bigint NOT NULL,
	name text,
    command text,
    start_date timestamp without time zone NOT NULL DEFAULT utc(),
	end_date timestamp without time zone,
	status text,
	is_error boolean DEFAULT false,
	error text,
    CONSTRAINT job_history_pkey PRIMARY KEY (job_history_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.job_history
    OWNER to postgres;

GRANT ALL ON TABLE operational.job_history TO etlsync;
GRANT ALL ON TABLE operational.job_history TO postgres;