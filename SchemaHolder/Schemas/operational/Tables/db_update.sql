﻿-- Table: operational.db_update

-- DROP TABLE operational.db_update;

CREATE TABLE operational.db_update
(
    db_update_id bigint DEFAULT nextval('db_update_seq'::regclass) NOT NULL,
	db_update_record_type text NOT NULL,
	db_update_filename text,
	db_update_file_hash text,
	db_update_created_date timestamp with time zone DEFAULT now(),
	db_update_modified_date timestamp with time zone,
	db_update_ignore boolean,
	PRIMARY KEY(db_update_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.db_update
    OWNER to postgres;

GRANT ALL ON TABLE operational.db_update TO etlsync;
GRANT ALL ON TABLE operational.db_update TO postgres;