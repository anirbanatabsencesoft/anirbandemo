﻿-- Table: operational.sync_manager

-- DROP TABLE operational.sync_manager;

CREATE TABLE operational.sync_manager
(
    sync_manager_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    last_sync_date_update timestamp without time zone,
    dimension_id bigint DEFAULT 0,
    batch_size integer DEFAULT 0,
    is_locked boolean DEFAULT false,
    lock_task_client_id bigint DEFAULT 0,
    is_suspended boolean DEFAULT false,
    suspended_master_index_id bigint DEFAULT 0,
    lock_date timestamp without time zone,
    suspend_task_client_id bigint DEFAULT 0,
    suspend_date timestamp without time zone,
    is_dirty boolean DEFAULT false,
    is_dirty_date timestamp without time zone,
    source_count bigint DEFAULT 0,
	initial_load boolean DEFAULT TRUE,
	max_failure_attempts int NOT NULL DEFAULT 1,
	default_failure_action smallint NOT NULL DEFAULT 0,
	skip_buffer_count smallint NOT NULL DEFAULT 0,
    CONSTRAINT sync_manager_pkey PRIMARY KEY (sync_manager_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_manager
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_manager TO etlsync;
GRANT ALL ON TABLE operational.sync_manager TO postgres;