﻿-- Table: operational.sync_log_info

-- DROP TABLE operational.sync_log_info;

CREATE TABLE operational.sync_log_info
(
    sync_log_info_id bigint NOT NULL DEFAULT nextval('operational.sync_log_info_sync_log_info_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    msgsource character varying(255),
    msgaction character varying(255),
    msgtxt character varying(255),
    CONSTRAINT sync_log_info_pkey PRIMARY KEY (sync_log_info_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_log_info
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_log_info TO etlsync;

GRANT ALL ON TABLE operational.sync_log_info TO postgres;