﻿-- Table: operational.dbversion

-- DROP TABLE operational.dbversion;

CREATE TABLE operational.dbversion
(
    dbversion_id bigint NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    major_num integer DEFAULT 0,
    minur_num integer DEFAULT 0,
    notes text,
    CONSTRAINT dbversion_pkey PRIMARY KEY (dbversion_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.dbversion
    OWNER to postgres;

GRANT ALL ON TABLE operational.dbversion TO etlsync;
GRANT ALL ON TABLE operational.dbversion TO postgres;