﻿-- Table: operational.task_client

-- DROP TABLE operational.task_client;

CREATE TABLE operational.task_client
(
    task_client_id bigint NOT NULL DEFAULT nextval('operational.task_client_task_client_id_seq'::regclass),
    sync_manager_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
    modified_date timestamp without time zone,
    init_register_date timestamp without time zone,
    init_processing_date timestamp without time zone,
    last_process_date timestamp without time zone,
    last_heartbeat_date timestamp without time zone,
    process_id character varying(255),
    process_name character varying(255),
    connstring character varying(255),
    heartbeat_interval_in_ms integer DEFAULT 0,
    check_interval_in_ms integer DEFAULT 0,
    is_disabled boolean,
    api_settings character varying(255),
    notes text,
    CONSTRAINT task_client_pkey PRIMARY KEY (task_client_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.task_client
    OWNER to postgres;

GRANT ALL ON TABLE operational.task_client TO etlsync;
GRANT ALL ON TABLE operational.task_client TO postgres;