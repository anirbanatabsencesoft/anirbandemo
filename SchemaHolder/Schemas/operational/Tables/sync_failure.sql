﻿-- Table: operational.sync_failure

-- DROP TABLE operational.sync_failure;

CREATE TABLE IF NOT EXISTS operational.sync_failure
(
    sync_failure_id bigint NOT NULL DEFAULT nextval('operational.sync_failure_sync_failure_id_seq'::regclass),
    dimension_id bigint,
    created_date timestamp without time zone NOT NULL DEFAULT utc(),
	modified_date timestamp without time zone NOT NULL DEFAULT utc(),
    ext_ref_id character varying(32),
	ext_modified_date bigint,
    errormsg character varying(255),
    errortrace text,
	entity json,
	do_action smallint NOT NULL DEFAULT 0,
	attempts int NOT NULL DEFAULT 1,
	resolved boolean NOT NULL DEFAULT FALSE,
    resolved_date timestamp without time zone,
    CONSTRAINT sync_failure_pkey PRIMARY KEY (sync_failure_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE operational.sync_failure
    OWNER to postgres;

GRANT ALL ON TABLE operational.sync_failure TO etlsync;
GRANT ALL ON TABLE operational.sync_failure TO postgres;