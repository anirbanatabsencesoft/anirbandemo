﻿-- Data: operational.job

insert into operational.job (job_id, name, frequency, command, connection_string_name) values (1, 'Refresh Case Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_case', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (2, 'Refresh Customer Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_customer', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (3, 'Refresh Employee Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_employee', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (4, 'Refresh Employer Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_employer', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (5, 'Refresh Org Facts', '1h',  'select ufn_tbl_mv_fact_org_basedata_refresh();REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_organization;', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (6, 'Refresh User Facts', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_fact_user', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (7, 'Refresh Employee Supervisors', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_supervisor', 'DataWarehouseSettings');
insert into operational.job (job_id, name, frequency, command, connection_string_name) values (8, 'Refresh Employee HR', '30m', 'REFRESH MATERIALIZED VIEW CONCURRENTLY public.mview_hr', 'DataWarehouseSettings');
