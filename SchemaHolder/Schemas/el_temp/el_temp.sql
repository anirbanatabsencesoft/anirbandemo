﻿CREATE SCHEMA IF NOT EXISTS el_temp;

COMMENT ON SCHEMA el_temp
   IS 'el_temp tables stored under this schema';
GRANT ALL ON SCHEMA el_temp TO postgres;

GRANT USAGE ON SCHEMA el_temp TO absence_tracker;

GRANT USAGE ON SCHEMA el_temp TO PUBLIC;

GRANT USAGE ON SCHEMA el_temp TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA el_temp
GRANT SELECT ON TABLES TO absence_tracker;