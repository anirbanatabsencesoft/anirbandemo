﻿  
# This script needs to work if its ran locally using relative paths. 
# And work using absolute paths when automated in TeamCity

param(
   [string]$Path
)

if($Path){
	$schema_folder = $Path
	$script_folder = $Path+'\'+'Script'
}


New-Item -ItemType file "$script_folder\Schema.sql" –force

Get-Content $schema_folder\el_temp.sql | Add-Content $script_folder\Schema.sql
Get-Content $script_folder\Template\file_beg.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Sequences\*.sql | Add-Content $script_folder\Schema.sql
#Get-Content $schema_folder\Functions\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Tables\*.sql | Add-Content $script_folder\Schema.sql
#Get-Content "..\Materialized Views\*.sql" | Add-Content .\Schema.sql
#Get-Content $schema_folder\Data\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $script_folder\Template\file_end.sql | Add-Content $script_folder\Schema.sql
