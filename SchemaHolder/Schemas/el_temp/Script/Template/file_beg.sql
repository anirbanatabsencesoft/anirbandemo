/******************************************************************************
**		File: Schema.sql
**		Desc: Schema implementation.  Creates complete <<SCHEMA_NAME>> schema for Postgresql db
**				Absencesoft ETL project.
**
**		Auth: <<AUTHOR>>
**		Date: <<DATE>>
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		<<DATE>>		<<AUTHOR_INITIALS>>		Created
**    
*******************************************************************************/

set schema 'audit';

DO
$do$
BEGIN