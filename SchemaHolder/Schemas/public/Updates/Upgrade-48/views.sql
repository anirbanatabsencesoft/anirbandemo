﻿
set schema 'public';

DO
$do$
BEGIN

CREATE OR REPLACE VIEW public.view_case AS
 SELECT 
	view_empl.*,
    dim_cases.ext_ref_id AS case_id,
    dim_cases.case_number AS case_case_number,
    dim_cases.employer_case_number AS case_employer_case_number,
    dim_cases.start_date AS case_case_start_date,
        CASE
            WHEN dim_cases.end_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.end_date
        END AS case_case_end_date,
    dim_lookup_case_status.description AS case_case_status,
    dim_cases.spouse_case AS case_spouse_case_number,
    dim_lookup_case_types.description AS case_case_type,
    dim_cases.case_reason,
    dim_lookup_case_cancel_reason.description AS case_cancel_reason,
    dim_cases.assigned_to_name AS case_assigned_to,
    dim_lookup_case_closure_reason.description AS case_closed_reason,
    dim_cases.case_primary_diagnosis_code,
    dim_cases.case_primary_daignosis_name,
    dim_cases.case_secondary_diagnosis_code,
    dim_cases.case_secondary_daignosis_name,
    dim_cases.primary_path_text AS case_primary_rtw_path,
    dim_cases.primary_path_min_days AS case_primary_rtw_path_min_days,
    dim_cases.primary_path_max_days AS case_primary_rtw_path_max_days,
    dim_cases.hospitalization AS case_hospitalization,
        CASE
            WHEN dim_cases.condition_start_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.condition_start_date
        END AS case_condition_start_date,
    dim_cases.related_case_number AS case_related_case_number,
    dim_lookup_case_related_type.description AS case_related_case_type,
	dim_cases.relationship_type_code AS case_relationship_type_code,
	dim_cases.relationship_type_name AS case_relationship_type_name,
	dim_cases.is_intermittent AS case_is_intermittent,
    dim_cases.case_cert_start_date,
    dim_cases.case_cert_start_date,
    dim_cases.occurances AS case_cert_occurances,
    dim_cases.frequency AS case_cert_frequency,
    dim_lookup_case_frequency_type.description AS case_cert_frequency_type,
    dim_lookup_case_frequency_unit_type.description AS case_cert_frequency_unit_type,
    dim_cases.duration AS case_cert_duration,
    dim_lookup_case_duration_type.description AS case_cert_duration_type,
    dim_cases.is_accommodation AS case_is_accommodation,
    dim_cases.is_work_related AS case_is_work_related,
    dim_cases.case_is_denied,
    dim_cases.case_is_approved,
		CASE
            WHEN dim_cases.case_is_eligible = 1 THEN 'True'::text 
			ELSE 'False'::text
		END AS case_is_eligible,
	dim_lookup_case_determination.description AS case_case_determination,
    dim_cases.case_fmla_is_approved,
    dim_cases.case_fmla_is_denied,
    dim_cases.case_fmla_projected_usage,
        CASE
            WHEN dim_cases.case_fmla_exhaust_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_fmla_exhaust_date
        END AS case_fmla_exhaust_date,
        CASE
            WHEN dim_cases.case_max_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_max_approved_thru_date
        END AS case_case_max_approved_thru_date,
        CASE
            WHEN dim_cases.case_min_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_min_approved_thru_date
        END AS case_case_min_approved_thru_date,
    dim_lookup_case_accom_status.description AS case_accom_status,
    dim_cases.case_accom_start_date,
    dim_cases.case_accom_end_date,
        CASE
            WHEN dim_cases.case_accom_min_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_min_approved_date
        END AS case_accom_min_approved_date,
        CASE
            WHEN dim_cases.case_accom_max_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_max_approved_date
        END AS case_accom_max_approved_date,
    dim_lookup_case_accom_determination.description AS case_accom_determination,
    dim_lookup_case_accom_cancel_reason.description AS case_accom_cancel_reason,
    dim_cases.case_accom_cancel_reason_other,
    dim_cases.case_accom_duration,
    dim_cases.case_accom_duration_type,
    dim_cases.ext_created_date AS case_case_created_date,
    dim_cases.case_created_by_email AS case_case_created_by_email,
    dim_cases.case_created_by_first_name AS case_case_created_by_first_name,
    dim_cases.case_created_by_last_name AS case_case_created_by_last_name,
    fact_case.return_to_work AS case_return_to_work,
    fact_case.delivery_date AS case_delivery_date,
    fact_case.bonding_start_date AS case_bonding_start_date,
    fact_case.bonding_end_date AS case_bonding_end_date,
    fact_case.illness_or_injury_date AS case_illness_or_injury_date,
    fact_case.hospital_admission_date AS case_hospital_admission_date,
    fact_case.hospital_release_date AS case_hospital_release_date,
    fact_case.release_received AS case_release_received,
    fact_case.paperwork_received AS case_paperwork_received,
    fact_case.accommodation_added AS case_accommodation_added,
    fact_case.case_closed AS case_case_closed,
    fact_case.case_cancelled AS case_case_cancelled,
    fact_case.adoption_date AS case_adoption_date,
    fact_case.estimated_return_to_work AS case_estimated_return_to_work,
    fact_case.employer_holiday_schedule_changed AS case_employer_holiday_schedule_changed,
    COALESCE(fact_case.case_count_todos, 0::bigint) AS case_case_todos,
    COALESCE(fact_case.case_count_open_todos, 0::bigint) AS case_case_open_todos,
    COALESCE(fact_case.case_count_closed_todos, 0::bigint) AS case_case_closed_todos,
    COALESCE(fact_case.case_count_overdue_todos, 0::bigint) AS case_case_overdue_todos,
    dim_cases.case_reporter_type_code,
    dim_cases.case_reporter_type_name,
    dim_cases.case_reporter_first_name,
    dim_cases.case_reporter_last_name,
    dim_cases.custom_fields AS case_custom_fields
   FROM dim_cases
     LEFT JOIN fact_case ON dim_cases.ext_ref_id = fact_case.case_ref_id
     LEFT JOIN view_empl ON dim_cases.employee_id = view_empl.empl_id
     LEFT JOIN dim_lookup_case_accom_status ON dim_cases.case_accom_status = dim_lookup_case_accom_status.code
     LEFT JOIN dim_lookup_case_accom_cancel_reason ON dim_cases.case_accom_cancel_reason = dim_lookup_case_accom_cancel_reason.code
     LEFT JOIN dim_lookup_case_accom_determination ON dim_cases.case_accom_determination = dim_lookup_case_accom_determination.code
     LEFT JOIN dim_lookup_case_determination ON dim_cases.case_determination = dim_lookup_case_determination.code
     LEFT JOIN dim_lookup_case_duration_type ON dim_cases.duration_type = dim_lookup_case_duration_type.code
     LEFT JOIN dim_lookup_case_frequency_type ON dim_cases.frequency_type = dim_lookup_case_frequency_type.code
     LEFT JOIN dim_lookup_case_frequency_unit_type ON dim_cases.frequency_unit_type = dim_lookup_case_frequency_unit_type.code
     LEFT JOIN dim_lookup_case_status ON dim_cases.status = dim_lookup_case_status.code
     LEFT JOIN dim_lookup_case_types ON dim_cases.type = dim_lookup_case_types.code
     LEFT JOIN dim_lookup_case_cancel_reason ON dim_cases.cancel_reason = dim_lookup_case_cancel_reason.code
     LEFT JOIN dim_lookup_case_closure_reason ON dim_cases.case_closure_reason = dim_lookup_case_closure_reason.code
     LEFT JOIN dim_lookup_case_related_type ON dim_cases.related_case_type = dim_lookup_case_related_type.code
  WHERE dim_cases.ver_is_current = true
     AND dim_cases.is_deleted = false;

ALTER TABLE public.view_case
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_case TO postgres;
GRANT SELECT ON TABLE public.view_case TO absence_tracker;
GRANT ALL ON TABLE public.view_case TO etldw;

END
$do$