CREATE OR REPLACE FUNCTION public.show_customerdetails()
  RETURNS TABLE(customerid bigint, customername character varying, employer character varying, employee_count_6month bigint, employee_count_3month bigint, employee_count_1month bigint, case_count bigint, todo_count bigint, active_users bigint) AS
 $BODY$
    BEGIN
		return  query 
		WITH grouped_employee_count_6 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '6 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id), 
		grouped_employee_count_3 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '3 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id), 
		grouped_employee_count_1 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '1 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id) , 
		grouped_case_count AS (SELECT customer_id, employer_id FROM dim_cases WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND  ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) , 
		grouped_todo_count AS (SELECT customer_id, employer_id FROM dim_todoitem WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) ,
		grouped_active_users AS (SELECT customer_id, employer_ids FROM dim_Users WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_ids)  
	   
		SELECT cust.dim_customer_ID, CAST(cust.cust_customer_name AS VARCHAR) customer, CAST(empr.NAME AS VARCHAR) Employer,
		(SELECT count(1) FROM grouped_employee_count_6 ec_6 WHERE ec_6.customer_id = cust.ext_ref_id AND ec_6.employer_id = empr.ext_ref_id) Employee_Count_6month,
		(SELECT count(1) FROM grouped_employee_count_3 ec_3 WHERE ec_3.customer_id = cust.ext_ref_id AND ec_3.employer_id = empr.ext_ref_id) Employee_Count_3month,
		(SELECT count(1) FROM grouped_employee_count_1 ec_1 WHERE ec_1.customer_id = cust.ext_ref_id AND ec_1.employer_id = empr.ext_ref_id) Employee_Count_1month,
		(SELECT count(1) FROM grouped_case_count cc  WHERE cc.customer_id = cust.ext_ref_id AND cc.employer_id = empr.ext_ref_id) Case_Count,
		(SELECT count(1) FROM grouped_todo_count tc  WHERE tc.customer_id = cust.ext_ref_id AND tc.employer_id = empr.ext_ref_id) Todo_Count,
		(SELECT count(1) FROM grouped_active_users au  WHERE au.customer_id = cust.ext_ref_id AND empr.ext_ref_id = ANY(au.employer_ids::varchar[])) Active_Users
		FROM dim_customer cust
		INNER JOIN dim_employer empr ON cust.ext_ref_id = empr.customer_id
		WHERE cust.ver_is_current = TRUE AND empr.ver_is_current = TRUE  AND cust.Is_Deleted = FALSE AND empr.Is_Deleted = FALSE
		GROUP BY cust.dim_customer_ID, cust.cust_customer_name, empr.NAME, empr.ext_ref_id
		ORDER BY 1;
    END;
  $BODY$
  LANGUAGE plpgsql;