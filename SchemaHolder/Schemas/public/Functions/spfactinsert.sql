﻿-- FUNCTION: public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying)

-- DROP FUNCTION public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.spfactinsert(
	factid integer DEFAULT 0,
	datecreated timestamp without time zone DEFAULT NULL::timestamp without time zone,
	code character varying DEFAULT NULL::character varying,
	description character varying DEFAULT NULL::character varying,
	visiblecode character varying DEFAULT NULL::character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
INSERT INTO fact
(
	fact_id,
	date_created,
	code,
	description,
	visible_code
)
 VALUES 
(
	FactID,
	DateCreated,
	Code,
	Description,
	VisibleCode
);
END;

$function$;

ALTER FUNCTION public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying)
    OWNER TO postgres;
