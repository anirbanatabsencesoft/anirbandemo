﻿-- FUNCTION: public.function_exists(text, text)

-- DROP FUNCTION public.function_exists(text, text);

CREATE OR REPLACE FUNCTION public.function_exists(
	sch text,
	fun text)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
	EXECUTE  'select pg_get_functiondef('''||sch||'.'||fun||'''::regprocedure)';
	RETURN true;
	exception when others then 
	RETURN false;
END;

$function$;

ALTER FUNCTION public.function_exists(text, text)
    OWNER TO postgres;
