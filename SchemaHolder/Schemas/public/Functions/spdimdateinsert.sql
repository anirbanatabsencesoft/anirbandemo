﻿-- FUNCTION: public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit)

-- DROP FUNCTION public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit);

CREATE OR REPLACE FUNCTION public.spdimdateinsert(
	iso8601date timestamp without time zone DEFAULT NULL::timestamp without time zone,
	year integer DEFAULT NULL::integer,
	month integer DEFAULT NULL::integer,
	day integer DEFAULT NULL::integer,
	minute integer DEFAULT NULL::integer,
	second integer DEFAULT NULL::integer,
	weekend bit DEFAULT NULL::"bit",
	weekday bit DEFAULT NULL::"bit",
	holiday bit DEFAULT NULL::"bit")
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
INSERT INTO dim_date
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
)
 VALUES 
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
);
END;

$function$;

ALTER FUNCTION public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit)
    OWNER TO postgres;
