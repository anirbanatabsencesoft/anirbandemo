﻿-- FUNCTION: public.custom_field(json, text)

-- DROP FUNCTION public.custom_field(json, text);

CREATE OR REPLACE FUNCTION public.custom_field(
	custom_fields jsonb,
	code text)
    RETURNS text
    LANGUAGE 'plpgsql'
AS $function$

BEGIN
	RETURN CASE WHEN custom_fields IS NULL THEN null::text ELSE custom_fields ->> code END;
END 

$function$;

ALTER FUNCTION public.custom_field(jsonb, text)
    OWNER TO postgres;