﻿-- FUNCTION: public.json_array_map(json, text[])

-- DROP FUNCTION public.json_array_map(json, text[]);

CREATE OR REPLACE FUNCTION public.json_array_map(
	json_arr json,
	path text[])
    RETURNS json[]
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

DECLARE
	rec json;
	len int;
	ret json[];
BEGIN
	-- If json_arr is not an array, return an empty array as the result
	BEGIN
		len := json_array_length(json_arr);
	EXCEPTION
		WHEN OTHERS THEN
			RETURN ret;
	END;

	-- Apply mapping in a loop
	FOR rec IN SELECT json_array_elements#>path FROM json_array_elements(json_arr)
	LOOP
		ret := array_append(ret,rec);
	END LOOP;
	RETURN ret;
END 
$function$;

ALTER FUNCTION public.json_array_map(json, text[])
    OWNER TO postgres;
