﻿-- FUNCTION: public.delete_non_current_records()

-- DROP FUNCTION public.delete_non_current_records();

CREATE OR REPLACE FUNCTION public.delete_non_current_records()
 RETURNS text
 LANGUAGE plpgsql
AS $function$ DECLARE tables CURSOR FOR
SELECT
    tablename
FROM
    pg_tables
WHERE
    tablename NOT LIKE 'pg_%'
    AND tablename LIKE 'dim_%'
    AND tablename NOT LIKE 'dim_lookup%'
    AND tablename <> 'dimension'
    and tablename not in (
        'dim_accommodation_usage', 'dim_date',
        'dim_employee_restriction_entry',
        'dim_case_certification'
    )
ORDER BY
    tablename; nbRow int;

     BEGIN FOR table_record IN tables LOOP EXECUTE 'DELETE FROM ' || table_record.tablename || ' where ver_is_current = False';
    END LOOP;

    Return 'Completed';

    END; 
$function$;

ALTER FUNCTION public.delete_non_current_records()
    OWNER TO postgres;