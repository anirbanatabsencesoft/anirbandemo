CREATE OR REPLACE FUNCTION public.show_customerdetails_new(IN p_date date)
    RETURNS TABLE (
        customerid int8
        ,customername varchar
        ,employer varchar
        ,employee_count_current_month int8
        ,case_count_current_month int8
        ,todo_count_current_month int8
        ,active_users_current_month int8
    ) AS 
$BODY$
BEGIN

    RETURN   query
    SELECT   cust.dim_customer_id customerid
            ,cust.cust_customer_name::varchar customername
            ,empr."name"::varchar employer
            ,(  SELECT  COUNT(distinct emp.employee_number)
                FROM    public.dim_employee emp
                WHERE   emp.customer_id = cust.ext_ref_id 
                        AND emp.employer_id = empr.ext_ref_id 
                        AND emp.ext_created_date::date <= p_date
                        AND emp.ext_modified_date::date <= p_date
                        AND NOT emp.is_deleted 
                        AND emp.status IN (65,76)
             ) employee_count_current_month
            ,(  SELECT  COUNT(distinct cases.case_number) 
                FROM    public.dim_cases cases
                WHERE   cases.customer_id = cust.ext_ref_id 
                        AND cases.employer_id = empr.ext_ref_id 
                        AND cases.ext_created_date::date <= p_date
                        AND cases.ext_modified_date::date <= p_date
                        AND NOT cases.is_deleted
             ) case_count_current_month
            ,(  SELECT  COUNT(distinct todo.ext_ref_id) 
                FROM    public.dim_todoitem todo
                WHERE   todo.customer_id = cust.ext_ref_id 
                        AND todo.employer_id = empr.ext_ref_id 
                        AND todo.ext_created_date::date <= p_date
                        AND todo.ext_modified_date::date <= p_date
                        AND NOT todo.is_deleted
             ) todo_count_current_month
            ,(  SELECT  COUNT(distinct usr.email) 
                FROM    public.dim_users usr
                WHERE   usr.customer_id = cust.ext_ref_id 
                        AND empr.ext_ref_id = ANY(usr.employer_ids::varchar[]) 
                        AND usr.ext_created_date::date <= p_date
                        AND usr.ext_modified_date::date <= p_date
                        AND NOT usr.is_deleted
             ) active_users_current_month
    FROM    public.dim_customer cust
            INNER JOIN public.dim_employer empr 
                ON cust.ext_ref_id = empr.customer_id
                AND empr.ver_is_current
                AND NOT empr.is_deleted
    WHERE   cust.ver_is_current
            AND NOT cust.is_deleted
    ORDER   BY 2;

END;
$BODY$
  LANGUAGE 'plpgsql'
;

ALTER FUNCTION public.show_customerdetails_new(IN p_date date) OWNER TO postgres;


