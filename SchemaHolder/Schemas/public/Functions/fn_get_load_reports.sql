--DROP FUNCTION public.fn_get_load_reports(bigint, character varying, bigint, character varying);

-- Create the function again just add OR condition on UPDATE part where customer_id IS NULL
-- FUNCTION: public.fn_get_load_reports(bigint, character varying, bigint, character varying)

CREATE OR REPLACE FUNCTION public.fn_get_load_reports(
	customerid bigint DEFAULT NULL::bigint,
	customerkey character varying DEFAULT NULL::character varying,
	userid bigint DEFAULT NULL::bigint,
	userkey character varying DEFAULT NULL::character varying)
    RETURNS TABLE(id bigint, report_name character varying, ui_sequence integer, report_category character varying, report_class_fqn character varying, report_json text, customer_id bigint, customer_key character varying, shared_with character, scheduled_report_id bigint, created_by_id bigint, created_by_key character varying, created_on timestamp with time zone, modified_on timestamp with time zone, recipient_id bigint, team_name character varying, team_id bigint, team_key character varying, role_name character varying, role_id bigint, user_key character varying, user_id bigint, request_id bigint, json_path character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

		BEGIN
			RETURN QUERY		
				SELECT 
					 sr.id
					,sr.report_name
					,sr.ui_sequence
					,sr.report_category
					,sr.report_class_fqn
					,sr.report_json
					,sr.customer_id
					,sr.customer_key
					,sr.shared_with
					,sr.scheduled_report_id
					,sr.created_by_id
					,sr.created_by_key
					,sr.created_on
					,sr.modified_on
					,srtru.id AS recipient_id
					,srtru.team_name
					,srtru.team_id
					,srtru.team_key
					,srtru.role_name
					,srtru.role_id
					,srtru.user_key
					,srtru.user_id
					,sr.last_request_id
					,rr.result_max_json_file_path
				FROM
					shared_report sr 
					LEFT JOIN shared_report_team_role_user srtru ON sr.id = srtru.shared_report_id
					LEFT JOIN report_request rr ON sr.last_request_id = rr.id
				WHERE
					sr.is_active = True
					AND sr.is_deleted = False
					AND (customerid IS NULL OR sr.customer_id = customerid)
					AND (customerkey IS NULL OR sr.customer_key = customerkey)
					AND (userid IS NULL OR (srtru.user_id = userid OR sr.created_by_id = userid))
					AND (userkey IS NULL OR (srtru.user_key = userkey OR sr.created_by_key = userkey))
				ORDER BY sr.id DESC;
	END;

$BODY$;

ALTER FUNCTION public.fn_get_load_reports(bigint, character varying, bigint, character varying)
    OWNER TO postgres;
