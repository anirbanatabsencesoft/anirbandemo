-- Drop the existing function
--DROP FUNCTION public.fn_save_notification(bigint, character varying, bigint, bigint, bigint, bigint, character varying, bigint, character varying, character varying, text, character varying, bigint, character varying, timestamp with time zone, character varying, bigint, character varying, numeric, timestamp without time zone, boolean, bigint, text, character varying);

-- Create the function again just add OR condition on UPDATE part where customer_id IS NULL
CREATE OR REPLACE FUNCTION public.fn_save_notification(
	noti_customer_id bigint DEFAULT NULL::bigint,
	noti_sender character varying DEFAULT NULL::character varying,
	noti_lookup_notification_type_id bigint DEFAULT NULL::bigint,
	noti_created_by_id bigint DEFAULT NULL::bigint,
	noti_lookup_message_format_id bigint DEFAULT NULL::bigint,
	noti_id bigint DEFAULT NULL::bigint,
	noti_customer_object_id character varying DEFAULT NULL::character varying,
	noti_employer_id bigint DEFAULT NULL::bigint,
	noti_employer_object_id character varying DEFAULT NULL::character varying,
	noti_subject character varying DEFAULT NULL::character varying,
	noti_message text DEFAULT NULL::text,
	noti_created_by_object_id character varying DEFAULT NULL::character varying,
	noti_modified_by_id bigint DEFAULT NULL::bigint,
	noti_modified_by_object_id character varying DEFAULT NULL::character varying,
	noti_notified_on timestamp with time zone DEFAULT NULL::timestamp with time zone,
	noti_unique_id character varying DEFAULT NULL::character varying,
	noti_no_of_retries bigint DEFAULT 3,
	noti_message_id character varying DEFAULT NULL::character varying,
	noti_allowed_no_of_retries numeric DEFAULT 3,
	noti_last_retried_on timestamp without time zone DEFAULT NULL::timestamp without time zone,
	noti_is_archived boolean DEFAULT false,
	noti_lookup_notification_status_id bigint DEFAULT NULL::bigint,
	noti_failure_reason text DEFAULT NULL::text,
	noti_display_name character varying DEFAULT NULL::character varying)
    RETURNS TABLE(id bigint, customerid bigint, customerobjectid character varying, employerid bigint, employerobjectid character varying, "from" character varying, displayname character varying, subject character varying, lookupmessageformatid bigint, message text, lookupnotificationtypeid bigint, createddate timestamp with time zone, createdbyid bigint, createdbyobjectid character varying, modifieddate timestamp with time zone, modifiedbyid bigint, modifiedbyobjectid character varying, notifiedon timestamp with time zone, uniqueid character varying, messageid character varying, noofretries numeric, allowednoofretries numeric, lastretriedon timestamp with time zone, isarchived boolean, lookupnotificationstatusid bigint, failurereason text, isdeleted boolean) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

  BEGIN
	  IF noti_id IS NULL THEN
	    BEGIN
        	RETURN QUERY
			INSERT INTO public.notification(
					  customer_id
					 ,customer_object_id
					 ,employer_id
					 ,employer_object_id
					 ,"from"
					 ,display_name
					 ,subject
					 ,lookup_message_format_id
					 ,message
					 ,lookup_notification_type_id
					 ,created_date
					 ,created_by_id
					 ,created_by_object_id
					 ,modified_date
					 ,modified_by_id
					 ,modified_by_object_id
					 ,notified_on
					 ,unique_id
					 ,message_id
					 ,no_of_retries
					 ,allowed_no_of_retries
					 ,last_retried_on
					 ,is_archived
					 ,lookup_notification_status_id
					 ,failure_reason)
			VALUES (noti_customer_id
				,noti_customer_object_id
				,noti_employer_id
				,noti_employer_object_id
				,noti_sender
				,noti_display_name
				,noti_subject
				,noti_lookup_message_format_id
				,noti_message
				,noti_lookup_notification_type_id
				,CURRENT_TIMESTAMP
				,noti_created_by_id
				,noti_created_by_object_id
				,CURRENT_TIMESTAMP
				,noti_modified_by_id
				,noti_modified_by_object_id
				,noti_notified_on
				,noti_unique_id
				,noti_message_id
				,noti_no_of_retries
				,noti_allowed_no_of_retries
				,noti_last_retried_on
				,noti_is_archived
				,noti_lookup_notification_status_id
				,noti_failure_reason)
				RETURNING *;
		EXCEPTION WHEN OTHERS THEN 
	
			RAISE 'Could not save notification for Customer id : %', noti_customer_id USING ERRCODE = 'invalid_parameter_value';
    
		END;
	  ELSE
	    BEGIN
        	RETURN QUERY
			UPDATE public.notification noti
				SET  modified_date = CURRENT_TIMESTAMP
					,modified_by_id = noti_modified_by_id
					,notified_on = noti_notified_on
					,failure_reason = noti_failure_reason
					,lookup_notification_status_id = noti_lookup_notification_status_id
					,message_id = noti_message_id
				WHERE noti.id = noti_id AND (noti.customer_id = noti_customer_id OR customer_id IS NULL)
				RETURNING *;
		
		EXCEPTION WHEN OTHERS THEN 
			RAISE 'Could not update notification for Notification id : %', noti_id USING ERRCODE = 'invalid_parameter_value';
    		
		END;        
	  END IF;   
	
	
  END
  
$BODY$;

ALTER FUNCTION public.fn_save_notification(bigint, character varying, bigint, bigint, bigint, bigint, character varying, bigint, character varying, character varying, text, character varying, bigint, character varying, timestamp with time zone, character varying, bigint, character varying, numeric, timestamp without time zone, boolean, bigint, text, character varying)
    OWNER TO postgres;