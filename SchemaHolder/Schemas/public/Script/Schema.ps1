﻿# This path argument is optional and should be absolute path up to SchemaHolder\Schemas\public. 
# This script needs to work if its ran locally using relative paths. 
# And work using absolute paths when automated in TeamCity

param(
   [string]$Path
)

if($Path){
	$schema_folder = $Path
	$script_folder = $Path+'\'+'Script'
}


New-Item -ItemType file "$script_folder\Schema.sql" –force

Get-Content $schema_folder\public.sql | Add-Content $script_folder\Schema.sql
Get-Content $script_folder\Template\file_beg.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Sequences\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Functions\*.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Tables\*.sql | Add-Content $script_folder\Schema.sql
Get-Content "$schema_folder\Materialized Views\*.sql" | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Data\*.sql | Add-Content $script_folder\Schema.sql

# view creation order matters due to dependencies in the join

Get-Content $schema_folder\Views\view_cust.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_eplr.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_empl.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_case.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_acom.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_org.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_todo.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_user.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_wkrs.sql | Add-Content $script_folder\Schema.sql

Get-Content $schema_folder\Views\view_acmu.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_asbm.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_attm.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_cnot.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_comm.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_comr.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_prwk.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_cpwf.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_cuso.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_demt.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_emct.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_empc.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_empj.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_empn.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_empo.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_emre.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_emso.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_enot.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_evdt.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_todo.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_evnt.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_oain.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_payp.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_poly.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_pypd.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_role.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_team.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_tmem.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_usge.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_vsch.sql | Add-Content $script_folder\Schema.sql
Get-Content $schema_folder\Views\view_wfin.sql | Add-Content $script_folder\Schema.sql


Get-Content $script_folder\Template\file_end.sql | Add-Content $script_folder\Schema.sql
