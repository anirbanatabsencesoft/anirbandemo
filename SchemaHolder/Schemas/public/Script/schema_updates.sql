﻿2142/***********************************************************************************************************
** schema_update.sql
** NOTE: This file should be re-runnable all day long. Every single schema version and/or update goes into
**	     this single file, and gets wrapped with a proper if not already at this schema version so we
**       only apply upgrades that are necessary to any given target database we run it against.
**
** ALSO: You don't need to put changes to any of the view_* views as those get re-built by the deployment
**       every time. Now, if you do create a new view, then you need to put it in the /Script/RebuildViews.cmd
**       file to be sure it get's built accordingly in the proper dependency order.
**
**       Removed all view_* scripts from this file
**
** LAST VERSION: 107, ID: 107
************************************************************************************************************/
set schema 'public';

DO
$do$
BEGIN

42/***********************************************************************************************************
** TEMPLATE
************************************************************************************************************/
-- IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= DBVERSION_ID_VALUE) THEN
/***********************************************************************************************************/

-- Do stuff

/***********************************************************************************************************/
-- INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (DBVERSION_ID_VALUE, 1, DBVERSION_VALUE, 'Upgrade-VERSION');
-- END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** UNIVERSAL (every time)
************************************************************************************************************/
CREATE OR REPLACE FUNCTION public.utc()
   RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'
   AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$;



/***********************************************************************************************************
** UPGRADE 50, dbversion_id = 52
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 52) THEN
/***********************************************************************************************************/

DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_organization CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_organization
TABLESPACE pg_default
AS
WITH cases AS(
	SELECT DISTINCT
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH) AS PATH,
		COUNT(dim_cases.ext_ref_id) AS total,
		SUM( CASE WHEN dim_cases.status IN(0, 1, 4) THEN 1 ELSE 0 END) AS open_cases,
		SUM( CASE WHEN dim_cases.status = 1 THEN 1 ELSE 0 END) AS closed_cases
	FROM
		dim_cases
	INNER JOIN dim_employee ON dim_cases.employee_id = dim_employee.ext_ref_id
	AND dim_employee.ver_is_current
	AND NOT dim_employee.is_deleted
	INNER JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number
	AND dim_employee.customer_id = dim_employee_organization.customer_id
	AND dim_employee.employer_id = dim_employee_organization.employer_id
	AND dim_employee_organization.ver_is_current
	AND NOT dim_employee_organization.is_deleted
	WHERE
		dim_cases.ver_is_current
	AND NOT dim_cases.is_deleted
	GROUP BY
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH)
),
 todos AS(
	SELECT DISTINCT
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH) AS PATH,
		COUNT(dim_todoitem.ext_ref_id) AS total,
		SUM( CASE WHEN dim_todoitem.status IN(- 1, 2, 4) THEN 1 ELSE 0 END) AS open_todos,
		SUM( CASE WHEN dim_todoitem.status = 1 THEN 1 ELSE 0 END) AS closed_todos,
		SUM( CASE WHEN dim_todoitem.status IN(- 1, 2, 4) AND dim_todoitem.due_date < utc() THEN 1 ELSE 0 END) AS overdue_todos
	FROM
		dim_todoitem
	INNER JOIN dim_employee ON dim_todoitem.employee_id = dim_employee.ext_ref_id
	AND dim_employee.ver_is_current
	AND NOT dim_employee.is_deleted
	INNER JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number
	AND dim_employee.customer_id = dim_employee_organization.customer_id
	AND dim_employee.employer_id = dim_employee_organization.employer_id
	AND dim_employee_organization.ver_is_current
	AND NOT dim_employee_organization.is_deleted
	WHERE
		dim_todoitem.ver_is_current
	AND NOT dim_todoitem.is_deleted
	GROUP BY
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH)
),
 children AS(
	SELECT DISTINCT
		dim_organization.ext_ref_id,
		COUNT(children.ext_ref_id) AS total,
		SUM( CASE WHEN UPPER( dim_organization. PATH || children.code || '/') = UPPER(children. PATH) THEN 1 
			  	   ELSE 0 
			  END
		) AS total_children
	FROM
		dim_organization
	INNER JOIN dim_organization AS children ON dim_organization.customer_id = children.customer_id
	AND dim_organization.employer_id = children.employer_id
	AND POSITION(
		UPPER(dim_organization. PATH) IN UPPER(children. PATH)
	) = 1
	AND CHAR_LENGTH(dim_organization. PATH) < CHAR_LENGTH(children. PATH)
	AND children.ver_is_current
	AND NOT children.is_deleted
	WHERE
		dim_organization.ver_is_current
	AND NOT dim_organization.is_deleted
	GROUP BY
		dim_organization.ext_ref_id
),
 employees AS(
	SELECT DISTINCT
		customer_id,
		employer_id,
		UPPER(PATH) AS PATH,
		COUNT(ext_ref_id) AS total
	FROM
		dim_employee_organization
	WHERE
		ver_is_current
	AND NOT is_deleted
	GROUP BY
		customer_id,
		employer_id,
		UPPER(PATH)
)

SELECT
	A.dim_organization_id as dim_organization_id,
	A.organization_ref_id as organization_ref_id,
	A.customer_ref_id as customer_ref_id,
	A.employer_ref_id as employer_ref_id,
	A.org_level as org_level,
	SUM(A.count_children) as count_children,
	SUM(A.count_descendents) as count_descendents,
	SUM(A.count_employees) as count_employees,
	SUM(A.count_cases) as count_cases,
	SUM(A.count_open_cases) as count_open_cases,
	SUM(A.count_closed_cases) as count_closed_cases,
	SUM(A.count_todos) as count_todos,
	SUM(A.count_open_todos) as count_open_todos,
	SUM(A.count_closed_todos) as count_closed_todos,
	SUM(A.count_overdue_todos) as count_overdue_todos
FROM
	(
		SELECT 
			dim_organization.dim_organization_id,
			dim_organization.ext_ref_id AS organization_ref_id,
			dim_organization.customer_id AS customer_ref_id,
			dim_organization.employer_id AS employer_ref_id,
			array_length(string_to_array(dim_organization. PATH, '/'),1) - 2 AS org_level,
			COALESCE(children.total_children, 0) AS count_children,
			COALESCE(children.total, 0) AS count_descendents,
			COALESCE(employees.total, 0) AS count_employees,
			coalesce(cases.total, 0) AS count_cases,
			coalesce(cases.open_cases, 0) AS count_open_cases,
			coalesce(cases.closed_cases, 0) AS count_closed_cases,
			coalesce(todos.total, 0) AS count_todos,
			coalesce(todos.open_todos, 0) AS count_open_todos,
			coalesce(todos.closed_todos, 0) AS count_closed_todos,
			coalesce(todos.overdue_todos, 0) AS count_overdue_todos
		FROM
			dim_organization
		LEFT JOIN children ON dim_organization.ext_ref_id = children.ext_ref_id
		LEFT JOIN employees ON dim_organization.customer_id = employees.customer_id
			AND dim_organization.employer_id = employees.employer_id
			AND POSITION(UPPER(dim_organization. PATH) IN employees. PATH) = 1
			AND CHAR_LENGTH(dim_organization. PATH) <= CHAR_LENGTH(employees. PATH)      		
		LEFT JOIN cases ON dim_organization.customer_id = cases.customer_id
			AND dim_organization.employer_id = cases.employer_id
			AND position(upper(dim_organization.path) in cases.path) = 1
			AND char_length(dim_organization.path) <= char_length(cases.path)
		LEFT JOIN todos ON dim_organization.customer_id = todos.customer_id
			AND dim_organization.employer_id = todos.employer_id
			AND position(upper(dim_organization.path) in todos.path) = 1
			AND char_length(dim_organization.path) <= char_length(todos.path)
		WHERE
			dim_organization.ver_is_current
			AND NOT dim_organization.is_deleted
	) A
GROUP BY
	A.dim_organization_id,
	A.organization_ref_id,
	A.customer_ref_id,
	A.employer_ref_id,
	A.org_level;

ALTER TABLE public.mview_fact_organization
    OWNER to etldw;

GRANT SELECT ON TABLE public.mview_fact_organization TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_organization TO etldw;
GRANT ALL ON TABLE public.mview_fact_organization TO postgres;

-- Index: mview_fact_organization_organization_ref_id

-- DROP INDEX IF EXISTS public.mview_fact_organization_organization_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_organization_organization_ref_id
    ON public.mview_fact_organization USING btree
    (organization_ref_id)
    TABLESPACE pg_default;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (52, 1, 50, 'Upgrade-50');
END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** UPGRADE 51, dbversion_id = 53
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 53) THEN
/***********************************************************************************************************/

UPDATE dim_lookup_user_status SET code = 3 WHERE _id = 3;
UPDATE dim_lookup_user_status SET code = 2 WHERE _id = 2;
UPDATE dim_lookup_user_status SET code = 1 WHERE _id = 1;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (53, 1, 51, 'Upgrade-51');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 52, dbversion_id = 54
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 54) THEN
/***********************************************************************************************************/

UPDATE dim_lookup_user_type SET code = 2 WHERE _id = 2;
UPDATE dim_lookup_user_type SET code = 1 WHERE _id = 1;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (54, 1, 52, 'Upgrade-52');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 53, dbversion_id = 55
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 55) THEN
/***********************************************************************************************************/

-- Views Only

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (55, 1, 53, 'Upgrade-53');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** UPGRADE 54, dbversion_id = 56
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 56) THEN
/***********************************************************************************************************/

DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_user CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_user
TABLESPACE pg_default
AS
WITH cases AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
), todos AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
)
SELECT	dim_users.dim_user_id,
		dim_users.ext_ref_id AS user_ref_id,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos
FROM	dim_users
		LEFT JOIN cases ON dim_users.ext_ref_id = cases.assigned_to_id
      	LEFT JOIN todos ON dim_users.ext_ref_id = todos.assigned_to_id
WHERE	dim_users.ver_is_current
		AND NOT dim_users.is_deleted;

ALTER TABLE public.mview_fact_user
    OWNER to etldw;

GRANT SELECT ON TABLE public.mview_fact_user TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_user TO etldw;
GRANT ALL ON TABLE public.mview_fact_user TO postgres;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_user_user_ref_id
    ON public.mview_fact_user USING btree
    (user_ref_id)
    TABLESPACE pg_default;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (56, 1, 54, 'Upgrade-54');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
**** Updgrade to DB Version 57
***********************************************************************************************************/

IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 57) THEN
/***********************************************************************************************************/
	-- Create sequences 
	CREATE SEQUENCE IF NOT EXISTS public.dim_case_policy_id_seq;
	CREATE SEQUENCE IF NOT EXISTS public.dim_case_policy_usage_id_seq;

	ALTER SEQUENCE public.dim_case_policy_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO etldw;

	ALTER SEQUENCE public.dim_case_policy_usage_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO etldw;

	-- Create Table dim_case_policy 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy
	(
		dim_case_policy_id bigint NOT NULL DEFAULT nextval('dim_case_policy_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL,
		policy_code varchar(50) NOT NULL,
		policy_name varchar(255) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		case_type int,
		eligibility int,
		determination int,
		denial_reason int,
		denial_reason_other text,
		payout_percentage decimal,
		is_deleted boolean NOT NULL default false,
		created_date timestamp with time zone DEFAULT utc(),

		CONSTRAINT dim_case_policy_pkey PRIMARY KEY (dim_case_policy_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy TO postgres;


	--- Table for dim_case_policy_usage 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy_usage
	(
		dim_case_policy_usage_id bigint NOT NULL DEFAULT nextval('dim_case_policy_usage_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		dim_case_policy_id  bigint NOT NULL,
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL, 
		policy_code varchar(50) NOT NULL,
		date_used timestamp with time zone,
		minutes_used int,
		minutes_in_day int,
		hours_used decimal,
		hours_in_day decimal,
		user_entered Boolean,
		determination int,
		denial_reason int,
		denial_reason_other text,
		determined_by varchar(24),
		intermittent_determination int,
		is_locked Boolean,
		uses_time Boolean,
		percent_of_day decimal,
		status int,
		created_date timestamp with time zone DEFAULT utc(),

		CONSTRAINT dim_case_policy_usage_pkey PRIMARY KEY (dim_case_policy_usage_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy_usage
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy_usage TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO postgres;


	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_phone_number text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_email text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_address1 text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_address2 text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_city text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_state text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_postal_code text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_country text;


	/* Create a generic lookup enum type */
	IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'lookup_type') THEN
		CREATE TYPE lookup_type AS enum ('eligibility_status', 'case_denial_reason', 'case_intermittent_status', 'empl_job_classification', 'work_classification', 'type_of_injury', 'sharp_incdnt_when', 'sharp_job_class', 'sharp_location_dept', 'sharp_procedure', 'case_closure_reason');
	END IF;


	/* Create Sequence for Lookups */
	CREATE SEQUENCE IF NOT EXISTS public.dim_lookup_id_seq;

	ALTER SEQUENCE public.dim_lookup_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO etldw;

	/* Create lookup table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type lookup_type NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;

	


	/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);

	/* Case Denial Reason Lookup (type: case_denial_reason) */	
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);

	/* Case Intermittent Status. (type: case_intermittent_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


	
	/* Employee Job Classification (type: empl_job_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);



	/* Appending new fields to dim_employee */
	alter table dim_employee add column if not exists ssn character varying(11);
	alter table dim_employee add column if not exists job_classification bigint;
	alter table dim_employee add column if not exists risk_profile_code character varying(255);
	alter table dim_employee add column if not exists risk_profile_name character varying(255);
	alter table dim_employee add column if not exists risk_profile_description text;
	alter table dim_employee add column if not exists risk_profile_order int;
	alter table dim_employee add column if not exists start_day_of_week character varying(12);


	/* Add work related fields to dim_cases */
	alter table dim_cases add column if not exists wr_date_of_injury timestamp with time zone;
	alter table dim_cases add column if not exists wr_is_pvt_healthcare boolean;
	alter table dim_cases add column if not exists wr_is_reportable boolean;
	alter table dim_cases add column if not exists wr_place_of_occurance text;
	alter table dim_cases add column if not exists wr_work_classification bigint;
	alter table dim_cases add column if not exists wr_type_of_injury bigint;
	alter table dim_cases add column if not exists wr_days_away_from_work int;
	alter table dim_cases add column if not exists wr_restricted_days int;

	alter table dim_cases add column if not exists wr_provider_first_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_last_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_company_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_address1 character varying(512);
	alter table dim_cases add column if not exists wr_provider_address2 character varying(512);
	alter table dim_cases add column if not exists wr_provider_city character varying(512);
	alter table dim_cases add column if not exists wr_provider_state character varying(512);
	alter table dim_cases add column if not exists wr_provider_country character varying(512);
	alter table dim_cases add column if not exists wr_provider_postalcode character varying(20);

	alter table dim_cases add column if not exists wr_is_treated_in_emer_room boolean;
	alter table dim_cases add column if not exists wr_is_hospitalized boolean;
	alter table dim_cases add column if not exists wr_time_started_working character varying(100);
	alter table dim_cases add column if not exists wr_time_of_injury character varying(100);
	alter table dim_cases add column if not exists wr_activity_before_injury text;
	alter table dim_cases add column if not exists wr_what_happened text;
	alter table dim_cases add column if not exists wr_injury_details text;
	alter table dim_cases add column if not exists wr_what_harmed_empl text;
	alter table dim_cases add column if not exists wr_date_of_death timestamp with time zone;
	alter table dim_cases add column if not exists wr_is_sharp boolean;

	alter table dim_cases add column if not exists wr_sharp_body_part text;
	alter table dim_cases add column if not exists wr_type_of_sharp text;
	alter table dim_cases add column if not exists wr_brand_of_sharp text;
	alter table dim_cases add column if not exists wr_model_of_sharp text;
	alter table dim_cases add column if not exists wr_body_fluid text;
	alter table dim_cases add column if not exists wr_is_engn_injury_protection boolean;
	alter table dim_cases add column if not exists wr_is_protective_mech_activated boolean;
	alter table dim_cases add column if not exists wr_when_sharp_incident_happened bigint;
	alter table dim_cases add column if not exists wr_sharp_job_classification bigint;
	alter table dim_cases add column if not exists wr_sharp_other_job_classification text;
	alter table dim_cases add column if not exists wr_sharp_location_department bigint;
	alter table dim_cases add column if not exists wr_sharp_other_location_dept text;
	alter table dim_cases add column if not exists wr_sharp_procedure bigint;
	alter table dim_cases add column if not exists wr_sharp_other_procedure text;
	alter table dim_cases add column if not exists wr_sharp_exposure_detail text;



	
	/* Work Classification (type: work_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);

	
	/* Add data to lookup_type (type_of_injury). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'type_of_injury'; */

	/* Type of Injury (type: type_of_injury)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);


	/* Add data to lookup_type (sharp_incdnt_when). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_incdnt_when'; */
	
	/* Sharp Incident When (type: sharp_incdnt_when) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);


	/* Add data to lookup_type (sharp_job_class). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_job_class'; */

	/* Sharp Job Class (type: sharp_job_class) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


	/* Add data to lookup_type (sharp_location_dept). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_location_dept'; */
	
	/* Sharp Location/Department (type: sharp_location_dept) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


	/* Add data to lookup_type (sharp_procedure). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_procedure'; */
	
	/* Sharp Procedure (type: sharp_procedure) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);




	/* Co-morbidity guidelines data for dim_cases */
	alter table dim_cases add column if not exists cg_risk_asmt_score decimal;
	alter table dim_cases add column if not exists cg_risk_asmt_status character varying(512);
	alter table dim_cases add column if not exists cg_mid_range_all_absence decimal;
	alter table dim_cases add column if not exists cg_at_risk_all_absence decimal;
	alter table dim_cases add column if not exists cg_best_practice_days decimal;
	alter table dim_cases add column if not exists cg_actual_days_lost decimal;


	/* Add new fields for case */
	alter table dim_cases add column if not exists closure_reason bigint;
	alter table dim_cases add column if not exists closure_reason_detail text;
	alter table dim_cases add column if not exists description text;
	alter table dim_cases add column if not exists narrative text;
	alter table dim_cases add column if not exists is_rpt_auth_submitter boolean;
	alter table dim_cases add column if not exists send_e_communication boolean;
	alter table dim_cases add column if not exists e_communication_request_date timestamp with time zone;
	alter table dim_cases add column if not exists risk_profile_code character varying(255);
	alter table dim_cases add column if not exists risk_profile_name character varying(255);
	alter table dim_cases add column if not exists current_office_location text;
	alter table dim_cases add column if not exists total_paid decimal;
	alter table dim_cases add column if not exists total_offset decimal;
	alter table dim_cases add column if not exists apply_offsets_by_default boolean;
	alter table dim_cases add column if not exists waive_waiting_period boolean;
	alter table dim_cases add column if not exists pay_schedule_id character varying(36);


	/* Add data to lookup_type (case_closure_reason). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'case_closure_reason'; */

	/* Case Closure Reason (type: case_closure_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);



	/***********************************************************************************************************/
	/******* Update DB Version *********/
	INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (57, 1, 55, 'Upgrade-55');
	/***********************************************************************************************************/
	
END IF;

IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 58) THEN
	alter table dim_cases add column if not exists alt_phone_number text;
	alter table dim_cases add column if not exists alt_email text;
	alter table dim_cases add column if not exists alt_address1 text;
	alter table dim_cases add column if not exists alt_address2 text;
	alter table dim_cases add column if not exists alt_city text;
	alter table dim_cases add column if not exists alt_state text;
	alter table dim_cases add column if not exists alt_postal_code text;
	alter table dim_cases add column if not exists alt_country text;
	
/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (58, 1, 56, 'Upgrade-56');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
Updgrade to DB Version 58
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 59) THEN
/***********************************************************************************************************/

-- Repeat this as cleanup because Freedom.
alter table dim_cases add column if not exists alt_phone_number text;
alter table dim_cases add column if not exists alt_email text;
alter table dim_cases add column if not exists alt_address1 text;
alter table dim_cases add column if not exists alt_address2 text;
alter table dim_cases add column if not exists alt_city text;
alter table dim_cases add column if not exists alt_state text;
alter table dim_cases add column if not exists alt_postal_code text;
alter table dim_cases add column if not exists alt_country text;
ALTER TABLE IF EXISTS public.dim_case_policy ADD COLUMN IF NOT EXISTS crosswalk_codes jsonb;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (59, 1, 57, 'Upgrade-57');
END IF;
/***********************************************************************************************************/
	


/***********************************************************************************************************
Updgrade to DB Version 60
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 60) THEN
/***********************************************************************************************************/

--Update Accommodation table for audit data
ALTER TABLE dim_accommodation ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_accommodation ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update Case table for audit data
ALTER TABLE dim_cases RENAME COLUMN cby TO ext_created_by;
ALTER TABLE dim_cases ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update customers table for audit data
ALTER TABLE dim_customer ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_customer ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update demand table for audit data
ALTER TABLE dim_demand ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_demand ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update demand type table for audit data
ALTER TABLE dim_demand_type ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_demand_type ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee table for audit data
ALTER TABLE dim_employee ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee organization table for audit data
ALTER TABLE dim_employee_organization ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee_organization ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee restriction table for audit data
ALTER TABLE dim_employee_restriction ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee_restriction ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employer table for audit data
ALTER TABLE dim_employer ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employer ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update organization table for audit data
ALTER TABLE dim_organization ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_organization ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update to-do item table for audit data
ALTER TABLE dim_todoitem ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_todoitem ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update users table for audit data
ALTER TABLE dim_users ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_users ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Rename columns of Employee Contact of cby and mby
ALTER TABLE dim_employee_contact RENAME COLUMN cby TO ext_created_by;
ALTER TABLE dim_employee_contact RENAME COLUMN mby TO ext_modified_by;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (60, 1, 58, 'Upgrade-58');
END IF;
/***********************************************************************************************************/
	



/***********************************************************************************************************
Updgrade to DB Version 61
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 61) THEN
/***********************************************************************************************************/

-- View updates only

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (61, 1, 59, 'Upgrade-59');
END IF;
/***********************************************************************************************************/
	


/***********************************************************************************************************
Updgrade to DB Version 60
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 62) THEN
/***********************************************************************************************************/

alter table dim_case_policy add column if not exists crosswalk_codes jsonb;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (62, 1, 60, 'Upgrade-60');
END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** Updgrade to DB Version 61
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 63) THEN
/***********************************************************************************************************/

-- Only View Changes.


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (63, 1, 61, 'Upgrade-61');
END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** Updgrade to DB Version 62
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 64) THEN
/***********************************************************************************************************/

ALTER TABLE dim_employee 
	ALTER COLUMN ssn 
	SET DATA TYPE character varying(20) 
	USING ssn::character varying(20);

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (64, 1, 62, 'Upgrade-62');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** ** Updgrade to DB Version 63
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 65) THEN
/***********************************************************************************************************/

ALTER TABLE dim_case_policy ADD COLUMN IF NOT EXISTS record_hash text;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (65, 1, 63, 'Upgrade-63');
END IF;
/***********************************************************************************************************/



/***********************************************************************************************************
** ** Updgrade to DB Version 64
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 66) THEN
/***********************************************************************************************************/

IF NOT EXISTS (SELECT 1 FROM public.dim_lookup_case_determination WHERE _id = 4) THEN
  insert into public.dim_lookup_case_determination (_id, code, description) values (4, 3, 'Approved');
END IF;
IF NOT EXISTS (SELECT 1 FROM public.dim_lookup_case_determination WHERE _id = 5) THEN
  insert into public.dim_lookup_case_determination (_id, code, description) values (5, 4, 'Not Eligible');
END IF;

ALTER TABLE public.dim_cases ADD COLUMN IF NOT EXISTS case_closure_reason_other text;

-- NOTE: Views automatically get dropped and re-created in /Script/RebuildViews.cmd by TeamCity deployment.

-- This requires a re-sync, the logic was all wrong and the only way to correct it is a re-sync, so it
--	stands to reason to do this here in the schema update script, even though it may be suprising behavior.
-- It will only happen for this specific schema version at least.
UPDATE operational.sync_manager SET 
	modified_date = utc(),
	last_sync_date_update = null,
	source_count = 0,
	initial_load = true
WHERE dimension_id = 4;
TRUNCATE TABLE public.dim_cases;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (66, 1, 64, 'Upgrade-64');
END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** ** Updgrade to DB Version 65
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 67) THEN
/***********************************************************************************************************/

/**** Drop function sp_dim_ver for generating new version */
DROP FUNCTION IF EXISTS public.sp_dim_ver(IN p_dimension_id bigint,
    IN p_ext_ref_id character varying,
    IN p_ext_created_date timestamp with time zone,
    IN p_ext_modified_date timestamp with time zone);


/**** Drop table dim_ver as it's no more required ****/
DROP TABLE IF EXISTS public.dim_ver;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (67, 1, 65, 'Upgrade-65');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** ** Updgrade to DB Version 66
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 68) THEN
/***********************************************************************************************************/

ALTER TABLE dim_cases ADD COLUMN IF NOT EXISTS intermittent_type integer;

DROP TYPE public.lookup_type CASCADE;

CREATE TYPE public.lookup_type AS ENUM
    ('eligibility_status', 'case_denial_reason', 'case_intermittent_status', 'empl_job_classification', 'work_classification', 'type_of_injury', 'sharp_incdnt_when', 'sharp_job_class', 'sharp_location_dept', 'sharp_procedure', 'case_closure_reason','intermittent_type');

DROP TABLE public.dim_lookup;

ALTER SEQUENCE public.dim_lookup_id_seq RESTART;

CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type lookup_type NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;

/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);


	/* Case Denial Reason Lookup (type: case_denial_reason) */	
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);

	
	/* Case Intermittent Status. (type: case_intermittent_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


	/* Employee Job Classification (type: empl_job_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);

	/* Work Classification (type: work_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);

	
	/* Type of Injury (type: type_of_injury)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);

	
	/* Sharp Incident When (type: sharp_incdnt_when) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);

		
	/* Sharp Job Class (type: sharp_job_class) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


	/* Sharp Location/Department (type: sharp_location_dept) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


	/* Sharp Procedure (type: sharp_procedure) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);


	/* Case Closure Reason (type: case_closure_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);

	/* IntermittentType Lookup (type: intermittent_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 0, 'OfficeVisit' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 1, 'Incapacity' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=1);


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (68, 1, 66, 'Upgrade-66');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** ** Upgrade to DB Version 68
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 70) THEN
/***********************************************************************************************************/

alter table dim_employer add column if not exists ignore_average_minutes_worked_per_week boolean;
alter table dim_employee add column if not exists average_minutes_worked_per_week double precision;
alter table dim_employee add column if not exists average_minutes_worked_per_week_as_of timestamp with time zone;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (70, 1, 70, 'Upgrade-70');
END IF;
/***********************************************************************************************************/




/***********************************************************************************************************
** ** Upgrade to DB Version 70
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 72) THEN
/***********************************************************************************************************/

alter table dim_organization add column if not exists siccode text;
alter table dim_organization add column if not exists naicscode text;
alter table dim_organization add column if not exists address1 text;
alter table dim_organization add column if not exists address2 text;
alter table dim_organization add column if not exists city text;
alter table dim_organization add column if not exists postalcode text;
alter table dim_organization add column if not exists state text;
alter table dim_organization add column if not exists country text;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (72, 1, 72, 'Upgrade-72');
END IF;
/***********************************************************************************************************/
-- 


/***********************************************************************************************************
** ** Updgrade to DB Version 67
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 74) THEN
/***********************************************************************************************************/

CREATE OR REPLACE FUNCTION public.show_customerdetails()
  RETURNS TABLE(customerid bigint, customername character varying, employer character varying, employee_count_6month bigint, employee_count_3month bigint, employee_count_1month bigint, case_count bigint, todo_count bigint, active_users bigint) AS
 $BODY$
    BEGIN
		return  query 
		WITH grouped_employee_count_6 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '6 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id), 
		grouped_employee_count_3 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '3 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id), 
		grouped_employee_count_1 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '1 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id) , 
		grouped_case_count AS (SELECT customer_id, employer_id FROM dim_cases WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND  ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) , 
		grouped_todo_count AS (SELECT customer_id, employer_id FROM dim_todoitem WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) ,
		grouped_active_users AS (SELECT customer_id, employer_ids FROM dim_Users WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_ids)  
	   
		SELECT cust.dim_customer_ID, CAST(cust.cust_customer_name AS VARCHAR) customer, CAST(empr.NAME AS VARCHAR) Employer,
		(SELECT count(1) FROM grouped_employee_count_6 ec_6 WHERE ec_6.customer_id = cust.ext_ref_id AND ec_6.employer_id = empr.ext_ref_id) Employee_Count_6month,
		(SELECT count(1) FROM grouped_employee_count_3 ec_3 WHERE ec_3.customer_id = cust.ext_ref_id AND ec_3.employer_id = empr.ext_ref_id) Employee_Count_3month,
		(SELECT count(1) FROM grouped_employee_count_1 ec_1 WHERE ec_1.customer_id = cust.ext_ref_id AND ec_1.employer_id = empr.ext_ref_id) Employee_Count_1month,
		(SELECT count(1) FROM grouped_case_count cc  WHERE cc.customer_id = cust.ext_ref_id AND cc.employer_id = empr.ext_ref_id) Case_Count,
		(SELECT count(1) FROM grouped_todo_count tc  WHERE tc.customer_id = cust.ext_ref_id AND tc.employer_id = empr.ext_ref_id) Todo_Count,
		(SELECT count(1) FROM grouped_active_users au  WHERE au.customer_id = cust.ext_ref_id AND empr.ext_ref_id = ANY(au.employer_ids::varchar[])) Active_Users
		FROM dim_customer cust
		INNER JOIN dim_employer empr ON cust.ext_ref_id = empr.customer_id
		WHERE cust.ver_is_current = TRUE AND empr.ver_is_current = TRUE  AND cust.Is_Deleted = FALSE AND empr.Is_Deleted = FALSE
		GROUP BY cust.dim_customer_ID, cust.cust_customer_name, empr.NAME, empr.ext_ref_id
		ORDER BY 1;
    END;
  $BODY$
  LANGUAGE plpgsql;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (74, 1, 74, 'Upgrade-74');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** ** Upgrade to DB Version 70
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 75) THEN
/***********************************************************************************************************/

CREATE SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    INCREMENT 1
    START 213
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO etldw;

CREATE TABLE public.dim_organization_annual_info
(
    dim_organization_annual_info_id bigint NOT NULL DEFAULT nextval('dim_organization_annual_info_dim_organization_annual_info_id_seq'::regclass),
	ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    org_code text,
    org_year int,
	average_employee_count int,
	total_hours_worked decimal,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_organization_annual_info_pkey PRIMARY KEY (dim_organization_annual_info_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_organization_annual_info
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_organization_annual_info TO absence_tracker;
GRANT ALL ON TABLE public.dim_organization_annual_info TO etldw;
GRANT ALL ON TABLE public.dim_organization_annual_info TO postgres;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (75, 1, 75, 'Upgrade-75');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** ** Upgrade to DB Version 74
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 74) THEN
/***********************************************************************************************************/

alter table dim_employer add column if not exists start_date date;
alter table dim_employer add column if not exists end_date date;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (74, 1, 74, 'Upgrade-74');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** ** Upgrade to DB Version 100
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 100) THEN
/***********************************************************************************************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_case_authorized_submitter_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_authorized_submitter_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO etldw;

-- Table: public.dim_case_authorized_submitter

-- Create Table dim_case_authorized_submitter
	CREATE TABLE IF NOT EXISTS public.dim_case_authorized_submitter
	(
		dim_case_authorized_submitter_id bigint NOT NULL DEFAULT nextval('dim_case_authorized_submitter_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		authorized_submitter_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		contact_type_code text,
		contact_type_name text,	
		contact_first_name text,    
		contact_last_name text,	
		contact_email text,
		contact_alt_email text,
		contact_work_phone text,
		contact_home_phone text,
		contact_cell_phone text,
		contact_fax text,
		contact_street text,
		contact_address1 text,
		contact_address2 text,
		contact_city text,
		contact_state text,
		contact_postal_code text,
		contact_country text,	   	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_case_authorized_submitter_pkey PRIMARY KEY (dim_case_authorized_submitter_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_authorized_submitter
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_authorized_submitter TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO etldw;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO postgres;

-- Index: dim_case_authorized_submitter_cur_ver_ref_id_is_del

CREATE INDEX IF NOT EXISTS dim_case_authorized_submitter_cur_ver_ref_id_is_del
    ON public.dim_case_authorized_submitter USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE IF NOT EXISTS public.dim_case_event_date_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_event_date_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO etldw;

-- Table: public.dim_case_event_date

-- Create Table dim_case_event_date
	CREATE TABLE IF NOT EXISTS public.dim_case_event_date
	(
		dim_case_event_date_id bigint NOT NULL DEFAULT nextval('dim_case_event_date_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		event_type bigint,		
		event_date timestamp with time zone,
		record_hash text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_case_event_date_pkey PRIMARY KEY (dim_case_event_date_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_event_date
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_event_date TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_event_date TO etldw;
	GRANT ALL ON TABLE public.dim_case_event_date TO postgres;

	-- Index: dim_case_event_date_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_event_date_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_event_date_cur_ver_ref_id_is_del
    ON public.dim_case_event_date USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

/*************************************************************************
 CASE PAY PERIOD 
**************************************************************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_case_pay_period_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_case_pay_period_detail_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_detail_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO etldw;

-- Table: public.dim_case_pay_period

-- Create Table dim_case_pay_period
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period
	(
		dim_case_pay_period_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		status int,
		locked_date timestamp with time zone,
		locked_by	varchar(24),
		max_weekly_pay_amount double precision,		
		payroll_date_override timestamp with time zone,
		payroll_date timestamp with time zone,
		payroll_date_override_by varchar(24),
		end_date_override_by varchar(24),
		offset_amount double precision,
		total double precision,
		sub_total double precision,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_pay_period_pkey PRIMARY KEY (dim_case_pay_period_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period TO postgres;

-- Index: dim_case_pay_period_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_pay_period_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_pay_period_cur_ver_ref_id_is_del
    ON public.dim_case_pay_period USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_case_pay_period_detail

-- Create Table dim_case_pay_period_detail
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period_detail
	(
		dim_case_pay_period_detail_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_detail_id_seq'::regclass),
		dim_case_pay_period_id bigint,
		ext_ref_id character varying(36),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		is_Offset boolean,
		max_payment_amount double precision,
		pay_amount_override_value double precision,
		pay_amount_system_value double precision,
		payment_tier_percentage double precision,
		pay_percentage_override_by varchar(24),
		pay_percentage_override_date timestamp with time zone,
		pay_percentage_override_value double precision,
		pay_percentage_system_value double precision,
		policy_code text,
		policy_name text,
		salary_total text,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_case_pay_period_detail_pkey PRIMARY KEY (dim_case_pay_period_detail_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period_detail
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period_detail TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO postgres;

/**************************************************************
                     Communication
*************************************************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_communication_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_paperwork_field_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_field_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_paperwork_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_recipient_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_recipient_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO etldw;

-- Table: public.dim_communication

-- Create Table dim_communication
	CREATE TABLE IF NOT EXISTS public.dim_communication
	(
		dim_communication_id bigint NOT NULL DEFAULT nextval('dim_communication_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		attachment_id varchar(24),
		body text,
		communication_type int,
		created_by_email text,
		created_by_name text,
		email_replies_type int,	
		is_draft boolean,
		name text,
		public boolean,
		first_send_date timestamp with time zone,
		last_send_date timestamp with time zone,
		subject text,
		template text,
		to_do_item_id varchar(24),
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_communication_id_pkey PRIMARY KEY (dim_communication_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication TO etldw;
	GRANT ALL ON TABLE public.dim_communication TO postgres;

-- Index: dim_communication_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_id_cur_ver_ref_id_is_del
    ON public.dim_communication USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_communication_paperwork

-- Create Table dim_communication_paperwork
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork
	(
		dim_communication_paperwork_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_id_seq'::regclass),
		communication_id character varying(24),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		attachment_id	varchar(24),		
		checked	 boolean,
		due_date	timestamp with time zone,
		return_attachment_id	varchar(24),
		status	int,
		status_date	 timestamp with time zone,
		
		paperwork_id	varchar(24),
		paperwork_cby	varchar(24),
		paperwork_cdt	timestamp with time zone,
		paperwork_code	text,
		paperwork_customer_id	varchar(24),
		paperwork_description	text,
		paperwork_doc_type	int,
		paperwork_employer_id	varchar(24),
		paperwork_enclosure_text	text,		
		paperwork_file_id	varchar(24),
		paperwork_file_name	text,
		paperwork_is_deleted boolean default false,
		paperwork_mby	varchar(24),
		paperwork_mdt	timestamp with time zone,
		paperwork_name	text,
		paperwork_requires_review	boolean,
		paperwork_return_date_adjustment	text,
		paperwork_return_date_adjustment_days	int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_pkey PRIMARY KEY (dim_communication_paperwork_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO postgres;
	
	-- Index: dim_communication_paperwork_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_paperwork_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_paperwork_id_cur_ver_ref_id_is_del
    ON public.dim_communication_paperwork USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

	-- Table: public.dim_communication_paperwork_field

-- Create Table dim_communication_paperwork_field
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork_field
	(
		dim_communication_paperwork_field_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_field_id_seq'::regclass),
		dim_communication_paperwork_id bigint,
		ext_ref_id character varying(36),
		name text,
		status int,
		type int,
		required boolean default false,
		field_order int,
		value text,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_field_pkey PRIMARY KEY (dim_communication_paperwork_field_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork_field
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

	-- Table: public.dim_communication_recipient

-- Create Table dim_communication_recipient
	CREATE TABLE IF NOT EXISTS public.dim_communication_recipient
	(
		dim_communication_recipient_id bigint NOT NULL DEFAULT nextval('dim_communication_recipient_id_seq'::regclass),
		dim_communication_id bigint,
		recipient_type int,
		ext_ref_id character varying(36),
		address_id	varchar(36),
		address_address1 text,
		address_city	text,
		address_country	text,
		address_postal_code	text,
		address_state	text,
		alt_email	text,
		email	text,
		fax	text,
		title text,
		first_name	text,
		middle_name text,		
		last_name	text,
		home_email	text,
		company_name text,
		contact_person_name text,
		date_of_birth timestamp with time zone ,
		home_phone text,
		cell_phone text,
		is_primary boolean,
		CONSTRAINT dim_communication_recipient_pkey PRIMARY KEY (dim_communication_recipient_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_recipient
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_recipient TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_recipient TO etldw;
	GRANT ALL ON TABLE public.dim_communication_recipient TO postgres;


	/******************************************************************
	                        Attachment
	******************************************************************/
	CREATE SEQUENCE IF NOT EXISTS public.dim_attachment_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_attachment_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO etldw;

-- Table: public.dim_attachment

-- Create Table dim_attachment
	CREATE TABLE IF NOT EXISTS public.dim_attachment
	(
		dim_attachment_id bigint NOT NULL DEFAULT nextval('dim_attachment_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		content_length text,
		content_type text,
		attachment_type int,
		created_by_name text,
		description text,
		file_id character varying(24),
		file_Name text,
		public boolean,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_attachment_pkey PRIMARY KEY (dim_attachment_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_attachment
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
	GRANT ALL ON TABLE public.dim_attachment TO etldw;
	GRANT ALL ON TABLE public.dim_attachment TO postgres;

-- Index: dim_attachment_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_attachment_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_attachment_cur_ver_ref_id_is_del
    ON public.dim_attachment USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

ALTER TABLE dim_case_policy_usage ADD COLUMN IF NOT EXISTS is_intermittent_restriction BOOLEAN;
ALTER TABLE public.dim_communication_paperwork ADD COLUMN case_id character varying(24);
ALTER TABLE public.dim_communication_paperwork ADD COLUMN paperwork_review_status INT;

/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (100, 1, 100, 'Upgrade-100');
END IF;
/***********************************************************************************************************/


/***********************************************************************************************************
** ** Upgrade to DB Version 102
************************************************************************************************************/
IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 102) THEN
/***********************************************************************************************************/

/*************Lookups**********************/
-- Fix this sequence just in case.
PERFORM setval('dim_lookup_id_seq', (select max(lookup_id) from dim_lookup), true);

	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='recipient_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'recipient_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='paperwork_review_status' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'paperwork_review_status', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='document_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'document_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='paperwork_field_status' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'paperwork_field_status', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='paperwork_field_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'paperwork_field_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='case_event_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'case_event_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='payroll_status' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'payroll_status', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='attachment_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'attachment_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='communication_type' ) THEN
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'communication_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='email_replies_type' ) THEN	
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'email_replies_type', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='note_category' ) THEN	
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'note_category', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumlabel='workflow_instance_status' ) THEN	
		INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)
		SELECT 'lookup_type'::regtype::oid, 'workflow_instance_status', ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'lookup_type'::regtype );
	END IF;
	/* RecipientType Lookup(type: recipient_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 0, 'TO' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 1, 'CC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 2, 'BCC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=2);
	
	/* PaperworkReviewStatus Lookup(type: paperwork_review_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 0, 'NotApplicable' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 1, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 2, 'Received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 3, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 4, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 5, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 6, 'NotReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=6);

	/*DocumentType Lookup(type: document_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 0, 'None' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 1, 'Template' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 2, 'Html' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 3, 'MSWordDocument' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 4, 'Pdf' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=4);

	/*PaperworkFieldStatus Lookup(type: paperwork_field_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 0, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 1, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=1);

	/*PaperworkFieldType Lookup(type: paperwork_field_type)*/	
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 0, 'Text' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 4, 'WorkRestrictions' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=4);
/********************/
	
	/* RecipientType Lookup(type: recipient_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 0, 'TO' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 1, 'CC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 2, 'BCC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=2);
	
	/* PaperworkReviewStatus Lookup(type: paperwork_review_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 0, 'NotApplicable' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 1, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 2, 'Received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 3, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 4, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 5, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 6, 'NotReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=6);

	/*DocumentType Lookup(type: document_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 0, 'None' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 1, 'Template' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 2, 'Html' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 3, 'MSWordDocument' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 4, 'Pdf' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=4);

	/*PaperworkFieldStatus Lookup(type: paperwork_field_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 0, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 1, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=1);

	/*PaperworkFieldType Lookup(type: paperwork_field_type)*/	
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 0, 'Text' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 4, 'WorkRestrictions' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=4);
/********************/
	/* EventType Lookup(type: event_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 0, 'CaseCreated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 1, 'ReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=1); 
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 2, 'DeliveryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 3, 'BondingStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 4, 'BondingEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 5, 'IllnessOrInjuryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 6, 'HospitalAdmissionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 7, 'HospitalReleaseDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 8, 'ReleaseReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 9, 'PaperworkReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 10, 'AccommodationAdded' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 11, 'CaseClosed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 12, 'CaseCancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 13, 'AdoptionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 14, 'EstimatedReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 15, 'EmployerHolidayScheduleChanged' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 16, 'CaseStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 17, 'CaseEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=17);

	/* PayrollStatus Lookup(type: payroll_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 1, 'SentToException' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 2, 'SentToPayroll' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=2);

	/* AttachmentType Lookup(type: attachment_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 0, 'Commmunication' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 1, 'Paperwork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 2, 'Documentation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=3);
	
	/*CommunicationType Lookup(type: communication_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 0, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 1, 'Email' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=1);

	/*EmailRepliesType Lookup(type: email_replies_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 1, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 2, 'CurrentUser' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=2);
	
	/*NoteCategory Lookup(type: note_category)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 0, 'CaseSummary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 1, 'EmployeeConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 2, 'ManagerConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 3, 'HRConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 4, 'Medical' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 5, 'TimeOffRequest' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 6, 'Certification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 7, 'InteractiveProcess' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 8, 'WorkRestriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 9, 'OperationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 10, 'HealthCareProviderConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 11, 'WorkerCompensationConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 12, 'SafetyConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 13, 'LegalConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 14, 'ERCConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 15, 'EmployeeRelationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 16, 'BenefitsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 17, 'LOAConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=17);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 18, 'FacilitiesConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=18);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 19, 'PayrollConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=19);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 20, 'STDLOAVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=20);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 21, 'LTDVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=21);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 22, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=22);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 23, 'Referral' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=23);

	/*WorkflowInstanceStatus Lookup(type: workflow_instance_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 1, 'Running' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 2, 'Paused' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 3, 'Completed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 4, 'Canceled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 5, 'Failed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=5);

/*********** Sequence ************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_case_note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_note_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO etldw;

CREATE SEQUENCE If Not Exists public.dim_workflow_instance_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_workflow_instance_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_variable_schedule_time_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_variable_schedule_time_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO etldw;

/****** Dimensions *********************/
Alter table dim_attachment add column IF NOT EXISTS checked boolean;

-- Table: public.dim_case_note
	DROP TABLE IF EXISTS dim_case_note CASCADE;
-- Create Table dim_case_note
	CREATE TABLE IF NOT EXISTS public.dim_case_note
	(
		dim_case_note_id bigint NOT NULL DEFAULT nextval('dim_case_note_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accomm_id	bigint,
		accomm_question_id	text,
		answer	boolean,		
		cert_id	text,
		contact	text,
		copied_from	bigint,
		copied_from_employee	bigint,		
		employee_number	text,		
		entered_by_name	text,		
		notes	text,
		process_path	text,
		public	boolean,
		request_date	timestamp with time zone,
		request_id	text,
		s_answer	text,
		work_restriction_id varchar(36),
		category_code text,
		category_name text,
		category int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_note_pkey PRIMARY KEY (dim_case_note_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_note
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_note TO etldw;
	GRANT ALL ON TABLE public.dim_case_note TO postgres;

-- Index: dim_case_note_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_note_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_note_cur_ver_ref_id_is_del
    ON public.dim_case_note USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_workflow_instance

-- Create Table dim_workflow_instance
	CREATE TABLE IF NOT EXISTS public.dim_workflow_instance
	(
		dim_workflow_instance_id bigint NOT NULL DEFAULT nextval('dim_workflow_instance_id_seq'::regclass),
		ext_ref_id character varying(36) ,
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accommodation_end_date	timestamp with time zone ,
		accommodation_id	character varying(36) ,
		accommodation_type_code	text ,
		accommodation_type_id	text ,
		active_accommodation_id	character varying(36) ,
		applied_policy_id	character varying(36) ,
		attachment_id	text ,		
		close_case	Boolean ,
		communication_id	text ,
		communication_type	int ,
		condition_start_date	timestamp with time zone ,
		contact_employee_due_date	timestamp with time zone ,
		contact_hcp_due_date	timestamp with time zone ,		
		denial_explanation	text ,
		denial_reason	int ,
		description	text ,
		determination	int ,
		due_date	timestamp with time zone ,
		ergonomic_assessment_date	timestamp with time zone ,
		event_id	varchar(24) ,
		event_type	int ,
		extend_grace_period	Boolean ,
		first_exhaustion_date	timestamp with time zone ,
		general_health_condition	text ,
		has_work_restrictions	Boolean ,
		illness_or_injury_date	timestamp with time zone ,
		new_due_date	timestamp with time zone ,
		paperwork_attachment_id	text ,
		paperwork_due_date	timestamp with time zone ,
		paperwork_id	character varying(36) ,
		paperwork_name	text ,
		paperwork_received	Boolean ,
		policy_code	text ,
		policy_id	text ,
		reason	int ,
		requires_review	Boolean ,
		return_attachment_id	text ,
		return_to_work_date	timestamp with time zone ,
		rtw	Boolean ,
		source_workflow_activity_activity_id	text ,
		source_workflow_activity_id	character varying(36) ,
		source_workflow_instance_id	text ,
		status	int ,
		template	text ,
		workflow_activity_id	character varying(36) ,
		workflow_id	varchar(24) ,
		workflow_name	text ,

		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_workflow_instance_pkey PRIMARY KEY (dim_workflow_instance_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_workflow_instance
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_workflow_instance TO absence_tracker;
	GRANT ALL ON TABLE public.dim_workflow_instance TO etldw;
	GRANT ALL ON TABLE public.dim_workflow_instance TO postgres;

-- Index: dim_workflow_instance_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_workflow_instance_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_workflow_instance_cur_ver_ref_id_is_del
    ON public.dim_workflow_instance USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

	-- Table: public.dim_variable_schedule_time

-- Create Table dim_variable_schedule_time
	CREATE TABLE IF NOT EXISTS public.dim_variable_schedule_time
	(
		dim_variable_schedule_time_id bigint NOT NULL DEFAULT nextval('dim_variable_schedule_time_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		time_sample_date timestamp with time zone ,
		time_total_minutes int,
		employee_number text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_variable_schedule_time_pkey PRIMARY KEY (dim_variable_schedule_time_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_variable_schedule_time
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_variable_schedule_time TO absence_tracker;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO etldw;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO postgres;

-- Index: dim_variable_schedule_time_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_variable_schedule_time_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_variable_schedule_time_cur_ver_ref_id_is_del
    ON public.dim_variable_schedule_time USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;


/***********************************************************************************************************/
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (102, 1, 102, 'Upgrade-102');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** ** Upgrade to DB Version 103
************************************************************************************************************/
 IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 103) THEN
/***********************************************************************************************************/


CREATE SEQUENCE public.dim_role_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_role_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO postgres;

-- Table: public.dim_role

-- DROP TABLE public.dim_role;

CREATE TABLE public.dim_role
(
    dim_role_id bigint NOT NULL DEFAULT nextval('dim_role_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    name text,
    customer_id character varying(24),
    permissions text[],
    role_type integer,
	is_deleted boolean,
    CONSTRAINT dim_role_pkey PRIMARY KEY (dim_role_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_role
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_role TO absence_tracker;

GRANT ALL ON TABLE public.dim_role TO etldw;

GRANT ALL ON TABLE public.dim_role TO postgres;

-- Index: dim_role_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_role_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_role_cur_ver_ref_id_is_del
    ON public.dim_role USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE public.dim_employer_contact_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_contact_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_contact_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_contact_id_seq TO postgres;

-- Table: public.dim_employer_contact

-- DROP TABLE public.dim_employer_contact;

CREATE TABLE public.dim_employer_contact
(
    dim_employer_contact_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    contact_firstname text,
    contact_lastname text,
    contact_email text,
    contact_alt_email text,
    contact_work_phone text,
    contact_fax text,
    contact_address text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean,
    CONSTRAINT dim_employer_contact_pkey PRIMARY KEY (dim_employer_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact TO postgres;

-- Index: dim_employer_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_contact_cur_ver_ref_id_is_del
    ON public.dim_employer_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE public.dim_team_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO postgres;

-- Table: public.dim_team

-- DROP TABLE public.dim_team;

CREATE TABLE public.dim_team
(
    dim_team_id bigint NOT NULL DEFAULT nextval('dim_team_id_seq'::regclass),
    ext_ref_id character varying(24) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    name text,
    description text,
    is_deleted boolean,
    CONSTRAINT dim_team_pkey PRIMARY KEY (dim_team_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team TO absence_tracker;

GRANT ALL ON TABLE public.dim_team TO etldw;

GRANT ALL ON TABLE public.dim_team TO postgres;

-- Index: dim_team_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_cur_ver_ref_id_is_del;

CREATE INDEX dim_team_cur_ver_ref_id_is_del
    ON public.dim_team USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;

/***********************************************************************************************************/
 INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (103, 1, 103, 'Upgrade-103');
 END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 104, dbversion_id = 104
************************************************************************************************************/
 IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 104) THEN
/***********************************************************************************************************/

CREATE SEQUENCE public.dim_customer_service_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_customer_service_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO postgres;

-- Table: public.dim_customer_service_id_seq

-- DROP TABLE public.dim_customer_service_id_seq;

-- Table: public.dim_customer_service_option

-- DROP TABLE public.dim_customer_service_option;

CREATE TABLE public.dim_customer_service_option
(
    dim_customer_service_option_id bigint NOT NULL DEFAULT nextval('dim_customer_service_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    is_deleted boolean,
    customer_id character varying(24),
    key text,
    value text,
    "order" integer,
    CONSTRAINT dim_customer_service_pkey PRIMARY KEY (dim_customer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_customer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_customer_service_option TO postgres;

-- Index: dim_employer_service_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_service_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_service_cur_ver_ref_id_is_del
    ON public.dim_customer_service_option USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;


CREATE SEQUENCE public.dim_team_member_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_member_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO postgres;

-- Table: public.dim_team_member

-- DROP TABLE public.dim_team_member;

CREATE TABLE public.dim_team_member
(
    dim_team_member_id bigint NOT NULL DEFAULT nextval('dim_team_member_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    user_id character varying(24),
    team_id character varying(24),
    is_team_lead boolean,
    assigned_by_id character varying(24),
    assigned_on timestamp with time zone,
    modified_by_id character varying(24),
    modified_on timestamp with time zone,
    is_deleted boolean,
    CONSTRAINT dim_team_member_pkey PRIMARY KEY (dim_team_member_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team_member
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team_member TO absence_tracker;

GRANT ALL ON TABLE public.dim_team_member TO etldw;

GRANT ALL ON TABLE public.dim_team_member TO postgres;

-- Index: dim_team_member_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_member_cur_ver_ref_id_is_del;

CREATE INDEX dim_team_member_cur_ver_ref_id_is_del
    ON public.dim_team_member USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;


CREATE SEQUENCE public.dim_event_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_event_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO postgres;

-- Table: public.dim_event

-- DROP TABLE public.dim_event;

CREATE TABLE public.dim_event
(
    dim_event_id bigint NOT NULL DEFAULT nextval('dim_event_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    employee_id character varying(24),
    todo_item_id character varying(24),
    accomodation_id character varying(24),
    active_accomodation_id character varying(24),
    accomodation_type_code text,
    accomodation_end_date timestamp with time zone,
    determination integer,
    denial_explanation text,
    case_id character varying(24),
    accomodation_type_id character varying(24),
    template text,
    communication_type integer,
    name text,
    event_type text,
    is_deleted boolean,
    applied_policy_id character varying(24),
    attachment_id character varying(24),
    close_case boolean,
    communication_id character varying(24),
    condition_start_date timestamp with time zone,
    contact_employee_due_date timestamp with time zone,
    contact_hcp_due_date timestamp with time zone,
    denial_reason integer,
    description text,
    due_date timestamp with time zone,
    ergonomic_assessment_date timestamp with time zone,
    extended_grace_period boolean,
    first_exhaustion_date timestamp with time zone,
    general_health_condition text,
    has_work_restriction boolean,
    illness_injury_date timestamp with time zone,
    new_due_date timestamp with time zone,
    paperwork_attachment_id character varying(24),
    paperwork_due_date timestamp with time zone,
    paperwork_id character varying(24),
    paperwork_name text,
    paperwork_received boolean,
    policy_code character varying(24),
    policy_id character varying(24),
    reason integer,
    requires_review boolean,
    return_attachment_id character varying(24),
    return_to_work_date timestamp with time zone,
    rtw boolean,
    source_workflow_activity_activityid character varying(24),
    source_workflow_activityid character varying(24),
    source_workflow_instanceid character varying(24),
    workflow_activity_id character varying(24),
    CONSTRAINT dim_event_pkey PRIMARY KEY (dim_event_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_event
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_event TO absence_tracker;

GRANT ALL ON TABLE public.dim_event TO etldw;

GRANT ALL ON TABLE public.dim_event TO postgres;

-- Index: dim_event_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_event_cur_ver_ref_id_is_del;

CREATE INDEX dim_event_cur_ver_ref_id_is_del
    ON public.dim_event USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;

/***********************************************************************************************************/
 INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (104, 1, 104, 'Upgrade-104');
 END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 105, dbversion_id = 105
************************************************************************************************************/
 IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 105) THEN
/***********************************************************************************************************/

CREATE SEQUENCE public.dim_employer_contact_types_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_contact_types_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_contact_types_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_contact_types_seq TO postgres;

-- Table: public.dim_employer_contact_types

-- DROP TABLE public.dim_employer_contact_types;

CREATE TABLE public.dim_employer_contact_types
(
    dim_employer_contact_types_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_types_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    is_deleted boolean,
	customer_id character varying(24),
	name text,
	code text,
    CONSTRAINT dim_employer_contact_types_pkey PRIMARY KEY (dim_employer_contact_types_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact_types TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact_types TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact_types TO postgres;

-- Index: dim_employer_contact_types_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_types_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_contact_types_cur_ver_ref_id_is_del
    ON public.dim_employer_contact_types USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;

/***********************************************************************************************************/
 INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (105, 1, 105, 'Upgrade-105');
 END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** UPGRADE 106, dbversion_id = 106
************************************************************************************************************/
 IF NOT EXISTS(SELECT 1 FROM dbversion WHERE dbversion_id >= 106) THEN
/***********************************************************************************************************/

Alter table public.dim_cases add column actual_duration int;
Alter table public.dim_cases add column exceeded_duration int;
 
ALTER TABLE public.dim_cases OWNER to postgres;
GRANT SELECT ON TABLE public.dim_cases TO absence_tracker;
GRANT ALL ON TABLE public.dim_cases TO etldw;
GRANT ALL ON TABLE public.dim_cases TO postgres;


Alter table public.dim_case_policy add column actual_duration int;
Alter table public.dim_case_policy add column exceeded_duration int;

ALTER TABLE public.dim_case_policy OWNER to postgres;
GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_policy TO etldw;
GRANT ALL ON TABLE public.dim_case_policy TO postgres;
/***********************************************************************************************************/
 INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (106, 1, 106, 'Upgrade-106');
END IF;
/***********************************************************************************************************/

/***********************************************************************************************************
** END OF FILE
************************************************************************************************************/	
/***********************************************************************************************************
** END OF FILE
************************************************************************************************************/
END
$do$
