-- SCHEMA: public

-- DROP SCHEMA public ;

CREATE SCHEMA IF NOT EXISTS public;

COMMENT ON SCHEMA public
    IS 'standard public schema';

	DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etldw')
THEN
    CREATE ROLE etldw LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etlsync')
THEN
    CREATE ROLE etlsync LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'absence_tracker')
THEN
    CREATE ROLE absence_tracker LOGIN PASSWORD 'password_goes_here';
END IF;


END
$do$;


GRANT ALL ON SCHEMA public TO postgres;

GRANT USAGE ON SCHEMA public TO absence_tracker;

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT USAGE ON SCHEMA public TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT ON TABLES TO absence_tracker;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT ALL ON TABLES TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT, USAGE ON SEQUENCES TO etldw;
/******************************************************************************
**		File: Schema.sql
**		Desc: Schema implementation.  Creates complete <<SCHEMA_NAME>> schema for Postgresql db
**				Absencesoft ETL project.
**
**		Auth: <<AUTHOR>>
**		Date: <<DATE>>
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		<<DATE>>		<<AUTHOR_INITIALS>>		Created
**    
*******************************************************************************/

set schema 'public';

DO
$do$
BEGIN
CREATE SEQUENCE IF NOT EXISTS public.db_update_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.db_update_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.db_update_seq TO postgres;

GRANT ALL ON SEQUENCE public.db_update_seq TO etldw;
CREATE SEQUENCE public.dim_accommodation_dim_accommodation_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_accommodation_dim_accommodation_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_accommodation_dim_accommodation_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_accommodation_dim_accommodation_id_seq TO etldw;
CREATE SEQUENCE public.dim_attachment_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_attachment_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO etldw;
CREATE SEQUENCE public.dim_cases_dim_case_id_seq
    INCREMENT 1
    START 2925
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_cases_dim_case_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_authorized_submitter_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_authorized_submitter_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_event_date_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_event_date_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO etldw;
CREATE SEQUENCE IF NOT EXISTS public.dim_case_note_category_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_note_category_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_category_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_category_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_note_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_pay_period_detail_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_detail_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_pay_period_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_policy_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_policy_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO etldw;
CREATE SEQUENCE public.dim_case_policy_usage_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_policy_usage_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO etldw;
CREATE SEQUENCE public.dim_communication_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO etldw;
CREATE SEQUENCE public.dim_communication_paperwork_field_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_field_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO etldw;
CREATE SEQUENCE public.dim_communication_paperwork_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO etldw;
CREATE SEQUENCE public.dim_communication_recipient_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_recipient_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO etldw;
CREATE SEQUENCE public.dim_customer_dim_customer_id_seq
    INCREMENT 1
    START 1356
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_customer_dim_customer_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_dim_customer_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_dim_customer_id_seq TO etldw;
CREATE SEQUENCE public.dim_customer_service_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_customer_service_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO postgres;
CREATE SEQUENCE public.dim_date_dim_date_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_date_dim_date_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_date_dim_date_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_date_dim_date_id_seq TO etldw;
CREATE SEQUENCE public.dim_demand_dim_demand_id_seq
    INCREMENT 1
    START 75143
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_demand_dim_demand_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_dim_demand_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_dim_demand_id_seq TO etldw;
CREATE SEQUENCE public.dim_demand_types_dim_demand_types_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_demand_types_dim_demand_types_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_types_dim_demand_types_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_types_dim_demand_types_id_seq TO etldw;
CREATE SEQUENCE public.dim_demand_type_dim_demand_type_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_demand_type_dim_demand_type_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_type_dim_demand_type_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_type_dim_demand_type_id_seq TO etldw;
CREATE SEQUENCE public.dim_demand_type_items_dim_demand_type_items_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_demand_type_items_dim_demand_type_items_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_type_items_dim_demand_type_items_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_demand_type_items_dim_demand_type_items_id_seq TO etldw;
CREATE SEQUENCE IF NOT EXISTS public.dim_denial_reason_id_seq  
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_denial_reason_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_denial_reason_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_denial_reason_id_seq TO postgres;
CREATE SEQUENCE IF NOT EXISTS public.dim_employee_contact_dim_employee_contact_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_contact_dim_employee_contact_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO etldw;
CREATE SEQUENCE public.dim_employee_dim_employee_id_seq
    INCREMENT 1
    START 109020
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_dim_employee_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_dim_employee_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_dim_employee_id_seq TO etldw;
CREATE SEQUENCE public.dim_employee_job_dim_employee_job_id_seq
    INCREMENT 1
    START 177
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_job_dim_employee_job_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_job_dim_employee_job_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_job_dim_employee_job_id_seq TO postgres;
CREATE SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq TO postgres;
CREATE SEQUENCE public.dim_employee_note_note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_note_note_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_note_note_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_note_note_id_seq TO postgres;
CREATE SEQUENCE public.dim_employee_organization_dim_employee_organization_id_seq
    INCREMENT 1
    START 145
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_organization_dim_employee_organization_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_organization_dim_employee_organization_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_organization_dim_employee_organization_id_seq TO etldw;
CREATE SEQUENCE public.dim_employee_restriction_dim_employee_restriction_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_restriction_dim_employee_restriction_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_dim_employee_restriction_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_dim_employee_restriction_id_seq TO etldw;
CREATE SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq TO etldw;
CREATE SEQUENCE public.dim_employer_access_dim_employer_access_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_access_dim_employer_access_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_access_dim_employer_access_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_access_dim_employer_access_id_seq TO etldw;
CREATE SEQUENCE public.dim_employer_contact_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_contact_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_contact_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_contact_id_seq TO postgres;
CREATE SEQUENCE public.dim_employer_contact_types_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_contact_types_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_contact_types_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_contact_types_id_seq TO postgres;
CREATE SEQUENCE public.dim_employer_dim_employer_id_seq
    INCREMENT 1
    START 1122
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_dim_employer_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_dim_employer_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employer_dim_employer_id_seq TO etldw;
CREATE SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq
    INCREMENT 1
    START 24
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO postgres;
CREATE SEQUENCE public.dim_event_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_event_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO postgres;

CREATE SEQUENCE IF NOT EXISTS public.dim_lookup_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


	ALTER SEQUENCE public.dim_lookup_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO etldw;
CREATE SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    INCREMENT 1
    START 213
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO etldw;
CREATE SEQUENCE public.dim_organization_dim_organization_id_seq
    INCREMENT 1
    START 213
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_organization_dim_organization_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_dim_organization_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_dim_organization_id_seq TO etldw;
CREATE SEQUENCE IF NOT EXISTS public.dim_relapse_dim_relapse_id_seq
    INCREMENT 1
    START 269
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_relapse_dim_relapse_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO postgres;
CREATE SEQUENCE public.dim_roles_dim_role_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_roles_dim_role_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_roles_dim_role_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_roles_dim_role_id_seq TO etldw;
CREATE SEQUENCE public.dim_role_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_role_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO postgres;
CREATE SEQUENCE public.dim_sync_lookup_dim_sync_lookup_id_seq
    INCREMENT 1
    START 20609
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_sync_lookup_dim_sync_lookup_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_sync_lookup_dim_sync_lookup_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_sync_lookup_dim_sync_lookup_id_seq TO etldw;
CREATE SEQUENCE public.dim_team_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO postgres;
CREATE SEQUENCE public.dim_team_member_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_member_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO postgres;
CREATE SEQUENCE public.dim_todoitem_dim_todoitem_id_seq
    INCREMENT 1
    START 8267
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_todoitem_dim_todoitem_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_todoitem_dim_todoitem_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_todoitem_dim_todoitem_id_seq TO etldw;
CREATE SEQUENCE public.dim_users_dim_user_id_seq
    INCREMENT 1
    START 2774
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_users_dim_user_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_users_dim_user_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_users_dim_user_id_seq TO etldw;
CREATE SEQUENCE public.dim_user_roles_dim_user_roles_id_seq
    INCREMENT 1
    START 598
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_user_roles_dim_user_roles_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_user_roles_dim_user_roles_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_user_roles_dim_user_roles_id_seq TO etldw;
CREATE SEQUENCE public.dim_variable_schedule_time_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_variable_schedule_time_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO etldw;
CREATE SEQUENCE public.dim_ver_dim_ver_id_seq
    INCREMENT 1
    START 202557
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_ver_dim_ver_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_ver_dim_ver_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_ver_dim_ver_id_seq TO etldw;
CREATE SEQUENCE public.dim_workflow_instance_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_workflow_instance_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO etldw;
CREATE SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq
    INCREMENT 1
    START 61111
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq TO etldw;
-- FUNCTION: public.create_index_if_not_exists(text, text, text)

-- DROP FUNCTION public.create_index_if_not_exists(text, text, text);

CREATE OR REPLACE FUNCTION public.create_index_if_not_exists(
	t_name text,
	i_name text,
	index_sql text)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

DECLARE
  full_index_name varchar;
  schema_name varchar;
BEGIN

full_index_name = t_name || '_' || i_name;
schema_name = 'public';

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = full_index_name
    AND    n.nspname = schema_name
    ) THEN

    execute 'CREATE INDEX ' || full_index_name || ' ON ' || schema_name || '.' || t_name || ' ' || index_sql;
END IF;
END

$function$;

ALTER FUNCTION public.create_index_if_not_exists(text, text, text)
    OWNER TO postgres;
-- FUNCTION: public.custom_field(json, text)

-- DROP FUNCTION public.custom_field(json, text);

CREATE OR REPLACE FUNCTION public.custom_field(
	custom_fields jsonb,
	code text)
    RETURNS text
    LANGUAGE 'plpgsql'
AS $function$

BEGIN
	RETURN CASE WHEN custom_fields IS NULL THEN null::text ELSE custom_fields ->> code END;
END 

$function$;

ALTER FUNCTION public.custom_field(jsonb, text)
    OWNER TO postgres;
-- FUNCTION: public.delete_non_current_records()

-- DROP FUNCTION public.delete_non_current_records();

CREATE OR REPLACE FUNCTION public.delete_non_current_records()
 RETURNS text
 LANGUAGE plpgsql
AS $function$ DECLARE tables CURSOR FOR
SELECT
    tablename
FROM
    pg_tables
WHERE
    tablename NOT LIKE 'pg_%'
    AND tablename LIKE 'dim_%'
    AND tablename NOT LIKE 'dim_lookup%'
    AND tablename <> 'dimension'
    and tablename not in (
        'dim_accommodation_usage', 'dim_date',
        'dim_employee_restriction_entry',
        'dim_case_certification'
    )
ORDER BY
    tablename; nbRow int;

     BEGIN FOR table_record IN tables LOOP EXECUTE 'DELETE FROM ' || table_record.tablename || ' where ver_is_current = False';
    END LOOP;

    Return 'Completed';

    END; 
$function$;

ALTER FUNCTION public.delete_non_current_records()
    OWNER TO postgres;
CREATE OR REPLACE FUNCTION fn_insert_audit_history
(
p_entity_type varchar(255),
p_field_name text[],
p_old_value text[],
p_new_value text[],
p_cdt timestamp with time zone,
p_cby varchar(100),
p_customerid varchar(24),
p_employerid varchar(24),
p_employeeid varchar(24),
p_caseid varchar(24),
p_isdeleted boolean,
p_username varchar(100),
p_entity_id varchar(36)
)
returns integer
as $$
DECLARE
p_audit_id integer;
counter integer := 1;
arrlength integer;
BEGIN

-- CHECK IF MASTER ROW EXISTS IN AUDIT MASTER TABLE IF EXISTS THEN DIRECTLY INSERT IN CHILD LOG TABLE
IF EXISTS ( SELECT 1 FROM audit.audit WHERE
customerid   = p_customerid and
employerid  = p_employerid and
employeeid  = p_employeeid and
COALESCE (caseid,'-A') = COALESCE (p_caseid,'-A') and
isdeleted  = p_isdeleted and
username  = p_username and
entity_id  = p_entity_id and
entity_type = p_entity_type
) THEN

select id  into p_audit_id
from audit.audit
where  customerid  = p_customerid and
employerid  = p_employerid and
employeeid  = p_employeeid and
COALESCE (caseid,'-A') = COALESCE (p_caseid,'-A') and
isdeleted  = p_isdeleted and
username  = p_username and
entity_id  = p_entity_id and
entity_type = p_entity_type;

ELSE

INSERT INTO audit.audit
(cdt,cby,customerid,employerid,employeeid,caseid,isdeleted,username,entity_id,entity_type)
VALUES
(p_cdt,p_cby,p_customerid,p_employerid,p_employeeid,p_caseid,p_isdeleted,p_username,p_entity_id,p_entity_type);

select id  into p_audit_id
from audit.audit
where  customerid  = p_customerid and
employerid  = p_employerid and
employeeid  = p_employeeid and
COALESCE (caseid,'-A') = COALESCE (p_caseid,'-A') and
isdeleted  = p_isdeleted and
username  = p_username and
entity_id  = p_entity_id and
entity_type = p_entity_type;
END IF;


arrlength:= array_length(p_field_name,1) ;

WHILE counter <= arrlength
LOOP

insert into audit.audit_detail(audit_id,field_name,old_value,new_value)
values(p_audit_id,p_field_name[counter],p_old_value[counter],p_new_value[counter]);
counter := counter + 1 ;
END LOOP ;



return 1;
END;
$$ language plpgsql;

ALTER FUNCTION public.fn_insert_audit_history(character varying, text[], text[], text[], timestamp with time zone, character varying, character varying, character varying, character varying, character varying, boolean, character varying, character varying)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION fn_insert_login_history
(
p_cdt timestamp with time zone,
p_cby varchar(100),
p_userid varchar(100),
p_customerid varchar(24),
p_ipaddress varchar(25),
p_browser varchar(50),
p_userroles text[],
p_employeraccess text[],
p_issuccess boolean,
p_issso boolean,
p_loginstatus integer,
p_email varchar(100),
p_subject varchar(100)
)
returns integer
as $$
BEGIN
insert into audit.login_history(cdt,cby,userid,customerid,ipaddress,browser,userroles,employeraccess,issuccess,issso,loginstatus,email,subject)
values(p_cdt,p_cby,p_userid,p_customerid,p_ipaddress,p_browser,p_userroles,p_employeraccess,p_issuccess,p_issso,p_loginstatus,p_email,p_subject);
return 1;
END;
$$ language plpgsql;

ALTER FUNCTION public.fn_insert_login_history(timestamp with time zone, character varying, character varying, character varying, character varying, character varying, text[], text[], boolean, boolean, integer, character varying, character varying)
  OWNER TO postgres;
create or replace function fn_insert_pageview_history
(
p_cdt timestamp with time zone,
p_cby varchar(100)  ,
p_username varchar(100)  ,
p_customerid varchar(24)  ,
p_employerid varchar(24)  ,
p_url text  ,
p_isess boolean  ,
p_routedatacontroller text[]  ,
p_routedataaction text[]  ,
p_httpmethod text  ,
p_email text
)
returns integer
as $$
BEGIN

INSERT INTO audit.pageview_history
(
    cdt,cby,username,customerid,employerid,url,isess,routedatacontroller,routedataaction,httpmethod,email
)
values
(
    p_cdt,p_cby,p_username,p_customerid,p_employerid,p_url,p_isess,p_routedatacontroller,p_routedataaction,p_httpmethod,p_email
);
return 1;

END;

$$ language plpgsql;


ALTER FUNCTION public.fn_insert_pageview_history(timestamp with time zone, character varying, character varying, character varying, character varying, text, boolean, text[], text[], text, text)
  OWNER TO postgres;
CREATE OR REPLACE FUNCTION fn_search_user_aggview(inputSearchTerm varchar, customerid varchar, employer_ids varchar [],
                                                 lname           varchar,
                                                 dobdate         date, employer_names varchar)
  RETURNS TABLE(
    fun_total_rows     int,
	fun_employers_all varchar[]
  )
AS $$
DECLARE
  inputSearch     varchar;
   emp_names varchar[];
BEGIN
  -- adding pattern :* to the search string which will enable partial search. if multiple arguments are passed an or clause is built
  inputSearch := array_to_string(array_remove(string_to_array(inputSearchTerm, ' '), ''), ':*|') || ':*';
  emp_names = string_to_array(employer_names,'|');
  RETURN QUERY
  select count(1)::int,array_agg(distinct  mv.employer::varchar)
  from mview_search_data mv LEFT OUTER JOIN (select unnest(employer_ids) :: text as employer_id) tmp
      on mv.employer_id = tmp.employer_id
	  LEFT OUTER JOIN (select unnest(emp_names) :: text as ename)  empname on mv.employer = empname.ename	 
	 
  where searchvector @@ to_tsquery(inputSearch) and customer_id = customerid
        -- coalesce condition to handle nulls
        and coalesce(lastname, '-AA') = coalesce(coalesce(lname, lastname), '-AA')
        and coalesce(dob, '1850-01-01') = coalesce(coalesce(dobdate, dob), '1850-01-01')
		and (tmp.employer_id is not null or  not exists(select employer_id from  (select unnest(employer_ids) :: text as employer_id) tmp1 limit 1))
		and (empname.ename is not null or  not exists(select ename from (select unnest(emp_names) :: text as ename) empname1 limit 1));

END; $$

LANGUAGE 'plpgsql';

alter FUNCTION fn_search_user_aggview(varchar ,varchar, varchar [], varchar,date,varchar)OWNER TO postgres;
-- FUNCTION: public.function_exists(text, text)

-- DROP FUNCTION public.function_exists(text, text);

CREATE OR REPLACE FUNCTION public.function_exists(
	sch text,
	fun text)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
	EXECUTE  'select pg_get_functiondef('''||sch||'.'||fun||'''::regprocedure)';
	RETURN true;
	exception when others then 
	RETURN false;
END;

$function$;

ALTER FUNCTION public.function_exists(text, text)
    OWNER TO postgres;
-- FUNCTION: public.json_array_map(json, text[])

-- DROP FUNCTION public.json_array_map(json, text[]);

CREATE OR REPLACE FUNCTION public.json_array_map(
	json_arr json,
	path text[])
    RETURNS json[]
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

DECLARE
	rec json;
	len int;
	ret json[];
BEGIN
	-- If json_arr is not an array, return an empty array as the result
	BEGIN
		len := json_array_length(json_arr);
	EXCEPTION
		WHEN OTHERS THEN
			RETURN ret;
	END;

	-- Apply mapping in a loop
	FOR rec IN SELECT json_array_elements#>path FROM json_array_elements(json_arr)
	LOOP
		ret := array_append(ret,rec);
	END LOOP;
	RETURN ret;
END 
$function$;

ALTER FUNCTION public.json_array_map(json, text[])
    OWNER TO postgres;
CREATE OR REPLACE FUNCTION public.show_customerdetails_new(IN p_date date)
    RETURNS TABLE (
        customerid int8
        ,customername varchar
        ,employer varchar
        ,employee_count_current_month int8
        ,case_count_current_month int8
        ,todo_count_current_month int8
        ,active_users_current_month int8
    ) AS 
$BODY$
BEGIN

    RETURN   query
    SELECT   cust.dim_customer_id customerid
            ,cust.cust_customer_name::varchar customername
            ,empr."name"::varchar employer
            ,(  SELECT  COUNT(distinct emp.employee_number)
                FROM    public.dim_employee emp
                WHERE   emp.customer_id = cust.ext_ref_id 
                        AND emp.employer_id = empr.ext_ref_id 
                        AND emp.ext_created_date::date <= p_date
                        AND emp.ext_modified_date::date <= p_date
                        AND NOT emp.is_deleted 
                        AND emp.status IN (65,76)
             ) employee_count_current_month
            ,(  SELECT  COUNT(distinct cases.case_number) 
                FROM    public.dim_cases cases
                WHERE   cases.customer_id = cust.ext_ref_id 
                        AND cases.employer_id = empr.ext_ref_id 
                        AND cases.ext_created_date::date <= p_date
                        AND cases.ext_modified_date::date <= p_date
                        AND NOT cases.is_deleted
             ) case_count_current_month
            ,(  SELECT  COUNT(distinct todo.ext_ref_id) 
                FROM    public.dim_todoitem todo
                WHERE   todo.customer_id = cust.ext_ref_id 
                        AND todo.employer_id = empr.ext_ref_id 
                        AND todo.ext_created_date::date <= p_date
                        AND todo.ext_modified_date::date <= p_date
                        AND NOT todo.is_deleted
             ) todo_count_current_month
            ,(  SELECT  COUNT(distinct usr.email) 
                FROM    public.dim_users usr
                WHERE   usr.customer_id = cust.ext_ref_id 
                        AND empr.ext_ref_id = ANY(usr.employer_ids::varchar[]) 
                        AND usr.ext_created_date::date <= p_date
                        AND usr.ext_modified_date::date <= p_date
                        AND NOT usr.is_deleted
             ) active_users_current_month
    FROM    public.dim_customer cust
            INNER JOIN public.dim_employer empr 
                ON cust.ext_ref_id = empr.customer_id
                AND empr.ver_is_current
                AND NOT empr.is_deleted
    WHERE   cust.ver_is_current
            AND NOT cust.is_deleted
    ORDER   BY 2;

END;
$BODY$
  LANGUAGE 'plpgsql'
;

ALTER FUNCTION public.show_customerdetails_new(IN p_date date) OWNER TO postgres;


-- FUNCTION: public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit)

-- DROP FUNCTION public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit);

CREATE OR REPLACE FUNCTION public.spdimdateinsert(
	iso8601date timestamp without time zone DEFAULT NULL::timestamp without time zone,
	year integer DEFAULT NULL::integer,
	month integer DEFAULT NULL::integer,
	day integer DEFAULT NULL::integer,
	minute integer DEFAULT NULL::integer,
	second integer DEFAULT NULL::integer,
	weekend bit DEFAULT NULL::"bit",
	weekday bit DEFAULT NULL::"bit",
	holiday bit DEFAULT NULL::"bit")
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
INSERT INTO dim_date
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
)
 VALUES 
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
);
END;

$function$;

ALTER FUNCTION public.spdimdateinsert(timestamp without time zone, integer, integer, integer, integer, integer, bit, bit, bit)
    OWNER TO postgres;
-- FUNCTION: public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying)

-- DROP FUNCTION public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.spfactinsert(
	factid integer DEFAULT 0,
	datecreated timestamp without time zone DEFAULT NULL::timestamp without time zone,
	code character varying DEFAULT NULL::character varying,
	description character varying DEFAULT NULL::character varying,
	visiblecode character varying DEFAULT NULL::character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF 
AS $function$

BEGIN
INSERT INTO fact
(
	fact_id,
	date_created,
	code,
	description,
	visible_code
)
 VALUES 
(
	FactID,
	DateCreated,
	Code,
	Description,
	VisibleCode
);
END;

$function$;

ALTER FUNCTION public.spfactinsert(integer, timestamp without time zone, character varying, character varying, character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION fn_search_user_mview(inputSearchTerm varchar, customerid varchar, employer_ids varchar [],
                                                 lname           varchar,
                                                 dobdate         date, pagenumber integer, pagesize integer,
                                                 srtkey          varchar, employer_names varchar)
  RETURNS TABLE(
    fun_employee_id    text,
    fun_employer_id    text,
    fun_employer       text,
    fun_customer_id    text,
    fun_casenumber     text,
    fun_dob            date,
    fun_firstname      text,
    fun_lastname       text,
    fun_employeenumber text,
    fun_caseid         text,
    fun_rnk            bigint,
    fun_searchtext     text,
    fun_title          text,
    fun_desc           text

  )
AS $$
DECLARE
  inputSearch     varchar;
  emp_names varchar[];
BEGIN
  -- adding pattern :* to the search string which will enable partial search. if multiple arguments are passed an or clause is built
  inputSearch := array_to_string(array_remove(string_to_array(inputSearchTerm, ' '), ''), ':*|') || ':*';
  emp_names = string_to_array(employer_names,'|');

		
  RETURN QUERY
  SELECT
    EmployeeId,
    employer_id,
    employer,
    customer_id,
    casenumber,
    dob,
    firstname,
    lastname,
    employeenumber,
    caseid,
    rnk,
    inputSearchTerm :: text,
    title,
    description
  FROM (
         SELECT
           EmployeeId,
           mv.employer_id,
           employer,
           customer_id,
           casenumber,
           dob,
           firstname,
           lastname,
           employeenumber,
           caseid,
           title,
           description,
           ROW_NUMBER()
           OVER (
             ORDER BY 
					case 
					when srtkey is null then title
					when upper(srtkey) = 'RELEVANCE'  then title
					when upper(srtkey) = 'LN'  then lastname
					when upper(srtkey) = 'FN'  then firstname
					when upper(srtkey) = 'CN'  then casenumber
					when upper(srtkey) = 'EM'  then employer
					else 'title'
					end  
		) as rnk
         from mview_search_data mv LEFT OUTER JOIN (select unnest(employer_ids) :: text as employer_id) tmp
             on mv.employer_id = tmp.employer_id
		LEFT OUTER JOIN (select unnest(emp_names) :: text as ename)  empname on mv.employer = empname.ename	 
         where searchvector @@ to_tsquery(inputSearch) and customer_id = customerid
               -- coalesce condition to handle nulls
               and coalesce(lastname, '-AA') = coalesce(coalesce(lname, lastname), '-AA')
               and coalesce(dob, '1850-01-01') = coalesce(coalesce(dobdate, dob), '1850-01-01')
			   and (tmp.employer_id is not null or  not exists(select employer_id from (select unnest(employer_ids) :: text as employer_id) tmp1 limit 1))
			   and (empname.ename is not null or  not exists(select ename from (select unnest(emp_names) :: text as ename) empname1 limit 1))
			   
       ) A
  WHERE A.rnk >= (pagesize * (pagenumber - 1) + 1) and a.rnk <= (pagesize * pagenumber);

END; $$

LANGUAGE 'plpgsql';

alter FUNCTION fn_search_user_mview( varchar, varchar, varchar [], varchar,
  date, integer, integer, varchar ,varchar)
OWNER TO postgres;
CREATE OR REPLACE FUNCTION UFN_TBL_MV_FACT_ORG_BASEDATA_REFRESH() 
RETURNS void 
AS $$
BEGIN
                
CREATE TEMP TABLE TBL_MV_FACT_ORG_TMP
(
	dim_organization_id bigint ,
	organization_ref_id varchar(24),
	customer_ref_id varchar(24),
	employer_ref_id varchar(24),
	count_children bigint, 
	count_descendents bigint,
	count_employees bigint,
	count_cases bigint,
	count_open_cases bigint,
	count_closed_cases bigint,
	count_todos bigint,
	count_open_todos bigint,
	count_closed_todos bigint,
	count_overdue_todos bigint,
	path text,
	concatvalue text,
	concatvalueparent text,
	code text,
	concatvalueroot text,
	org_level integer
 
);


--600 ms
INSERT INTO TBL_MV_FACT_ORG_TMP( dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,path)
SELECT dim_organization_id,ext_ref_id,customer_id,employer_id,path 
FROM dim_organization
WHERE  ver_is_current AND NOT  is_deleted;

 

update TBL_MV_FACT_ORG_TMP
set concatvalue = upper(customer_ref_id::text||employer_ref_id::text||path) ,
	concatvalueroot = upper(customer_ref_id::text||employer_ref_id::text||'/'||substr(ltrim(path,'/'),1,"strpos"(ltrim(path,'/'),'/'))),
	concatvalueparent = upper(reverse(substr(ltrim(reverse(path),'/'),"strpos"(ltrim(reverse(path),'/'),'/'),length(ltrim(reverse(path),'/'))))),
	org_level = (array_length(string_to_array(path, '/'::text), 1) - 2);
	
	
update TBL_MV_FACT_ORG_TMP
set  concatvalueparent = 
case when concatvalueparent = '/' then upper(path)
else upper(concatvalueparent)
end ;


update TBL_MV_FACT_ORG_TMP
set  concatvalueparent = upper(customer_ref_id::text||employer_ref_id::text||concatvalueparent);

update TBL_MV_FACT_ORG_TMP A
set code = B.code
from ( select code,ext_ref_id from dim_organization where ver_is_current and not is_deleted) B 
where A.organization_ref_id = B.ext_ref_id;


--create index ix_TBL_MV_FACT_ORG_TMP on TBL_MV_FACT_ORG_TMP(organization_ref_id,customer_ref_id,employer_ref_id);


--1.5 mins  
CREATE TEMP TABLE cases AS
SELECT DISTINCT dim_employee.customer_id,
dim_employee.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_cases.ext_ref_id) AS total,
sum(
CASE
WHEN dim_cases.status = ANY (ARRAY[0 , 4]) THEN 1
ELSE 0
END) AS open_cases,
sum(
CASE
WHEN dim_cases.status = 1 THEN 1
ELSE 0
END) AS closed_cases,
upper(dim_employee.customer_id::text||dim_employee.employer_id||upper(dim_employee_organization.path)) as concatvalue
FROM dim_cases
JOIN dim_employee ON dim_cases.employee_id = dim_employee.ext_ref_id AND dim_employee.ver_is_current AND NOT dim_employee.is_deleted
JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number AND dim_employee.customer_id = dim_employee_organization.customer_id AND dim_employee.employer_id = dim_employee_organization.employer_id AND dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
WHERE dim_cases.ver_is_current AND NOT dim_cases.is_deleted
GROUP BY dim_employee.customer_id, dim_employee.employer_id, (upper(dim_employee_organization.path));
 
 
 
--2 seconds
UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_cases =  COALESCE(B.total,0),
count_open_cases = COALESCE(B.open_cases,0),
count_closed_cases = COALESCE(B.closed_cases,0)
FROM cases B
WHERE   A.concatvalue = B.concatvalue;


--1m 11 seconds

create temp table todos AS
SELECT DISTINCT dim_employee.customer_id,
dim_employee.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_todoitem.ext_ref_id) AS total,
sum(
CASE
WHEN dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4]) THEN 1
ELSE 0
END) AS open_todos,
sum(
CASE
WHEN dim_todoitem.status = 1 THEN 1
ELSE 0
END) AS closed_todos,
sum(
CASE
WHEN (dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4])) AND dim_todoitem.due_date < utc() THEN 1
ELSE 0
END) AS overdue_todos,
upper(dim_employee.customer_id::text||dim_employee.employer_id||upper(dim_employee_organization.path)) as concatvalue
FROM dim_todoitem
JOIN dim_employee ON dim_todoitem.employee_id::text = dim_employee.ext_ref_id::text AND dim_employee.ver_is_current AND NOT dim_employee.is_deleted
JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number AND dim_employee.customer_id::text = dim_employee_organization.customer_id::text AND dim_employee.employer_id::text = dim_employee_organization.employer_id::text AND dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
WHERE dim_todoitem.ver_is_current AND NOT dim_todoitem.is_deleted
GROUP BY dim_employee.customer_id, dim_employee.employer_id, (upper(dim_employee_organization.path));

--2 seconds 
UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_todos =  COALESCE(B.total,0),
count_open_todos = COALESCE(B.open_todos,0),
count_closed_todos = COALESCE(B.closed_todos,0),
count_overdue_todos = COALESCE(B.overdue_todos,0)
FROM todos B
WHERE A.concatvalue = B.concatvalue;

UPDATE TBL_MV_FACT_ORG_TMP A
SET count_descendents = COALESCE(B.total,0)
FROM 
(
SELECT DISTINCT A.organization_ref_id,
count(B.organization_ref_id) AS total
FROM TBL_MV_FACT_ORG_TMP A
JOIN TBL_MV_FACT_ORG_TMP B 
ON A.concatvalue = B.concatvalueroot and A.organization_ref_id <> B.organization_ref_id
GROUP BY A.organization_ref_id
) B 
where  A.organization_ref_id = B.organization_ref_id;


UPDATE TBL_MV_FACT_ORG_TMP A
SET count_children = COALESCE(B.total_children,0)
FROM 
(
SELECT DISTINCT A.organization_ref_id,
sum(
CASE
WHEN upper((A.path || B.code) || '/'::text) = upper(B.path) THEN 1
ELSE 0
END) AS total_children
FROM TBL_MV_FACT_ORG_TMP A
JOIN TBL_MV_FACT_ORG_TMP B 
ON A.concatvalue = B.concatvalueparent and A.organization_ref_id <> B.organization_ref_id
GROUP BY A.organization_ref_id
) B 
where  A.organization_ref_id = B.organization_ref_id;




--12 seconds
CREATE TEMP TABLE employees AS 
SELECT DISTINCT dim_employee_organization.customer_id,
dim_employee_organization.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_employee_organization.ext_ref_id) AS total,
upper(dim_employee_organization.customer_id::text||dim_employee_organization.employer_id||dim_employee_organization.path) as concatvalue
FROM dim_employee_organization
WHERE dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
GROUP BY dim_employee_organization.customer_id, dim_employee_organization.employer_id, (upper(dim_employee_organization.path)),
upper(dim_employee_organization.customer_id::text||dim_employee_organization.employer_id||dim_employee_organization.path);

--15 min

UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_employees =  COALESCE(B.total, 0::bigint)
FROM employees B
WHERE A.concatvalue = B.concatvalue;

UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_employees =  COALESCE(count_employees, 0::bigint);



--update alll null values to zero 

update TBL_MV_FACT_ORG_TMP
set 
count_children = COALESCE(count_children,0),
count_descendents= COALESCE(count_descendents,0),
count_employees= COALESCE(count_employees,0),
count_cases= COALESCE(count_cases,0),
count_open_cases= COALESCE(count_open_cases,0),
count_closed_cases= COALESCE(count_closed_cases,0),
count_todos= COALESCE(count_todos,0),
count_open_todos= COALESCE(count_open_todos,0),
count_closed_todos= COALESCE(count_closed_todos,0),
count_overdue_todos= COALESCE(count_overdue_todos,0);


--249 milli seconds
DELETE FROM  TBL_MV_FACT_ORG_BASEDATA;

INSERT INTO TBL_MV_FACT_ORG_BASEDATA
(
dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,org_level,count_children,
count_descendents,count_employees,count_cases,count_open_cases,count_closed_cases,count_todos,count_open_todos,
count_closed_todos,count_overdue_todos
)
SELECT a.dim_organization_id,
a.organization_ref_id,
a.customer_ref_id,
a.employer_ref_id,
a.org_level,
sum(a.count_children) AS count_children,
sum(a.count_descendents) AS count_descendents,
sum(a.count_employees) AS count_employees,
sum(a.count_cases) AS count_cases,
sum(a.count_open_cases) AS count_open_cases,
sum(a.count_closed_cases) AS count_closed_cases,
sum(a.count_todos) AS count_todos,
sum(a.count_open_todos) AS count_open_todos,
sum(a.count_closed_todos) AS count_closed_todos,
sum(a.count_overdue_todos) AS count_overdue_todos
FROM TBL_MV_FACT_ORG_TMP a
GROUP BY a.dim_organization_id, a.organization_ref_id, a.customer_ref_id, a.employer_ref_id, a.org_level;

DROP TABLE employees;
DROP TABLE TBL_MV_FACT_ORG_TMP;
DROP TABLE cases ;
DROP table todos ;
 
END;
$$ LANGUAGE plpgsql;





 



CREATE OR REPLACE FUNCTION public.utc()
   RETURNS timestamp without time zone
    LANGUAGE 'plpgsql'
   AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$;

	/* Create lookup category table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup_category
	(
		lookup_category VARCHAR(60) NOT NULL,
		description text,
		CONSTRAINT dim_lookup_category_pkey PRIMARY KEY (lookup_category)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup_category
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup_category TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup_category TO etldw;
	GRANT ALL ON TABLE public.dim_lookup_category TO postgres;
-- Table: public.dbversion

-- DROP TABLE public.dbversion;

CREATE TABLE public.dbversion
(
    dbversion_id bigint NOT NULL,
    created_date timestamp with time zone NOT NULL DEFAULT public.utc(),
    major_num integer DEFAULT 0,
    minur_num integer DEFAULT 0,
    notes text,
    CONSTRAINT dbversion_pkey PRIMARY KEY (dbversion_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dbversion
    OWNER to postgres;

GRANT SELECT ON TABLE public.dbversion TO absence_tracker;

GRANT ALL ON TABLE public.dbversion TO etldw;

GRANT ALL ON TABLE public.dbversion TO postgres;

--Missing entry
ALTER TABLE dbversion ALTER COLUMN created_date SET DEFAULT public.utc();
-- Table: public.db_update

-- DROP TABLE public.db_update;

CREATE TABLE public.db_update
(
    db_update_id bigint DEFAULT nextval('db_update_seq'::regclass) NOT NULL,
	db_update_record_type text NOT NULL,
	db_update_filename text,
	db_update_file_hash text,
	db_update_created_date timestamp with time zone DEFAULT now(),
	db_update_modified_date timestamp with time zone,
	db_update_ignore boolean,
	PRIMARY KEY(db_update_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.db_update
    OWNER to postgres;

GRANT SELECT ON TABLE public.db_update TO absence_tracker;

GRANT ALL ON TABLE public.db_update TO etldw;

GRANT ALL ON TABLE public.db_update TO postgres;
-- Table: public.dim_accommodation

-- DROP TABLE public.dim_accommodation;

CREATE TABLE public.dim_accommodation
(
    dim_accommodation_id bigint NOT NULL DEFAULT nextval('dim_accommodation_dim_accommodation_id_seq'::regclass),
    ext_ref_id character varying(36),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    case_id character varying(24),
    accommodation_type text,
    accommodation_type_code text,
    duration integer,
    resolution text,
    resolved boolean,
    resolved_date timestamp with time zone,
    granted boolean,
    granted_date timestamp with time zone,
    cost double precision,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    determination integer,
    status integer,
    cancel_reason integer,
    cancel_reason_other_desc text,
	denial_reason_code text,
    work_related boolean,
    created_date timestamp with time zone DEFAULT public.utc(),
	is_deleted boolean default false,
	record_hash text,
    min_approved_date timestamp with time zone,
    max_approved_date timestamp with time zone,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_accommodation_pkey PRIMARY KEY (dim_accommodation_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_accommodation
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_accommodation TO absence_tracker;
GRANT ALL ON TABLE public.dim_accommodation TO etldw;
GRANT ALL ON TABLE public.dim_accommodation TO postgres;

-- Index: dim_accommodation_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_accommodation_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_accommodation_cur_ver_ref_id_is_del
    ON public.dim_accommodation USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_accommodation_usage

-- DROP TABLE public.dim_accommodation_usage;

CREATE TABLE public.dim_accommodation_usage
(
    dim_accommodation_id bigint,
    ext_ref_id character varying(36),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    determination integer,
	is_deleted boolean default false
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_accommodation_usage
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_accommodation_usage TO absence_tracker;
GRANT ALL ON TABLE public.dim_accommodation_usage TO etldw;
GRANT ALL ON TABLE public.dim_accommodation_usage TO postgres;

-- Index: dim_accommodation_usage_dim_accommodation_id

-- DROP INDEX public.dim_accommodation_usage_dim_accommodation_id;

CREATE INDEX dim_accommodation_usage_dim_accommodation_id
    ON public.dim_accommodation_usage USING btree
    (dim_accommodation_id)
    TABLESPACE pg_default;
-- Table: public.dim_attachment

-- Create Table dim_attachment
	CREATE TABLE IF NOT EXISTS public.dim_attachment
	(
		dim_attachment_id bigint NOT NULL DEFAULT nextval('dim_attachment_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		checked boolean,
		content_length text,
		content_type text,
		attachment_type int,
		created_by_name text,
		description text,
		file_id character varying(24),
		file_Name text,
		public boolean,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		time_sample_date timestamp with time zone,
		time_total_minutes int,
		employee_number text,
		dim_variable_schedule_time_id int,
		CONSTRAINT dim_attachment_pkey PRIMARY KEY (dim_attachment_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_attachment
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
	GRANT ALL ON TABLE public.dim_attachment TO etldw;
	GRANT ALL ON TABLE public.dim_attachment TO postgres;

-- Index: dim_attachment_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_attachment_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_attachment_cur_ver_ref_id_is_del
    ON public.dim_attachment USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_cases

-- DROP TABLE public.dim_cases;

CREATE TABLE IF NOT EXISTS public.dim_cases
(
    dim_case_id bigint NOT NULL DEFAULT nextval('dim_cases_dim_case_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employee_id character varying(24),
    case_reporter_id character varying(24),
    case_number text,
    customer_id character varying(24),
    employer_id character varying(24),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    status integer,
    created_date timestamp with time zone DEFAULT utc(),
    spouse_case character varying(24),
    employer_case_number text,
    type integer,
    case_reason text,
    cancel_reason integer,
    assigned_to_name text,
    case_closure_reason integer,
	case_closure_reason_other text,
	case_closure_other_reason_details text,
    case_primary_diagnosis_code text,
    case_primary_daignosis_name text,
    case_secondary_diagnosis_code text,
    case_secondary_daignosis_name text,
    primary_path_text text,
    primary_path_min_days bigint,
    primary_path_max_days bigint,
    hospitalization boolean,
    condition_start_date timestamp with time zone,
    related_case_number text,
    related_case_type integer,
    job_office_location_code text,
    job_office_location_name text,
    occurances bigint,
    frequency bigint,
    frequency_type integer,
    frequency_unit_type integer,
    duration bigint,
    duration_type integer,
	intermittent_type integer,
    is_accommodation boolean,
    is_work_related boolean,
    case_is_denied boolean,
    case_is_approved boolean,
    case_is_eligible integer,
    case_determination integer,
    case_fmla_is_approved boolean,
    case_fmla_is_denied boolean,
    case_fmla_projected_usage text,
    case_fmla_exhaust_date timestamp with time zone,
    case_max_approved_thru_date timestamp with time zone,
    case_min_approved_thru_date timestamp with time zone,
    case_accom_status integer,
    case_accom_start_date timestamp with time zone,
    case_accom_end_date timestamp with time zone,
    case_accom_min_approved_date timestamp with time zone,
    case_accom_max_approved_date timestamp with time zone,
    case_accom_determination integer,
    case_accom_cancel_reason integer,
    case_accom_cancel_reason_other text,
    case_accom_duration text,
    case_accom_duration_type text,
    ext_created_by character varying(24),
    custom_fields jsonb,
    case_created_by_email text,
    case_created_by_first_name text,
    case_created_by_last_name text,
    assigned_to_id character varying(24),
    case_reporter_type_code text,
    case_reporter_type_name text,
    case_reporter_last_name text,
    case_reporter_first_name text,
	relationship_type_code text,
	relationship_type_name text,
	is_intermittent boolean,
	is_deleted boolean default false,
    return_to_work timestamp with time zone,
    delivery_date timestamp with time zone,
    bonding_start_date timestamp with time zone,
    bonding_end_date timestamp with time zone,
    illness_or_injury_date timestamp with time zone,
    hospital_admission_date timestamp with time zone,
    hospital_release_date timestamp with time zone,
    release_received timestamp with time zone,
    paperwork_received timestamp with time zone,
    accommodation_added timestamp with time zone,
    case_closed timestamp with time zone,
    case_cancelled timestamp with time zone,
    adoption_date timestamp with time zone,
    estimated_return_to_work timestamp with time zone,
    employer_holiday_schedule_changed timestamp with time zone,
	case_cert_start_date timestamp with time zone,
	case_cert_end_date timestamp with time zone,

	wr_date_of_injury timestamp with time zone,
	wr_is_pvt_healthcare boolean,
	wr_is_reportable boolean,
	wr_place_of_occurance text,
	wr_work_classification bigint,
	wr_type_of_injury bigint,
	wr_days_away_from_work int,
	wr_restricted_days int,

	wr_provider_first_name character varying(512),
	wr_provider_last_name character varying(512),
	wr_provider_company_name character varying(512),
	wr_provider_address1 character varying(512),
	wr_provider_address2 character varying(512),
	wr_provider_city character varying(512),
	wr_provider_state character varying(512),
	wr_provider_country character varying(512),
	wr_provider_postalcode character varying(20),

	wr_is_treated_in_emer_room boolean,
	wr_is_hospitalized boolean,
	wr_time_started_working character varying(100),
	wr_time_of_injury character varying(100),
	wr_activity_before_injury text,
	wr_what_happened text,
	wr_injury_details text,
	wr_what_harmed_empl text,
	wr_date_of_death timestamp with time zone,

	wr_is_sharp boolean,
	wr_sharp_body_part text,
	wr_type_of_sharp text,
	wr_brand_of_sharp text,
	wr_model_of_sharp text,
	wr_body_fluid text,
	wr_is_engn_injury_protection boolean,
	wr_is_protective_mech_activated boolean,
	wr_when_sharp_incident_happened bigint,
	wr_sharp_job_classification bigint,
	wr_sharp_other_job_classification text,
	wr_sharp_location_department bigint,
	wr_sharp_other_location_dept text,
	wr_sharp_procedure bigint,
	wr_sharp_other_procedure text,
	wr_sharp_exposure_detail text,
	cg_risk_asmt_score decimal,
	cg_risk_asmt_status character varying(512),
	cg_mid_range_all_absence decimal,
	cg_at_risk_all_absence decimal,
	cg_best_practice_days decimal,
	cg_actual_days_lost decimal,

	closure_reason bigint,
	closure_reason_detail text,
	description text,
	narrative text,
	is_rpt_auth_submitter boolean,
	send_e_communication boolean,
	e_communication_request_date timestamp with time zone,
	risk_profile_code character varying(255),
	risk_profile_name character varying(255),
	current_office_location text,
	total_paid decimal,
	total_offset decimal,
	apply_offsets_by_default boolean,
	waive_waiting_period boolean,
	pay_schedule_id character varying(36),

	alt_phone_number text,
	alt_email text,
	alt_address1 text,
	alt_address2 text,
	alt_city text,
	alt_state text,
	alt_postal_code text,
	alt_country text,

	ext_modified_by character varying(24),

	actual_duration int,
	exceeded_duration int,
	has_relapse boolean,

    CONSTRAINT dim_cases_pkey PRIMARY KEY (dim_case_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_cases
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_cases TO absence_tracker;
GRANT ALL ON TABLE public.dim_cases TO etldw;
GRANT ALL ON TABLE public.dim_cases TO postgres;


-- Index: dim_cases_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_cases_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_cases_cur_ver_ref_id_is_del
    ON public.dim_cases USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_case_authorized_submitter

-- Create Table dim_case_authorized_submitter
	CREATE TABLE IF NOT EXISTS public.dim_case_authorized_submitter
	(
		dim_case_authorized_submitter_id bigint NOT NULL DEFAULT nextval('dim_case_authorized_submitter_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		authorized_submitter_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		contact_type_code text,
		contact_type_name text,	
		contact_first_name text,    
		contact_last_name text,	
		contact_email text,
		contact_alt_email text,
		contact_work_phone text,
		contact_home_phone text,
		contact_cell_phone text,
		contact_fax text,
		contact_street text,
		contact_address1 text,
		contact_address2 text,
		contact_city text,
		contact_state text,
		contact_postal_code text,
		contact_country text,	   	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		CONSTRAINT dim_case_authorized_submitter_pkey PRIMARY KEY (dim_case_authorized_submitter_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_authorized_submitter
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_authorized_submitter TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO etldw;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO postgres;

-- Index: dim_case_authorized_submitter_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_authorized_submitter_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_authorized_submitter_cur_ver_ref_id_is_del
    ON public.dim_case_authorized_submitter USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_case_event_date

-- Create Table dim_case_event_date
	CREATE TABLE IF NOT EXISTS public.dim_case_event_date
	(
		dim_case_event_date_id bigint NOT NULL DEFAULT nextval('dim_case_event_date_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		event_type bigint,		
		event_date timestamp with time zone,
		record_hash text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),		
		CONSTRAINT dim_case_event_date_pkey PRIMARY KEY (dim_case_event_date_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_event_date
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_event_date TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_event_date TO etldw;
	GRANT ALL ON TABLE public.dim_case_event_date TO postgres;

	-- Index: dim_case_event_date_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_event_date_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_event_date_cur_ver_ref_id_is_del
    ON public.dim_case_event_date USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_event_date_case_id_index ON public.dim_case_event_date (case_id);
-- Table: public.dim_case_note

-- Create Table dim_case_note
	CREATE TABLE IF NOT EXISTS public.dim_case_note
	(
		dim_case_note_id bigint NOT NULL DEFAULT nextval('dim_case_note_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24),
		employee_id varchar(24),
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accomm_id	varchar(36),
		accomm_question_id	text,
		answer	boolean,		
		cert_id	text,
		contact	text,
		copied_from	text,
		copied_from_employee	text,		
		employee_number	text,		
		entered_by_name	text,		
		notes	text,
		process_path	text,
		public	boolean,
		request_date	timestamp with time zone,
		request_id	text,
		s_answer	text,
		work_restriction_id varchar(36),
		category_code text,
		category_name text,
		category int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		CONSTRAINT dim_case_note_pkey PRIMARY KEY (dim_case_note_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_note
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_note TO etldw;
	GRANT ALL ON TABLE public.dim_case_note TO postgres;

-- Index: dim_case_note_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_note_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_note_cur_ver_ref_id_is_del
    ON public.dim_case_note USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_case_note_category

-- DROP TABLE public.dim_case_note_category;

CREATE TABLE public.dim_case_note_category
(
  dim_case_note_category_id bigint NOT NULL DEFAULT nextval('dim_case_note_category_id_seq'::regclass),
  dim_case_note_id bigint,
  category_id character varying(24) NOT NULL,
  category_code text,
  category_name text,
  cat_order integer,
  event_date timestamp with time zone,
  CONSTRAINT dim_case_note_category_pkey PRIMARY KEY (dim_case_note_category_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dim_case_note_category
  OWNER TO postgres;
GRANT ALL ON TABLE public.dim_case_note_category TO postgres;
GRANT SELECT ON TABLE public.dim_case_note_category TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_note_category TO etldw;
-- Table: public.dim_case_pay_period

-- Create Table dim_case_pay_period
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period
	(
		dim_case_pay_period_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		status int,
		locked_date timestamp with time zone,
		locked_by	varchar(24),
		max_weekly_pay_amount double precision,		
		payroll_date_override timestamp with time zone,
		end_date_override timestamp with time zone,
		payroll_date timestamp with time zone,
		payroll_date_override_by varchar(24),
		end_date_override_by varchar(24),		
		offset_amount double precision,
		total double precision,
		sub_total double precision,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_pay_period_pkey PRIMARY KEY (dim_case_pay_period_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period TO postgres;

-- Index: dim_case_pay_period_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_pay_period_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_pay_period_cur_ver_ref_id_is_del
    ON public.dim_case_pay_period USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;


create index IF NOT EXISTS dim_case_pay_period_caseid
  on dim_case_pay_period  USING btree (case_id, ver_is_current, ver_seq);
-- Table: public.dim_case_pay_period_detail

-- Create Table dim_case_pay_period_detail
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period_detail
	(
		dim_case_pay_period_detail_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_detail_id_seq'::regclass),
		dim_case_pay_period_id bigint,
		ext_ref_id character varying(36),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		is_Offset boolean,
		max_payment_amount double precision,
		pay_amount_override_value double precision,
		pay_amount_system_value double precision,
		payment_tier_percentage double precision,
		pay_percentage_override_by varchar(24),
		pay_percentage_override_date timestamp with time zone,
		pay_percentage_override_value double precision,
		pay_percentage_system_value double precision,
		policy_code text,
		policy_name text,
		salary_total text,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_case_pay_period_detail_pkey PRIMARY KEY (dim_case_pay_period_detail_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period_detail
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period_detail TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO postgres;
-- Table: public.dim_case_policy 

-- Create Table dim_case_policy 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy
	(
		dim_case_policy_id bigint NOT NULL DEFAULT nextval('dim_case_policy_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL,
		policy_code varchar(50) NOT NULL,
		policy_name varchar(255) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		case_type int,
		eligibility int,
		determination int,
		denial_reason int,
		denial_reason_other text,
		payout_percentage decimal,
		crosswalk_codes jsonb,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		
		actual_duration int,
		exceeded_duration int,
		denial_reason_code text,
		denial_reason_description text,

		CONSTRAINT dim_case_policy_pkey PRIMARY KEY (dim_case_policy_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy TO postgres;

-- Index: dim_accommodation_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_accommodation_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_policy_cur_ver_ref_id_is_del
    ON public.dim_case_policy USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_policy_caseid_verseq
  on public.dim_case_policy USING btree(case_id, ver_is_current,ver_seq)
  TABLESPACE pg_default;
--- Table for dim_case_policy_usage   
	CREATE TABLE IF NOT EXISTS public.dim_case_policy_usage
	(
		dim_case_policy_usage_id bigint NOT NULL DEFAULT nextval('dim_case_policy_usage_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		dim_case_policy_id  bigint NOT NULL,
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL, 
		policy_code varchar(50) NOT NULL,
		date_used timestamp with time zone,
		minutes_used int,
		minutes_in_day int,
		hours_used decimal,
		hours_in_day decimal,
		user_entered Boolean,
		determination int,
		denial_reason int,
		denial_reason_other text,
		determined_by varchar(24),
		intermittent_determination int,
		is_locked Boolean,
		uses_time Boolean,
	    is_intermittent_restriction Boolean,
		percent_of_day decimal,
		status int,
		created_date timestamp with time zone DEFAULT utc(),
		denial_reason_code text,
		denial_reason_description text,

		CONSTRAINT dim_case_policy_usage_pkey PRIMARY KEY (dim_case_policy_usage_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy_usage
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy_usage TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO postgres;


CREATE INDEX IF NOT EXISTS dim_case_policy_usage_dim_case_policy_id
    ON public.dim_case_policy_usage USING btree
    (dim_case_policy_id)
    TABLESPACE pg_default;
-- Table: public.dim_communication

-- Create Table dim_communication
	CREATE TABLE IF NOT EXISTS public.dim_communication
	(
		dim_communication_id bigint NOT NULL DEFAULT nextval('dim_communication_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		attachment_id varchar(24),
		body text,
		communication_type int,
		created_by_email text,
		created_by_name text,
		email_replies_type int,	
		is_draft boolean,
		name text,
		public boolean,
		first_send_date timestamp with time zone,
		last_send_date timestamp with time zone,
		subject text,
		template text,
		to_do_item_id varchar(24),
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_communication_id_pkey PRIMARY KEY (dim_communication_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication TO etldw;
	GRANT ALL ON TABLE public.dim_communication TO postgres;

-- Index: dim_communication_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_id_cur_ver_ref_id_is_del
    ON public.dim_communication USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_communication_paperwork

-- Create Table dim_communication_paperwork
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork
	(
		dim_communication_paperwork_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_id_seq'::regclass),
		communication_id character varying(24), 
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		attachment_id	varchar(24),		
		checked	 boolean,
		due_date	timestamp with time zone,
		return_attachment_id	varchar(24),
		status	int,
		status_date	 timestamp with time zone,
		
		paperwork_id	varchar(24),
		paperwork_cby	varchar(24),
		paperwork_cdt	timestamp with time zone,
		paperwork_code	text,
		paperwork_customer_id	varchar(24),
		paperwork_description	text,
		paperwork_doc_type	int,
		paperwork_employer_id	varchar(24),
		paperwork_enclosure_text	text,		
		paperwork_file_id	varchar(24),
		paperwork_file_name	text,
		paperwork_is_deleted boolean default false,
		paperwork_mby	varchar(24),
		paperwork_mdt	timestamp with time zone,
		paperwork_name	text,
		paperwork_requires_review	boolean,
		paperwork_return_date_adjustment	text,
		paperwork_return_date_adjustment_days	int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_pkey PRIMARY KEY (dim_communication_paperwork_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO postgres;
	
	-- Index: dim_communication_paperwork_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_paperwork_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_paperwork_id_cur_ver_ref_id_is_del
    ON public.dim_communication_paperwork USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

--Missing entries
ALTER TABLE dim_communication_paperwork ADD COLUMN case_id character varying(24);
ALTER TABLE dim_communication_paperwork ADD COLUMN paperwork_review_status integer;
 
-- Table: public.dim_communication_paperwork_field

-- Create Table dim_communication_paperwork_field
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork_field
	(
		dim_communication_paperwork_field_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_field_id_seq'::regclass),
		dim_communication_paperwork_id bigint,
		ext_ref_id character varying(36),
		name text,
		status int,
		type int,
		required boolean default false,
		field_order int,
		value text,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
        is_deleted boolean,
		CONSTRAINT dim_communication_paperwork_field_pkey PRIMARY KEY (dim_communication_paperwork_field_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork_field
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

--Missing entries
ALTER TABLE dim_communication_paperwork_field ALTER COLUMN is_deleted SET DEFAULT false;
ALTER TABLE dim_communication_paperwork_field ADD COLUMN ver_is_current boolean DEFAULT true;

-- Table: public.dim_communication_recipient

-- Create Table dim_communication_recipient
	CREATE TABLE IF NOT EXISTS public.dim_communication_recipient
	(
		dim_communication_recipient_id bigint NOT NULL DEFAULT nextval('dim_communication_recipient_id_seq'::regclass),
		dim_communication_id bigint,
		recipient_type int,
		ext_ref_id character varying(36),
		address_id	varchar(36),
		address_address1 text,
		address_city	text,
		address_country	text,
		address_postal_code	text,
		address_state	text,
		alt_email	text,
		email	text,
		fax	text,
		title text,
		first_name	text,
		middle_name text,
		last_name	text,
		home_email	text,
		company_name text,
		contact_person_name text,
		date_of_birth timestamp with time zone ,
		home_phone text,
		cell_phone text,
		is_primary boolean,
		CONSTRAINT dim_communication_recipient_pkey PRIMARY KEY (dim_communication_recipient_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_recipient
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_recipient TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_recipient TO etldw;
	GRANT ALL ON TABLE public.dim_communication_recipient TO postgres;
-- Table: public.dim_customer

-- DROP TABLE public.dim_customer;

CREATE TABLE public.dim_customer
(
    dim_customer_id bigint NOT NULL DEFAULT nextval('dim_customer_dim_customer_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    cust_customer_name text NOT NULL,
    cust_customer_url text,
    cust_contact_first_name text,
    cust_contact_last_name text,
    cust_contact_email text,
    cust_contact_address1 text,
    cust_contact_address2 text,
    cust_contact_city text,
    cust_contact_state text,
    cust_contact_postal_code text,
    cust_contact_country text,
    cust_billing_first_name text,
    cust_billing_last_name text,
    cust_billing_email text,
    cust_billing_address1 text,
    cust_billing_address2 text,
    cust_billing_city text,
    cust_billing_state text,
    cust_billing_postal_code text,
    cust_billing_country text,
    created_date timestamp with time zone DEFAULT utc(),
    cust_service_type text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_customer_pkey PRIMARY KEY (dim_customer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer TO absence_tracker;
GRANT ALL ON TABLE public.dim_customer TO etldw;
GRANT ALL ON TABLE public.dim_customer TO postgres;

-- Index: dim_customer_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_customer_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_customer_cur_ver_ref_id_is_del
    ON public.dim_customer USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
CREATE TABLE public.dim_customer_service_option
(
    dim_customer_service_option_id bigint NOT NULL DEFAULT nextval('dim_customer_service_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    is_deleted boolean,
    customer_id character varying(24),
    key text,
    value text,
    "order" integer,
    CONSTRAINT dim_customer_service_pkey PRIMARY KEY (dim_customer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_customer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_customer_service_option TO postgres;

-- Index: dim_employer_service_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_service_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_service_cur_ver_ref_id_is_del
    ON public.dim_customer_service_option USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_date

-- DROP TABLE public.dim_date;

CREATE TABLE public.dim_date
(
    dim_date_id bigint NOT NULL DEFAULT nextval('dim_date_dim_date_id_seq'::regclass),
    created_date timestamp with time zone NOT NULL DEFAULT utc(),
    iso8601date timestamp without time zone,
    year integer,
    month integer,
    day integer,
    minute integer,
    second integer,
    is_weekend boolean,
    is_weekday boolean,
    is_american_holiday boolean,
    quarter integer,
    day_of_week_name character varying(16),
    is_leap_year boolean,
    first_day_of_week timestamp with time zone,
    last_day_of_week timestamp with time zone,
    first_day_of_month timestamp with time zone,
    last_day_of_month timestamp with time zone,
    first_day_of_year timestamp with time zone,
    last_day_of_year timestamp with time zone,
    days_in_year integer,
    iso8601_week_of_year integer NOT NULL,
    closest_weekday timestamp with time zone NOT NULL,
    CONSTRAINT dim_date_pkey PRIMARY KEY (dim_date_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_date
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_date TO absence_tracker;
GRANT ALL ON TABLE public.dim_date TO etldw;
GRANT ALL ON TABLE public.dim_date TO postgres;
-- Table: public.dim_demand

-- DROP TABLE public.dim_demand;

CREATE TABLE public.dim_demand
(
    dim_demand_id bigint NOT NULL DEFAULT nextval('dim_demand_dim_demand_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    customer_id character varying(24),
    name text,
    code text,
    description text,
    order_data integer,
    category text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_demand_pkey PRIMARY KEY (dim_demand_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_demand
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_demand TO absence_tracker;
GRANT ALL ON TABLE public.dim_demand TO etldw;
GRANT ALL ON TABLE public.dim_demand TO postgres;

-- Index: dim_demand_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_demand_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_demand_cur_ver_ref_id_is_del
    ON public.dim_demand USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_demand_type

-- DROP TABLE public.dim_demand_type;

CREATE TABLE public.dim_demand_type
(
    dim_demand_type_id bigint NOT NULL DEFAULT nextval('dim_demand_type_dim_demand_type_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    customer_id character varying(24),
    description text,
    type_data text,
    order_data text,
    requirement_label text,
    restriction_label text,
    code text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_demand_type_pkey PRIMARY KEY (dim_demand_type_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_demand_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_demand_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_demand_type TO etldw;
GRANT ALL ON TABLE public.dim_demand_type TO postgres;

-- Index: dim_demand_type_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_demand_type_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_demand_type_cur_ver_ref_id_is_del
    ON public.dim_demand_type USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
CREATE SEQUENCE IF NOT EXISTS public.dim_denial_reason_id_seq 
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
-- Table: public.dim_denial_reason

-- DROP TABLE public.dim_denial_reason;

CREATE TABLE IF NOT EXISTS public.dim_denial_reason
(
    dim_denial_reason_id bigint NOT NULL DEFAULT nextval('dim_denial_reason_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    is_deleted boolean,
    customer_id character varying(24),
    description text,
    code text,
	disabled_date timestamp with time zone DEFAULT null,
    CONSTRAINT dim_denial_reason_pkey PRIMARY KEY (dim_denial_reason_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_denial_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_denial_reason TO absence_tracker;

GRANT ALL ON TABLE public.dim_denial_reason TO etldw;

GRANT ALL ON TABLE public.dim_denial_reason TO postgres;

-- Index: dim_denial_reason_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_denial_reason_cur_ver_ref_id_is_del;

CREATE INDEX dim_denial_reason_cur_ver_ref_id_is_del
    ON public.dim_denial_reason USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employee

-- DROP TABLE public.dim_employee;

CREATE TABLE public.dim_employee
(
    dim_employee_id bigint NOT NULL DEFAULT nextval('dim_employee_dim_employee_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employee_number text,
    first_name text,
    middle_name text,
    last_name text,
    status bigint,
    salary double precision,
    pay_type bigint,
    gender bigint,
    dob timestamp with time zone,
    work_type bigint,
    cost_center_code text,
    job_title text,
    department text,
    service_date timestamp with time zone,
    hire_date timestamp with time zone,
    rehire_date timestamp with time zone,
    termination_date timestamp with time zone,
    work_state text,
    work_country text,
    pay_schedule_id character varying(24),
    pay_schedule_name text,
    meets50in75mile_rule boolean,
    key_employee boolean,
    exempt boolean,
    military_status bigint,
    spouse_employee_number text,
    employer_name text,
    employer_id character varying(24),
    customer_id character varying(24),
    empl_employee_created_by_email text,
    empl_employee_created_by_first_name text,
    empl_employee_created_by_last_name text,
    email text,
    alt_email text,
    work_phone text,
    home_phone text,
    cell_phone text,
    alt_phone text,
    office_location text,
    empl_pay_schedule text,
    created_date timestamp with time zone DEFAULT utc(),
    address1 text,
    address2 text,
    city text,
    state text,
    postal_code text,
    country text,
    alt_address1 text,
    alt_address2 text,
    alt_city text,
    alt_state text,
    alt_postal_code text,
    alt_country text,
    job_start_date timestamp with time zone,
    job_end_date timestamp with time zone,
    job_code text,
    job_name text,
    job_office_location_code text,
    job_office_location_name text,
    custom_fields jsonb,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean default false,
    hours_worked double precision,
    hours_worked_as_of timestamp with time zone,
	average_minutes_worked_per_week double precision,
	average_minutes_worked_per_week_as_of timestamp with time zone,
	ssn character varying(20),
	job_classification bigint,
	risk_profile_code character varying(255),
	risk_profile_name character varying(255),
	risk_profile_description text,
	risk_profile_order int,
	start_day_of_week character varying(12),

	ext_created_by character varying(24),
	ext_modified_by character varying(24),

    CONSTRAINT dim_employee_pkey PRIMARY KEY (dim_employee_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee TO etldw;
GRANT ALL ON TABLE public.dim_employee TO postgres;

-- Index: dim_employee_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_cur_ver_ref_id_is_del
    ON public.dim_employee USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;


-- Table: public.dim_employee_contact

-- DROP TABLE public.dim_employee_contact;

CREATE TABLE public.dim_employee_contact
(
    dim_employee_contact_id bigint NOT NULL DEFAULT nextval('dim_employee_contact_dim_employee_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
	employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    cdt timestamp with time zone,
    ext_created_by character varying(24),
    mdt timestamp with time zone,
    ext_modified_by character varying(24),
	contact_type_code text,
	contact_type_name text,
	company_name text,
	title text,
    first_name text,
    middle_name text,
    last_name text,
	dob timestamp with time zone,
    email text,
    alt_email text,
    work_phone text,
    home_phone text,
    cell_phone text,
    fax text,
	street text,
    address1 text,
    address2 text,
    city text,
    state text,
    postal_code text,
    country text,
	is_primary boolean,
	contact_person_name text,
	years_of_age int,
	military_status int,
	contact_position int,
    employee_number text,
	is_deleted boolean default false,
    created_date timestamp with time zone DEFAULT utc(),
    CONSTRAINT dim_employee_contact_pkey PRIMARY KEY (dim_employee_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_contact TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_contact TO etldw;
GRANT ALL ON TABLE public.dim_employee_contact TO postgres;

-- Index: dim_employee_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_contact_cur_ver_ref_id_is_del
    ON public.dim_employee_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employee_job

-- DROP TABLE public.dim_employee_job;

CREATE TABLE public.dim_employee_job
(
    dim_employee_job_id bigint NOT NULL DEFAULT nextval('dim_employee_job_dim_employee_job_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    job_name text,
    job_code text,
    status_value int,
    status_reason int,
    status_comments text,
    status_username text,
    status_userid text,
    status_date timestamp with time zone,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
	is_deleted boolean default false,
    CONSTRAINT dim_employee_job_pkey PRIMARY KEY (dim_employee_job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_job
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_job TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_job TO etldw;

GRANT ALL ON TABLE public.dim_employee_job TO postgres;
-- Table: public.dim_employee_necessity

-- DROP TABLE public.dim_employee_necessity;

CREATE TABLE public.dim_employee_necessity
(
    dim_employee_necessity_id bigint NOT NULL DEFAULT nextval('dim_employee_necessity_dim_employee_necessity_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    name text,
    type int,
    description text,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    case_id text,
    case_number text,
	is_deleted boolean default false,
    CONSTRAINT dim_employee_necessity_pkey PRIMARY KEY (dim_employee_necessity_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_necessity
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_necessity TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_necessity TO etldw;

GRANT ALL ON TABLE public.dim_employee_necessity TO postgres;
-- Table: public.dim_employee_note

-- DROP TABLE public.dim_employee_note;

CREATE TABLE public.dim_employee_note
(
    dim_employee_note_id bigint NOT NULL DEFAULT nextval('dim_employee_note_note_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    note text,
    is_public boolean,
    category_code text,
    category_name text,    
    CONSTRAINT dim_employee_note_pkey PRIMARY KEY (dim_employee_note_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_note
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_note TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_note TO etldw;

GRANT ALL ON TABLE public.dim_employee_note TO postgres;
-- Table: public.dim_employee_organization

-- DROP TABLE public.dim_employee_organization;

CREATE TABLE public.dim_employee_organization
(
    dim_employee_organization_id bigint NOT NULL DEFAULT nextval('dim_employee_organization_dim_employee_organization_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    employee_number text,
    code text,
    name text,
    description text,
    path text,
    type_code text,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_employee_organization_pkey PRIMARY KEY (dim_employee_organization_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_organization
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_organization TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_organization TO etldw;
GRANT ALL ON TABLE public.dim_employee_organization TO postgres;

-- Index: dim_employee_organization_employee_path_ref

-- DROP INDEX public.dim_employee_organization_employee_path_ref;

CREATE INDEX IF NOT EXISTS dim_employee_organization_employee_path_ref
    ON public.dim_employee_organization USING btree
    (customer_id, employer_id, path, employee_number, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Index: dim_employee_organization_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_organization_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_organization_cur_ver_ref_id_is_del
    ON public.dim_employee_organization USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employee_restriction

-- DROP TABLE public.dim_employee_restriction;

CREATE TABLE public.dim_employee_restriction
(
    dim_employee_restriction_id bigint NOT NULL DEFAULT nextval('dim_employee_restriction_dim_employee_restriction_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    wkrs_restriction_name text,
    wkrs_restriction_code text,
    wkrs_restriction_category text,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    case_id character varying(24),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_employee_restriction_pkey PRIMARY KEY (dim_employee_restriction_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_restriction
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_restriction TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_restriction TO etldw;
GRANT ALL ON TABLE public.dim_employee_restriction TO postgres;

-- Index: dim_employee_restriction_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_restriction_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_restriction_cur_ver_ref_id_is_del
    ON public.dim_employee_restriction USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employee_restriction_entry

-- DROP TABLE public.dim_employee_restriction_entry;

CREATE TABLE public.dim_employee_restriction_entry
(
    dim_employee_restriction_entry_id bigint NOT NULL DEFAULT nextval('dim_employee_restriction_entr_dim_employee_restriction_entr_seq'::regclass),
    dim_employee_restriction_id bigint,
    case_id character varying(24),
    ext_ref_demand_type_id character varying(24),
    wkrs_restriction_type text,
    wkrs_restriction text,
	is_deleted boolean default false,
	ver_is_current boolean default true,
    CONSTRAINT dim_employee_restriction_entry_pkey PRIMARY KEY (dim_employee_restriction_entry_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_restriction_entry
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_restriction_entry TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO etldw;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO postgres;

-- Index: dim_employee_restriction_entry_dim_employee_restriction_id

-- DROP INDEX public.dim_employee_restriction_entry_dim_employee_restriction_id;

CREATE INDEX IF NOT EXISTS dim_employee_restriction_entry_dim_employee_restriction_id
    ON public.dim_employee_restriction_entry USING btree
    (dim_employee_restriction_id, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employer

-- DROP TABLE public.dim_employer;

CREATE TABLE public.dim_employer
(
    dim_employer_id bigint NOT NULL DEFAULT nextval('dim_employer_dim_employer_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    name text,
    reference_code text,
    url text,
    reset_month integer,
    reset_day_of_month integer,
	start_date date,
	end_date date,
    fml_period_type integer,
    allow_intermittent_for_birth_adoption_or_foster_care boolean,
    ignore_schedule_for_prior_hours_worked boolean,
	ignore_average_minutes_worked_per_week boolean,
    enable50in75mile_rule boolean,
    enable_spouse_at_same_employer_rule boolean,
    ft_weekly_work_hours double precision,
    publicly_traded_company boolean,
    customer_id character varying(24),
    contact_first_name text,
    contact_last_name text,
    contact_email text,
    contact_address1 text,
    contact_address2 text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    is_deleted boolean,
    created_date timestamp with time zone DEFAULT utc(),
    eplr_service_type text,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_employer_pkey PRIMARY KEY (dim_employer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer TO absence_tracker;
GRANT ALL ON TABLE public.dim_employer TO etldw;
GRANT ALL ON TABLE public.dim_employer TO postgres;

-- Index: dim_employer_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_cur_ver_ref_id_is_del
    ON public.dim_employer USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employer_contact

-- DROP TABLE public.dim_employer_contact;

CREATE TABLE IF NOT EXISTS public.dim_employer_contact
(
    dim_employer_contact_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    contact_firstname text,
    contact_lastname text,
    contact_email text,
    contact_alt_email text,
    contact_work_phone text,
    contact_fax text,
    contact_address1 text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean,
    CONSTRAINT dim_employer_contact_pkey PRIMARY KEY (dim_employer_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact TO postgres;

-- Index: dim_employer_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_contact_cur_ver_ref_id_is_del
    ON public.dim_employer_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Missing entries
ALTER TABLE dim_employer_contact ADD COLUMN contact_address text;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_is_current DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_is_expired DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_prev_id DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_root_id DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_seq DROP NOT NULL;
-- Table: public.dim_employer_contact_types

-- DROP TABLE public.dim_employer_contact_types;

CREATE TABLE public.dim_employer_contact_types
(
    dim_employer_contact_types_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_types_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    is_deleted boolean,
    customer_id character varying(24),
    name text,
    code text,
    CONSTRAINT dim_employer_contact_types_pkey PRIMARY KEY (dim_employer_contact_types_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact_types TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact_types TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact_types TO postgres;

-- Index: dim_employer_contact_types_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_types_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_contact_types_cur_ver_ref_id_is_del
    ON public.dim_employer_contact_types USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_employer_service_option

-- DROP TABLE public.dim_employer_service_option;

CREATE TABLE public.dim_employer_service_option
(
    dim_employer_service_option_id bigint NOT NULL DEFAULT nextval('dim_employer_service_option_dim_employer_service_option_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    is_deleted boolean,
    key text,
    value text,
    cusomerserviceoptionid character varying(24),
    CONSTRAINT dim_employer_service_option_pkey PRIMARY KEY (dim_employer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_employer_service_option TO postgres;
-- Table: public.dim_event

-- DROP TABLE public.dim_event;

CREATE TABLE public.dim_event
(
    dim_event_id bigint NOT NULL DEFAULT nextval('dim_event_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    employee_id character varying(24),
    todo_item_id character varying(36),
    accomodation_id character varying(36),
    active_accomodation_id character varying(36),
    accomodation_type_code text,
    accomodation_end_date timestamp with time zone,
    determination integer,
    denial_explanation text,
    case_id character varying(36),
    accomodation_type_id character varying(36),
    template text,
    communication_type integer,
    name text,
    event_type text,
    is_deleted boolean,
    applied_policy_id character varying(36),
    attachment_id character varying(36),
    close_case boolean,
    communication_id character varying(36),
    condition_start_date timestamp with time zone,
    contact_employee_due_date timestamp with time zone,
    contact_hcp_due_date timestamp with time zone,
    denial_reason integer,
    description text,
    due_date timestamp with time zone,
    ergonomic_assessment_date timestamp with time zone,
    extended_grace_period boolean,
    first_exhaustion_date timestamp with time zone,
    general_health_condition text,
    has_work_restriction boolean,
    illness_injury_date timestamp with time zone,
    new_due_date timestamp with time zone,
    paperwork_attachment_id character varying(36),
    paperwork_due_date timestamp with time zone,
    paperwork_id character varying(36),
    paperwork_name text,
    paperwork_received boolean,
    policy_code character varying(36),
    policy_id character varying(36),
    reason integer,
    requires_review boolean,
    return_attachment_id character varying(36),
    return_to_work_date timestamp with time zone,
    rtw boolean,
    source_workflow_activity_activityid character varying(36),
    source_workflow_activityid character varying(36),
    source_workflow_instanceid character varying(36),
    workflow_activity_id character varying(36),
    CONSTRAINT dim_event_pkey PRIMARY KEY (dim_event_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_event
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_event TO absence_tracker;

GRANT ALL ON TABLE public.dim_event TO etldw;

GRANT ALL ON TABLE public.dim_event TO postgres;

-- Index: dim_event_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_event_cur_ver_ref_id_is_del;

CREATE INDEX dim_event_cur_ver_ref_id_is_del
    ON public.dim_event USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;

	/* Create lookup table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type varchar(60) NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		ADD CONSTRAINT fk_lookup_lookup_cataegory FOREIGN KEY (type) REFERENCES dim_lookup_category (lookup_category);

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;
-- Table: public.dim_lookup_accom_cancel_reason

-- DROP TABLE public.dim_lookup_accom_cancel_reason;

CREATE TABLE public.dim_lookup_accom_cancel_reason
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_accom_cancel_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_accom_cancel_reason TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_accom_cancel_reason TO etldw;
GRANT ALL ON TABLE public.dim_lookup_accom_cancel_reason TO postgres;

-- Index: dim_lookup_accom_cancel_reason_code

-- DROP INDEX IF EXISTS public.dim_lookup_accom_cancel_reason_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_accom_cancel_reason_code
    ON public.dim_lookup_accom_cancel_reason USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_acom_duration

-- DROP TABLE public.dim_lookup_acom_duration;

CREATE TABLE public.dim_lookup_acom_duration
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_acom_duration
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_acom_duration TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_acom_duration TO etldw;
GRANT ALL ON TABLE public.dim_lookup_acom_duration TO postgres;

-- Index: dim_lookup_acom_duration_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_acom_duration_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_acom_duration_code
    ON public.dim_lookup_acom_duration USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_accom_cancel_reason

-- DROP TABLE public.dim_lookup_case_accom_cancel_reason;

CREATE TABLE public.dim_lookup_case_accom_cancel_reason
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_accom_cancel_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_accom_cancel_reason TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_accom_cancel_reason TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_accom_cancel_reason TO postgres;

-- Index: dim_lookup_case_accom_cancel_reason_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_accom_cancel_reason_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_accom_cancel_reason_code
    ON public.dim_lookup_case_accom_cancel_reason USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_accom_determination

-- DROP TABLE public.dim_lookup_case_accom_determination;

CREATE TABLE public.dim_lookup_case_accom_determination
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_accom_determination
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_accom_determination TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_accom_determination TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_accom_determination TO postgres;

-- Index: dim_lookup_case_accom_determination_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_accom_determination_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_accom_determination_code
    ON public.dim_lookup_case_accom_determination USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_accom_status

-- DROP TABLE public.dim_lookup_case_accom_status;

CREATE TABLE public.dim_lookup_case_accom_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_accom_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_accom_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_accom_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_accom_status TO postgres;

-- Index: dim_lookup_case_accom_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_accom_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_accom_status_code
    ON public.dim_lookup_case_accom_status USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_cancel_reason

-- DROP TABLE public.dim_lookup_case_cancel_reason;

CREATE TABLE public.dim_lookup_case_cancel_reason
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_cancel_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_cancel_reason TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_cancel_reason TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_cancel_reason TO postgres;

-- Index: dim_lookup_case_cancel_reason_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_cancel_reason_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_cancel_reason_code
    ON public.dim_lookup_case_cancel_reason USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_closure_reason

-- DROP TABLE public.dim_lookup_case_closure_reason;

CREATE TABLE public.dim_lookup_case_closure_reason
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_closure_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_closure_reason TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_closure_reason TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_closure_reason TO postgres;

-- Index: dim_lookup_case_closure_reason_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_closure_reason_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_closure_reason_code
    ON public.dim_lookup_case_closure_reason USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_determination

-- DROP TABLE public.dim_lookup_case_determination;

CREATE TABLE public.dim_lookup_case_determination
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_determination
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_determination TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_determination TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_determination TO postgres;

-- Index: dim_lookup_case_determination_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_determination_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_determination_code
    ON public.dim_lookup_case_determination USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_duration_type

-- DROP TABLE public.dim_lookup_case_duration_type;

CREATE TABLE public.dim_lookup_case_duration_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_duration_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_duration_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_duration_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_duration_type TO postgres;

-- Index: dim_lookup_case_duration_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_duration_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_duration_type_code
    ON public.dim_lookup_case_duration_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_frequency_type

-- DROP TABLE public.dim_lookup_case_frequency_type;

CREATE TABLE public.dim_lookup_case_frequency_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_frequency_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_frequency_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_frequency_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_frequency_type TO postgres;

-- Index: dim_lookup_case_frequency_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_frequency_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_frequency_type_code
    ON public.dim_lookup_case_frequency_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_frequency_unit_type

-- DROP TABLE public.dim_lookup_case_frequency_unit_type;

CREATE TABLE public.dim_lookup_case_frequency_unit_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_frequency_unit_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_frequency_unit_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_frequency_unit_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_frequency_unit_type TO postgres;

-- Index: dim_lookup_case_frequency_unit_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_frequency_unit_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_frequency_unit_type_code
    ON public.dim_lookup_case_frequency_unit_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_related_type

-- DROP TABLE public.dim_lookup_case_related_type;

CREATE TABLE public.dim_lookup_case_related_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_related_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_related_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_related_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_related_type TO postgres;

-- Index: dim_lookup_case_related_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_related_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_related_type_code
    ON public.dim_lookup_case_related_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_status

-- DROP TABLE public.dim_lookup_case_status;

CREATE TABLE public.dim_lookup_case_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_status TO postgres;

-- Index: dim_lookup_case_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_status_code
    ON public.dim_lookup_case_status USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_case_types

-- DROP TABLE public.dim_lookup_case_types;

CREATE TABLE public.dim_lookup_case_types
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_types TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_types TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_types TO postgres;

-- Index: dim_lookup_case_types_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_types_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_types_code
    ON public.dim_lookup_case_types USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_empl_gender

-- DROP TABLE public.dim_lookup_empl_gender;

CREATE TABLE public.dim_lookup_empl_gender
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_gender
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_gender TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_gender TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_gender TO postgres;

-- Index: dim_lookup_empl_gender_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_gender_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_gender_code
    ON public.dim_lookup_empl_gender USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_empl_military_status

-- DROP TABLE public.dim_lookup_empl_military_status;

CREATE TABLE public.dim_lookup_empl_military_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_military_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_military_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_military_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_military_status TO postgres;

-- Index: dim_lookup_empl_military_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_military_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_military_status_code
    ON public.dim_lookup_empl_military_status USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_empl_pay_type

-- DROP TABLE public.dim_lookup_empl_pay_type;

CREATE TABLE public.dim_lookup_empl_pay_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_pay_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_pay_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_pay_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_pay_type TO postgres;

-- Index: dim_lookup_empl_pay_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_pay_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_pay_type_code
    ON public.dim_lookup_empl_pay_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_empl_status

-- DROP TABLE public.dim_lookup_empl_status;

CREATE TABLE public.dim_lookup_empl_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_status TO postgres;

-- Index: dim_lookup_empl_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_status_code
    ON public.dim_lookup_empl_status USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_empl_work_type

-- DROP TABLE public.dim_lookup_empl_work_type;

CREATE TABLE public.dim_lookup_empl_work_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_work_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_work_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_work_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_work_type TO postgres;

-- Index: dim_lookup_empl_work_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_work_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_work_type_code
    ON public.dim_lookup_empl_work_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_todoitem_priority

-- DROP TABLE public.dim_lookup_todoitem_priority;

CREATE TABLE public.dim_lookup_todoitem_priority
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_todoitem_priority
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_todoitem_priority TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_todoitem_priority TO etldw;
GRANT ALL ON TABLE public.dim_lookup_todoitem_priority TO postgres;

-- Index: dim_lookup_todoitem_priority_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_todoitem_priority_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_todoitem_priority_code
    ON public.dim_lookup_todoitem_priority USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_todoitem_types

-- DROP TABLE public.dim_lookup_todoitem_types;

CREATE TABLE public.dim_lookup_todoitem_types
(
    _id bigint NOT NULL,
    code bigint,
    name text,
    category text,
    feature text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_todoitem_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_todoitem_types TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_todoitem_types TO etldw;
GRANT ALL ON TABLE public.dim_lookup_todoitem_types TO postgres;

-- Index: dim_lookup_todoitem_types_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_todoitem_types_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_todoitem_types_code
    ON public.dim_lookup_todoitem_types USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_user_status

-- DROP TABLE public.dim_lookup_user_status;

CREATE TABLE public.dim_lookup_user_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_user_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_user_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_user_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_user_status TO postgres;

-- Index: dim_lookup_user_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_user_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_user_status_code
    ON public.dim_lookup_user_status USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_lookup_user_type

-- DROP TABLE public.dim_lookup_user_type;

CREATE TABLE public.dim_lookup_user_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_user_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_user_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_user_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_user_type TO postgres;

-- Index: dim_lookup_user_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_user_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_user_type_code
    ON public.dim_lookup_user_type USING btree
    (code)
    TABLESPACE pg_default;
-- Table: public.dim_organization

-- DROP TABLE public.dim_organization;

CREATE TABLE public.dim_organization
(
    dim_organization_id bigint NOT NULL DEFAULT nextval('dim_organization_dim_organization_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    code text,
    name text,
	siccode text,
    naicscode text,
	address1 text,
	address2 text,
	city text,
	postalcode text,
	state text,
	country text,
    description text,
    path text,
    type_code text,
    type_name text,
    org_created_by_email text,
    org_created_by_first_name text,
    org_created_by_last_name text,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_organization_pkey PRIMARY KEY (dim_organization_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_organization
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_organization TO absence_tracker;
GRANT ALL ON TABLE public.dim_organization TO etldw;
GRANT ALL ON TABLE public.dim_organization TO postgres;

-- Index: dim_organization_path_ref

-- DROP INDEX public.dim_organization_path_ref;

CREATE INDEX IF NOT EXISTS dim_organization_path_ref
    ON public.dim_organization USING btree
    (customer_id, employer_id, path, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Index: dim_organization_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_organization_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_organization_cur_ver_ref_id_is_del
    ON public.dim_organization USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_organization_annual_info

-- DROP TABLE public.dim_organization_annual_info;

CREATE TABLE public.dim_organization_annual_info
(
    dim_organization_annual_info_id bigint NOT NULL DEFAULT nextval('dim_organization_annual_info_dim_organization_annual_info_id_seq'::regclass),
	ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    org_code text,
    org_year int,
	average_employee_count int,
	total_hours_worked decimal,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_organization_annual_info_pkey PRIMARY KEY (dim_organization_annual_info_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_organization_annual_info
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_organization_annual_info TO absence_tracker;
GRANT ALL ON TABLE public.dim_organization_annual_info TO etldw;
GRANT ALL ON TABLE public.dim_organization_annual_info TO postgres;

CREATE TABLE IF NOT EXISTS public.dim_relapse
(
    dim_relapse_id bigint NOT NULL DEFAULT nextval('dim_relapse_dim_relapse_id_seq'::regclass),
    ext_ref_id character varying(36) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24) COLLATE pg_catalog."default",
    ext_modified_by character varying(24) COLLATE pg_catalog."default",
    employee_id character varying(24) COLLATE pg_catalog."default",
    employer_id character varying(24) COLLATE pg_catalog."default",
    customer_id character varying(24) COLLATE pg_catalog."default",
    case_id character varying(24) COLLATE pg_catalog."default",
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    prior_rtw_date timestamp with time zone,
    prior_rtw_days bigint,
    is_deleted boolean,
    CONSTRAINT dim_relapse_pkey PRIMARY KEY (dim_relapse_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_relapse
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_relapse TO absence_tracker;

GRANT ALL ON TABLE public.dim_relapse TO etldw;

GRANT ALL ON TABLE public.dim_relapse TO postgres;
-- Table: public.dim_role

-- DROP TABLE public.dim_role;

CREATE TABLE IF NOT EXISTS public.dim_role
(
    dim_role_id bigint NOT NULL DEFAULT nextval('dim_role_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    name text,
    customer_id character varying(24),
    permissions text[],
    role_type integer,
	is_deleted boolean,
    CONSTRAINT dim_role_pkey PRIMARY KEY (dim_role_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_role
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_role TO absence_tracker;

GRANT ALL ON TABLE public.dim_role TO etldw;

GRANT ALL ON TABLE public.dim_role TO postgres;

-- Index: dim_role_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_role_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_role_cur_ver_ref_id_is_del
    ON public.dim_role USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_team

-- DROP TABLE public.dim_team;

CREATE TABLE public.dim_team
(
    dim_team_id bigint NOT NULL DEFAULT nextval('dim_team_id_seq'::regclass),
    ext_ref_id character varying(24) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    name text,
    description text,
    is_deleted boolean,
    CONSTRAINT dim_team_pkey PRIMARY KEY (dim_team_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team TO absence_tracker;

GRANT ALL ON TABLE public.dim_team TO etldw;

GRANT ALL ON TABLE public.dim_team TO postgres;

-- Index: dim_team_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_cur_ver_ref_id_is_del;

CREATE INDEX dim_team_cur_ver_ref_id_is_del
    ON public.dim_team USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
CREATE TABLE public.dim_team_member
(
    dim_team_member_id bigint NOT NULL DEFAULT nextval('dim_team_member_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    user_id character varying(24),
    team_id character varying(24),
    is_team_lead boolean,
    assigned_by_id character varying(24),
    assigned_on timestamp with time zone,
    modified_by_id character varying(24),
    modified_on timestamp with time zone,
    is_deleted boolean,
    CONSTRAINT dim_team_member_pkey PRIMARY KEY (dim_team_member_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team_member
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team_member TO absence_tracker;

GRANT ALL ON TABLE public.dim_team_member TO etldw;

GRANT ALL ON TABLE public.dim_team_member TO postgres;

-- Index: dim_team_member_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_member_cur_ver_ref_id_is_del;

CREATE INDEX dim_team_member_cur_ver_ref_id_is_del
    ON public.dim_team_member USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_todoitem

-- DROP TABLE public.dim_todoitem;

CREATE TABLE public.dim_todoitem
(
    dim_todoitem_id bigint NOT NULL DEFAULT nextval('dim_todoitem_dim_todoitem_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    accommodation_id character varying(24),
    active_accommodation_id character varying(36),
    case_number text,
    case_id character varying(24),
    item_type integer,
    status integer,
    assigned_to_id character varying(24),
    assigned_to_name text,
    employee_id character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    due_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
    priority integer,
    title text,
    assigned_to_email text,
    assigned_to_first_name text,
    assigned_to_last_name text,
    due_date_change_reason text,
    weight double precision,
    result_text text,
    optional boolean,
    hide_until timestamp with time zone,
    template text,
    work_flow_name text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_todoitem_pkey PRIMARY KEY (dim_todoitem_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_todoitem
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_todoitem TO absence_tracker;
GRANT ALL ON TABLE public.dim_todoitem TO etldw;
GRANT ALL ON TABLE public.dim_todoitem TO postgres;

-- Index: dim_todoitem_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_todoitem_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_todoitem_cur_ver_ref_id_is_del
    ON public.dim_todoitem USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_users

-- DROP TABLE public.dim_users;

CREATE TABLE public.dim_users
(
    dim_user_id bigint NOT NULL DEFAULT nextval('dim_users_dim_user_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    customer_id character varying(24),
    first_name text,
    last_name text,
    email text,
    is_deleted boolean,
    created_date timestamp with time zone DEFAULT utc(),
    failed_login_attempts bigint,
    last_failed_login_attempt timestamp with time zone,
    locked boolean,
    locked_date timestamp with time zone,
    disabled boolean,
    disabled_date timestamp with time zone,
    last_activity_date timestamp with time zone,
    user_flags bigint,
    is_self_service_user boolean,
    is_portal_user boolean,
    user_status bigint,
    user_type bigint,
    role_name text,
    user_employee_id character varying(24),
    user_employee_number text,
	must_change_password boolean,
	display_name text,
	employer_ids character varying(24)[],
	role_ids character varying(24)[],
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_users_pkey PRIMARY KEY (dim_user_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_users
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_users TO absence_tracker;
GRANT ALL ON TABLE public.dim_users TO etldw;
GRANT ALL ON TABLE public.dim_users TO postgres;

-- Index: dim_users_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_users_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_users_cur_ver_ref_id_is_del
    ON public.dim_users USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_variable_schedule_time

-- Create Table dim_variable_schedule_time
	CREATE TABLE IF NOT EXISTS public.dim_variable_schedule_time
	(
		dim_variable_schedule_time_id bigint NOT NULL DEFAULT nextval('dim_variable_schedule_time_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		time_sample_date timestamp with time zone ,
		time_total_minutes int,
		employee_number text,	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_variable_schedule_time_pkey PRIMARY KEY (dim_variable_schedule_time_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_variable_schedule_time
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_variable_schedule_time TO absence_tracker;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO etldw;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO postgres;

-- Index: dim_variable_schedule_time_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_variable_schedule_time_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_variable_schedule_time_cur_ver_ref_id_is_del
    ON public.dim_variable_schedule_time USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
-- Table: public.dim_workflow_instance

-- Create Table dim_workflow_instance
	CREATE TABLE IF NOT EXISTS public.dim_workflow_instance
	(
		dim_workflow_instance_id bigint NOT NULL DEFAULT nextval('dim_workflow_instance_id_seq'::regclass),
		ext_ref_id character varying(36) ,
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24),
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accommodation_end_date	timestamp with time zone ,
		accommodation_id	character varying(36) ,
		accommodation_type_code	text ,
		accommodation_type_id	text ,
		active_accommodation_id	character varying(36) ,
		applied_policy_id	character varying(36) ,
		attachment_id	text ,		
		close_case	Boolean ,
		communication_id	text ,
		communication_type	int ,
		condition_start_date	timestamp with time zone ,
		contact_employee_due_date	timestamp with time zone ,
		contact_hcp_due_date	timestamp with time zone ,		
		denial_explanation	text ,
		denial_reason	int ,
		description	text ,
		determination	int ,
		due_date	timestamp with time zone ,
		ergonomic_assessment_date	timestamp with time zone ,
		event_id	varchar(24) ,
		event_type	int ,
		extend_grace_period	Boolean ,
		first_exhaustion_date	timestamp with time zone ,
		general_health_condition	text ,
		has_work_restrictions	Boolean ,
		illness_or_injury_date	timestamp with time zone ,
		new_due_date	timestamp with time zone ,
		paperwork_attachment_id	text ,
		paperwork_due_date	timestamp with time zone ,
		paperwork_id	character varying(36) ,
		paperwork_name	text ,
		paperwork_received	Boolean ,
		policy_code	text ,
		policy_id	text ,
		reason	int ,
		requires_review	Boolean ,
		return_attachment_id	text ,
		return_to_work_date	timestamp with time zone ,
		rtw	Boolean ,
		source_workflow_activity_activity_id	text ,
		source_workflow_activity_id	character varying(36) ,
		source_workflow_instance_id	text ,
		status	int ,
		template	text ,
		workflow_activity_id	character varying(36) ,
		workflow_id	varchar(24) ,
		workflow_name	text ,

		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_workflow_instance_pkey PRIMARY KEY (dim_workflow_instance_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_workflow_instance
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_workflow_instance TO absence_tracker;
	GRANT ALL ON TABLE public.dim_workflow_instance TO etldw;
	GRANT ALL ON TABLE public.dim_workflow_instance TO postgres;

-- Index: dim_workflow_instance_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_workflow_instance_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_workflow_instance_cur_ver_ref_id_is_del
    ON public.dim_workflow_instance USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
CREATE TABLE TBL_MV_FACT_ORG_BASEDATA
(
	dim_organization_id bigint ,
	organization_ref_id varchar(24),
	customer_ref_id varchar(24),
	employer_ref_id varchar(24),
	org_level integer,
	count_children bigint, 
	count_descendents bigint,
	count_employees bigint,
	count_cases bigint,
	count_open_cases bigint,
	count_closed_cases bigint,
	count_todos bigint,
	count_open_todos bigint,
	count_closed_todos bigint,
	count_overdue_todos bigint
);


-- View: public.mview_fact_case

-- REFRESH MATERIALIZED VIEW public.mview_fact_case
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_case;

CREATE MATERIALIZED VIEW public.mview_fact_case
TABLESPACE pg_default
AS
 WITH relapse AS (
         SELECT dim_relapse.case_id,
            count(dim_relapse.ext_ref_id) AS relapses_count           
           FROM dim_relapse
          WHERE dim_relapse.ver_is_current AND NOT dim_relapse.is_deleted
          GROUP BY dim_relapse.case_id
        ), todos AS (
         SELECT dim_todoitem.case_id,
            count(dim_todoitem.ext_ref_id) AS total,
            sum(
                CASE
                    WHEN dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4]) THEN 1
                    ELSE 0
                END) AS open_todos,
            sum(
                CASE
                    WHEN dim_todoitem.status = 1 THEN 1
                    ELSE 0
                END) AS closed_todos,
            sum(
                CASE
                    WHEN (dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4])) AND dim_todoitem.due_date < utc() THEN 1
                    ELSE 0
                END) AS overdue_todos
           FROM dim_todoitem
          WHERE dim_todoitem.ver_is_current AND NOT dim_todoitem.is_deleted
          GROUP BY dim_todoitem.case_id
        )
 SELECT dim_cases.dim_case_id,
    dim_cases.ext_ref_id AS case_ref_id,
    COALESCE(todos.total, 0::bigint) AS count_todos,
    COALESCE(todos.open_todos, 0::bigint) AS count_open_todos,
    COALESCE(todos.closed_todos, 0::bigint) AS count_closed_todos,
    COALESCE(todos.overdue_todos, 0::bigint) AS count_overdue_todos,
	 COALESCE(relapse.relapses_count, 0::bigint) AS count_relapses
   FROM dim_cases
     LEFT JOIN todos ON dim_cases.ext_ref_id::text = todos.case_id::text
	 LEFT JOIN relapse ON dim_cases.ext_ref_id::text = relapse.case_id::text
  WHERE dim_cases.ver_is_current AND NOT dim_cases.is_deleted
WITH DATA;

ALTER TABLE public.mview_fact_case
    OWNER TO postgres;

GRANT SELECT ON TABLE public.mview_fact_case TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_case TO etldw;
GRANT ALL ON TABLE public.mview_fact_case TO postgres;

-- Index: mview_fact_case_case_ref_id

-- DROP INDEX public.mview_fact_case_case_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_case_case_ref_id
    ON public.mview_fact_case USING btree
    (case_ref_id)
    TABLESPACE pg_default;
-- View: public.mview_fact_customer

-- REFRESH MATERIALIZED VIEW public.mview_fact_customer
DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_customer;

CREATE MATERIALIZED VIEW public.mview_fact_customer
TABLESPACE pg_default
AS
WITH employers AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_employer
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), employees AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_employee
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), cases AS (
	SELECT	customer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), todos AS (
	SELECT	customer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), users AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_users
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
)
SELECT	dim_customer.dim_customer_id,
		dim_customer.ext_ref_id AS customer_ref_id,
		coalesce(employers.total, 0) AS count_employers,
		coalesce(employees.total, 0) AS count_employees,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos,
        coalesce(users.total, 0) as count_users
FROM	dim_customer
		LEFT JOIN employers ON dim_customer.ext_ref_id = employers.customer_id
		LEFT JOIN employees ON dim_customer.ext_ref_id = employees.customer_id
		LEFT JOIN cases ON dim_customer.ext_ref_id = cases.customer_id
		LEFT JOIN todos ON dim_customer.ext_ref_id = todos.customer_id
		LEFT JOIN users ON dim_customer.ext_ref_id = users.customer_id
WHERE	dim_customer.ver_is_current
		AND NOT dim_customer.is_deleted;

ALTER TABLE public.mview_fact_customer
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_customer TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_customer TO etldw;
GRANT ALL ON TABLE public.mview_fact_customer TO postgres;

-- Index: mview_fact_customer_customer_ref_id

-- DROP INDEX public.mview_fact_customer_customer_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_customer_customer_ref_id
    ON public.mview_fact_customer USING btree
    (customer_ref_id)
    TABLESPACE pg_default;
-- View: public.mview_fact_employee

-- REFRESH MATERIALIZED VIEW public.mview_fact_employee;
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_employee;

CREATE MATERIALIZED VIEW public.mview_fact_employee
TABLESPACE pg_default
AS
WITH cases AS (
	SELECT	employee_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employee_id
), todos AS (
	SELECT	employee_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employee_id
)
SELECT	dim_employee.dim_employee_id,
		dim_employee.ext_ref_id AS employee_ref_id,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos
FROM	dim_employee
		LEFT JOIN cases ON dim_employee.ext_ref_id = cases.employee_id
		LEFT JOIN todos ON dim_employee.ext_ref_id = todos.employee_id
WHERE	dim_employee.ver_is_current
		AND NOT dim_employee.is_deleted;


ALTER TABLE public.mview_fact_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_employee TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_employee TO etldw;
GRANT ALL ON TABLE public.mview_fact_employee TO postgres;

-- Index: mview_fact_employee_employee_ref_id

-- DROP INDEX public.mview_fact_employee_employee_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_employee_employee_ref_id
    ON public.mview_fact_employee USING btree
    (employee_ref_id)
    TABLESPACE pg_default;
-- View: public.mview_fact_employer

-- REFRESH MATERIALIZED VIEW public.mview_fact_employer
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_employer;

CREATE MATERIALIZED VIEW public.mview_fact_employer
TABLESPACE pg_default
AS
WITH employees AS (
	SELECT	employer_id,
			count(ext_ref_id) as total
	FROM	dim_employee
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), cases AS (
	SELECT	employer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), todos AS (
	SELECT	employer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), users AS (
	SELECT	employers.employer_id,
			count(dim_users.ext_ref_id) as total
	FROM	dim_users
			JOIN LATERAL unnest(dim_users.employer_ids) as employers(employer_id) ON true
    WHERE	dim_users.ver_is_current
			AND NOT dim_users.is_deleted
	GROUP	BY employers.employer_id
)
SELECT	dim_employer.dim_employer_id,
		dim_employer.ext_ref_id AS employer_ref_id,
		coalesce(employees.total, 0) AS count_employees,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos,
        coalesce(users.total, 0) as count_users
FROM	dim_employer
		LEFT JOIN employees ON dim_employer.ext_ref_id = employees.employer_id
		LEFT JOIN cases ON dim_employer.ext_ref_id = cases.employer_id
		LEFT JOIN todos ON dim_employer.ext_ref_id = todos.employer_id
		LEFT JOIN users ON dim_employer.ext_ref_id = users.employer_id
WHERE	dim_employer.ver_is_current
		AND NOT dim_employer.is_deleted;

ALTER TABLE public.mview_fact_employer
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_employer TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_employer TO etldw;
GRANT ALL ON TABLE public.mview_fact_employer TO postgres;

-- Index: mview_fact_employer_employer_ref_id

-- DROP INDEX public.mview_fact_employer_employer_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_employer_employer_ref_id
    ON public.mview_fact_employer USING btree
    (employer_ref_id)
    TABLESPACE pg_default;
-- View: public.mview_fact_organization

-- DROP MATERIALIZED VIEW public.mview_fact_organization;

CREATE MATERIALIZED VIEW public.mview_fact_organization
TABLESPACE pg_default
AS

SELECT dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,org_level,count_children,
count_descendents,count_employees,count_cases,count_open_cases,count_closed_cases,count_todos,count_open_todos,
count_closed_todos,count_overdue_todos
FROM TBL_MV_FACT_ORG_BASEDATA
WITH DATA;
 
-- View: public.mview_fact_user

-- REFRESH MATERIALIZED VIEW public.mview_fact_user
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_user;

CREATE MATERIALIZED VIEW public.mview_fact_user
TABLESPACE pg_default
AS
WITH cases AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
), todos AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
)
SELECT	dim_users.dim_user_id,
		dim_users.ext_ref_id AS user_ref_id,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos
FROM	dim_users
		LEFT JOIN cases ON dim_users.ext_ref_id = cases.assigned_to_id
      	LEFT JOIN todos ON dim_users.ext_ref_id = todos.assigned_to_id
WHERE	dim_users.ver_is_current
		AND NOT dim_users.is_deleted;

ALTER TABLE public.mview_fact_user
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_user TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_user TO etldw;
GRANT ALL ON TABLE public.mview_fact_user TO postgres;

-- Index: mview_fact_user_user_ref_id

-- DROP INDEX public.mview_fact_user_user_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_user_user_ref_id
    ON public.mview_fact_user USING btree
    (user_ref_id)
    TABLESPACE pg_default;
-- View: public.mview_hr

-- REFRESH MATERIALIZED VIEW public.mview_hr
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_hr;

CREATE MATERIALIZED VIEW public.mview_hr
TABLESPACE pg_default
AS
SELECT	DISTINCT ON (employee_id)
		employee_id,
		trim(both ' ' from (coalesce(first_name, '') || ' ' || coalesce(last_name, company_name, ''))) AS name,
		employee_number,
		email,
		work_phone AS phone
FROM	dim_employee_contact
WHERE	contact_type_code = 'HR'
		AND ver_is_current = true
		AND is_deleted = false
ORDER	BY employee_id ASC, dim_employee_contact_id DESC;

ALTER TABLE public.mview_hr
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_hr TO absence_tracker;
GRANT ALL ON TABLE public.mview_hr TO etldw;
GRANT ALL ON TABLE public.mview_hr TO postgres;

-- Index: mview_hr_employee_id

-- DROP INDEX public.mview_hr_employee_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_hr_employee_id
    ON public.mview_hr USING btree
    (employee_id)
    TABLESPACE pg_default;
-- View: public.mview_search_data

-- REFRESH MATERIALIZED VIEW public.mview_search_data
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_search_data;
CREATE MATERIALIZED VIEW IF NOT EXISTS mview_search_data AS
  select distinct 
    concat_ws(' ', a.first_name, a.last_name, b.case_number,  a.employee_number, a.email)              as searchtext,
    a.customer_id :: text                                                                                           as customer_id,
    a.employer_id :: text                                                                                           as employer_id,
    a.ext_ref_id :: text                                                                                            as EmployeeId,
    c.name :: text                                                                                                  as Employer,
    b.case_number :: text                                                                                           as CaseNumber,
    a.dob :: date                                                                                                   as DOB,
    a.first_name :: text                                                                                            as FirstName,
    a.last_name :: text                                                                                             as LastName,
    a.employee_number :: text                                                                                       as EmployeeNumber,
    (case
     when b.ext_ref_id is not null
       then
         concat_ws(' | ', 'Case: ' || b.case_number, b.case_reason, d.description,
                   (to_char(b.start_date, 'MM/DD/YYYY') || ' - ' || to_char(b.end_date, 'MM/DD/YYYY')), c.name)
     when a.ext_ref_id is not null
       then concat_ws(' ', a.first_name, a.middle_name, a.last_name) || concat_ws('', '(', '#', a.employee_number, ')')
            || concat_ws('', ' | ', a.employer_name)
     end) :: text                                                                                                   as title,
    (concat_ws(' | ', concat_ws(' ', 'Employee:', a.first_name, a.middle_name, a.last_name) ||
                      concat_ws('', ' (', '#', a.employee_number, ')'),
               -- gender description values not available in database hence coding them in materialized view
               case when upper(e.description) = 'M'
                 then 'Male'
               when upper(e.description) = 'F'
                 then 'Female' end,
               case when a.dob is not null
                 then concat_ws(' ', 'DOB:', to_char(a.dob, 'MM/DD/YYYY')) end,
               case when a.hire_date is not null
                 then concat_ws(' ', 'Hire Date:', to_char(a.hire_date, 'MM/DD/YYYY')) end,
               case when a.service_date is not null
                 then concat_ws(' ', 'Service Date:', to_char(a.service_date, 'MM/DD/YYYY')) end,
               a.email,
               case when a.office_location is not null
                 then concat_ws(' ', 'Location:',
                                a.office_location) end)) :: text                                                    as Description,
    b.ext_ref_id :: text                                                                                            as caseid,
    to_tsvector(concat_ws(' ', a.first_name, a.last_name, b.case_number,  a.employee_number,
                          a.email))                                                                                 as searchvector
  from dim_employee a inner join dim_cases b
      on a.ext_ref_id :: text = b.employee_id :: text
         and a.employer_id :: text = b.employer_id :: text
         and a.customer_id :: text = b.customer_id :: text
         and a.ver_is_current
         and b.ver_is_current
    inner join dim_employer c on a.employer_id = c.ext_ref_id
                                 and a.customer_id = c.customer_id
                                 and c.ver_is_current
    left join dim_lookup_case_status d on b.status = d.code
    left join dim_lookup_empl_gender e on a.gender = e.code


  union all

  select distinct
    concat_ws(' ', a.first_name, a.last_name, a.employee_number, a.email)              as searchtext,
    a.customer_id :: text                                                              as customer_id,
    a.employer_id :: text                                                              as employer_id,
    a.ext_ref_id :: text                                                               as EmployeeId,
    c.name :: text                                                                     as Employer,
    null                                                                               as CaseNumber,
    a.dob :: date                                                                      as DOB,
    a.first_name :: text                                                               as FirstName,
    a.last_name :: text                                                                as LastName,
    a.employee_number :: text                                                          as EmployeeNumber,
    (case
     when a.ext_ref_id is not null
       then concat_ws(' ', a.first_name, a.middle_name, a.last_name) || concat_ws('', ' (', '#', a.employee_number, ')')
            || concat_ws('', ' | ', a.employer_name)
     end) :: text                                                                      as title,
    (concat_ws(' | ', concat_ws(' ', 'Employee:', a.first_name, a.middle_name, a.last_name) ||
                      concat_ws('', ' (', '#', a.employee_number, ')'),
               -- gender description values not available in database hence coding them in materialized view
               case when upper(e.description) = 'M'
                 then 'Male'
               when upper(e.description) = 'F'
                 then 'Female' end,
               case when a.dob is not null
                 then concat_ws(' ', 'DOB:', to_char(a.dob, 'MM/DD/YYYY')) end,
               case when a.hire_date is not null
                 then concat_ws(' ', 'Hire Date:', to_char(a.hire_date, 'MM/DD/YYYY')) end,
               case when a.service_date is not null
                 then concat_ws(' ', 'Service Date:', to_char(a.service_date, 'MM/DD/YYYY')) end,
               a.email,
               case when a.office_location is not null
                 then concat_ws(' ', 'Location:', a.office_location) end)) :: text     as Description,
    null                                                                               as caseid,
    to_tsvector(concat_ws(' ', a.first_name, a.last_name, a.employee_number, a.email)) as searchvector
  from dim_employee a
    inner join dim_employer c on a.employer_id = c.ext_ref_id and a.customer_id = c.customer_id
                                 and c.ver_is_current
    left join dim_lookup_empl_gender e on a.gender = e.code
  where a.ver_is_current;



ALTER TABLE public.mview_search_data
  OWNER to postgres;

GRANT SELECT ON TABLE public.mview_search_data TO absence_tracker;
GRANT ALL ON TABLE public.mview_search_data TO etldw;
GRANT ALL ON TABLE public.mview_search_data TO postgres;

CREATE INDEX IF NOT EXISTS ix_mview_search_data
  ON mview_search_data
  USING GIN (searchvector);

create unique index IF NOT EXISTS ux_mview_search_data
  on mview_search_data (caseid,EmployeeId);
-- View: public.mview_supervisor

-- REFRESH MATERIALIZED VIEW public.mview_supervisor
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_supervisor;

CREATE MATERIALIZED VIEW public.mview_supervisor
TABLESPACE pg_default
AS
SELECT	DISTINCT ON (employee_id)
		employee_id,
		trim(both ' ' from (coalesce(first_name, '') || ' ' || coalesce(last_name, company_name, ''))) AS name,
		employee_number,
		email,
		work_phone AS phone
FROM	dim_employee_contact
WHERE	contact_type_code = 'SUPERVISOR'
		AND ver_is_current = true
		AND is_deleted = false
ORDER	BY employee_id ASC, dim_employee_contact_id DESC;

ALTER TABLE public.mview_supervisor
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_supervisor TO absence_tracker;
GRANT ALL ON TABLE public.mview_supervisor TO etldw;
GRANT ALL ON TABLE public.mview_supervisor TO postgres;

-- Index: mview_supervisor_employee_id

-- DROP INDEX public.mview_supervisor_employee_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_supervisor_employee_id
    ON public.mview_supervisor USING btree
    (employee_id)
    TABLESPACE pg_default;
/* Insert Enum Values to dim_lookup_category */
	INSERT INTO public.dim_lookup_category SELECT 'eligibility_status', 'Eligibility status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'eligibility_status');
	INSERT INTO public.dim_lookup_category SELECT 'case_denial_reason', 'Case denial reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_denial_reason');
	INSERT INTO public.dim_lookup_category SELECT 'case_intermittent_status', 'Case intermittent status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_intermittent_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_classification', 'Employee job classification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_classification');
	INSERT INTO public.dim_lookup_category SELECT 'work_classification', 'Work classification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'work_classification');
	INSERT INTO public.dim_lookup_category SELECT 'type_of_injury', 'Type of injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'type_of_injury');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_incdnt_when', 'Sharp incident when' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_incdnt_when');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_job_class', 'Sharp job class' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_job_class');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_location_dept', 'Sharp location/department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_location_dept');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_procedure', 'Sharp procedure' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_procedure');
	INSERT INTO public.dim_lookup_category SELECT 'case_closure_reason', 'Case closure reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_closure_reason');
	INSERT INTO public.dim_lookup_category SELECT 'intermittent_type', 'Intermittent type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'intermittent_type');
	INSERT INTO public.dim_lookup_category SELECT 'recipient_type', 'Recipient type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'recipient_type');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_review_status', 'Paperwork review status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_review_status');
	INSERT INTO public.dim_lookup_category SELECT 'document_type', 'Document type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'document_type');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_field_status', 'Paperwork field status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_field_status');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_field_type', 'Paperwork field type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_field_type');
	INSERT INTO public.dim_lookup_category SELECT 'case_event_type', 'Case event type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_event_type');
	INSERT INTO public.dim_lookup_category SELECT 'payroll_status', 'Payroll status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'payroll_status');
	INSERT INTO public.dim_lookup_category SELECT 'attachment_type', 'Attachment type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'attachment_type');
	INSERT INTO public.dim_lookup_category SELECT 'communication_type', 'Communication type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'communication_type');
	INSERT INTO public.dim_lookup_category SELECT 'email_replies_type', 'Email replies type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'email_replies_type');
	INSERT INTO public.dim_lookup_category SELECT 'note_category', 'Note category' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'note_category');
	INSERT INTO public.dim_lookup_category SELECT 'workflow_instance_status', 'Workflow instance status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'workflow_instance_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_adjudication_status', 'Employee job adjudication status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_adjudication_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_denial_reason', 'Employee job denial reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_denial_reason');
	INSERT INTO public.dim_lookup_category SELECT 'empl_necessity_type', 'Employee necessity type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_necessity_type');
	INSERT INTO public.dim_lookup_category SELECT 'role_type', 'Role type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'role_type');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (46, 1, 44, 'Upgrade-44');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (66, 1, 64, 'Upgrade-64');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (67, 1, 64, 'Upgrade-65');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (100, 1, 100, 'Upgrade-100');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (101, 1, 101, 'Upgrade-75');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (102, 1, 102, 'Upgrade-76');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (103, 1, 103, 'Upgrade-77');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (104, 1, 104, 'Upgrade-78');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (105, 1, 105, 'Upgrade-79');
INSERT INTO dbversion (dbversion_id, major_num, minur_num, notes) VALUES (106, 1, 106, 'Upgrade-80');
	/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);


	/* Case Denial Reason Lookup (type: case_denial_reason) */	
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);

	
	/* Case Intermittent Status. (type: case_intermittent_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


	/* Employee Job Classification (type: empl_job_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);

	/* Work Classification (type: work_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);

	
	/* Type of Injury (type: type_of_injury)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);

	
	/* Sharp Incident When (type: sharp_incdnt_when) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);

		
	/* Sharp Job Class (type: sharp_job_class) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


	/* Sharp Location/Department (type: sharp_location_dept) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


	/* Sharp Procedure (type: sharp_procedure) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);


	/* Case Closure Reason (type: case_closure_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);

	/* IntermittentType Lookup (type: intermittent_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 0, 'OfficeVisit' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 1, 'Incapacity' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=1);

	/* RecipientType Lookup(type: recipient_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 0, 'TO' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 1, 'CC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 2, 'BCC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=2);
	
	/* PaperworkReviewStatus Lookup(type: paperwork_review_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 0, 'NotApplicable' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 1, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 2, 'Received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 3, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 4, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 5, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 6, 'NotReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=6);

	/*DocumentType Lookup(type: document_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 0, 'None' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 1, 'Template' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 2, 'Html' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 3, 'MSWordDocument' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 4, 'Pdf' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=4);

	/*PaperworkFieldStatus Lookup(type: paperwork_field_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 0, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 1, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=1);

	/*PaperworkFieldType Lookup(type: paperwork_field_type)*/	
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 0, 'Text' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 4, 'WorkRestrictions' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=4);

	/* Employee Job Adjudication Status (type: empl_job_adjudication_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 1, 'Approved' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=2);

	/* Employee Job Denial Reason (type: empl_job_denial_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 1, 'RestrictionsConflictWithTasks' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 2, 'BodyMechanics' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 3, 'NoWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 4, 'NotTrained' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=4);

	/* Employee Job Adjudication Status (type: empl_job_adjudication_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_necessity_type', 0, 'MedicalDevice' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_necessity_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_necessity_type', 1, 'Medication' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_necessity_type' AND code=1);

	/* EventType Lookup(type: event_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 0, 'CaseCreated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 1, 'ReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=1); 
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 2, 'DeliveryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 3, 'BondingStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 4, 'BondingEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 5, 'IllnessOrInjuryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 6, 'HospitalAdmissionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 7, 'HospitalReleaseDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 8, 'ReleaseReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 9, 'PaperworkReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 10, 'AccommodationAdded' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 11, 'CaseClosed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 12, 'CaseCancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 13, 'AdoptionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 14, 'EstimatedReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 15, 'EmployerHolidayScheduleChanged' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 16, 'CaseStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 17, 'CaseEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=17);

	/* PayrollStatus Lookup(type: payroll_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 1, 'SentToException' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 2, 'SentToPayroll' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=2);

	/* AttachmentType Lookup(type: attachment_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 0, 'Communication' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 1, 'Paperwork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 2, 'Documentation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=3);
	
	/*CommunicationType Lookup(type: communication_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 0, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 1, 'Email' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=1);

	/*EmailRepliesType Lookup(type: email_replies_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 1, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 2, 'CurrentUser' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=2);
	
	/*NoteCategory Lookup(type: note_category)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 0, 'CaseSummary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 1, 'EmployeeConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 2, 'ManagerConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 3, 'HRConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 4, 'Medical' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 5, 'TimeOffRequest' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 6, 'Certification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 7, 'InteractiveProcess' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 8, 'WorkRestriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 9, 'OperationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 10, 'HealthCareProviderConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 11, 'WorkerCompensationConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 12, 'SafetyConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 13, 'LegalConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 14, 'ERCConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 15, 'EmployeeRelationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 16, 'BenefitsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 17, 'LOAConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=17);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 18, 'FacilitiesConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=18);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 19, 'PayrollConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=19);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 20, 'STDLOAVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=20);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 21, 'LTDVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=21);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 22, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=22);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 23, 'Referral' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=23);

	/*WorkflowInstanceStatus Lookup(type: workflow_instance_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 1, 'Running' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 2, 'Paused' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 3, 'Completed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 4, 'Canceled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 5, 'Failed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=5);
	   

	/* Role Type (type: role_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 0, 'Portal' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 1, 'SelfService' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 2, 'Administration' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=2);
-- Data: public.dim_lookup_accom_cancel_reason

insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (2, 1, 'Accommodation No Longer Needed');
insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (3, 2, 'Leave No Longer Needed');
insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (4, 3, 'Request Withdrawn');
insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (5, 4, 'Entered For Wrong Employee');
insert into public.dim_lookup_accom_cancel_reason (_id, code, description) values (6, 5, 'Entered In Error');
-- Data: public.dim_lookup_acom_duration

insert into public.dim_lookup_acom_duration (_id, code, description) values (1, 1, 'Temporary');
insert into public.dim_lookup_acom_duration (_id, code, description) values (2, 2, 'Permanent');
-- Data: public.dim_lookup_case_accom_cancel_reason

insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (2, 1, 'Accommodation No Longer Needed');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (3, 2, 'Leave No Longer Needed');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (4, 3, 'Request Withdrawn');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (5, 4, 'Entered For Wrong Employee');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (6, 5, 'Entered In Error');
-- Data: public.dim_lookup_case_accom_determination

insert into public.dim_lookup_case_accom_determination (_id, code, description) values (1, 0, 'Pending');
insert into public.dim_lookup_case_accom_determination (_id, code, description) values (2, 1, 'Approved');
insert into public.dim_lookup_case_accom_determination (_id, code, description) values (3, 2, 'Denied');
-- Data: public.dim_lookup_case_accom_status

insert into public.dim_lookup_case_accom_status (_id, code, description) values (1, 0, 'Open');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (2, 1, 'Closed');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (3, 2, 'Cancelled');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (4, 3, 'Requested');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (5, 4, 'Inquiry');
-- Data: public.dim_lookup_case_cancel_reason

insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (2, 1, 'Leave Not Needed Or Cancelled');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (3, 2, 'Entered In Error');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (4, 3, 'Inquiry Closed');
-- Data: public.dim_lookup_case_closure_reason

insert into public.dim_lookup_case_closure_reason (_id, code, description) values (1, 0, 'ReturnToWork');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (2, 1, 'Terminated');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (3, 2, 'LeaveCancelled');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (4, 3, 'Other');
-- Data: public.dim_lookup_case_determination

insert into public.dim_lookup_case_determination (_id, code, description) values (1, 0, 'Pending');
insert into public.dim_lookup_case_determination (_id, code, description) values (2, 1, 'Approved');
insert into public.dim_lookup_case_determination (_id, code, description) values (3, 2, 'Denied');
insert into public.dim_lookup_case_determination (_id, code, description) values (4, 3, 'Approved');
insert into public.dim_lookup_case_determination (_id, code, description) values (5, 4, 'Not Eligible');
-- Data: public.dim_lookup_case_duration_type

insert into public.dim_lookup_case_duration_type (_id, code, description) values (1, 0, 'Minutes');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (2, 1, 'Hours');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (3, 2, 'Days');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (4, 3, 'Weeks');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (5, 4, 'Months');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (6, 5, 'Years');
-- Data: public.dim_lookup_case_frequency_type

insert into public.dim_lookup_case_frequency_type (_id, code, description) values (1, 0, 'Minutes');
insert into public.dim_lookup_case_frequency_type (_id, code, description) values (2, 1, 'Hours');
insert into public.dim_lookup_case_frequency_type (_id, code, description) values (3, 2, 'Days');
insert into public.dim_lookup_case_frequency_type (_id, code, description) values (4, 3, 'Weeks');
insert into public.dim_lookup_case_frequency_type (_id, code, description) values (5, 4, 'Months');
insert into public.dim_lookup_case_frequency_type (_id, code, description) values (6, 5, 'Years');
-- Data: public.dim_lookup_case_frequency_unit_type

insert into public.dim_lookup_case_frequency_unit_type (_id, code, description) values (1, 0, 'Workday');
insert into public.dim_lookup_case_frequency_unit_type (_id, code, description) values (2, 1, 'Calendar Day');
-- Data: public.dim_lookup_case_related_type

insert into public.dim_lookup_case_related_type (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_case_related_type (_id, code, description) values (2, 1, 'Recurring');
-- Data: public.dim_lookup_case_status

insert into public.dim_lookup_case_status (_id, code, description) values (1, 0, 'Open');
insert into public.dim_lookup_case_status (_id, code, description) values (2, 1, 'Closed');
insert into public.dim_lookup_case_status (_id, code, description) values (3, 2, 'Cancelled');
insert into public.dim_lookup_case_status (_id, code, description) values (3, 3, 'Requested');
insert into public.dim_lookup_case_status (_id, code, description) values (3, 4, 'Inquiry');
-- Data: public.dim_lookup_case_types

insert into public.dim_lookup_case_types (_id, code, description) values (1, 0, 'None');
insert into public.dim_lookup_case_types (_id, code, description) values (2, 1, 'Consecutive');
insert into public.dim_lookup_case_types (_id, code, description) values (3, 2, 'Intermittent');
insert into public.dim_lookup_case_types (_id, code, description) values (4, 4, 'Reduced');
insert into public.dim_lookup_case_types (_id, code, description) values (5, 8, 'Administrative');
-- Data: public.dim_lookup_empl_gender

insert into public.dim_lookup_empl_gender (_id, code, description) values (1, 77, 'M');
insert into public.dim_lookup_empl_gender (_id, code, description) values (2, 70, 'F');
-- Data: public.dim_lookup_empl_military_status

insert into public.dim_lookup_empl_military_status (_id, code, description) values (1, 0, 'Civilian');
insert into public.dim_lookup_empl_military_status (_id, code, description) values (2, 1, 'Active Duty');
insert into public.dim_lookup_empl_military_status (_id, code, description) values (3, 2, 'Veteran');
-- Data: public.dim_lookup_empl_pay_type

insert into public.dim_lookup_empl_pay_type (_id, code, description) values (1, 1, 'Salary');
insert into public.dim_lookup_empl_pay_type (_id, code, description) values (2, 2, 'Hourly');
-- Data: public.dim_lookup_empl_status

insert into public.dim_lookup_empl_status (_id, code, description) values (1, 65, 'A');
insert into public.dim_lookup_empl_status (_id, code, description) values (2, 76, 'L');
insert into public.dim_lookup_empl_status (_id, code, description) values (3, 84, 'T');
insert into public.dim_lookup_empl_status (_id, code, description) values (4, 73, 'I');
-- Data: public.dim_lookup_empl_work_type

insert into public.dim_lookup_empl_work_type (_id, code, description) values (1, 1, 'FT');
insert into public.dim_lookup_empl_work_type (_id, code, description) values (2, 2, 'PT');
insert into public.dim_lookup_empl_work_type (_id, code, description) values (3, 3, 'Per Diem');
-- Data: public.dim_lookup_todoitem_priority

insert into public.dim_lookup_todoitem_priority (_id, code, description) values (1, -1, 'Low');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (2, 0, 'Normal');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (3, 1, 'High');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (4, 2, 'Urgent');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (5, 3, 'Critical');
-- Data: public.dim_lookup_todoitem_types

insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (1, -1, 'Not Specified', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (2, 0, 'Communication', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (3, 1, 'PaperworkDue', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (4, 2, 'Cancelled', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (5, 3, 'CaseReview', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (6, 4, 'ReturnToWork', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (7, 5, 'Manual', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (8, 6, 'CloseCase', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (9, 7, 'VerifyReturnToWork', '', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (10, 8, 'ScheduleErgonomicAssessment', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (11, 9, 'CompleteErgonomicAssessment', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (12, 10, 'EquipmentSoftwareOrdered', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (13, 11, 'EquipmentSoftwareInstalled', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (14, 12, 'HRISScheduleChange', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (15, 13, 'OtherAccommodation', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (16, 25, 'NotifyManager', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (17, 26, 'InitiateNewLeaveCase', 'Accommodations', 'ADA');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (18, 14, 'STDCaseManagerContactsEE', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (19, 15, 'STDContactHCP', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (20, 16, 'STDDiagnosis', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (21, 17, 'STDDesignatePrimaryHCP', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (22, 18, 'STDGenHealthCond', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (23, 19, 'STDFollowUp', 'STD', 'ShortTermDisability');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (24, 20, 'ReviewCaseAttachment', 'ESS', 'EmployeeSelfService');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (25, 21, 'ReviewCaseNote', 'ESS', 'EmployeeSelfService');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (26, 22, 'ReviewEmployeeInformation', 'ESS', 'EmployeeSelfService');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (27, 23, 'ReviewCaseCreated', 'ESS', 'EmployeeSelfService');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (28, 24, 'ReviewWorkSchedule', 'ESS', 'EmployeeSelfService');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (29, 27, 'EnterWorkRestrictions', 'JSA', '');
insert into public.dim_lookup_todoitem_types (_id, code, name, category, feature) values (30, 28, 'DetermineRequest', 'JSA', '');
-- Data: public.dim_lookup_user_status

insert into public.dim_lookup_user_status (_id, code, description) values (1, 1, 'Active');
insert into public.dim_lookup_user_status (_id, code, description) values (2, 2, 'Disabled');
insert into public.dim_lookup_user_status (_id, code, description) values (3, 3, 'Locked');
-- Data: public.dim_lookup_user_type

insert into public.dim_lookup_user_type (_id, code, description) values (1, 1, 'Portal');
insert into public.dim_lookup_user_type (_id, code, description) values (2, 2, 'ESS');
-- View: public.view_cust

DROP VIEW IF EXISTS public.view_cust CASCADE;

CREATE OR REPLACE VIEW public.view_cust AS
 SELECT
	dim_customer.ext_ref_id as cust_id,
    dim_customer.cust_customer_name,
    dim_customer.cust_customer_url,
    dim_customer.cust_contact_first_name,
    dim_customer.cust_contact_last_name,
    dim_customer.cust_contact_email,
    dim_customer.cust_contact_address1,
    dim_customer.cust_contact_address2,
    dim_customer.cust_contact_city,
    dim_customer.cust_contact_state,
    dim_customer.cust_contact_postal_code,
    dim_customer.cust_contact_country,
    dim_customer.cust_billing_first_name,
    dim_customer.cust_billing_last_name,
    dim_customer.cust_billing_email,
    dim_customer.cust_billing_address1,
    dim_customer.cust_billing_address2,
    dim_customer.cust_billing_city,
    dim_customer.cust_billing_state,
    dim_customer.cust_billing_postal_code,
    dim_customer.cust_billing_country,
    dim_customer.ext_created_date as cust_customer_created_date,
    COALESCE(mview_fact_customer.count_employers, 0::bigint) as cust_customer_employer_count,
    COALESCE(mview_fact_customer.count_employees, 0::bigint) as cust_customer_employee_count,
    COALESCE(mview_fact_customer.count_cases, 0::bigint) AS cust_customer_case_count,
    COALESCE(mview_fact_customer.count_open_cases, 0::bigint) as cust_customer_open_cases,
    COALESCE(mview_fact_customer.count_closed_cases, 0::bigint) as cust_customer_closed_cases,
    COALESCE(mview_fact_customer.count_todos, 0::bigint) as cust_customer_todos,
    COALESCE(mview_fact_customer.count_open_todos, 0::bigint) as cust_customer_open_todos,
    COALESCE(mview_fact_customer.count_closed_todos, 0::bigint) as cust_customer_closed_todos,
    COALESCE(mview_fact_customer.count_overdue_todos, 0::bigint) as cust_customer_overdue_todos,
    COALESCE(mview_fact_customer.count_users, 0::bigint) as cust_customer_users,
	
	cb.display_name as cust_created_by_name,
	cb.email as cust_created_by_email,
	mb.display_name as cust_modified_by_name,
	mb.email as cust_modified_by_email

   FROM dim_customer
     LEFT JOIN mview_fact_customer ON dim_customer.ext_ref_id = mview_fact_customer.customer_ref_id
	 LEFT JOIN dim_users cb ON dim_customer.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_customer.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_customer.ver_is_current = true
     AND dim_customer.is_deleted = false;

ALTER VIEW public.view_cust
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cust TO postgres;
GRANT SELECT ON TABLE public.view_cust TO absence_tracker;
GRANT ALL ON TABLE public.view_cust TO etldw;
-- View: public.view_eplr

--DROP VIEW IF EXISTS public.view_eplr CASCADE;

CREATE OR REPLACE VIEW public.view_eplr AS
 SELECT 
	view_cust.*, 	
    dim_employer.ext_ref_id AS eplr_id,
    dim_employer.name AS eplr_employer_name,
    dim_employer.reference_code AS eplr_reference_code,
    dim_employer.url AS eplr_employer_url,
    dim_employer.contact_first_name AS eplr_contact_first_name,
    dim_employer.contact_last_name AS eplr_contact_last_name,
    dim_employer.contact_email AS eplr_contact_email,
    dim_employer.contact_address1 AS eplr_contact_address1,
    dim_employer.contact_address2 AS eplr_contact_address2,
    dim_employer.contact_city AS eplr_contact_city,
    dim_employer.contact_state AS eplr_contact_state,
    dim_employer.contact_postal_code AS eplr_contact_postal_code,
    dim_employer.contact_country AS eplr_contact_country,
    dim_employer.reset_month AS eplr_reset_month,
    dim_employer.reset_day_of_month AS eplr_reset_day_of_month,
	dim_employer.start_date AS eplr_start_date,
	dim_employer.end_date AS eplr_end_date,
    dim_employer.fml_period_type AS eplr_fml_period_type,
    dim_employer.allow_intermittent_for_birth_adoption_or_foster_care AS eplr_allow_intermittent_for_birth_adoption_or_foster_care,
    dim_employer.ignore_schedule_for_prior_hours_worked AS eplr_ignore_schedule_for_prior_hours_worked,
    dim_employer.enable50in75mile_rule AS eplr_enable_50_in_75_mile_rule,
    dim_employer.enable_spouse_at_same_employer_rule AS eplr_enable_spouse_at_same_employer_rule,
    dim_employer.ft_weekly_work_hours AS eplr_ft_weekly_work_hours,
    dim_employer.publicly_traded_company AS eplr_is_publicly_traded,
    COALESCE(mview_fact_employer.count_employees, 0::bigint) AS eplr_employer_employees,
    COALESCE(mview_fact_employer.count_cases, 0::bigint) AS eplr_employer_cases,
    COALESCE(mview_fact_employer.count_open_cases, 0::bigint) AS eplr_employer_open_cases,
    COALESCE(mview_fact_employer.count_closed_cases, 0::bigint) AS eplr_employer_closed_cases,
    COALESCE(mview_fact_employer.count_todos, 0::bigint) AS eplr_employer_todos,
    COALESCE(mview_fact_employer.count_open_todos, 0::bigint) AS eplr_employer_open_todos,
    COALESCE(mview_fact_employer.count_closed_todos, 0::bigint) AS eplr_employer_closed_todos,
    COALESCE(mview_fact_employer.count_overdue_todos, 0::bigint) AS eplr_employer_overdue_todos,
    COALESCE(mview_fact_employer.count_users, 0::bigint) AS eplr_employer_users,
    dim_employer.eplr_service_type,

	cb.display_name AS eplr_created_by_name,
	cb.email AS eplr_created_by_email,
	mb.display_name AS eplr_modified_by_name,
	mb.email AS eplr_modified_by_email

   FROM dim_employer
     LEFT JOIN mview_fact_employer ON dim_employer.ext_ref_id = mview_fact_employer.employer_ref_id
     LEFT JOIN view_cust ON dim_employer.customer_id = view_cust.cust_id
	 LEFT JOIN dim_users cb ON dim_employer.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employer.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_employer.ver_is_current = true
     AND dim_employer.is_deleted = false;

ALTER VIEW public.view_eplr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_eplr TO postgres;
GRANT SELECT ON TABLE public.view_eplr TO absence_tracker;
GRANT ALL ON TABLE public.view_eplr TO etldw;
-- View: public.view_empl


--DROP VIEW IF EXISTS public.view_empl CASCADE;

CREATE OR REPLACE VIEW public.view_empl AS
 SELECT 
	view_eplr.*,	
    dim_employee.ext_ref_id AS empl_id,
    dim_employee.employee_number AS empl_employee_number,
    dim_employee.first_name AS empl_first_name,
    dim_employee.middle_name AS empl_middle_name,
    dim_employee.last_name AS empl_last_name,
    dim_lookup_empl_gender.description AS empl_gender,
    dim_employee.dob AS empl_dob,
        CASE
            WHEN dim_employee.dob IS NULL THEN NULL::double precision
            ELSE date_part('year'::text, now()) - date_part('year'::text, dim_employee.dob)
        END AS empl_age_in_years,
    dim_lookup_empl_status.description AS empl_employee_status,
    dim_employee.salary AS empl_salary,
    dim_lookup_empl_pay_type.description AS empl_pay_type,
    dim_lookup_empl_work_type.description AS empl_work_type,
    dim_employee.cost_center_code AS empl_cost_center_code,
    dim_employee.service_date AS empl_service_date,
    (date_part('year'::text, now()) - date_part('year'::text, dim_employee.service_date)) * 12::double precision + (date_part('month'::text, now()) - date_part('month'::text, dim_employee.service_date)) AS empl_months_of_service,
    date_part('day'::text, now() - dim_employee.service_date::timestamp with time zone) AS empl_days_of_service,
    trunc(date_part('day'::text, now() - dim_employee.service_date::timestamp with time zone) / 7::double precision) AS empl_weeks_of_service,
    date_part('year'::text, now()) - date_part('year'::text, dim_employee.service_date) AS empl_years_of_service,
    dim_employee.hire_date AS empl_hire_date,
    dim_employee.rehire_date AS empl_rehire_date,
    dim_employee.termination_date AS empl_termination_date,
    dim_employee.work_state AS empl_work_state,
    dim_employee.work_country AS empl_work_country,
    dim_employee.meets50in75mile_rule AS empl_meets_50_in_75_mile_rule,
    dim_employee.key_employee AS empl_is_key_employee,
    dim_employee.exempt AS empl_is_exempt,
    dim_lookup_empl_military_status.description AS empl_military_status,
    dim_employee.spouse_employee_number AS empl_spouse_employee_number,
    dim_employee.ext_created_date AS empl_employee_created_date,
    dim_employee.email AS empl_employee_created_by_email,
    dim_employee.empl_employee_created_by_first_name,
    dim_employee.empl_employee_created_by_last_name,
    dim_employee.email AS empl_employee_email,
    dim_employee.alt_email AS empl_employee_alt_email,
    dim_employee.address1 AS empl_employee_address1,
    dim_employee.address2 AS empl_employee_address2,
    dim_employee.city AS empl_employee_city,
    dim_employee.state AS empl_employee_state,
    dim_employee.postal_code AS empl_employee_postal_code,
    dim_employee.country AS empl_employee_country,
    dim_employee.alt_address1 AS empl_employee_alt_address1,
    dim_employee.alt_address2 AS empl_employee_alt_address2,
    dim_employee.alt_city AS empl_employee_alt_city,
    dim_employee.alt_state AS empl_employee_alt_state,
    dim_employee.alt_postal_code AS empl_employee_alt_postal_code,
    dim_employee.alt_country AS empl_employee_alt_country,
    dim_employee.work_phone AS empl_employee_work_phone,
    dim_employee.home_phone AS empl_employee_home_phone,
    dim_employee.cell_phone AS empl_employee_cell_phone,
    dim_employee.alt_phone AS empl_employee_alt_phone,
    dim_employee.office_location AS empl_office_location,
    dim_employee.job_title AS empl_job_title,
    dim_employee.department AS empl_department,
    dim_employee.job_start_date AS empl_job_start_date,
    dim_employee.job_end_date AS empl_job_end_date,
    dim_employee.job_code AS empl_job_code,
    dim_employee.job_name AS empl_job_name,
    dim_employee.job_office_location_code AS empl_job_office_location_code,
    dim_employee.job_office_location_name AS empl_job_office_location_name,
    dim_employee.hours_worked AS empl_hours_worked_last_12_months,
	dim_employee.hours_worked_as_of AS empl_hours_worked_last_12_months_as_of,
	dim_employee.average_minutes_worked_per_week AS empl_average_minutes_worked_per_week,
	dim_employee.average_minutes_worked_per_week_as_of AS empl_average_minutes_worked_per_week_as_of,
    dim_employee.pay_schedule_name AS empl_pay_schedule,
    COALESCE(mview_fact_employee.count_cases, 0::bigint) AS empl_employee_cases,
    COALESCE(mview_fact_employee.count_open_cases, 0::bigint) AS empl_employee_open_cases,
    COALESCE(mview_fact_employee.count_closed_cases, 0::bigint) AS empl_employee_closed_cases,
    COALESCE(mview_fact_employee.count_todos, 0::bigint) AS empl_employee_todos,
    COALESCE(mview_fact_employee.count_open_todos, 0::bigint) AS empl_employee_open_todos,
    COALESCE(mview_fact_employee.count_closed_todos, 0::bigint) AS empl_employee_closed_todos,
    COALESCE(mview_fact_employee.count_overdue_todos, 0::bigint) AS empl_employee_overdue_todos,
    dim_employee.custom_fields AS empl_custom_fields,
	mview_supervisor.name AS empl_supervisor_name,
	mview_supervisor.employee_number AS empl_supervisor_employee_number,
	mview_supervisor.email AS empl_supervisor_email,
	mview_supervisor.phone AS empl_supervisor_phone,
	mview_hr.name AS empl_hr_name,
	mview_hr.employee_number AS empl_hr_employee_number,
	mview_hr.email AS empl_hr_email,
	mview_hr.phone AS empl_hr_phone,

	dim_employee.ssn AS empl_ssn,
	empl_job_classification.description AS empl_job_classification,
	dim_employee.risk_profile_code AS empl_risk_profile_code,
	dim_employee.risk_profile_name AS empl_risk_profile_name,
	dim_employee.risk_profile_description AS empl_risk_profile_description,
	dim_employee.risk_profile_order AS empl_risk_profile_order,
	dim_employee.start_day_of_week AS empl_start_day_of_week,

	cb.display_name AS empl_created_by_name,
	cb.email AS empl_created_by_email,
	mb.display_name AS empl_modified_by_name,
	mb.email AS empl_modified_by_email

   FROM dim_employee
     LEFT JOIN mview_fact_employee ON dim_employee.ext_ref_id = mview_fact_employee.employee_ref_id
	 LEFT JOIN view_eplr ON dim_employee.employer_id = view_eplr.eplr_id
     LEFT JOIN dim_lookup_empl_gender ON dim_employee.gender = dim_lookup_empl_gender.code
     LEFT JOIN dim_lookup_empl_status ON dim_employee.status = dim_lookup_empl_status.code
     LEFT JOIN dim_lookup_empl_military_status ON dim_employee.military_status = dim_lookup_empl_military_status.code
     LEFT JOIN dim_lookup_empl_work_type ON dim_employee.work_type = dim_lookup_empl_work_type.code
     LEFT JOIN dim_lookup_empl_pay_type ON dim_employee.pay_type = dim_lookup_empl_pay_type.code
	 LEFT JOIN mview_supervisor ON dim_employee.ext_ref_id = mview_supervisor.employee_id
	 LEFT JOIN mview_hr ON dim_employee.ext_ref_id = mview_hr.employee_id
	 LEFT JOIN dim_users cb ON dim_employee.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employee.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true

	 LEFT JOIN dim_lookup empl_job_classification ON dim_employee.job_classification = empl_job_classification.code AND empl_job_classification.type = 'empl_job_classification'
  WHERE dim_employee.ver_is_current = true
     AND dim_employee.is_deleted = false;

ALTER VIEW public.view_empl
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empl TO postgres;
GRANT SELECT ON TABLE public.view_empl TO absence_tracker;
GRANT ALL ON TABLE public.view_empl TO etldw;
-- View: public.view_case

--DROP VIEW IF EXISTS public.view_case CASCADE;

CREATE OR REPLACE VIEW public.view_case AS
 SELECT 
	view_empl.*,
    dim_cases.ext_ref_id AS case_id,
    dim_cases.case_number AS case_case_number,
    dim_cases.employer_case_number AS case_employer_case_number,
    dim_cases.start_date AS case_case_start_date,
        CASE
            WHEN dim_cases.end_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.end_date
        END AS case_case_end_date,
    dim_lookup_case_status.description AS case_case_status,
    dim_cases.spouse_case AS case_spouse_case_number,
    dim_lookup_case_types.description AS case_case_type,
    dim_cases.case_reason,
    dim_lookup_case_cancel_reason.description AS case_cancel_reason,
    dim_cases.assigned_to_name AS case_assigned_to,
    dim_lookup_case_closure_reason.description AS case_closed_reason,
	dim_cases.case_closure_reason_other,
	dim_cases.case_closure_other_reason_details,
    dim_cases.case_primary_diagnosis_code,
    dim_cases.case_primary_daignosis_name,
    dim_cases.case_secondary_diagnosis_code,
    dim_cases.case_secondary_daignosis_name,
    dim_cases.primary_path_text AS case_primary_rtw_path,
    dim_cases.primary_path_min_days AS case_primary_rtw_path_min_days,
    dim_cases.primary_path_max_days AS case_primary_rtw_path_max_days,
    dim_cases.hospitalization AS case_hospitalization,
        CASE
            WHEN dim_cases.condition_start_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.condition_start_date
        END AS case_condition_start_date,
    dim_cases.related_case_number AS case_related_case_number,
    dim_lookup_case_related_type.description AS case_related_case_type,
	dim_cases.relationship_type_code AS case_relationship_type_code,
	dim_cases.relationship_type_name AS case_relationship_type_name,
	dim_cases.is_intermittent AS case_is_intermittent,
    dim_cases.case_cert_start_date,
    dim_cases.case_cert_end_date,
    dim_cases.occurances AS case_cert_occurances,
    dim_cases.frequency AS case_cert_frequency,
    dim_lookup_case_frequency_type.description AS case_cert_frequency_type,
    dim_lookup_case_frequency_unit_type.description AS case_cert_frequency_unit_type,
    dim_cases.duration AS case_cert_duration,
    dim_lookup_case_duration_type.description AS case_cert_duration_type,
    dim_cases.is_accommodation AS case_is_accommodation,
    dim_cases.is_work_related AS case_is_work_related,
    dim_cases.case_is_denied,
    dim_cases.case_is_approved,
		CASE
            WHEN dim_cases.case_is_eligible = 1 THEN 'True'::text 
			ELSE 'False'::text
		END AS case_is_eligible,
	dim_lookup_case_determination.description AS case_case_determination,
    dim_cases.case_fmla_is_approved,
    dim_cases.case_fmla_is_denied,
    dim_cases.case_fmla_projected_usage,
        CASE
            WHEN dim_cases.case_fmla_exhaust_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_fmla_exhaust_date
        END AS case_fmla_exhaust_date,
        CASE
            WHEN dim_cases.case_max_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_max_approved_thru_date
        END AS case_case_max_approved_thru_date,
        CASE
            WHEN dim_cases.case_min_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_min_approved_thru_date
        END AS case_case_min_approved_thru_date,
    dim_lookup_case_accom_status.description AS case_accom_status,
    dim_cases.case_accom_start_date,
    dim_cases.case_accom_end_date,
        CASE
            WHEN dim_cases.case_accom_min_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_min_approved_date
        END AS case_accom_min_approved_date,
        CASE
            WHEN dim_cases.case_accom_max_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_max_approved_date
        END AS case_accom_max_approved_date,
    dim_lookup_case_accom_determination.description AS case_accom_determination,
    dim_lookup_case_accom_cancel_reason.description AS case_accom_cancel_reason,
    dim_cases.case_accom_cancel_reason_other,
    dim_cases.case_accom_duration,
    dim_cases.case_accom_duration_type,
    dim_cases.ext_created_date AS case_case_created_date,
    dim_cases.case_created_by_email AS case_case_created_by_email,
    dim_cases.case_created_by_first_name AS case_case_created_by_first_name,
    dim_cases.case_created_by_last_name AS case_case_created_by_last_name,
    dim_cases.return_to_work AS case_return_to_work,
    dim_cases.delivery_date AS case_delivery_date,
    dim_cases.bonding_start_date AS case_bonding_start_date,
    dim_cases.bonding_end_date AS case_bonding_end_date,
    dim_cases.illness_or_injury_date AS case_illness_or_injury_date,
    dim_cases.hospital_admission_date AS case_hospital_admission_date,
    dim_cases.hospital_release_date AS case_hospital_release_date,
    dim_cases.release_received AS case_release_received,
    dim_cases.paperwork_received AS case_paperwork_received,
    dim_cases.accommodation_added AS case_accommodation_added,
    dim_cases.case_closed AS case_case_closed,
    dim_cases.case_cancelled AS case_case_cancelled,
    dim_cases.adoption_date AS case_adoption_date,
    dim_cases.estimated_return_to_work AS case_estimated_return_to_work,
    dim_cases.employer_holiday_schedule_changed AS case_employer_holiday_schedule_changed,
    COALESCE(mview_fact_case.count_todos, 0::bigint) AS case_case_todos,
    COALESCE(mview_fact_case.count_open_todos, 0::bigint) AS case_case_open_todos,
    COALESCE(mview_fact_case.count_closed_todos, 0::bigint) AS case_case_closed_todos,
    COALESCE(mview_fact_case.count_overdue_todos, 0::bigint) AS case_case_overdue_todos,
    dim_cases.case_reporter_type_code,
    dim_cases.case_reporter_type_name,
    dim_cases.case_reporter_first_name,
    dim_cases.case_reporter_last_name,
    dim_cases.custom_fields AS case_custom_fields,
		
	dim_cases.wr_date_of_injury as case_wr_date_of_injury,
	dim_cases.wr_is_pvt_healthcare as case_wr_is_pvt_healthcare,
	dim_cases.wr_is_reportable as case_wr_is_reportable,
	dim_cases.wr_place_of_occurance as case_wr_place_of_occurance,
	work_classification.description as case_wr_work_classification,
	type_of_injury.description as case_wr_type_of_injury,
	dim_cases.wr_days_away_from_work as case_wr_days_away_from_work,
	dim_cases.wr_restricted_days as case_wr_restricted_days,
	dim_cases.wr_provider_first_name as case_wr_provider_first_name,
	dim_cases.wr_provider_last_name as case_wr_provider_last_name,
	dim_cases.wr_provider_company_name as case_wr_provider_company_name,
	dim_cases.wr_provider_address1 as case_wr_provider_address1,
	dim_cases.wr_provider_address2 as case_wr_provider_address2,
	dim_cases.wr_provider_city as case_wr_provider_city,
	dim_cases.wr_provider_state as case_wr_provider_state,
	dim_cases.wr_provider_country as case_wr_provider_country,
	dim_cases.wr_provider_postalcode as case_wr_provider_postalcode,
	dim_cases.wr_is_treated_in_emer_room as case_wr_is_treated_in_emer_room,
	dim_cases.wr_is_hospitalized as case_wr_is_hospitalized,
	dim_cases.wr_time_started_working as case_wr_time_started_working,
	dim_cases.wr_time_of_injury as case_wr_time_of_injury,
	dim_cases.wr_activity_before_injury as case_wr_activity_before_injury,
	dim_cases.wr_what_happened as case_wr_what_happened,
	dim_cases.wr_injury_details as case_wr_injury_details,
	dim_cases.wr_what_harmed_empl as case_wr_what_harmed_empl,
	dim_cases.wr_date_of_death as case_wr_date_of_death,
	dim_cases.wr_is_sharp as case_wr_is_sharp,
	dim_cases.wr_sharp_body_part as case_wr_sharp_body_part,
	dim_cases.wr_type_of_sharp as case_wr_type_of_sharp,
	dim_cases.wr_brand_of_sharp as case_wr_brand_of_sharp,
	dim_cases.wr_model_of_sharp as case_wr_model_of_sharp,
	dim_cases.wr_body_fluid as case_wr_body_fluid,
	dim_cases.wr_is_engn_injury_protection as case_wr_is_engn_injury_protection,
	dim_cases.wr_is_protective_mech_activated as case_wr_is_protective_mech_activated,
	sharp_incdnt_when.description as case_wr_when_sharp_incident_happened,
	sharp_job_class.description as case_wr_sharp_job_classification,
	dim_cases.wr_sharp_other_job_classification as case_wr_sharp_other_job_classification,
	sharp_location_dept.description as case_wr_sharp_location_department,
	dim_cases.wr_sharp_other_location_dept as case_wr_sharp_other_location_dept,
	sharp_procedure.description as case_wr_sharp_procedure,
	dim_cases.wr_sharp_other_procedure as case_wr_sharp_other_procedure,
	dim_cases.wr_sharp_exposure_detail as case_wr_sharp_exposure_detail,

	dim_cases.cg_risk_asmt_score as case_cg_risk_asmt_score,
	dim_cases.cg_risk_asmt_status as case_cg_risk_asmt_status,
	dim_cases.cg_mid_range_all_absence as case_cg_mid_range_all_absence,
	dim_cases.cg_at_risk_all_absence as case_cg_at_risk_all_absence,
	dim_cases.cg_best_practice_days as case_cg_best_practice_days,
	dim_cases.cg_actual_days_lost as case_cg_actual_days_lost,

	dim_cases.closure_reason as case_closure_reason,
	dim_cases.closure_reason_detail as case_closure_reason_detail,
	dim_cases.description as case_description,
	dim_cases.narrative as case_narrative,
	dim_cases.is_rpt_auth_submitter as case_is_rpt_auth_submitter,
	dim_cases.send_e_communication case_send_e_communication,
	dim_cases.e_communication_request_date case_e_communication_request_date,
	dim_cases.risk_profile_code case_risk_profile_code,
	dim_cases.risk_profile_name case_risk_profile_name,
	dim_cases.current_office_location case_current_office_location,
	dim_cases.total_paid case_pay_total_paid,
	dim_cases.total_offset case_pay_total_offset,
	dim_cases.apply_offsets_by_default case_pay_apply_offsets_by_default,
	dim_cases.waive_waiting_period case_pay_waive_waiting_period,
	dim_cases.pay_schedule_id case_pay_pay_schedule_id,
	
	dim_cases.alt_phone_number as case_alt_phone_number,
	dim_cases.alt_email as case_alt_email,
	dim_cases.alt_address1 as case_alt_address1,
	dim_cases.alt_address2 as case_alt_address2,
	dim_cases.alt_city as case_alt_city,
	dim_cases.alt_state as case_alt_state,
	dim_cases.alt_postal_code as case_alt_postal_code,
	dim_cases.alt_country as case_alt_country,

	cb.display_name AS case_created_by_name,
	cb.email AS case_created_by_email,
	mb.display_name AS case_modified_by_name,
	mb.email AS case_modified_by_email,
	intermittent_type.description As case_cert_intermittent_type,

	dim_cases.actual_duration AS case_actual_duration,
	dim_cases.exceeded_duration AS case_exceeded_duration,
	has_relapse AS case_has_relapse,
	COALESCE(mview_fact_case.count_relapses, 0::bigint) AS case_count_relapses

   FROM dim_cases
     LEFT JOIN mview_fact_case ON dim_cases.ext_ref_id = mview_fact_case.case_ref_id
     LEFT JOIN view_empl ON dim_cases.employee_id = view_empl.empl_id
     LEFT JOIN dim_lookup_case_accom_status ON dim_cases.case_accom_status = dim_lookup_case_accom_status.code
     LEFT JOIN dim_lookup_case_accom_cancel_reason ON dim_cases.case_accom_cancel_reason = dim_lookup_case_accom_cancel_reason.code
     LEFT JOIN dim_lookup_case_accom_determination ON dim_cases.case_accom_determination = dim_lookup_case_accom_determination.code
     LEFT JOIN dim_lookup_case_determination ON dim_cases.case_determination = dim_lookup_case_determination.code
     LEFT JOIN dim_lookup_case_duration_type ON dim_cases.duration_type = dim_lookup_case_duration_type.code
     LEFT JOIN dim_lookup_case_frequency_type ON dim_cases.frequency_type = dim_lookup_case_frequency_type.code
     LEFT JOIN dim_lookup_case_frequency_unit_type ON dim_cases.frequency_unit_type = dim_lookup_case_frequency_unit_type.code
     LEFT JOIN dim_lookup_case_status ON dim_cases.status = dim_lookup_case_status.code
     LEFT JOIN dim_lookup_case_types ON dim_cases.type = dim_lookup_case_types.code
     LEFT JOIN dim_lookup_case_cancel_reason ON dim_cases.cancel_reason = dim_lookup_case_cancel_reason.code
     LEFT JOIN dim_lookup_case_closure_reason ON dim_cases.case_closure_reason = dim_lookup_case_closure_reason.code
     LEFT JOIN dim_lookup_case_related_type ON dim_cases.related_case_type = dim_lookup_case_related_type.code

	 LEFT JOIN dim_lookup work_classification ON dim_cases.wr_work_classification = work_classification.code AND work_classification.type = 'work_classification'
	 LEFT JOIN dim_lookup type_of_injury ON dim_cases.wr_work_classification = type_of_injury.code AND type_of_injury.type = 'type_of_injury'
	 LEFT JOIN dim_lookup sharp_incdnt_when ON dim_cases.wr_work_classification = sharp_incdnt_when.code AND sharp_incdnt_when.type = 'sharp_incdnt_when'
	 LEFT JOIN dim_lookup sharp_job_class ON dim_cases.wr_work_classification = sharp_job_class.code AND sharp_job_class.type = 'sharp_job_class'
	 LEFT JOIN dim_lookup sharp_location_dept ON dim_cases.wr_work_classification = sharp_location_dept.code AND sharp_location_dept.type = 'sharp_location_dept'
	 LEFT JOIN dim_lookup sharp_procedure ON dim_cases.wr_work_classification = sharp_procedure.code AND sharp_procedure.type = 'sharp_procedure'
	 LEFT JOIN dim_lookup intermittent_type ON dim_cases.intermittent_type = intermittent_type.code AND intermittent_type.type = 'intermittent_type'
	 
	 LEFT JOIN dim_users cb ON dim_cases.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_cases.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_cases.ver_is_current = true
     AND dim_cases.is_deleted = false;

ALTER VIEW public.view_case
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_case TO postgres;
GRANT SELECT ON TABLE public.view_case TO absence_tracker;
GRANT ALL ON TABLE public.view_case TO etldw;
-- View: public.view_acom

--DROP VIEW IF EXISTS public.view_acom CASCADE;

CREATE OR REPLACE VIEW public.view_acom AS
 SELECT
	view_case.*,
	dim_accommodation.ext_ref_id as acom_id,
	dim_accommodation.dim_accommodation_id as acom_dim_accommodation_id,
    dim_accommodation.accommodation_type AS acom_accommodation_type,
    dim_accommodation.accommodation_type_code AS acom_accommodation_type_code,
    dim_lookup_acom_duration.description AS acom_accommodation_duration,
    dim_accommodation.resolution AS acom_accommodation_resolution,
    dim_accommodation.resolved AS acom_accommodation_resolved,
    dim_accommodation.resolved_date AS acom_accommodation_resolved_date,
    dim_accommodation.granted AS acom_accommodation_granted,
    dim_accommodation.granted_date AS acom_accommodation_granted_date,
    dim_accommodation.cost AS acom_accommodation_cost,
        CASE
            WHEN dim_accommodation.start_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_accommodation.start_date
        END AS acom_accommodation_start_date,
        CASE
            WHEN dim_accommodation.end_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_accommodation.end_date
        END AS acom_accommodation_end_date,
    acom_determination.description AS acom_accommodation_determination,
    dim_accommodation.min_approved_date AS acom_accommodation_min_approved_date,
    dim_accommodation.max_approved_date AS acom_accommodation_max_approved_date,
    dim_accommodation.ext_created_date AS acom_accommodation_created_date,
    accom_status.description AS acom_accommodation_status,
        CASE
            WHEN dim_accommodation.cancel_reason = 0 THEN COALESCE(dim_accommodation.cancel_reason_other_desc, 'Other'::text::character varying)
            ELSE dim_lookup_accom_cancel_reason.description
        END AS acom_accommodation_cancel_reason,
	dim_accommodation.denial_reason_code AS accom_accommodation_denial_reason_code,
    dim_accommodation.work_related AS acom_accommodation_is_work_related,

	cb.display_name AS acom_created_by_name,
	cb.email AS acom_created_by_email,
	mb.display_name AS acom_modified_by_name,
	mb.email AS acom_modified_by_email

   FROM dim_accommodation
	 LEFT JOIN view_case ON dim_accommodation.case_id = view_case.case_id
     LEFT JOIN dim_lookup_acom_duration ON dim_accommodation.duration = dim_lookup_acom_duration.code
     LEFT JOIN dim_lookup_case_accom_determination acom_determination ON dim_accommodation.determination = acom_determination.code
     LEFT JOIN dim_lookup_case_accom_status accom_status ON dim_accommodation.status = accom_status.code
     LEFT JOIN dim_lookup_accom_cancel_reason ON dim_accommodation.cancel_reason = dim_lookup_accom_cancel_reason.code
	 LEFT JOIN dim_users cb ON dim_accommodation.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_accommodation.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_accommodation.ver_is_current = true
     AND dim_accommodation.is_deleted = false;

ALTER VIEW public.view_acom
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_acom TO postgres;
GRANT SELECT ON TABLE public.view_acom TO absence_tracker;
GRANT ALL ON TABLE public.view_acom TO etldw;
-- View: public.view_org

--DROP VIEW IF EXISTS public.view_org CASCADE;

CREATE OR REPLACE VIEW public.view_org AS
 SELECT
	view_eplr.*,
    dim_organization.ext_ref_id AS org_id,
    dim_organization.code AS org_org_code,
    dim_organization.name AS org_org_name,
    dim_organization.description AS org_org_description,
    dim_organization.path AS org_org_path,
    dim_organization.type_code AS org_org_type_code,
    dim_organization.type_name AS org_org_type_name,
	dim_organization.siccode AS org_siccode,
    dim_organization.naicscode AS org_naicscode,
	dim_organization.address1 AS org_address1,
	dim_organization.address2 AS org_address2,
	dim_organization.city AS org_city,
	dim_organization.postalcode AS org_postalcode,
	dim_organization.state AS org_state,
	dim_organization.country AS org_country,
    dim_organization.ext_created_date AS org_org_created_date,
    dim_organization.org_created_by_email AS org_org_created_by_email,
    dim_organization.org_created_by_first_name AS org_org_created_by_first_name,
    dim_organization.org_created_by_last_name AS org_org_created_by_last_name,
    COALESCE(mview_fact_organization.org_level, 1) AS org_org_level,
    COALESCE(mview_fact_organization.count_children, 0::bigint) AS org_org_children,
    COALESCE(mview_fact_organization.count_descendents, 0::bigint) AS org_org_descendents,
    COALESCE(mview_fact_organization.count_employees, 0::bigint) AS org_org_employees,
    COALESCE(mview_fact_organization.count_cases, 0::bigint) AS org_org_cases,
    COALESCE(mview_fact_organization.count_open_cases, 0::bigint) AS org_org_open_cases,
    COALESCE(mview_fact_organization.count_closed_cases, 0::bigint) AS org_org_closed_cases,
    COALESCE(mview_fact_organization.count_todos, 0::bigint) AS org_org_todos,
    COALESCE(mview_fact_organization.count_open_todos, 0::bigint) AS org_org_open_todos,
    COALESCE(mview_fact_organization.count_closed_todos, 0::bigint) AS org_org_closed_todos,
    COALESCE(mview_fact_organization.count_overdue_todos, 0::bigint) AS org_org_overdue_todos,

	cb.display_name AS org_created_by_name,
	cb.email AS org_created_by_email,
	mb.display_name AS org_modified_by_name,
	mb.email AS org_modified_by_email

   FROM dim_organization
     LEFT JOIN mview_fact_organization ON dim_organization.ext_ref_id = mview_fact_organization.organization_ref_id
	 LEFT JOIN view_eplr ON dim_organization.employer_id = view_eplr.eplr_id
	 LEFT JOIN dim_users cb ON dim_organization.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_organization.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_organization.ver_is_current = true
     AND dim_organization.is_deleted = false;

ALTER VIEW public.view_org
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_org TO postgres;
GRANT SELECT ON TABLE public.view_org TO absence_tracker;
GRANT ALL ON TABLE public.view_org TO etldw;
-- View: public.view_todo

--DROP VIEW IF EXISTS public.view_todo CASCADE;

CREATE OR REPLACE VIEW public.view_todo AS
 SELECT 
	view_case.*,
    dim_todoitem.ext_ref_id AS todo_id,
    dim_lookup_todoitem_priority.description AS todo_priority,
	CASE
        WHEN dim_todoitem.status = 1 THEN 'Complete'::text
        WHEN dim_todoitem.status = 3 THEN 'Cancelled'::text
		WHEN (dim_todoitem.due_date::timestamp::date < utc()::timestamp::date) THEN 'Overdue'::text
        ELSE 'Pending'::text
    END AS todo_todo_status,
    dim_lookup_todoitem_types.name AS todo_todo_type,
    dim_lookup_todoitem_types.category AS todo_category,
    dim_lookup_todoitem_types.feature AS todo_feature,
    dim_todoitem.title AS todo_title,
    dim_todoitem.assigned_to_email AS todo_assigned_to_email,
    dim_todoitem.assigned_to_first_name AS todo_assigned_to_first_name,
    dim_todoitem.assigned_to_last_name AS todo_assigned_to_last_name,
    dim_todoitem.due_date AS todo_due_date,
    dim_todoitem.due_date_change_reason AS todo_due_date_change_reason,
    dim_todoitem.weight AS todo_weight,
    dim_todoitem.result_text AS todo_result,
    dim_todoitem.optional AS todo_is_optional,
    dim_todoitem.hide_until AS todo_hide_until_date,
    dim_todoitem.hide_until IS NULL OR dim_todoitem.hide_until <= utc() AS todo_is_visible,
        CASE
            WHEN dim_todoitem.due_date < ( CASE WHEN dim_todoitem.status IN (1, 3) THEN dim_todoitem.ext_modified_date ELSE utc() END ) THEN date_part('day'::text, utc() - dim_todoitem.due_date::timestamp with time zone)
            ELSE 0::double precision
        END AS todo_days_overdue,
    dim_todoitem.template AS todo_communication,
    dim_todoitem.ext_created_date AS todo_todo_created_date,
    dim_todoitem.ext_modified_date AS todo_todo_last_modified_date,
    dim_todoitem.work_flow_name AS todo_workflow_name,

	cb.display_name AS todo_created_by_name,
	cb.email AS todo_created_by_email,
	mb.display_name AS todo_modified_by_name,
	mb.email AS todo_modified_by_email

   FROM dim_todoitem
	 LEFT JOIN view_case ON dim_todoitem.case_id = view_case.case_id
     LEFT JOIN dim_lookup_todoitem_priority ON dim_todoitem.priority = dim_lookup_todoitem_priority.code
     LEFT JOIN dim_lookup_todoitem_types ON dim_todoitem.item_type = dim_lookup_todoitem_types.code
	 LEFT JOIN dim_users cb ON dim_todoitem.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_todoitem.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_todoitem.ver_is_current = true AND dim_todoitem.is_deleted = false;

ALTER VIEW public.view_todo
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_todo TO postgres;
GRANT SELECT ON TABLE public.view_todo TO absence_tracker;
GRANT ALL ON TABLE public.view_todo TO etldw;
-- View: public.view_user

--DROP VIEW IF EXISTS public.view_user CASCADE;

CREATE OR REPLACE VIEW public.view_user AS
 SELECT 
	view_cust.*,
	dim_users.ext_ref_id AS user_id,
    dim_users.email AS user_user_email,
    dim_users.first_name AS user_user_first_name,
    dim_users.last_name AS user_user_last_name,
    dim_users.display_name AS user_user_display_name,
    dim_users.must_change_password AS user_must_change_password,
    dim_users.failed_login_attempts AS user_failed_login_attempts,
    dim_users.last_failed_login_attempt AS user_last_failed_login_attempt,
    dim_users.locked AS user_is_locked,
    dim_users.locked_date AS user_locked_date,
    dim_users.disabled AS user_disabled,
    dim_users.disabled_date AS user_disabled_date,
    dim_users.role_name AS user_role,
    dim_users.last_activity_date AS user_last_activity_date,
    dim_lookup_user_status.description AS user_user_status,
    dim_lookup_user_type.description AS user_user_type,
    dim_users.is_self_service_user AS user_is_ess,
    dim_users.is_portal_user AS user_is_portal,
    dim_users.user_employee_id,
    dim_users.user_employee_number,
    mview_fact_user.count_cases AS user_user_cases,
    mview_fact_user.count_open_cases AS user_user_open_cases,
    mview_fact_user.count_closed_cases AS user_user_closed_cases,
    mview_fact_user.count_todos AS user_user_todos,
    mview_fact_user.count_open_todos AS user_user_open_todos,
    mview_fact_user.count_closed_todos AS user_user_closed_todos,
    mview_fact_user.count_overdue_todos AS user_user_overdue_todos,

	cb.display_name AS user_created_by_name,
	cb.email AS user_created_by_email,
	mb.display_name AS user_modified_by_name,
	mb.email AS user_modified_by_email

   FROM dim_users
     LEFT JOIN mview_fact_user ON dim_users.ext_ref_id = mview_fact_user.user_ref_id
     LEFT JOIN view_cust ON dim_users.customer_id = view_cust.cust_id
     LEFT JOIN dim_lookup_user_status ON dim_users.user_status = dim_lookup_user_status.code
     LEFT JOIN dim_lookup_user_type ON dim_users.user_type = dim_lookup_user_type.code
	 LEFT JOIN dim_users cb ON dim_users.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_users.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_users.is_deleted = false AND dim_users.ver_is_current = true;

ALTER VIEW public.view_user
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_user TO postgres;
GRANT SELECT ON TABLE public.view_user TO absence_tracker;
GRANT ALL ON TABLE public.view_user TO etldw;
-- View: public.view_wkrs

-- DROP VIEW public.view_wkrs;
--DROP VIEW IF EXISTS public.view_wkrs CASCADE;


CREATE OR REPLACE VIEW public.view_wkrs AS
 SELECT 
	view_case.*,
	dim_employee_restriction.dim_employee_restriction_id as view_employee_restriction_id,
	dim_employee_restriction.ext_ref_id as wkrs_id,
    dim_employee_restriction.start_date AS wkrs_restriction_start_date,
    dim_employee_restriction.end_date AS wkrs_restriction_end_date,
    dim_employee_restriction.wkrs_restriction_name,
    dim_employee_restriction.wkrs_restriction_code,
    dim_employee_restriction.wkrs_restriction_category,
	dim_employee_restriction_entry.wkrs_restriction_type,
    dim_employee_restriction_entry.wkrs_restriction,

   	cb.display_name AS wkrs_created_by_name,
	cb.email AS wkrs_created_by_email,
	mb.display_name AS wkrs_modified_by_name,
	mb.email AS wkrs_modified_by_email

   FROM dim_employee_restriction   
	 LEFT JOIN dim_employee_restriction_entry ON dim_employee_restriction.dim_employee_restriction_id = dim_employee_restriction_entry.dim_employee_restriction_id
     LEFT JOIN view_case ON dim_employee_restriction.case_id = view_case.case_id
	 LEFT JOIN dim_users cb ON dim_employee_restriction.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employee_restriction.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_employee_restriction.ver_is_current = true
     AND dim_employee_restriction.is_deleted = false;

ALTER VIEW public.view_wkrs
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_wkrs TO postgres;
GRANT SELECT ON TABLE public.view_wkrs TO absence_tracker;
GRANT ALL ON TABLE public.view_wkrs TO etldw;

-- View: public.view_acmu

DROP VIEW IF EXISTS public.view_acmu CASCADE;

CREATE OR REPLACE VIEW public.view_acmu AS
SELECT
	view_acom.*	
    ,au.ext_ref_id as acmu_id
    ,au.start_date as acmu_start_date
    ,au.end_date as acmu_end_date
    ,dim_lookup_case_accom_status.code as acmu_code
	,dim_lookup_case_accom_status.description as acmu_description
	,au.is_deleted as acmu_is_deleted
FROM public.dim_accommodation_usage au
LEFT JOIN view_acom ON au.dim_accommodation_id = view_acom.acom_dim_accommodation_id
LEFT JOIN dim_lookup_case_accom_status ON au.determination = dim_lookup_case_accom_status.code
WHERE au.is_deleted = FALSE;

ALTER VIEW public.view_acmu
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_acmu TO postgres;
GRANT SELECT ON TABLE public.view_acmu TO absence_tracker;
GRANT ALL ON TABLE public.view_acmu TO etldw;
-- View: public.view_asbm

--DROP VIEW IF EXISTS public.view_asbm CASCADE;

CREATE OR REPLACE VIEW public.view_asbm AS
 SELECT
	view_case.*,
	dim_case_authorized_submitter.employee_id  as asbm_employee_Id,
	dim_case_authorized_submitter.customer_id as asbm_customer_id,
	dim_case_authorized_submitter.contact_type_code as asbm_contact_type_code,
	dim_case_authorized_submitter.contact_type_name as asbm_contact_type_name,
	dim_case_authorized_submitter.contact_first_name as asbm_contact_first_name ,
	dim_case_authorized_submitter.contact_last_name as asbm_contact_last_name ,
	dim_case_authorized_submitter.contact_email as asbm_contact_email ,
	dim_case_authorized_submitter.contact_alt_email as asbm_contact_alt_email ,
	dim_case_authorized_submitter.contact_work_phone as asbm_contact_work_phone ,
	dim_case_authorized_submitter.contact_home_phone as asbm_contact_home_phone ,
	dim_case_authorized_submitter.contact_cell_phone as asbm_contact_cell_phone ,
	dim_case_authorized_submitter.contact_fax as asbm_contact_fax ,
	dim_case_authorized_submitter.contact_street as asbm_contact_street ,
	dim_case_authorized_submitter.contact_address1 as asbm_contact_address1 ,
	dim_case_authorized_submitter.contact_address2 as asbm_contact_address2 ,
	dim_case_authorized_submitter.contact_city as asbm_contact_city ,
	dim_case_authorized_submitter.contact_state as asbm_contact_state ,
	dim_case_authorized_submitter.contact_postal_code as asbm_contact_postal_code ,
	dim_case_authorized_submitter.contact_country as asbm_contact_country ,

	cb.display_name as asbm_created_by_name,
	cb.email as asbm_created_by_email,
	mb.display_name as asbm_modified_by_name,
	mb.email as asbm_modified_by_email

   FROM dim_case_authorized_submitter
	 INNER JOIN view_case ON dim_case_authorized_submitter.ext_ref_id = view_case.case_id
	 LEFT JOIN dim_users cb ON dim_case_authorized_submitter.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_authorized_submitter.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_authorized_submitter.ver_is_current = true
     AND dim_case_authorized_submitter.is_deleted = false;

ALTER VIEW public.view_asbm
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_asbm TO postgres;
GRANT SELECT ON TABLE public.view_asbm TO absence_tracker;
GRANT ALL ON TABLE public.view_asbm TO etldw;
-- View: public.view_attm

--DROP VIEW IF EXISTS public.view_attm CASCADE;

CREATE OR REPLACE VIEW public.view_attm AS
 SELECT
	view_case.*,  	
	dim_attachment.ext_ref_id as attm_id,
	dim_attachment.content_length as attm_content_length,
	dim_attachment.content_type as attm_content_type,
	lookup_attachment_type.description as attm_attachment_type,
	dim_attachment.created_by_name as attm_created_by_name,
	dim_attachment.description as attm_description,
	dim_attachment.file_id as attm_file_id,
	dim_attachment.file_Name as attm_file_Name,
	dim_attachment.public as attm_public,    
		
	cb.email as attm_created_by_email,
	mb.display_name as attm_modified_by_name,
	mb.email as attm_modified_by_email

   FROM dim_attachment
	 LEFT JOIN view_case ON dim_attachment.case_id = view_case.case_id
     LEFT JOIN dim_lookup lookup_attachment_type ON dim_attachment.attachment_type = lookup_attachment_type.code and lookup_attachment_type.type='attachment_type'
     LEFT JOIN dim_users cb ON dim_attachment.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_attachment.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_attachment.ver_is_current = true AND dim_attachment.is_deleted = false;
ALTER VIEW public.view_attm OWNER TO postgres;
GRANT ALL ON TABLE public.view_attm TO postgres;
GRANT SELECT ON TABLE public.view_attm TO absence_tracker;
GRANT ALL ON TABLE public.view_attm TO etldw;
-- View: public.view_cnot


--DROP VIEW IF EXISTS public.view_cnot CASCADE;

CREATE OR REPLACE VIEW public.view_cnot AS
 SELECT
	view_case.*,
	dim_case_note.ext_ref_id as cnot_id,
    dim_case_note.accomm_id  as cnot_accomm_id ,
	dim_case_note.accomm_question_id  as cnot_accomm_question_id ,
	dim_case_note.answer  as cnot_answer ,	
	dim_case_note.cert_id     as cnot_cert_id ,
	dim_case_note.contact     as cnot_contact ,
	dim_case_note.copied_from     as cnot_copied_from ,
	dim_case_note.copied_from_employee    as cnot_copied_from_employee ,
	dim_case_note.employee_number     as cnot_employee_number ,
	dim_case_note.entered_by_name     as cnot_entered_by_name ,
	dim_case_note.notes   as cnot_notes ,
	dim_case_note.process_path as cnot_process_path ,
	dim_case_note.request_date as cnot_request_date ,
	dim_case_note.request_id  as cnot_request_id ,
	dim_case_note.s_answer as cnot_s_answer ,
	dim_case_note.work_restriction_id as cnot_work_restriction_id ,	
	dim_case_note.category_code  as cnot_category_code ,
	dim_case_note.category_name  as cnot_category_name ,
	dim_lookup.description as cnot_category,

	cb.display_name as cnot_created_by_name,
	cb.email as cnot_created_by_email,
	mb.display_name as cnot_modified_by_name,
	mb.email as cnot_modified_by_email

   FROM dim_case_note
	 INNER JOIN view_case ON dim_case_note.case_id = view_case.case_id
     LEFT JOIN dim_lookup  ON dim_case_note.category = dim_lookup.code and dim_lookup.type='note_category'
     LEFT JOIN dim_users cb ON dim_case_note.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_note.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_note.ver_is_current = true
     AND dim_case_note.is_deleted = false;

ALTER VIEW public.view_cnot
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cnot TO postgres;
GRANT SELECT ON TABLE public.view_cnot TO absence_tracker;
GRANT ALL ON TABLE public.view_cnot TO etldw;
-- View: public.view_comm

--DROP VIEW IF EXISTS public.view_comm CASCADE;

CREATE OR REPLACE VIEW public.view_comm AS
 SELECT
	view_case.*,
	 dim_communication.dim_communication_id as view_communication_id, 
	 dim_communication.ext_ref_id as comm_id,
	 dim_communication.body as comm_body ,
	 lookup_communication_type.description as comm_communication_type, 
	 lookup_email_replies_type.description as comm_email_replies_type,
	 dim_communication.created_by_email as comm_created_by_email,
	 dim_communication.created_by_name as comm_created_by_name,
	 dim_communication.is_draft  as comm_is_draft,
	 dim_communication.name as comm_name,
	 dim_communication.public as comm_public,
	 dim_communication.first_send_date as comm_first_send_date,
	 dim_communication.last_send_date as comm_last_send_date,	
	 dim_communication.subject as comm_subject,
	 dim_communication.template  as comm_template,
	 dim_communication.ext_created_date  as comm_created_date,
	 
 /*attachment fields */
	view_attm.attm_id,
	view_attm.attm_content_length, 
	view_attm.attm_content_type, 
	view_attm.attm_attachment_type, 
	view_attm.attm_created_by_name, 
	view_attm.attm_description, 
	view_attm.attm_file_id,
	view_attm.attm_file_Name, 
	view_attm.attm_public,
	view_attm.attm_created_by_email,
	view_attm.attm_modified_by_name,
	view_attm.attm_modified_by_email,

 /*todo item fields*/
	view_todo.todo_id,
	view_todo.todo_priority,
	view_todo.todo_todo_status,	
	view_todo.todo_todo_type,
	view_todo.todo_category,
	view_todo.todo_feature,
	view_todo.todo_title,
	view_todo.todo_assigned_to_email,
	view_todo.todo_assigned_to_first_name,
	view_todo.todo_assigned_to_last_name,
	view_todo.todo_due_date,
	view_todo.todo_due_date_change_reason,
	view_todo.todo_weight,
	view_todo.todo_result,
	view_todo.todo_is_optional,
	view_todo.todo_hide_until_date,
	view_todo.todo_days_overdue,
	view_todo.todo_communication,
	view_todo.todo_todo_created_date,
	view_todo.todo_todo_last_modified_date,
	view_todo.todo_workflow_name,
	view_todo.todo_created_by_name,
	view_todo.todo_created_by_email,
	view_todo.todo_modified_by_name,
	view_todo.todo_modified_by_email,

	mb.display_name as comm_modified_by_name,
	mb.email as comm_modified_by_email

   FROM dim_communication
	 INNER JOIN view_case ON dim_communication.case_id = view_case.case_id
     LEFT JOIN dim_lookup lookup_communication_type ON dim_communication.communication_type = lookup_communication_type.code and lookup_communication_type.type='communication_type'
	 LEFT JOIN dim_lookup lookup_email_replies_type ON dim_communication.email_replies_type = lookup_email_replies_type.code and lookup_email_replies_type.type='email_replies_type'
     LEFT JOIN view_attm on dim_communication.attachment_id = view_attm.attm_id 
	 LEFT JOIN view_todo on dim_communication.to_do_item_id = view_todo.todo_id 
	 LEFT JOIN dim_users cb ON dim_communication.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_communication.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_communication.ver_is_current = true
     AND dim_communication.is_deleted = false;

ALTER VIEW public.view_comm
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_comm TO postgres;
GRANT SELECT ON TABLE public.view_comm TO absence_tracker;
GRANT ALL ON TABLE public.view_comm TO etldw;
-- View: public.view_comr


--DROP VIEW IF EXISTS public.view_comr CASCADE;

CREATE OR REPLACE VIEW public.view_comr AS
 SELECT
	view_comm.*,
	dim_communication_recipient.ext_ref_id as comr_Id ,	
	lookup_recipient_type.description as comr_recipient_type,	
	dim_communication_recipient.address_id	as comr_address_id ,
	dim_communication_recipient.address_address1 as comr_address_address1 ,
	dim_communication_recipient.address_city	as comr_address_city ,
	dim_communication_recipient.address_country	as comr_address_country ,
	dim_communication_recipient.address_postal_code	as comr_address_postal_code ,
	dim_communication_recipient.address_state	as comr_address_state ,
	dim_communication_recipient.alt_email	as comr_alt_email ,
	dim_communication_recipient.email	as comr_email ,
	dim_communication_recipient.fax	as comr_fax ,
	dim_communication_recipient.title as comr_title ,
	dim_communication_recipient.first_name	as comr_first_name ,
	dim_communication_recipient.middle_name as comr_middle_name ,
	dim_communication_recipient.last_name	as comr_last_name ,
	dim_communication_recipient.home_email	as comr_home_email ,
	dim_communication_recipient.company_name as comr_company_name ,
	dim_communication_recipient.contact_person_name as comr_contact_person_name ,
	dim_communication_recipient.date_of_birth  as comr_date_of_birth ,
	dim_communication_recipient.home_phone as comr_home_phone ,
	dim_communication_recipient.cell_phone as comr_cell_phone ,
	dim_communication_recipient.is_primary as comr_is_primary 
	
   FROM dim_communication_recipient
	 LEFT JOIN view_comm ON dim_communication_recipient.dim_communication_id = view_comm.view_communication_id
     LEFT JOIN dim_lookup lookup_recipient_type ON dim_communication_recipient.recipient_type = lookup_recipient_type.code and lookup_recipient_type.type='recipient_type';

ALTER VIEW public.view_comr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_comr TO postgres;
GRANT SELECT ON TABLE public.view_comr TO absence_tracker;
GRANT ALL ON TABLE public.view_comr TO etldw;
-- View: public.view_prwk

--DROP VIEW IF EXISTS public.view_prwk CASCADE;

CREATE OR REPLACE VIEW public.view_prwk AS
 SELECT
	view_comm.*,
	dim_communication_paperwork.ext_ref_id as prwk_id,
	dim_communication_paperwork.checked	 as prwk_checked ,
	dim_communication_paperwork.due_date as prwk_due_date ,
	lookup_paperwork_review_status.description as prwk_status,
	dim_communication_paperwork.status_date as prwk_status_date,	
	dim_communication_paperwork.paperwork_id as prwk_paperwork_id ,
	dim_communication_paperwork.paperwork_cdt as prwk_paperwork_cdt ,
	dim_communication_paperwork.paperwork_code as prwk_paperwork_code ,
	dim_communication_paperwork.paperwork_description as prwk_paperwork_description ,
	lookup_paperwork_doc_type.description as prwk_paperwork_doc_type,
	dim_communication_paperwork.paperwork_enclosure_text	as prwk_paperwork_enclosure_text ,
	dim_communication_paperwork.paperwork_file_id	as prwk_paperwork_file_id ,
	dim_communication_paperwork.paperwork_file_name	as prwk_paperwork_file_name ,
	dim_communication_paperwork.paperwork_mdt	as prwk_paperwork_mdt ,
	dim_communication_paperwork.paperwork_name	as prwk_paperwork_name ,
	dim_communication_paperwork.paperwork_requires_review	as prwk_paperwork_requires_review ,
	dim_communication_paperwork.paperwork_return_date_adjustment as prwk_paperwork_return_date_adjustment,	
	dim_communication_paperwork.paperwork_return_date_adjustment_days	as prwk_paperwork_return_date_adjustment_days ,	
	 
/*attachment fields */
	prwk_attm.attm_content_length as prwk_attm_content_length,
	prwk_attm.attm_content_type as prwk_attm_content_type,
	prwk_attm.attm_attachment_type as prwk_attm_attachment_type,
	prwk_attm.attm_created_by_name as prwk_attm_created_by_name,
	prwk_attm.attm_description as prwk_attm_description,
	prwk_attm.attm_file_id as prwk_attm_file_id,
	prwk_attm.attm_file_Name as prwk_attm_file_Name,
	prwk_attm.attm_public as prwk_attm_public ,

	/*return attachment fields */
	prwk_rtn_attm.attm_content_length as prwk_rtn_attm_content_length,
	prwk_rtn_attm.attm_content_type as prwk_rtn_attm_content_type,
	prwk_rtn_attm.attm_attachment_type as prwk_rtn_attm_attachment_type,
	prwk_rtn_attm.attm_created_by_name as prwk_rtn_attm_created_by_name,
	prwk_rtn_attm.attm_description as prwk_rtn_attm_description,
	prwk_rtn_attm.attm_file_id as prwk_rtn_attm_file_id,
	prwk_rtn_attm.attm_file_Name as prwk_rtn_attm_file_Name,
	prwk_rtn_attm.attm_public as prwk_rtn_attm_public ,
 
	cb.display_name as prwk_created_by_name,
	cb.email as prwk_created_by_email,
	mb.display_name as prwk_modified_by_name,
	mb.email as prwk_modified_by_email,

	prwk_cb.display_name as prwk_paperwork_created_by_name,
	prwk_cb.email as prwk_paperwork_created_by_email,
	prwk_mb.display_name as prwk_paperwork_modified_by_name,
	prwk_mb.email as prwk_paperwork_modified_by_email

   FROM dim_communication_paperwork
	 LEFT JOIN dim_lookup lookup_paperwork_doc_type ON dim_communication_paperwork.paperwork_doc_type = lookup_paperwork_doc_type.code and lookup_paperwork_doc_type.type='document_type'
	 LEFT JOIN dim_lookup lookup_paperwork_review_status ON dim_communication_paperwork.status = lookup_paperwork_review_status.code and lookup_paperwork_review_status.type='paperwork_review_status'
     LEFT JOIN view_comm on dim_communication_paperwork.communication_id = view_comm.comm_id 
	 LEFT JOIN view_attm prwk_attm on dim_communication_paperwork.attachment_id = prwk_attm.attm_id 
	 LEFT JOIN view_attm prwk_rtn_attm on dim_communication_paperwork.return_attachment_id = prwk_rtn_attm.attm_id 
	 LEFT JOIN dim_users cb ON dim_communication_paperwork.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_communication_paperwork.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
	 LEFT JOIN dim_users prwk_cb ON dim_communication_paperwork.paperwork_cby = prwk_cb.ext_ref_id AND prwk_cb.ver_is_current = true
	 LEFT JOIN dim_users prwk_mb ON dim_communication_paperwork.paperwork_mby = prwk_mb.ext_ref_id AND prwk_mb.ver_is_current = true
  WHERE dim_communication_paperwork.ver_is_current = true
     AND dim_communication_paperwork.is_deleted = false;

ALTER VIEW public.view_prwk
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_prwk TO postgres;
GRANT SELECT ON TABLE public.view_prwk TO absence_tracker;
GRANT ALL ON TABLE public.view_prwk TO etldw;

-- View: public.view_cpwf

--DROP VIEW IF EXISTS public.view_cpwf CASCADE;

CREATE OR REPLACE VIEW public.view_cpwf AS
SELECT 
	 view_prwk.*
	,cpwf.ext_ref_id as cpwf_id
    ,cpwf.name as cpwf_name
    ,communication_paperwork_field_status.description as cpwf_description
    ,cpwf.type as cpwf_type
	,cpwf.status as cpwf_status
    ,cpwf.required as cpwf_required
    ,cpwf.field_order as cpwf_field_order
    ,cpwf.value as cpwf_dim_value
    ,cpwf.created_date as cpwf_created_date
    ,cpwf.record_hash as cpwf_record_hash
FROM public.dim_communication_paperwork_field cpwf
LEFT JOIN dim_lookup communication_paperwork_field_status ON cpwf.status = communication_paperwork_field_status.code AND communication_paperwork_field_status.type = 'paperwork_field_status'
LEFT JOIN view_prwk ON cpwf.ext_ref_id = view_prwk.prwk_id
WHERE cpwf.is_deleted = false;

ALTER VIEW public.view_cpwf
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cpwf TO postgres;
GRANT SELECT ON TABLE public.view_cpwf TO absence_tracker;
GRANT ALL ON TABLE public.view_cpwf TO etldw;
-- View: public.view_cuso

--DROP VIEW IF EXISTS public.view_cuso CASCADE;

CREATE OR REPLACE VIEW public.view_cuso AS
SELECT
	view_cust.*
	,cso.dim_customer_service_option_id as view_customer_service_option_id
    ,cso.ext_ref_id as cuso_id
    ,cso.ext_created_date as cuso_created_date
    ,cso.ext_modified_date as cuso_modified_date        
    ,cso.key as cuso_key
    ,cso.value as cuso_value
    ,cso."order" as cuso_order

	,cb.display_name as cuso_created_by_name
	,cb.email as cuso_created_by_email
	,mb.display_name as cuso_modified_by_name
	,mb.email as cuso_modified_by_email

FROM public.dim_customer_service_option cso
LEFT JOIN dim_users cb ON cso.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON cso.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON cso.customer_id = view_cust.cust_id
 WHERE cso.ver_is_current = true
     AND cso.is_deleted = false;

ALTER VIEW public.view_cuso
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cuso TO postgres;
GRANT SELECT ON TABLE public.view_cuso TO absence_tracker;
GRANT ALL ON TABLE public.view_cuso TO etldw;
-- View: public.view_demt

DROP VIEW IF EXISTS public.view_demt CASCADE;

CREATE OR REPLACE VIEW public.view_demt AS
SELECT 
	view_cust.*
	,dt.dim_demand_type_id as view_demand_type_id
    ,dt.ext_ref_id as demt_id    
    ,dt.ext_modified_date as demt_modified_date
    ,dt.created_date as demt_created_date   
    ,dt.description as demt_description
    ,dt.type_data as demt_type_data
    ,dt.order_data as demt_order_data
    ,dt.requirement_label as demt_requirement_label
    ,dt.restriction_label as demt_restriction_label
    ,dt.code as demt_code    
  
	,cb.display_name as demt_created_by_name
	,cb.email as demt_created_by_email
	,mb.display_name as demt_modified_by_name
	,mb.email as demt_modified_by_email

FROM public.dim_demand_type dt
LEFT JOIN dim_users cb ON dt.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON dt.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON dt.customer_id = view_cust.cust_id
 WHERE dt.ver_is_current = true
     AND dt.is_deleted = false;

ALTER VIEW public.view_demt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_demt TO postgres;
GRANT SELECT ON TABLE public.view_demt TO absence_tracker;
GRANT ALL ON TABLE public.view_demt TO etldw;
-- View: public.view_emct

--DROP VIEW IF EXISTS public.view_emct CASCADE;

CREATE OR REPLACE VIEW public.view_emct AS
 SELECT 
	view_eplr.*
    ,ect.dim_employer_contact_types_id as view_employer_contact_types_id
	,ect.ext_ref_id  as emct_id
    ,ect.ext_created_date  as emct_created_date
    ,ect.ext_modified_date  as emct_modified_date    
	,ect.name as emct_name
	,ect.code as emct_code
	
	,cb.display_name as emct_created_by_name
	,cb.email as emct_created_by_email
	,mb.display_name as emct_modified_by_name
	,mb.email as emct_modified_by_email

FROM dim_employer_contact_types ect
LEFT JOIN dim_users cb ON ect.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ect.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_eplr ON ect.customer_id = view_eplr.cust_id
WHERE ect.ver_is_current = true AND ect.is_deleted = false;

ALTER VIEW public.view_emct
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emct TO etldw;
GRANT SELECT ON TABLE public.view_emct TO absence_tracker;
GRANT ALL ON TABLE public.view_emct TO postgres;
-- View: public.view_empc

DROP VIEW IF EXISTS public.view_empc CASCADE;

CREATE OR REPLACE VIEW public.view_empc AS
SELECT 
view_empl.*
	,ec.dim_employee_contact_id as view_employee_contact_id
    ,ec.ext_ref_id as empc_id
    ,ec.ext_created_date as empc_created_date
    ,ec.ext_modified_date as empc_modified_date
    ,ec.contact_type_code as empc_contact_type_code
    ,ec.contact_type_name as empc_contact_type_name
    ,ec.company_name as empc_company_name
    ,ec.title as empc_title
    ,ec.first_name as empc_first_name
    ,ec.middle_name as empc_middle_name
    ,ec.last_name as empc_last_name
    ,ec.dob as empc_dob
    ,ec.email as empc_email
    ,ec.alt_email as empc_alt_email
    ,ec.work_phone as empc_work_phone
    ,ec.home_phone as empc_home_phone
    ,ec.cell_phone as empc_cell_phone
    ,ec.fax as empc_fax
    ,ec.street as empc_street
    ,ec.address1 as empc_contact_address1
    ,ec.address2 as empc_contact_address2
    ,ec.city as empc_city
    ,ec.state as empc_state
    ,ec.postal_code as empc_postal_code
    ,ec.country as empc_country
    ,ec.is_primary as empc_is_primary
    ,ec.contact_person_name as empc_contact_person_name
    ,ec.years_of_age as empc_years_of_age
    ,le.description as empc_description
    ,ec.contact_position as empc_contact_position
    ,ec.employee_number as empc_employee_number        
	,ec.military_status as empc_military_status
	,cb.display_name as empc_created_by_name
	,cb.email as empc_created_by_email
	,mb.display_name as empc_modified_by_name
	,mb.email as empc_modified_by_email
FROM public.dim_employee_contact ec
LEFT JOIN dim_users cb ON ec.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ec.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (ec.employee_id = view_empl.empl_id and ec.customer_id = view_empl.cust_id and ec.employer_id = view_empl.eplr_id)
LEFT JOIN dim_lookup_empl_military_status le ON ec.military_status = le.code
WHERE ec.ver_is_current = true AND ec.is_deleted = false;


ALTER VIEW public.view_empc
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empc TO postgres;
GRANT SELECT ON TABLE public.view_empc TO absence_tracker;
GRANT ALL ON TABLE public.view_empc TO etldw;
-- View: public.view_empj

--DROP VIEW IF EXISTS public.view_empj CASCADE;

CREATE OR REPLACE VIEW public.view_empj AS
SELECT
	view_empl.*
	,ej.dim_employee_job_id as view_employee_job_id
    ,ej.ext_ref_id as empj_id
    ,ej.ext_created_date as empj_created_date
    ,ej.ext_modified_date as empj_modified_date
    ,ej.job_name as empj_job_name
    ,ej.job_code as empj_job_code    
    ,ej.status_reason as empj_status_reason
    ,ej.status_comments as empj_status_comments
    ,ej.status_username as empj_status_username
    ,ej.status_userid as empj_status_userid
    ,ej.status_date as empj_status_date
    ,ej.start_date as empj_start_date
    ,ej.end_date as empj_end_date
	,empl_job_adjudication_status.description as empj_description

	,cb.display_name as empj_created_by_name
	,cb.email as empj_created_by_email
	,mb.display_name as empj_modified_by_name
	,mb.email as empj_modified_by_email

FROM public.dim_employee_job ej
LEFT JOIN dim_users cb ON ej.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ej.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (ej.employee_id = view_empl.empl_id and ej.customer_id = view_empl.cust_id and ej.employer_id = view_empl.eplr_id)
LEFT JOIN dim_lookup empl_job_adjudication_status ON ej.status_value = empl_job_adjudication_status.code
WHERE ej.ver_is_current = TRUE AND ej.is_deleted = FALSE;

ALTER VIEW public.view_empj
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empj TO postgres;
GRANT SELECT ON TABLE public.view_empj TO absence_tracker;
GRANT ALL ON TABLE public.view_empj TO etldw;
-- View: public.view_empn

--DROP VIEW IF EXISTS public.view_empn CASCADE;

CREATE OR REPLACE VIEW public.view_empn AS
SELECT 
	view_empl.*
	,en.dim_employee_necessity_id as view_employee_necessity_id
    ,en.ext_ref_id as empn_id
    ,en.ext_created_date as empn_created_date
    ,en.ext_modified_date as empn_modified_date   
    ,en.name as empn_name
    ,empl_necessity_type.description as empn_necessity_type
    ,en.description as empn_description
    ,en.start_date as empn_start_date
    ,en.end_date as empn_end_date
    ,en.case_id as empn_case_id
    ,en.case_number as empn_case_number
	,cb.display_name as empn_created_by_name
	,cb.email as empn_created_by_email
	,mb.display_name as empn_modified_by_name
	,mb.email as empn_modified_by_email
FROM public.dim_employee_necessity en
LEFT JOIN dim_users cb ON en.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON en.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (en.employee_id = view_empl.empl_id and en.customer_id = view_empl.cust_id and en.employer_id = view_empl.eplr_id)
INNER JOIN dim_lookup empl_necessity_type ON empl_necessity_type.type = 'empl_necessity_type' and en.type = empl_necessity_type.code
WHERE en.ver_is_current = true and en.is_deleted = false;

ALTER VIEW public.view_empn
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empn TO postgres;
GRANT SELECT ON TABLE public.view_empn TO absence_tracker;
GRANT ALL ON TABLE public.view_empn TO etldw;
-- View: public.view_empo

DROP VIEW IF EXISTS public.view_empo CASCADE;

CREATE OR REPLACE VIEW public.view_empo AS
SELECT 
     view_eplr.*
	,eo.dim_employee_organization_id as view_employee_organization_id
    ,eo.ext_ref_id as empo_id
    ,eo.ext_created_date as empo_created_date
    ,eo.ext_modified_date as empo_modified_date   
    ,eo.employee_number as empo_employee_number
    ,eo.code as empo_code
    ,eo.name as empo_name
    ,eo.description as empo_description
    ,eo.path as empo_path
    ,eo.type_code as empo_type_code    
    
	,cb.display_name as empo_created_by_name
	,cb.email as empo_created_by_email
	,mb.display_name as empo_modified_by_name
	,mb.email as empo_modified_by_email

FROM public.dim_employee_organization eo
LEFT JOIN dim_users cb ON eo.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON eo.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_eplr ON (eo.customer_id = view_eplr.cust_id and eo.employer_id = view_eplr.eplr_id)
where eo.ver_is_current = true AND eo.is_deleted = false;
ALTER VIEW public.view_empo
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empo TO postgres;
GRANT SELECT ON TABLE public.view_empo TO absence_tracker;
GRANT ALL ON TABLE public.view_empo TO etldw;
-- View: public.view_emre

--DROP VIEW IF EXISTS public.view_emre CASCADE;

CREATE OR REPLACE VIEW public.view_emre AS
SELECT 
	view_wkrs.*
	,ere.dim_employee_restriction_entry_id as view_employee_restriction_entry_id    
    ,ere.wkrs_restriction_type as emre_wkrs_restriction_type
    ,ere.wkrs_restriction as emre_wkrs_restriction

	,view_demt.demt_id    
	,view_demt.demt_modified_date
	,view_demt.demt_created_date   
	,view_demt.demt_description
	,view_demt.demt_type_data
	,view_demt.demt_order_data
	,view_demt.demt_requirement_label
	,view_demt.demt_restriction_label
	,view_demt.demt_code    
	,view_demt.demt_created_by_name
	,view_demt.demt_created_by_email
	,view_demt.demt_modified_by_name
	,view_demt.demt_modified_by_email
    
FROM public.dim_employee_restriction_entry ere
LEFT JOIN view_wkrs ON ere.dim_employee_restriction_id = view_wkrs.view_employee_restriction_id
LEFT JOIN view_demt on ere.ext_ref_demand_type_id = view_demt.demt_id
WHERE ere.is_deleted = FALSE and ere.ver_is_current = true;

ALTER VIEW public.view_emre
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emre TO postgres;
GRANT SELECT ON TABLE public.view_emre TO absence_tracker;
GRANT ALL ON TABLE public.view_emre TO etldw;
-- View: public.view_emso

--DROP VIEW IF EXISTS public.view_emso CASCADE;

CREATE OR REPLACE VIEW public.view_emso AS
SELECT 
     view_eplr.*
	,eso.dim_employer_service_option_id as view_employer_service_option_id
	,eso.ext_ref_id as emso_id
	,eso.ext_created_date as emso_created_date
	,eso.ext_modified_date as emso_modified_date
	,eso.key as emso_key
	,eso.value as emso_value,	

	cso.cuso_id,
	cso.cuso_created_date,
	cso.cuso_modified_date,        
	cso.cuso_key,
	cso.cuso_value,
	cso.cuso_order,
	cso.cuso_created_by_name,
	cso.cuso_created_by_email,
	cso.cuso_modified_by_name,
	cso.cuso_modified_by_email

	,cb.display_name AS emso_created_by_name
	,cb.email AS emso_created_by_email
	,mb.display_name AS emso_modified_by_name
	,mb.email AS emso_modified_by_email

FROM public.dim_employer_service_option eso
LEFT JOIN dim_users cb ON eso.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON eso.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cuso cso ON eso.cusomerserviceoptionid = cso.cuso_id
LEFT JOIN view_eplr ON (eso.customer_id = view_eplr.cust_id and eso.employer_id = view_eplr.eplr_id)
WHERE eso.ver_is_current = true AND eso.is_deleted = false;

ALTER VIEW public.view_emso
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emso TO postgres;
GRANT SELECT ON TABLE public.view_emso TO absence_tracker;
GRANT ALL ON TABLE public.view_emso TO etldw;
-- View: public.view_enot

--DROP VIEW IF EXISTS public.view_enot CASCADE;

CREATE OR REPLACE VIEW public.view_enot AS
SELECT 
	view_empl.*
	,en.dim_employee_note_id as view_employee_note_id
    ,en.ext_ref_id as enot_id
    ,en.ext_created_date as enot_created_date
    ,en.ext_modified_date as enot_modified_date  
    ,en.note as enot_note
    ,en.is_public as enot_is_public
    ,en.category_code as enot_category_code
    ,en.category_name as enot_category_name

	,cb.display_name AS enot_created_by_name
	,cb.email AS enot_created_by_email
	,mb.display_name AS enot_modified_by_name
	,mb.email AS enot_modified_by_email

FROM public.dim_employee_note en
LEFT JOIN dim_users cb ON en.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON en.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (en.employee_id = view_empl.empl_id and en.customer_id =view_empl.cust_id and en.employer_id = view_empl.eplr_id)
WHERE en.ver_is_current = true;

ALTER VIEW public.view_enot
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_enot TO postgres;
GRANT SELECT ON TABLE public.view_enot TO absence_tracker;
GRANT ALL ON TABLE public.view_enot TO etldw;
-- View: public.view_evdt


--DROP VIEW IF EXISTS public.view_evdt CASCADE;

CREATE OR REPLACE VIEW public.view_evdt AS
 SELECT
	view_case.*,
	dim_lookup.description AS evdt_event_type,
	dim_case_event_date.event_date AS evdt_event_date,
	cb.display_name AS evdt_created_by_name,
	cb.email AS evdt_created_by_email,
	mb.display_name AS evdt_modified_by_name,
	mb.email AS evdt_modified_by_email

   FROM dim_case_event_date
	 INNER JOIN view_case ON dim_case_event_date.case_id = view_case.case_id
	 LEFT JOIN dim_lookup  ON dim_case_event_date.event_type = dim_lookup.code AND dim_lookup.type = 'case_event_type'
	 LEFT JOIN dim_users cb ON dim_case_event_date.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_event_date.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_event_date.ver_is_current = true
     AND dim_case_event_date.is_deleted = false;

ALTER VIEW public.view_evdt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_evdt TO postgres;
GRANT SELECT ON TABLE public.view_evdt TO absence_tracker;
GRANT ALL ON TABLE public.view_evdt TO etldw;
-- View: public.view_todo

--DROP VIEW IF EXISTS public.view_todo CASCADE;

CREATE OR REPLACE VIEW public.view_todo AS
 SELECT 
	view_case.*,
    dim_todoitem.ext_ref_id AS todo_id,
    dim_lookup_todoitem_priority.description AS todo_priority,
	CASE
        WHEN dim_todoitem.status = 1 THEN 'Complete'::text
        WHEN dim_todoitem.status = 3 THEN 'Cancelled'::text
		WHEN (dim_todoitem.due_date::timestamp::date < utc()::timestamp::date) THEN 'Overdue'::text
        ELSE 'Pending'::text
    END AS todo_todo_status,
    dim_lookup_todoitem_types.name AS todo_todo_type,
    dim_lookup_todoitem_types.category AS todo_category,
    dim_lookup_todoitem_types.feature AS todo_feature,
    dim_todoitem.title AS todo_title,
    dim_todoitem.assigned_to_email AS todo_assigned_to_email,
    dim_todoitem.assigned_to_first_name AS todo_assigned_to_first_name,
    dim_todoitem.assigned_to_last_name AS todo_assigned_to_last_name,
    dim_todoitem.due_date AS todo_due_date,
    dim_todoitem.due_date_change_reason AS todo_due_date_change_reason,
    dim_todoitem.weight AS todo_weight,
    dim_todoitem.result_text AS todo_result,
    dim_todoitem.optional AS todo_is_optional,
    dim_todoitem.hide_until AS todo_hide_until_date,
    dim_todoitem.hide_until IS NULL OR dim_todoitem.hide_until <= utc() AS todo_is_visible,
        CASE
            WHEN dim_todoitem.due_date < ( CASE WHEN dim_todoitem.status IN (1, 3) THEN dim_todoitem.ext_modified_date ELSE utc() END ) THEN date_part('day'::text, utc() - dim_todoitem.due_date::timestamp with time zone)
            ELSE 0::double precision
        END AS todo_days_overdue,
    dim_todoitem.template AS todo_communication,
    dim_todoitem.ext_created_date AS todo_todo_created_date,
    dim_todoitem.ext_modified_date AS todo_todo_last_modified_date,
    dim_todoitem.work_flow_name AS todo_workflow_name,

	cb.display_name AS todo_created_by_name,
	cb.email AS todo_created_by_email,
	mb.display_name AS todo_modified_by_name,
	mb.email AS todo_modified_by_email

   FROM dim_todoitem
	 LEFT JOIN view_case ON dim_todoitem.case_id = view_case.case_id
     LEFT JOIN dim_lookup_todoitem_priority ON dim_todoitem.priority = dim_lookup_todoitem_priority.code
     LEFT JOIN dim_lookup_todoitem_types ON dim_todoitem.item_type = dim_lookup_todoitem_types.code
	 LEFT JOIN dim_users cb ON dim_todoitem.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_todoitem.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_todoitem.ver_is_current = true AND dim_todoitem.is_deleted = false;

ALTER VIEW public.view_todo
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_todo TO postgres;
GRANT SELECT ON TABLE public.view_todo TO absence_tracker;
GRANT ALL ON TABLE public.view_todo TO etldw;
-- View: public.view_evnt


--DROP VIEW IF EXISTS public.view_evnt CASCADE;

CREATE OR REPLACE VIEW public.view_evnt AS
SELECT 
	view_todo.*
	,evt.dim_event_id as view_event_id
    ,evt.ext_ref_id as evnt_id
    ,evt.ext_created_date as evnt_created_date
    ,evt.ext_modified_date as evnt_modified_date   
	,evt.active_accomodation_id as evnt_active_accomodation_id
    ,evt.accomodation_type_code as evnt_accomodation_type_code
    ,evt.accomodation_end_date as evnt_accomodation_end_date
    ,evt.determination as evnt_determination
    ,evt.denial_explanation as evnt_denial_explanation
    ,evt.case_id AS evnt_event_case_id
    ,evt.accomodation_type_id as evnt_accomodation_type_id
    ,evt.template AS evnt_event_template
    ,communication_type.description AS evnt_communication_Desc
    ,evt.name as evnt_name
    ,evt.event_type as evnt_event_type    
    ,evt.applied_policy_id as evnt_applied_policy_id
    ,evt.close_case as evnt_close_case
    ,evt.communication_id as evnt_communication_id
    ,evt.condition_start_date as evnt_condition_start_date
    ,evt.contact_employee_due_date as evnt_contact_employee_due_date
    ,evt.contact_hcp_due_date as evnt_contact_hcp_due_date
    ,evt.denial_reason as evnt_denial_reason
    ,evt.description as evnt_event_description
    ,evt.due_date as evnt_event_due_date
    ,evt.ergonomic_assessment_date as evnt_ergonomic_assessment_date
    ,evt.extended_grace_period as evnt_extended_grace_period
    ,evt.first_exhaustion_date as evnt_first_exhaustion_date
    ,evt.general_health_condition as evnt_general_health_condition
    ,evt.has_work_restriction as evnt_has_work_restriction
    ,evt.illness_injury_date as evnt_illness_injury_date
    ,evt.new_due_date as evnt_new_due_date
    ,evt.paperwork_attachment_id as evnt_paperwork_attachment_id
    ,evt.paperwork_due_date as evnt_paperwork_due_date
    ,evt.paperwork_id as evnt_paperwork_id
    ,evt.paperwork_name as evnt_paperwork_name
    ,evt.paperwork_received as evnt_paperwork_received
    ,evt.policy_code as evnt_policy_code
    ,evt.policy_id as evnt_policy_id
    ,evt.reason as evnt_reason
    ,evt.requires_review as evnt_requires_review
    ,evt.return_attachment_id as evnt_return_attachment_id
    ,evt.return_to_work_date as evnt_return_to_work_date
    ,evt.rtw as evnt_rtw
    ,evt.source_workflow_activity_activityid as evnt_source_workflow_activity_activityid
    ,evt.source_workflow_activityid as evnt_source_workflow_activityid
    ,evt.source_workflow_instanceid as evnt_source_workflow_instanceid
    ,evt.workflow_activity_id as evnt_workflow_activity_id ,

	ac.acom_id,
	ac.acom_dim_accommodation_id,
	ac.acom_accommodation_type,
	ac.acom_accommodation_type_code,
	ac.acom_accommodation_duration,
	ac.acom_accommodation_resolution,
	ac.acom_accommodation_resolved,
	ac.acom_accommodation_resolved_date,
	ac.acom_accommodation_granted,
	ac.acom_accommodation_granted_date,
	ac.acom_accommodation_cost,
	ac.acom_accommodation_start_date,
	ac.acom_accommodation_end_date,
	ac.acom_accommodation_determination,
	ac.acom_accommodation_min_approved_date,
	ac.acom_accommodation_max_approved_date,
	ac.acom_accommodation_created_date,
	ac.acom_accommodation_status,
	ac.acom_accommodation_cancel_reason,
	ac.acom_accommodation_is_work_related,
	ac.acom_created_by_name,
	ac.acom_created_by_email,
	ac.acom_modified_by_name,
	ac.acom_modified_by_email,

	at.attm_id,
	at.attm_content_length,
	at.attm_content_type,
	at.attm_attachment_type,
	at.attm_created_by_name,
	at.attm_description,
	at.attm_file_id,
	at.attm_file_Name,
	at.attm_public,    
	at.attm_created_by_email,
	at.attm_modified_by_name,
	at.attm_modified_by_email,
   
	co.comm_id,
	co.comm_body ,
	co.comm_communication_type, 
	co.comm_email_replies_type,
	co.comm_created_by_email,
	co.comm_created_by_name,
	co.comm_is_draft,
	co.comm_name,
	co.comm_public,
	co.comm_first_send_date,
	co.comm_last_send_date,	
	co.comm_subject,
	co.comm_template,
	co.comm_created_date,
	co.comm_modified_by_name,
	co.comm_modified_by_email,

	pw.prwk_id,
	pw.prwk_checked ,
	pw.prwk_due_date ,
	pw.prwk_status,
	pw.prwk_status_date,	
	pw.prwk_paperwork_id ,
	pw.prwk_paperwork_cdt ,
	pw.prwk_paperwork_code ,
	pw.prwk_paperwork_description ,
	pw.prwk_paperwork_doc_type,
	pw.prwk_paperwork_enclosure_text ,
	pw.prwk_paperwork_file_id ,
	pw.prwk_paperwork_file_name ,
	pw.prwk_paperwork_mdt ,
	pw.prwk_paperwork_name ,
	pw.prwk_paperwork_requires_review ,
	pw.prwk_paperwork_return_date_adjustment,	
	pw.prwk_paperwork_return_date_adjustment_days 	
	 
   ,cu.display_name AS evnt_created_by_name
	,cu.email AS evnt_created_by_email
	,mu.display_name AS evnt_modified_by_name
	,mu.email AS evnt_modified_by_email

FROM public.dim_event evt

LEFT JOIN dim_users cu ON evt.ext_created_by = cu.ext_ref_id AND cu.ver_is_current = true
LEFT JOIN dim_users mu ON evt.ext_modified_by = mu.ext_ref_id AND mu.ver_is_current = true
LEFT JOIN view_todo ON evt.todo_item_id = view_todo.todo_id
LEFT JOIN view_acom ac ON evt.accomodation_id = ac.acom_id
LEFT JOIN view_attm at ON evt.attachment_id = at.attm_id
LEFT JOIN view_comm co ON evt.communication_id=co.comm_id
LEFT JOIN view_prwk pw ON evt.paperwork_id=pw.prwk_id
LEFT JOIN dim_lookup communication_type ON evt.communication_type = communication_type.code
WHERE communication_type.type = 'communication_type' and evt.ver_is_current = true and evt.is_deleted = false;

ALTER VIEW public.view_evnt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_evnt TO postgres;
GRANT SELECT ON TABLE public.view_evnt TO absence_tracker;
GRANT ALL ON TABLE public.view_evnt TO etldw;
-- View: public.view_oain

DROP VIEW IF EXISTS public.view_oain CASCADE;

CREATE OR REPLACE VIEW public.view_oain AS
SELECT
	view_org.*
	,oa.dim_organization_annual_info_id as view_organization_annual_info_id
    ,oa.ext_ref_id as oain_id
    ,oa.ext_created_date as oain_created_date
    ,oa.ext_modified_date as oain_modified_date  
    ,oa.org_code as oain_org_code
    ,oa.org_year as oain_org_year
    ,oa.average_employee_count as oain_average_employee_count
    ,oa.total_hours_worked as oain_total_hours_worked
    ,oa.created_date as oain_organization_created_Date
  
 ,	cb.display_name AS oain_created_by_name
	,cb.email AS oain_created_by_email
	,mb.display_name AS oain_modified_by_name
	,mb.email AS oain_modified_by_email

FROM public.dim_organization_annual_info oa
LEFT JOIN dim_users cb ON oa.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON oa.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_org ON  view_org.cust_id = oa.customer_id
WHERE oa.ver_is_current = true AND oa.is_deleted = false;

ALTER VIEW public.view_oain
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_oain TO postgres;
GRANT SELECT ON TABLE public.view_oain TO absence_tracker;
GRANT ALL ON TABLE public.view_oain TO etldw;

-- View: public.view_payp

--DROP VIEW IF EXISTS public.view_payp CASCADE;

CREATE OR REPLACE VIEW public.view_payp AS
 SELECT
	view_case.*,
	dim_case_pay_period.dim_case_pay_period_id as view_case_pay_period_Id,
	dim_case_pay_period.ext_ref_id as payp_id,
	dim_case_pay_period.start_date AS payp_start_date,
	dim_case_pay_period.end_date AS payp_end_date,
	lookup_status_type.description AS payp_status,
	dim_case_pay_period.locked_date AS payp_locked_date,	
	lb.display_name AS payp_locked_by_name,
	dim_case_pay_period.max_weekly_pay_amount  AS payp_max_weekly_pay_amount,
	dim_case_pay_period.payroll_date_override AS payp_payroll_date_override,
	dim_case_pay_period.end_date_override AS payp_end_date_override,
	dim_case_pay_period.payroll_date AS payp_payroll_date,	
	pdob.display_name AS payp_payroll_date_override_by_name,
	edob.display_name AS payp_end_date_override_by_name,
	dim_case_pay_period.offset_amount  AS payp_offset_amount,
	dim_case_pay_period.total  AS payp_total,
	dim_case_pay_period.sub_total AS payp_sub_total,
	dim_case_pay_period.payroll_date_override_by AS payp_payroll_date_override_by,
	dim_case_pay_period.end_date_override_by AS payp_end_date_override_by,
	dim_case_pay_period.locked_by AS payp_locked_by,
	cb.display_name AS payp_created_by_name,
	cb.email AS payp_created_by_email,
	mb.display_name AS payp_modified_by_name,
	mb.email AS payp_modified_by_email

   FROM dim_case_pay_period
	 LEFT JOIN view_case ON dim_case_pay_period.case_id = view_case.case_id
	 LEFT JOIN dim_lookup lookup_status_type ON dim_case_pay_period.status = lookup_status_type.code AND lookup_status_type.type = 'payroll_status'
	 LEFT JOIN dim_users lb ON dim_case_pay_period.locked_by = lb.ext_ref_id AND lb.ver_is_current = true
	 LEFT JOIN dim_users pdob ON dim_case_pay_period.end_date_override_by = pdob.ext_ref_id AND pdob.ver_is_current = true
	 LEFT JOIN dim_users edob ON dim_case_pay_period.payroll_date_override_by = edob.ext_ref_id AND edob.ver_is_current = true	 
	 LEFT JOIN dim_users cb ON dim_case_pay_period.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_pay_period.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_pay_period.ver_is_current = true
     AND dim_case_pay_period.is_deleted = false;

ALTER VIEW public.view_payp
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_payp TO postgres;
GRANT SELECT ON TABLE public.view_payp TO absence_tracker;
GRANT ALL ON TABLE public.view_payp TO etldw;
-- View: public.view_poly


--DROP VIEW IF EXISTS public.view_poly CASCADE; 

CREATE OR REPLACE VIEW public.view_poly AS
	
	SELECT 
	  view_case.*,
	  dim_case_policy.dim_case_policy_id AS view_poly_id, 
	  dim_case_policy.policy_code AS poly_code, 
	  dim_case_policy.policy_name AS poly_name, 
	  dim_case_policy.start_date AS poly_start_date, 
	  dim_case_policy.end_date AS poly_end_date, 
	  dim_lookup_case_types.description AS poly_case_type, 
	  eligibility.description AS poly_eligibility_status, 
	  determination.description AS poly_determination, 
	  denial_reason.description AS poly_denial_reason, 
	  dim_case_policy.denial_reason_other AS poly_other_denial_reason, 
	  dim_case_policy.payout_percentage AS poly_payout_percentage,
	  dim_case_policy.crosswalk_codes AS poly_crosswalk_codes,	 
	  dim_case_policy.actual_duration AS poly_actual_duration,
	  dim_case_policy.exceeded_duration AS poly_exceeded_duration,
	  dim_case_policy.denial_reason_code AS poly_denial_reason_code, 
	  dim_case_policy.denial_reason_description AS poly_denial_reason_description
	FROM 
	public.dim_case_policy
	
	LEFT JOIN view_case ON dim_case_policy.case_id = view_case.case_id
	LEFT JOIN dim_lookup_case_types ON dim_case_policy.case_type = dim_lookup_case_types.code
	LEFT JOIN dim_lookup eligibility ON dim_case_policy.eligibility = eligibility.code AND eligibility.type = 'eligibility_status'
	LEFT JOIN dim_lookup_case_determination determination ON dim_case_policy.determination = determination.code
	LEFT JOIN dim_lookup denial_reason ON dim_case_policy.denial_reason = denial_reason.code AND denial_reason.type = 'case_denial_reason'

	WHERE dim_case_policy.ver_is_current = true
	AND dim_case_policy.is_deleted = false;



ALTER VIEW public.view_poly OWNER TO postgres;

GRANT ALL ON TABLE public.view_poly TO postgres;
GRANT SELECT ON TABLE public.view_poly TO absence_tracker;
GRANT ALL ON TABLE public.view_poly TO etldw;
-- View: public.view_pypd

--DROP VIEW IF EXISTS public.view_pypd CASCADE;

CREATE OR REPLACE VIEW public.view_pypd AS
 SELECT
	view_payp.*,
	dim_case_pay_period_detail.start_date AS pypd_start_date,
	dim_case_pay_period_detail.end_date AS pypd_end_date,
	dim_case_pay_period_detail.is_Offset AS pypd_is_Offset,
	dim_case_pay_period_detail.max_payment_amount AS pypd_max_payment_amount ,
	dim_case_pay_period_detail.pay_amount_override_value AS pypd_pay_amount_override_value,
	dim_case_pay_period_detail.pay_amount_system_value AS pypd_pay_amount_system_value,
	dim_case_pay_period_detail.payment_tier_percentage AS pypd_payment_tier_percentage,
	ppob.display_name AS pypd_pay_percentage_override_by_name,
	dim_case_pay_period_detail.pay_percentage_override_date AS pypd_pay_percentage_override_date,
	dim_case_pay_period_detail.pay_percentage_override_value AS pypd_pay_percentage_override_value,
	dim_case_pay_period_detail.pay_percentage_system_value AS pypd_pay_percentage_system_value,
	dim_case_pay_period_detail.policy_code AS pypd_policy_code,
	dim_case_pay_period_detail.policy_name AS pypd_policy_name,
	dim_case_pay_period_detail.salary_total AS pypd_salary_total
	
   FROM dim_case_pay_period_detail
	 INNER JOIN view_payp ON dim_case_pay_period_detail.dim_case_pay_period_id = view_payp.view_case_pay_period_Id and dim_case_pay_period_detail.case_id=view_payp.case_id 
	 LEFT JOIN dim_users ppob ON dim_case_pay_period_detail.pay_percentage_override_by = ppob.ext_ref_id AND ppob.ver_is_current = true;
	

ALTER VIEW public.view_pypd
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_pypd TO postgres;
GRANT SELECT ON TABLE public.view_pypd TO absence_tracker;
GRANT ALL ON TABLE public.view_pypd TO etldw;
-- View: public.view_role

--DROP VIEW IF EXISTS public.view_role CASCADE;

CREATE OR REPLACE VIEW public.view_role AS
SELECT 
	view_cust.*
	,r.dim_role_id as view_role_id
    ,r.ext_ref_id as role_id
    ,r.ext_created_date as role_created_date
    ,r.ext_modified_date as role_modified_date   
    ,r.name as role_name
    ,r.permissions as role_permissions
    ,role_type.description as role_role_type

	,cb.display_name AS role_created_by_name
	,cb.email AS role_created_by_email
	,mb.display_name AS role_modified_by_name
	,mb.email AS role_modified_by_email
    
FROM public.dim_role r
LEFT JOIN dim_users cb ON r.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON r.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON r.customer_id = view_cust.cust_id
LEFT JOIN dim_lookup role_type ON r.role_type = role_type.code
WHERE role_type.type = 'role_type' and r.ver_is_current = true AND r.is_deleted = false;

ALTER VIEW public.view_role
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_role TO postgres;
GRANT SELECT ON TABLE public.view_role TO absence_tracker;
GRANT ALL ON TABLE public.view_role TO etldw;
-- View: public.view_team

--DROP VIEW IF EXISTS public.view_team CASCADE;

CREATE OR REPLACE VIEW public.view_team AS
SELECT 
	view_cust.*
	,team.dim_team_id as view_team_id
    ,team.ext_ref_id as team_id
    ,team.ext_created_date as team_ext_created_date
    ,team.ext_modified_date as team_ext_modified_date
    
    ,team.name as team_name
    ,team.description as team_description

	,cb.display_name AS team_created_by_name
	,cb.email AS team_created_by_email
	,mb.display_name AS team_modified_by_name
	,mb.email AS team_modified_by_email

FROM public.dim_team team
LEFT JOIN dim_users cb ON team.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON team.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON team.customer_id = view_cust.cust_id
WHERE team.ver_is_current = true AND team.is_deleted = FALSE;

ALTER VIEW public.view_team
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_team TO postgres;
GRANT SELECT ON TABLE public.view_team TO absence_tracker;
GRANT ALL ON TABLE public.view_team TO etldw;
-- View: public.view_tmem

--DROP VIEW IF EXISTS public.view_tmem CASCADE;

CREATE OR REPLACE VIEW public.view_tmem AS
SELECT
	view_cust.*
	,tm.dim_team_member_id as view_team_member_id
    ,tm.ext_ref_id as tmem_id
    ,tm.ext_created_date as tmem_created_date
    ,tm.ext_modified_date as tmem_modified_date   
    ,tm.user_id as tmem_user_id
    ,tm.team_id as tmem_team_id
    ,tm.is_team_lead as tmem_is_team_lead
    ,tm.assigned_by_id as tmem_assigned_by_id
    ,tm.assigned_on as tmem_assigned_on
    ,tm.modified_by_id as tmem_modified_by_id
    ,tm.modified_on as tmem_modified_on
    
	,cb.display_name AS tmem_created_by_name
	,cb.email AS tmem_created_by_email
	,mb.display_name AS tmem_modified_by_name
	,mb.email AS tmem_modified_by_email

FROM public.dim_team_member tm
LEFT JOIN dim_team t ON tm.team_id::bigint = t.dim_team_id
LEFT JOIN dim_users uid ON tm.user_id::bigint = uid.dim_user_id
LEFT JOIN dim_users cb ON tm.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON tm.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON tm.customer_id = view_cust.cust_id
WHERE tm.ver_is_current = true and tm.is_deleted = FALSE;

ALTER VIEW public.view_tmem
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_tmem TO postgres;
GRANT SELECT ON TABLE public.view_tmem TO absence_tracker;
GRANT ALL ON TABLE public.view_tmem TO etldw;
-- View: public.view_usge   

-- DROP VIEW public.view_usge;
--DROP VIEW IF EXISTS public.view_usge CASCADE;

CREATE OR REPLACE VIEW public.view_usge AS
	SELECT 
	  view_poly.*,
	  dim_case_policy_usage.dim_case_policy_usage_id AS view_usge_id, 
	  dim_case_policy_usage.date_used AS usge_date_used, 
	  dim_case_policy_usage.minutes_used AS usge_minutes_used, 
	  dim_case_policy_usage.minutes_in_day AS usge_minutes_in_day, 
	  dim_case_policy_usage.hours_used AS usge_hours_used, 
	  dim_case_policy_usage.hours_in_day AS usge_hours_in_day, 
	  dim_case_policy_usage.user_entered AS usge_user_entered, 
	  determination.description AS usge_determination, 
	  denial_reason.description AS usge_denial_reason, 
	  dim_case_policy_usage.denial_reason_other AS usge_denial_reason_other, 
	  dim_case_policy_usage.denial_reason_code AS usge_denial_reason_code,
	  dim_case_policy_usage.denial_reason_description AS usge_denial_reason_description,
	  dim_case_policy_usage.determined_by as usge_determined_by_id,
	  concat(empl.first_name, ' ', empl.last_name) AS usge_determined_by, 
	  intermittent_determination.description AS usge_intermittent_determination, 
	  dim_case_policy_usage.is_locked AS usge_is_locked, 
	  dim_case_policy_usage.uses_time AS usge_uses_time, 
	  dim_case_policy_usage.is_intermittent_restriction as usge_is_intermittent_restriction,
	  dim_case_policy_usage.percent_of_day AS usge_percent_of_day, 
	  status.description AS usge_status
	FROM 
	  public.dim_case_policy_usage

	LEFT JOIN view_poly ON dim_case_policy_usage.dim_case_policy_id = view_poly.view_poly_id AND dim_case_policy_usage.policy_code = view_poly.poly_code
	LEFT JOIN dim_lookup_case_determination determination ON dim_case_policy_usage.determination = determination.code
	LEFT JOIN dim_lookup denial_reason ON dim_case_policy_usage.denial_reason = denial_reason.code AND denial_reason.type = 'case_denial_reason'
	LEFT JOIN dim_employee empl ON dim_case_policy_usage.determined_by = empl.ext_ref_id
	LEFT JOIN dim_lookup intermittent_determination ON dim_case_policy_usage.intermittent_determination = intermittent_determination.code AND intermittent_determination.type = 'case_intermittent_status'
	LEFT JOIN dim_lookup_case_determination status ON dim_case_policy_usage.status = status.code

	WHERE dim_case_policy_usage.ver_is_current = true;


ALTER VIEW public.view_usge OWNER TO postgres;

GRANT ALL ON TABLE public.view_usge TO postgres;
GRANT SELECT ON TABLE public.view_usge TO absence_tracker;
GRANT ALL ON TABLE public.view_usge TO etldw;
-- View: public.view_vsch

--DROP VIEW IF EXISTS public.view_vsch CASCADE;

CREATE OR REPLACE VIEW public.view_vsch AS
 SELECT
	view_empl.*,  	
	dim_variable_schedule_time.ext_ref_id as vsch_id,
	dim_variable_schedule_time.time_sample_date as vsch_time_sample_date,
	dim_variable_schedule_time.time_total_minutes as vsch_time_total_minutes,
	dim_variable_schedule_time.employee_number as vsch_employee_number,
	
	cb.display_name as vsch_created_by_name,
	cb.email as vsch_created_by_email,
	mb.display_name as vsch_modified_by_name,
	mb.email as vsch_modified_by_email

   FROM dim_variable_schedule_time
	 INNER JOIN view_empl ON dim_variable_schedule_time.employee_id = view_empl.empl_id     
     LEFT JOIN dim_users cb ON dim_variable_schedule_time.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_variable_schedule_time.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_variable_schedule_time.ver_is_current = true
     AND dim_variable_schedule_time.is_deleted = false;

ALTER VIEW public.view_vsch
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_vsch TO postgres;
GRANT SELECT ON TABLE public.view_vsch TO absence_tracker;
GRANT ALL ON TABLE public.view_vsch TO etldw;
-- View: public.view_wfin

--DROP VIEW IF EXISTS public.view_wfin CASCADE;

CREATE OR REPLACE VIEW public.view_wfin AS
 SELECT
	view_evnt.*,  	
	dim_workflow_instance.ext_ref_id as wfin_id,
	dim_workflow_instance.accommodation_end_date as wfin_accommodation_end_date	 ,
	dim_workflow_instance.accommodation_id as wfin_accommodation_id	,
	dim_workflow_instance.accommodation_type_code as wfin_accommodation_type_code	 ,
	dim_workflow_instance.accommodation_type_id as wfin_accommodation_type_id	 ,
	dim_workflow_instance.active_accommodation_id as wfin_active_accommodation_id,
	dim_workflow_instance.applied_policy_id as wfin_applied_policy_id,
	dim_workflow_instance.attachment_id as wfin_attachment_id	 ,
	dim_workflow_instance.close_case as wfin_close_case	 ,
	dim_workflow_instance.communication_id as wfin_communication_id	 ,
	dim_workflow_instance.communication_type as wfin_communication_type	 ,
	dim_workflow_instance.condition_start_date as wfin_condition_start_date	 ,
	dim_workflow_instance.contact_employee_due_date as wfin_contact_employee_due_date	 ,
	dim_workflow_instance.contact_hcp_due_date as wfin_contact_hcp_due_date	 ,
	dim_workflow_instance.customer_id as wfin_customer_id	 ,
	dim_workflow_instance.denial_explanation as wfin_denial_explanation	 ,
	dim_workflow_instance.denial_reason as wfin_denial_reason	 ,
	dim_workflow_instance.description as wfin_Description	 ,
	dim_workflow_instance.determination as wfin_Determination	 ,
	dim_workflow_instance.due_date as wfin_due_date	 ,
	dim_workflow_instance.ergonomic_assessment_date as wfin_ergonomic_assessment_date	 ,
	dim_workflow_instance.event_id as wfin_event_id	 ,
	dim_workflow_instance.event_type as wfin_event_type	 ,
	dim_workflow_instance.extend_grace_period as wfin_extend_grace_period	 ,
	dim_workflow_instance.first_exhaustion_date as wfin_first_exhaustion_date	 ,
	dim_workflow_instance.general_health_condition as wfin_general_health_condition	 ,
	dim_workflow_instance.has_work_restrictions as wfin_has_work_restrictions	 ,
	dim_workflow_instance.illness_or_injury_date as wfin_illness_or_injury_date	 ,
	dim_workflow_instance.new_due_date	 as wfin_new_due_date,
	dim_workflow_instance.paperwork_attachment_id as wfin_paperwork_attachment_id	 ,
	dim_workflow_instance.paperwork_due_date as wfin_paperwork_due_date	 ,
	dim_workflow_instance.paperwork_id as wfin_paperwork_id ,
	dim_workflow_instance.paperwork_name	as wfin_paperwork_name ,
	dim_workflow_instance.paperwork_received as wfin_paperwork_received	 ,
	dim_workflow_instance.policy_code as wfin_policy_code	 ,
	dim_workflow_instance.policy_id as wfin_policy_id	 ,
	dim_workflow_instance.reason as wfin_Reason	 ,
	dim_workflow_instance.requires_review as wfin_requires_review	 ,
	dim_workflow_instance.return_attachment_id as wfin_return_attachment_id	 ,
	dim_workflow_instance.return_to_work_date as wfin_return_to_work_date	 ,
	dim_workflow_instance.rtw as wfin_rtw	 ,
	dim_workflow_instance.source_workflow_activity_activity_id as wfin_source_workflow_activity_activity_id	 ,
	dim_workflow_instance.source_workflow_activity_id as wfin_source_workflow_activity_id,
	dim_workflow_instance.source_workflow_instance_id	as wfin_source_workflow_instance_id ,
	lookup_workflow_instance_status.description as wfin_Status	 ,
	dim_workflow_instance.template as wfin_Template	 ,
	dim_workflow_instance.workflow_activity_id as wfin_workflow_activity_id ,
	dim_workflow_instance.workflow_id	as wfin_workflow_id ,
	dim_workflow_instance.workflow_name as wfin_workflow_name	 ,

	cb.display_name as wfin_created_by_name,
	cb.email as wfin_created_by_email,
	mb.display_name as wfin_modified_by_name,
	mb.email as wfin_modified_by_email

   FROM dim_workflow_instance
	 LEFT JOIN view_evnt ON dim_workflow_instance.case_id = view_evnt.evnt_event_case_id
     LEFT JOIN dim_lookup lookup_workflow_instance_status ON dim_workflow_instance.status = lookup_workflow_instance_status.code and lookup_workflow_instance_status.type='workflow_instance_status'
     LEFT JOIN dim_users cb ON dim_workflow_instance.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_workflow_instance.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_workflow_instance.ver_is_current = true
     AND dim_workflow_instance.is_deleted = false;

ALTER VIEW public.view_wfin
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_wfin TO postgres;
GRANT SELECT ON TABLE public.view_wfin TO absence_tracker;
GRANT ALL ON TABLE public.view_wfin TO etldw;


/* END SCHEMA */

END
$do$
