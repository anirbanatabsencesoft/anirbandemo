﻿/* Modified the view */
/*
Commenting the create view command as it will be created as part of deployment process which automatically drops and rebuild views

CREATE OR REPLACE VIEW public.view_comm AS
 SELECT
	view_case.*,
	 dim_communication.dim_communication_id as view_communication_id, 
	 dim_communication.ext_ref_id as comm_id,
	 dim_communication.body as comm_body ,
	 lookup_communication_type.description as comm_communication_type, 
	 lookup_email_replies_type.description as comm_email_replies_type,
	 dim_communication.created_by_email as comm_created_by_email,
	 dim_communication.created_by_name as comm_created_by_name,
	 dim_communication.is_draft  as comm_is_draft,
	 dim_communication.name as comm_name,
	 dim_communication.public as comm_public,
	 dim_communication.first_send_date as comm_first_send_date,
	 dim_communication.last_send_date as comm_last_send_date,	
	 dim_communication.subject as comm_subject,
	 dim_communication.template  as comm_template,
	 dim_communication.ext_created_date  as comm_created_date,
	 
 /*attachment fields */
 	dim_attachment.ext_ref_id as attm_id,
	dim_attachment.content_length as attm_content_length,
	dim_attachment.content_type as attm_content_type,
	lookup_attachment_type.description as attm_attachment_type,
	dim_attachment.created_by_name as attm_created_by_name,
	dim_attachment.description as attm_description,
	dim_attachment.file_id as attm_file_id,
	dim_attachment.file_Name as attm_file_Name,
	dim_attachment.public as attm_public,    
		
	cb_attachment.email as attm_created_by_email,
	mb_attachment.display_name as attm_modified_by_name,
	mb_attachment.email as attm_modified_by_email,

 /*todo item fields*/
 	dim_todoitem.ext_ref_id AS todo_id,
    dim_lookup_todoitem_priority.description AS todo_priority,
	CASE
        WHEN dim_todoitem.status = 1 THEN 'Complete'::text
        WHEN dim_todoitem.status = 3 THEN 'Cancelled'::text
		WHEN (dim_todoitem.due_date::timestamp::date < utc()::timestamp::date) THEN 'Overdue'::text
        ELSE 'Pending'::text
    END AS todo_todo_status,
    dim_lookup_todoitem_types.name AS todo_todo_type,
    dim_lookup_todoitem_types.category AS todo_category,
    dim_lookup_todoitem_types.feature AS todo_feature,
    dim_todoitem.title AS todo_title,
    dim_todoitem.assigned_to_email AS todo_assigned_to_email,
    dim_todoitem.assigned_to_first_name AS todo_assigned_to_first_name,
    dim_todoitem.assigned_to_last_name AS todo_assigned_to_last_name,
    dim_todoitem.due_date AS todo_due_date,
    dim_todoitem.due_date_change_reason AS todo_due_date_change_reason,
    dim_todoitem.weight AS todo_weight,
    dim_todoitem.result_text AS todo_result,
    dim_todoitem.optional AS todo_is_optional,
    dim_todoitem.hide_until AS todo_hide_until_date,
        CASE
            WHEN dim_todoitem.due_date < ( CASE WHEN dim_todoitem.status IN (1, 3) THEN dim_todoitem.ext_modified_date ELSE utc() END ) THEN date_part('day'::text, utc() - dim_todoitem.due_date::timestamp with time zone)
            ELSE 0::double precision
        END AS todo_days_overdue,
    dim_todoitem.template AS todo_communication,
    dim_todoitem.ext_created_date AS todo_todo_created_date,
    dim_todoitem.ext_modified_date AS todo_todo_last_modified_date,
    dim_todoitem.work_flow_name AS todo_workflow_name,
	cb_todo.display_name AS todo_created_by_name,
	cb_todo.email AS todo_created_by_email,
	mb_todo.display_name AS todo_modified_by_name,
	mb_todo.email AS todo_modified_by_email,

	mb.display_name as comm_modified_by_name,
	mb.email as comm_modified_by_email

   FROM dim_communication
	 INNER JOIN view_case ON dim_communication.case_id = view_case.case_id
     LEFT JOIN dim_lookup lookup_communication_type ON dim_communication.communication_type = lookup_communication_type.code and lookup_communication_type.type='communication_type'
	 LEFT JOIN dim_lookup lookup_email_replies_type ON dim_communication.email_replies_type = lookup_email_replies_type.code and lookup_email_replies_type.type='email_replies_type'
	 LEFT JOIN dim_users cb ON dim_communication.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_communication.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true

	 LEFT JOIN dim_attachment on dim_communication.attachment_id = dim_attachment.ext_ref_id
	 LEFT JOIN dim_lookup lookup_attachment_type ON dim_attachment.attachment_type = lookup_attachment_type.code and lookup_attachment_type.type='attachment_type'
     LEFT JOIN dim_users cb_attachment ON dim_attachment.ext_created_by = cb_attachment.ext_ref_id AND cb_attachment.ver_is_current = true
	 LEFT JOIN dim_users mb_attachment ON dim_attachment.ext_modified_by = mb_attachment.ext_ref_id AND mb_attachment.ver_is_current = true
	 
	 LEFT JOIN dim_todoitem on dim_communication.to_do_item_id = dim_todoitem.ext_ref_id
	 LEFT JOIN dim_lookup_todoitem_priority ON dim_todoitem.priority = dim_lookup_todoitem_priority.code
     LEFT JOIN dim_lookup_todoitem_types ON dim_todoitem.item_type = dim_lookup_todoitem_types.code
	 LEFT JOIN dim_users cb_todo ON dim_todoitem.ext_created_by = cb_todo.ext_ref_id AND cb_todo.ver_is_current = true
	 LEFT JOIN dim_users mb_todo ON dim_todoitem.ext_modified_by = mb_todo.ext_ref_id AND mb_todo.ver_is_current = true

  WHERE dim_communication.ver_is_current = true
     AND dim_communication.is_deleted = false;

ALTER VIEW public.view_comm
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_comm TO postgres;
GRANT SELECT ON TABLE public.view_comm TO absence_tracker;
GRANT ALL ON TABLE public.view_comm TO etldw;

*/
/* Add indexes */
CREATE INDEX IF NOT EXISTS ix_dim_employee_eplr_id_cust_id
    ON public.dim_employee USING btree
    (employer_id ASC NULLS LAST, customer_id ASC NULLS LAST)
    WITH (FILLFACTOR=70)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS ix_dim_employer_cust_id
    ON public.dim_employer USING btree
    (customer_id ASC NULLS LAST)
    WITH (FILLFACTOR=70)
    TABLESPACE pg_default;
