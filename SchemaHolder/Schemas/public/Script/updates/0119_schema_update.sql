CREATE INDEX IF NOT EXISTS dim_case_policy_cur_ver_ref_id_is_del
    ON public.dim_case_policy USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_policy_caseid_verseq
  on public.dim_case_policy USING btree(case_id, ver_is_current,ver_seq)
  TABLESPACE pg_default;

 