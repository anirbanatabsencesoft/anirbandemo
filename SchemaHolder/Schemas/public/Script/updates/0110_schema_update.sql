DO
$do$
BEGIN

DROP VIEW IF EXISTS view_user cascade;
DROP MATERIALIZED VIEW IF EXISTS mview_fact_customer cascade;
-- schemaType: SEQUENCE
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
CREATE SEQUENCE IF NOT EXISTS dim_case_note_category_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1;
CREATE SEQUENCE IF NOT EXISTS dim_demand_type_items_dim_demand_type_items_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1;
CREATE SEQUENCE IF NOT EXISTS dim_demand_types_dim_demand_types_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1;
CREATE SEQUENCE IF NOT EXISTS dim_employer_access_dim_employer_access_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1;
CREATE SEQUENCE IF NOT EXISTS dim_roles_dim_role_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1;
CREATE SEQUENCE IF NOT EXISTS dim_sync_lookup_dim_sync_lookup_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 20609;
CREATE SEQUENCE IF NOT EXISTS dim_user_roles_dim_user_roles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 598;
CREATE SEQUENCE IF NOT EXISTS dim_ver_dim_ver_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 202557;
CREATE SEQUENCE IF NOT EXISTS fact_workrestriction_fact_workrestriction_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 61111;
-- schemaType: TABLE
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
CREATE TABLE IF NOT EXISTS dim_case_note_category();
DROP TABLE IF EXISTS fact_case;
DROP TABLE IF EXISTS fact_customer;
DROP TABLE IF EXISTS fact_employee;
DROP TABLE IF EXISTS fact_employer;
DROP TABLE IF EXISTS fact_organization;
DROP TABLE IF EXISTS fact_user;
-- schemaType: COLUMN
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
ALTER TABLE dim_case_authorized_submitter ADD COLUMN IF NOT EXISTS record_hash text;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS cat_order integer;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS category_code text;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS category_id character varying(24) NOT NULL;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS category_name text;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS dim_case_note_category_id bigint NOT NULL DEFAULT nextval('dim_case_note_category_id_seq'::regclass);
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS dim_case_note_id bigint;
ALTER TABLE dim_case_note_category ADD COLUMN IF NOT EXISTS event_date timestamp with time zone;
ALTER TABLE dim_case_pay_period ADD COLUMN IF NOT EXISTS end_date_override timestamp with time zone;
ALTER TABLE dim_case_policy ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE dim_case_policy ALTER COLUMN is_deleted DROP NOT NULL;
ALTER TABLE dim_case_policy_usage ALTER COLUMN created_date SET DEFAULT utc();
ALTER TABLE dim_employee_job ALTER COLUMN ver_is_current DROP NOT NULL;
ALTER TABLE dim_employee_job ALTER COLUMN ver_is_expired DROP NOT NULL;
ALTER TABLE dim_employee_job ALTER COLUMN ver_prev_id DROP NOT NULL;
ALTER TABLE dim_employee_job ALTER COLUMN ver_root_id DROP NOT NULL;
ALTER TABLE dim_employee_job ALTER COLUMN ver_seq DROP NOT NULL;
-- WARNING: The next statement will shorten a character varying column, which may result in data loss.
ALTER TABLE dim_users ALTER COLUMN customer_id TYPE character varying(24);
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS all_event_dates;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS case_count_closed_todos;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS case_count_open_todos;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS case_count_overdue_todos;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS case_count_todos;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS case_ref_id;
ALTER TABLE IF EXISTS fact_case DROP COLUMN IF EXISTS dim_case_id;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_cases;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_closed_cases;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_closed_todos;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_employees;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_employers;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_open_cases;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_open_todos;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_overdue_todos;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_todos;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS cust_count_users;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS customer_ref_id;
ALTER TABLE IF EXISTS fact_customer DROP COLUMN IF EXISTS dim_customer_id;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS dim_employee_id;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_cases;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_closed_cases;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_closed_todos;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_open_cases;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_open_todos;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_overdue_todos;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS empl_count_todos;
ALTER TABLE IF EXISTS fact_employee DROP COLUMN IF EXISTS employee_ref_id;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS dim_employer_id;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS employer_ref_id;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_cases;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_closed_cases;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_closed_todos;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_employees;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_open_cases;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_open_todos;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_overdue_todos;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_todos;
ALTER TABLE IF EXISTS fact_employer DROP COLUMN IF EXISTS eplr_count_users;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS customer_ref_id;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS dim_organization_id;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS employer_ref_id;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_cases;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_children;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_closed_cases;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_closed_todos;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_descendents;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_employees;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_open_cases;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_open_todos;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_overdue_todos;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_count_todos;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS org_level;
ALTER TABLE IF EXISTS fact_organization DROP COLUMN IF EXISTS organization_ref_id;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS dim_user_id;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_cases;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_closed_cases;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_closed_todos;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_open_cases;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_open_todos;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_overdue_todos;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_count_todos;
ALTER TABLE IF EXISTS fact_user DROP COLUMN IF EXISTS user_ref_id;
-- schemaType: VIEW
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: INDEX
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
DROP INDEX IF EXISTS dim_accommodation_customer_id;
DROP INDEX IF EXISTS dim_accommodation_employee_id;
DROP INDEX IF EXISTS dim_accommodation_employer_id;
DROP INDEX IF EXISTS dim_accommodation_is_deleted;
DROP INDEX IF EXISTS dim_accommodation_ver_is_current;
CREATE INDEX IF NOT EXISTS dim_case_policy_cur_ver_ref_id_is_del ON dim_accommodation USING btree (ext_ref_id, ver_is_current, is_deleted);


CREATE UNIQUE INDEX IF NOT EXISTS dim_case_note_category_pkey ON dim_case_note_category USING btree (dim_case_note_category_id);
--ALTER TABLE ONLY dim_case_note_category PRIMARY KEY USING INDEX dim_case_note_category_pkey;


CREATE INDEX IF NOT EXISTS dim_case_policy_usage_dim_case_policy_id ON dim_case_policy_usage USING btree (dim_case_policy_id);
DROP INDEX IF EXISTS dim_cases_billing_report;
DROP INDEX IF EXISTS dim_cases_customer_id;
DROP INDEX IF EXISTS dim_cases_employee_id;
DROP INDEX IF EXISTS dim_cases_employer_id;
DROP INDEX IF EXISTS dim_employee_billing_report;
DROP INDEX IF EXISTS dim_employer_billing_report;
DROP INDEX IF EXISTS dim_todoitem_billing_report;
DROP INDEX IF EXISTS dim_users_billing_report;
DROP INDEX IF EXISTS fact_customer_customer_ref_id;
DROP INDEX IF EXISTS fact_employee_employee_ref_id;
-- Warning, this may drop foreign keys pointing at this column.  Make sure you re-run the FOREIGN_KEY diff after running this SQL.
ALTER TABLE IF EXISTS  fact_employee DROP CONSTRAINT IF EXISTS fact_employee_pkey CASCADE;
DROP INDEX IF EXISTS fact_employee_pkey;
DROP INDEX IF EXISTS fact_employer_employer_ref_id;
DROP INDEX IF EXISTS fact_organization_customer_ref_id;
DROP INDEX IF EXISTS fact_organization_employer_ref_id;
-- schemaType: FOREIGN_KEY
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: TRIGGER
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
-- schemaType: FUNCTION
-- db1: {datawarehouse_dev 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- db2: {datawarehouse_qa 172.31.40.118 5432 postgres Absences0ft#postGRESQL sslmode=disable}
-- Run the following SQL against db2:
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.create_index_if_not_exists(t_name text, i_name text, index_sql text)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

DECLARE
  full_index_name varchar;
  schema_name varchar;
BEGIN

full_index_name = t_name || '_' || i_name;
schema_name = 'public';

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = full_index_name
    AND    n.nspname = schema_name
    ) THEN

    execute 'CREATE INDEX ' || full_index_name || ' ON ' || schema_name || '.' || t_name || ' ' || index_sql;
END IF;
END

$function$
;
-- STATEMENT-END
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html)
DROP FUNCTION IF EXISTS custom_field(json,text) CASCADE;
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.custom_field(custom_fields jsonb, code text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

BEGIN
	RETURN CASE WHEN custom_fields IS NULL THEN null::text ELSE custom_fields ->> code END;
END

$function$
;
-- STATEMENT-END
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.delete_non_current_records()
 RETURNS text
 LANGUAGE plpgsql
AS $function$ DECLARE tables CURSOR FOR
SELECT
    tablename
FROM
    pg_tables
WHERE
    tablename NOT LIKE 'pg_%'
    AND tablename LIKE 'dim_%'
    AND tablename NOT LIKE 'dim_lookup%'
    AND tablename <> 'dimension'
    and tablename not in (
        'dim_accommodation_usage', 'dim_date',
        'dim_employee_restriction_entry',
        'dim_case_certification'
    )
ORDER BY
    tablename; nbRow int;

     BEGIN FOR table_record IN tables LOOP EXECUTE 'DELETE FROM ' || table_record.tablename || ' where ver_is_current = False';
    END LOOP;

    Return 'Completed';

    END; $function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.function_exists(sch text, fun text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$

BEGIN
	EXECUTE  'select pg_get_functiondef('''||sch||'.'||fun||'''::regprocedure)';
	RETURN true;
	exception when others then
	RETURN false;
END;

$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.json_array_map(json_arr json, path text[])
 RETURNS json[]
 LANGUAGE plpgsql
AS $function$

DECLARE
	rec json;
	len int;
	ret json[];
BEGIN
	-- If json_arr is not an array, return an empty array as the result
	BEGIN
		len := json_array_length(json_arr);
	EXCEPTION
		WHEN OTHERS THEN
			RETURN ret;
	END;

	-- Apply mapping in a loop
	FOR rec IN SELECT json_array_elements#>path FROM json_array_elements(json_arr)
	LOOP
		ret := array_append(ret,rec);
	END LOOP;
	RETURN ret;
END
$function$
;
-- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.show_customerdetails()
 RETURNS TABLE(customerid bigint, customername character varying, employer character varying, employee_count_6month bigint, employee_count_3month bigint, employee_count_1month bigint, case_count bigint, todo_count bigint, active_users bigint)
 LANGUAGE plpgsql
AS $function$
    BEGIN
		return  query
		WITH grouped_employee_count_6 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '6 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id),
		grouped_employee_count_3 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '3 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id),
		grouped_employee_count_1 AS (SELECT customer_id, employer_id From dim_employee WHERE ver_is_current = TRUE AND ext_modified_date >= (current_date - '1 month'::interval) AND Is_Deleted = FALSE GROUP BY customer_id, employer_id) ,
		grouped_case_count AS (SELECT customer_id, employer_id FROM dim_cases WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND  ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) ,
		grouped_todo_count AS (SELECT customer_id, employer_id FROM dim_todoitem WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_id) ,
		grouped_active_users AS (SELECT customer_id, employer_ids FROM dim_Users WHERE ver_is_current = TRUE AND Is_Deleted = FALSE AND ext_modified_date >= (current_date - '6 month'::interval) GROUP BY customer_id, employer_ids)

		SELECT cust.dim_customer_ID, CAST(cust.cust_customer_name AS VARCHAR) customer, CAST(empr.NAME AS VARCHAR) Employer,
		(SELECT count(1) FROM grouped_employee_count_6 ec_6 WHERE ec_6.customer_id = cust.ext_ref_id AND ec_6.employer_id = empr.ext_ref_id) Employee_Count_6month,
		(SELECT count(1) FROM grouped_employee_count_3 ec_3 WHERE ec_3.customer_id = cust.ext_ref_id AND ec_3.employer_id = empr.ext_ref_id) Employee_Count_3month,
		(SELECT count(1) FROM grouped_employee_count_1 ec_1 WHERE ec_1.customer_id = cust.ext_ref_id AND ec_1.employer_id = empr.ext_ref_id) Employee_Count_1month,
		(SELECT count(1) FROM grouped_case_count cc  WHERE cc.customer_id = cust.ext_ref_id AND cc.employer_id = empr.ext_ref_id) Case_Count,
		(SELECT count(1) FROM grouped_todo_count tc  WHERE tc.customer_id = cust.ext_ref_id AND tc.employer_id = empr.ext_ref_id) Todo_Count,
		(SELECT count(1) FROM grouped_active_users au  WHERE au.customer_id = cust.ext_ref_id AND empr.ext_ref_id = ANY(au.employer_ids::varchar[])) Active_Users
		FROM dim_customer cust
		INNER JOIN dim_employer empr ON cust.ext_ref_id = empr.customer_id
		WHERE cust.ver_is_current = TRUE AND empr.ver_is_current = TRUE  AND cust.Is_Deleted = FALSE AND empr.Is_Deleted = FALSE
		GROUP BY cust.dim_customer_ID, cust.cust_customer_name, empr.NAME, empr.ext_ref_id
		ORDER BY 1;
    END;
  $function$

; -- STATEMENT-END
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html)
DROP FUNCTION IF EXISTS  show_customerdetails_new(date) CASCADE;
-- Note that CASCADE in the statement below will also drop any triggers depending on this function.
-- Also, if there are two functions with this name, you will need to add arguments to identify the correct one to drop.
-- (See http://www.postgresql.org/docs/9.4/interactive/sql-dropfunction.html)
DROP FUNCTION IF EXISTS  show_customerdetails_new2(date) CASCADE;
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.spdimdateinsert(iso8601date timestamp without time zone DEFAULT NULL::timestamp without time zone, year integer DEFAULT NULL::integer, month integer DEFAULT NULL::integer, day integer DEFAULT NULL::integer, minute integer DEFAULT NULL::integer, second integer DEFAULT NULL::integer, weekend bit DEFAULT NULL::"bit", weekday bit DEFAULT NULL::"bit", holiday bit DEFAULT NULL::"bit")
 RETURNS void
 LANGUAGE plpgsql
AS $function$

BEGIN
INSERT INTO dim_date
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
)
 VALUES
(
	iso8601date,
	year,
	month,
	day,
	minute,
	second,
	weekend,
	weekday,
	holiday
);
END;

$function$

; -- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.spfactinsert(factid integer DEFAULT 0, datecreated timestamp without time zone DEFAULT NULL::timestamp without time zone, code character varying DEFAULT NULL::character varying, description character varying DEFAULT NULL::character varying, visiblecode character varying DEFAULT NULL::character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

BEGIN
INSERT INTO fact
(
	fact_id,
	date_created,
	code,
	description,
	visible_code
)
 VALUES
(
	FactID,
	DateCreated,
	Code,
	Description,
	VisibleCode
);
END;

$function$
;
 -- STATEMENT-END
-- This function is different so we'll recreate it:
-- STATEMENT-BEGIN
CREATE OR REPLACE FUNCTION public.utc()
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN now() at time zone 'utc';
END
$function$
;-- STATEMENT-END
END
$do$
