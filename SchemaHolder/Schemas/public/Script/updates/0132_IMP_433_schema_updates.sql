﻿ALTER TABLE public.dim_cases
	ADD COLUMN IF NOT EXISTS medical_complications boolean;

ALTER TABLE public.dim_cases
	ADD COLUMN IF NOT EXISTS general_health_condition text;