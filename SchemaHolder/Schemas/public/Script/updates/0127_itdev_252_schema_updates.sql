CREATE INDEX IF NOT EXISTS dim_case_policy_usage_caseid
    ON public.dim_case_policy_usage USING btree
    (case_id)
    TABLESPACE pg_default;