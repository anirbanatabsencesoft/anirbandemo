
/**** Drop function sp_dim_ver for generating new version */
DROP FUNCTION IF EXISTS public.sp_dim_ver(IN p_dimension_id bigint,
    IN p_ext_ref_id character varying,
    IN p_ext_created_date timestamp with time zone,
    IN p_ext_modified_date timestamp with time zone);


/**** Drop table dim_ver as it's no more required ****/
DROP TABLE IF EXISTS public.dim_ver;