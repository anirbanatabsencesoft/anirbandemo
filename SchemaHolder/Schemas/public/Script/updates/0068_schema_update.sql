DROP VIEW IF EXISTS view_cust CASCADE;

ALTER TABLE dim_cases ADD COLUMN IF NOT EXISTS intermittent_type integer;

DROP TYPE public.lookup_type CASCADE;

CREATE TYPE public.lookup_type AS ENUM
    ('eligibility_status', 'case_denial_reason', 'case_intermittent_status', 'empl_job_classification', 'work_classification', 'type_of_injury', 'sharp_incdnt_when', 'sharp_job_class', 'sharp_location_dept', 'sharp_procedure', 'case_closure_reason','intermittent_type');

DROP TABLE public.dim_lookup;

ALTER SEQUENCE public.dim_lookup_id_seq RESTART;

CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type lookup_type NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;

/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);


/* Case Denial Reason Lookup (type: case_denial_reason) */	
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);


/* Case Intermittent Status. (type: case_intermittent_status) */
INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


/* Employee Job Classification (type: empl_job_classification) */
INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);

/* Work Classification (type: work_classification) */
INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);


/* Type of Injury (type: type_of_injury)*/
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);


/* Sharp Incident When (type: sharp_incdnt_when) */
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);

	
/* Sharp Job Class (type: sharp_job_class) */
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


/* Sharp Location/Department (type: sharp_location_dept) */
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


/* Sharp Procedure (type: sharp_procedure) */
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);


/* Case Closure Reason (type: case_closure_reason) */
INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);

/* IntermittentType Lookup (type: intermittent_type) */
INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 0, 'OfficeVisit' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=0);
INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 1, 'Incapacity' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=1);

