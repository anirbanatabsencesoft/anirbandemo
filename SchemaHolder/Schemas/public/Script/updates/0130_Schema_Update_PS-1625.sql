﻿--adding segment id to the dim_case_policy table

ALTER TABLE public.dim_case_policy ADD COLUMN IF NOT EXISTS segment_id character varying(36);

DROP INDEX IF EXISTS dim_case_policy_caseid_verseq;


-- adding segment id to the index to make sure it is used in view_poly.sql file  
CREATE INDEX IF NOT EXISTS dim_case_policy_caseid_verseq
  on public.dim_case_policy USING btree(case_id,segment_id, ver_is_current,ver_seq)
  TABLESPACE pg_default;
   