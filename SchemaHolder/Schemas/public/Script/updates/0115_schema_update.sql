﻿ALTER TABLE dim_cases ADD COLUMN IF NOT EXISTS has_relapse boolean;

CREATE SEQUENCE IF NOT EXISTS public.dim_relapse_dim_relapse_id_seq
    INCREMENT 1
    START 269
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_relapse_dim_relapse_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO postgres;

-- Table: public.dim_relapse

CREATE TABLE IF NOT EXISTS public.dim_relapse
(
    dim_relapse_id bigint NOT NULL DEFAULT nextval('dim_relapse_dim_relapse_id_seq'::regclass),
    ext_ref_id character varying(36) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24) COLLATE pg_catalog."default",
    ext_modified_by character varying(24) COLLATE pg_catalog."default",
    employee_id character varying(24) COLLATE pg_catalog."default",
    employer_id character varying(24) COLLATE pg_catalog."default",
    customer_id character varying(24) COLLATE pg_catalog."default",
    case_id character varying(24) COLLATE pg_catalog."default",
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    prior_rtw_date timestamp with time zone,
    prior_rtw_days bigint,
    is_deleted boolean,
    CONSTRAINT dim_relapse_pkey PRIMARY KEY (dim_relapse_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_relapse
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_relapse TO absence_tracker;

GRANT ALL ON TABLE public.dim_relapse TO etldw;

GRANT ALL ON TABLE public.dim_relapse TO postgres;


-- View: public.mview_fact_case

DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_case CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_case
TABLESPACE pg_default
AS
 WITH relapse AS (
         SELECT dim_relapse.case_id,
            count(dim_relapse.ext_ref_id) AS relapses_count           
           FROM dim_relapse
          WHERE dim_relapse.ver_is_current AND NOT dim_relapse.is_deleted
          GROUP BY dim_relapse.case_id
        ), todos AS (
         SELECT dim_todoitem.case_id,
            count(dim_todoitem.ext_ref_id) AS total,
            sum(
                CASE
                    WHEN dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4]) THEN 1
                    ELSE 0
                END) AS open_todos,
            sum(
                CASE
                    WHEN dim_todoitem.status = 1 THEN 1
                    ELSE 0
                END) AS closed_todos,
            sum(
                CASE
                    WHEN (dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4])) AND dim_todoitem.due_date < utc() THEN 1
                    ELSE 0
                END) AS overdue_todos
           FROM dim_todoitem
          WHERE dim_todoitem.ver_is_current AND NOT dim_todoitem.is_deleted
          GROUP BY dim_todoitem.case_id
        )
 SELECT dim_cases.dim_case_id,
    dim_cases.ext_ref_id AS case_ref_id,
    COALESCE(todos.total, 0::bigint) AS count_todos,
    COALESCE(todos.open_todos, 0::bigint) AS count_open_todos,
    COALESCE(todos.closed_todos, 0::bigint) AS count_closed_todos,
    COALESCE(todos.overdue_todos, 0::bigint) AS count_overdue_todos,
	 COALESCE(relapse.relapses_count, 0::bigint) AS count_relapses
   FROM dim_cases
     LEFT JOIN todos ON dim_cases.ext_ref_id::text = todos.case_id::text
	 LEFT JOIN relapse ON dim_cases.ext_ref_id::text = relapse.case_id::text
  WHERE dim_cases.ver_is_current AND NOT dim_cases.is_deleted
WITH DATA;

ALTER TABLE public.mview_fact_case
    OWNER TO postgres;

GRANT SELECT ON TABLE public.mview_fact_case TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_case TO etldw;
GRANT ALL ON TABLE public.mview_fact_case TO postgres;

CREATE UNIQUE INDEX mview_fact_case_case_ref_id
    ON public.mview_fact_case USING btree
    (case_ref_id COLLATE pg_catalog."default")
    TABLESPACE pg_default;

REFRESH MATERIALIZED VIEW mview_fact_case with Data;


