
CREATE SEQUENCE IF NOT EXISTS public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    INCREMENT 1
    START 213
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq 
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO postgres;

GRANT ALL ON SEQUENCE public.dim_organization_annual_info_dim_organization_annual_info_id_seq  TO etldw;

CREATE TABLE IF NOT EXISTS public.dim_organization_annual_info
(
    dim_organization_annual_info_id bigint NOT NULL DEFAULT nextval('dim_organization_annual_info_dim_organization_annual_info_id_seq'::regclass),
	ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    org_code text,
    org_year int,
	average_employee_count int,
	total_hours_worked decimal,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_organization_annual_info_pkey PRIMARY KEY (dim_organization_annual_info_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_organization_annual_info
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_organization_annual_info TO absence_tracker;
GRANT ALL ON TABLE public.dim_organization_annual_info TO etldw;
GRANT ALL ON TABLE public.dim_organization_annual_info TO postgres;

