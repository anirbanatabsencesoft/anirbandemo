alter table dim_organization add column if not exists siccode text;
alter table dim_organization add column if not exists naicscode text;
alter table dim_organization add column if not exists address1 text;
alter table dim_organization add column if not exists address2 text;
alter table dim_organization add column if not exists city text;
alter table dim_organization add column if not exists postalcode text;
alter table dim_organization add column if not exists state text;
alter table dim_organization add column if not exists country text;
