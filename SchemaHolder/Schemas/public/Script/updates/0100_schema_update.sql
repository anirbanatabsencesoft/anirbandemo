CREATE SEQUENCE IF NOT EXISTS public.dim_case_authorized_submitter_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_authorized_submitter_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_authorized_submitter_id_seq TO etldw;

-- Table: public.dim_case_authorized_submitter

-- Create Table dim_case_authorized_submitter
	CREATE TABLE IF NOT EXISTS public.dim_case_authorized_submitter
	(
		dim_case_authorized_submitter_id bigint NOT NULL DEFAULT nextval('dim_case_authorized_submitter_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		authorized_submitter_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		contact_type_code text,
		contact_type_name text,	
		contact_first_name text,    
		contact_last_name text,	
		contact_email text,
		contact_alt_email text,
		contact_work_phone text,
		contact_home_phone text,
		contact_cell_phone text,
		contact_fax text,
		contact_street text,
		contact_address1 text,
		contact_address2 text,
		contact_city text,
		contact_state text,
		contact_postal_code text,
		contact_country text,	   	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_case_authorized_submitter_pkey PRIMARY KEY (dim_case_authorized_submitter_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_authorized_submitter
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_authorized_submitter TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO etldw;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO postgres;

-- Index: dim_case_authorized_submitter_cur_ver_ref_id_is_del

CREATE INDEX IF NOT EXISTS dim_case_authorized_submitter_cur_ver_ref_id_is_del
    ON public.dim_case_authorized_submitter USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE IF NOT EXISTS public.dim_case_event_date_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_event_date_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_event_date_id_seq TO etldw;

-- Table: public.dim_case_event_date

-- Create Table dim_case_event_date
	CREATE TABLE IF NOT EXISTS public.dim_case_event_date
	(
		dim_case_event_date_id bigint NOT NULL DEFAULT nextval('dim_case_event_date_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		event_type bigint,		
		event_date timestamp with time zone,
		record_hash text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_case_event_date_pkey PRIMARY KEY (dim_case_event_date_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_event_date
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_event_date TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_event_date TO etldw;
	GRANT ALL ON TABLE public.dim_case_event_date TO postgres;

	-- Index: dim_case_event_date_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_event_date_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_event_date_cur_ver_ref_id_is_del
    ON public.dim_case_event_date USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

/*************************************************************************
 CASE PAY PERIOD 
**************************************************************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_case_pay_period_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_case_pay_period_detail_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_detail_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_detail_id_seq TO etldw;

-- Table: public.dim_case_pay_period

-- Create Table dim_case_pay_period
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period
	(
		dim_case_pay_period_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		status int,
		locked_date timestamp with time zone,
		locked_by	varchar(24),
		max_weekly_pay_amount double precision,		
		payroll_date_override timestamp with time zone,
		payroll_date timestamp with time zone,
		payroll_date_override_by varchar(24),
		end_date_override_by varchar(24),
		offset_amount double precision,
		total double precision,
		sub_total double precision,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_pay_period_pkey PRIMARY KEY (dim_case_pay_period_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period TO postgres;

-- Index: dim_case_pay_period_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_pay_period_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_pay_period_cur_ver_ref_id_is_del
    ON public.dim_case_pay_period USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_case_pay_period_detail

-- Create Table dim_case_pay_period_detail
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period_detail
	(
		dim_case_pay_period_detail_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_detail_id_seq'::regclass),
		dim_case_pay_period_id bigint,
		ext_ref_id character varying(36),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		is_Offset boolean,
		max_payment_amount double precision,
		pay_amount_override_value double precision,
		pay_amount_system_value double precision,
		payment_tier_percentage double precision,
		pay_percentage_override_by varchar(24),
		pay_percentage_override_date timestamp with time zone,
		pay_percentage_override_value double precision,
		pay_percentage_system_value double precision,
		policy_code text,
		policy_name text,
		salary_total text,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_case_pay_period_detail_pkey PRIMARY KEY (dim_case_pay_period_detail_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period_detail
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period_detail TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO postgres;

/**************************************************************
                     Communication
*************************************************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_communication_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_paperwork_field_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_field_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_field_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_paperwork_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_paperwork_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_paperwork_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_communication_recipient_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_communication_recipient_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_communication_recipient_id_seq TO etldw;

-- Table: public.dim_communication

-- Create Table dim_communication
	CREATE TABLE IF NOT EXISTS public.dim_communication
	(
		dim_communication_id bigint NOT NULL DEFAULT nextval('dim_communication_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		attachment_id varchar(24),
		body text,
		communication_type int,
		created_by_email text,
		created_by_name text,
		email_replies_type int,	
		is_draft boolean,
		name text,
		public boolean,
		first_send_date timestamp with time zone,
		last_send_date timestamp with time zone,
		subject text,
		template text,
		to_do_item_id varchar(24),
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_communication_id_pkey PRIMARY KEY (dim_communication_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication TO etldw;
	GRANT ALL ON TABLE public.dim_communication TO postgres;

-- Index: dim_communication_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_id_cur_ver_ref_id_is_del
    ON public.dim_communication USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_communication_paperwork

-- Create Table dim_communication_paperwork
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork
	(
		dim_communication_paperwork_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_id_seq'::regclass),
		communication_id character varying(24),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		attachment_id	varchar(24),		
		checked	 boolean,
		due_date	timestamp with time zone,
		return_attachment_id	varchar(24),
		status	int,
		status_date	 timestamp with time zone,
		
		paperwork_id	varchar(24),
		paperwork_cby	varchar(24),
		paperwork_cdt	timestamp with time zone,
		paperwork_code	text,
		paperwork_customer_id	varchar(24),
		paperwork_description	text,
		paperwork_doc_type	int,
		paperwork_employer_id	varchar(24),
		paperwork_enclosure_text	text,		
		paperwork_file_id	varchar(24),
		paperwork_file_name	text,
		paperwork_is_deleted boolean default false,
		paperwork_mby	varchar(24),
		paperwork_mdt	timestamp with time zone,
		paperwork_name	text,
		paperwork_requires_review	boolean,
		paperwork_return_date_adjustment	text,
		paperwork_return_date_adjustment_days	int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_pkey PRIMARY KEY (dim_communication_paperwork_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO postgres;
	
	-- Index: dim_communication_paperwork_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_paperwork_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_paperwork_id_cur_ver_ref_id_is_del
    ON public.dim_communication_paperwork USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

	-- Table: public.dim_communication_paperwork_field

-- Create Table dim_communication_paperwork_field
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork_field
	(
		dim_communication_paperwork_field_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_field_id_seq'::regclass),
		dim_communication_paperwork_id bigint,
		ext_ref_id character varying(36),
		name text,
		status int,
		type int,
		required boolean default false,
		field_order int,
		value text,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_field_pkey PRIMARY KEY (dim_communication_paperwork_field_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork_field
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

	-- Table: public.dim_communication_recipient

-- Create Table dim_communication_recipient
	CREATE TABLE IF NOT EXISTS public.dim_communication_recipient
	(
		dim_communication_recipient_id bigint NOT NULL DEFAULT nextval('dim_communication_recipient_id_seq'::regclass),
		dim_communication_id bigint,
		recipient_type int,
		ext_ref_id character varying(36),
		address_id	varchar(36),
		address_address1 text,
		address_city	text,
		address_country	text,
		address_postal_code	text,
		address_state	text,
		alt_email	text,
		email	text,
		fax	text,
		title text,
		first_name	text,
		middle_name text,		
		last_name	text,
		home_email	text,
		company_name text,
		contact_person_name text,
		date_of_birth timestamp with time zone ,
		home_phone text,
		cell_phone text,
		is_primary boolean,
		CONSTRAINT dim_communication_recipient_pkey PRIMARY KEY (dim_communication_recipient_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_recipient
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_recipient TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_recipient TO etldw;
	GRANT ALL ON TABLE public.dim_communication_recipient TO postgres;


	/******************************************************************
	                        Attachment
	******************************************************************/
	CREATE SEQUENCE IF NOT EXISTS public.dim_attachment_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_attachment_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_attachment_id_seq TO etldw;

-- Table: public.dim_attachment

-- Create Table dim_attachment
	CREATE TABLE IF NOT EXISTS public.dim_attachment
	(
		dim_attachment_id bigint NOT NULL DEFAULT nextval('dim_attachment_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		content_length text,
		content_type text,
		attachment_type int,
		created_by_name text,
		description text,
		file_id character varying(24),
		file_Name text,
		public boolean,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_attachment_pkey PRIMARY KEY (dim_attachment_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_attachment
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
	GRANT ALL ON TABLE public.dim_attachment TO etldw;
	GRANT ALL ON TABLE public.dim_attachment TO postgres;

-- Index: dim_attachment_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_attachment_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_attachment_cur_ver_ref_id_is_del
    ON public.dim_attachment USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

ALTER TABLE dim_case_policy_usage ADD COLUMN IF NOT EXISTS is_intermittent_restriction BOOLEAN;
ALTER TABLE public.dim_communication_paperwork ADD COLUMN IF NOT EXISTS case_id character varying(24);
ALTER TABLE public.dim_communication_paperwork ADD COLUMN IF NOT EXISTS paperwork_review_status INT;