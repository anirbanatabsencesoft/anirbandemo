DO
$do$
	BEGIN

PERFORM setval('dim_lookup_id_seq', (select max(lookup_id) from dim_lookup), true);


/*********** Sequence ************************/
CREATE SEQUENCE IF NOT EXISTS public.dim_case_note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_note_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_id_seq TO etldw;

CREATE SEQUENCE If Not Exists public.dim_workflow_instance_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_workflow_instance_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_workflow_instance_id_seq TO etldw;

CREATE SEQUENCE IF NOT EXISTS public.dim_variable_schedule_time_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_variable_schedule_time_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_variable_schedule_time_id_seq TO etldw;

/****** Dimensions *********************/
Alter table dim_attachment add column IF NOT EXISTS checked boolean;

-- Table: public.dim_case_note
	DROP TABLE IF EXISTS dim_case_note CASCADE;
-- Create Table dim_case_note
	CREATE TABLE IF NOT EXISTS public.dim_case_note
	(
		dim_case_note_id bigint NOT NULL DEFAULT nextval('dim_case_note_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accomm_id	bigint,
		accomm_question_id	text,
		answer	boolean,		
		cert_id	text,
		contact	text,
		copied_from	bigint,
		copied_from_employee	bigint,		
		employee_number	text,		
		entered_by_name	text,		
		notes	text,
		process_path	text,
		public	boolean,
		request_date	timestamp with time zone,
		request_id	text,
		s_answer	text,
		work_restriction_id varchar(36),
		category_code text,
		category_name text,
		category int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_note_pkey PRIMARY KEY (dim_case_note_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_note
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_note TO etldw;
	GRANT ALL ON TABLE public.dim_case_note TO postgres;

-- Index: dim_case_note_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_note_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_note_cur_ver_ref_id_is_del
    ON public.dim_case_note USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Table: public.dim_workflow_instance

-- Create Table dim_workflow_instance
	CREATE TABLE IF NOT EXISTS public.dim_workflow_instance
	(
		dim_workflow_instance_id bigint NOT NULL DEFAULT nextval('dim_workflow_instance_id_seq'::regclass),
		ext_ref_id character varying(36) ,
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accommodation_end_date	timestamp with time zone ,
		accommodation_id	character varying(36) ,
		accommodation_type_code	text ,
		accommodation_type_id	text ,
		active_accommodation_id	character varying(36) ,
		applied_policy_id	character varying(36) ,
		attachment_id	text ,		
		close_case	Boolean ,
		communication_id	text ,
		communication_type	int ,
		condition_start_date	timestamp with time zone ,
		contact_employee_due_date	timestamp with time zone ,
		contact_hcp_due_date	timestamp with time zone ,		
		denial_explanation	text ,
		denial_reason	int ,
		description	text ,
		determination	int ,
		due_date	timestamp with time zone ,
		ergonomic_assessment_date	timestamp with time zone ,
		event_id	varchar(24) ,
		event_type	int ,
		extend_grace_period	Boolean ,
		first_exhaustion_date	timestamp with time zone ,
		general_health_condition	text ,
		has_work_restrictions	Boolean ,
		illness_or_injury_date	timestamp with time zone ,
		new_due_date	timestamp with time zone ,
		paperwork_attachment_id	text ,
		paperwork_due_date	timestamp with time zone ,
		paperwork_id	character varying(36) ,
		paperwork_name	text ,
		paperwork_received	Boolean ,
		policy_code	text ,
		policy_id	text ,
		reason	int ,
		requires_review	Boolean ,
		return_attachment_id	text ,
		return_to_work_date	timestamp with time zone ,
		rtw	Boolean ,
		source_workflow_activity_activity_id	text ,
		source_workflow_activity_id	character varying(36) ,
		source_workflow_instance_id	text ,
		status	int ,
		template	text ,
		workflow_activity_id	character varying(36) ,
		workflow_id	varchar(24) ,
		workflow_name	text ,

		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_workflow_instance_pkey PRIMARY KEY (dim_workflow_instance_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_workflow_instance
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_workflow_instance TO absence_tracker;
	GRANT ALL ON TABLE public.dim_workflow_instance TO etldw;
	GRANT ALL ON TABLE public.dim_workflow_instance TO postgres;

-- Index: dim_workflow_instance_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_workflow_instance_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_workflow_instance_cur_ver_ref_id_is_del
    ON public.dim_workflow_instance USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

	-- Table: public.dim_variable_schedule_time

-- Create Table dim_variable_schedule_time
	CREATE TABLE IF NOT EXISTS public.dim_variable_schedule_time
	(
		dim_variable_schedule_time_id bigint NOT NULL DEFAULT nextval('dim_variable_schedule_time_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		time_sample_date timestamp with time zone ,
		time_total_minutes int,
		employee_number text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_variable_schedule_time_pkey PRIMARY KEY (dim_variable_schedule_time_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_variable_schedule_time
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_variable_schedule_time TO absence_tracker;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO etldw;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO postgres;

-- Index: dim_variable_schedule_time_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_variable_schedule_time_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_variable_schedule_time_cur_ver_ref_id_is_del
    ON public.dim_variable_schedule_time USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

END
$do$