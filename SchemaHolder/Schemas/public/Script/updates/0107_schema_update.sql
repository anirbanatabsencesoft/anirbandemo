
ALTER TABLE public.dim_cases ADD COLUMN IF NOT EXISTS case_closure_other_reason_details text;
 
ALTER TABLE public.dim_cases OWNER to postgres;
GRANT SELECT ON TABLE public.dim_cases TO absence_tracker;
GRANT ALL ON TABLE public.dim_cases TO etldw;
GRANT ALL ON TABLE public.dim_cases TO postgres;