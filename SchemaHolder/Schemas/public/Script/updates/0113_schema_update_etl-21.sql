
/*
	Step 1: Create Lookup Category table (dim_lookup_category)
	Step 2: Insert ENUM values to Lookup Category table
	Step 3: Drop dependent views
	Step 4: Try changing the type from enum to lookup_category
	Step 5: Add foreign key constraint to lookup with references category
	Step 6: Recreate views - Expecting automation or psql
	Step 7: Drop Type lookup_type
*/

/* Step 1*/

	/* Create lookup category table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup_category
	(
		lookup_category VARCHAR(60) NOT NULL,
		description text,
		CONSTRAINT dim_lookup_category_pkey PRIMARY KEY (lookup_category)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup_category
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup_category TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup_category TO etldw;
	GRANT ALL ON TABLE public.dim_lookup_category TO postgres;


/* Step 2 */

	/* Insert Enum Values to dim_lookup_category */
	INSERT INTO public.dim_lookup_category SELECT 'eligibility_status', 'Eligibility status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'eligibility_status');
	INSERT INTO public.dim_lookup_category SELECT 'case_denial_reason', 'Case denial reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_denial_reason');
	INSERT INTO public.dim_lookup_category SELECT 'case_intermittent_status', 'Case intermittent status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_intermittent_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_classification', 'Employee job classification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_classification');
	INSERT INTO public.dim_lookup_category SELECT 'work_classification', 'Work classification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'work_classification');
	INSERT INTO public.dim_lookup_category SELECT 'type_of_injury', 'Type of injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'type_of_injury');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_incdnt_when', 'Sharp incident when' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_incdnt_when');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_job_class', 'Sharp job class' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_job_class');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_location_dept', 'Sharp location/department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_location_dept');
	INSERT INTO public.dim_lookup_category SELECT 'sharp_procedure', 'Sharp procedure' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'sharp_procedure');
	INSERT INTO public.dim_lookup_category SELECT 'case_closure_reason', 'Case closure reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_closure_reason');
	INSERT INTO public.dim_lookup_category SELECT 'intermittent_type', 'Intermittent type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'intermittent_type');
	INSERT INTO public.dim_lookup_category SELECT 'recipient_type', 'Recipient type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'recipient_type');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_review_status', 'Paperwork review status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_review_status');
	INSERT INTO public.dim_lookup_category SELECT 'document_type', 'Document type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'document_type');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_field_status', 'Paperwork field status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_field_status');
	INSERT INTO public.dim_lookup_category SELECT 'paperwork_field_type', 'Paperwork field type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'paperwork_field_type');
	INSERT INTO public.dim_lookup_category SELECT 'case_event_type', 'Case event type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'case_event_type');
	INSERT INTO public.dim_lookup_category SELECT 'payroll_status', 'Payroll status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'payroll_status');
	INSERT INTO public.dim_lookup_category SELECT 'attachment_type', 'Attachment type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'attachment_type');
	INSERT INTO public.dim_lookup_category SELECT 'communication_type', 'Communication type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'communication_type');
	INSERT INTO public.dim_lookup_category SELECT 'email_replies_type', 'Email replies type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'email_replies_type');
	INSERT INTO public.dim_lookup_category SELECT 'note_category', 'Note category' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'note_category');
	INSERT INTO public.dim_lookup_category SELECT 'workflow_instance_status', 'Workflow instance status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'workflow_instance_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_adjudication_status', 'Employee job adjudication status' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_adjudication_status');
	INSERT INTO public.dim_lookup_category SELECT 'empl_job_denial_reason', 'Employee job denial reason' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_job_denial_reason');
	INSERT INTO public.dim_lookup_category SELECT 'empl_necessity_type', 'Employee necessity type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'empl_necessity_type');
	INSERT INTO public.dim_lookup_category SELECT 'role_type', 'Role type' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup_category WHERE lookup_category = 'role_type');


/* Step 3 */
	/* Drop Views sequentially */
	DROP VIEW IF EXISTS public.view_wfin;
	DROP VIEW IF EXISTS public.view_usge;
	DROP VIEW IF EXISTS public.view_role;
	DROP VIEW IF EXISTS public.view_poly;
	DROP VIEW IF EXISTS public.view_pypd; -- Dependent on view_payp
	DROP VIEW IF EXISTS public.view_payp;
	DROP VIEW IF EXISTS public.view_evnt;
	DROP VIEW IF EXISTS public.view_evdt;
	DROP VIEW IF EXISTS public.view_empn;
	DROP VIEW IF EXISTS public.view_cpwf;
	DROP VIEW IF EXISTS public.view_prwk;
	DROP VIEW IF EXISTS public.view_comr;
	DROP VIEW IF EXISTS public.view_comm;
	DROP VIEW IF EXISTS public.view_cnot;
	DROP VIEW IF EXISTS public.view_attm;
	DROP VIEW IF EXISTS public.view_acmu; --Dependent on view_acom
	DROP VIEW IF EXISTS public.view_acom; --Dependent on view_case
	DROP VIEW IF EXISTS public.view_todo; --Dependent on view_case
	DROP VIEW IF EXISTS public.view_emre; --Dependent on view_case/view_wkrs
	DROP VIEW IF EXISTS public.view_wkrs; --Dependent on view_case
	DROP VIEW IF EXISTS public.view_asbm; --Dependent on view_case
	DROP VIEW IF EXISTS public.view_case;
	DROP VIEW IF EXISTS public.view_empj; -- Dependent on view_empl
	DROP VIEW IF EXISTS public.view_enot; -- Dependent on view_empl
	DROP VIEW IF EXISTS public.view_vsch; -- Dependent on view_empl
	DROP VIEW IF EXISTS public.view_empc; -- Dependent on view_empl
	DROP VIEW IF EXISTS public.view_empl; 
	
	
/* Step 4 */
	/* Try chaging type to Lookup table */
	
	ALTER TABLE dim_lookup
		ALTER COLUMN type TYPE VARCHAR(60);


/* Step 5 */
	/* Add foreign key constraint */
	
	ALTER TABLE public.dim_lookup
		DROP CONSTRAINT IF EXISTS fk_lookup_lookup_cataegory;
		
		ALTER TABLE public.dim_lookup
			ADD CONSTRAINT fk_lookup_lookup_cataegory FOREIGN KEY (type) REFERENCES dim_lookup_category (lookup_category);


/* Step 6 */
	/* Creating views which are dropped.
		This I think can be done using automation 
		-- Set the encoding and page size first using the following commands
		chcp 65001
		set PGCLIENTENCODING=UTF8
		
		--Create a master SQL (master.sql) with this
		\set ON_ERROR_STOP 1
		begin;
    		\i view_empl.sql 
			\i view_empc.sql 
			\i view_vsch.sql 
			\i view_enot.sql 
			\i view_empj.sql 
			\i view_case.sql 
			\i view_asbm.sql
			\i view_asbm.sql 
			\i view_wkrs.sql 
			\i view_emre.sql 
			\i view_todo.sql 
			\i view_acom.sql 
			\i view_acmu.sql 
			\i view_attm.sql 
			\i view_cnot.sql 
			\i view_comm.sql 
			\i view_comr.sql
			\i view_prwk.sql 
			\i view_cpwf.sql 
			\i view_empn.sql 
			\i view_evdt.sql 
			\i view_evnt.sql 
			\i view_payp.sql 
			\i view_pypd.sql 
			\i view_poly.sql 
			\i view_role.sql 
			\i view_usge.sql 
			\i view_wfin

		commit;
		
		--now run
		psql -U postgres -h localhost -d datawarehouse_dev -a -f master.sql
		
	*/
	

/* Step 7 */
	/* Drop type lookup_type */

	DROP TYPE IF EXISTS lookup_type;


