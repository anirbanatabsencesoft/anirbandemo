﻿ALTER TABLE public.dim_case_policy
	ADD COLUMN IF NOT EXISTS denial_reason_code text;

ALTER TABLE public.dim_case_policy
	ADD COLUMN IF NOT EXISTS denial_reason_description text;

ALTER TABLE public.dim_case_policy_usage
	ADD COLUMN IF NOT EXISTS denial_reason_code text;

ALTER TABLE public.dim_case_policy_usage
	ADD COLUMN IF NOT EXISTS denial_reason_description text;

CREATE SEQUENCE IF NOT EXISTS public.dim_denial_reason_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_denial_reason_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_denial_reason_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_denial_reason_id_seq TO postgres;

CREATE TABLE IF NOT EXISTS public.dim_denial_reason
(
    dim_denial_reason_id bigint NOT NULL DEFAULT nextval('dim_denial_reason_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    is_deleted boolean,
    customer_id character varying(24),
    description text,
    code text,
	disabled_date timestamp with time zone DEFAULT null,
    CONSTRAINT dim_denial_reason_pkey PRIMARY KEY (dim_denial_reason_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_denial_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_denial_reason TO absence_tracker;

GRANT ALL ON TABLE public.dim_denial_reason TO etldw;

GRANT ALL ON TABLE public.dim_denial_reason TO postgres;

-- Index: dim_denial_reason_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_denial_reason_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_denial_reason_cur_ver_ref_id_is_del
    ON public.dim_denial_reason USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;
