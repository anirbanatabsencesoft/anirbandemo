--Adding columns
ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS document_key text;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS operation_type INT;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS operation_type_description varchar(100);

--Add processing status for error
ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS has_error boolean;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS err_message text;

ALTER TABLE public.mongo_changestream_log
	ADD COLUMN IF NOT EXISTS err_stack_trace text;