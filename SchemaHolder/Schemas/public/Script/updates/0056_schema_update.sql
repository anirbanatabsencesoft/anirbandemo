
DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_user CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_user
TABLESPACE pg_default
AS
WITH cases AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
), todos AS (
	SELECT	assigned_to_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY assigned_to_id
)
SELECT	dim_users.dim_user_id,
		dim_users.ext_ref_id AS user_ref_id,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos
FROM	dim_users
		LEFT JOIN cases ON dim_users.ext_ref_id = cases.assigned_to_id
      	LEFT JOIN todos ON dim_users.ext_ref_id = todos.assigned_to_id
WHERE	dim_users.ver_is_current
		AND NOT dim_users.is_deleted;

ALTER TABLE public.mview_fact_user
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_user TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_user TO etldw;
GRANT ALL ON TABLE public.mview_fact_user TO postgres;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_user_user_ref_id
    ON public.mview_fact_user USING btree
    (user_ref_id)
    TABLESPACE pg_default;
