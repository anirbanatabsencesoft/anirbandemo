
CREATE SEQUENCE IF NOT EXISTS public.dim_customer_service_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_customer_service_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_customer_service_id_seq TO postgres;

-- Table: public.dim_customer_service_id_seq

-- DROP TABLE public.dim_customer_service_id_seq;

-- Table: public.dim_customer_service_option

-- DROP TABLE public.dim_customer_service_option;

CREATE TABLE IF NOT EXISTS public.dim_customer_service_option
(
    dim_customer_service_option_id bigint NOT NULL DEFAULT nextval('dim_customer_service_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    is_deleted boolean,
    customer_id character varying(24),
    key text,
    value text,
    "order" integer,
    CONSTRAINT dim_customer_service_pkey PRIMARY KEY (dim_customer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_customer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_customer_service_option TO postgres;

-- Index: dim_employer_service_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_service_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS  dim_employer_service_cur_ver_ref_id_is_del
    ON public.dim_customer_service_option USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;


CREATE SEQUENCE IF NOT EXISTS  public.dim_team_member_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_member_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO postgres;

-- Table: public.dim_team_member

-- DROP TABLE public.dim_team_member;

CREATE TABLE IF NOT EXISTS  public.dim_team_member
(
    dim_team_member_id bigint NOT NULL DEFAULT nextval('dim_team_member_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    user_id character varying(24),
    team_id character varying(24),
    is_team_lead boolean,
    assigned_by_id character varying(24),
    assigned_on timestamp with time zone,
    modified_by_id character varying(24),
    modified_on timestamp with time zone,
    is_deleted boolean,
    CONSTRAINT dim_team_member_pkey PRIMARY KEY (dim_team_member_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team_member
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team_member TO absence_tracker;

GRANT ALL ON TABLE public.dim_team_member TO etldw;

GRANT ALL ON TABLE public.dim_team_member TO postgres;

-- Index: dim_team_member_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_member_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_team_member_cur_ver_ref_id_is_del
    ON public.dim_team_member USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;


CREATE SEQUENCE IF NOT EXISTS public.dim_event_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_event_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_event_id_seq TO postgres;

-- Table: public.dim_event

-- DROP TABLE public.dim_event;

CREATE TABLE IF NOT EXISTS public.dim_event
(
    dim_event_id bigint NOT NULL DEFAULT nextval('dim_event_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    employee_id character varying(24),
    todo_item_id character varying(24),
    accomodation_id character varying(24),
    active_accomodation_id character varying(24),
    accomodation_type_code text,
    accomodation_end_date timestamp with time zone,
    determination integer,
    denial_explanation text,
    case_id character varying(24),
    accomodation_type_id character varying(24),
    template text,
    communication_type integer,
    name text,
    event_type text,
    is_deleted boolean,
    applied_policy_id character varying(24),
    attachment_id character varying(24),
    close_case boolean,
    communication_id character varying(24),
    condition_start_date timestamp with time zone,
    contact_employee_due_date timestamp with time zone,
    contact_hcp_due_date timestamp with time zone,
    denial_reason integer,
    description text,
    due_date timestamp with time zone,
    ergonomic_assessment_date timestamp with time zone,
    extended_grace_period boolean,
    first_exhaustion_date timestamp with time zone,
    general_health_condition text,
    has_work_restriction boolean,
    illness_injury_date timestamp with time zone,
    new_due_date timestamp with time zone,
    paperwork_attachment_id character varying(24),
    paperwork_due_date timestamp with time zone,
    paperwork_id character varying(24),
    paperwork_name text,
    paperwork_received boolean,
    policy_code character varying(24),
    policy_id character varying(24),
    reason integer,
    requires_review boolean,
    return_attachment_id character varying(24),
    return_to_work_date timestamp with time zone,
    rtw boolean,
    source_workflow_activity_activityid character varying(24),
    source_workflow_activityid character varying(24),
    source_workflow_instanceid character varying(24),
    workflow_activity_id character varying(24),
    CONSTRAINT dim_event_pkey PRIMARY KEY (dim_event_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_event
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_event TO absence_tracker;

GRANT ALL ON TABLE public.dim_event TO etldw;

GRANT ALL ON TABLE public.dim_event TO postgres;

-- Index: dim_event_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_event_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_event_cur_ver_ref_id_is_del
    ON public.dim_event USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;