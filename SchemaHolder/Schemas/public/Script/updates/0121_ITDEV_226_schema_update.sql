create index IF NOT EXISTS dim_case_pay_period_caseid
  on dim_case_pay_period  USING btree (case_id, ver_is_current, ver_seq);