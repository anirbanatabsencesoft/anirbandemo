DROP VIEW IF EXISTS view_evnt cascade;
ALTER TABLE public.dim_event ALTER COLUMN policy_id TYPE varchar(36) USING policy_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN todo_item_id TYPE varchar(36) USING todo_item_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN case_id TYPE varchar(36) USING case_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN accomodation_type_id TYPE varchar(36) USING accomodation_type_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN attachment_id TYPE varchar(36) USING attachment_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN communication_id TYPE varchar(36) USING communication_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN paperwork_attachment_id TYPE varchar(36) USING paperwork_attachment_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN policy_code TYPE varchar(36) USING policy_code::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN return_attachment_id TYPE varchar(36) USING return_attachment_id::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN source_workflow_activity_activityid TYPE varchar(36) USING source_workflow_activity_activityid::varchar(36);
ALTER TABLE public.dim_event ALTER COLUMN source_workflow_instanceid TYPE varchar(36) USING source_workflow_instanceid::varchar(36);