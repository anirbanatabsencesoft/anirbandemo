
alter table dim_employer add column if not exists ignore_average_minutes_worked_per_week boolean;
alter table dim_employee add column if not exists average_minutes_worked_per_week double precision;
alter table dim_employee add column if not exists average_minutes_worked_per_week_as_of timestamp with time zone;