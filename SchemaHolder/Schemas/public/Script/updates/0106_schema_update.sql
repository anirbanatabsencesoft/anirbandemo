
Alter table public.dim_cases add column IF NOT EXISTS actual_duration int;
Alter table public.dim_cases add column IF NOT EXISTS exceeded_duration int;
 
ALTER TABLE public.dim_cases OWNER to postgres;
GRANT SELECT ON TABLE public.dim_cases TO absence_tracker;
GRANT ALL ON TABLE public.dim_cases TO etldw;
GRANT ALL ON TABLE public.dim_cases TO postgres;


Alter table public.dim_case_policy add column IF NOT EXISTS actual_duration int;
Alter table public.dim_case_policy add column IF NOT EXISTS exceeded_duration int;

ALTER TABLE public.dim_case_policy OWNER to postgres;
GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_policy TO etldw;
GRANT ALL ON TABLE public.dim_case_policy TO postgres;
