-- Repeat this as cleanup because Freedom.
alter table dim_cases add column if not exists alt_phone_number text;
alter table dim_cases add column if not exists alt_email text;
alter table dim_cases add column if not exists alt_address1 text;
alter table dim_cases add column if not exists alt_address2 text;
alter table dim_cases add column if not exists alt_city text;
alter table dim_cases add column if not exists alt_state text;
alter table dim_cases add column if not exists alt_postal_code text;
alter table dim_cases add column if not exists alt_country text;
ALTER TABLE IF EXISTS public.dim_case_policy ADD COLUMN IF NOT EXISTS crosswalk_codes jsonb;