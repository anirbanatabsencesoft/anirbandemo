
CREATE MATERIALIZED VIEW IF NOT EXISTS  public.mview_fact_customer
TABLESPACE pg_default
AS
WITH employers AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_employer
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), employees AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_employee
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), cases AS (
	SELECT	customer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), todos AS (
	SELECT	customer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
), users AS (
	SELECT	customer_id,
			count(ext_ref_id) as total
	FROM	dim_users
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY customer_id
)
SELECT	dim_customer.dim_customer_id,
		dim_customer.ext_ref_id AS customer_ref_id,
		coalesce(employers.total, 0) AS count_employers,
		coalesce(employees.total, 0) AS count_employees,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos,
        coalesce(users.total, 0) as count_users
FROM	dim_customer
		LEFT JOIN employers ON dim_customer.ext_ref_id = employers.customer_id
		LEFT JOIN employees ON dim_customer.ext_ref_id = employees.customer_id
		LEFT JOIN cases ON dim_customer.ext_ref_id = cases.customer_id
		LEFT JOIN todos ON dim_customer.ext_ref_id = todos.customer_id
		LEFT JOIN users ON dim_customer.ext_ref_id = users.customer_id
WHERE	dim_customer.ver_is_current
		AND NOT dim_customer.is_deleted;

ALTER TABLE public.mview_fact_customer
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_customer TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_customer TO etldw;
GRANT ALL ON TABLE public.mview_fact_customer TO postgres;

-- Index: mview_fact_customer_customer_ref_id

-- DROP INDEX public.mview_fact_customer_customer_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_customer_customer_ref_id
    ON public.mview_fact_customer USING btree
    (customer_ref_id)
    TABLESPACE pg_default;
