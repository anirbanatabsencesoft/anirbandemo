CREATE TABLE IF NOT EXISTS  public.TBL_MV_FACT_ORG_BASEDATA
(
	dim_organization_id bigint ,
	organization_ref_id varchar(24),
	customer_ref_id varchar(24),
	employer_ref_id varchar(24),
	org_level integer,
	count_children bigint, 
	count_descendents bigint,
	count_employees bigint,
	count_cases bigint,
	count_open_cases bigint,
	count_closed_cases bigint,
	count_todos bigint,
	count_open_todos bigint,
	count_closed_todos bigint,
	count_overdue_todos bigint
);


CREATE OR REPLACE FUNCTION UFN_TBL_MV_FACT_ORG_BASEDATA_REFRESH() 
RETURNS void 
AS $$
BEGIN
                
CREATE TEMP TABLE TBL_MV_FACT_ORG_TMP
(
	dim_organization_id bigint ,
	organization_ref_id varchar(24),
	customer_ref_id varchar(24),
	employer_ref_id varchar(24),
	count_children bigint, 
	count_descendents bigint,
	count_employees bigint,
	count_cases bigint,
	count_open_cases bigint,
	count_closed_cases bigint,
	count_todos bigint,
	count_open_todos bigint,
	count_closed_todos bigint,
	count_overdue_todos bigint,
	path text,
	concatvalue text,
	concatvalueparent text,
	code text,
	concatvalueroot text,
	org_level integer
 
);


--600 ms
INSERT INTO TBL_MV_FACT_ORG_TMP( dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,path)
SELECT dim_organization_id,ext_ref_id,customer_id,employer_id,path 
FROM dim_organization
WHERE  ver_is_current AND NOT  is_deleted;

 

update TBL_MV_FACT_ORG_TMP
set concatvalue = upper(customer_ref_id::text||employer_ref_id::text||path) ,
	concatvalueroot = upper(customer_ref_id::text||employer_ref_id::text||'/'||substr(ltrim(path,'/'),1,"strpos"(ltrim(path,'/'),'/'))),
	concatvalueparent = upper(reverse(substr(ltrim(reverse(path),'/'),"strpos"(ltrim(reverse(path),'/'),'/'),length(ltrim(reverse(path),'/'))))),
	org_level = (array_length(string_to_array(path, '/'::text), 1) - 2);
	
	
update TBL_MV_FACT_ORG_TMP
set  concatvalueparent = 
case when concatvalueparent = '/' then upper(path)
else upper(concatvalueparent)
end ;


update TBL_MV_FACT_ORG_TMP
set  concatvalueparent = upper(customer_ref_id::text||employer_ref_id::text||concatvalueparent);

update TBL_MV_FACT_ORG_TMP A
set code = B.code
from ( select code,ext_ref_id from dim_organization where ver_is_current and not is_deleted) B 
where A.organization_ref_id = B.ext_ref_id;


--create index ix_TBL_MV_FACT_ORG_TMP on TBL_MV_FACT_ORG_TMP(organization_ref_id,customer_ref_id,employer_ref_id);

  
CREATE TEMP TABLE cases AS
SELECT DISTINCT dim_employee.customer_id,
dim_employee.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_cases.ext_ref_id) AS total,
sum(
CASE
WHEN dim_cases.status = ANY (ARRAY[0, 4]) THEN 1
ELSE 0
END) AS open_cases,
sum(
CASE
WHEN dim_cases.status = 1 THEN 1
ELSE 0
END) AS closed_cases,
upper(dim_employee.customer_id::text||dim_employee.employer_id||upper(dim_employee_organization.path)) as concatvalue
FROM dim_cases
JOIN dim_employee ON dim_cases.employee_id = dim_employee.ext_ref_id AND dim_employee.ver_is_current AND NOT dim_employee.is_deleted
JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number AND dim_employee.customer_id = dim_employee_organization.customer_id AND dim_employee.employer_id = dim_employee_organization.employer_id AND dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
WHERE dim_cases.ver_is_current AND NOT dim_cases.is_deleted
GROUP BY dim_employee.customer_id, dim_employee.employer_id, (upper(dim_employee_organization.path));
 
 
 

UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_cases =  COALESCE(B.total,0),
count_open_cases = COALESCE(B.open_cases,0),
count_closed_cases = COALESCE(B.closed_cases,0)
FROM cases B
WHERE   A.concatvalue = B.concatvalue;



create temp table todos AS
SELECT DISTINCT dim_employee.customer_id,
dim_employee.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_todoitem.ext_ref_id) AS total,
sum(
CASE
WHEN dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4]) THEN 1
ELSE 0
END) AS open_todos,
sum(
CASE
WHEN dim_todoitem.status = 1 THEN 1
ELSE 0
END) AS closed_todos,
sum(
CASE
WHEN (dim_todoitem.status = ANY (ARRAY['-1'::integer, 2, 4])) AND dim_todoitem.due_date < utc() THEN 1
ELSE 0
END) AS overdue_todos,
upper(dim_employee.customer_id::text||dim_employee.employer_id||upper(dim_employee_organization.path)) as concatvalue
FROM dim_todoitem
JOIN dim_employee ON dim_todoitem.employee_id::text = dim_employee.ext_ref_id::text AND dim_employee.ver_is_current AND NOT dim_employee.is_deleted
JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number AND dim_employee.customer_id::text = dim_employee_organization.customer_id::text AND dim_employee.employer_id::text = dim_employee_organization.employer_id::text AND dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
WHERE dim_todoitem.ver_is_current AND NOT dim_todoitem.is_deleted
GROUP BY dim_employee.customer_id, dim_employee.employer_id, (upper(dim_employee_organization.path));

 
UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_todos =  COALESCE(B.total,0),
count_open_todos = COALESCE(B.open_todos,0),
count_closed_todos = COALESCE(B.closed_todos,0),
count_overdue_todos = COALESCE(B.overdue_todos,0)
FROM todos B
WHERE A.concatvalue = B.concatvalue;

UPDATE TBL_MV_FACT_ORG_TMP A
SET count_descendents = COALESCE(B.total,0)
FROM 
(
SELECT DISTINCT A.organization_ref_id,
count(B.organization_ref_id) AS total
FROM TBL_MV_FACT_ORG_TMP A
JOIN TBL_MV_FACT_ORG_TMP B 
ON A.concatvalue = B.concatvalueroot and A.organization_ref_id <> B.organization_ref_id
GROUP BY A.organization_ref_id
) B 
where  A.organization_ref_id = B.organization_ref_id;


UPDATE TBL_MV_FACT_ORG_TMP A
SET count_children = COALESCE(B.total_children,0)
FROM 
(
SELECT DISTINCT A.organization_ref_id,
sum(
CASE
WHEN upper((A.path || B.code) || '/'::text) = upper(B.path) THEN 1
ELSE 0
END) AS total_children
FROM TBL_MV_FACT_ORG_TMP A
JOIN TBL_MV_FACT_ORG_TMP B 
ON A.concatvalue = B.concatvalueparent and A.organization_ref_id <> B.organization_ref_id
GROUP BY A.organization_ref_id
) B 
where  A.organization_ref_id = B.organization_ref_id;





CREATE TEMP TABLE employees AS 
SELECT DISTINCT dim_employee_organization.customer_id,
dim_employee_organization.employer_id,
upper(dim_employee_organization.path) AS path,
count(dim_employee_organization.ext_ref_id) AS total,
upper(dim_employee_organization.customer_id::text||dim_employee_organization.employer_id||dim_employee_organization.path) as concatvalue
FROM dim_employee_organization
WHERE dim_employee_organization.ver_is_current AND NOT dim_employee_organization.is_deleted
GROUP BY dim_employee_organization.customer_id, dim_employee_organization.employer_id, (upper(dim_employee_organization.path)),
upper(dim_employee_organization.customer_id::text||dim_employee_organization.employer_id||dim_employee_organization.path);

UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_employees =  COALESCE(B.total, 0::bigint)
FROM employees B
WHERE A.concatvalue = B.concatvalue;

UPDATE TBL_MV_FACT_ORG_TMP A
SET 
count_employees =  COALESCE(count_employees, 0::bigint);



--update alll null values to zero 

update TBL_MV_FACT_ORG_TMP
set 
count_children = COALESCE(count_children,0),
count_descendents= COALESCE(count_descendents,0),
count_employees= COALESCE(count_employees,0),
count_cases= COALESCE(count_cases,0),
count_open_cases= COALESCE(count_open_cases,0),
count_closed_cases= COALESCE(count_closed_cases,0),
count_todos= COALESCE(count_todos,0),
count_open_todos= COALESCE(count_open_todos,0),
count_closed_todos= COALESCE(count_closed_todos,0),
count_overdue_todos= COALESCE(count_overdue_todos,0);



DELETE FROM  TBL_MV_FACT_ORG_BASEDATA;

INSERT INTO TBL_MV_FACT_ORG_BASEDATA
(
dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,org_level,count_children,
count_descendents,count_employees,count_cases,count_open_cases,count_closed_cases,count_todos,count_open_todos,
count_closed_todos,count_overdue_todos
)
SELECT a.dim_organization_id,
a.organization_ref_id,
a.customer_ref_id,
a.employer_ref_id,
a.org_level,
sum(a.count_children) AS count_children,
sum(a.count_descendents) AS count_descendents,
sum(a.count_employees) AS count_employees,
sum(a.count_cases) AS count_cases,
sum(a.count_open_cases) AS count_open_cases,
sum(a.count_closed_cases) AS count_closed_cases,
sum(a.count_todos) AS count_todos,
sum(a.count_open_todos) AS count_open_todos,
sum(a.count_closed_todos) AS count_closed_todos,
sum(a.count_overdue_todos) AS count_overdue_todos
FROM TBL_MV_FACT_ORG_TMP a
GROUP BY a.dim_organization_id, a.organization_ref_id, a.customer_ref_id, a.employer_ref_id, a.org_level;

DROP TABLE employees;
DROP TABLE TBL_MV_FACT_ORG_TMP;
DROP TABLE cases ;
DROP table todos ;
 
END;
$$ LANGUAGE plpgsql;

DROP MATERIALIZED VIEW  IF EXISTS public.mview_fact_organization CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_organization
AS

SELECT dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,org_level,count_children,
count_descendents,count_employees,count_cases,count_open_cases,count_closed_cases,count_todos,count_open_todos,
count_closed_todos,count_overdue_todos
FROM TBL_MV_FACT_ORG_BASEDATA
WITH DATA;

ALTER TABLE public.mview_fact_organization
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_organization TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_organization TO etldw;
GRANT ALL ON TABLE public.mview_fact_organization TO postgres;

-- Index: mview_fact_organization_organization_ref_id

-- DROP INDEX IF EXISTS public.mview_fact_organization_organization_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_organization_organization_ref_id
    ON public.mview_fact_organization USING btree
    (organization_ref_id)
    TABLESPACE pg_default;



