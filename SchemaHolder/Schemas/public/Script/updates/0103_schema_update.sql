

CREATE SEQUENCE IF NOT EXISTS public.dim_role_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_role_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_role_id_seq TO postgres;

-- Table: public.dim_role

-- DROP TABLE public.dim_role;

CREATE TABLE IF NOT EXISTS public.dim_role
(
    dim_role_id bigint NOT NULL DEFAULT nextval('dim_role_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    name text,
    customer_id character varying(24),
    permissions text[],
    role_type integer,
	is_deleted boolean,
    CONSTRAINT dim_role_pkey PRIMARY KEY (dim_role_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_role
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_role TO absence_tracker;

GRANT ALL ON TABLE public.dim_role TO etldw;

GRANT ALL ON TABLE public.dim_role TO postgres;

-- Index: dim_role_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_role_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_role_cur_ver_ref_id_is_del
    ON public.dim_role USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE IF NOT EXISTS public.dim_employer_contact_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_contact_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_contact_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_contact_id_seq TO postgres;

-- Table: public.dim_employer_contact

-- DROP TABLE public.dim_employer_contact;

CREATE TABLE IF NOT EXISTS public.dim_employer_contact
(
    dim_employer_contact_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    contact_firstname text,
    contact_lastname text,
    contact_email text,
    contact_alt_email text,
    contact_work_phone text,
    contact_fax text,
    contact_address text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean,
    CONSTRAINT dim_employer_contact_pkey PRIMARY KEY (dim_employer_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact TO postgres;

-- Index: dim_employer_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_contact_cur_ver_ref_id_is_del
    ON public.dim_employer_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE SEQUENCE IF NOT EXISTS public.dim_team_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_id_seq TO postgres;

-- Table: public.dim_team

-- DROP TABLE public.dim_team;

CREATE TABLE IF NOT EXISTS public.dim_team
(
    dim_team_id bigint NOT NULL DEFAULT nextval('dim_team_id_seq'::regclass),
    ext_ref_id character varying(24) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    name text,
    description text,
    is_deleted boolean,
    CONSTRAINT dim_team_pkey PRIMARY KEY (dim_team_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team TO absence_tracker;

GRANT ALL ON TABLE public.dim_team TO etldw;

GRANT ALL ON TABLE public.dim_team TO postgres;

-- Index: dim_team_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_team_cur_ver_ref_id_is_del
    ON public.dim_team USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;