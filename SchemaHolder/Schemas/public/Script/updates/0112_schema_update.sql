DROP VIEW if exists view_cnot CASCADE;
ALTER TABLE dim_case_note ALTER COLUMN copied_from TYPE TEXT;
ALTER TABLE dim_case_note ALTER COLUMN copied_from_employee TYPE TEXT;