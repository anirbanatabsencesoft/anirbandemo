DO
$do$
BEGIN
	-- Create sequences 
	CREATE SEQUENCE IF NOT EXISTS public.dim_case_policy_id_seq;
	CREATE SEQUENCE IF NOT EXISTS public.dim_case_policy_usage_id_seq;

	ALTER SEQUENCE public.dim_case_policy_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_id_seq TO etldw;

	ALTER SEQUENCE public.dim_case_policy_usage_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_case_policy_usage_id_seq TO etldw;

	-- Create Table dim_case_policy 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy
	(
		dim_case_policy_id bigint NOT NULL DEFAULT nextval('dim_case_policy_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL,
		policy_code varchar(50) NOT NULL,
		policy_name varchar(255) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		case_type int,
		eligibility int,
		determination int,
		denial_reason int,
		denial_reason_other text,
		payout_percentage decimal,
		is_deleted boolean NOT NULL default false,
		created_date timestamp with time zone DEFAULT utc(),

		CONSTRAINT dim_case_policy_pkey PRIMARY KEY (dim_case_policy_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy TO postgres;


	--- Table for dim_case_policy_usage 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy_usage
	(
		dim_case_policy_usage_id bigint NOT NULL DEFAULT nextval('dim_case_policy_usage_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		dim_case_policy_id  bigint NOT NULL,
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL, 
		policy_code varchar(50) NOT NULL,
		date_used timestamp with time zone,
		minutes_used int,
		minutes_in_day int,
		hours_used decimal,
		hours_in_day decimal,
		user_entered Boolean,
		determination int,
		denial_reason int,
		denial_reason_other text,
		determined_by varchar(24),
		intermittent_determination int,
		is_locked Boolean,
		uses_time Boolean,
		percent_of_day decimal,
		status int,
		created_date timestamp with time zone DEFAULT utc(),

		CONSTRAINT dim_case_policy_usage_pkey PRIMARY KEY (dim_case_policy_usage_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy_usage
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy_usage TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO postgres;


	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_phone_number text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_email text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_address1 text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_address2 text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_city text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_state text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_postal_code text;
	ALTER TABLE IF EXISTS public.dim_cases ADD COLUMN IF NOT EXISTS alt_country text;


	/* Create a generic lookup enum type */
	IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'lookup_type') THEN
		CREATE TYPE lookup_type AS enum ('eligibility_status', 'case_denial_reason', 'case_intermittent_status', 'empl_job_classification', 'work_classification', 'type_of_injury', 'sharp_incdnt_when', 'sharp_job_class', 'sharp_location_dept', 'sharp_procedure', 'case_closure_reason');
	END IF;


	/* Create Sequence for Lookups */
	CREATE SEQUENCE IF NOT EXISTS public.dim_lookup_id_seq;

	ALTER SEQUENCE public.dim_lookup_id_seq OWNER TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO postgres;
	GRANT ALL ON SEQUENCE public.dim_lookup_id_seq TO etldw;

	/* Create lookup table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type lookup_type NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;

	


	/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);

	/* Case Denial Reason Lookup (type: case_denial_reason) */	
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);

	/* Case Intermittent Status. (type: case_intermittent_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


	
	/* Employee Job Classification (type: empl_job_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);



	/* Appending new fields to dim_employee */
	alter table dim_employee add column if not exists ssn character varying(11);
	alter table dim_employee add column if not exists job_classification bigint;
	alter table dim_employee add column if not exists risk_profile_code character varying(255);
	alter table dim_employee add column if not exists risk_profile_name character varying(255);
	alter table dim_employee add column if not exists risk_profile_description text;
	alter table dim_employee add column if not exists risk_profile_order int;
	alter table dim_employee add column if not exists start_day_of_week character varying(12);


	/* Add work related fields to dim_cases */
	alter table dim_cases add column if not exists wr_date_of_injury timestamp with time zone;
	alter table dim_cases add column if not exists wr_is_pvt_healthcare boolean;
	alter table dim_cases add column if not exists wr_is_reportable boolean;
	alter table dim_cases add column if not exists wr_place_of_occurance text;
	alter table dim_cases add column if not exists wr_work_classification bigint;
	alter table dim_cases add column if not exists wr_type_of_injury bigint;
	alter table dim_cases add column if not exists wr_days_away_from_work int;
	alter table dim_cases add column if not exists wr_restricted_days int;

	alter table dim_cases add column if not exists wr_provider_first_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_last_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_company_name character varying(512);
	alter table dim_cases add column if not exists wr_provider_address1 character varying(512);
	alter table dim_cases add column if not exists wr_provider_address2 character varying(512);
	alter table dim_cases add column if not exists wr_provider_city character varying(512);
	alter table dim_cases add column if not exists wr_provider_state character varying(512);
	alter table dim_cases add column if not exists wr_provider_country character varying(512);
	alter table dim_cases add column if not exists wr_provider_postalcode character varying(20);

	alter table dim_cases add column if not exists wr_is_treated_in_emer_room boolean;
	alter table dim_cases add column if not exists wr_is_hospitalized boolean;
	alter table dim_cases add column if not exists wr_time_started_working character varying(100);
	alter table dim_cases add column if not exists wr_time_of_injury character varying(100);
	alter table dim_cases add column if not exists wr_activity_before_injury text;
	alter table dim_cases add column if not exists wr_what_happened text;
	alter table dim_cases add column if not exists wr_injury_details text;
	alter table dim_cases add column if not exists wr_what_harmed_empl text;
	alter table dim_cases add column if not exists wr_date_of_death timestamp with time zone;
	alter table dim_cases add column if not exists wr_is_sharp boolean;

	alter table dim_cases add column if not exists wr_sharp_body_part text;
	alter table dim_cases add column if not exists wr_type_of_sharp text;
	alter table dim_cases add column if not exists wr_brand_of_sharp text;
	alter table dim_cases add column if not exists wr_model_of_sharp text;
	alter table dim_cases add column if not exists wr_body_fluid text;
	alter table dim_cases add column if not exists wr_is_engn_injury_protection boolean;
	alter table dim_cases add column if not exists wr_is_protective_mech_activated boolean;
	alter table dim_cases add column if not exists wr_when_sharp_incident_happened bigint;
	alter table dim_cases add column if not exists wr_sharp_job_classification bigint;
	alter table dim_cases add column if not exists wr_sharp_other_job_classification text;
	alter table dim_cases add column if not exists wr_sharp_location_department bigint;
	alter table dim_cases add column if not exists wr_sharp_other_location_dept text;
	alter table dim_cases add column if not exists wr_sharp_procedure bigint;
	alter table dim_cases add column if not exists wr_sharp_other_procedure text;
	alter table dim_cases add column if not exists wr_sharp_exposure_detail text;



	
	/* Work Classification (type: work_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);

	
	/* Add data to lookup_type (type_of_injury). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'type_of_injury'; */

	/* Type of Injury (type: type_of_injury)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);


	/* Add data to lookup_type (sharp_incdnt_when). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_incdnt_when'; */
	
	/* Sharp Incident When (type: sharp_incdnt_when) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);


	/* Add data to lookup_type (sharp_job_class). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_job_class'; */

	/* Sharp Job Class (type: sharp_job_class) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


	/* Add data to lookup_type (sharp_location_dept). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_location_dept'; */
	
	/* Sharp Location/Department (type: sharp_location_dept) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


	/* Add data to lookup_type (sharp_procedure). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'sharp_procedure'; */
	
	/* Sharp Procedure (type: sharp_procedure) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);




	/* Co-morbidity guidelines data for dim_cases */
	alter table dim_cases add column if not exists cg_risk_asmt_score decimal;
	alter table dim_cases add column if not exists cg_risk_asmt_status character varying(512);
	alter table dim_cases add column if not exists cg_mid_range_all_absence decimal;
	alter table dim_cases add column if not exists cg_at_risk_all_absence decimal;
	alter table dim_cases add column if not exists cg_best_practice_days decimal;
	alter table dim_cases add column if not exists cg_actual_days_lost decimal;


	/* Add new fields for case */
	alter table dim_cases add column if not exists closure_reason bigint;
	alter table dim_cases add column if not exists closure_reason_detail text;
	alter table dim_cases add column if not exists description text;
	alter table dim_cases add column if not exists narrative text;
	alter table dim_cases add column if not exists is_rpt_auth_submitter boolean;
	alter table dim_cases add column if not exists send_e_communication boolean;
	alter table dim_cases add column if not exists e_communication_request_date timestamp with time zone;
	alter table dim_cases add column if not exists risk_profile_code character varying(255);
	alter table dim_cases add column if not exists risk_profile_name character varying(255);
	alter table dim_cases add column if not exists current_office_location text;
	alter table dim_cases add column if not exists total_paid decimal;
	alter table dim_cases add column if not exists total_offset decimal;
	alter table dim_cases add column if not exists apply_offsets_by_default boolean;
	alter table dim_cases add column if not exists waive_waiting_period boolean;
	alter table dim_cases add column if not exists pay_schedule_id character varying(36);


	/* Add data to lookup_type (case_closure_reason). Commented as this is a bug in postgres to be executed in multi-command mode. */
	/* ALTER TYPE lookup_type ADD VALUE 'case_closure_reason'; */

	/* Case Closure Reason (type: case_closure_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);

END
$do$