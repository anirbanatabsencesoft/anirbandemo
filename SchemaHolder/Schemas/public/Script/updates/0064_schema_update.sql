DROP VIEW IF EXISTS view_empl CASCADE;

ALTER TABLE dim_employee 
	ALTER COLUMN ssn 
	SET DATA TYPE character varying(20) 
	USING ssn::character varying(20);
