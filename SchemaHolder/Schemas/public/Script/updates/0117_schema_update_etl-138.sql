--Modifying column accomm_id of table dim_case_note
DROP VIEW IF EXISTS public.view_cnot CASCADE;


ALTER TABLE public.dim_case_note
	ALTER COLUMN accomm_id TYPE varchar(36);
