
DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_organization CASCADE;

CREATE MATERIALIZED VIEW public.mview_fact_organization
TABLESPACE pg_default
AS
WITH cases AS(
	SELECT DISTINCT
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH) AS PATH,
		COUNT(dim_cases.ext_ref_id) AS total,
		SUM( CASE WHEN dim_cases.status IN(0, 1, 4) THEN 1 ELSE 0 END) AS open_cases,
		SUM( CASE WHEN dim_cases.status = 1 THEN 1 ELSE 0 END) AS closed_cases
	FROM
		dim_cases
	INNER JOIN dim_employee ON dim_cases.employee_id = dim_employee.ext_ref_id
	AND dim_employee.ver_is_current
	AND NOT dim_employee.is_deleted
	INNER JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number
	AND dim_employee.customer_id = dim_employee_organization.customer_id
	AND dim_employee.employer_id = dim_employee_organization.employer_id
	AND dim_employee_organization.ver_is_current
	AND NOT dim_employee_organization.is_deleted
	WHERE
		dim_cases.ver_is_current
	AND NOT dim_cases.is_deleted
	GROUP BY
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH)
),
 todos AS(
	SELECT DISTINCT
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH) AS PATH,
		COUNT(dim_todoitem.ext_ref_id) AS total,
		SUM( CASE WHEN dim_todoitem.status IN(- 1, 2, 4) THEN 1 ELSE 0 END) AS open_todos,
		SUM( CASE WHEN dim_todoitem.status = 1 THEN 1 ELSE 0 END) AS closed_todos,
		SUM( CASE WHEN dim_todoitem.status IN(- 1, 2, 4) AND dim_todoitem.due_date < utc() THEN 1 ELSE 0 END) AS overdue_todos
	FROM
		dim_todoitem
	INNER JOIN dim_employee ON dim_todoitem.employee_id = dim_employee.ext_ref_id
	AND dim_employee.ver_is_current
	AND NOT dim_employee.is_deleted
	INNER JOIN dim_employee_organization ON dim_employee.employee_number = dim_employee_organization.employee_number
	AND dim_employee.customer_id = dim_employee_organization.customer_id
	AND dim_employee.employer_id = dim_employee_organization.employer_id
	AND dim_employee_organization.ver_is_current
	AND NOT dim_employee_organization.is_deleted
	WHERE
		dim_todoitem.ver_is_current
	AND NOT dim_todoitem.is_deleted
	GROUP BY
		dim_employee.customer_id,
		dim_employee.employer_id,
		UPPER(dim_employee_organization. PATH)
),
 children AS(
	SELECT DISTINCT
		dim_organization.ext_ref_id,
		COUNT(children.ext_ref_id) AS total,
		SUM( CASE WHEN UPPER( dim_organization. PATH || children.code || '/') = UPPER(children. PATH) THEN 1 
			  	   ELSE 0 
			  END
		) AS total_children
	FROM
		dim_organization
	INNER JOIN dim_organization AS children ON dim_organization.customer_id = children.customer_id
	AND dim_organization.employer_id = children.employer_id
	AND POSITION(
		UPPER(dim_organization. PATH) IN UPPER(children. PATH)
	) = 1
	AND CHAR_LENGTH(dim_organization. PATH) < CHAR_LENGTH(children. PATH)
	AND children.ver_is_current
	AND NOT children.is_deleted
	WHERE
		dim_organization.ver_is_current
	AND NOT dim_organization.is_deleted
	GROUP BY
		dim_organization.ext_ref_id
),
 employees AS(
	SELECT DISTINCT
		customer_id,
		employer_id,
		UPPER(PATH) AS PATH,
		COUNT(ext_ref_id) AS total
	FROM
		dim_employee_organization
	WHERE
		ver_is_current
	AND NOT is_deleted
	GROUP BY
		customer_id,
		employer_id,
		UPPER(PATH)
)

SELECT
	A.dim_organization_id as dim_organization_id,
	A.organization_ref_id as organization_ref_id,
	A.customer_ref_id as customer_ref_id,
	A.employer_ref_id as employer_ref_id,
	A.org_level as org_level,
	SUM(A.count_children) as count_children,
	SUM(A.count_descendents) as count_descendents, 
	SUM(A.count_employees) as count_employees,
	SUM(A.count_cases) as count_cases,
	SUM(A.count_open_cases) as count_open_cases,
	SUM(A.count_closed_cases) as count_closed_cases,
	SUM(A.count_todos) as count_todos,
	SUM(A.count_open_todos) as count_open_todos,
	SUM(A.count_closed_todos) as count_closed_todos,
	SUM(A.count_overdue_todos) as count_overdue_todos
FROM
	(
		SELECT 
			dim_organization.dim_organization_id,
			dim_organization.ext_ref_id AS organization_ref_id,
			dim_organization.customer_id AS customer_ref_id,
			dim_organization.employer_id AS employer_ref_id,
			array_length(string_to_array(dim_organization. PATH, '/'),1) - 2 AS org_level,
			COALESCE(children.total_children, 0) AS count_children,
			COALESCE(children.total, 0) AS count_descendents,
			COALESCE(employees.total, 0) AS count_employees,
			coalesce(cases.total, 0) AS count_cases,
			coalesce(cases.open_cases, 0) AS count_open_cases,
			coalesce(cases.closed_cases, 0) AS count_closed_cases,
			coalesce(todos.total, 0) AS count_todos,
			coalesce(todos.open_todos, 0) AS count_open_todos,
			coalesce(todos.closed_todos, 0) AS count_closed_todos,
			coalesce(todos.overdue_todos, 0) AS count_overdue_todos
		FROM
			dim_organization
		LEFT JOIN children ON dim_organization.ext_ref_id = children.ext_ref_id
		LEFT JOIN employees ON dim_organization.customer_id = employees.customer_id
			AND dim_organization.employer_id = employees.employer_id
			AND POSITION(UPPER(dim_organization. PATH) IN employees. PATH) = 1
			AND CHAR_LENGTH(dim_organization. PATH) <= CHAR_LENGTH(employees. PATH)      		
		LEFT JOIN cases ON dim_organization.customer_id = cases.customer_id
			AND dim_organization.employer_id = cases.employer_id
			AND position(upper(dim_organization.path) in cases.path) = 1
			AND char_length(dim_organization.path) <= char_length(cases.path)
		LEFT JOIN todos ON dim_organization.customer_id = todos.customer_id
			AND dim_organization.employer_id = todos.employer_id
			AND position(upper(dim_organization.path) in todos.path) = 1
			AND char_length(dim_organization.path) <= char_length(todos.path)
		WHERE
			dim_organization.ver_is_current
			AND NOT dim_organization.is_deleted
	) A
GROUP BY
	A.dim_organization_id,
	A.organization_ref_id,
	A.customer_ref_id,
	A.employer_ref_id,
	A.org_level;

ALTER TABLE public.mview_fact_organization
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_organization TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_organization TO etldw;
GRANT ALL ON TABLE public.mview_fact_organization TO postgres;

-- Index: mview_fact_organization_organization_ref_id

-- DROP INDEX IF EXISTS public.mview_fact_organization_organization_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_organization_organization_ref_id
    ON public.mview_fact_organization USING btree
    (organization_ref_id)
    TABLESPACE pg_default;