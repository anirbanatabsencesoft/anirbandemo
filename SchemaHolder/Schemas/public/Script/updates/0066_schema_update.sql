
DO
$do$
	BEGIN


	IF NOT EXISTS (SELECT 1 FROM public.dim_lookup_case_determination WHERE _id = 4) THEN
	  insert into public.dim_lookup_case_determination (_id, code, description) values (4, 3, 'Approved');
	END IF;
	IF NOT EXISTS (SELECT 1 FROM public.dim_lookup_case_determination WHERE _id = 5) THEN
	  insert into public.dim_lookup_case_determination (_id, code, description) values (5, 4, 'Not Eligible');
	END IF;

	ALTER TABLE public.dim_cases ADD COLUMN IF NOT EXISTS case_closure_reason_other text;

END $do$

-- NOTE: Views automatically get dropped and re-created in /Script/RebuildViews.cmd by TeamCity deployment.

-- This requires a re-sync, the logic was all wrong and the only way to correct it is a re-sync, so it
--	stands to reason to do this here in the schema update script, even though it may be suprising behavior.
-- It will only happen for this specific schema version at least.

--COMMENTED OUT BELOW STATEMENTS, WERE NOT DOING THIS AGAIN SINCE WE ARE JUST REFACTORING SCHEMA UPDATES FILE INTO SEPARATED FILE STRUCTURE

-- UPDATE operational.sync_manager SET 
-- 	modified_date = utc(),
-- 	last_sync_date_update = null,
-- 	source_count = 0,
-- 	initial_load = true
-- WHERE dimension_id = 4;
-- TRUNCATE TABLE public.dim_cases;