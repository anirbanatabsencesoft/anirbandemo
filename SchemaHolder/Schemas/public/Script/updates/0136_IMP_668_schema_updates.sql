﻿ALTER TABLE public.dim_employee
	ADD COLUMN IF NOT EXISTS employee_class_code text;

ALTER TABLE public.dim_employee
	ADD COLUMN IF NOT EXISTS employee_class_name text;
