CREATE SEQUENCE IF NOT EXISTS public.dim_employee_job_dim_employee_job_id_seq
    INCREMENT 1
    START 177
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_job_dim_employee_job_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_job_dim_employee_job_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_job_dim_employee_job_id_seq TO postgres;

CREATE TABLE IF NOT EXISTS public.dim_employee_job
(
    dim_employee_job_id bigint NOT NULL DEFAULT nextval('dim_employee_job_dim_employee_job_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    job_name text,
    job_code text,
    status_value int,
    status_reason int,
    status_comments text,
    status_username text,
    status_userid text,
    status_date timestamp with time zone,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
	is_deleted boolean default false,
    CONSTRAINT dim_employee_job_pkey PRIMARY KEY (dim_employee_job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_job
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_job TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_job TO etldw;

GRANT ALL ON TABLE public.dim_employee_job TO postgres;



CREATE SEQUENCE IF NOT EXISTS public.dim_employee_necessity_dim_employee_necessity_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_necessity_dim_employee_necessity_id_seq TO postgres;



CREATE TABLE IF NOT EXISTS public.dim_employee_necessity
(
    dim_employee_necessity_id bigint NOT NULL DEFAULT nextval('dim_employee_necessity_dim_employee_necessity_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    name text,
    type int,
    description text,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    case_id text,
    case_number text,
    CONSTRAINT dim_employee_necessity_pkey PRIMARY KEY (dim_employee_necessity_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_necessity
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_necessity TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_necessity TO etldw;

GRANT ALL ON TABLE public.dim_employee_necessity TO postgres;



CREATE SEQUENCE IF NOT EXISTS public.dim_employee_note_note_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_note_note_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employee_note_note_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employee_note_note_id_seq TO postgres;


CREATE TABLE IF NOT EXISTS  public.dim_employee_note
(
    dim_employee_note_id bigint NOT NULL DEFAULT nextval('dim_employee_note_note_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    note text,
    is_public boolean,
    category_code text,
    category_name text,    
    CONSTRAINT dim_employee_note_pkey PRIMARY KEY (dim_employee_note_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_note
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_note TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_note TO etldw;

GRANT ALL ON TABLE public.dim_employee_note TO postgres;



CREATE SEQUENCE IF NOT EXISTS public.dim_employer_service_option_dim_employer_service_option_id_seq
    INCREMENT 1
    START 24
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO postgres;


CREATE TABLE IF NOT EXISTS  public.dim_employer_service_option
(
    dim_employer_service_option_id bigint NOT NULL DEFAULT nextval('dim_employer_service_option_dim_employer_service_option_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    is_deleted boolean,
    key text,
    value text,
    cusomerserviceoptionid character varying(24),
    CONSTRAINT dim_employer_service_option_pkey PRIMARY KEY (dim_employer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_employer_service_option TO postgres;






ALTER TABLE public.dim_attachment
ADD COLUMN IF NOT EXISTS time_sample_date timestamp with time zone;

ALTER TABLE public.dim_attachment OWNER to postgres;
GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
GRANT ALL ON TABLE public.dim_attachment TO etldw;
GRANT ALL ON TABLE public.dim_attachment TO postgres;

ALTER TABLE public.dim_attachment
ADD COLUMN IF NOT EXISTS time_total_minutes int;

ALTER TABLE public.dim_attachment OWNER to postgres;
GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
GRANT ALL ON TABLE public.dim_attachment TO etldw;
GRANT ALL ON TABLE public.dim_attachment TO postgres;

ALTER TABLE public.dim_attachment
ADD COLUMN IF NOT EXISTS employee_number text;

ALTER TABLE public.dim_attachment OWNER to postgres;
GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
GRANT ALL ON TABLE public.dim_attachment TO etldw;
GRANT ALL ON TABLE public.dim_attachment TO postgres;

ALTER TABLE public.dim_attachment 
ADD COLUMN IF NOT EXISTS  dim_variable_schedule_time_id int;

ALTER TABLE public.dim_attachment OWNER to postgres;
GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
GRANT ALL ON TABLE public.dim_attachment TO etldw;
GRANT ALL ON TABLE public.dim_attachment TO postgres;

ALTER TABLE public.dim_employer_contact
ADD COLUMN IF NOT EXISTS contact_address1 text;

ALTER TABLE public.dim_employer_contact OWNER to postgres;
GRANT SELECT ON TABLE public.dim_employer_contact TO absence_tracker;
GRANT ALL ON TABLE public.dim_employer_contact TO etldw;
GRANT ALL ON TABLE public.dim_employer_contact TO postgres;

ALTER TABLE public.dim_communication_paperwork_field 
ADD COLUMN IF NOT EXISTS is_deleted boolean default false;

ALTER TABLE public.dim_communication_paperwork_field OWNER to postgres;
GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

ALTER TABLE public.dim_communication_paperwork_field 
ADD COLUMN IF NOT EXISTS ver_is_current boolean default true;

ALTER TABLE public.dim_communication_paperwork_field OWNER to postgres;
GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

ALTER TABLE public.dim_employee_job 
ADD COLUMN IF NOT EXISTS is_deleted boolean default false;

ALTER TABLE public.dim_employee_job OWNER to postgres;
GRANT SELECT ON TABLE public.dim_employee_job TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_job TO etldw;
GRANT ALL ON TABLE public.dim_employee_job TO postgres;

ALTER TABLE public.dim_employee_restriction_entry 
ADD COLUMN IF NOT EXISTS ver_is_current boolean default true;

ALTER TABLE public.dim_employee_restriction_entry OWNER to postgres;
GRANT SELECT ON TABLE public.dim_employee_restriction_entry TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO etldw;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO postgres;

ALTER TABLE dim_workflow_instance
ALTER COLUMN employee_id DROP NOT NULL;

ALTER TABLE public.dim_workflow_instance OWNER to postgres;
GRANT SELECT ON TABLE public.dim_workflow_instance TO absence_tracker;
GRANT ALL ON TABLE public.dim_workflow_instance TO etldw;
GRANT ALL ON TABLE public.dim_workflow_instance TO postgres;

ALTER TABLE dim_case_note
ALTER COLUMN case_id DROP NOT NULL;

ALTER TABLE public.dim_case_note OWNER to postgres;
GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_note TO etldw;
GRANT ALL ON TABLE public.dim_case_note TO postgres;

ALTER TABLE dim_case_note
ALTER COLUMN employee_id DROP NOT NULL;

ALTER TABLE public.dim_case_note OWNER to postgres;
GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_note TO etldw;
GRANT ALL ON TABLE public.dim_case_note TO postgres;

ALTER TABLE dim_employee_necessity
ADD COLUMN IF NOT EXISTS is_deleted boolean default false;

ALTER TABLE public.dim_employee_necessity OWNER to postgres;
GRANT SELECT ON TABLE public.dim_employee_necessity TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_necessity TO etldw;
GRANT ALL ON TABLE public.dim_employee_necessity TO postgres;