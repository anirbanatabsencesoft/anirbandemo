
DO
$do$
	BEGIN


--Update Accommodation table for audit data
ALTER TABLE dim_accommodation ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_accommodation ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update Case table for audit data

IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='dim_cases' AND column_name='cby') THEN
	ALTER TABLE dim_cases RENAME COLUMN cby TO ext_created_by;
END IF;


ALTER TABLE dim_cases ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update customers table for audit data
ALTER TABLE dim_customer ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_customer ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update demand table for audit data
ALTER TABLE dim_demand ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_demand ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update demand type table for audit data
ALTER TABLE dim_demand_type ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_demand_type ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee table for audit data
ALTER TABLE dim_employee ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee organization table for audit data
ALTER TABLE dim_employee_organization ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee_organization ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employee restriction table for audit data
ALTER TABLE dim_employee_restriction ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employee_restriction ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update employer table for audit data
ALTER TABLE dim_employer ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_employer ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update organization table for audit data
ALTER TABLE dim_organization ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_organization ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update to-do item table for audit data
ALTER TABLE dim_todoitem ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_todoitem ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

--Update users table for audit data
ALTER TABLE dim_users ADD COLUMN IF NOT EXISTS ext_created_by character varying(24);
ALTER TABLE dim_users ADD COLUMN IF NOT EXISTS ext_modified_by character varying(24);

	
--Rename columns of Employee Contact of cby and mby
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='dim_employee_contact' AND column_name='mby') THEN
	ALTER TABLE dim_employee_contact RENAME COLUMN mby TO ext_modified_by;
END IF;


IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='dim_employee_contact' AND column_name='cby') THEN
	ALTER TABLE dim_employee_contact RENAME COLUMN cby TO ext_created_by;
END IF;


END
$do$