DROP VIEW IF EXISTS view_cust CASCADE;
ALTER TABLE dim_event ALTER COLUMN accomodation_id  TYPE character varying (36); 
ALTER TABLE dim_event ALTER COLUMN active_accomodation_id  TYPE character varying (36); 
ALTER TABLE dim_event ALTER COLUMN applied_policy_id  TYPE character varying (36); 
ALTER TABLE dim_event ALTER COLUMN paperwork_id  TYPE character varying (36); 
ALTER TABLE dim_event ALTER COLUMN source_workflow_activityid  TYPE character varying (36); 
ALTER TABLE dim_event ALTER COLUMN workflow_activity_id  TYPE character varying (36); 