-- View: public.view_emct

--DROP VIEW IF EXISTS public.view_emct CASCADE;

CREATE OR REPLACE VIEW public.view_emct AS
 SELECT 
	view_eplr.*
    ,ect.dim_employer_contact_types_id as view_employer_contact_types_id
	,ect.ext_ref_id  as emct_id
    ,ect.ext_created_date  as emct_created_date
    ,ect.ext_modified_date  as emct_modified_date    
	,ect.name as emct_name
	,ect.code as emct_code
	
	,cb.display_name as emct_created_by_name
	,cb.email as emct_created_by_email
	,mb.display_name as emct_modified_by_name
	,mb.email as emct_modified_by_email

FROM dim_employer_contact_types ect
LEFT JOIN dim_users cb ON ect.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ect.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_eplr ON ect.customer_id = view_eplr.cust_id
WHERE ect.ver_is_current = true AND ect.is_deleted = false;

ALTER VIEW public.view_emct
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emct TO etldw;
GRANT SELECT ON TABLE public.view_emct TO absence_tracker;
GRANT ALL ON TABLE public.view_emct TO postgres;