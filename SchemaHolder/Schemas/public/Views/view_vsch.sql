﻿-- View: public.view_vsch

--DROP VIEW IF EXISTS public.view_vsch CASCADE;

CREATE OR REPLACE VIEW public.view_vsch AS
 SELECT
	view_empl.*,  	
	dim_variable_schedule_time.ext_ref_id as vsch_id,
	dim_variable_schedule_time.time_sample_date as vsch_time_sample_date,
	dim_variable_schedule_time.time_total_minutes as vsch_time_total_minutes,
	dim_variable_schedule_time.employee_number as vsch_employee_number,
	
	cb.display_name as vsch_created_by_name,
	cb.email as vsch_created_by_email,
	mb.display_name as vsch_modified_by_name,
	mb.email as vsch_modified_by_email

   FROM dim_variable_schedule_time
	 INNER JOIN view_empl ON dim_variable_schedule_time.employee_id = view_empl.empl_id     
     LEFT JOIN dim_users cb ON dim_variable_schedule_time.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_variable_schedule_time.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_variable_schedule_time.ver_is_current = true
     AND dim_variable_schedule_time.is_deleted = false;

ALTER VIEW public.view_vsch
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_vsch TO postgres;
GRANT SELECT ON TABLE public.view_vsch TO absence_tracker;
GRANT ALL ON TABLE public.view_vsch TO etldw;
