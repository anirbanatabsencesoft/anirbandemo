﻿-- View: public.view_usge   

-- DROP VIEW public.view_usge;
--DROP VIEW IF EXISTS public.view_usge CASCADE;

CREATE OR REPLACE VIEW public.view_usge AS
	SELECT 
	  view_poly.*,
	  dim_case_policy_usage.dim_case_policy_usage_id AS view_usge_id, 
	  dim_case_policy_usage.date_used AS usge_date_used, 
	  dim_case_policy_usage.minutes_used AS usge_minutes_used, 
	  dim_case_policy_usage.minutes_in_day AS usge_minutes_in_day, 
	  dim_case_policy_usage.hours_used AS usge_hours_used, 
	  dim_case_policy_usage.hours_in_day AS usge_hours_in_day, 
	  dim_case_policy_usage.user_entered AS usge_user_entered, 
	  determination.description AS usge_determination, 
	  denial_reason.description AS usge_denial_reason, 
	  dim_case_policy_usage.denial_reason_other AS usge_denial_reason_other, 
	  dim_case_policy_usage.denial_reason_code AS usge_denial_reason_code,
	  dim_case_policy_usage.denial_reason_description AS usge_denial_reason_description,
	  dim_case_policy_usage.determined_by as usge_determined_by_id,
	  concat(empl.first_name, ' ', empl.last_name) AS usge_determined_by, 
	  intermittent_determination.description AS usge_intermittent_determination, 
	  dim_case_policy_usage.is_locked AS usge_is_locked, 
	  dim_case_policy_usage.uses_time AS usge_uses_time, 
	  dim_case_policy_usage.is_intermittent_restriction as usge_is_intermittent_restriction,
	  dim_case_policy_usage.percent_of_day AS usge_percent_of_day, 
	  status.description AS usge_status
	FROM 
	  public.dim_case_policy_usage

	LEFT JOIN view_poly ON dim_case_policy_usage.dim_case_policy_id = view_poly.view_poly_id 
	LEFT JOIN dim_lookup_case_determination determination ON dim_case_policy_usage.determination = determination.code
	LEFT JOIN dim_lookup denial_reason ON dim_case_policy_usage.denial_reason = denial_reason.code AND denial_reason.type = 'case_denial_reason'
	LEFT JOIN dim_employee empl ON dim_case_policy_usage.determined_by = empl.ext_ref_id
	LEFT JOIN dim_lookup intermittent_determination ON dim_case_policy_usage.intermittent_determination = intermittent_determination.code AND intermittent_determination.type = 'case_intermittent_status'
	LEFT JOIN dim_lookup_case_determination status ON dim_case_policy_usage.status = status.code

	WHERE dim_case_policy_usage.ver_is_current = true;


ALTER VIEW public.view_usge OWNER TO postgres;

GRANT ALL ON TABLE public.view_usge TO postgres;
GRANT SELECT ON TABLE public.view_usge TO absence_tracker;
GRANT ALL ON TABLE public.view_usge TO etldw;
