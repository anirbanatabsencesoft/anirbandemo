﻿-- View: public.view_empn

--DROP VIEW IF EXISTS public.view_empn CASCADE;

CREATE OR REPLACE VIEW public.view_empn AS
SELECT 
	view_empl.*
	,en.dim_employee_necessity_id as view_employee_necessity_id
    ,en.ext_ref_id as empn_id
    ,en.ext_created_date as empn_created_date
    ,en.ext_modified_date as empn_modified_date   
    ,en.name as empn_name
    ,empl_necessity_type.description as empn_necessity_type
    ,en.description as empn_description
    ,en.start_date as empn_start_date
    ,en.end_date as empn_end_date
    ,en.case_id as empn_case_id
    ,en.case_number as empn_case_number
	,cb.display_name as empn_created_by_name
	,cb.email as empn_created_by_email
	,mb.display_name as empn_modified_by_name
	,mb.email as empn_modified_by_email
FROM public.dim_employee_necessity en
LEFT JOIN dim_users cb ON en.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON en.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (en.employee_id = view_empl.empl_id and en.customer_id = view_empl.cust_id and en.employer_id = view_empl.eplr_id)
INNER JOIN dim_lookup empl_necessity_type ON empl_necessity_type.type = 'empl_necessity_type' and en.type = empl_necessity_type.code
WHERE en.ver_is_current = true and en.is_deleted = false;

ALTER VIEW public.view_empn
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empn TO postgres;
GRANT SELECT ON TABLE public.view_empn TO absence_tracker;
GRANT ALL ON TABLE public.view_empn TO etldw;