﻿-- View: public.view_prwk

--DROP VIEW IF EXISTS public.view_prwk CASCADE;

CREATE OR REPLACE VIEW public.view_prwk AS
 SELECT
	view_comm.*,
	dim_communication_paperwork.ext_ref_id as prwk_id,
	dim_communication_paperwork.checked	 as prwk_checked ,
	dim_communication_paperwork.due_date as prwk_due_date ,
	lookup_paperwork_review_status.description as prwk_status,
	dim_communication_paperwork.status_date as prwk_status_date,	
	dim_communication_paperwork.paperwork_id as prwk_paperwork_id ,
	dim_communication_paperwork.paperwork_cdt as prwk_paperwork_cdt ,
	dim_communication_paperwork.paperwork_code as prwk_paperwork_code ,
	dim_communication_paperwork.paperwork_description as prwk_paperwork_description ,
	lookup_paperwork_doc_type.description as prwk_paperwork_doc_type,
	dim_communication_paperwork.paperwork_enclosure_text	as prwk_paperwork_enclosure_text ,
	dim_communication_paperwork.paperwork_file_id	as prwk_paperwork_file_id ,
	dim_communication_paperwork.paperwork_file_name	as prwk_paperwork_file_name ,
	dim_communication_paperwork.paperwork_mdt	as prwk_paperwork_mdt ,
	dim_communication_paperwork.paperwork_name	as prwk_paperwork_name ,
	dim_communication_paperwork.paperwork_requires_review	as prwk_paperwork_requires_review ,
	dim_communication_paperwork.paperwork_return_date_adjustment as prwk_paperwork_return_date_adjustment,	
	dim_communication_paperwork.paperwork_return_date_adjustment_days	as prwk_paperwork_return_date_adjustment_days ,	
	 
/*attachment fields */
	prwk_attm.attm_content_length as prwk_attm_content_length,
	prwk_attm.attm_content_type as prwk_attm_content_type,
	prwk_attm.attm_attachment_type as prwk_attm_attachment_type,
	prwk_attm.attm_created_by_name as prwk_attm_created_by_name,
	prwk_attm.attm_description as prwk_attm_description,
	prwk_attm.attm_file_id as prwk_attm_file_id,
	prwk_attm.attm_file_Name as prwk_attm_file_Name,
	prwk_attm.attm_public as prwk_attm_public ,

	/*return attachment fields */
	prwk_rtn_attm.attm_content_length as prwk_rtn_attm_content_length,
	prwk_rtn_attm.attm_content_type as prwk_rtn_attm_content_type,
	prwk_rtn_attm.attm_attachment_type as prwk_rtn_attm_attachment_type,
	prwk_rtn_attm.attm_created_by_name as prwk_rtn_attm_created_by_name,
	prwk_rtn_attm.attm_description as prwk_rtn_attm_description,
	prwk_rtn_attm.attm_file_id as prwk_rtn_attm_file_id,
	prwk_rtn_attm.attm_file_Name as prwk_rtn_attm_file_Name,
	prwk_rtn_attm.attm_public as prwk_rtn_attm_public ,
 
	cb.display_name as prwk_created_by_name,
	cb.email as prwk_created_by_email,
	mb.display_name as prwk_modified_by_name,
	mb.email as prwk_modified_by_email,

	prwk_cb.display_name as prwk_paperwork_created_by_name,
	prwk_cb.email as prwk_paperwork_created_by_email,
	prwk_mb.display_name as prwk_paperwork_modified_by_name,
	prwk_mb.email as prwk_paperwork_modified_by_email

   FROM dim_communication_paperwork
	 LEFT JOIN dim_lookup lookup_paperwork_doc_type ON dim_communication_paperwork.paperwork_doc_type = lookup_paperwork_doc_type.code and lookup_paperwork_doc_type.type='document_type'
	 LEFT JOIN dim_lookup lookup_paperwork_review_status ON dim_communication_paperwork.status = lookup_paperwork_review_status.code and lookup_paperwork_review_status.type='paperwork_review_status'
     LEFT JOIN view_comm on dim_communication_paperwork.communication_id = view_comm.comm_id 
	 LEFT JOIN view_attm prwk_attm on dim_communication_paperwork.attachment_id = prwk_attm.attm_id 
	 LEFT JOIN view_attm prwk_rtn_attm on dim_communication_paperwork.return_attachment_id = prwk_rtn_attm.attm_id 
	 LEFT JOIN dim_users cb ON dim_communication_paperwork.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_communication_paperwork.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
	 LEFT JOIN dim_users prwk_cb ON dim_communication_paperwork.paperwork_cby = prwk_cb.ext_ref_id AND prwk_cb.ver_is_current = true
	 LEFT JOIN dim_users prwk_mb ON dim_communication_paperwork.paperwork_mby = prwk_mb.ext_ref_id AND prwk_mb.ver_is_current = true
  WHERE dim_communication_paperwork.ver_is_current = true
     AND dim_communication_paperwork.is_deleted = false;

ALTER VIEW public.view_prwk
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_prwk TO postgres;
GRANT SELECT ON TABLE public.view_prwk TO absence_tracker;
GRANT ALL ON TABLE public.view_prwk TO etldw;
