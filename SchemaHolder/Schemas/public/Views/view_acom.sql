﻿-- View: public.view_acom

--DROP VIEW IF EXISTS public.view_acom CASCADE;

CREATE OR REPLACE VIEW public.view_acom AS
 SELECT
	view_case.*,
	dim_accommodation.ext_ref_id as acom_id,
	dim_accommodation.dim_accommodation_id as acom_dim_accommodation_id,
    dim_accommodation.accommodation_type AS acom_accommodation_type,
    dim_accommodation.accommodation_type_code AS acom_accommodation_type_code,
    dim_lookup_acom_duration.description AS acom_accommodation_duration,
    dim_accommodation.resolution AS acom_accommodation_resolution,
    dim_accommodation.resolved AS acom_accommodation_resolved,
    dim_accommodation.resolved_date AS acom_accommodation_resolved_date,
    dim_accommodation.granted AS acom_accommodation_granted,
    dim_accommodation.granted_date AS acom_accommodation_granted_date,
    dim_accommodation.cost AS acom_accommodation_cost,
        CASE
            WHEN dim_accommodation.start_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_accommodation.start_date
        END AS acom_accommodation_start_date,
        CASE
            WHEN dim_accommodation.end_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_accommodation.end_date
        END AS acom_accommodation_end_date,
    acom_determination.description AS acom_accommodation_determination,
    dim_accommodation.min_approved_date AS acom_accommodation_min_approved_date,
    dim_accommodation.max_approved_date AS acom_accommodation_max_approved_date,
    dim_accommodation.ext_created_date AS acom_accommodation_created_date,
    accom_status.description AS acom_accommodation_status,
        CASE
            WHEN dim_accommodation.cancel_reason = 0 THEN COALESCE(dim_accommodation.cancel_reason_other_desc, 'Other'::text::character varying)
            ELSE dim_lookup_accom_cancel_reason.description
        END AS acom_accommodation_cancel_reason,
	dim_accommodation.denial_reason_code AS accom_accommodation_denial_reason_code,
    dim_accommodation.work_related AS acom_accommodation_is_work_related,

	cb.display_name AS acom_created_by_name,
	cb.email AS acom_created_by_email,
	mb.display_name AS acom_modified_by_name,
	mb.email AS acom_modified_by_email

   FROM dim_accommodation
	 LEFT JOIN view_case ON dim_accommodation.case_id = view_case.case_id
     LEFT JOIN dim_lookup_acom_duration ON dim_accommodation.duration = dim_lookup_acom_duration.code
     LEFT JOIN dim_lookup_case_accom_determination acom_determination ON dim_accommodation.determination = acom_determination.code
     LEFT JOIN dim_lookup_case_accom_status accom_status ON dim_accommodation.status = accom_status.code
     LEFT JOIN dim_lookup_accom_cancel_reason ON dim_accommodation.cancel_reason = dim_lookup_accom_cancel_reason.code
	 LEFT JOIN dim_users cb ON dim_accommodation.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_accommodation.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_accommodation.ver_is_current = true
     AND dim_accommodation.is_deleted = false;

ALTER VIEW public.view_acom
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_acom TO postgres;
GRANT SELECT ON TABLE public.view_acom TO absence_tracker;
GRANT ALL ON TABLE public.view_acom TO etldw;
