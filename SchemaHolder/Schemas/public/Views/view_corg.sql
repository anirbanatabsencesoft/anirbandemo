-- View: public.view_org
 --DROP VIEW IF EXISTS public.view_corg CASCADE;

CREATE OR REPLACE VIEW public.view_corg AS
SELECT view_case.*,
       dim_organization.ext_ref_id AS org_id,
       dim_organization.code AS org_org_code,
       dim_organization.name AS org_org_name,
       dim_organization.description AS org_org_description,
       dim_organization.path AS org_org_path,
       dim_organization.type_code AS org_org_type_code,
       dim_organization.type_name AS org_org_type_name,
       dim_organization.siccode AS org_siccode,
       dim_organization.naicscode AS org_naicscode,
       dim_organization.address1 AS org_address1,
       dim_organization.address2 AS org_address2,
       dim_organization.city AS org_city,
       dim_organization.postalcode AS org_postalcode,
       dim_organization.state AS org_state,
       dim_organization.country AS org_country,
       dim_organization.ext_created_date AS org_org_created_date,
       dim_organization.org_created_by_email AS org_org_created_by_email,
       dim_organization.org_created_by_first_name AS org_org_created_by_first_name,
       dim_organization.org_created_by_last_name AS org_org_created_by_last_name,
       COALESCE(mview_fact_organization.org_level, 1) AS org_org_level,
       COALESCE(mview_fact_organization.count_children, 0::bigint) AS org_org_children,
       COALESCE(mview_fact_organization.count_descendents, 0::bigint) AS org_org_descendents,
       COALESCE(mview_fact_organization.count_employees, 0::bigint) AS org_org_employees,
       COALESCE(mview_fact_organization.count_cases, 0::bigint) AS org_org_cases,
       COALESCE(mview_fact_organization.count_open_cases, 0::bigint) AS org_org_open_cases,
       COALESCE(mview_fact_organization.count_closed_cases, 0::bigint) AS org_org_closed_cases,
       COALESCE(mview_fact_organization.count_todos, 0::bigint) AS org_org_todos,
       COALESCE(mview_fact_organization.count_open_todos, 0::bigint) AS org_org_open_todos,
       COALESCE(mview_fact_organization.count_closed_todos, 0::bigint) AS org_org_closed_todos,
       COALESCE(mview_fact_organization.count_overdue_todos, 0::bigint) AS org_org_overdue_todos,
       cb.display_name AS org_created_by_name,
       cb.email AS org_created_by_email,
       mb.display_name AS org_modified_by_name,
       mb.email AS org_modified_by_email
FROM view_case
	inner join dim_employee_organization on dim_employee_organization.customer_id = view_case.cust_id
	and dim_employee_organization.employer_id = view_case.eplr_id
	and dim_employee_organization.employee_number = view_case.empl_employee_number
	and dim_employee_organization.ver_is_current = true

	LEFT JOIN dim_organization ON dim_organization.employer_id = view_case.eplr_id
					AND dim_organization.customer_id = view_case.cust_id
					and dim_organization.code = dim_employee_organization.code
					and dim_organization.name = dim_employee_organization.name
	and dim_organization.ver_is_current = true
	LEFT JOIN mview_fact_organization ON dim_organization.ext_ref_id = mview_fact_organization.organization_ref_id


	LEFT JOIN dim_users cb ON dim_organization.ext_created_by = cb.ext_ref_id
					AND cb.ver_is_current = TRUE
	LEFT JOIN dim_users mb ON dim_organization.ext_modified_by = mb.ext_ref_id
					AND mb.ver_is_current = TRUE
WHERE dim_organization.ver_is_current = TRUE
    AND dim_organization.is_deleted = FALSE;


ALTER VIEW public.view_corg OWNER TO postgres;

GRANT ALL ON TABLE public.view_corg TO postgres;

GRANT
SELECT ON TABLE public.view_corg TO absence_tracker;

GRANT ALL ON TABLE public.view_corg TO etldw;