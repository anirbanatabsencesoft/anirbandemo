﻿-- View: public.view_rlps

--DROP VIEW IF EXISTS public.view_rlps CASCADE;

CREATE OR REPLACE VIEW public.view_rlps AS
 SELECT
	view_case.*,  	
	dim_relapse.ext_ref_id as rlps_relapse_id,
	dim_relapse.start_date as rlps_start_date,
	dim_relapse.end_date as rlps_end_date,
	dim_relapse.ext_created_date as rlps_created_date,
	dim_relapse.ext_modified_date as rlps_modified_date,
	
	cb.display_name AS rlps_created_by_name,
	cb.email AS rlps_created_by_email,
	mb.display_name AS rlps_modified_by_name,
	mb.email AS rlps_modified_by_email,

	dim_relapse.prior_rtw_date as rlps_prior_rtw_date,
	dim_relapse.prior_rtw_days as rlps_prior_rtw_days 
   FROM dim_relapse
	 LEFT JOIN view_case ON dim_relapse.case_id = view_case.case_id
     LEFT JOIN dim_users cb ON dim_relapse.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_relapse.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_relapse.ver_is_current = true AND dim_relapse.is_deleted = false;
ALTER VIEW public.view_rlps OWNER TO postgres;

GRANT ALL ON TABLE public.view_rlps TO postgres;
GRANT SELECT ON TABLE public.view_rlps TO absence_tracker;
GRANT ALL ON TABLE public.view_rlps TO etldw;