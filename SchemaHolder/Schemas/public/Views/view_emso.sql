﻿-- View: public.view_emso

--DROP VIEW IF EXISTS public.view_emso CASCADE;

CREATE OR REPLACE VIEW public.view_emso AS
SELECT 
     view_eplr.*
	,eso.dim_employer_service_option_id as view_employer_service_option_id
	,eso.ext_ref_id as emso_id
	,eso.ext_created_date as emso_created_date
	,eso.ext_modified_date as emso_modified_date
	,eso.key as emso_key
	,eso.value as emso_value,	

	cso.cuso_id,
	cso.cuso_created_date,
	cso.cuso_modified_date,        
	cso.cuso_key,
	cso.cuso_value,
	cso.cuso_order,
	cso.cuso_created_by_name,
	cso.cuso_created_by_email,
	cso.cuso_modified_by_name,
	cso.cuso_modified_by_email

	,cb.display_name AS emso_created_by_name
	,cb.email AS emso_created_by_email
	,mb.display_name AS emso_modified_by_name
	,mb.email AS emso_modified_by_email

FROM public.dim_employer_service_option eso
LEFT JOIN dim_users cb ON eso.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON eso.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cuso cso ON eso.cusomerserviceoptionid = cso.cuso_id
LEFT JOIN view_eplr ON (eso.customer_id = view_eplr.cust_id and eso.employer_id = view_eplr.eplr_id)
WHERE eso.ver_is_current = true AND eso.is_deleted = false;

ALTER VIEW public.view_emso
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emso TO postgres;
GRANT SELECT ON TABLE public.view_emso TO absence_tracker;
GRANT ALL ON TABLE public.view_emso TO etldw;