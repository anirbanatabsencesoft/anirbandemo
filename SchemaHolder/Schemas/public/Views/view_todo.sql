﻿-- View: public.view_todo

--DROP VIEW IF EXISTS public.view_todo CASCADE;

CREATE OR REPLACE VIEW public.view_todo AS
 SELECT 
	view_case.*,
    dim_todoitem.ext_ref_id AS todo_id,
    dim_lookup_todoitem_priority.description AS todo_priority,
	CASE
        WHEN dim_todoitem.status = 1 THEN 'Complete'::text
        WHEN dim_todoitem.status = 3 THEN 'Cancelled'::text
		WHEN (dim_todoitem.due_date::timestamp::date < utc()::timestamp::date) THEN 'Overdue'::text
        ELSE 'Pending'::text
    END AS todo_todo_status,
    dim_lookup_todoitem_types.name AS todo_todo_type,
    dim_lookup_todoitem_types.category AS todo_category,
    dim_lookup_todoitem_types.feature AS todo_feature,
    dim_todoitem.title AS todo_title,
    dim_todoitem.assigned_to_email AS todo_assigned_to_email,
    dim_todoitem.assigned_to_first_name AS todo_assigned_to_first_name,
    dim_todoitem.assigned_to_last_name AS todo_assigned_to_last_name,
    dim_todoitem.due_date AS todo_due_date,
    dim_todoitem.due_date_change_reason AS todo_due_date_change_reason,
    dim_todoitem.weight AS todo_weight,
    dim_todoitem.result_text AS todo_result,
    dim_todoitem.optional AS todo_is_optional,
    dim_todoitem.hide_until AS todo_hide_until_date,
    dim_todoitem.hide_until IS NULL OR dim_todoitem.hide_until <= utc() AS todo_is_visible,
        CASE
            WHEN dim_todoitem.due_date < ( CASE WHEN dim_todoitem.status IN (1, 3) THEN dim_todoitem.ext_modified_date ELSE utc() END ) THEN date_part('day'::text, utc() - dim_todoitem.due_date::timestamp with time zone)
            ELSE 0::double precision
        END AS todo_days_overdue,
    dim_todoitem.template AS todo_communication,
    dim_todoitem.ext_created_date AS todo_todo_created_date,
    dim_todoitem.ext_modified_date AS todo_todo_last_modified_date,
    dim_todoitem.work_flow_name AS todo_workflow_name,

	cb.display_name AS todo_created_by_name,
	cb.email AS todo_created_by_email,
	mb.display_name AS todo_modified_by_name,
	mb.email AS todo_modified_by_email,

	dim_todoitem.assigned_to_id AS todo_assigned_to_id

   FROM dim_todoitem
	 LEFT JOIN view_case ON dim_todoitem.case_id = view_case.case_id
     LEFT JOIN dim_lookup_todoitem_priority ON dim_todoitem.priority = dim_lookup_todoitem_priority.code
     LEFT JOIN dim_lookup_todoitem_types ON dim_todoitem.item_type = dim_lookup_todoitem_types.code
	 LEFT JOIN dim_users cb ON dim_todoitem.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_todoitem.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_todoitem.ver_is_current = true AND dim_todoitem.is_deleted = false;

ALTER VIEW public.view_todo
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_todo TO postgres;
GRANT SELECT ON TABLE public.view_todo TO absence_tracker;
GRANT ALL ON TABLE public.view_todo TO etldw;
