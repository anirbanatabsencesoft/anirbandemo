﻿-- View: public.view_cuso

--DROP VIEW IF EXISTS public.view_cuso CASCADE;

CREATE OR REPLACE VIEW public.view_cuso AS
SELECT
	view_cust.*
	,cso.dim_customer_service_option_id as view_customer_service_option_id
    ,cso.ext_ref_id as cuso_id
    ,cso.ext_created_date as cuso_created_date
    ,cso.ext_modified_date as cuso_modified_date        
    ,cso.key as cuso_key
    ,cso.value as cuso_value
    ,cso."order" as cuso_order

	,cb.display_name as cuso_created_by_name
	,cb.email as cuso_created_by_email
	,mb.display_name as cuso_modified_by_name
	,mb.email as cuso_modified_by_email

FROM public.dim_customer_service_option cso
LEFT JOIN dim_users cb ON cso.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON cso.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON cso.customer_id = view_cust.cust_id
 WHERE cso.ver_is_current = true
     AND cso.is_deleted = false;

ALTER VIEW public.view_cuso
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cuso TO postgres;
GRANT SELECT ON TABLE public.view_cuso TO absence_tracker;
GRANT ALL ON TABLE public.view_cuso TO etldw;