﻿-- View: public.view_comr


--DROP VIEW IF EXISTS public.view_comr CASCADE;

CREATE OR REPLACE VIEW public.view_comr AS
 SELECT
	view_comm.*,
	dim_communication_recipient.ext_ref_id as comr_Id ,	
	lookup_recipient_type.description as comr_recipient_type,	
	dim_communication_recipient.address_id	as comr_address_id ,
	dim_communication_recipient.address_address1 as comr_address_address1 ,
	dim_communication_recipient.address_city	as comr_address_city ,
	dim_communication_recipient.address_country	as comr_address_country ,
	dim_communication_recipient.address_postal_code	as comr_address_postal_code ,
	dim_communication_recipient.address_state	as comr_address_state ,
	dim_communication_recipient.alt_email	as comr_alt_email ,
	dim_communication_recipient.email	as comr_email ,
	dim_communication_recipient.fax	as comr_fax ,
	dim_communication_recipient.title as comr_title ,
	dim_communication_recipient.first_name	as comr_first_name ,
	dim_communication_recipient.middle_name as comr_middle_name ,
	dim_communication_recipient.last_name	as comr_last_name ,
	dim_communication_recipient.home_email	as comr_home_email ,
	dim_communication_recipient.company_name as comr_company_name ,
	dim_communication_recipient.contact_person_name as comr_contact_person_name ,
	dim_communication_recipient.date_of_birth  as comr_date_of_birth ,
	dim_communication_recipient.home_phone as comr_home_phone ,
	dim_communication_recipient.cell_phone as comr_cell_phone ,
	dim_communication_recipient.is_primary as comr_is_primary 
	
   FROM dim_communication_recipient
	 LEFT JOIN view_comm ON dim_communication_recipient.dim_communication_id = view_comm.view_communication_id
     LEFT JOIN dim_lookup lookup_recipient_type ON dim_communication_recipient.recipient_type = lookup_recipient_type.code and lookup_recipient_type.type='recipient_type';

ALTER VIEW public.view_comr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_comr TO postgres;
GRANT SELECT ON TABLE public.view_comr TO absence_tracker;
GRANT ALL ON TABLE public.view_comr TO etldw;
