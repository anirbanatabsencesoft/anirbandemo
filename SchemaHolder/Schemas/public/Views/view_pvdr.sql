﻿-- View: public.view_pvdr

--DROP VIEW IF EXISTS public.view_pvdr CASCADE;

CREATE OR REPLACE VIEW public.view_pvdr AS
 SELECT 
	view_case.*
    ,ec.dim_employee_contact_id as view_employee_contact_id
    ,ec.ext_ref_id as empc_id
    ,ec.ext_created_date as empc_created_date
    ,ec.ext_modified_date as empc_modified_date
    ,ec.contact_type_code as empc_contact_type_code
    ,ec.contact_type_name as empc_contact_type_name
    ,ec.company_name as empc_company_name
    ,ec.title as empc_title
    ,ec.first_name as empc_first_name
    ,ec.middle_name as empc_middle_name
    ,ec.last_name as empc_last_name
    ,ec.dob as empc_dob
    ,ec.email as empc_email
    ,ec.alt_email as empc_alt_email
    ,ec.work_phone as empc_work_phone
    ,ec.home_phone as empc_home_phone
    ,ec.cell_phone as empc_cell_phone
    ,ec.fax as empc_fax
    ,ec.street as empc_street
    ,ec.address1 as empc_contact_address1
    ,ec.address2 as empc_contact_address2
    ,ec.city as empc_city
    ,ec.state as empc_state
    ,ec.postal_code as empc_postal_code
    ,ec.country as empc_country
    ,ec.is_primary as empc_is_primary
    ,ec.contact_person_name as empc_contact_person_name
    ,ec.years_of_age as empc_years_of_age
    ,ec.contact_position as empc_contact_position
    ,ec.employee_number as empc_employee_number        
	,ec.military_status as empc_military_status

   FROM public.dim_employee_contact ec
   LEFT JOIN view_case ON ec.employee_id = view_case.empl_id
   WHERE ec.ver_is_current = true
   AND ec.is_deleted = false
   AND ec.contact_type_code in ('PROVIDER','URGENTCARE','EMERGENCYROOM','OTHER');

ALTER VIEW public.view_pvdr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_pvdr TO postgres;
GRANT SELECT ON TABLE public.view_pvdr TO absence_tracker;
GRANT ALL ON TABLE public.view_pvdr TO etldw;
