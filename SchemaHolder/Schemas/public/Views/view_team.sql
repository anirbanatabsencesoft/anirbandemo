﻿-- View: public.view_team

--DROP VIEW IF EXISTS public.view_team CASCADE;

CREATE OR REPLACE VIEW public.view_team AS
SELECT 
	view_cust.*
	,team.dim_team_id as view_team_id
    ,team.ext_ref_id as team_id
    ,team.ext_created_date as team_ext_created_date
    ,team.ext_modified_date as team_ext_modified_date
    
    ,team.name as team_name
    ,team.description as team_description

	,cb.display_name AS team_created_by_name
	,cb.email AS team_created_by_email
	,mb.display_name AS team_modified_by_name
	,mb.email AS team_modified_by_email

FROM public.dim_team team
LEFT JOIN dim_users cb ON team.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON team.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON team.customer_id = view_cust.cust_id
WHERE team.ver_is_current = true AND team.is_deleted = FALSE;

ALTER VIEW public.view_team
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_team TO postgres;
GRANT SELECT ON TABLE public.view_team TO absence_tracker;
GRANT ALL ON TABLE public.view_team TO etldw;