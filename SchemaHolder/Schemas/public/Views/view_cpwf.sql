﻿
-- View: public.view_cpwf

--DROP VIEW IF EXISTS public.view_cpwf CASCADE;

CREATE OR REPLACE VIEW public.view_cpwf AS
SELECT 
	 view_prwk.*
	,cpwf.ext_ref_id as cpwf_id
    ,cpwf.name as cpwf_name
    ,communication_paperwork_field_status.description as cpwf_description
    ,cpwf.type as cpwf_type
	,cpwf.status as cpwf_status
    ,cpwf.required as cpwf_required
    ,cpwf.field_order as cpwf_field_order
    ,cpwf.value as cpwf_dim_value
    ,cpwf.created_date as cpwf_created_date
    ,cpwf.record_hash as cpwf_record_hash
FROM public.dim_communication_paperwork_field cpwf
LEFT JOIN dim_lookup communication_paperwork_field_status ON cpwf.status = communication_paperwork_field_status.code AND communication_paperwork_field_status.type = 'paperwork_field_status'
LEFT JOIN view_prwk ON cpwf.ext_ref_id = view_prwk.prwk_id
WHERE cpwf.is_deleted = false;

ALTER VIEW public.view_cpwf
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cpwf TO postgres;
GRANT SELECT ON TABLE public.view_cpwf TO absence_tracker;
GRANT ALL ON TABLE public.view_cpwf TO etldw;