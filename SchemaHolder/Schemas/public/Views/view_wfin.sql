﻿-- View: public.view_wfin

--DROP VIEW IF EXISTS public.view_wfin CASCADE;

CREATE OR REPLACE VIEW public.view_wfin AS
 SELECT
	view_evnt.*,  	
	dim_workflow_instance.ext_ref_id as wfin_id,
	dim_workflow_instance.accommodation_end_date as wfin_accommodation_end_date	 ,
	dim_workflow_instance.accommodation_id as wfin_accommodation_id	,
	dim_workflow_instance.accommodation_type_code as wfin_accommodation_type_code	 ,
	dim_workflow_instance.accommodation_type_id as wfin_accommodation_type_id	 ,
	dim_workflow_instance.active_accommodation_id as wfin_active_accommodation_id,
	dim_workflow_instance.applied_policy_id as wfin_applied_policy_id,
	dim_workflow_instance.attachment_id as wfin_attachment_id	 ,
	dim_workflow_instance.close_case as wfin_close_case	 ,
	dim_workflow_instance.communication_id as wfin_communication_id	 ,
	dim_workflow_instance.communication_type as wfin_communication_type	 ,
	dim_workflow_instance.condition_start_date as wfin_condition_start_date	 ,
	dim_workflow_instance.contact_employee_due_date as wfin_contact_employee_due_date	 ,
	dim_workflow_instance.contact_hcp_due_date as wfin_contact_hcp_due_date	 ,
	dim_workflow_instance.customer_id as wfin_customer_id	 ,
	dim_workflow_instance.denial_explanation as wfin_denial_explanation	 ,
	dim_workflow_instance.denial_reason as wfin_denial_reason	 ,
	dim_workflow_instance.description as wfin_Description	 ,
	dim_workflow_instance.determination as wfin_Determination	 ,
	dim_workflow_instance.due_date as wfin_due_date	 ,
	dim_workflow_instance.ergonomic_assessment_date as wfin_ergonomic_assessment_date	 ,
	dim_workflow_instance.event_id as wfin_event_id	 ,
	dim_workflow_instance.event_type as wfin_event_type	 ,
	dim_workflow_instance.extend_grace_period as wfin_extend_grace_period	 ,
	dim_workflow_instance.first_exhaustion_date as wfin_first_exhaustion_date	 ,
	dim_workflow_instance.general_health_condition as wfin_general_health_condition	 ,
	dim_workflow_instance.has_work_restrictions as wfin_has_work_restrictions	 ,
	dim_workflow_instance.illness_or_injury_date as wfin_illness_or_injury_date	 ,
	dim_workflow_instance.new_due_date	 as wfin_new_due_date,
	dim_workflow_instance.paperwork_attachment_id as wfin_paperwork_attachment_id	 ,
	dim_workflow_instance.paperwork_due_date as wfin_paperwork_due_date	 ,
	dim_workflow_instance.paperwork_id as wfin_paperwork_id ,
	dim_workflow_instance.paperwork_name	as wfin_paperwork_name ,
	dim_workflow_instance.paperwork_received as wfin_paperwork_received	 ,
	dim_workflow_instance.policy_code as wfin_policy_code	 ,
	dim_workflow_instance.policy_id as wfin_policy_id	 ,
	dim_workflow_instance.reason as wfin_Reason	 ,
	dim_workflow_instance.requires_review as wfin_requires_review	 ,
	dim_workflow_instance.return_attachment_id as wfin_return_attachment_id	 ,
	dim_workflow_instance.return_to_work_date as wfin_return_to_work_date	 ,
	dim_workflow_instance.rtw as wfin_rtw	 ,
	dim_workflow_instance.source_workflow_activity_activity_id as wfin_source_workflow_activity_activity_id	 ,
	dim_workflow_instance.source_workflow_activity_id as wfin_source_workflow_activity_id,
	dim_workflow_instance.source_workflow_instance_id	as wfin_source_workflow_instance_id ,
	lookup_workflow_instance_status.description as wfin_Status	 ,
	dim_workflow_instance.template as wfin_Template	 ,
	dim_workflow_instance.workflow_activity_id as wfin_workflow_activity_id ,
	dim_workflow_instance.workflow_id	as wfin_workflow_id ,
	dim_workflow_instance.workflow_name as wfin_workflow_name	 ,

	cb.display_name as wfin_created_by_name,
	cb.email as wfin_created_by_email,
	mb.display_name as wfin_modified_by_name,
	mb.email as wfin_modified_by_email

   FROM dim_workflow_instance
	 LEFT JOIN view_evnt ON dim_workflow_instance.case_id = view_evnt.evnt_event_case_id
     LEFT JOIN dim_lookup lookup_workflow_instance_status ON dim_workflow_instance.status = lookup_workflow_instance_status.code and lookup_workflow_instance_status.type='workflow_instance_status'
     LEFT JOIN dim_users cb ON dim_workflow_instance.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_workflow_instance.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_workflow_instance.ver_is_current = true
     AND dim_workflow_instance.is_deleted = false;

ALTER VIEW public.view_wfin
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_wfin TO postgres;
GRANT SELECT ON TABLE public.view_wfin TO absence_tracker;
GRANT ALL ON TABLE public.view_wfin TO etldw;
