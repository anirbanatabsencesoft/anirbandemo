﻿-- View: public.view_eplr

--DROP VIEW IF EXISTS public.view_eplr CASCADE;

CREATE OR REPLACE VIEW public.view_eplr AS
 SELECT 
	view_cust.*, 	
    dim_employer.ext_ref_id AS eplr_id,
    dim_employer.name AS eplr_employer_name,
    dim_employer.reference_code AS eplr_reference_code,
    dim_employer.url AS eplr_employer_url,
    dim_employer.contact_first_name AS eplr_contact_first_name,
    dim_employer.contact_last_name AS eplr_contact_last_name,
    dim_employer.contact_email AS eplr_contact_email,
    dim_employer.contact_address1 AS eplr_contact_address1,
    dim_employer.contact_address2 AS eplr_contact_address2,
    dim_employer.contact_city AS eplr_contact_city,
    dim_employer.contact_state AS eplr_contact_state,
    dim_employer.contact_postal_code AS eplr_contact_postal_code,
    dim_employer.contact_country AS eplr_contact_country,
    dim_employer.reset_month AS eplr_reset_month,
    dim_employer.reset_day_of_month AS eplr_reset_day_of_month,
	dim_employer.start_date AS eplr_start_date,
	dim_employer.end_date AS eplr_end_date,
    dim_employer.fml_period_type AS eplr_fml_period_type,
    dim_employer.allow_intermittent_for_birth_adoption_or_foster_care AS eplr_allow_intermittent_for_birth_adoption_or_foster_care,
    dim_employer.ignore_schedule_for_prior_hours_worked AS eplr_ignore_schedule_for_prior_hours_worked,
    dim_employer.enable50in75mile_rule AS eplr_enable_50_in_75_mile_rule,
    dim_employer.enable_spouse_at_same_employer_rule AS eplr_enable_spouse_at_same_employer_rule,
    dim_employer.ft_weekly_work_hours AS eplr_ft_weekly_work_hours,
    dim_employer.publicly_traded_company AS eplr_is_publicly_traded,
    COALESCE(mview_fact_employer.count_employees, 0::bigint) AS eplr_employer_employees,
    COALESCE(mview_fact_employer.count_cases, 0::bigint) AS eplr_employer_cases,
    COALESCE(mview_fact_employer.count_open_cases, 0::bigint) AS eplr_employer_open_cases,
    COALESCE(mview_fact_employer.count_closed_cases, 0::bigint) AS eplr_employer_closed_cases,
    COALESCE(mview_fact_employer.count_todos, 0::bigint) AS eplr_employer_todos,
    COALESCE(mview_fact_employer.count_open_todos, 0::bigint) AS eplr_employer_open_todos,
    COALESCE(mview_fact_employer.count_closed_todos, 0::bigint) AS eplr_employer_closed_todos,
    COALESCE(mview_fact_employer.count_overdue_todos, 0::bigint) AS eplr_employer_overdue_todos,
    COALESCE(mview_fact_employer.count_users, 0::bigint) AS eplr_employer_users,
    dim_employer.eplr_service_type,

	cb.display_name AS eplr_created_by_name,
	cb.email AS eplr_created_by_email,
	mb.display_name AS eplr_modified_by_name,
	mb.email AS eplr_modified_by_email

   FROM dim_employer
     LEFT JOIN mview_fact_employer ON dim_employer.ext_ref_id = mview_fact_employer.employer_ref_id
     LEFT JOIN view_cust ON dim_employer.customer_id = view_cust.cust_id
	 LEFT JOIN dim_users cb ON dim_employer.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employer.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_employer.ver_is_current = true
     AND dim_employer.is_deleted = false;

ALTER VIEW public.view_eplr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_eplr TO postgres;
GRANT SELECT ON TABLE public.view_eplr TO absence_tracker;
GRANT ALL ON TABLE public.view_eplr TO etldw;
