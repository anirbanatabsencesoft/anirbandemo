﻿-- View: public.view_empc

DROP VIEW IF EXISTS public.view_empc CASCADE;

CREATE OR REPLACE VIEW public.view_empc AS
SELECT 
view_empl.*
	,ec.dim_employee_contact_id as view_employee_contact_id
    ,ec.ext_ref_id as empc_id
    ,ec.ext_created_date as empc_created_date
    ,ec.ext_modified_date as empc_modified_date
    ,ec.contact_type_code as empc_contact_type_code
    ,ec.contact_type_name as empc_contact_type_name
    ,ec.company_name as empc_company_name
    ,ec.title as empc_title
    ,ec.first_name as empc_first_name
    ,ec.middle_name as empc_middle_name
    ,ec.last_name as empc_last_name
    ,ec.dob as empc_dob
    ,ec.email as empc_email
    ,ec.alt_email as empc_alt_email
    ,ec.work_phone as empc_work_phone
    ,ec.home_phone as empc_home_phone
    ,ec.cell_phone as empc_cell_phone
    ,ec.fax as empc_fax
    ,ec.street as empc_street
    ,ec.address1 as empc_contact_address1
    ,ec.address2 as empc_contact_address2
    ,ec.city as empc_city
    ,ec.state as empc_state
    ,ec.postal_code as empc_postal_code
    ,ec.country as empc_country
    ,ec.is_primary as empc_is_primary
    ,ec.contact_person_name as empc_contact_person_name
    ,ec.years_of_age as empc_years_of_age
    ,le.description as empc_description
    ,ec.contact_position as empc_contact_position
    ,ec.employee_number as empc_employee_number        
	,ec.military_status as empc_military_status
	,cb.display_name as empc_created_by_name
	,cb.email as empc_created_by_email
	,mb.display_name as empc_modified_by_name
	,mb.email as empc_modified_by_email
FROM public.dim_employee_contact ec
LEFT JOIN dim_users cb ON ec.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ec.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (ec.employee_id = view_empl.empl_id and ec.customer_id = view_empl.cust_id and ec.employer_id = view_empl.eplr_id)
LEFT JOIN dim_lookup_empl_military_status le ON ec.military_status = le.code
WHERE ec.ver_is_current = true AND ec.is_deleted = false;


ALTER VIEW public.view_empc
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empc TO postgres;
GRANT SELECT ON TABLE public.view_empc TO absence_tracker;
GRANT ALL ON TABLE public.view_empc TO etldw;