﻿-- View: public.view_user

--DROP VIEW IF EXISTS public.view_user CASCADE;

CREATE OR REPLACE VIEW public.view_user AS
 SELECT 
	view_cust.*,
	dim_users.ext_ref_id AS user_id,
    dim_users.email AS user_user_email,
    dim_users.first_name AS user_user_first_name,
    dim_users.last_name AS user_user_last_name,
    dim_users.display_name AS user_user_display_name,
    dim_users.must_change_password AS user_must_change_password,
    dim_users.failed_login_attempts AS user_failed_login_attempts,
    dim_users.last_failed_login_attempt AS user_last_failed_login_attempt,
    dim_users.locked AS user_is_locked,
    dim_users.locked_date AS user_locked_date,
    dim_users.disabled AS user_disabled,
    dim_users.disabled_date AS user_disabled_date,
    dim_users.role_name AS user_role,
    dim_users.last_activity_date AS user_last_activity_date,
    dim_lookup_user_status.description AS user_user_status,
    dim_lookup_user_type.description AS user_user_type,
    dim_users.is_self_service_user AS user_is_ess,
    dim_users.is_portal_user AS user_is_portal,
    dim_users.user_employee_id,
    dim_users.user_employee_number,
    mview_fact_user.count_cases AS user_user_cases,
    mview_fact_user.count_open_cases AS user_user_open_cases,
    mview_fact_user.count_closed_cases AS user_user_closed_cases,
    mview_fact_user.count_todos AS user_user_todos,
    mview_fact_user.count_open_todos AS user_user_open_todos,
    mview_fact_user.count_closed_todos AS user_user_closed_todos,
    mview_fact_user.count_overdue_todos AS user_user_overdue_todos,

	cb.display_name AS user_created_by_name,
	cb.email AS user_created_by_email,
	mb.display_name AS user_modified_by_name,
	mb.email AS user_modified_by_email

   FROM dim_users
     LEFT JOIN mview_fact_user ON dim_users.ext_ref_id = mview_fact_user.user_ref_id
     LEFT JOIN view_cust ON dim_users.customer_id = view_cust.cust_id
     LEFT JOIN dim_lookup_user_status ON dim_users.user_status = dim_lookup_user_status.code
     LEFT JOIN dim_lookup_user_type ON dim_users.user_type = dim_lookup_user_type.code
	 LEFT JOIN dim_users cb ON dim_users.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_users.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_users.is_deleted = false AND dim_users.ver_is_current = true;

ALTER VIEW public.view_user
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_user TO postgres;
GRANT SELECT ON TABLE public.view_user TO absence_tracker;
GRANT ALL ON TABLE public.view_user TO etldw;
