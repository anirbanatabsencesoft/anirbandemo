﻿-- View: public.view_asbm

--DROP VIEW IF EXISTS public.view_asbm CASCADE;

CREATE OR REPLACE VIEW public.view_asbm AS
 SELECT
	view_case.*,
	dim_case_authorized_submitter.employee_id  as asbm_employee_Id,
	dim_case_authorized_submitter.customer_id as asbm_customer_id,
	dim_case_authorized_submitter.contact_type_code as asbm_contact_type_code,
	dim_case_authorized_submitter.contact_type_name as asbm_contact_type_name,
	dim_case_authorized_submitter.contact_first_name as asbm_contact_first_name ,
	dim_case_authorized_submitter.contact_last_name as asbm_contact_last_name ,
	dim_case_authorized_submitter.contact_email as asbm_contact_email ,
	dim_case_authorized_submitter.contact_alt_email as asbm_contact_alt_email ,
	dim_case_authorized_submitter.contact_work_phone as asbm_contact_work_phone ,
	dim_case_authorized_submitter.contact_home_phone as asbm_contact_home_phone ,
	dim_case_authorized_submitter.contact_cell_phone as asbm_contact_cell_phone ,
	dim_case_authorized_submitter.contact_fax as asbm_contact_fax ,
	dim_case_authorized_submitter.contact_street as asbm_contact_street ,
	dim_case_authorized_submitter.contact_address1 as asbm_contact_address1 ,
	dim_case_authorized_submitter.contact_address2 as asbm_contact_address2 ,
	dim_case_authorized_submitter.contact_city as asbm_contact_city ,
	dim_case_authorized_submitter.contact_state as asbm_contact_state ,
	dim_case_authorized_submitter.contact_postal_code as asbm_contact_postal_code ,
	dim_case_authorized_submitter.contact_country as asbm_contact_country ,

	cb.display_name as asbm_created_by_name,
	cb.email as asbm_created_by_email,
	mb.display_name as asbm_modified_by_name,
	mb.email as asbm_modified_by_email

   FROM dim_case_authorized_submitter
	 INNER JOIN view_case ON dim_case_authorized_submitter.ext_ref_id = view_case.case_id
	 LEFT JOIN dim_users cb ON dim_case_authorized_submitter.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_authorized_submitter.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_authorized_submitter.ver_is_current = true
     AND dim_case_authorized_submitter.is_deleted = false;

ALTER VIEW public.view_asbm
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_asbm TO postgres;
GRANT SELECT ON TABLE public.view_asbm TO absence_tracker;
GRANT ALL ON TABLE public.view_asbm TO etldw;
