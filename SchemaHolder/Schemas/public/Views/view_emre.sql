﻿-- View: public.view_emre

--DROP VIEW IF EXISTS public.view_emre CASCADE;

CREATE OR REPLACE VIEW public.view_emre AS
SELECT 
	view_wkrs.*
	,ere.dim_employee_restriction_entry_id as view_employee_restriction_entry_id    
    ,ere.wkrs_restriction_type as emre_wkrs_restriction_type
    ,ere.wkrs_restriction as emre_wkrs_restriction

	,view_demt.demt_id    
	,view_demt.demt_modified_date
	,view_demt.demt_created_date   
	,view_demt.demt_description
	,view_demt.demt_type_data
	,view_demt.demt_order_data
	,view_demt.demt_requirement_label
	,view_demt.demt_restriction_label
	,view_demt.demt_code    
	,view_demt.demt_created_by_name
	,view_demt.demt_created_by_email
	,view_demt.demt_modified_by_name
	,view_demt.demt_modified_by_email
    
FROM public.dim_employee_restriction_entry ere
LEFT JOIN view_wkrs ON ere.dim_employee_restriction_id = view_wkrs.view_employee_restriction_id
LEFT JOIN view_demt on ere.ext_ref_demand_type_id = view_demt.demt_id
WHERE ere.is_deleted = FALSE and ere.ver_is_current = true;

ALTER VIEW public.view_emre
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_emre TO postgres;
GRANT SELECT ON TABLE public.view_emre TO absence_tracker;
GRANT ALL ON TABLE public.view_emre TO etldw;