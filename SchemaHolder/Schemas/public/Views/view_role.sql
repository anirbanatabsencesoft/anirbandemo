﻿-- View: public.view_role

--DROP VIEW IF EXISTS public.view_role CASCADE;

CREATE OR REPLACE VIEW public.view_role AS
SELECT 
	view_cust.*
	,r.dim_role_id as view_role_id
    ,r.ext_ref_id as role_id
    ,r.ext_created_date as role_created_date
    ,r.ext_modified_date as role_modified_date   
    ,r.name as role_name
    ,r.permissions as role_permissions
    ,role_type.description as role_role_type

	,cb.display_name AS role_created_by_name
	,cb.email AS role_created_by_email
	,mb.display_name AS role_modified_by_name
	,mb.email AS role_modified_by_email
    
FROM public.dim_role r
LEFT JOIN dim_users cb ON r.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON r.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON r.customer_id = view_cust.cust_id
LEFT JOIN dim_lookup role_type ON r.role_type = role_type.code
WHERE role_type.type = 'role_type' and r.ver_is_current = true AND r.is_deleted = false;

ALTER VIEW public.view_role
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_role TO postgres;
GRANT SELECT ON TABLE public.view_role TO absence_tracker;
GRANT ALL ON TABLE public.view_role TO etldw;