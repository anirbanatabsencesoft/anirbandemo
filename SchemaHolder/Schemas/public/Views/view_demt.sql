﻿-- View: public.view_demt

DROP VIEW IF EXISTS public.view_demt CASCADE;

CREATE OR REPLACE VIEW public.view_demt AS
SELECT 
	view_cust.*
	,dt.dim_demand_type_id as view_demand_type_id
    ,dt.ext_ref_id as demt_id    
    ,dt.ext_modified_date as demt_modified_date
    ,dt.created_date as demt_created_date   
    ,dt.description as demt_description
    ,dt.type_data as demt_type_data
    ,dt.order_data as demt_order_data
    ,dt.requirement_label as demt_requirement_label
    ,dt.restriction_label as demt_restriction_label
    ,dt.code as demt_code    
  
	,cb.display_name as demt_created_by_name
	,cb.email as demt_created_by_email
	,mb.display_name as demt_modified_by_name
	,mb.email as demt_modified_by_email

FROM public.dim_demand_type dt
LEFT JOIN dim_users cb ON dt.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON dt.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON dt.customer_id = view_cust.cust_id
 WHERE dt.ver_is_current = true
     AND dt.is_deleted = false;

ALTER VIEW public.view_demt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_demt TO postgres;
GRANT SELECT ON TABLE public.view_demt TO absence_tracker;
GRANT ALL ON TABLE public.view_demt TO etldw;