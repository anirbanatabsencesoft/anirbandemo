﻿-- View: public.view_empj

--DROP VIEW IF EXISTS public.view_empj CASCADE;

CREATE OR REPLACE VIEW public.view_empj AS
SELECT
	view_empl.*
	,ej.dim_employee_job_id as view_employee_job_id
    ,ej.ext_ref_id as empj_id
    ,ej.ext_created_date as empj_created_date
    ,ej.ext_modified_date as empj_modified_date
    ,ej.job_name as empj_job_name
    ,ej.job_code as empj_job_code    
    ,ej.status_reason as empj_status_reason
    ,ej.status_comments as empj_status_comments
    ,ej.status_username as empj_status_username
    ,ej.status_userid as empj_status_userid
    ,ej.status_date as empj_status_date
    ,ej.start_date as empj_start_date
    ,ej.end_date as empj_end_date
	,empl_job_adjudication_status.description as empj_description

	,cb.display_name as empj_created_by_name
	,cb.email as empj_created_by_email
	,mb.display_name as empj_modified_by_name
	,mb.email as empj_modified_by_email

FROM public.dim_employee_job ej
LEFT JOIN dim_users cb ON ej.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON ej.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (ej.employee_id = view_empl.empl_id and ej.customer_id = view_empl.cust_id and ej.employer_id = view_empl.eplr_id)
LEFT JOIN dim_lookup empl_job_adjudication_status ON ej.status_value = empl_job_adjudication_status.code
WHERE ej.ver_is_current = TRUE AND ej.is_deleted = FALSE;

ALTER VIEW public.view_empj
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empj TO postgres;
GRANT SELECT ON TABLE public.view_empj TO absence_tracker;
GRANT ALL ON TABLE public.view_empj TO etldw;