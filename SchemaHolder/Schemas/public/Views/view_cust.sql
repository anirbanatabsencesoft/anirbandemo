﻿-- View: public.view_cust

DROP VIEW IF EXISTS public.view_cust CASCADE;

CREATE OR REPLACE VIEW public.view_cust AS
 SELECT
	dim_customer.ext_ref_id as cust_id,
    dim_customer.cust_customer_name,
    dim_customer.cust_customer_url,
    dim_customer.cust_contact_first_name,
    dim_customer.cust_contact_last_name,
    dim_customer.cust_contact_email,
    dim_customer.cust_contact_address1,
    dim_customer.cust_contact_address2,
    dim_customer.cust_contact_city,
    dim_customer.cust_contact_state,
    dim_customer.cust_contact_postal_code,
    dim_customer.cust_contact_country,
    dim_customer.cust_billing_first_name,
    dim_customer.cust_billing_last_name,
    dim_customer.cust_billing_email,
    dim_customer.cust_billing_address1,
    dim_customer.cust_billing_address2,
    dim_customer.cust_billing_city,
    dim_customer.cust_billing_state,
    dim_customer.cust_billing_postal_code,
    dim_customer.cust_billing_country,
    dim_customer.ext_created_date as cust_customer_created_date,
    COALESCE(mview_fact_customer.count_employers, 0::bigint) as cust_customer_employer_count,
    COALESCE(mview_fact_customer.count_employees, 0::bigint) as cust_customer_employee_count,
    COALESCE(mview_fact_customer.count_cases, 0::bigint) AS cust_customer_case_count,
    COALESCE(mview_fact_customer.count_open_cases, 0::bigint) as cust_customer_open_cases,
    COALESCE(mview_fact_customer.count_closed_cases, 0::bigint) as cust_customer_closed_cases,
    COALESCE(mview_fact_customer.count_todos, 0::bigint) as cust_customer_todos,
    COALESCE(mview_fact_customer.count_open_todos, 0::bigint) as cust_customer_open_todos,
    COALESCE(mview_fact_customer.count_closed_todos, 0::bigint) as cust_customer_closed_todos,
    COALESCE(mview_fact_customer.count_overdue_todos, 0::bigint) as cust_customer_overdue_todos,
    COALESCE(mview_fact_customer.count_users, 0::bigint) as cust_customer_users,
	
	cb.display_name as cust_created_by_name,
	cb.email as cust_created_by_email,
	mb.display_name as cust_modified_by_name,
	mb.email as cust_modified_by_email

   FROM dim_customer
     LEFT JOIN mview_fact_customer ON dim_customer.ext_ref_id = mview_fact_customer.customer_ref_id
	 LEFT JOIN dim_users cb ON dim_customer.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_customer.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_customer.ver_is_current = true
     AND dim_customer.is_deleted = false;

ALTER VIEW public.view_cust
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cust TO postgres;
GRANT SELECT ON TABLE public.view_cust TO absence_tracker;
GRANT ALL ON TABLE public.view_cust TO etldw;
