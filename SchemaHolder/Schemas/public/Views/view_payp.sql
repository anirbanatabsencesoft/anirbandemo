﻿-- View: public.view_payp

--DROP VIEW IF EXISTS public.view_payp CASCADE;

CREATE OR REPLACE VIEW public.view_payp AS
 SELECT
	view_case.*,
	dim_case_pay_period.dim_case_pay_period_id as view_case_pay_period_Id,
	dim_case_pay_period.ext_ref_id as payp_id,
	dim_case_pay_period.start_date AS payp_start_date,
	dim_case_pay_period.end_date AS payp_end_date,
	lookup_status_type.description AS payp_status,
	dim_case_pay_period.locked_date AS payp_locked_date,	
	lb.display_name AS payp_locked_by_name,
	dim_case_pay_period.max_weekly_pay_amount  AS payp_max_weekly_pay_amount,
	dim_case_pay_period.payroll_date_override AS payp_payroll_date_override,
	dim_case_pay_period.end_date_override AS payp_end_date_override,
	dim_case_pay_period.payroll_date AS payp_payroll_date,	
	pdob.display_name AS payp_payroll_date_override_by_name,
	edob.display_name AS payp_end_date_override_by_name,
	dim_case_pay_period.offset_amount  AS payp_offset_amount,
	dim_case_pay_period.total  AS payp_total,
	dim_case_pay_period.sub_total AS payp_sub_total,
	dim_case_pay_period.payroll_date_override_by AS payp_payroll_date_override_by,
	dim_case_pay_period.end_date_override_by AS payp_end_date_override_by,
	dim_case_pay_period.locked_by AS payp_locked_by,
	cb.display_name AS payp_created_by_name,
	cb.email AS payp_created_by_email,
	mb.display_name AS payp_modified_by_name,
	mb.email AS payp_modified_by_email

   FROM dim_case_pay_period
	 LEFT JOIN view_case ON dim_case_pay_period.case_id = view_case.case_id
	 LEFT JOIN dim_lookup lookup_status_type ON dim_case_pay_period.status = lookup_status_type.code AND lookup_status_type.type = 'payroll_status'
	 LEFT JOIN dim_users lb ON dim_case_pay_period.locked_by = lb.ext_ref_id AND lb.ver_is_current = true
	 LEFT JOIN dim_users pdob ON dim_case_pay_period.end_date_override_by = pdob.ext_ref_id AND pdob.ver_is_current = true
	 LEFT JOIN dim_users edob ON dim_case_pay_period.payroll_date_override_by = edob.ext_ref_id AND edob.ver_is_current = true	 
	 LEFT JOIN dim_users cb ON dim_case_pay_period.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_pay_period.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_pay_period.ver_is_current = true
     AND dim_case_pay_period.is_deleted = false;

ALTER VIEW public.view_payp
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_payp TO postgres;
GRANT SELECT ON TABLE public.view_payp TO absence_tracker;
GRANT ALL ON TABLE public.view_payp TO etldw;
