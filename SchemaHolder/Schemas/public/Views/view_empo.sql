﻿-- View: public.view_empo

DROP VIEW IF EXISTS public.view_empo CASCADE;

CREATE OR REPLACE VIEW public.view_empo AS
SELECT 
     view_eplr.*
	,eo.dim_employee_organization_id as view_employee_organization_id
    ,eo.ext_ref_id as empo_id
    ,eo.ext_created_date as empo_created_date
    ,eo.ext_modified_date as empo_modified_date   
    ,eo.employee_number as empo_employee_number
    ,eo.code as empo_code
    ,eo.name as empo_name
    ,eo.description as empo_description
    ,eo.path as empo_path
    ,eo.type_code as empo_type_code    
    
	,cb.display_name as empo_created_by_name
	,cb.email as empo_created_by_email
	,mb.display_name as empo_modified_by_name
	,mb.email as empo_modified_by_email

FROM public.dim_employee_organization eo
LEFT JOIN dim_users cb ON eo.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON eo.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_eplr ON (eo.customer_id = view_eplr.cust_id and eo.employer_id = view_eplr.eplr_id)
where eo.ver_is_current = true AND eo.is_deleted = false;
ALTER VIEW public.view_empo
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empo TO postgres;
GRANT SELECT ON TABLE public.view_empo TO absence_tracker;
GRANT ALL ON TABLE public.view_empo TO etldw;