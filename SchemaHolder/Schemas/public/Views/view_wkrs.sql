﻿-- View: public.view_wkrs

-- DROP VIEW public.view_wkrs;
--DROP VIEW IF EXISTS public.view_wkrs CASCADE;


CREATE OR REPLACE VIEW public.view_wkrs AS
 SELECT 
	view_case.*,
	dim_employee_restriction.dim_employee_restriction_id as view_employee_restriction_id,
	dim_employee_restriction.ext_ref_id as wkrs_id,
    dim_employee_restriction.start_date AS wkrs_restriction_start_date,
    dim_employee_restriction.end_date AS wkrs_restriction_end_date,
    dim_employee_restriction.wkrs_restriction_name,
    dim_employee_restriction.wkrs_restriction_code,
    dim_employee_restriction.wkrs_restriction_category,
	dim_employee_restriction_entry.wkrs_restriction_type,
    dim_employee_restriction_entry.wkrs_restriction,

   	cb.display_name AS wkrs_created_by_name,
	cb.email AS wkrs_created_by_email,
	mb.display_name AS wkrs_modified_by_name,
	mb.email AS wkrs_modified_by_email

   FROM dim_employee_restriction   
	 LEFT JOIN dim_employee_restriction_entry ON dim_employee_restriction.dim_employee_restriction_id = dim_employee_restriction_entry.dim_employee_restriction_id
     LEFT JOIN view_case ON dim_employee_restriction.case_id = view_case.case_id
	 LEFT JOIN dim_users cb ON dim_employee_restriction.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employee_restriction.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_employee_restriction.ver_is_current = true
     AND dim_employee_restriction.is_deleted = false;

ALTER VIEW public.view_wkrs
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_wkrs TO postgres;
GRANT SELECT ON TABLE public.view_wkrs TO absence_tracker;
GRANT ALL ON TABLE public.view_wkrs TO etldw;
