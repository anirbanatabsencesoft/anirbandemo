﻿-- View: public.view_poly


--DROP VIEW IF EXISTS public.view_poly CASCADE; 

CREATE OR REPLACE VIEW public.view_poly AS
	
	SELECT 
	  view_case.*,
	  dim_case_policy.dim_case_policy_id AS view_poly_id, 
	  dim_case_policy.policy_code AS poly_code, 
	  dim_case_policy.policy_name AS poly_name, 
	  dim_case_policy.start_date AS poly_start_date, 
	  dim_case_policy.end_date AS poly_end_date, 
	  dim_lookup_case_types.description AS poly_case_type, 
	  eligibility.description AS poly_eligibility_status, 
	  determination.description AS poly_determination, 
	  denial_reason.description AS poly_denial_reason, 
	  dim_case_policy.denial_reason_other AS poly_other_denial_reason, 
	  dim_case_policy.payout_percentage AS poly_payout_percentage,
	  dim_case_policy.crosswalk_codes AS poly_crosswalk_codes,	 
	  dim_case_policy.actual_duration AS poly_actual_duration,
	  dim_case_policy.exceeded_duration AS poly_exceeded_duration,
	  dim_case_policy.denial_reason_code AS poly_denial_reason_code, 
	  dim_case_policy.denial_reason_description AS poly_denial_reason_description,
	  segm_case_type.description AS segm_case_type
	FROM 
	public.dim_case_policy
	
	LEFT JOIN view_case ON dim_case_policy.case_id = view_case.case_id
	LEFT JOIN dim_lookup_case_types ON dim_case_policy.case_type = dim_lookup_case_types.code
	LEFT JOIN dim_lookup eligibility ON dim_case_policy.eligibility = eligibility.code AND eligibility.type = 'eligibility_status'
	LEFT JOIN dim_lookup_case_determination determination ON dim_case_policy.determination = determination.code
	LEFT JOIN dim_lookup denial_reason ON dim_case_policy.denial_reason = denial_reason.code AND denial_reason.type = 'case_denial_reason'
	LEFT JOIN dim_case_segment on dim_case_policy.segment_id  = dim_case_segment.ext_ref_id
				and dim_case_segment.ver_is_current = true
				and dim_case_segment.is_deleted = false
	LEFT JOIN dim_lookup_case_types segm_case_type ON dim_case_segment.case_type = segm_case_type.code
				
	
	WHERE dim_case_policy.ver_is_current = true
	AND dim_case_policy.is_deleted = false ;


ALTER VIEW public.view_poly OWNER TO postgres;

GRANT ALL ON TABLE public.view_poly TO postgres;
GRANT SELECT ON TABLE public.view_poly TO absence_tracker;
GRANT ALL ON TABLE public.view_poly TO etldw;
