﻿-- View: public.view_evnt


--DROP VIEW IF EXISTS public.view_evnt CASCADE;

CREATE OR REPLACE VIEW public.view_evnt AS
SELECT 
	view_todo.*
	,evt.dim_event_id as view_event_id
    ,evt.ext_ref_id as evnt_id
    ,evt.ext_created_date as evnt_created_date
    ,evt.ext_modified_date as evnt_modified_date   
	,evt.active_accomodation_id as evnt_active_accomodation_id
    ,evt.accomodation_type_code as evnt_accomodation_type_code
    ,evt.accomodation_end_date as evnt_accomodation_end_date
    ,evt.determination as evnt_determination
    ,evt.denial_explanation as evnt_denial_explanation
    ,evt.case_id AS evnt_event_case_id
    ,evt.accomodation_type_id as evnt_accomodation_type_id
    ,evt.template AS evnt_event_template
    ,communication_type.description AS evnt_communication_Desc
    ,evt.name as evnt_name
    ,evt.event_type as evnt_event_type    
    ,evt.applied_policy_id as evnt_applied_policy_id
    ,evt.close_case as evnt_close_case
    ,evt.communication_id as evnt_communication_id
    ,evt.condition_start_date as evnt_condition_start_date
    ,evt.contact_employee_due_date as evnt_contact_employee_due_date
    ,evt.contact_hcp_due_date as evnt_contact_hcp_due_date
    ,evt.denial_reason as evnt_denial_reason
    ,evt.description as evnt_event_description
    ,evt.due_date as evnt_event_due_date
    ,evt.ergonomic_assessment_date as evnt_ergonomic_assessment_date
    ,evt.extended_grace_period as evnt_extended_grace_period
    ,evt.first_exhaustion_date as evnt_first_exhaustion_date
    ,evt.general_health_condition as evnt_general_health_condition
    ,evt.has_work_restriction as evnt_has_work_restriction
    ,evt.illness_injury_date as evnt_illness_injury_date
    ,evt.new_due_date as evnt_new_due_date
    ,evt.paperwork_attachment_id as evnt_paperwork_attachment_id
    ,evt.paperwork_due_date as evnt_paperwork_due_date
    ,evt.paperwork_id as evnt_paperwork_id
    ,evt.paperwork_name as evnt_paperwork_name
    ,evt.paperwork_received as evnt_paperwork_received
    ,evt.policy_code as evnt_policy_code
    ,evt.policy_id as evnt_policy_id
    ,evt.reason as evnt_reason
    ,evt.requires_review as evnt_requires_review
    ,evt.return_attachment_id as evnt_return_attachment_id
    ,evt.return_to_work_date as evnt_return_to_work_date
    ,evt.rtw as evnt_rtw
    ,evt.source_workflow_activity_activityid as evnt_source_workflow_activity_activityid
    ,evt.source_workflow_activityid as evnt_source_workflow_activityid
    ,evt.source_workflow_instanceid as evnt_source_workflow_instanceid
    ,evt.workflow_activity_id as evnt_workflow_activity_id ,

	ac.acom_id,
	ac.acom_dim_accommodation_id,
	ac.acom_accommodation_type,
	ac.acom_accommodation_type_code,
	ac.acom_accommodation_duration,
	ac.acom_accommodation_resolution,
	ac.acom_accommodation_resolved,
	ac.acom_accommodation_resolved_date,
	ac.acom_accommodation_granted,
	ac.acom_accommodation_granted_date,
	ac.acom_accommodation_cost,
	ac.acom_accommodation_start_date,
	ac.acom_accommodation_end_date,
	ac.acom_accommodation_determination,
	ac.acom_accommodation_min_approved_date,
	ac.acom_accommodation_max_approved_date,
	ac.acom_accommodation_created_date,
	ac.acom_accommodation_status,
	ac.acom_accommodation_cancel_reason,
	ac.acom_accommodation_is_work_related,
	ac.acom_created_by_name,
	ac.acom_created_by_email,
	ac.acom_modified_by_name,
	ac.acom_modified_by_email,

	at.attm_id,
	at.attm_content_length,
	at.attm_content_type,
	at.attm_attachment_type,
	at.attm_created_by_name,
	at.attm_description,
	at.attm_file_id,
	at.attm_file_Name,
	at.attm_public,    
	at.attm_created_by_email,
	at.attm_modified_by_name,
	at.attm_modified_by_email,
   
	co.comm_id,
	co.comm_body ,
	co.comm_communication_type, 
	co.comm_email_replies_type,
	co.comm_created_by_email,
	co.comm_created_by_name,
	co.comm_is_draft,
	co.comm_name,
	co.comm_public,
	co.comm_first_send_date,
	co.comm_last_send_date,	
	co.comm_subject,
	co.comm_template,
	co.comm_created_date,
	co.comm_modified_by_name,
	co.comm_modified_by_email,

	pw.prwk_id,
	pw.prwk_checked ,
	pw.prwk_due_date ,
	pw.prwk_status,
	pw.prwk_status_date,	
	pw.prwk_paperwork_id ,
	pw.prwk_paperwork_cdt ,
	pw.prwk_paperwork_code ,
	pw.prwk_paperwork_description ,
	pw.prwk_paperwork_doc_type,
	pw.prwk_paperwork_enclosure_text ,
	pw.prwk_paperwork_file_id ,
	pw.prwk_paperwork_file_name ,
	pw.prwk_paperwork_mdt ,
	pw.prwk_paperwork_name ,
	pw.prwk_paperwork_requires_review ,
	pw.prwk_paperwork_return_date_adjustment,	
	pw.prwk_paperwork_return_date_adjustment_days 	
	 
   ,cu.display_name AS evnt_created_by_name
	,cu.email AS evnt_created_by_email
	,mu.display_name AS evnt_modified_by_name
	,mu.email AS evnt_modified_by_email

FROM public.dim_event evt

LEFT JOIN dim_users cu ON evt.ext_created_by = cu.ext_ref_id AND cu.ver_is_current = true
LEFT JOIN dim_users mu ON evt.ext_modified_by = mu.ext_ref_id AND mu.ver_is_current = true
LEFT JOIN view_todo ON evt.todo_item_id = view_todo.todo_id
LEFT JOIN view_acom ac ON evt.accomodation_id = ac.acom_id
LEFT JOIN view_attm at ON evt.attachment_id = at.attm_id
LEFT JOIN view_comm co ON evt.communication_id=co.comm_id
LEFT JOIN view_prwk pw ON evt.paperwork_id=pw.prwk_id
LEFT JOIN dim_lookup communication_type ON evt.communication_type = communication_type.code
WHERE communication_type.type = 'communication_type' and evt.ver_is_current = true and evt.is_deleted = false;

ALTER VIEW public.view_evnt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_evnt TO postgres;
GRANT SELECT ON TABLE public.view_evnt TO absence_tracker;
GRANT ALL ON TABLE public.view_evnt TO etldw;