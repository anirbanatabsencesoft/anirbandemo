﻿-- View: public.view_cnot


--DROP VIEW IF EXISTS public.view_cnot CASCADE;

CREATE OR REPLACE VIEW public.view_cnot AS
 SELECT
	view_case.*,
	dim_case_note.ext_ref_id as cnot_id,
    dim_case_note.accomm_id  as cnot_accomm_id ,
	dim_case_note.accomm_question_id  as cnot_accomm_question_id ,
	dim_case_note.answer  as cnot_answer ,	
	dim_case_note.cert_id     as cnot_cert_id ,
	dim_case_note.contact     as cnot_contact ,
	dim_case_note.copied_from     as cnot_copied_from ,
	dim_case_note.copied_from_employee    as cnot_copied_from_employee ,
	dim_case_note.employee_number     as cnot_employee_number ,
	dim_case_note.entered_by_name     as cnot_entered_by_name ,
	dim_case_note.notes   as cnot_notes ,
	dim_case_note.process_path as cnot_process_path ,
	dim_case_note.request_date as cnot_request_date ,
	dim_case_note.request_id  as cnot_request_id ,
	dim_case_note.s_answer as cnot_s_answer ,
	dim_case_note.work_restriction_id as cnot_work_restriction_id ,	
	dim_case_note.category_code  as cnot_category_code ,
	dim_case_note.category_name  as cnot_category_name ,	
	CASE WHEN dim_lookup.description IS NOT NULL THEN dim_lookup.description ELSE dim_case_note.category_name END as cnot_category,
	cb.display_name as cnot_created_by_name,
	cb.email as cnot_created_by_email,
	mb.display_name as cnot_modified_by_name,
	mb.email as cnot_modified_by_email

   FROM dim_case_note
	 INNER JOIN view_case ON dim_case_note.case_id = view_case.case_id
     LEFT JOIN dim_lookup  ON dim_case_note.category = dim_lookup.code and dim_lookup.type='note_category'
     LEFT JOIN dim_users cb ON dim_case_note.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_note.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_note.ver_is_current = true
     AND dim_case_note.is_deleted = false;

ALTER VIEW public.view_cnot
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_cnot TO postgres;
GRANT SELECT ON TABLE public.view_cnot TO absence_tracker;
GRANT ALL ON TABLE public.view_cnot TO etldw;
