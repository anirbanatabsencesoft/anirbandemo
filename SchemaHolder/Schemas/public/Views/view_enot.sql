﻿-- View: public.view_enot

--DROP VIEW IF EXISTS public.view_enot CASCADE;

CREATE OR REPLACE VIEW public.view_enot AS
SELECT 
	view_empl.*
	,en.dim_employee_note_id as view_employee_note_id
    ,en.ext_ref_id as enot_id
    ,en.ext_created_date as enot_created_date
    ,en.ext_modified_date as enot_modified_date  
    ,en.note as enot_note
    ,en.is_public as enot_is_public
    ,en.category_code as enot_category_code
    ,en.category_name as enot_category_name

	,cb.display_name AS enot_created_by_name
	,cb.email AS enot_created_by_email
	,mb.display_name AS enot_modified_by_name
	,mb.email AS enot_modified_by_email

FROM public.dim_employee_note en
LEFT JOIN dim_users cb ON en.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON en.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_empl ON (en.employee_id = view_empl.empl_id and en.customer_id =view_empl.cust_id and en.employer_id = view_empl.eplr_id)
WHERE en.ver_is_current = true;

ALTER VIEW public.view_enot
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_enot TO postgres;
GRANT SELECT ON TABLE public.view_enot TO absence_tracker;
GRANT ALL ON TABLE public.view_enot TO etldw;