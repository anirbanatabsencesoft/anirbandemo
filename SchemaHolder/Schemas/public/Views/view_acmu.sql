﻿
-- View: public.view_acmu

DROP VIEW IF EXISTS public.view_acmu CASCADE;

CREATE OR REPLACE VIEW public.view_acmu AS
SELECT
	view_acom.*	
    ,au.ext_ref_id as acmu_id
    ,au.start_date as acmu_start_date
    ,au.end_date as acmu_end_date
    ,dim_lookup_case_accom_status.code as acmu_code
	,dim_lookup_case_accom_status.description as acmu_description
	,au.is_deleted as acmu_is_deleted
FROM public.dim_accommodation_usage au
LEFT JOIN view_acom ON au.dim_accommodation_id = view_acom.acom_dim_accommodation_id
LEFT JOIN dim_lookup_case_accom_status ON au.determination = dim_lookup_case_accom_status.code
WHERE au.is_deleted = FALSE;

ALTER VIEW public.view_acmu
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_acmu TO postgres;
GRANT SELECT ON TABLE public.view_acmu TO absence_tracker;
GRANT ALL ON TABLE public.view_acmu TO etldw;