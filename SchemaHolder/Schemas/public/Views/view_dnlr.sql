-- View: public.view_dnlr 

--DROP VIEW IF EXISTS public.view_dnlr CASCADE;

CREATE OR REPLACE VIEW public.view_dnlr AS
 SELECT 
	view_eplr.*
    ,dnlr.dim_denial_reason_id as view_denial_reason_id
	,dnlr.ext_ref_id  as dnlr_id
    ,dnlr.ext_created_date  as dnlr_created_date
    ,dnlr.ext_modified_date  as dnlr_modified_date    
	,dnlr.description as dnlr_description
	,dnlr.code as dnlr_code
	,dnlr.disabled_date as dnlr_disabled_date
	,cb.display_name as dnlr_created_by_name
	,cb.email as dnlr_created_by_email
	,mb.display_name as dnlr_modified_by_name
	,mb.email as dnlr_modified_by_email

FROM dim_denial_reason dnlr
LEFT JOIN dim_users cb ON dnlr.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON dnlr.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_eplr ON dnlr.customer_id = view_eplr.cust_id
WHERE dnlr.ver_is_current = true AND dnlr.is_deleted = false;

ALTER VIEW public.view_dnlr
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_dnlr TO etldw;
GRANT ALL ON TABLE public.view_dnlr TO absence_tracker;
GRANT ALL ON TABLE public.view_dnlr TO postgres;