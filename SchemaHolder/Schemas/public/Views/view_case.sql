﻿-- View: public.view_case

--DROP VIEW IF EXISTS public.view_case CASCADE;

CREATE OR REPLACE VIEW public.view_case AS
 SELECT 
	view_empl.*,
    dim_cases.ext_ref_id AS case_id,
    dim_cases.case_number AS case_case_number,
    dim_cases.employer_case_number AS case_employer_case_number,
    dim_cases.start_date AS case_case_start_date,
        CASE
            WHEN dim_cases.end_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.end_date
        END AS case_case_end_date,
    dim_lookup_case_status.description AS case_case_status,
    dim_cases.spouse_case AS case_spouse_case_number,
    dim_lookup_case_types.description AS case_case_type,
    dim_cases.case_reason,
    dim_lookup_case_cancel_reason.description AS case_cancel_reason,
    dim_cases.assigned_to_name AS case_assigned_to,
    dim_lookup_case_closure_reason.description AS case_closed_reason,
	dim_cases.case_closure_reason_other,
	dim_cases.case_closure_other_reason_details,
    dim_cases.case_primary_diagnosis_code,
    dim_cases.case_primary_daignosis_name,
    dim_cases.case_secondary_diagnosis_code,
    dim_cases.case_secondary_daignosis_name,
    dim_cases.primary_path_text AS case_primary_rtw_path,
    dim_cases.primary_path_min_days AS case_primary_rtw_path_min_days,
    dim_cases.primary_path_max_days AS case_primary_rtw_path_max_days,
    dim_cases.hospitalization AS case_hospitalization,
        CASE
            WHEN dim_cases.condition_start_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.condition_start_date
        END AS case_condition_start_date,
	dim_cases.medical_complications AS case_medical_complications,
	dim_cases.general_health_condition AS case_general_health_condition,
    dim_cases.related_case_number AS case_related_case_number,
    dim_lookup_case_related_type.description AS case_related_case_type,
	dim_cases.relationship_type_code AS case_relationship_type_code,
	dim_cases.relationship_type_name AS case_relationship_type_name,
	dim_cases.is_intermittent AS case_is_intermittent,
    dim_cases.case_cert_start_date,
    dim_cases.case_cert_end_date,
    dim_cases.occurances AS case_cert_occurances,
    dim_cases.frequency AS case_cert_frequency,
    dim_lookup_case_frequency_type.description AS case_cert_frequency_type,
    dim_lookup_case_frequency_unit_type.description AS case_cert_frequency_unit_type,
    dim_cases.duration AS case_cert_duration,
    dim_lookup_case_duration_type.description AS case_cert_duration_type,
    dim_cases.is_accommodation AS case_is_accommodation,
    dim_cases.is_work_related AS case_is_work_related,
    dim_cases.case_is_denied,
    dim_cases.case_is_approved,
		CASE
            WHEN dim_cases.case_is_eligible = 1 THEN 'True'::text 
			ELSE 'False'::text
		END AS case_is_eligible,
	dim_lookup_case_determination.description AS case_case_determination,
    dim_cases.case_fmla_is_approved,
    dim_cases.case_fmla_is_denied,
    dim_cases.case_fmla_projected_usage,
        CASE
            WHEN dim_cases.case_fmla_exhaust_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_fmla_exhaust_date
        END AS case_fmla_exhaust_date,
        CASE
            WHEN dim_cases.case_max_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_max_approved_thru_date
        END AS case_case_max_approved_thru_date,
        CASE
            WHEN dim_cases.case_min_approved_thru_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_min_approved_thru_date
        END AS case_case_min_approved_thru_date,
    dim_lookup_case_accom_status.description AS case_accom_status,
    dim_cases.case_accom_start_date,
    dim_cases.case_accom_end_date,
        CASE
            WHEN dim_cases.case_accom_min_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_min_approved_date
        END AS case_accom_min_approved_date,
        CASE
            WHEN dim_cases.case_accom_max_approved_date = '0001-01-01 00:00:00'::timestamp without time zone THEN NULL::timestamp without time zone
            ELSE dim_cases.case_accom_max_approved_date
        END AS case_accom_max_approved_date,
    dim_lookup_case_accom_determination.description AS case_accom_determination,
    dim_lookup_case_accom_cancel_reason.description AS case_accom_cancel_reason,
    dim_cases.case_accom_cancel_reason_other,
    dim_cases.case_accom_duration,
    dim_cases.case_accom_duration_type,
    dim_cases.ext_created_date AS case_case_created_date,
    dim_cases.case_created_by_email AS case_case_created_by_email,
    dim_cases.case_created_by_first_name AS case_case_created_by_first_name,
    dim_cases.case_created_by_last_name AS case_case_created_by_last_name,
    dim_cases.return_to_work AS case_return_to_work,
    dim_cases.delivery_date AS case_delivery_date,
    dim_cases.bonding_start_date AS case_bonding_start_date,
    dim_cases.bonding_end_date AS case_bonding_end_date,
    dim_cases.illness_or_injury_date AS case_illness_or_injury_date,
    dim_cases.hospital_admission_date AS case_hospital_admission_date,
    dim_cases.hospital_release_date AS case_hospital_release_date,
    dim_cases.release_received AS case_release_received,
    dim_cases.paperwork_received AS case_paperwork_received,
    dim_cases.accommodation_added AS case_accommodation_added,
    dim_cases.case_closed AS case_case_closed,
    dim_cases.case_cancelled AS case_case_cancelled,
    dim_cases.adoption_date AS case_adoption_date,
    dim_cases.estimated_return_to_work AS case_estimated_return_to_work,
    dim_cases.employer_holiday_schedule_changed AS case_employer_holiday_schedule_changed,
    COALESCE(mview_fact_case.count_todos, 0::bigint) AS case_case_todos,
    COALESCE(mview_fact_case.count_open_todos, 0::bigint) AS case_case_open_todos,
    COALESCE(mview_fact_case.count_closed_todos, 0::bigint) AS case_case_closed_todos,
    COALESCE(mview_fact_case.count_overdue_todos, 0::bigint) AS case_case_overdue_todos,
    dim_cases.case_reporter_type_code,
    dim_cases.case_reporter_type_name,
    dim_cases.case_reporter_first_name,
    dim_cases.case_reporter_last_name,
    dim_cases.custom_fields AS case_custom_fields,
		
	dim_cases.wr_date_of_injury as case_wr_date_of_injury,
	dim_cases.wr_is_pvt_healthcare as case_wr_is_pvt_healthcare,
	dim_cases.wr_is_reportable as case_wr_is_reportable,
	dim_cases.wr_place_of_occurance as case_wr_place_of_occurance,
	work_classification.description as case_wr_work_classification,
	type_of_injury.description as case_wr_type_of_injury,
	dim_cases.wr_days_away_from_work as case_wr_days_away_from_work,
	dim_cases.wr_restricted_days as case_wr_restricted_days,
	dim_cases.wr_provider_first_name as case_wr_provider_first_name,
	dim_cases.wr_provider_last_name as case_wr_provider_last_name,
	dim_cases.wr_provider_company_name as case_wr_provider_company_name,
	dim_cases.wr_provider_address1 as case_wr_provider_address1,
	dim_cases.wr_provider_address2 as case_wr_provider_address2,
	dim_cases.wr_provider_city as case_wr_provider_city,
	dim_cases.wr_provider_state as case_wr_provider_state,
	dim_cases.wr_provider_country as case_wr_provider_country,
	dim_cases.wr_provider_postalcode as case_wr_provider_postalcode,
	dim_cases.wr_is_treated_in_emer_room as case_wr_is_treated_in_emer_room,
	dim_cases.wr_is_hospitalized as case_wr_is_hospitalized,
	dim_cases.wr_time_started_working as case_wr_time_started_working,
	dim_cases.wr_time_of_injury as case_wr_time_of_injury,
	dim_cases.wr_activity_before_injury as case_wr_activity_before_injury,
	dim_cases.wr_what_happened as case_wr_what_happened,
	dim_cases.wr_injury_details as case_wr_injury_details,
	dim_cases.wr_what_harmed_empl as case_wr_what_harmed_empl,
	dim_cases.wr_date_of_death as case_wr_date_of_death,
	dim_cases.wr_is_sharp as case_wr_is_sharp,
	dim_cases.wr_sharp_body_part as case_wr_sharp_body_part,
	dim_cases.wr_type_of_sharp as case_wr_type_of_sharp,
	dim_cases.wr_brand_of_sharp as case_wr_brand_of_sharp,
	dim_cases.wr_model_of_sharp as case_wr_model_of_sharp,
	dim_cases.wr_body_fluid as case_wr_body_fluid,
	dim_cases.wr_is_engn_injury_protection as case_wr_is_engn_injury_protection,
	dim_cases.wr_is_protective_mech_activated as case_wr_is_protective_mech_activated,
	sharp_incdnt_when.description as case_wr_when_sharp_incident_happened,
	sharp_job_class.description as case_wr_sharp_job_classification,
	dim_cases.wr_sharp_other_job_classification as case_wr_sharp_other_job_classification,
	sharp_location_dept.description as case_wr_sharp_location_department,
	dim_cases.wr_sharp_other_location_dept as case_wr_sharp_other_location_dept,
	sharp_procedure.description as case_wr_sharp_procedure,
	dim_cases.wr_sharp_other_procedure as case_wr_sharp_other_procedure,
	dim_cases.wr_sharp_exposure_detail as case_wr_sharp_exposure_detail,

	dim_cases.cg_risk_asmt_score as case_cg_risk_asmt_score,
	dim_cases.cg_risk_asmt_status as case_cg_risk_asmt_status,
	dim_cases.cg_mid_range_all_absence as case_cg_mid_range_all_absence,
	dim_cases.cg_at_risk_all_absence as case_cg_at_risk_all_absence,
	dim_cases.cg_best_practice_days as case_cg_best_practice_days,
	dim_cases.cg_actual_days_lost as case_cg_actual_days_lost,

	dim_cases.closure_reason as case_closure_reason,
	dim_cases.closure_reason_detail as case_closure_reason_detail,
	dim_cases.description as case_description,
	dim_cases.narrative as case_narrative,
	dim_cases.is_rpt_auth_submitter as case_is_rpt_auth_submitter,
	dim_cases.send_e_communication case_send_e_communication,
	dim_cases.e_communication_request_date case_e_communication_request_date,
	dim_cases.risk_profile_code case_risk_profile_code,
	dim_cases.risk_profile_name case_risk_profile_name,
	dim_cases.current_office_location case_current_office_location,
	dim_cases.total_paid case_pay_total_paid,
	dim_cases.total_offset case_pay_total_offset,
	dim_cases.apply_offsets_by_default case_pay_apply_offsets_by_default,
	dim_cases.waive_waiting_period case_pay_waive_waiting_period,
	dim_cases.pay_schedule_id case_pay_pay_schedule_id,
	
	dim_cases.alt_phone_number as case_alt_phone_number,
	dim_cases.alt_email as case_alt_email,
	dim_cases.alt_address1 as case_alt_address1,
	dim_cases.alt_address2 as case_alt_address2,
	dim_cases.alt_city as case_alt_city,
	dim_cases.alt_state as case_alt_state,
	dim_cases.alt_postal_code as case_alt_postal_code,
	dim_cases.alt_country as case_alt_country,

	cb.display_name AS case_created_by_name,
	cb.email AS case_created_by_email,
	mb.display_name AS case_modified_by_name,
	mb.email AS case_modified_by_email,
	intermittent_type.description As case_cert_intermittent_type,

	dim_cases.actual_duration AS case_actual_duration,
	dim_cases.exceeded_duration AS case_exceeded_duration,
	has_relapse AS case_has_relapse,
	COALESCE(mview_fact_case.count_relapses, 0::bigint) AS case_count_relapses

   FROM dim_cases
     LEFT JOIN mview_fact_case ON dim_cases.ext_ref_id = mview_fact_case.case_ref_id
     LEFT JOIN view_empl ON dim_cases.employee_id = view_empl.empl_id
     LEFT JOIN dim_lookup_case_accom_status ON dim_cases.case_accom_status = dim_lookup_case_accom_status.code
     LEFT JOIN dim_lookup_case_accom_cancel_reason ON dim_cases.case_accom_cancel_reason = dim_lookup_case_accom_cancel_reason.code
     LEFT JOIN dim_lookup_case_accom_determination ON dim_cases.case_accom_determination = dim_lookup_case_accom_determination.code
     LEFT JOIN dim_lookup_case_determination ON dim_cases.case_determination = dim_lookup_case_determination.code
     LEFT JOIN dim_lookup_case_duration_type ON dim_cases.duration_type = dim_lookup_case_duration_type.code
     LEFT JOIN dim_lookup_case_frequency_type ON dim_cases.frequency_type = dim_lookup_case_frequency_type.code
     LEFT JOIN dim_lookup_case_frequency_unit_type ON dim_cases.frequency_unit_type = dim_lookup_case_frequency_unit_type.code
     LEFT JOIN dim_lookup_case_status ON dim_cases.status = dim_lookup_case_status.code
     LEFT JOIN dim_lookup_case_types ON dim_cases.type = dim_lookup_case_types.code
     LEFT JOIN dim_lookup_case_cancel_reason ON dim_cases.cancel_reason = dim_lookup_case_cancel_reason.code
     LEFT JOIN dim_lookup_case_closure_reason ON dim_cases.case_closure_reason = dim_lookup_case_closure_reason.code
     LEFT JOIN dim_lookup_case_related_type ON dim_cases.related_case_type = dim_lookup_case_related_type.code

	 LEFT JOIN dim_lookup work_classification ON dim_cases.wr_work_classification = work_classification.code AND work_classification.type = 'work_classification'
	 LEFT JOIN dim_lookup type_of_injury ON dim_cases.wr_work_classification = type_of_injury.code AND type_of_injury.type = 'type_of_injury'
	 LEFT JOIN dim_lookup sharp_incdnt_when ON dim_cases.wr_work_classification = sharp_incdnt_when.code AND sharp_incdnt_when.type = 'sharp_incdnt_when'
	 LEFT JOIN dim_lookup sharp_job_class ON dim_cases.wr_work_classification = sharp_job_class.code AND sharp_job_class.type = 'sharp_job_class'
	 LEFT JOIN dim_lookup sharp_location_dept ON dim_cases.wr_work_classification = sharp_location_dept.code AND sharp_location_dept.type = 'sharp_location_dept'
	 LEFT JOIN dim_lookup sharp_procedure ON dim_cases.wr_work_classification = sharp_procedure.code AND sharp_procedure.type = 'sharp_procedure'
	 LEFT JOIN dim_lookup intermittent_type ON dim_cases.intermittent_type = intermittent_type.code AND intermittent_type.type = 'intermittent_type'
	 
	 LEFT JOIN dim_users cb ON dim_cases.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_cases.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_cases.ver_is_current = true
     AND dim_cases.is_deleted = false;

ALTER VIEW public.view_case
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_case TO postgres;
GRANT SELECT ON TABLE public.view_case TO absence_tracker;
GRANT ALL ON TABLE public.view_case TO etldw;
