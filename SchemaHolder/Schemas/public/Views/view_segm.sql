﻿-- View: public.view_segm

--DROP VIEW IF EXISTS public.view_segm CASCADE;

CREATE OR REPLACE VIEW public.view_segm AS
 SELECT 
	view_case.*,
	dim_lookup_case_status.description AS segm_case_status,
    dim_lookup_case_types.description AS segm_case_type,
	dim_case_segment.start_date AS segm_start_date,
	dim_case_segment.end_date AS segm_end_date,
	dim_case_segment_request.start_time_of_leave AS segm_request_start_time_of_leave,
	dim_case_segment_request.end_time_of_leave AS segm_request_end_time_of_leave,
	dim_case_segment_request.total_minutes AS segm_request_total_minutes,
	dim_case_segment_request.notes AS segm_request_notes,
	dim_case_segment_request.request_date AS segm_request_request_date
	

   FROM dim_case_segment 
     
     LEFT JOIN view_case ON dim_case_segment.case_id = view_case.case_id
     LEFT JOIN dim_lookup_case_types ON dim_case_segment.case_type = dim_lookup_case_types.code
	 LEFT JOIN dim_case_segment_request ON dim_case_segment.dim_case_segment_id = dim_case_segment_request.dim_case_segment_id
	 LEFT JOIN dim_lookup_case_status ON dim_case_segment.case_status = dim_lookup_case_status.code

  WHERE dim_case_segment.ver_is_current = true
     AND dim_case_segment.is_deleted = false;

ALTER VIEW public.view_segm
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_segm TO postgres;
GRANT SELECT ON TABLE public.view_segm TO absence_tracker;
GRANT ALL ON TABLE public.view_segm TO etldw;
