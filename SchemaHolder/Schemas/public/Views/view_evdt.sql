﻿-- View: public.view_evdt


--DROP VIEW IF EXISTS public.view_evdt CASCADE;

CREATE OR REPLACE VIEW public.view_evdt AS
 SELECT
	view_case.*,
	dim_lookup.description AS evdt_event_type,
	dim_case_event_date.event_date AS evdt_event_date,
	cb.display_name AS evdt_created_by_name,
	cb.email AS evdt_created_by_email,
	mb.display_name AS evdt_modified_by_name,
	mb.email AS evdt_modified_by_email

   FROM dim_case_event_date
	 INNER JOIN view_case ON dim_case_event_date.case_id = view_case.case_id
	 LEFT JOIN dim_lookup  ON dim_case_event_date.event_type = dim_lookup.code AND dim_lookup.type = 'case_event_type'
	 LEFT JOIN dim_users cb ON dim_case_event_date.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_case_event_date.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_case_event_date.ver_is_current = true
     AND dim_case_event_date.is_deleted = false;

ALTER VIEW public.view_evdt
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_evdt TO postgres;
GRANT SELECT ON TABLE public.view_evdt TO absence_tracker;
GRANT ALL ON TABLE public.view_evdt TO etldw;
