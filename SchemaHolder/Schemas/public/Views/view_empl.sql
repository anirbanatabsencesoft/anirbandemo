﻿-- View: public.view_empl


--DROP VIEW IF EXISTS public.view_empl CASCADE;

CREATE OR REPLACE VIEW public.view_empl AS
 SELECT 
	view_eplr.*,	
    dim_employee.ext_ref_id AS empl_id,
    dim_employee.employee_number AS empl_employee_number,
    dim_employee.first_name AS empl_first_name,
    dim_employee.middle_name AS empl_middle_name,
    dim_employee.last_name AS empl_last_name,
    dim_lookup_empl_gender.description AS empl_gender,
    dim_employee.dob AS empl_dob,
        CASE
            WHEN dim_employee.dob IS NULL THEN NULL::double precision
            ELSE date_part('year'::text, now()) - date_part('year'::text, dim_employee.dob)
        END AS empl_age_in_years,
    dim_lookup_empl_status.description AS empl_employee_status,
    dim_employee.salary AS empl_salary,
    dim_lookup_empl_pay_type.description AS empl_pay_type,
    dim_lookup_empl_work_type.description AS empl_work_type,
	dim_employee.employee_class_code AS empl_employee_class_code,
	dim_employee.employee_class_name AS empl_employee_class_name,
    dim_employee.cost_center_code AS empl_cost_center_code,
    dim_employee.service_date AS empl_service_date,
    (date_part('year'::text, now()) - date_part('year'::text, dim_employee.service_date)) * 12::double precision + (date_part('month'::text, now()) - date_part('month'::text, dim_employee.service_date)) AS empl_months_of_service,
    date_part('day'::text, now() - dim_employee.service_date::timestamp with time zone) AS empl_days_of_service,
    trunc(date_part('day'::text, now() - dim_employee.service_date::timestamp with time zone) / 7::double precision) AS empl_weeks_of_service,
    date_part('year'::text, now()) - date_part('year'::text, dim_employee.service_date) AS empl_years_of_service,
    dim_employee.hire_date AS empl_hire_date,
    dim_employee.rehire_date AS empl_rehire_date,
    dim_employee.termination_date AS empl_termination_date,
    dim_employee.work_state AS empl_work_state,
    dim_employee.work_country AS empl_work_country,
    dim_employee.meets50in75mile_rule AS empl_meets_50_in_75_mile_rule,
    dim_employee.key_employee AS empl_is_key_employee,
    dim_employee.exempt AS empl_is_exempt,
    dim_lookup_empl_military_status.description AS empl_military_status,
    dim_employee.spouse_employee_number AS empl_spouse_employee_number,
    dim_employee.ext_created_date AS empl_employee_created_date,
    dim_employee.email AS empl_employee_created_by_email,
    dim_employee.empl_employee_created_by_first_name,
    dim_employee.empl_employee_created_by_last_name,
    dim_employee.email AS empl_employee_email,
    dim_employee.alt_email AS empl_employee_alt_email,
    dim_employee.address1 AS empl_employee_address1,
    dim_employee.address2 AS empl_employee_address2,
    dim_employee.city AS empl_employee_city,
    dim_employee.state AS empl_employee_state,
    dim_employee.postal_code AS empl_employee_postal_code,
    dim_employee.country AS empl_employee_country,
    dim_employee.alt_address1 AS empl_employee_alt_address1,
    dim_employee.alt_address2 AS empl_employee_alt_address2,
    dim_employee.alt_city AS empl_employee_alt_city,
    dim_employee.alt_state AS empl_employee_alt_state,
    dim_employee.alt_postal_code AS empl_employee_alt_postal_code,
    dim_employee.alt_country AS empl_employee_alt_country,
    dim_employee.work_phone AS empl_employee_work_phone,
    dim_employee.home_phone AS empl_employee_home_phone,
    dim_employee.cell_phone AS empl_employee_cell_phone,
    dim_employee.alt_phone AS empl_employee_alt_phone,
    dim_employee.office_location AS empl_office_location,
    dim_employee.job_title AS empl_job_title,
    dim_employee.department AS empl_department,
    dim_employee.job_start_date AS empl_job_start_date,
    dim_employee.job_end_date AS empl_job_end_date,
    dim_employee.job_code AS empl_job_code,
    dim_employee.job_name AS empl_job_name,
    dim_employee.job_office_location_code AS empl_job_office_location_code,
    dim_employee.job_office_location_name AS empl_job_office_location_name,
    dim_employee.hours_worked AS empl_hours_worked_last_12_months,
	dim_employee.hours_worked_as_of AS empl_hours_worked_last_12_months_as_of,
	dim_employee.average_minutes_worked_per_week AS empl_average_minutes_worked_per_week,
	dim_employee.average_minutes_worked_per_week_as_of AS empl_average_minutes_worked_per_week_as_of,
    dim_employee.pay_schedule_name AS empl_pay_schedule,
    COALESCE(mview_fact_employee.count_cases, 0::bigint) AS empl_employee_cases,
    COALESCE(mview_fact_employee.count_open_cases, 0::bigint) AS empl_employee_open_cases,
    COALESCE(mview_fact_employee.count_closed_cases, 0::bigint) AS empl_employee_closed_cases,
    COALESCE(mview_fact_employee.count_todos, 0::bigint) AS empl_employee_todos,
    COALESCE(mview_fact_employee.count_open_todos, 0::bigint) AS empl_employee_open_todos,
    COALESCE(mview_fact_employee.count_closed_todos, 0::bigint) AS empl_employee_closed_todos,
    COALESCE(mview_fact_employee.count_overdue_todos, 0::bigint) AS empl_employee_overdue_todos,
    dim_employee.custom_fields AS empl_custom_fields,
	mview_supervisor.name AS empl_supervisor_name,
	mview_supervisor.employee_number AS empl_supervisor_employee_number,
	mview_supervisor.email AS empl_supervisor_email,
	mview_supervisor.phone AS empl_supervisor_phone,
	mview_hr.name AS empl_hr_name,
	mview_hr.employee_number AS empl_hr_employee_number,
	mview_hr.email AS empl_hr_email,
	mview_hr.phone AS empl_hr_phone,

	dim_employee.ssn AS empl_ssn,
	empl_job_classification.description AS empl_job_classification,
	dim_employee.risk_profile_code AS empl_risk_profile_code,
	dim_employee.risk_profile_name AS empl_risk_profile_name,
	dim_employee.risk_profile_description AS empl_risk_profile_description,
	dim_employee.risk_profile_order AS empl_risk_profile_order,
	dim_employee.start_day_of_week AS empl_start_day_of_week,

	cb.display_name AS empl_created_by_name,
	cb.email AS empl_created_by_email,
	mb.display_name AS empl_modified_by_name,
	mb.email AS empl_modified_by_email

   FROM dim_employee
     LEFT JOIN mview_fact_employee ON dim_employee.ext_ref_id = mview_fact_employee.employee_ref_id
	 LEFT JOIN view_eplr ON dim_employee.employer_id = view_eplr.eplr_id
     LEFT JOIN dim_lookup_empl_gender ON dim_employee.gender = dim_lookup_empl_gender.code
     LEFT JOIN dim_lookup_empl_status ON dim_employee.status = dim_lookup_empl_status.code
     LEFT JOIN dim_lookup_empl_military_status ON dim_employee.military_status = dim_lookup_empl_military_status.code
     LEFT JOIN dim_lookup_empl_work_type ON dim_employee.work_type = dim_lookup_empl_work_type.code
     LEFT JOIN dim_lookup_empl_pay_type ON dim_employee.pay_type = dim_lookup_empl_pay_type.code
	 LEFT JOIN mview_supervisor ON dim_employee.ext_ref_id = mview_supervisor.employee_id
	 LEFT JOIN mview_hr ON dim_employee.ext_ref_id = mview_hr.employee_id
	 LEFT JOIN dim_users cb ON dim_employee.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_employee.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true

	 LEFT JOIN dim_lookup empl_job_classification ON dim_employee.job_classification = empl_job_classification.code AND empl_job_classification.type = 'empl_job_classification'
  WHERE dim_employee.ver_is_current = true
     AND dim_employee.is_deleted = false;

ALTER VIEW public.view_empl
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_empl TO postgres;
GRANT SELECT ON TABLE public.view_empl TO absence_tracker;
GRANT ALL ON TABLE public.view_empl TO etldw;
