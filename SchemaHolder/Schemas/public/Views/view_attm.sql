﻿-- View: public.view_attm

--DROP VIEW IF EXISTS public.view_attm CASCADE;

CREATE OR REPLACE VIEW public.view_attm AS
 SELECT
	view_case.*,  	
	dim_attachment.ext_ref_id as attm_id,
	dim_attachment.content_length as attm_content_length,
	dim_attachment.content_type as attm_content_type,
	lookup_attachment_type.description as attm_attachment_type,
	dim_attachment.created_by_name as attm_created_by_name,
	dim_attachment.description as attm_description,
	dim_attachment.file_id as attm_file_id,
	dim_attachment.file_Name as attm_file_Name,
	dim_attachment.public as attm_public,    
		
	cb.email as attm_created_by_email,
	mb.display_name as attm_modified_by_name,
	mb.email as attm_modified_by_email

   FROM dim_attachment
	 LEFT JOIN view_case ON dim_attachment.case_id = view_case.case_id
     LEFT JOIN dim_lookup lookup_attachment_type ON dim_attachment.attachment_type = lookup_attachment_type.code and lookup_attachment_type.type='attachment_type'
     LEFT JOIN dim_users cb ON dim_attachment.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
	 LEFT JOIN dim_users mb ON dim_attachment.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
  WHERE dim_attachment.ver_is_current = true AND dim_attachment.is_deleted = false;
ALTER VIEW public.view_attm OWNER TO postgres;
GRANT ALL ON TABLE public.view_attm TO postgres;
GRANT SELECT ON TABLE public.view_attm TO absence_tracker;
GRANT ALL ON TABLE public.view_attm TO etldw;