﻿-- View: public.view_pypd

--DROP VIEW IF EXISTS public.view_pypd CASCADE;

CREATE OR REPLACE VIEW public.view_pypd AS
 SELECT
	view_payp.*,
	dim_case_pay_period_detail.start_date AS pypd_start_date,
	dim_case_pay_period_detail.end_date AS pypd_end_date,
	dim_case_pay_period_detail.is_Offset AS pypd_is_Offset,
	dim_case_pay_period_detail.max_payment_amount AS pypd_max_payment_amount ,
	dim_case_pay_period_detail.pay_amount_override_value AS pypd_pay_amount_override_value,
	dim_case_pay_period_detail.pay_amount_system_value AS pypd_pay_amount_system_value,
	dim_case_pay_period_detail.payment_tier_percentage AS pypd_payment_tier_percentage,
	ppob.display_name AS pypd_pay_percentage_override_by_name,
	dim_case_pay_period_detail.pay_percentage_override_date AS pypd_pay_percentage_override_date,
	dim_case_pay_period_detail.pay_percentage_override_value AS pypd_pay_percentage_override_value,
	dim_case_pay_period_detail.pay_percentage_system_value AS pypd_pay_percentage_system_value,
	dim_case_pay_period_detail.policy_code AS pypd_policy_code,
	dim_case_pay_period_detail.policy_name AS pypd_policy_name,
	dim_case_pay_period_detail.salary_total AS pypd_salary_total
	
   FROM dim_case_pay_period_detail
	 INNER JOIN view_payp ON dim_case_pay_period_detail.dim_case_pay_period_id = view_payp.view_case_pay_period_Id and dim_case_pay_period_detail.case_id=view_payp.case_id 
	 LEFT JOIN dim_users ppob ON dim_case_pay_period_detail.pay_percentage_override_by = ppob.ext_ref_id AND ppob.ver_is_current = true;
	

ALTER VIEW public.view_pypd
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_pypd TO postgres;
GRANT SELECT ON TABLE public.view_pypd TO absence_tracker;
GRANT ALL ON TABLE public.view_pypd TO etldw;
