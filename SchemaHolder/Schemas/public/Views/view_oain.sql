﻿-- View: public.view_oain

DROP VIEW IF EXISTS public.view_oain CASCADE;

CREATE OR REPLACE VIEW public.view_oain AS
SELECT
	view_org.*
	,oa.dim_organization_annual_info_id as view_organization_annual_info_id
    ,oa.ext_ref_id as oain_id
    ,oa.ext_created_date as oain_created_date
    ,oa.ext_modified_date as oain_modified_date  
    ,oa.org_code as oain_org_code
    ,oa.org_year as oain_org_year
    ,oa.average_employee_count as oain_average_employee_count
    ,oa.total_hours_worked as oain_total_hours_worked
    ,oa.created_date as oain_organization_created_Date
  
 ,	cb.display_name AS oain_created_by_name
	,cb.email AS oain_created_by_email
	,mb.display_name AS oain_modified_by_name
	,mb.email AS oain_modified_by_email

FROM public.dim_organization_annual_info oa
LEFT JOIN dim_users cb ON oa.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON oa.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_org ON  view_org.cust_id = oa.customer_id
WHERE oa.ver_is_current = true AND oa.is_deleted = false;

ALTER VIEW public.view_oain
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_oain TO postgres;
GRANT SELECT ON TABLE public.view_oain TO absence_tracker;
GRANT ALL ON TABLE public.view_oain TO etldw;

