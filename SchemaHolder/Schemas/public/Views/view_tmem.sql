﻿-- View: public.view_tmem

--DROP VIEW IF EXISTS public.view_tmem CASCADE;

CREATE OR REPLACE VIEW public.view_tmem AS
SELECT
	view_cust.*
	,tm.dim_team_member_id as view_team_member_id
    ,tm.ext_ref_id as tmem_id
    ,tm.ext_created_date as tmem_created_date
    ,tm.ext_modified_date as tmem_modified_date   
    ,tm.user_id as tmem_user_id
    ,tm.team_id as tmem_team_id
    ,tm.is_team_lead as tmem_is_team_lead
    ,tm.assigned_by_id as tmem_assigned_by_id
    ,tm.assigned_on as tmem_assigned_on
    ,tm.modified_by_id as tmem_modified_by_id
    ,tm.modified_on as tmem_modified_on
    
	,cb.display_name AS tmem_created_by_name
	,cb.email AS tmem_created_by_email
	,mb.display_name AS tmem_modified_by_name
	,mb.email AS tmem_modified_by_email

FROM public.dim_team_member tm
LEFT JOIN dim_team t ON tm.team_id::bigint = t.dim_team_id
LEFT JOIN dim_users uid ON tm.user_id::bigint = uid.dim_user_id
LEFT JOIN dim_users cb ON tm.ext_created_by = cb.ext_ref_id AND cb.ver_is_current = true
LEFT JOIN dim_users mb ON tm.ext_modified_by = mb.ext_ref_id AND mb.ver_is_current = true
LEFT JOIN view_cust ON tm.customer_id = view_cust.cust_id
WHERE tm.ver_is_current = true and tm.is_deleted = FALSE;

ALTER VIEW public.view_tmem
    OWNER TO postgres;

GRANT ALL ON TABLE public.view_tmem TO postgres;
GRANT SELECT ON TABLE public.view_tmem TO absence_tracker;
GRANT ALL ON TABLE public.view_tmem TO etldw;