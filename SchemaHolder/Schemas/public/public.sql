﻿-- SCHEMA: public

-- DROP SCHEMA public ;

CREATE SCHEMA IF NOT EXISTS public;

COMMENT ON SCHEMA public
    IS 'standard public schema';

	DO
$do$
BEGIN


IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etldw')
THEN
    CREATE ROLE etldw LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'etlsync')
THEN
    CREATE ROLE etlsync LOGIN PASSWORD 'password_goes_here';
END IF;

IF NOT EXISTS (
      SELECT                       -- SELECT list can stay empty for this
      FROM   pg_catalog.pg_user
      WHERE  usename = 'absence_tracker')
THEN
    CREATE ROLE absence_tracker LOGIN PASSWORD 'password_goes_here';
END IF;


END
$do$;


GRANT ALL ON SCHEMA public TO postgres;

GRANT USAGE ON SCHEMA public TO absence_tracker;

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT USAGE ON SCHEMA public TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT ON TABLES TO absence_tracker;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT ALL ON TABLES TO etldw;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT, USAGE ON SEQUENCES TO etldw;