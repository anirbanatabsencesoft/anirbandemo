CREATE SEQUENCE public.dim_employee_dim_employee_id_seq
    INCREMENT 1
    START 109020
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_dim_employee_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_dim_employee_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_dim_employee_id_seq TO etldw;