﻿CREATE SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq
    INCREMENT 1
    START 24
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq
    OWNER TO postgres;

GRANT USAGE, SELECT ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_employer_service_option_dim_employer_service_option_id_seq TO postgres;