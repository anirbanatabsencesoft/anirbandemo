CREATE SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq
    INCREMENT 1
    START 61111
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.fact_workrestriction_fact_workrestriction_id_seq TO etldw;