﻿CREATE SEQUENCE IF NOT EXISTS public.dim_employee_contact_dim_employee_contact_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_contact_dim_employee_contact_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.dim_cases_dim_case_id_seq TO etldw;