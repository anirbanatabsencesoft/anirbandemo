﻿CREATE SEQUENCE IF NOT EXISTS public.dim_relapse_dim_relapse_id_seq
    INCREMENT 1
    START 269
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_relapse_dim_relapse_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_relapse_dim_relapse_id_seq TO postgres;