CREATE SEQUENCE public.dim_user_roles_dim_user_roles_id_seq
    INCREMENT 1
    START 598
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_user_roles_dim_user_roles_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_user_roles_dim_user_roles_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_user_roles_dim_user_roles_id_seq TO etldw;