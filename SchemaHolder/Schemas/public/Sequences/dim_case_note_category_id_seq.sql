CREATE SEQUENCE IF NOT EXISTS public.dim_case_note_category_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_note_category_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_category_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_note_category_id_seq TO etldw;