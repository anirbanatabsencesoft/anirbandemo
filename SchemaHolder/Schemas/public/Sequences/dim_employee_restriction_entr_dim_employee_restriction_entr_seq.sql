CREATE SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_employee_restriction_entr_dim_employee_restriction_entr_seq TO etldw;