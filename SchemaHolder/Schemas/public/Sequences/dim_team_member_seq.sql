CREATE SEQUENCE public.dim_team_member_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_team_member_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO etldw;

GRANT ALL ON SEQUENCE public.dim_team_member_seq TO postgres;