CREATE SEQUENCE public.dim_case_pay_period_id_seq
    INCREMENT 1
    START 1592
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_case_pay_period_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_case_pay_period_id_seq TO etldw;