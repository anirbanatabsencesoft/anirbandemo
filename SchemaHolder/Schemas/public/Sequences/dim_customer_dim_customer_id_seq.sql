CREATE SEQUENCE public.dim_customer_dim_customer_id_seq
    INCREMENT 1
    START 1356
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dim_customer_dim_customer_id_seq
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_dim_customer_id_seq TO postgres;

GRANT ALL ON SEQUENCE public.dim_customer_dim_customer_id_seq TO etldw;