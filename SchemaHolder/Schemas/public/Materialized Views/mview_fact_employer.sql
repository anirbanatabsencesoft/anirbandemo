-- View: public.mview_fact_employer

-- REFRESH MATERIALIZED VIEW public.mview_fact_employer
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_employer;

CREATE MATERIALIZED VIEW public.mview_fact_employer
TABLESPACE pg_default
AS
WITH employees AS (
	SELECT	employer_id,
			count(ext_ref_id) as total
	FROM	dim_employee
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), cases AS (
	SELECT	employer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), todos AS (
	SELECT	employer_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employer_id
), users AS (
	SELECT	employers.employer_id,
			count(dim_users.ext_ref_id) as total
	FROM	dim_users
			JOIN LATERAL unnest(dim_users.employer_ids) as employers(employer_id) ON true
    WHERE	dim_users.ver_is_current
			AND NOT dim_users.is_deleted
	GROUP	BY employers.employer_id
)
SELECT	dim_employer.dim_employer_id,
		dim_employer.ext_ref_id AS employer_ref_id,
		coalesce(employees.total, 0) AS count_employees,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos,
        coalesce(users.total, 0) as count_users
FROM	dim_employer
		LEFT JOIN employees ON dim_employer.ext_ref_id = employees.employer_id
		LEFT JOIN cases ON dim_employer.ext_ref_id = cases.employer_id
		LEFT JOIN todos ON dim_employer.ext_ref_id = todos.employer_id
		LEFT JOIN users ON dim_employer.ext_ref_id = users.employer_id
WHERE	dim_employer.ver_is_current
		AND NOT dim_employer.is_deleted;

ALTER TABLE public.mview_fact_employer
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_employer TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_employer TO etldw;
GRANT ALL ON TABLE public.mview_fact_employer TO postgres;

-- Index: mview_fact_employer_employer_ref_id

-- DROP INDEX public.mview_fact_employer_employer_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_employer_employer_ref_id
    ON public.mview_fact_employer USING btree
    (employer_ref_id)
    TABLESPACE pg_default;
