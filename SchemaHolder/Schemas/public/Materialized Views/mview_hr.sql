-- View: public.mview_hr

-- REFRESH MATERIALIZED VIEW public.mview_hr
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_hr;

CREATE MATERIALIZED VIEW public.mview_hr
TABLESPACE pg_default
AS
SELECT	DISTINCT ON (employee_id)
		employee_id,
		trim(both ' ' from (coalesce(first_name, '') || ' ' || coalesce(last_name, company_name, ''))) AS name,
		employee_number,
		email,
		work_phone AS phone
FROM	dim_employee_contact
WHERE	contact_type_code = 'HR'
		AND ver_is_current = true
		AND is_deleted = false
ORDER	BY employee_id ASC, dim_employee_contact_id DESC;

ALTER TABLE public.mview_hr
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_hr TO absence_tracker;
GRANT ALL ON TABLE public.mview_hr TO etldw;
GRANT ALL ON TABLE public.mview_hr TO postgres;

-- Index: mview_hr_employee_id

-- DROP INDEX public.mview_hr_employee_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_hr_employee_id
    ON public.mview_hr USING btree
    (employee_id)
    TABLESPACE pg_default;
