-- View: public.mview_supervisor

-- REFRESH MATERIALIZED VIEW public.mview_supervisor
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_supervisor;

CREATE MATERIALIZED VIEW public.mview_supervisor
TABLESPACE pg_default
AS
SELECT	DISTINCT ON (employee_id)
		employee_id,
		trim(both ' ' from (coalesce(first_name, '') || ' ' || coalesce(last_name, company_name, ''))) AS name,
		employee_number,
		email,
		work_phone AS phone
FROM	dim_employee_contact
WHERE	contact_type_code = 'SUPERVISOR'
		AND ver_is_current = true
		AND is_deleted = false
ORDER	BY employee_id ASC, dim_employee_contact_id DESC;

ALTER TABLE public.mview_supervisor
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_supervisor TO absence_tracker;
GRANT ALL ON TABLE public.mview_supervisor TO etldw;
GRANT ALL ON TABLE public.mview_supervisor TO postgres;

-- Index: mview_supervisor_employee_id

-- DROP INDEX public.mview_supervisor_employee_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_supervisor_employee_id
    ON public.mview_supervisor USING btree
    (employee_id)
    TABLESPACE pg_default;
