-- View: public.mview_fact_employee

-- REFRESH MATERIALIZED VIEW public.mview_fact_employee;
-- DROP MATERIALIZED VIEW IF EXISTS public.mview_fact_employee;

CREATE MATERIALIZED VIEW public.mview_fact_employee
TABLESPACE pg_default
AS
WITH cases AS (
	SELECT	employee_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (0,1,4) THEN 1 ELSE 0 END) as open_cases,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_cases
	FROM	dim_cases
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employee_id
), todos AS (
	SELECT	employee_id,
			count(ext_ref_id) as total,
			sum(CASE WHEN status IN (-1, 2, 4) THEN 1 ELSE 0 END) as open_todos,
			sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) as closed_todos,
			sum(CASE WHEN status IN (-1, 2, 4) AND due_date < utc() THEN 1 ELSE 0 END) as overdue_todos
	FROM	dim_todoitem
    WHERE	ver_is_current
			AND NOT is_deleted
	GROUP	BY employee_id
)
SELECT	dim_employee.dim_employee_id,
		dim_employee.ext_ref_id AS employee_ref_id,
		coalesce(cases.total, 0) AS count_cases,
		coalesce(cases.open_cases, 0) AS count_open_cases,
		coalesce(cases.closed_cases, 0) AS count_closed_cases,
		coalesce(todos.total, 0) AS count_todos,
		coalesce(todos.open_todos, 0) AS count_open_todos,
		coalesce(todos.closed_todos, 0) AS count_closed_todos,
		coalesce(todos.overdue_todos, 0) AS count_overdue_todos
FROM	dim_employee
		LEFT JOIN cases ON dim_employee.ext_ref_id = cases.employee_id
		LEFT JOIN todos ON dim_employee.ext_ref_id = todos.employee_id
WHERE	dim_employee.ver_is_current
		AND NOT dim_employee.is_deleted;


ALTER TABLE public.mview_fact_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.mview_fact_employee TO absence_tracker;
GRANT ALL ON TABLE public.mview_fact_employee TO etldw;
GRANT ALL ON TABLE public.mview_fact_employee TO postgres;

-- Index: mview_fact_employee_employee_ref_id

-- DROP INDEX public.mview_fact_employee_employee_ref_id;

CREATE UNIQUE INDEX IF NOT EXISTS mview_fact_employee_employee_ref_id
    ON public.mview_fact_employee USING btree
    (employee_ref_id)
    TABLESPACE pg_default;
