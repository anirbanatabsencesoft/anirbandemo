-- View: public.mview_fact_organization

-- DROP MATERIALIZED VIEW public.mview_fact_organization;

CREATE MATERIALIZED VIEW public.mview_fact_organization
TABLESPACE pg_default
AS

SELECT dim_organization_id,organization_ref_id,customer_ref_id,employer_ref_id,org_level,count_children,
count_descendents,count_employees,count_cases,count_open_cases,count_closed_cases,count_todos,count_open_todos,
count_closed_todos,count_overdue_todos
FROM TBL_MV_FACT_ORG_BASEDATA
WITH DATA;
 