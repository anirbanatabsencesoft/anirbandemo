-- Data: public.dim_lookup_empl_status

insert into public.dim_lookup_empl_status (_id, code, description) values (1, 65, 'A');
insert into public.dim_lookup_empl_status (_id, code, description) values (2, 76, 'L');
insert into public.dim_lookup_empl_status (_id, code, description) values (3, 84, 'T');
insert into public.dim_lookup_empl_status (_id, code, description) values (4, 73, 'I');
