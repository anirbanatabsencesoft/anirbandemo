-- Data: public.dim_lookup_todoitem_priority

insert into public.dim_lookup_todoitem_priority (_id, code, description) values (1, -1, 'Low');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (2, 0, 'Normal');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (3, 1, 'High');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (4, 2, 'Urgent');
insert into public.dim_lookup_todoitem_priority (_id, code, description) values (5, 3, 'Critical');
