	/******** Eligibility Status Lookup Values (type eligibility_status) ***************/
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', -1, 'Ineligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=-1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'eligibility_status', 1, 'Eligible' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='eligibility_status' AND code=1);


	/* Case Denial Reason Lookup (type: case_denial_reason) */	
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 1, 'Exhausted' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 2, 'Not a valid healthcare provider' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 3, 'Not a serious health condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 4, 'Elimination period' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 5, 'PerUseCap' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 6, 'Intermittent non-allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 7, 'Paperwork not received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_denial_reason', 8, 'Not an eligible family member' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_denial_reason' AND code=8);

	
	/* Case Intermittent Status. (type: case_intermittent_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 1, 'Allowed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_intermittent_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_intermittent_status' AND code=2);


	/* Employee Job Classification (type: empl_job_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 1, 'Sedentary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 2, 'Light' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 3, 'Medium' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 4, 'Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_classification', 5, 'Very Heavy' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_classification' AND code=5);

	/* Work Classification (type: work_classification) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 1, 'Death' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 2, 'Days Away From Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'work_classification', 3, 'Job Transfer or Restriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='work_classification' AND code=3);

	
	/* Type of Injury (type: type_of_injury)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 0, 'Injury' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 1, 'Skin Disorder' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 2, 'Respiratory Condition' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 3, 'Poisoning' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 4, 'Hearing Loss' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 5, 'All Other Illnesses' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'type_of_injury', 6, 'Patient Handling' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='type_of_injury' AND code=6);

	
	/* Sharp Incident When (type: sharp_incdnt_when) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 0, 'Dont Know' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 1, 'Before Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 2, 'During Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_incdnt_when', 3, 'After Activation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_incdnt_when' AND code=3);

		
	/* Sharp Job Class (type: sharp_job_class) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 1, 'Doctor' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 2, 'Nurse' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 3, 'Intern Or Resident' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 4, 'Patient Care Support Staff' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 5, 'Technologist OR' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 6, 'Technologist RT' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 7, 'Technologist RAD' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 8, 'Phlebotomist Or Lab Tech' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 9, 'Housekeeper Or Laundry Worker' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_job_class', 10, 'Trainee' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_job_class' AND code=10);


	/* Sharp Location/Department (type: sharp_location_dept) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 1, 'Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 2, 'ICU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 3, 'Outside Patient Room' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 4, 'Emergency Department' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 5, 'Operating Room Or PACU' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 6, 'Clinical Laboratory' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 7, 'Outpatient Clinic Or Office' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_location_dept', 8, 'Utility Area' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_location_dept' AND code=8);


	/* Sharp Procedure (type: sharp_procedure) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 1, 'Draw Venous Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 2, 'Draw Arterial Blood' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 3, 'Injection' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 4, 'Start IV Or Central Line' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 5, 'Heparin Or Saline Flush' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 6, 'Obtain Body Fluid Or Tissue Sample' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 7, 'Cutting' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'sharp_procedure', 8, 'Suturing' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='sharp_procedure' AND code=8);


	/* Case Closure Reason (type: case_closure_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 0, 'Return To Work' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 1, 'Terminated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 2, 'Leave Cancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_closure_reason', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_closure_reason' AND code=3);

	/* IntermittentType Lookup (type: intermittent_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 0, 'OfficeVisit' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'intermittent_type', 1, 'Incapacity' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='intermittent_type' AND code=1);

	/* RecipientType Lookup(type: recipient_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 0, 'TO' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 1, 'CC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'recipient_type', 2, 'BCC' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='recipient_type' AND code=2);
	
	/* PaperworkReviewStatus Lookup(type: paperwork_review_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 0, 'NotApplicable' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 1, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 2, 'Received' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 3, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 4, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 5, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_review_status', 6, 'NotReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_review_status' AND code=6);

	/*DocumentType Lookup(type: document_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 0, 'None' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 1, 'Template' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 2, 'Html' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 3, 'MSWordDocument' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'document_type', 4, 'Pdf' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='document_type' AND code=4);

	/*PaperworkFieldStatus Lookup(type: paperwork_field_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 0, 'Incomplete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_status', 1, 'Complete' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_status' AND code=1);

	/*PaperworkFieldType Lookup(type: paperwork_field_type)*/	
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 0, 'Text' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'paperwork_field_type', 4, 'WorkRestrictions' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='paperwork_field_type' AND code=4);

	/* Employee Job Adjudication Status (type: empl_job_adjudication_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 1, 'Approved' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_adjudication_status', 2, 'Denied' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_adjudication_status' AND code=2);

	/* Employee Job Denial Reason (type: empl_job_denial_reason) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 0, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 1, 'RestrictionsConflictWithTasks' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 2, 'BodyMechanics' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 3, 'NoWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_job_denial_reason', 4, 'NotTrained' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_job_denial_reason' AND code=4);

	/* Employee Job Adjudication Status (type: empl_job_adjudication_status) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_necessity_type', 0, 'MedicalDevice' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_necessity_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'empl_necessity_type', 1, 'Medication' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='empl_necessity_type' AND code=1);

	/* EventType Lookup(type: event_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 0, 'CaseCreated' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 1, 'ReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=1); 
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 2, 'DeliveryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 3, 'BondingStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 4, 'BondingEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 5, 'IllnessOrInjuryDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 6, 'HospitalAdmissionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 7, 'HospitalReleaseDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 8, 'ReleaseReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 9, 'PaperworkReceived' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 10, 'AccommodationAdded' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 11, 'CaseClosed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 12, 'CaseCancelled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 13, 'AdoptionDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 14, 'EstimatedReturnToWork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 15, 'EmployerHolidayScheduleChanged' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 16, 'CaseStartDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'case_event_type', 17, 'CaseEndDate' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='case_event_type' AND code=17);

	/* PayrollStatus Lookup(type: payroll_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 1, 'SentToException' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'payroll_status', 2, 'SentToPayroll' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='payroll_status' AND code=2);

	/* AttachmentType Lookup(type: attachment_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 0, 'Communication' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 1, 'Paperwork' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 2, 'Documentation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'attachment_type', 3, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='attachment_type' AND code=3);
	
	/*CommunicationType Lookup(type: communication_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 0, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'communication_type', 1, 'Email' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='communication_type' AND code=1);

	/*EmailRepliesType Lookup(type: email_replies_type)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 1, 'Mail' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'email_replies_type', 2, 'CurrentUser' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='email_replies_type' AND code=2);
	
	/*NoteCategory Lookup(type: note_category)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 0, 'CaseSummary' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 1, 'EmployeeConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 2, 'ManagerConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 3, 'HRConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 4, 'Medical' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 5, 'TimeOffRequest' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=5);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 6, 'Certification' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=6);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 7, 'InteractiveProcess' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=7);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 8, 'WorkRestriction' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=8);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 9, 'OperationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=9);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 10, 'HealthCareProviderConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=10);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 11, 'WorkerCompensationConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=11);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 12, 'SafetyConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=12);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 13, 'LegalConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=13);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 14, 'ERCConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=14);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 15, 'EmployeeRelationsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=15);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 16, 'BenefitsConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=16);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 17, 'LOAConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=17);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 18, 'FacilitiesConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=18);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 19, 'PayrollConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=19);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 20, 'STDLOAVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=20);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 21, 'LTDVendorConversation' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=21);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 22, 'Other' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=22);
	INSERT INTO dim_lookup (type, code, description) SELECT 'note_category', 23, 'Referral' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='note_category' AND code=23);

	/*WorkflowInstanceStatus Lookup(type: workflow_instance_status)*/
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 0, 'Pending' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 1, 'Running' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 2, 'Paused' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=2);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 3, 'Completed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=3);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 4, 'Canceled' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=4);
	INSERT INTO dim_lookup (type, code, description) SELECT 'workflow_instance_status', 5, 'Failed' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='workflow_instance_status' AND code=5);
	   

	/* Role Type (type: role_type) */
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 0, 'Portal' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=0);
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 1, 'SelfService' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=1);
	INSERT INTO dim_lookup (type, code, description) SELECT 'role_type', 2, 'Administration' WHERE NOT EXISTS (SELECT 1 FROM dim_lookup WHERE type='role_type' AND code=2);
