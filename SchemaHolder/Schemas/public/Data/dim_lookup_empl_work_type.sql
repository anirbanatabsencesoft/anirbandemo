-- Data: public.dim_lookup_empl_work_type

insert into public.dim_lookup_empl_work_type (_id, code, description) values (1, 1, 'FT');
insert into public.dim_lookup_empl_work_type (_id, code, description) values (2, 2, 'PT');
insert into public.dim_lookup_empl_work_type (_id, code, description) values (3, 3, 'Per Diem');
