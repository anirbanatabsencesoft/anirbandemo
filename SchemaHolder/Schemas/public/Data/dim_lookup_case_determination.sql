-- Data: public.dim_lookup_case_determination

insert into public.dim_lookup_case_determination (_id, code, description) values (1, 0, 'Pending');
insert into public.dim_lookup_case_determination (_id, code, description) values (2, 1, 'Approved');
insert into public.dim_lookup_case_determination (_id, code, description) values (3, 2, 'Denied');
insert into public.dim_lookup_case_determination (_id, code, description) values (4, 3, 'Approved');
insert into public.dim_lookup_case_determination (_id, code, description) values (5, 4, 'Not Eligible');
