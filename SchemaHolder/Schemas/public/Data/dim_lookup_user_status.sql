-- Data: public.dim_lookup_user_status

insert into public.dim_lookup_user_status (_id, code, description) values (1, 1, 'Active');
insert into public.dim_lookup_user_status (_id, code, description) values (2, 2, 'Disabled');
insert into public.dim_lookup_user_status (_id, code, description) values (3, 3, 'Locked');
