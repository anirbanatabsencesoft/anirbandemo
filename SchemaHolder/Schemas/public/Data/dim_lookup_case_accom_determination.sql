-- Data: public.dim_lookup_case_accom_determination

insert into public.dim_lookup_case_accom_determination (_id, code, description) values (1, 0, 'Pending');
insert into public.dim_lookup_case_accom_determination (_id, code, description) values (2, 1, 'Approved');
insert into public.dim_lookup_case_accom_determination (_id, code, description) values (3, 2, 'Denied');
