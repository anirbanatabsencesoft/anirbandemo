-- Data: public.dim_lookup_case_duration_type

insert into public.dim_lookup_case_duration_type (_id, code, description) values (1, 0, 'Minutes');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (2, 1, 'Hours');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (3, 2, 'Days');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (4, 3, 'Weeks');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (5, 4, 'Months');
insert into public.dim_lookup_case_duration_type (_id, code, description) values (6, 5, 'Years');
