-- Data: public.dim_lookup_case_closure_reason

insert into public.dim_lookup_case_closure_reason (_id, code, description) values (1, 0, 'ReturnToWork');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (2, 1, 'Terminated');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (3, 2, 'LeaveCancelled');
insert into public.dim_lookup_case_closure_reason (_id, code, description) values (4, 3, 'Other');
