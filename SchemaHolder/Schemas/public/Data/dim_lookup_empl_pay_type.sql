-- Data: public.dim_lookup_empl_pay_type

insert into public.dim_lookup_empl_pay_type (_id, code, description) values (1, 1, 'Salary');
insert into public.dim_lookup_empl_pay_type (_id, code, description) values (2, 2, 'Hourly');
