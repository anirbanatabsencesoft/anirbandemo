-- Data: public.dim_lookup_empl_military_status

insert into public.dim_lookup_empl_military_status (_id, code, description) values (1, 0, 'Civilian');
insert into public.dim_lookup_empl_military_status (_id, code, description) values (2, 1, 'Active Duty');
insert into public.dim_lookup_empl_military_status (_id, code, description) values (3, 2, 'Veteran');
