-- Data: public.dim_lookup_case_types

insert into public.dim_lookup_case_types (_id, code, description) values (1, 0, 'None');
insert into public.dim_lookup_case_types (_id, code, description) values (2, 1, 'Consecutive');
insert into public.dim_lookup_case_types (_id, code, description) values (3, 2, 'Intermittent');
insert into public.dim_lookup_case_types (_id, code, description) values (4, 4, 'Reduced');
insert into public.dim_lookup_case_types (_id, code, description) values (5, 8, 'Administrative');
