-- Data: public.dim_lookup_case_accom_status

insert into public.dim_lookup_case_accom_status (_id, code, description) values (1, 0, 'Open');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (2, 1, 'Closed');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (3, 2, 'Cancelled');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (4, 3, 'Requested');
insert into public.dim_lookup_case_accom_status (_id, code, description) values (5, 4, 'Inquiry');
