-- Data: public.dim_lookup_case_accom_cancel_reason

insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (2, 1, 'Accommodation No Longer Needed');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (3, 2, 'Leave No Longer Needed');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (4, 3, 'Request Withdrawn');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (5, 4, 'Entered For Wrong Employee');
insert into public.dim_lookup_case_accom_cancel_reason (_id, code, description) values (6, 5, 'Entered In Error');
