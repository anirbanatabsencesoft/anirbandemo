-- Data: public.dim_lookup_case_cancel_reason

insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (1, 0, 'Other');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (2, 1, 'Leave Not Needed Or Cancelled');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (3, 2, 'Entered In Error');
insert into public.dim_lookup_case_cancel_reason (_id, code, description) values (4, 3, 'Inquiry Closed');
