-- Table: public.dim_accommodation_usage

-- DROP TABLE public.dim_accommodation_usage;

CREATE TABLE public.dim_accommodation_usage
(
    dim_accommodation_id bigint,
    ext_ref_id character varying(36),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    determination integer,
	is_deleted boolean default false
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_accommodation_usage
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_accommodation_usage TO absence_tracker;
GRANT ALL ON TABLE public.dim_accommodation_usage TO etldw;
GRANT ALL ON TABLE public.dim_accommodation_usage TO postgres;

-- Index: dim_accommodation_usage_dim_accommodation_id

-- DROP INDEX public.dim_accommodation_usage_dim_accommodation_id;

CREATE INDEX dim_accommodation_usage_dim_accommodation_id
    ON public.dim_accommodation_usage USING btree
    (dim_accommodation_id)
    TABLESPACE pg_default;