--- Table for dim_case_policy_usage   
	CREATE TABLE IF NOT EXISTS public.dim_case_policy_usage
	(
		dim_case_policy_usage_id bigint NOT NULL DEFAULT nextval('dim_case_policy_usage_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		dim_case_policy_id  bigint NOT NULL,
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL, 
		policy_code varchar(50) NOT NULL,
		date_used timestamp with time zone,
		minutes_used int,
		minutes_in_day int,
		hours_used decimal,
		hours_in_day decimal,
		user_entered Boolean,
		determination int,
		denial_reason int,
		denial_reason_other text,
		determined_by varchar(24),
		intermittent_determination int,
		is_locked Boolean,
		uses_time Boolean,
	    is_intermittent_restriction Boolean,
		percent_of_day decimal,
		status int,
		created_date timestamp with time zone DEFAULT utc(),
		denial_reason_code text,
		denial_reason_description text,

		CONSTRAINT dim_case_policy_usage_pkey PRIMARY KEY (dim_case_policy_usage_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy_usage
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy_usage TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy_usage TO postgres;


CREATE INDEX IF NOT EXISTS dim_case_policy_usage_dim_case_policy_id
    ON public.dim_case_policy_usage USING btree
    (dim_case_policy_id)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_policy_usage_caseid
    ON public.dim_case_policy_usage USING btree
    (case_id)
    TABLESPACE pg_default;

