-- Table: public.dim_case_note_category

-- DROP TABLE public.dim_case_note_category;

CREATE TABLE public.dim_case_note_category
(
  dim_case_note_category_id bigint NOT NULL DEFAULT nextval('dim_case_note_category_id_seq'::regclass),
  dim_case_note_id bigint,
  category_id character varying(24) NOT NULL,
  category_code text,
  category_name text,
  cat_order integer,
  event_date timestamp with time zone,
  CONSTRAINT dim_case_note_category_pkey PRIMARY KEY (dim_case_note_category_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dim_case_note_category
  OWNER TO postgres;
GRANT ALL ON TABLE public.dim_case_note_category TO postgres;
GRANT SELECT ON TABLE public.dim_case_note_category TO absence_tracker;
GRANT ALL ON TABLE public.dim_case_note_category TO etldw;
