﻿-- Table: public.dim_employer_contact

-- DROP TABLE public.dim_employer_contact;

CREATE TABLE IF NOT EXISTS public.dim_employer_contact
(
    dim_employer_contact_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    contact_firstname text,
    contact_lastname text,
    contact_email text,
    contact_alt_email text,
    contact_work_phone text,
    contact_fax text,
    contact_address1 text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean,
    CONSTRAINT dim_employer_contact_pkey PRIMARY KEY (dim_employer_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact TO postgres;

-- Index: dim_employer_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_contact_cur_ver_ref_id_is_del
    ON public.dim_employer_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Missing entries
ALTER TABLE dim_employer_contact ADD COLUMN contact_address text;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_is_current DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_is_expired DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_prev_id DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_root_id DROP NOT NULL;
ALTER TABLE dim_employer_contact ALTER COLUMN ver_seq DROP NOT NULL;