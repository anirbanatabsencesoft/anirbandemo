-- Table: public.dim_communication_recipient

-- Create Table dim_communication_recipient
	CREATE TABLE IF NOT EXISTS public.dim_communication_recipient
	(
		dim_communication_recipient_id bigint NOT NULL DEFAULT nextval('dim_communication_recipient_id_seq'::regclass),
		dim_communication_id bigint,
		recipient_type int,
		ext_ref_id character varying(36),
		address_id	varchar(36),
		address_address1 text,
		address_city	text,
		address_country	text,
		address_postal_code	text,
		address_state	text,
		alt_email	text,
		email	text,
		fax	text,
		title text,
		first_name	text,
		middle_name text,
		last_name	text,
		home_email	text,
		company_name text,
		contact_person_name text,
		date_of_birth timestamp with time zone ,
		home_phone text,
		cell_phone text,
		is_primary boolean,
		CONSTRAINT dim_communication_recipient_pkey PRIMARY KEY (dim_communication_recipient_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_recipient
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_recipient TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_recipient TO etldw;
	GRANT ALL ON TABLE public.dim_communication_recipient TO postgres;
