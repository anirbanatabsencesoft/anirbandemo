-- Table: public.dim_lookup_empl_status

-- DROP TABLE public.dim_lookup_empl_status;

CREATE TABLE public.dim_lookup_empl_status
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_status
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_status TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_status TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_status TO postgres;

-- Index: dim_lookup_empl_status_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_status_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_status_code
    ON public.dim_lookup_empl_status USING btree
    (code)
    TABLESPACE pg_default;