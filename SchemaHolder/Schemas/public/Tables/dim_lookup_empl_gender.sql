-- Table: public.dim_lookup_empl_gender

-- DROP TABLE public.dim_lookup_empl_gender;

CREATE TABLE public.dim_lookup_empl_gender
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_empl_gender
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_empl_gender TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_empl_gender TO etldw;
GRANT ALL ON TABLE public.dim_lookup_empl_gender TO postgres;

-- Index: dim_lookup_empl_gender_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_empl_gender_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_empl_gender_code
    ON public.dim_lookup_empl_gender USING btree
    (code)
    TABLESPACE pg_default;