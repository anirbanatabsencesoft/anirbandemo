-- Table: public.dim_lookup_case_duration_type

-- DROP TABLE public.dim_lookup_case_duration_type;

CREATE TABLE public.dim_lookup_case_duration_type
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_duration_type
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_duration_type TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_duration_type TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_duration_type TO postgres;

-- Index: dim_lookup_case_duration_type_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_duration_type_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_duration_type_code
    ON public.dim_lookup_case_duration_type USING btree
    (code)
    TABLESPACE pg_default;