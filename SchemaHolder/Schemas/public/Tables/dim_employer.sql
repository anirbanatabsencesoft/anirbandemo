-- Table: public.dim_employer

-- DROP TABLE public.dim_employer;

CREATE TABLE public.dim_employer
(
    dim_employer_id bigint NOT NULL DEFAULT nextval('dim_employer_dim_employer_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    name text,
    reference_code text,
    url text,
    reset_month integer,
    reset_day_of_month integer,
	start_date date,
	end_date date,
    fml_period_type integer,
    allow_intermittent_for_birth_adoption_or_foster_care boolean,
    ignore_schedule_for_prior_hours_worked boolean,
	ignore_average_minutes_worked_per_week boolean,
    enable50in75mile_rule boolean,
    enable_spouse_at_same_employer_rule boolean,
    ft_weekly_work_hours double precision,
    publicly_traded_company boolean,
    customer_id character varying(24),
    contact_first_name text,
    contact_last_name text,
    contact_email text,
    contact_address1 text,
    contact_address2 text,
    contact_city text,
    contact_state text,
    contact_postal_code text,
    contact_country text,
    is_deleted boolean,
    created_date timestamp with time zone DEFAULT utc(),
    eplr_service_type text,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_employer_pkey PRIMARY KEY (dim_employer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer TO absence_tracker;
GRANT ALL ON TABLE public.dim_employer TO etldw;
GRANT ALL ON TABLE public.dim_employer TO postgres;

-- Index: dim_employer_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employer_cur_ver_ref_id_is_del
    ON public.dim_employer USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;