
	/* Create lookup table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup
	(
		lookup_id bigint NOT NULL DEFAULT nextval('dim_lookup_id_seq'::regclass),
		type varchar(60) NOT NULL,  
		code bigint NOT NULL,
		description text,
		data json,
		CONSTRAINT dim_lookup_pkey PRIMARY KEY (lookup_id),
		CONSTRAINT dim_lookup_unique_type_code UNIQUE (type, code)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup
		ADD CONSTRAINT fk_lookup_lookup_cataegory FOREIGN KEY (type) REFERENCES dim_lookup_category (lookup_category);

	ALTER TABLE public.dim_lookup
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup TO etldw;
	GRANT ALL ON TABLE public.dim_lookup TO postgres;