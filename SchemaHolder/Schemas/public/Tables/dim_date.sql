-- Table: public.dim_date

-- DROP TABLE public.dim_date;

CREATE TABLE public.dim_date
(
    dim_date_id bigint NOT NULL DEFAULT nextval('dim_date_dim_date_id_seq'::regclass),
    created_date timestamp with time zone NOT NULL DEFAULT utc(),
    iso8601date timestamp without time zone,
    year integer,
    month integer,
    day integer,
    minute integer,
    second integer,
    is_weekend boolean,
    is_weekday boolean,
    is_american_holiday boolean,
    quarter integer,
    day_of_week_name character varying(16),
    is_leap_year boolean,
    first_day_of_week timestamp with time zone,
    last_day_of_week timestamp with time zone,
    first_day_of_month timestamp with time zone,
    last_day_of_month timestamp with time zone,
    first_day_of_year timestamp with time zone,
    last_day_of_year timestamp with time zone,
    days_in_year integer,
    iso8601_week_of_year integer NOT NULL,
    closest_weekday timestamp with time zone NOT NULL,
    CONSTRAINT dim_date_pkey PRIMARY KEY (dim_date_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_date
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_date TO absence_tracker;
GRANT ALL ON TABLE public.dim_date TO etldw;
GRANT ALL ON TABLE public.dim_date TO postgres;