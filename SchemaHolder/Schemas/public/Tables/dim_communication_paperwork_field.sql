 
-- Table: public.dim_communication_paperwork_field

-- Create Table dim_communication_paperwork_field
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork_field
	(
		dim_communication_paperwork_field_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_field_id_seq'::regclass),
		dim_communication_paperwork_id bigint,
		ext_ref_id character varying(36),
		name text,
		status int,
		type int,
		required boolean default false,
		field_order int,
		value text,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
        is_deleted boolean,
		CONSTRAINT dim_communication_paperwork_field_pkey PRIMARY KEY (dim_communication_paperwork_field_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork_field
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork_field TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork_field TO postgres;

--Missing entries
ALTER TABLE dim_communication_paperwork_field ALTER COLUMN is_deleted SET DEFAULT false;
ALTER TABLE dim_communication_paperwork_field ADD COLUMN ver_is_current boolean DEFAULT true;

