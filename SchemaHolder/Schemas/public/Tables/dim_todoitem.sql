-- Table: public.dim_todoitem

-- DROP TABLE public.dim_todoitem;

CREATE TABLE public.dim_todoitem
(
    dim_todoitem_id bigint NOT NULL DEFAULT nextval('dim_todoitem_dim_todoitem_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    accommodation_id character varying(24),
    active_accommodation_id character varying(36),
    case_number text,
    case_id character varying(24),
    item_type integer,
    status integer,
    assigned_to_id character varying(24),
    assigned_to_name text,
    employee_id character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    due_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
    priority integer,
    title text,
    assigned_to_email text,
    assigned_to_first_name text,
    assigned_to_last_name text,
    due_date_change_reason text,
    weight double precision,
    result_text text,
    optional boolean,
    hide_until timestamp with time zone,
    template text,
    work_flow_name text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_todoitem_pkey PRIMARY KEY (dim_todoitem_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_todoitem
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_todoitem TO absence_tracker;
GRANT ALL ON TABLE public.dim_todoitem TO etldw;
GRANT ALL ON TABLE public.dim_todoitem TO postgres;

-- Index: dim_todoitem_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_todoitem_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_todoitem_cur_ver_ref_id_is_del
    ON public.dim_todoitem USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;