﻿-- Table: public.dim_employer_service_option

-- DROP TABLE public.dim_employer_service_option;

CREATE TABLE public.dim_employer_service_option
(
    dim_employer_service_option_id bigint NOT NULL DEFAULT nextval('dim_employer_service_option_dim_employer_service_option_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    employer_id character varying(24),
    is_deleted boolean,
    key text,
    value text,
    cusomerserviceoptionid character varying(24),
    CONSTRAINT dim_employer_service_option_pkey PRIMARY KEY (dim_employer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_employer_service_option TO postgres;