-- Table: public.dim_users

-- DROP TABLE public.dim_users;

CREATE TABLE public.dim_users
(
    dim_user_id bigint NOT NULL DEFAULT nextval('dim_users_dim_user_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    customer_id character varying(24),
    first_name text,
    last_name text,
    email text,
    is_deleted boolean,
    created_date timestamp with time zone DEFAULT utc(),
    failed_login_attempts bigint,
    last_failed_login_attempt timestamp with time zone,
    locked boolean,
    locked_date timestamp with time zone,
    disabled boolean,
    disabled_date timestamp with time zone,
    last_activity_date timestamp with time zone,
    user_flags bigint,
    is_self_service_user boolean,
    is_portal_user boolean,
    user_status bigint,
    user_type bigint,
    role_name text,
    user_employee_id character varying(24),
    user_employee_number text,
	must_change_password boolean,
	display_name text,
	employer_ids character varying(24)[],
	role_ids character varying(24)[],
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_users_pkey PRIMARY KEY (dim_user_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_users
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_users TO absence_tracker;
GRANT ALL ON TABLE public.dim_users TO etldw;
GRANT ALL ON TABLE public.dim_users TO postgres;

-- Index: dim_users_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_users_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_users_cur_ver_ref_id_is_del
    ON public.dim_users USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;