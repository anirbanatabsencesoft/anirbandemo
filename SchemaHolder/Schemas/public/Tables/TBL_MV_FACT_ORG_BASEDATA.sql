CREATE TABLE TBL_MV_FACT_ORG_BASEDATA
(
	dim_organization_id bigint ,
	organization_ref_id varchar(24),
	customer_ref_id varchar(24),
	employer_ref_id varchar(24),
	org_level integer,
	count_children bigint, 
	count_descendents bigint,
	count_employees bigint,
	count_cases bigint,
	count_open_cases bigint,
	count_closed_cases bigint,
	count_todos bigint,
	count_open_todos bigint,
	count_closed_todos bigint,
	count_overdue_todos bigint
);


