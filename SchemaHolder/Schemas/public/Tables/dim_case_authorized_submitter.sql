-- Table: public.dim_case_authorized_submitter

-- Create Table dim_case_authorized_submitter
	CREATE TABLE IF NOT EXISTS public.dim_case_authorized_submitter
	(
		dim_case_authorized_submitter_id bigint NOT NULL DEFAULT nextval('dim_case_authorized_submitter_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		authorized_submitter_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		contact_type_code text,
		contact_type_name text,	
		contact_first_name text,    
		contact_last_name text,	
		contact_email text,
		contact_alt_email text,
		contact_work_phone text,
		contact_home_phone text,
		contact_cell_phone text,
		contact_fax text,
		contact_street text,
		contact_address1 text,
		contact_address2 text,
		contact_city text,
		contact_state text,
		contact_postal_code text,
		contact_country text,	   	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		CONSTRAINT dim_case_authorized_submitter_pkey PRIMARY KEY (dim_case_authorized_submitter_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_authorized_submitter
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_authorized_submitter TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO etldw;
	GRANT ALL ON TABLE public.dim_case_authorized_submitter TO postgres;

-- Index: dim_case_authorized_submitter_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_authorized_submitter_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_authorized_submitter_cur_ver_ref_id_is_del
    ON public.dim_case_authorized_submitter USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;