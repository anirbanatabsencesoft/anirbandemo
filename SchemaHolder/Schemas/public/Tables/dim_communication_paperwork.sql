-- Table: public.dim_communication_paperwork

-- Create Table dim_communication_paperwork
	CREATE TABLE IF NOT EXISTS public.dim_communication_paperwork
	(
		dim_communication_paperwork_id bigint NOT NULL DEFAULT nextval('dim_communication_paperwork_id_seq'::regclass),
		communication_id character varying(24), 
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		attachment_id	varchar(24),		
		checked	 boolean,
		due_date	timestamp with time zone,
		return_attachment_id	varchar(24),
		status	int,
		status_date	 timestamp with time zone,
		
		paperwork_id	varchar(24),
		paperwork_cby	varchar(24),
		paperwork_cdt	timestamp with time zone,
		paperwork_code	text,
		paperwork_customer_id	varchar(24),
		paperwork_description	text,
		paperwork_doc_type	int,
		paperwork_employer_id	varchar(24),
		paperwork_enclosure_text	text,		
		paperwork_file_id	varchar(24),
		paperwork_file_name	text,
		paperwork_is_deleted boolean default false,
		paperwork_mby	varchar(24),
		paperwork_mdt	timestamp with time zone,
		paperwork_name	text,
		paperwork_requires_review	boolean,
		paperwork_return_date_adjustment	text,
		paperwork_return_date_adjustment_days	int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_communication_paperwork_pkey PRIMARY KEY (dim_communication_paperwork_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication_paperwork
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication_paperwork TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO etldw;
	GRANT ALL ON TABLE public.dim_communication_paperwork TO postgres;
	
	-- Index: dim_communication_paperwork_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_paperwork_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_paperwork_id_cur_ver_ref_id_is_del
    ON public.dim_communication_paperwork USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

--Missing entries
ALTER TABLE dim_communication_paperwork ADD COLUMN case_id character varying(24);
ALTER TABLE dim_communication_paperwork ADD COLUMN paperwork_review_status integer;