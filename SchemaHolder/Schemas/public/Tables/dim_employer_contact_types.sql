-- Table: public.dim_employer_contact_types

-- DROP TABLE public.dim_employer_contact_types;

CREATE TABLE public.dim_employer_contact_types
(
    dim_employer_contact_types_id bigint NOT NULL DEFAULT nextval('dim_employer_contact_types_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    is_deleted boolean,
    customer_id character varying(24),
    name text,
    code text,
    CONSTRAINT dim_employer_contact_types_pkey PRIMARY KEY (dim_employer_contact_types_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employer_contact_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employer_contact_types TO absence_tracker;

GRANT ALL ON TABLE public.dim_employer_contact_types TO etldw;

GRANT ALL ON TABLE public.dim_employer_contact_types TO postgres;

-- Index: dim_employer_contact_types_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_contact_types_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_contact_types_cur_ver_ref_id_is_del
    ON public.dim_employer_contact_types USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;