-- Table: public.dim_lookup_todoitem_priority

-- DROP TABLE public.dim_lookup_todoitem_priority;

CREATE TABLE public.dim_lookup_todoitem_priority
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_todoitem_priority
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_todoitem_priority TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_todoitem_priority TO etldw;
GRANT ALL ON TABLE public.dim_lookup_todoitem_priority TO postgres;

-- Index: dim_lookup_todoitem_priority_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_todoitem_priority_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_todoitem_priority_code
    ON public.dim_lookup_todoitem_priority USING btree
    (code)
    TABLESPACE pg_default;