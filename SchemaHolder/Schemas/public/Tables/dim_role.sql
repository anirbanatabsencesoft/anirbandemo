﻿-- Table: public.dim_role

-- DROP TABLE public.dim_role;

CREATE TABLE IF NOT EXISTS public.dim_role
(
    dim_role_id bigint NOT NULL DEFAULT nextval('dim_role_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    name text,
    customer_id character varying(24),
    permissions text[],
    role_type integer,
	is_deleted boolean,
    CONSTRAINT dim_role_pkey PRIMARY KEY (dim_role_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_role
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_role TO absence_tracker;

GRANT ALL ON TABLE public.dim_role TO etldw;

GRANT ALL ON TABLE public.dim_role TO postgres;

-- Index: dim_role_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_role_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_role_cur_ver_ref_id_is_del
    ON public.dim_role USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;