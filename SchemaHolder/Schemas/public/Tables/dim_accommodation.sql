-- Table: public.dim_accommodation

-- DROP TABLE public.dim_accommodation;

CREATE TABLE public.dim_accommodation
(
    dim_accommodation_id bigint NOT NULL DEFAULT nextval('dim_accommodation_dim_accommodation_id_seq'::regclass),
    ext_ref_id character varying(36),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    case_id character varying(24),
    accommodation_type text,
    accommodation_type_code text,
    duration integer,
    resolution text,
    resolved boolean,
    resolved_date timestamp with time zone,
    granted boolean,
    granted_date timestamp with time zone,
    cost double precision,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    determination integer,
    status integer,
    cancel_reason integer,
    cancel_reason_other_desc text,
	denial_reason_code text,
    work_related boolean,
    created_date timestamp with time zone DEFAULT public.utc(),
	is_deleted boolean default false,
	record_hash text,
    min_approved_date timestamp with time zone,
    max_approved_date timestamp with time zone,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_accommodation_pkey PRIMARY KEY (dim_accommodation_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_accommodation
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_accommodation TO absence_tracker;
GRANT ALL ON TABLE public.dim_accommodation TO etldw;
GRANT ALL ON TABLE public.dim_accommodation TO postgres;

-- Index: dim_accommodation_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_accommodation_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_accommodation_cur_ver_ref_id_is_del
    ON public.dim_accommodation USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;