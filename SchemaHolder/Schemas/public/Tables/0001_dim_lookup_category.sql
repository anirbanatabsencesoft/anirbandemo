
	/* Create lookup category table */
	CREATE TABLE IF NOT EXISTS public.dim_lookup_category
	(
		lookup_category VARCHAR(60) NOT NULL,
		description text,
		CONSTRAINT dim_lookup_category_pkey PRIMARY KEY (lookup_category)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_lookup_category
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_lookup_category TO absence_tracker;
	GRANT ALL ON TABLE public.dim_lookup_category TO etldw;
	GRANT ALL ON TABLE public.dim_lookup_category TO postgres;