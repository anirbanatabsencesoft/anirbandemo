-- Table: public.dim_customer

-- DROP TABLE public.dim_customer;

CREATE TABLE public.dim_customer
(
    dim_customer_id bigint NOT NULL DEFAULT nextval('dim_customer_dim_customer_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    cust_customer_name text NOT NULL,
    cust_customer_url text,
    cust_contact_first_name text,
    cust_contact_last_name text,
    cust_contact_email text,
    cust_contact_address1 text,
    cust_contact_address2 text,
    cust_contact_city text,
    cust_contact_state text,
    cust_contact_postal_code text,
    cust_contact_country text,
    cust_billing_first_name text,
    cust_billing_last_name text,
    cust_billing_email text,
    cust_billing_address1 text,
    cust_billing_address2 text,
    cust_billing_city text,
    cust_billing_state text,
    cust_billing_postal_code text,
    cust_billing_country text,
    created_date timestamp with time zone DEFAULT utc(),
    cust_service_type text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_customer_pkey PRIMARY KEY (dim_customer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer TO absence_tracker;
GRANT ALL ON TABLE public.dim_customer TO etldw;
GRANT ALL ON TABLE public.dim_customer TO postgres;

-- Index: dim_customer_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_customer_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_customer_cur_ver_ref_id_is_del
    ON public.dim_customer USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;