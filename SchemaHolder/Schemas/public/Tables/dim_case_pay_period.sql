-- Table: public.dim_case_pay_period

-- Create Table dim_case_pay_period
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period
	(
		dim_case_pay_period_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		status int,
		locked_date timestamp with time zone,
		locked_by	varchar(24),
		max_weekly_pay_amount double precision,		
		payroll_date_override timestamp with time zone,
		end_date_override timestamp with time zone,
		payroll_date timestamp with time zone,
		payroll_date_override_by varchar(24),
		end_date_override_by varchar(24),		
		offset_amount double precision,
		total double precision,
		sub_total double precision,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		CONSTRAINT dim_case_pay_period_pkey PRIMARY KEY (dim_case_pay_period_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period TO postgres;

-- Index: dim_case_pay_period_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_pay_period_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_pay_period_cur_ver_ref_id_is_del
    ON public.dim_case_pay_period USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;


create index IF NOT EXISTS dim_case_pay_period_caseid
  on dim_case_pay_period  USING btree (case_id, ver_is_current, ver_seq);