-- Table: public.dim_lookup_acom_duration

-- DROP TABLE public.dim_lookup_acom_duration;

CREATE TABLE public.dim_lookup_acom_duration
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_acom_duration
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_acom_duration TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_acom_duration TO etldw;
GRANT ALL ON TABLE public.dim_lookup_acom_duration TO postgres;

-- Index: dim_lookup_acom_duration_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_acom_duration_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_acom_duration_code
    ON public.dim_lookup_acom_duration USING btree
    (code)
    TABLESPACE pg_default;