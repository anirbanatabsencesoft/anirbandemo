-- Table: public.dim_case_pay_period_detail

-- Create Table dim_case_pay_period_detail
	CREATE TABLE IF NOT EXISTS public.dim_case_pay_period_detail
	(
		dim_case_pay_period_detail_id bigint NOT NULL DEFAULT nextval('dim_case_pay_period_detail_id_seq'::regclass),
		dim_case_pay_period_id bigint,
		ext_ref_id character varying(36),
		case_id varchar(24) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		is_Offset boolean,
		max_payment_amount double precision,
		pay_amount_override_value double precision,
		pay_amount_system_value double precision,
		payment_tier_percentage double precision,
		pay_percentage_override_by varchar(24),
		pay_percentage_override_date timestamp with time zone,
		pay_percentage_override_value double precision,
		pay_percentage_system_value double precision,
		policy_code text,
		policy_name text,
		salary_total text,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_case_pay_period_detail_pkey PRIMARY KEY (dim_case_pay_period_detail_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_pay_period_detail
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_pay_period_detail TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO etldw;
	GRANT ALL ON TABLE public.dim_case_pay_period_detail TO postgres;
