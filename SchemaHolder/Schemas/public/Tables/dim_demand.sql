-- Table: public.dim_demand

-- DROP TABLE public.dim_demand;

CREATE TABLE public.dim_demand
(
    dim_demand_id bigint NOT NULL DEFAULT nextval('dim_demand_dim_demand_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    customer_id character varying(24),
    name text,
    code text,
    description text,
    order_data integer,
    category text,
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_demand_pkey PRIMARY KEY (dim_demand_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_demand
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_demand TO absence_tracker;
GRANT ALL ON TABLE public.dim_demand TO etldw;
GRANT ALL ON TABLE public.dim_demand TO postgres;

-- Index: dim_demand_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_demand_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_demand_cur_ver_ref_id_is_del
    ON public.dim_demand USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;