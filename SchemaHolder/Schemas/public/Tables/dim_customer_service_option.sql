CREATE TABLE public.dim_customer_service_option
(
    dim_customer_service_option_id bigint NOT NULL DEFAULT nextval('dim_customer_service_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    is_deleted boolean,
    customer_id character varying(24),
    key text,
    value text,
    "order" integer,
    CONSTRAINT dim_customer_service_pkey PRIMARY KEY (dim_customer_service_option_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_customer_service_option
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_customer_service_option TO absence_tracker;

GRANT ALL ON TABLE public.dim_customer_service_option TO etldw;

GRANT ALL ON TABLE public.dim_customer_service_option TO postgres;

-- Index: dim_employer_service_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employer_service_cur_ver_ref_id_is_del;

CREATE INDEX dim_employer_service_cur_ver_ref_id_is_del
    ON public.dim_customer_service_option USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;