﻿-- Table: public.dim_employee_note

-- DROP TABLE public.dim_employee_note;

CREATE TABLE public.dim_employee_note
(
    dim_employee_note_id bigint NOT NULL DEFAULT nextval('dim_employee_note_note_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    note text,
    is_public boolean,
    category_code text,
    category_name text,    
    CONSTRAINT dim_employee_note_pkey PRIMARY KEY (dim_employee_note_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_note
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_note TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_note TO etldw;

GRANT ALL ON TABLE public.dim_employee_note TO postgres;