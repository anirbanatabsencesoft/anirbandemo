-- Table: public.dim_lookup_case_accom_determination

-- DROP TABLE public.dim_lookup_case_accom_determination;

CREATE TABLE public.dim_lookup_case_accom_determination
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_accom_determination
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_accom_determination TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_accom_determination TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_accom_determination TO postgres;

-- Index: dim_lookup_case_accom_determination_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_accom_determination_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_accom_determination_code
    ON public.dim_lookup_case_accom_determination USING btree
    (code)
    TABLESPACE pg_default;