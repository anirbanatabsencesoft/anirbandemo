-- Table: public.dim_case_note

-- Create Table dim_case_note
	CREATE TABLE IF NOT EXISTS public.dim_case_note
	(
		dim_case_note_id bigint NOT NULL DEFAULT nextval('dim_case_note_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24),
		employee_id varchar(24),
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accomm_id	varchar(36),
		accomm_question_id	text,
		answer	boolean,		
		cert_id	text,
		contact	text,
		copied_from	text,
		copied_from_employee	text,		
		employee_number	text,		
		entered_by_name	text,		
		notes	text,
		process_path	text,
		public	boolean,
		request_date	timestamp with time zone,
		request_id	text,
		s_answer	text,
		work_restriction_id varchar(36),
		category_code text,
		category_name text,
		category int,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		CONSTRAINT dim_case_note_pkey PRIMARY KEY (dim_case_note_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_note
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_note TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_note TO etldw;
	GRANT ALL ON TABLE public.dim_case_note TO postgres;

-- Index: dim_case_note_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_note_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_note_cur_ver_ref_id_is_del
    ON public.dim_case_note USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;