-- Table: public.dim_lookup_case_closure_reason

-- DROP TABLE public.dim_lookup_case_closure_reason;

CREATE TABLE public.dim_lookup_case_closure_reason
(
    _id bigint NOT NULL,
    code bigint,
    description text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_case_closure_reason
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_case_closure_reason TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_case_closure_reason TO etldw;
GRANT ALL ON TABLE public.dim_lookup_case_closure_reason TO postgres;

-- Index: dim_lookup_case_closure_reason_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_case_closure_reason_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_case_closure_reason_code
    ON public.dim_lookup_case_closure_reason USING btree
    (code)
    TABLESPACE pg_default;