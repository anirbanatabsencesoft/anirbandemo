CREATE TABLE public.dim_team_member
(
    dim_team_member_id bigint NOT NULL DEFAULT nextval('dim_team_member_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    customer_id character varying(24),
    user_id character varying(24),
    team_id character varying(24),
    is_team_lead boolean,
    assigned_by_id character varying(24),
    assigned_on timestamp with time zone,
    modified_by_id character varying(24),
    modified_on timestamp with time zone,
    is_deleted boolean,
    CONSTRAINT dim_team_member_pkey PRIMARY KEY (dim_team_member_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_team_member
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_team_member TO absence_tracker;

GRANT ALL ON TABLE public.dim_team_member TO etldw;

GRANT ALL ON TABLE public.dim_team_member TO postgres;

-- Index: dim_team_member_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_team_member_cur_ver_ref_id_is_del;

CREATE INDEX dim_team_member_cur_ver_ref_id_is_del
    ON public.dim_team_member USING btree
    (ext_ref_id COLLATE pg_catalog."default", ver_is_current, is_deleted)
    TABLESPACE pg_default;