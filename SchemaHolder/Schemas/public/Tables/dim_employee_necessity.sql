﻿-- Table: public.dim_employee_necessity

-- DROP TABLE public.dim_employee_necessity;

CREATE TABLE public.dim_employee_necessity
(
    dim_employee_necessity_id bigint NOT NULL DEFAULT nextval('dim_employee_necessity_dim_employee_necessity_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    name text,
    type int,
    description text,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    case_id text,
    case_number text,
	is_deleted boolean default false,
    CONSTRAINT dim_employee_necessity_pkey PRIMARY KEY (dim_employee_necessity_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_necessity
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_necessity TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_necessity TO etldw;

GRANT ALL ON TABLE public.dim_employee_necessity TO postgres;