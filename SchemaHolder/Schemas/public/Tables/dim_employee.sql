-- Table: public.dim_employee

-- DROP TABLE public.dim_employee;

CREATE TABLE public.dim_employee
(
    dim_employee_id bigint NOT NULL DEFAULT nextval('dim_employee_dim_employee_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employee_number text,
    first_name text,
    middle_name text,
    last_name text,
    status bigint,
    salary double precision,
    pay_type bigint,
    gender bigint,
    dob timestamp with time zone,
	work_type bigint,
    employee_class_code text,
	employee_class_name text,
    cost_center_code text,
    job_title text,
    department text,
    service_date timestamp with time zone,
    hire_date timestamp with time zone,
    rehire_date timestamp with time zone,
    termination_date timestamp with time zone,
    work_state text,
    work_country text,
    pay_schedule_id character varying(24),
    pay_schedule_name text,
    meets50in75mile_rule boolean,
    key_employee boolean,
    exempt boolean,
    military_status bigint,
    spouse_employee_number text,
    employer_name text,
    employer_id character varying(24),
    customer_id character varying(24),
    empl_employee_created_by_email text,
    empl_employee_created_by_first_name text,
    empl_employee_created_by_last_name text,
    email text,
    alt_email text,
    work_phone text,
    home_phone text,
    cell_phone text,
    alt_phone text,
    office_location text,
    empl_pay_schedule text,
    created_date timestamp with time zone DEFAULT utc(),
    address1 text,
    address2 text,
    city text,
    state text,
    postal_code text,
    country text,
    alt_address1 text,
    alt_address2 text,
    alt_city text,
    alt_state text,
    alt_postal_code text,
    alt_country text,
    job_start_date timestamp with time zone,
    job_end_date timestamp with time zone,
    job_code text,
    job_name text,
    job_office_location_code text,
    job_office_location_name text,
    custom_fields jsonb,
    contact_type_code text,
    contact_type_name text,
	is_deleted boolean default false,
    hours_worked double precision,
    hours_worked_as_of timestamp with time zone,
	average_minutes_worked_per_week double precision,
	average_minutes_worked_per_week_as_of timestamp with time zone,
	ssn character varying(20),
	job_classification bigint,
	risk_profile_code character varying(255),
	risk_profile_name character varying(255),
	risk_profile_description text,
	risk_profile_order int,
	start_day_of_week character varying(12),

	ext_created_by character varying(24),
	ext_modified_by character varying(24),

    CONSTRAINT dim_employee_pkey PRIMARY KEY (dim_employee_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee TO etldw;
GRANT ALL ON TABLE public.dim_employee TO postgres;

-- Index: dim_employee_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_cur_ver_ref_id_is_del
    ON public.dim_employee USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;


