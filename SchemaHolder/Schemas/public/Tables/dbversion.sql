﻿-- Table: public.dbversion

-- DROP TABLE public.dbversion;

CREATE TABLE public.dbversion
(
    dbversion_id bigint NOT NULL,
    created_date timestamp with time zone NOT NULL DEFAULT public.utc(),
    major_num integer DEFAULT 0,
    minur_num integer DEFAULT 0,
    notes text,
    CONSTRAINT dbversion_pkey PRIMARY KEY (dbversion_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dbversion
    OWNER to postgres;

GRANT SELECT ON TABLE public.dbversion TO absence_tracker;

GRANT ALL ON TABLE public.dbversion TO etldw;

GRANT ALL ON TABLE public.dbversion TO postgres;

--Missing entry
ALTER TABLE dbversion ALTER COLUMN created_date SET DEFAULT public.utc();