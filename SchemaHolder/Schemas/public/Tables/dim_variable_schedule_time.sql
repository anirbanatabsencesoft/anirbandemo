-- Table: public.dim_variable_schedule_time

-- Create Table dim_variable_schedule_time
	CREATE TABLE IF NOT EXISTS public.dim_variable_schedule_time
	(
		dim_variable_schedule_time_id bigint NOT NULL DEFAULT nextval('dim_variable_schedule_time_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		time_sample_date timestamp with time zone ,
		time_total_minutes int,
		employee_number text,	
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_variable_schedule_time_pkey PRIMARY KEY (dim_variable_schedule_time_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_variable_schedule_time
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_variable_schedule_time TO absence_tracker;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO etldw;
	GRANT ALL ON TABLE public.dim_variable_schedule_time TO postgres;

-- Index: dim_variable_schedule_time_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_variable_schedule_time_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_variable_schedule_time_cur_ver_ref_id_is_del
    ON public.dim_variable_schedule_time USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;