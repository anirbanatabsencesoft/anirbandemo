-- Table: public.dim_organization

-- DROP TABLE public.dim_organization;

CREATE TABLE public.dim_organization
(
    dim_organization_id bigint NOT NULL DEFAULT nextval('dim_organization_dim_organization_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    employer_id character varying(24),
    customer_id character varying(24),
    code text,
    name text,
	siccode text,
    naicscode text,
	address1 text,
	address2 text,
	city text,
	postalcode text,
	state text,
	country text,
    description text,
    path text,
    type_code text,
    type_name text,
    org_created_by_email text,
    org_created_by_first_name text,
    org_created_by_last_name text,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_organization_pkey PRIMARY KEY (dim_organization_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_organization
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_organization TO absence_tracker;
GRANT ALL ON TABLE public.dim_organization TO etldw;
GRANT ALL ON TABLE public.dim_organization TO postgres;

-- Index: dim_organization_path_ref

-- DROP INDEX public.dim_organization_path_ref;

CREATE INDEX IF NOT EXISTS dim_organization_path_ref
    ON public.dim_organization USING btree
    (customer_id, employer_id, path, ver_is_current, is_deleted)
    TABLESPACE pg_default;

-- Index: dim_organization_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_organization_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_organization_cur_ver_ref_id_is_del
    ON public.dim_organization USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;