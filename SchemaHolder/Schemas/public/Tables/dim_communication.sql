-- Table: public.dim_communication

-- Create Table dim_communication
	CREATE TABLE IF NOT EXISTS public.dim_communication
	(
		dim_communication_id bigint NOT NULL DEFAULT nextval('dim_communication_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		attachment_id varchar(24),
		body text,
		communication_type int,
		created_by_email text,
		created_by_name text,
		email_replies_type int,	
		is_draft boolean,
		name text,
		public boolean,
		first_send_date timestamp with time zone,
		last_send_date timestamp with time zone,
		subject text,
		template text,
		to_do_item_id varchar(24),
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		CONSTRAINT dim_communication_id_pkey PRIMARY KEY (dim_communication_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_communication
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_communication TO absence_tracker;
	GRANT ALL ON TABLE public.dim_communication TO etldw;
	GRANT ALL ON TABLE public.dim_communication TO postgres;

-- Index: dim_communication_id_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_communication_id_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_communication_id_cur_ver_ref_id_is_del
    ON public.dim_communication USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;