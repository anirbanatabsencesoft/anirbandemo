-- Table: public.dim_employee_contact

-- DROP TABLE public.dim_employee_contact;

CREATE TABLE public.dim_employee_contact
(
    dim_employee_contact_id bigint NOT NULL DEFAULT nextval('dim_employee_contact_dim_employee_contact_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
	employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    cdt timestamp with time zone,
    ext_created_by character varying(24),
    mdt timestamp with time zone,
    ext_modified_by character varying(24),
	contact_type_code text,
	contact_type_name text,
	company_name text,
	title text,
    first_name text,
    middle_name text,
    last_name text,
	dob timestamp with time zone,
    email text,
    alt_email text,
    work_phone text,
    home_phone text,
    cell_phone text,
    fax text,
	street text,
    address1 text,
    address2 text,
    city text,
    state text,
    postal_code text,
    country text,
	is_primary boolean,
	contact_person_name text,
	years_of_age int,
	military_status int,
	contact_position int,
    employee_number text,
	is_deleted boolean default false,
    created_date timestamp with time zone DEFAULT utc(),
    CONSTRAINT dim_employee_contact_pkey PRIMARY KEY (dim_employee_contact_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_contact TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_contact TO etldw;
GRANT ALL ON TABLE public.dim_employee_contact TO postgres;

-- Index: dim_employee_contact_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_contact_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_contact_cur_ver_ref_id_is_del
    ON public.dim_employee_contact USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;
