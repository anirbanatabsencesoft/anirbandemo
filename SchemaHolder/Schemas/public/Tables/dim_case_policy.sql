-- Table: public.dim_case_policy 

-- Create Table dim_case_policy 
	CREATE TABLE IF NOT EXISTS public.dim_case_policy
	(
		dim_case_policy_id bigint NOT NULL DEFAULT nextval('dim_case_policy_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24) NOT NULL,
		customer_id varchar(24) NOT NULL,
		employer_id varchar(24) NOT NULL,
		policy_code varchar(50) NOT NULL,
		policy_name varchar(255) NOT NULL,
		start_date timestamp with time zone,
		end_date timestamp with time zone,
		case_type int,
		eligibility int,
		determination int,
		denial_reason int,
		denial_reason_other text,
		payout_percentage decimal,
		crosswalk_codes jsonb,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),
		record_hash text,
		
		actual_duration int,
		exceeded_duration int,
		denial_reason_code text,
		denial_reason_description text,
		segment_id character varying(36),

		CONSTRAINT dim_case_policy_pkey PRIMARY KEY (dim_case_policy_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_policy
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_policy TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_policy TO etldw;
	GRANT ALL ON TABLE public.dim_case_policy TO postgres;

-- Index: dim_accommodation_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_accommodation_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_policy_cur_ver_ref_id_is_del
    ON public.dim_case_policy USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_policy_caseid_verseq
  on public.dim_case_policy USING btree(case_id,segment_id, ver_is_current,ver_seq)
  TABLESPACE pg_default;
   