-- Table: public.dim_case_event_date

-- Create Table dim_case_event_date
	CREATE TABLE IF NOT EXISTS public.dim_case_event_date
	(
		dim_case_event_date_id bigint NOT NULL DEFAULT nextval('dim_case_event_date_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		dim_ver_id bigint NOT NULL,
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		event_type bigint,		
		event_date timestamp with time zone,
		record_hash text,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),		
		CONSTRAINT dim_case_event_date_pkey PRIMARY KEY (dim_case_event_date_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_case_event_date
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_case_event_date TO absence_tracker;
	GRANT ALL ON TABLE public.dim_case_event_date TO etldw;
	GRANT ALL ON TABLE public.dim_case_event_date TO postgres;

	-- Index: dim_case_event_date_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_case_event_date_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_case_event_date_cur_ver_ref_id_is_del
    ON public.dim_case_event_date USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS dim_case_event_date_case_id_index ON public.dim_case_event_date (case_id);