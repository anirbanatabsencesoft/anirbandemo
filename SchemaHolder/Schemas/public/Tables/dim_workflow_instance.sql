-- Table: public.dim_workflow_instance

-- Create Table dim_workflow_instance
	CREATE TABLE IF NOT EXISTS public.dim_workflow_instance
	(
		dim_workflow_instance_id bigint NOT NULL DEFAULT nextval('dim_workflow_instance_id_seq'::regclass),
		ext_ref_id character varying(36) ,
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		case_id varchar(24) NOT NULL,
		employee_id varchar(24),
		customer_id varchar(24) NOT NULL,
		employer_id	varchar(24) NOT NULL,
		accommodation_end_date	timestamp with time zone ,
		accommodation_id	character varying(36) ,
		accommodation_type_code	text ,
		accommodation_type_id	text ,
		active_accommodation_id	character varying(36) ,
		applied_policy_id	character varying(36) ,
		attachment_id	text ,		
		close_case	Boolean ,
		communication_id	text ,
		communication_type	int ,
		condition_start_date	timestamp with time zone ,
		contact_employee_due_date	timestamp with time zone ,
		contact_hcp_due_date	timestamp with time zone ,		
		denial_explanation	text ,
		denial_reason	int ,
		description	text ,
		determination	int ,
		due_date	timestamp with time zone ,
		ergonomic_assessment_date	timestamp with time zone ,
		event_id	varchar(24) ,
		event_type	int ,
		extend_grace_period	Boolean ,
		first_exhaustion_date	timestamp with time zone ,
		general_health_condition	text ,
		has_work_restrictions	Boolean ,
		illness_or_injury_date	timestamp with time zone ,
		new_due_date	timestamp with time zone ,
		paperwork_attachment_id	text ,
		paperwork_due_date	timestamp with time zone ,
		paperwork_id	character varying(36) ,
		paperwork_name	text ,
		paperwork_received	Boolean ,
		policy_code	text ,
		policy_id	text ,
		reason	int ,
		requires_review	Boolean ,
		return_attachment_id	text ,
		return_to_work_date	timestamp with time zone ,
		rtw	Boolean ,
		source_workflow_activity_activity_id	text ,
		source_workflow_activity_id	character varying(36) ,
		source_workflow_instance_id	text ,
		status	int ,
		template	text ,
		workflow_activity_id	character varying(36) ,
		workflow_id	varchar(24) ,
		workflow_name	text ,

		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT utc(),		
		CONSTRAINT dim_workflow_instance_pkey PRIMARY KEY (dim_workflow_instance_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_workflow_instance
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_workflow_instance TO absence_tracker;
	GRANT ALL ON TABLE public.dim_workflow_instance TO etldw;
	GRANT ALL ON TABLE public.dim_workflow_instance TO postgres;

-- Index: dim_workflow_instance_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_workflow_instance_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_workflow_instance_cur_ver_ref_id_is_del
    ON public.dim_workflow_instance USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;