-- Table: public.dim_employee_restriction

-- DROP TABLE public.dim_employee_restriction;

CREATE TABLE public.dim_employee_restriction
(
    dim_employee_restriction_id bigint NOT NULL DEFAULT nextval('dim_employee_restriction_dim_employee_restriction_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    wkrs_restriction_name text,
    wkrs_restriction_code text,
    wkrs_restriction_category text,
    ver_is_current boolean NOT NULL,
    ver_seq bigint NOT NULL,
    ver_is_expired boolean NOT NULL,
    ver_root_id bigint NOT NULL,
    ver_prev_id bigint NOT NULL,
    ver_history_count bigint,
    case_id character varying(24),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    created_date timestamp with time zone DEFAULT utc(),
	is_deleted boolean default false,
	ext_created_by character varying(24),
	ext_modified_by character varying(24),
    CONSTRAINT dim_employee_restriction_pkey PRIMARY KEY (dim_employee_restriction_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_restriction
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_restriction TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_restriction TO etldw;
GRANT ALL ON TABLE public.dim_employee_restriction TO postgres;

-- Index: dim_employee_restriction_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_employee_restriction_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_employee_restriction_cur_ver_ref_id_is_del
    ON public.dim_employee_restriction USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;