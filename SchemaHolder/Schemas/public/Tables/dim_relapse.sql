﻿CREATE TABLE IF NOT EXISTS public.dim_relapse
(
    dim_relapse_id bigint NOT NULL DEFAULT nextval('dim_relapse_dim_relapse_id_seq'::regclass),
    ext_ref_id character varying(36) COLLATE pg_catalog."default",
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24) COLLATE pg_catalog."default",
    ext_modified_by character varying(24) COLLATE pg_catalog."default",
    employee_id character varying(24) COLLATE pg_catalog."default",
    employer_id character varying(24) COLLATE pg_catalog."default",
    customer_id character varying(24) COLLATE pg_catalog."default",
    case_id character varying(24) COLLATE pg_catalog."default",
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    prior_rtw_date timestamp with time zone,
    prior_rtw_days bigint,
    is_deleted boolean,
    CONSTRAINT dim_relapse_pkey PRIMARY KEY (dim_relapse_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_relapse
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_relapse TO absence_tracker;

GRANT ALL ON TABLE public.dim_relapse TO etldw;

GRANT ALL ON TABLE public.dim_relapse TO postgres;