﻿-- Table: public.dim_employee_job

-- DROP TABLE public.dim_employee_job;

CREATE TABLE public.dim_employee_job
(
    dim_employee_job_id bigint NOT NULL DEFAULT nextval('dim_employee_job_dim_employee_job_id_seq'::regclass),
    ext_ref_id character varying(24),
    ext_created_date timestamp with time zone,
    ext_modified_date timestamp with time zone,
    dim_ver_id bigint NOT NULL,
    ver_is_current boolean,
    ver_seq bigint,
    ver_is_expired boolean,
    ver_root_id bigint,
    ver_prev_id bigint,
    ver_history_count bigint,
    ext_created_by character varying(24),
    ext_modified_by character varying(24),
    employee_id character varying(24),
    employer_id character varying(24),
    customer_id character varying(24),
    job_name text,
    job_code text,
    status_value int,
    status_reason int,
    status_comments text,
    status_username text,
    status_userid text,
    status_date timestamp with time zone,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
	is_deleted boolean default false,
    CONSTRAINT dim_employee_job_pkey PRIMARY KEY (dim_employee_job_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_job
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_job TO absence_tracker;

GRANT ALL ON TABLE public.dim_employee_job TO etldw;

GRANT ALL ON TABLE public.dim_employee_job TO postgres;