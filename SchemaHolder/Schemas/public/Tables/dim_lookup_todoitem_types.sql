-- Table: public.dim_lookup_todoitem_types

-- DROP TABLE public.dim_lookup_todoitem_types;

CREATE TABLE public.dim_lookup_todoitem_types
(
    _id bigint NOT NULL,
    code bigint,
    name text,
    category text,
    feature text
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_lookup_todoitem_types
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_lookup_todoitem_types TO absence_tracker;
GRANT ALL ON TABLE public.dim_lookup_todoitem_types TO etldw;
GRANT ALL ON TABLE public.dim_lookup_todoitem_types TO postgres;

-- Index: dim_lookup_todoitem_types_code

-- DROP UNIQUE INDEX IF EXISTS public.dim_lookup_todoitem_types_code;

CREATE UNIQUE INDEX IF NOT EXISTS dim_lookup_todoitem_types_code
    ON public.dim_lookup_todoitem_types USING btree
    (code)
    TABLESPACE pg_default;