-- Table: public.dim_employee_restriction_entry

-- DROP TABLE public.dim_employee_restriction_entry;

CREATE TABLE public.dim_employee_restriction_entry
(
    dim_employee_restriction_entry_id bigint NOT NULL DEFAULT nextval('dim_employee_restriction_entr_dim_employee_restriction_entr_seq'::regclass),
    dim_employee_restriction_id bigint,
    case_id character varying(24),
    ext_ref_demand_type_id character varying(24),
    wkrs_restriction_type text,
    wkrs_restriction text,
	is_deleted boolean default false,
	ver_is_current boolean default true,
    CONSTRAINT dim_employee_restriction_entry_pkey PRIMARY KEY (dim_employee_restriction_entry_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dim_employee_restriction_entry
    OWNER to postgres;

GRANT SELECT ON TABLE public.dim_employee_restriction_entry TO absence_tracker;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO etldw;
GRANT ALL ON TABLE public.dim_employee_restriction_entry TO postgres;

-- Index: dim_employee_restriction_entry_dim_employee_restriction_id

-- DROP INDEX public.dim_employee_restriction_entry_dim_employee_restriction_id;

CREATE INDEX IF NOT EXISTS dim_employee_restriction_entry_dim_employee_restriction_id
    ON public.dim_employee_restriction_entry USING btree
    (dim_employee_restriction_id, is_deleted)
    TABLESPACE pg_default;