-- Table: public.dim_attachment

-- Create Table dim_attachment
	CREATE TABLE IF NOT EXISTS public.dim_attachment
	(
		dim_attachment_id bigint NOT NULL DEFAULT nextval('dim_attachment_id_seq'::regclass),
		ext_ref_id character varying(36),
		ext_created_date timestamp with time zone,
		ext_modified_date timestamp with time zone,		
		ver_is_current boolean NOT NULL,
		ver_seq bigint NOT NULL,
		ver_is_expired boolean NOT NULL,
		ver_root_id bigint NOT NULL,
		ver_prev_id bigint NOT NULL,
		ver_history_count bigint,
		ext_created_by varchar(24),
		ext_modified_by varchar(24),
		employee_id character varying(24),
		employer_id character varying(24),
		customer_id character varying(24),
		case_id varchar(24) NOT NULL,
		checked boolean,
		content_length text,
		content_type text,
		attachment_type int,
		created_by_name text,
		description text,
		file_id character varying(24),
		file_Name text,
		public boolean,
		is_deleted boolean default false,
		created_date timestamp with time zone DEFAULT public.utc(),
		record_hash text,
		time_sample_date timestamp with time zone,
		time_total_minutes int,
		employee_number text,
		dim_variable_schedule_time_id int,
		CONSTRAINT dim_attachment_pkey PRIMARY KEY (dim_attachment_id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE public.dim_attachment
		OWNER to postgres;

	GRANT SELECT ON TABLE public.dim_attachment TO absence_tracker;
	GRANT ALL ON TABLE public.dim_attachment TO etldw;
	GRANT ALL ON TABLE public.dim_attachment TO postgres;

-- Index: dim_attachment_cur_ver_ref_id_is_del

-- DROP INDEX public.dim_attachment_cur_ver_ref_id_is_del;

CREATE INDEX IF NOT EXISTS dim_attachment_cur_ver_ref_id_is_del
    ON public.dim_attachment USING btree
    (ext_ref_id, ver_is_current, is_deleted)
    TABLESPACE pg_default;