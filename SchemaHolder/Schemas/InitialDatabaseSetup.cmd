﻿REM pass in location to base solution directory.
set loc=%1

REM - Run powershell scripts that make our schema scripts
powershell.exe "%loc%\SchemaHolder\Schemas\audit\Script\Schema.ps1" "%loc%\SchemaHolder\Schemas\audit"
powershell.exe "%loc%\SchemaHolder\Schemas\operational\Script\Schema.ps1" "%loc%\SchemaHolder\Schemas\operational"
powershell.exe "%loc%\SchemaHolder\Schemas\public\Script\Schema.ps1" "%loc%\SchemaHolder\Schemas\public"
REM powershell.exe "%loc%\SchemaHolder\Schemas\archive\Script\Schema.ps1" "%loc%\SchemaHolder\Schemas\archive"
powershell.exe "%loc%\SchemaHolder\Schemas\el_temp\Script\Schema.ps1" "%loc%\SchemaHolder\Schemas\el_temp"

REM - Now run files against database. Environment variables are used to make --no-password switch work for psql
REM - ENV VARIABLES NEEDED: PGDATABASE, PGHOST, PGPASSWORD, PGUSER
psql --file="%loc%\SchemaHolder\Schemas\audit\Script\Schema.sql" --no-password
psql --file="%loc%\SchemaHolder\Schemas\operational\Script\Schema.sql" --no-password
psql --file="%loc%\SchemaHolder\Schemas\public\Script\Schema.sql" --no-password
psql --file="%loc%\SchemaHolder\Schemas\el_temp\Script\Schema.sql" --no-password