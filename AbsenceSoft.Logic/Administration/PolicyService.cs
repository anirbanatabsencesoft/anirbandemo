﻿
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    public class PolicyService : BaseExpressionService
    {
        public PolicyService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        public PolicyService(string customerId, string employerId, User u = null) : base(customerId, employerId, u)
        {

        }//end: .ctor

        public ListResults PolicyList(ListCriteria criteria)
        {
            using (new InstrumentationContext("PolicyService.PolicyList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string code = criteria.Get<string>("Code");
                string name = criteria.Get<string>("Name");
                string workState = criteria.Get<string>("WorkState");

                long? policyType = null;
                string policyTypeText = criteria.Get<string>("PolicyType");
                if (!string.IsNullOrEmpty(policyTypeText))
                    policyType = Convert.ToInt64(policyTypeText);


                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                // HACK: Since we can't use set based SQL style operations against MongoDB, to exclude
                //  the result of another query, we have to execute that query first outselves, then use
                //  the results to custom build our other query (yuck).
                List<string> customPolicies = Policy.AsQueryable().Where(p => p.EmployerId == EmployerId).Select(p => p.Code).ToList();
                ands.Add(Policy.Query.Or(
                    Policy.Query.EQ(e => e.EmployerId, EmployerId),
                    Policy.Query.And(
                        Policy.Query.EQ(e => e.EmployerId, null),
                        Policy.Query.NotIn(e => e.Code, customPolicies)
                    )
                ));

                Policy.Query.MatchesString(e => e.Code, code, ands);
                Policy.Query.MatchesString(e => e.Name, name, ands);
                Policy.Query.MatchesString(e => e.WorkState, workState, ands);

                if (policyType.HasValue)
                    ands.Add(Policy.Query.EQ(e => e.PolicyType, (PolicyType)policyType));


                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0 || (!criteria.OnlyPageRecords)) skip = 0;
                var query = Policy.Query.Find(ands.Count > 0 ? Policy.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetFields(Policy.Query.IncludeFields(
                        e => e.Code,
                        e => e.Name,
                        e => e.PolicyType,
                        e => e.WorkState,
                        e => e.EmployerId
                    ));

                if (criteria.PageSize > 0)
                    query.SetLimit(criteria.PageSize * (criteria.OnlyPageRecords ? 1 : criteria.PageNumber));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                        ? MongoDB.Driver.Builders.SortBy.Ascending(criteria.SortBy)
                        : MongoDB.Driver.Builders.SortBy.Descending(criteria.SortBy));

                result.Total = query.Count();
                Employer er = null;
                if (result.Total > 0)
                    er = Employer.GetById(EmployerId) ?? new Employer();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("Id", e.Id)
                    .Set("Code", e.Code)
                    .Set("Name", e.Name)
                    .Set("PolicyType", e.PolicyType.ToString().SplitCamelCaseString())
                    .Set("WorkState", e.WorkState)
                    .Set("IsCore", string.IsNullOrWhiteSpace(e.EmployerId))
                    .Set("Disabled", IsPolicyDisabledInCurrentContext(e.Code))
                    .Set("EmployerId", e.EmployerId)
                );

                return result;
            }
        }//end: PolicyList

        public List<PolicyCrosswalkType> GetAllPolicyCrosswalkTypes()
        {
            return PolicyCrosswalkType.DistinctFind(null, CustomerId, EmployerId).ToList();
        }

        public PolicyCrosswalkType GetPolicyCrosswalkTypeById(string id)
        {
            return PolicyCrosswalkType.GetById(id);
        }

        public PolicyCrosswalkType SavePolicyCrosswalkType(PolicyCrosswalkType type)
        {
            using (new InstrumentationContext("PolicyService.SavePolicyCrosswalkType"))
            {
                PolicyCrosswalkType savedType = PolicyCrosswalkType.GetById(type.Id);
                if (savedType != null && (!savedType.IsCustom || savedType.EmployerId != EmployerId))
                {
                    type.Clean();
                }

                type.CustomerId = CustomerId;
                type.EmployerId = EmployerId;

                return type.Save();
            }
        }

        public void DeletePolicyCrosswalkType(PolicyCrosswalkType type)
        {
            if (!type.IsCustom)
                return;

            type.Delete();
        }

        /// <summary>
        /// Gets a list of policy crosswalk types
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults PolicyCrosswalkList(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults result = new ListResults(criteria);
            string name = criteria.Get<string>("Name");
            string code = criteria.Get<string>("Code");

            List<IMongoQuery> ands = PolicyCrosswalkType.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            PolicyCrosswalkType.Query.MatchesString(at => at.Name, name, ands);
            PolicyCrosswalkType.Query.MatchesString(at => at.Code, code, ands);
            List<PolicyCrosswalkType> types = PolicyCrosswalkType.DistinctAggregation(ands);
            result.Total = types.Count;
            types = types
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = types.Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("CustomerId", c.CustomerId)
                .Set("EmployerId", c.EmployerId)
                .Set("Name", c.Name)
                .Set("Code", c.Code)
                .Set("ModifiedDate", c.ModifiedDate)
            );

            return result;
        }


        /// <summary>
        /// This method checks whether a policy code belongs to the suppressed policies of an employer or customer.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool IsPolicyDisabledInCurrentContext(string code)
        {
            bool returnValue = false;

            if (CurrentEmployer != null)
                returnValue = CurrentEmployer.SuppressPolicies.Contains(code, StringComparer.InvariantCultureIgnoreCase);

            if (CurrentCustomer != null && !returnValue)
                returnValue = CurrentCustomer.SuppressPolicies.Contains(code, StringComparer.InvariantCultureIgnoreCase);

            return returnValue;
        }


        private bool IsAbsenceReasonDisabledInCurrentContext(string code)
        {
            bool returnValue = false;

            if (CurrentEmployer != null)
                returnValue = CurrentEmployer.SuppressReasons.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            if (CurrentCustomer != null && !returnValue)
                returnValue = CurrentCustomer.SuppressReasons.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            return returnValue;
        }

        /// <summary>
        /// Gets an absence reason by the unique id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AbsenceReason GetAbsenceReason(string code)
        {
            return AbsenceReason.GetByCode(code,CustomerId,EmployerId);
        }

        public List<string> GetPolicyCodes()
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            List<string> customPolicies = Policy.AsQueryable().Where(p => p.EmployerId == EmployerId).Select(p => p.Code).ToList();
            ands.Add(Policy.Query.Or(
                Policy.Query.EQ(e => e.EmployerId, EmployerId),
                Policy.Query.And(
                    Policy.Query.EQ(e => e.EmployerId, null),
                    Policy.Query.NotInIgnoreCase(e => e.Code, customPolicies)
                )
            ));
            return Policy.Query.Find(ands.Count > 0 ? Policy.Query.And(ands) : null).ToList().Select(policy => policy.Code).ToList();
        }

        /// <summary>
        /// Retrieves a unique list of absence reasons by code
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults AbsenceReasonList(ListCriteria criteria)
        {
            using (new InstrumentationContext("PolicyService.ReasonList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string code = criteria.Get<string>("Code");
                string name = criteria.Get<string>("Name");
                string cat = criteria.Get<string>("Category");

                List<IMongoQuery> ands = AbsenceReason.DistinctAnds(CurrentUser, CustomerId, EmployerId);
                AbsenceReason.Query.MatchesString(ar => ar.Code, code, ands);
                AbsenceReason.Query.MatchesString(ar => ar.Name, name, ands);
                AbsenceReason.Query.MatchesString(ar => ar.Category, cat, ands);

                List<AbsenceReason> absenceReasons = AbsenceReason.DistinctAggregation(ands);
                result.Total = absenceReasons.Count();

                if (criteria.PageSize > 0)
                    absenceReasons = absenceReasons.SortByListCriteria(criteria).PageByListCriteria(criteria).ToList();
                else
                    absenceReasons = absenceReasons.SortByListCriteria(criteria).ToList();

                result.Results = absenceReasons.ToList().Select(e => new ListResult()
                    .Set("Id", e.Id)
                    .Set("CustomerId", e.CustomerId)
                    .Set("EmployerId", e.EmployerId)
                    .Set("Code", e.Code)
                    .Set("Name", e.Name)
                    .Set("Category", e.Category)
                    .Set("IsCore", !e.IsCustom)
                    .Set("Disabled", IsAbsenceReasonDisabledInCurrentContext(e.Code))
                    .Set("ModifiedDate", e.ModifiedDate)
                );

                return result;
            }
        }//end: CustomReasonList

        public List<AbsenceReason> GetAllAbsenceReasons()
        {
            var customerReason = Customer.Current.SuppressReasons.ToList();
            var absenceReasons = AbsenceReason.DistinctFind(null, CustomerId, EmployerId, false).ToList();
            absenceReasons.RemoveAll(x => x.Code != null && customerReason.Any(c => c.Contains(x.Code)));
            return absenceReasons;
        }

        public ListResults PolicyAbsReasonList(ListCriteria criteria)
        {
            using (new InstrumentationContext("PolicyService.PolicyAbsReasonList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string policyId = criteria.Get<string>("PolicyId");
                string policyCode = criteria.Get<string>("PolicyCode");
                string employerId = criteria.Get<string>("EmployerId") ?? EmployerId;
                string absReasonName = criteria.Get<string>("AbsReasonName");
                long? entType = criteria.Get<long?>("EntitlementType");
                string workState = criteria.Get<string>("WorkState");
                string residenceState = criteria.Get<string>("ResidenceState");

                EntitlementType? entitlementType = null;
                if (entType.HasValue)
                    entitlementType = ((EntitlementType)entType);

                Policy p = string.IsNullOrWhiteSpace(policyId) ? Policy.GetById(policyId) : Policy.GetByCode(policyCode, this.CurrentCustomer.Id, employerId, true);
                List<PolicyAbsenceReason> resultList = new List<PolicyAbsenceReason>();
                bool noCriteria = true;

                if (!string.IsNullOrEmpty(absReasonName))
                {
                    resultList.AddRangeFluid(p.AbsenceReasons.Where(a => a.Reason.Name.ToLower().StartsWith(absReasonName)).ToList());
                    noCriteria = false;
                }

                if (entitlementType.HasValue)
                {
                    resultList.AddRangeFluid(p.AbsenceReasons.Where(a => a.EntitlementType == entitlementType).ToList());
                    noCriteria = false;
                }


                if (!string.IsNullOrEmpty(workState))
                {
                    resultList.AddRangeFluid(p.AbsenceReasons.Where(a => !string.IsNullOrEmpty(a.WorkState) && a.WorkState.ToLowerInvariant().Contains(workState.ToLowerInvariant())).ToList());
                    noCriteria = false;
                }

                if (!string.IsNullOrWhiteSpace(residenceState))
                {
                    resultList.AddRangeFluid(p.AbsenceReasons.Where(a => a.ResidenceState.Equals(residenceState)).ToList());
                    noCriteria = false;
                }

                if (noCriteria)
                    resultList = p.AbsenceReasons;

                result.Total = resultList.Count();

                result.Results = resultList.Select(e => new ListResult()
                    .Set("PolicyId", policyId)                    
                    .Set("ReasonCode", e.ReasonCode)
                    .Set("AbsReasonName", e.Reason == null ? AbsenceReason.GetByCode(e.ReasonCode, CustomerId, EmployerId).Name : e.Reason.Name)
                    .Set("EntitlementType", e.EntitlementType.ToString())
                    .Set("WorkState", e.WorkState)
                );

                return result;
            }
        }//end: CustomReasonList

        public ListResults PolicySearch(ListCriteria criteria)
        {
            using (new InstrumentationContext("PolicyService.PolicySearch"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string text = criteria.Get<string>("text");

                List<IMongoQuery> ands = new List<IMongoQuery>();

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (string.IsNullOrWhiteSpace(employerId) && Employer.Current != null)
                    employerId = Employer.Current.Id;
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Policy.Query.Or(
                        Policy.Query.EQ(e => e.EmployerId, employerId),
                        Policy.Query.EQ(e => e.EmployerId, null)));


                if (!string.IsNullOrWhiteSpace(text))
                {
                    var reggie = new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(text), RegexOptions.IgnoreCase));
                    ands.Add(Policy.Query.Or(
                        Policy.Query.Matches(e => e.Name, reggie),
                        Policy.Query.Matches(e => e.Code, reggie)
                    ));
                }

                var query = Policy.Query.Find(ands.Count > 0 ? Policy.Query.And(ands) : null)
                    .SetFields(Policy.Query.IncludeFields(
                        e => e.Id,
                        e => e.Name,
                        e => e.Code
                    ));

                result.Total = query.Count();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("id", e.Code)
                    .Set("text", e.Name)
                );

                return result;
            }
        }

        public void RenameReason(string reasonCode, string name)
        {
            if (string.IsNullOrWhiteSpace(reasonCode))
                throw new ArgumentNullException("reasonCode");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.RenameReason"))
            {
                AbsenceReason reason = AbsenceReason.GetByCode(reasonCode, CustomerId, EmployerId);
                if (reason == null)
                    throw new AbsenceSoftException(string.Format("Unable to find the absence reason with code '{0}' to rename.", reasonCode));
                if (string.IsNullOrWhiteSpace(reason.EmployerId) || reason.EmployerId != EmployerId)
                    throw new AbsenceSoftException("You may not rename a core absence reason or another employer's absence reason.");
                reason.Name = name;
                reason.Save();
            }
        }//end: RenameReason

        public void DisableReason(string reasonCode)
        {
            if (string.IsNullOrWhiteSpace(reasonCode))
                throw new ArgumentNullException("reasonCode");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.DisableReason"))
            {
                var emp = Employer.GetById(EmployerId);
                if (emp.SuppressReasons == null)
                    emp.SuppressReasons = new List<string>();
                emp.SuppressReasons.AddIfNotExists(reasonCode.ToUpperInvariant());
                emp.Save();
            }
        }//end: DisableReason

        public void EnableReason(string reasonCode)
        {
            if (string.IsNullOrWhiteSpace(reasonCode))
                throw new ArgumentNullException("reasonCode");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.EnableReason"))
            {
                var emp = Employer.GetById(EmployerId);
                if (emp.SuppressReasons == null || !emp.SuppressReasons.Contains(reasonCode.ToUpperInvariant()))
                    return;
                emp.SuppressReasons.Remove(reasonCode.ToUpperInvariant());
                emp.Save();
            }
        }//end: EnableReason

        public bool DeleteReason(AbsenceReason reason)
        {
            if (reason == null)
                return false;

            if (!CurrentUser.HasEmployerAccess(reason.EmployerId))
                return false;

            if (!reason.IsCustom)
                return false;

            if (reason.IsNew)
                return true;

            reason.Delete();

            return true;
        }//end: DeleteReason

        public AbsenceReason ToggleAbsenceReasonByCode(string code)
        {
            using (new InstrumentationContext("PolicyService.ToggleAbsenceReasonByCode"))
            {
                AbsenceReason absenceReason = AbsenceReason.GetByCode(code, CustomerId, EmployerId, true);
                if (CurrentEmployer != null)
                {
                    return ToggleAbsenceReasonForEmployer(absenceReason);
                }
                if (CurrentCustomer != null)
                {
                    return ToggleAbsenceReasonForCustomer(absenceReason);
                }
                /// This should not be reachable, as we should always have a customer or employer
                /// But just in case it is, we're going to return the absence reason as is
                return absenceReason;
            }
        }

        private AbsenceReason ToggleAbsenceReasonForEmployer(AbsenceReason reason)
        {
            if (CurrentEmployer.SuppressReasons.Contains(reason.Code))
            {
                CurrentEmployer.SuppressReasons.RemoveAll(w => w == reason.Code);                
            }
            else
            {
                CurrentEmployer.SuppressReasons.Add(reason.Code);
            }

            CurrentEmployer.Save();
            return reason;
        }

        private AbsenceReason ToggleAbsenceReasonForCustomer(AbsenceReason reason)
        {
            if (CurrentCustomer.SuppressReasons.Contains(reason.Code, StringComparer.InvariantCultureIgnoreCase))
            {
                CurrentCustomer.SuppressReasons.RemoveAll(w => string.Equals(w, reason.Code, StringComparison.InvariantCultureIgnoreCase));                
            }
            else
            {
                CurrentCustomer.SuppressReasons.Add(reason.Code.ToUpperInvariant());
            }

            CurrentCustomer.Save();

            return reason;
        }

        public void DisablePolicy(string policyCode)
        {
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.DisablePolicy"))
            {
                var emp = Employer.GetById(EmployerId);
                if (emp.SuppressPolicies == null)
                    emp.SuppressPolicies = new List<string>();
                emp.SuppressPolicies.AddIfNotExists(policyCode.ToUpperInvariant(), c => string.Equals(c, policyCode, StringComparison.InvariantCultureIgnoreCase));
                emp.Save();
            }
        }//end: DisablePolicy

        public void EnablePolicy(string policyCode)
        {
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.EnablePolicy"))
            {
                var emp = Employer.GetById(EmployerId);
                if (emp.SuppressPolicies == null || !emp.SuppressPolicies.Contains(policyCode, StringComparer.InvariantCultureIgnoreCase))
                    return;
                emp.SuppressPolicies.RemoveAll(p => string.Equals(p, policyCode, StringComparison.InvariantCultureIgnoreCase));
                emp.Save();
            }
        }//end: EnablePolicy

        public void DeleteOrRevertPolicy(string policyCode)
        {
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.DeleteOrRevertPolicy"))
            {
                Policy policy = Policy.AsQueryable().Where(p => p.Code == policyCode && p.EmployerId == EmployerId).FirstOrDefault();
                if (policy == null)
                    return;
                if (string.IsNullOrWhiteSpace(policy.EmployerId) || policy.EmployerId != EmployerId)
                {
                    DisablePolicy(policyCode);
                    return;
                }
                policy.Delete();
            }
        }//end: DeleteOrRevertPolicy

        public AbsenceReason UpsertAbsenceReason(string reasonCode, string name, string category, string helpText)
        {
            if (string.IsNullOrWhiteSpace(reasonCode))
                throw new ArgumentNullException("reasonCode");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.UpsertAbsenceReason"))
            {
                reasonCode = reasonCode.ToUpperInvariant();
                if (AbsenceReason.AsQueryable().Any(q => q.Code == reasonCode && q.EmployerId == null))
                    throw new AbsenceSoftException(string.Format("The code '{0}' selected for '{1}' is a system code and may not be altered. Try using a company prefix or other indicator to ensure your reason code is unique.", reasonCode, name));

                AbsenceReason reason = AbsenceReason.AsQueryable().Where(r => r.Code == reasonCode && r.EmployerId == EmployerId).FirstOrDefault();
                if (reason == null)
                    return new AbsenceReason()
                    {
                        Code = reasonCode,
                        Category = category,
                        Name = name,
                        CustomerId = CustomerId,
                        EmployerId = EmployerId,
                        HelpText = helpText
                    }.Save();

                reason.Name = name;
                reason.Category = category;
                reason.HelpText = helpText;
                return reason.Save();
            }
        }//end: UpsertAbsenceReason

        public Policy UpsertPolicy(Policy policy)
        {
            if (policy == null)
                throw new ArgumentNullException("policy");
            if (string.IsNullOrWhiteSpace(policy.Code))
                throw new ArgumentNullException("policy.Code");
            if (string.IsNullOrWhiteSpace(policy.Name))
                throw new ArgumentNullException("policy.Name");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.UpsertPolicy"))
            {
                policy.Code = policy.Code.ToUpperInvariant();
                Policy myPolicy = GetPolicyOrCoreClone(policy.Id, policy.Code);

                // If this is a new policy, create one
                if (myPolicy == null)
                {
                    myPolicy = policy;
                    myPolicy.Id = null;
                    myPolicy.Code = policy.Code;
                    myPolicy.CustomerId = CustomerId;
                    myPolicy.EmployerId = EmployerId;
                    myPolicy.PolicyBehaviorType = policy.PolicyBehaviorType.HasValue ? policy.PolicyBehaviorType : PolicyBehavior.ReducesWorkWeek;
                    return myPolicy.Save();
                }

                // Set the policy parameters, and then save it
                myPolicy.Name = policy.Name;
                myPolicy.PolicyBehaviorType = policy.PolicyBehaviorType.HasValue ? policy.PolicyBehaviorType : PolicyBehavior.ReducesWorkWeek;
                myPolicy.Code = policy.Code;
                myPolicy.Description = policy.Description;
                myPolicy.EffectiveDate = policy.EffectiveDate;
                myPolicy.PolicyType = policy.PolicyType;
                myPolicy.WorkState = string.IsNullOrWhiteSpace(policy.WorkState) ? null : policy.WorkState;
                myPolicy.ResidenceState = string.IsNullOrWhiteSpace(policy.ResidenceState) ? null : policy.ResidenceState;
                myPolicy.WorkCountry = string.IsNullOrWhiteSpace(policy.WorkCountry) ? null : policy.WorkCountry;
                myPolicy.ResidenceCountry = string.IsNullOrWhiteSpace(policy.ResidenceCountry) ? null : policy.ResidenceCountry;
                myPolicy.Gender = policy.Gender;
                myPolicy.AbsenceReasons = policy.AbsenceReasons;
                myPolicy.ConsecutiveTo = policy.ConsecutiveTo;
                myPolicy.SubstituteFor = policy.SubstituteFor;
                myPolicy.RuleGroups = policy.RuleGroups;
                myPolicy.IsDisabled = policy.IsDisabled;
                return myPolicy.Save();
            }
        }//end: UpsertPolicy

        public void AddConsecutivePolicy(string policyId, string consecutiveTo)
        {
            if (string.IsNullOrWhiteSpace(policyId))
                throw new ArgumentNullException("id");
            if (string.IsNullOrWhiteSpace(consecutiveTo))
                throw new ArgumentNullException("consecutiveTo");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.AddConsecutivePolicy"))
            {
                Policy target = GetPolicyOrCoreClone(policyId, null);
                if (target == null)
                    throw new AbsenceSoftException("Policy does not exist");
                if (target.ConsecutiveTo == null)
                    target.ConsecutiveTo = new List<string>();
                target.ConsecutiveTo.AddIfNotExists(consecutiveTo.ToUpperInvariant());
                target.Save();
            }
        }//end: AddConsecutivePolicy

        public void RemoveConsecutivePolicy(string policyId, string consecutiveTo)
        {
            if (string.IsNullOrWhiteSpace(policyId))
                throw new ArgumentNullException("id");
            if (string.IsNullOrWhiteSpace(consecutiveTo))
                throw new ArgumentNullException("consecutiveTo");
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.AddConsecutivePolicy"))
            {
                Policy target = GetPolicyOrCoreClone(policyId, null);
                if (target == null)
                    throw new AbsenceSoftException("Policy does not exist");
                if (target.ConsecutiveTo == null)
                    return;
                target.ConsecutiveTo.Remove(consecutiveTo.ToUpperInvariant());
                target.Save();
            }
        }//end: RemoveConsecutivePolicy

        public Dictionary<string, string> GetConsecutivePolicies(string policyId)
        {
            using (var context = new InstrumentationContext("PolicyService.GetConsecutivePolicies"))
            {
                if (string.IsNullOrWhiteSpace(policyId))
                    throw new ArgumentNullException("id");

                var policy = Policy.GetById(policyId);
                if (policy == null)
                    throw new AbsenceSoftException(string.Format("Policy with Id '{0}' does not exist", policyId));

                if (policy.ConsecutiveTo == null || policy.ConsecutiveTo.Count == 0)
                    return new Dictionary<string, string>();

                var policies = Policy.AsQueryable()
                        .Where(p => p.EmployerId == null || p.EmployerId == EmployerId)
                        .Where(p => policy.ConsecutiveTo.Contains(p.Code))
                        .OrderBy(p => p.EmployerId == null ? 999 : 0)
                        .ToList();

                Dictionary<string, string> ret = new Dictionary<string, string>();
                policies.ForEach(p =>
                {
                    if (!ret.ContainsKey(p.Code))
                        ret.Add(p.Code, p.Name);
                });
                return ret;
            }
        }//end: GetConsecutivePolicies



        public Policy GetPolicyById(string policyId)
        {
            Policy p = Policy.GetById(policyId);
            Employer e = Employer.GetById(EmployerId);
            p.IsDisabled = e.SuppressPolicies.Contains(p.Code);
            return p;
        }

        private Policy GetPolicyOrCoreClone(string policyId, string code)
        {
            using (InstrumentationContext context = new InstrumentationContext("PolicyService.CloneCorePolicy"))
            {
                Policy target = null;
                if (!string.IsNullOrWhiteSpace(policyId))
                    target = Policy.GetById(policyId);

                if (target == null && string.IsNullOrWhiteSpace(code))
                    return null;
                else if (target != null && target.EmployerId == EmployerId)
                    return target;
                else if (target != null)
                    code = target.Code;

                target = Policy.AsQueryable()
                     .Where(p => p.EmployerId == null || p.EmployerId == EmployerId)
                     .Where(p => p.Code == code).ToList()
                     .OrderBy(p => p.EmployerId == null ? 999 : 0)
                     .FirstOrDefault() ?? target;
                if (target == null)
                    return null;

                if (target.EmployerId == EmployerId)
                    return target;

                target = target.Clone();
                target.Clean();
                target.CustomerId = CustomerId;
                target.EmployerId = EmployerId;

                return target;
            }
        }//end: CloneCorePolicy

        /// <summary>
        /// Updates an absence reason.  Will overwrite if is already the customers, if not it will create a copy and save it to the customer
        /// </summary>
        /// <param name="absenceReason"></param>
        /// <returns></returns>
        public AbsenceReason SaveAbsenceReason(AbsenceReason absenceReason)
        {
            using (new InstrumentationContext("PolicyService.SaveAbsenceReason"))
            {
                AbsenceReason savedAbsenceReason = AbsenceReason.GetById(absenceReason.Id);
                if (savedAbsenceReason != null &&
                    (!savedAbsenceReason.IsCustom || savedAbsenceReason.EmployerId != EmployerId))
                {
                    absenceReason.Clean();
                }

                absenceReason.CustomerId = CustomerId;
                absenceReason.EmployerId = EmployerId;

                return absenceReason.Save();
            }
        }

        /// <summary>
        ///  Updates an absence reason. Will overwrite if is already the customers,
        /// </summary>
        /// <param name="absenceReason"></param>
        /// <returns></returns>
        public AbsenceReason DisableAbsenceReason(AbsenceReason absenceReason)
        {
            using (new InstrumentationContext("PolicyService.SaveAbsenceReason"))
            {
                AbsenceReason savedAbsenceReason = AbsenceReason.GetById(absenceReason.Id);
                savedAbsenceReason.IsDisabled = true;
                absenceReason.Suppressed = true;

                absenceReason.IsCustom = savedAbsenceReason.IsCustom;
                return absenceReason.Save();
            }
        }

        public AbsenceReason EnableAbsenceReason(AbsenceReason absenceReason)
        {
            using (new InstrumentationContext("PolicyService.SaveAbsenceReason"))
            {
                AbsenceReason savedAbsenceReason = AbsenceReason.GetById(absenceReason.Id);
                absenceReason.IsDisabled = false;
                savedAbsenceReason.IsDisabled = false;

                absenceReason.IsCustom = savedAbsenceReason.IsCustom;
                absenceReason.Suppressed = false;
                absenceReason.CustomerId = null;
                absenceReason.EmployerId = null;
                return absenceReason.Save();
            }
        }
    }//end: PolicyService
}//end: Namespace
