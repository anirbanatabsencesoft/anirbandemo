﻿
using AbsenceSoft.Common.Security;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.Properties;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Cases;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Users;
using System.Xml;
using System.Web;
using AT.Common.Core.NotificationEnums;
using AbsenceSoft.Logic.Notification;
using AbsenceSoft.Logic.Administration.Contracts;

namespace AbsenceSoft.Logic.Administration
{
    public class AdministrationService : LogicService, IAdministrationService
    {

        public AdministrationService()
        {

        }

        public AdministrationService(User currentUser)
            : base(currentUser)
        {

        }



        public AdministrationService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        private ExportService exportService = new ExportService();

        public bool CustomerHasFeature(Feature feature)
        {
            return CurrentCustomer.HasFeature(feature);
        }

        public bool UserHasPermission(Permission permission)
        {
            return User.Permissions.GetPermissions(CurrentUser, EmployerId).Contains(permission.Id);
        }


        public bool UserHasPermissionFromList(Permission permission, List<string> permissionList)
        {
            return permissionList.Contains(permission.Id);
        }

        public List<string> GetUserPermissionsList()
        {
            return User.Permissions.GetPermissions(CurrentUser, EmployerId);

        }



        public bool HasFeatureAndPermission(Feature feature, Permission permission, List<string> permissions)
        {
            return CustomerHasFeature(feature) && UserHasPermissionFromList(permission, permissions);
        }

        /// <summary>
        /// Check current password user entered is right or not
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPassword"></param>
        /// <returns></returns>
        public bool CheckPassword(User user, string currentPassword)
        {
            using (new InstrumentationContext("AdministrationService.CheckPassword"))
            {
                // 1. Get user by email address                
                // 2. Hash password
                // 3. Compare password hashes and validate
                // 4. If valid, return the user, otherwise null

                // var user = User.AsQueryable().Where(c => c.Id == userId).FirstOrDefault();
                if (user != null && user.Password.Hashed.CompareHash(currentPassword))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }



        /// <summary>
        /// Update user password and send email of intimation
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool ChangePassword(User user, string newPassword)
        {
            using (new InstrumentationContext("AdministrationService.ChangePassword"))
            {
                if (string.IsNullOrWhiteSpace(user.Id))
                {
                    throw new ArgumentNullException("UserId");
                }

                PasswordPolicy policy = PasswordPolicy.Default();
                if (user.Customer != null)
                {
                    policy = user.Customer.PasswordPolicy;
                }
                // Validate password min length
                var minlen = policy.MinimumLength;
                if (minlen > 0 && newPassword.Length < minlen)
                {
                    throw new AbsenceSoftException(string.Format(
                        "New password must be at least {0} characters long.",
                        policy.MinimumLength
                    ));
                }

                // 1. Validate password complexity
                using (AuthenticationService authService = new AuthenticationService())
                {
                    ICollection<string> errors;

                    if (!authService.ValidatePasswordAgainstPolicy(policy, user, newPassword, out errors))
                    {
                        throw new AbsenceSoftException(string.Join("\n", errors));
                    }
                }

                // 2. Find user by userId
                // User user = User.AsQueryable().Where(c => c.Id == userId).FirstOrDefault();
                if (user == null)
                {
                    throw new AbsenceSoftException("user not found");
                }

                // Set the new password
                user.Password = new CryptoString() { PlainText = newPassword };
                user.MustChangePassword = false;
                // Save the user account
                user.Save();

                // Send email

                // Get our forgot email password template
                string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_ChangePassword);

                body = Rendering.Template.RenderTemplate(body, new ForgotPasswordMessageData()
                {
                    FirstName = user.FirstName,
                });

                using (MailMessage msg = new MailMessage())
                {
                    msg.To.Add(user.Email);
                    msg.Subject = "AbsenceTracker Password Changed";
                    msg.Body = body;
                    msg.IsBodyHtml = true;
                    return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
                }              
            }
        }
        
        /// <summary>
        /// Delete current user and related data
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool DisableUser(User user)
        {
            user.Employers.Clear();
            user.Roles.Clear();
            user.ResetKey = null;
            user.UserFlags = Data.Enums.UserFlag.None;
            user.DisabledDate = DateTime.UtcNow;
            user.Save();
            return true;
        }

        /// <summary>
        /// Delete current employer
        /// </summary>
        /// <returns></returns>
        public void DeleteCurrentEmployer()
        {
            if (CurrentEmployer == null)
            {
                throw new AbsenceSoftException("Current Employer is null");
            }

            CurrentEmployer.IsDeleted = true;
            CurrentEmployer.Save();
        }

        /// <summary>
        /// Delete current employer
        /// </summary>
        /// <returns></returns>
        public void RestoreCurrentEmployer()
        {
            if (CurrentEmployer == null)
            {
                throw new AbsenceSoftException("Current Employer is null");
            }

            CurrentEmployer.IsDeleted = false;
            CurrentEmployer.Save();
        }



        /// <summary>
        /// To check if this is the only employer of the user
        /// </summary>
        /// <param name="employerId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool IsLastEmployer()
        {
            return AbsenceSoft.Data.Customers.Employer.AsQueryable().Count(m => m.CustomerId == CustomerId && m.Id != EmployerId && !m.IsDeleted) <= 0;
        }

        /// <summary>
        /// Get paged list of users
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults EmployersList(User user, ListCriteria criteria)
        {
            using (new InstrumentationContext("AdministrationService.EmployersList"))
            {
                if (criteria == null)
                {
                    criteria = new ListCriteria();
                }
                ListResults result = new ListResults(criteria);

                string employerFilter = criteria.Get<string>("EmployerName");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if ((user ?? User.Current) != null)
                {
                    ands.Add((user ?? User.Current).BuildDataAccessFilters());
                }

                //filter for employer name
                if (!string.IsNullOrWhiteSpace(employerFilter))
                {
                    ands.Add(Employer.Query.Matches(e => e.Name, new BsonRegularExpression(employerFilter, "i")));
                }

                // Ensure not deleted
                ands.Add(Employer.Query.NE(e => e.IsDeleted, true));

                // apply filter
                // Set fields
                var query = Employer.Query.Find(ands.Count > 0 ? Employer.Query.And(ands) : null)
                    .SetFields(Employer.Query.IncludeFields(
                        w => w.Name
                    ));

                // apply sorting
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                       ? SortBy.Ascending(criteria.SortBy)
                       : SortBy.Descending(criteria.SortBy));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0)
                {
                    skip = 0;
                }
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                result.Total = query.Count();
                result.Results = query.Select(c => new ListResult()
                    .Set("EmployerId", c.Id)
                    .Set("EmployerName", c.Name)
                    .Set("DoExportComplete", exportService.GetTotalLinks(c.Id).Count > 0 ? true : false)
                ).ToList();

                return result;
            }
        }//TodoItemList


        /// <summary>
        /// Get paged list of employers
        /// </summary>
        /// <param name="user"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults UsersList(ListCriteria criteria)
        {
            using (new InstrumentationContext("AdministrationService.EmployersList"))
            {
                if (criteria == null)
                {
                    criteria = new ListCriteria();
                }
                ListResults result = new ListResults(criteria);

                string userFilter = criteria.Get<string>("UserName");
                string roleFilter = criteria.Get<string>("Role");
                string emailFilter = criteria.Get<string>("Email");
                string employerFilter = criteria.Get<string>("Employer");
                var statusFilter = criteria.Get<string>("Status") ?? UserStatus.Active;

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if ((CurrentUser) != null)
                {
                    ands.Add(CurrentUser.BuildDataAccessFilters());
                    ands.Add(User.Query.EQ(e => e.CustomerId, CurrentUser.CustomerId));
                }
                string lName = null;
                string fName = null;
                // If the lastname and firstname are equal, then it's a "full name" filter, duh, so do an OR filter
                if (!string.IsNullOrWhiteSpace(userFilter))
                {
                    if (userFilter.Contains(','))
                    {
                        string[] arrName = userFilter.Split(',');
                        if (arrName.Length > 0)
                        {
                            lName = arrName[0].Trim();
                            fName = arrName[1].Trim();
                        }
                    }
                    else
                    {
                        var safeUserFilter = Regex.Escape(userFilter);
                        ands.Add(User.Query.Or(
                            User.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + safeUserFilter, RegexOptions.IgnoreCase))),
                            User.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + safeUserFilter, RegexOptions.IgnoreCase)))
                        ));
                    }
                }

                if (!string.IsNullOrWhiteSpace(lName))
                {
                    ands.Add(User.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(lName), RegexOptions.IgnoreCase))));
                }
                if (!string.IsNullOrWhiteSpace(fName))
                {
                    ands.Add(User.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(fName), RegexOptions.IgnoreCase))));
                }

                // If we're filtering on roles, then we need to check the primary roles collection or employer access (ESS) roles
                if (!string.IsNullOrWhiteSpace(roleFilter))
                {
                    ands.Add(User.Query.Or(
                        User.Query.EQ(e => e.Roles, roleFilter),
                        User.Query.ElemMatch(e => e.Employers, e => e.EQ(r => r.Roles, roleFilter))
                    ));
                }

                if (!string.IsNullOrWhiteSpace(emailFilter))
                {
                    ands.Add(User.Query.Where(e => e.Email.ToLower().StartsWith(emailFilter.ToLower())));
                }

                if (!string.IsNullOrWhiteSpace(employerFilter))
                {
                    ands.Add(User.Query.ElemMatch(e => e.Employers, m => m.EQ(k => k.EmployerId, employerFilter)));
                }

                if (statusFilter != "All")
                { 
                    if (statusFilter == UserStatus.Active)
                    {
                        ands.Add(User.Query.EQ(e => e.DisabledDate, null));
                        ands.Add(User.Query.Or(
                            User.Query.EQ(e => e.EndDate, null),
                            User.Query.GT(e => e.EndDate, DateTime.Now)
                        ));
                    }
                    else if (statusFilter == UserStatus.Disabled)
                    {
                        ands.Add(User.Query.Or(
                            User.Query.Exists(e => e.DisabledDate),
                            User.Query.LTE(e => e.EndDate, DateTime.Now)
                        ));
                    }
                    else if (statusFilter == UserStatus.Locked)
                    {                        
                        ands.Add(User.Query.Or(
                            User.Query.Exists(e => e.LockedDate)
                        ));
                    }
                }

                // apply filter
                // Set fields
                var fields = User.Query.RequiredFields();
                fields.AddIfNotExists("FirstName");
                fields.AddIfNotExists("LastName");
                fields.AddIfNotExists("Roles");
                fields.AddIfNotExists("Email");
                fields.AddIfNotExists("IsDeleted");
                fields.AddIfNotExists("EndDate");                
                fields.AddIfNotExists("DisabledDate");                
                fields.AddIfNotExists("LockedDate");
                fields.AddIfNotExists("ContactInfo");
                fields.AddIfNotExists("Employers._id");
                fields.AddIfNotExists("Employers.EmployerId");
                fields.AddIfNotExists("Employers.Roles");
                var query = User.Query.Find(ands.Count > 0 ? User.Query.And(ands) : null).SetFields(Fields.Include(fields.ToArray()));

                List<User> tempUsersList = query.ToList();

                // apply sorting
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLowerInvariant() == "name")
                    {// Need a compound order by because name is a psedo field.
                        if (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending)
                        {
                            tempUsersList = tempUsersList.OrderBy(q => (string.IsNullOrWhiteSpace(q.LastName) ? q.FirstName : q.LastName + ", " + q.FirstName).ToLowerInvariant()).ToList();// MongoDB.Driver.Builders.SortBy.Ascending("FirstName").Ascending("LastName")
                        }
                        else
                        {
                            tempUsersList = tempUsersList.OrderByDescending(q => (string.IsNullOrWhiteSpace(q.LastName) ? q.FirstName : q.LastName + ", " + q.FirstName).ToLowerInvariant()).ToList();// MongoDB.Driver.Builders.SortBy.Ascending("FirstName").Ascending("LastName")
                        }
                    }
                    else
                    {// Normal order by
                        if (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending)
                        {
                            tempUsersList = tempUsersList.OrderBy(criteria.SortBy).ToList();
                        }
                        else
                        {
                            tempUsersList = tempUsersList.OrderByDescending(criteria.SortBy).ToList();
                        }
                    }
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0)
                {
                    skip = 0;
                }
                result.Total = tempUsersList.Count();
                tempUsersList = tempUsersList.Skip(skip).Take(criteria.PageSize).ToList();

                List<Role> allRoles;
                using (var svc = new AuthenticationService())
                {
                    allRoles = svc.GetRoles(CurrentUser.CustomerId).OrderBy(x => x.Name).ToList();
                }

                var allEmployers = Employer.Repository.Collection.FindAs<BsonDocument>(
                    Employer.Query.And(Employer.Query.IsNotDeleted(), Employer.Query.EQ(e => e.CustomerId, CurrentUser.CustomerId)))
                    .SetFields(Fields<Employer>.Include(e => e.Id, e => e.Name)).ToDictionary(f => f["_id"].ToString(), f => f["Name"].ToString());

                result.Results = tempUsersList
                    .Select(c =>
                    {
                        var isLoggedInUser = c.Id == CurrentUser.Id;
                        var listResult = new ListResult()
                            .Set("Id", c.Id)
                            .Set("FirstName", c.FirstName)
                            .Set("LastName", c.LastName)
                            .Set("DisplayName", c.DisplayName)
                            .Set("Roles", GetAllCombinedRolesForTheUser(c))
                            .Set("RolesName", GetAllCombinedRoleNamesForTheUser(c, allRoles))
                            .Set("IsLoggedInUser", isLoggedInUser)
                            .Set("Email", c.Email)
                            .Set("Status", c.Status)
                            .Set("IsDeletable", !c.IsDeleted && CurrentUser.HasPermission(Permission.DisableUser) && !isLoggedInUser)
                            .Set("IsRestorable", c.IsDeleted && CurrentUser.HasPermission(Permission.EnableUser))
                            .Set("IsEditable", !c.IsDeleted)
                            .Set("EndDate", c.EndDate)
                            .Set("Employers", c.Employers
                                .Where(e => e.IsActive)
                                .Where(e => allEmployers.ContainsKey(e.EmployerId))
                                .Select(m => new { Id = m.EmployerId, Name = allEmployers[m.EmployerId], Roles = m.Roles }));
                        return listResult;
                    }).ToList();

                return result;
            }
        }//TodoItemList

        /// <summary>
        /// Gets all combined roles for the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private List<string> GetAllCombinedRolesForTheUser(User user)
        {
            List<string> roles = new List<string>();
            roles.AddRange(user.Roles);

            var moreThanOneEmployer = (user.Employers == null ? 0 : user.Employers.Count) > 1;
            var isTpa = user.Customer.HasFeature(Feature.MultiEmployerAccess) || moreThanOneEmployer;
            var isEss = user.Customer.HasFeature(Feature.EmployeeSelfService);
            if (user.Employers != null && (isTpa || isEss))
            {
                roles.AddRangeIfNotExists(user.Employers.SelectMany(e => e.Roles));
            }
            return roles.Distinct().ToList();
        }
        /// <summary>
        /// Gets all permissions that apply to a specific role type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Permission> GetPermissionsForRoleType(RoleType type, bool includeCommunications = false, bool includeToDoItems = false)
        {
            var permissions = Role.SystemAdministrator.GetPermissions().Where(p => p.RoleTypes.Contains(type));
            if (!includeCommunications)
            {
                permissions = permissions.Where(p => p.Category != "Communications");
            }
            if (!includeToDoItems)
            {
                permissions = permissions.Where(p => p.Category != "ToDo Items");
            }
            return permissions.ToList();
        }

        private List<string> GetAllCombinedRoleNamesForTheUser(User user, List<Role> allRoles)
        {
            return allRoles.Where(m => GetAllCombinedRolesForTheUser(user).Contains(m.Id))
                .Select(m => m.Type == RoleType.SelfService ? string.Format("{0} (ESS)", m.Name) : m.Name)
                .ToList();
        }

        /// <summary>
        /// Removes role from all users that have this role, then deletes the role itself
        /// </summary>
        /// <param name="r"></param>
        public void DeleteRole(Role r)
        {
            List<User> users = User.AsQueryable().Where(u => u.CustomerId == r.CustomerId && u.Roles.Contains(r.Id)).ToList();
            foreach (User u in users)
            {
                u.Roles.Remove(r.Id);
                u.Save();
            }

            r.Delete();
        }

        /// <summary>
        /// Restore as user that has been marked with
        /// IsDeleted flag
        /// </summary>
        /// <param name="userId"></param>
        public void EnableUser(string userId, DateTime? endDate)
        {
            var user = User.GetById(userId);
            if (user == null)
            {
                throw new InvalidOperationException("User not found with id: " + userId);
            }
            user.DisabledDate = null;
            user.EndDate = endDate.HasValue ? endDate.Value.EndOfDay() : endDate;
            if (user.EndDate != null && user.EndDate < DateTime.Now)
            {
                throw new InvalidOperationException(" User's End date has elapsed. ");
            }
            user.Save();
        }

        public User GetUserByEmail(string email)
        {
            return User.AsQueryable().FirstOrDefault(e => e.Email == email);
        }


        /// <summary>
        /// If user account gets locked for any reason, say, attempt to login multiple times with wrong credentials.
        /// Admin can unlock the user account.
        /// </summary>
        /// <param name="userId">UserId of the user to be unlocked</param>
        public void UnLockUser(string userId)
        {
            var user = User.GetById(userId);
            if (user == null)
            {
                throw new InvalidOperationException("User not found with id: " + userId);
            }
            user.LockedDate = null;
            user.Status = UserStatus.Active;
            user.Save();
        }

        #region Custom Fields
        public ListResults ListCustomFields(ListCriteria criteria)
        {
            if (criteria == null)
            {
                criteria = new ListCriteria();
            }

            ListResults results = new ListResults(criteria);
            string name = criteria.Get<string>("Name");
            string code = criteria.Get<string>("Code");
            string description = criteria.Get<string>("Description");

            List<IMongoQuery> ands = CustomField.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            CustomField.Query.MatchesString(cf => cf.Name, name, ands);
            CustomField.Query.MatchesString(cf => cf.Code, code, ands);
            CustomField.Query.MatchesString(cf => cf.Description, description, ands);

            List<CustomField> customFields = CustomField.DistinctAggregation(ands);
            results.Total = customFields.Count;

            customFields = customFields
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            results.Results = customFields.Select(cf => new ListResult()
                .Set("Id", cf.Id)
                .Set("CustomerId", cf.CustomerId)
                .Set("EmployerId", cf.EmployerId)
                .Set("Code", cf.Code)
                .Set("Name", cf.Name)
                .Set("Description", cf.Description)
                .Set("ModifiedDate", cf.ModifiedDate)
            ).ToList();

            return results;
        }

        public CustomField GetCustomField(string id)
        {
            return CustomField.GetById(id);
        }

        public CustomField SaveCustomField(CustomField field)
        {
            CustomField savedField = CustomField.GetById(field.Id);
            if (savedField != null &&
                (!savedField.IsCustom || savedField.EmployerId != EmployerId))
            {
                field.Clean();
            }

            field.CustomerId = CustomerId;
            field.EmployerId = EmployerId;
            return field.Save();
        }

        public void DeleteCustomField(CustomField field)
        {
            if (!field.IsCustom)
            {
                throw new AbsenceSoftException("Can not delete a default custom field");
            }
            field.Delete();
        }



        public List<Country> GetCountriesFromXML(bool includeRegions)
        {
            var countryXML = new XmlDocument();

            countryXML.LoadXml(AbsenceSoft.Logic.Properties.Resources.countries);
            var regionXML = new XmlDocument();
            if (includeRegions)
            {
                regionXML.LoadXml(AbsenceSoft.Logic.Properties.Resources.states);
            }

            var countries = new List<Country>();
            var countriesNodeList = countryXML.SelectNodes("//country");
            if (countriesNodeList == null)
            {
                return countries;
            }

            foreach (XmlNode country in countriesNodeList)
            {
                var regionNameAttribute = country.Attributes["region-name"];
                string regionName = null;
                if (regionNameAttribute != null)
                {
                    regionName = regionNameAttribute.Value;
                }

                var newCountry = new Country()
                {
                    Name = country.InnerText,
                    RegionName = regionName,
                    Code = country.Attributes["code"].Value
                };

                if (includeRegions)
                {
                    newCountry.Regions = GetRegionsForCountry(newCountry.Code, regionXML);
                }

                countries.Add(newCountry);
            }

            return countries;
        }

        private List<Region> GetRegionsForCountry(string countryCode, XmlDocument regionXML)
        {
            var regions = new List<Region>();
            var matchingRegions = regionXML.SelectNodes(string.Format("//state[@country='{0}']", countryCode));
            if (matchingRegions == null)
            {
                return regions;
            }

            foreach (XmlNode region in matchingRegions)
            {
                var newRegion = new Region()
                {
                    Name = region.InnerText,
                    Code = region.Attributes["code"].Value
                };
                regions.Add(newRegion);
            }

            return regions;
        }

        #endregion
    }
}
