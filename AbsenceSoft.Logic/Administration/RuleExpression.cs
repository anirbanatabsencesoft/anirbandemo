﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    [Serializable]
    public class RuleExpression
    {
        public RuleExpression()
        {
            Options = new List<RuleExpressionOption>();
            Parameters = new List<RuleExpressionParameter>();
            OnChangeMethodName = null;
        }

        //
        // ###############################
        // RULE VALUES (set by the user)
        // ###############################
        //
        /// <summary>
        /// Gets or sets the Id of the rule this applies to.
        /// </summary>
        public Guid? RuleId { get; set; }

        /// <summary>
        /// Gets or sets the description of the rule/expression; which can be edited by the user
        /// but may also be defaulted to provide some helpful context. This is primarily for admin
        /// purposes and is never displayed to a regular user outside of the configuration tools or
        /// API (if one is ever built).
        /// </summary>
        public string RuleDescription { get; set; }

        /// <summary>
        /// Gets or sets the name of the rule (how it should be displayed). This may be defaulted but
        /// is ultimately set by the user themselves.
        /// </summary>
        public string RuleName { get; set; }

        /// <summary>
        /// Gets or sets the actual operator to use for the rule, may be set as a default, but is
        /// selected by the user out of the available operators from the <see cref="P:Operators"/> collection.
        /// </summary>
        public string RuleOperator { get; set; }
        //
        // ###############################
        //

        /// <summary>
        /// Gets or sets the formal name of the LEFT field/expression, such as a property name, field name or method/function name
        /// extended on the loa object or other base expression eval object.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the textual label or prompt used to gather the expression, e.g. "First Name" or "Hours Worked Last 12 Months", etc.
        /// </summary>
        public string Prompt { get; set; }

        /// <summary>
        /// Gets or sets the help text to be shown on hover for the prompt of this rule to assist
        /// in proper usage. This isn't an easy concept, so this is important.
        /// </summary>
        public string HelpText { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this expression is a function call or not.
        /// </summary>
        public bool IsFunction { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not this is a custom field rule.
        /// </summary>
        public bool IsCustom { get; set; }

        /// <summary>
        /// Gets or sets the type of control for displaying the expression prompt in the UI.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Gets or sets the list of options, if any, for the expression prompt in the UI to choose from
        /// for select types, checkbox lists, etc.
        /// </summary>
        public List<RuleExpressionOption> Options { get; set; }

        /// <summary>
        /// Gets or set a hashtable of available expression operators that are allowed for this type.
        /// </summary>
        public Dictionary<string, string> Operators { get; set; }

        /// <summary>
        /// Gets or sets the RIGHT value (either default value or populated by the user, etc.) for this expression item.
        /// This value may be a Date, a string, a number, an Enumeration value (integer), a boolean, etc. but forms
        /// the right-hand side of the expression.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the tweener text that is used to separate the name and value as a representative
        /// plain English phrase.
        /// </summary>
        public string Tweener { get; set; }

        /// <summary>
        /// Gets or sets the collection of function parameters for use when defining an expression that calls a function
        /// that expects parameters.
        /// </summary>
        public List<RuleExpressionParameter> Parameters { get; set; }

        /// <summary>
        /// Gets the current value as a string.
        /// </summary>
        /// <returns></returns>
        public string StringValue
        {
            get
            {
                // If the value is null, return the default of type T
                if (Value == null) return "null";

                if (Value is DateTime || ControlType == ControlType.Date)
                    return string.Format("#{0:MM/dd/yyyy}#", Value);

                if (Value is bool)
                    return Value.ToString().ToLowerInvariant();

                if (Value is Guid)
                    return string.Format("Guid.Parse('{0}')", Value);

                // Get our type of T so we can use it for other tests and ensure .NET reflection caches the result
                //  as an instance on our memory heap for performance reasons
                Type type = Value.GetType();
                Type realType = string.IsNullOrWhiteSpace(TypeName) ? type : Type.GetType(TypeName, false) ?? type;
                if (realType != type)
                {
                    Value = Convert.ChangeType(Value, type);
                    type = realType;
                }

                // If it is an enum, we need to put in the actual enum name.value format.
                if (type.IsEnum)
                    return string.Format("{0}.{1}", type.Name, Enum.GetName(type, Value));

                // If it's a string, easy enough, just return the string, escape any characters that need escaping
                if (Value is string || Value is char)
                    return string.Format("'{0}'", Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'"));
                
                // Can't figure anything else out, so just return the default of T again. Maybe this
                //  should attempt to throw an exception instead, will need to look into that.
                string val = Value.ToString();
                return string.IsNullOrWhiteSpace(val) ? "''" : val;
            }//end: Get
            set
            {
                // Test is null or a null literal representation
                if (value == null || value == "null")
                {
                    Value = null;
                    return;
                }

                // Test if it is a Boolean value of true
                if (value.ToLowerInvariant() == "true")
                {
                    Value = true;
                    return;
                }

                // Test if it is a Boolean value of false
                if (value.ToLowerInvariant() == "false")
                {
                    Value = false;
                    return;
                }

                // Test if it is a string/char literal
                if (Regex.IsMatch(value, @"^'.*'$", RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                {
                    if (value.Length == 2)
                        Value = "";
                    else
                        Value = value.Substring(1, value.Length - 2).Replace("\\\\", "\\").Replace("\\'", "'");
                    return;
                }

                // Test if it is a Date
                if (value.StartsWith("#"))
                {
                    Value = DateTime.Parse(value.Replace("#", "")).ToMidnight();
                    return;
                }

                // Test if it is a Guid
                if (value.StartsWith("Guid.Parse"))
                {
                    Value = Guid.Parse(value.Replace("Guid.Parse('", "").Replace("')", ""));
                    return;
                }

                // Test if it is an Enum
                if (Regex.IsMatch(value, @"^[A-Za-z][A-Za-z0-9]{0,}\.[A-Za-z][A-Za-z0-9]{0,}$", RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                {
                    var enumParts = value.Split('.');
                    Type enumType = LeaveOfAbsenceEvaluator.GetEnumTypes().FirstOrDefault(t => t.Name == enumParts[0]);
                    if (enumType != null && enumType.IsEnum)
                    {
                        Value = Enum.Parse(enumType, enumParts[1], true);
                        return;
                    }
                }

                // Test if it is a numeric value - old expression ^[0-9]{1,}?(\.[0-9]{1,})$
                if (Regex.IsMatch(value, @"^[\d,]+(\.\d{1,5})?$", RegexOptions.Singleline)) 
                {
                    if (value.Contains('.'))
                    {
                        Value = Decimal.Parse(value);
                        return;
                    }
                    int j;
                    if (int.TryParse(value, out j))
                    {
                        Value = j;
                        return;
                    }
                    Value = long.Parse(value);
                    return;
                }

                // Default to just the string value itself
                Value = value;
            }//end: Set
        }// ValueAsString

        /// <summary>
        /// Returns the result of the ToString method for serialization
        /// </summary>
        public string DisplayText
        {
            get
            {
                return this.ToString();
            }
        }

        public override string ToString()
        {
            if (Value == null)
                return string.Empty;

            string niceValue = null;
            if (Options != null)
            {
                var opt = Options.FirstOrDefault(o => Value.Equals(o.Value));
                if (opt == null)
                    opt = Options.FirstOrDefault(o => Value.ToString() == o.Value.ToString());
                if (opt != null)
                    niceValue = opt.Text;
            }

            switch (ControlType)
            {
                case ControlType.Date:
                    niceValue = Convert.ToDateTime(Value).Date.ToString("MM/dd/yyyy");
                    break;
                case ControlType.Minutes:
                    int min = Convert.ToInt32(Value);
                    niceValue = min.ToFriendlyTime();
                    break;
                case ControlType.Numeric:
                    niceValue = Value.ToString();
                    break;
                case ControlType.CheckBox:
                    if (Value.Equals(true))
                        return string.Concat("is ", Prompt);
                    return string.Concat("is not ", Prompt);
                case ControlType.CheckBoxList:
                    if (Value.GetType().IsArray)
                        return string.Concat(Prompt, " ", Tweener ?? "is one of", " ['", string.Join("', '", Value), "']");
                    niceValue = Value.ToString();
                    break;
            }

            if (niceValue == null && Value is string && string.IsNullOrWhiteSpace(Value.ToString()))
                return string.Empty;

            return string.Format("{0} {1} '{2}'", Prompt, Tweener ?? "is", niceValue ?? Value);
        }// ToStrings

        public string OnChangeMethodName { get; set; }

        public List<EventType> AvailableForEventTypes { get; set; }
        /// <summary>
        /// Display style is added to hide the control if it is not required to take user input
        /// </summary>
        public string DisplayStyle { get; set; }
    }

    [Serializable]
    public class RuleExpressionOption
    {
        /// <summary>
        /// Gets or sets the name of the option (display text)
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value of the option (value text)
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the optional group text (for grouped checkbox lists or grouped select lists, etc.)
        /// </summary>
        public string GroupText { get; set; }
    }

    [Serializable]
    public class RuleExpressionGroup
    {
        public RuleExpressionGroup()
        {
            Expressions = new List<RuleExpression>();
        }

        /// <summary>
        /// Gets or sets the title of the group as displayed and categorized in the UI
        /// to the user when selecting expressions to use within the rule builder.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description of the expression group to assist with user cognition
        /// of WTF is going on, lol, 'cause hey, it's complex stuff.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The collection of expressions within the group.
        /// </summary>
        public List<RuleExpression> Expressions { get; set; }
    }

    public class RuleExpressionParameter
    {
        public RuleExpressionParameter()
        {
            Options = new List<RuleExpressionOption>();
        }

        /// <summary>
        /// Gets or sets the formal name of the LEFT field/expression, such as a property name, field name or method/function name
        /// extended on the loa object or other base expression eval object.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the textual label or prompt used to gather the expression, e.g. "First Name" or "Hours Worked Last 12 Months", etc.
        /// </summary>
        public string Prompt { get; set; }

        /// <summary>
        /// Gets or sets the help text to be shown on hover for the prompt of this rule to assist
        /// in proper usage. This isn't an easy concept, so this is important.
        /// </summary>
        public string HelpText { get; set; }

        /// <summary>
        /// Gets or sets the type of control for displaying the expression prompt in the UI.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Gets or sets the list of options, if any, for the expression prompt in the UI to choose from
        /// for select types, checkbox lists, etc.
        /// </summary>
        public List<RuleExpressionOption> Options { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the RIGHT value (either default value or populated by the user, etc.) for this expression item.
        /// This value may be a Date, a string, a number, an Enumeration value (integer), a boolean, etc. but forms
        /// the right-hand side of the expression.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets the current value as a string.
        /// </summary>
        /// <returns></returns>
        public string StringValue
        {
            get
            {
                // If the value is null, return the default of type T
                if (Value == null) return "null";

                if (Value is DateTime || ControlType == ControlType.Date)
                    return string.Format("#{0:MM/dd/yyyy}#", Value);

                if (Value is bool)
                    return Value.ToString().ToLowerInvariant();

                if (Value is Guid)
                    return string.Format("Guid.Parse('{0}')", Value);

                // Get our type of T so we can use it for other tests and ensure .NET reflection caches the result
                //  as an instance on our memory heap for performance reasons
                Type type = Value.GetType();
                Type realType = string.IsNullOrWhiteSpace(TypeName) ? type : Type.GetType(TypeName, false) ?? type;
                if (realType != type)
                {
                    Value = Convert.ChangeType(Value, type);
                    type = realType;
                }

                // If it is an enum, we need to put in the actual enum name.value format.
                if (type.IsEnum)
                    return string.Format("{0}.{1}", type.Name, Enum.GetName(type, Value));

                // If it's a string, easy enough, just return the string, escape any characters that need escaping
                if (Value is string || Value is char)
                    return string.Format("'{0}'", Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'"));

                // Can't figure anything else out, so just return the default of T again. Maybe this
                //  should attempt to throw an exception instead, will need to look into that.
                string val = Value.ToString();
                return string.IsNullOrWhiteSpace(val) ? "''" : val;
            }//end: Get
            set
            {
                // Test is null or a null literal representation
                if (value == null || value == "null")
                {
                    Value = null;
                    return;
                }

                // Test if it is a Boolean value of true
                if (value.ToLowerInvariant() == "true")
                {
                    Value = true;
                    return;
                }

                // Test if it is a Boolean value of false
                if (value.ToLowerInvariant() == "false")
                {
                    Value = false;
                    return;
                }

                // Test if it is a string/char literal
                if (Regex.IsMatch(value, @"^'.*'$", RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                {
                    if (value.Length == 2)
                        Value = "";
                    else
                        Value = value.Substring(1, value.Length - 2).Replace("\\\\", "\\").Replace("\\'", "'");
                    return;
                }

                // Test if it is a Date
                if (value.StartsWith("#"))
                {
                    Value = DateTime.Parse(value.Replace("#", "")).ToMidnight();
                    return;
                }

                // Test if it is a Guid
                if (value.StartsWith("Guid.Parse"))
                {
                    Value = DateTime.Parse(value.Replace("Guid.Parse('", "").Replace("')", ""));
                    return;
                }

                // Test if it is an Enum
                if (Regex.IsMatch(value, @"^[A-Za-z][A-Za-z0-9]{0,}\.[A-Za-z][A-Za-z0-9]{0,}$", RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                {
                    var enumParts = value.Split('.');
                    Type enumType = LeaveOfAbsenceEvaluator.GetEnumTypes().FirstOrDefault(t => t.Name == enumParts[0]);
                    if (enumType != null && enumType.IsEnum)
                    {
                        Value = Enum.Parse(enumType, enumParts[1], true);
                        return;
                    }
                }

                // Test if it is a numeric value
                if (Regex.IsMatch(value, @"^([0]|[1-9][0-9]{0,})(\.[0-9]{1,})?$", RegexOptions.Singleline))
                {
                    if (value.Contains('.'))
                    {
                        Value = Decimal.Parse(value);
                        return;
                    }
                    int j;
                    if (int.TryParse(value, out j)) 
                    {
                        Value = j;
                        return;
                    }
                    Value = long.Parse(value);
                    return;
                }

                // Default to just the string value itself
                Value = value;
            }//end: Set
        }// ValueAsString
    }
}
