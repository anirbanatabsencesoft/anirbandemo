﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Common;
using Ionic.Crc;
using Ionic.Zip;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Administration.Configuration
{
    /// <summary>
    /// Provides methods for importing and exporting customer and employer configuration data
    /// </summary>
    public class ImportExportService : LogicService
    {
        #region .ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExportService" /> class.
        /// </summary>
        public ImportExportService() : this(null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExportService" /> class.
        /// </summary>
        /// <param name="u">The u.</param>
        public ImportExportService(User u = null) : this((string)null, u) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExportService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="u">The u.</param>
        public ImportExportService(string customerId, User u) : base(customerId, null, u) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExportService" /> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="u">The u.</param>
        public ImportExportService(Customer customer, User u) : base(customer, null, u) { }

        #endregion .ctor

        #region Properties

        /// <summary>
        /// The settings file name
        /// </summary>
        private const string SettingsFileName = "Settings.json";

        /// <summary>
        /// Gets or sets the settings for import/export.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public ImportExportSettings Settings { get; set; }

        /// <summary>
        /// Gets or sets the output action.
        /// </summary>
        /// <value>
        /// The output action.
        /// </value>
        public Action<string> Output { get; set; }

        #endregion Properties

        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        private void WriteOutput(string message, params object[] args)
        {
            if (Output != null && !string.IsNullOrWhiteSpace(message))
                Output(string.Format(message, args));
        }

        #region Import

        /// <summary>
        /// Imports the specified file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <exception cref="System.ArgumentNullException">file</exception>
        /// <exception cref="System.ArgumentException">File did not contain any data;file</exception>
        public void Import(ImportExportFile file)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            if (string.IsNullOrWhiteSpace(file.FileLocator))
                throw new ArgumentException("File did not contain any locator", "file");

            WriteOutput("Downloading Import File from S3 at '{0}'", file.FileLocator);
            using (FileService fs = new FileService())
            using (Stream stream = fs.DownloadStream(file.FileLocator, AbsenceSoft.Common.Properties.Settings.Default.S3BucketName_Exports))
            using (ZipFile zip = ZipFile.Read(stream))
            {
                WriteOutput("Loading Import Settings from file");
                LoadSettings(zip);
                WriteOutput("Settings loaded");

                WriteOutput("Reading reference documents from file");
                List<Document> docs = ReadEntities<Document>(zip);
                List<Paperwork> paperwork = null;
                WriteOutput("Read {0} reference documents from file", docs.Count);

                if (Settings.IncludeCustomer)
                    ImportCustomer(zip, docs);
                if (Settings.IncludeEmployers)
                    ImportEmployers(zip, docs);
                if (Settings.IncludeAbsenceReasons)
                    ImportAbsenceReasons(zip);
                if (Settings.IncludeAccommodationQuestions)
                    ImportAccommodationQuestions(zip);
                if (Settings.IncludeAccommodationTypes)
                    ImportAccommodationTypes(zip);
                if (Settings.IncludeContactTypes)
                    ImportContactTypes(zip);
                if (Settings.IncludeCustomFields)
                    ImportCustomFields(zip);
                if (Settings.IncludePaperwork)
                    paperwork = ImportPaperwork(zip, docs);
                if (Settings.IncludePaySchedules)
                    ImportPaySchedules(zip);
                if (Settings.IncludePolicies)
                    ImportPolicies(zip);
                if (Settings.IncludeRoles)
                    ImportRoles(zip);
                if (Settings.IncludeTemplates)
                    ImportTemplates(zip, docs, paperwork);
                if (Settings.IncludeWorkflows)
                    ImportWorkflows(zip);
            }
        }

        /// <summary>
        /// Imports the customer.
        /// </summary>
        /// <param name="zip">The zip.</param>
        /// <param name="docs">The docs.</param>
        private void ImportCustomer(ZipFile zip, List<Document> docs)
        {
            WriteOutput("Importing Customer Record");
            Customer customer = Customer.GetById(CustomerId);
            Customer newCust = ReadEntities<Customer>(zip).FirstOrDefault();
            if (newCust == null)
                return;
            if (customer == null)
            {
                WriteOutput("Customer not found, creating a new one");
                customer = newCust.Clone();
                customer.LogoId = null;
                // new customer, we need to create a dummy admin user for it
                var user = new User()
                {
                    ContactInfo = customer.Contact.Clone(),
                    CustomerId = customer.Id,
                    Email = string.Concat(customer.Id, "@absencesoft.com"),
                    Employers = ReadEntities<Employer>(zip).Select(e => new EmployerAccess()
                    {
                        AutoAssignCases = false,
                        EmployerId = e.Id
                    }).ToList(),
                    FirstName = "AbsenceTracker",
                    LastName = customer.Name,
                    Password = CryptoString.GetHash("!Complex001"),
                    Roles = new List<string>(1) { Role.SystemAdministrator.Id },
                    UserFlags = UserFlag.None
                }.Save();
                WriteOutput("Created default Admin user for customer, '{0}' with password: '!Complex001'; User ID = '{1}'", user.Email, user.Id);
            }
            else
            {
                WriteOutput("Customer already exists, updating supression list and features");
                customer.SuppressPolicies = newCust.SuppressPolicies;
                customer.SuppressReasons = newCust.SuppressReasons;
                customer.SuppressWorkflows = newCust.SuppressWorkflows;
                customer.Features = newCust.Features;
            }
            if (docs != null && customer.LogoId == null && newCust.LogoId != null)
            {
                var logo = docs.FirstOrDefault(d => d.Id == newCust.LogoId);
                if (logo != null)
                {
                    WriteOutput("Adding Customer Logo, '{0}'", logo.FileName);
                    customer.LogoId = logo.Upload().Id;
                }
            }
            customer.Save();
            WriteOutput("Customer saved");
        }

        /// <summary>
        /// Imports the employers.
        /// </summary>
        /// <param name="zip">The zip.</param>
        /// <param name="docs">The docs.</param>
        private void ImportEmployers(ZipFile zip, List<Document> docs)
        {
            WriteOutput("Importing Employers");
            List<Employer> newEmployers = ReadEntities<Employer>(zip, Settings.EmployerIds);
            foreach (var newEmployer in newEmployers)
            {
                Employer employer = Employer.GetById(newEmployer.Id);
                WriteOutput("Importing Employer '{0}'", newEmployer.Name);
                if (employer == null)
                {
                    WriteOutput("Employer '{0}' does not already exist, creating a new one", newEmployer.Name);
                    employer = newEmployer.Clone();
                    employer.LogoId = null;
                }
                else
                {
                    WriteOutput("Employer '{0}' already exists, updating the configuration and Employer settings", newEmployer.Name);
                    employer.AllowIntermittentForBirthAdoptionOrFosterCare = newEmployer.AllowIntermittentForBirthAdoptionOrFosterCare;
                    employer.Contact = newEmployer.Contact.Clone();
                    employer.DefaultEmailFromAddress = newEmployer.DefaultEmailFromAddress;
                    employer.DefaultPayScheduleId = newEmployer.DefaultPayScheduleId;
                    employer.Enable50In75MileRule = newEmployer.Enable50In75MileRule;
                    employer.EnableSpouseAtSameEmployerRule = newEmployer.EnableSpouseAtSameEmployerRule;
                    employer.FMLPeriodType = newEmployer.FMLPeriodType;
                    employer.FTWeeklyWorkHours = newEmployer.FTWeeklyWorkHours;
                    employer.Holidays = newEmployer.Holidays;
                    employer.Features = newEmployer.Features;
                    employer.IsPubliclyTradedCompany = newEmployer.IsPubliclyTradedCompany;
                    employer.MaxEmployees = newEmployer.MaxEmployees;
                    employer.Name = newEmployer.Name;
                    employer.ReferenceCode = newEmployer.ReferenceCode;
                    employer.ResetDayOfMonth = newEmployer.ResetDayOfMonth;
                    employer.ResetMonth = newEmployer.ResetMonth;
                    employer.SuppressPolicies = newEmployer.SuppressPolicies;
                    employer.SuppressReasons = newEmployer.SuppressReasons;
                    employer.SuppressWorkflows = newEmployer.SuppressWorkflows;
                }
                if (docs != null && employer.LogoId == null && newEmployer.LogoId != null)
                {
                    var logo = docs.FirstOrDefault(d => d.Id == newEmployer.LogoId);
                    if (logo != null)
                    {
                        WriteOutput("Adding Employer Logo, '{0}'", logo.FileName);
                        employer.LogoId = logo.Upload().Id;
                    }
                }
                employer.Save();
            }
            WriteOutput("Done Importing Employers");
        }

        /// <summary>
        /// Imports the absence reasons.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportAbsenceReasons(ZipFile zip)
        {
            WriteOutput("Importing Absence Reasons");
            List<AbsenceReason> newReasons = ReadEntities<AbsenceReason>(zip, Settings.AbsenceReasons);
            foreach (var newReason in newReasons)
            {
                if (!newReason.IsCustom)
                    continue;

                AbsenceReason reason = AbsenceReason.GetByCode(newReason.Code, newReason.CustomerId, newReason.EmployerId);
                WriteOutput("Importing Absence Reason '{0}'", newReason.Code);
                if (reason == null || !reason.IsCustom)
                {
                    WriteOutput("Absence Reason '{0}' does not already exist, creating a new one", newReason.Code);
                    reason = newReason.Clone();
                }
                else
                {
                    WriteOutput("Absence Reason '{0}' already exists, updating the configuration", newReason.Code);
                    if (reason.EmployerId != newReason.EmployerId)
                        reason.Clean();
                    reason.Name = newReason.Name;
                    reason.CaseTypes = newReason.CaseTypes;
                    reason.Category = newReason.Category;
                    reason.Flags = newReason.Flags;
                    reason.HelpText = newReason.HelpText;
                }
                reason.CustomerId = newReason.CustomerId;
                reason.EmployerId = newReason.EmployerId;
                reason.Save();
            }
            WriteOutput("Done Importing Absence Reasons");
        }

        /// <summary>
        /// Imports the accommodation questions.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportAccommodationQuestions(ZipFile zip)
        {
            WriteOutput("Importing Accommodation Questions");
            List<AccommodationQuestion> newAccommodationQuestions = ReadEntities<AccommodationQuestion>(zip, Settings.AccommodationQuestions);
            foreach (var newQuestion in newAccommodationQuestions)
            {
                if (string.IsNullOrWhiteSpace(newQuestion.CustomerId))
                    continue;

                WriteOutput("Importing Accommodation Question '{0}'", newQuestion.Question);
                var question = AccommodationQuestion.GetById(newQuestion.Id) ??
                    AccommodationQuestion.AsQueryable().Where(q => 
                        q.CustomerId == newQuestion.CustomerId 
                        && q.EmployerId == newQuestion.EmployerId 
                        && q.Question == newQuestion.Question).FirstOrDefault();
                if (question == null || string.IsNullOrWhiteSpace(question.CustomerId))
                {
                    WriteOutput("Accommodation Question '{0}' does not exist, creating a new one", newQuestion.Question);
                    question = newQuestion.Clone();
                }
                else
                {
                    WriteOutput("Accommodation Question '{0}' already exist, updating settings", newQuestion.Question);
                    if (question.EmployerId != newQuestion.EmployerId)
                        question.Clean();
                    question.IsInteractiveProcessQuestion = newQuestion.IsInteractiveProcessQuestion;
                    question.Choices = newQuestion.Choices;
                    question.AccommodationTypeCode = newQuestion.AccommodationTypeCode;
                    question.Group = newQuestion.Group;
                    question.Order = newQuestion.Order;
                    question.PromptDate = newQuestion.PromptDate;
                    question.PromptDateDecisionOverturned = newQuestion.PromptDateDecisionOverturned;
                    question.PromptDateOfInitialDecision = newQuestion.PromptDateOfInitialDecision;
                    question.PromptProvider = newQuestion.PromptProvider;
                    question.PromptTime = newQuestion.PromptTime;
                }
                question.CustomerId = newQuestion.CustomerId;
                question.EmployerId = newQuestion.EmployerId;
                question.Save();
            }
            WriteOutput("Done Importing Accommodation Questions");
        }

        /// <summary>
        /// Imports the accommodation types.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportAccommodationTypes(ZipFile zip)
        {
            WriteOutput("Importing Accommodation Types");
            List<AccommodationType> newAccommodationTypes = ReadEntities<AccommodationType>(zip, Settings.AccommodationTypes);
            foreach (var newType in newAccommodationTypes)
            {
                if (!newType.IsCustom)
                    continue;

                WriteOutput("Importing Accommodation Type '{0}'", newType.Code);
                var type = AccommodationType.GetByCode(newType.Code, newType.CustomerId, newType.EmployerId);
                if (type == null || !type.IsCustom)
                {
                    WriteOutput("Accommodation Type '{0}' does not exist, creating a new one", newType.Code);
                    type = newType.Clone();
                }
                else
                {
                    WriteOutput("Accommodation Type '{0}' already exist, updating settings", newType.Code);
                    if (type.EmployerId != newType.EmployerId)
                        type.Clean();
                    type.CaseTypeFlag = newType.CaseTypeFlag;
                    type.Name = newType.Name;
                    type.Order = newType.Order;
                    type.PreventNew = newType.PreventNew;
                }
                type.CustomerId = newType.CustomerId;
                type.EmployerId = newType.EmployerId;
                type.Save();
            }
            WriteOutput("Done Importing Accommodation Types");
        }

        /// <summary>
        /// Imports the contact types.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportContactTypes(ZipFile zip)
        {
            WriteOutput("Importing Contact Types");
            List<ContactType> newContactTypes = ReadEntities<ContactType>(zip, Settings.ContactTypes);
            foreach (var newType in newContactTypes)
            {
                if (string.IsNullOrWhiteSpace(newType.CustomerId))
                    continue;

                WriteOutput("Importing Contact Type '{0}'", newType.Code);
                var type = ContactType.GetByCode(newType.Code, newType.CustomerId, newType.EmployerId);
                if (type == null || string.IsNullOrWhiteSpace(newType.CustomerId))
                {
                    WriteOutput("Contact Type '{0}' does not exist, creating a new one", newType.Code);
                    type = newType.Clone();
                }
                else
                {
                    WriteOutput("Contact Type '{0}' already exist, updating settings", newType.Code);
                    if (type.EmployerId != newType.EmployerId)
                        type.Clean();
                    type.Name = newType.Name;
                    type.ContactCategory = newType.ContactCategory;
                    type.DisplayOnCommunicationsAs = newType.DisplayOnCommunicationsAs;
                    type.WorkStateRestrictions = newType.WorkStateRestrictions;
                }
                type.CustomerId = newType.CustomerId;
                type.EmployerId = newType.EmployerId;
                type.Save();
            }
            WriteOutput("Done Importing Contact Types");
        }

        /// <summary>
        /// Imports the custom fields.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportCustomFields(ZipFile zip)
        {
            WriteOutput("Importing Custom Fields");
            List<CustomField> newCustomFields = ReadEntities<CustomField>(zip, Settings.CustomFields);
            foreach (var newField in newCustomFields)
            {
                WriteOutput("Importing Custom Field '{0}'", newField.Name);
                var field = CustomField.GetById(newField.Id) ?? CustomField.AsQueryable().Where(f => 
                    f.CustomerId == newField.CustomerId 
                    && f.EmployerId == newField.EmployerId 
                    && f.Name == newField.Name).FirstOrDefault();
                if (field == null)
                {
                    WriteOutput("Custom Field '{0}' does not exist, creating a new one", newField.Name);
                    field = newField.Clone();
                }
                else
                {
                    WriteOutput("Custom Field '{0}' already exist, updating settings", newField.Name);
                    if (field.EmployerId != newField.EmployerId)
                        field.Clean();
                    field.DataType = newField.DataType;
                    field.Description = newField.Description;
                    field.FileOrder = newField.FileOrder;
                    field.HelpText = newField.HelpText;
                    field.IsCollectedAtIntake = newField.IsCollectedAtIntake;
                    field.IsRequired = newField.IsRequired;
                    field.Label = newField.Label;
                    field.ListValues = newField.ListValues;
                    field.Target = newField.Target;
                    field.ValueType = newField.ValueType;
                }
                field.CustomerId = newField.CustomerId;
                field.EmployerId = newField.EmployerId;
                field.Save();
            }
            WriteOutput("Done Importing Custom Fields");
        }

        /// <summary>
        /// Imports the pay schedules.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportPaySchedules(ZipFile zip)
        {
            WriteOutput("Importing Pay Schedules");
            List<PaySchedule> newPaySchedules = ReadEntities<PaySchedule>(zip, Settings.PaySchedules);
            foreach (var newSchedule in newPaySchedules)
            {
                WriteOutput("Importing Pay Schedule '{0}'", newSchedule.Name);
                var schedule = PaySchedule.GetById(newSchedule.Id);
                if (schedule == null)
                {
                    WriteOutput("Pay Schedule '{0}' does not exist, creating a new one", newSchedule.Name);
                    schedule = newSchedule.Clone();
                }
                else
                {
                    WriteOutput("Pay Schedule '{0}' already exist, updating settings", newSchedule.Name);
                    schedule.AssignmentRules = newSchedule.AssignmentRules;
                    schedule.DayEnd = newSchedule.DayEnd;
                    schedule.DayStart = newSchedule.DayStart;
                    schedule.Default = newSchedule.Default;
                    schedule.Name = newSchedule.Name;
                    schedule.PayPeriodType = newSchedule.PayPeriodType;
                    schedule.ProcessingDays = newSchedule.ProcessingDays;
                    schedule.StartDate = newSchedule.StartDate;
                }
                schedule.CustomerId = newSchedule.CustomerId;
                schedule.EmployerId = newSchedule.EmployerId;
                schedule.Save();
            }
            WriteOutput("Done Importing Pay Schedules");
        }

        /// <summary>
        /// Imports the policies.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportPolicies(ZipFile zip)
        {
            WriteOutput("Importing Policies");
            List<Policy> newPolicies = ReadEntities<Policy>(zip, Settings.Policies);
            foreach (var newPolicy in newPolicies)
            {
                if (!newPolicy.IsCustom)
                    continue;

                WriteOutput("Importing Policy '{0}'", newPolicy.Code);
                var policy = GetPolicyByCode(newPolicy.Code, newPolicy.CustomerId, newPolicy.EmployerId);
                if (policy == null || !policy.IsCustom)
                {
                    WriteOutput("Policy '{0}' does not exist, creating a new one", newPolicy.Code);
                    policy = newPolicy.Clone();
                }
                else
                {
                    WriteOutput("Policy '{0}' already exist, updating settings", newPolicy.Code);
                    if (policy.EmployerId != newPolicy.EmployerId)
                        policy.Clean();
                    policy.AbsenceReasons = newPolicy.AbsenceReasons;
                    policy.ConsecutiveTo = newPolicy.ConsecutiveTo;
                    policy.Description = newPolicy.Description;
                    policy.EffectiveDate = newPolicy.EffectiveDate;
                    policy.Gender = newPolicy.Gender;
                    policy.IsDisabled = newPolicy.IsDisabled;
                    policy.Name = newPolicy.Name;
                    policy.PolicyType = newPolicy.PolicyType;
                    policy.ResidenceState = newPolicy.ResidenceState;
                    policy.RuleGroups = newPolicy.RuleGroups;
                    policy.WorkState = newPolicy.WorkState;
                }
                policy.CustomerId = newPolicy.CustomerId;
                policy.EmployerId = newPolicy.EmployerId;
                policy.Save();
            }
            WriteOutput("Done Importing Policies");
        }

        /// <summary>
        /// Gets the policy by its code rather than Id
        /// </summary>
        /// <param name="code">The code for the policy</param>
        /// <returns>The policy if found, otherwise <c>null</c>.</returns>
        private Policy GetPolicyByCode(string code, string customerId = null, string employerId = null)
        {
            string c = code.ToUpperInvariant();
            return Policy.Query.Find(
                Policy.Query.And(
                    Policy.Query.EQ(r => r.Code, c),
                    Policy.Query.Or(
                        Policy.Query.EQ(r => r.CustomerId, customerId),
                        Policy.Query.EQ(r => r.CustomerId, null),
                        Policy.Query.NotExists(r => r.CustomerId)
                    ),
                    Policy.Query.Or(
                        Policy.Query.EQ(r => r.EmployerId, employerId),
                        Policy.Query.EQ(r => r.EmployerId, null),
                        Policy.Query.NotExists(r => r.EmployerId)
                    )
                )).ToList().OrderBy(r =>
                {
                    if (!string.IsNullOrWhiteSpace(r.EmployerId))
                        return 1;
                    if (!string.IsNullOrWhiteSpace(r.CustomerId))
                        return 2;
                    return 3;
                }).FirstOrDefault();
        }

        /// <summary>
        /// Imports the roles.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportRoles(ZipFile zip)
        {
            WriteOutput("Importing Roles");
            List<Role> newRoles = ReadEntities<Role>(zip, Settings.Roles);
            foreach (var newRole in newRoles)
            {
                WriteOutput("Importing Role '{0}'", newRole.Name);
                var role = Role.GetById(newRole.Id) ?? Role.AsQueryable().Where(r =>
                    r.CustomerId == newRole.CustomerId
                    && r.Name == newRole.Name).FirstOrDefault();
                if (role == null)
                {
                    WriteOutput("Role '{0}' does not exist, creating a new one", newRole.Name);
                    role = newRole.Clone();
                }
                else
                {
                    WriteOutput("Role '{0}' already exist, updating permissions and contact types", newRole.Name);
                    role.Permissions = newRole.Permissions;
                    role.ContactTypes = newRole.ContactTypes;
                    role.RevokeAccessIfNoContactsOfTypes = newRole.RevokeAccessIfNoContactsOfTypes;
                    role.Type = newRole.Type;
                }
                role.CustomerId = newRole.CustomerId;
                role.Save();
            }
            WriteOutput("Done Importing Roles");
        }

        /// <summary>
        /// Imports the workflows.
        /// </summary>
        /// <param name="zip">The zip.</param>
        private void ImportWorkflows(ZipFile zip)
        {
            WriteOutput("Importing Workflows");
            List<Workflow> newWorkflows = ReadEntities<Workflow>(zip, Settings.Workflows);
            foreach (var newWorkflow in newWorkflows)
            {
                if (!newWorkflow.IsCustom)
                    continue;

                WriteOutput("Importing Policy '{0}'", newWorkflow.Code);
                var workflow = Workflow.GetByCode(newWorkflow.Code, newWorkflow.CustomerId, newWorkflow.EmployerId);
                if (workflow == null || !workflow.IsCustom)
                {
                    WriteOutput("Policy '{0}' does not exist, creating a new one", newWorkflow.Code);
                    workflow = newWorkflow.Clone();
                }
                else
                {
                    WriteOutput("Policy '{0}' already exist, updating settings", newWorkflow.Code);
                    if (workflow.EmployerId != newWorkflow.EmployerId)
                        workflow.Clean();

                    workflow.Activities = newWorkflow.Activities;
                    workflow.Criteria = newWorkflow.Criteria;
                    workflow.Description = newWorkflow.Description;
                    workflow.DesignerData = newWorkflow.DesignerData;
                    workflow.EffectiveDate = newWorkflow.EffectiveDate;
                    workflow.EventTypeString = newWorkflow.EventTypeString;
                    workflow.IsDisabled = newWorkflow.IsDisabled;
                    workflow.Name = newWorkflow.Name;
                    workflow.TargetEventType = newWorkflow.TargetEventType;
                    workflow.Transitions = newWorkflow.Transitions;
                }
                workflow.CustomerId = newWorkflow.CustomerId;
                workflow.EmployerId = newWorkflow.EmployerId;
                workflow.Save();
            }
            WriteOutput("Done Importing Workflows");
        }

        /// <summary>
        /// Imports the paperwork.
        /// </summary>
        /// <param name="zip">The zip.</param>
        /// <param name="docs">The docs.</param>
        /// <returns></returns>
        private List<Paperwork> ImportPaperwork(ZipFile zip, List<Document> docs)
        {
            WriteOutput("Importing Paperwork");
            List<Paperwork> newPaperworks = ReadEntities<Paperwork>(zip);
            foreach (var newPaperwork in newPaperworks)
            {
                if (string.IsNullOrWhiteSpace(newPaperwork.CustomerId))
                    continue;

                WriteOutput("Importing Paperwork '{0}'", newPaperwork.Name);
                var paperwork = Paperwork.GetByCode(newPaperwork.Code, newPaperwork.CustomerId, newPaperwork.EmployerId) ?? Paperwork.AsQueryable().FirstOrDefault(p =>
                    p.CustomerId == newPaperwork.CustomerId
                    && p.EmployerId == newPaperwork.EmployerId
                    && p.Name == newPaperwork.Name);
                if (paperwork == null)
                {
                    WriteOutput("Paperwork '{0}' does not exist, creating a new one", newPaperwork.Name);
                    paperwork = newPaperwork.Clone();
                    paperwork.FileId = null;
                    paperwork.FileName = null;
                }
                else
                {
                    if (!Settings.Paperwork.Contains(newPaperwork.Code))
                    {
                        newPaperwork.Metadata.SetRawValue("TargetPaperworkCode", paperwork.Code);
                        continue;
                    }
                    WriteOutput("Paperwork '{0}' already exist, updating permissions and contact types", newPaperwork.Name);
                    paperwork.Criteria = newPaperwork.Criteria;
                    paperwork.Description = newPaperwork.Description;
                    paperwork.DocType = newPaperwork.DocType;
                    paperwork.EnclosureText = newPaperwork.EnclosureText;
                    paperwork.Fields = newPaperwork.Fields;
                    paperwork.RequiresReview = newPaperwork.RequiresReview;
                    paperwork.IsForApprovalDecision = newPaperwork.IsForApprovalDecision;
                    paperwork.ReturnDateAdjustment = newPaperwork.ReturnDateAdjustment;
                    paperwork.ReturnDateAdjustmentDays = newPaperwork.ReturnDateAdjustmentDays;
                }
                paperwork.CustomerId = newPaperwork.CustomerId;
                paperwork.EmployerId = newPaperwork.EmployerId;

                if (docs != null && newPaperwork.FileId != null && newPaperwork.FileId != paperwork.FileId)
                {
                    var file = docs.FirstOrDefault(d => d.Id == newPaperwork.FileId);
                    if (file != null)
                    {
                        WriteOutput("Uploading Paperwork '{0}' document '{1}'", newPaperwork.Name, file.FileName);
                        file = file.Upload();
                        paperwork.FileId = file.Id;
                        paperwork.FileName = file.FileName;
                        WriteOutput("Uploaded Paperwork '{0}' document '{1}'", newPaperwork.Name, file.FileName);
                    }
                }

                newPaperwork.Metadata.SetRawValue("TargetPaperworkCode", paperwork.Save().Code);
            }
            WriteOutput("Done Importing Paperwork");
            return newPaperworks;
        }

        /// <summary>
        /// Imports the templates.
        /// </summary>
        /// <param name="zip">The zip.</param>
        /// <param name="docs">The docs.</param>
        /// <param name="paperwork">The paperwork.</param>
        private void ImportTemplates(ZipFile zip, List<Document> docs, List<Paperwork> paperwork)
        {
            WriteOutput("Importing Templates");
            List<Template> newTemplates = ReadEntities<Template>(zip, Settings.Templates);
            foreach (var newTemplate in newTemplates)
            {
                if (!newTemplate.IsCustom)
                    continue;

                WriteOutput("Importing Template '{0}'", newTemplate.Code);
                var template = Template.GetByCode(newTemplate.Code, newTemplate.CustomerId, newTemplate.EmployerId);
                if (template == null || !template.IsCustom)
                {
                    WriteOutput("Template '{0}' does not exist, creating a new one", newTemplate.Code);
                    template = newTemplate.Clone();
                    template.DocumentId = null;
                    template.DocumentName = null;
                }
                else
                {
                    WriteOutput("Template '{0}' already exist, updating settings", newTemplate.Code);
                    template.Name = newTemplate.Name;
                    template.AttachLatestIncompletePaperwork = newTemplate.AttachLatestIncompletePaperwork;
                    template.Body = newTemplate.Body;
                    template.IsLOASpecific = newTemplate.IsLOASpecific;
                    template.Subject = newTemplate.Subject;
                    template.DocType = newTemplate.DocType;
                    template.PaperworkCodes = paperwork == null ? template.PaperworkCodes : paperwork
                        .Where(p => p != null)
                        .Where(p => newTemplate.PaperworkCodes != null && newTemplate.PaperworkCodes.Contains(p.Code))
                        .Select(p => p.Metadata.GetRawValue<string>("TargetPaperworkCode"))
                        .Union(newTemplate.PaperworkCodes ?? new List<string>(0))
                        .Distinct()
                        .ToList();
                }
                template.CustomerId = newTemplate.CustomerId;
                template.EmployerId = newTemplate.EmployerId;

                if (docs != null && newTemplate.DocumentId != null && newTemplate.DocumentId != template.DocumentId)
                {
                    var file = docs.FirstOrDefault(d => d.Id == newTemplate.DocumentId);
                    if (file != null)
                    {
                        WriteOutput("Uploading Template '{0}' document '{1}'", newTemplate.Code, file.FileName);
                        file = file.Upload();
                        template.DocumentId = file.Id;
                        template.DocumentName = file.FileName;
                        WriteOutput("Uploaded Template '{0}' document '{1}'", newTemplate.Code, file.FileName);
                    }
                }

                template.Save();
            }
            WriteOutput("Done Importing Templates");
        }

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <param name="zip">The zip file.</param>
        private void LoadSettings(ZipFile zip)
        {
            ZipEntry entry = zip.Where(f => f.FileName == SettingsFileName).FirstOrDefault();
            if (entry != null)
            {
                string json = null;
                using (CrcCalculatorStream entryReader = entry.OpenReader())
                using (StreamReader reader = new StreamReader(entryReader))
                    json = reader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(json))
                    Settings = JsonConvert.DeserializeObject<ImportExportSettings>(json);
            }
            if (Settings == null)
                Settings = ImportExportSettings.Default(CustomerId);
        }
        
        /// <summary>
        /// Fetches the entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        private List<TEntity> ReadEntities<TEntity>(ZipFile zip, List<string> ids = null) where TEntity : BaseEntity<TEntity>, new()
        {
            List<TEntity> items = new List<TEntity>();
            var ext = string.Concat(".", BaseEntity<TEntity>.ClassTypeName.ToLowerInvariant());
            foreach (ZipEntry entry in zip.Where(e => !e.IsDirectory && Path.GetExtension(e.FileName) == ext))
            {
                var entity = ReadEntityFromZipEntry<TEntity>(entry);
                if (entity != null)
                {
                    if (ids != null && ids.Any())
                    {
                        if (!ids.Contains(entity.Id))
                            continue;
                    }
                    items.Add(entity);
                }
            }
            return items;
        }

        /// <summary>
        /// Reads the entity from a zip file
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        private TEntity ReadEntityFromZipEntry<TEntity>(ZipEntry entry) where TEntity : BaseEntity<TEntity>, new()
        {
            if (entry == null)
                return default(TEntity);

            string json = null;
            using (CrcCalculatorStream entryReader = entry.OpenReader())
            using (StreamReader reader = new StreamReader(entryReader))
                json = reader.ReadToEnd();

            if (string.IsNullOrWhiteSpace(json))
                return default(TEntity);

            return JsonConvert.DeserializeObject<TEntity>(json);
        }

        #endregion Import

        #region Export

        /// <summary>
        /// Exports configuration data using the specified settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public ImportExportFile Export(ImportExportSettings settings = null)
        {
            Settings = settings ?? Settings ?? ImportExportSettings.Default(CustomerId);

            ImportExportFile returnVal = new ImportExportFile()
            {
                CustomerId = CustomerId,
                FileName = string.Format("{0}_{1:yyyy-MM-dd_HHmmss}.zip", CurrentCustomer.Name, DateTime.UtcNow)
            };
            using (ZipFile zip = new ZipFile(returnVal.FileName))
            {
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.Comment = string.Format("This compressed, password protected archive is for '{0}' only and is for use with the AbsenceTracker system.", CurrentCustomer.Name);
                zip.AddEntry(SettingsFileName, Settings.ToJSON2(), Encoding.UTF8);
                zip.AddDirectoryByName(Document.ClassTypeName);
                WriteEntityToZip(CurrentCustomer, zip);
                if (Settings.IncludeCustomer && !string.IsNullOrWhiteSpace(CurrentCustomer.LogoId))
                {
                    var logo = Document.GetById(CurrentCustomer.LogoId);
                    if (logo != null)
                    {
                        logo.Download();
                        WriteEntityToZip(logo, zip);
                    }
                }
                if (Settings.IncludeEmployers && Settings.EmployerIds.Any())
                {
                    zip.AddDirectoryByName(Employer.ClassTypeName);
                    var employers = Employer.Query.Find(Employer.Query.And(Employee.Query.EQ(e => e.CustomerId, CustomerId), Employee.Query.In(e => e.Id, Settings.EmployerIds))).ToList();
                    foreach (var e in employers)
                    {
                        WriteEntityToZip(e, zip);
                        if (!string.IsNullOrWhiteSpace(e.LogoId))
                        {
                            var logo = Document.GetById(e.LogoId);
                            if (logo != null)
                            {
                                logo.Download();
                                WriteEntityToZip(logo, zip);
                            }
                        }
                    }
                }
                if (Settings.IncludeAbsenceReasons)
                    WriteEntitiesToZip<AbsenceReason>(zip, Settings.AbsenceReasons);
                if (Settings.IncludeAccommodationQuestions)
                    WriteEntitiesToZip<AccommodationQuestion>(zip, Settings.AccommodationQuestions);
                if (Settings.IncludeAccommodationTypes)
                    WriteEntitiesToZip<AccommodationType>(zip, Settings.AccommodationTypes);
                if (Settings.IncludeContactTypes)
                    WriteEntitiesToZip<ContactType>(zip, Settings.ContactTypes);
                if (Settings.IncludeCustomFields)
                    WriteEntitiesToZip<CustomField>(zip, Settings.CustomFields);
                if (Settings.IncludePaperwork)
                    WriteEntitiesToZip<Paperwork>(zip, Settings.Paperwork, p => p.Document);
                if (Settings.IncludePaySchedules)
                    WriteEntitiesToZip<PaySchedule>(zip, Settings.PaySchedules);
                if (Settings.IncludePolicies)
                    WriteEntitiesToZip<Policy>(zip, Settings.Policies);
                if (Settings.IncludeRoles)
                    WriteEntitiesToZip<Role>(zip, Settings.Roles);
                if (Settings.IncludeTemplates)
                    WriteEntitiesToZip<Template>(zip, Settings.Templates, t => t.Document);
                if (Settings.IncludeWorkflows)
                    WriteEntitiesToZip<Workflow>(zip, Settings.Workflows);

                using (MemoryStream stream = new MemoryStream())
                {
                    zip.Save(stream);
                    stream.Position = 0;
                    using (FileService fs = new FileService())
                        returnVal.FileLocator = fs.UploadFile(stream, returnVal.FileName, AbsenceSoft.Common.Properties.Settings.Default.S3BucketName_Exports, CustomerId, null, null, null);
                }
            }
            returnVal.File = Encoding.UTF8.GetBytes(returnVal.ToJSON2());
            returnVal.FileName = string.Concat(returnVal.FileName, ".json");
            return returnVal;
        }

        /// <summary>
        /// Writes the entities to zip.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="zip">The file.</param>
        /// <param name="docFetcher">The document fetcher.</param>
        private void WriteEntitiesToZip<TEntity>(ZipFile zip, List<string> ids = null, Func<TEntity, Document> docFetcher = null) where TEntity : BaseEntity<TEntity>, new()
        {
            var list = FetchEntities<TEntity>(ids);
            if (!list.Any())
                return;

            zip.AddDirectoryByName(BaseEntity<TEntity>.ClassTypeName);

            foreach (var entity in list)
            {
                WriteEntityToZip(entity, zip);
                if (docFetcher != null)
                {
                    var doc = docFetcher(entity);
                    if (doc != null)
                    {
                        doc.Download();
                        WriteEntityToZip(doc, zip);
                    }
                }
            }
        }

        /// <summary>
        /// Fetches the entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        private List<TEntity> FetchEntities<TEntity>(List<string> ids = null) where TEntity : BaseEntity<TEntity>, new()
        {
            // If they didn't select anything to include, don't include it
            if (ids == null || !ids.Any())
                return new List<TEntity>();

            List<BsonValue> employers = Settings.EmployerIds.Select(e => (BsonValue)new ObjectId(e)).ToList();
            employers.Insert(0, BsonNull.Value);
            List<IMongoQuery> ands = new List<IMongoQuery>()
            {
                // Always ensure it's our customer's data, no mater what
                Query.EQ("CustomerId", new ObjectId(CustomerId)),
                // Ensure it meets the requirements of one of the passed in EmployerIds, OR, it's a core customer only item
                Query.Or(
                    Query.In("EmployerId", employers),
                    Query.NotExists("EmployerId")
                ),
                // Ensure not employee specific info, that would be bad
                Query.NotExists("EmployeeId"),
                Query.NotExists("CaseId")
            };
            
            ands.Add(Query<TEntity>.In(q => q.Id, ids));
            var query = Query.And(ands);
            return BaseEntity<TEntity>.Query.Find(query).ToList();
        }

        /// <summary>
        /// Writes the entity to zip.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="zip">The file.</param>
        private void WriteEntityToZip<TEntity>(TEntity entity, ZipFile zip) where TEntity : BaseEntity<TEntity>, new()
        {
            if (entity == null || entity.IsNew || zip == null)
                return;

            string json = entity.ToJSON2();
            string fileName = string.Concat(entity.Id, ".", BaseEntity<TEntity>.ClassTypeName.ToLowerInvariant());
            string path = Path.Combine(BaseEntity<TEntity>.ClassTypeName, fileName);
            
            int i = 1;
            while (zip.ContainsEntry(path))
            {
                path = Path.Combine(BaseEntity<TEntity>.ClassTypeName, i.ToString(), ".", fileName);
                i++;
            }
            zip.AddEntry(path, json);
        }

        #endregion Export

        #region IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            Settings = null;
            base.Dispose();
        }

        #endregion IDisposable
    }
}
