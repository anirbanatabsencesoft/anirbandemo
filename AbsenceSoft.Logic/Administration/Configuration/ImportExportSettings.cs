﻿using AbsenceSoft.Data.Customers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration.Configuration
{
    [Serializable]
    public class ImportExportSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportExportSettings"/> class.
        /// </summary>
        public ImportExportSettings()
        {
            this.EmployerIds = new List<string>();
            this.AbsenceReasons = new List<string>();
            this.AccommodationQuestions = new List<string>();
            this.AccommodationTypes = new List<string>();
            this.ContactTypes = new List<string>();
            this.CustomFields = new List<string>();
            this.Paperwork = new List<string>();
            this.PaySchedules = new List<string>();
            this.Policies = new List<string>();
            this.Roles = new List<string>();
            this.Templates = new List<string>();
            this.Workflows = new List<string>();
        }
        
        /// <summary>
        /// Gets the default settings.
        /// </summary>
        /// <value>
        /// The default.
        /// </value>
        public static ImportExportSettings Default(string customerId)
        {
            var settings = new ImportExportSettings()
            {
                IncludeCustomer = true,
                IncludeEmployers = true,
                IncludeAbsenceReasons = true,
                IncludeAccommodationQuestions = true,
                IncludeAccommodationTypes = true,
                IncludeContactTypes = true,
                IncludeCustomFields = true,
                IncludePaperwork = true,
                IncludePaySchedules = true,
                IncludePolicies = true,
                IncludeRoles = true,
                IncludeTemplates = true,
                IncludeWorkflows = true,
            };

            settings.EmployerIds = Employer.Query.Find(Employer.Query.EQ(e => e.CustomerId, customerId)).ToList().Select(e => e.Id).ToList();

            return settings;
        }

        /// <summary>
        /// Gets or sets the employer ids.
        /// </summary>
        /// <value>
        /// The employer ids.
        /// </value>
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> EmployerIds { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include customer].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include customer]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeCustomer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include employers].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include employers]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeEmployers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include absence reasons].
        /// </summary>
        /// <value>
        /// <c>true</c> if [include absence reasons]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeAbsenceReasons { get; set; }

        /// <summary>
        /// Gets or sets the absence reasons to include explicitly.
        /// </summary>
        /// <value>
        /// The absence reasons.
        /// </value>
        public List<string> AbsenceReasons { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include accommodation questions].
        /// </summary>
        /// <value>
        /// <c>true</c> if [include accommodation questions]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeAccommodationQuestions { get; set; }

        /// <summary>
        /// Gets or sets the accommodation questions.
        /// </summary>
        /// <value>
        /// The accommodation questions.
        /// </value>
        public List<string> AccommodationQuestions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include accommodation types].
        /// </summary>
        /// <value>
        /// <c>true</c> if [include accommodation types]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeAccommodationTypes { get; set; }

        /// <summary>
        /// Gets or sets the accommodation types.
        /// </summary>
        /// <value>
        /// The accommodation types.
        /// </value>
        public List<string> AccommodationTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include contact types].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include contact types]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeContactTypes { get; set; }

        /// <summary>
        /// Gets or sets the contact types.
        /// </summary>
        /// <value>
        /// The contact types.
        /// </value>
        public List<string> ContactTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include custom fields].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include custom fields]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeCustomFields { get; set; }

        /// <summary>
        /// Gets or sets the custom fields.
        /// </summary>
        /// <value>
        /// The custom fields.
        /// </value>
        public List<string> CustomFields { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include paperworks].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include paperworks]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludePaperwork { get; set; }

        /// <summary>
        /// Gets or sets the paperwork.
        /// </summary>
        /// <value>
        /// The paperwork.
        /// </value>
        public List<string> Paperwork { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include pay schedules].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include pay schedules]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludePaySchedules { get; set; }

        /// <summary>
        /// Gets or sets the pay schedules.
        /// </summary>
        /// <value>
        /// The pay schedules.
        /// </value>
        public List<string> PaySchedules { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include policies].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include policies]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludePolicies { get; set; }

        /// <summary>
        /// Gets or sets the policies.
        /// </summary>
        /// <value>
        /// The policies.
        /// </value>
        public List<string> Policies { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include roles].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include roles]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeRoles { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        public List<string> Roles { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include templates].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include templates]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeTemplates { get; set; }

        /// <summary>
        /// Gets or sets the templates.
        /// </summary>
        /// <value>
        /// The templates.
        /// </value>
        public List<string> Templates { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include workflows].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include workflows]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeWorkflows { get; set; }

        /// <summary>
        /// Gets or sets the workflows.
        /// </summary>
        /// <value>
        /// The workflows.
        /// </value>
        public List<string> Workflows { get; set; }
    }
}
