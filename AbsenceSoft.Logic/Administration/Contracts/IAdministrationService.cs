﻿
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Administration.Contracts
{
    public interface IAdministrationService
    {
        User GetUserByEmail(string email);
    }
}
