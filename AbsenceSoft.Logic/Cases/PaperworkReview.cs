﻿using AbsenceSoft.Data.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    [Serializable]
    public class PaperworkReview
    {
        public Communication Communication { get; set; }
        public CommunicationPaperwork Paperwork { get; set; }
    }
}
