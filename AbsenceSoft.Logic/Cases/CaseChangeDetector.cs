﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Cases
{
    public delegate void CasePropertyChangedHandler(Case @case, Dictionary<string, object> extras);

    public static class CasePropertyChanged
    {
        public const string MinimumLeaveExhaustionDate = "Calculated.MinimumLeaveExhaustionDate";
        public const string PolicyExhaustionDate = "Calculated.PolicyExhaustionDate";
    }

    public class CaseChangeHandlers : Dictionary<String, CasePropertyChangedHandler>
    {
        public CaseChangeHandlers()
        {

        }
        public CaseChangeHandlers(String caseChangedProperty, CasePropertyChangedHandler handler)
        {
            this.Add(caseChangedProperty, handler);
        }
    }

    public class CaseChangeDetector
    {
        public static void Detect(Case original, Case changed, CaseChangeHandlers handlers)
        {
            handlers = handlers ?? new CaseChangeHandlers();
            if (handlers.ContainsKey(CasePropertyChanged.MinimumLeaveExhaustionDate))
                DetectMinimumLeaveExhaustionDate(original, changed, handlers);
            if (handlers.ContainsKey(CasePropertyChanged.PolicyExhaustionDate))
                DetectPolicyExhaustionDate(original, changed, handlers);
        }

        private static void DetectMinimumLeaveExhaustionDate(Case original, Case changed, CaseChangeHandlers handlers)
        {
            if (original.GetMinLeaveExhaustionDate() != changed.GetMinLeaveExhaustionDate())
            {
                handlers[CasePropertyChanged.MinimumLeaveExhaustionDate](changed, null);
            }
        }

        private static void DetectPolicyExhaustionDate(Case original, Case changed, CaseChangeHandlers handlers)
        {
            Dictionary<string, DateTime?> oldExhaustion = new Dictionary<string, DateTime?>();
            Dictionary<string, DateTime?> newExhaustion = new Dictionary<string, DateTime?>();

            original.Segments.SelectMany(s => s.AppliedPolicies).ForEach(p => 
            {
                if (!oldExhaustion.ContainsKey(p.Policy.Code))
                    oldExhaustion.Add(p.Policy.Code, p.FirstExhaustionDate);
            });
            changed.Segments.SelectMany(s => s.AppliedPolicies).ForEach(p => 
            {
                if (!newExhaustion.ContainsKey(p.Policy.Code))
                    newExhaustion.Add(p.Policy.Code, p.FirstExhaustionDate);
            });

            foreach (var e in newExhaustion)
            {
                DateTime? o = oldExhaustion.ContainsKey(e.Key) ? oldExhaustion[e.Key] : null;
                if (o != e.Value)
                {
                    var c = changed.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.Policy.Code == e.Key).OrderBy(s => s.FirstExhaustionDate).FirstOrDefault();
                    if (c != null)
                        handlers[CasePropertyChanged.PolicyExhaustionDate](changed, new Dictionary<string, object>(1) { { "Policy", c } });
                }
            }
        }
    }
}