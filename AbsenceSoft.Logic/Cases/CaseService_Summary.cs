﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService
    {
        public Case CalcCaseSummary(Case c)
        {
            CaseSummary summary = new CaseSummary();

            // Case Type
            // Set the case type enumeration flags value (bitwise OR)
            c.Segments.Where(s => s.Status != CaseStatus.Cancelled).ForEach(s => summary.CaseType |= s.Type);

            // Eligibility
            var apList = c.Segments.Where(s => s.Status != CaseStatus.Cancelled).SelectMany(s => s.AppliedPolicies).ToList();
            // Set Overall Eligibility Status, if any eligible, then Eligible, if any Pending, then Pending, otherwise Ineligible
            summary.Eligibility = apList.Any(p => p.Status == EligibilityStatus.Eligible) ? EligibilityStatus.Eligible :
                (apList.Any(p => p.Status == EligibilityStatus.Pending) ? EligibilityStatus.Pending : EligibilityStatus.Ineligible);

            // Determination
            // Set Overall Adjudication Status, if any Approved, then Approved, if any Pending, then Pending, otherwise Denied
            var sumDet = c.GetSummaryDetermination();
            switch (sumDet)
            {
                case AdjudicationSummaryStatus.Approved:
                case AdjudicationSummaryStatus.Allowed:
                    summary.Determination = AdjudicationStatus.Approved;
                    break;
                case AdjudicationSummaryStatus.Denied:
                    summary.Determination = AdjudicationStatus.Denied;
                    break;
                default:
                    summary.Determination = AdjudicationStatus.Pending;
                    break;
            }

            //Additional Case Flags
            summary.CaseApproved = summary.Determination == AdjudicationStatus.Approved;
            summary.CaseDenied = summary.Determination == AdjudicationStatus.Denied;
            // Ensure we're comparing all FMLAs 'n' stuff
            var fmlas = apList.Where(p => p.Policy.Code == "FMLA").SelectMany(p => p.Usage).ToList();
            summary.FmlaApproved = fmlas.Any(u => u.Determination == AdjudicationStatus.Approved);
            summary.FmlaDenied = fmlas.Any() && fmlas.All(u => u.Determination == AdjudicationStatus.Denied);
            summary.FmlaExhaustDate = (DateTime?)apList.Where(p => p.Policy.Code == "FMLA").Max(x => x.FirstExhaustionDate);
            if (apList.Any() && apList.SelectMany(x => x.Usage).Any() && apList.SelectMany(x => x.Usage).ToList().Where(y => ( y.Determination == AdjudicationStatus.Approved || y.IntermittentDetermination == IntermittentStatus.Allowed )).Any())
            {
                summary.MaxApprovedThruDate = apList.SelectMany(x => x.Usage).ToList().Where(y => ( y.Determination == AdjudicationStatus.Approved || y.IntermittentDetermination == IntermittentStatus.Allowed )).Max(z => z.DateUsed);
                summary.MaxApprovedThruDatePlus1 = summary.MaxApprovedThruDate.Value.AddDays(1);
            }

            if (apList.Any() && apList.SelectMany(x => x.Usage).Any() && apList.SelectMany(x => x.Usage).ToList().Where(y => ( y.Determination == AdjudicationStatus.Approved || y.IntermittentDetermination == IntermittentStatus.Allowed )).Any())
            {
                summary.MinApprovedThruDate = apList.SelectMany(x => x.Usage).ToList().Where(y => ( y.Determination == AdjudicationStatus.Approved || y.IntermittentDetermination == IntermittentStatus.Allowed )).Min(z => z.DateUsed);
            }

            if (fmlas.Any())
            {
                PolicySummary ps = GetEmployeePolicySummary(c.Employee, DateTime.UtcNow, c, true, "FMLA").FirstOrDefault();
                if (ps != null)
                    summary.FmlaProjectedUsageText = string.Format("{0} {1}", ps.TimeUsed, ps.Units);
            }

            // Policies
            // Get the policy summary information for our date ranges, types and adjudications
            summary.Policies = GetCaseSummaryPolicies(c);
            summary.Policies.ForEach(x => {
                PolicySummary ps = GetEmployeePolicySummary(c.Employee, c.EndDate.HasValue ? c.EndDate.Value.AddDays(1) 
                    : DateTime.UtcNow, c, true, x.PolicyCode).FirstOrDefault();
                if (ps != null)
                    x.ProjectedUsageText = string.Format("{0} {1}", Math.Round(ps.TimeUsed, 2), ps.Units);
            });
            // Assign the newly calculated case summary
            c.Summary = summary;

            // Return the case with the re-calculated case summary
            return c;
        }

        private class _CaseAdjudicationEvalGroup
        {
            public _CaseAdjudicationEvalGroup(CaseSummaryPolicy m) { model = m; }
            public CaseSummaryPolicy model { get; set; }
            public DateTime start { get; set; }
            public DateTime? end { get; set; }
        }

        protected List<CaseSummaryPolicy> GetCaseSummaryPolicies(Case c)
        {
            List<_CaseAdjudicationEvalGroup> eval = EvaluatePolicies(c);
            List<CaseSummaryPolicy> model = BuildPolicyModel(c, eval);
            AddPolicyModelDetail(c, model);
            AddEliminationPolicyMessage(c, model);
            AddKeyEmployeeFMLAMessage(c, model);
            AddSharedWithSpousePolicyMessage(c, model);
            return model;
        }

       


        private List<_CaseAdjudicationEvalGroup> EvaluatePolicies(Case c)
        {
            var eval = new List<_CaseAdjudicationEvalGroup>();
            Dictionary<string, CaseSummaryPolicyDetail> detailChange = new Dictionary<string, CaseSummaryPolicyDetail>();

            foreach (var s in c.Segments.Where(s => s.Status != CaseStatus.Cancelled).OrderBy(g => g.StartDate))
            {
                foreach (var p in s.AppliedPolicies.Where(r => r.Status == EligibilityStatus.Eligible).OrderBy(n => n.StartDate))
                {

                    // Remove any policies that are completely wiped out by consecutive-to rules
                    DateTime realStartDate = p.StartDate;
                    DateTime? realEndDate = p.EndDate;

                    bool isNixedConsecutiveTo = false;
                    List<AppliedPolicy> consecutiveTo = new List<AppliedPolicy>(0);
                    if (p.Policy.ConsecutiveTo.Any())
                    {
                        consecutiveTo = s.AppliedPolicies.Where(ap => ap.Status != EligibilityStatus.Ineligible && ap.Policy.Code != p.Policy.Code && p.Policy.ConsecutiveTo.Contains(ap.Policy.Code)).ToList();
                        var consecutiveToAvailableUsages = consecutiveTo.SelectMany(ap => ap.Usage).Where(u => u.Determination != AdjudicationStatus.Denied);
                        var consecutiveToDeniedUsages = consecutiveTo.SelectMany(ap => ap.Usage).Where(u => u.Determination == AdjudicationStatus.Denied);
                        DateTime? policyStartDate = null;
                        if (consecutiveToDeniedUsages.Any())
                        {
                            DateTime consecutiveToPolicyUsageEndDate = DateTime.MinValue;
                            if (consecutiveToAvailableUsages.Any())
                            {
                                consecutiveToPolicyUsageEndDate = consecutiveToAvailableUsages.Select(au => au.DateUsed).Max();
                            }
                            if (consecutiveToDeniedUsages.Any(du => du.DateUsed > consecutiveToPolicyUsageEndDate))
                            {

                                if (!GetPolicyDateAdjustmentEvent(p.PolicyReason.PolicyEvents, EventDateType.StartPolicyBasedOnEventDate))
                                {
                                    policyStartDate = consecutiveToDeniedUsages.Where(du => du.DateUsed > consecutiveToPolicyUsageEndDate).Select(du => du.DateUsed).Min();
                                }
                                else
                                {
                                    //When we are setting PolicyStartDate or PolicyEndDate Based on StartPolicyBasedOnEventDate ,then we need to check if 
                                    //the consequtiveTo policy has any denied date which is after the policy real date , and that should not exceed 
                                    //policy realEndDate.

                                    policyStartDate = consecutiveToDeniedUsages.Where(du => du.DateUsed >= realStartDate).Select(du => du.DateUsed).Min();
                                    if (policyStartDate.Value >= realEndDate.Value)
                                    {
                                        policyStartDate = null;
                                    }
                                }
                            }
                        }
                        if (consecutiveTo.Any())
                        {
                            if (policyStartDate.HasValue)
                            {
                                realStartDate = policyStartDate.Value;
                            }
                            else if (consecutiveTo.All(cp => cp.EndDate < p.EndDate))
                            {
                                realStartDate = consecutiveTo.Max(cp =>
                                {
                                    if (cp.EndDate.HasValue)
                                        return cp.EndDate.Value.AddDays(1);
                                    return realStartDate;
                                });
                            }
                            else
                            {
                                // This policy will never get applied to the case cause it's dependant policies are not yet exhausted or ended.
                                isNixedConsecutiveTo = true;
                            }
                        }
                    }

                    _CaseAdjudicationEvalGroup cpsm = eval.FirstOrDefault(m => m.model.PolicyCode == p.Policy.Code) ?? eval.AddFluid(new _CaseAdjudicationEvalGroup(new CaseSummaryPolicy()
                    {
                        PolicyName = p.Policy.Name,
                        PolicyCode = p.Policy.Code,
                        Detail = new List<CaseSummaryPolicyDetail>()
                    })
                    {
                        start = realStartDate,
                        end = p.EndDate
                    });

                    // If there are any consecutive
                    if (consecutiveTo.Any())
                        cpsm.model.Messages.Add(string.Format("Will only apply after {0} {1} ended",
                            string.Join(", ", consecutiveTo.Select(r => r.Policy.Name)), consecutiveTo.Count > 1 ? "have" : "has"));

                    // Determine if any policies were adjusted by case events
                    if (p.StartDate > s.StartDate && p.PolicyReason != null && p.PolicyReason.PolicyEvents.Any(e => e.DateType == EventDateType.StartPolicyBasedOnEventDate))
                    {
                        var evt = p.PolicyReason.PolicyEvents.FirstOrDefault(e => e.DateType == EventDateType.StartPolicyBasedOnEventDate);
                        cpsm.model.Messages.Add(string.Format("Starts after the {0}{1}", evt.EventType.ToString().SplitCamelCaseString(), evt.EventType.ToString().EndsWith("Date") ? "" : " Date"));
                    }

                    if(p.Metadata.Contains("SusbstituteFor"))
                    {
                        cpsm.model.Messages.Add($"This policy is only available when {p.Metadata.GetRawValue<string>("SusbstituteFor")} is ineligible");
                    }
                    // maybe this should be named currentDetail? (load a up a pending for the first date of the case. if there is
                    // a value for the first day of the case then switch it over to that)
                    CaseSummaryPolicyDetail currentDetail = cpsm.model.Detail.AddFluid(new CaseSummaryPolicyDetail()
                    {
                        CaseType = s.Type,
                        Determination = AdjudicationSummaryStatus.Pending,
                        StartDate = realStartDate
                    });

                    var lastDetail = detailChange.ContainsKey(p.Policy.Code) ? detailChange[p.Policy.Code] : null;
                    if (lastDetail == null)
                    {
                        lastDetail = currentDetail;
                        detailChange.Add(p.Policy.Code, lastDetail);
                    }

                    AppliedPolicyUsage startDatePol = p.Usage.FirstOrDefault(sdp => sdp.DateUsed == p.StartDate);
                    List<AppliedPolicyUsage> theUsages = p.Usage.OrderBy(n => n.DateUsed).ToList();

                    // if we didn't get a hit on the first day of the case, then see if there are any days usage in the case
                    if (startDatePol == null)
                    {
                        // if there are usages, get the first one and set the current end date to it -1
                        if (theUsages.Count > 0)
                            currentDetail.EndDate = theUsages[0].DateUsed.AddDays(-1);
                        else
                            // otherwise the current end date is the end of the case
                            currentDetail.EndDate = p.EndDate;
                    }
                    else
                    {
                        // if there is a usage for the first day of the case, then set it up instead
                        currentDetail.Determination = startDatePol.SummaryStatus;
                        currentDetail.DenialReasonCode = startDatePol.DenialReasonCode;
                        currentDetail.DenialReasonName = startDatePol.DenialReasonName;
                        currentDetail.DenialReasonOther = startDatePol.DenialExplanation;
                        currentDetail.EndDate = currentDetail.EndDate.HasValue ? currentDetail.EndDate.Value : DateTime.MinValue;
                        AppliedPolicyUsage apuPP = p.Usage.FirstOrDefault(fd => fd.DateUsed == currentDetail.EndDate);

#warning TODO: STD Pay - Set Payout Percentage for Case Summary on Policy
                        //if (apuPP == null)
                        //    currentDetail.PayoutPercentage = 0m;
                        //else
                        //    currentDetail.PayoutPercentage = apuPP.PaymentPercentage;
                    }

                    // now build up the rest of the values
                    DateTime usageDate = DateTime.MinValue;
                    foreach (var u in theUsages)
                    {
                        int addDays = 1;

                        // If we're doing the same date again, skip it, it happens, especially on split status intermittent thingies
                        if (u.DateUsed <= usageDate)
                            continue;

                        usageDate = u.DateUsed;

                        if (lastDetail.CaseType != s.Type || // The case types do not match (new segment), OR
                            (s.Type != CaseType.Intermittent && lastDetail.DenialReasonCode != u.DenialReasonCode) || // We have a different denial reason, OR
                            lastDetail.Determination != u.SummaryStatus || // We have a different adjudication status, OR
                            (lastDetail.EndDate != DateTime.MinValue && lastDetail.EndDate.Value.AddDays(addDays) != u.DateUsed) // We are skipping a date (not the last date used for this type)
                            )
                        {
                            // Then we create a new detail if not the first part of a segment or grouping (already added to the collection)
                            if ((currentDetail.CaseType != s.Type ||
                                currentDetail.Determination != u.SummaryStatus) &&
                                currentDetail.StartDate != u.DateUsed)
                            {
                                currentDetail = cpsm.model.Detail.AddFluid(new CaseSummaryPolicyDetail()
                                {
                                    CaseType = s.Type,
                                    DenialReasonCode = u.DenialReasonCode,
                                    DenialReasonName = u.DenialReasonName,
                                    Determination = u.SummaryStatus,
                                    StartDate = u.DateUsed,
                                    EndDate = u.DateUsed,
                                    DenialReasonOther = u.DenialExplanation
                                });
                            }
                            else
                            {
                                currentDetail.DenialReasonCode = u.DenialReasonCode;
                                currentDetail.DenialReasonName = u.DenialReasonName;
                                currentDetail.Determination = u.SummaryStatus;
                                currentDetail.EndDate = u.DateUsed;
                                currentDetail.DenialReasonOther = u.DenialExplanation;
                            }
                        }

                        // Otherwise, we're going to up the date of our current one.
                        currentDetail.EndDate = u.DateUsed;

                        // and now this gets even uglier (if this is an intermitent type, then peek ahead to tomorrow and see if
                        // it is used and if not, then create a pending for it as well
                        DateTime workDate = currentDetail.EndDate.Value.AddDays(addDays);
                        if (currentDetail.CaseType == CaseType.Intermittent && workDate <= p.EndDate && !theUsages.Any(tu => tu.DateUsed == workDate))
                        {
                            currentDetail = cpsm.model.Detail.AddFluid(new CaseSummaryPolicyDetail()
                            {
                                CaseType = s.Type,
                                Determination = AdjudicationSummaryStatus.Pending,
                                StartDate = workDate,
                                EndDate = workDate
                            });

                            // then find the end of this gap
                            AppliedPolicyUsage isThereANextOne = theUsages.FirstOrDefault(tu => tu.DateUsed > workDate);
                            if (isThereANextOne == null)
                                currentDetail.EndDate = p.EndDate;
                            else
                                currentDetail.EndDate = isThereANextOne.DateUsed.AddDays(-1);
                        }

                        // Assign our last detail here.
                        lastDetail = detailChange[p.Policy.Code] = currentDetail;
                    }

                    // now.... if the last one we have is an intermittent object, and it isn't on the last day of the case then
                    // we need to create a set of pendings from the day after the last intermittent, to the end of the case
                    if (currentDetail.CaseType == CaseType.Intermittent && currentDetail.EndDate != s.EndDate)
                    {
                        CaseSummaryPolicyDetail finishIt = cpsm.model.Detail.AddFluid(new CaseSummaryPolicyDetail()
                        {
                            CaseType = s.Type,
                            DenialReasonCode = currentDetail.DenialReasonCode,
                            DenialReasonName = currentDetail.DenialReasonName,
                            Determination = AdjudicationSummaryStatus.Pending,
                            StartDate = currentDetail.EndDate.Value.AddDays(1),
                            EndDate = p.EndDate,
                            DenialReasonOther = currentDetail.DenialReasonOther,
                            PayoutPercentage = currentDetail.PayoutPercentage
                        });
                    }

                    if (isNixedConsecutiveTo)
                    {
                        // Clear out the detail, 'cause it's all rubbish.
                        //  there is no real usage because this is a non-concurrent policy (consecutive-to)
                        //  and it's predecessing policy will not exhaust during this leave, so it's nixed
                        //  from the usage and will get assigned a nice friendly message below.
                        cpsm.model.Detail = new List<CaseSummaryPolicyDetail>(0);
                    }
                }
            }

            return eval;
        }
        /// <summary>
        /// Get the List of Policy Event and check if any date adjustment event exists 
        /// </summary>
        /// <param name="PolicyEvents"></param>
        /// <param name="policyEvent"></param>
        /// <returns></returns>
        private bool GetPolicyDateAdjustmentEvent(List<PolicyEvent> PolicyEvents, EventDateType policyEvent)
        {
            foreach(var pEvent in PolicyEvents)
            {
                if (pEvent.DateType == policyEvent)
                {
                    return true;
                }
            }
            return false;
        }

        private List<CaseSummaryPolicy> BuildPolicyModel(Case c, List<_CaseAdjudicationEvalGroup> evaluatedPolicies)
        {
            // Flatten gaps
            List<CaseSummaryPolicy> model = new List<CaseSummaryPolicy>();
            foreach (var p in evaluatedPolicies.OrderBy(d => d.start))
            {
                var policy = model.AddFluid(new CaseSummaryPolicy()
                {
                    PolicyName = p.model.PolicyName,
                    PolicyCode = p.model.PolicyCode,
                    Detail = new List<CaseSummaryPolicyDetail>(),
                    Messages = p.model.Messages ?? new List<string>()
                });
                CaseSummaryPolicyDetail lastDetail = null;
                foreach (var u in p.model.Detail)
                {
                    if (lastDetail == null)
                    {
                        lastDetail = policy.Detail.AddFluid(new CaseSummaryPolicyDetail()
                        {
                            CaseType = u.CaseType,
                            DenialReasonCode = u.DenialReasonCode,
                            DenialReasonName = u.DenialReasonName,
                            EndDate = u.EndDate,
                            StartDate = u.StartDate,
                            Determination = u.Determination,
                            DenialReasonOther = u.DenialReasonOther,
                            PayoutPercentage = u.PayoutPercentage
                        });
                    }
                    else
                    {
                        if ((lastDetail.CaseType != CaseType.Intermittent && lastDetail.CaseType != CaseType.Reduced && lastDetail.Determination == u.Determination && lastDetail.CaseType == u.CaseType && lastDetail.EndDate.Value.AddDays(1) == u.StartDate)
                            || (lastDetail.CaseType == CaseType.Intermittent && lastDetail.Determination == u.Determination && lastDetail.EndDate.Value.AddDays(1) == u.EndDate && lastDetail.CaseType == u.CaseType))
                            lastDetail.EndDate = u.EndDate;
                        else
                        {
                            var newDetail = policy.Detail.AddFluid(new CaseSummaryPolicyDetail()
                            {
                                CaseType = u.CaseType,
                                DenialReasonCode = u.DenialReasonCode,
                                DenialReasonName = u.DenialReasonName,
                                EndDate = u.EndDate,
                                StartDate = u.StartDate,
                                Determination = u.Determination,
                                DenialReasonOther = u.DenialReasonOther,
                                PayoutPercentage = u.PayoutPercentage
                            });

                            /// Commented out as part of AT-1309 on 2/10/2016
                            /// Policy Summary was displaying incorrect value
                            /// IE - saying the policy was ending right before the next segment instead of leaving a gap
                            /// If you see this some months after 2/10, feel free to nuke
                            //if (lastDetail.CaseType != CaseType.Intermittent)
                            //    lastDetail.EndDate = newDetail.StartDate.AddDays(-1);

                            lastDetail = newDetail;
                        }
                    } // else if (lastDetail == null)
                } // foreach (var u in p.model.Detail)

                if (lastDetail != null && p.end > lastDetail.EndDate && lastDetail.CaseType != CaseType.Intermittent)
                    lastDetail.EndDate = p.end;

            } // foreach (var p in eval.OrderBy(d => d.start))
            return model;
        }

        private void AddPolicyModelDetail(Case c, List<CaseSummaryPolicy> model)
        {
            // Now, for some truly clever UI finaggling :-)
            foreach (var m in model)
            {
                if (m.Detail == null || !m.Detail.Any())
                {
                    var applicableChunks = c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.Policy.Code == m.PolicyCode).ToList();
                    var consecs = applicableChunks.SelectMany(r => r.Policy.ConsecutiveTo).Intersect(model.Select(k => k.PolicyCode)).ToList();
                    if (consecs.Any())
                    {
                        m.Messages.Add(string.Format("Will only apply after {0} {1} ended",
                            string.Join(", ", model.Where(s => consecs.Any(j => j == s.PolicyCode)).Select(r => r.PolicyName)), consecs.Count > 1 ? "have" : "has"));
                        continue;
                    }
                    m.Detail = m.Detail ?? new List<CaseSummaryPolicyDetail>();
                    m.Detail.AddRange(
                        applicableChunks.OrderBy(o => o.StartDate).Select(a => new CaseSummaryPolicyDetail()
                        {
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            CaseType = c.Segments.First(s => s.StartDate <= a.StartDate && s.EndDate >= a.EndDate).Type,
                            Determination = AdjudicationSummaryStatus.Pending
                        })
                   );
                }
            }
        }

        private void AddEliminationPolicyMessage(Case c, List<CaseSummaryPolicy> model)
        {
            //check if any of the policies were denied entirely due to reason 'Elimination Period'            
            foreach (CaseSummaryPolicy apm in model)
            {
                if (apm.Detail.Any() && apm.Detail.All(y => y.Determination == AdjudicationSummaryStatus.Denied && y.DenialReasonCode == AdjudicationDenialReason.EliminationPeriod))
                {
                    string elimMessage = "Policy is eligible but no time is available due to elimination period";

                    // Determine if the elimination period was hosed because of a dumb work schedule or retarded leave duration.
                    var appliedPolicy = c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.Policy.Code == apm.PolicyCode).FirstOrDefault();
                    if (appliedPolicy != null)
                    {
                        double elim = appliedPolicy.PolicyReason.Elimination ?? 0d;
                        EntitlementType et = appliedPolicy.PolicyReason.EliminationType ?? EntitlementType.WorkDays;
                     
                        List <Time> sched = new LeaveOfAbsence() { Case = c, Employee = c.Employee, Employer = c.Employer, Customer = c.Customer }
                            .MaterializeSchedule(c.StartDate, c.EndDate ?? appliedPolicy.EndDate ?? DateTime.UtcNow.ToMidnight(),true,null,appliedPolicy.PolicyBehaviorType);
                        var eUnit = 0d;
                        var wUnit = 0d;
                        DateTime start;
                        switch (et)
                        {
                            case EntitlementType.CalendarWeeks:
                            case EntitlementType.WorkWeeks:
                            case EntitlementType.CalendarWeeksFromFirstUse:
                                start = c.StartDate.GetFirstDayOfWeek();
                                while (start < c.EndDate)
                                {
                                    eUnit++;
                                    wUnit += sched.Where(s => s.SampleDate.IsInRange(start, start.GetLastDayOfWeek())).Any(s => s.TotalMinutes.HasValue && s.TotalMinutes > 0) ? 1 : 0;
                                    start = start.AddDays(7d);
                                }
                                break;
                            case EntitlementType.CalendarMonths:
                            case EntitlementType.WorkingMonths:
                            case EntitlementType.CalendarMonthsFromFirstUse:
                                start = c.StartDate.GetFirstDayOfMonth();
                                while (start < c.EndDate)
                                {
                                    eUnit++;
                                    wUnit += sched.Where(s => s.SampleDate.IsInRange(start, start.GetLastDayOfMonth())).Any(s => s.TotalMinutes.HasValue && s.TotalMinutes > 0) ? 1 : 0;
                                    start = start.GetLastDayOfMonth().AddDays(1);
                                }
                                break;
                            case EntitlementType.CalendarYears:
                            case EntitlementType.CalendarYearsFromFirstUse:
                                eUnit = c.StartDate.AddYears(Convert.ToInt32(eUnit)).Subtract(c.StartDate).TotalDays;
                                break;
                            case EntitlementType.WorkingHours:
                                eUnit = elim;
                                wUnit = sched.Any() ? sched.Sum(s => s.TotalMinutes ?? 0d) / 60d : 0d;
                                break;
                            case EntitlementType.WorkDays:
                            case EntitlementType.FixedWorkDays:
                            case EntitlementType.CalendarDaysFromFirstUse:
                                eUnit = c.EndDate.Value.Subtract(c.StartDate).TotalDays;
                                wUnit = sched.Count(s => s.TotalMinutes.HasValue && s.TotalMinutes > 0);
                                break;
                        }

                        if (eUnit < elim)
                            elimMessage = "The policy is eligible but no time is available due to the duration of the leave not meeting the elimination period";
                        else if (et.PercentageType() && wUnit < elim)
                            // We're dealing with "work" something or anothers. So, if they're not scheduled to work the total number of days according to the elimination period
                            //  then we need to let 'em know.
                            elimMessage = "The policy is eligible but no time is available due to the employee schedule lacking enough work days to meet the elimination period";
                    }
                    apm.Messages.Add(elimMessage);
                }
            }
        }


        private void AddKeyEmployeeFMLAMessage(Case employeeCase, List<CaseSummaryPolicy> model)
        {
            if (model != null && employeeCase != null && employeeCase.Segments != null && employeeCase.Segments.Any() && employeeCase.Employee.IsKeyEmployee)
            {                
                List <string> fmlaPolicyCodes = employeeCase.Segments.SelectMany(s => s.AppliedPolicies).Where(ap => ap.Policy.PolicyType == PolicyType.FMLA && ap.Status != EligibilityStatus.Ineligible).Select(ap => ap.Policy.Code).ToList();
                var fmlaCaseSummaryPolicies = model.Where(m => fmlaPolicyCodes.Contains(m.PolicyCode));
                if (fmlaCaseSummaryPolicies != null && fmlaCaseSummaryPolicies.Any())
                {
                    foreach (CaseSummaryPolicy fmlaCaseSummaryPolicy in fmlaCaseSummaryPolicies)
                    {
                        if (fmlaCaseSummaryPolicy.Detail != null && fmlaCaseSummaryPolicy.Detail.Any())
                        {
                            fmlaCaseSummaryPolicy.Messages.Add("This employee is designated as a Key Employee. Under FMLA regulation, Key Employees are not required to be reinstated to a same or similar position");
                        }
                    }
                }
            }
        }

        private void AddSharedWithSpousePolicyMessage(Case employeeCase, List<CaseSummaryPolicy> model)
        {
            if (model != null && employeeCase != null && employeeCase.Segments != null && employeeCase.Segments.Any() && employeeCase.Employee.SpouseEmployeeNumber != null)
            {
                List<AppliedPolicy> appliedPolicies = employeeCase.Segments.SelectMany(s => s.AppliedPolicies).Where(ap => ap.Status != EligibilityStatus.Ineligible && ap.IsCombinedForSpouses.HasValue).ToList();
                List<string> allPolicyCodes = appliedPolicies.Select(ap => ap.Policy.Code).ToList();
                var allCaseSummaryPolicies = model.Where(m => allPolicyCodes.Contains(m.PolicyCode));
                if (allCaseSummaryPolicies != null && allCaseSummaryPolicies.Any())
                {
                    foreach (CaseSummaryPolicy allCaseSummaryPolicy in allCaseSummaryPolicies)
                    {
                        if (appliedPolicies.Any(p => p.Policy.Code == allCaseSummaryPolicy.PolicyCode && p.IsCombinedForSpouses.Value))
                        {
                            allCaseSummaryPolicy.Messages.Add("Check shared spouse entitlement.");
                        }
                    }
                }
            }
        }
    }
}
