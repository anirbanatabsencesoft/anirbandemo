﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases.Contracts;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public class AccommodationService : LogicService, IAccommodationService
    {
        public AccommodationService()
        {

        }

        public AccommodationService(User currentUser)
            : base(currentUser)
        {

        }

        public AccommodationService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }
        /// <summary>
        /// Gets all accommodations for an employee, perfect for displaying on an employee summary or something like that.
        /// </summary>
        /// <param name="employeeId">The employee to pull accommodations for.</param>
        /// <returns>A list of all accommodations for an employee, if any.</returns>
        public List<AccommodationRequest> GetEmployeeAccommodations(string employeeId)
        {
            using (new InstrumentationContext("AccommodationService.GetEmployeeAccommodations"))
            {
                return Case.AsQueryable().Where(c => c.Employee.Id == employeeId && c.IsAccommodation).OrderByDescending(c => c.StartDate).ToList().Select(c =>
                {
                    var req = c.AccommodationRequest;
                    req.CaseId = c.Id;
                    req.CaseNumber = c.CaseNumber;
                    return req;
                }).ToList();
            }
        }//end: GetEmployeeAccommodations


        /// <summary>
        /// Creates or modifies the accommodation request on the case and assigns it to the case itself.
        /// </summary>
        /// <param name="myCase">The case </param>
        /// <param name="type">The type of accommodation request</param>
        /// <param name="duration">The duration of the accommodation request</param>
        /// <param name="generalHealthCondition"></param>
        /// <returns></returns>
        public AccommodationRequest CreateOrModifyAccommodationRequest(Case myCase, string generalHealthCondition, List<Accommodation> newAccomms)
        {
            using (new InstrumentationContext("AccommodationService.CreateOrModifyAccommodationRequest"))
            {
                AccommodationRequest request = myCase.AccommodationRequest ?? new AccommodationRequest();
                request.GeneralHealthCondition = generalHealthCondition;
                request.Accommodations = newAccomms;
                myCase.AccommodationRequest = request;
                myCase.IsAccommodation = true;


                if (myCase.AccommodationRequest.AdditionalInfo == null || !myCase.AccommodationRequest.AdditionalInfo.Steps.Any())
                    request.AdditionalInfo = GetEmployerAccommodationInteractiveProcess(myCase.CustomerId, myCase.EmployerId, null, false);
                else
                {
                    // Backfill any missing steps to the additional info
                    var steps = GetEmployerAccommodationInteractiveProcess(myCase.CustomerId, myCase.EmployerId, null, false).Steps
                        .Where(step => !myCase.AccommodationRequest.AdditionalInfo.Steps.Any(s => s.QuestionId == step.QuestionId))
                        .ToList();
                    if (steps.Any())
                        myCase.AccommodationRequest.AdditionalInfo.Steps.AddRange(steps);
                }



                foreach (Accommodation a in myCase.AccommodationRequest.Accommodations)
                {
                    if (a.InteractiveProcess == null || !a.InteractiveProcess.Steps.Any())
                        a.InteractiveProcess = GetEmployerAccommodationInteractiveProcess(myCase.CustomerId, myCase.EmployerId, a.Type.Code);
                    else
                    {
                        // Backfill any missing steps to the interactive process
                        var steps = GetEmployerAccommodationInteractiveProcess(myCase.CustomerId, myCase.EmployerId, a.Type.Code).Steps
                            .Where(step => !a.InteractiveProcess.Steps.Any(s => s.QuestionId == step.QuestionId))
                            .ToList();
                        if (steps.Any())
                            a.InteractiveProcess.Steps.AddRange(steps);
                    }
                }

                return request;
            }
        }//end: CreateAccommodationRequest

        /// <summary>
        /// Takes a list of accommodation usage and a start and optional end date, then crops/trims up the usage appropriately
        /// where it falls outside of those boundaries (start and optional end dates). This way we can remove from the beginning
        /// and optionally the end (for temporary accommodations) of the usage collection to ensure the entire usage collection
        /// falls appropriate within the boundary dates.
        /// </summary>
        /// <param name="usage">The usage collection to box</param>
        /// <param name="startDate">The start date which should be the earliest date of all usage.</param>
        /// <param name="endDate">The optional end date which should be the latest date of all usage, or <c>null</c> for no end date (unbounded/perpetual).</param>
        /// <returns>A list of <see cref="AbsenceSoft.Data.Cases.AccommodationUsage">usage</see> that has been cropped/trimmed and boxed to the passed in dates.</returns>
        public List<AccommodationUsage> BoxUsage(List<AccommodationUsage> usage, DateTime startDate, DateTime? endDate)
        {
            using (new InstrumentationContext("AccommodationService.BoxUsage"))
            {
                List<AccommodationUsage> retVal = new List<AccommodationUsage>();
                foreach (var u in usage.OrderBy(u => u.StartDate))
                {
                    if (u.EndDate.HasValue && u.EndDate.Value < startDate)
                        continue; // skip this one, it's no longer relevant

                    if (endDate.HasValue && u.StartDate > endDate)
                        continue; // skip this one, it's no longer relevant

                    // If this uage's dates encompass the start date, then we need to set the start date explicitly
                    if (startDate.DateInRange(u.StartDate, u.EndDate))
                        u.StartDate = startDate;

                    // If we are using a real end date, and this usage's dates encompass the end date, then we need to set the end date explicitly
                    if (endDate.HasValue && (!u.EndDate.HasValue || endDate.Value.DateInRange(u.StartDate, u.EndDate)))
                        u.EndDate = endDate;

                    // Add the usage to the return value collection.
                    retVal.Add(u);
                }
                // No usage to apply, then add the last one or if none create a pending one for the entire accommodation
                if (!retVal.Any())
                {
                    var u = usage.FirstOrDefault() ?? new AccommodationUsage() { Determination = AdjudicationStatus.Pending };
                    u.StartDate = startDate;
                    u.EndDate = endDate;
                    retVal.Add(u);
                }
                // If we have any a desired null end date, yet all our usage is end-dated, we need to null the last usage's end date, duh.
                if (!endDate.HasValue && !retVal.Any(r => !r.EndDate.HasValue))
                    retVal.OrderBy(r => r.EndDate.Value).Last(r => r.EndDate.HasValue).EndDate = null;

                return retVal.OrderBy(r => r.StartDate).ToList();
            }
        }//end: BoxUsage

        /// <summary>
        /// Takes a list of accommodation usage and a start and optional end date, then crops/trims up the usage appropriately
        /// where it falls outside of those boundaries (start and optional end dates). This way we can remove from the beginning
        /// and optionally the end (for temporary accommodations) of the usage collection to ensure the entire usage collection
        /// falls appropriate within the boundary dates. This should be called before assigning determination status
        /// </summary>
        /// <param name="usage"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<AccommodationUsage> BoxUsageBeforeDetermination(List<AccommodationUsage> usage, DateTime startDate, DateTime? endDate)
        {
            using (new InstrumentationContext("AccommodationService.BoxUsageBeforeDetermination"))
            {
                List<AccommodationUsage> retVal = new List<AccommodationUsage>();
                foreach (var u in usage.OrderBy(u => u.StartDate))
                {
                    if (u.EndDate.HasValue && u.EndDate.Value < startDate)
                        continue; // skip this one, it's no longer relevant

                    if (endDate.HasValue && u.StartDate > endDate)
                        continue; // skip this one, it's no longer relevant

                    if (u.EndDate.HasValue && startDate > u.EndDate.Value)
                        continue; // skip this one, it's no longer relevant

                    //If Request Start Date is occurs way before any of the detrmination date range, then insert additional usage.
                    if (startDate < u.StartDate && (endDate.HasValue == false || (endDate.HasValue && endDate.Value >= u.StartDate)))
                    {
                        if (!retVal.OrderBy(r => r.StartDate).ToList().Any(r => startDate.DateInRange(r.StartDate, r.EndDate)))
                        {
                            AccommodationUsage au = new AccommodationUsage()
                            {
                                StartDate = startDate,
                                EndDate = u.StartDate.AddDays(-1),
                                Determination = AdjudicationStatus.Pending
                            };
                            retVal.Add(au);
                        }
                    }

                    // If this uage's dates encompass the start date, then we need to set the start date explicitly
                    if (startDate.DateInRange(u.StartDate, u.EndDate))
                        u.StartDate = startDate;

                    // If we are using a real end date, and this usage's dates encompass the end date, then we need to set the end date explicitly
                    if (endDate.HasValue && (!u.EndDate.HasValue || endDate.Value.DateInRange(u.StartDate, u.EndDate)))
                        u.EndDate = endDate;

                    // Add the usage to the return value collection.
                    retVal.Add(u);
                }
                // No usage to apply, then add the last one or if none create a pending one for the entire accommodation
                if (!retVal.Any())
                {
                    var u = usage.FirstOrDefault() ?? new AccommodationUsage() { Determination = AdjudicationStatus.Pending };
                    u.StartDate = startDate;
                    u.EndDate = endDate;
                    retVal.Add(u);
                }
                // If we have any a desired null end date, yet all our usage is end-dated, we need to null the last usage's end date, duh.
                if (!endDate.HasValue && !retVal.Any(r => !r.EndDate.HasValue))
                    retVal.OrderBy(r => r.EndDate.Value).Last(r => r.EndDate.HasValue).EndDate = null;

                // If we have an end date greater than last usage's end date, we need to assign it to the last usage's end date,.                
                DateTime? usageEndDate = retVal.OrderBy(r => r.StartDate).Last().EndDate;
                if (usageEndDate.HasValue && endDate.HasValue && usageEndDate.Value < endDate.Value)
                {
                    AccommodationUsage au = new AccommodationUsage()
                    {
                        StartDate = usageEndDate.Value.AddDays(1),
                        EndDate = endDate,
                        Determination = AdjudicationStatus.Pending
                    };
                    retVal.Add(au);
                }

                //Merge usages if they occur in sequence i.e. with just one day difference between prior usage
                //end date and next usage start date, and have same determination values.
                List<AccommodationUsage> usagelist = retVal.OrderBy(r => r.StartDate).ToList();
                List<AccommodationUsage> returnlist = new List<AccommodationUsage>();
                if (usagelist.Count > 1)
                {
                    foreach (AccommodationUsage item in usagelist)
                    {
                        if (returnlist.Count == 0)
                        {
                            returnlist.Add(item);
                        }
                        else if (returnlist[returnlist.Count - 1].EndDate.HasValue
                            && returnlist[returnlist.Count - 1].EndDate.Value.AddDays(1) == item.StartDate
                            && item.Determination == returnlist[returnlist.Count - 1].Determination)
                        {
                            if(item.DenialReasonCode != null && item.DenialReasonCode != returnlist[returnlist.Count - 1].DenialReasonCode)
                                returnlist.Add(item);
                            else
                                returnlist[returnlist.Count - 1].EndDate = item.EndDate;
                        }
                        else
                        {
                            returnlist.Add(item);
                        }
                    }
                }
                else
                {
                    returnlist = usagelist;
                }
                return returnlist;
            }
        }//end: BoxUsageBeforeDetermination


        /// <summary>
        /// Applies an approval decision to a case for accommodations only based on a start and optional end date given the specific status
        /// and any optional denial reason if denied with the option of specifying only specific accommodation ids for the decision.
        /// </summary>
        /// <param name="myCase">The case containing the accommodation request to apply the approval decision.</param>
        /// <param name="startDate">The start date of the approval decision.</param>
        /// <param name="endDate">The optional end date of the approval decision.</param>
        /// <param name="status">The decision status itself.</param>
        /// <param name="reasonCode">If denied, the denial reason code.</param>
        /// <param name="reasonOther">If the denied and the reason of other is specified, the other reason description.</param>
        /// <param name="reasonName">If denied, the denial reason name.</param>
        /// <param name="accommodationIds">An optional list/array of of accommodation Ids to specifically apply the approval decision to.</param>
        /// <returns>The modified Case which has NOT been saved yet.</returns>
        public Case ApplyDetermination(Case myCase, DateTime startDate, DateTime? endDate, AdjudicationStatus status, string reasonCode, string reasonOther, string reasonName, params Guid[] accommodationIds)
        {
            using (new InstrumentationContext("AccommodationService.ApplyDetermination"))
            {
                reasonCode = string.IsNullOrWhiteSpace(reasonCode) ? reasonCode : reasonCode.ToUpperInvariant();
                // See if there's anything to do, or if this is an invalid request; if it's invalid maybe we should throw an exception.
                if (myCase == null)
                    throw new ArgumentNullException("myCase");
                if (!myCase.IsAccommodation)
                    throw new AbsenceSoftException("This is not an accommodations case, you may not make an approval decision on accommodations for a non-accommodations case");
                if (myCase.AccommodationRequest == null)
                    throw new AbsenceSoftException("No accommodation request was found on the case to make an approval decision");
                if (!myCase.AccommodationRequest.Accommodations.Any())
                    throw new AbsenceSoftException("No accommodations were found on the case to make an approval decision");

                // date times kinda suck. .net / JS / Mongo all want to fix the timezone even when we set it
                // or don't want it fixed because it's not broken. So, just force the start and end dates back to midnight
                startDate = startDate.ToMidnight();

                if (endDate.HasValue)
                {
                    endDate = endDate.ToMidnight();
                    if (startDate > endDate)
                        throw new AbsenceSoftException("End Date occurrs before Start Date, did you intend to reverse them?");
                }

                if (status == AdjudicationStatus.Denied && string.IsNullOrWhiteSpace(reasonCode))
                    throw new AbsenceSoftException("Denial Reason must be specified if the adjudication status is Denied");
                if (reasonCode == AccommodationAdjudicationDenialReason.Other && string.IsNullOrWhiteSpace(reasonOther))
                    throw new AbsenceSoftException("Denial Reason other must be specified if the adjudication status is Denied and the Reason 'Other' is selected");

                // Loop through each of our accommodations to see where and how this adjudication should be supplied
                foreach (Accommodation accom in myCase.AccommodationRequest.Accommodations.Where(a => accommodationIds.Length == 0 || accommodationIds.Contains(a.Id)))
                {
                    // Get our bounding dates; this will re-box our final outcome (we wouldn't want dates to change you know).
                    DateTime boundingStart = accom.StartDate ?? startDate;
                    DateTime? boundingEnd = accom.EndDate;

                    // Get our test dates for testing stuff
                    DateTime newStart = startDate;
                    // On the end date, since we want a nice easy test, if it's null use max date instead, it's OK, we'll live
                    DateTime newEnd = endDate ?? DateTime.MaxValue;

                    // Loop through our existing usage and see what we need to trim up.
                    // Begin Brain Damage here:
                    List<AccommodationUsage> splitUsage = new List<AccommodationUsage>();
                    List<AccommodationUsage> removeUsage = new List<AccommodationUsage>();
                    foreach (var u in accom.Usage.OrderBy(s => s.StartDate).ThenBy(s => s.EndDate))
                    {
                        DateTime oldStart = u.StartDate;
                        DateTime oldEnd = u.EndDate ?? DateTime.MaxValue;

                        // If our test date range is completely before this usage, then skip it
                        if (newStart < oldStart && newEnd < oldStart)
                            continue;

                        // If our test date range is completely after this usage, then skip it
                        if (newStart > oldEnd)
                            continue;

                        // If our test date range engulfs this one; overwrite this one.
                        if (newStart <= oldStart && newEnd >= oldEnd)
                        {
                            removeUsage.Add(u);
                            continue;
                        }

                        if (newStart == oldStart && newEnd == oldEnd)
                        {
                            removeUsage.Add(u);
                            continue;
                        }

                        // SCENARIO A
                        // 
                        // 1.)      |---------->    (old)
                        //     |--------------->    (new)
                        //
                        // 2.) |--------------->    (old)
                        //          |---------->    (new)
                        // 
                        // 3.)      |----------|    (old)
                        //     |--------------->    (new)
                        //
                        // 4.) |---------------|    (old)
                        //          |---------->    (new)
                        // 
                        // If the new segment is a perpetual segment, this would only create 2 possible ways of handling this
                        if (endDate == null)
                        {
                            // If our new segment starts before our old one, guess what, it's perpetual nature will
                            //  completely overwrite this one, so we do a wholesale replacement and flip the add bit to false.
                            if (newStart < oldStart)
                                removeUsage.Add(u);
                            else
                                // Otherwise, we know we just need to end date our other perpetual segment to end the day
                                //  before our new segment starts, so that one is no longer perpetual.
                                u.EndDate = newStart.AddDays(-1);
                            continue;
                        } // end: A

                        // SCENARIO B
                        // 
                        // 1.)      |---------->    (old)
                        //     |---------------|    (new)
                        //
                        // 2.) |--------------->    (old)
                        //          |----------|    (new)
                        // 
                        // If the old segment was perpetual, there are only 2 possible ways to handle this too
                        if (u.EndDate == null)
                        {
                            // If our new segment starts on or before our existing perpetual segment, then we need to
                            //  have our perptual segment start later on, specifically the day after our finite segment ends
                            if (newStart <= oldStart)
                                u.StartDate = newEnd.AddDays(1);
                            else
                            {
                                // In the case where our new finite segment starts after the perpetual segment's start date, then
                                //   we're going to end up splitting the existing perpetual segment so it wraps the new one.
                                // Clone our current segment, we need a deep copy of it
                                var clone = u.Clone();
                                // Ensure our clone has a unique Id
                                clone.Id = Guid.NewGuid();
                                // Set the new split item's start date to the day after the new finite segment's end date
                                clone.StartDate = newEnd.AddDays(1);
                                // Set the old perpetual item's end date here (since we have a new perpetual segment, the clone, to pick up after
                                //  the new finite segment's end date).
                                u.EndDate = newStart.AddDays(-1);
                                // Add our clone to the new split-usage collection so it can be added later on in the process.
                                splitUsage.Add(clone);
                            }
                            continue;
                        } // end: B

                        // SCENARIO C
                        // 
                        // 1.)      |----------|    (old)
                        //     |----------|         (new)
                        //
                        // 2.) |----------|         (old)
                        //          |----------|    (new)
                        // 
                        // 3.) |---------------|    (old)
                        //         |-------|        (new)
                        //
                        // Both segments have end dates, so one of 3 possible scenarios could happen, shorten the front of the old, 
                        //  shorten the end of the old, or split the old
                        if (endDate.HasValue && u.EndDate.HasValue)
                        {
                            if (newStart <= oldStart)
                                u.StartDate = newEnd.AddDays(1);
                            else
                            {
                                // Easy enough, if the new segment ends after the old segment (end date overlap) then we just need to shorten
                                //  the old segment to start the day after the new segment ends
                                if (newEnd >= oldEnd)
                                    u.EndDate = newStart.AddDays(-1);
                                else
                                {
                                    // In the case where our new segment starts after the old segment's start date, then
                                    //   we're going to end up splitting the existing segment so it wraps the new one.
                                    // Clone our current segment, we need a deep copy of it
                                    var clone = u.Clone();
                                    // Ensure our clone has a unique Id
                                    clone.Id = Guid.NewGuid();
                                    // Set the new split item's start date to the day after the new segment's end date
                                    clone.StartDate = newEnd.AddDays(1);
                                    // Set the old perpetual item's end date here (since we have a new segment, the clone, to pick up after
                                    //  the new segment's end date).
                                    u.EndDate = newStart.AddDays(-1);
                                    // Add our clone to the new split-usage collection so it can be added later on in the process.
                                    splitUsage.Add(clone);
                                }
                            }
                        } // end: C
                    }

                    // Remove any unecessary usage after the add operation
                    removeUsage.ForEach(s => accom.Usage.Remove(s));
                    removeUsage.Clear();

                    // If we need to add (meaning we didn't just update an existing one) then
                    //  let's go ahead and add it
                    accom.Usage.AddIfNotExists(new AccommodationUsage()
                    {
                        StartDate = startDate,
                        EndDate = endDate,
                        Determination = status,
                        DenialReasonCode = reasonCode,
                        DenialReasonName = reasonName,
                        DenialReasonOther = reasonOther,
                        DeterminationBy = User.Current ?? User.System,
                        DeterminationDate = DateTime.UtcNow
                    });

                    // For each of our split usage records, we need to add those, these account for anything that comes
                    //  after the new or existing accommodation usage segment because that new segment split an existing
                    //  one and the existing one needs to continue on afterwards and get on with its life.
                    splitUsage.ForEach(s => accom.Usage.Add(s));

                    // Flatten out the usage, ugly I know, but we have to do it this way 'cause it's even uglier trying to prevent it
                    AccommodationUsage last = null;
                    foreach (var u in accom.Usage.OrderBy(s => s.StartDate).ThenBy(s => s.EndDate))
                    {
                        if (last != null)
                        {
                            if (last.EndDate == u.StartDate.AddDays(-1)
                                && last.Determination == u.Determination
                                && last.DenialReasonCode == u.DenialReasonCode
                                && last.DenialReasonOther == u.DenialReasonOther)
                            {
                                last.EndDate = u.EndDate;
                                removeUsage.Add(u);
                                continue;
                            }
                        }
                        last = u;
                    }
                    // Remove any unecessary usage after the flatten operation.
                    removeUsage.ForEach(s => accom.Usage.Remove(s));

                    // Box our usage using the original boundary start and end dates from when we started.
                    accom.Usage = BoxUsage(accom.Usage, boundingStart, boundingEnd);

                    // Wire up our case onSaved event handler to call the accommodation adjudicated event.
                    myCase.OnSavedNOnce((o, e) => accom.WfOnAccommodationAdjudicated(myCase, status, reasonCode, reasonOther, reasonName));
                }

                return myCase;
            }
        }//end:: ApplyDetermination

        /// <summary>
        /// Setup and Return AccommodationInteractiveProcess for specific employer
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public AccommodationInteractiveProcess GetEmployerAccommodationInteractiveProcess(string customerId, string employerId, string accomCode = null, bool isInteractiveProcess = true)
        {
            using (new InstrumentationContext("AccommodationService.GetEmployerAccommodationInteractiveProcess"))
            {
                AccommodationInteractiveProcess process = new AccommodationInteractiveProcess();

                //Get list of questions for Accommodation Process
                List<AccommodationQuestion> questions = new List<AccommodationQuestion>();

                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(AccommodationQuestion.Query.In(q => q.CustomerId, new[] { customerId, null }));
                ands.Add(AccommodationQuestion.Query.In(q => q.EmployerId, new[] { employerId, null }));
                ands.Add(AccommodationQuestion.Query.In(q => q.AccommodationTypeCode, new[] { accomCode, null }));
                ands.Add(AccommodationQuestion.Query.EQ(q => q.IsInteractiveProcessQuestion, isInteractiveProcess));
                var queryResults = AccommodationQuestion.Query.Find(Query.And(ands)).ToList();

                questions = queryResults.Where(q => q.EmployerId == employerId).OrderBy(q => q.Order).ToList();

                if (questions.Count <= 0)
                    questions = queryResults.Where(q => q.CustomerId == customerId).OrderBy(q => q.Order).ToList();

                if (questions.Count <= 0)
                    questions = queryResults.OrderBy(q => q.Order).ToList();

                //Setup our steps
                foreach (AccommodationQuestion q in questions)
                {
                    process.Steps.Add(new AccommodationInteractiveProcess.Step()
                    {
                        Question = q,
                        Answer = null,
                        CompletedDate = null,
                        Order = q.Order,
                        SAnswer = null
                    });
                }

                return process;
            }
        }

        public List<CaseNote> GetAccommIntProcCaseNotes(Guid accommId, string caseId)
        {
            using (new InstrumentationContext("AccommodationService.GetAccommIntProcCaseNotes"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();

                if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(CaseNote.Query.EQ(e => e.CaseId, caseId));

                if (accommId != null)
                    ands.Add(Query.EQ("AccommId", accommId));

                var noteQuery = CaseNote.Query.Find(ands.Count > 0 ? CaseNote.Query.And(ands) : null).OrderByDescending(n => n.CreatedDate);
                List<CaseNote> caseNotes = noteQuery.ToList();
                return caseNotes;
            }
        }

        public CaseNote GetAccommIntProcCaseNote(string questionId, string caseId)
        {
            using (new InstrumentationContext("AccommodationService.GetAccommIntProcCaseNotes"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();

                if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(CaseNote.Query.EQ(e => e.CaseId, caseId));

                if (questionId != null)
                    ands.Add(Query.EQ("AccommQuestionId", questionId));

                var noteQuery = CaseNote.Query.Find(ands.Count > 0 ? CaseNote.Query.And(ands) : null).OrderByDescending(n => n.CreatedDate);
                CaseNote caseNotes = noteQuery.FirstOrDefault();
                return caseNotes;
            }
        }

        public void CancelAccommodationRequest(Case myCase, string AccomRequestId, AccommodationCancelReason cancelReason = AccommodationCancelReason.Other, string OtherDesc = null)
        {
            using (new InstrumentationContext("AccommodationService.CancelAccommodationRequest"))
            {
                Guid id = new Guid(AccomRequestId);

                foreach (Accommodation accom in myCase.AccommodationRequest.Accommodations)
                {
                    if (accom.Id == id)
                    {
                        accom.Status = CaseStatus.Cancelled;
                        accom.CancelReason = cancelReason;
                        if (cancelReason == AccommodationCancelReason.Other)
                            accom.OtherReasonDesc = OtherDesc;
                        myCase.Save();
                        accom.WfOnAccommodationDeleted(myCase);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Closes the case accommodation as per the provided accommodation id
        /// </summary>
        /// <param name="myCase"></param>
        /// <param name="AccommodationRequestId"></param>
        /// <param name="AccommodationId"></param>
        public void CloseCaseAccommodation(Case myCase, string AccommodationRequestId, string AccommodationId)
        {
            using (new InstrumentationContext("AccommodationService.CloseAccommodation"))
            {
                Guid id = new Guid(AccommodationId);

                foreach (Accommodation accom in myCase.AccommodationRequest.Accommodations)
                {
                    if (accom.Id == id)
                    {
                        accom.Status = CaseStatus.Closed;                        
                        myCase.Save();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Get List of Accommodation Types for employer, customer or root/core.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns>A querable interface for pulling accommodation types accordingly.</returns>
        public IQueryable<AccommodationType> GetBaseAccommodationTypes(string customerId = null, string employerId = null)
        {
            using (new InstrumentationContext("AccommodationService.GetFlatAccommodationTypes"))
            {
                customerId = customerId ?? CustomerId ?? CurrentUser.CustomerId;
                employerId = employerId ?? EmployerId;
                // Pass in the employerId if known (employer context), but if it happens to be null we also want ALL employers included, 
                //  so pass the includeAllEmployers flag as TRUE as well here:
                var accommodationTypes = AccommodationType.DistinctFind(null, customerId, employerId, includeAllEmployers: true).AsQueryable().Where(a => !a.PreventNew);
                if (CurrentCustomer != null)
                    accommodationTypes = accommodationTypes.Where(at => !CurrentCustomer.SuppressAccommodationTypes.Contains(at.Code, StringComparer.InvariantCultureIgnoreCase));

                if (CurrentEmployer != null)
                    accommodationTypes = accommodationTypes.Where(at => !CurrentEmployer.SuppressAccommodationTypes.Contains(at.Code, StringComparer.InvariantCultureIgnoreCase));

                return accommodationTypes.OrderBy(r => r.Name);
            }
        }

        /// <summary>
        /// Get List of Accommodation Types for employer, customer or root/core.
        /// </summary>
        /// <returns></returns>
        public List<AccommodationType> GetAccommodationTypes(string customerId = null, string employerId = null)
        {
            using (new InstrumentationContext("AccommodationService.GetAccommodationTypes"))
            {
                var accommodationTypes = GetBaseAccommodationTypes(customerId, employerId);
                List<AccommodationType> flatCategories = accommodationTypes.Where(a => a.Categories == null || a.Categories.Count == 0)
                    .Select(a => new AccommodationType()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ParentName = null,
                        Code = a.Code,
                        Order = a.Order,
                        Duration = a.Duration,
                        Categories = null
                    }).ToList();

                if (!((CurrentCustomer?.HasFeature(Feature.AccommodationTypeCategories)) ?? true))
                {
                    return flatCategories;
                }

                foreach (var accommodationType in accommodationTypes.OrderBy(n => n.Order).Where(a => a.Categories != null && a.Categories.Count > 0))
                {
                    for (int i = 0; i < accommodationType.Categories.Count; i++)
                    {
                        string parentName = null;
                        string name = accommodationType.Categories[i];
                        if (i > 0)
                            parentName = accommodationType.Categories[i - 1];

                        // Doesn't exist, add it to the list
                        if (flatCategories.FirstOrDefault(fc => fc.ParentName == parentName && fc.Name == name) == null)
                        {
                            flatCategories.Add(new AccommodationType()
                            {
                                Id = accommodationType.Id,
                                ParentName = parentName,
                                Order = accommodationType.Order,
                                Duration = accommodationType.Duration,
                                Name = name
                            });
                        }
                    }

                    // Add the actual accommodation type as a child of the last category
                    flatCategories.Add(new AccommodationType()
                    {
                        Id = accommodationType.Id,
                        ParentName = accommodationType.Categories.Last().Value,
                        Name = accommodationType.Name,
                        Order = accommodationType.Order,
                        Duration = accommodationType.Duration,
                        Code = accommodationType.Code
                    });
                }

                // Now to turn it into an actual tree
                List<AccommodationType> rootCategories = GetAccommodationTypeCategories(null, flatCategories);

                foreach (var category in rootCategories)
                {
                    category.Children = GetAccommodationTypeCategories(category, flatCategories);
                }
                return rootCategories;
            }
        }
        private List<AccommodationType> GetAccommodationTypeCategories(AccommodationType category, List<AccommodationType> categories)
        {
            string parentName = null;
            if (category != null)
                parentName = category.Name;

            List<AccommodationType> childCategories = categories.Where(c => c.ParentName == parentName).OrderBy(q => q.Order).ToList();
            foreach (var cat in childCategories)
            {
                cat.Children = GetAccommodationTypeCategories(cat, categories);
            }

            return childCategories;
        }
        public ListResults AccommodationTypeList(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults result = new ListResults(criteria);
            string name = criteria.Get<string>("Name");
            string code = criteria.Get<string>("Code");

            List<IMongoQuery> ands = AccommodationType.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            AccommodationType.Query.MatchesString(at => at.Name, name, ands);
            AccommodationType.Query.MatchesString(at => at.Code, code, ands);
            List<AccommodationType> types = AccommodationType.DistinctAggregation(ands);
            result.Total = types.Count;
            types = types
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = types.Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("CustomerId", c.CustomerId)
                .Set("EmployerId", c.EmployerId)
                .Set("Name", c.Name)
                .Set("Code", c.Code)
                .Set("Disabled", IsAccommodationTypeDisabledInCurrentContext(c.Code))
                .Set("ModifiedDate", c.ModifiedDate)
            );

            return result;
        }

        private bool IsAccommodationTypeDisabledInCurrentContext(string code)
        {
            bool returnValue = false;
            if (CurrentEmployer != null)
            {
                returnValue = CurrentEmployer.SuppressAccommodationTypes.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            if (CurrentCustomer != null && !returnValue)
            {
                returnValue = CurrentCustomer.SuppressAccommodationTypes.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            return returnValue;
        }

        public AccommodationType GetAccommodationTypeById(string id)
        {
            return AccommodationType.GetById(id);
        }

        public void DeleteAccommodationType(AccommodationType type)
        {
            if (!type.IsCustom)
                return;

            type.Delete();
        }

        public AccommodationType ToggleAccommodationTypeByCode(string code)
        {
            using (new InstrumentationContext("PolicyService.ToggleAbsenceReasonByCode"))
            {
                AccommodationType accommodationType = AccommodationType.GetByCode(code, CustomerId, EmployerId, true);
                if (accommodationType.IsCustom)
                {
                    accommodationType.IsDisabled = false;
                    return accommodationType;
                }

                if (CurrentEmployer != null)
                {
                    return ToggleAccommodationTypeForEmployer(accommodationType);
                }

                if (CurrentCustomer != null)
                {
                    return ToggleAccommodationTypeForCustomer(accommodationType);
                }

                /// This should not be reachable, as we should always have a customer or employer
                /// But just in case it is, we're going to return the absence reason as is
                return accommodationType;
            }
        }

        private AccommodationType ToggleAccommodationTypeForEmployer(AccommodationType accommodationType)
        {
            if (CurrentEmployer.SuppressAccommodationTypes.Contains(accommodationType.Code, StringComparer.InvariantCultureIgnoreCase))
            {
                CurrentEmployer.SuppressAccommodationTypes.RemoveAll(w => string.Equals(w, accommodationType.Code, StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                CurrentEmployer.SuppressAccommodationTypes.Add(accommodationType.Code.ToUpperInvariant());
            }

            CurrentEmployer.Save();
            return accommodationType;
        }

        private AccommodationType ToggleAccommodationTypeForCustomer(AccommodationType accommodationType)
        {
            if (CurrentCustomer.SuppressAccommodationTypes.Contains(accommodationType.Code, StringComparer.InvariantCultureIgnoreCase))
            {
                CurrentCustomer.SuppressAccommodationTypes.RemoveAll(w => string.Equals(w, accommodationType.Code, StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                CurrentCustomer.SuppressAccommodationTypes.Add(accommodationType.Code.ToUpperInvariant());
            }

            CurrentCustomer.Save();
            return accommodationType;
        }

        public AccommodationType SaveAccommodationType(AccommodationType type)
        {
            using (new InstrumentationContext("AccommodationService.SaveAccommodationType"))
            {
                AccommodationType savedType = AccommodationType.GetById(type.Id);
                if (savedType != null &&
                    (!savedType.IsCustom || savedType.EmployerId != EmployerId))
                {
                    type.Clean();
                }

                type.CustomerId = CustomerId;
                type.EmployerId = EmployerId;

                return type.Save();
            }
        }

    }//end: AccommodationService
}//end: namespace
