﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Rules;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// The Eligibility service provides methods for policy selection, eligibility calculation,
    /// usage, work schedule application to absence calcs, changing case dates, type and reason. 
    /// This service is the backbone to the entire application as it controls the actual “calcs” 
    /// that comprise the majority of the intellectual property of absenceSoft.
    /// </summary>
    public partial class EligibilityService : LogicService, ILogicService, IEligibilityService
    {
        public EligibilityService()
        {
        }
        public EligibilityService(Employer CurrentEmployer)
        {
            this.CurrentEmployer = CurrentEmployer;
        }


        /// <summary>
        /// Gets a leave of absence object that contains all of the data necessary for evaluating rules, sending communications, etc.
        /// and provides the basis for tokenization and expression evaluation in the rules engine :-).
        /// </summary>
        /// <param name="caseId">The case Id for the leave of absence case that is the primary case we're evaluating.</param>
        /// <returns></returns>
        public LeaveOfAbsence GetLeaveOfAbsence(string caseId)
        {
            using (new InstrumentationContext("EligibilityService.GetLeaveOfAbsence"))
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    return null;

                Case myCase = Case.GetById(caseId);
                if (myCase == null)
                    return null;

                LeaveOfAbsence leave = new LeaveOfAbsence() { Case = myCase };
                leave.ActiveSegment = myCase.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault();
                leave.CaseHistory = Case.AsQueryable().Where(c => c.Id != caseId && c.Employee.Id == myCase.Employee.Id).ToList();
                leave.Employee = leave.Case.Employee;
                leave.Employer = leave.Employee.Employer;
                leave.Customer = leave.Employer.Customer;
                leave.Communications = Communication.AsQueryable().Where(c => c.CaseId == caseId).ToList();
                leave.Tasks = Data.ToDo.ToDoItem.AsQueryable().Where(t => t.CaseId == caseId).ToList();
                leave.WorkSchedule = Employee.GetById(myCase.Employee.Id).WorkSchedules;

                return leave;
            }
        }//GetLeaveOfAbsence

        /// <summary>
        /// Gets a leave of absence object that contains all of the data necessary for evaluating rules, sending communications, etc.
        /// and provides the basis for tokenization and expression evaluation in the rules engine :-).
        /// </summary>
        /// <param name="myCase">The case for the leave of absence object to be populated for.</param>
        /// <returns></returns>
        public LeaveOfAbsence GetLeaveOfAbsence(Case myCase)
        {
            using (new InstrumentationContext("EligibilityService.GetLeaveOfAbsence"))
            {
                if (myCase == null)
                    return null;

                LeaveOfAbsence leave = new LeaveOfAbsence() { Case = myCase };
                leave.ActiveSegment = myCase.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault();
                leave.Employee = leave.Case.Employee;
                leave.Employer = leave.Employee.Employer;
                leave.Customer = leave.Employer.Customer;
                if (!string.IsNullOrWhiteSpace(myCase.Id))
                {
                    leave.CaseHistory = Case.AsQueryable().Where(c => c.Id != myCase.Id && c.Employee.Id == myCase.Employee.Id).ToList();
                    leave.Communications = Communication.AsQueryable().Where(c => c.CaseId == myCase.Id).ToList();
                    leave.Tasks = Data.ToDo.ToDoItem.AsQueryable().Where(t => t.CaseId == myCase.Id).ToList();
                }
                else
                    leave.CaseHistory = Case.AsQueryable().Where(c => c.Employee.Id == myCase.Employee.Id).ToList();
                leave.WorkSchedule = Employee.GetById(myCase.Employee.Id).WorkSchedules;

                return leave;
            }
        }

        /// <summary>
        /// Runs policy selection if necesssary as well as runs eligibility against
        /// all non-manual policies on the case and returns the Leave Of Absence object
        /// fully populated. This method does NOT persist the case.
        /// </summary>
        /// <param name="myCase">The case that should have policy selection and eligibilty ran for it.</param>
        /// <returns>The fully populated Leave Of Absence object for this case.</returns>
        public LeaveOfAbsence RunEligibility(Case myCase)
        {
            using (new InstrumentationContext("EligibilityService.RunEligibility"))
            {
                /*
                 * 1. Get the case from Id
                 * 2. Perform plan selection
                 * 3. Perform eligibility
                 */
                if (myCase == null)
                    throw new ArgumentNullException("myCase");

                LeaveOfAbsence loa = GetLeaveOfAbsence(myCase);
                if (myCase.Status != CaseStatus.Inquiry)
                {
                    PerformPolicySelection(loa);
                    PerformEligibility(loa);
                    EvaluateTimePayAndCrosswalkRules(loa);
                    if (loa.Case.Employee.WorkSchedules != null && loa.Case.Employee.WorkSchedules.Count > 0)
                    {
                        loa.Case = new CaseService().Using(c => c.RunCalcs(loa.Case));
                        // If any policy comes out to be ineligibe, then, look for substitute policy that can be used in place of ineligible policy.
                        if(SelectSubstitutePolicy(loa))
                        {
                            RunEligibility(myCase);
                        }                       
                    }
                }

                return loa;
            }
        }

        /// <summary>
        /// Evaluates the time, pay, and crosswalk rule groups.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="appliedPolicy">The applied policy.</param>
        protected void EvaluateTimePayAndCrosswalkRules(LeaveOfAbsence loa, AppliedPolicy appliedPolicy = null)
        {
            if (appliedPolicy != null && (appliedPolicy.RuleGroups == null || !appliedPolicy.RuleGroups.Any()))
            {
                List<PolicyRuleGroup> ruleGroups = GetRuleGroups(appliedPolicy.Policy, loa);
                List<AppliedRuleGroup> appliedGroups = GetAppliedRuleGroups(ruleGroups);

                // Ensure our selection & eligibility rules don't get copied over
                appliedPolicy.RuleGroups = appliedGroups.ToList(r => r.RuleGroup.RuleGroupType != PolicyRuleGroupType.Selection
                    && r.RuleGroup.RuleGroupType != PolicyRuleGroupType.Eligibility);
            }
            PerformEligibility_TimeRuleGroups(loa, appliedPolicy);
            PerformEligibility_STDPayment(loa, appliedPolicy);
            PerformEligibility_Crosswalk(loa, appliedPolicy);
        }

        /// <summary>
        /// If any of the applied policy(s) in the Case are inelligible, then search for policy(s) which are configured to act as substitute policy
        /// for the ineligible policy in the case. If found, then add the substitute policy to the case. 
        /// </summary>
        /// <param name="loa"></param>
        /// <returns></returns>
        private bool SelectSubstitutePolicy(LeaveOfAbsence loa)
        {
            bool returnValue = false;
            var primaryPolicies = new List<AppliedPolicy>(loa.ActiveSegment.AppliedPolicies.Where(p => p.Status == EligibilityStatus.Ineligible).ToList());

            if (!primaryPolicies.Any())
            {
                //If substitute policy was added earlier against an ineligible policy, and now user had overridden rules to make that policy eligible.
                //then substitute policy will need to be removed as it is supposed to be used for ineligible policy only. 
                loa.ActiveSegment.AppliedPolicies.RemoveAll(p => p.Metadata.Contains("SusbstituteFor"));
                return returnValue;
            }
            //primary policy => is ineligible policy in Case, for which a substitute policy need to be searched and added.
            foreach (var primaryPolicy in primaryPolicies)
            {
                //1. Pre - Condition: Policy is not eligible
                List<Policy> substitutePolicies = Policy.AsQueryable().Where(p => p.CustomerId == null || p.CustomerId == loa.Employee.CustomerId)
                    .Where(p => p.EmployerId == null || p.EmployerId == loa.Employee.EmployerId)
                    .Where(p => p.EffectiveDate <= loa.Case.StartDate)
                    .ToList();

                //If the substitute policy is already present in the case then there is no need to add it again.
                substitutePolicies.RemoveAll(p => loa.ActiveSegment.AppliedPolicies.Any(a => a.Policy.Code == p.Code));

                //2. Inspect Policy Absence Reason applied to that Case, determine if any Substitute For is configured (absence reason level, and only that specific absence reason) 
                //Yes: Use that, done.
                returnValue = AddSubstitutePolicy(loa, substitutePolicies.Where(p => p.AbsenceReasons.Any(r => r.ReasonCode == loa.Case.Reason.Code
                                                                                            && r.CaseTypes.HasFlag(loa.Case.Segments.First().Type)
                                                                                            && r.SubstituteFor != null
                                                                                            && r.SubstituteFor.Contains(primaryPolicy.Policy.Code))).ToList(), primaryPolicy);
                // No: Go to step 3.
                if (!returnValue)
                {
                    //3. Inspect Policy applied to that Case that is not eligible, determine if any Substitute For is configured (policy level) 
                    //Yes: Use that
                    returnValue = AddSubstitutePolicy(loa, substitutePolicies.Where(p => p.SubstituteFor != null && p.SubstituteFor.Contains(primaryPolicy.Policy.Code)).ToList(), primaryPolicy);                    
                }
            }
            return returnValue;
        }

        /// <summary>
        /// loop through all selected substitute policies and add it to the case
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="substitutePolicies"></param>
        /// <param name="primaryPolicy"></param>
        /// <returns></returns>
        private bool AddSubstitutePolicy(LeaveOfAbsence loa, List<Policy> substitutePolicies, AppliedPolicy primaryPolicy)
        {
            bool returnValue = false;
            foreach (var policy in substitutePolicies)
            {
                // add the policy to the applied policy group to get ready for eligibility
                var policyReason = policy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == loa.Case.Reason.Code);
                if (policyReason != null)
                {
                    AppliedPolicy aPolicy = new AppliedPolicy();
                    aPolicy.StartDate = loa.ActiveSegment.StartDate;
                    aPolicy.EndDate = loa.ActiveSegment.EndDate;
                    aPolicy.Policy = policy;
                    aPolicy.PolicyReason = policyReason;
                    aPolicy.Status = EligibilityStatus.Pending;
                    //Make use of Metadata to identify the policy as substitute policy. This will be used in avoiding duplicates and showing message in UI
                    aPolicy.Metadata.SetRawValue("SusbstituteFor", primaryPolicy.Policy.Code);
                    aPolicy.Policy.AbsenceReasons.Where(a => !a.PeriodType.HasValue).ForEach(a => a.PeriodType = loa.Employee.Employer.FMLPeriodType);

                    loa.ActiveSegment.AppliedPolicies.Add(aPolicy);
                    EvaluateTimePayAndCrosswalkRules(loa, aPolicy);
                    returnValue = true;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Gets a list of all available policies that may be manually added to an absence.
        /// </summary>
        /// <param name="myCase">The case that the policies would possibly be applied to.</param>
        /// <returns>A list of policies with only the name, description, id and type populated.</returns>
        public IEnumerable<Policy> GetAvailableManualPolicies(Case myCase)
        {
            var existingPolicies = myCase.Segments.SelectMany(s => s.AppliedPolicies.Select(p => p.Policy.Code));

            List<IMongoQuery> ands = Policy.DistinctAnds(CurrentUser, CustomerId, myCase.EmployerId);
            ands.Add(Policy.Query.NotIn(p => p.Code, existingPolicies));
            ands.Add(Policy.Query.LTE(p => p.EffectiveDate, DateTime.UtcNow.ToMidnight()));
            var results = Policy.DistinctAggregation(ands);
            if (myCase.Status != CaseStatus.Inquiry && myCase.Reason != null)
            {
                results = results.Where(p => !CurrentEmployer.SuppressPolicies.Contains(p.Code) && p.AbsenceReasons.Select(abs => abs.ReasonCode).Contains(myCase.Reason.Code))
                    .Where(p => p.AirlineFlightCrew == null || p.AirlineFlightCrew == (myCase.Employee.IsFlightCrew ?? false))
                    .Where(p => p.AbsenceReasons.Any(r => r.ReasonCode == myCase.Reason.Code && (r.AirlineFlightCrew == null || r.AirlineFlightCrew == (myCase.Employee.IsFlightCrew ?? false))))
                    .ToList();

                //Check if Policy is supported against Segment type. There may be scenario where a policy will be suitable to be added as manual policy in 
                // case based on absence reason code, but we have to check supported segment type as well. For example FMLA and FMLA-FC both have
                //similer absence reason code but FMLA-FC policy is not supposed to be added in Case having REDUCED type of segment.
                results = results.Where(p => myCase.Segments.Any(s => p.AbsenceReasons.Any(r => r.ReasonCode == myCase.Reason.Code && r.CaseTypes.HasFlag(s.Type)))).ToList();
            }
            return results.OrderBy(p => p.Name);
        }

        /// <summary>
        /// Adds a manual policy to the each case segment. Does NOT persist changes, returns
        /// ready for calcs on the newly added policy, if any.
        /// </summary>
        /// <param name="myCase">The case to modify</param>
        /// <param name="policyCode">The policy code of the policy to add</param>
        /// <param name="notes">The notes, if any, to set to the overriden policy addition</param>
        /// <returns>The populated case with the newly added and applied policy ready for calcs</returns>
        public Case AddManualPolicy(Case myCase, string policyCode, string notes)
        {
            if (myCase == null)
                throw new ArgumentNullException("myCase");
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");
            var policy = Policy.GetByCode(policyCode, myCase.CustomerId, myCase.EmployerId);
            if (policy == null)
                throw new AbsenceSoftException("Policy was not found");

            return AddManualPolicy(myCase, policy, notes);
        }

        /// <summary>
        /// Adds a manual policy to the each case segment. Does NOT persist changes, returns
        /// ready for calcs on the newly added policy, if any.
        /// </summary>
        /// <param name="myCase"></param>
        /// <param name="manualPolicy"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public Case AddManualPolicy(Case myCase, Policy manualPolicy, string notes)
        {
            if (myCase == null)
                throw new ArgumentNullException("myCase");
            if (manualPolicy == null)
                throw new ArgumentNullException("manualPolicy");

            // If the policy already exists, just bail, no need to re-add it, and not really exception material here.
            if (myCase.Segments.SelectMany(s => s.AppliedPolicies.Select(p => p.Policy.Code)).Any(p => p == manualPolicy.Code))
                return myCase;

            AppliedPolicy refPolicy = null;

            // add the policy to the applied policy group to get ready for eligibility
            string absenceReasonCode = myCase.Reason.Code;
            PolicyAbsenceReason reason = manualPolicy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == absenceReasonCode);
            if (reason == null)
            {
                // This is bad, policy reason should never be null, that means it's a configuration issue and not allowed.
                throw new ArgumentException(
                    $"The policy '{manualPolicy.Name} ({manualPolicy.Code})' is not configured to support the absence reason, '{myCase.Reason.Name} ({absenceReasonCode})'.", 
                    "manualPolicy");
            }

            //Get the current segments for any Case / Relapse
            List<CaseSegment> currentSegments = null;
            if (myCase.HasRelapse())
            {
                Relapse currentRelapse = Relapse.AsQueryable().Where(r => r.Id == myCase.CurrentRelapseId).FirstOrDefault();
                currentSegments = myCase.Segments.Where(s => s.StartDate >= currentRelapse.StartDate).ToList();
            }
            else
            {
                currentSegments = myCase.Segments;
            }

            var loa = GetLeaveOfAbsence(myCase);
             
            foreach (CaseSegment segment in currentSegments)
            {
                //the drop down for manual policy list can have policies based on all segment types in the case.
                //There can be a situation where a policy in the dropdown is eligible for one (or more) segment but not 
                //for all segments in the case, based on the absence reason in the policy. So we need to validate the supported segment 
                //type(s) in the policy absence reason.
                if (reason.CaseTypes.HasFlag(segment.Type))
                {
                    AppliedPolicy aPolicy = new AppliedPolicy();
                    aPolicy.StartDate = segment.StartDate;
                    aPolicy.EndDate = segment.EndDate.Value;
                    aPolicy.Policy = manualPolicy;
                    aPolicy.PolicyReason = reason;
                    aPolicy.PolicyReason.PeriodType = aPolicy.PolicyReason.PeriodType ?? myCase.Employer.FMLPeriodType;
                    aPolicy.Status = EligibilityStatus.Eligible;
                    aPolicy.ManuallyAdded = true;
                    aPolicy.ManuallyAddedNote = notes;
                    segment.AppliedPolicies.Add(aPolicy);
                    EvaluateTimePayAndCrosswalkRules(loa, aPolicy);
                    refPolicy = refPolicy ?? aPolicy;
                }
            }

            if (refPolicy != null)
                myCase.OnSavedNOnce((s, c) => refPolicy.WfOnPolicyManuallyAdded(c.Entity));

            return myCase;
        }

        /// <summary>
        /// Removes a previously added manual policy from the case based on the policy Id from all case segments.
        /// This does NOT persist the case changes, but makes it ready to re-run calcs again.
        /// </summary>
        /// <param name="myCase">The case to remove the manually added policy from</param>
        /// <param name="policyCode">The code of the policy that was previously manually added to now remove from the case</param>
        /// <returns>The modified case.</returns>
        public Case RemoveManualPolicy(Case myCase, string policyCode)
        {
            if (myCase == null)
                throw new ArgumentNullException("myCase");
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");

            AppliedPolicy refPolicy = null;

            myCase.Segments.ForEach(s =>
            {
                refPolicy = refPolicy ?? s.AppliedPolicies.FirstOrDefault(p => p.ManuallyAdded && p.Policy.Code == policyCode);
                s.AppliedPolicies.RemoveAll(p => p.ManuallyAdded && p.Policy.Code == policyCode);
            });

            if (refPolicy != null)
                myCase.OnSavedNOnce((s, c) => refPolicy.WfOnPolicyManuallyDeleted(c.Entity));

            return myCase;
        }

        /// <summary>
        /// Performs the policy selection process by evaluating policy selection rules for each policy that has them
        /// and applying them to the absence when applicable (passes selection criteria, if any).
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        protected void PerformPolicySelection(LeaveOfAbsence loa)
        {
            using (new InstrumentationContext("EligibilityService.PerformPolicySelection"))
            {
                // If there are already policies applied, jump out, we can't run this again.
                if (loa.ActiveSegment == null || loa.ActiveSegment.AppliedPolicies != null && loa.ActiveSegment.AppliedPolicies.Any())
                    return;

                // Read all global and employer specific policies that are effective as of the start date of our active segment.
                List<Policy> policies = PerformEmployeePolicySelection(loa.Employee, loa.Case.StartDate, loa.Case.Reason.Code)
                    .Where(p => !(loa.Employer.SuppressPolicies ?? new List<string>(0)).Contains(p.Code)).ToList();

                // foreach policy, get the global rules, and then the policy reason id rules, apply them as a single list
                foreach (var policy in policies.Where(p => p.AbsenceReasons.Any(r => r.ReasonCode == loa.Case.Reason.Code && r.CaseTypes.HasFlag(loa.Case.Segments.First().Type))))
                {
                    List<PolicyRuleGroup> ruleGroups = GetRuleGroups(policy, loa);
                    List<AppliedRuleGroup> appliedGroups = GetAppliedRuleGroups(ruleGroups);
                    EvaluateRuleGroups(loa, appliedGroups, PolicyRuleGroupType.Selection);
                    if (appliedGroups.Where(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Selection).All(g => g.Pass))
                    {
                        // add the policy to the applied policy group to get ready for eligibility
                        AppliedPolicy aPolicy = new AppliedPolicy();
                        aPolicy.StartDate = loa.ActiveSegment.StartDate;
                        aPolicy.EndDate = loa.ActiveSegment.EndDate;
                        aPolicy.Policy = policy;
                        aPolicy.PolicyReason = policy.AbsenceReasons.First(a => a.ReasonCode == loa.Case.Reason.Code);
                        aPolicy.Status = EligibilityStatus.Pending;
                        // Ensure our selection rules don't get copied over, that would be bad; they shouldn't show in the UI.
                        aPolicy.RuleGroups = appliedGroups.ToList(r => r.RuleGroup.RuleGroupType != PolicyRuleGroupType.Selection);
                        // Save our selection rules so we have proof of why we selected 'n' stuff
                        aPolicy.Selection = appliedGroups.ToList(r => r.RuleGroup.RuleGroupType == PolicyRuleGroupType.Selection);
                        foreach (var r in aPolicy.Policy.AbsenceReasons)
                            if (!r.PeriodType.HasValue)
                                r.PeriodType = loa.Employee.Employer.FMLPeriodType;

                        loa.ActiveSegment.AppliedPolicies.Add(aPolicy);
                    }
                }
            }
        }

        public string GetWhySelected(Case myCase, AppliedPolicy policy)
        {
            using (new InstrumentationContext("EligibilityService.GetWhySelected"))
            {
                if (myCase == null || policy == null || policy.Status != EligibilityStatus.Eligible)
                    return null;
                if (policy.ManuallyAdded)
                    return "Policy was manually added.";

                List<string> reasons = new List<string>();
                var emp = myCase.Employee;
                if (emp != null)
                {
                    // See if any of our short-cut selection criteria match.
                    var gender = emp.Gender;
                    var state = emp.WorkState;
                    var resState = emp.Info != null ? emp.Info.Address != null ? emp.Info.Address.State : null : null;

                    if (gender.HasValue)
                    {
                        if (gender == policy.PolicyReason.Gender)
                            reasons.AddIfNotExists(string.Format("employee is {0}", gender));
                        if (gender == policy.Policy.Gender)
                            reasons.AddIfNotExists(string.Format("employee is {0}", gender));
                    }

                    if (!string.IsNullOrWhiteSpace(state))
                    {
                        if (state == policy.PolicyReason.WorkState)
                            reasons.AddIfNotExists(string.Format("employee works in the state of '{0}'", state));
                        if (state == policy.Policy.WorkState)
                            reasons.AddIfNotExists(string.Format("employee works in the state of '{0}'", state));
                    }

                    if (!string.IsNullOrWhiteSpace(resState))
                    {
                        if (resState == policy.PolicyReason.ResidenceState)
                            reasons.AddIfNotExists(string.Format("employee resides in the state of '{0}'", resState));
                        if (resState == policy.Policy.ResidenceState)
                            reasons.AddIfNotExists(string.Format("employee resides in the state of '{0}'", resState));
                    }
                }
                if (policy.Selection != null)
                {
                    var passingRules = policy.Selection.Where(s => s.Pass && s.Rules.Any(r => r.Pass)).Select(s => s.Rules.First(r => r.Pass)).ToList();
                    foreach (var rg in passingRules)
                    {
                        reasons.AddIfNotExists(rg.Rule.Description);
                    }
                }

                if (reasons.Count > 1)
                {
                    reasons.Add(string.Concat("and ", reasons.Last()));
                    reasons.RemoveAt(reasons.Count - 2);
                }

                if (reasons.Any())
                    return string.Concat("Employee is eligible because ", string.Join(", ", reasons), ".");

                return "All employees are eligible for this policy.";
            }
        }

        public List<Policy> PerformEmployeePolicySelection(Employee emp, DateTime policyDate, string reasonCode = null)
        {
            using (new InstrumentationContext("EligibilityService.PerformEmployeePolicySelection"))
            {
                // Read all global and employer specific policies that are effective as of the start date of our active segment.
                List<Policy> policies = Policy.AsQueryable().Where(p => p.CustomerId == null || p.CustomerId == emp.CustomerId)
                    .Where(p => p.EmployerId == null || p.EmployerId == emp.EmployerId)
                    .Where(p => p.EffectiveDate <= policyDate)
                    .ToList()
                    .OrderBy(p => p.EmployerId == null ? 999 : 1)
                    .Distinct(p => p.Code)
                    .ToList();

                return policies.Where(p => PolicyMeetsShortcutCriteriaSelection(p, emp, reasonCode)).ToList();
            }
        }

        private bool PolicyMeetsShortcutCriteriaSelection(Policy policy, Employee emp, string reasonCode = null)
        {

            var validatePolicyCountryState = new Func<string, string, bool>((policyCountryStateCode, employeeCountryStateCode) =>
            {
                if (string.IsNullOrEmpty(policyCountryStateCode) || string.IsNullOrEmpty(employeeCountryStateCode))
                {
                    return false;
                }

                if (policyCountryStateCode.ToLowerInvariant() != employeeCountryStateCode.ToLowerInvariant())
                {
                    return true;
                }
                return false;
            });

            // The value of "AirlineFlightCrew" property for "FMLA = false", "FMLA-FC = true" and is null for rest all policies.
            // If "AirlineFlightCrew" property of policy is set, then we have to check, whether this policy can be used for current Employee or not.
            // e.g. Policies "FMLA" and "FMLA-FC" are mutually exclusive. "FMLA-FC" is to be used only for Flight Crew employees
            bool policyConflictWithEmp = policy.AirlineFlightCrew.HasValue && policy.AirlineFlightCrew != (emp.IsFlightCrew ?? false)
                && (String.IsNullOrEmpty(reasonCode) || policy.AbsenceReasons.Any(a => a.Reason.Code == reasonCode && a.AirlineFlightCrew  != (emp.IsFlightCrew ?? false)));
            if (policyConflictWithEmp)
            {
                return false;
            }

            Gender? gender = emp.Gender;

            string workState = null;
            if (!string.IsNullOrEmpty(emp.WorkState))
                workState = emp.WorkState.ToLowerInvariant();

            string workCountry = emp.WorkCountry;
            string residenceState = null;
            string residenceCountry = null;
            if (emp.Info != null && emp.Info.Address != null)
            {
                if (!string.IsNullOrEmpty(emp.Info.Address.State))
                    residenceState = emp.Info.Address.State.ToLowerInvariant();

                residenceCountry = emp.Info.Address.Country;
            }

            if (validatePolicyCountryState(policy.WorkCountry, workCountry) && validatePolicyCountryState(policy.ResidenceCountry, residenceCountry))
            {
                return false;
            }

            if (validatePolicyCountryState(policy.WorkState, workState) || validatePolicyCountryState(policy.ResidenceState, residenceState))
            {
                return false;
            }



            if (policy.Gender.HasValue && policy.Gender != gender)
                return false;

            bool anyAbsenceReasonMatchesCriteria = false;
            foreach (var absenceReason in policy.AbsenceReasons)
            {
                if (!string.IsNullOrEmpty(reasonCode) && reasonCode != absenceReason.ReasonCode)
                    continue;

                if (absenceReason.Gender.HasValue && absenceReason.Gender != gender)
                    continue;

                if (validatePolicyCountryState(policy.WorkCountry, workCountry) && validatePolicyCountryState(policy.ResidenceCountry, residenceCountry))
                {
                    continue;
                }

                if (validatePolicyCountryState(policy.WorkState, workState) || validatePolicyCountryState(policy.ResidenceState, residenceState))
                {
                    continue;
                }

                /// if we get all the way down here, all the checks have succeeded, so we know this particular absence reason matches
                /// set the bool to true and stop looping
                anyAbsenceReasonMatchesCriteria = true;
                break;
            }

            if (!anyAbsenceReasonMatchesCriteria)
                return false;

            return true;
        }

        /// <summary>
        /// Performs the primary eligibility process against a leave of absence, evaluating each policy in the
        /// case and applies the eligibility status baseed on the rule group evaluations.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        protected void PerformEligibility(LeaveOfAbsence loa)
        {
            using (new InstrumentationContext("EligibilityService.PerformEligibility"))
            {
                // Enumerate our applied policies for eligibility (already placed on the active segment)
                foreach (var policy in loa.ActiveSegment.AppliedPolicies.Where(p => !p.ManuallyAdded))
                {
                    EvaluateRuleGroups(loa, policy.RuleGroups, PolicyRuleGroupType.Eligibility);
                    if (policy.RuleGroups.Where(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Eligibility).All(g => g.Pass))
                        policy.Status = EligibilityStatus.Eligible;
                    else
                        policy.Status = EligibilityStatus.Ineligible;
                }
            }
        }

        /// <summary>
        /// Gets the applied policies for running rules.
        /// </summary>
        /// <param name="loa">The loa.</param>
        /// <param name="appliedPolicy">The applied policy.</param>
        /// <returns></returns>
        protected IEnumerable<AppliedPolicy> GetAppliedPolicies(LeaveOfAbsence loa, AppliedPolicy appliedPolicy = null)
        {
            if (appliedPolicy != null)
            {
                yield return appliedPolicy;
                yield break;
            }

            foreach (var policy in loa.ActiveSegment.AppliedPolicies)
            {
                yield return policy;
            }
        }

        /// <summary>
        /// Performs the secondary eligibility process for RuleGroupsTypes that equal 'Time'.
        /// Entitlement is updated if the rule group passes and the entitlement is
        /// greater than the current.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="ap">The ap.</param>
        protected void PerformEligibility_TimeRuleGroups(LeaveOfAbsence loa, AppliedPolicy appliedPolicy = null)
        {
            using (new InstrumentationContext("EligibilityService.PerformEligibility_TimeRuleGroupsPerformEligibility_TimeRuleGroups"))
            {
                foreach (var policy in GetAppliedPolicies(loa, appliedPolicy))
                {
                    EvaluateRuleGroups(loa, policy.RuleGroups, PolicyRuleGroupType.Time);
                    //get all passing rule groups of type 'Time'
                    var passingTimeRuleGroups = policy.RuleGroups.Where(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Time && g.Pass);
                    //update entitlement
                    if (passingTimeRuleGroups.Any())
                    {
                        var max = passingTimeRuleGroups.OrderByDescending(r => r.RuleGroup.Entitlement).First();
                        if (max.RuleGroup.Entitlement.HasValue && max.RuleGroup.Entitlement.Value > policy.PolicyReason.Entitlement)
                        {
                            policy.PolicyReason.Entitlement = max.RuleGroup.Entitlement.Value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Performs the STD or Paid Leave payment tier override rule groups where RuleGroupType is 'Pay'.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="appliedPolicy">The applied policy.</param>
        protected void PerformEligibility_STDPayment(LeaveOfAbsence loa, AppliedPolicy appliedPolicy = null)
        {
            using (new InstrumentationContext("EligibilityService.PerformEligibility_STDPayment"))
            {
                foreach (var policy in GetAppliedPolicies(loa, appliedPolicy))
                {
                    EvaluateRuleGroups(loa, policy.RuleGroups, PolicyRuleGroupType.Payment);
                    //get all passing rule groups of type 'Payment'
                    // if any rules passed, then see if any of them set the entitlement to be more than the current policy amount and if so, set it

                    // with an std policy take the first passing ruling group and then add it's payment list
                    AppliedRuleGroup arg = policy.RuleGroups.FirstOrDefault(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Payment && g.Pass);

                    if (arg != null)
                    {
                        policy.PolicyReason.AddPaymentInfo(arg.RuleGroup.PaymentEntitlement);
                    }
                }
            }
        }

        /// <summary>
        /// Performs the crosswalk rule group evaluation and applies the policy crosswalk.
        /// </summary>
        /// <param name="loa">The loa.</param>
        /// <param name="appliedPolicy">The applied policy.</param>
        protected void PerformEligibility_Crosswalk(LeaveOfAbsence loa, AppliedPolicy appliedPolicy = null)
        {
            using (new InstrumentationContext("EligibilityService.PerformEligibility_Crosswalk"))
            {
                foreach (var policy in GetAppliedPolicies(loa, appliedPolicy))
                {
                    EvaluateRuleGroups(loa, policy.RuleGroups, PolicyRuleGroupType.Crosswalk);
                }
            }
        }

        /// <summary>
        /// Gets a list of policy rule groups for a given policy and leave of absence (case used to get the proper Reason).
        /// </summary>
        /// <param name="policy">The policy to pull the rule groups from that are applicable to the specified case reason.</param>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <returns>A list of policy rule groups that are applicable to the policy</returns>
        private List<PolicyRuleGroup> GetRuleGroups(Policy policy, LeaveOfAbsence loa)
        {
            using (new InstrumentationContext("EligibilityService.GetRuleGroups"))
            {
                List<PolicyRuleGroup> ruleGroups = new List<PolicyRuleGroup>();
                ruleGroups.AddRange(policy.RuleGroups);
                var absenceReason = policy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == loa.Case.Reason.Code);
                if (absenceReason != null)
                {
                    ruleGroups.AddRange(absenceReason.RuleGroups);
                }
                return ruleGroups;
            }
        }

        /// <summary>
        /// Translates a list of policy rule groups to their applied rule group counterparts which are
        /// ready to be attached to an applied policy for rule evaluation.
        /// </summary>
        /// <param name="groups">The policy rule groups to convert to applied rule groups.</param>
        /// <returns>A list of applied rule groups based on the original list or policy rule groups</returns>
        private List<AppliedRuleGroup> GetAppliedRuleGroups(List<PolicyRuleGroup> groups)
        {
            using (new InstrumentationContext("EligibilityService.GetAppliedRuleGroups"))
            {
                return groups.Select(g => new AppliedRuleGroup()
                {
                    RuleGroup = g,
                    Rules = g.Rules.Select(r => new AppliedRule() { Rule = r }).ToList()
                }).ToList();
            }
        }

        /// <summary>
        /// Evaluates a list of rule groups together. The policy group type dictates the types of rule groups that
        /// should be evaluated in this operation (e.g. selection, eligibility, time, etc.).
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="groups">The list of applied rule groups to evaluate which may or may not match the specified rule group type</param>
        /// <param name="type">The policy rule group type </param>
        private void EvaluateRuleGroups(LeaveOfAbsence loa, List<AppliedRuleGroup> groups, PolicyRuleGroupType type)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRuleGroups"))
            {
                foreach (var group in groups.Where(g => g.RuleGroup.RuleGroupType == type))
                    EvaluateRuleGroup(loa, group);
            }
        }

        /// <summary>
        /// Evaluates each rule within the specified rule group, sets the eval date.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="group">The applied rule group containing the rules to evaluate</param>
        private void EvaluateRuleGroup(LeaveOfAbsence loa, AppliedRuleGroup group)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRuleGroup"))
            {
                foreach (var rule in group.Rules)
                    EvaluateRule(loa, rule);

                group.EvalDate = DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Evaluates an applied rule annd determines what the actual value is and whether or not the rule
        /// passes.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="rule">The applied rule to evaluate expressions from</param>
        private void EvaluateRule(LeaveOfAbsence loa, AppliedRule rule)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRule"))
            {
                // If the rule has been overridden, we still calculate because we need to show the user the original evaluation 

                AppliedRuleEvalResult ruleEvalResult = AppliedRuleEvalResult.Unknown;
                try
                {
                    rule.ActualValue = LeaveOfAbsenceEvaluator.Eval(rule.Rule.LeftExpression, loa);
                    if (rule.ActualValue != null)
                        rule.ActualValueString = rule.ActualValue.ToString();

                    FormatRightExpressionForMultipleValues(rule);

                    bool result = LeaveOfAbsenceEvaluator.Eval<bool>(rule.Rule.ToString(), loa);
                    ruleEvalResult = result ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (NullReferenceException)
                {
                    // If the result was NULL and it threw an exception, that's OK, let's check
                    //  though to see if we were actually expecting null.
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                    if (rule.Rule.RightExpression == "null")
                        ruleEvalResult = rule.Rule.Operator == "==" ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (Exception ex)
                {
                    rule.Messages.AddFormat("An error occurred evaluating this rule, {0}", ex.Message);
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                }

                if (rule.Overridden)
                {
                    rule.OverrideFromResult = ruleEvalResult;
                }
                else
                {
                    rule.Result = ruleEvalResult;
                }
            }
        }

        /// <summary>
        /// The function to format the RightExpression for multivalued operator
        /// </summary>
        /// <param name="rule"></param>
        private static void FormatRightExpressionForMultipleValues(AppliedRule rule)
        {
            // In case of multiple value operator, the right expression contains values in the form 'SELF,PARENT,SPOUSE'
            // The values needs to be converted to 'SELF','PARENT','SPOUSE'
            // If it is already formatted, then don't format again
            if (rule != null && rule.Rule != null && rule.Rule.ToString().Contains("::") &&
                !Regex.IsMatch(rule.Rule.RightExpression, "'([A-Z])\\w+'"))
            {
                string concatValues = string.Empty;

                if (!string.IsNullOrEmpty(rule.Rule.RightExpression))
                {
                    string[] singleValue = rule.Rule.RightExpression.Split(',');

                    if (singleValue.Length > 0)
                    {
                        singleValue.ForEach(b => { concatValues = concatValues + "'" + b + "',"; });
                    }
                }

                if (concatValues.Length > 0)
                {
                    concatValues = concatValues.Replace("''", "'");
                    concatValues = concatValues.Substring(0, concatValues.Length - 1);
                    rule.Rule.RightExpression = "(" + concatValues + ")";
                }
            }
        }
    }
}
