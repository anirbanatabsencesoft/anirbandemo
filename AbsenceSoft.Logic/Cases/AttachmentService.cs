﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Logic.Cases
{
    public class AttachmentService : LogicService, ILogicService, IAttachmentService
    {
        public AttachmentService()
        {

        }

        public AttachmentService(User currentUser) : base(currentUser)
        {

        }

        public AttachmentService(Customer currentCustomer, Employer currentEmployer, User currentUser)
           : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        public List<Attachment> ExportAttachments(string employerId)
        {
            return Attachment.AsQueryable().Where(m => m.EmployerId == employerId && m.CaseId != null).ToList();
        }

        /// <summary>
        /// Deletes an attachment (a copy of it is kept in audit and the file is kept preserved for audit
        /// purposes).
        /// </summary>
        /// <param name="attachmentId">The attachment id to delete.</param>
        public void DeleteAttachment(string attachmentId)
        {
            var att = Attachment.GetById(attachmentId);
            if (att != null)
                att.Delete();
        }

        /// <summary>
        /// Gets all case attachments.
        /// </summary>
        /// <param name="caseId">The case Id to pull attachments for</param>
        /// <returns>A list of attachments for the specified case id</returns>
        public List<Attachment> GetCaseAttachments(string caseId)
        {
            return Attachment.AsQueryable().Where(a => a.CaseId == caseId).ToList();
        }//GetCaseAttachments

        /// <summary>
        /// Gets all case attachments.
        /// </summary>
        /// <param name="caseIds">The array of case Ids to pull attachments for</param>
        /// <returns>A list of attachments for the specified case ids</returns>
        public List<Attachment> GetCaseAttachments(string[] caseIds)
        {
            return Attachment.AsQueryable().Where(a => caseIds.Contains(a.CaseId)).ToList();
        }//GetCaseAttachments

        /// <summary>
        /// Gets an attachment instance from its Id.
        /// </summary>
        /// <param name="attachmentId">The attachment Id to pull</param>
        /// <returns>The attachment from the Id or <c>null</c> if no attachment was found by that Id</returns>
        public Attachment GetAttachment(string attachmentId)
        {
            return Attachment.GetById(attachmentId);
        }//GetAttachment

        /// <summary>
        /// Gets the actual document underneath an attachment in order to download that document to the browser
        /// or file system as a byte array and returns the populated attachment instance with <c>File</c> populated.
        /// </summary>
        /// <param name="attachmentId">The Id of the attachment to get the document for.</param>
        /// <returns>An attachment with its File byte array representing the attachment document populated.</returns>
        public Attachment DownloadAttachment(string attachmentId)
        {
            Attachment att = GetAttachment(attachmentId);
            return DownloadAttachment(att);
        }//DownloadAttachment

        /// <summary>
        /// Gets the actual document underneath an attachment in order to download that document to the browser
        /// or file system as a byte array and returns the populated attachment instance with <c>File</c> populated.
        /// </summary>
        /// <param name="attachment">The attachment to get the document for.</param>
        /// <returns>An attachment with its File byte array representing the attachment document populated.</returns>
        public Attachment DownloadAttachment(Attachment attachment)
        {
            if (attachment == null || string.IsNullOrWhiteSpace(attachment.FileId))
                throw new ArgumentNullException("attachment");

            // If it's already populated, why would we continue on?
            if (attachment.File != null && attachment.File.Length > 0)
                return attachment;

            Document doc = Document.Repository.GetById(attachment.FileId);
            if (doc == null || string.IsNullOrWhiteSpace(doc.Locator))
                throw new ArgumentException("Underlying file was not found", "attachment");

            // Assign the file property of our attachment and return the resulting instance.
            attachment.File = doc.Download().File;
            return attachment;
        }//DownloadAttachment

        /// <summary>
        /// Creates an attachment, uploads and stores the file and returns the saved result.
        /// </summary>
        /// <param name="employeeId">The employee Id that owns the case Id to save the attachment for</param>
        /// <param name="caseId">The case Id to save the attachment for</param>
        /// <param name="type">The type of attachment to save</param>
        /// <param name="fileName">The name of the file that should be uploaded for the attachment</param>
        /// <param name="file">The file contents in a byte array</param>
        /// <param name="contentType">The content type of the file being stored</param>
        /// <returns>The saved attachment with the appropriate Id and FileId populated</returns>
        public Attachment CreateAttachment(string employeeId, string caseId, AttachmentType type, string fileName, byte[] file, string contentType, bool isPublic, string description = null)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
                throw new ArgumentNullException("employeeId");
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("caseId");
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException("fileName");
            if (file == null || file.Length == 0)
                throw new ArgumentNullException("file");

            Attachment att = new Attachment()
            {
                EmployeeId = employeeId,
                CaseId = caseId,
                AttachmentType = type,
                File = file,
                ContentLength = file.Length,
                ContentType = contentType,
                FileName = fileName,
                Description = description,
                Public = isPublic
            };

            att.EmployerId = att.Employee.EmployerId;
            att.CustomerId = att.Employee.CustomerId;
            att.EmployeeNumber = att.Employee.EmployeeNumber;

            return CreateAttachment(att);
        }//CreateAttachment

        /// <summary>
        /// Creates an attachment, uploads and stores the file and returns the saved result.
        /// </summary>
        /// <param name="employeeId">The employee Id that owns the case Id to save the attachment for</param>
        /// <param name="caseId">The case Id to save the attachment for</param>
        /// <param name="type">The type of attachment to save</param>
        /// <param name="file">The posted file of the attachment</param>
        /// <returns>The saved attachment with the appropriate Id and FileId populated</returns>
        public Attachment CreateAttachment(string employeeId, string caseId, AttachmentType type, string description, HttpPostedFileBase file, bool isPublic = true)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            Attachment attachment = new Attachment()
            {
                EmployeeId = employeeId,
                CaseId = caseId,
                AttachmentType = type,
                ContentLength = file.ContentLength,
                ContentType = file.ContentType,
                FileName = file.FileName,
                Description = description,
                Public = isPublic
            };

            attachment.EmployerId = attachment.Employee.EmployerId;
            attachment.CustomerId = attachment.Employee.CustomerId;
            attachment.EmployeeNumber = attachment.Employee.EmployeeNumber;

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            Document doc = new Document()
            {
                EmployeeId = employeeId,
                CaseId = caseId,
                ContentLength = file.ContentLength,
                ContentType = file.ContentType,
                FileName = file.FileName,
                CustomerId = attachment.CustomerId,
                EmployerId = attachment.EmployerId,
                File = fileData,
            };
            doc.Metadata.SetRawValue("AttachmentType", attachment.AttachmentType);

            attachment.FileId = doc.Upload().Id;

            attachment.Save();

            attachment.WfOnAttachmentCreated();//Trigger Attachment Created Workflow
            return attachment;
        }//CreateAttachment


        /// <summary>
        /// Creates an attachment, uploads and stores the file in the <c>File</c> property and returns the saved result.
        /// </summary>
        /// <param name="attachment">The fully popluated attachment to save</param>
        /// <returns>The saved attachment with the appropriate Id and FileId populated</returns>
        public Attachment CreateAttachment(Attachment attachment)
        {
            if (attachment == null)
                throw new ArgumentNullException("attachment");
            if (attachment.File == null || attachment.File.Length == 0)
                throw new ArgumentNullException("attachment.File");

            // Back fill customer and employer ids
            if (string.IsNullOrWhiteSpace(attachment.EmployerId))
                attachment.EmployerId = attachment.Employee.EmployerId;
            if (string.IsNullOrWhiteSpace(attachment.CustomerId))
                attachment.CustomerId = attachment.Employee.CustomerId;

            Document doc = new Document()
            {
                EmployeeId = attachment.EmployeeId,
                CaseId = attachment.CaseId,
                ContentLength = attachment.ContentLength,
                ContentType = attachment.ContentType,
                FileName = attachment.FileName,
                CustomerId = attachment.CustomerId,
                EmployerId = attachment.EmployerId,
                File = attachment.File
            };
            doc.Metadata.SetRawValue("AttachmentType", attachment.AttachmentType);

            attachment.FileId = doc.Upload().Id;
            attachment.Document = doc;
            attachment.WfOnAttachmentCreated();

            return attachment.Save();
        }//CreateAttachment

        /// <summary>
        /// Gets a list of attachments of a case for display in the UI using
        /// the standard list criteria.
        /// </summary>
        /// <param name="criteria">The list criteria that must at least containt caseId.</param>
        /// <returns>A list results with a list of attachment style objects.</returns>
        public ListResults AttachmentList(ListCriteria criteria)
        {
            using (new InstrumentationContext("AttachmentService.AttachmentList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("CaseId");
                if (string.IsNullOrWhiteSpace(caseId))
                    throw new AbsenceSoftException("caseId is a required list criteria for getting communications");

                /// Criteria for filter               
                long? attachmentType = criteria.Get<long?>("AttachmentType");
                string description = criteria.Get<string>("Description");
                string fileName = criteria.Get<string>("FileName");
                long? createdDate = criteria.Get<long?>("CreatedDate");
                string attachedBy = criteria.Get<string>("AttachedBy");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (User.Current != null)
                    ands.Add(User.Current.BuildDataAccessFilters());

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Attachment.Query.EQ(e => e.EmployerId, employerId));

                if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(Attachment.Query.EQ(e => e.CaseId, caseId));

                // Fetch user permission
                List<string> userPermissions;
                if (Employer.Current != null)
                    userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
                else
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();

                
                if (!userPermissions.Any(m => m == Permission.ViewConfidentialAttachments))
                {
                    ands.Add(Attachment.Query.Or(Attachment.Query.EQ(a => a.Public, true), Query.EQ("Public", BsonNull.Value), Attachment.Query.EQ(e => e.CreatedById, User.Current?.Id ??CurrentUser.Id)));
                }

                if (attachmentType.HasValue)
                {
                    AttachmentType type = ((AttachmentType)attachmentType);
                    ands.Add(Attachment.Query.EQ(m => m.AttachmentType, type));
                }

                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    ands.Add(Attachment.Query.Matches(e => e.FileName, new BsonRegularExpression("^" + fileName, "i")));    //("^" + employeeFilter, "i")
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    ands.Add(Attachment.Query.Matches(e => e.Description, new BsonRegularExpression(description, "i")));    //("^" + employeeFilter, "i")
                }

                if (!string.IsNullOrWhiteSpace(attachedBy))
                {
                    ands.Add(Attachment.Query.Matches(e => e.CreatedByName, new BsonRegularExpression(attachedBy, "i")));    //("^" + employeeFilter, "i")
                }

                if (createdDate.HasValue)
                {
                    ands.Add(Attachment.Query.GTE(e => e.CreatedDate, new BsonDateTime(createdDate.Value)));
                    // Window is 24 hours
                    ands.Add(Attachment.Query.LT(e => e.CreatedDate, new BsonDateTime(createdDate.Value + 86400000)));
                }

                var query = Attachment.Query.Find(ands.Count > 0 ? Communication.Query.And(ands) : null)
                    .SetFields(Attachment.Query.IncludeFields(
                    // outside of the required fields, this is the only additional one we need
                        e => e.Id,
                        e => e.FileId,
                        e => e.FileName,
                        e => e.AttachmentType,
                        e => e.ContentLength,
                        e => e.ContentType,
                        e => e.Description,
                        e => e.CreatedDate,
                        e => e.CreatedByName
                    ));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending(criteria.SortBy)
                        : SortBy.Descending(criteria.SortBy));

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                result.Total = query.Count();
                result.Results = query.ToList().Select(c => new ListResult()
                        .Set("Id", c.Id)
                        .Set("FileName", c.FileName)
                        .Set("FileId", c.FileId)
                        .Set("AttachmentType", c.AttachmentType)
                        .Set("AttachmentTypeName", c.AttachmentType.ToString().SplitCamelCaseString())
                        .Set("ContentLength", c.ContentLength)
                        .Set("ContentType", c.ContentType)
                        .Set("Description", string.IsNullOrWhiteSpace(c.Description) ? c.FileName : c.Description)
                        .Set("CreatedDate", c.CreatedDate)
                        .Set("CreatedDateText", string.Format("{0:MM/dd/yyyy}", c.CreatedDate))
                        .Set("AttachedBy", c.CreatedByName)
                    ); 

                return result;
            }
        }

        /// <summary>
        /// Gets a list of attachments of a case by context.
        /// the standard list criteria.
        /// </summary>
        /// <param name="criteria">The list criteria that must at least containt caseId.</param>
        /// <returns>A list results with a list of attachment style objects.</returns>
        public ListResults GetAttachmentListByContext(ListCriteria criteria)
        {
            using (new InstrumentationContext("AttachmentService.AttachmentList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("CaseId");
                if (string.IsNullOrWhiteSpace(caseId))
                    throw new AbsenceSoftException("caseId is a required list criteria for getting communications");

                /// Criteria for filter               
                long? attachmentType = criteria.Get<long?>("AttachmentType");
                string description = criteria.Get<string>("Description");
                string fileName = criteria.Get<string>("FileName");
                long? createdDate = criteria.Get<long?>("CreatedDate");
                string attachedBy = criteria.Get<string>("AttachedBy");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (User.Current != null)
                    ands.Add(User.Current.BuildDataAccessFilters());

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Attachment.Query.EQ(e => e.EmployerId, employerId));

                if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(Attachment.Query.EQ(e => e.CaseId, caseId));

                // Fetch user permission
                List<string> userPermissions;
                if (Employer.Current != null)
                    userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
                else
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();


                if (!userPermissions.Any(m => m == Permission.ViewConfidentialAttachments))
                {
                    //ands.Add(Attachment.Query.Or(Attachment.Query.EQ(a => a.Public, true), Query.EQ("Public", BsonNull.Value), Attachment.Query.EQ(e => e.CreatedById, User.Current.Id)));
                    ands.Add(Attachment.Query.Or(Attachment.Query.EQ(a => a.Public, true), Query.EQ("Public", BsonNull.Value), Attachment.Query.EQ(e => e.CreatedById, (CurrentUser ?? User.Current).Id)));
                }

                if (attachmentType.HasValue)
                {
                    AttachmentType type = ((AttachmentType)attachmentType);
                    ands.Add(Attachment.Query.EQ(m => m.AttachmentType, type));
                }

                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    ands.Add(Attachment.Query.Matches(e => e.FileName, new BsonRegularExpression("^" + fileName, "i")));    //("^" + employeeFilter, "i")
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    ands.Add(Attachment.Query.Matches(e => e.Description, new BsonRegularExpression(description, "i")));    //("^" + employeeFilter, "i")
                }

                if (!string.IsNullOrWhiteSpace(attachedBy))
                {
                    ands.Add(Attachment.Query.Matches(e => e.CreatedByName, new BsonRegularExpression(attachedBy, "i")));    //("^" + employeeFilter, "i")
                }

                if (createdDate.HasValue)
                {
                    ands.Add(Attachment.Query.GTE(e => e.CreatedDate, new BsonDateTime(createdDate.Value)));
                    // Window is 24 hours
                    ands.Add(Attachment.Query.LT(e => e.CreatedDate, new BsonDateTime(createdDate.Value + 86400000)));
                }

                var query = Attachment.Query.Find(ands.Count > 0 ? Communication.Query.And(ands) : null)
                    .SetFields(Attachment.Query.IncludeFields(
                        // outside of the required fields, this is the only additional one we need
                        e => e.Id,
                        e => e.FileId,
                        e => e.FileName,
                        e => e.AttachmentType,
                        e => e.ContentLength,
                        e => e.ContentType,
                        e => e.Description,
                        e => e.CreatedDate,
                        e => e.CreatedByName
                    ));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending(criteria.SortBy)
                        : SortBy.Descending(criteria.SortBy));

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                result.Total = query.Count();
                result.Results = query.ToList().Select(c => new ListResult()
                        .Set("Id", c.Id)
                        .Set("FileName", c.FileName)
                        .Set("FileId", c.FileId)
                        .Set("AttachmentType", c.AttachmentType)
                        .Set("AttachmentTypeName", c.AttachmentType.ToString().SplitCamelCaseString())
                        .Set("ContentLength", c.ContentLength)
                        .Set("ContentType", c.ContentType)
                        .Set("Description", string.IsNullOrWhiteSpace(c.Description) ? c.FileName : c.Description)
                        .Set("CreatedDate", c.CreatedDate)
                        .Set("CreatedDateText", string.Format("{0:MM/dd/yyyy}", c.CreatedDate))
                        .Set("AttachedBy", c.CreatedByName)
                    );

                return result;
            }
        }

        /// <summary>
        /// Gets an attachment instance from its Id.
        /// </summary>
        /// <param name="attachmentId">The attachment Id to pull</param>
        /// <param name="caseId">Case Id</param>
        /// <returns>The attachment from the Id or <c>null</c> if no attachment was found by that Id</returns>
        public Attachment GetAttachmentById(string caseId, string attachmentId)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters());
            if (!string.IsNullOrEmpty(caseId))
            {
                ands.Add(Attachment.Query.EQ(at => at.CaseId, caseId));
            }
            if (!string.IsNullOrEmpty(attachmentId))
            {
                ands.Add(Attachment.Query.EQ(at => at.Id, attachmentId));
            }

            var attachment = Attachment.Repository.Collection.Find(ands.Count > 0 ? Attachment.Query.And(ands) : null).First();
            return attachment != null ? attachment : null;


        }
    }
}
