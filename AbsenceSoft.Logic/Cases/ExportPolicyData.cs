﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Logic.Cases
{
    public class ExportPolicyData
    {
        public string CaseNumber { get; set; }

        public string EmployeeNumber { get; set; }

        public string CaseType { get; set; }

        public string PolicyName { get; set; }

        public List<CaseSegment> Segments { get; set; }
    }
}