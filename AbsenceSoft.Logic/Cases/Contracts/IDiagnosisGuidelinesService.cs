﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface IDiagnosisGuidelinesService
    {
        DiagnosisGuidelines GetDiagnosisGuidelines(string medicalCode);

        CoMorbidityGuideline GetCoMorbidityGuideline(string caseId, CoMorbidityGuideline.CoMorbidityArgs arg);
        DiagnosisCode GetByCode(string code);        
    }
}