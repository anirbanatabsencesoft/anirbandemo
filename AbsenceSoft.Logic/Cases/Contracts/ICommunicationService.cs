﻿namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface ICommunicationService
    {
        ListResults CommunicationList(ListCriteria criteria);
    }
}
