﻿using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface IAttachmentService
    {
        ListResults GetAttachmentListByContext(ListCriteria criteria);

        Attachment GetAttachmentById(string caseId, string attachmentId);
    }
}
