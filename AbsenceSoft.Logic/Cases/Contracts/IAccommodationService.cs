﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface IAccommodationService
    {
        void CancelAccommodationRequest(Case myCase, string AccomRequestId, AccommodationCancelReason cancelReason = AccommodationCancelReason.Other, string OtherDesc = null);

        AccommodationRequest CreateOrModifyAccommodationRequest(Case myCase, string generalHealthCondition, List<Accommodation> newAccomms);

        void CloseCaseAccommodation(Case myCase, string AccommodationRequestId, string AccommodationId);
                
        List<AccommodationUsage> BoxUsage(List<AccommodationUsage> usage, DateTime startDate, DateTime? endDate);

        List<AccommodationType> GetAccommodationTypes(string customerId = null, string employerId = null);

        AccommodationInteractiveProcess GetEmployerAccommodationInteractiveProcess(string customerId, string employerId, string accomCode = null, bool isInteractiveProcess = true);

        List<CaseNote> GetAccommIntProcCaseNotes(Guid accommId, string caseId);

        IQueryable<AccommodationType> GetBaseAccommodationTypes(string customerId = null, string employerId = null);
    }
}
