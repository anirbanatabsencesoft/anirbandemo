﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface IEligibilityService
    {
        LeaveOfAbsence RunEligibility(Case myCase);
        Case AddManualPolicy(Case myCase, string policyCode, string notes);
        IEnumerable<Policy> GetAvailableManualPolicies(Case myCase);
        string GetWhySelected(Case myCase, AppliedPolicy policy);
        Employer CurrentEmployer { get; set; }
        LeaveOfAbsence GetLeaveOfAbsence(string caseId);
    }
}
