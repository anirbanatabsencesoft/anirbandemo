﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Cases.Contracts
{
    public interface ICaseService
    {
        Case GetCaseById(string caseId);
        List<PolicySummary> GetEmployeePolicySummaryByCaseId(string caseId, EligibilityStatus[] includeStatus, params string[] policyCodes);
        List<IntermittentTimeRequest> GenerateIntermittentRequests(Case c, PolicyAbsenceReason policyAbsenceReason, DateTime requestDate, List<IntermittentTimeRequest> intermittentTimeRequests, bool isEditTOR = false);
        int GetTorTime(Case c, PolicyAbsenceReason policyReason, int approvedTime);
        Case CreateOrModifyTimeOffRequest(Case theCase, List<IntermittentTimeRequest> timeRequested);
        Case UpdateCase(Case myCase, CaseEventType? evt = null);
        List<ValidationMessage> PreValidateTimeOffRequest(string caseId, List<IntermittentTimeRequest> timeRequested);
        Case CancelCase(string caseId, CaseCancelReason reason = CaseCancelReason.Other);        
        Case RunCalcs(Case myCase, CaseChangeHandlers changeHandlers = null);
        Case GetCaseByCaseNumber(string caseNumber);

        Case CreateCase(CaseStatus status, string employeeId, DateTime startDate, DateTime? endDate, CaseType type, string reasonCode, Schedule reducedSchedule = null, string description = null, string narrative = null, string caseId = null);
        Case ChangeCase(Case theCase, DateTime startDate, DateTime endDate, bool changeStartDate, bool changeEndDate, CaseType newCaseType,
            bool modifyDecisions, bool modifyAccommodations, Schedule workSchedule = null, List<VariableScheduleTime> variableTime = null);
        CaseNote IntermittentAbsenceEntryNote(Case c, IntermittentTimeRequest r, User CurrentUser);
        Case CreateOrModifyCertification(Case myCase, Certification cert, bool isNewCert);
        List<AbsenceReason> GetAbsenceReasons(string employeeId, CaseType? caseType = null);
        int CalculateDaysAwayFromWork(Case theCase, DateTime? injuryDate = null);

        void UpdateContactOnAssociatedCases(EmployeeContact contact, bool update = false);
        EmployeeContact GetCaseProviderContact(string hcpContactId);
        void ReassignCase(Case theCase, User assignTo, string caseAssigneeTypeCode = null);
        Case CaseClosed(Case caseToClose, DateTime? rtwDate, CaseClosureReason? reason, string reasonDetails = null, string otherReasonDetails = null, string Outcome = null);
        Case ApplyDetermination(Case myCase, string policyCode, DateTime startDate, DateTime endDate, AdjudicationStatus status, string reasonCode = null, string explanation = null, string reasonName = null);
        void EnqueueRecalcFutureCases(Case activeCase);
        List<DenialReason> GetAllDenialReasons(DenialReasonTarget target = DenialReasonTarget.None);
        void UpdateTimeOfRequestNoteToRemovedRequest(Case existingCase, List<IntermittentTimeRequest> intermittentTimeRequests);
    }
}
