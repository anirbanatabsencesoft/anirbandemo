﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Pay;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService
    {
        #region std member variables

        private Case _pl_case { get; set; }
        private Employee _pl_Employee { get; set; }
        private LeaveOfAbsence _pl_loa { get; set; }
        private List<DateTime> _pl_holidays { get; set; }
        private List<PaymentTierInfo> _pl_payment_tiers { get; set; }
        private Dictionary<string, List<PaymentDateInfo>> _pl_payment_date_info { get; set; }
        private Dictionary<string, Policy> _pl_policies { get; set; }
        private List<Time> _pl_work_days { get; set; }
        private List<PolicyPayOrder> _pl_policy_pay_order { get; set; }
        private PayService _pl_pay_service { get; set; }

        /// <summary>
        /// initialize all the member data that is used throughout
        /// the std process
        /// </summary>
        /// <param name="theCase"></param>
        private void PLInitialzeMembers()
        {
            // get the employee
            _pl_Employee = Employee.GetById(_pl_case.Employee.Id);

            // set up the leave of absence object
            _pl_loa = new LeaveOfAbsence();
            _pl_loa.Employee = _pl_case.Employee;
            _pl_loa.Employer = _pl_case.Employee.Employer;
            _pl_loa.Customer = _pl_case.Employee.Customer;
            _pl_loa.Case = _pl_case;

            // get the work schedule
            _pl_loa.WorkSchedule = _pl_Employee.WorkSchedules;

            // build the holidays
            DateTime endDate = _pl_case.EndDate ?? _pl_case.StartDate.AddYears(1);

            HolidayService hs = new HolidayService();
            List<int> years = Enumerable.Range(0, (endDate.Year - _pl_case.StartDate.Year) + 1).Select(i => _pl_case.StartDate.Year + i).ToList();
            _pl_holidays = hs.GetHolidayList(years, _pl_case.Employer);

            // get the payment tiers and turn it into a dictionary to make it eaiser to use
            _pl_payment_tiers = PaymentTiersByPolicy(_pl_case);

            // we need an actual work schedule in order to calculate the payment amounts
            // and we need complete time periods around the start and end. An extra 120 loops won't kill us
            _pl_work_days = _pl_loa.MaterializeSchedule(_pl_case.StartDate.AddDays(-60), endDate.AddDays(60), false);

            // shortcuts
            _pl_payment_date_info = _pl_payment_tiers.ToDictionary(d => d.PolicyCode, d => d.PaymentDates);
            _pl_policies = new Dictionary<string, Policy>();

            _pl_pay_service = new PayService();
            _pl_policy_pay_order = _pl_pay_service.GetEmployerPolicyPayOrder(_pl_case.EmployerId);

            // if the employer does not have any order defined then just stuff them all
            // in there, otherwise make sure they are in the correct order
            if (_pl_policy_pay_order == null || _pl_policy_pay_order.Count == 0)
            {
                // or maybe you want to throw an error here..... 
                int x = 0;
                _pl_policy_pay_order = _pl_case.Segments.SelectMany(s => s.AppliedPolicies)
                                            .Select(ap => new { Name = ap.Policy.Name, Code = ap.Policy.Code })
                                            .Distinct()
                                            .Select(pppo => new PolicyPayOrder() { Name = pppo.Name, Code = pppo.Code, Order = x++ })
                                            .ToList();
            }
            else
            {
                _pl_policy_pay_order = _pl_policy_pay_order.OrderBy(o => o.Order).ToList();
            }

        }

        #endregion std member variables

        /// <summary>
        /// Calculate the std payments for the case
        /// Call this anytime a change is made to the case that impacts money
        /// (except for anything that calls run calcs because this is called from there)
        /// </summary>
        /// <param name="theCase"></param>
        public void PaidLeaveCalculate(Case theCase)
        {
            using (new InstrumentationContext("CaseService.PaidLeaveCalculate"))
            {
                // if there are not any paid policies then skip all of this
                if (!theCase.Segments.SelectMany(s => s.AppliedPolicies).Any(ap => ap.PolicyReason.Paid))
                    return;

                // If the employer isn't subscribed to paid leave calcs anyway, then don't do this.
                if (!theCase.Employer.HasFeature(Feature.ShortTermDisablityPay))
                    return;

                // If there are no pay schedules for the employer, then skip all of this
                if (PaySchedule.AsQueryable().Where(q => q.EmployerId == theCase.EmployerId).Count() <= 0)
                {
                    Log.Warn("The Employer _id:{2} for Employee _id:{1} for Case _id:{0} does not have any Pay Schedules, yet has paid policies", theCase.Id, theCase.Employee.Id, theCase.EmployerId);
                    return;
                }

                // now this is an error.... if we have a paid policy and no info to pay them
                if (!theCase.Employee.PayType.HasValue)
                {
                    Log.Warn("The Employee _id:{1} for Case _id:{0} does not have PayType defined", theCase.Id, theCase.Employee.Id);
                    return;
                }

                if (!theCase.Employee.Salary.HasValue && theCase.Employee.Salary != 0)
                {
                    Log.Warn("The Employee _id:{1} for Case _id:{0} does not have Salary defined", theCase.Id, theCase.Employee.Id);
                    return;
                }

                // if the pay collection is empty, it needs to be created
                // but only if there is a paid type of policy
                if (theCase.Pay == null)
                    theCase.Pay = new PayInfo();

                // initialize the variables that are used throughout the lifetime of the calc
                _pl_case = theCase;
                PLInitialzeMembers();

                // we have not paid them yet, so build up the payment schedule
                // that we will use to pay
                if (_pl_case.Pay.PayPeriods.Count == 0)
                    PLGeneratePayPeriods();

                // now calc payments for ones that have not already been locked
                foreach (PayPeriod pp in _pl_case.Pay.PayPeriods)
                {
                    // the user has locked it there is nothing else to do
                    if (pp.IsLocked)
                        continue;

                    PLCalculatePayPeriod(pp, _pl_case.Pay.ApplyOffsetsByDefault);
                }
            }
        }

        /// <summary>
        /// Removes unlocked pay periods and regenerates them.
        /// Should only be called after changing the pay schedule
        /// </summary>
        /// <param name="theCase"></param>
        public void PaidLeaveRecalculateForPayScheduleChange(Case theCase)
        {
            using (new InstrumentationContext("CaseService.PaidLeaveRecalculateForPayScheduleChange"))
            {
                // If the employer isn't subscribed to paid leave calcs anyway, then don't do this.
                if (!theCase.Employer.HasFeature(Feature.ShortTermDisablityPay))
                    return;

                // if there are not any paid policies then skip all of this
                if (!theCase.Segments.SelectMany(s => s.AppliedPolicies).Any(ap => ap.PolicyReason.Paid))
                    return;

                /// All pay periods have been locked, nothing to recalculate
                if (theCase.Pay.PayPeriods.All(p => p.IsLocked))
                    return;

                // now this is an error.... if we have a paid policy and no info to pay them
                if (!theCase.Employee.PayType.HasValue)
                {
                    Log.Warn("The Employee _id:{1} for Case _id:{0} does not have PayType defined", theCase.Id, theCase.Employee.Id);
                    return;
                }

                if (!theCase.Employee.Salary.HasValue && theCase.Employee.Salary != 0)
                {
                    Log.Warn("The Employee _id:{1} for Case _id:{0} does not have Salary defined", theCase.Id, theCase.Employee.Id);
                    return;
                }

                // initialize the variables that are used throughout the lifetime of the calc
                _pl_case = theCase;
                PLInitialzeMembers();

                PLRegeneratePayPeriods();

                // now calc payments for ones that have not already been locked
                foreach (PayPeriod pp in _pl_case.Pay.PayPeriods)
                {
                    // the user has locked it there is nothing else to do
                    if (pp.IsLocked)
                        continue;

                    PLCalculatePayPeriod(pp, _pl_case.Pay.ApplyOffsetsByDefault);
                }
            }
        }

        // create a new pay schedule
        private void PLGeneratePayPeriods()
        {
            if (_pl_case.Pay.PayPeriods == null || _pl_case.Pay.PayPeriods.Count > 0)
                throw new AbsenceSoftException("GeneratePayPeriods called on an invalid case");

            PayScheduleService paySchedSvc = new PayScheduleService();
            PaySchedule paySchedule = _pl_case.Employee.PaySchedule;

            // if no pay schedule, we need to get the appropriate one for this employee.
            if (paySchedule == null)
            {
                paySchedSvc.SetDefaultEmployeePaySchedule(_pl_case.Employee);
                paySchedule = _pl_case.Employee.PaySchedule;
            }

            if (paySchedule == null)
                throw new AbsenceSoftException("No pay scehdule defined for employee and Employer does not have a default schedule configured");

            //get the payment dates for the pay schedule, give it some sort of default end date
            DateTime calcEndDate = _pl_case.EndDate ?? _pl_case.StartDate.AddYears(1);
            List<PaySchedulePayments> payments = paySchedSvc.PaymentDateRangesForSchedule(paySchedule, _pl_case.StartDate, calcEndDate);

            // copy the pay schedules into the pay collection
            foreach (PaySchedulePayments psp in payments)
                _pl_case.Pay.PayPeriods.Add(new PayPeriod()
                {
                    StartDate = psp.StartDate,
                    EndDate = psp.EndDate,
                    PayrollDate = new UserOverrideableValue<DateTime>(psp.EndDate.AddDays(psp.ProcessingDays)),
                    Status = PayrollStatus.Pending
                });
        }

        /// <summary>
        /// Will delete any unlocked pay periods and regenerate them according to the employees current pay schedule
        /// </summary>
        private void PLRegeneratePayPeriods()
        {
            if (_pl_case.Pay.PayPeriods == null)
                throw new AbsenceSoftException("RegeneratePayPeriods called on an invalid case");

            PayScheduleService paySchedSvc = new PayScheduleService();
            PaySchedule paySchedule = _pl_case.Pay.PaySchedule ?? _pl_case.Employee.PaySchedule;

            // if no pay schedule, we need to get the appropriate one for this employee.
            if (paySchedule == null)
            {
                paySchedSvc.SetDefaultEmployeePaySchedule(_pl_case.Employee);
                paySchedule = _pl_case.Employee.PaySchedule;
            }

            if (paySchedule == null)
                throw new AbsenceSoftException("No pay scehdule defined for employee and Employer does not have a default schedule configured");

            _pl_case.Pay.PayPeriods = _pl_case.Pay.PayPeriods.Where(p => p.IsLocked).ToList();

            // get the payment dates for the pay schedule, give it some sort of default end date and start date
            // they might not have paid anything at this point, so if we don't have any records left after removing the unlocked ones
            // we use the case start date, otherwise we get the maximum end date, which should be the latest locked pay period
            DateTime calcStartDate = _pl_case.Pay.PayPeriods.Count > 0 ? _pl_case.Pay.PayPeriods.Max(p => p.EndDate) : _pl_case.StartDate;
            DateTime calcEndDate = _pl_case.EndDate ?? _pl_case.StartDate.AddYears(1);

            List<PaySchedulePayments> payments = paySchedSvc.PaymentDateRangesForSchedule(paySchedule, calcStartDate, calcEndDate);

            // copy the pay schedules into the pay collection
            foreach (PaySchedulePayments psp in payments)

                _pl_case.Pay.PayPeriods.Add(new PayPeriod()
                {
                    StartDate = psp.StartDate,
                    EndDate = psp.EndDate,
                    PayrollDate = new UserOverrideableValue<DateTime>(psp.EndDate.AddDays(psp.ProcessingDays)),
                    Status = PayrollStatus.Pending
                });
        }


        /// <summary>
        /// we have to calcualte the payments for each policy one day at at time
        /// </summary>
        private class PlTotaling
        {
            /// <summary>
            /// Gets or sets the policy code.
            /// </summary>
            /// <value>
            /// The policy code.
            /// </value>
            public string PolicyCode { get; set; }

            /// <summary>
            /// Gets or sets the date used.
            /// </summary>
            /// <value>
            /// The date used.
            /// </value>
            public DateTime DateUsed { get; set; }

            /// <summary>
            /// Gets or sets the actual pay for the day.
            /// </summary>
            /// <value>
            /// The actual pay for the day.
            /// </value>
            public decimal ActualPayForTheDay { get; set; }

            /// <summary>
            /// Gets or sets the maximum allowed pay for the day.
            /// </summary>
            /// <value>
            /// The maximum allowed pay for the day.
            /// </value>
            public decimal MaxAllowedPayForTheDay { get; set; }

            /// <summary>
            /// Gets or sets the minimum allowed pay for the day.
            /// </summary>
            /// <value>
            /// The minimum allowed pay for the day.
            /// </value>
            public decimal MinAllowedPayForTheDay { get; set; }

            /// <summary>
            /// Gets or sets the applied percentage.
            /// </summary>
            /// <value>
            /// The applied percentage.
            /// </value>
            public decimal AppliedPercentage { get; set; }

            /// <summary>
            /// Gets or sets the tier percentage.
            /// </summary>
            /// <value>
            /// The tier percentage.
            /// </value>
            public decimal TierPercentage { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is a waiting period.
            /// </summary>
            /// <value>
            ///   <c>true</c> if this instance is a waiting period; otherwise, <c>false</c>.
            /// </value>
            public bool IsWaitingPeriod { get; set; }

            /// <summary>
            /// Gets or sets the pay period salary.
            /// </summary>
            /// <value>
            /// The pay period salary.
            /// </value>
            public decimal PayPeriodSalary { get; set; }
        }

        /// <summary>
        /// calculate the totlas for the pay period on this day
        /// applying all the payment tier rules and and offset logic
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="pp"></param>
        private void PLCalculatePayPeriod(PayPeriod pp, bool offsetByDefault)
        {
            // get the applied policies we are working on 
            // (so we don't have to build the list everytime
            List<AppliedPolicy> appliedPols = _pl_case.Segments.SelectMany(s => s.AppliedPolicies)
                .Where(ap => ap.PolicyReason.Paid)
                .ToList();

            // get the policies we are working on
            List<string> policyCodes = new List<string>();


            // we have a list of policies to process, and an order to process them in, match the two
            // up and put them in the correct order
            foreach (PolicyPayOrder ppo in _pl_policy_pay_order)
            {
                List<string> toAdd = _pl_case.Segments.SelectMany(s => s.AppliedPolicies)
                    .Where(ap => ap.PolicyReason.Paid && ap.Policy.Code == ppo.Code)
                    .Select(ap => ap.Policy.Code)
                    .Distinct()
                    .ToList();

                foreach (string ta in toAdd)
                {
                    if (!policyCodes.Any(a => a == ta))
                    {
                        policyCodes.Add(ta);
                    }
                }
            }

            // we need policy info to popluate the pay detail objects, make sure we have all of it
            PLBuildCheckPolicyList(policyCodes);

            // and then build up the list of policy reasons that we are going to be applying
            Dictionary<string, PolicyAbsenceReason> theReasons = new Dictionary<string, PolicyAbsenceReason>();
            foreach (AppliedPolicy thisAp in appliedPols)
            {
                if (!theReasons.ContainsKey(thisAp.Policy.Code))
                {
                    theReasons[thisAp.Policy.Code] = thisAp.PolicyReason;
                }
            }

            // we have to calculate each day one at a time, to apply the correct percentage
            // tiers and dates. We'll total them after we calc all of them
            List<PlTotaling> dayTotals = new List<PlTotaling>();

            // and if a policy does not have data, then when we do a summary
            // we can't summarize it because it's not there but
            // it will need a zero row. Set a flag so we know who to add a
            // blank too
            Dictionary<string, bool> policyHasData = policyCodes.ToDictionary(d => d, d => false);

            // now we can start calculating the payment for each day of the leave
            // after we do this we can summarize
            DateTime workDate = pp.StartDate;
            while (workDate <= pp.EndDate)
            {
                // for each day we need to know how much we have paid out so far
                // and at what percentage we have paid it out as we go
                // across the policies
                decimal daysPercent = 0;
                decimal daysPay = 0;

                foreach (string polCode in policyCodes)
                {
                    PaymentDateInfo todaysTier = _pl_payment_date_info[polCode].FirstOrDefault(pd => workDate.DateInRange(pd.StartDate, pd.EndDate));
                    if (todaysTier == null)
                    {
                        // provide a 0% bogus tier so that we can keep running
                        todaysTier = new PaymentDateInfo()
                        {
                            StartDate = DateTime.UtcNow,
                            EndDate = DateTime.UtcNow,
                            PaymentInfo = new PaymentInfo()
                            {
                                PaymentPercentage = 0,
                                MaxPayAmount = 0,
                                MaxPaymentPercentage = 0,
                                MinPayAmount = 0
                            }
                        };
                    }

                    // get the usages for this policy and day, and there better be only
                    // one because if there isn't something else is really wrong
                    AppliedPolicyUsage usage = appliedPols.Where(ap => ap.Policy.Code == polCode)
                        .SelectMany(ap => ap.Usage.Where(u => u.DateUsed == workDate && u.Determination == AdjudicationStatus.Approved))
                        .FirstOrDefault();

                    // A pay period can cross the end of a leave and there would not be any
                    // usage, so move on
                    if (usage == null)
                    {
                        continue;
                    }

                    // and then total those up. There should really only be one usage per day
                    // but why take the chance
                    decimal fullDaysSalary = PLPayRateByDay(usage);

                    // if there is no salary for today, then we don't need to do the rest
                    // of this do the next day
                    if (fullDaysSalary <= 0)
                        continue;

                    // first get what would (will) be paid if there are no other policies
                    // we may want the 0 rows for other totalling purposes
                    // don't ask my which, better safe than sorry
                    PlTotaling thisTotal = new PlTotaling()
                    {
                        PolicyCode = polCode,
                        DateUsed = workDate,
                        TierPercentage = todaysTier.PaymentInfo.PaymentPercentage,
                        IsWaitingPeriod = todaysTier.PaymentInfo.IsWaitingPeriod,
                        PayPeriodSalary = fullDaysSalary,
                    };
                    dayTotals.Add(thisTotal);

                    // now figure out what the tier should be for today

                    // allow the user to specify over 1 percent, but if it is set to 0 that is the same as
                    // 100% so adjust the number accordingly
                    decimal theMaxPercentage = todaysTier.PaymentInfo.MaxPaymentPercentage == 0 ? 1 : todaysTier.PaymentInfo.MaxPaymentPercentage;

                    // pay up to the payment percentage, but don't go over the max (less what has been used)
                    theMaxPercentage = Math.Min(todaysTier.PaymentInfo.PaymentPercentage, Math.Max(theMaxPercentage - daysPercent, 0));

                    decimal theMaxPayment = Math.Round(theMaxPercentage * fullDaysSalary, 10);

                    // decimal actual payment percentage. If there is a waiting period (and it is not waived)
                    // then set the percentage to 0
                    if (todaysTier.PaymentInfo.IsWaitingPeriod)
                    {
                        theMaxPayment = 0;
                        theMaxPercentage = 0;
                    }

                    // now find the max payment amount allowed for today, start with how
                    // much we want to pay based on percent so far
                    decimal theActualPayment = theMaxPayment;

                    // there is a weekly total. Turn that into a daily total and then set that for
                    // today
                    if (todaysTier.PaymentInfo.MaxPayAmount.HasValue)
                    {
                        // get the number of minutes in the week. We are going to use this
                        // to figure out what the max amount for the day is
                        int minutesPerWeek = PLWorkMinutesInWeek(workDate);

                        // next policy
                        if (minutesPerWeek == 0)
                            continue;

                        // to get to the rate for the day, we know the dollar value for today
                        // and the minute value for today, and the minutes and dollar value for the week
                        // we have RATIOS!
                        int minutesIndDay = PLWorkMinutesInDay(usage);
                        decimal maxPayAllowedForDay = minutesIndDay * (todaysTier.PaymentInfo.MaxPayAmount.Value / minutesPerWeek);

                        // we can pay up to
                        theActualPayment = Math.Min(theActualPayment, maxPayAllowedForDay - daysPay);
                    }

                    if (todaysTier.PaymentInfo.MinPayAmount.HasValue)
                    {
                        // get the number of minutes in the week. We are going to use this
                        // to figure out what the max amount for the day is
                        int minutesPerWeek = PLWorkMinutesInWeek(workDate);

                        // next policy
                        if (minutesPerWeek == 0)
                            continue;

                        // to get to the rate for the day, we know the dollar value for today
                        // and the minute value for today, and the minutes and dollar value for the week
                        // we have RATIOS!
                        int minutesIndDay = PLWorkMinutesInDay(usage);

                        decimal minPayAllowedForDay = minutesIndDay * (todaysTier.PaymentInfo.MinPayAmount.Value / minutesPerWeek);

                        // but should pay at least
                        if (theActualPayment < minPayAllowedForDay)
                        {
                            theActualPayment = minPayAllowedForDay;
                        }

                        thisTotal.MinAllowedPayForTheDay = minPayAllowedForDay;
                    }

                    // yeah, set it everyday. It won't hurt anything
                    policyHasData[polCode] = true;

                    // update the totals that are being checked across the policy for the day
                    daysPercent += theMaxPercentage;
                    daysPay += theActualPayment;

                    // first get what would (will) be paid if there are no other policies
                    thisTotal.ActualPayForTheDay = theActualPayment;
                    thisTotal.MaxAllowedPayForTheDay = theMaxPayment;
                    thisTotal.AppliedPercentage = theMaxPercentage;
                }

                // do the next day
                workDate = workDate.AddDays(1);
            }

            List<PayPeriodDetail> oldDetail = pp.Detail;
            // now that we have all of that info by day, we can roll it up into our
            // level needed for the pay detail
            pp.Detail = dayTotals.GroupBy(dt => new {  dt.PolicyCode, IsWaitingPeriod = dt.IsWaitingPeriod, dt.AppliedPercentage })
                .Select(dt => new PayPeriodDetail()
                {
                    PolicyCode = _pl_policies[dt.Key.PolicyCode].Code,
                    PolicyName = _pl_policies[dt.Key.PolicyCode].Name,
                    StartDate = dt.Min(d => d.DateUsed),
                    EndDate = dt.Max(d => d.DateUsed),
                    PaymentTierPercentage = dt.Key.AppliedPercentage,
                    SalaryTotal = Math.Round(dt.Sum(d => d.PayPeriodSalary), 2),
                    PayAmount = new UserOverrideableValue<decimal>(Math.Round(dt.Sum(d => d.ActualPayForTheDay), 2)),
                    MaxPaymentAmount = Math.Round(dt.Sum(d => d.MaxAllowedPayForTheDay), 2),
                    MinPaymentAmount = Math.Round(dt.Sum(d => d.MinAllowedPayForTheDay), 2),
                    PayPercentage = new UserOverrideableValue<decimal>(dt.Key.AppliedPercentage),
                    IsOffset = (theReasons[dt.Key.PolicyCode].AllowOffset ?? false) && offsetByDefault
                }).ToList();



            // now, just because life hates us, we may not have anything in there because this was
            // a 0% tier or waiting period or whatever, so if that is missing, create it
            foreach (string polCode in policyCodes)
            {
                //HACK: Somehow, in dev at least, we have policies that no longer exist but are specified in a segment
                if (!_pl_policies.ContainsKey(polCode) || _pl_policies[polCode] == null) continue;

                if (!policyHasData[polCode])
                {
                    pp.Detail.Add(new PayPeriodDetail()
                    {
                        StartDate = pp.StartDate,
                        EndDate = pp.EndDate,
                        PayAmount = new UserOverrideableValue<decimal>(0),
                        PolicyName = _pl_policies[polCode].Name,
                        PolicyCode = _pl_policies[polCode].Code,
                        MaxPaymentAmount = 0,
                        MinPaymentAmount = 0,
                        PaymentTierPercentage = 0,
                        PayPercentage = new UserOverrideableValue<decimal>(0),
                        IsOffset = (theReasons[polCode].AllowOffset ?? false) && offsetByDefault
                    });
                }
            }

            // see if any of the reason require rounding, if not we are done
            // otherwise, do the rounding
            if (!theReasons.Any(a => a.Value.UseWholeDollarAmountsOnly))
                return;

            // great... apply the rounding because we can't do this until we have an entire payment calculated
            decimal carryForward = 0;
            foreach (PayPeriodDetail det in pp.Detail)
            {
                // check to see if we are carring anything forward
                decimal originalAmount = det.PayAmount.Value;
                if (carryForward != 0)
                {
                    // reduce the next payment by the amount brough forward
                    det.PayAmount = new UserOverrideableValue<decimal>(det.PayAmount.Value - carryForward);
                }

                // now see if this is a rounding policy
                // if not go onto the next one, but the carry forward as been
                // applied so it is gone. we only need to carry it forward again
                // if there are two+ rounding policies in a row
                if (!theReasons[det.PolicyCode].UseWholeDollarAmountsOnly)
                {
                    carryForward = 0;
                    continue;
                }

                // if so, round it, and carry the rounding amount forward
                decimal newAmount = Math.Ceiling(det.PayAmount.Value);
                carryForward += (newAmount - originalAmount);

                det.PayAmount = new UserOverrideableValue<decimal>(newAmount);
            }

            CopyOverrideValues(pp.Detail, oldDetail);
        }

        /// <summary>
        /// Should be ran at the end of paid calcs to make sure that any user set values are saved
        /// </summary>
        /// <param name="newDetails"></param>
        /// <param name="oldDetails"></param>
        private void CopyOverrideValues(List<PayPeriodDetail> newDetails, List<PayPeriodDetail> oldDetails)
        {
            foreach (PayPeriodDetail detail in newDetails)
            {
                PayPeriodDetail matchingDetail = oldDetails.FirstOrDefault(p => p.StartDate == detail.StartDate && p.EndDate == detail.EndDate && p.PolicyCode == detail.PolicyCode);
                if (matchingDetail == null)
                    continue;

                if (matchingDetail.PayPercentage.HasOverride)
                    detail.PayPercentage.SetOverride(matchingDetail.PayPercentage.Value, matchingDetail.PayPercentage.OverrideBy);

                if (matchingDetail.PayAmount.HasOverride)
                    detail.PayAmount.SetOverride(matchingDetail.PayAmount.Value, matchingDetail.PayAmount.OverrideBy);

                detail.IsOffset = matchingDetail.IsOffset;
            }
        }


        /// <summary>
        /// don't retrieve the policies over and over again,
        /// build / check the list of the ones we have used and
        /// "cache" them
        /// </summary>
        /// <param name="policyCodes">The policy codes.</param>
        private void PLBuildCheckPolicyList(List<string> policyCodes)
        {
            foreach (string polCode in policyCodes)
                if (!_pl_policies.ContainsKey(polCode))
                    _pl_policies[polCode] = Policy.GetByCode(polCode, _pl_case.CustomerId, _pl_case.EmployerId);
        }

        /// <summary>
        /// find the rate of pay per day
        /// unfortunately this needs to be done each day,
        /// there is no way to know 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private decimal PLPayRateByDay(AppliedPolicyUsage apu)
        {
            // find out how many minutes we are going to be calcing for
            // this could be a 0 day (like a weekend) and then there
            // is nothing to do, but, it could be a holiday, which require
            // some other contortions to get the minutes
            int calcMinutes = (int)apu.MinutesUsed;
            // is it a holiday?
            if (calcMinutes == 0 && _pl_holidays.Any(a => a == apu.DateUsed))
            {
                Time actualWorkDay = _pl_work_days.FirstOrDefault(fd => fd.SampleDate == apu.DateUsed);
                if (actualWorkDay != null)
                {
                    calcMinutes = actualWorkDay.TotalMinutes ?? 0;
                }
            }

            // if there are not any work minutes then there isn't any pay
            if (calcMinutes == 0)
            {
                return 0;
            }

            // everything is done off of total minutes so....
            decimal payRatePerMinute = 0;

            // aaaahhhh hourly, why can't life always be this easy?
            if (_pl_case.Employee.PayType.Value == PayType.Hourly)
            {
                payRatePerMinute = ((decimal)(_pl_case.Employee.Salary.Value) / 60m);
            }
            else
            {
                // crap, they are salary, so here's what we are gonna do, we gonna back up
                // till sunday, and look forward to saturday (because that's what we all do, look forward to Saturday))
                // and then find out they rate per minute:
                // ((annual rate / 52)) / current weeks scheduled minutes to get our payrate per minute
                int totalMinutes = PLWorkMinutesInWeek(apu.DateUsed);
                if (totalMinutes > 0)
                {
                    payRatePerMinute = (((decimal)_pl_case.Employee.Salary.Value) / 52m) / totalMinutes;
                }
            }

            // if rate per minutes is still 0 (how did that happen?)(well it did)(so just roll with)(ok)
            // then just return
            if (payRatePerMinute == 0)
            {
                return 0;
            }

            decimal todaysPay = Math.Round(calcMinutes * payRatePerMinute, 10);

            return todaysPay;
        }

        private int PLWorkMinutesInDay(AppliedPolicyUsage apu)
        {
            int minutesInDay = (int)apu.MinutesInDay;
            // is it a holiday?
            if (minutesInDay == 0 && _pl_holidays.Any(a => a == apu.DateUsed))
            {
                Time actualWorkDay = _pl_work_days.FirstOrDefault(fd => fd.SampleDate == apu.DateUsed);
                if (actualWorkDay != null)
                {
                    minutesInDay = actualWorkDay.TotalMinutes ?? 0;
                }
            }
            return minutesInDay;
        }

        private int PLWorkMinutesInWeek(DateTime sampleDate)
        {
            DateTime payCalcStartDate = sampleDate.GetFirstDayOfWeek();
            DateTime payCalcEndDate = payCalcStartDate.AddDays(6); // inclusive, remember
            return _pl_work_days.Where(wd => wd.SampleDate >= payCalcStartDate && wd.SampleDate <= payCalcEndDate).Sum(twd => (twd.TotalMinutes ?? 0)); ;
        }

        // used in the PaymentAmount Method, cuz I hate anonomous types and tuples (I really hate tuples) (item7, what is that?)
        public class PaymentDateInfo
        {
            /// <summary>
            /// Gets or sets the start date.
            /// </summary>
            /// <value>
            /// The start date.
            /// </value>
            public DateTime StartDate { get; set; }

            /// <summary>
            /// Gets or sets the end date.
            /// </summary>
            /// <value>
            /// The end date.
            /// </value>
            public DateTime EndDate { get; set; }

            /// <summary>
            /// Gets or sets the payment information.
            /// </summary>
            /// <value>
            /// The payment information.
            /// </value>
            public PaymentInfo PaymentInfo { get; set; }
        }

        // used in the payment amount function, for a bit of performance boost so we don't do this every time
        // calc the dates for the payment tiers and "cache" them and only regen if we need a different
        // date range
        private Dictionary<PolicyAbsenceReason, DateTime> _currentPaymentStartDateDict;
        private Dictionary<PolicyAbsenceReason, List<PaymentDateInfo>> _paymentDatesDict;

        /// <summary>
        /// build up a "cache" of the policy usage payment bands
        /// </summary>
        /// <param name="par"></param>
        private void PaymentDatesBuild(PolicyAbsenceReason par, DateTime startDate, bool waiveWaitingPeriods)
        {
            if (_paymentDatesDict == null)
            {
                _paymentDatesDict = new Dictionary<PolicyAbsenceReason, List<PaymentDateInfo>>();
                _currentPaymentStartDateDict = new Dictionary<PolicyAbsenceReason, DateTime>();
            }

            if (!_paymentDatesDict.ContainsKey(par))
            {
                _paymentDatesDict[par] = new List<PaymentDateInfo>();
                _currentPaymentStartDateDict[par] = DateTime.MinValue;
            }
            
            // if the these dates are different then calc our list
            if (_currentPaymentStartDateDict[par] != startDate)
            {
                var paymentDates = new List<PaymentDateInfo>();
                _paymentDatesDict[par] = paymentDates;
                DateTime nextStartDate = startDate;
                foreach (PaymentInfo pi in par.PaymentTiers.OrderBy(o => o.Order).Where(t => !t.IsWaitingPeriod || (t.IsWaitingPeriod && !waiveWaitingPeriods)))
                {
                    // for right now, days are days and weeks are weeks
                    int daysToAdd = 0;
                    switch (par.EntitlementType.Value)
                    {
                        case EntitlementType.CalendarDays:
                        case EntitlementType.WorkDays:
                        case EntitlementType.FixedWorkDays:
                        case EntitlementType.CalendarDaysFromFirstUse:
                            daysToAdd = pi.Duration;
                            break;
                        case EntitlementType.CalendarWeeks:
                        case EntitlementType.WorkWeeks:
                        case EntitlementType.CalendarWeeksFromFirstUse:
                            daysToAdd = pi.Duration * 7;
                            break;
                        case EntitlementType.CalendarMonths:
                        case EntitlementType.WorkingMonths:
                        case EntitlementType.CalendarMonthsFromFirstUse:
                            DateTime testDate = nextStartDate;
                            for (var i = 0; i < pi.Duration; i++)
                            {
                                daysToAdd += DateTime.DaysInMonth(testDate.Year, testDate.Month);
                                testDate = testDate.GetLastDayOfMonth().AddDays(1);
                            }
                            break;
                        default:
                            throw new AbsenceSoftException(string.Format("Payment Amount is of an unsupported type, \"{0}\"", Enum.GetName(typeof(EntitlementType), par.EntitlementType)));
                    }

                    // fill in our date picker thingy (remember days are inclusive so back out 1)(if I say it enough times I might remember it)
                    paymentDates.Add(new PaymentDateInfo() { StartDate = nextStartDate, EndDate = nextStartDate.AddDays(daysToAdd - 1), PaymentInfo = pi });

                    nextStartDate = nextStartDate.AddDays(daysToAdd);
                }

                // add one past the end to show zero for the rest of time, cuz after that time you get nothing
                if (paymentDates.Count > 0)
                {
                    paymentDates.Add(new PaymentDateInfo()
                    {
                        StartDate = nextStartDate,
                        EndDate = DateTime.MaxValue,
                        PaymentInfo = new PaymentInfo()
                        {
                            Duration = int.MaxValue,
                            Order = paymentDates.Count + 100,
                            PaymentPercentage = 0
                        }
                    });
                }

                _currentPaymentStartDateDict[par] = startDate;
            }
        }

        /// <summary>
        /// not sure if this class should go with the data because this is the only place it
        /// is used
        /// </summary>
        public class PaymentTierInfo
        {
            /// <summary>
            /// Gets or sets the policy code.
            /// </summary>
            /// <value>
            /// The policy code.
            /// </value>
            public string PolicyCode { get; set; }

            /// <summary>
            /// Gets or sets the payment dates.
            /// </summary>
            /// <value>
            /// The payment dates.
            /// </value>
            public List<PaymentDateInfo> PaymentDates { get; set; }
        }

        /// <summary>
        /// get a list of pay
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        public List<PaymentTierInfo> PaymentTiersByPolicy(Case theCase)
        {
            List<PaymentTierInfo> ptiList = new List<PaymentTierInfo>();

            // need something to calc to
            DateTime calDate = theCase.StartDate;

            int daysAlreadyUsed = 0;

            // if there is a releated case "fake" out the start date
            // and build the tiers like it was all one case
            if (!string.IsNullOrWhiteSpace(theCase.RelatedCaseNumber))
                daysAlreadyUsed = PLNumberOfDaysInRelatedCase(theCase.RelatedCaseNumber);

            foreach (CaseSegment cs in theCase.Segments)
            {
                foreach (AppliedPolicy ap in cs.AppliedPolicies)
                {
                    if (!ap.PolicyReason.Paid)
                        continue;

                    PaymentTierInfo pti = ptiList.FirstOrDefault(p => p.PolicyCode == ap.Policy.Code) ?? ptiList.AddFluid(new PaymentTierInfo() { PolicyCode = ap.Policy.Code });
                    PaymentDatesBuild(ap.PolicyReason, calDate, theCase.Pay.WaiveWaitingPeriod.Value);
                    pti.PaymentDates = _paymentDatesDict[ap.PolicyReason].Clone();
                }
            }

            // now remove out the tiers that our outside our real date range
            if (daysAlreadyUsed > 0)
            {
                ptiList = PLFixUpTierDate(ptiList, daysAlreadyUsed, theCase.StartDate);

                // and make sure there is at least a 0 tier in each
                DateTime fakeEndDate = theCase.StartDate.AddYears(2);
                foreach (PaymentTierInfo info in ptiList)
                {
                    if (info.PaymentDates.Count == 0)
                        info.PaymentDates.Add(new PaymentDateInfo()
                        {
                            StartDate = theCase.StartDate,
                            EndDate = theCase.EndDate ?? fakeEndDate,
                            PaymentInfo = new PaymentInfo()
                            {
                                Duration = 720,
                                IsWaitingPeriod = false,
                                MaxPayAmount = 0,
                                MaxPaymentPercentage = 0m,
                                MinPayAmount = 0,
                                Order = 1,
                                PaymentPercentage = 0
                            }
                        });
                }
            }

            return ptiList;
        }

        private List<PaymentTierInfo> PLFixUpTierDate(List<PaymentTierInfo> oldList, int numOfDays, DateTime originalStartDate)
        {
            List<PaymentTierInfo> newPtiList = new List<PaymentTierInfo>();

            DateTime realStartDate = originalStartDate.AddDays(numOfDays);
            foreach (PaymentTierInfo pti in oldList)
            {
                PaymentTierInfo newPti = new PaymentTierInfo() { PolicyCode = pti.PolicyCode, PaymentDates = new List<PaymentDateInfo>() };

                foreach (PaymentDateInfo pdi in pti.PaymentDates)
                {
                    // if we used it all up, skip it
                    if (pdi.EndDate <= realStartDate)
                        continue;

                    // it has start or starts today
                    if (pdi.StartDate >= realStartDate)
                        newPti.PaymentDates.Add(pdi);

                    // only state that is left is that the start date is less then or new start date
                    // and the end date is greater than the new start date, so set the start date
                    pdi.StartDate = realStartDate;
                    newPti.PaymentDates.Add(pdi);
                }
            }

            return newPtiList;
        }

        private int PLNumberOfDaysInRelatedCase(string relatedCaseId)
        {
            Case checkCase = Case.GetById(relatedCaseId);
            if (checkCase == null)
                return 0;

            if (checkCase.EndDate == null)
                throw new AbsenceSoftException("Related case does not have an end date");

            // inclusive
            int numOfDaysInCase = (checkCase.EndDate.Value - checkCase.StartDate).Days + 1;

            // it's cases all the way down
            if (!string.IsNullOrWhiteSpace(checkCase.RelatedCaseNumber))
                numOfDaysInCase += PLNumberOfDaysInRelatedCase(checkCase.RelatedCaseNumber);

            return numOfDaysInCase;
        }

    }
}
