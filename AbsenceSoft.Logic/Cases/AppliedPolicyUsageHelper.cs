﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// This class takes a schedule, materializes it for 4 years (2 forward and
    /// 2 back). Then it can be used to create a applied policy usage object that
    /// contains all the dates set.
    /// 
    /// You will need to set the rest of the info yourself (determination, etc)
    /// 
    /// 
    /// </summary>
    public class AppliedPolicyUsageHelper
    {
        private Employee _emp { get; set; }
        private List<Time> _schedule { get; set; }

        private DateTime _lastCalcDate { get; set; }
        private List<AppliedPolicyUsageType> _lastReturn { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedPolicyUsageHelper"/> class.
        /// </summary>
        /// <param name="loa">The loa.</param>
        /// <param name="minCaseDate">The minimum case date.</param>
        /// <param name="maxCaseDate">The maximum case date.</param>
        public AppliedPolicyUsageHelper(LeaveOfAbsence loa, DateTime minCaseDate, DateTime maxCaseDate, PolicyBehavior policyBehavior = PolicyBehavior.ReducesWorkWeek)
        {
            _schedule = loa.MaterializeSchedule(minCaseDate.AddYears(-2), maxCaseDate.AddYears(2), false,null, policyBehavior);
            _lastCalcDate = DateTime.MinValue;
            _emp = loa.Employee;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedPolicyUsageHelper"/> class.
        /// </summary>
        /// <param name="scheduleTimes">The schedule times.</param>
        /// <param name="emp">The emp.</param>
        public AppliedPolicyUsageHelper(List<Time> scheduleTimes, Employee emp)
        {
            _emp = emp;
            _schedule = scheduleTimes;
            _lastCalcDate = DateTime.MinValue;
        }

        /// <summary>
        /// build the list that can be used to calc whatever timeframe we want
        /// </summary>
        /// <param name="sampleDate"></param>
        /// <returns></returns>
        public List<AppliedPolicyUsageType> GetUsage(DateTime sampleDate,PolicyBehavior policyBehaviour = PolicyBehavior.ReducesWorkWeek, List<DateTime> holidays = null)
        {
            // maybe use a dictionary instead, then clone the date you already calc'd... thinking on that too
            // you could do that if this becomes a bottleneck
            if(_lastCalcDate != DateTime.MinValue && sampleDate == _lastCalcDate)
                return CloneLastResult();

            _lastCalcDate = sampleDate;

            List<AppliedPolicyUsageType> rtVal = new List<AppliedPolicyUsageType>();

            AppliedPolicyUsageType wh = new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.WorkingHours,
                BaseUnit = 0
            };

            // working hours.... since we do everything as a percentage we need to
            // force the minutes into hours, and then slice that up for what percentage
            // of the day an hour is. Basically, we are just going in a circle
            if (_schedule != null)
            {
                Time theDay = _schedule.FirstOrDefault(s => s.SampleDate == sampleDate);
                if (theDay != null)
                    wh.BaseUnit = 1d / ((theDay.TotalMinutes ?? 0) / 60d);
            }
            rtVal.Add(wh);

            // work day is just 1 (it's always 1) (but we have to add it so the code will work)
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.WorkDays,
                BaseUnit = 1
            });

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarDays,
                BaseUnit = 1
            });

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarDaysFromFirstUse,
                BaseUnit = 1
            });

            // not sure what to do with this yet
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.ReasonablePeriod,
                BaseUnit = 1
            });

            DayOfWeek? firstDay = null;
            if (_emp != null)
                firstDay = _emp.StartDayOfWeek;
            
            // take the sample date, get the first day of the week and then find out
            // how many work days there are that week
            DateTime startDate = sampleDate.GetFirstDayOfWeek(firstDay);
            DateTime endDate = startDate.AddDays(6);
            int daysCounted = 0;
            if (_schedule != null)
            {
                if (policyBehaviour == PolicyBehavior.IgnoredNoLostTime)
                {
                    daysCounted = DaysInSchedule(_schedule, holidays, startDate, endDate);
                }
                else
                {
                    daysCounted = _schedule.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);
                }
            }
           
            // until explained what the dif is these are the same
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.WorkWeeks,
                BaseUnit = daysCounted
            });


            // there are 7 days in the week... nothing really to think about there
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarWeeks,
                BaseUnit = 7
            });

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarWeeksFromFirstUse,
                BaseUnit = 7
            });

            // months in AT = 30 days per 1 month always, In case of 28 Days or 31 Days month also we will do calculation based on 30
            startDate = sampleDate.GetFirstDayOfMonth();
            endDate = startDate.GetLastDayOfMonth();
            if (_schedule != null)
            {
                if (policyBehaviour == PolicyBehavior.IgnoredNoLostTime)
                {
                    daysCounted = DaysInSchedule(_schedule, holidays, startDate, endDate);
                }
                else
                {
                    daysCounted = _schedule.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);
                }
            }

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.WorkingMonths,
                BaseUnit = daysCounted
            });

            //calendar months in AT = 30 days per 1 month always
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarMonths,
                BaseUnit = 30
            });

            //calendar months in AT = 30 days per 1 month always
            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarMonthsFromFirstUse,
                BaseUnit = 30
            });


            startDate = sampleDate.GetFirstDayOfYear();
            endDate = sampleDate.GetLastDayOfYear();
            //daysCounted = _schedule.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarYears,
                BaseUnit = (endDate - startDate).Days + 1
            });

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.CalendarYearsFromFirstUse,
                BaseUnit = (endDate - startDate).Days + 1
            });

            rtVal.Add(new AppliedPolicyUsageType()
            {
                UnitType = EntitlementType.FixedWorkDays,
                BaseUnit = 1
            });

            // cache the result for next time
            _lastReturn = rtVal.Clone();

            return rtVal;
        } // public List<AppliedPolicyUsageType> GetUsage(DateTime sampleDate)
        private int DaysInSchedule(List<Time> schedule, List<DateTime> holidays, DateTime startDate, DateTime endDate)
        {
            int daysCounted = schedule.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);
            if (holidays != null && holidays.Any())
            {
                int holidayCount = holidays.Where(h => h >= startDate && h <= endDate).Count();
                daysCounted = daysCounted + holidayCount;
            }
            return daysCounted;
        }
        private List<AppliedPolicyUsageType> CloneLastResult()
        {
            if (_lastReturn == null) return new List<AppliedPolicyUsageType>();
            _lastReturn.RemoveAll(r => r == null);
            if (!_lastReturn.Any()) return new List<AppliedPolicyUsageType>();
            return _lastReturn.Clone();
        }
    }//end: class AppliedPolicyUsageHelper
}
