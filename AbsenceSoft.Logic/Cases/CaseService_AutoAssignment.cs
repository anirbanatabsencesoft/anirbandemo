﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.CaseAssignmentRules;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService
    {
        /// <summary>
        /// Gets the case assignee types for customer.
        /// </summary>
        /// <returns></returns>
        public List<CaseAssigneeType> GetCaseAssigneeTypesForCustomer()
        {
            return CaseAssigneeType
                .AsQueryable()
                .Where(cat => cat.CustomerId == CurrentCustomer.Id)
                .OrderBy(cat => cat.Name)
                .ToList();
        }

        /// <summary>
        /// Get case assignee by caseId
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public List<CaseAssignee> GetCaseAssignees(string caseId)
        {
            var caseAssignees = CaseAssignee.AsQueryable().Where(ca => ca.CaseId == caseId).ToList();
            return caseAssignees;
        }

        /// <summary>
        /// Get case assignee code list
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<string> GetCaseAssigneeTypeCode(string customerId)
        {
            var caseAssignees = CaseAssignee
                .AsQueryable()
                .Where(ca => ca.CustomerId == customerId && !ca.IsDeleted)
                .Select(ca => new { CaseTypeAssigneeCode = ca.Code })
                .ToList();

            return caseAssignees
                .Select(ca => ca.CaseTypeAssigneeCode)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Lists the case assignee types.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults ListCaseAssigneeTypes(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.ListCaseAssigneeTypes"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);
                string customerId = criteria.Get<string>("CustomerId");
                string name = criteria.Get<string>("Name");
                string description = criteria.Get<string>("Description");
                string code = criteria.Get<string>("Code");
                List<IMongoQuery> ands = new List<IMongoQuery>
                {
                    CurrentUser.BuildDataAccessFilters()
                };

                if (!string.IsNullOrEmpty(customerId))
                {
                    ands.Add(Query.EQ("CustomerId", new BsonObjectId(new ObjectId(customerId))));
                }

                if (!string.IsNullOrEmpty(name))
                {
                    ands.Add(CaseAssigneeType.Query.Matches(o => o.Name, new BsonRegularExpression(name, "i")));
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ands.Add(CaseAssigneeType.Query.Matches(o => o.Description, new BsonRegularExpression(description, "i")));
                }

                if (!string.IsNullOrEmpty(code))
                {
                    ands.Add(CaseAssigneeType.Query.Matches(o => o.Code, new BsonRegularExpression(code, "i")));
                }

                var query = CaseAssigneeType.Query.Find(OrganizationType.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(o => new ListResult()
                    .Set("Id", o.Id)
                    .Set("CustomerId", o.CustomerId)
                    .Set("Name", o.Name)
                    .Set("Description", o.Description)
                    .Set("Code", o.Code)
                );

                return results;
            }
        }

        /// <summary>
        /// Lists the case assignment rule sets.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults ListCaseAssignmentRuleSets(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.ListCaseAssigneeRuleSets"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);
                string customerId = criteria.Get<string>("CustomerId");
                string name = criteria.Get<string>("Name");
                string description = criteria.Get<string>("Description");
                string code = criteria.Get<string>("Code");
                string caseAssigneeTypeName = criteria.Get<string>("CaseAssigneeTypeName");
                List<IMongoQuery> ands = new List<IMongoQuery>();

                if (!string.IsNullOrEmpty(customerId))
                {
                    ands.Add(Query.EQ("CustomerId", new BsonObjectId(new ObjectId(customerId))));
                }

                if (!string.IsNullOrEmpty(name))
                {
                    ands.Add(CaseAssignmentRuleSet.Query.Matches(o => o.Name, new BsonRegularExpression(name, "i")));
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ands.Add(CaseAssignmentRuleSet.Query.Matches(o => o.Description, new BsonRegularExpression(description, "i")));
                }

                if (!string.IsNullOrEmpty(code))
                {
                    ands.Add(CaseAssignmentRuleSet.Query.Matches(o => o.Code, new BsonRegularExpression(code, "i")));
                }
                if (!string.IsNullOrEmpty(caseAssigneeTypeName))
                {
                    List<string> caseAssigneeTypeCodes = CaseAssigneeType.AsQueryable().Where(cat => cat.Name.ToLower().Contains(caseAssigneeTypeName.ToLower().Trim())).Select(cat => cat.Code).ToList();
                    ands.Add(CaseAssignmentRuleSet.Query.In(o => o.CaseAssigneeTypeCode, caseAssigneeTypeCodes));
                }

                var query = CaseAssignmentRuleSet.Query.Find(CaseAssignmentRuleSet.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(o => new ListResult()
                    .Set("Id", o.Id)
                    .Set("CustomerId", o.CustomerId)
                    .Set("Name", o.Name)
                    .Set("Code", o.Code)
                    .Set("Description", o.Description)
                    .Set("CaseAssigneeTypeName", o.CaseAssigneeType == null ? "" : o.CaseAssigneeType.Name)
                    .Set("CaseAssignmentRuleOrder", o.CaseAssignmentRuleOrder)
                );

                return results;
            }
        }

        /// <summary>
        /// The user population cache, used to hold the list of users for sequential assignment
        /// </summary>
        private Dictionary<string, List<User>> _userPopulationCache = new Dictionary<string, List<User>>();
        private Dictionary<string, List<string>> _roleCache = new Dictionary<string, List<string>>();
        /// <summary>
        /// Evaluates the saved case assignment rules to see who should be assigned to the case
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="assigneeTypeCode">The assignee type code.</param>
        /// <param name="user">The user.</param>
        /// <param name="saveCase">if set to <c>true</c> [save case].</param>
        public void EvaluateCaseAssignmentRules(Case theCase, string assigneeTypeCode = null, User user = null, bool saveCase = true)
        {
            List<CaseAssignmentRuleSet> assignmentRuleSets = new List<CaseAssignmentRuleSet>();
            string customerId = theCase.CustomerId;

            // Get all of the roles that have the Edit Case permission(s) in them
            List<string> roles = _roleCache.ContainsKey(customerId) ? _roleCache[customerId]?.ToList() : null;
            if (roles == null)
            {
                roles = GetRolesThatCanEditCases(customerId);
                // Cache these roles for subsequent requests, they'll probably get expensive
                _roleCache.Add(customerId, roles);
            }

            // Bring all the customers users into memory
            List<User> assignees = _userPopulationCache.ContainsKey(customerId) ? _userPopulationCache[customerId]?.ToList() : null;
            if (assignees == null)
            {
                assignees = GetPossibleCaseAssigneeUsers(customerId, theCase.EmployerId, roles);
                // Cache this list for subsequent operations, it's probably an expensive query
                _userPopulationCache.Add(customerId, assignees);
            }

            if (!string.IsNullOrWhiteSpace(assigneeTypeCode))
                assignmentRuleSets.AddRange(CaseAssignmentRuleSet.AsQueryable().Where(ruleSet => ruleSet.CaseAssigneeTypeCode == assigneeTypeCode && ruleSet.CustomerId == customerId).ToList());
            else
                // if they didn't provide a code, let's get the base set
                assignmentRuleSets = CaseAssignmentRuleSet.AsQueryable().Where(ruleSet => ruleSet.CustomerId == customerId && ruleSet.CaseAssigneeTypeCode == null).ToList();

            foreach (CaseAssignmentRuleSet assignmentRuleSet in assignmentRuleSets.OrderBy(a => a.CaseAssignmentRuleOrder))
            {
                List<User> ruleSetAssignees = EvaluateAssignmentRuleSet(theCase, user, saveCase, customerId, assignees, assignmentRuleSet);
                if (ruleSetAssignees.Count == 1)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Evaluates the assignment rule set.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        /// <param name="saveCase">if set to <c>true</c> [save case].</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="assignees">The assignees.</param>
        /// <param name="assignmentRuleSet">The assignment rule set.</param>
        /// <returns></returns>
        private List<User> EvaluateAssignmentRuleSet(Case theCase, User user, bool saveCase, string customerId, List<User> assignees, CaseAssignmentRuleSet assignmentRuleSet)
        {
            List<User> ruleSetAssignees = new List<User>();
            ruleSetAssignees.AddRange(assignees);
            foreach (CaseAssignmentRule assignmentRule in assignmentRuleSet.CaseAssignmentRules.OrderBy(rule => rule.Order))
            {
                // Determine if we have a rule-set behavior but no rule behavior, then set it here so we don't have to change
                //  the underlying API of the base rule and evaluation engine.
                if (assignmentRuleSet.Behavior.HasValue && !assignmentRule.Behavior.HasValue)
                {
                    assignmentRule.Behavior = assignmentRuleSet.Behavior;
                }

                BaseRule rule = CreateRule(assignmentRule.RuleType);
                if (rule == null || rule.Skip)
                    continue;

                rule.ApplyCaseAssignmentRules(ref ruleSetAssignees, theCase, assignmentRule);
                if (rule.Terminate || ruleSetAssignees.Count <= 1)
                    break;
            }
            if (ruleSetAssignees.Count > 1)
            {
                RotaryRule rotaryRule = new RotaryRule();
                rotaryRule.ApplyCaseAssignmentRules(ref ruleSetAssignees, theCase, null);
            }
            CreateCaseAssignee(theCase, customerId, assignmentRuleSet.CaseAssigneeTypeCode, ruleSetAssignees, user, saveCase);
            return ruleSetAssignees;
        }

        /// <summary>
        /// Gets the roles that can edit cases.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        private static List<string> GetRolesThatCanEditCases(string customerId)
        {
            List<string> roles = Role.Query.Find(Role.Query.And(
                Role.Query.EQ(r => r.CustomerId, customerId),
                Role.Query.EQ(r => r.Permissions, Permission.EditCase.Id)
            )).Select(r => r.Id).ToList();
            roles.Add(Role.SystemAdministrator.Id);
            return roles;
        }

        /// <summary>
        /// Gets the possible case assignee users. Used internally by case auto assignment routine AND should be used by the UI
        /// in the configuration of case auto assignment rule sets and rules (for selecting target users)
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The optional employer identifier which allows to further filter on roles and employer access.</param>
        /// <param name="roles">The roles to filter users by.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">customerId</exception>
        public List<User> GetPossibleCaseAssigneeUsers(string customerId = null, string employerId = null, List<string> roles = null)
        {
            // If no customer id is provided, that's okay, our service-tier should have it in context, otherwise throw an exception
            if (string.IsNullOrWhiteSpace(customerId))
            {
                customerId = CustomerId ?? CurrentCustomer?.Id;
            }
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            // If no roles were provided, we need to get them for the current customer
            if (roles == null)
            {
                roles = GetRolesThatCanEditCases(customerId) ?? new List<string>(1);
            }
            
            // If we still don't have any roles, then we need to add the system admin role at least
            if (!roles.Any())
            {
                roles.Add(Role.SystemAdministrator.Id);
            }

            // Build our dynamic query document, we have to do this because there are some conditional
            //  conditions that we only include if the user has passed in an employerId
            List<IMongoQuery> ands = new List<IMongoQuery>()
            {
                // Has to be for the same customer
                User.Query.EQ(u => u.CustomerId, customerId),
                // And, also must not be a disabled user (in any way that manifests itself here)
                User.Query.Or(
                    User.Query.EQ(u => u.DisabledDate, null),
                    User.Query.NotExists(u => u.DisabledDate),
                    User.Query.GT(u => u.DisabledDate, DateTime.UtcNow)
                ),
                // And, must also be a Portal user (not ESS or Admin or any other user type)
                User.Query.EQ(u => u.UserType, UserType.Portal)
            };

            // If we have an employer ID, we need to match explicitly on that Employer ID
            if (!string.IsNullOrWhiteSpace(employerId))
            {
                // And, also must have permissions/roles and employer access as
                ands.Add(Query.Or(
                    // Either the user has access to the employer AND
                    User.Query.ElemMatch(u => u.Employers, q => q.And(
                        q.EQ(a => a.EmployerId, employerId),
                        Query.Or(
                            // Either has Auto Assign Cases = true
                            q.EQ(a => a.AutoAssignCases, true),
                            // Or they have the Edit Case permission
                            q.In(a => a.Roles, roles)
                        )
                    )),
                    // OR
                    Query.And(
                        // They have access to the Employer AND
                        User.Query.ElemMatch(u => u.Employers, q => q.EQ(a => a.EmployerId, employerId)),
                        // Also have a role with the Edit Case permission
                        User.Query.In(u => u.Roles, roles)
                    )
                ));
            }
            else
            {
                // We don't have any specific employer, so we have to assume they could
                //  be included in this rule for any employer (user should know then to use an Employer filter in the rule set)

                // And, also must have permissions/roles and employer access as
                ands.Add(Query.Or(
                    // Either the user has access to the employer AND
                    User.Query.ElemMatch(u => u.Employers, q => q.Or(
                        // Either has Auto Assign Cases = true
                        q.EQ(a => a.AutoAssignCases, true),
                        // Or they have the Edit Case permission
                        q.In(a => a.Roles, roles)
                    )),
                    // OR, Also have a role for any employer with the Edit Case permission
                    User.Query.In(u => u.Roles, roles)
                ));
            }

            // Get our possible assignee list and return that to the method caller
            List<User> assignees;
            assignees = User.Query.Find(User.Query.And(ands)).ToList();
            return assignees;
        }

        /// <summary>
        /// Creates a BaseRule based on the passed in ruleType
        /// </summary>
        /// <param name="ruleType"></param>
        /// <returns></returns>
        private BaseRule CreateRule(string ruleType)
        {
            if (ruleType.Trim().Length > 0)
            {
                if (int.TryParse(ruleType, out int ruleTypeValue))
                {
                    CaseAssignmentRuleType ruleTypeEnum = (CaseAssignmentRuleType)ruleTypeValue;
                    if (Enum.TryParse(ruleType, out ruleTypeEnum))
                    {
                        return Activator.CreateInstance(Type.GetType(string.Format("AbsenceSoft.Logic.CaseAssignmentRules.{0}", ruleTypeEnum))) as BaseRule;
                    }
                }
                else
                {
                    return new CustomFieldsRule();
                }
            }

            return null;
        }

        /// <summary>
        /// Reassigns the case.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="assignTo">The assign to.</param>
        /// <param name="caseAssigneeTypeCode">The case assignee type code.</param>
        public void ReassignCase(Case theCase, User assignTo, string caseAssigneeTypeCode = null)
        {
            // Delete all existing case assignees that are primary assignees.
            var caseAssigneeList = CaseAssignee.GetByCaseId(theCase.Id).Where(ca => ca.Code == caseAssigneeTypeCode).ToList();

            // Let's copy the old user id
            var oldUserId = caseAssigneeList.FirstOrDefault()?.UserId ?? theCase.AssignedToId;
            // Now delete the case assignment, we're going to just create a new one
            caseAssigneeList.ForEach(a => a.Delete());
            // Clean the old instance, this clears the _id and dates/audit fields so it is "new"
            var caseAssignee = caseAssigneeList.FirstOrDefault() ?? new CaseAssignee()
            {
                CaseId = theCase.Id,
                CustomerId = assignTo.CustomerId,
                Code = caseAssigneeTypeCode,
                Status = theCase.Status
            };
            caseAssignee.Clean();
            // Set the UserId, everything else stays the same (case, type, status, etc.)
            caseAssignee.UserId = assignTo.Id;
            // Save it, this will insert the new case assignment record
            caseAssignee.Save();

            // Update the cases to the new assignee
            theCase.AssignedTo = assignTo;
            theCase.AssignedToId = assignTo.Id;
            theCase.AssignedToName = assignTo.DisplayName;
            theCase.Save();

            // Reassign todos for case that were previously assigned to the old user                 
            var todos = ToDoItem.AsQueryable().Where(t => t.CaseId == theCase.Id && t.AssignedToId == oldUserId).ToList();
            foreach (ToDoItem item in todos)
            {
                if (item.Status != ToDoItemStatus.Pending && item.Status != ToDoItemStatus.Overdue)
                    continue;

                item.AssignedToId = assignTo.Id;
                item.AssignedToName = assignTo.DisplayName;
                item.Save();
            }

            // Update all the ToDo items with the new assignee
            ToDoItem.Update(
                ToDoItem.Query.And(
                    ToDoItem.Query.EQ(t => t.CaseId, theCase.Id),
                    ToDoItem.Query.NotIn(t => t.Status, new[] { ToDoItemStatus.Cancelled, ToDoItemStatus.Complete }),
                    ToDoItem.Query.NE(t => t.IsDeleted, true),
                    ToDoItem.Query.EQ(t => t.AssignedToId, oldUserId)
                ),
                ToDoItem.Updates
                    .Set(t => t.AssignedToId, assignTo.Id)
                    .Set(t => t.AssignedToName, assignTo.DisplayName)
                    .Set(t => t.ModifiedById, CurrentUser.Id)
                    .CurrentDate(t => t.ModifiedDate),
                UpdateFlags.Multi);
        }

        /// <summary>
        /// Creates the case assignee
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="customerId"></param>
        /// <param name="caseAssigneeTypeCode"></param>
        /// <param name="ruleSetAssignees"></param>
        private static void CreateCaseAssignee(Case theCase, string customerId, string caseAssigneeTypeCode, List<User> ruleSetAssignees, User user = null, bool saveCase = true)
        {
            // If there are none to assign, let's not even create a DB entry
            if (!ruleSetAssignees.Any())
                return;

            if (string.IsNullOrWhiteSpace(caseAssigneeTypeCode))
            {
                var ass = ruleSetAssignees.First();
                // If it's already assigned, let's not thrash the case by over-saving it.
                if (theCase.AssignedToId == ass.Id)
                {
                    return;
                }

                theCase.AssignedToId = ass.Id;
                theCase.AssignedToName = ass.DisplayName;

                if (saveCase)
                {
                    UpdateCaseAssignee(theCase, customerId);
                }
            }
            else
            {
                // this will ensure we only ever have one assignee of each type auto-assigned to a case
                //  because these things could be running in parallel and we wouldn't want them stepping
                //  over each other.
                var bulk = CaseAssignee.Repository.Collection.InitializeUnorderedBulkOperation();
                foreach (var assignee in ruleSetAssignees.Distinct(u => u.Id))
                {
                    UpsertCaseAssignee(theCase, customerId, caseAssigneeTypeCode, user, bulk, assignee);
                }
                bulk.Execute();
            }
        }

        /// <summary>
        /// Updates the case assignee.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="customerId">The customer identifier.</param>
        private static void UpdateCaseAssignee(Case theCase, string customerId)
        {
            var existingAssignee = CaseAssignee.AsQueryable().FirstOrDefault(c => c.CaseId == theCase.Id && c.CustomerId == customerId && c.Code == null);
            if (existingAssignee != null)
            {
                existingAssignee.Delete();
                existingAssignee.Clean();
            }
            else
            {
                existingAssignee = new CaseAssignee()
                {
                    CaseId = theCase.Id,
                    Status = theCase.Status,
                    Code = null,
                    CustomerId = theCase.CustomerId
                };
            }
            existingAssignee.UserId = theCase.AssignedToId;
            existingAssignee.Save();
            theCase.Save();
        }

        /// <summary>
        /// Upserts the case assignee.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="caseAssigneeTypeCode">The case assignee type code.</param>
        /// <param name="user">The user.</param>
        /// <param name="bulk">The bulk.</param>
        /// <param name="assignee">The assignee.</param>
        private static void UpsertCaseAssignee(Case theCase, string customerId, string caseAssigneeTypeCode, User user, BulkWriteOperation<CaseAssignee> bulk, User assignee)
        {
            bulk.Find(CaseAssignee.Query.And(
                CaseAssignee.Query.EQ(a => a.CaseId, theCase.Id),
                CaseAssignee.Query.EQ(a => a.Code, caseAssigneeTypeCode),
                CaseAssignee.Query.IsNotDeleted()))
                // Key this as an upsert, this will match based on the case id and assignee type code above
                .Upsert()
                .UpdateOne(CaseAssignee.Updates
                    // Then, only use set on insert, this will leave any existing assignee unadulterated and if one
                    //  exists the upsert wil not insert a new one, but if it does, then it  will set these values
                    //  on insert.... This is the best way in MongoDB to do all of this "set based" rather than read
                    //  in all records which is still prone to duplication due to timing of parallel processes.
                    .SetOnInsert(a => a.CaseId, theCase.Id)
                    .SetOnInsert(a => a.Code, caseAssigneeTypeCode)
                    .SetOnInsert(a => a.CreatedById, user == null ? User.DefaultUserId : user.Id)
                    .SetOnInsert(a => a.CreatedDate, DateTime.UtcNow)
                    .SetOnInsert(a => a.CustomerId, customerId)
                    .SetOnInsert(a => a.IsDeleted, false)
                    .SetOnInsert(a => a.ModifiedById, user == null ? User.DefaultUserId : user.Id)
                    .SetOnInsert(a => a.ModifiedDate, DateTime.UtcNow)
                    .SetOnInsert(a => a.Status, theCase.Status)
                    .SetOnInsert(a => a.UserId, assignee.Id));
        }

        /// <summary>
        /// Updates the case assignee status.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="newStatus">The new status.</param>
        public void UpdateCaseAssigneeStatus(string caseId, CaseStatus newStatus)
        {
            // Set the case status updates for the assignees
            CaseAssignee.Update(
                Query.And(
                    CaseAssignee.Query.EQ(a => a.CaseId, caseId),
                    CaseAssignee.Query.NE(a => a.Status, newStatus),
                    CaseAssignee.Query.NE(a => a.IsDeleted, true)),
                CaseAssignee.Updates
                .Set(a => a.Status, newStatus)
                .Set(a => a.ModifiedById, CurrentUser.Id)
                .CurrentDate(a => a.ModifiedDate)
            , UpdateFlags.Multi);
        }

        /// <summary>
        /// Gets the case assignment list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults GetCaseAssignmentList(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.GetCaseAssignmentList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                List<IMongoQuery> ands = new List<IMongoQuery>
                {
                    // Always add the customer id to the filter
                    CaseAssignee.Query.EQ(a => a.CustomerId, CurrentUser.CustomerId)
                };

                string caseId = criteria.Get<string>("CaseId");
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    ands.Add(CaseAssignee.Query.EQ(e => e.CaseId, caseId));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                // We want deleted records here
                var query = CaseAssignee.Repository.Collection.Find(ands.Count > 0 ? CaseAssignee.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);
                query = SetCaseAssigneeQuerySortBy(criteria, query);

                result.Total = query.Count();
                var results = query.ToList();
                var userIds = results.Select(r => r.UserId).Distinct().ToList();
                var users = User.AsQueryable().Where(u => userIds.Contains(u.Id)).ToList();

                List<ListResult> resultPrimaryList = results.Select(e => MapCaseAssigneeToListResult(e, users)).ToList();

                result.Results = resultPrimaryList;
                return result;
            }
        }

        private static MongoCursor<CaseAssignee> SetCaseAssigneeQuerySortBy(ListCriteria criteria, MongoCursor<CaseAssignee> query)
        {
            if (!string.IsNullOrWhiteSpace(criteria.SortBy))
            {
                if (criteria.SortBy.ToLower() == "createddate")
                {
                    query = query.SetSortOrder(criteria.SortDirection == Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending("cdt")
                        : SortBy.Descending("cdt"));
                }
                else
                {
                    query = query.SetSortOrder(criteria.SortDirection == Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending(criteria.SortBy)
                        : SortBy.Descending(criteria.SortBy));
                }
            }

            return query;
        }

        /// <summary>
        /// Maps the case assignee to list result.
        /// </summary>
        /// <param name="assignee">The assignee.</param>
        /// <param name="users">The users.</param>
        /// <returns></returns>
        private ListResult MapCaseAssigneeToListResult(CaseAssignee assignee, List<User> users)
        {
            ListResult result;
            result = new ListResult()
                .Set("Id", assignee.Id)
                .Set("CaseId", assignee.CaseId)
                .Set("Category", "Case Assigned")
                .Set("CreatedDate",assignee.CreatedDate)
                .Set("ModifiedDate", assignee.ModifiedDate)
                .Set("ModifiedBy", (assignee.ModifiedBy ?? assignee.CreatedBy ?? User.System).DisplayName)
                .Set("Description", $"Case was re/assigned to {users.FirstOrDefault(u => u.Id == assignee.UserId)?.DisplayName}{(assignee.Code == null ? " as the primary case assignee." : $" as the {assignee.Code} case assignee.")}");
            return result;
        }
    }
}
