﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService
    {
        /// <summary>
        /// Reopens the case.
        /// </summary>
        /// <param name="thisCase">The case to reopen.</param>
        /// <param name="relapse">Relapse details</param>
        /// <returns></returns>
        public LeaveOfAbsence ReopenCase(Case thisCase, Relapse relapse)
        {
            LeaveOfAbsence loa;
            using (new InstrumentationContext("CaseService.ReopenCase"))
            {
                if (thisCase == null)
                {
                    throw new ArgumentNullException("thisCase");
                }
                foreach (CaseSegment cs in thisCase.Segments)
                {
                    cs.Status = CaseStatus.Open;
                }
                thisCase.EndDate = relapse.EndDate;
                thisCase.Status = CaseStatus.Open;
                using (var eligiblityService = new EligibilityService())
                {
                    loa = eligiblityService.RunEligibility(thisCase, relapse);
                }
            }
            return loa;
        }

        /// <summary>
        /// Get the list of relapse for the particular case 
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public List<Relapse> GetRelapses(string caseId)
        {
            return Relapse.AsQueryable().Where(r => r.CaseId == caseId).ToList();
        }

        /// <summary>
        /// Update the Relpase case
        /// </summary>
        /// <param name="myCase"></param>
        /// <param name="relapse"></param>
        /// <returns></returns>
        public Case UpdateRelapse(Case myCase, Relapse relapse)
        {
            if (myCase.EndDate.HasValue)
            {
                var times = new LeaveOfAbsence() { Case = myCase, Employee = myCase.Employee, Employer = myCase.Employer, Customer = myCase.Customer }
                    .MaterializeSchedule(myCase.EndDate.Value.AddDays(1), myCase.EndDate.Value.AddDays(60), true);
                DateTime rtwDate = myCase.EndDate.Value;
                if (times != null)
                {
                    foreach (var t in times.OrderBy(d => d.SampleDate))
                        if (t.TotalMinutes.HasValue && t.TotalMinutes.Value > 0)
                        {
                            rtwDate = t.SampleDate;
                            break;
                        }
                }
                myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, rtwDate);
            }

            if (relapse != null)
            {
                myCase.SetCaseEvent(CaseEventType.Relapse, relapse.StartDate);
                relapse.Save();
                myCase.CurrentRelapseId = relapse.Id;
            }
            // no harm to remove meta data to all the segment, however only new segment will have this tag
            myCase.Segments.ForEach(p => p.Metadata.Remove("UnSaved"));
            if (myCase.WorkRelated != null && myCase.EndDate.HasValue)
            {
                myCase.WorkRelated.DaysAwayFromWork = CalculateDaysAwayFromWork(myCase);
                myCase.WorkRelated.DaysOnJobTransferOrRestriction = CalculateDaysOnJobTransferOrRestriction(myCase);
            }
            UpdateCase(myCase);
            if (relapse != null)
            {
                myCase.WfOnRelapse(relapse);
            }
            return myCase;
        }
    }
}
