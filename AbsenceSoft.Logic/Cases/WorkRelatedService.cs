﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Rendering;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AT.Common.Rendering.Helper;
using AbsenceSoft.Logic.Embedded;
using AbsenceSoft.Logic.WorkRelated;

namespace AbsenceSoft.Logic.Cases
{
    public class WorkRelatedService : LogicService
    {

        public WorkRelatedService()
        {

        }

        public WorkRelatedService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Generates the OSHA Form 301, Injury and Illness Incident Report.
        /// </summary>
        /// <param name="theCase">The case to generate the report for.</param>
        /// <returns>The <see cref="T:Attachment"/> for the case representing the OSHA Form 301 output.</returns>
        public Attachment GenerateOSHAForm301(Case theCase)
        {
            // Determine if we can even do this
            if (theCase == null
                || theCase.Employee == null
                || theCase.CustomerId == null
                || theCase.EmployerId == null
                || theCase.WorkRelated == null)
                return null;

            // Get the core PDF document
            var pdf = CreateOSHAForm301(theCase);

            // Double check we're still good
            if (pdf == null || pdf.Length == 0)
                return null;

            // Build the attachment
            Attachment att = new Attachment()
            {
                CustomerId = theCase.CustomerId,
                EmployerId = theCase.EmployerId,
                EmployeeId = theCase.Employee.Id,
                CaseId = theCase.Id,
                AttachmentType = AttachmentType.Documentation,
                ContentLength = pdf.Length,
                ContentType = "application/pdf",
                Description = "OSHA’s Form 301; Injury and Illness Incident Report",
                File = pdf,
                FileName = "OSHA Form 301.pdf",
                Public = false,
                CreatedBy = CurrentUser,
                ModifiedBy = CurrentUser
            };

            // Create the attachment, upload the document and be happy
            return new AttachmentService().Using(s => s.CreateAttachment(att));
        }

        public IEnumerable<int> GetOshaReportingYearsForEmployer()
        {
            return new DateTime(2000, 1, 1).ToMidnight().AllYearsInRange(DateTime.UtcNow.ToMidnight().AddYears(1));
        }

        /// <summary>
        /// Generates employer locations for OSHA reports
        /// </summary>
        /// <param name="employerId">The employer whose location is to be generated</param>
        /// <returns> List of employer location objects</returns>
        public IEnumerable<Organization> GetOshaReportingLocationsForEmployer()
        {
            return Organization.AsQueryable()
                .Where(o => o.EmployerId == EmployerId && o.TypeCode == OrganizationType.OfficeLocationTypeCode)
                .OrderBy(o => o.Name).ToList();
        }


        /// <summary>
        /// Generates the OSHA Form 301, Injury and Illness Incident Report.
        /// </summary>
        /// <param name="query">The case to generate the report for.</param>
        /// <returns>The <see cref="T:Attachment"/> for the case representing the OSHA Form 301 output.</returns>
        public byte[] GenerateOSHAForm300(int year, string employerId, string officeLocation = null)
        {
            var query = Form300Query(year, employerId, officeLocation);
            var employer = Employer.GetById(employerId);

            if (employer == null)
            {
                return null;
            }

            // Get the core PDF document
            var pdf = CreateOSHAForm300(query, year, employer, officeLocation);

            // Double check we're still good
            if (pdf == null || pdf.Length == 0)
                return null;

            // Create the attachment, upload the document and be happy
            return pdf;
        }

        public byte[] GenerateOSHAForm300a(int year, string employerId, string officeLocation = null)
        {
            var query = Form300Query(year, employerId, officeLocation);
            var employer = Employer.GetById(employerId);

            if (employer == null)
            {
                return null;
            }

            // Get the core PDF document
            var pdf = CreateOSHAForm300a(query, year, employer, officeLocation);

            // Double check we're still good
            if (pdf == null || pdf.Length == 0)
                return null;

            // Create the attachment, upload the document and be happy
            return pdf;
        }
        /// <summary>
        /// Generates the osha form300a preview.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="officeLocation">The office location.</param>
        /// <returns></returns>
        public Osha300AFormPreview GenerateOshaForm300aPreview(int year, string employerId, string officeLocation = null)
        {
            var query = Form300Query(year, employerId, officeLocation);
            var employer = Employer.GetById(employerId);

            if (employer == null)
            {
                return null;
            }

            if (query.Count == 0)
            {
                return null;
            }
            var Osha300aForm = GetForm300ASummaryReportPreview(query, year, employer, officeLocation);

            return Osha300aForm;
        }

        /// <summary>
        /// Generates the MI Form 100.
        /// </summary>
        /// <param name="theCase">The case to generate the report for.</param>
        /// <returns>The <see cref="T:Attachment"/> for the case representing the MI Form 100 output.</returns>
        public Attachment GenerateForm100(Case theCase)
        {
            // Determine if we can even do this
            if (theCase == null
                || theCase.Employee == null
                || theCase.CustomerId == null
                || theCase.EmployerId == null
                || theCase.WorkRelated == null)
                return null;

            // Get the core PDF document
            var pdf = CreateForm100(theCase);

            // Double check we're still good
            if (pdf == null || pdf.Length == 0)
                return null;

            // Build the attachment
            Attachment att = new Attachment()
            {
                CustomerId = theCase.CustomerId,
                EmployerId = theCase.EmployerId,
                EmployeeId = theCase.Employee.Id,
                CaseId = theCase.Id,
                AttachmentType = AttachmentType.Documentation,
                ContentLength = pdf.Length,
                ContentType = "application/pdf",
                Description = "WC Form 100; Injury and Illness Incident Report",
                File = pdf,
                FileName = "Form 100.pdf",
                Public = false,
                CreatedBy = CurrentUser,
                ModifiedBy = CurrentUser
            };

            // Create the attachment, upload the document and be happy
            return new AttachmentService().Using(s => s.CreateAttachment(att));
        }

        /// <summary>
        /// Creates the osha form 301 PDF.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        internal byte[] CreateOSHAForm301(Case theCase)
        {
            // Get the stamp collection that will populate the form
            var stamps = BuildOSHAForm301Stamps(theCase);

            // Get the core PDF document
            var pdf = Embedded.OSHA.OSHA301;

            // Stamp the PDF and get the new populated copy
            pdf = Pdf.StampPdf(pdf, stamps);

            return pdf;
        }

        /// <summary>
        /// Creates the osha form 300 PDF.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        internal byte[] CreateOSHAForm300(IEnumerable<Case> query, int year, Employer employer, string officeLocation = null)
        {
            // Get the master form
            var oshaForm300 = Embedded.OSHA.OSHA300_300a;

            // Start with an empty report template.
            byte[] form300Template = Pdf.ExtractPages(oshaForm300, 7, 7);
            var form300Pages = CreateForm300Pages(form300Template, year, query, employer, officeLocation).ToList();
            var report = Pdf.MergePdfDocuments(form300Pages.ToArray());
            return report;
        }

        /// <summary>
        /// Creates the osha form 300a PDF.
        /// </summary>
        /// <param name="auery">List of cases that match the criteria for the report.</param>
        /// <returns></returns>
        internal byte[] CreateOSHAForm300a(IEnumerable<Case> query, int year, Employer employer, string officeLocation = null)
        {
            // Get the master form
            var oshaForm300 = Embedded.OSHA.OSHA300_300a;
            // Start with an empty report template.
            byte[] form300ASummaryTemplate = Pdf.ExtractPages(oshaForm300, 8, 8);
            var form300_APage = CreateForm300ASummaryReport(form300ASummaryTemplate, query, year, employer, officeLocation);
            var report = Pdf.MergePdfDocuments(new[] { form300_APage }.ToArray());
            return report;
        }

        /// <summary>
        /// Creates the WC form 100 PDF.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        internal byte[] CreateForm100(Case theCase)
        {
            // Get the core PDF document
            var pdf = Embedded.OSHA.FORM100;

            pdf = BuildForm100Fields(theCase, pdf);

            return pdf;
        }

        #region OSHA Form 301

        /// <summary>
        /// Builds the osha form 301 stamps from the provided case to populate the form.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        internal IEnumerable<PdfStamp> BuildOSHAForm301Stamps(Case theCase)
        {

            
            if (theCase.AssignedTo != null)
            {
                // Completed by
                yield return new PdfStamp() { Value = theCase.AssignedTo.DisplayName, FontSize = 8f, X = 80.34f, Y = 130f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

                // Job title
                yield return new PdfStamp() { Value = theCase.AssignedTo.JobTitle, FontSize = 8f, X = 50f, Y = 105f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };

                // Phone, CurrentUser.ContactInfo.WorkPhone
                if (!string.IsNullOrEmpty(theCase.AssignedTo.ContactInfo.WorkPhone))
                    yield return new PdfStamp() { Value = String.Format(theCase.AssignedTo.ContactInfo.WorkPhone.Length == 9 ? "{0:  ###    ###     ####}" : "{0: ###   ###     ####}", Int64.Parse(theCase.AssignedTo.ContactInfo.WorkPhone)), FontSize = 8f, X = 50f, Y = 77f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };
                                
            }

            // Date Completed
            yield return new PdfStamp() { Value = (theCase.GetCaseEventDate(CaseEventType.CaseCreated) ?? theCase.CreatedDate), Format = "{0:MM}", FontSize = 8f, X = 202f, Y = 77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
            yield return new PdfStamp() { Value = (theCase.GetCaseEventDate(CaseEventType.CaseCreated) ?? theCase.CreatedDate), Format = "{0:dd}", FontSize = 8f, X = 228f, Y = 77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
            yield return new PdfStamp() { Value = (theCase.GetCaseEventDate(CaseEventType.CaseCreated) ?? theCase.CreatedDate), Format = "{0:yyyy}", FontSize = 8f, X = 249.16f, Y = 77f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };


            if (theCase.Employee != null)
            {
                // Employee Full Name
                // Hiding name if Health Care Privacy option is checked
                string employeeName = theCase.Employee.FullName;
                if (theCase.WorkRelated != null && theCase.WorkRelated.HealthCarePrivate != null && theCase.WorkRelated.HealthCarePrivate == true)
                {
                    employeeName = "privacy case";
                }
                yield return new PdfStamp() { Value = employeeName, FontSize = 8f, X = 338.34f, Y = 467f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };
                // Employee Address
                yield return new PdfStamp() { Value = theCase.Employee.Info.Address.StreetAddress, FontSize = 8f, X = 325.47f, Y = 442.34f, MaxHeight = 24f, MaxWidth = 226.7f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.Info.Address.City, FontSize = 8f, X = 319.28f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 130.03f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.Info.Address.State, FontSize = 8f, X = 471.09f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 29.27f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.Info.Address.PostalCode, FontSize = 8f, X = 518.06f, Y = 418.85f, MaxHeight = 24f, MaxWidth = 45.61f, AutoFit = true };
                // DoB
                yield return new PdfStamp() { Value = theCase.Employee.DoB, Format = "{0:MM}", FontSize = 8f, X = 347.6f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.DoB, Format = "{0:dd}", FontSize = 8f, X = 375.6f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.DoB, Format = "{0:yyyy}", FontSize = 8f, X = 394.76f, Y = 396.77f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };
                // Hire Date
                yield return new PdfStamp() { Value = theCase.Employee.HireDate, Format = "{0:MM}", FontSize = 8f, X = 346.21f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.HireDate, Format = "{0:dd}", FontSize = 8f, X = 374.21f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.Employee.HireDate, Format = "{0:yyyy}", FontSize = 8f, X = 393.37f, Y = 384.14f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };
                // Male?
                yield return new PdfStamp() { Value = theCase.Employee.Gender == Gender.Male, FontSize = 8f, X = 303.99f, Y = 374.78f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                // Female?
                yield return new PdfStamp() { Value = theCase.Employee.Gender == Gender.Female, FontSize = 8f, X = 303.99f, Y = 361.6f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
            }
            // Ensure Work related
            if (theCase.WorkRelated != null)
            {
                // Provider Info
                if (theCase.WorkRelated.Provider != null && theCase.WorkRelated.Provider.Contact != null)
                {
                    // Physician Name (or HCP)
                    yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.FullName, FontSize = 8f, X = 303.99f, Y = 251.9f, MaxHeight = 24f, MaxWidth = 250.77f, AutoFit = true };
                    // Facility Name
                    yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.CompanyName, FontSize = 8f, X = 338.34f, Y = 212.94f, MaxHeight = 24f, MaxWidth = 218f, AutoFit = true };
                    // Provider Address
                    if (theCase.WorkRelated.Provider.Contact.Address != null)
                    {
                        yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.Address.StreetAddress, FontSize = 8f, X = 325.47f, Y = 189.9f, MaxHeight = 24f, MaxWidth = 226.7f, AutoFit = true };
                        yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.Address.City, FontSize = 8f, X = 319.28f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 130.03f, AutoFit = true };
                        yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.Address.State, FontSize = 8f, X = 471.09f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 29.27f, AutoFit = true };
                        yield return new PdfStamp() { Value = theCase.WorkRelated.Provider.Contact.Address.PostalCode, FontSize = 8f, X = 518.06f, Y = 166.3f, MaxHeight = 24f, MaxWidth = 45.61f, AutoFit = true };
                    }
                }
                //AT-6337 - OSHA Form 301 not checking the "No" check-box for Emergency Room or Hospitalized Overnight
                // Was employee treated in the emergency room (yes and no), No box/option to be checked on the 301 in this scenario or any scenario other than "Yes"
                yield return new PdfStamp() { Value = theCase.WorkRelated.EmergencyRoom == true, FontSize = 8f, X = 303.99f, Y = 141.57f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.EmergencyRoom != true, FontSize = 8f, X = 303.99f, Y = 130.39f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                // Was the employee hospitalized overnight as an in-patient? (yes and no), No box/option to be checked on the 301 in this scenario or any scenario other than "Yes"
                yield return new PdfStamp() { Value = theCase.WorkRelated.HospitalizedOvernight == true, FontSize = 8f, X = 303.99f, Y = 97.58f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.HospitalizedOvernight != true, FontSize = 8f, X = 303.99f, Y = 85.4f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                // Case Number
                yield return new PdfStamp() { Value = theCase.CaseNumber, FontSize = 8f, X = 699.08f, Y = 466.45f, MaxHeight = 24f, MaxWidth = 90.27f, AutoFit = true };
                // Date of Illness/Injury
                yield return new PdfStamp() { Value = theCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:MM}", FontSize = 8f, X = 703.08f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:dd}", FontSize = 8f, X = 727.08f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 24f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.IllnessOrInjuryDate, Format = "{0:yyyy}", FontSize = 8f, X = 753.24f, Y = 451.09f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };
                // Time employee began work
                yield return new PdfStamp() { Value = theCase.WorkRelated.TimeEmployeeBeganWork, FontSize = 8f, X = 699.08f, Y = 436.27f, MaxHeight = 24f, MaxWidth = 76.27f, AutoFit = true };
                // Time of event
                yield return new PdfStamp() { Value = theCase.WorkRelated.TimeOfEvent, FontSize = 8f, X = 699.08f, Y = 417.07f, MaxHeight = 24f, MaxWidth = 76.27f, AutoFit = true };
                // Time of event cannot be determined?
                yield return new PdfStamp() { Value = !theCase.WorkRelated.TimeOfEvent.HasValue, FontSize = 8f, X = 815.21f, Y = 426.26f, MaxHeight = 16f, MaxWidth = 16f, AutoFit = true };
                // What was they doing before the incident occurred?
                yield return new PdfStamp() { Value = theCase.WorkRelated.ActivityBeforeIncident, FontSize = 8f, X = 593.17f, Y = 334.33f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };
                // What happened?
                yield return new PdfStamp() { Value = theCase.WorkRelated.WhatHappened, FontSize = 8f, X = 593.17f, Y = 255.32f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };
                // What was the injury or illness?
                yield return new PdfStamp() { Value = theCase.WorkRelated.InjuryOrIllness, FontSize = 8f, X = 593.17f, Y = 175.75f, MaxHeight = 50f, MaxWidth = 384f, AutoFit = true };
                // What object or substance harmed the employee?
                yield return new PdfStamp() { Value = theCase.WorkRelated.WhatHarmedTheEmployee, FontSize = 8f, X = 593.17f, Y = 92.34f, MaxHeight = 60f, MaxWidth = 384f, AutoFit = true };
                // Date of death!
                yield return new PdfStamp() { Value = theCase.WorkRelated.DateOfDeath, Format = "{0:MM}", FontSize = 8f, X = 829.67f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 18f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.DateOfDeath, Format = "{0:dd}", FontSize = 8f, X = 857.67f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 18f, AutoFit = true };
                yield return new PdfStamp() { Value = theCase.WorkRelated.DateOfDeath, Format = "{0:yyyy}", FontSize = 8f, X = 876.83f, Y = 73.57f, MaxHeight = 24f, MaxWidth = 30f, AutoFit = true };
            }
        }

        #endregion

        #region OSHA Form 300 and 300a


        /// <summary>
        /// Creates all the pages for this report. The count of pages will be 1 to N depending on the number of reported cases.
        /// </summary>
        /// <param name="form300PageTemplate"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<byte[]> CreateForm300Pages(byte[] form300PageTemplate, int year, IEnumerable<Case> query, Employer employer, string officeLocation = null)
        {
            // There are 13 rows available for each page. Each reportable case can take up multiple rows, but will always consume at least one. 
            // (Based on the length of WhatHappened field).
            // Once the total number of rows are determined, this will be used to determine how many pages are needed.
            var form300Rows = CreateForm300RowStamps(query).ToList();

            // Determine how many total pages there will be in this report.
            int lastPageRowCount, rowCountPerPage = 13;
            int extraPageCount = Math.DivRem(form300Rows.Count - rowCountPerPage, rowCountPerPage, out lastPageRowCount);
            // Fix for IMP-93, It was happening when you have exact number of row in pdf to display total number of rows is specified 13 row per page so if record is 13 / 26 / 39 / 52 etc
            if (form300Rows.Count % rowCountPerPage == 0 && form300Rows.Count > 0 && form300Rows.Count != rowCountPerPage)
            {
                extraPageCount = 0;
                lastPageRowCount = rowCountPerPage;
            }
            if (extraPageCount < 0)
            {
                extraPageCount = 0;
            }
            int numberOfRowsForFirstPage = form300Rows.Count > rowCountPerPage ? rowCountPerPage : form300Rows.Count;
            int totalPages = (1 + extraPageCount + (lastPageRowCount > 0 ? 1 : 0));


            Organization organization = null;
            if (officeLocation != null)
            {
                organization = Organization.AsQueryable().FirstOrDefault(r => r.Code == officeLocation && r.EmployerId == employer.Id);
            }
            var address = GetLocationAddress(employer, organization);
            var EstablishmentName = organization == null ? employer.Name : organization.Name;

            //Create the header stamps for each page.
            var headerStampsForEachPage = CreateForm300PageHeaderStamps(year, EstablishmentName, address.City, address.State).ToList();

            // Create the page of rows.
            var pageRows = form300Rows.Skip(0).Take(numberOfRowsForFirstPage).ToList();
            var pageRowStamps = pageRows.SelectMany(row => row.Item4).ToList();
            var pageIndex = 1;

            // Create the page footer totals.
            var typeOfInuryPageTotalStamps = new List<PdfStamp>();
            var workClassificationPageTotalStamps = new List<PdfStamp>();
            var daysAwayStamps = new List<PdfStamp>();
            // Combine all the stamps for the page.
            var allPageStamps = new List<PdfStamp>();
            var pageNumberStamps = CreateForm300PageNumberFooterStamps(pageIndex, totalPages).ToList();
            if (pageIndex == totalPages)
            {
                typeOfInuryPageTotalStamps = CreateForm300WorkRelatedTypeOfInjuryPageTotalStamps(query).ToList();
                workClassificationPageTotalStamps = CreateForm300WorkRelatedCaseClassificationPageTotal(query).ToList();
                daysAwayStamps = CreateForm300DaysAwayFromWorkPageTotalStamps(query).ToList();
                // Combine all the stamps for the page.
                allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(typeOfInuryPageTotalStamps).Concat(workClassificationPageTotalStamps)
                                                    .Concat(daysAwayStamps).Concat(pageNumberStamps).ToList();
            }
            else
            {
                // Combine all the stamps for the page.
                allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(pageNumberStamps).ToList();
            }

            // Outpout the first page of the report.
            yield return Pdf.StampPdf(form300PageTemplate, allPageStamps.Where(s => s != null));

            // Do the same for each page, starting at page 2.
            var pages = Enumerable.Range(1, extraPageCount).Select(pageNbr =>
            {
                pageRows = form300Rows.Skip(pageNbr * rowCountPerPage).Take(rowCountPerPage).ToList();
                pageRowStamps = pageRows.SelectMany(s => s.Item4).ToList();
                pageNumberStamps = CreateForm300PageNumberFooterStamps(pageNbr + 1, totalPages).ToList();
                if (pageIndex == totalPages)
                {
                    typeOfInuryPageTotalStamps = CreateForm300WorkRelatedTypeOfInjuryPageTotalStamps(query).ToList();
                    workClassificationPageTotalStamps = CreateForm300WorkRelatedCaseClassificationPageTotal(query).ToList();
                    daysAwayStamps = CreateForm300DaysAwayFromWorkPageTotalStamps(query).ToList();
                    allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(typeOfInuryPageTotalStamps).Concat(workClassificationPageTotalStamps)
                                                        .Concat(daysAwayStamps).Concat(pageNumberStamps).ToList();
                }
                else
                {
                    allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(pageNumberStamps).ToList();
                }

                return Pdf.StampPdf(form300PageTemplate, allPageStamps);
            });

            foreach (var page in pages)
            {
                pageIndex++;
                yield return page;
            }

            // Finally, do the same for the last page, if any.
            if (lastPageRowCount > 0)
            {
                pageIndex++;
                pageRows = form300Rows.Skip((pageIndex - 1) * rowCountPerPage).Take(lastPageRowCount).ToList();
                pageRowStamps = pageRows.SelectMany(s => s.Item4).ToList();
                pageNumberStamps = CreateForm300PageNumberFooterStamps(pageIndex, totalPages).ToList();
                if (pageIndex == totalPages)
                {
                    typeOfInuryPageTotalStamps = CreateForm300WorkRelatedTypeOfInjuryPageTotalStamps(query).ToList();
                    workClassificationPageTotalStamps = CreateForm300WorkRelatedCaseClassificationPageTotal(query).ToList();
                    daysAwayStamps = CreateForm300DaysAwayFromWorkPageTotalStamps(query).ToList();
                    allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(typeOfInuryPageTotalStamps).Concat(workClassificationPageTotalStamps)
                                        .Concat(daysAwayStamps).Concat(pageNumberStamps).ToList();
                }
                else
                {
                    allPageStamps = headerStampsForEachPage.Concat(pageRowStamps).Concat(pageNumberStamps).ToList();
                }

                yield return Pdf.StampPdf(form300PageTemplate, allPageStamps);
            }
        }

        /// <summary>
        /// Creates the stamps at the top of each page.
        /// </summary>
        /// <param name="year">The year of the report</param>
        /// <param name="employerName">The name of the employer.</param>
        /// <param name="city">The city of the employer</param>
        /// <param name="state">The state of the employer</param>
        /// <returns></returns>
        private IEnumerable<PdfStamp> CreateForm300PageHeaderStamps(int year, string employerName, string city, string state)
        {
            yield return new PdfStamp() { Value = year.ToString().Substring(2, 1), FontSize = 16f, Y = 612f - 51f, X = 916f, MaxWidth = 24f, MaxHeight = 18f, AutoFit = true };
            yield return new PdfStamp() { Value = year.ToString().Substring(3, 1), FontSize = 16f, Y = 612f - 51f, X = 932f, MaxWidth = 24f, MaxHeight = 18f, AutoFit = true };
            yield return new PdfStamp() { Value = employerName, FontSize = 8.0f, X = 852f, Y = 612 - 112f, MaxHeight = 21f, MaxWidth = 984f - 852f, AutoFit = true };
            yield return new PdfStamp() { Value = city, FontSize = 8f, X = 804f, Y = 612f - 130f, MaxHeight = 21f, MaxWidth = 904f - 804f, AutoFit = true };
            yield return new PdfStamp() { Value = state, FontSize = 8f, X = 930f, Y = 612f - 130f, MaxHeight = 21f, MaxWidth = 984f - 930f, AutoFit = true };
        }

        /// <summary>
        /// Creates the Page number stamps at the bottom of each page. In the form of 1 of N.
        /// </summary>
        /// <param name="pageX">current page index being rendered.</param>
        /// <param name="ofYPages">total pages in the report.</param>
        /// <returns></returns>
        private IEnumerable<PdfStamp> CreateForm300PageNumberFooterStamps(int pageX, int ofYPages)
        {
            float yCoord = 612f - 580.0f;
            yield return new PdfStamp() { Value = pageX.ToString(), FontSize = 8f, X = 798f, Y = yCoord, MaxHeight = 21f, MaxWidth = 2 * 8f, AutoFit = true };
            yield return new PdfStamp() { Value = ofYPages.ToString(), FontSize = 8f, X = 822f, Y = yCoord, MaxHeight = 21f, MaxWidth = 2 * 8f, AutoFit = true };
        }

        /// <summary>
        /// Creates all the row stamps required for this report based on the query.
        /// </summary>
        /// <param name="form300PageTemplate">The pdf document to use as a template</param>
        /// <param name="query">The reportable OSHA cases. (Case has a Workrelated record and that record is also Reportable</param>
        /// <returns>The pdf document containing the stamped data.</returns>
        private IEnumerable<Tuple<Case, int, int, IEnumerable<PdfStamp>>> CreateForm300RowStamps(IEnumerable<Case> query)
        {
            // Quick reference to the 13 y-coord locations. 
            float[] rowYCoordinates = new[] { 355.0f, 332f, 311f, 290f, 269f, 246f, 224f, 203f, 182f, 161f, 140f, 119f, 97f };

            int rowIndex = -1;
            int totalRowsAvailablePerPage = 13;

            //Create a counter that will show up in column A on the report.
            int colACaseNumber = 0;
            foreach (var reportableCase in query)
            {
                int index = 0;
                // Determine how many report lines this reportable case will consume. This is based on the length of the what happened field.
                // If WhatHappend.Length <= 40, fits in single line with font = 8.
                // If WhatHappend.Length > 40 and <=54, fits in single line with font = 6.
                // If WhatHappend.Length > 54, wrap to the next line with font = 6.
                // Each yield return is a "row" on the report.
                int whatHappendMaxCharacterCount = 40;
                float whatHappendFontSize = 8f;
                string whatHappened = String.IsNullOrEmpty(reportableCase.WorkRelated.WhatHappened) ? string.Empty : reportableCase.WorkRelated.WhatHappened.Replace("\r\n", " ");
                if (reportableCase.WorkRelated.WhatHappened != null && whatHappened.Length > whatHappendMaxCharacterCount)
                {
                    whatHappendFontSize = 6f;
                    whatHappendMaxCharacterCount = 46;
                }
                List<string> whatHappenedList = whatHappened.SplitStringOnMaxLength(whatHappendMaxCharacterCount);
                int whatHappenedCount = whatHappenedList.Count;

                // Determine how many report lines this reportable case will consume. This is based on the length of the where occurred field.
                // If WhereOccurred.Length <= 15, fits in single line with font = 8.
                // If WhereOccurred.Length > 15 and <=20, fits in single line with font = 6.
                // If WhereOccurred.Length > 20, wrap to the next line with font = 6.
                // Each yield return is a "row" on the report.
                int whereOccurredMaxCharacterCount = 15;
                float whereOccurredFontSize = 8f;
                string whereOccurred = String.IsNullOrEmpty(reportableCase.WorkRelated.WhereOccurred) ? string.Empty : reportableCase.WorkRelated.WhereOccurred;
                if (reportableCase.WorkRelated.WhereOccurred != null && whereOccurred.Length > whereOccurredMaxCharacterCount)
                {
                    whereOccurredFontSize = 6f;
                    whereOccurredMaxCharacterCount = 18;
                }
                List<string> whereOccurredList = whereOccurred.SplitStringOnMaxLength(whereOccurredMaxCharacterCount);
                int whereOccurredCount = whereOccurredList.Count;

                //Another row, advance the row index. the row index is used to determine the ycoord.
                rowIndex++;
                colACaseNumber++;

                float caseNumberFontSize = 8f;
                if (reportableCase.CaseNumber.Length > 7)
                {
                    caseNumberFontSize = 7f;
                }
                if (reportableCase.CaseNumber.Length > 10)
                {
                    caseNumberFontSize = 6f;
                }

                #region First Row for each Reportable Case

                // Create the first row of stamps for this case.
                // Employee Details, Columns A-L and 1-6
                var daysOnJobTransferOrRestriction = GetDaysOnJobTransferOrRestrictionForOshaReports(reportableCase);                
                // Hiding name if Health Care Privacy option is checked
                string employeeName = string.Empty;
                if (reportableCase.Employee == null)
                {
                    employeeName = "NO NAME ON FILE";
                }
                else
                {
                    if (reportableCase.WorkRelated != null && reportableCase.WorkRelated.HealthCarePrivate != null && reportableCase.WorkRelated.HealthCarePrivate == true)
                    {
                        employeeName = "privacy case";
                    }
                    else
                    {
                        employeeName = reportableCase.Employee.FullName;
                    }
                }

                int employeeNameMaxCharacterCount = 18;
                float employeeNameFontSize = 8f;
                if (employeeName.Length > employeeNameMaxCharacterCount)
                {
                    employeeNameFontSize = 6f;
                    employeeNameMaxCharacterCount = 24;
                }
                List<string> employeeNameList = employeeName.SplitStringOnMaxLength(employeeNameMaxCharacterCount);
                int employeeNameCount = employeeNameList.Count;

                var firstRow = Tuple.Create(reportableCase, colACaseNumber, rowIndex,
                                (new[]
                                {
                                    //Col A.
                                    new PdfStamp() { Value = reportableCase.CaseNumber,
                                        FontSize = caseNumberFontSize, X = 10f, MaxWidth=44f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, MaxHeight=42.0f, AutoFit = true },
                                    //Col B.
                                    new PdfStamp() { Value = employeeNameList[index],
                                        FontSize = employeeNameFontSize, X = 55f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, MaxWidth=160f-55f,  MaxHeight=21.0f,AutoFit = true },
                                    //Col C.
                                    new PdfStamp() { Value = String.IsNullOrEmpty(reportableCase.Employee.JobTitle) ? string.Empty : string.Concat(reportableCase.Employee.JobTitle.Take(13)),
                                        FontSize = 8f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 179f, MaxWidth=230f-179f, MaxHeight=21.0f,AutoFit = true },
                                    //Col D.
                                    new PdfStamp() { Value = reportableCase.WorkRelated.IllnessOrInjuryDate.GetValueOrDefault().Month.ToString("00"),
                                        FontSize = 8f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 4f, X = 240f, MaxWidth=2*8f, MaxHeight=21.0f,AutoFit = true },
                                    //Col E.
                                    new PdfStamp() { Value = reportableCase.WorkRelated.IllnessOrInjuryDate.GetValueOrDefault().Day.ToString("00"),
                                        FontSize = 8f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 263f, MaxWidth=2*8f,  MaxHeight=21.0f,AutoFit = true },
                                    //Col F.
                                    new PdfStamp() { Value = string.IsNullOrEmpty(reportableCase.WorkRelated.WhereOccurred) ? string.Empty : whereOccurredList[index],
                                        FontSize = whereOccurredFontSize, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 303f, MaxWidth=380f-303f, MaxHeight=21.0f,AutoFit = true },
                                    //Col G.
                                    new PdfStamp() { Value = whatHappenedCount < 1 ? string.Empty : whatHappenedList[index],
                                        FontSize = whatHappendFontSize, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 405, MaxWidth=600f-405f,  MaxHeight=21.0f,AutoFit = true },
                                    //Cols G-J
                                    CreateForm300WorkRelatedClassificationCheckmarkStamp(reportableCase.WorkRelated.Classification, rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)]),
                                    //Col K.
                                    new PdfStamp() { Value = GetDaysAwayFromWorkForOshaReports(reportableCase),
                                        FontSize = 8f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 789, MaxWidth=3*8f, MaxHeight=21.0f, AutoFit = true },
                                    //Col L.
                                    new PdfStamp() { Value = daysOnJobTransferOrRestriction,
                                        FontSize = 8f, Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f, X = 828, MaxWidth=3*8f, MaxHeight=21.0f,AutoFit = true },
                                    //Col M. (1-6)
                                    CreateForm300WorkRelatedTypeOfInjuryCheckmarkStamp(reportableCase.WorkRelated.TypeOfInjury, rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)])
                                }).AsEnumerable());

                yield return firstRow;

                #endregion

                #region Add'l rows for the case if What Happened characters > 54 or Where Occurred characters > 20.

                // create a new row for any What Happened characters > 54. 
                // create a new row for any Where Occurred characters > 20. 
                // This consumes extraRowCountWhatHappened row's on the report.
                if (employeeNameCount > 1 || whatHappenedCount > 1 || whereOccurredCount > 1)
                {
                    int maxCount = (((whatHappenedCount > whereOccurredCount) && (whatHappenedCount > employeeNameCount)) ? whatHappenedCount-1 : (whereOccurredCount > employeeNameCount) ? whereOccurredCount-1 : employeeNameCount-1);
                    var extraWhatHappendRowStamps = Enumerable.Range(1, maxCount).Select(n =>
                    {
                        rowIndex++;
                        index++;
                        return Tuple.Create<Case, int, int, IEnumerable<PdfStamp>>(null, colACaseNumber, rowIndex, (new[] {
                            new PdfStamp()
                            {
                                Value = employeeNameCount < 1 || index >= employeeNameCount ? string.Empty : employeeNameList[index].Trim(),
                                FontSize = employeeNameFontSize,
                                X = 55f,
                                Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f,
                                MaxWidth = 160f-55f,
                                MaxHeight = 21.0f,
                                AutoFit = true
                            },
                            new PdfStamp()
                            {
                                Value = whatHappenedCount < 1 || index >= whatHappenedCount ? string.Empty : whatHappenedList[index].Trim(),
                                FontSize = whatHappendFontSize,
                                X = 405f,
                                Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f,
                                MaxWidth = 600f-405f,
                                MaxHeight = 21.0f,
                                AutoFit = true
                            },
                            new PdfStamp()
                            {
                                Value = whereOccurredCount < 1 ||  index >= whereOccurredCount ? string.Empty : whereOccurredList[index].Trim(),
                                FontSize = whereOccurredFontSize,
                                X = 303f,
                                Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 5f,
                                MaxWidth = 380f-303f,
                                MaxHeight = 21.0f,
                                AutoFit = true
                            }
                        }));
                    });

                    // Iterate thru each add'l row, if any.
                    foreach (var extraRow in extraWhatHappendRowStamps)
                    {
                        yield return extraRow;
                    }
                }

                #endregion

                #region The Last Row of the Report (if any) if What Happened characters > 54 or Where Occurred characters > 20.

                if (index < employeeNameCount - 1 || index < whatHappenedCount-1|| index < whereOccurredCount-1)
                {
                    index++;
                    rowIndex++; //The final row, so advance the rowIndex

                    yield return Tuple.Create<Case, int, int, IEnumerable<PdfStamp>>(null, colACaseNumber, rowIndex, (new[]
                    {
                        new PdfStamp()
                        {
                            Value = employeeNameCount < 1 || index >= employeeNameCount ? string.Empty : employeeNameList[index].Trim(),
                            FontSize = employeeNameFontSize,
                            X = 55f,
                            Y = rowYCoordinates[(rowIndex % totalRowsAvailablePerPage)] + 4f,
                            MaxWidth = 160f-55f,
                            MaxHeight = 21.0f,
                            AutoFit = true
                        },
                        new PdfStamp()
                        {
                            Value = whatHappenedCount < 1 || index >= whatHappenedCount ? string.Empty : whatHappenedList[index].Trim(),
                            FontSize = whatHappendFontSize,
                            X = 405f,
                            Y = rowYCoordinates[(rowIndex) % 13] + 4f,
                            MaxWidth = 600f-405f,
                            MaxHeight = 21.0f,
                            AutoFit = true
                        },
                        new PdfStamp()
                        {
                            Value = whereOccurredCount < 1 ||  index >= whereOccurredCount ? string.Empty : whereOccurredList[index].Trim(),
                            FontSize = whereOccurredFontSize,
                            X = 303f,
                            Y = rowYCoordinates[(rowIndex) % 13] + 4f,
                            MaxWidth = 380f-303f,
                            MaxHeight = 21.0f,
                            AutoFit = true
                        }
                    }).AsEnumerable());
                }

                #endregion
            }
        }

        /// <summary>
        /// Gets the days away from work for OSHA 300 and 300a Reports. This has to calculate
        /// the totals based on both Days Away from Work and Days Restricted.
        /// </summary>
        /// <param name="reportableCase"></param>
        /// <returns></returns>
        private int GetDaysAwayFromWorkForOshaReports(Case reportableCase)
        {
            // OSHA only requires/needs the first 180 days reported for any case
            //  this means that Col K comes first up to 180 and Col L comes second
            //  of either L (if K + L < 180), otherwise 180 - K = L.
            int daysAway = reportableCase.WorkRelated?.OverrideDaysAwayFromWork ?? new CaseService().Using(cs => cs.CalculateDaysAwayFromWork(reportableCase));
            if (daysAway > 180)
            {
                daysAway = 180;
            }
            return daysAway;
        }

        private int GetDaysOnJobTransferOrRestrictionForOshaReports(Case reportableCase)
        {
            // OSHA only requires/needs the first 180 days reported for any case
            //  this means that Col K comes first up to 180 and Col L comes second
            //  of either L (if K + L < 180), otherwise 180 - K = L.
            int daysAway = reportableCase.WorkRelated?.OverrideDaysAwayFromWork ?? new CaseService().Using(cs => cs.CalculateDaysAwayFromWork(reportableCase));
            int daysRestricted = reportableCase.WorkRelated?.DaysOnJobTransferOrRestriction ?? new CaseService().Using(cs => cs.CalculateDaysOnJobTransferOrRestriction(reportableCase));
            if (daysAway + daysRestricted > 180)
            {
                if (daysAway > 180)
                {
                    daysAway = 180;
                }
                daysRestricted = 180 - daysAway;
                if (daysRestricted < 0)
                {
                    daysRestricted = 0;
                }
            }
            return daysRestricted;
        }

        /// <summary>
        /// Based on the work related classification, create a stamp with the correct x-y coordinates
        /// </summary>
        /// <param name="key">The value required to create the stamp</param>
        /// <param name="yCoord">The y-coordinate of the stamp loaction</param>
        /// <returns></returns>
        private PdfStamp CreateForm300WorkRelatedClassificationCheckmarkStamp(WorkRelatedCaseClassification? key, float yCoord)
        {
            if (!key.HasValue)
                return null;

            float[] pageTotalXCoords = { 615f, 651f, 695f, 741f };
            PdfStamp toReturn = null;
            switch (key.Value)
            {
                // To Create Checkmarks, use the "ZapfDingbats", Value 4. 
                case WorkRelatedCaseClassification.Death:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = pageTotalXCoords[0], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedCaseClassification.DaysAwayFromWork:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = pageTotalXCoords[1], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedCaseClassification.JobTransferOrRestriction:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = pageTotalXCoords[2], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedCaseClassification.Other:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = pageTotalXCoords[3], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                default:
                    break;
            }
            return toReturn;
        }

        /// <summary>
        /// Creates the summed up page total stamps. One stamp returne for each value of case classification
        /// </summary>
        /// <param name="pageData">The rows used on the page that will be used to aggregate counts.</param>
        /// <returns>A bunch of stamps</returns>
        private IEnumerable<PdfStamp> CreateForm300WorkRelatedCaseClassificationPageTotal(IEnumerable<Case> pageData)
        {
            PdfStamp createCaseClassificationStamps(WorkRelatedCaseClassification? key, int total)
            {
                if (!key.HasValue)
                    return null;

                PdfStamp toReturn = null;
                float[] pageTotalXCoords = { 615f, 651f, 695f, 741f };
                float yCoord = 612.0f - 534.0f;

                switch (key.Value)
                {
                    case WorkRelatedCaseClassification.Death:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[0], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedCaseClassification.DaysAwayFromWork:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[1], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedCaseClassification.JobTransferOrRestriction:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[2], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedCaseClassification.Other:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[3], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    default:
                        break;
                }

                return toReturn;
            }

            return Enumerable.Range(0, 4).Select(k => new
            {
                Key = (WorkRelatedCaseClassification)k,
                Count = pageData.Count(s => s.WorkRelated.Classification.HasValue && s.WorkRelated.Classification.Value == (WorkRelatedCaseClassification)k)
            }).Select(a => createCaseClassificationStamps(a.Key, a.Count)).ToList();
        }

        /// <summary>
        /// Based on the work related classification, create a stamp with the correct x-y coordinates
        /// </summary>
        /// <param name="key">The value required to create the stamp</param>
        /// <param name="yCoord">The y-coordinate of the stamp loaction</param>
        /// <returns></returns>
        private PdfStamp CreateForm300WorkRelatedTypeOfInjuryCheckmarkStamp(WorkRelatedTypeOfInjury? key, float yCoord)
        {
            if (!key.HasValue)
                return null;

            PdfStamp toReturn = null;
            float[] checkmarkXCoords = new[] { 881f, 901f, 921f, 941f, 961f, 981f };
            switch (key.Value)
            {
                case WorkRelatedTypeOfInjury.Injury:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[0], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedTypeOfInjury.SkinDisorder:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[1], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedTypeOfInjury.RespiratoryCondition:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[2], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedTypeOfInjury.Poisoning:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[3], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedTypeOfInjury.HearingLoss:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[4], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                case WorkRelatedTypeOfInjury.PatientHandling:
                case WorkRelatedTypeOfInjury.AllOtherIllnesses:
                    toReturn = new PdfStamp() { Value = true, FontSize = 8f, X = checkmarkXCoords[5], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                    break;
                default:
                    break;
            }
            return toReturn;
        }

        /// <summary>
        /// Creates the summed up page total stamps. One stamp returne for each type of injury.
        /// </summary>
        /// <param name="pageData">The rows used on the page that will be used to aggregate counts.</param>
        /// <returns>A bunch of stamps</returns>
        private IEnumerable<PdfStamp> CreateForm300WorkRelatedTypeOfInjuryPageTotalStamps(IEnumerable<Case> pageData)
        {
            PdfStamp createTypeOfInjuryStamps(WorkRelatedTypeOfInjury? key, int total)
            {
                if (!key.HasValue)
                    return null;

                float[] pageTotalXCoords = new[] { 881f, 901f, 921f, 941f, 961f, 981f };
                float yCoord = 612.0f - 534.0f;
                PdfStamp toReturn = null;
                switch (key.Value)
                {
                    case WorkRelatedTypeOfInjury.Injury:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[0], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedTypeOfInjury.SkinDisorder:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[1], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedTypeOfInjury.RespiratoryCondition:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[2], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedTypeOfInjury.Poisoning:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[3], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedTypeOfInjury.HearingLoss:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[4], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    case WorkRelatedTypeOfInjury.PatientHandling:
                    case WorkRelatedTypeOfInjury.AllOtherIllnesses:
                        toReturn = new PdfStamp() { Value = total.ToString(), FontSize = 8f, X = pageTotalXCoords[5], Y = yCoord, MaxWidth = 2 * 8f, MaxHeight = 21.0f, AutoFit = true };
                        break;
                    default:
                        break;
                }

                return toReturn;
            }

            return Enumerable.Range(0, 6).Select(k => new
            {
                Key = (WorkRelatedTypeOfInjury)k,
                Count = pageData.Count(s => s.WorkRelated.TypeOfInjury.HasValue && s.WorkRelated.TypeOfInjury.Value == (WorkRelatedTypeOfInjury)k)
            }).Select(a => createTypeOfInjuryStamps(a.Key, a.Count)).ToList();
        }

        /// <summary>
        /// Creates the 2 stamps to display the summed up page total for the days away from work.
        /// </summary>
        /// <param name="pageData">The rows used on the page that will be used to aggregate counts.</param>
        /// <returns>A bunch of stamps</returns>
        private IEnumerable<PdfStamp> CreateForm300DaysAwayFromWorkPageTotalStamps(IEnumerable<Case> pageData)
        {

            var daysAwayFromWork = pageData.Sum(GetDaysAwayFromWorkForOshaReports);
            var daysOnJobTransferRestrictionTotal = pageData.Sum(GetDaysOnJobTransferOrRestrictionForOshaReports);           
            float yCoord = 612.0f - 534.0f;

            yield return new PdfStamp() { Value = daysAwayFromWork.ToString(), FontSize = 8f, X = 786f, Y = yCoord, MaxWidth = 3 * 8f, MaxHeight = 21.0f, AutoFit = true };
            yield return new PdfStamp() { Value = daysOnJobTransferRestrictionTotal.ToString(), FontSize = 8f, X = 828f, Y = yCoord, MaxWidth = 3 * 8f, MaxHeight = 21.0f, AutoFit = true };
        }

        /// <summary>
        /// Creates the 300a Summary report with the given query.
        /// </summary>
        /// <param name="formTemplate"></param>
        /// <param name="query">Query of reportable work related cases.</param>
        /// <returns>The pdf stamped document</returns>
        private byte[] CreateForm300ASummaryReport(byte[] formTemplate, IEnumerable<Case> query, int year, Employer employer, string officeLocation = null)
        {
            var totalCasesDeaths = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.Death);
            var totalCasesWithDaysAwayFromWork = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.DaysAwayFromWork);
            var totalCasesWithDaysTransferRestriction = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.JobTransferOrRestriction);
            var totalCasesOther = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.Other);

            var numberOfDays = query.Sum(GetDaysAwayFromWorkForOshaReports);
            var numberOfDaysJobRestriction = query.Sum(GetDaysOnJobTransferOrRestrictionForOshaReports);
            var totalInjuries = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.Injury);
            var totalSkinDisorder = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.SkinDisorder);
            var totalRespiratory = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.RespiratoryCondition);
            var totalPoisoning = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.Poisoning);
            var totalHearingLoss = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.HearingLoss);
            var totalAllOtherIllnesses = query.Count(q => (q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.AllOtherIllnesses) || (q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.PatientHandling));

            Organization organization = null;
            OrganizationAnnualInfo organizationAnnualInfo = null;
            if (officeLocation != null)
            {
                organization = Organization.AsQueryable().FirstOrDefault(r => r.Code == officeLocation && r.EmployerId == employer.Id);
                organizationAnnualInfo = OrganizationAnnualInfo.AsQueryable().FirstOrDefault(r => r.OrganizationCode == officeLocation && r.EmployerId == employer.Id && r.Year == year);
            }
            var address = GetLocationAddress(employer, organization);


            var form300AStamps = new[]{
                
                #region Year Header

                new PdfStamp() { Value = year.ToString().Substring(2, 1), FontSize = 16f, Y = 612f - 49f, X = 920f, MaxWidth = 2*16f, MaxHeight = 18f, AutoFit = true },
                new PdfStamp() { Value = year.ToString().Substring(3, 1), FontSize = 16f, Y = 612f - 49f, X = 934f, MaxWidth = 2*16f, MaxHeight = 18f, AutoFit = true },
            
                #endregion

                #region  Establishment Information

                     

                    // Company name, address, misc info
                     new PdfStamp() { Value = organization == null ? employer.Name : organization.Name,
                         FontSize = 8f, AutoFit = true, MaxWidth=902f-744f, MaxHeight = 21.0f, X = 744f, Y = 612f - 148f },
                     new PdfStamp() { Value = address.StreetAddress,
                         FontSize = 8f, AutoFit = true, MaxWidth=902f-696f, MaxHeight = 21.0f, X = 696f, Y = 612f - 169f },
                     new PdfStamp() { Value = address.City,
                         FontSize = 8f, AutoFit = true, MaxWidth=798f-696f,MaxHeight = 21.0f, X = 696f, Y = 612f - 187f },
                     new PdfStamp() { Value = address.State ,
                         FontSize = 8f, AutoFit = true, MaxWidth=847f-828f,MaxHeight = 21.0f, X = 828f, Y = 612f - 187f },
                     new PdfStamp() { Value =  address.PostalCode,
                         FontSize = 8f, AutoFit = true, MaxWidth=902f-872f,MaxHeight = 21.0f, X = 872f, Y = 612f - 187f },

                 
                     // We currently do not keep the below information.
                     
                     // Industry description
                     new PdfStamp() { Value = "", FontSize = 8f, MaxWidth=8f,AutoFit = true, MaxHeight = 21.0f, X = 696f, Y = 612f - 237f },

                     new PdfStamp() { Value = GetSicCode(organization,0), FontSize = 8f, AutoFit = true, MaxWidth = 8f, MaxHeight = 21.0f, X = 696f, Y = 612f - 266f },
                     new PdfStamp() { Value = GetSicCode(organization,1), FontSize = 8f, AutoFit = true, MaxWidth = 8, MaxHeight = 21.0f, X = 712f, Y = 612f - 266f },
                     new PdfStamp() { Value = GetSicCode(organization,2), FontSize = 8f, AutoFit = true, MaxWidth = 8f, MaxHeight = 21.0f, X = 734f, Y = 612f - 266f },
                     new PdfStamp() { Value = GetSicCode(organization,3), FontSize = 8f, AutoFit = true, MaxWidth = 8f, MaxHeight = 21.0f, X = 753f, Y = 612f - 266f },

                     new PdfStamp() { Value = GetNaicsCode(organization,0), FontSize = 8f, AutoFit = true, MaxWidth=8f,MaxHeight = 21.0f, X = 692f, Y = 612f - 314f },
                     new PdfStamp() { Value = GetNaicsCode(organization,1), FontSize = 8f, AutoFit = true, MaxWidth=8f,MaxHeight = 21.0f, X = 712f, Y = 612f - 314f },
                     new PdfStamp() { Value = GetNaicsCode(organization,2), FontSize = 8f, AutoFit = true, MaxWidth=8f,MaxHeight = 21.0f, X = 732f, Y = 612f - 314f },
                     new PdfStamp() { Value = GetNaicsCode(organization,3), FontSize = 8f, AutoFit = true, MaxWidth=8f,MaxHeight = 21.0f, X = 752f, Y = 612f - 314f },
                     new PdfStamp() { Value = GetNaicsCode(organization,4), FontSize = 8f, AutoFit = true, MaxWidth=8f,MaxHeight = 21.0f, X = 772f, Y = 612f - 314f },
                     new PdfStamp() { Value = GetNaicsCode(organization,5), FontSize = 8f, AutoFit = true, MaxWidth=8f, MaxHeight = 21.0f, X = 792f, Y = 612f - 314f },

                     // Employment Info - Annual avg # of employees, total hours worked by all employees last year.
                     new PdfStamp() { Value = organizationAnnualInfo == null ? "" : organizationAnnualInfo.AverageEmployeeCount.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=32f,MaxHeight = 21.0f, X = 818f, Y = 612f - 369f },
                     new PdfStamp() { Value = organizationAnnualInfo == null ? "" : organizationAnnualInfo.TotalHoursWorked.ToString(), FontSize = 8f, AutoFit = true,MaxWidth=32f, MaxHeight = 21.0f, X = 818f, Y = 612f - 389f },

                #endregion

                #region Number of Cases (G-J)

                     new PdfStamp() { Value = totalCasesDeaths.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f,MaxHeight = 21.0f, X = 42f, Y = 612f - 268f },
                     new PdfStamp() { Value = totalCasesWithDaysAwayFromWork.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f,MaxHeight = 21.0f, X = 128f, Y = 612f - 268f },
                     new PdfStamp() { Value = totalCasesWithDaysTransferRestriction.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f,MaxHeight = 21.0f, X = 222f, Y = 612f - 268f },
                     new PdfStamp() { Value = totalCasesOther.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f,MaxHeight = 21.0f, X = 332f, Y = 612f - 268f },

                #endregion

                #region Number of Days (K-L)
            
                 new PdfStamp() { Value = numberOfDays.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 42f, Y = 612f - 375f },
                 new PdfStamp() { Value = numberOfDaysJobRestriction.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 200f, Y = 612f - 375f },

                #endregion

                #region Injury or Illness Type (M 1-6)

                 new PdfStamp() { Value = totalInjuries.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 156f, Y = 612f - 463f },
                 new PdfStamp() { Value = totalSkinDisorder.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 156f, Y = 612f - 491f },
                 new PdfStamp() { Value = totalRespiratory.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 156f, Y = 612f - 507f },

                 new PdfStamp() { Value = totalPoisoning.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 367f, Y = 612f - 463f },
                 new PdfStamp() { Value = totalHearingLoss.ToString(), FontSize = 8f, AutoFit = true, MaxWidth=5f*8f, MaxHeight = 21.0f, X = 367f, Y = 612f - 479f },
                 new PdfStamp() { Value = totalAllOtherIllnesses.ToString(), FontSize = 8f, AutoFit = true,  MaxWidth=5f*8f, MaxHeight = 21.0f, X = 367f, Y = 612f - 496f },

                #endregion

            };
            
            return Pdf.StampPdf(formTemplate, form300AStamps);
        }
        /// <summary>
        /// Creating Summary preview of Form 300A
        /// </summary>
        /// <param name="query"></param>
        /// <param name="year"></param>
        /// <param name="employer"></param>
        /// <param name="officeLocation"></param>
        /// <returns></returns>
        private Osha300AFormPreview GetForm300ASummaryReportPreview(IEnumerable<Case> query, int year, Employer employer, string officeLocation = null)
        {
            Osha300AFormPreview preview = new Osha300AFormPreview();
            preview.EmployerId = employer.Id;
            preview.Year = year;
            preview.TotalCasesDeaths = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.Death);
            preview.TotalCasesWithDaysAwayFromWork = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.DaysAwayFromWork);
            preview.TotalCasesWithDaysTransferRestriction = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.JobTransferOrRestriction);
            preview.TotalCasesOther = query.Count(q => q.WorkRelated.Classification.HasValue && q.WorkRelated.Classification.Value == WorkRelatedCaseClassification.Other);

            preview.NumberOfDays = query.Sum(GetDaysAwayFromWorkForOshaReports);
            preview.NumberOfDaysJobRestriction = query.Sum(GetDaysOnJobTransferOrRestrictionForOshaReports);
            preview.TotalInjuries = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.Injury);
            preview.TotalSkinDisorder = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.SkinDisorder);
            preview.TotalRespiratory = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.RespiratoryCondition);
            preview.TotalPoisoning = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.Poisoning);
            preview.TotalHearingLoss = query.Count(q => q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.HearingLoss);
            preview.TotalAllOtherIllnesses = query.Count(q => (q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.AllOtherIllnesses) || (q.WorkRelated.TypeOfInjury.HasValue && q.WorkRelated.TypeOfInjury.Value == WorkRelatedTypeOfInjury.PatientHandling));

            Organization organization = null;
            OrganizationAnnualInfo organizationAnnualInfo = null;
            if (officeLocation != null)
            {
                organization = Organization.AsQueryable().FirstOrDefault(r => r.Code == officeLocation && r.EmployerId == employer.Id);
                organizationAnnualInfo = OrganizationAnnualInfo.AsQueryable().FirstOrDefault(r => r.OrganizationCode == officeLocation && r.EmployerId == employer.Id && r.Year == year);
                if (organization != null)
                {
                    preview.EstablishmentCode = organization.Code;
                    preview.EstablishmentTypeCode = organization.TypeCode;
                }
            }
            var address = GetLocationAddress(employer, organization);
            preview.EmployerName = employer.Name;
            preview.StreetAddress =  address.StreetAddress;
            preview.City = address.City;
            preview.State = address.State;
            preview.PostalCode = address.PostalCode;
            preview.EstablishmentName = organization == null ? employer.Name : organization.Name;
            preview.SicCode = organization?.SicCode;
            preview.NaicsCode = 0;
            if (int.TryParse(organization?.NaicsCode, out int result))
            {
                preview.NaicsCode = result;
            }
            
            preview.IndustryDescription = IndustryDescription.GetIndustryDescription(preview.NaicsCode.ToString() ?? preview.SicCode);
            if (organizationAnnualInfo != null)
            {
                preview.AverageEmployeeCount = Convert.ToInt32(organizationAnnualInfo.AverageEmployeeCount);
                preview.TotalHoursWorked = Convert.ToInt32(organizationAnnualInfo.TotalHoursWorked);
            }
            
            if (preview.AverageEmployeeCount > 250)
            {
                preview.Size = 3; //Enter 3 if the establishment has 250 + employees
            }
            else if (preview.AverageEmployeeCount < 250 && preview.AverageEmployeeCount >= 20)
            {
                preview.Size = 2; //Enter 2 if the establishment has 20 - 249 employees
            }
            else
            {
                preview.Size = 1; // if the establishment has < 20 employees
            }
           
            return preview;

        }

        /// <summary>
        /// Gets the sic code.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        private string GetSicCode(Organization organization, int index)
        {
           
            if (organization == null || string.IsNullOrEmpty(organization.SicCode))
                return string.Empty;

            // AT-6341 - 300a OSHA report, show if NAICS code is available, if not then show SIC code.Do not show both the codes in the report
            if (!string.IsNullOrWhiteSpace(organization.NaicsCode) && organization.NaicsCode.Length > 0)
                return string.Empty;

            if (index >= organization.SicCode.Length)
            {
                return string.Empty;
            }
            else
            {
                return organization.SicCode.Substring(index, 1);
            }
        }

        /// <summary>
        /// Gets the nacis code.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        private string GetNaicsCode(Organization organization, int index)
        {
            if (organization == null || string.IsNullOrEmpty(organization.NaicsCode))
                return string.Empty;

            if (index >= organization.NaicsCode.Length)
            {
                return string.Empty;
            }
            else
            {
                return organization.NaicsCode.Substring(index, 1);
            }
        }

        /// <summary>
        /// Gets the string part.
        /// </summary>
        /// <param name="organization">The organization.</param>
        /// <param name="codeType">Type of the code.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        private string GetSicNacisCode(Organization organization, string codeType, int index)
        {
            string retValue = string.Empty;
            if (organization != null)
            {
                if (codeType.Equals("SIC") && !string.IsNullOrEmpty(organization.SicCode))
                {
                    retValue = organization.SicCode.Substring(index, 1);
                }
                else if (codeType.Equals("NAICS") && !string.IsNullOrEmpty(organization.NaicsCode))
                {
                    retValue = organization.NaicsCode.Substring(index, 1);
                }
            }
            return retValue;
        }

        /// <summary>
        /// Gets the location address.
        /// </summary>
        /// <param name="employer">The employer.</param>
        /// <param name="organization">The organization.</param>
        /// <returns></returns>
        private Address GetLocationAddress(Employer employer, Organization organization)
        {
            if (organization != null && organization.Address != null && !string.IsNullOrEmpty(organization.Address.Address1))
            {
                return organization.Address;
            }
            else
            {
                return employer.Contact.Address;
            }
        }

        /// <summary>
        /// Generates the PDF.
        /// </summary>
        /// <param name="documents">The documents.</param>
        /// <returns></returns>
        public byte[] GeneratePDF(params byte[][] documents)
        {
            byte[] output = null;
            output = Rendering.Pdf.MergePdfDocuments(documents.ToArray());
            return output;
        }

        private List<Case> Form300Query(int year, string employerId, string officeLocation = null)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();

            var startDate = Convert.ToDateTime("1/1/" + year.ToString()).ToMidnight();
            var endDate = Convert.ToDateTime("12/31/" + year.ToString()).ToMidnight();

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(Case.Query.And(Case.Query.ElemMatch(e => e.CaseEvents, b => b.And(
                        b.EQ(e => e.EventType, CaseEventType.IllnessOrInjuryDate),
                        b.ElemMatch(e => e.AllEventDates, f => new QueryDocument(new List<BsonElement>() {
                        new BsonElement("$gte", new BsonDateTime(startDate)),
                        new BsonElement("$lte", new BsonDateTime(endDate))
                    }))
                    ))),
                    Case.Query.And(Case.Query.GTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(startDate)),
                    Case.Query.LTE(e => e.WorkRelated.IllnessOrInjuryDate, new BsonDateTime(endDate)))));

            ands.Add(Case.Query.EQ(e => e.EmployerId, employerId));
            ands.Add(Case.Query.EQ(e => e.WorkRelated.Reportable, true));
            ands.Add(Case.Query.NE(e => e.WorkRelated, null));
            if (!string.IsNullOrEmpty(officeLocation))
            {
                ands.Add(Case.Query.EQ(e => e.WorkRelated.InjuryLocation, officeLocation));
            }
            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null).ToList();
            query = query.OrderBy(c => c.WorkRelated.IllnessOrInjuryDate).ToList();
            return query;

        }

        #endregion

        #region WC Form 100

        internal string TotalGrossWeeklyWage(List<Data.Pay.PayPeriod> payPeriods)
        {
            if (payPeriods != null && payPeriods.Count > 0)
            {
                return payPeriods[0].MaxWeeklyPayAmount != null ? payPeriods[0].MaxWeeklyPayAmount.ToString() : string.Empty;
            }

            return string.Empty;
        }

        internal string GetSicNacisCode(Organization organization)
        {
            StringBuilder code = new StringBuilder();

            code.Append(GetSicCode(organization, 0));
            code.Append(GetSicCode(organization, 1));
            code.Append(GetSicCode(organization, 2));
            code.Append(GetSicCode(organization, 3));

            if (code.ToString().Length > 0)
            {
                return code.ToString();
            }

            code.Append(GetNaicsCode(organization, 0));
            code.Append(GetNaicsCode(organization, 1));
            code.Append(GetNaicsCode(organization, 2));
            code.Append(GetNaicsCode(organization, 3));
            code.Append(GetNaicsCode(organization, 4));
            code.Append(GetNaicsCode(organization, 5));

            return code.ToString();
        }

        internal string GetTimeOfDay(AbsenceSoft.Common.TimeOfDay? timeOfDay)
        {
            string time = String.Empty;

            if (timeOfDay.HasValue)
            {
                int hour = timeOfDay.Value.Hour;
                hour = hour > 12 ? hour - 12 : hour;
                int minutes = timeOfDay.Value.Minutes;

                time = string.Concat(hour.ToString().PadLeft(2, '0'), ":", minutes.ToString().PadLeft(2, '0'));
            }

            return time;
        }

        /// <summary>
        /// Builds the WC form 100 from the provided case to populate the form.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="pdf">The PDF document in bytes.</param>
        /// <returns></returns>
        internal byte[] BuildForm100Fields(Case theCase, byte[] pdf)
        {
            Form100PdfHelper helper = new Form100PdfHelper(pdf);

            try
            {
                // Employee Data section
                helper.SetField(Form100PdfHelper.SSN, theCase.Employee.Ssn != null ? theCase.Employee.Ssn.Encrypted.Decrypt() : String.Empty);
                helper.SetField(Form100PdfHelper.DateOfInjury, theCase.WorkRelated.IllnessOrInjuryDate.HasValue ? theCase.WorkRelated.IllnessOrInjuryDate.Value.ToString("MM/dd/yyyy") : String.Empty);
                // Hiding name if Health Care Privacy option is checked
                string employeeName = theCase.Employee.FullName;
                if (theCase.WorkRelated != null && theCase.WorkRelated.HealthCarePrivate != null && theCase.WorkRelated.HealthCarePrivate == true)
                {
                    employeeName = "privacy case";
                }
                helper.SetField(Form100PdfHelper.EmployeeName, employeeName);
                helper.SetField(Form100PdfHelper.AddressStreet, theCase.Employee.Info.Address.StreetAddress);
                helper.SetField(Form100PdfHelper.City, theCase.Employee.Info.Address.City);
                helper.SetField(Form100PdfHelper.State, theCase.Employee.Info.Address.State);
                helper.SetField(Form100PdfHelper.PostalCode, theCase.Employee.Info.Address.PostalCode);
                helper.SetField(Form100PdfHelper.DOB, theCase.Employee.DoB.ToUIString());
                helper.SetField(Form100PdfHelper.Gender, theCase.Employee.Gender != null ? theCase.Employee.Gender.ToString() : String.Empty);
                helper.SetField(Form100PdfHelper.Dependents, String.Empty);
                helper.SetField(Form100PdfHelper.Telephone, theCase.Employee.Info.HomePhone ?? theCase.Employee.Info.CellPhone ?? String.Empty);
                helper.SetField(Form100PdfHelper.TaxFilingStatus, String.Empty);

                // Employer/Carrier Data section
                Organization organization = null;
                string injuryLocation = theCase.WorkRelated.InjuryLocation;
                if (!string.IsNullOrWhiteSpace(injuryLocation))
                {
                    organization = Organization.AsQueryable().FirstOrDefault(r => r.Code == injuryLocation && r.EmployerId == theCase.Employer.Id);
                }
                helper.SetField(Form100PdfHelper.EmployerName, theCase.Employer.Name);
                helper.SetField(Form100PdfHelper.EmployerFederalId, theCase.Employer.Ein != null ? theCase.Employer.Ein.Encrypted.Decrypt() : String.Empty);
                helper.SetField(Form100PdfHelper.InjuryLocation, organization?.Name ?? string.Empty);
                helper.SetField(Form100PdfHelper.MailingLocation, String.Empty);
                helper.SetField(Form100PdfHelper.UINumber, String.Empty);

                var address = GetLocationAddress(theCase.Employer, organization);

                helper.SetField(Form100PdfHelper.TypeOfBusiness, GetSicNacisCode(organization));
                helper.SetField(Form100PdfHelper.EmployerStreet, theCase.Employer.Contact.Address.StreetAddress);
                helper.SetField(Form100PdfHelper.EmployerCity, theCase.Employer.Contact.Address.City);
                helper.SetField(Form100PdfHelper.EmployerState, theCase.Employer.Contact.Address.State);
                helper.SetField(Form100PdfHelper.EmployerPostalCode, theCase.Employer.Contact.Address.PostalCode);
                helper.SetField(Form100PdfHelper.InsuranceCompany, String.Empty);
                helper.SetField(Form100PdfHelper.InsuranceCoTelephone, String.Empty);

                DateTime? returnToWork = theCase.GetCaseEventDate(CaseEventType.ReturnToWork);

                // Injury/Medical Data section
                helper.SetField(Form100PdfHelper.LastDayWorked, String.Empty);
                helper.SetField(Form100PdfHelper.DateEmployeeReturnedToWork, returnToWork != null ? returnToWork.ToString("MM/dd/yyyy") : String.Empty);
                helper.SetField(Form100PdfHelper.DidEmployeeDie, theCase.WorkRelated.DateOfDeath.HasValue ? "Yes" : "No");
                helper.SetField(Form100PdfHelper.DateOfDeath, theCase.WorkRelated.DateOfDeath.ToString("MM/dd/yyyy"));
                helper.SetField(Form100PdfHelper.InjuryCity, address != null ? address.City : String.Empty);
                helper.SetField(Form100PdfHelper.InjuryState, address != null ? address.State : String.Empty);
                helper.SetField(Form100PdfHelper.InjuryCounty, String.Empty);
                helper.SetField(Form100PdfHelper.InjuryAtEmployerPremises, injuryLocation == theCase.Employee.Info.OfficeLocation ? "Yes" : "No");
                helper.SetField(Form100PdfHelper.CaseNumber, theCase.CaseNumber);
                helper.SetField(Form100PdfHelper.TimeEmployeeBeganWork, GetTimeOfDay(theCase.WorkRelated.TimeEmployeeBeganWork));
                helper.SetField(Form100PdfHelper.TimeAMPM, theCase.WorkRelated.TimeEmployeeBeganWork.HasValue ? (theCase.WorkRelated.TimeEmployeeBeganWork.Value.PM ? "pm" : "am") : "");
                helper.SetField(Form100PdfHelper.TimeOfEvent, GetTimeOfDay(theCase.WorkRelated.TimeOfEvent));
                helper.SetField(Form100PdfHelper.EvenTimeAMPM, theCase.WorkRelated.TimeOfEvent.HasValue ? (theCase.WorkRelated.TimeOfEvent.Value.PM ? "pm" : "am") : "");
                helper.SetField(Form100PdfHelper.TimeCannotBeDetermined, theCase.WorkRelated.TimeOfEvent.HasValue == false ? "undetermined" : "Off");
                helper.SetField(Form100PdfHelper.EmployeeReasonBeforeIncident, theCase.WorkRelated.ActivityBeforeIncident);
                helper.SetField(Form100PdfHelper.HowDidInjuryOccur, theCase.WorkRelated.WhatHappened);
                helper.SetField(Form100PdfHelper.NatureOfInjury, theCase.WorkRelated.InjuryOrIllness);
                // PartOfBody Field Population - Form 100 OSHA 
                helper.SetField(Form100PdfHelper.PartOfBodyAffected, theCase.WorkRelated.InjuredBodyPart);

                helper.SetField(Form100PdfHelper.WhatObjectHarmedEmployee, theCase.WorkRelated.WhatHarmedTheEmployee);
                if (theCase.WorkRelated.Provider != null)
                {
                    helper.SetField(Form100PdfHelper.NameOfPhysician, theCase.WorkRelated.Provider.Contact.LastName + ", " + theCase.WorkRelated.Provider.Contact.FirstName);
                    helper.SetField(Form100PdfHelper.TreatmentLocation, theCase.WorkRelated.Provider.Contact.Address.StreetAddress + ", " + theCase.WorkRelated.Provider.Contact.Address.City + ", " + theCase.WorkRelated.Provider.Contact.Address.State + ", " + theCase.WorkRelated.Provider.Contact.Address.PostalCode);
                }
                helper.SetField(Form100PdfHelper.TreatedInEmergencyRoom, theCase.WorkRelated.EmergencyRoom.HasValue ? (theCase.WorkRelated.EmergencyRoom.Value ? "Yes" : "No") : "");
                helper.SetField(Form100PdfHelper.EmployeeHospitalizedOvernight, theCase.WorkRelated.HospitalizedOvernight.HasValue ? (theCase.WorkRelated.HospitalizedOvernight.Value ? "Yes" : "No") : "");

                // Occupation and Wage Data section
                helper.SetField(Form100PdfHelper.DateHired, theCase.Employee.HireDate.ToString("MM/dd/yyyy"));
                helper.SetField(Form100PdfHelper.TotalGrossWeeklyWage, String.Empty);
                helper.SetField(Form100PdfHelper.NoWeeks, String.Empty);
                helper.SetField(Form100PdfHelper.DiscountBenefits, String.Empty);
                if (theCase.WorkRelated.IllnessOrInjuryDate != null)
                {
                    helper.SetField(Form100PdfHelper.Occupation, theCase.Employee.GetPeriodJob(theCase.WorkRelated.IllnessOrInjuryDate.Value)?.JobTitle ?? String.Empty);
                }
                else
                {
                    helper.SetField(Form100PdfHelper.Occupation, String.Empty);
                }
                helper.SetField(Form100PdfHelper.EmployeeVolunterWorker, String.Empty);
                helper.SetField(Form100PdfHelper.EmployeeHandicapped, String.Empty);
                helper.SetField(Form100PdfHelper.DateEmployerNotified, theCase.CreatedDate.ToString("MM/dd/yyyy"));
                helper.SetField(Form100PdfHelper.TempAgency, String.Empty);

                // Preparer Data section
                if (theCase.AssignedTo != null)
                {
                    helper.SetField(Form100PdfHelper.PreparersName, theCase.AssignedTo.DisplayName);
                    helper.SetField(Form100PdfHelper.TelephoneNumber, theCase.AssignedTo.ContactInfo.WorkPhone.FormatPhone() ?? String.Empty);
                    
                }
                helper.SetField(Form100PdfHelper.DatePrepared, ( theCase.GetCaseEventDate(CaseEventType.CaseCreated) ?? theCase.CreatedDate).ToString("MM/dd/yyyy"));

                string file = helper.GetFile();

                using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    pdf = fs.ReadAllBytes();
                }

                return pdf;
            }
            finally
            {
                helper.Close();
            }
        }
        #endregion
    }
}
