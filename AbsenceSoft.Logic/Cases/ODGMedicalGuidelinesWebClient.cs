﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace AbsenceSoft.Logic.Cases
{
    internal class ODGMedicalGuidelinesWebClient: ILogicService
    {
        private string _contentType = "text/xml";
        private string _postData = String.Empty;
        private List<DiagnosisGuidelines> _odgData;

        public ODGMedicalGuidelinesWebClient()
        {
        }

        public DiagnosisGuidelines GetMedicalGuidelines(string medicalCode)
        {
            if (String.IsNullOrWhiteSpace(medicalCode))
                throw new AbsenceSoftException("MedicalCode is not provided for ODG Guidelines webservice");

            _odgData = new List<DiagnosisGuidelines>();

            try
            {
                GetSummaryGuidelines(medicalCode);
                GetBestPracticeGuidelines(medicalCode);
            }
            catch
            {
                throw;
            }

            return _odgData.FirstOrDefault();
        }


        /// <summary>
        /// Returns the comorbidity guidelines based on the arguments provided.
        /// </summary>
        /// <param name="arg">Comorbidity Arguments</param>
        /// <returns></returns>
        public CoMorbidityGuideline GetComorbidityGuidelines(CoMorbidityGuideline.CoMorbidityArgs arg)
        {
            if (arg == null)
                throw new AbsenceSoftException("Comorbidity Arguments not provided for ODG Guidelines webservice");

            CoMorbidityGuideline result  = new CoMorbidityGuideline();

            try
            {
                this._postData = arg.ToString();

                string endPoint = Settings.Default.ODGCoMorbidityGuideEndPoint;

                //Call ODG REST webservice
                var strResult = MakeRequest(endPoint, "", HttpVerb.POST);

                if (String.IsNullOrWhiteSpace(strResult))
                    return null;

                //parse the data
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strResult);

                //set risk assessments
                var rslist = doc.SelectNodes("root/riskassessment");
                if(rslist.Count > 0)
                {
                    var rs = rslist[0];
                    result.RiskAssessmentScore = rs.GetDecimal("score").Value;
                    result.RiskAssessmentStatus = rs.GetString("status");

                }

                //set Adjusted Summary Guidelines
                rslist = doc.SelectNodes("root/adjustedsummaryguidelines");
                if (rslist.Count > 0)
                {
                    var rs = rslist[0];
                    result.MidrangeAllAbsence = rs.GetDecimal("midrangeallabsence");
                    result.AtRiskAllAbsence = rs.GetDecimal("atriskallabsence");
                }

                //set the Adjusted Duration
                rslist = doc.SelectNodes("root/adjustedduration/bp");
                if (rslist.Count > 0)
                {
                    var rs = rslist[0];
                    result.BestPracticesDays = rs.GetDecimal("days").Value;
                }

                //get the claim profile 95 for Adjusted Duration
                rslist = doc.SelectNodes("root/adjustedduration/cp95");
                if (rslist.Count > 0)
                {
                    var rs = rslist[0];
                    result.ActualDaysLost = rs.GetDecimal("days").Value;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        private void GetSummaryGuidelines(string medicalCode)
        {
            string result = null;

            string endPoint = Settings.Default.ODGSummaryGuideEndPoint;

            //Call ODG REST webservice
            result = MakeRequest(endPoint, medicalCode, HttpVerb.GET);

            if (String.IsNullOrWhiteSpace(result))
                return;

            //Parse result
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/root/item");

            foreach (XmlNode node in nodes)
            {
                DiagnosisGuidelines odgGuideline = new DiagnosisGuidelines();

                odgGuideline.Code = node.GetString("icdcode");
                odgGuideline.Description = node.GetString("name");
                odgGuideline.RtwSummary = new RtwSummaryGuidelines() 
                {
                    ClaimsMidRange = node.GetInt32("claimsmidrange") ?? 0, 
                    ClaimsAtRisk = node.GetInt32("claimsatrisk") ?? 0,
                    AbsencesMidRange = node.GetInt32("absencesmidrange") ?? 0,
                    AbsencesAtRisk = node.GetInt32("absencesatrisk") ?? 0 
                };

                _odgData.Add(odgGuideline);
            }
        }

        private void GetBestPracticeGuidelines(string medicalCode)
        {
            string result = null;

            string endPoint = Settings.Default.ODGBPEndPoint;

            if (String.IsNullOrWhiteSpace(medicalCode))
                return;


            result = MakeRequest(endPoint, medicalCode, HttpVerb.GET);

            if (String.IsNullOrWhiteSpace(result))
                return;

            //Parse result
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/root/item");


            foreach (XmlNode node in nodes)
            {
                var odgItem = _odgData.SingleOrDefault(x => x.Code.Equals(node.GetString("icdcode")));

                if (odgItem == null)
                {
                    odgItem = new DiagnosisGuidelines()
                    {
                        Code = node.GetString("icdcode"),
                        Description = node.GetString("name")
                    };

                    _odgData.Add(odgItem);
                }

                odgItem.RtwBestPractices = GetRtwBestPracticeGuideline(node);
                odgItem.PhisicalTherapy = GetPhisicalTherapyGuideline(node);
                odgItem.ActivityModifications = GetActivityModification(node);
                odgItem.Chiropractical = GetChiropracticalGuideline(node);
            }

            return;
        }

        private List<RtwBestPractice> GetRtwBestPracticeGuideline(XmlNode node)
        {
            List<RtwBestPractice> list = new List<RtwBestPractice>();

            XmlNodeList nodes = node.SelectNodes("bp/item");

            foreach (XmlNode subNode in nodes)
            {
                RtwBestPractice prac = new RtwBestPractice();
                prac.RtwPath = subNode.GetString("primaryRTWpath");
                if (string.IsNullOrWhiteSpace(prac.RtwPath))
                    continue;
                prac.MinDays = subNode.GetInt32("days1");
                prac.MaxDays = subNode.GetInt32("days");
                list.Add(prac);
            }

            return list;
        }

        private List<ActivityModification> GetActivityModification(XmlNode node)
        {
            List<ActivityModification> list = new List<ActivityModification>();

            XmlNodeList nodes = node.SelectNodes("activitymod/item");

            foreach (XmlNode subNote in nodes)
            {
                list.Add(new ActivityModification()
                {
                    JobType = subNote.GetString("jobtype"),
                    JobModifications = subNote.GetString("jobmodification"),
                });
            }

            return list;
        }

        private List<PhisicalTherapyGuideline> GetPhisicalTherapyGuideline(XmlNode node)
        {
            List<PhisicalTherapyGuideline> list = new List<PhisicalTherapyGuideline>();

            XmlNodeList nodes = node.SelectNodes("pt/item");

            foreach (XmlNode subNode in nodes)
            {
                PhisicalTherapyGuideline guideline = new PhisicalTherapyGuideline();
                guideline.PrimaryPath = subNode.GetString("primarypath");
                if (string.IsNullOrWhiteSpace(guideline.PrimaryPath))
                    continue;
                guideline.Visits = subNode.GetInt32("visits");
                guideline.Weeks = subNode.GetInt32("weeks");
                list.Add(guideline);
            }

            return list;
        }

        private List<ChiropracticalGuideline> GetChiropracticalGuideline(XmlNode node)
        {
            List<ChiropracticalGuideline> list = new List<ChiropracticalGuideline>();

            XmlNodeList nodes = node.SelectNodes("chiro/item");

            foreach (XmlNode subNode in nodes)
            {
                ChiropracticalGuideline guideline = new ChiropracticalGuideline();
                guideline.PrimaryPath = subNode.GetString("primarypath");
                if (string.IsNullOrWhiteSpace(guideline.PrimaryPath))
                    continue;
                guideline.Visits = subNode.GetInt32("visits");
                guideline.Weeks = subNode.GetInt32("weeks");
                list.Add(guideline);
            }

            return list;
        }

        //Make a HTTP request to REST webservice
        private string MakeRequest(string endPoint, string parameters, HttpVerb method)
        {
            var responseValue = string.Empty;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(endPoint + parameters);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                
                request.Method = method.ToString();
                request.ContentLength = 0;
                request.ContentType = _contentType;

                //add ODG API Key Header
                request.Headers.Add("X-ODG-KEY", Settings.Default.ODGApiKey);

                if (!string.IsNullOrEmpty(_postData) && method == HttpVerb.POST)
                {
                    var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(_postData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {                    

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request to ODG Rest webservice failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    if (((HttpWebResponse)e.Response).StatusCode != HttpStatusCode.NotFound)
                    {
                        Log.Error("Error occured. Exception: " + e);
                        throw;
                    }
                }
            
            }
            
            return responseValue;
        }

        public void Dispose()
        {
        }

        private enum HttpVerb
        {
            GET,
            POST,
            PUT,
            DELETE
        }

    }
}
