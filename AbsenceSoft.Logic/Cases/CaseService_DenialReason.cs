﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// Partial class for case service to handle denial reason related services
    /// </summary>
    public partial class CaseService : LogicService
    {

        /// <summary>
        /// Checks whether denial reason code already exists
        /// We need to pass in the CustomerId instead of using what is in context because the UI needs to tell whether we're looking at a core one or a customer one
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool DenialReasonCodeIsInUse(string id, string customerId, string code)
        {
            return DenialReason.AsQueryable().Any(nc => nc.Code == code.ToUpperInvariant() && nc.Id != id && nc.EmployerId == EmployerId && nc.CustomerId == customerId);
        }

        /// <summary>
        /// Gets denial reason by code and employer/customer 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public DenialReason GetDenialReasonByCode(string code)
        {
            using (new InstrumentationContext("CaseService.GetDenialReasonByCode"))
            {
                DenialReason reason = DenialReason.GetByCode(code.ToUpperInvariant(), CustomerId, EmployerId, true);
                return reason;
            }
        }

        /// <summary>
        /// Toggles a denial reason by code and employer/customer depending on what is most specific
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public DenialReason ToggleDenialReasonByCode(string code)
        {
            using (new InstrumentationContext("CaseService.ToggleDenialReasonByCode"))
            {
                DenialReason denialReason = DenialReason.GetByCode(code.ToUpperInvariant(), CustomerId, EmployerId, true);
                
                if (denialReason != null)
                {
                    SuppressedEntity suppressedEntity = new SuppressedEntity { EntityName = "DenialReason", EntityCode = denialReason.Code.ToUpperInvariant(), EntityModifiedDate = DateTime.UtcNow };
                    if (CurrentEmployer != null)
                    {
                        CurrentEmployer.SuppressedEntities = AddOrRemoveSuppressedEntity(CurrentEmployer.SuppressedEntities, suppressedEntity);
                        CurrentEmployer.Save();
                    }
                    else if(CurrentCustomer != null)
                    {
                        CurrentCustomer.SuppressedEntities = AddOrRemoveSuppressedEntity(CurrentCustomer.SuppressedEntities, suppressedEntity);
                        CurrentCustomer.Save();
                    }
                }
                return denialReason;
            }
        }

        /// <summary>
        ///  Gets all denial reasons filtered by target and employer/ customer
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public List<DenialReason> GetAllDenialReasons(DenialReasonTarget target = DenialReasonTarget.None)
        {
            IEnumerable<DenialReason> denialReasons = DenialReason.DistinctFind(null, CustomerId, EmployerId);

            if (target != DenialReasonTarget.None)
            {
                return denialReasons.Where(nc => !IsDenialReasonDisabledInCurrentContext(nc.Code) && (nc.Target == target || nc.Target == (nc.Target.HasFlag(target) ? nc.Target : DenialReasonTarget.None))).ToList();
            }
            else
            {
                return denialReasons.Where(nc => !IsDenialReasonDisabledInCurrentContext(nc.Code)).ToList();
            }
        }

        /// <summary>
        /// To list Denial Reason for search
        /// </summary>
        /// <param name="criteria"></param> 
        public ListResults DenialReasonList(ListCriteria criteria)
        {
            if (criteria == null)
            {
                criteria = new ListCriteria();
            }

            ListResults result = new ListResults(criteria);
            string description = criteria.Get<string>("Description");
            string code = criteria.Get<string>("Code");

            List<IMongoQuery> ands = DenialReason.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            DenialReason.Query.MatchesString(at => at.Description, description, ands);
            DenialReason.Query.MatchesString(at => at.Code, code, ands);
            List<DenialReason> types = DenialReason.DistinctAggregation(ands);
            result.Total = types.Count;
            types = types
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = types.Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("CustomerId", c.CustomerId)
                .Set("EmployerId", c.EmployerId)
                .Set("Description", c.Description)
                .Set("Code", c.Code)
                .Set("Target", c.Target)
                .Set("Disabled", IsDenialReasonDisabledInCurrentContext(c.Code))
                .Set("ModifiedDate", c.ModifiedDate)
            );

            return result;
        }

        /// <summary>
        /// Check if denial reason is disabled in current context(employer/customer)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool IsDenialReasonDisabledInCurrentContext(string code)
        {
            bool returnValue = false;
            if (CurrentEmployer != null)
            {
                var denialReason = CurrentEmployer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "DenialReason" && p.EntityCode == code.ToUpperInvariant());
                if (denialReason != null)
                {
                    returnValue = true;
                }
            }
            if (CurrentCustomer != null && !returnValue)
            {
                var denialReason = CurrentCustomer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "DenialReason" && p.EntityCode == code.ToUpperInvariant());
                if (denialReason != null)
                {
                    returnValue = true;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Get denial reason by Id
        /// </summary>
        /// <param name="DenialReasonId"></param>
        /// <returns></returns>
        public DenialReason GetDenialReasonById(string DenialReasonId)
        {
            using (new InstrumentationContext("CaseService.GetDenialReasonById"))
            {
                return DenialReason.AsQueryable().FirstOrDefault(nc => nc.Id == DenialReasonId);
            }
        }

        /// <summary>
        /// Save denial reason
        /// </summary>
        /// <param name="reason"></param>
        /// <returns></returns>
        public DenialReason SaveDenialReason(DenialReason reason)
        {
            using (new InstrumentationContext("CaseService.SaveDenialReason"))
            {
                if (reason == null)
                    return null;

                DenialReason savedReason = DenialReason.GetById(reason.Id);

                if (savedReason != null && (!savedReason.IsCustom || savedReason.EmployerId != EmployerId))
                    reason.Clean();

                reason.CustomerId = CustomerId;
                reason.EmployerId = EmployerId;

                return reason.Save();
            }
        }

        /// <summary>
        /// Delete denial reason
        /// </summary>
        /// <param name="reason"></param>
        public void DeleteDenialReason(DenialReason reason)
        {
            using (new InstrumentationContext("CaseService.DeleteDenialReason"))
            {
                reason.Delete();
            }
        }

        /// <summary>
        /// Add or remove code from supress entity
        /// </summary>
        /// <param name="suppressedEntities"></param>
        /// <param name="suppressedEntity"></param>
        /// <returns></returns>
        private List<SuppressedEntity> AddOrRemoveSuppressedEntity(List<SuppressedEntity> suppressedEntities, SuppressedEntity suppressedEntity)
        {
            SuppressedEntity entity = suppressedEntities
                .FirstOrDefault(se => se.EntityCode == suppressedEntity.EntityCode && se.EntityName == suppressedEntity.EntityName);
            if (entity == null)
            {
                suppressedEntities.Add(suppressedEntity);
            }
            else
            {
                suppressedEntities.Remove(entity);
            }

            return suppressedEntities;
        }

    }
}
