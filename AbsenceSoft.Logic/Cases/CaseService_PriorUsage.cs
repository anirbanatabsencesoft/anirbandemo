﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using System.Text;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson.Serialization;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService
    {
        /// <summary>
        /// There are two ways to run this. 
        ///     The first is to calculate the usage for all the applied policies on a case from the "perspective" of that case. 
        ///     The second is from the perspective of a date. If run by date, then there is no related case logic and all cases
        ///         are summarized together for that date
        /// 
        /// this is in sticking with my current love of one time use objects which I believe is the, "Private Class Data Pattern" 
        /// </summary>
        public class CalculatePriorUsage
        {

            /// <summary>
            /// The results. This is basically a dictionary view of the usage totals. 
            /// 
            /// </summary>
            public PolicyUsageHistory UsageHistory { get; private set; }

            /// <summary>
            /// This will remove all usage allocation beyond and including end date 
            /// </summary>
            /// <param name="usageHistroy"></param>
            /// <param name="endDate"></param>
            internal void RemovePostDateUsage(DateTime endDate)
            {
                if (UsageHistory != null && UsageHistory.UsageTotals != null && UsageHistory.UsageTotals.Any())
                {
                    UsageHistory.UsageTotals.ForEach(usage => usage.Allocated = usage.Allocated.Where(allocation => allocation.DateUsed < endDate).ToList());
                }
            }

            /// <summary>
            /// When running in the context of the case, this is that case. Careful, this should only be
            /// used by the related case methods.
            /// </summary>
            private Case _workingCase { get; set; }

            /// <summary>
            /// The start date of the case or calc period
            /// </summary>
            private DateTime _workingStartDate { get; set; }

            /// <summary>
            /// the end date of the case or calc period
            /// </summary>
            private DateTime _workingEndDate { get; set; }

            /// <summary>
            /// the list of statuses we are calculating for
            /// </summary>
            private List<AdjudicationStatus> _workingStatuses { get; set; }

            /// <summary>
            /// these are the cases that are being included on the calc for the current policy id
            /// this list is transient and is used only for processing 
            /// </summary>
            private List<Case> _includedCases { get; set; }

            /// <summary>
            /// this is the list of related cases, built both up and down the case list
            /// </summary>
            private List<Case> _relatedCases { get; set; }
            private bool _relatedCasesBuilt { get; set; }

            /// <summary>
            /// all the cases an employee has ever had
            /// </summary>
            private List<Case> _allOtherCases { get; set; }

            private List<AppliedPolicy> _allAppliedPolicies { get; set; }

            /// <summary>
            /// for the current applied policy this is the anchor point for the current start include date
            /// </summary>
            private DateTime _windowStartDate { get; set; }

            /// <summary>
            /// this is the date that the calculations are relative to,
            /// this is provided as part of the set up. It can be the case start date
            /// or a new date to calc against (say when the period end date is crossed
            /// during allocation)
            /// </summary>
            private DateTime _calcStartDate { get; set; }

            /// <summary>
            /// for the current applied policy this is the anchor point for the ending of the period
            /// </summary>
            private DateTime _windowEndDate { get; set; }

            /// <summary>
            /// the window is the look back period, this will tell us when the current period will end
            /// </summary>
            private DateTime _nextPeriodStartDate { get; set; }

            /// <summary>
            /// Force the reset period to be something other than the days between _windowStartDate and _windowEndDate
            /// So far this is only used in rolling back and is used to trim the usage collection
            /// each day
            /// </summary>
            private int? _daysInPeriodOverride { get; set; }

            /// <summary>
            /// the current employe
            /// </summary>
            private Employee _employee { get; set; }

            /// <summary>
            /// the current employer
            /// </summary>
            private Employer _employer { get; set; }

            /// <summary>
            /// The time frame type we are working with
            /// </summary>
            private PeriodType _currentPeriodType { get; set; }

            private bool _includePerUseCase { get; set; }

            /// <summary>
            /// The third use case: CalcPriorUsage isn't really calculating prior usage but is displaying
            /// the current case on the UI. In this case our PerUse calc type needs to include
            /// the current case otherwise there will be nothing to display on the UI.
            /// This is different then when we are running it to calculate for the current case
            /// when that happens we to ignore the data from the current case because we are regenerating this
            /// This confusion seems like a good 2.0 refactor canidate
            /// </summary>
            /// <param name="theCase">The case.</param>
            /// <param name="startDate">The start date.</param>
            /// <param name="newCase">if set to <c>true</c> [new case].</param>
            /// <param name="includeCurrentCase">if set to <c>true</c> [include current case].</param>
            /// <param name="excludeOtherCases">if set to <c>true</c> [exclude other cases].</param>
            /// <param name="onlyPolicyCode">The only policy code.</param>
            /// <param name="inlcudePerUseCase">if set to <c>true</c> [inlcude per use case].</param>
            /// <param name="adjStatuses">The adj statuses.</param>
            public CalculatePriorUsage(Case theCase, DateTime startDate, bool newCase, bool includeCurrentCase, bool excludeOtherCases,
                string onlyPolicyCode, bool inlcudePerUseCase, params AdjudicationStatus[] adjStatuses)
            {
                using (new InstrumentationContext("CaseService.CalcPriorUsage:3"))
                {
                    _includePerUseCase = inlcudePerUseCase;
                    RunMethodInCurrentCaseContext(theCase, startDate, newCase, includeCurrentCase, excludeOtherCases, onlyPolicyCode, adjStatuses);
                }
            }

            /// <summary>
            /// initialize the object and then call the main processing method
            /// once it is done the results will be readable from the UsageTotals object
            /// </summary>
            /// <param name="theCase">Calculate relative to this case</param>
            /// <param name="startDate">The start date.</param>
            /// <param name="newCase">Is this a new case?</param>
            /// <param name="includeCurrentCase">Include usage from the current case (example: used in intermitent)</param>
            /// <param name="excludeOtherCases">if set to <c>true</c> [exclude other cases].</param>
            /// <param name="onlyPolicyCode">The only policy code.</param>
            /// <param name="adjStatuses">The status to calculate if not pending and approved</param>
            public CalculatePriorUsage(Case theCase, DateTime startDate, bool newCase, bool includeCurrentCase, bool excludeOtherCases, string onlyPolicyCode, params AdjudicationStatus[] adjStatuses)
            {
                using (new InstrumentationContext("CaseService.CalcPriorUsage:1"))
                {
                    RunMethodInCurrentCaseContext(theCase, startDate, newCase, includeCurrentCase, excludeOtherCases, onlyPolicyCode, adjStatuses);
                }                                           // using (new InstrumentationContext("CaseService.PreValidateTimeOffRequest"))
            }


            /// <summary>
            /// Runs the method in current case context.
            /// </summary>
            /// <param name="theCase">The case.</param>
            /// <param name="startDate">The start date.</param>
            /// <param name="newCase">if set to <c>true</c> [new case].</param>
            /// <param name="includeCurrentCase">if set to <c>true</c> [include current case].</param>
            /// <param name="excludeOtherCases">if set to <c>true</c> [exclude other cases].</param>
            /// <param name="onlyPolicyCode">The only policy code.</param>
            /// <param name="adjStatuses">The adj statuses.</param>
            /// <exception cref="AbsenceSoftException">CaseService.CalcPriorUsage:1: Mutually exclusive options would result in no cases calculated.</exception>
            private void RunMethodInCurrentCaseContext(Case theCase, DateTime startDate, bool newCase, bool includeCurrentCase, bool excludeOtherCases, string onlyPolicyCode, params AdjudicationStatus[] adjStatuses)
            {
                // mutally exclusive swithces
                if (!includeCurrentCase && excludeOtherCases)
                    throw new AbsenceSoftException("CaseService.CalcPriorUsage:1: Mutually exclusive options would result in no cases calculated.");

                _workingCase = theCase;
                _calcStartDate = startDate;

                _workingStatuses = new List<AdjudicationStatus>(adjStatuses);

                // the default behaviour is to get the usage for pending and approved
                if (adjStatuses.Length == 0)
                {
                    _workingStatuses.Add(AdjudicationStatus.Pending);
                    _workingStatuses.Add(AdjudicationStatus.Approved);
                }

                // initalize the state
                _includedCases = new List<Case>();
                _relatedCases = new List<Case>();
                _allOtherCases = new List<Case>();
                _relatedCasesBuilt = false;

                _employee = theCase.Employee;
                _employer = theCase.Employer;
                _workingStartDate = startDate;
                _workingEndDate = theCase.EndDate ?? startDate.AddDays(1).ToMidnight();

                // get all the cases we might care about while doing this calc
                if (!excludeOtherCases)
                    _allOtherCases = Case.AsQueryable().Where(c => c.Employee.Id == _employee.Id && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Inquiry && c.Id != _workingCase.Id).OrderBy(o => o.StartDate).ToList();

                // if including the current case, then add in those applied policies as well
                if (includeCurrentCase)
                    _allOtherCases.Add(theCase);

                // build a shortcust list off all of the applied policies on this case
                _allAppliedPolicies = theCase.Segments
                    .Where(s => s.Status != CaseStatus.Cancelled)
                    .SelectMany(s => s.AppliedPolicies)
                    .Where(p => p.Status != EligibilityStatus.Ineligible && (string.IsNullOrWhiteSpace(onlyPolicyCode) || p.Policy.Code == onlyPolicyCode))
                    .ToList();

                // build the releated cases (if necessary)
                BuildRelatedCases();

                // and do the work
                Process();
            }


            /*******************************************
             * 
             * The other way to run this. For an employee by date with no case
             * 
             * *****************************************/
            public CalculatePriorUsage(string empId, DateTime startDate, params AdjudicationStatus[] adjStatuses)
            {
                using (new InstrumentationContext("CaseService.CalcPriorUsage:2"))
                {

                    _calcStartDate = startDate;

                    _workingStatuses = new List<AdjudicationStatus>(adjStatuses);

                    // the default behaviour is to get the usage for pending and approved
                    if (adjStatuses.Length == 0)
                    {
                        _workingStatuses.Add(AdjudicationStatus.Pending);
                        _workingStatuses.Add(AdjudicationStatus.Approved);
                    }

                    // initalize the state
                    _includedCases = new List<Case>();
                    _relatedCases = new List<Case>();
                    _relatedCasesBuilt = false;

                    _employee = Employee.GetById(empId);
                    if (_employee == null)
                        throw new AbsenceSoftException("CaseService_PriorUsage: CalculatePriorUsage:2 Employee not found.");

                    _employer = _employee.Employer;
                    _workingStartDate = startDate;
                    _workingEndDate = DateTime.MaxValue;

                    _allOtherCases = Case.AsQueryable().Where(c => c.Employee.Id == _employee.Id && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Inquiry).OrderBy(o => o.StartDate).ToList();

                    // when calcing all for policy summary we don't care that the cases are related, as far as we
                    // are concerned for this use they are all related
                    _relatedCases = _allOtherCases;

                    // build a shortcust list off all of the applied policies for all time
                    _allAppliedPolicies = _allOtherCases.SelectMany(aoc => aoc.Segments).Where(s => s.Status != CaseStatus.Cancelled)
                        .SelectMany(s => s.AppliedPolicies).Where(a => a.Status != EligibilityStatus.Ineligible).ToList();

                    // and do the work
                    Process();
                }
            }


            /// <summary>
            /// check the _applied policy list and see if any of those include spouses, and if they do
            /// and they employee has a spouse, add in the required policies
            /// </summary>
            private void LoadSpouse()
            {
                // if no spouse... don't bother
                if (string.IsNullOrWhiteSpace(_employee.SpouseEmployeeNumber))
                    return;

                if (!_employee.Employer.EnableSpouseAtSameEmployerRule)
                    return;

                // get a list of policies on this employee that are combined with the spouse
                List<string> matchThese = _allAppliedPolicies.Where(ap => ap.PolicyReason.CombinedForSpouses && ap.Status != EligibilityStatus.Ineligible).Select(ap => ap.Policy.Code).Distinct().ToList();
                if (matchThese == null || matchThese.Count == 0)
                    return;

                // get the spouses cases where Absence Reason Matches with the _Working case reason Code
                if (_workingCase == null)
                    return;

                List<Case> oldCases = Case.AsQueryable().Where(c => c.CustomerId == _employee.CustomerId
                            && c.EmployerId == _employee.EmployerId
                            && c.Employee.EmployeeNumber == _employee.SpouseEmployeeNumber
                            && c.Status != CaseStatus.Cancelled && c.Reason.Code == _workingCase.Reason.Code)
                            .ToList();

                // if there arn't any, then why bother?
                if (oldCases == null || oldCases.Count == 0)
                    return;

                // pull out just the policies that we care about
                List<AppliedPolicy> spousal = oldCases.SelectMany(oc => oc.Segments)
                                                    .Where(s => s.Status != CaseStatus.Cancelled)
                                                    .SelectMany(s => s.AppliedPolicies)
                                                    .Where(ap => matchThese.Any(mt => mt == ap.Policy.Code) && ap.Status != EligibilityStatus.Ineligible)
                                                    .ToList();

                if (spousal == null || spousal.Count == 0)
                    return;
               
                // we need to pull out the cases that have the policies we are interested in
                // however, we only need the applied policies that are in the matches these
                // and this should probably be refactored to just use an applied policy collection
                // instead of a case (that would be more effiecient and I wish I had thought of 
                // that when I first wrote it)                
                foreach (Case sc in oldCases)
                {
                    Case holdingPen = new Case();
                    CaseSegment cs = new CaseSegment();
                    foreach (CaseSegment oldcs in sc.Segments.Where(s => s.Status != CaseStatus.Cancelled))
                    {
                        foreach (AppliedPolicy iap in oldcs.AppliedPolicies.Where(p => p.Status != EligibilityStatus.Ineligible))
                        {
                            if (matchThese.Any(mt => mt == iap.Policy.Code))
                                cs.AppliedPolicies.Add(iap);
                        }
                        if (cs.AppliedPolicies.Count > 0)
                            holdingPen.Segments.Add(cs);
                    }
                    if (holdingPen.Segments.Count > 0)
                    {
                        holdingPen.Employee = sc.Employee;
                        holdingPen.Employer = sc.Employer;
                        holdingPen.StartDate = sc.StartDate;
                        holdingPen.EndDate = sc.EndDate;
                        _allOtherCases.Add(holdingPen);
                    }
                }

                _allAppliedPolicies.AddRange(spousal);
            }

            /// <summary>
            /// this is what drives the work
            /// </summary>
            private void Process()
            {

                LoadSpouse();

                // get all the policsies that we need the totals for
                UsageHistory = new PolicyUsageHistory();

                // get a unique list of the policy ids that we are working with
                List<string> policyCodes = _allAppliedPolicies.Select(ap => ap.Policy.Code).Distinct().ToList();

                LeaveOfAbsence los = new LeaveOfAbsence();
                AppliedPolicyUsageHelper theHelper = null;
                HolidayService hs = new HolidayService();
                List<DateTime> holidays = new List<DateTime>();
                List<int> holidayYears = new List<int>();

                //LeaveOfSegments object is needed for AppliedPolicyUsageHelper object creation
                //_workingCase will be null if Employee is being created OR the employee has no case OR employee view is being opened
                if (_workingCase != null && policyCodes.Any())
                {
                    los.Employee = _workingCase.Employee;
                    los.Employer = _workingCase.Employee.Employer;
                    los.Customer = _workingCase.Employee.Customer;
                    los.WorkSchedule = Employee.GetById(_workingCase.Employee.Id)?.WorkSchedules;

                    // get the holidays for the employer                        
                    holidayYears = Enumerable.Range(0, (_workingCase.EndDate.Value.Year - _workingCase.StartDate.Year) + 1).Select(i => _workingCase.StartDate.Year + i).ToList();
                    holidays = hs.GetHolidayList(holidayYears, _workingCase.Employer);
                }

                // for each applied policy we need to look at it's processing method and set the _includedCases collection
                foreach (string applying in policyCodes)
                {
                    // this builds the list of cases to look at and sets the start and end dates
                    SetProcessingParamaters(applying);

                    // set the enddate for this policies history, so we know if we have to reset while calculating in the future
                    UsageHistory[applying].NextPeriodStartDate = _nextPeriodStartDate;
                    UsageHistory[applying].MeasureStartDate = _windowStartDate;
                    UsageHistory[applying].MeasureEndDate = _windowEndDate;
                    UsageHistory[applying].MeasurePeriodType = _currentPeriodType;
                    UsageHistory[applying].DaysInPeriod = (_windowEndDate - _windowStartDate).Days + 1;
                    UsageHistory[applying].ResetDays = _daysInPeriodOverride ?? UsageHistory[applying].DaysInPeriod;

                    //look for current policy usage across all related cases
                    foreach (Case caseForPolicy in _includedCases)
                    {
                        // now that we have set the date and the policies to process get all the applied policies usage objects that are in that date range
                        List<AppliedPolicyUsage> usage = caseForPolicy.Segments.SelectMany(s => s.AppliedPolicies)
                                                            .Where(ap => ap.Policy.Code == applying && ap.Status != EligibilityStatus.Ineligible)
                                                            .SelectMany(ap => ap.Usage)
                                                            .Where(apu => apu.DateUsed >= _windowStartDate && apu.DateUsed <= _windowEndDate)
                                                            .ToList();
                        if (usage == null || usage.Count == 0)
                        {
                            if (_workingCase != null)
                            {
                                //System doesn't found any usage during Window period , but user may create other cases 
                                //which intersect or are in the future of the current case those need to be taken care also  
                                //for usage calculation 
                                usage = caseForPolicy.Segments.SelectMany(s => s.AppliedPolicies)
                                          .Where(ap => ap.Policy.Code == applying && ap.Status != EligibilityStatus.Ineligible)
                                          .SelectMany(ap => ap.Usage)
                                          .Where(apu => apu.DateUsed >= _workingCase.StartDate && apu.DateUsed <= _workingCase.EndDate)
                                          .ToList();
                                if (usage == null || usage.Count == 0)
                                {
                                    continue;
                                }
                            }
                        }
                        
                        if (los.Employee == null)
                        {
                            los.Employee = _employee;
                            los.Employer = _employee.Employer;
                            los.Customer = _employee.Customer;
                            los.WorkSchedule = Employee.GetById(_employee.Id).WorkSchedules;
                        }

                        //LeaveOfSegments object is needed for AppliedPolicyUsageHelper object creation
                        if (los.Employee == null || caseForPolicy.Employee.Id != los.Employee.Id)
                        {
                            los.Employee = caseForPolicy.Employee;
                            los.Employer = caseForPolicy.Employee.Employer;
                            los.Customer = caseForPolicy.Employee.Customer;
                            los.WorkSchedule = Employee.GetById(caseForPolicy.Employee.Id).WorkSchedules;
                        }                                                
                        //holidayYears will be empty for all conditions where _workingCase is null
                        //Need to fetch the holidays again if a case being processed is stretched to a different year
                        if (holidayYears.Count <= 0 || !(holidayYears.Contains(caseForPolicy.EndDate.Value.Year)) || !(holidayYears.Contains(caseForPolicy.StartDate.Year)))
                        {
                            // get the holidays for the employer                        
                            holidayYears = Enumerable.Range(0, (caseForPolicy.EndDate.Value.Year - caseForPolicy.StartDate.Year) + 1).Select(i => caseForPolicy.StartDate.Year + i).ToList();
                            holidays = hs.GetHolidayList(holidayYears, caseForPolicy.Employer);
                        }
                        var CasePolicies = caseForPolicy.Segments.SelectMany(s => s.AppliedPolicies)
                            .Where(p => p.Policy.Code == applying && p.Status != EligibilityStatus.Ineligible);

                        foreach (AppliedPolicy policy in CasePolicies)
                        {
                            theHelper = new AppliedPolicyUsageHelper(los, caseForPolicy.StartDate, caseForPolicy.EndDate.Value, policy.PolicyBehaviorType);

                            // now decide what to do with those usage totals
                            foreach (AppliedPolicyUsage u in usage.Where(u => u.DateUsed >= policy.StartDate && u.DateUsed <= policy.EndDate))
                            {
                                //prepare the AppliedPolicyUsageType object, to be used in calculation of total usage 
                                List<AppliedPolicyUsageType> availableToUse = theHelper.GetUsage(u.DateUsed, policy.PolicyBehaviorType, holidays);
                                var policyReasonCode = !string.IsNullOrEmpty(policy.PolicyReason.ReasonCode) ? policy.PolicyReason.ReasonCode : policy.PolicyReason.Reason.Code;

                                if (_workingStatuses.Any(aa => aa == u.Determination))
                                    UsageHistory[applying].AddAllocated(u.DateUsed, u.MinutesUsed, u.MinutesInDay, policyReasonCode, u.Determination, 
                                        u.DenialReasonCode, u.IntermittentDetermination, availableToUse,null,null,false, caseForPolicy.CaseNumber);
                                else
                                    // We still need to add it, however minutes used will be 0
                                    UsageHistory[applying].AddAllocated(u.DateUsed, 0d, u.MinutesInDay, policyReasonCode, u.Determination, 
                                        u.DenialReasonCode, u.IntermittentDetermination, availableToUse,null, null, false, caseForPolicy.CaseNumber);

                            }
                        }
                    }                                       // foreach (Case caseForPolicy in _includedCases)
                }                                           // foreach (Guid applying in policyIds)

            }                                               // private void Process()

            /// <summary>
            /// Take the policy id, get the first instance of it in the case and use this to
            /// set up how we are processing the current policy, this includes
            /// setting the start and end dates and what policies are being processed
            /// </summary>
            /// <param name="theAP"></param>
            private void SetProcessingParamaters(string policyCode)
            {
                // get the first instance of the policy
                AppliedPolicy workPolicy = _allAppliedPolicies.FirstOrDefault(ap => ap.Policy.Code == policyCode);

                // something has gone really wrong
                if (workPolicy == null)
                    throw new AbsenceSoftException("CaseService_PriorUsage: SetProcessingParamaters policy not found.");

                // at this point in time this is only used by the rolling back, but, in theory
                // it could be used by any calc type. Set this to change the days
                // between recalcs otherwise it defaults to the difference between the window start and the window end date
                _daysInPeriodOverride = null;
                _currentPeriodType = workPolicy.PolicyReason.PeriodType.Value;

                // based on the policy period type, load up the _includedCases collection
                switch (_currentPeriodType)
                {
                    // per case does not look at history
                    case PeriodType.PerCase:
                    case PeriodType.PerOccurrence:
                        _windowStartDate = DateTime.MinValue;                  // just here for consistency
                        _windowEndDate = _workingEndDate;                // ditto

                        // Never count "prior" usage, always just this case, so set this to an empty list.

                        // see note in PerUse... it is the same problem. If we are calculating eligibility
                        // then this needs to be empty, if we are displaying on the UI
                        // then we need the data to total
                        if (_includePerUseCase)
                            _includedCases = new List<Case>() { _workingCase };
                        else
                            _includedCases = new List<Case>(0);

                        _nextPeriodStartDate = DateTime.MaxValue;                     // doesn't matter, not used
                        break;
                    // calendar year is fixed to the year, the only question is how far to look back
                    case PeriodType.CalendarYear:
                        _windowStartDate = _calcStartDate.GetFirstDayOfYear();
                        _windowEndDate = _calcStartDate.GetLastDayOfYear();
                        if (workPolicy.PolicyReason.Period.HasValue && workPolicy.PolicyReason.Period.Value > 1)
                        {
                            //We are doing a hack for good cause ,Normally Calendar means 1 calendar year , but there are policy 
                            //such as  USERRA which used 5years . so we can't set it blindly , instead after discussion 
                            //with Chad we are overrideing  it if `Period % 12 != 0` then use whatever value specified in Period
                            //we can't do null check here as PeriodType gets replaced much upstream 

                            if (workPolicy.PolicyReason.Period % 12 != 0)
                            {
                                _windowStartDate = _windowStartDate.AddYears(-((int)workPolicy.PolicyReason.Period.Value - 1));
                                _windowEndDate = _calcStartDate.AddYears((int)workPolicy.PolicyReason.Period.Value - 1).GetLastDayOfYear();
                            }
                        }

                        // If the workPolicy.PolicyReason.Period is null than set to 12 months back at least
                        if (!workPolicy.PolicyReason.Period.HasValue && workPolicy.PolicyReason.Period % 12 != 0)
                        {
                            _windowStartDate = _windowStartDate.AddYears(-Convert.ToInt32(12d)).AddDays(1);
                        }

                        // and the period rolls over on Jan 1 of the next year
                        _nextPeriodStartDate = _windowEndDate.AddDays(1);
                        _includedCases = _allOtherCases;

                        break;
                    case PeriodType.FixedResetPeriod:
                        // set the year back, we will roll it forward to find the right date for the case but we need it to start off outside the
                        // limit (we only want to worry about rolling the date forward not back)
                        DateTime? policyFixedResetDate = GetPolicyFixedResetDate(workPolicy);
                        _windowEndDate = (policyFixedResetDate != null) ? (DateTime)policyFixedResetDate : new DateTime(DateTime.UtcNow.Year - 5, (int)(_employer.ResetMonth ?? Month.January), _employer.ResetDayOfMonth ?? 1, 0, 0, 0, DateTimeKind.Utc);
                        _windowStartDate = _windowEndDate.AddYears(-1);
                        _nextPeriodStartDate = _windowEndDate;
                        _windowEndDate = _windowEndDate.AddDays(-1);
                        _includedCases = _allOtherCases;

                        SetWorkingDatesToCaseYear();

                        break;
                    case PeriodType.SchoolYear:
                        // set the year back, we will roll it forward to find the right date for the case but we need it to start off outside the
                        // limit (we only want to worry about rolling the date forward not back)
                        _windowStartDate = new DateTime(DateTime.UtcNow.Year - 5, (int)Month.July, 1, 0, 0, 0, DateTimeKind.Utc);
                        _windowEndDate = _windowStartDate.AddYears(1);
                        _includedCases = _allOtherCases;

                        SetWorkingDatesToCaseYear();
                        _nextPeriodStartDate = _windowEndDate.AddYears(1);
                        break;
                    case PeriodType.SchoolYearPerContact:
                        // set the year back, we will roll it forward to find the right date for the case but we need it to start off outside the
                        // limit (we only want to worry about rolling the date forward not back)
                        _windowStartDate = new DateTime(DateTime.UtcNow.Year - 5, (int)Month.July, 1, 0, 0, 0, DateTimeKind.Utc);
                        _windowEndDate = _windowStartDate.AddYears(1);
                        _includedCases = _allOtherCases.Where(c => _workingCase == null || c.Contact == null || _workingCase.Contact == null || c.Contact.Id == _workingCase.Contact.Id).ToList();

                        SetWorkingDatesToCaseYear();
                        _nextPeriodStartDate = _windowEndDate.AddYears(1);
                        break;

                    case PeriodType.FixedYearFromServiceDate:
                        _windowStartDate = _employee.ServiceDate.Value;
                        _windowEndDate = _windowStartDate.AddYears(1).AddDays(-1);
                        _includedCases = _allOtherCases;

                        SetWorkingDatesToCaseYear();
                        _nextPeriodStartDate = _windowEndDate.AddYears(1);
                        break;

                    case PeriodType.Lifetime:
                        _windowStartDate = DateTime.MinValue;
                        _windowEndDate = _calcStartDate.AddDays(-1);
                        _nextPeriodStartDate = DateTime.MaxValue;                 // it never ends
                        _includedCases = _allOtherCases;
                        break;

                    // rolling back, usually set to 12 to set a rolling window of back a year
                    case PeriodType.RollingBack:
                        SetDatesBasedOnPeriodUnit(workPolicy);
                        //_windowStartDate = _calcStartDate.AddMonths(-Convert.ToInt32(workPolicy.PolicyReason.Period ?? 12d));
                        //_windowEndDate = _calcStartDate.AddDays(-1);
                        _nextPeriodStartDate = _calcStartDate.AddDays(1);
                        _daysInPeriodOverride = 1;
                        _includedCases = _allOtherCases;
                        break;
                    case PeriodType.RollingForward:
                        SetRollingForwardDates(workPolicy);
                        _nextPeriodStartDate = _windowStartDate.AddYears(1);
                        _includedCases = _allOtherCases;
                        break;
                }
            }                           

            /// <summary>
            /// rolling forward is a little to complicated for a few lines of code in a switch
            /// so it is broken out here
            /// </summary>
            private void SetRollingForwardDates(AppliedPolicy workPolicy)
            {
                // cop out check, if there are no other cases then set it and forget it
                if (_allOtherCases.Count == 0)
                {
                    _windowStartDate = workPolicy?.StartDate ?? _calcStartDate;

                    SetWindowEndDateForRollingForward(workPolicy);
                    return;
                }

                var absoluteMin = _allOtherCases.SelectMany(c => c.Segments).SelectMany(s => s.AppliedPolicies).Where(p => p.Policy.Code == workPolicy.Policy.Code)
                    .SelectMany(p => p.Usage.Where(u => u.Determination == AdjudicationStatus.Approved && u.MinutesUsed > 0)).Min(u => new DateTime?(u.DateUsed))
                    ?? _calcStartDate;

                // otherwise the date is based off of their first time off,
                // set it and roll it forward(just make sure they are not trying to back date it)
                if (absoluteMin >= _calcStartDate)
                {
                    _windowStartDate = _calcStartDate;
                    SetWindowEndDateForRollingForward(workPolicy);
                    
                }
                else
                {
                    _windowStartDate = absoluteMin;
                    SetWindowEndDateForRollingForward(workPolicy);
                    RollWorkingDatesForwardToCaseYear(workPolicy);
                }
            }

            /// <summary>
            /// Set Start/End dates based on PeriodUnit for RollingForward
            /// </summary>
            private void RollWorkingDatesForwardToCaseYear(AppliedPolicy workPolicy)
            {
                // if the case start date is before the window date then something is wrong
                if (_calcStartDate < _windowStartDate)
                    throw new AbsenceSoftException("Caseservice_PriorUsate: RollWorkingDateForToCaseYear: Case start date before calc window begin date.");

                // if the case start date falls within the window then we are good
                if (_calcStartDate.DateInRange(_windowStartDate, _windowEndDate))
                    return;

                // otherwise, start rolling forward until we get there
                int x = 0;          // it's a govner
                while (!_calcStartDate.DateRangesOverLap(_workingEndDate, _windowStartDate, _windowEndDate))
                {
                    switch (workPolicy.PolicyReason.PeriodUnit)
                    {
                        case Unit.Days:
                            _windowStartDate = _windowStartDate.AddDays(workPolicy.PolicyReason.Period??1d);
                            _windowEndDate = _windowEndDate.AddDays(workPolicy.PolicyReason.Period??1d);
                            break;
                        case Unit.Weeks:
                            _windowStartDate = _windowStartDate.AddWeeks(workPolicy.PolicyReason.Period??1d);
                            _windowEndDate = _windowEndDate.AddWeeks(workPolicy.PolicyReason.Period??1d);
                            break;
                        case Unit.Years:
                            _windowStartDate = _windowStartDate.AddYears(Convert.ToInt32(workPolicy.PolicyReason.Period));
                            _windowEndDate = _windowEndDate.AddYears(Convert.ToInt32(workPolicy.PolicyReason.Period));
                            break;
                        // Default Month
                        default:
                            _windowStartDate = _windowStartDate.AddMonths(Convert.ToInt32(workPolicy.PolicyReason.Period ?? 12d));
                            _windowEndDate = _windowEndDate.AddMonths(Convert.ToInt32(workPolicy.PolicyReason.Period ?? 12d));
                            break;
                    }
                    
                    x++;
                    if (x > 100)
                        throw new AbsenceSoftException("Caseservice_PriorUsate: RollWorkingDateForToCaseYear: Case start date out of range.");

                }
            }                               

            
            /// <summary>
            /// if there are any related case policies, then this builds the list
            /// </summary>
            private void BuildRelatedCases()
            {
                // if there are not any per occurance types, then we do not need to build the list of related cases
                if (!_workingCase.Segments.Where(s => s.Status != CaseStatus.Cancelled).SelectMany(s => s.AppliedPolicies)
                    .Any(ap => ap.Status != EligibilityStatus.Ineligible && ap.PolicyReason.PeriodType == PeriodType.PerOccurrence))
                    return;

                // if we have already built them, then don't do it again
                if (_relatedCasesBuilt)
                    return;

                _relatedCases = new List<Case>();

                // find the top most case in this list
                Case theTop;
                if (string.IsNullOrWhiteSpace(_workingCase.RelatedCaseNumber))
                    theTop = _workingCase;
                else
                    theTop = FindTopRelatedCase(_workingCase.RelatedCaseNumber);

                // if it wansn't found then make it this one
                if (theTop == null)
                    theTop = _workingCase;

                // then build the list down from here
                _relatedCases = new List<Case>();
                if (theTop.Id != null)
                    FindRelatedCases(theTop.Id.ToString());

                _relatedCasesBuilt = true;

            }

            public bool WorkingCaseHasAnyPerOccurrencePolicies()
            {
                if (_workingCase.Segments.All(s => s.Status == CaseStatus.Cancelled))
                {
                    return false;
                }

                var appliedPolicies = _workingCase.Segments
                    .Where(s => s.Status != CaseStatus.Cancelled)
                    .SelectMany(s => s.AppliedPolicies);

                return appliedPolicies.Any(ap =>
                    ap != null
                    && ap.Status != EligibilityStatus.Ineligible
                    && ap.PolicyReason != null
                    && ap.PolicyReason.PeriodType == PeriodType.PerOccurrence);
            }

            /// <summary>
            /// it's cases all the way up
            /// </summary>
            /// <param name="caseId"></param>
            /// <returns></returns>
            private Case FindTopRelatedCase(string caseId)
            {
                Case found = _allOtherCases.FirstOrDefault(c => c.Id == caseId);
                if (found == null)
                    return null;

                if (string.IsNullOrWhiteSpace(found.RelatedCaseNumber))
                    return found;

                return FindTopRelatedCase(found.RelatedCaseNumber);
            }

            /// <summary>
            /// build the list from the top down
            /// </summary>
            /// <param name="caseId"></param>
            private void FindRelatedCases(string caseId)
            {
                Case current = _allOtherCases.FirstOrDefault(c => c.Id == caseId);
                if (current == null)
                    return;

                if (current.Id != _workingCase.Id)
                    _relatedCases.Add(current);

                List<Case> theKids = _allOtherCases.Where(c => c.RelatedCaseNumber == caseId).ToList();
                foreach (Case kid in theKids)
                    FindRelatedCases(kid.Id.ToString());
            }

            /// <summary>
            /// build the list from the top down
            /// </summary>
            /// <param name="workPolicy"></param>
            private DateTime? GetPolicyFixedResetDate(AppliedPolicy workPolicy)
            {
                if (!workPolicy.PolicyReason.ResetMonth.HasValue || !workPolicy.PolicyReason.ResetDayOfMonth.HasValue)
                    return null;

                DateTime? policyFixedResetDate = null;

                DateTime tmpPolicyFixedResetDate = new DateTime(DateTime.UtcNow.Year, (int)(workPolicy.PolicyReason.ResetMonth), (int)(workPolicy.PolicyReason.ResetDayOfMonth), 0, 0, 0, DateTimeKind.Utc);
                if (tmpPolicyFixedResetDate <= DateTime.UtcNow.Date)
                {
                    policyFixedResetDate = new DateTime(DateTime.UtcNow.Year, (int)(workPolicy.PolicyReason.ResetMonth), (int)(workPolicy.PolicyReason.ResetDayOfMonth), 0, 0, 0, DateTimeKind.Utc);
                }
                else
                {
                    policyFixedResetDate = new DateTime(DateTime.UtcNow.Year - 1, (int)(workPolicy.PolicyReason.ResetMonth), (int)(workPolicy.PolicyReason.ResetDayOfMonth), 0, 0, 0, DateTimeKind.Utc);
                }

                return policyFixedResetDate;
            }

           

            /// <summary>
            /// Set the StartDate/EndDate based on PeriodUnit 
            /// </summary>
            /// <param name="workPolicy"></param>
            private void SetDatesBasedOnPeriodUnit(AppliedPolicy workPolicy)
            {
                switch (workPolicy.PolicyReason.PeriodUnit)
                {
                    case Unit.Days:
                        _windowStartDate = _calcStartDate.AddDays(-workPolicy.PolicyReason.Period??1d).AddDays(1);
                        _windowEndDate = _calcStartDate.AddDays(-1);
                        break;
                    case Unit.Weeks:
                        _windowStartDate = _calcStartDate.AddWeeks(-workPolicy.PolicyReason.Period??1d).AddDays(1);
                        _windowEndDate = _calcStartDate.AddDays(-1);
                        break;
                    case Unit.Years:
                        _windowStartDate = _calcStartDate.AddYears(-Convert.ToInt32(workPolicy.PolicyReason.Period)).AddDays(1);
                        _windowEndDate = _calcStartDate.AddDays(-1);
                        break;
                    // Default Month
                    default:
                        _windowStartDate = _calcStartDate.AddMonths(-Convert.ToInt32(workPolicy.PolicyReason.Period ?? 12d)).AddDays(1);
                        _windowEndDate = _calcStartDate;
                        break;
                }
            }

            /// <summary>
            /// some of the start date types are based on an arbitrary date in the past, hire date, fixed date
            /// we need to roll those forward to the current year so that the working start and 
            /// </summary>
            private void SetWorkingDatesToCaseYear()
            {
                // if the case start date is before the window date then something is wrong
                if (_calcStartDate < _windowStartDate)
                    throw new AbsenceSoftException("Caseservice_PriorUsate: RollWorkingDateForToCaseYear: Case start date before calc window begin date.");

                // if the case start date falls within the window then we are good
                if (_calcStartDate.DateInRange(_windowStartDate, _windowEndDate))
                    return;

                // otherwise, start rolling forward until we get there
                int x = 0;          // it's a govner
                while (!_calcStartDate.DateRangesOverLap(_workingEndDate, _windowStartDate, _windowEndDate))
                {
                    _windowStartDate = _windowStartDate.AddYears(1);
                    _windowEndDate = _windowEndDate.AddYears(1);
                    x++;
                    if (x > 100)
                        throw new AbsenceSoftException("Caseservice_PriorUsate: RollWorkingDateForToCaseYear: Case start date out of range.");

                }
            }

            /// <summary>
            /// Set WindowEndDate based on PeriodUnit for RollingForward 
            /// </summary>
            /// <param name="workPolicy"></param>
            private void SetWindowEndDateForRollingForward(AppliedPolicy workPolicy)
            { 
                switch (workPolicy.PolicyReason.PeriodUnit)
                {
                    case Unit.Days:
                        _windowEndDate = _windowStartDate.AddDays(workPolicy.PolicyReason.Period??1d).AddDays(-1);
                        break;
                    case Unit.Weeks:
                        _windowEndDate = _windowStartDate.AddWeeks(workPolicy.PolicyReason.Period??1d).AddDays(-1);
                        break;
                    case Unit.Years:
                        _windowEndDate = _windowStartDate.AddYears(Convert.ToInt32(workPolicy.PolicyReason.Period)).AddDays(-1); 
                        break;
                    // Default Month
                    default:
                        _windowEndDate = _windowStartDate.AddMonths(Convert.ToInt32(workPolicy.PolicyReason.Period ?? 12d)).AddDays(-1); 
                        break;
                }
            }

            /// <summary>
            /// •	When calculating prior usage for an Administrative segment type date, will only match prior usage for other Administrative case type segments.
            /// •	And, When calculating prior usage for any other segment type date, will exclude Administrative time
            /// </summary>
            public PolicyUsageHistory ProcessAdministrativePolicyUsage()
            {
                //If the flag "OnlyCalculateUsageForLostTime" on the Policy Absence Reason is TRUE, it does not count the time together (Admin time is separate from other lost-time types).
                List<AppliedPolicy> allAdministrativeAppliedPolicies = _allOtherCases.SelectMany(aoc => aoc.Segments).Where(s => s.Status != CaseStatus.Cancelled && s.Type == CaseType.Administrative)?
                        .SelectMany(s => s.AppliedPolicies).Where(a => a.Status != EligibilityStatus.Ineligible && a.PolicyReason.OnlyCalculateUsageForLostTime == true)?.ToList();

                if (allAdministrativeAppliedPolicies == null || !allAdministrativeAppliedPolicies.Any())
                {
                    return null;
                }

                if (UsageHistory == null || UsageHistory.UsageTotals == null || !UsageHistory.UsageTotals.Any())
                {
                    return null;
                }

                PolicyUsageHistory adminPolicyUsageHistory = new PolicyUsageHistory();
                adminPolicyUsageHistory.UsageTotals = UsageHistory.UsageTotals.Clone();
                adminPolicyUsageHistory.UsageTotals = adminPolicyUsageHistory.UsageTotals.Where(u => allAdministrativeAppliedPolicies.Any(a => a.Policy.Code == u.PolicyCode))?.ToList();

                //have saperate collection of administrative policy usage and non administrative policy usage
                foreach (AppliedPolicy adminPolicy in allAdministrativeAppliedPolicies)
                {
                    if (adminPolicyUsageHistory.UsageTotals != null && adminPolicyUsageHistory.UsageTotals.Any())
                    {
                        //select all allocations that belong to Administrative policy usage and store it saperate from non administrative usage 
                        adminPolicyUsageHistory.UsageTotals.ForEach(usage => usage.Allocated = usage.Allocated.Where(allocation => allocation.DateUsed.DateInRange(adminPolicy.StartDate, adminPolicy.EndDate)).ToList());                        
                    }
                    //exclude all allocations that belong to Administrative policy usage
                    UsageHistory.UsageTotals.ForEach(usage => usage.Allocated = usage.Allocated.Where(allocation => !allocation.DateUsed.DateInRange(adminPolicy.StartDate, adminPolicy.EndDate)).ToList());
                }                

                return adminPolicyUsageHistory;
            }
      
        }

    }
}
