﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using System.Text;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson.Serialization;
using AbsenceSoft.Common;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService
    {
        /// <summary>
        /// Checks the intermittent time request to ensure it's not stupid, 'cause stupid TORs are dumb and suck and
        /// are generally pretty aweful and we want to warn the caller that they're being stupid and should not be
        /// so. In all respects, this method is the gatekeeper to the "I told you so" path of virtual TOR rectitude.
        /// </summary>
        /// <param name="theCase">The case to check the TOR against.</param>
        /// <param name="tor">The Time Off Request to validate 'n' stuff (obviously need this, duh).</param>
        /// <returns>A <see cref="T:CertificationCheckResult"/> which contains the pass/fail, what was checked and any
        /// error messages as a result.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// theCase
        /// or
        /// tor
        /// </exception>
        public CertificationCheckResult CheckIntermittentTimeRequest(Case theCase, IntermittentTimeRequest tor)
        {
            // Check that our case and TOR passed in are not null, duh, they shouldn't be, but you never know
            if (theCase == null)
                throw new ArgumentNullException("theCase");
            if (tor == null)
                throw new ArgumentNullException("tor");

            // Evaluate the request within the case
            using (InstrumentationContext context = new InstrumentationContext("CaseService.CheckIntermittentTimeRequest"))
                return new CertificationChecker(theCase, tor).Evaluate();
        }

        /// <summary>
        /// Represents a single-use, stateful class for evaluating a single time off request against a case
        /// </summary>
        private class CertificationChecker
        {
            #region Members

            /// <summary>
            /// The case
            /// </summary>
            private Case theCase;
            /// <summary>
            /// The time off reqest
            /// </summary>
            private IntermittentTimeRequest tor;
            /// <summary>
            /// The work schedule, if any
            /// </summary>
            private List<Time> workSchedule;

            #endregion

            /// <summary>
            /// Initializes a new instance of the <see cref="CertificationChecker" /> class.
            /// </summary>
            /// <param name="case">The case.</param>
            /// <param name="request">The Time Off Request to validate 'n' stuff (obviously need this, duh).</param>
            /// <exception cref="System.ArgumentNullException">case
            /// or
            /// request</exception>
            public CertificationChecker(Case @case, IntermittentTimeRequest request)
            {
                #region .ctor

                // Check that our case and TOR passed in are not null, duh, they shouldn't be, but you never know
                if (@case == null)
                    throw new ArgumentNullException("case");
                if (request == null)
                    throw new ArgumentNullException("request");

                theCase = @case;
                tor = request;

                #endregion
            }

            /// <summary>
            /// Checks the intermittent time request to ensure it's not stupid, 'cause stupid TORs are dumb and suck and
            /// are generally pretty aweful and we want to warn the caller that they're being stupid and should not be
            /// so. In all respects, this method is the gatekeeper to the "I told you so" path of virtual TOR rectitude.
            /// </summary>
            /// <returns>A <see cref="T:CertificationCheckResult"/> which contains the pass/fail, what was checked and any
            /// error messages as a result.</returns>
            public CertificationCheckResult Evaluate()
            {
                using (InstrumentationContext context = new InstrumentationContext("CaseService.CertificationChecker.Evaluate"))
                {
                    // This is the magical date we're revolving everything around for our checks
                    DateTime testDate = tor.RequestDate;

                    // Build result that will be the return value
                    CertificationCheckResult result = new CertificationCheckResult()
                    {
                        CheckedTime = new Time() { SampleDate = testDate, TotalMinutes = tor.TotalMinutes },
                        Pass = true
                    };

                    try
                    {
                        // Check to ensure the requested date fits within the case first, easiest check obviously
                        if (!testDate.DateInRange(theCase.StartDate, theCase.EndDate))
                            return result.SetFail().SetErrorMessage(string.Format("The requested date, {0:MM/dd/yyyy}, is outside of the start and end date of this case.", testDate));

                        // Get the segment on the case that encompasses the requested date, regardless of type
                        var intSegment = theCase.Segments.FirstOrDefault(s => testDate.DateInRange(s.StartDate, s.EndDate));
                        // Ensure we have a matching segment and that the segment case type is intermittent, otherwise return a failure
                        if (intSegment == null || intSegment.Type != CaseType.Intermittent)
                            return result.SetFail().SetErrorMessage(string.Format("The requested date, {0:MM/dd/yyyy}, is outside of the start and end date of any intermittent period for this case.", testDate));

                        // Define a short-cut function to determine whether a TOR has any pending or approved time
                        var hasTime = new Func<IntermittentTimeRequest, bool>(t => t != null
                            && t.TotalMinutes > 0
                            && t.Detail != null
                            && t.Detail.Any()
                            && t.Detail.Max(d => d.Approved + d.Pending) > 0);

                        // If we don't have any time assigned for the TOR passed in, no need to do
                        //  any further validation, they're denying it, so let it slide.
                        if (!hasTime(tor))
                            return result;

                        // ********************************************************************************
                        // Check the actual Time Requested against Usage for each policy in the TOR detail
                        //  These are the sanity validation checks before we even get to the certifications
                        // ********************************************************************************
                        // Check time pending or approved in the new TOR against each policy's approved period to ensure they're not
                        //  approving or pending time where that time is denied, OR they are not approving time when the policy is pending
                        StringBuilder errors = new StringBuilder();
                        foreach (var detail in tor.Detail)
                        {
                            // Try to get the applied policy for this detail row
                            var ap = intSegment.AppliedPolicies.FirstOrDefault(p => p.Policy.Code == detail.PolicyCode);
                            if (ap == null)
                            {
                                // If we couldn't find one, that's an error, but since it's null let's see if it's anywhere on the policy so we can
                                //  give the user a better, more friendly error/validation message
                                ap = theCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == detail.PolicyCode)).FirstOrDefault();
                                if (ap == null)
                                {
                                    // !@#$, really!?!, not even on the case for crying out loud, ok, read it from the DB
                                    var policy = Policy.GetByCode(detail.PolicyCode, theCase.CustomerId, theCase.EmployerId);
                                    // If we can't find it in the DB at all, something horrible happened, we just need to bail all-together and return this as a fail with
                                    //  a super special error message 'n' stuff
                                    if (policy == null)
                                        return result.SetFail().SetErrorMessage(string.Format("The policy, \"{0}\", is missing or could not be found", detail.PolicyCode));
                                    // Ok, we actually found the policy, let's create a faux applied policy so we can use it for the error message after this block exits
                                    ap = new AppliedPolicy() { Policy = policy };
                                }
                                // Add an error message that the applied policy has no coverage for this date, denied or otherwise
                                errors.AppendLine(string.Format("{0} is not applicable for the requested date of {1:MM/dd/yyyy}. ", ap.Policy.Name, testDate));
                            }
                            else
                            {
                                // Ok, we have an applied policy for this date, let's check the status of the applied usage shall we?
                                var usage = ap.Usage.FirstOrDefault(u => u.DateUsed == tor.RequestDate);

                                // No usage and we have approved or pending time, then that's bad, we should set an error for that.
                                if (usage == null && detail.Approved + detail.Pending > 0)
                                {
                                    errors.AppendLine(string.Format("{0} has no time to use for the requested date of {1:MM/dd/yyyy}. ", ap.Policy.Name, testDate));
                                    continue;
                                }

                                // We have usage, but that usage is denied yet we're trying to pull the wool over and approve/pend time, naughty user!
                                if (usage.IntermittentDetermination == IntermittentStatus.Denied && detail.Approved + detail.Pending > 0)
                                {
                                    errors.AppendLine(string.Format("{0} is denied for the requested date of {1:MM/dd/yyyy}. ", ap.Policy.Name, testDate));
                                    continue;
                                }
                                // We have usage, but the usage is only pending.... Let's ensure we're not trying too approve when only pending time.
                                if (usage.IntermittentDetermination == IntermittentStatus.Pending && detail.Approved > 0)
                                {
                                    errors.AppendLine(string.Format("{0} is pending for the requested date of {1:MM/dd/yyyy} and you are requesting approved time. ", ap.Policy.Name, testDate));
                                    continue;
                                }


                                if (usage.MinutesInDay == 0d)
                                {
                                    continue;
                                }

                                //For the new FTE feature we should not have any warning for any ITOR unless that ITOR would put the usage for 
                                //that week > the employee's FTE weekly scheduled time. So need to check if it is FTE schedule before adding any message
                                LeaveOfAbsence leaveOfAbsence = new LeaveOfAbsence();
                                leaveOfAbsence.Employee = theCase.Employee;
                                double fteTimeForWeek = leaveOfAbsence.TimeLimitForTheDay(tor.RequestDate);

                                // We have usage, but the total minutes in the day are falling a little short of the total time they're trying to approve and/or pend for this day
                                //  that's not good so we should probably let them know they are being stupid
                                if (usage.MinutesInDay < detail.Approved + detail.Pending && fteTimeForWeek < 1)
                                {
                                    if (usage.MinutesInDay < detail.Approved)
                                        errors.AppendLine(string.Format("{0} is applicable based on the employee's work schedule for only {2} and you have entered a total of {3} for the requested date of {1:MM/dd/yyyy}. ",
                                            ap.Policy.Name, testDate, usage.MinutesInDay.ToFriendlyTime(), (detail.Approved + detail.Pending).ToFriendlyTime()));
                                    else
                                        errors.AppendLine(string.Format("{0} is applicable based on the employee's work schedule for only {2} and you have entered a total of {3} for the requested date of {1:MM/dd/yyyy}. ",
                                            ap.Policy.Name, testDate, usage.MinutesInDay.ToFriendlyTime(), (detail.Approved + detail.Pending).ToFriendlyTime()));
                                    continue;
                                }
                            }
                        }
                        // If there were any policy approval errors, return those as warnings here.
                        if (errors.Length > 0)
                            return result.SetFail().SetErrorMessage(errors.ToString());

                        // Check to ensure we actually have at least 1 certification on the case, 'cause otherwise they shouldn't be requesting time off
                        if (theCase.Certifications == null || !theCase.Certifications.Any())
                            return result.SetFail().SetErrorMessage("There are currently no certifications for intermittent time off on this case.");

                        // Get the applicable certifications for the date being requested which would be the active certifications to check against (may be more than 1)
                        var certifications = theCase.Certifications == null ? null : theCase.Certifications
                                .Where(c => testDate.DateInRange(c.StartDate, c.EndDate) && (!c.IntermittentType.HasValue || c.IntermittentType == tor.IntermittentType)).ToList();
                        if (certifications == null || !certifications.Any())
                            return result.SetFail().SetErrorMessage("The requested date, {0:MM/dd/yyyy}, is outside of any certifications on this case.Do you wish to proceed", testDate);

                        // ********************************************************************************
                        // Here's what we're doing here... Basically we get date range based on the TOR date passed in
                        //  that we're checking. Based on the frequency and type, we're creating a window that spans the
                        //  extremes of that total frequency before and after the sample TOR date (both directions).
                        //
                        // So, if we have a frequency of 3 weeks, we would end up with a total date range of 6 weeks where the TOR
                        //  date is somewhere in the middle of that 6 week timeframe (get the picture so far).
                        //
                        // Then, we have to increment our measurement window on a sliding basis between those 2 spans which could encompass
                        //  the sample TOR date. This means we swing starting at the earliest extremety, count out 3 weeks, then roll 1
                        //  week forward and test out another 3 weeks, etc.
                        //
                        // In that, we ignore any date ranges in our rolling period that actually do not include the TOR sample date, this
                        //  ensures we're not doing something stupid and over-measuring stuff which would be bad.
                        // ********************************************************************************
                        // Loop through all applicable certifications to check each one to see if we're blowing it
                        foreach (Certification cert in certifications)
                        {
                            try
                            {
                                // Get the total range, forward and backward for this certification based on the frequency.
                                var totalRange = GetCertificationRange(cert);
                                var nextRange = GetNextCertificationRange(cert, totalRange.Item1, false);

                                // Loop while our next range's start date is less than our total encompassing range's end date.
                                //  This check will ensure while we're still starting in the range before our final check end date
                                //  we continue on (otherwise we may accidentally skip some dates, which would be bad 'n' stuff).
                                while (nextRange.Item1 < totalRange.Item2)
                                {
                                    // Define our action which sets the start and end date based on the next range
                                    var next = new Action(() =>
                                    {
                                        nextRange = GetNextCertificationRange(cert, nextRange.Item1, true);
                                    });
                                    // Get the list of user requests for the entire case within the given date range based on frequency
                                    var requests = theCase.Segments
                                       .Where(s => s.Type == CaseType.Intermittent)
                                       .SelectMany(s => s.UserRequests)
                                       .Where(r => r.RequestDate.DateInRange(nextRange.Item1, nextRange.Item2) && hasTime(r)
                                                && (!r.IntermittentType.HasValue || r.IntermittentType == tor.IntermittentType))
                                       .ToList();
                                    // Remove any existing TOR for this date
                                    requests.RemoveAll(r => r.RequestDate == testDate);
                                    // Add our new TOR that we're checking
                                    requests.Add(tor);

                                    // Do we even have any w/ pending or approved time? Nope, ok great, 
                                    //  move to the next cert or date range, if any
                                    if (!requests.Any())
                                    {
                                        next();
                                        continue;
                                    }

                                    // Now, armed with the full list of user requests for the given certification period's frequency, let's test
                                    //  for duration and occurrences against this certification.
                                    // We track occurrences and consecutive time based on status, so combined = Pending + Approved, where the other
                                    //  is only approved time. This allows for proper messaging/criteria at a future point to be used for
                                    //  properly communicating the right message to the user.
                                    int occurrences = 0;
                                    int occurrencesCombined = 0;
                                    int consecutiveDays = 0;
                                    int consecutiveDaysCombined = 0;
                                    int consecutiveMinutes = 0;
                                    int consecutiveMinutesCombined = 0;

                                    // Define and measure out our duration window in days based on the certification's duration
                                    int durationWindowInDays = GetCertificationDurationInDays(cert, nextRange.Item1, nextRange.Item2);
                                    int durationWindowInMinutes = GetCertificationDurationInMinutes(cert, nextRange.Item1, nextRange.Item2);

                                    // Start on the start date and loop through day by day accumulating occurrences
                                    DateTime checkDate = nextRange.Item1;
                                    while (checkDate <= nextRange.Item2)
                                    {
                                        // Get any request for this date.

                                        IntermittentTimeRequest req = requests.FirstOrDefault(r => r.RequestDate == checkDate && (!r.IntermittentType.HasValue || r.IntermittentType == tor.IntermittentType));

                                        // Do we have a TOR for the check date?
                                        if (req == null || !hasTime(req) || consecutiveDaysCombined >= durationWindowInDays)
                                        {
                                            // Check to see if we're in consecutive mode, if so, then bump our occurrences
                                            if (consecutiveDays > 0 || consecutiveMinutes > 0)
                                                occurrences++;
                                            // Check to see if we're in consecutive mode for combined time, if so then bump out combined
                                            //  occurrences.
                                            if (consecutiveDaysCombined > 0 || consecutiveMinutesCombined > 0)
                                                occurrencesCombined++;

                                            // Obviously we're exiting any consecutive time off, so clear this value back to zero
                                            consecutiveDays = consecutiveDaysCombined = 0;
                                            consecutiveMinutes = consecutiveMinutesCombined = 0;

                                            // Loopity loop-ah, loop-de-loop, loopy loopy loop :-)
                                            if (req == null || !hasTime(req))
                                            {
                                                checkDate = checkDate.AddDays(1);
                                                continue;
                                            }
                                        }

                                        // If this TOR has any approved time, then increment the consecutive days counter
                                        if (req.Detail.Max(d => d.Approved) > 0)
                                            consecutiveDays++;

                                        // Always count combined consecutive days counter because if we've gotten this far, there is at least
                                        //  pending time.
                                        consecutiveDaysCombined++;

                                        // Increment the consecutive minutes by the max approved amount for all detail rows 
                                        //  (if none are approved this will add zero anyway)
                                        consecutiveMinutes += req.Detail.Max(d => d.Approved);

                                        // Increment the consecutive minutes by the max of all pending + approved combo minutes
                                        consecutiveMinutesCombined += req.Detail.Max(d => d.Pending + d.Approved);

                                        // If we've lapped up enough consecutive days to where we've overreached the threshold with our
                                        //  measured duration window, we're overutilized and we need to fail the certification.
                                        if (consecutiveDays > durationWindowInDays)
                                            return result.SetFail().SetFailedCertification(cert)
                                                .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} consecutive days.",
                                                testDate, durationWindowInDays);

                                        // Also check to see if we've overrun our consecutive minutes for the current occurrence, if so, drop da bomb.
                                        if (consecutiveMinutes > durationWindowInMinutes)
                                            return result.SetFail().SetFailedCertification(cert)
                                                .SetErrorMessage("The time off request exceeds the frequency and duration certification details. Do you wish to proceed?",
                                                testDate, cert.Duration, cert.DurationType);

                                        // If we've lapped up enough consecutive days to where we've overreached the threshold with our
                                        //  measured duration window, we're overutilized and we need to fail the certification.
                                        // Check the same, but for all combined time to message that if time was "pending" and then approved this would fail.
                                        if (consecutiveDaysCombined > durationWindowInDays)
                                            return result.SetFail().SetFailedCertification(cert)
                                                .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} consecutive days if all pending time {2} were to be approved.",
                                                testDate, durationWindowInDays, IfIWereApproved(nextRange));

                                        // Also check to see if we've overrun our consecutive minutes for the current occurrence, if so, drop da bomb.
                                        // Check the same, but for all combined time to message that if time was "pending" and then approved this would fail.
                                        if (consecutiveMinutesCombined > durationWindowInMinutes)
                                            return result.SetFail().SetFailedCertification(cert)
                                                .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} {2} if all pending time {3} were to be approved.",
                                                testDate, cert.Duration, cert.DurationType, IfIWereApproved(nextRange));

                                        // Go to the next day 'n' stuff, 'cause that's what you do when you're looping and want to progress
                                        checkDate = checkDate.AddDays(1);
                                    }

                                    // If we've lapped up enough consecutive days to where we've overreached the threshold with our
                                    //  measured duration window, we're overutilized and we need to fail the certification.
                                    if (consecutiveDays > durationWindowInDays)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} consecutive days.",
                                            testDate, durationWindowInDays);

                                    // Also check to see if we've overrun our consecutive minutes for the current occurrence, if so, drop da bomb.
                                    if (consecutiveMinutes > durationWindowInMinutes)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("The time off request exceeds the frequency and duration certification details. Do you wish to proceed?.",
                                            testDate, cert.Duration, cert.DurationType);

                                    // If we've lapped up enough consecutive days to where we've overreached the threshold with our
                                    //  measured duration window, we're overutilized and we need to fail the certification.
                                    // Check the same, but for all combined time to message that if time was "pending" and then approved this would fail.
                                    if (consecutiveDaysCombined > durationWindowInDays)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} consecutive days if all pending time {2} were to be approved.",
                                            testDate, durationWindowInDays, IfIWereApproved(nextRange));

                                    // Also check to see if we've overrun our consecutive minutes for the current occurrence, if so, drop da bomb.
                                    // Check the same, but for all combined time to message that if time was "pending" and then approved this would fail.
                                    if (consecutiveMinutesCombined > durationWindowInMinutes)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("This request for {0:MM/dd/yyyy} would cause an occurrence duration of greater than {1} {2} if all pending time {3} were to be approved.",
                                            testDate, cert.Duration, cert.DurationType, IfIWereApproved(nextRange));

                                    // Add an additional occurrence if we have any consecutive days in our counter (post-cleanup)
                                    if (consecutiveDays > 0 || consecutiveMinutes > 0)
                                        occurrences++;
                                    if (consecutiveDaysCombined > 0 || consecutiveMinutesCombined > 0)
                                        occurrencesCombined++;
                                    // Clear out our consecutive days/minutes counters
                                    consecutiveDays = consecutiveDaysCombined = 0;
                                    consecutiveMinutes = consecutiveMinutesCombined = 0;

                                    // If our occurrence count is greater than the allowed occurrences, then fail, 'cause that's bad
                                    if (occurrences > cert.Occurances)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("This request for {0:MM/dd/yyyy} would exceed the certified number of occurrences, {1}, by {2}.",
                                            testDate, cert.Occurances, occurrences - cert.Occurances);

                                    // If our combined occurrence count is greater than the allowed occurrences, then fail, 'cause that's bad
                                    // Check the same, only this time for combined pending + approved occurrences.
                                    if (occurrencesCombined > cert.Occurances)
                                        return result.SetFail().SetFailedCertification(cert)
                                            .SetErrorMessage("This request for {0:MM/dd/yyyy} would exceed the certified number of occurrences, {1}, by {2} if all pending time {3} were to be approved.",
                                            testDate, cert.Occurances, occurrences - cert.Occurances, IfIWereApproved(nextRange));

                                    // Set our next window and get goin'
                                    next();
                                }
                            }
                            catch (AbsenceSoftException absEx)
                            {
                                // We need to fail this certification, this inner catch is simply to be able to
                                //  set the certification which failed w/ an exception in this instance rather
                                //  than a non-contextual exception which is below.
                                return result.SetFail().SetFailedCertification(cert).SetErrorMessage(absEx.Message);
                            }
                        }
                    }
                    catch (AbsenceSoftException absEx)
                    {
                        // Some overarching validation or error happened, which sucks, so we need to
                        //  set the error message w/o a cert and return the result as failed.
                        return result.SetFail().SetErrorMessage(absEx.Message);
                    }
                    catch (Exception ex)
                    {
                        // Yikes, something really bad happened. We need to return success, but let's log this error 'cause
                        //  it maybe bad and we may want to do some troubleshooting here if the user comes back and says:
                        //  "Hey guys, why didn't this give me a failure or error or something?"
                        Log.Error("Error performing certification check", ex);
                    }

                    // Return the final result... Which, in case you were wondering, is more than likely
                    //  a Success/Pass result since all the other failures we're exiting early and returning
                    //  the failed result using our super-sexy fluid inline methods.
                    return result;
                }
            }

            /// <summary>
            /// Gets the certification range around the current TOR's requested date forward and backward
            /// so a rolling measure can be taken during the certification period, up to the certification dates.
            /// </summary>
            /// <param name="cert">The cert.</param>
            /// <returns></returns>
            /// <exception cref="AbsenceSoftException"></exception>
            private Tuple<DateTime, DateTime> GetCertificationRange(Certification cert)
            {
                #region GetCertificationRange

                // Define the start and end date to pull all current TORs for
                DateTime startDate = tor.RequestDate;
                DateTime endDate = tor.RequestDate;

                switch (cert.FrequencyType)
                {
                    case Unit.Days:
                        // calendar days are the easiest, just do the math and remember the date counts are inclusive so add / remove one for the current day
                        if (cert.FrequencyUnitType == DayUnitType.CalendarDay)
                        {
                            startDate = tor.RequestDate.AddDays((-cert.Frequency) + 1);
                            endDate = tor.RequestDate.AddDays(cert.Frequency - 1);
                        }
                        else
                        {
                            if (workSchedule == null)
                            {
                                // Get the employee and build an LOA instance so we can get work schedule crap, etc.
                                Employee emp = Employee.GetById(theCase.Employee.Id);
                                LeaveOfAbsence loa = new LeaveOfAbsence()
                                {
                                    Case = theCase,
                                    Employee = emp,
                                    Employer = theCase.Employer,
                                    Customer = theCase.Customer,
                                    WorkSchedule = emp.WorkSchedules
                                };

                                // Materialize the work schedule based on the case's start and end date/test date 30+ days (when necessary) including holidays
                                //  for the employee so that we can accurately determine work days/periods within boxed calendar periods.
                                workSchedule = loa.MaterializeSchedule(theCase.StartDate, theCase.EndDate ?? tor.RequestDate.AddDays(30), true, emp.WorkSchedules);
                            }
                            // work days aren't so bad either, just look back and forward that many elements in 
                            // the array and see what is there
                            int pos = workSchedule.FindIndex(fi => fi.SampleDate == tor.RequestDate);
                            if (pos == -1)
                                throw new AbsenceSoftException(string.Format("Requested date of {0:MM/dd/yyyy} is not a work day", tor.RequestDate));

                            // Get the ordinal date positions within our work schedule dates to determine where to grab each work day
                            //  from the collection to build our final range.
                            int startPos = Math.Max(pos - cert.Frequency, 0);
                            int endPos = Math.Min(pos + cert.Frequency, workSchedule.Count - 1);

                            // Get the actual start and end date based on our work schedule positions.
                            startDate = workSchedule[startPos].SampleDate;
                            endDate = workSchedule[endPos].SampleDate;
                        }
                        break;
                    case Unit.Weeks:
                        // Set the start and end date based on the total number of weeks in the frequency
                        startDate = tor.RequestDate.GetFirstDayOfWeek();
                        endDate = tor.RequestDate.GetLastDayOfWeek();
                        for (int i = 1; i < cert.Frequency; i++)
                        {
                            startDate = startDate.AddDays(-1).GetFirstDayOfWeek();
                            endDate = endDate.AddDays(1).GetLastDayOfWeek();
                        }
                        break;
                    case Unit.Months:
                        // Set the start and end date based on the total number of months in the frequency
                        //  Since each month can be different, we just crawl the months based on the sample date
                        //  for our measure period.
                        startDate = tor.RequestDate.GetFirstDayOfMonth();
                        endDate = tor.RequestDate.GetLastDayOfMonth();
                        // Set our sliding window parts to the first set of months up to encompassing
                        for (int i = 1; i < cert.Frequency; i++)
                        {
                            startDate = startDate.AddDays(-1).GetFirstDayOfMonth();
                            endDate = endDate.AddDays(1).GetLastDayOfMonth();
                        }
                        break;
                    case Unit.Years:
                        // Set the start and end date based on the total number of years in the frequency
                        //  Since each year can be different given leap years 'n' stuff, we just crawl the
                        //  years based on the sample date's current year for our measure period.
                        startDate = tor.RequestDate.GetFirstDayOfYear();
                        endDate = tor.RequestDate.GetLastDayOfYear();
                        for (int i = 1; i < cert.Frequency; i++)
                        {
                            startDate = startDate.AddDays(-1).GetFirstDayOfYear();
                            endDate = endDate.AddDays(1).GetLastDayOfYear();
                        }
                        break;
                }

                // Ensure we're still within the certification boundaries and not starting before the 
                //  certification is applicable.
                startDate = Date.Max(startDate, cert.StartDate);

                return new Tuple<DateTime, DateTime>(startDate, endDate);

                #endregion
            }

            /// <summary>
            /// Gets the next certification date range for measuring the next set of occurrences within the
            /// certified frequency.
            /// </summary>
            /// <param name="cert">The certification that is being measured.</param>
            /// <param name="lastStartDate">The last start date when this was previously calculated or <c>null</c>.
            /// If <c>null</c> then will start from the original start date without forward rolling.</param>
            /// <returns>
            /// Either a Tuple containing the new date pair, OR <c>null</c> if we've left the boundaries of the certification.
            /// </returns>
            /// <remarks>
            /// This method helps break up the sliding measure window between the 2 extremes of the frequency
            /// boundaries (e.g. 2 occurrences every 3 years, we need to be able to measure 3 years back and 3 years forward
            /// and then for each logical boundary sliding between those 2 dates, measure total occurrences.
            /// </remarks>
            private Tuple<DateTime, DateTime> GetNextCertificationRange(Certification cert, DateTime lastStartDate, bool rollForward)
            {
                #region GetNextCertificationRange

                // Define the start and end date to pull all current TORs for
                DateTime startDate = lastStartDate;
                DateTime endDate = startDate;

                // If the new start date is out of range of the certification end date, then return null
                if (startDate > cert.EndDate)
                    return null;

                switch (cert.FrequencyType)
                {
                    case Unit.Minutes:
                    case Unit.Hours:
                        // Start with the prior start date + 1 day, sliding same day increments for the full frequency
                        startDate = rollForward ? startDate.AddDays(1) : startDate;
                        endDate = startDate;
                        break;
                    case Unit.Days:
                        // calendar days are the easiest, just do the math and remember the date counts are inclusive so add / remove one for the current day
                        if (cert.FrequencyUnitType == DayUnitType.CalendarDay)
                        {
                            // Start with the prior start date + 1 day, sliding 1 day increments for the full frequency
                            startDate = rollForward ? startDate.AddDays(1) : startDate;
                            endDate = startDate.AddDays(cert.Frequency - 1);
                        }
                        else
                        {
                            if (workSchedule == null)
                            {
                                // Get the employee and build an LOA instance so we can get work schedule crap, etc.
                                Employee emp = Employee.GetById(theCase.Employee.Id);
                                LeaveOfAbsence loa = new LeaveOfAbsence()
                                {
                                    Case = theCase,
                                    Employee = emp,
                                    Employer = theCase.Employer,
                                    Customer = theCase.Customer,
                                    WorkSchedule = emp.WorkSchedules
                                };

                                // Materialize the work schedule based on the case's start and end date/test date 30+ days (when necessary) including holidays
                                //  for the employee so that we can accurately determine work days/periods within boxed calendar periods.
                                workSchedule = loa.MaterializeSchedule(theCase.StartDate, theCase.EndDate ?? tor.RequestDate.AddDays(30), true, emp.WorkSchedules);
                            }
                            // work days aren't so bad either, just look back and forward that many elements in 
                            // the array and see what is there
                            int pos = workSchedule.FindIndex(fi => fi.SampleDate == startDate);
                            if (pos == -1)
                                return null;

                            // We're going to add 1 day to our position because we're sliding by 1 day for each rolling window
                            if (rollForward)
                                pos += 1;

                            // Get the ordinal date positions within our work schedule dates to determine where to grab each work day
                            //  from the collection to build our final range.
                            int startPos = Math.Max(pos, 0);
                            int endPos = Math.Min(pos + cert.Frequency, workSchedule.Count - 1);

                            // Get the actual start and end date based on our work schedule positions.
                            startDate = workSchedule[startPos].SampleDate;
                            endDate = workSchedule[endPos].SampleDate;
                        }
                        break;
                    case Unit.Weeks:
                        // Start with our prior start + 1 week, sliding 1 week increments for the full frequency
                        startDate = rollForward ? startDate.GetLastDayOfWeek().AddDays(1) : startDate.GetFirstDayOfWeek();
                        endDate = startDate.GetLastDayOfWeek();
                        for (int i = 1; i < cert.Frequency; i++)
                            endDate = endDate.AddDays(1).GetLastDayOfWeek();
                        break;
                    case Unit.Months:
                        // Start with our prior start date + 1 month, sliding 1 month increments for the full frequency
                        startDate = rollForward ? startDate.GetLastDayOfMonth().AddDays(1) : startDate.GetFirstDayOfMonth();
                        endDate = startDate.GetLastDayOfMonth();
                        // Set our sliding window parts to the first set of months up to encompassing
                        for (int i = 1; i < cert.Frequency; i++)
                            endDate = endDate.AddDays(1).GetLastDayOfMonth();
                        break;
                    case Unit.Years:
                        // Start with our prior start date + 1 year, sliding 1 year increments for the full frequency
                        startDate = rollForward ? startDate.GetLastDayOfYear().AddDays(1) : startDate.GetFirstDayOfYear();
                        endDate = startDate.GetLastDayOfYear();
                        for (int i = 1; i < cert.Frequency; i++)
                            endDate = endDate.AddDays(1).GetLastDayOfYear();
                        break;
                }

                // Ensure we're still within the certification boundaries for the end date
                endDate = Date.Min(endDate, cert.EndDate);

                return new Tuple<DateTime, DateTime>(startDate, endDate);

                #endregion
            }

            /// <summary>
            /// Gets the certified duration in days of what the max of each occurrence should be in general days. For all types
            /// that are less than 1 day, will return 1.
            /// </summary>
            /// <param name="cert">The certification that is being measured.</param>
            /// <param name="startDate">The start date from which to start measurement of the duration.</param>
            /// <param name="endDate">The end date to which the measurement of the duration should not exceed.</param>
            /// <returns>The total number of days, rounded up to the nearest whole day.</returns>
            private int GetCertificationDurationInDays(Certification cert, DateTime startDate, DateTime endDate)
            {
                #region GetCertificationDurationInDays

                int durationWindowInDays = 1;
                int dur = Convert.ToInt32(Math.Ceiling(cert.Duration));
                switch (cert.DurationType)
                {
                    case Unit.Days:
                        durationWindowInDays = dur;
                        break;
                    case Unit.Weeks:
                        durationWindowInDays = dur * 7;
                        break;
                    case Unit.Months:
                        durationWindowInDays = DateTime.DaysInMonth(startDate.Year, startDate.Month);
                        DateTime nextMonthDate = startDate.GetLastDayOfMonth().AddDays(1);
                        for (var i = 1; i < dur && nextMonthDate < endDate.GetLastDayOfMonth(); i++)
                        {
                            durationWindowInDays += DateTime.DaysInMonth(nextMonthDate.Year, nextMonthDate.Month);
                            nextMonthDate = nextMonthDate.GetLastDayOfMonth().AddDays(1);
                        }
                        break;
                    case Unit.Years:
                        durationWindowInDays = startDate.DaysInYear();
                        DateTime nextYearDate = startDate.GetLastDayOfYear().AddDays(1);
                        for (var i = 1; i < dur && nextYearDate < endDate.GetLastDayOfYear(); i++)
                        {
                            durationWindowInDays += nextYearDate.DaysInYear();
                            nextYearDate = nextYearDate.GetLastDayOfYear().AddDays(1);
                        }
                        break;
                }
                return durationWindowInDays;

                #endregion
            }

            /// <summary>
            /// Gets the certified duration in minutes of what the max of each occurrence should be in general/total minutes. For all
            /// types that are greater than 1 day, will return 1,140 * (# of days).
            /// </summary>
            /// <param name="cert">The certification that is being measured.</param>
            /// <param name="startDate">The start date from which to start measurement of the duration.</param>
            /// <param name="endDate">The end date to which the measurement of the duration should not exceed.</param>
            /// <returns>The total number of minutes, rounded up to the nearest whole minute.</returns>
            private int GetCertificationDurationInMinutes(Certification cert, DateTime startDate, DateTime endDate)
            {
                #region GetCertificationDurationInMinutes

                int durationWindowInMinutes = 0;
                int dur = Convert.ToInt32(Math.Ceiling(cert.Duration));
                switch (cert.DurationType)
                {
                    case Unit.Minutes:
                        return dur;
                    case Unit.Hours:
                        return Convert.ToInt32(Math.Ceiling(cert.Duration * 60));
                    case Unit.Days:
                    case Unit.Months:
                    case Unit.Years:
                        return GetCertificationDurationInDays(cert, startDate, endDate) * 24 * 60;
                }
                return durationWindowInMinutes;

                #endregion
            }

            /// <summary>
            /// I could while away the hours
            /// Conferrin' with the flowers
            /// Consultin' with the rain...
            /// And my head, I'd be scratchin'
            /// While my thoughts were busy hatchin'
            /// If I only were approved.
            /// </summary>
            /// <param name="nextRange">The next range to format.</param>
            /// <returns>A string representing the if pending time were approved string for easy of re-use and formatting.</returns>
            private string IfIWereApproved(Tuple<DateTime, DateTime> nextRange)
            {
                #region IfIWereApproved

                return string.Format("{3} {0:MM/dd/yyyy}{2}{1:MM/dd/yyyy}",
                    nextRange.Item1,
                    nextRange.Item2,
                    nextRange.Item1 == nextRange.Item2 ? "" : " and ",
                    nextRange.Item1 == nextRange.Item2 ? "on" : "between");

                #endregion
            }
        }
    }
}
