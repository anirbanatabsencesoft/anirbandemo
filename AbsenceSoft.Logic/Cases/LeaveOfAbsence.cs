﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// Represents an entire leave of absence snapshot for use in creating communications, emails,
    /// populating fields via the tokenization service, performing rules evaluation, etc.
    /// </summary>
    [Serializable]
    public class LeaveOfAbsence : BaseNonEntity
    {
        /// <summary>
        /// Constant representing the value used for all expression trees that access the LeaveOfAbsence object
        /// when evaluating rules, tokenizing communications, etc.
        /// </summary>
        public const string EXPRESSION_INSTANCE_NAME = "LoA";

        #region instance variables

        /// <summary>
        /// Initializes a new instance of the <see cref="AbsenceSoft.Logic.Cases.LeaveOfAbsence"/> class.
        /// </summary>
        public LeaveOfAbsence()
        {
            CaseHistory = new List<Case>();
            Communications = new List<Communication>();
            Tasks = new List<ToDoItem>();
            WorkSchedule = new List<Schedule>();
        }//ctor

        private string EmployeeId
        {
            get
            {
                if (Employee != null)
                    return Employee.Id;

                if (Case != null && Case.Employee != null)
                    return Case.Employee.Id;

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the case for this leave of absence.
        /// </summary>
        public Case Case { get; set; }

        /// <summary>
        /// Gets or sets the active segment being used for tokenization, evaluation, etc.
        /// This is always the last segment created (for now).
        /// </summary>
        public CaseSegment ActiveSegment { get; set; }

        /// <summary>
        /// Gets or sets the employee for this leave of absence.
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the employer for this leave of absence.
        /// </summary>
        public Employer Employer { get; set; }

        /// <summary>
        /// Gets or sets the customer for this leave of absence.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the case history for this employee.
        /// </summary>
        public List<Case> CaseHistory { get; set; }

        /// <summary>
        /// Gets or sets all of the communications for this leave of absence.
        /// </summary>
        public List<Communication> Communications { get; set; }

        /// <summary>
        /// Gets or sets all of the tasks for this leave of absence.
        /// </summary>
        public List<ToDoItem> Tasks { get; set; }

        /// <summary>
        /// Gets or sets the work schedule.
        /// </summary>
        /// <value>
        /// The work schedule.
        /// </value>
        public List<Schedule> WorkSchedule { get; set; }

        /// <summary>
        /// Gets or Sets relapse details
        /// </summary>
        public Relapse Relapse { get; set; }

        /// <summary>
        /// Indicate if case has relapse
        /// </summary>
        public bool IsRelapse
        {
            get
            {
                return Relapse != null || Case.HasRelapse();
            }
        }

        /// <summary>
        /// Gets the employee's related contact relationship type for the case.
        /// </summary>
        public string CaseRelationshipType
        {
            get
            {
                return this.Case == null ? null : this.Case.Contact == null
                    ? "SELF"
                    : this.Case.Contact.ContactTypeCode;
            }
        }

        /// <summary>
        /// Set to the ToDoItem that is currently included in the rules
        /// engine evaluation (for example: set during Todos complete a step to the step that is
        /// being completed)
        /// </summary>
        public ToDoItem ActiveWorkFlowItem { get; set; }

        /// <summary>
        /// Get Case Level Metadata value for WillUseBonding
        /// </summary>
        public bool WillUseBonding
        {
            get
            {

                return (IsMetadataValueTrue("WillUseBonding") && ( Case.FindCaseEvent(CaseEventType.BondingStartDate) != null ));
            }
        }
        #endregion instance variables


        /// <summary>
        /// Gets the total prior hours worked by the employee for this leave of absence based on prior hours worked +
        /// any hours worked from the initial leave of absence start date for the active segment.
        /// This is intended to solve the 1250 hours problem for determining FMLA eligibility.
        /// </summary>
        /// <returns>A total of all compounded hours worked up to the requested start date of the leave of absence.</returns>
        public double TotalHoursWorkedLast12Months()
        {
            using (new InstrumentationContext("LeaveOfAbsence.TotalHoursWorkedLast12Months"))
            {
                bool validToCalc = ValidToCalc();
                // shortcut variables  
                DateTime endDate = Case == null ? DateTime.UtcNow.Date.AddDays(-1) : Case.StartDate.AddDays(-1);
                DateTime lastYear = Date.Max(
                    // We can't rely on Materialize Schedule to truncate/trim our usage list, it will project out days of usage both directions endlessly.
                    Employee?.ServiceDate ?? Case?.Employee?.ServiceDate ?? Employee?.HireDate ?? Case?.Employee?.HireDate,
                    endDate.SameDayLastYear()
                ) ?? endDate.SameDayLastYear();

                // Determine if we need to calculate the materialized schedule to a later time because we have prior hours.
                double prior = 0d; // Measures the prior hours worked
                double offset = 0d; // Measures an offset to prior hours worked based on the difference in dates from the case start date/today from the As Of date
                if (Employee.PriorHours != null && Employee.PriorHours.Any())
                {
                    var p = Employee.PriorHours.OrderBy(h => h.AsOf).LastOrDefault();
                    if (p != null)
                    {
                        // Employer-UI setting to skip Prior Hours internal calcs and accept imported/input amount instead
                        if ((Employer != null && Employer.IgnoreScheduleForPriorHoursWorked) ||
                             (Employee != null && Employee.Employer != null && Employee.Employer.IgnoreScheduleForPriorHoursWorked) ||
                             (Case != null && Case.Employer != null && Case.Employer.IgnoreScheduleForPriorHoursWorked))
                            return p.HoursWorked;

                        // If we already have hours worked stored for the given As Of date, or AFTER our As Of date, just return that instead.
                        if (p.AsOf >= endDate)
                            return p.HoursWorked;

                        // Set the prior hours worked to the value stored in the DB for the given range, last set.
                        prior = p.HoursWorked;

                        // We need to get the number of sliding days we're offset from the case start date/today's date vs. the As Of date
                        //   That will allow us to figure out how much scheduled time to lob off the final measure of hours worked.
                        double offsetDays = Math.Ceiling((endDate - p.AsOf).TotalDays);
                        // Get the total materialized schedule for the start date of last year for the offset number of days
                        int offsetMinutes = 0;
                        if (validToCalc)
                            offsetMinutes = MaterializeSchedule(lastYear, lastYear.AddDays(offsetDays), true).Sum(t => t.TotalMinutes ?? 0);
                        // Convert our offset to hours and set them to the offset value
                        offset = offsetMinutes / 60d;

                        // Decrement last year's date to exactly 1 day AFTER the As Of date, so that we get the scheduled hours
                        //  worked SINCE the As Of date from the employee's work schedule
                        lastYear = p.AsOf.AddDays(1).ToMidnight();
                    }
                }

                // Calculate the total minutes worked from the given start date last year (or from As Of date) to the target calc end date
                var totalMinutes = 0;
                if (validToCalc)
                {
                    totalMinutes = MaterializeSchedule(lastYear, endDate, true).Sum(t => t.TotalMinutes ?? 0);
                }
                // Convert those minutes into hours
                var newHours = totalMinutes / 60d;

                // Return the sum of newly schedule hours and prior hours worked, then subtract the offset to get the actual hours worked in
                //  last 12 months.
                return newHours + prior - offset;
            }
        }


        public int TotalMinutesWorkedPerWeek()
        {
            using (new InstrumentationContext("LeaveOfAbsence.TotalMinutesWorkedPerWeek"))
            {
                if (Employee.MinutesWorkedPerWeek != null && Employee.MinutesWorkedPerWeek.Any())
                {
                    var p = Employee.MinutesWorkedPerWeek.LastOrDefault();
                    if (p != null)
                    {
                        // Employer-UI setting to skip Prior Hours internal calcs and accept imported/input amount instead
                        if (IgnoreAverageMinutesWorkedPerWeekCalculation())
                            return p.MinutesWorked;
                        else
                            return Employee.MinutesWorkedPerWeek.Sum(x => x.MinutesWorked);
                    }
                }
                return 0;
            }
        }

        /// <summary>
        /// Gets the total prior hours worked by the employee as of today based on prior hours worked.
        /// This is intended to solve the 1250 hours problem for determining FMLA eligibility.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        public static double TotalHoursWorkedLast12Months(Employee employee)
        {
            if (employee == null) return 0d;
            LeaveOfAbsence loa = new LeaveOfAbsence() { Employee = employee, Employer = employee.Employer, Customer = employee.Customer, WorkSchedule = employee.WorkSchedules };
            return loa.TotalHoursWorkedLast12Months();
        }

        public static int TotalMinutesWorkedPerWeek(Employee employee)
        {
            if (employee == null) return 0;
            LeaveOfAbsence loa = new LeaveOfAbsence() { Employee = employee, Employer = employee.Employer, Customer = employee.Customer, WorkSchedule = employee.WorkSchedules };
            return loa.TotalMinutesWorkedPerWeek();
        }

        /// <summary>
        /// return the hours scheduled over a one week period (don't consider
        /// holidays)
        /// </summary>
        /// <returns></returns>
        public double HoursScheduledPerWeek()
        {
            using (new InstrumentationContext("LeaveOfAbsence.HoursScheduledPerWeek"))
            {
                // all the error checks, don't try to do anything if we can't get past these
                if (!ValidToCalc())
                    return 0;


                // shortcut variables
                DateTime startDate = this.Case == null ? DateTime.UtcNow.Date.GetFirstDayOfWeek() : this.Case.StartDate.GetFirstDayOfWeek();
                DateTime endDate = startDate.AddDays(7);

                var totalMinutes = MaterializeSchedule(startDate, endDate, false).Sum(t => t.TotalMinutes ?? 0);

                return (double)totalMinutes / 60d;
            } // using instrumentation
        }

        /// <summary>
        /// Gets the hours worked by the employee each week for determining requirements of eligibility for certain leave policies.
        /// </summary>
        /// <returns>A total of all compounded hours worked the week before the start date of the absence.</returns>
        public double HoursWorkedPerWeek()
        {
            using (new InstrumentationContext("LeaveOfAbsence.HoursWorkedPerWeek"))
            {
                // all the error checks, don't try to do anything if we can't get past these
                if (!ValidToCalc())
                    return 0;


                // shortcut variables
                DateTime startDate = this.Case == null ? DateTime.UtcNow.Date.GetFirstDayOfWeek() : this.Case.StartDate.GetFirstDayOfWeek();
                DateTime endDate = startDate.AddDays(7);

                var totalMinutes = MaterializeSchedule(startDate, endDate, true).Sum(t => t.TotalMinutes ?? 0);

                return (double)totalMinutes / 60d;
            } // using instrumentation
        }

        /// <summary>
        /// Returns the calculated value from other sources
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public static double HoursWorkedPerWeek(Case oCase)
        {
            if (oCase == null) return 0d;
            LeaveOfAbsence loa = new LeaveOfAbsence() { Employee = oCase.Employee, Employer = oCase.Employee.Employer, Customer = oCase.Employee.Customer, WorkSchedule = oCase.Employee.WorkSchedules, Case = oCase };
            return loa.HoursWorkedPerWeek();
        }

        /// <summary>
        /// Returns the calculated value for other objects
        /// </summary>
        /// <param name="oCase"></param>
        /// <returns></returns>
        public static double HoursScheduledPerWeek(Case oCase)
        {
            if (oCase == null) return 0d;
            LeaveOfAbsence loa = new LeaveOfAbsence() { Employee = oCase.Employee, Employer = oCase.Employee.Employer, Customer = oCase.Employee.Customer, WorkSchedule = oCase.Employee.WorkSchedules, Case = oCase };
            return loa.HoursScheduledPerWeek();
        }
        public int TotalWorkDaysInWeek(DateTime weekStart, DayOfWeek firstDayOfWeek, PolicyBehavior policyBehavior = PolicyBehavior.Ignored)
        {
            int totalWorkDays = 0;
            weekStart = weekStart.AddDays(DayOfWeek.Sunday - weekStart.DayOfWeek);
            List<Time> normalSchedule = MaterializeSchedule(weekStart, weekStart.AddDays(7), schedules: this.WorkSchedule, policyBehavior: policyBehavior); 
            DateTime weekEnd = weekStart.AddDays(7);
            totalWorkDays = normalSchedule.Count(s => s.SampleDate >= weekStart && s.SampleDate < weekEnd && s.TotalMinutes > 0);
            return totalWorkDays;
        }

        public int? TimeScheduledPerDay(DateTime workDay, DayOfWeek firstDayOfWeek, PolicyBehavior policyBehavior = PolicyBehavior.Ignored)
        {
            int? totalWorkDays = 0;
            workDay = workDay.AddDays(DayOfWeek.Sunday - workDay.DayOfWeek);
            List<Time> normalSchedule = MaterializeSchedule(workDay, workDay.AddDays(7), schedules: this.WorkSchedule, policyBehavior: policyBehavior);
            totalWorkDays = normalSchedule.FirstOrDefault(s => s.TotalMinutes > 0)?.TotalMinutes;
            return totalWorkDays;
        }

        /// <summary>
        /// Gets the holidays.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="includeHolidays">if set to <c>true</c> [include holidays].</param>
        /// <param name="policyBehavior">The policy behavior.</param>
        /// <returns></returns>
        private List<DateTime> GetHolidays(DateTime startDate, DateTime endDate, bool includeHolidays = true, PolicyBehavior? policyBehavior = null)
        {
            return includeHolidays || (policyBehavior ?? PolicyBehavior.ReducesWorkWeek) != PolicyBehavior.Ignored
                    ? GetHolidayList(startDate.AllYearsInRange(endDate))
                    : new List<DateTime>(0);
        }

        /// <summary>
        /// Gets the variable schedule times.
        /// </summary>
        /// <param name="workSchedule">The work schedule.</param>
        /// <returns></returns>
        private List<VariableScheduleTime> GetVariableScheduleTimes(DateTime startDate, DateTime endDate, List<Schedule> workSchedule, string caseId = null)
        {
            if (!workSchedule.Any(s => s.ScheduleType == ScheduleType.Variable))
            {
                return new List<VariableScheduleTime>(0);
            }

            List<IMongoQuery> ands = new List<IMongoQuery>(5)
            {
                VariableScheduleTime.Query.EQ(s => s.CustomerId, Customer?.Id ?? Employee?.CustomerId ?? Case?.CustomerId ?? User.Current?.CustomerId),
                VariableScheduleTime.Query.EQ(s => s.EmployeeId, EmployeeId ?? Employee?.Id),
                VariableScheduleTime.Query.GTE(s => s.Time.SampleDate, startDate),
                VariableScheduleTime.Query.LTE(s => s.Time.SampleDate, endDate),
                VariableScheduleTime.Query.NE(s => s.IsDeleted, true)
            };

            if (!string.IsNullOrWhiteSpace(caseId))
            {
                ands.Add(VariableScheduleTime.Query.EQ(s => s.EmployeeCaseId, caseId));
            }
            else
            {
                ands.Add(Query.Or(
                    VariableScheduleTime.Query.NotExists(s => s.EmployeeCaseId),
                    VariableScheduleTime.Query.EQ(s => s.EmployeeCaseId, null)
                ));
            }

            var returnValue = VariableScheduleTime.Query.Find(Query.And(ands)).ToList();
            return returnValue;
        }

        /// <summary>
        /// Use an initialized LOA (this) ojbect and generate a schedule that is between 
        /// the passed start and end dates, inclusive of both dates
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="includeHolidays">if set to <c>true</c> include holidays explicitly, otherwise ignore/don't include holidays.</param>
        /// <param name="schedules">The schedules.</param>
        /// <param name="policyBehavior">The policy behavior, if set.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="totalWorkDays">this param is required for Segment FTE leave schedule, it should be = Emp work days </param>
        /// <returns></returns>
        private IEnumerable<Time> MaterializeScheduleByDay(DateTime startDate, DateTime endDate, bool includeHolidays = true, List<Schedule> schedules = null, PolicyBehavior? policyBehavior = null, string caseId = null, int? totalWorkDays = null)
        {
            using (new InstrumentationContext("LeaveOfAbsence.MaterializeScheduleByDay"))
            {
                var workSchedule = (schedules ?? (WorkSchedule == null || !WorkSchedule.Any() ? Employee.GetById(Employee.Id)?.WorkSchedules : WorkSchedule));
                workSchedule = workSchedule?.OrderByDescending(x => x.StartDate)?.ToList() ?? new List<Schedule>(0);

                // make sure we are in a runable state
                if (!workSchedule.Any() || !ValidToCalc(workSchedule))
                {
                    yield break;
                }

                // Clone our schedules so we can calc the calc without modifying the underlying entity
                workSchedule = workSchedule.Clone().OrderBy(s => s.StartDate).ToList();
                // Because they're ordered by Start Date in ascending order, we can take [0] and simply set the start date to
                //  our test range start date for materialization
                workSchedule[0].StartDate = Date.Min(startDate, workSchedule[0].StartDate);
                // Also because they're ordered by Start Date in ascending order, we can just set the last schedule's end date
                //  to null to make it perpetual rather than take any other measures, that ensures it encompasses the entire range
                workSchedule.Last().EndDate = null;

                // Get any holidays we need to deal with, if applicable
                List<DateTime> holidays = GetHolidays(startDate, endDate, includeHolidays, policyBehavior);

                // Get any variable scheduled time, if applicable
                List<VariableScheduleTime> variableScheduleTimes = GetVariableScheduleTimes(startDate, endDate, workSchedule, caseId);

                //process FTE schedule(s), to distribute the weekly time among appropriate days in week
                if(workSchedule.Any(s => s.ScheduleType == ScheduleType.FteVariable))
                {
                    ProcessAllFteSchedules(workSchedule, startDate, endDate, holidays, policyBehavior, totalWorkDays);
                    workSchedule = workSchedule.OrderBy(s => s.StartDate).ToList();
                }

                foreach (var date in startDate.AllDatesInRange(endDate))
                {
                    Time time = BuildScheduleTime(policyBehavior, workSchedule, holidays, variableScheduleTimes, date );
                    yield return time;
                }
            }
        }

        #region FTE Schedule

        /// <summary>
        /// process FTE schedule(s), to distribute the weekly time among appropriate days in week
        /// </summary>
        /// <param name="workSchedule"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="holidays"></param>
        /// <param name="policyBehavior"></param>
        /// <param name="totalWorkDays">this param is required for Segment FTE leave schedule, it should be = Emp work days </param>
        public void ProcessAllFteSchedules(List<Schedule> workSchedule, DateTime startDate, DateTime endDate, List<DateTime> holidays, PolicyBehavior? policyBehavior = null, int? totalWorkDays = null)
        {
            var allFteSchedule = workSchedule.Where(s => s.ScheduleType == ScheduleType.FteVariable && (s.EndDate is null || startDate <= s.EndDate.Value) && endDate >= s.StartDate).ToList();
            DayOfWeek firstDayOfWork = Employee.StartDayOfWeek ?? DayOfWeek.Monday;

            foreach (var fteSchedule in allFteSchedule)
            {
                var fteScheduleStartDate = fteSchedule.StartDate.Clone();

                //move start date ahead, so that it matches Start day of work for the employee
                fteScheduleStartDate = fteScheduleStartDate.AddDays(firstDayOfWork >= fteScheduleStartDate.DayOfWeek ? firstDayOfWork - fteScheduleStartDate.DayOfWeek : 7 + firstDayOfWork - fteScheduleStartDate.DayOfWeek);                

                //create a Time list that will be populated with appropriate number of hours, based on FTE weekly time
                fteSchedule.Times = ProcessFteScheduleTimesForWeek(fteScheduleStartDate, fteSchedule.FteMinutesPerWeek, firstDayOfWork, totalWorkDays);
            }            
        }

        /// <summary>
        /// If Employee has FTE schedue, it is not certain that how many hours in a day can be leave hours.
        /// For Intermittent case, number of requested leave hours in a day will be limited by the weekly work duration.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public double TimeLimitForTheDay(DateTime date)
        {
            double totalMinutes = 0;
            if((WorkSchedule == null || WorkSchedule.Count == 0) && Employee != null)
            {
                WorkSchedule = Employee.GetById(Employee.Id).WorkSchedules;
            }
            var allFteSchedule = WorkSchedule.Where(s => s.ScheduleType == ScheduleType.FteVariable && (s.EndDate is null || date <= s.EndDate.Value) && date >= s.StartDate);
            if (allFteSchedule != null && allFteSchedule.Any())
            {
                totalMinutes = allFteSchedule.FirstOrDefault(s => (s.EndDate is null || date <= s.EndDate.Value) && date >= s.StartDate).FteMinutesPerWeek;
            }
            return totalMinutes;
        }      

        /// <summary>
        /// Distribution of total week's time among all calculated working days of week
        /// </summary>
        /// <param name="fteStartDate"></param>
        /// <param name="minutesPerWeek"></param>
        /// <param name="firstDay"></param>
        /// <param name="workDaysInWeek"></param>
        /// <param name="scheduleTimes"></param>
        /// <returns></returns>
        private List<Time> ProcessFteScheduleTimesForWeek(DateTime fteStartDate, int minutesPerWeek, DayOfWeek firstDay, int? totalWorkDays, List<Time> scheduleTimes = null)
        {
            int workDaysInWeek = totalWorkDays ?? 7;
            List<Time> fteTimes = CreateTimeList(fteStartDate, scheduleTimes);

            fteTimes.Where(t => t.SampleDate.DateInRange(fteStartDate, fteStartDate.AddDays(workDaysInWeek - 1))).ForEach(d => d.TotalMinutes = 0);

            var timeInWeek = minutesPerWeek;
            //'totalWorkDays' has a value while reduced case. Number of work days is already decided during Employee schedule creation,
            //so the Time should be equally divided among all specifies work days, So the Time unit used will be Minutes. 
            //If 'totalWorkDays' is null, then it should be Employee schedule, and the time unit should be Hours to decide number of work days
            if (totalWorkDays == null)
            {
                timeInWeek = minutesPerWeek / 60;
            }
            int totalWeekDays = workDaysInWeek;

            //find number of working days in week.             
            workDaysInWeek = Enumerable.Range( 1, workDaysInWeek ).Last( x => timeInWeek % x == 0 && ( x + (int)firstDay ) <= 7);


            //If "hoursInWeek" is a prime value(only divisible by 1 and itself) or is not divisible by any no less than "totalWeekDays", e.g. 37, then there will be only one work day 
            //in week and rest all day of the week will have 0 working hours. In such scenario we will use  "minutesPerWeek" to find out number of work days in a week, Only concern 
            //with this approach is that number of work days will always be greater than or equal to 5, if we use "minutesPerWeek" to find out number of work days in a week.
            // This solution will be applied to main work schedule only. The first/Last week leave schedule can be eaisly overridden in UI.
            // (int)firstDay < 6 : Added this to handle situation where only one day is going to be work day(Start Day: Saturday)
            if (workDaysInWeek == 1 && (int)firstDay < 6)
            {
                workDaysInWeek = Enumerable.Range(1, totalWeekDays).Last(x => minutesPerWeek % x == 0 && (x + (int)firstDay) < 7);
            }

            //Working time for each work day
            var minutesInDay = minutesPerWeek / workDaysInWeek;

            //update schedule to have calculated time for each work day
            Enumerable.Range( 0, workDaysInWeek ).ForEach( r =>
            {             
                fteTimes[(int)firstDay + r].TotalMinutes = minutesInDay;
            });

            return fteTimes;
        }

        /// <summary>
        /// Create a list of Time for week starting n a specified date
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="scheduleTimes"></param>
        /// <returns></returns>
        private List<Time> CreateTimeList(DateTime startDate, List<Time> scheduleTimes = null)
        {
            if (scheduleTimes == null)
            {
                return new List<Time>(7)
                        {
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek(), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek().AddDays(1), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek().AddDays(2), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek().AddDays(3), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek().AddDays(4), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetFirstDayOfWeek().AddDays(5), TotalMinutes = 0 },
                            new Time() { SampleDate = startDate.GetLastDayOfWeek(), TotalMinutes = 0 },
                        };
            }

            var dayCounter = 0;
            scheduleTimes.OrderBy( o => o.SampleDate ).ForEach( s => s.SampleDate = startDate.GetFirstDayOfWeek().AddDays(dayCounter++) );
            
            return scheduleTimes;
        }

        #endregion

        /// <summary>
        /// Builds the schedule time.
        /// </summary>
        /// <param name="policyBehavior">The policy behavior.</param>
        /// <param name="workSchedule">The work schedule.</param>
        /// <param name="holidays">The holidays.</param>
        /// <param name="variableScheduleTimes">The variable schedule times.</param>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        private Time BuildScheduleTime(PolicyBehavior? policyBehavior, List<Schedule> workSchedule, List<DateTime> holidays, List<VariableScheduleTime> variableScheduleTimes, DateTime date)
        {
            // Build our time object
            Time time = new Time() { SampleDate = date };

            // Schedule for this date
            Schedule schedule = workSchedule.Where(s => date.DateInRange(s.StartDate, s.EndDate)).OrderBy(s => (date - s.StartDate)).FirstOrDefault();
            // Check if the scheudle is null, if it is, then determine if it's before or after the max range of schedules
            if (schedule == null)
            {
                // Is it before the earliest schedule; we should have covered this, but if we didn't, there's a reason
                //  so just return zero for this time.
                return time;
            }

            if (schedule.ScheduleType == ScheduleType.Rotating)
            {
                // Rotating schedules use pattern positions from the schedule start date and length of
                //  pattern rotation to get to today
                int position = PatternPosition(schedule.Times.Min(t => t.SampleDate), date, schedule.Times.Count);
                if (schedule.Times.Count > position)
                {
                    // It fits the pattern, so let's set it
                    time.TotalMinutes = schedule.Times[position].TotalMinutes;
                }
            }
            else
            {
                // It's a simple weekly basis schedule, therefore just match the weekday and use those minutes
                time.TotalMinutes = schedule.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek)?.TotalMinutes ?? 0;
            }

            // This is a variable schedule, so has some special handling
            if (schedule.ScheduleType == ScheduleType.Variable)
            {
                var variable = variableScheduleTimes.FirstOrDefault(v => v.Time.SampleDate == date);
                if (variable != null)
                {
                    time.TotalMinutes = variable.Time.TotalMinutes;
                }
                else if (variableScheduleTimes.Any(v => v.Time.SampleDate.DateInRange(
                    // We want the start of the week OR start of the schedule effective date
                    //  whichever is further out
                    Date.Max(date.GetFirstDayOfWeek(Employee?.StartDayOfWeek), schedule.StartDate),
                    date.GetLastDayOfWeek(Employee?.StartDayOfWeek))))
                {
                    // This means there are other variable times for this calendar week
                    //  but not one for this date, so set to the total minutes to zero
                    time.TotalMinutes = 0;
                }
            }

            // Check if this is a holiday, and if so take appropriate action
            if (holidays.Any(h => h.Date == date))
            {
                switch (policyBehavior)
                {
                    // Holiday is ignored, just set is holiday to false and don't adjust time
                    case PolicyBehavior.Ignored:
                        time.IsHoliday = false;
                        break;
                    // Holiday is a holiday and no lost time, set total minutes to zero and mark it as a holiday
                    default:
                        time.IsHoliday = true;
                        time.TotalMinutes = 0;
                        break;
                }
            }

            return time;
        }


        /// <summary>
        /// Use an initialized LOA (this) ojbect and generate a schedule that is between
        /// the passed start and end dates, inclusive of both dates
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="includeHolidays">if set to <c>true</c> [include holidays].</param>
        /// <param name="schedules">The schedules.</param>
        /// <param name="policyBehavior">The policy behavior.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="totalWorkDays">this param is required for Segment FTE leave schedule, it should be = Emp work days </param>
        /// <returns>
        /// List of Time objects
        /// </returns>
        public List<Time> MaterializeSchedule(DateTime startDate, DateTime endDate, bool includeHolidays = true, List<Schedule> schedules = null, PolicyBehavior policyBehavior = PolicyBehavior.ReducesWorkWeek, string caseId = null, int? totalWorkDays = null)
        {
            return MaterializeScheduleByDay(startDate, endDate, includeHolidays, schedules, policyBehavior, caseId, totalWorkDays).ToList();
        }

        /// <summary>
        /// pass this method the pattern's start date, the date you are looking at and how
        /// many days are in the pattern and it will return the element position of
        /// the target date
        /// </summary>
        /// <param name="patternStartDate"></param>
        /// <param name="targetDate"></param>
        /// <param name="patternLength"></param>
        /// <returns></returns>
        private int PatternPosition(DateTime patternStartDate, DateTime targetDate, int patternLength)
        {
            int days = (patternStartDate - targetDate).Days;
            int pos = Math.Abs(days) % patternLength;
            if (days > 0)
            {
                pos = patternLength - pos;
                if (pos == patternLength)
                    pos = 0;
            }

            return pos;
        }

        /// <summary>
        /// Builds the holiday list for the years covered by the collection of years input
        /// </summary>
        /// <param name="years">The years collection to get holidays for</param>
        /// <returns>A list of dates that are holidays (ignore usage)</returns>
        private List<DateTime> GetHolidayList(List<int> years)
        {
            using (new InstrumentationContext("LeaveOfAbsence.GetHolidayList(years)"))
            {
                List<DateTime> holidays = new List<DateTime>();
                HolidayService hs = new HolidayService();

                foreach (int i in years)
                    holidays.AddRange(hs.HolidaysForYear(Employer.Holidays, i));

                return holidays;
            }
        }

        /// <summary>
        /// check the internal state of the object to make sure that calcs
        /// can be run without errors
        /// </summary>
        private bool ValidToCalc(List<Schedule> schedules = null)
        {
            if (!(Case != null && Case.Segments != null && Case.Segments.Any(segment => segment.Type == CaseType.Administrative)))
            {
                if ((schedules ?? WorkSchedule) == null || (schedules ?? WorkSchedule).Count == 0)
                    if (Employee != null || Case != null)
                        WorkSchedule = Employee.GetById(Employee == null ? Case.Employee.Id : Employee.Id)?.WorkSchedules;

                if ((schedules ?? WorkSchedule) == null || (schedules ?? WorkSchedule).Count == 0)
                    return false;


                // check to see if any of the work schedules do not have times
                if ((schedules ?? WorkSchedule).Exists(ws => ws.Times.Count == 0 && ws.FteMinutesPerWeek <=0))
                {
                    return false;
                }
            }

            // if there is no employer then there are no holidays
            if (Employer == null)
                return false;


            if (Employer.Holidays == null)
            {
                return false;
            }



#warning TODO: proof there are no errors in the schedule dates / times (function on the schedule object?)
#warning TODO: proof there are no pattern errors on the schedule as well, e.g. duplicate dates, gaps in dates
            return true;
        }


        /// <summary>
        /// Gets a value indicating whether or not the employee meets the 50 in 75 mile rule.
        /// This always returns <c>true</c> if the employer has not opted in for this rule.
        /// </summary>
        /// <returns><c>true</c> if the employee meets the 50 in 75 mile rule, otherwise <c>false</c>.</returns>
        public bool Meets50In75MileRule()
        {
            if (Employer.Enable50In75MileRule)
                return Employee.Meets50In75MileRule;

            // Always default to true.
            return true;
        }



        /// <summary>
        /// Gets the total number of months worked (assuming partial months still = one month).
        /// </summary>
        /// <returns></returns>
        public int TotalMonthsWorked()
        {
            using (new InstrumentationContext("LeaveOfAbsence.TotalMonthsWorked"))
            {
                var scheds = (WorkSchedule ?? Employee.WorkSchedules);
                DateTime startDate = Employee.ServiceDate ?? Employee.HireDate ?? (scheds != null && scheds.Any() ? scheds.Min(s => s.StartDate) : DateTime.UtcNow.ToMidnight());
                DateTime endDate = Case.StartDate;
                int months = 0;
                if (ValidToCalc() && scheds != null)
                {
                    months = MaterializeSchedule(startDate, endDate, true, scheds)
                    .Where(t => t.TotalMinutes.HasValue && t.TotalMinutes.Value > 0)
                    .OrderBy(t => t.SampleDate)
                    .Select(t => new { year = t.SampleDate.Year, month = t.SampleDate.Month })
                    .Distinct()
                    .Count();
                }
                else
                {
                    //Since scheule is not available and there arn't enough details 
                    //available to convert Prior Hours Worked to Total Months Worked,
                    //assuming month difference starting from Employee Service Date / Hire Date
                    months = (int)Math.Ceiling(startDate.GetMonthDiff(endDate));
                }

                return months < 0 ? 0 : months;
            }
        }



        [NonSerialized]
        private bool? _has1YearOfService = null;
        /// <summary>
        /// Gets a value indicating whether or not the employee for this case has met the 1 year of service rule.
        /// </summary>
        /// <returns></returns>
        public bool Has1YearOfService()
        {
            if (_has1YearOfService.HasValue)
                return _has1YearOfService.Value;

            // If no service date or hire date is specified, then it has to be null, duh.
            if ((Employee.ServiceDate ?? Employee.HireDate) == null)
                return false;

            // Get a date to test with that is 1 year ago from case's start date.
            DateTime cs = Case.StartDate;
            DateTime testDate = new DateTime(cs.Year - 1, cs.Month, cs.Month == 2 && cs.Day == 29 ? 28 : cs.Day, 0, 0, 0, DateTimeKind.Utc).Date;

            // Get our real measure/service date, either the service, or, if not provided, the employee's date of hire.
            DateTime serviceDate = (Employee.ServiceDate ?? Employee.HireDate.Value).Date;

            _has1YearOfService = serviceDate <= testDate;
            // Return whether or not the service date is before or equal to our test date 1 year ago.
            return _has1YearOfService.Value;
        }

        /// <summary>
        /// Determines whether or not the employee has met the minimum length of service
        /// from their service/hire date.
        /// </summary>
        /// <param name="time">The amount of time the employee</param>
        /// <param name="unitType"></param>
        /// <returns></returns>
        public bool HasMinLengthOfService(int minLengthOfService, Unit unitType)
        {
            // If no service date or hire date is specified, then it has to be null, duh.
            if ((Employee.ServiceDate ?? Employee.HireDate) == null)
                return false;

            // Get a date to test with that is 1 year ago from case's start date.
            DateTime cs = Case.StartDate;
            DateTime testDate;

            switch (unitType)
            {
                case Unit.Minutes:
                    testDate = cs.AddMinutes(-1 * minLengthOfService);
                    break;
                case Unit.Hours:
                    testDate = cs.AddHours(-1 * minLengthOfService);
                    break;
                case Unit.Days:
                    testDate = cs.AddDays(-1 * minLengthOfService);
                    break;
                case Unit.Weeks:
                    testDate = cs.AddDays(-1 * 7 * minLengthOfService);
                    break;
                case Unit.Months:
                    testDate = cs.AddMonths(-1 * minLengthOfService);
                    break;
                case Unit.Years:
                default:
                    testDate = cs.AddYears(-1 * minLengthOfService);
                    break;
            }

            // Get our real measure/service date, either the service, or, if not provided, the employee's date of hire.
            DateTime serviceDate = (Employee.ServiceDate ?? Employee.HireDate.Value).Date;

            // Return whether or not the service date is before or equal to our test date 1 year ago.
            return serviceDate <= testDate;
        }//HasMinLengthOfService


        public double AverageHoursWorkedPerWeekOverPeriod(int hoursWorked, int minLengthToConsider, Unit unitType)
        {
            // If no service date or hire date is specified, then it has to be null, duh.
            if ((Employee.ServiceDate ?? Employee.HireDate) == null)
                return 0;


            // Get a date to test with that is 1 year ago from case's start date.
            DateTime cs = Case.StartDate;
            DateTime testDate;

            switch (unitType)
            {
                case Unit.Minutes:
                    testDate = cs.AddMinutes(-1 * minLengthToConsider);
                    break;
                case Unit.Hours:
                    testDate = cs.AddHours(-1 * minLengthToConsider);
                    break;
                case Unit.Days:
                    testDate = cs.AddDays(-1 * minLengthToConsider);
                    break;
                case Unit.Weeks:
                    testDate = cs.AddDays(-1 * 7 * minLengthToConsider);
                    break;
                case Unit.Months:
                    testDate = cs.AddMonths(-1 * minLengthToConsider);
                    break;
                case Unit.Years:
                default:
                    testDate = cs.AddYears(-1 * minLengthToConsider);
                    break;
            }

            if (Employee.MinutesWorkedPerWeek != null && Employee.MinutesWorkedPerWeek.Any())
            {
                var p = Employee.MinutesWorkedPerWeek.OrderBy(o => o.AsOf).LastOrDefault();
                if (p != null)
                {
                    if (IgnoreAverageMinutesWorkedPerWeekCalculation())
                        return p.MinutesWorked / 60d;
                    else
                    {
                        double averageHoursWorkedPerWeek = 0;
                        List<MinutesWorkedPerWeek> workedPerWeek = Employee.MinutesWorkedPerWeek.Where(h => h.AsOf.ToMidnight() > testDate.ToMidnight()).ToList();
                        if (workedPerWeek != null && workedPerWeek.Any())
                        {
                            averageHoursWorkedPerWeek = (workedPerWeek.Sum(x => x.MinutesWorked) / workedPerWeek.Count) / 60d;
                        }
                        return averageHoursWorkedPerWeek;
                    }

                }
            }
            return 0;
        }//AverageHoursWorkedPerWeekOverPeriod


        /// <summary>
        /// Stupid rule for Minnesota where they take the lesser of some number of hours
        /// per week or half of the employer's FT defined work week.
        /// </summary>
        /// <param name="nHours">The number of hours to measure against, OR the employer's FT hours / 2, whichever is less.</param>
        /// <returns>Whether or not the employee meets this rule</returns>
        public bool HasLesserOfHalfFTHoursOrNHoursPerWeek(double nHours)
        {
            var hours = HoursWorkedPerWeek();
            return hours > nHours || hours > ((Employer.FTWeeklyWorkHours ?? (nHours * 2)) / 2);
        }


        /// <summary>
        /// Determines whether or not this leave of absence has a segment of the type passed in.
        /// This is a short-cut method for the expression evaluation engine because it can't do LINQ.
        /// </summary>
        /// <param name="caseType">The case type to check for in the segments collection.</param>
        /// <returns><c>true</c> if a segment exists for that type, otherwise <c>false</c>.</returns>
        public bool HasSegmentOfType(CaseType caseType)
        {
            if (Case == null || Case.Segments == null)
                return false;

            return Case.Segments.Any(s => s.Type == caseType);
        }//HasSegmentOfType



        /// <summary>
        /// Determines whether or not this leave of absence has a communication of the specified type.
        /// This is a short-cut method for the expression evaluation engine because it can't do LINQ.
        /// </summary>
        /// <param name="communicationType">The communication type to check for.</param>
        /// <returns><c>true</c> if a communication exists for that type, otherwise <c>false</c>.</returns>
        public bool HasCommunication(CommunicationType communicationType)
        {
            if (Communications == null || this.Communications.Count == 0)
                return false;

            return this.Communications.Any(c => c.CommunicationType == communicationType);
        }//HasCommunication


        /// <summary>
        /// Gets a value indicating whether or not this case has any pending paperwork requiring review.
        /// </summary>
        /// <returns></returns>
        public bool HasPaperworkRequiringReview()
        {
            if (Communications == null || Communications.Count == 0)
                return false;

            return this.Communications.Any(c => c.Paperwork != null && c.Paperwork.Any(p => p.Paperwork != null && p.Paperwork.RequiresReview && p.Status != PaperworkReviewStatus.Complete && p.Status != PaperworkReviewStatus.NotApplicable));
        }


        /// <summary>
        /// Determines whether or not this leave of absence has a task of the specified type.
        /// This is a short-cut method for the expression evaluation engine because it can't do LINQ.
        /// </summary>
        /// <param name="taskType">The task type to check for.</param>
        /// <param name="taskStatus">The optional task status that the task
        /// of the specified type must be (this is optional).</param>
        /// <returns><c>true</c> if a task exists for that type, otherwise <c>false</c>.</returns>
        public bool HasTask(ToDoItemType taskType, ToDoItemStatus? taskStatus = null)
        {
            if (Tasks == null || this.Tasks.Count == 0)
                return false;

            if (taskStatus == null)
                return this.Tasks.Any(t => t.ItemType == taskType);

            return this.Tasks.Any(t => t.ItemType == taskType && t.Status == taskStatus.Value);
        }//HasTask


        /// <summary>
        /// Gets a value indicating whether or not there is any paperwork due for the absence on any
        /// communications that have been sent.
        /// </summary>
        /// <returns></returns>
        public bool IsPaperworkDue()
        {
            return Communications.Any(c => c.Paperwork.Any(p => p.DueDate.HasValue && (p.Status == PaperworkReviewStatus.Pending || p.Status == PaperworkReviewStatus.Incomplete)));
        }


        [NonSerialized]
        private int? _totalEmployeesAtEmployer = null;
        /// <summary>
        /// Gets the total number of employee's at the employer for this employee's case.
        /// Used to typically filter on policy selection for FMLA or CFRA type policies.
        /// </summary>
        /// <returns></returns>
        public int TotalEmployeesAtEmployer()
        {
            if (_totalEmployeesAtEmployer.HasValue)
            {
                return _totalEmployeesAtEmployer.Value;
            }
            _totalEmployeesAtEmployer = Employee.AsQueryable().Where(e => e.EmployerId == Employer.Id).Count();

            return _totalEmployeesAtEmployer.Value;
        }

        [NonSerialized]
        private int? _totalEmployeesAtEmployerInTheSameState = null;
        /// <summary>
        /// Gets the total number of employee's at the employer for this employee's case in the same work state.
        /// Used to typically filter on policy selection for state FML type policies.
        /// </summary>
        /// <returns></returns>
        public int TotalEmployeesAtEmployerInTheSameWorkState()
        {
            if (_totalEmployeesAtEmployerInTheSameState.HasValue)
            {
                return _totalEmployeesAtEmployerInTheSameState.Value;
            }

            _totalEmployeesAtEmployerInTheSameState = Employee.AsQueryable().Where(e => e.EmployerId == Employer.Id && e.WorkState == Employee.WorkState).Count();
            return _totalEmployeesAtEmployerInTheSameState.Value;
        }

        /// <summary>
        /// Gets the Employee's Age in years as of the case start date or <c>0</c>.
        /// </summary>
        /// <returns>The employee's age in years from the case start date or 0 if no DOB recorded for the employee.</returns>
        public int AgeInYears()
        {
            if (Case == null || Employee == null || Employee.DoB == null)
                return 0;

            int years = Employee.DoB.Value.AgeInYears(Case.StartDate);
            return years;
        }

        /// <summary>
        /// Gets the contact's age in years as of the case start date or <c>0</c>.
        /// </summary>
        /// <returns>The contact's age in years from the case start date or 0 if no DOB recorded for the contact.</returns>
        public int ContactAgeInYears()
        {
            if (Case == null || Case.Contact == null || Case.Contact.Contact == null || Case.Contact.Contact.DateOfBirth == null)
                return 0;

            int years = Case.Contact.Contact.DateOfBirth.Value.AgeInYears(Case.StartDate);
            return years;
        }

        /// <summary>
        /// Gets a raw value from the case's metadata for comparison or evaluation by the eligibility rules engine.
        /// </summary>
        /// <param name="fieldName">The field name of the metadata attribute to get the value of</param>
        /// <returns>The value or default for the given field</returns>
        public bool IsMetadataValueTrue(string fieldName)
        {
            return Case.Metadata.GetRawValue<bool>(fieldName);
        }

        public bool IsWorkRelated
        {
            /// Is Work Related if the metadata is set or any of the accommodations are work related
            get
            {
                return IsMetadataValueTrue("IsWorkRelated") ||
                  (Case.AccommodationRequest != null &&
                  Case.AccommodationRequest.Accommodations != null
                  && Case.AccommodationRequest.Accommodations.Any(a => a.IsWorkRelated.HasValue && a.IsWorkRelated.Value));
            }
        }

        /// <summary>
        /// Gets the number of children set on the leave in the metadata used as a multiple for people in Massachusets or however you spell it.
        /// </summary>
        /// <returns></returns>
        public int NumberOfChildrenForLeave()
        {
            return Case.Metadata.GetRawValue<int?>("NumberOfChildren") ?? 0;
        }

        public double EffectiveAnnualPayFromLeaveStartDate()
        {
            if (Employee.PayType == null || Employee.Salary == null)
                return 0;

            if (Employee.PayType == PayType.Salary)
                return Employee.Salary.Value;

            return TotalHoursWorkedLast12Months() * Employee.Salary.Value;
        }

        public double NumberOfMonthsFromAdoptionDate()
        {
            return Math.Round(this.Case.FindCaseEvent(CaseEventType.AdoptionDate).EventDate.GetMonthDiff(this.Case.StartDate), 2);
        }

        public double NumberOfMonthsFromDeliveryDate()
        {
            return Math.Round(this.Case.FindCaseEvent(CaseEventType.DeliveryDate).EventDate.GetMonthDiff(this.Case.StartDate), 2);
        }

        /// <summary>
        /// Get the date difference between service date and expected delivery date of an employee 
        /// </summary>
        /// <returns>double</returns>
        public double LengthOfServiceFromExpectedDeliveryDate()
        {
            return Math.Round(this.Case.Employee.ServiceDate.Value.GetMonthDiff(this.Case.FindCaseEvent(CaseEventType.DeliveryDate).EventDate), 2);
        }

        public string EmployerServiceOption()
        {
            using (var serviceOptionService = new EmployerServiceOptionService(Customer, Employer, null))
            {
                return serviceOptionService.GetCustomerServiceOptionId(Employer.Id);
            }
        }

        /// <summary>
        /// Gets the the custom field by name and target.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        private CustomField GetCustomFieldByName(string name, EntityTarget target)
        {
            List<CustomField> fields = null;
            switch (target)
            {
                case EntityTarget.Employee:
                    if (Employee != null)
                    {
                        fields = Employee.CustomFields;
                    }
                    break;
                case EntityTarget.Case:
                    if (Case != null)
                    {
                        fields = Case.CustomFields;
                    }
                    break;
                case EntityTarget.None:
                default:
                    return null;
            }
            var fieldNameTrim = System.Text.RegularExpressions.Regex.Replace(name, @"\s", "");
            if (fields != null && fields.Any())
            {
                return fields.FirstOrDefault(cf => cf.Name.Replace(" ", string.Empty).Equals(fieldNameTrim, StringComparison.InvariantCultureIgnoreCase));
            }
            return null;
        }

        /// <summary>
        /// Returns the custom field string.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        public string CustomFieldString(CustomField field)
        {
            if (field != null)
                return field.SelectedValueText;

            return null;
        }

        /// <summary>
        /// Returns the custom field date.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        public DateTime CustomFieldDate(CustomField field)
        {
            if (field != null)
                return Convert.ToDateTime(field.SelectedValue);

            return DateTime.MinValue;
        }

        /// <summary>
        /// Returns the custom field number.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        public int CustomFieldNumber(CustomField field)
        {
            if (field != null)
                return Convert.ToInt32(field.SelectedValue);

            return int.MinValue;
        }

        /// <summary>
        /// Returns the custom field boolean.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        public bool CustomFieldBoolean(CustomField field)
        {
            if (field != null)
                return Convert.ToBoolean(field.SelectedValue);

            return false;
        }

        /// <summary>
        /// Gets an employee custom field by name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public string CustomField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Employee);
            return CustomFieldString(field);
        }

        /// <summary>
        /// Gets an employee custom date field by name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public DateTime CustomDateField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Employee);
            return CustomFieldDate(field);
        }

        /// <summary>
        /// Gets an employee custom number field by name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public int CustomNumberField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Employee);
            return CustomFieldNumber(field);
        }


        /// <summary>
        /// Gets an employee custom boolean field by name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public bool CustomBoolField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Employee);
            return CustomFieldBoolean(field);
        }

        /// <summary>
        /// Gets a case custom field by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public string CaseCustomField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Case);
            return CustomFieldString(field);
        }

        /// <summary>
        /// Gets a case custom date field by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public DateTime CaseCustomDateField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Case);
            return CustomFieldDate(field);
        }

        /// <summary>
        /// Gets a case custom number field by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public int CaseCustomNumberField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Case);
            return CustomFieldNumber(field);
        }

        /// <summary>
        /// Gets a case custom boolean field by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public bool CaseCustomBoolField(string name)
        {
            CustomField field = GetCustomFieldByName(name, EntityTarget.Case);
            return CustomFieldBoolean(field);
        }

        /// <summary>
        /// Evaluates an Employee Custom Field of type Text or Number
        /// </summary>
        /// <param name="op">The op.</param>
        /// <param name="value">The value.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public bool EmployeeCustomField(string op, object value, string fieldName)
        {
            CustomField field = GetCustomFieldByName(fieldName, EntityTarget.Employee);
            if (field == null)
            {
                return false;
            }

            switch (field.DataType)
            {
                case CustomFieldType.Text:
                    return EvaluateTextCustomField(field, value, op);
                case CustomFieldType.Number:
                    return EvaluateNumberCustomField(field, value, op);
                case CustomFieldType.Flag:
                    return EvaluateBooleanCustomField(field, value, op);
                case CustomFieldType.Date:
                    return EvaluateDateCustomField(field, value, op);
            }

            return false;
        }

        /// <summary>
        /// Evaluates the boolean custom field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="op">The op.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Unimplemented operator</exception>
        private bool EvaluateBooleanCustomField(CustomField field, object value, string op)
        {
            bool compareValues(bool compareA, bool compareB)
            {
                switch (op)
                {
                    case "==":
                        return compareA == compareB;
                    case "!=":
                        return compareA != compareB;
                    default:
                        throw new AbsenceSoftException("Unimplemented operator");
                }
            }

            if (bool.TryParse(value.ToString(), out bool compareValue) && bool.TryParse(field.SelectedValueText, out bool fieldValue))
                return compareValues(compareValue, fieldValue);
            return false;
        }

        /// <summary>
        /// Evaluates the text custom field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="op">The op.</param>
        /// <returns></returns>
        private bool EvaluateTextCustomField(CustomField field, object value, string op)
        {
            string trimmedValue = value.ToString().ToLower().Trim();
            string trimmedSelectedValue = field.SelectedValueText.ToLower().Trim();
            if (op == "==")
            {
                return trimmedValue == trimmedSelectedValue;
            }
            else if (op == "!=")
            {
                return trimmedValue != trimmedSelectedValue;
            }

            return false;
        }

        /// <summary>
        /// Evaluates the number custom field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="op">The op.</param>
        /// <returns></returns>
        private bool EvaluateNumberCustomField(CustomField field, object value, string op)
        {
            if (int.TryParse(value.ToString(), out int numberValue) && int.TryParse(field.SelectedValueText, out int selectedNumberValue))
            {
                switch (op)
                {
                    case "==":
                        return selectedNumberValue == numberValue;
                    case "!=":
                        return selectedNumberValue != numberValue;
                    case ">":
                        return selectedNumberValue > numberValue;
                    case ">=":
                        return selectedNumberValue >= numberValue;
                    case "<":
                        return selectedNumberValue < numberValue;
                    case "<=":
                        return selectedNumberValue <= numberValue;
                    default:
                        throw new AbsenceSoftException("Unimplemented operator");
                }
            }

            return false;
        }

        /// <summary>
        /// Evaluates the date custom field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="op">The op.</param>
        /// <returns></returns>
        private bool EvaluateDateCustomField(CustomField field, object value, string op)
        {
            if (DateTime.TryParse(value.ToString(), out DateTime dateValue) && DateTime.TryParse(field.SelectedValueText, out DateTime selectedDateValue))
            {
                switch (op)
                {
                    case "==":
                        return selectedDateValue == dateValue;
                    case "!=":
                        return selectedDateValue != dateValue;
                    case ">":
                        return selectedDateValue > dateValue;
                    case ">=":
                        return selectedDateValue >= dateValue;
                    case "<":
                        return selectedDateValue < dateValue;
                    case "<=":
                        return selectedDateValue <= dateValue;
                    default:
                        throw new AbsenceSoftException("Unimplemented operator");
                }
            }

            return false;
        }

        public bool CaseCustomField(string op, object value, string fieldName)
        {
            if (Case != null && Case.CustomFields != null && Case.CustomFields.Count > 0)
            {
                var cf = Case.CustomFields.FirstOrDefault(c => c.Name == fieldName.Trim() && c.DataType == CustomFieldType.Text && ((op == "==" && c.SelectedValueText.ToLower().Trim() == value.ToString().ToLower().Trim()) || (op == "!=" && c.SelectedValueText.ToLower().Trim() != value.ToString().ToLower().Trim())));
                if (cf != null)
                    return true;
                var cfn = Case.CustomFields.FirstOrDefault(c => c.Name == fieldName.Trim() && c.DataType == CustomFieldType.Number
                    && ((op == ">" && Convert.ToInt32(c.SelectedValueText) > Convert.ToInt32(value))
                        || (op == ">=" && Convert.ToInt32(c.SelectedValueText) >= Convert.ToInt32(value))
                        || (op == "<" && Convert.ToInt32(c.SelectedValueText) < Convert.ToInt32(value))
                        || (op == "<=" && Convert.ToInt32(c.SelectedValueText) <= Convert.ToInt32(value))
                    ));
                if (cfn != null)
                    return true;
            }

            return false;
        }

        public bool CaseCustomField(string op, DateTime value, string fieldName)
        {
            if (Case != null && Case.CustomFields != null && Case.CustomFields.Count > 0)
            {
                var cfd = Case.CustomFields.FirstOrDefault(c => c.Name == fieldName.Trim() && c.DataType == CustomFieldType.Date
                    && ((op == ">" && Convert.ToDateTime(c.SelectedValueText).ToUtcDateTime() > Convert.ToDateTime(value).ToUtcDateTime())
                        || (op == ">=" && Convert.ToDateTime(c.SelectedValueText).ToUtcDateTime() >= Convert.ToDateTime(value).ToUtcDateTime())
                        || (op == "<" && Convert.ToDateTime(c.SelectedValueText).ToUtcDateTime() < Convert.ToDateTime(value).ToUtcDateTime())
                        || (op == "<=" && Convert.ToDateTime(c.SelectedValueText).ToUtcDateTime() <= Convert.ToDateTime(value).ToUtcDateTime())
                    ));
                if (cfd != null)
                    return true;
            }

            return false;
        }

        public string AccommodationCategory()
        {
            if (this.Case.IsAccommodation)
            {
                List<Accommodation> afterFilterAccommodations = new List<Accommodation>();

                // it can be an accommodation case but, not have any yet
                if (this.Case.AccommodationRequest != null)
                {
                    foreach (Accommodation accom in this.Case.AccommodationRequest.Accommodations)
                    {
                        if (accom.EndDate.HasValue)
                        {
                            if (accom.StartDate.Value < DateTime.Today && accom.EndDate.Value > DateTime.Today)
                            {
                                afterFilterAccommodations.Add(accom);
                            }
                        }
                        else
                        {
                            if (accom.StartDate.Value < DateTime.Today)
                            {
                                afterFilterAccommodations.Add(accom);
                            }
                        }
                    }
                }
                List<string> accomNames = afterFilterAccommodations.Select(k => k.Type.Name).ToList();
                return accomNames.Count > 0 ? String.Join(", ", accomNames) : "None";
            }
            else
            {
                return "None";
            }
        }

        private bool IgnoreAverageMinutesWorkedPerWeekCalculation()
        {

            var employer = this.Employer;
            if (employer == null && this.Employee != null)
            {
                employer = this.Employee.Employer;
            }

            if (employer == null && this.Case != null)
            {
                employer = this.Case.Employer;
            }

            if (employer != null)
            {
                return employer.IgnoreAverageMinutesWorkedPerWeek;
            }

            return false;

        }

        #region Risk Profile Calculations

        public int NumberOfConsecutiveCasesInTheLastYear()
        {
            return NumberOfCasesInTheLastYear(CaseType.Consecutive);
        }

        public int NumberOfIntermittentCasesInTheLastYear()
        {
            return NumberOfCasesInTheLastYear(CaseType.Intermittent);
        }

        public int NumberOfReducedCasesInTheLastYear()
        {
            return NumberOfCasesInTheLastYear(CaseType.Reduced);
        }

        public int NumberOfAdministrativeCasesInTheLastYear()
        {
            return NumberOfCasesInTheLastYear(CaseType.Administrative);
        }

        private int NumberOfCasesInTheLastYear(CaseType type)
        {
            return EmployeeCasesInTheLastYear()
                .Count(c => c.Segments.Any(s => s.Type == type));
        }

        public int NumberOfHoursUsedInTheLastYear()
        {
            var employeeCases = EmployeeCasesInTheLastYear()
                .SelectMany(c => c.Segments)
                .SelectMany(c => c.AppliedPolicies)
                .SelectMany(ap => ap.Usage)
                .Where(u => u.Determination == AdjudicationStatus.Approved);
            Dictionary<DateTime, double> dateAndTimeUsed = new Dictionary<DateTime, double>();
            foreach (var usage in employeeCases)
            {
                if (!dateAndTimeUsed.ContainsKey(usage.DateUsed))
                {
                    dateAndTimeUsed.Add(usage.DateUsed, usage.MinutesUsed);
                }
            }
            return Convert.ToInt32(dateAndTimeUsed.Sum(dat => dat.Value) / 60);
        }

        private IEnumerable<Case> EmployeeCasesInTheLastYear()
        {
            var now = DateTime.UtcNow;
            var oneYearAgo = now.AddYears(-1);

            var employeeCases = Case.AsQueryable().
                Where(c => c.Employee.Id == EmployeeId).ToList();

            return employeeCases.Where(c => c.StartDate.DateInRange(oneYearAgo, now)
                || !c.EndDate.HasValue
                || (c.EndDate.HasValue && c.EndDate.Value.DateInRange(oneYearAgo, now)));
        }

        public int AverageAbsenceDurationComparedToODGGuidelines()
        {
            decimal averageAbsenceDuration = 0;
            if (Case == null)
            {
                averageAbsenceDuration = CalculateDurationForEmployee();
            }
            else
            {
                averageAbsenceDuration = CalculateDurationForCase();
            }

            return Convert.ToInt32(averageAbsenceDuration * 100);
        }

        private decimal CalculateDurationForCase(Case theCase = null)
        {
            if (theCase == null)
                theCase = Case;

            if (theCase.Disability == null || theCase.Disability.PrimaryDiagnosis == null || string.IsNullOrEmpty(theCase.Disability.PrimaryDiagnosis.Code))
                return 0;

            string diagnosisCode = theCase.Disability.PrimaryDiagnosis.Code;
            using (var diagnosisGuidelinesService = new DiagnosisGuidelinesService())
            {
                DiagnosisGuidelines guidelines = diagnosisGuidelinesService.GetDiagnosisGuidelines(diagnosisCode);
                if (guidelines == null)
                    return 0;

                decimal odgGuidelines = guidelines.RtwSummary.AbsencesMidRange;
                decimal approvedDaysOff = ApprovedDaysOff(theCase);
                decimal percentage = (approvedDaysOff - odgGuidelines) / odgGuidelines;
                return percentage;
            }
        }

        private int ApprovedDaysOff(Case theCase)
        {
            if (theCase.Summary == null || theCase.Summary.Policies == null)
                return 0;

            List<DateTime> approvedDays = new List<DateTime>();
            foreach (var policy in ApprovedPolicies(theCase))
            {
                foreach (var day in policy.StartDate.AllDatesInRange(policy.EndDate.Value))
                {
                    approvedDays.AddIfNotExists(day);
                }
            }

            return approvedDays.Count;
        }

        private IEnumerable<CaseSummaryPolicyDetail> ApprovedPolicies(Case theCase)
        {
            return theCase.Summary.Policies
                .SelectMany(p => p.Detail)
                .Where(d => d != null
                        && d.Determination == AdjudicationSummaryStatus.Approved
                        && d.EndDate.HasValue);
        }

        private decimal CalculateDurationForEmployee()
        {
            var cases = Case.AsQueryable().Where(c => c.Employee.Id == Employee.Id).ToList();
            decimal total = 0;
            foreach (var aCase in cases)
            {
                total += CalculateDurationForCase(aCase);
            }
            decimal employeeAverage = total / cases.Count;
            return employeeAverage;
        }


        public string MedicalCondition
        {
            get
            {
                if (Case == null || Case.Disability == null)
                    return null;

                if (Case.Disability.PrimaryDiagnosis != null && Case.Disability.PrimaryDiagnosis.CodeType == DiagnosisType.ICD10)
                    return Case.Disability.PrimaryDiagnosis.Code;

                if (Case.Disability.SecondaryDiagnosis != null)
                {
                    var firstICD10Diagnosis = Case.Disability.SecondaryDiagnosis.FirstOrDefault(d => d.CodeType == DiagnosisType.ICD10);
                    if (firstICD10Diagnosis != null)
                        return firstICD10Diagnosis.Code;
                }

                return null;
            }
        }

        #endregion

        #region New York PFL Length Of Service Calculations        
        /// <summary>
        /// This method determines whether employee works part time and meets the condition of minimum configured continous employment duration
        /// and condition of minimum configured period of time worked which excludes any leave of absence.
        /// </summary>
        /// <param name="minConsecutiveLengthOfEmployment"></param>
        /// <param name="unitType"></param>
        /// <param name="minimumEmploymentPeriod"></param>
        /// <param name="minimumEmploymentPeriodUnitType"></param>
        /// <returns></returns>
        public bool MeetsNewYorkPflMinLengthOfServicePT(int minConsecutiveLengthOfEmployment, Unit unitType, int minimumEmploymentPeriod)
        {
            if (this.Case.Employee.EmployeeClassCode != null)
            {
                return (this.Case.Employee.EmployeeClassCode == WorkType.PartTime && HasWorkedMinConsecutivePeriod(minConsecutiveLengthOfEmployment, unitType) && HasWorkedMinEmploymentPeriod(minConsecutiveLengthOfEmployment, unitType, minimumEmploymentPeriod));
            }
            else
            {
                return (HasWorkedMinConsecutivePeriod(minConsecutiveLengthOfEmployment, unitType) && HasWorkedMinEmploymentPeriod(minConsecutiveLengthOfEmployment, unitType, minimumEmploymentPeriod));
            }
        }

        /// <summary>
        /// /// This method determines whether employee works full time and is continously employed for a minimum configured duration.
        /// /// </summary>
        /// <param name="minConsecutiveLengthOfEmployment"></param>
        /// <param name="unitType"></param>
        /// <returns></returns>
        public bool MeetsNewYorkPflMinLengthOfServiceFT(int minConsecutiveLengthOfEmployment, Unit unitType)
        {
            if (this.Case.Employee.EmployeeClassCode != null)
            {
                return (this.Case.Employee.EmployeeClassCode == WorkType.FullTime && HasWorkedMinConsecutivePeriod(minConsecutiveLengthOfEmployment, unitType));
            }
            else
            {
                return HasWorkedMinConsecutivePeriod(minConsecutiveLengthOfEmployment, unitType);
            }
        }

        /// <summary>
        /// This method returns employee cases within a data range which will be work related or non-work related depending on the workRelatedCase boolean flag.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="workRelatedCase"></param>
        /// <returns></returns>
        private List<Case> GetRelatedCases(DateTime startDate, DateTime endDate, bool workRelatedCase)
        {

            List<IMongoQuery> ands = new List<IMongoQuery>();

            if (!string.IsNullOrWhiteSpace(this.Case.Id))
            {
                ands.Add(Case.Query.NE(c => c.Id, this.Case.Id));
            }
            ands.Add(Case.Query.EQ(c => c.Employee.Id, this.Case.Employee.Id));
            ands.Add(Case.Query.IsNotDeleted());
            if (workRelatedCase)
            {
                ands.Add(Query.EQ("IsWorkRelated", BsonBoolean.True));
            }
            else
            {
                ands.Add(Query.NE("IsWorkRelated", BsonBoolean.True));
            }

            List<IMongoQuery> ors = new List<IMongoQuery>();
            ors.Add(Case.Query.Or(Case.Query.GTE(c => c.StartDate, new BsonDateTime(startDate)), Case.Query.LTE(c => c.StartDate, new BsonDateTime(endDate))));
            ors.Add(Case.Query.Or(Case.Query.GTE(c => c.EndDate, new BsonDateTime(startDate)), Case.Query.LTE(c => c.EndDate, new BsonDateTime(endDate))));
            ors.Add(Case.Query.And(Case.Query.LTE(c => c.StartDate, new BsonDateTime(startDate)), Case.Query.GTE(c => c.EndDate, new BsonDateTime(endDate))));
            ands.Add(Case.Query.Or(ors));

            ands.Add(Case.Query.NE(c => c.Status, CaseStatus.Cancelled));
            ands.Add(Case.Query.And(
                Case.Query.ElemMatch(c => c.Segments, s => s.NE(t => t.Type, CaseType.Administrative)),
                Case.Query.ElemMatch(c => c.Segments,
            s => s.ElemMatch(cs => cs.AppliedPolicies,
                                   ap => ap.In(p => p.Status, new BsonValue[] { EligibilityStatus.Pending, EligibilityStatus.Eligible })))));

            return Case.Repository.Collection.Find(Query.And(ands)).ToList();

        }
        /// <summary>
        /// This method determines whether minimum continuous employment condition is met by the employee.
        /// </summary>
        /// <param name="minConsecutiveLengthOfEmployment"></param>
        /// <param name="unitType"></param>
        /// <returns></returns>
        private bool HasWorkedMinConsecutivePeriod(int minConsecutiveLengthOfEmployment, Unit unitType)
        {
            DateTime? startDate = new[] { this.Case.Employee.ServiceDate, this.Case.Employee.HireDate, this.Case.Employee.RehireDate }.Max();
            DateTime endDate = this.Case.StartDate;

            if (startDate.HasValue)
            {
                var workRelatedCases = GetRelatedCases(startDate.Value, this.Case.StartDate, true);
                if (workRelatedCases.Any())
                {
                    startDate = workRelatedCases.Max(c => (c.EndDate ?? this.Case.StartDate.AddDays(-1)));
                    startDate = startDate.Value.AddDays(1);
                }
            }
            else
            {
                return false;
            }
            DateTime testDate = GetEmploymentPeriodStartDate(minConsecutiveLengthOfEmployment, unitType, endDate);

            return startDate <= testDate;
        }

        /// <summary>
        /// Given the end date of the minimum continous employment duration, this method returns the start date of the duration 
        /// </summary>
        /// <param name="minConsecutiveLengthOfEmployment"></param>
        /// <param name="unitType"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private DateTime GetEmploymentPeriodStartDate(int minConsecutiveLengthOfEmployment, Unit unitType, DateTime endDate)
        {
            DateTime startDate;
            switch (unitType)
            {
                case Unit.Minutes:
                    startDate = endDate.AddMinutes(-1 * minConsecutiveLengthOfEmployment);
                    break;
                case Unit.Hours:
                    startDate = endDate.AddHours(-1 * minConsecutiveLengthOfEmployment);
                    break;
                case Unit.Days:
                    startDate = endDate.AddDays(-1 * minConsecutiveLengthOfEmployment);
                    break;
                case Unit.Weeks:
                    startDate = endDate.AddDays(-1 * 7 * minConsecutiveLengthOfEmployment);
                    break;
                case Unit.Months:
                    startDate = endDate.AddMonths(-1 * minConsecutiveLengthOfEmployment);
                    break;
                default:
                    startDate = endDate.AddYears(-1 * minConsecutiveLengthOfEmployment);
                    break;
            }

            return startDate;
        }

        /// <summary>
        /// This method determines whether an employee meets the condition of minimum duration of time worked which exludes any leave of absence or non working days.
        /// </summary>
        /// <param name="minConsecutiveLengthOfEmployment"></param>
        /// <param name="unitType"></param>
        /// <param name="minimumEmploymentPeriod"></param>
        /// <param name="minimumEmploymentPeriodUnitType"></param>
        /// <returns></returns>
        private bool HasWorkedMinEmploymentPeriod(int minConsecutiveLengthOfEmployment, Unit unitType, int minimumEmploymentPeriod)
        {
            DateTime endDate = this.Case.StartDate;

            DateTime startDate = GetEmploymentPeriodStartDate(minConsecutiveLengthOfEmployment, unitType, endDate);
            //Get the Employee Schedule
            var schedules = (WorkSchedule ?? Employee.WorkSchedules);

            List<Case> relatedCases = GetRelatedCases(startDate, endDate, false);

            Dictionary<DateTime, double> usageCollection = relatedCases.SelectMany(c => c.Segments)
                .SelectMany(s => s.AppliedPolicies).SelectMany(u => u.Usage)
                .Where(u => u.MinutesUsed > 0 && u.Determination == AdjudicationStatus.Approved)
                .GroupBy(usage => usage.DateUsed)
                .OrderBy(u => u.Key).ToDictionary(gdc => gdc.Key, gdc => gdc.Max(u => u.MinutesUsed));

            double totalDaysWorked = 0;
            List<Time> scheduleList = MaterializeSchedule(startDate, endDate, true, schedules);

            foreach (Time schedule in scheduleList)
            {
                //Check if SampleDate  inside any of the existing case , then we are not going to add up
                if (schedule.TotalMinutes > 0)
                {
                    if (!usageCollection.ContainsKey(schedule.SampleDate) || (usageCollection.ContainsKey(schedule.SampleDate) &&
                        usageCollection[schedule.SampleDate] < schedule.TotalMinutes))
                    {
                        totalDaysWorked += 1;
                    }
                }
            }
            return totalDaysWorked >= minimumEmploymentPeriod;
        }
        #endregion
    }
}
