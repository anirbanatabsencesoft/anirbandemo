﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public static class CaseExtensions
    {
        public static DateTime? GetMinLeaveExhaustionDate(this Case @case)
        {
            var exhaustDates = @case.Segments.SelectMany(s => s.AppliedPolicies)
                .Where(p => p.FirstExhaustionDate.HasValue)
                .Select(p => p.FirstExhaustionDate.Value)
                .ToList();
            return exhaustDates.Any() ? exhaustDates.Min() : new DateTime?();
        }

        /// <summary>
        /// Retrieve most recent event date for eventType or null if none are found.
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public static DateTime? GetMostRecentEventDate(this Case @case, CaseEventType eventType)
        {
            if (@case == null || @case.CaseEvents == null)
            {
                return new DateTime?();
            }

            var events = @case.CaseEvents.Where(e => e.EventType == eventType).ToList();
            return events.Any() ? events.Max(e => e.EventDate) : new DateTime?();
        }

        /// <summary>
        /// Retrieve case closed date or null of case status is not closed
        /// </summary>
        /// <param name="@case"></param>
        /// <returns></returns>
        public static DateTime? GetClosedDate(this Case @case)
        {
            if (@case == null || @case.CaseEvents == null || @case.Status != CaseStatus.Closed)
            {
                return new DateTime?();
            }
            return @case.GetMostRecentEventDate(CaseEventType.CaseClosed);
        }

        /// <summary>
        /// Retrieve case cancelled date or null of case status is not cancelled
        /// </summary>
        /// <param name="case">case</param>
        /// <returns>cancelled date or null</returns>
        public static DateTime? GetCancelledDate(this Case @case)
        {
            if (@case == null || @case.CaseEvents == null || @case.Status != CaseStatus.Cancelled)
            {
                return new DateTime?();
            }
            return @case.GetMostRecentEventDate(CaseEventType.CaseCancelled);
        }

        public static bool IsIneligibleForAll(this Case @case)
        {
            return @case.Segments
                .SelectMany(s => s.AppliedPolicies)
                .All(a => a.Status == EligibilityStatus.Ineligible);
        }

        public static bool IsParrot(this Case @case)
        {
            var customer = @case.Customer ?? Customer.Current;
            return customer.Id == "546e5097a32aa00d60e3210a";
        }
    }
}
