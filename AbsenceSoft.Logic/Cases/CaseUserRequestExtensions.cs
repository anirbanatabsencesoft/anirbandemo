﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public static class CaseUserRequestExtensions
    {
        public static IEnumerable<IGrouping<int, IntermittentTimeRequest>> GroupByFrequency(this IEnumerable<IntermittentTimeRequest> requests, Unit frequencyType)
        {
            switch (frequencyType)
            {
                case Unit.Weeks:
                    return requests.GroupBy(g => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                        g.RequestDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday
                    ));
                case Unit.Months:
                    return requests.GroupBy(g => g.RequestDate.Month);
                case Unit.Years:
                    return requests.GroupBy(g => g.RequestDate.Year);
                default:
                    throw new NotSupportedException("FrequencyType not supported:" + frequencyType.ToString());
            }
        }

        /// <summary>
        /// Return MetricComparison values where provided
        /// IntermittentTimeRequest exceeds specified maxFrequency.
        /// </summary>
        /// <param name="requests">Time Off Requests to evaluate against. NOTE: Must be prefiltered to the the date range of the target Certification!</param>
        /// <param name="maxFrequency"></param>
        /// <param name="frequencyType"></param>
        /// <returns></returns>
        public static IEnumerable<MetricComparison> GetExceededFrequencies(this IEnumerable<IntermittentTimeRequest> requests, int maxFrequency, Unit frequencyType)
        {
            return requests.GroupByFrequency(frequencyType)
                .Select(s => new
                {
                    Items = s.OrderBy(o => o.RequestDate).ToList()
                })
                .Where(w => w.Items.Count > maxFrequency)
                .Select(s => new MetricComparison
                {
                    MaxValue = maxFrequency,
                    ActualValue = s.Items.Count,
                    Difference = s.Items.Count - maxFrequency
                });

        } // GetExceededFrequencies

        public static IEnumerable<MetricComparison> GetExceededDurations(this IEnumerable<IntermittentTimeRequest> requests, Unit frequencyType, double maxDuration, Unit durationType)
        {
            var maxDurationInMinutes = UnitToMinutes(maxDuration, durationType);
            return requests
                .GroupByFrequency(frequencyType)
                .Select(s => new
                {
                    Items = s.OrderBy(o => o.RequestDate).ToList()
                })
                .SelectMany(m => m.Items)
                .Where(w => w.TotalMinutes > maxDurationInMinutes)
                .Select(s => new MetricComparison
                {
                    MaxValue = maxDuration,
                    ActualValue = MinutesToUnit(s.TotalMinutes, durationType),
                    Difference = MinutesToUnit(s.TotalMinutes, durationType) - maxDuration
                });
        }

        public static double UnitToMinutes(double value, Unit fromUnit)
        {
            switch (fromUnit)
            {
                case Unit.Minutes:
                    return value;
                case Unit.Hours:
                    return value * 60;
                case Unit.Days:
                    return value * 60 * 24;
                default:
                    throw new NotSupportedException("unit not supported:" + fromUnit.ToString());

            }
        }

        public static int MinutesToUnit(int value, Unit toUnit)
        {
            switch (toUnit)
            {
                case Unit.Minutes:
                    return value;
                case Unit.Hours:
                    return value / 60;
                case Unit.Days:
                    return value / 60 / 24;
                default:
                    throw new NotSupportedException("unit not supported:" + toUnit.ToString());

            }
        }
    }
}
