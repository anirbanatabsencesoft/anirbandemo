﻿using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService
    {
        /// <summary>
        /// Get case reporter by caseId
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public List<CaseReporter> GetCaseReporters(string caseId)
        {
            var caseReporters = CaseReporter.AsQueryable().Where(ca => ca.CaseId == caseId).ToList();
            return caseReporters;
        }

        /// <summary>
        /// Gets the case reporter list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults GetCaseReporterList(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.GetCaseReporterList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                List<IMongoQuery> ands = new List<IMongoQuery>
                {
                    // Always add the customer id to the filter
                    CaseReporter.Query.EQ(a => a.CustomerId, CurrentUser.CustomerId)
                };

                string caseId = criteria.Get<string>("CaseId");
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    ands.Add(CaseReporter.Query.EQ(e => e.CaseId, caseId));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                // We want deleted records here
                var query = CaseReporter.Repository.Collection.Find(ands.Count > 0 ? CaseReporter.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);
                query = SetCaseReporterQuerySortBy(criteria, query);

                result.Total = query.Count();
                var results = query.ToList();

                List<ListResult> resultReportersList = results.Select(e => MapCaseReporterToListResult(e)).ToList();

                result.Results = resultReportersList;
                return result;
            }
        }

        private static MongoCursor<CaseReporter> SetCaseReporterQuerySortBy(ListCriteria criteria, MongoCursor<CaseReporter> query)
        {
            if (!string.IsNullOrWhiteSpace(criteria.SortBy))
            {
                if (criteria.SortBy.ToLower() == "createddate")
                {
                    query = query.SetSortOrder(criteria.SortDirection == Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending("cdt")
                        : SortBy.Descending("cdt"));
                }
                else
                {
                    query = query.SetSortOrder(criteria.SortDirection == Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending(criteria.SortBy)
                        : SortBy.Descending(criteria.SortBy));
                }
            }

            return query;
        }

        /// <summary>
        /// Maps the case assignee to list result.
        /// </summary>
        /// <param name="reporter">The assignee.</param>
        /// <param name="users">The users.</param>
        /// <returns></returns>
        private ListResult MapCaseReporterToListResult(CaseReporter reporter)
        {
            ListResult result;
            result = new ListResult()
                .Set("Id", reporter.Id)
                .Set("CaseId", reporter.CaseId)
                .Set("Category", "Case Reporter")
                .Set("CreatedDate", reporter.CreatedDate)
                .Set("ModifiedDate", reporter.ModifiedDate)
                .Set("ModifiedBy", (reporter.ModifiedBy ?? reporter.CreatedBy).DisplayName)
                .Set("Description", (reporter.Reporter.Contact).FullName);
            return result;
        }

    }
}
