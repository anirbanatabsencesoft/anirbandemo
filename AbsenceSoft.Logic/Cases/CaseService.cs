﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.CaseAssignmentRules;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Processing.CaseRecalcFutureCases;
using AbsenceSoft.Logic.Rules;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Notes;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService, ICaseService
    {
        private bool RunWorkflow = true;

        public CaseService(string customerId, string employerId, User user)
            : base(customerId, employerId, user)
        {

        }
        public CaseService() : base() { }
        public CaseService(bool runWorkflow) : this() { RunWorkflow = runWorkflow; }


        // Constructor used to allow nulls on current employer and user
        public CaseService(Customer currentCustomer, Employer currentEmployer = null, User currentUser = null)
            : base(currentCustomer, currentEmployer, currentUser)
        {
        }

        /// <summary>
        /// Returns a Distinct list of all Absence Reasons based on Current Customer and Employer
        /// </summary>
        /// <returns></returns>
        public List<AbsenceReason> GetDistinctAbsenceReasons()
        {
            return AbsenceReason.DistinctFind(null, CustomerId, EmployerId).ToList();
        }

        /// <summary>
        /// Gets the employers list of absence reasons an employee may use to request a leave of absence.
        /// </summary>
        /// <param name="employeeId">The employee Id to get the absence reasons for</param>
        /// <returns>A list of availabe absence reasons.</returns>
        public List<AbsenceReason> GetAbsenceReasons(string employeeId, CaseType? caseType = null)
        {
            using (new InstrumentationContext("CaseService.GetAbsenceReasons"))
            {
                Employee emp = Employee.GetById(employeeId);
                List<AbsenceReason> reasons = AbsenceReason.DistinctFind(MatchesEmployeeWorkCountry(emp), emp.CustomerId, emp.EmployerId, false).ToList()
                    .OrderBy(a => a.Name).ToList();

                if (caseType != null)
                    reasons = reasons.Where(a => (a.CaseTypes & caseType) == caseType || a.CaseTypes == null).ToList();

                if (User.IsEmployeeSelfServicePortal)
                {
                    reasons = reasons.Where(a => (a.Flags & AbsenceReasonFlag.HideInEmployeeSelfServiceSelection) != AbsenceReasonFlag.HideInEmployeeSelfServiceSelection).ToList();
                }

                if (emp.Employer != null && emp.Employer.SuppressReasons != null)
                {
                    reasons = reasons.Where(r => !emp.Employer.SuppressReasons.Contains(r.Code)).ToList();
                }

                var suppressReasons = emp.Customer.SuppressReasons.ToArray();
                reasons = reasons.Where(s => !suppressReasons.Contains(s.Code) || s.IsCustom == true).ToList();
                return reasons;
            }
        }

        /// <summary>
        /// Builds a query that checks if the employer matches the country
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private IMongoQuery MatchesEmployeeWorkCountry(Employee emp)
        {
            IMongoQuery query = AbsenceReason.Query.Or(
                AbsenceReason.Query.NotExists(ar => ar.Country),
                AbsenceReason.Query.EQ(ar => ar.Country, null),
                AbsenceReason.Query.EQ(ar => ar.Country, emp.WorkCountry)
                    );

            return query;
        }

        /// <summary>
        /// Returns an absence reason by customerId and employerId
        /// </summary>
        /// <param name="absenceReasonCode"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public AbsenceReason AbsenceReasonGetByCode(string absenceReasonCode, string customerId = null, string employerId = null)
        {
            return AbsenceReason.GetByCode(absenceReasonCode, customerId, employerId);
        }

        /// <summary>
        /// Returns an absence reason by its id
        /// </summary>
        /// <param name="absenceReasonId"></param>
        /// <returns></returns>
        public AbsenceReason AbsenceReasonGetById(string absenceReasonId)
        {
            return AbsenceReason.GetById(absenceReasonId);
        }

        /// <summary>
        /// Creates a new case after performing case validation. Method does NOT perform any persistence.
        /// </summary>
        /// <param name="employeeId">The employee to create the case for</param>
        /// <param name="startDate">The start date of the case's initial segment</param>
        /// <param name="endDate">The anticipated end date of the case's initial segment</param>
        /// <param name="type">The case type for the intitial segment</param>
        /// <param name="reasonCode">The selected reason for the leave of absence</param>
        /// <param name="reducedSchedule">The reduced schedule for reduced case types</param>
        /// <param name="description">An optional description of the case for both internal/external use</param>
        /// <param name="narrative">An optional narrative description to set for the case</param>
        /// <returns>The newly created case</returns>
        /// <exception cref="AbsenceSoft.AbsenceSoftException">
        /// The end date may not come before the start date.
        /// User does not have access to create a case for an employee with this employer.
        /// Invalid reason for leave.
        /// Leave reason is not valid for this employer.
        /// There is already an open or completed case for this reason during some or all of these dates.
        /// </exception>
        public Case CreateCase(CaseStatus status, string employeeId, DateTime startDate, DateTime? endDate, CaseType type, string reasonCode, Schedule reducedSchedule = null, string description = null, string narrative = null, string caseId = null)
        {
            using (new InstrumentationContext("CaseService.CreateCase"))
            {
                /*
                 * 1. Get the employer and customer ids from the employee id passed in
                 * 2. Validate the user has access to create a case for this employer/customer
                 * 3. Validate the combination of reason and type are valid
                 * 4. Return the case entity
                 */
                if (endDate.HasValue && (endDate < startDate))
                    throw new AbsenceSoftException("The end date may not come before the start date");

                var emp = Employee.GetById(employeeId);
                if (User.Current != null && !User.Current.HasEmployerAccess(emp.EmployerId, Permission.CreateCase))
                    throw new AbsenceSoftException("User does not have access to create a case for an employee with this employer");

                EmployerId = EmployerId ?? emp.EmployerId;
                CustomerId = CustomerId ?? emp.CustomerId;

                var reason = AbsenceReason.GetByCode(reasonCode, CustomerId, EmployerId);
                if (status != CaseStatus.Inquiry)
                {
                    if (reason == null)
                        throw new AbsenceSoftException("Invalid reason for leave");
                    if (!string.IsNullOrWhiteSpace(reason.EmployerId) && reason.EmployerId != emp.EmployerId)
                        throw new AbsenceSoftException("Leave reason is not valid for this employer");

                }

                // If we have a caseId, then that means the status was a saved inquiry.
                Case myCase = null;
                if (!String.IsNullOrEmpty(caseId))
                {
                    myCase = Case.GetById(caseId);
                    myCase.Status = status;
                    myCase.Employee = emp;
                    myCase.EmployerId = emp.EmployerId;
                    myCase.CustomerId = emp.CustomerId;
                    myCase.Reason = reason;
                    myCase.Description = description;
                    myCase.Narrative = narrative;
                    myCase.StartDate = startDate.ToMidnight();
                    myCase.EndDate = endDate.HasValue ? endDate.ToMidnight() : null;
                    myCase.AssignedTo = User.Current ?? CurrentUser;
                    myCase.Segments.Clear();
                    myCase.Segments.Add(new CaseSegment()
                    {
                        StartDate = startDate.ToMidnight(),
                        EndDate = endDate.HasValue ? endDate.ToMidnight() : null,
                        Type = type,
                        Status = status,
                    });
                }
                else
                {
                    myCase = new Case()
                    {
                        Status = status,
                        Employee = emp,
                        EmployerId = emp.EmployerId,
                        CustomerId = emp.CustomerId,
                        Reason = reason,
                        Description = description,
                        Narrative = narrative,
                        StartDate = startDate.ToMidnight(),
                        EndDate = endDate.HasValue ? endDate.ToMidnight() : null,
                        AssignedTo = User.Current ?? CurrentUser,
                        Segments = new List<CaseSegment>()
                    {
                        new CaseSegment()
                        {
                            StartDate = startDate.ToMidnight(),
                            EndDate = endDate.HasValue ? endDate.ToMidnight() : null,
                            Type = type,
                            Status = status,
                        }
                    },
                        Pay = new PayInfo()
                        {
                            PaySchedule = emp.PaySchedule
                        }
                    };
                }

                DateTime? ed = endDate.HasValue ? endDate.ToMidnight() : endDate;
                if (myCase.Status != CaseStatus.Inquiry)
                {
                    ValidateCasesNotOverlapping(myCase);
                }

                if (reducedSchedule != null && type == CaseType.Reduced)
                {
                    DateTime date = reducedSchedule.StartDate != null ? reducedSchedule.StartDate : DateTime.Now.ToMidnight();
                    date = date.GetFirstDayOfWeek();
                    reducedSchedule.Times.ForEach(x => { x.SampleDate = date; date = date.AddDays(1); });
                    myCase.Segments.ForEach(s => { if (s.Type == CaseType.Reduced) { s.LeaveSchedule = new List<Schedule> { reducedSchedule }; } });
                }

                return myCase;
            }
        }

        public Case GetCaseById(string caseId)
        {
            return Case.GetById(caseId);
        }

        public Case GetCaseByCaseNumber(string caseNumber)
        {
            return Case.AsQueryable().Where(x => x.CaseNumber == caseNumber).FirstOrDefault();
        }

        /// <summary>
        /// Saves a case to the database after performing basic validation. Returns the saved instance of the case.
        /// Works for both new and existing cases.
        /// </summary>
        /// <param name="myCase">The case to update in the database.</param>
        /// <param name="evt">The evt.</param>
        /// <returns>
        /// The saved case.
        /// </returns>
        /// <exception cref="AbsenceSoftException">
        /// The end date may not come before the start date
        /// or
        /// Invalid Employee for leave or Employee missing
        /// or
        /// User does not have access to create a case for an employee with this employer
        /// or
        /// User does not have access to update a case for an employee with this employer
        /// or
        /// Invalid reason for leave
        /// or
        /// Leave reason is not valid for this employer
        /// or
        /// Case End Date is required when not Information Only
        /// </exception>
        public Case UpdateCase(Case myCase, CaseEventType? evt = null)
        {
            using (new InstrumentationContext("CaseService.UpdateCase"))
            {
                bool isNew = string.IsNullOrWhiteSpace(myCase.Id);
                bool isCreating = isNew || (evt.HasValue && evt.Value == CaseEventType.CaseCreated);

                var oldCase = Case.GetById(myCase.Id);

                /*
                 * 1. Get the employer and customer ids from the employee id passed in
                 * 2. Validate the user has access to create a case for this employer/customer
                 * 3. Validate the combination of reason and type are valid
                 * 4. Save the case
                 * 5. Return the saved case
                 */
                if (myCase.EndDate.HasValue && myCase.EndDate < myCase.StartDate)
                    throw new AbsenceSoftException("The end date may not come before the start date");

                if (myCase.Employee == null || string.IsNullOrWhiteSpace(myCase.Employee.Id))
                    throw new AbsenceSoftException("Invalid Employee for leave or Employee missing");

                if (User.Current != null && isNew && !User.Current.HasEmployerAccess(myCase.Employee.EmployerId, Permission.CreateCase))
                    throw new AbsenceSoftException("User does not have access to create a case for an employee with this employer");

                if (User.Current != null && !isNew && !User.Current.HasEmployerAccess(myCase.Employee.EmployerId, Permission.EditCase))
                    throw new AbsenceSoftException("User does not have access to update a case for an employee with this employer");

                if (myCase.Status != CaseStatus.Inquiry && myCase.Status != CaseStatus.Cancelled)
                {
                    var reason = myCase.Reason;
                    if (reason == null)
                        throw new AbsenceSoftException("Invalid reason for leave");
                    if (!string.IsNullOrWhiteSpace(reason.EmployerId) && reason.EmployerId != myCase.Employee.EmployerId)
                        throw new AbsenceSoftException("Leave reason is not valid for this employer");

                }

                if (myCase.Segments.Any(s => s.Type != CaseType.Administrative) && myCase.EndDate == null)
                    throw new AbsenceSoftException("Case End Date is required when not Information Only");

                if (myCase.Status != CaseStatus.Cancelled && myCase.Status != CaseStatus.Inquiry)
                    ValidateCasesNotOverlapping(myCase);

                // Check if somehow the assigned to name got wiped (not sure how this could happen, but hey, stranger things right?!)
                if (myCase.AssignedToId == null && oldCase != null && oldCase.AssignedToId != null)
                {
                    myCase.AssignedToId = oldCase.AssignedToId;
                    myCase.AssignedToName = oldCase.AssignedToName;
                }

                // Ensure assigned to
                if (CurrentUser != null && (myCase.AssignedToId == null || myCase.AssignedToId == CurrentUser.Id) && (myCase.IsNew || evt == CaseEventType.CaseCreated))
                {
                    if (!User.IsEmployeeSelfServicePortal && myCase.Employer.Metadata.GetRawValue<bool>("AssignCasesToSelf"))
                    {
                        // HACK: Parrot, AT-464, this will be removed anyway when we do Team Management.
                        myCase.AssignedToId = CurrentUser.Id;
                        myCase.AssignedToName = CurrentUser.DisplayName;
                    }
                    else
                    {
                        // First, run the default auto assignment rules, if any exist
                        EvaluateCaseAssignmentRules(myCase, null, CurrentUser, false);

                        // Still no assigned to user? Then lets 
                        if (myCase.AssignedToId == null)
                        {
                            if (!User.IsEmployeeSelfServicePortal && myCase.Employer.Metadata.GetRawValue<bool>("AssignCasesToSelf"))
                            {
                                // HACK: Parrot, AT-464, this will be removed anyway when we do Team Management.
                                myCase.AssignedToId = CurrentUser.Id;
                                myCase.AssignedToName = CurrentUser.DisplayName;
                            }
                            else
                            {
                                List<User> assignees = new List<User>();
                                // The rotary rule will automatically do round-robin and fallback to "self", but we'll do self as
                                //  an ultimate backup again anyway.
                                new RotaryRule().ApplyCaseAssignmentRules(ref assignees, myCase, null);
                                var match = assignees.FirstOrDefault() ?? CurrentUser;
                                myCase.AssignedTo = match;
                                myCase.AssignedToId = match.Id;
                                myCase.AssignedToName = match.DisplayName;
                            }
                        }
                    }
                }

                // Set the estimated return to work date based on case end date.
                if (isCreating && myCase.EndDate.HasValue)
                {
                    var times = new LeaveOfAbsence() { Case = myCase, Employee = myCase.Employee, Employer = myCase.Employer, Customer = myCase.Customer }
                        .MaterializeSchedule(myCase.EndDate.Value.AddDays(1), myCase.EndDate.Value.AddDays(60), true);
                    DateTime rtwDate = myCase.EndDate.Value;
                    if (times != null)
                    {
                        foreach (var t in times.OrderBy(d => d.SampleDate))
                            if (t.TotalMinutes.HasValue && t.TotalMinutes.Value > 0)
                            {
                                rtwDate = t.SampleDate;
                                break;
                            }
                    }
                    myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, rtwDate);
                }
                if (isCreating)
                {
                    if (!myCase.CaseEvents.Any(e => e.EventType == CaseEventType.CaseCreated))
                    {
                        myCase.SetCaseEvent(CaseEventType.CaseCreated, DateTime.UtcNow);
                    }
                }

                else if (evt.HasValue)
                    myCase.SetCaseEvent(evt.Value, DateTime.UtcNow);

                if (isNew && myCase.Status != CaseStatus.Inquiry)
                {
                    Case duplicateCase = DuplicateCase(myCase);
                    if (duplicateCase != null)
                        return duplicateCase;
                }

                myCase.Save();

                if (isCreating)
                {
                    // Record the primary case assignee
                    new CaseAssignee()
                    {
                        CaseId = myCase.Id,
                        CustomerId = myCase.CustomerId,
                        Code = null,
                        Status = myCase.Status,
                        UserId = myCase.AssignedToId
                    }.Save();

                    // Record the case reporter
                    if (myCase.CaseReporter != null)
                    {
                        new CaseReporter()
                        {
                            CaseId = myCase.Id,
                            CustomerId = myCase.CustomerId,
                            EmployerId = myCase.EmployerId,
                            Reporter = myCase.CaseReporter
                        }.Save();
                    }
                }

                if (RunWorkflow && isCreating)
                {
                    if (myCase.Status == CaseStatus.Inquiry)
                        myCase.WfOnInquiryCreated();
                    else
                    {
                        myCase.WfOnCaseCreated();
                        myCase = GetCaseById(myCase.Id);

                        if (myCase.IsAccommodation && myCase.AccommodationRequest != null && myCase.AccommodationRequest.Accommodations != null && myCase.AccommodationRequest.Accommodations.Any())
                            myCase.AccommodationRequest.Accommodations.ForEach(a => myCase.WfOnAccommodationCreated(a));
                        myCase = GetCaseById(myCase.Id);
                    }

                    new ToDoService().Using(t => t.CaseCreated(myCase, CurrentUser ?? User.System));
                }

                return myCase;
            }
        }

        /// <summary>
        /// Finds the first duplicate case create in the last three minutes and returns it, if any found.
        /// Logs an error message for each duplicate case found
        /// </summary>
        /// <param name="myCase"></param>
        /// <returns></returns>
        public Case DuplicateCase(Case myCase)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(Case.Query.EQ(c => c.CustomerId, myCase.CustomerId));
            ands.Add(Case.Query.EQ(c => c.EmployerId, myCase.EmployerId));
            ands.Add(Case.Query.EQ(c => c.Employee.Id, myCase.Employee.Id));
            ands.Add(Case.Query.EQ(c => c.StartDate, myCase.StartDate));
            ands.Add(Case.Query.EQ(c => c.EndDate, myCase.EndDate));
            ands.Add(Case.Query.EQ(c => c.Reason.Code, myCase.Reason.Code));
            ands.Add(Case.Query.EQ(c => c.Status, myCase.Status));
            if (myCase.Segments != null && myCase.Segments.Any())
                ands.Add(Case.Query.EQ(c => c.Segments[0].Type, myCase.Segments[0].Type));
            if (!string.IsNullOrWhiteSpace(myCase.Description))
                ands.Add(Case.Query.EQ(c => c.Description, myCase.Description));
            if (!string.IsNullOrWhiteSpace(myCase.Narrative))
                ands.Add(Case.Query.EQ(c => c.Narrative, myCase.Narrative));

            if ((CurrentUser ?? User.Current) != null)
            {
                ands.Add(Case.Query.EQ(c => c.CreatedById, (CurrentUser ?? User.Current).Id));
                ands.Add(Case.Query.EQ(c => c.ModifiedById, (CurrentUser ?? User.Current).Id));
            }
            // Filter by a 3 minute window.
            ands.Add(Case.Query.GTE(c => c.CreatedDate, DateTime.UtcNow.AddMinutes(-3)));

            var dupCases = Case.Query.Find(Case.Query.And(ands)).ToList();
            if (dupCases.Any())
            {
                foreach (var dup in dupCases)
                {
                    string errorMessage = string.Format("Potential duplicate case would be created; Appears to be a copy of case # '{0}'", dup.CaseNumber);
                    Log.Warn(errorMessage);
                }

                return dupCases.First();
            }

            return null;
        }

        /// <summary>
        /// Applies a determination to one or more segments on a case based on a policy (optional) and start and end 
        /// dates along with a status and possible denial reason for that status.
        /// </summary>
        /// <param name="myCase">The case to adjudicate by applying the determination across one or more days.</param>
        /// <param name="policyCode">The optional code of the policy to adjudicate determination for. If no code is 
        /// provided, assumes determination across ALL policies.</param>
        /// <param name="startDate">The start date for the determination.</param>
        /// <param name="endDate">The end date for the determination. This may be the same as the start date.</param>
        /// <param name="status">The adjudication status to set for the determination.</param>
        /// <param name="reasonCode">The denial reason code if applicable. This is required if status = <c>Denied</c>.</param>
        /// <param name="explanation">The denial reason explanation when Other is selected if applicable. This is required if status = <c>Denied</c>.</param>
        /// <param name="reasonName">The denial reason name if applicable. This is required if status = <c>Denied</c>.</param>
        /// <returns>The modified case with the applied determination.</returns>
        public Case ApplyDetermination(Case myCase, string policyCode, DateTime startDate, DateTime endDate, AdjudicationStatus status, string reasonCode = null, string explanation = null, string reasonName = null)
        {
            // date times kinda suck. .net / JS / Mongo all want to fix the timezone even whenn we set it
            // or don't want it fixed because it's not broken. So, just force the start and end dates back to midnight
            startDate = startDate.ToMidnight();
            endDate = endDate.ToMidnight();
            reasonCode = string.IsNullOrWhiteSpace(reasonCode) ? reasonCode : reasonCode.ToUpperInvariant();

            if (myCase == null)
                throw new ArgumentNullException("myCase");
            if (endDate < startDate)
                throw new AbsenceSoftException("End Date occurrs before Start Date, did you intend to reverse them?");
            if (status == AdjudicationStatus.Denied && string.IsNullOrWhiteSpace(reasonCode))
                throw new AbsenceSoftException("Denial Reason must be specified if the approval decision is Denied");
            if (reasonCode == AdjudicationDenialReason.Other && string.IsNullOrWhiteSpace(explanation))
                throw new AbsenceSoftException("Denial Reason other must be specified if the approval decision is Denied and the Reason 'Other' is selected");
            if (User.Current != null && !User.Current.HasEmployerAccess(myCase.EmployerId, Permission.AdjudicateCase))
                throw new AbsenceSoftException("User does not have access to make an approval decision for a case with this employer");

            // Ensure if this is going to be denied, we don't have any approved intermittent time during that period, otherwise, watch out buster
            if (status == AdjudicationStatus.Denied)
            {
                var offendingUsage = myCase.Segments.Where(s => s.Type == CaseType.Intermittent)
                    .SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                    .SelectMany(p => p.Usage.Where(u => u.DateUsed >= startDate && u.DateUsed <= endDate && u.Determination == AdjudicationStatus.Approved && u.MinutesUsed > 0))
                    .ToList();
                if (offendingUsage.Any())
                    throw new AbsenceSoftException(string.Format("You may not deny '{0}' for dates which have approved intermittent absences. Please change these absences first for the following dates: {1}",
                        myCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode)).First().Policy.Name,
                        string.Join(", ", offendingUsage.Select(u => u.DateUsed.ToUIString()))));

                var deniedWorkDays = myCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                        .SelectMany(p => p.Usage).Count(u => u.DateUsed >= startDate && u.DateUsed <= endDate && u.MinutesInDay > 0);

                if (deniedWorkDays > 0)
                {
                    myCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                        .SelectMany(p => p.Usage).Where(u => u.DateUsed < startDate || u.DateUsed > endDate)
                        .Where(u => !(u.IsLocked ?? false) && u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted)
                        .OrderBy(u => u.DateUsed).ForEach(u =>
                        {
                            if (u.MinutesInDay > 0)
                            {
                                deniedWorkDays -= 1;
                            }
                            if (deniedWorkDays >= 0)
                            {
                                u.Determination = AdjudicationStatus.Pending;
                                u.DenialReasonCode = null;
                                u.DenialExplanation = "";
                                u.UserEntered = true;
                            }
                        });
                }
            }

            // do the update skipping any ones that are locked
            myCase.Segments.Where(s => s.Type != CaseType.Intermittent).SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                .SelectMany(p => p.Usage.Where(u => u.DateUsed >= startDate && u.DateUsed <= endDate))
                .Where(u => !u.IsLocked.HasValue || !u.IsLocked.Value)
                .ForEach(u =>
                {
                    u.Determination = status;
                    u.DenialReasonCode = reasonCode;
                    u.DenialReasonName = reasonName;
                    u.DenialExplanation = explanation;
                    u.UserEntered = true;
                });

            // fix up the status, 
            AdjudicationStatus forIntStatus = status;
 

            // do the update skipping any ones that are locked
            myCase.Segments.Where(s => s.Type == CaseType.Intermittent).SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                .SelectMany(p => p.Usage.Where(u => u.DateUsed >= startDate && u.DateUsed <= endDate))
                .Where(u => !u.IsLocked.HasValue || !u.IsLocked.Value)
                .ForEach(u =>
                {
                    u.Determination = forIntStatus;
                    u.DenialReasonCode = reasonCode;
                    u.DenialReasonName = reasonName;
                    u.DenialExplanation = explanation;
                    u.UserEntered = true;
                    u.IntermittentDetermination = status.ToIntermittentStatus();
                });

            this.GetProjectedUsage(myCase);


            myCase.Save();

            if (RunWorkflow)
                myCase.Segments
                    .SelectMany(s => s.AppliedPolicies.Where(p => string.IsNullOrWhiteSpace(policyCode) || p.Policy.Code == policyCode))
                    .Distinct(p => p.Policy.Code)
                    .ForEach(p => p.WfOnPolicyAdjudicated(myCase, status, reasonCode, explanation, reasonName, null, startDate, endDate));



            return myCase;
        }
        /// <summary>
        /// Update the casenote
        /// </summary>
        public void UpdateTimeOfRequestNoteToRemovedRequest(AbsenceSoft.Data.Cases.Case existingCase, List<IntermittentTimeRequest> intermittentTimeRequests)
        {
            foreach (var r in intermittentTimeRequests)
            {
                IQueryable<CaseNote> noteQuery =
                     from n in CaseNote.AsQueryable()
                     where Query.EQ("RequestDate", new BsonDateTime(r.RequestDate)).Inject() && n.CaseId == existingCase.Id && n.IsItorRemoved == false
                     select n;


                if (noteQuery != null && noteQuery.Any())
                {
                    string Approved = string.Empty;
                    string Denied = string.Empty;
                    string Pending = string.Empty;
                    string Statement = string.Empty;

                    List<string> myCollection = new List<string>();

                    Approved = r.Detail?.FirstOrDefault().Approved > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Approved)).ToString(@"hh\:mm")) + " hours Approved " : "";
                    Pending = r.Detail?.FirstOrDefault().Pending > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Pending)).ToString(@"hh\:mm")) + " hours Pending " : "";
                    Denied = r.Detail?.FirstOrDefault().Denied > 0 ? "" + ((TimeSpan.FromMinutes((double)r.Detail?.FirstOrDefault().Denied)).ToString(@"hh\:mm")) + " hours Denied " : "";

                    if (Approved != "")
                    {
                        myCollection.Add(Approved);
                    }
                    if (Pending != "")
                    {
                        myCollection.Add(Pending);
                    }
                    if (Denied != "")
                    {
                        myCollection.Add(Denied);
                    }

                    Statement = String.Join(", ", myCollection);
                     
                   string RemovedRequest=$"Intermittent Time Off Request:\n Removed Request\n {r.IntermittentType} \n" +
                                   $" {r.RequestDate.ToShortDateString()} \n { r?.TotalTime} ({ Statement})\n " +
                                   $"{r.StartTimeForLeave.ToString()} - {r.EndTimeForLeave.ToString()} \n {r?.Notes} ";
                   

                    CaseNote prevnote = noteQuery.FirstOrDefault();
                    CaseNote note = noteQuery.FirstOrDefault() ?? new CaseNote()
                    {
                        Category = NoteCategoryEnum.TimeOffRequest,
                        Categories = prevnote.Categories,
                        CreatedBy = prevnote.CreatedBy,
                        CustomerId = prevnote.CustomerId,
                        EmployerId = prevnote.EmployerId,
                        Public = false,
                        Case = existingCase,
                        IsItorRemoved = true
                    };
                    note.ModifiedBy = prevnote.ModifiedBy;

                    string[] notearray = !string.IsNullOrEmpty(note.Notes) ? note.Notes.Split(new[] { "Intermittent" }, StringSplitOptions.None) : null;
                    note.IsItorRemoved = notearray != null && notearray.Length == 1 ? true : false;
                    string notes = $"{note.Notes}\n" +
                                  $" \n" +
                                  $"{RemovedRequest}";
                    note.Notes = notes;                                                                             
                    note.Save();                   
                }
            }
        }

        /// <summary>
        /// The ingore overlapping cases
        /// </summary>
        private static bool IgnoreOverlappingCases = false;
        /// <summary>
        /// Sets the ignore overlapping cases.
        /// </summary>
        /// <param name="ignore">if set to <c>true</c> [ignore].</param>
        public static void SetIgnoreOverlappingCases(bool ignore = true)
        {
            IgnoreOverlappingCases = ignore;
        }

        protected virtual void ValidateCasesNotOverlapping(Case myCase)
        {
            // We should ignore any overlapping cases. Just because we're special.
            if (IgnoreOverlappingCases)
                return;

            // Only care if it's consecutive to ensure there no other consecutive cases to overlap
            if (myCase.Segments != null && myCase.Segments.Any(s => s.Type == CaseType.Consecutive))
            {
                var cases = Case.AsQueryable().Where(c => c.Employee.Id == myCase.Employee.Id && c.Id != myCase.Id).ToList();
                List<CaseSegment> consecutiveSegments = cases
                    .Where(c => c.GetCaseStatus() != CaseStatus.Cancelled)
                    .SelectMany(c => c.Segments.Where(cs => cs.Type == CaseType.Consecutive)).ToList();

                foreach (var myCaseConsecutiveSegment in myCase.Segments.Where(cs => cs.Type == CaseType.Consecutive))
                {
                    DateTime? myCaseConsecutiveEndDate = myCaseConsecutiveSegment.EndDate.HasValue ? myCaseConsecutiveSegment.EndDate.ToMidnight() : null;
                    foreach (var segment in consecutiveSegments)
                    {
                        DateTime? endDate = segment.EndDate.HasValue ? segment.EndDate.ToMidnight() : null;
                        if (myCaseConsecutiveSegment.StartDate.DateRangesOverLap(myCaseConsecutiveEndDate, segment.StartDate, endDate))
                        {
                            var matchingCase = cases.FirstOrDefault(c => c.Segments.Any(cs => cs.Id == segment.Id));
                            throw new AbsenceSoftException(string.Format("There is already an open or completed case during some or all of these dates. Please modify Case '{0}'.", matchingCase.CaseNumber));
                        }
                    }
                }
            }
#warning TODO: At some point, we need to do some fancy reduced schedule validation to ensure work schedules aren't overblown due to stacked reduced schedule absences.
        }

        public ListResults CaseList(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.CaseList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("CaseNumber");
                string reasonId = criteria.Get<string>("ReasonId");
                string empName = criteria.Get<string>("EmployeeName");
                string reason = criteria.Get<string>("Reason");
                long? updated = criteria.Get<long?>("Updated");
                string employeeId = criteria.Get<String>("EmployeeId");
                long? statusFilter = criteria.Get<long?>("Status");
                long? typeFilter = criteria.Get<long?>("Type");
                bool? includeApproved = criteria.Get<bool?>("Approved");
                bool? includePending = criteria.Get<bool?>("Pending");
                bool? includeDenied = criteria.Get<bool?>("Denied");
                string assignedToName = criteria.Get<string>("AssignedToName");
                string listTypeFilterText = criteria.Get<string>("ListType");
                VisibilityListType listTypeFilter;
                if (string.IsNullOrWhiteSpace(listTypeFilterText) || !Enum.TryParse<VisibilityListType>(listTypeFilterText, out listTypeFilter))
                    listTypeFilter = VisibilityListType.Individual;
                if (listTypeFilter == VisibilityListType.Team && !User.Permissions.ProjectedPermissions.Contains(Permission.ViewTeam.Id))
                    listTypeFilter = VisibilityListType.Individual;

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (User.Current != null)
                    ands.Add(User.Current.BuildDataAccessFilters(Permission.ViewCase, "Employee._id"));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");

                //Check if user is limited to one employer
                if (string.IsNullOrWhiteSpace(employerId) && Employer.Current != null)
                    employerId = Employer.Current.Id;

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));

                string lName = null;
                string fName = null;
                if (!string.IsNullOrWhiteSpace(empName))
                {

                    if (empName.Contains(','))
                    {
                        string[] arrName = empName.Split(',');
                        if (arrName.Length > 0)
                        {

                            lName = arrName[0].Trim();
                            fName = arrName[1].Trim();
                        }
                    }
                    else
                    {
                        var safeName = Regex.Escape(empName);

                        ands.Add(
                            Case.Query.Or(
                                Case.Query.Matches(e => e.Employee.LastName, new BsonRegularExpression("^" + empName, "i")),
                                Case.Query.Matches(e => e.Employee.FirstName, new BsonRegularExpression("^" + empName, "i")),
                                Case.Query.Matches(e => e.Employee.MiddleName, new BsonRegularExpression("^" + empName, "i"))
                        ));
                    }
                }
                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                Case.Query.MatchesString(c => c.Employee.LastName, lName, ands);
                Case.Query.MatchesString(c => c.Employee.FirstName, fName, ands);
                Case.Query.MatchesString(c => c.CaseNumber, caseId, ands);
                if (!string.IsNullOrWhiteSpace(reasonId))
                    ands.Add(Case.Query.EQ(c => c.Reason.Id, reasonId));
                if (!string.IsNullOrWhiteSpace(employeeId))
                    ands.Add(Case.Query.EQ(c => c.Employee.Id, employeeId));

                if (statusFilter.HasValue && statusFilter.Value >= 0)  //&& statusFilter != 0
                {
                    CaseStatus status = ((CaseStatus)statusFilter);
                    ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Status, status)));
                }
                else if (statusFilter == -2L)
                {
                    ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.In(s => s.Status, new CaseStatus[] { CaseStatus.Open, CaseStatus.Requested })));
                }

                if (includeApproved.HasValue || includePending.HasValue || includeDenied.HasValue)
                {
                    List<IMongoQuery> adjudicationOrs = new List<IMongoQuery>();
                    List<IMongoQuery> adjudicationAnds = new List<IMongoQuery>();
                    CreateAdjudicationQuery(includeApproved, AdjudicationStatus.Approved, adjudicationOrs, adjudicationAnds);
                    CreateAdjudicationQuery(includePending, AdjudicationStatus.Pending, adjudicationOrs, adjudicationAnds);
                    CreateAdjudicationQuery(includeDenied, AdjudicationStatus.Denied, adjudicationOrs, adjudicationAnds);

                    if (adjudicationOrs.Count > 0)
                        ands.Add(Case.Query.Or(adjudicationOrs));

                    if (adjudicationAnds.Count > 0)
                        ands.Add(Case.Query.And(adjudicationAnds));
                }

                if (typeFilter.HasValue && typeFilter.Value >= 0)
                {
                    CaseType type = ((CaseType)typeFilter);
                    ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Type, type)));
                }

                if (!string.IsNullOrWhiteSpace(reason))
                {
                    ands.Add(
                         Case.Query.Or(
                            Case.Query.Matches(e => e.Reason.Name, new BsonRegularExpression("^" + reason, "i")),
                            Case.Query.ElemMatch(c => c.AccommodationRequest.Accommodations, q => q.Matches(s => s.Type.Code, new BsonRegularExpression("^" + reason, "i")))
                        ));
                }
                if (updated.HasValue)
                {
                    ands.Add(Case.Query.GTE(e => e.ModifiedDate, new BsonDateTime(updated.Value)));
                    // Window is 24 hours
                    ands.Add(Case.Query.LT(e => e.ModifiedDate, new BsonDateTime(updated.Value + 86400000)));
                }

                if (!User.IsEmployeeSelfServicePortal)
                {
                    // Check the user visibility for the user's team or just themselves based on permissions or filter selection and/or both
                    if (listTypeFilter == VisibilityListType.Team)
                    {
                        List<string> myTeamsUserIds = new TeamService(User.Current).GetTeamMemberIdsForCurrentUser();
                        ands.Add(Case.Query.In(e => e.AssignedToId, myTeamsUserIds));
                    }
                    else if (listTypeFilter == VisibilityListType.Individual)
                    {
                        ands.Add(Case.Query.EQ(e => e.AssignedToId, (User.Current ?? new User()).Id ?? "".PadLeft(24, '9')));
                    }
                }

                if(!string.IsNullOrWhiteSpace(assignedToName))
                {
                    ands.Add(Case.Query.Matches(e => e.AssignedToName, new BsonRegularExpression("^" + assignedToName, "i")));
                }
                // Otherwise, in ESS limit visibility to user's subordinates, which is already being done by AsQueryable
                //  so there's nothing else to do here.


                //filter the case against accommodation status.
                //If closed filter it.
                var accmStatus = criteria.Get<CaseStatus?>("AccommodationStatus");
                if (accmStatus != null)
                {
                    ands.Add(
                        Case.Query.Or(
                            Case.Query.EQ(p => p.IsAccommodation, false),
                            Case.Query.And(
                                Case.Query.EQ(p => p.IsAccommodation, true),
                                Case.Query.EQ(p => p.AccommodationRequest.Status, accmStatus)
                        )));
                }


                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize)
                    .SetFields(Case.Query.IncludeFields(
                        // outside of the required fields, this is the only additional one we need
                        e => e.Reason,
                        e => e.Segments,
                        e => e.AssignedToName,
                        e => e.EmployerId,
                        e => e.EmployerName,
                        e => e.AccommodationRequest,
                        e => e.IsAccommodation,
                        e => e.CaseType,
                        e => e.Summary,
                        e => e.Summary.Determination,
                        e => e.Description,
                        e => e.Narrative,
                        e => e.EndDate,
                        e => e.CaseEvents
                    ));

                // Build sort by (try to use our index as much as possible)
                var sortBy = SortBy.Ascending("Employee.LastName", "Employee.EmployeeNumber");

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    string sortByCheck = criteria.SortBy.ToLowerInvariant();
                    if (sortByCheck == "employeename") // EmployeeName is a pseudo field, so we have to handle it here all special like.
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("Employee.LastName")
                            : SortBy.Descending("Employee.LastName"));
                    else if (sortByCheck == "reason" || sortByCheck == "reasonid")
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("Reason.Name")
                            : SortBy.Descending("Reason.Name"));
                    else if (sortByCheck == "status")
                        //HACK: Not sure what should happen here, since in reality this should be a MapReduce function, but wow, what a pain
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("Status")
                            : SortBy.Descending("Status"));
                    else if (sortByCheck == "updated" || sortByCheck == "modifieddate")
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("mdt")
                            : SortBy.Descending("mdt"));
                    else if (sortByCheck == "assignedToName")
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("AssignedToName")
                            : SortBy.Descending("AssignedToName"));
                    else // Normal sort
                        sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending(criteria.SortBy)
                            : SortBy.Descending(criteria.SortBy));
                }

                query = query.SetSortOrder(sortBy);

                result.Total = query.Count();
                result.Results = query.ToList().Select(c => new ListResult()
                    .Set("Id", c.Id)
                    .Set("CaseNumber", c.CaseNumber)
                    .Set("EmployeeFirstName", c.Employee.FirstName)
                    .Set("EmployeeLastName", c.Employee.LastName)
                    .Set("EmployeeId", c.Employee.Id)
                    .Set("EmployeeName", string.Concat(c.Employee.LastName, ", ", c.Employee.FirstName, " ", c.Employee.MiddleName))
                    .Set("EmployerId", c.EmployerId)
                    .Set("EmployerName", c.EmployerName)
                    .Set("ReasonId", c.Reason != null ? c.Reason.Id : null)
                    .Set("ReasonName", c.Reason != null ? c.Reason.Name : "Unknown")
                    .Set("StartDate", c.StartDate)
                    .Set("EndDate", c.EndDate)
                    .Set("Status", c.GetCaseStatus().ToString())
                    .Set("Type", c.CaseType.ToString())
                    .Set("ModifiedDate", c.ModifiedDate)
                    .Set("ModifiedDateText", c.ModifiedDate.ToUIString())
                    .Set("AssignedToName", c.AssignedToName)
                    .Set("IsAccommodation", c.IsAccommodation)
                    .Set("Determination", c.Summary != null ? c.Summary.Determination : AdjudicationStatus.Pending)
                    .Set("AccommodationDetermination", c.AccommodationRequest != null ? c.AccommodationRequest.Determination : AdjudicationStatus.Pending)
                    .Set("AccommodationRequest", this.AccommodationCategory(c.AccommodationRequest, c.IsAccommodation))
                    .Set("Summary", c.Narrative)
                    .Set("Description", c.Description)
                    .Set("ReturnToWorkDate", c.FindCaseEvent(CaseEventType.EstimatedReturnToWork) != null ? c.FindCaseEvent(CaseEventType.EstimatedReturnToWork).EventDate : (DateTime?)null)
                );

                return result;
            }
        }

        private void CreateAdjudicationQuery(bool? include, AdjudicationStatus adjudicationStatus, List<IMongoQuery> adjudicationOrs, List<IMongoQuery> adjudicationAnds)
        {
            if (!include.HasValue)
                return;

            if (include.Value)
                adjudicationOrs.Add(Case.Query.Or(
                Case.Query.And(Case.Query.EQ(c => c.IsAccommodation, false), Case.Query.EQ(c => c.Summary.Determination, adjudicationStatus)),
                Case.Query.And(Case.Query.EQ(c => c.IsAccommodation, true), Case.Query.EQ(c => c.AccommodationRequest.Determination, adjudicationStatus))
            ));
            else
                adjudicationAnds.Add(Case.Query.Or(
                Case.Query.And(Case.Query.EQ(c => c.IsAccommodation, false), Case.Query.NE(c => c.Summary.Determination, adjudicationStatus)),
                Case.Query.And(Case.Query.EQ(c => c.IsAccommodation, true), Case.Query.NE(c => c.AccommodationRequest.Determination, adjudicationStatus))
            ));
        }

        public ListResults CaseLastViewedList(ListCriteria criteria)
        {
            using (new InstrumentationContext("CaseService.CaseLastViewedList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (User.Current != null)
                    ands.Add(ItemViewHistory.Query.EQ(i => i.CreatedById, User.Current.Id));
                ands.Add(ItemViewHistory.Query.EQ(i => i.ItemType, "Case"));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(ItemViewHistory.Query.EQ(e => e.EmployerId, employerId));

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = ItemViewHistory.Query.Find(ands.Count > 0 ? ItemViewHistory.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                query = query.SetSortOrder(SortBy<ItemViewHistory>.Descending(i => i.CreatedDate));
                List<ItemViewHistory> ivh = query.ToList();
                var ids = ivh.Select(r => r.ItemId).Distinct().OrderBy(p => ivh.IndexOf(ivh.First(i => i.ItemId == p))).ToList();

                var cases = Case.Query.Find(Case.Query.In(q => q.Id, ids));

                result.Total = cases.Count();

                if (Customer.Current.HasFeature(Feature.ADA) && !Customer.Current.HasFeature(Feature.LOA))
                {
                    result.Results = ids.Where(i => cases.Any(c => c.Id == i)).Select(i =>
                    {
                        var c = cases.FirstOrDefault(a => a.Id == i);

                        return new ListResult()
                            .Set("Id", c.Id)
                            .Set("CaseNumber", c.CaseNumber)
                            .Set("EmployeeId", c.Employee.Id)
                            .Set("EmployeeFirstName", c.Employee.FirstName)
                            .Set("EmployeeLastName", c.Employee.LastName)
                            .Set("EmployeeName", string.Concat(c.Employee.LastName, ", ", c.Employee.FirstName, " ", c.Employee.MiddleName))
                            .Set("EmployerId", c.EmployerId)
                            .Set("EmployerName", c.EmployerName)
                            .Set("ReasonId", c.Reason != null ? c.Reason.Id : string.Empty)
                            .Set("ReasonName", c.Reason != null ? c.Reason.Name : string.Empty)
                            .Set("StartDate", c.StartDate)
                            .Set("EndDate", c.EndDate)
                            .Set("Status", c.GetCaseStatus().ToString())
                            .Set("Type", c.CaseType.ToString())
                            .Set("ModifiedDate", c.ModifiedDate)
                            .Set("ModifiedDateText", c.ModifiedDate.ToUIString())
                            .Set("AssignedToName", c.AssignedToName)
                            .Set("IsAccommodation", c.IsAccommodation)
                            .Set("AccommodationRequest", this.AccommodationCategory(c.AccommodationRequest, c.IsAccommodation));
                    });
                }
                else if (Customer.Current.HasFeature(Feature.LOA))
                {
                    result.Results = ids.Where(i => cases.Any(c => c.Id == i)).Select(i =>
                    {
                        var c = cases.FirstOrDefault(a => a.Id == i);
                        return new ListResult()
                           .Set("Id", c.Id)
                           .Set("CaseNumber", c.CaseNumber)
                           .Set("EmployeeId", c.Employee.Id)
                           .Set("EmployeeFirstName", c.Employee.FirstName)
                           .Set("EmployeeLastName", c.Employee.LastName)
                           .Set("EmployeeName", string.Concat(c.Employee.LastName, ", ", c.Employee.FirstName, " ", c.Employee.MiddleName))
                           .Set("EmployerId", c.EmployerId)
                           .Set("EmployerName", c.EmployerName)
                           .Set("ReasonId", c.Reason != null ? c.Reason.Id : string.Empty)
                           .Set("ReasonName", c.Reason != null ? c.Reason.Name : string.Empty)
                           .Set("StartDate", c.StartDate)
                           .Set("EndDate", c.EndDate)
                           .Set("Status", c.GetCaseStatus().ToString())
                           .Set("Type", c.CaseType.ToString())
                           .Set("ModifiedDate", c.ModifiedDate)
                           .Set("ModifiedDateText", c.ModifiedDate.ToUIString())
                           .Set("AssignedToName", c.AssignedToName);
                    });
                }

                return result;
            }
        }

        /// <summary>
        /// Get the Pimary Provider Contact for a given Case.
        /// </summary>
        /// <param name="myCase">The Case.</param>
        /// <returns>The Case's Primary Employee Contact.</returns>
        public EmployeeContact GetCasePrimaryProviderContact(Case theCase)
        {
            if (theCase.Employee != null)
            {
                var employeeContacts = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == theCase.Employee.Id).ToList();
                if (employeeContacts.Any())
                {
                    List<EmployeeContact> providerContacts = null;
                    using (var contactTypeService = new AbsenceSoft.Logic.Customers.ContactTypeService())
                    {
                        var medicalContactTypes = contactTypeService.GetMedicalContactTypeCodes();
                        providerContacts = employeeContacts.Where(c => medicalContactTypes.Any(m => m == c.ContactTypeCode)).ToList();
                    }
                    if (providerContacts != null && providerContacts.Any())
                        return providerContacts.SingleOrDefault(pc => pc.Contact.IsPrimary.HasValue && pc.Contact.IsPrimary.Value);
                }
            }
            return null;
        }

        public EmployeeContact GetCaseProviderContact(string hcpContactId)
        {
            if (!string.IsNullOrEmpty(hcpContactId))
            {
                EmployeeContact providerContact = EmployeeContact.AsQueryable().Where(m => m.Id == hcpContactId).FirstOrDefault();
                return providerContact;
            }
            return null;
        }


        private string AccommodationCategory(AccommodationRequest accommodationRequest, bool isAccommodation)
        {
            if (isAccommodation && accommodationRequest != null && accommodationRequest.Accommodations != null)
            {
                List<Accommodation> afterFilterAccommodations = new List<Accommodation>();

                foreach (Accommodation accom in accommodationRequest.Accommodations)
                {
                    if (accom.EndDate.HasValue)
                    {
                        if (accom.StartDate.HasValue && (accom.StartDate.Value < DateTime.Today && accom.EndDate.Value > DateTime.Today))
                        {
                            afterFilterAccommodations.Add(accom);
                        }
                    }
                    else
                    {
                        if (accom.StartDate.HasValue && accom.StartDate.Value < DateTime.Today)
                        {
                            afterFilterAccommodations.Add(accom);
                        }
                    }
                }

                List<string> accomNames = afterFilterAccommodations.Where(k => k.Type != null).Select(k => k.Type.Code).ToList();
                return accomNames.Count > 0 ? String.Join(", ", accomNames) : "None";
            }
            else
            {
                return "None";
            }
        }


        public List<Case> ExportCaseList(string employerId)
        {
            using (new InstrumentationContext("CaseService.ExportCaseList"))
            {
                return Case.AsQueryable().Where(e => e.EmployerId == employerId).ToList();
            }
        }

        /// <summary>
        /// Runs the entitlement and usage calcs on the case and returns the
        /// resulting case re-populated with all of the fun included. Basically
        /// wraps the GetProjectedUsage method, but in a friendly happy way. (Which is?)
        /// </summary>
        /// <param name="myCase">The case to run calculations on.</param>
        /// <returns>The calculated case result.</returns>
        public Case RunCalcs(Case myCase, CaseChangeHandlers changeHandlers = null)
        {
            if (myCase == null)
                throw new ArgumentNullException("myCase");

            if (myCase.Segments != null && myCase.Segments.Any(segment => segment.Type != CaseType.Administrative))
            {
                var empWorkSchedules = Employee.GetById(myCase.Employee.Id)?.WorkSchedules;
                if (empWorkSchedules == null || !new EmployeeService().Using(s => s.AreSchedulesValid(empWorkSchedules)))
                    throw new AbsenceSoftException("CaseService: Invalid Work Schedule");
            }

            Case original = myCase.Clone();

            GetProjectedUsage(myCase);

            // If we don't have this feature turned on, let's save some CPU time
            if (CurrentEmployer == null || CurrentEmployer.HasFeature(Feature.PolicyCrosswalk))
                CalculatePolicyCrosswalkCodes(myCase);

            if (RunWorkflow)
            {
                var prop = new CasePropertyChangedHandler((c, o) =>
                {
                    AppliedPolicy p = o.ContainsKey("Policy") ? o["Policy"] as AppliedPolicy : null;
                    if (p != null)
                        c.OnSavedNOnce((source, args) => p.WfOnPolicyExhausted(args.Entity));
                });
                if (changeHandlers == null)
                    changeHandlers = new CaseChangeHandlers(CasePropertyChanged.PolicyExhaustionDate, prop);
                else if (!changeHandlers.ContainsKey(CasePropertyChanged.PolicyExhaustionDate))
                    changeHandlers.Add(CasePropertyChanged.PolicyExhaustionDate, prop);
            }

            if (changeHandlers != null)
                CaseChangeDetector.Detect(original, myCase, changeHandlers);

            return myCase;
        }

        private void EvaluateAndSetPolicyCrosswalkCodes(LeaveOfAbsence loa, List<PolicyRuleGroup> ruleGroupsToEvaluate, List<PolicyCrosswalkCode> codesToApply)
        {
            using (var ruleService = new RuleService<LeaveOfAbsence>())
            {
                var policyCrosswalkTypes = GetTypesThatMayApplyToCase(loa.Case);

                var passedRuleGroups = ruleGroupsToEvaluate.Where(rg => ruleService.EvaluateRuleGroup(rg, loa));
                if (passedRuleGroups.Any())
                {
                    foreach (var ruleGroup in passedRuleGroups)
                    {
                        var matchingType = policyCrosswalkTypes.FirstOrDefault(pct => pct.Code == ruleGroup.CrosswalkTypeCode);

                        if (matchingType != null)
                        {
                            var existingCode = codesToApply.FirstOrDefault(r => r.TypeCode == matchingType.Code);
                            if (existingCode != null)
                            {
                                existingCode.Code = ruleGroup.CrosswalkCode;
                            }
                            else
                            {
                                codesToApply.Add(new PolicyCrosswalkCode(matchingType.Code, matchingType.Name, ruleGroup.CrosswalkCode));
                            }
                        }
                    }
                }
            }
        }

        public void CalculatePolicyCrosswalkCodes(Case @case)
        {
            LeaveOfAbsence loa = new LeaveOfAbsence()
            {
                Employee = @case.Employee,
                Case = @case,
                Employer = @case.Employer,
                Customer = @case.Customer
            };

            foreach (var segment in @case.Segments)
            {
                foreach (var policy in segment.AppliedPolicies)
                {
                    policy.PolicyCrosswalkCodes = null;
                    var thePolicy = policy.Policy;
                    List<PolicyCrosswalkCode> codesToApply = new List<PolicyCrosswalkCode>();

                    EvaluateAndSetPolicyCrosswalkCodes(loa, thePolicy.RuleGroups.Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk).ToList(), codesToApply);

                    var policyAbsenceReason = thePolicy.AbsenceReasons.FirstOrDefault(ar => ar.ReasonCode == @case.Reason.Code);
                    if (policyAbsenceReason != null)
                    {
                        EvaluateAndSetPolicyCrosswalkCodes(loa, policyAbsenceReason.RuleGroups.Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk).ToList(), codesToApply);
                    }

                    policy.PolicyCrosswalkCodes = codesToApply;
                }
            }
        }

        private List<PolicyCrosswalkType> GetTypesThatMayApplyToCase(Case myCase)
        {
            var policyCrosswalkTypeCodes = myCase.Segments
                .SelectMany(s => s.AppliedPolicies)
                .Select(ap => ap.Policy)
                .SelectMany(p => p.RuleGroups)
                .Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk)
                .Select(rg => rg.CrosswalkTypeCode).ToList();

            var query = PolicyCrosswalkType.Query.In(pct => pct.Code, policyCrosswalkTypeCodes);

            return PolicyCrosswalkType.DistinctFind(query, CustomerId, myCase.EmployerId).ToList();
        }

        /// <summary>
        /// store all the user overrides and provide an easy way to find the afterwards
        /// </summary>
        private class SegmentUsage
        {
            // maybe one of my worst data structures.... preserve the list of approved intermittent usage
            // a dictionary of segment ids, and then applied policy ids and then the list of times used against those
            // (maybe I should turn this into a class?)(of some kind)
            private Dictionary<Guid, Dictionary<Guid, List<AppliedPolicyUsage>>> userOverRides = new Dictionary<Guid, Dictionary<Guid, List<AppliedPolicyUsage>>>();
            public bool HasLockedUsage { get; private set; }

            public SegmentUsage(Case theCase)
            {
                // build the list up, just so no combination will be null, but it will contain only what we used
                foreach (CaseSegment cs in theCase.Segments.Where(s => s.Status != CaseStatus.Cancelled))
                {
                    userOverRides[cs.Id] = new Dictionary<Guid, List<AppliedPolicyUsage>>();
                    foreach (AppliedPolicy ap in cs.AppliedPolicies.Where(a => a.Status != EligibilityStatus.Ineligible))
                    {
                        userOverRides[cs.Id][ap.Id] = new List<AppliedPolicyUsage>();
                        foreach (AppliedPolicyUsage apu in ap.Usage)
                        {
                            // if it is a user override, save it, or if it is intermittent and was some sort of type that caused the user
                            // to enter data then add it back too (this will force the status on intermittents back to IntermittentPending
                            // if the user removed a usaged)
                            if (apu.IsLocked.HasValue && apu.IsLocked.Value)
                                HasLockedUsage = true;

                            if (apu.UserEntered || (apu.IsLocked.HasValue && apu.IsLocked.Value) ||
                                                    (cs.Type == CaseType.Intermittent && (apu.Determination == AdjudicationStatus.Denied && apu.DenialReasonCode != AdjudicationDenialReason.Exhausted)))
                                userOverRides[cs.Id][ap.Id].Add(apu);
                        }
                    }                                   // foreach (AppliedPolicy ap in cs.AppliedPolicies)
                }                                       // foreach (CaseSegment cs in theCase.Segments)
            }                                           // public SegmentUsage(Case theCase)

            /// <summary>
            /// get the usage for the segement, policy and date
            /// </summary>
            /// <param name="segmentId"></param>
            /// <param name="appliedPolicyId"></param>
            /// <param name="date"></param>
            /// <returns>Null if nothing found</returns>
            public AppliedPolicyUsage FindUsage(Guid segmentId, Guid appliedPolicyId, DateTime date)
            {
                if (!userOverRides.ContainsKey(segmentId))
                    return null;

                if (!userOverRides[segmentId].ContainsKey(appliedPolicyId))
                    return null;

                return userOverRides[segmentId][appliedPolicyId].FirstOrDefault(apu => apu.DateUsed == date);
            }

            public List<AppliedPolicyUsage> FindAllUsage(Guid segmentId, Guid appliedPolicyId, DateTime date)
            {
                if (!userOverRides.ContainsKey(segmentId))
                    return new List<AppliedPolicyUsage>();

                if (!userOverRides[segmentId].ContainsKey(appliedPolicyId))
                    return new List<AppliedPolicyUsage>();

                return userOverRides[segmentId][appliedPolicyId].Where(apu => apu.DateUsed == date).ToList();
            }

            public List<AppliedPolicyUsage> FindAllUsage(Guid segmentId, Guid appliedPolicyId, DateTime date, AdjudicationStatus status)
            {
                if (!userOverRides.ContainsKey(segmentId))
                    return new List<AppliedPolicyUsage>();

                if (!userOverRides[segmentId].ContainsKey(appliedPolicyId))
                    return new List<AppliedPolicyUsage>();

                return userOverRides[segmentId][appliedPolicyId].Where(apu => apu.DateUsed == date && apu.Determination == status).ToList();

            }

        }                                               // private class SegmentUsage

        /// <summary>
        /// Take a case id, and project out into the future how that will be used based 
        /// on previous usage and a set of rules
        /// 
        /// If you want to have the data persisted call with just the caseID
        /// (The thinking goes, if you loaded it you save it, if I loaded it I save)
        /// </summary>
        /// <param name="theCase"></param>
        public void GetProjectedUsage(string caseId)
        {
            // start with the normal checks

            // make sure there is a case
            if (string.IsNullOrWhiteSpace(caseId))
                throw new AbsenceSoftException("Case Service: GetProjectedUsage called with no case id");

            // start by getting the case (and making sure we get one)
            Case theCase = Case.GetById(caseId);

            if (theCase == null)
                throw new AbsenceSoftException("Case Service: GetProjectedUsage called with invalid case id");

            // now hand if off
            GetProjectedUsage(theCase);

            // it's all been generated so save the case
            // I retrieve it, I save it 
            theCase.Save();

        }


        /// <summary>
        /// Takes the case object calculates the projected usage and loads into the case object
        /// this method does not persist the data
        /// 
        /// Take Two: And this is how it will work going forward.
        /// 
        /// The case segment contains a LeaveSchedule. Based on the following conditions the leave schedule
        /// will / will not be populated. If it is not populated (because of the type), it will be created
        /// and populated. This is then fed to the calculation. Leave is the difference bewteen the schedule
        /// and the work day. This happens on the case segment.
        /// 
        /// Conditions:
        ///     Consecutive: The leave schedule is a clone of the employee's regular work schedule with all days set to 0. Leave schedule will be created.
        ///     
        ///     Reduced: The leave schedule contains the number of hours that the employee is working. It is their new schedule. Leave schedule will be used.
        ///     
        ///     Intermittent: Use only the dates specified within the list. So if on 2/4 the employee normally works 8 hours and they have 2 hours off the schedule should have an entry for 6 
        ///                     which is how long they worked that day.
        /// </summary>
        /// <param name="theCase"></param>
        public void GetProjectedUsage(Case theCase)
        {
            using (new InstrumentationContext("CaseService.GetProjectedUsage"))
            {
                // make sure the case dates are all valid
                // this throws an error that will just bubble up
                ValidateCaseDates(theCase);

                // make sure all data elements are in the correct state (so far this is really just for intermittent)
                ValidateCaseData(theCase);

                // step one: get the prior usage, this is used across all the policy processing
                List<PolicyUsageTotals> policyTotals;
                List<PolicyUsageTotals> deniedTotals;
                List<PolicyUsageTotals> adminPolicyTotals = null;
                List<PolicyUsageTotals> adminDeniedTotals = null;

                // this can be called on a new case, in which case there is no case, so get the all the prior usage.
                // if there is a case id then do not get the prior usage with this case included.
                if (string.IsNullOrWhiteSpace(theCase.Id))
                {
                    CalculatePriorUsage pt = new CalculatePriorUsage(theCase, theCase.StartDate, true, false, false, null);
                    CalculatePriorUsage dt = new CalculatePriorUsage(theCase, theCase.StartDate, true, false, false, null, AdjudicationStatus.Denied);
                    adminPolicyTotals = pt.ProcessAdministrativePolicyUsage()?.UsageTotals;
                    policyTotals = pt.UsageHistory.UsageTotals; // GetPriorUsage(theCase.Employee.Id, theCase.StartDate);                    
                    adminDeniedTotals = dt.ProcessAdministrativePolicyUsage()?.UsageTotals;
                    deniedTotals = dt.UsageHistory.UsageTotals; // GetPriorUsage(theCase.Employee.Id, theCase.StartDate, AdjudicationStatus.Denied);                    
                }
                else
                {
                    CalculatePriorUsage pt = new CalculatePriorUsage(theCase, theCase.StartDate, false, false, false, null);
                    CalculatePriorUsage dt = new CalculatePriorUsage(theCase, theCase.StartDate, false, false, false, null, AdjudicationStatus.Denied);
                    adminPolicyTotals = pt.ProcessAdministrativePolicyUsage()?.UsageTotals;
                    policyTotals = pt.UsageHistory.UsageTotals; // GetPriorUsage(theCase.Employee.Id, theCase.Id, theCase.StartDate);
                    adminDeniedTotals = dt.ProcessAdministrativePolicyUsage()?.UsageTotals;
                    deniedTotals = dt.UsageHistory.UsageTotals;  //GetPriorUsage(theCase.Employee.Id, theCase.Id, theCase.StartDate, AdjudicationStatus.Denied);                    
                }

                // save everything the user has done
                SegmentUsage userOverrides = new SegmentUsage(theCase);


                //We need to get the the list of Distinct Policy , thumb rule is 
                //elimination period should only be applied contiguously through the beginning of 
                //the leave until satisfied, it's not really specific to the "segment" but rather the duration

                List<AppliedPolicy> lstOfPolicyinCase = theCase.Segments.SelectMany(s => s.AppliedPolicies).
                    Distinct(p => p.Policy.Code).ToList();

                //New set of calc will start again so set the value false
                lstOfPolicyinCase.ForEach(m =>
                {
                    m.IsEliminationApplied = false;
                });

                // now that we copied our intermittent types, wipe out all the usage (we will put back our)
                // approved intermittents later
                // we need to keep anything that was already sent to payroll, it stays no matter what

                theCase.Segments.ForEach(s => s.AppliedPolicies.ForEach(ap => ap.Usage = new List<AppliedPolicyUsage>()));

                // now process the segments in order
                foreach (CaseSegment cs in theCase.Segments.Where(s => s.Status != CaseStatus.Cancelled))
                {
                    switch (cs.Type)
                    {
                        case CaseType.Intermittent:
                            ProcessIntermittentSegment(cs, theCase, policyTotals, deniedTotals, userOverrides);
                            break;
                        case CaseType.Consecutive:
                        case CaseType.Reduced:
                            ProcessSegment(cs, theCase, policyTotals, deniedTotals, userOverrides, lstOfPolicyinCase);
                            break;
                        case CaseType.Administrative:
                            List<AppliedPolicy> lstOfAdminPolicyinCase = lstOfPolicyinCase.Where(p => p.PolicyReason.OnlyCalculateUsageForLostTime == true)?.ToList();
                            if (lstOfAdminPolicyinCase != null && lstOfAdminPolicyinCase.Any())
                            {
                                ProcessSegment(cs, theCase, adminPolicyTotals ?? new List<PolicyUsageTotals>(), adminDeniedTotals ?? new List<PolicyUsageTotals>(), userOverrides, lstOfAdminPolicyinCase);
                            }
                            else
                            {
                                ProcessSegment(cs, theCase, policyTotals, deniedTotals, userOverrides, lstOfPolicyinCase);
                            }
                            break;
                        default:
                            break;
                    }                                   // switch (cs.Type)
                }                                       // foreach(CaseSegment cs in theCase.Segments)

                // now that the entitlements are applied
                // calculate the money
                PaidLeaveCalculate(theCase);

                // after we recalc the case rebuild the summary.
                CalcCaseSummary(theCase);
            }                                           // using (new InstrumentationContext("CaseService.GetProjectedUsage"))
        }                                               // public void GetProjectedUsage(Case theCase, bool justAPlaceHolderUntilIDeleteThePriviousVersion)


        /// <summary>
        /// Validate the data is correct on the case before blowing away the user info
        /// </summary>
        /// <param name="theCase"></param>
        private void ValidateCaseData(Case theCase)
        {

            // currently the only data to verify is the absence data for intermittent cases
            // each abscence record must have the meta data for the leave detail attached to it
            // (this is the data that the user has interacted with)
            if (!theCase.IsIntermittent)
                return;

            // currently the only data to verify is the absence data for intermittent cases
            // each abscence record must have the meta data for the leave detail attached to it
            // (this is the data that the user has interacted with)
            foreach (CaseSegment cs in theCase.Segments.Where(s => s.Status != CaseStatus.Cancelled))
            {
                if (cs.Type != CaseType.Intermittent)
                    continue;

                var policyName = new Func<string, string>((p) =>
                {
                    var policy = cs.AppliedPolicies.FirstOrDefault(a => a.Policy.Code == p);
                    if (policy == null) return p;
                    return policy.Policy.Name;
                });

                int totalMinutes = 0;
                foreach (IntermittentTimeRequest itr in cs.UserRequests)
                {
                    totalMinutes = 0;

                    foreach (IntermittentTimeRequestDetail itrd in itr.Detail)
                    {
                        //find the max total minutes for all the policies.
                        //this should match the request total minutes
                        var tempMinutes = (itrd.Approved + itrd.Denied + itrd.Pending);
                        if (tempMinutes > totalMinutes)
                        {
                            // we have to keep both as our UI doesn't support policy wise TOR
                            itrd.TotalMinutes = tempMinutes;
                            totalMinutes = tempMinutes;
                        }
                    }

                    // Just clean up here, we're no longer using this the way originally intended but it makes UI and reporting way easier
                    if (itr.TotalMinutes != totalMinutes)
                        itr.TotalMinutes = totalMinutes;

                    // Validate there are no wonky things going on here; all time must match for all policies, period.
                    var badDetails = itr.Detail.Where(d => (d.Denied + d.Approved + d.Pending) < d.TotalMinutes).ToList();
                    if (badDetails.Any())
                    {
                        StringBuilder badNaughtyLittlePolicies = new StringBuilder();
                        badNaughtyLittlePolicies.AppendFormat("The time requested per policy on {0:MM-dd-yyyy} does not match. All policies' approved, pending and denied time should add up to {1}.",
                            itr.RequestDate, totalMinutes.ToFriendlyTime());
                        badNaughtyLittlePolicies.AppendFormat(" The polic{0} missing time {1} '{2}'", badDetails.Count == 1 ? "y" : "ies", badDetails.Count == 1 ? "is" : "are",
                            string.Join("', '", badDetails.Select(d => policyName(d.PolicyCode))));
                        throw new AbsenceSoftException(badNaughtyLittlePolicies.ToString());
                    }
                }
            }
        }// private void ValidateCaseData(Case theCase)


        /// <summary>
        /// This case type regenerates the usage based on the schedule
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        private void ProcessSegment(CaseSegment theSegment, Case theCase, List<PolicyUsageTotals> policyTotals,
            List<PolicyUsageTotals> deniedTotals, SegmentUsage userOverrides, List<AppliedPolicy> lstOfPolicyinCase)
        {

            // if this is a consecutive or administrative schedule then the LeaveSchedule may or may not exists
            // it doesn't matter. We manifest the current schedule and then set all the time to
            // 0, otherwise we expect that the UI / User / not this method has manifested some sort
            // of schedule to drive the process 
            // for intermittents all of the info will be copied straight to pending
            if (theSegment.Type == CaseType.Consecutive || theSegment.Type == CaseType.Administrative)
            {
                CopyWorkSchedule(theSegment, theCase);
            }

            if (theSegment.Type == CaseType.Reduced)
            {
                CopyWorkScheduleForReduced(theSegment, theCase);
                MaterializeReducedSchedule(theSegment, theCase);
            }

            if (theCase.MissedTimeOverride != null && theCase.MissedTimeOverride.Any())
            {
                theCase.MissedTimeOverride.ForEach(m =>
                {
                    if (theSegment.Absences.Any(a => a.SampleDate == m.SampleDate))
                    {
                        theSegment.Absences.FirstOrDefault(a => a.SampleDate == m.SampleDate).TotalMinutes = m.TotalMinutes;
                    }
                });
            }
            // now apply all the policies (if they are eligible), first sort them and then calc them
            List<AppliedPolicy> sorted = AppliedPolicySort(theSegment.AppliedPolicies);

            applyCaseEvents(theCase, sorted, theSegment);

            foreach (AppliedPolicy ap in sorted)
                if (ap.Status != EligibilityStatus.Ineligible)
                    ApplyEntitlement(ap, theSegment, theCase, policyTotals, deniedTotals, userOverrides, lstOfPolicyinCase);

        }

        /// <summary>
        /// case events might cause a policy to end early, go through each policy
        /// and see if any of the events that the policy reason listens for has occured
        /// if so, set the applied policies end date 
        /// </summary>
        /// <param name="case"></param>
        /// <param name="theSegment"></param>
        private void applyCaseEvents(Case @case, List<AppliedPolicy> thePolicies, CaseSegment caseSegment = null)
        {


            foreach (AppliedPolicy ap in thePolicies)
            {

                foreach (PolicyEvent pe in ap.PolicyReason.PolicyEvents)
                {

                    CaseEvent ce = @case.FindCaseEvent(pe.EventType);
                    if (ce == null)
                        continue;

                    switch (pe.DateType)
                    {
                        case EventDateType.EndPolicyBasedOnEventDate:
                        default:
                            if (!ap.PolicyDateOverridden.Value)
                            {
                                ap.EndDate = ce.EventType == CaseEventType.BondingStartDate ? ce.EventDate.AddDays(-1) : ce.EventDate;
                            }
                            break;
                        case EventDateType.EndPolicyBasedOnCaseEndDate:
                            ap.EndDate = @case.EndDate.Value;
                            break;
                        case EventDateType.EndPolicyBasedOnCaseStartDate:
                            ap.EndDate = @case.StartDate;
                            break;
                        case EventDateType.StartPolicyBasedOnEventDate:
                            ap.StartDate = ce.EventDate;
                            break;
                        case EventDateType.EndPolicyBasedOnPeriod:
                            if (!ap.ManuallyAdded && ap.Status == EligibilityStatus.Eligible)
                            {
                                //This should be Segment StartDate & EndDate not Case Start Date & End Date ,because one case has multipile Segment Type , (Consecutive and Intermittent)
                                //and for each segment type we hadnle holiday in a different way, so if we put Case Start Date & End Date then it consider the entire range without

                                ap.EndDate = ce.EventDate.AddMonths(pe.EventPeriod.Value);
                                //We Cannot just set applied policy enddate with case enddate , because there could be multipile 
                                //segment and each segment may have different endate , than the caseend date
                                if (ap.EndDate > @case.EndDate) ap.EndDate = (caseSegment != null ? @case.EndDate > caseSegment.EndDate ? caseSegment.EndDate : @case.EndDate : @case.EndDate);
                            }
                            break;

                    }
                }
            }
            return;
        }

        private void ApplyEntitlement(AppliedPolicy ap, CaseSegment theSegment, Case theCase,
            List<PolicyUsageTotals> policyTotals, List<PolicyUsageTotals> deniedTotals,
            SegmentUsage userOverrides, List<AppliedPolicy> lstOfPolicyinCase)
        {

            // Set IntermittentDetermination to null for previous user request
            policyTotals.ForEach(x => x.Allocated?.ForEach(a => a.IntermittentDetermination = null));

            // value checks
            if (ap.PolicyReason.Entitlement == null || !ap.PolicyReason.Entitlement.HasValue)
                throw new AbsenceSoftException("Case Service: ApplyEntitlement entitlment does not have a value");

            // another check, if there is a per usage cap make sure that it is the same entitlement type
            // as the policy reason's entititlement type
            if (ap.PolicyReason.PerUseCapType != null && !ap.PolicyReason.PerUseCapType.Value.Compare(ap.PolicyReason.EntitlementType.Value))
                throw new AbsenceSoftException("Case Service: ApplyEntitlement: Incompatible per use cap and entitlement types");

            if (ap.Policy.Code == "WI-FML" )
            {
                int x = 0;
            }
            // shortcut variable
            bool hasUsageCap = ap.PolicyReason.PerUseCap.HasValue && ap.PolicyReason.PerUseCapType.HasValue;

            var policyReasonCode = !string.IsNullOrEmpty(ap.PolicyReason.ReasonCode) ? ap.PolicyReason.ReasonCode : ap.PolicyReason.Reason.Code;

            // here's a special check (and build this up for later)
            // if a policy is consecutive too another one, then check to see if the other one has even been used
            // and build up a list of them
            List<PolicyUsageTotals> parentDeniedPolicies = new List<PolicyUsageTotals>();
            if (ap.Policy.ConsecutiveTo != null & ap.Policy.ConsecutiveTo.Count > 0)
            {
                // we look through the denied totals becuase the child can only get used
                // if the parent is exhuasted or ended (we'll worry about denial reasons furter down)
                foreach (PolicyUsageTotals ppp in deniedTotals)
                {
                    Policy tempP = Policy.GetByCode(ppp.PolicyCode, theCase.CustomerId, theCase.EmployerId);
                    if (tempP == null)
                        continue;

                    if (ap.Policy.ConsecutiveTo.Any(apct => apct == tempP.Code))
                        parentDeniedPolicies.Add(ppp);
                }
            }

            // find their schedule across this time period
            LeaveOfAbsence los = new LeaveOfAbsence
            {
                Case = theCase,
                Employee = theCase.Employee,
                Employer = theCase.Employee.Employer,
                Customer = theCase.Employee.Customer,
                WorkSchedule = Employee.GetById(theCase.Employee.Id)?.WorkSchedules
            };

            if (ap.PolicyBehaviorType == PolicyBehavior.Ignored)
            {
                if (theSegment.Type == CaseType.Reduced)
                {
                    MaterializeReducedSchedule(theSegment, theCase, ap.PolicyBehaviorType);
                    if (theCase.MissedTimeOverride != null && theCase.MissedTimeOverride.Any())
                    {
                        theCase.MissedTimeOverride.ForEach(m =>
                        {
                            if (theSegment.Absences.Any(a => a.SampleDate == m.SampleDate))
                            {
                                theSegment.Absences.FirstOrDefault(a => a.SampleDate == m.SampleDate).TotalMinutes = m.TotalMinutes;
                            }
                        });
                    }
                }
                else
                {
                    theSegment.Absences = los.MaterializeSchedule(theSegment.StartDate, theSegment.EndDate.Value, false, null, ap.PolicyBehaviorType);
                }
            }
            // get the holidays for the employer
            HolidayService hs = new HolidayService();
            List<int> years = Enumerable.Range(0, (ap.EndDate.Value.Year - ap.StartDate.Year) + 1).Select(i => ap.StartDate.Year + i).ToList();
            years.Add(years.Min() - 1);
            List<DateTime> holidays = hs.GetHolidayList(years, theCase.Employer);

            // then get the list times worked, pretend there are no holidays for now
            // we will 0 out the hours in the main processing loop
            List<Time> regularTimes = los.MaterializeSchedule(ap.StartDate, ap.EndDate.Value, false, null, ap.PolicyBehaviorType);

            // we need a materialized schedule that does not include holidays
            // this is used to find out how many work days are in the week of the day (and days in the calendar month)
            // that we are currently computing (expanding the window 120 days just to make sure we always
            // have a full week (and month) to use when getting the number of days (It's overkill))
            List<Time> theWorkDays = los.MaterializeSchedule(ap.StartDate.AddDays(-60), ap.EndDate.Value.AddDays(60), false, null, ap.PolicyBehaviorType);

            // we need help (used for date calcs)
            AppliedPolicyUsageHelper theHelper = new AppliedPolicyUsageHelper(los, theCase.StartDate, theCase.EndDate.Value, ap.PolicyBehaviorType);

            // everything is in it's own units of measurements, could be minutes
            // days, weeks or months. We don't care we just calc to the 
            // unit
            double unitsToApply = 0;

            unitsToApply = ap.PolicyReason.Entitlement.Value;
            //if(ap.PolicyReason.EntitlementType == EntitlementType.WorkingHours)
            //    unitsToApply = ap.PolicyReason.Entitlement.Value * 60d;         // we care a little, convert to minutes

            // get the units already used on this policy, if it doesn't exist then create it
            // this tracks our usage across all the segments in the case
            PolicyUsageTotals put = policyTotals.FirstOrDefault(f => f.PolicyCode == ap.Policy.Code);
            if (put == null)
            {
                put = new PolicyUsageTotals() { PolicyCode = ap.Policy.Code };
                policyTotals.Add(put);
            }

            put = policyTotals.FirstOrDefault(f => f.PolicyCode == ap.Policy.Code);
            PolicyUsageTotals putForHolidays = put.Clone();
            // flag set to true when the entitlement is all used up. Used to short circuit the rest of the logic
            bool entitlementUsedUp = false;
            bool reachedTheLimit = false;
            // check to see if there is an elimination period
            bool wasIncEliminationPeriodExceeded = false;
            bool hasIncEliminationPeriod = false;
            PolicyUsageTotals incEliminationTotals = new PolicyUsageTotals { PolicyCode = ap.Policy.Code };


            bool hasExcEliminationPeriod = false;
            bool wasExcEliminationPeriodExeceeded = false;

            DateTime maxEliminationDay = DateTime.MinValue;

            DateTime firstWorkDay = DateTime.MinValue; // used when calc'ing a calendar type elim period (when policy pays in working hours),
            bool elimFirstWorkDaySet = false;

            // now, if elimination periods are by calendar days, then the first day of the period is the first day of the leave
            if (ap.PolicyReason.EliminationType.HasValue && ap.PolicyReason.EliminationType.Value.CalendarType())
            {
                elimFirstWorkDaySet = true;
                firstWorkDay = ap.StartDate;
            }

            double eliminationRemaining = 0;
            EntitlementType eliminationType = EntitlementType.WorkingHours;

            // find out how much of the elimination period has been used up
            PolicyUsageTotals deniedTotal = deniedTotals.FirstOrDefault(f => f.PolicyCode == ap.Policy.Code);
            if (deniedTotal == null)
            {
                deniedTotal = new PolicyUsageTotals() { PolicyCode = ap.Policy.Code };
                deniedTotals.Add(deniedTotal);
            }

            // here is a bit more stuff to hurt you.
            // if anything (at all) has been sent to payroll, then a couple of other rules kick in.
            // one. You cannot change the case start date, because that will ruin the elimination period calcs and they
            // can't be redone once they are paid. They will have to be manually adjusted.
            // So if anything has been sent to payroll then there are no elim calcs to do and we will just copy what was calculated
            // which will happen down in the main loop

            //if the employee has already exhausted a policy prior to the case start date(i.e.it's already exhausted) then it should not be possible for any time to be pending until they are 
            //legitimately able to take time again against that policy. Amy is correct, it should not work the way it is today, that time should still be exhausted, same goes for the elimination period, 
            //if the policy is/was already exhausted to begin with, the denial reason should be exhausted, not elimination period for those first 7 days; in the case of STD even, 
            //the elimination period wouldn't even apply at all until the employee had time available again, and then the elimination period to kick in for those first available 7 days of allowed 
            //usage(after no longer exhausted).

            //We are starting new calc , check do we have any thing left to use ,else no point wasting time checking Elimination period,create a
            //dummy workusage and check the value
            AppliedPolicyUsage currentWorkUsage = new AppliedPolicyUsage()
            {
                MinutesInDay = 0,
                MinutesUsed = 0,
                PolicyReasonId = ap.PolicyReason.ReasonId
            };
            CalcUsage entitlementCurrentUsage = new CalcUsage(currentWorkUsage, put, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, theSegment.Type);
            bool entitlementCurrentUsedUp = (entitlementCurrentUsage.ReachedTheLimit || entitlementCurrentUsage.WentOverTheLimit);

            // if there is an elimination period then we need to set up for it
            // fun fact: a consecutive too policy uses up the elimination period even if the parent(s) awarded time. How awesome is that! (The more you know)
            if (!userOverrides.HasLockedUsage && theSegment.Type == CaseType.Consecutive && ap.PolicyReason.EliminationType != null
                && ap.PolicyReason.EliminationType.HasValue && ap.PolicyReason.Elimination != null && ap.PolicyReason.Elimination.HasValue && !entitlementCurrentUsedUp)
            {
                // inclusive is the default, if it is not set then use that
                if (ap.PolicyReason.PolicyEliminationCalcType == null || ap.PolicyReason.PolicyEliminationCalcType == PolicyEliminationType.Inclusive)
                {
                    wasIncEliminationPeriodExceeded = false;
                    hasIncEliminationPeriod = true;
                }
                else
                {
                    wasExcEliminationPeriodExeceeded = false;
                    hasExcEliminationPeriod = true;
                }

                eliminationRemaining = ap.PolicyReason.Elimination.Value;
                eliminationType = ap.PolicyReason.EliminationType.Value;


                // all used up, nothing to do
                if (eliminationRemaining <= 0)
                {
                    wasIncEliminationPeriodExceeded = true;
                    wasExcEliminationPeriodExeceeded = true;
                    hasExcEliminationPeriod = false;
                    hasIncEliminationPeriod = false;
                }
            }

            // now to make the logic just a bit more twisted (and if you've been playing along at home)
            // here's how it goes... if we are doing a calendar type then we count every day of the leave
            // towards the total (even days they would not work because these are calendar days....
            // unless this intermitent then we just count the days off again.) 
            // so figure out which list we want to do, and then it get's weirder. The theSegment.Absences collection
            // contains the days they do not work. However, holidays they don't work, but we need a 0 row for those
            // so add that into the days here
            List<Time> countWhichDays;
            if (theSegment.Type != CaseType.Intermittent && ap.PolicyReason.EntitlementType.Value.CalendarType())
            {
                countWhichDays = Enumerable.Range(0, (ap.EndDate.Value - ap.StartDate).Days + 1).Select(d => new Time() { SampleDate = ap.StartDate.AddDays(d), TotalMinutes = 1 }).ToList();
            }
            else
            {
                // we need to make a new list of items so we don't mess up the collection on the object
                countWhichDays = theSegment.Absences.Select(a => new Time() { SampleDate = a.SampleDate, TotalMinutes = a.TotalMinutes, IsHoliday = a.IsHoliday }).ToList();
                // then add in the holdays in our range

                if (ap.PolicyBehaviorType != PolicyBehavior.Ignored)
                {
                    // We can't just tack holidays onto the end as duplicates and hope for the best, we need to wipe and replace.
                    countWhichDays.RemoveAll(t => holidays.Any(h => h == t.SampleDate));
                    countWhichDays.AddRange(holidays.Where(h => h >= ap.StartDate && h <= ap.EndDate.Value).Select(h => new Time() { SampleDate = h, TotalMinutes = 0 }).ToList());
                    countWhichDays = countWhichDays.OrderBy(day => day.SampleDate).ToList();
                }
            }

            // calc gap usages
            int gapUsageCount = ap.PolicyReason.EntitlementType.Value.CalendarTypeFromFirstUse() ? CalculateGapUsage(ap, theCase, policyTotals) : 0;

            #region elim period

            DayOfWeek startDayOfWeek = theCase.Employee.StartDayOfWeek ?? theCase.StartDate.DayOfWeek;
            int fixedWorkDaysPerWeek = ap.PolicyReason.FixedWorkDaysPerWeek ?? 6; //default 6
            int weekDaysCount = 0;
            int totalDaysCount = 0;
            //elimination period should only be applied contiguously through the beginning of the leave until satisfied, 
            //it's not really specific to the "segment" but rather the duration. The configuration would naturally be 
            //copied to other segments, even after a relapse, however with a relapse it depends on whether or 
            //not the "relapse allowed within" configuration had been met, and if not then the waiting period would 
            //apply again to that policy.
            if (lstOfPolicyinCase.FirstOrDefault(p => p.Policy.Code == ap.Policy.Code && !p.IsEliminationApplied) != null)
            {
                // now apply the elimination period logic (same type of logic as below, but if there is an elimination period
                // we have to work that first. If it isn't met then there is nothing else to do
                if (hasExcEliminationPeriod || hasIncEliminationPeriod)
                {
                    foreach (Time timeOut in countWhichDays)
                    {
                        maxEliminationDay = timeOut.SampleDate;

                        // the abscences are at the segment level and are applied across all applied policies
                        // so if this abscense date is not in the range of the applied policy then skip it
                        if (!timeOut.SampleDate.DateInRange(ap.StartDate, ap.EndDate))
                            continue;

                        // break down the usage into parts that can be measured against
                        List<AppliedPolicyUsageType> todaysUsage = theHelper.GetUsage(timeOut.SampleDate, ap.PolicyBehaviorType, holidays);
                        // find the absence date on our work schedule
                        Time normalWorkDay = regularTimes.FirstOrDefault(nwd => nwd.SampleDate == timeOut.SampleDate);

                        // we didn't find a day. It's not a normal work day for them, so skip it
                        // unless it is a calendar type and then every day counts
                        if (normalWorkDay == null && !ap.PolicyReason.EntitlementType.Value.CalendarType())
                            continue;

                        // So, if it's null and it's a calendar type, we don't have it in the collection, so @##$%, we need to create a fake one @ 0 minutes.
                        if (normalWorkDay == null)
                            normalWorkDay = new Time() { TotalMinutes = 0, SampleDate = timeOut.SampleDate, Id = Guid.NewGuid() };

                        if (ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays)
                        {
                            AdjustFixedWorkDaysChanges(startDayOfWeek, fixedWorkDaysPerWeek, timeOut, normalWorkDay, ref weekDaysCount, ref totalDaysCount);
                        }

                        // now, if the day is a holiday, 0 out the time so that we will create a 0 use row that can
                        // have a status set so that it does not create a break in the date range
                        if (holidays.Any(h => h == timeOut.SampleDate) && ap.PolicyBehaviorType != PolicyBehavior.Ignored && ap.PolicyReason.EntitlementType != EntitlementType.FixedWorkDays)
                            timeOut.TotalMinutes = 0;

                        // the total to use is the diference between the scheduled minutes and their projected minutes
                        // remember on calendar types we use all or nothing so it's just a 1
                        double totalRequested = (timeOut.TotalMinutes ?? 0);
                        if (ap.PolicyReason.EntitlementType.Value.CalendarType())
                        {
                            totalRequested = 1;
                            normalWorkDay.TotalMinutes = 1;
                        }

                        // yes, if somehow they have requested more time off then the should be allowed
                        // then this will be less than 0. e.g. normal 8 hour day and 10 hours off are requested. That's not allowed
                        // so default it to their normal work day
                        if (totalRequested < 0)
                            totalRequested = (normalWorkDay.TotalMinutes ?? 0);


                        // 1. find past usage based on the segment type
                        // 2. create a type to be used to see what today's usage calcs to as a percent
                        // 3. add up the prior percentages
                        // 4. add the two percentages together and if it is greater than 100% convert the remainder back into minutes for today
                        double minutesUsedForPolicyAndDay = 0;

                        minutesUsedForPolicyAndDay = put.PercentUsed(timeOut.SampleDate, AdjudicationStatus.Approved, ap.PolicyReason.EntitlementType.Value) +
                                                        put.PercentUsed(timeOut.SampleDate, AdjudicationStatus.Pending, ap.PolicyReason.EntitlementType.Value);

                        // this does the work for us, and we pass it to the calc usage class
                        AppliedPolicyUsage workUsage = new AppliedPolicyUsage()
                        {
                            DateUsed = timeOut.SampleDate,
                            MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                            MinutesUsed = totalRequested,
                            AvailableToUse = todaysUsage,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode
                        };

                        // there are two types of elimination periods that are mutually exclusive
                        // this is the check for a period must be met and is not included in the
                        // benefit. To rephrase, the elimination period must be exhausted before
                        // the benefit begins
                        if (hasExcEliminationPeriod)
                        {

                            // we have a problem with mix and match unit types on elimination. If the 
                            // entitlement is in work days and the elimination period is in calendar days,
                            // then we need to calc the elim period a little differently
                            if (ap.PolicyReason.EntitlementType.Value.PercentageType() && ap.PolicyReason.EliminationType.Value.CalendarType())
                            {
                                // currently support days and weeks
                                if (!elimFirstWorkDaySet && firstWorkDay == DateTime.MinValue && timeOut.TotalMinutes != 0)
                                    firstWorkDay = timeOut.SampleDate;

                                // start off setting this to the current process date plus one just in case we haven't hit the first work day yet
                                DateTime elimEndDate = timeOut.SampleDate.AddDays(1);

                                // then if we are really passed the first work day calc the elim end date
                                // this will allow non work days at the start of the case to display elimination period
                                if (firstWorkDay != DateTime.MinValue)
                                {
                                    int daysToAdd = 0;
                                    switch (ap.PolicyReason.EliminationType.Value)
                                    {
                                        case EntitlementType.CalendarDays:
                                        case EntitlementType.CalendarDaysFromFirstUse:
                                            daysToAdd = (int)ap.PolicyReason.Elimination.Value;
                                            break;
                                        case EntitlementType.CalendarWeeks:
                                        case EntitlementType.CalendarWeeksFromFirstUse:
                                            daysToAdd = 7 * (int)ap.PolicyReason.Elimination.Value;
                                            break;
                                        default:
                                            throw new NotSupportedException(string.Format("Elimination period: {0} - is not supported", Enum.GetName(ap.PolicyReason.EliminationType.Value.GetType(), ap.PolicyReason.EliminationType.Value)));
                                    }

                                    // if it is less than or equal to today then it is in the period (dates are inclusive, back one off)
                                    elimEndDate = firstWorkDay.AddDays(daysToAdd - 1);
                                }                                       // if (firstWorkDay != DateTime.MinValue)
                                if (elimEndDate >= timeOut.SampleDate)
                                {
                                    // now.... because we are adding these to the usage, and the user
                                    // may have created an override of their own for it, check to see if there
                                    // is an override and use that. we only care about denied in here
                                    // because if the elim period is exceeded the overrides will be approved in the
                                    // main loop
                                    string thisReasonCode = AdjudicationDenialReason.EliminationPeriod;
                                    string thisReasonName = AdjudicationDenialReason.EliminationPeriodDescription;
                                    string thisReasonOther = "";
                                    AppliedPolicyUsage elimTimeOverride = userOverrides.FindUsage(theSegment.Id, ap.Id, timeOut.SampleDate);
                                    bool ue = false;
                                    if (elimTimeOverride != null && elimTimeOverride.Determination == AdjudicationStatus.Denied)
                                    {
                                        thisReasonCode = elimTimeOverride.DenialReasonCode;
                                        thisReasonName = elimTimeOverride.DenialReasonName;
                                        thisReasonOther = elimTimeOverride.DenialExplanation;
                                        ue = elimTimeOverride.UserEntered;
                                    }

                                    ap.Usage.Add(new AppliedPolicyUsage()
                                    {
                                        MinutesUsed = (timeOut.TotalMinutes ?? 0),
                                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                                        Determination = AdjudicationStatus.Denied,
                                        DenialReasonCode = thisReasonCode,
                                        DenialReasonName = thisReasonName,
                                        DenialExplanation = thisReasonOther,
                                        DateUsed = timeOut.SampleDate,
                                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                        AvailableToUse = todaysUsage,
                                        UserEntered = ue
                                    });

                                    // and if it equals the day, then the period elim period is over
                                    if (elimEndDate == timeOut.SampleDate)
                                    {
                                        wasExcEliminationPeriodExeceeded = true;
                                        break;
                                    }

                                    continue;               // do the next day

                                }

                                // now... it is possible that the sample date is greater than the calc end date (cuz there was no work day that day)
                                // so when that happens we can set the elim exceeded flag and let it fall through and count this as a valid
                                // work day
                                if (timeOut.SampleDate > elimEndDate)
                                {
                                    wasExcEliminationPeriodExeceeded = true;
                                    break;
                                }
                            }                                   // if (ap.PolicyReason.EntitlementType.Value.PercentageType() && ap.PolicyReason.EliminationType.Value.CalendarType())
                            else
                            {

                                //calculate the totals used for elimination
                                CalcUsage eliminationUsage = new CalcUsage(workUsage, deniedTotal, ap.PolicyReason.EliminationType.Value, ap.PolicyReason.Elimination.Value, ap, true,
                                    theSegment.Type, gapUsageCount, null, los, 0,
                                    AdjudicationDenialReason.EliminationPeriod);

                                // this could be a bit wierd looking. Denied cuz over the limit is how much of the elimination was denied,
                                // which is really time that is now available for them to use, so change the totalRequested to it and
                                // update the workUsage so that it will be accurate for the next check.
                                // If there is an exclusive elimination period, non working days before the 
                                // first working days should also be inserted as usage, with 0 time used, so
                                // they appear as denied days in the Policy section driven by Case Summary.  
                                if (eliminationUsage.NewTotal > 0 || (!eliminationUsage.WentOverTheLimit && !eliminationUsage.ReachedTheLimit))
                                {
                                    string thisReason2Code = AdjudicationDenialReason.EliminationPeriod;
                                    string denialReasonName = AdjudicationDenialReason.EliminationPeriodDescription;
                                    string thisReason2Other = "";
                                    AppliedPolicyUsage elimTimeOverride = userOverrides.FindUsage(theSegment.Id, ap.Id, timeOut.SampleDate);
                                    bool ue2 = false;
                                    if (elimTimeOverride != null && elimTimeOverride.Determination == AdjudicationStatus.Denied)
                                    {
                                        thisReason2Code = elimTimeOverride.DenialReasonCode;
                                        denialReasonName = elimTimeOverride.DenialReasonName;
                                        thisReason2Other = elimTimeOverride.DenialExplanation;
                                        ue2 = elimTimeOverride.UserEntered;
                                    }

                                    ap.Usage.Add(new AppliedPolicyUsage()
                                    {
                                        MinutesUsed = eliminationUsage.NewTotal,
                                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                                        Determination = AdjudicationStatus.Denied,
                                        DenialReasonCode = thisReason2Code,
                                        DenialReasonName = denialReasonName,
                                        DenialExplanation = thisReason2Other,
                                        DateUsed = timeOut.SampleDate,
                                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                        AvailableToUse = todaysUsage,
                                        UserEntered = ue2
                                    });

                                    deniedTotal.AddAllocated(timeOut.SampleDate, eliminationUsage.NewTotal, (normalWorkDay.TotalMinutes ?? 0),
                                        policyReasonCode, AdjudicationStatus.Denied, AdjudicationDenialReason.EliminationPeriod, null,
                                        todaysUsage, thisReason2Other, denialReasonName, false, theCase.CaseNumber);
                                }
                                if (eliminationUsage.WentOverTheLimit)
                                {
                                    totalRequested = eliminationUsage.DeniedCuzOverTheLimit;
                                    workUsage.MinutesUsed = eliminationUsage.DeniedCuzOverTheLimit;
                                    wasExcEliminationPeriodExeceeded = true;  // don't have to calc it anymore
                                                                              // If elimination limit is crossed, then we should check if usage was inserted for 
                                                                              //the elimination day/sample date. If not, we deduct 1 day from maxEliminationDay
                                                                              //before the break statment, so that with the rest of the flow the usage can be inserted depending
                                                                              //on the eligibity rules. This is required since there are sceanrios where for loop is broken
                                                                              //without inserting usage.
                                    if (!ap.Usage.Any(us => us.DateUsed.ToMidnight() == maxEliminationDay.ToMidnight()) && maxEliminationDay.ToMidnight() > DateTime.MinValue)
                                        maxEliminationDay = maxEliminationDay.AddDays(-1).ToMidnight();
                                    break;
                                }
                                else
                                {
                                    // we didn't go over, but we hit the elimination period cap, so we don't have to do this anymore
                                    if (eliminationUsage.ReachedTheLimit)
                                    {
                                        wasExcEliminationPeriodExeceeded = true;
                                        // If elimination limit is reached, then we should check if usage was inserted for 
                                        //the elimination day/sample date. If not, we deduct 1 day from maxEliminationDay
                                        //before the break statment, so that with the rest of the flow the usage can be inserted depending
                                        //on the eligibity rules. This is required since there are sceanrios where for loop is broken
                                        //without inserting usage.
                                        if (!ap.Usage.Any(us => us.DateUsed.ToMidnight() == maxEliminationDay.ToMidnight()) && maxEliminationDay.ToMidnight() > DateTime.MinValue)
                                            maxEliminationDay = maxEliminationDay.AddDays(-1).ToMidnight();
                                        break;
                                    }

                                    continue;                   // wierd place for a continue, but we want to do the next day and skip everything else
                                }
                            }                                   // else if (ap.PolicyReason.EntitlementType.Value.PercentageType() && ap.PolicyReason.EntitlementType.Value.CalendarType())

                        }                      // (hasExcEliminationPeriod)

                        // if we are in an elimination period, then check the totals against the period. if we go over the limit
                        // flip the flag to true. we only need to check until the period was exceeded
                        if (hasIncEliminationPeriod)
                        {

                            CalcUsage eliminationUsage = new CalcUsage(workUsage, incEliminationTotals, ap.PolicyReason.EliminationType.Value, ap.PolicyReason.Elimination.Value, ap, false, theSegment.Type);
                            if (eliminationUsage.WentOverTheLimit || eliminationUsage.ReachedTheLimit)
                            {
                                wasIncEliminationPeriodExceeded = true;
                                break;
                            }

                            // yes we are tracking it twice, the inc total is just for this loop to know if we have gone over
                            incEliminationTotals.AddAllocated(timeOut.SampleDate, eliminationUsage.NewTotal,
                                (normalWorkDay.TotalMinutes ?? 0), policyReasonCode, AdjudicationStatus.Pending,
                                null, null, todaysUsage, null, null, false, theCase.CaseNumber);

                        }                               // if (hasIncEliminationPeriod)
                    }                                   // foreach (Time timeOut in countWhichDays)

                    // if it is an exclusive elimination period and it has not been met then return
                    if (hasExcEliminationPeriod && !wasExcEliminationPeriodExeceeded)
                        return;

                    // if it is an exclusive elimination period and has been met then take the days off
                    // the start of the count which days, so we can skip them
                    if (hasExcEliminationPeriod)
                        countWhichDays = countWhichDays.Where(cwd => cwd.SampleDate > maxEliminationDay).ToList();

                    // if it is an inclusive elim period and it has not been met set them all to exhausted
                    // and return
                    if (hasIncEliminationPeriod && !wasIncEliminationPeriodExceeded)
                    {
                        // we did not exceed the use so move our temporary calcs to the permanent denied
                        foreach (AppliedPolicyUsage elimmed in incEliminationTotals.Allocated)
                        {
                            elimmed.Determination = AdjudicationStatus.Denied;
                            elimmed.DenialReasonCode = AdjudicationDenialReason.EliminationPeriod;
                            elimmed.DenialReasonName = AdjudicationDenialReason.EliminationPeriodDescription;
                            elimmed.DateTimeAdded = DateTime.Now;
                            deniedTotal.Allocated.Add(elimmed);

                            ap.Usage.Add(elimmed);
                        }

                        return;
                    }                               /// if (hasIncEliminationPeriod && !wasIncEliminationPeriodExceeded)

                }                                       // if (hasExcEliminationPeriod || hasIncEliminationPeriod)

                //We have processed the first segment of a Policy so set the flag true
                //so elimination doesn't apply on second segment
                lstOfPolicyinCase.ForEach(m =>
                {
                    if (m.Policy.Code == ap.Policy.Code && !m.IsEliminationApplied)
                    {
                        m.IsEliminationApplied = true;
                    }
                });
            }
            #endregion elim period

            // we may need to carry forward a status from the previous day
            // these hold that prior status
            string priorDayDenialReasonCode = null;
            string priorDayDenialReasonName = null;
            string priorDayDenialOther = null;
            AdjudicationStatus priorDayStatus = AdjudicationStatus.Pending;



            // determine if the first work day of the case is already exhausted and then mark
            // all the days up to that one as exhausted (this is really to catch requests
            // starting on non-workdays
            // this bool is only required because we don't do any calcs for holidays, weekends
            // or other non work days. If set to try it is check in the 0 work for the day section
            // and if true creates an exhausted recored. It is set back to false the first
            // time it makes it past the holiday / 0 hour schedule check
            if (countWhichDays != null && countWhichDays.Count > 0)
            {
                // do a calc and see if it is exhausted



                List<AppliedPolicyUsageType> exhaustTestUsage = theHelper.GetUsage(countWhichDays[0].SampleDate, ap.PolicyBehaviorType, holidays);
                AppliedPolicyUsage exhuastedUsage = new AppliedPolicyUsage()
                {
                    DateUsed = countWhichDays[0].SampleDate,
                    MinutesInDay = countWhichDays[0].TotalMinutes.Value,
                    MinutesUsed = 0,
                    AvailableToUse = exhaustTestUsage
                };


                CalcUsage alreadyExhausted = new CalcUsage(exhuastedUsage, put, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, theSegment.Type);

                // just set the flag that says this is used up. logic in the loop will take care of the rest
                //for WentOvetTheLimit we can set the entitlementUsedUp to true, but if on current date we are reaching the Limit
                //that doesn't mean we exhausted . We need to use the current date and the if any other then get exhausted
                entitlementUsedUp = alreadyExhausted.WentOverTheLimit;
                if (entitlementUsedUp)
                {
                    priorDayDenialReasonCode = AdjudicationDenialReason.Exhausted;
                    priorDayDenialReasonName = AdjudicationDenialReason.ExhaustedDescription;
                    priorDayStatus = AdjudicationStatus.Denied;
                }
            }

            string entitlementUsedUpReasonCode = AdjudicationDenialReason.Exhausted;
            string entitlementUsedUpReasonName = AdjudicationDenialReason.ExhaustedDescription;
            bool priorDayExceeded = false;
            weekDaysCount = 0;
            totalDaysCount = 0;
            int daysCount = 0;

            // now walk across the abscences collection (it is filled with what they are taking)
            // and the difference between their schedule and the absceces is thier time off
            // do this and apply the minutes util gone (then create denied rows) or out of abscence days
            foreach (Time timeOut in countWhichDays)
            {
                // for a break point (sorry) (this is just in the check in cuz I figure I'll need this a few more times)
                // feel free to rip it out when no longer useful (I couldn't set the conditional the way I wanted and didn't
                // want to spend a lot of time trying)

                //Console.WriteLine(timeOut.SampleDate);
                // now check the list of stuff we saved and see if we find a user override for today
                // this can be used here, if the amount as already gone to payroll then there is nothing
                // to do just copy the values, other wise it is used farther down to set the status for the day

                AppliedPolicyUsage theUserSaid = userOverrides.FindUsage(theSegment.Id, ap.Id, timeOut.SampleDate);
                bool userOverride = false;

                // if the user didn't say then the default is pending and there is nothing else to think about
                if (theUserSaid == null)
                {
                    theUserSaid = new AppliedPolicyUsage()
                    {
                        Determination = AdjudicationStatus.Pending,
                        IsLocked = false
                    };
                    priorDayStatus = AdjudicationStatus.Pending;
                }
                else
                {
                    // if it was submitted to payroll then there is nothing to do but copy it
                    if (theUserSaid.IsLocked.HasValue && theUserSaid.IsLocked.Value)
                    {
                        ap.Usage.Add(theUserSaid);
                        if (theUserSaid.Determination == AdjudicationStatus.Denied)
                            deniedTotal.AddAllocated(theUserSaid.DateUsed, theUserSaid.MinutesUsed,
                                theUserSaid.MinutesInDay, policyReasonCode, theUserSaid.Determination,
                                theUserSaid.DenialReasonCode, null, theUserSaid.AvailableToUse, null,
                                theUserSaid.DenialReasonName, userOverride, theCase.CaseNumber);
                        else
                            put.AddAllocated(theUserSaid.DateUsed, theUserSaid.MinutesUsed, theUserSaid.MinutesInDay,
                                policyReasonCode, theUserSaid.Determination, theUserSaid.DenialReasonCode,
                                null, theUserSaid.AvailableToUse, null, null, userOverride, theCase.CaseNumber);

                        // do the next day because there is nothing else to do for this one
                        continue;
                    }
                }

                // if our date crosses the calc boundry, clear out our usage totals and start them all over again
                if (timeOut.SampleDate >= put.NextPeriodStartDate)
                {

                    // clear out what we have used and set the date up by the period span
                    put.RollToNextPeriod(timeOut.SampleDate, ap);
                    deniedTotal.RollToNextPeriod(timeOut.SampleDate, ap);

                    // the period starts again
                    entitlementUsedUp = false;
                    if (put.MeasurePeriodType == PeriodType.FixedResetPeriod)
                    {
                        priorDayExceeded = false;
                    }
                }

                else if (put.MeasurePeriodType == PeriodType.PerCase && put.MeasurePeriodType == PeriodType.PerOccurrence)
                {
                    // the period starts again
                    entitlementUsedUp = false;
                }                

                else if (put.MeasurePeriodType == PeriodType.PerCase)
                {
                    // the period starts again
                    entitlementUsedUp = false;
                }

                // the abscences are at the segment level and are applied across all applied policies
                // so if this abscense date is not in the range of the applied policy then skip it
                if (!timeOut.SampleDate.DateInRange(ap.StartDate, ap.EndDate))
                    continue;


                // break down the usage into parts that can be measured against
                List<AppliedPolicyUsageType> todaysUsage = theHelper.GetUsage(timeOut.SampleDate, ap.PolicyBehaviorType, holidays);

                // find the absence date on our work schedule
                Time normalWorkDay = null;
                if (regularTimes != null)
                {
                    normalWorkDay = regularTimes.FirstOrDefault(nwd => nwd.SampleDate == timeOut.SampleDate);
                    // For Reduced weekly schedule exclude this condition otherwise full day reduced value reset again
                    if (theSegment.Type != CaseType.Reduced)
                    {
                        if ((timeOut.TotalMinutes == 0 && normalWorkDay.TotalMinutes > 0)
                        || (timeOut.TotalMinutes > 0 && normalWorkDay.TotalMinutes == 0))
                        {
                            timeOut.TotalMinutes = normalWorkDay.TotalMinutes;
                        }
                    }
                }
                // we didn't find a day. It's not a normal work day for them, so skip it
                // unless it is a calendar type and then every day counts
                if (normalWorkDay == null && !ap.PolicyReason.EntitlementType.Value.CalendarType())
                    continue;

                // So, if it's null and it's a calendar type, we don't have it in the collection, so @##$%, we need to create a fake one @ 0 minutes.
                if (normalWorkDay == null)
                    normalWorkDay = new Time() { TotalMinutes = 0, SampleDate = timeOut.SampleDate, Id = Guid.NewGuid() };

                if (ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays)
                {
                    AdjustFixedWorkDaysChanges(startDayOfWeek, fixedWorkDaysPerWeek, timeOut, normalWorkDay, ref weekDaysCount, ref totalDaysCount);
                }

                // now, if the day is a holiday, 0 out the time so that we will create a 0 use row that can
                // have a status set so that it does not create a break in the date range
                bool isHoliday = false;

                if (ap.PolicyBehaviorType != PolicyBehavior.Ignored)
                {
                    isHoliday = holidays.Any(h => h == timeOut.SampleDate);
                }
                // the total to use is the diference between the scheduled minutes and their projected minutes
                // remember on calendar types we use all or nothing so it's just a 1
                double totalRequested = (timeOut.TotalMinutes ?? 0);
                if (ap.PolicyReason.EntitlementType.Value.CalendarType())
                {
                    totalRequested = 1;
                    normalWorkDay.TotalMinutes = 1;
                }

                // yes, if somehow they have requested more time off then the should be allowed
                // then this will be less than 0. e.g. normal 8 hour day and 10 hours off are requested. That's not allowed
                // so default it to their normal work day
                if (totalRequested < 0)
                    totalRequested = (normalWorkDay.TotalMinutes ?? 0);

                int holidayMinutes = 0;

                // and now that we have set everything up, if today is a holiday then
                // set total requested and normal work day to 0
                if (isHoliday && ap.PolicyBehaviorType != PolicyBehavior.Ignored && ap.PolicyReason.EntitlementType != EntitlementType.FixedWorkDays)
                {
                    // need to find how many hours would have been worked if this was not a holiday
                    // for std's they can get paid for holidays, we need that to calc the payment amount
                    Time holidayHours = theWorkDays.FirstOrDefault(fd => fd.SampleDate == timeOut.SampleDate);
                    if (holidayHours != null)
                        holidayMinutes = holidayHours.TotalMinutes.Value;

                    timeOut.TotalMinutes = 0;
                    normalWorkDay.TotalMinutes = 0;
                }

                if (!userOverride && totalRequested == 0 && reachedTheLimit && entitlementUsedUp)
                {
                    entitlementUsedUp = false;
                }
                // if we are already out of minutes just create the denial and move on
                if (entitlementUsedUp)
                {
                    ap.Usage.Add(new AppliedPolicyUsage()
                    {
                        MinutesUsed = totalRequested,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        DenialReasonCode = entitlementUsedUpReasonCode,
                        DenialReasonName = entitlementUsedUpReasonName,
                        DenialExplanation = null,
                        Determination = AdjudicationStatus.Denied,
                        DateUsed = timeOut.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        CaseNumber = theCase.CaseNumber
                    });
                    deniedTotal.AddAllocated(timeOut.SampleDate, totalRequested, (normalWorkDay.TotalMinutes ?? 0),
                        policyReasonCode, AdjudicationStatus.Denied, entitlementUsedUpReasonCode, null,
                        todaysUsage, null, null, userOverride);
                    priorDayExceeded = true;
                    continue;
                }

                // 1. find past usage based on the segment type
                // 2. create a type to be used to see what today's usage calcs to as a percent
                // 3. add up the prior percentages
                // 4. add the two percentages together and if it is greater than 100% convert the remainder back into minutes for today
                double minutesUsedForPolicyAndDay = 0;

                minutesUsedForPolicyAndDay = put.PercentUsed(timeOut.SampleDate, AdjudicationStatus.Approved, ap.PolicyReason.EntitlementType.Value) +
                    put.PercentUsed(timeOut.SampleDate, AdjudicationStatus.Pending, ap.PolicyReason.EntitlementType.Value);

                // this does the work for us, and we pass it to the calc usage class
                AppliedPolicyUsage workUsage = new AppliedPolicyUsage()
                {
                    DateUsed = timeOut.SampleDate,
                    MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                    MinutesUsed = totalRequested,
                    AvailableToUse = todaysUsage,
                    PolicyReasonCode = ap.PolicyReason.ReasonCode // i know we aren't using this, but load it anyway (just in case in the future we do)
                };

                // here's a fun fact. Applied policies can overlap. So, we have to make sure
                // that we apply them in an order in which they are appropriate so check if this policy is consecutive to
                // any other policies and don't award time on this policy unless the parent policy is used up
                // and if this seems like a weird place to do this, it is OK. The elimination period
                // can be used up while a parent policy is awarding time
                if (ap.Policy.ConsecutiveTo != null && ap.Policy.ConsecutiveTo.Count > 0)
                {
                    var consecutiveToPoliciesInQuestion = theSegment.AppliedPolicies.Where(sap => ap.Policy.ConsecutiveTo.Contains(sap.Policy.Code)).ToList();
                    var maxDate = consecutiveToPoliciesInQuestion.All(p => p.Status == EligibilityStatus.Ineligible) ? DateTime.MinValue :
                        consecutiveToPoliciesInQuestion.Any() ? consecutiveToPoliciesInQuestion.Max(u => u.EndDate) : (DateTime?)null;

                    var consecutiveToPoliciesAvailableUsages = consecutiveToPoliciesInQuestion.Where(p => p.Status == EligibilityStatus.Eligible).SelectMany(p => p.Usage).Where(u => u.Determination != AdjudicationStatus.Denied);
                    DateTime consecutiveToPoliciesAvailableMaxDate = DateTime.MinValue;
                    if (consecutiveToPoliciesAvailableUsages.Any())
                        consecutiveToPoliciesAvailableMaxDate = consecutiveToPoliciesInQuestion.Where(p => p.Status == EligibilityStatus.Eligible).SelectMany(p => p.Usage).Where(u => u.Determination != AdjudicationStatus.Denied).Max(u => u.DateUsed);
                    // check all the parent policies and see if there is a denied record for today
                    // if there is no parent, that is OK, then we can continue, but, 
                    // if there is a parent and no denied then this policy can not be applied.
                    // (we were good and collected all policies with denials on them into one spot earlier)
                    if (!parentDeniedPolicies.Any(pdp11 => pdp11.Allocated.Any(pdpdu => pdpdu.DateUsed == timeOut.SampleDate)))
                    {
                        // Determine if we're moving beyond the end date of the prior policy which would not be recorded
                        // in the denied totals. If prior policy has not ended, skip the rest .                                                
                        // If **ALL** of the consecutive to policies are ineligible, then we should allow this one to go, either way.
                        if ((maxDate.HasValue ? maxDate.Value : DateTime.MaxValue) >= timeOut.SampleDate)
                        {
                            continue; // next day off
                        }
                    }
                    //If bunch of days are denied towards the end of the consecutive to policy, then consectuive policy should begin from
                    //first denied day of consecutive to policy. This condition will ensure there are no pending or approval 
                    //usage after consecutive policy denied dates. 
                    else if (consecutiveToPoliciesInQuestion.SelectMany(p => p.Usage).Any(u => u.DateUsed == timeOut.SampleDate) && consecutiveToPoliciesAvailableMaxDate >= timeOut.SampleDate)
                    {
                        continue;
                    }
                }
                //daysCount need to be increased after consecutive check is done .
                daysCount++;
                // now calculate the entitlement
                CalcUsage entitlementUsage = new CalcUsage(workUsage, put, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, theSegment.Type, gapUsageCount, timeOut.SampleDate, null, daysCount);

                if (entitlementUsage.WentOverTheLimit && (ap.PolicyBehaviorType == PolicyBehavior.ReducesWorkWeek || ap.PolicyBehaviorType == PolicyBehavior.IgnoredNoLostTime))
                {
                    if(entitlementUsage.AdjustCalcUsageForHolidays(put, holidays, putForHolidays, timeOut.SampleDate))
                    {
                        entitlementUsage = new CalcUsage(workUsage, put, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, theSegment.Type, gapUsageCount,  timeOut.SampleDate );
                    }                    
                }

                //entitlementUsedUp = (entitlementUsage.WentOverTheLimit);
                reachedTheLimit = false;
                if (entitlementUsage.ReachedTheLimit)
                {
                    reachedTheLimit = true;
                }
                entitlementUsedUp = (entitlementUsage.ReachedTheLimit || entitlementUsage.WentOverTheLimit);

                // this defaults to exhausted, but it can change as we move along
                string denailReasonCode = null;
                string denialReasonName = null;
                string denialOther = null;

                if (entitlementUsage.WentOverTheLimit)
                    denailReasonCode = AdjudicationDenialReason.Exhausted;

                // and if there is a per use cap then do something with that.
                if (hasUsageCap && !entitlementUsedUp)
                {
                    CalcUsage usageCap = new CalcUsage(workUsage, put, ap.PolicyReason.PerUseCapType.Value, ap.PolicyReason.PerUseCap.Value, ap, false, theSegment.Type,  gapUsageCount);

                    // if we went hit the cap then set the flags
                    // and if we went over the cap, then fix some things up
                    if (usageCap.WentOverTheLimit || usageCap.ReachedTheLimit)
                    {
                        entitlementUsedUpReasonCode = AdjudicationDenialReason.PerUseCap;           // this is for the short circuit logic
                        entitlementUsedUpReasonName = AdjudicationDenialReason.PerUseCapDescription;
                        denailReasonCode = AdjudicationDenialReason.PerUseCap;                      // this is for this pass
                        denialReasonName = AdjudicationDenialReason.PerUseCapDescription;
                        denialOther = null;
                        entitlementUsedUp = true;
                        // in a nice little twist of fate, when we hit the cap we can just substitute the calc that did
                        // cap and it will all work out
                        if (usageCap.WentOverTheLimit)
                        {
                            entitlementUsage = usageCap;
                        }
                    }                               // if (usageCap.WentOverTheLimit || usageCap.ReachedTheLimit)
                }                                   // if(hasUsageCap)


                // Default status is always pending, the user is the only one who can approve time
                AdjudicationStatus setDeterminationTo = AdjudicationStatus.Pending;

                // the entitlement usage as down the heavy lifting for us, but first we have
                // to see if the user has overriden anything
                double pendingAndAllowed = entitlementUsage.NewTotal;
                double deniedAmount = 0;

                if (entitlementUsage.WentOverTheLimit)
                {
                    deniedAmount = entitlementUsage.DeniedCuzOverTheLimit;
                }
                else
                {
                    if (!entitlementUsage.ReachedTheLimit && pendingAndAllowed > 0 && priorDayExceeded)
                    {
                        priorDayExceeded = false;
                    }
                }



                // if the user said it's approved then by dog it is approved
                // since this is consecutive / reduced only whole (scheduled) days are approved so 
                // it's all or nothing


                //Directly Assign theUserSaid.UserEntered to userOverride , 
                //if theUserSaid is null we are creating a new object and setting
                //its value to Pending Line 2161 .So Determination could be either Approved or Pending
                userOverride = theUserSaid.UserEntered;
                setDeterminationTo = theUserSaid.Determination;

                // if there is pending but the user said deny, then we really need to check what is going on
                // if it is set to exhaustion then flip it back to pending because something on the leave changed
                // so it is being recalced, any other reason then reassign it
                if (theUserSaid.Determination == AdjudicationStatus.Denied)
                {
                    if (pendingAndAllowed > 0 && theUserSaid.DenialReasonCode == AdjudicationDenialReason.Exhausted && !theUserSaid.UserEntered)
                    {
                        setDeterminationTo = AdjudicationStatus.Pending;
                    }
                    else
                    {
                        // if the user denied it and then it stays denied (we'll even let the total adjust because no means no)
                        denailReasonCode = theUserSaid.DenialReasonCode;
                        denialReasonName = theUserSaid.DenialReasonName;
                        denialOther = theUserSaid.DenialExplanation;
                        userOverride = true;
                        deniedAmount = entitlementUsage.NewTotal;
                        //deniedAmount = theUserSaid.MinutesUsed;
                        setDeterminationTo = AdjudicationStatus.Denied;

                        // now we have to reduce what was allowed by how much the user denied and don't let it go negative
                        pendingAndAllowed = Math.Max(pendingAndAllowed - deniedAmount, 0);
                    }
                }

                //Another twist, its first day of Case and a non-work day , and funny part entitlementUsage reached the limit 
                //due to prior usage from other cases , So first day  should be denied,hence no point in putting pressure on calc engine ,
                //just set the priorDayExceeded to true rest all already present in the system so no point deduplicate again
                if (pendingAndAllowed == 0 && timeOut.SampleDate == theCase.StartDate && entitlementUsage.ReachedTheLimit)
                {
                    priorDayExceeded = true;
                }
                // now the approved amount can go to appoved (and remember we can have approved and denied on the same day
                // that's where ther are two ifs in a row and not and else (just in case you think this is a mistake)
                // if the user wants us to force something, then force it

                // first on days when there is exhaustion, then that is the only choice
              
                if (entitlementUsage.WentOverTheLimit || priorDayExceeded ||
                  (entitlementUsage.ReachedTheLimit && priorDayExceeded))
                {

                    //Hey we have reached Limit, what about prior day is it denied Exhausted but non-work day
                    //then its not right , we should make it with the current status, till we found work day and denied
                    //We will force the calcualtion only if usage conditon satisfied
                    //In scenario where we reached the Limit on a day(that day will remain pending) and next day is non work-day 
                    //we are setting it Exhausted while creating case , which is  not correct it should be "Pending" also.



                    denailReasonCode = AdjudicationDenialReason.Exhausted;
                    denialReasonName = AdjudicationDenialReason.ExhaustedDescription;

                    ap.Usage.Add(new AppliedPolicyUsage()
                    {
                        MinutesUsed = deniedAmount,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        DenialReasonCode = denailReasonCode,
                        DenialReasonName = denialReasonName,
                        DenialExplanation = denialOther,
                        Determination = AdjudicationStatus.Denied,
                        DateUsed = timeOut.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        CaseNumber = theCase.CaseNumber,
                        UserEntered = userOverride ? theUserSaid.Determination == AdjudicationStatus.Denied : userOverride
                    });
                    deniedTotal.AddAllocated(timeOut.SampleDate, deniedAmount, (normalWorkDay.TotalMinutes ?? 0),
                        policyReasonCode, AdjudicationStatus.Denied, denailReasonCode, null,
                        todaysUsage, null, null, userOverride);

                    priorDayExceeded = true;

                    // all done here, next day
                    continue;
                }

                priorDayExceeded = false;

                if (pendingAndAllowed > 0 || (userOverride && setDeterminationTo == AdjudicationStatus.Approved))
                {
                    // save in case we need for the next processed day (but don't save a user override)

                    priorDayStatus = setDeterminationTo;
                    priorDayDenialReasonCode = null;
                    priorDayDenialReasonName = null;
                    priorDayDenialOther = null;


                    ap.Usage.Add(new AppliedPolicyUsage()
                    {
                        MinutesUsed = pendingAndAllowed,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        Determination = setDeterminationTo,
                        DateUsed = timeOut.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        CaseNumber = theCase.CaseNumber,
                        UserEntered = userOverride,
                    });

                    put.AddAllocated(timeOut.SampleDate, pendingAndAllowed, (normalWorkDay.TotalMinutes ?? 0),
                        policyReasonCode, setDeterminationTo, null, null,
                        todaysUsage, null, null, userOverride);

                    //Hey we got a Approved , what about prior day is it denied Exhausted but non-work day
                    //then its not right , we should make it approved till we found work day and denied
                    //We will force the calcualtion only if usage conditon satisfied
                    if (ap.Usage.FirstOrDefault(u => u.Determination == AdjudicationStatus.Denied
                    && u.MinutesUsed == 0 && u.DenialReasonCode == AdjudicationDenialReason.Exhausted) != null)
                    {
                        SetNonWorkDayToApprovedOnlyForLookBack(timeOut.SampleDate, ap, deniedTotal, put,
                            setDeterminationTo, regularTimes);
                    }



                    // the prior day wasn't really exceeded but we reached the max, so that
                    //is really the same thing
                    if (entitlementUsage.ReachedTheLimit)
                    {
                        priorDayStatus = AdjudicationStatus.Denied;
                        priorDayDenialReasonCode = AdjudicationDenialReason.Exhausted;
                    }
                }


                // see.... this is the part that is denied (or the user is forcing a denied)
                if (deniedAmount > 0 || (userOverride && setDeterminationTo == AdjudicationStatus.Denied))
                {
                    // save in case we need for the next processed day (but not when the user overrides)
                    if (!userOverride)
                    {
                        priorDayDenialReasonCode = denailReasonCode;
                        priorDayDenialReasonName = denialReasonName;
                        priorDayDenialOther = denialOther;
                        priorDayStatus = AdjudicationStatus.Denied;
                    }
                    else
                    {
                        //this happens, because when u do change case and extend the end date
                        //selecting this is new end date if u go just before the loop
                        //there is if userOverride condition there priorday status is set to pending they should pick up the status of the last working day o even if the prior day is denied
                        //the following non working day becomes pending it should be denied


                        //Its got something to do with non working day, which should be denied, 
                        //if prior day is denied when the day in question is not a user override


                        AppliedPolicyUsage priorDayPolicyUsage = put.Allocated.FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                        if (priorDayPolicyUsage == null)
                        {
                            priorDayPolicyUsage = deniedTotal.Allocated.FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                        }
                        if (priorDayPolicyUsage != null)
                        {
                            priorDayStatus = priorDayPolicyUsage.Determination;
                            priorDayDenialReasonCode = priorDayPolicyUsage.DenialReasonCode;
                            priorDayDenialReasonName = priorDayPolicyUsage.DenialReasonName;
                            priorDayDenialOther = priorDayPolicyUsage.DenialExplanation;
                        }
                    }
                    ap.Usage.Add(new AppliedPolicyUsage()
                    {
                        MinutesUsed = deniedAmount,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        DenialReasonCode = denailReasonCode,
                        DenialReasonName = denialReasonName,
                        DenialExplanation = denialOther,
                        Determination = AdjudicationStatus.Denied,
                        DateUsed = timeOut.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        CaseNumber = theCase.CaseNumber,
                        UserEntered = userOverride
                    });
                    deniedTotal.AddAllocated(timeOut.SampleDate, deniedAmount, (normalWorkDay.TotalMinutes ?? 0),
                        policyReasonCode, AdjudicationStatus.Denied, denailReasonCode, null,
                        todaysUsage, null, null, userOverride);

                }

                // who knew that a leave could be in a quantum super-postion? but... here it is
                // we do not have pending, and we do not have denied. Y? Because today is not a workday
                // so we carry the status over from the day before (kinda-sorta)
                if (deniedAmount == 0 && pendingAndAllowed == 0 && !userOverride)
                {
                    // zero usage days are a royal pain in the butt (Thanks Chad!)
                    // so, if today is a zero usage and we have reached the limit
                    // we are now going to call it exhausted and set the prior day
                    // exhausted flag
                    if (entitlementUsage.ReachedTheLimit)
                    {
                        priorDayDenialReasonCode = AdjudicationDenialReason.Exhausted;
                        priorDayDenialReasonName = AdjudicationDenialReason.ExhaustedDescription;
                        priorDayStatus = AdjudicationStatus.Denied;

                        if (!priorDayExceeded && !userOverride && !theCase.IsInquiry)
                        {
                            AppliedPolicyUsage priorDayPolicyUsage = put.Allocated.OrderByDescending(o => o.DateTimeAdded).FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                            if (priorDayPolicyUsage == null)
                            {
                                priorDayPolicyUsage = deniedTotal.Allocated.OrderByDescending(o => o.DateTimeAdded).FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                            }
                            if (priorDayPolicyUsage != null &&
                                priorDayPolicyUsage.Determination != AdjudicationStatus.Approved
                                    && !priorDayPolicyUsage.UserEntered)
                            {

                                priorDayStatus = priorDayPolicyUsage.Determination;
                                priorDayDenialReasonCode = priorDayPolicyUsage.DenialReasonCode;
                                priorDayDenialReasonName = priorDayPolicyUsage.DenialReasonName;
                                priorDayDenialOther = priorDayPolicyUsage.DenialExplanation;

                            }
                        }
                        if (priorDayStatus == AdjudicationStatus.Denied && priorDayDenialReasonCode == AdjudicationDenialReason.Exhausted)
                        {
                            priorDayExceeded = true;
                        }
                    }
                    else
                    {
                        //this happens, because when u do change case and extend the end date
                        //selecting this is new end date if u go just before the loop
                        //there is if userOverride condition there priorday status is set to pending they should pick up the status of the last working day o even if the prior day is denied
                        //the following non working day becomes pending it should be denied


                        //Its got something to do with non working day, which should be denied, 
                        //if prior day is denied when the day in question is not a user override

                        //if a new case is starting on a zero usage day which was earlier denied due to automatic calc
                        //movement to handle weekend should not be denied, it should be pending
                        if (timeOut.SampleDate != theCase.StartDate)
                        {
                            AppliedPolicyUsage priorDayPolicyUsage = put.Allocated.OrderByDescending(o => o.DateTimeAdded).FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                            if (priorDayPolicyUsage == null)
                            {
                                priorDayPolicyUsage = deniedTotal.Allocated.OrderByDescending(o => o.DateTimeAdded).FirstOrDefault(p => p.DateUsed == timeOut.SampleDate.AddDays(-1));
                            }
                            if (priorDayPolicyUsage != null &&
                                priorDayPolicyUsage.Determination != AdjudicationStatus.Approved
                                    && !priorDayPolicyUsage.UserEntered)
                            {

                                priorDayStatus = priorDayPolicyUsage.Determination;
                                priorDayDenialReasonCode = priorDayPolicyUsage.DenialReasonCode;
                                priorDayDenialReasonName = priorDayPolicyUsage.DenialReasonName;
                                priorDayDenialOther = priorDayPolicyUsage.DenialExplanation;
                            }
                        }
                    }
                    // but... if today has 0 usage because it is a holiday, they get paid for that, just to really mess with you
                    priorDayStatus = String.IsNullOrEmpty(priorDayDenialReasonCode) ? priorDayStatus : AdjudicationStatus.Denied;
                    ap.Usage.Add(new AppliedPolicyUsage()
                    {
                        MinutesUsed = 0,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        DenialReasonCode = priorDayDenialReasonCode,
                        DenialReasonName = priorDayDenialReasonName,
                        DenialExplanation = priorDayDenialOther,
                        Determination = priorDayStatus,
                        DateUsed = timeOut.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        CaseNumber = theCase.CaseNumber,
                        UserEntered = userOverride
                    });

                    // then add it to the correct collection
                    if (priorDayStatus == AdjudicationStatus.Denied)
                        deniedTotal.AddAllocated(timeOut.SampleDate, 0, 0, policyReasonCode, priorDayStatus,
                            priorDayDenialReasonCode, null, todaysUsage, priorDayDenialOther, priorDayDenialReasonName);
                    else
                        put.AddAllocated(timeOut.SampleDate, 0, 0, policyReasonCode, priorDayStatus, null, null, todaysUsage);
                }
            }
        }


        /// <summary>
        /// Loop through AppliedPolicyUsage colletion and set non work day to Approved if its state
        /// is Denied Exhausted ,once you get a workday came out of loop
        /// 
        /// </summary>
        /// <param name="lookBackStartDate"></param>
        /// <param name="lstAppliedUsage"></param>
        private void SetNonWorkDayToApprovedOnlyForLookBack(DateTime lookBackStartDate,
            AppliedPolicy apToLoopBack, PolicyUsageTotals deniedTotal,
            PolicyUsageTotals allocatedTotal, AdjudicationStatus setDeterminationTo, List<Time> regularTimes)
        {
            //We need to loopback the Usage collection  for any non-work day with denied status
            int dayCount = 0;
            for (int iCounter = apToLoopBack.Usage.Count - 1; iCounter >= 0; iCounter--)
            {
                dayCount = dayCount + 1;
                DateTime lookBackDate = lookBackStartDate.AddDays(-dayCount);
                var usage = apToLoopBack.Usage.FirstOrDefault(u => u.DateUsed == lookBackDate);
                if (usage != null)
                {

                    //if previous day is working day and denied then we can't do anything ,
                    if (usage.MinutesUsed > 0)
                    {
                        return;
                    }
                    //In Case of Auto denied we are not setting the MinutesUsed, so lets check from the schedule if its
                    //workday or not
                    Time normalWorkDay = null;
                    if (regularTimes != null)
                    {
                        normalWorkDay = regularTimes.FirstOrDefault(nwd => nwd.SampleDate == lookBackDate);
                        if (normalWorkDay.TotalMinutes > 0)
                        {
                            return;
                        }
                    }
                    ////Check if the previous day is Denied and non working day
                    ////We should set the passed the setDeterminationTo it could be pending or approved
                    if (usage.Determination == AdjudicationStatus.Denied
                        && usage.DenialReasonCode == AdjudicationDenialReason.Exhausted
                        && usage.MinutesUsed == 0)
                    {
                        usage.Determination = setDeterminationTo;
                        usage.DenialReasonCode = string.Empty;
                        usage.DenialReasonName = string.Empty;
                        //Remove earlier value from Denied Collection and add it to Allocated one
                        deniedTotal.Allocated.RemoveAll(ap => ap.DateUsed == usage.DateUsed);
                        allocatedTotal.AddAllocated(usage.DateUsed, 0, 0,
                            !string.IsNullOrEmpty(apToLoopBack.PolicyReason.ReasonCode) ? apToLoopBack.PolicyReason.ReasonCode : apToLoopBack.PolicyReason.Reason.Code,
                            usage.Determination, null, null, usage.AvailableToUse);

                    }
                }
            }

        }

        /// <summary>
        /// take the schedule on the case segment and turn it into a materilzed 
        /// date range working schedule
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        private void MaterializeReducedSchedule(CaseSegment theSegment, Case theCase, PolicyBehavior policyBehavior = PolicyBehavior.ReducesWorkWeek)
        {
            // use the same calc to materialize the schedule, just swap in the
            // reduced schedule, calc and put it back
            List<Schedule> empSched = Employee.GetById(theCase.Employee.Id)?.WorkSchedules ?? new List<Schedule>(0);
            LeaveOfAbsence los = new LeaveOfAbsence
            {
                Case = theCase,
                Employee = theCase.Employee,
                Employer = theCase.Employee.Employer,
                Customer = theCase.Employee.Customer,
                WorkSchedule = empSched
            };

            // build a list of the regular work schedule

            List<Time> normalSchedule = los.MaterializeSchedule(theSegment.StartDate, theSegment.EndDate.Value, schedules: empSched, policyBehavior: policyBehavior);
            int? totalWorkDays = los.TotalWorkDaysInWeek(theSegment.StartDate, theCase.Employee.StartDayOfWeek ?? DayOfWeek.Monday, PolicyBehavior.Ignored);
            List<Time> reducedSchedule = los.MaterializeSchedule(theSegment.StartDate, theSegment.EndDate.Value, schedules: theSegment.LeaveSchedule, policyBehavior: policyBehavior, caseId: theCase.Id, totalWorkDays: totalWorkDays);

            theSegment.Absences = new List<Time>();

            // now, reduced schedule is the driver, because there will be a work day for
            // each one of these
            foreach (Time t in reducedSchedule)
            {
                int totalMinutes = 0;
                if (normalSchedule != null)
                {
                    Time pt = normalSchedule.FirstOrDefault(rs => rs.SampleDate == t.SampleDate);
                    if (pt != null)
                    {
                        totalMinutes = pt.TotalMinutes.Value;
                    }
                }

                // you can't work more than your normal so adjust it (in case their normal day is 8 and somehow their reduced is 10)
                theSegment.Absences.Add(new Time() { SampleDate = t.SampleDate, TotalMinutes = Math.Max(totalMinutes - t.TotalMinutes.Value, 0) });
            }
        }
        
        /// <summary>
        /// build the Work Schedule with all the time set to 0
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        private void CopyWorkSchedule(CaseSegment theSegment, Case theCase)
        {
            // create and assign a new schedule for this segment based on the 
            // employee's schedule
            LeaveOfAbsence los = new LeaveOfAbsence();
            los.Case = theCase;
            los.Employee = theCase.Employee;
            los.Employer = theCase.Employee.Employer;
            los.Customer = theCase.Employee.Customer;
            los.WorkSchedule = Employee.GetById(theCase.Employee.Id).WorkSchedules;

            theSegment.Absences = los.MaterializeSchedule(theSegment.StartDate, theSegment.EndDate.Value);
        }

        /// <summary>
        /// build the Work Schedule for Reduced
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        private void CopyWorkScheduleForReduced(CaseSegment theSegment, Case theCase)
        {
            // create and assign a new schedule for this segment based on the 
            // employee's schedule
            if (theSegment.LeaveSchedule == null || !theSegment.LeaveSchedule.Any())
                theSegment.LeaveSchedule = Employee.GetById(theCase.Employee.Id).WorkSchedules;

        }

        /// <summary>
        /// Make sure that the dates nest correctly all the way down.
        /// 
        /// (It's turtles all the way down)
        /// 
        /// Throws an exception if there is an error
        /// </summary>
        /// <param name="theCase"></param>
        public void ValidateCaseDates(Case theCase)
        {
            if (theCase == null)
                throw new AbsenceSoftException("CaseService: ValidateCaseDates null policy");

            // the case start and end are the anchor dates for the segments
            // and none of the nested dates can overlap
            // (I know these are long variable names, but I'll confuse myself if I
            // try to get clever naming them) 
            DateTime previousSegmentStartDate = DateTime.MinValue;
            DateTime? previousSegmentEndDate = DateTime.MinValue;


            //if the case start date is earlier than 50 years throw an exception
            if (theCase.StartDate < DateTime.Now.AddYears(-50))
            {
                throw new AbsenceSoftException("CaseService: ValidateCaseDates StartDate earlier than 50 years");
            }

            foreach (CaseSegment cs in theCase.Segments)
            {
                // check to make sure it is within the case date
                if (!cs.StartDate.DateRangesOverLap(cs.EndDate, theCase.StartDate, theCase.EndDate))
                    throw new AbsenceSoftException("CaseService: ValidateCaseDates case segments dates outside of case dates");

                // first pass they are both min, so after that check them                
                if (previousSegmentEndDate.HasValue && previousSegmentStartDate != DateTime.MinValue && previousSegmentEndDate != DateTime.MinValue)
                {
                    if (cs.StartDate.DateRangesOverLap(cs.EndDate, previousSegmentStartDate, previousSegmentEndDate))
                        throw new AbsenceSoftException("CaseService: ValidateCaseDates case segments overlap");
                }

                // set them (even though they are named previous well use them for our future tests
                previousSegmentStartDate = cs.StartDate;
                previousSegmentEndDate = cs.EndDate;

                foreach (AppliedPolicy ap in cs.AppliedPolicies)
                {
                    if (!ap.StartDate.DateRangesOverLap(ap.EndDate, previousSegmentStartDate, previousSegmentEndDate))
                        throw new AbsenceSoftException("CaseService: ValidateCaseDates applied policy (" + ap.Policy.Code + "),enddate " + ap.EndDate.Value.ToShortDateString() + ",is outside of segment " +
                             "range which start " + previousSegmentStartDate.ToShortDateString() +
                             " end " + (previousSegmentEndDate.HasValue ? previousSegmentEndDate.Value.ToShortDateString() : ""));

                    // we can have the same date twice but not the same date and status 
                    // this is because an employee can run out of hours half way through a day 
                    // if the policy reason is working hour based
                    DateTime previousDateUsed = DateTime.MinValue;
                    AdjudicationStatus previousAdjStatus = AdjudicationStatus.Pending;            // just pick one, it's ok cuz on the first pass the date won't be min

                    // we have reached the last turtle
                    foreach (AppliedPolicyUsage apu in ap.Usage.OrderBy(o => o.DateUsed).ThenBy(o => o.Determination).ToList())
                    {
                        // make sure the the dates are valid
                        if (!apu.DateUsed.DateInRange(ap.StartDate, ap.EndDate))
                            throw new AbsenceSoftException("CaseService: ValidateCaseDates applied policy usage date outside of applied policy range");

                        previousDateUsed = apu.DateUsed;
                        previousAdjStatus = apu.Determination;
                    }
                }
            }
        }


        /// <summary>
        /// Gets the list of contacts of an case.
        /// </summary>
        /// <param name="caseId">The employee Id to get the contacts for</param>
        /// <returns>A list of contacts of this case</returns>
        public List<EmployeeContact> GetContacts(string caseId)
        {
            using (new InstrumentationContext("CaseService.GetContacts"))
            {
                var contact = (Case.GetById(caseId) ?? new Case()).Contact;
                if (contact == null)
                    return new List<EmployeeContact>();
                return new List<EmployeeContact>() { contact };
            }
        } //List<EmployeeContact> GetContacts(string employeeId)

        /// <summary>
        /// Updates contact on cases where contact is added.
        /// </summary>
        /// <param name="contact">The EmployeeContact for which DOB needs to be updated on related cases</param>
        /// <param name="update">Flag is used to determine  the RunEligibility</param>
        public void UpdateContactOnAssociatedCases(EmployeeContact contact, bool update = false)
        {
            IEnumerable<Case> relatedCases = Case.AsQueryable().Where(c => c.Employee.Id == contact.EmployeeId && c.Contact.Id == contact.Id);
            using (EligibilityService e = new EligibilityService())
            {
                foreach (var relatedCase in relatedCases)
                {
                    relatedCase.Contact.Contact = contact.Contact;
                    if (update)
                    {
                        // Run the Eligibility again if DOB changes
                        Case newCase = e.RunEligibility(relatedCase).Case;
                        UpdateCase(newCase);
                    }
                    else
                    {
                        UpdateCase(relatedCase);
                    }
                }
            }
        }

        /// <summary>
        /// Checks the database to see if the employee has any open intermittent cases
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public bool EmployeeHasOpenIntermittentCase(string employeeId)
        {
            IEnumerable<Case> openEmployeeCases = GetCasesForEmployee(employeeId, CaseStatus.Open, CaseStatus.Requested);
            return openEmployeeCases.Any(c => c.Segments.Any(s => s.Type == CaseType.Intermittent));
        }

        /// <summary>
        /// Gets the employee policy summary by case identifier.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="includeStatus">The include status.</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Case not found</exception>
        public List<PolicySummary> GetEmployeePolicySummaryByCaseId(string caseId, EligibilityStatus[] includeStatus, params string[] policyCodes)
        {
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Case not found");

            // Get all of the policies that apply to this case, 'cause we only show those
            var casePolicies = (theCase.Segments ?? new List<CaseSegment>(0))
                .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
                .Where(p => includeStatus != null && includeStatus.Any() && includeStatus.Contains(p.Status))
                .Where(p => p.Policy != null && (policyCodes.Length == 0 || policyCodes.Contains(p.Policy.Code)))
                .Select(p => p.Policy.Code)
                .ToArray();
            return GetEmployeePolicySummary(theCase.Employee, DateTime.UtcNow, theCase, true, casePolicies);
        }

        /// <summary>
        /// Gets the employee policy summary for any specific Date
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="includeStatus"></param>
        /// <param name="asOfDate"></param>
        /// <returns></returns>
        public List<PolicySummary> GetEmployeePolicySummaryByCaseIdBasedOnAsOfDate(string caseId,
           EligibilityStatus[] includeStatus, DateTime? asOfDate)
        {
            Case theCase = Case.GetById(caseId);
            if (theCase == null)
                throw new AbsenceSoftException("Case not found");

            //Get all of the policies that apply to this case, 'cause we only show those
            var casePolicies = (theCase.Segments ?? new List<CaseSegment>(0))
                .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
                .Where(p => includeStatus != null && includeStatus.Any() && includeStatus.Contains(p.Status))
                .Where(p => p.Policy != null)
                .Select(p => p.Policy.Code)
                .ToArray();
            return GetEmployeePolicySummary(theCase.Employee, asOfDate.Value, theCase, true, casePolicies);
        }
        // ? why are these marked virtual?
        /// <summary>
        /// Gets the employee policy summary.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        public virtual List<PolicySummary> GetEmployeePolicySummary(string employeeId, params string[] policyCodes)
        {
            Employee emp = Employee.GetById(employeeId);
            return GetEmployeePolicySummary(emp, DateTime.UtcNow, policyCodes);
        }


        /// <summary>
        /// Gets the employee policy summary.
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        public virtual List<PolicySummary> GetEmployeePolicySummary(Employee emp, params string[] policyCodes)
        {
            return GetEmployeePolicySummary(emp, DateTime.UtcNow, policyCodes);
        }

        /// <summary>
        /// Gets the employee policy summary.
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="projectedToDate">The projected to date.</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        public virtual List<PolicySummary> GetEmployeePolicySummary(Employee emp, DateTime projectedToDate, params string[] policyCodes)
        {
            return GetEmployeePolicySummary(emp, projectedToDate, null, false, policyCodes);
        }

        /// <summary>
        /// If this is called with a current case, then when it calculates per case usage it will only
        /// use the current case. If the current case is null then it will return all the usage
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="projectedToDate">The projected to date.</param>
        /// <param name="currentCase">The current case.</param>
        /// <param name="includePerCaseUse">if set to <c>true</c> [include per case use].</param>
        /// <param name="policyCodes">The policy codes.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">emp;Employee may not be null</exception>
        public virtual List<PolicySummary> GetEmployeePolicySummary(Employee emp, DateTime projectedToDate, Case currentCase, bool includePerCaseUse, params string[] policyCodes)
        {
            if (emp == null)
                throw new ArgumentNullException("emp", "Employee may not be null");

            using (new InstrumentationContext("CaseService.GetEmployeePolicySummary"))
            {
                // Need to get this list of all policies an employee could use, merge it against all policies they have used
                //  and format it into a nice little summary thingy.
                List<Policy> employeePolicies = new EligibilityService().Using(e => e.PerformEmployeePolicySelection(emp, DateTime.UtcNow.ToMidnight()));

                // there maybe policies used that did not come up in that quick policy selection
                // get all the ones ever used by an employee and add the missing ones in
                List<Case> empsCases = Case.AsQueryable().Where(c => c.Employee.Id == emp.Id && c.EmployerId == emp.EmployerId).ToList();
                // from the cases get all the policies broken out by id
                List<Policy> pols = empsCases.SelectMany(c => c.Segments.SelectMany(s => s.AppliedPolicies.Select(ap => ap.Policy))).ToList();
                foreach (Policy pp in pols)
                {
                    if (!employeePolicies.Any(ep => ep.Code == pp.Code))
                        employeePolicies.Add(pp);
                }

                List<PolicySummary> rtVal = new List<PolicySummary>();

                // get the usage history using now as our start date
                CalculatePriorUsage priorCalc;
                if (currentCase == null)
                    priorCalc = new CalculatePriorUsage(emp.Id, projectedToDate, AdjudicationStatus.Approved);
                else
                    priorCalc = new CalculatePriorUsage(currentCase, projectedToDate, false, true, false, null, includePerCaseUse, AdjudicationStatus.Approved);

                priorCalc.RemovePostDateUsage(projectedToDate.ToMidnight());
                PolicyUsageHistory puh = priorCalc.UsageHistory; // new PolicyUsageHistory() { UsageTotals = GetPriorUsage(emp.Id, projectedToDate, AdjudicationStatus.Approved) };

                LeaveOfAbsence loa = new LeaveOfAbsence();
                loa.Case = currentCase;
                loa.Employee = emp;
                loa.Employer = emp.Employer;
                loa.Customer = emp.Customer;
                loa.WorkSchedule = Employee.GetById(emp.Id)?.WorkSchedules;

                // for each policy in the list copy it to the sumary, find out how much was allowed and how much
                // was used
                foreach (Policy p in employeePolicies)
                {
                    PolicySummary ps = new PolicySummary()
                    {
                        PolicyCode = p.Code,
                        PolicyName = p.Name,
                        TimeUsed = 0,
                        HoursUsed = (0).ToFriendlyTime(),
                        PolicyType = p.PolicyType
                    };

                    ps.TimeUsed = 0;
                    ps.HoursUsed = puh[p.Code].MinutesUsed(AdjudicationStatus.Approved).ToFriendlyTime();
                    ps.MinutesUsed = puh[p.Code].MinutesUsed(AdjudicationStatus.Approved);

                    AppliedPolicy theAPforThisPolicy = null;
                    if (currentCase == null)
                    {
                        // the employee doesn't have any cases so just "fake" one up and grab
                        // the entitilement amount from the last absence reason                            
                        AppliedPolicy theAPforThisCasePolicyTemp = empsCases
                                .SelectMany(c => c.Segments)
                                .SelectMany(s => s.AppliedPolicies)
                                .LastOrDefault(ap => ap.Policy.Code == p.Code);
                        if ((empsCases == null || empsCases.Count == 0 || (empsCases.Count > 0 && theAPforThisCasePolicyTemp == null)) && p.AbsenceReasons.Count > 0)
                        {
                            // go through all the available reasons (make an attempt to figure out the max one)
                            // and set the reason to use to that
                            int previousCloseEnoughDays = 0;
                            for (int i = 0; i < p.AbsenceReasons.Count; i++)
                            {
                                double daysInPeriod = 0;
                                switch (p.AbsenceReasons[i].EntitlementType.Value)
                                {
                                    case EntitlementType.CalendarDays:
                                    case EntitlementType.WorkDays:
                                    case EntitlementType.FixedWorkDays:
                                    case EntitlementType.CalendarDaysFromFirstUse:
                                        daysInPeriod = p.AbsenceReasons[0].Entitlement.Value;
                                        break;
                                    case EntitlementType.CalendarWeeks:
                                    case EntitlementType.WorkWeeks:
                                    case EntitlementType.CalendarWeeksFromFirstUse:
                                        daysInPeriod = p.AbsenceReasons[0].Entitlement.Value * 7;
                                        break;
                                    case EntitlementType.CalendarMonths:
                                    case EntitlementType.WorkingMonths:
                                    case EntitlementType.CalendarMonthsFromFirstUse:
                                        daysInPeriod = daysInPeriod = p.AbsenceReasons[0].Entitlement.Value * 30;
                                        break;
                                    case EntitlementType.CalendarYears:
                                    case EntitlementType.CalendarYearsFromFirstUse:
                                        daysInPeriod = p.AbsenceReasons[0].Entitlement.Value * 365.25;
                                        break;
                                    default:
                                        daysInPeriod = 1; // you have something less than a day so just round it up to a day
                                        break;
                                }

                                if (daysInPeriod > previousCloseEnoughDays)
                                {
                                    theAPforThisPolicy = new AppliedPolicy()
                                    {
                                        PolicyReason = new PolicyAbsenceReason()
                                        {
                                            EntitlementType = p.AbsenceReasons[i].EntitlementType,
                                            Entitlement = p.AbsenceReasons[i].Entitlement
                                        }
                                    };
                                }
                            }
                        }
                        else
                        {
                            theAPforThisPolicy = theAPforThisCasePolicyTemp;
                        }
                    }
                    else
                    {
                        theAPforThisPolicy = currentCase.Segments
                            .SelectMany(s => s.AppliedPolicies)
                            .FirstOrDefault(ap => ap.Policy.Code == p.Code);
                    }

                    if (theAPforThisPolicy != null)
                    {
                        PolicyAbsenceReason parForThis = theAPforThisPolicy.PolicyReason;
                        
                        ps.TimeUsed = Convert.ToDouble(puh[p.Code].PercentUsed(parForThis.EntitlementType.Value, AdjudicationStatus.Approved));
                        
                        if (parForThis.EntitlementType.Value.CalendarTypeFromFirstUse())
                        {
                            ps.TimeUsed = CalculateGapUsageInTimeTracker(theAPforThisPolicy, currentCase, puh.UsageTotals, emp.Id, ps.TimeUsed); // calculate and add gap (if any)
                        }

                        ps.HoursUsed = ps.MinutesUsed.ToFriendlyTime();
                        ps.Units = parForThis.EntitlementType.ToUnit();
                        ps.TimeUsed = Math.Round(ps.TimeUsed, 8); 
                        if (parForThis.EntitlementType == EntitlementType.ReasonablePeriod)
                        {
                            ps.TimeRemaining = -1;
                            ps.HoursRemaining = "reasonable amount";
                        }                       
                        else
                        {
                            double remaining = parForThis.Entitlement.Value - ps.TimeUsed;
                            ps.TimeRemaining = Math.Round(remaining, 8);
                            //We don't need to send remaining ,just send the total entitlement balue. we already have Minutes Used with us so just need to 
                            //calculate the total minutes for that schedule and minus the the minutes used 
                            //which will give us the hours remaining
                            ps.HoursRemaining = ConvertUnitsToMinutesBasedOnRemaining(parForThis, projectedToDate, ps, loa).ToHours();
                        }

                        //keep HoursUsed and HoursRemaining in days for EntitlementType FixedWorkDays
                        if (parForThis.EntitlementType == EntitlementType.FixedWorkDays)
                        {
                            ps.HoursUsed = string.Format("{0} {1}", ps.TimeUsed, ps.Units);
                            ps.HoursRemaining = string.Format("{0} {1}", ps.TimeRemaining, ps.Units);
                        }
                    } // if(p.AbsenceReasons != null && p.AbsenceReasons.Count > 0)

                    // Only add this to the list if it has a show type of Always and it isn't suppressed at either the customer or the employer level
                    ///or a show type of time used and there is actually time used.
                    if (p.AbsenceReasons.Any(r => (r.ShowType == PolicyShowType.Always
                        && !emp.Customer.SuppressPolicies.Contains(p.Code, StringComparer.InvariantCultureIgnoreCase)
                        && !emp.Employer.SuppressPolicies.Contains(p.Code, StringComparer.InvariantCultureIgnoreCase))
                        || (r.ShowType == PolicyShowType.OnlyTimeUsed && ps.TimeUsed > 0))
                        || policyCodes.Contains(p.Code))
                        rtVal.Add(ps);

                } // foreach (Policy p in employeePolicies)

                return rtVal.Where(p => policyCodes.Length == 0 || policyCodes.Contains(p.PolicyCode)).OrderBy(o => (int)o.PolicyType).ThenBy(o => o.PolicyName).ToList();
            }  // using InstrumentationContext
        } // GetEmployeePolicySummary        

        /// <summary>
        /// Take the work schedule based on the entitlement unit (like work weeks), and multiply time used entitlement * units,
        /// </summary>
        /// <param name="parForThis"></param>
        /// <param name="projectedStartDate"></param>
        /// <param name="ps"></param>
        /// <param name="loa"></param>
        /// <returns></returns>
        double ConvertUnitsToMinutesBasedOnTimeUsed(PolicyAbsenceReason parForThis, DateTime projectedStartDate, PolicySummary ps, LeaveOfAbsence loa)
        {
            EntitlementType et = parForThis.EntitlementType.Value;
            if (et != EntitlementType.WorkDays && et != EntitlementType.FixedWorkDays && et != EntitlementType.WorkingMonths && et != EntitlementType.WorkWeeks)
            {
                ps.ExcludeFromTimeConversion = true;
                return 0;
            }
            double duration = parForThis.Entitlement.Value;
            if (duration == 0d)
                return duration;

            //We need to get the the startDate and End of the Week , so we can fetch the WorkSchedule for that Emp
            /* Projected out schedule from start to end date */
            DateTime startDate = DateTime.UtcNow;
            DateTime endDate = DateTime.UtcNow;

            switch (et)
            {
                case EntitlementType.WorkDays:
                    startDate = projectedStartDate;
                    endDate = projectedStartDate;
                    break;
                case EntitlementType.WorkWeeks:
                    startDate = projectedStartDate.GetFirstDayOfWeek();
                    endDate = projectedStartDate.GetLastDayOfWeek();
                    break;
                case EntitlementType.WorkingMonths:
                    startDate = projectedStartDate.GetFirstDayOfMonth();
                    endDate = projectedStartDate.GetLastDayOfMonth();
                    break;
            }

            //Get the Projected Schedule
            List<Time> projectedSchedule = loa.MaterializeSchedule(startDate, endDate, false);
            //Convert it to total Hours
            int? totalMinutesInSchedule = projectedSchedule.Sum(a => a.TotalMinutes);

            if (ps.TimeUsed > 0)
            {
                double usedMinutes = (ps.TimeUsed * totalMinutesInSchedule.Value);
                //Now we have remainingMinutes , send back
                return usedMinutes;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// Pass the case id, and the case will be closed all RTW'd, trimmed and todoed up
        /// </summary>
        /// <param name="caseToClose">The case to close.</param>
        /// <param name="rtwDate">The RTW date.</param>
        /// <param name="reason">The reason.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Case not found;caseToClose</exception>
        /// <exception cref="AbsenceSoftException">
        /// Case Closed Date out of case range
        /// or
        /// User request is after close date
        /// or
        /// or
        /// Intermitent date approved after RTW date
        /// </exception>
        public Case CaseClosed(Case caseToClose, DateTime? rtwDate, CaseClosureReason? reason, string reasonDetails = null, string otherReasonDetails = null, string Outcome = null)
        {
            using (new InstrumentationContext("CaseService.CaseClosed"))
            {
                // get it
                if (caseToClose == null)
                    throw new ArgumentException("Case not found", "caseToClose");

                DateTime endDate = rtwDate == null || rtwDate == DateTime.MinValue ? (caseToClose.EndDate ?? DateTime.UtcNow.Date) : rtwDate.Value.AddDays(-1);
                if (endDate < caseToClose.StartDate)
                    endDate = caseToClose.StartDate;

                // check that the closed date falls within the case date
                if (!endDate.DateInRange(caseToClose.StartDate, caseToClose.EndDate))
                    throw new AbsenceSoftException("Case Closed Date out of case range");

                // validate the dates all the way down, throw an error if the closed date is outside the case range
                foreach (CaseSegment cs in caseToClose.Segments)
                {
                    // check that there are no user requests outside the case date and that none of
                    // the user requests within the case date are pending (but only on intermittent segments)
                    if (cs.Type == CaseType.Intermittent)
                    {
                        if (cs.UserRequests != null)
                        {
                            foreach (IntermittentTimeRequest req in cs.UserRequests.Where(x => x.RequestDate < cs.EndDate))
                            {
                                if (req.RequestDate > cs.EndDate)
                                    throw new AbsenceSoftException("User request is after close date");

                                if (req.Detail.Any(d => d.Pending > 0)) 
                                {
                                    throw new AbsenceSoftException(string.Format("User request on {0:MM/dd/yyyy} is pending", req.RequestDate));
                                }
                            }
                        }
                    }

                    foreach (AppliedPolicy ap in cs.AppliedPolicies)
                    {
                        // if the type is intermittent then make sure that there are no 
                        // approved instances after the closed date
                        if (cs.Type == CaseType.Intermittent)
                            foreach (AppliedPolicyUsage apu in ap.Usage)
                                if (apu.DateUsed > endDate && apu.Determination == AdjudicationStatus.Approved)
                                    throw new AbsenceSoftException("Intermitent date approved after RTW date");
                    }

                    cs.Status = CaseStatus.Closed;
                }

                // if we made it to here without throwing an error then we can go ahead and trim 
                // up the data
                // and while we are here set the case end date
                caseToClose.EndDate = endDate;
                foreach (CaseSegment gcs in caseToClose.Segments)
                {
                    if (gcs.EndDate > endDate)
                        gcs.EndDate = endDate;

                    foreach (AppliedPolicy gap in gcs.AppliedPolicies)
                    {
                        if (gap.EndDate > endDate)
                            gap.EndDate = endDate;

                        gap.Usage = gap.Usage.Where(gapu => gapu.DateUsed <= endDate).ToList();
                    }
                }

                if (caseToClose.Status != CaseStatus.Closed)
                {
                    caseToClose.Status = CaseStatus.Closed;
                    caseToClose.ClosureReason = reason;
                    var caseCategory = GetCaseCategoryByCode(reasonDetails);
                    var caseCategoryByName = GetCaseCategoryByName(reasonDetails);
                    if (caseCategory != null)
                    {
                        caseToClose.ClosureReasonDetails = reasonDetails;
                        caseToClose.ClosureOtherReasonDetails = caseCategory.Name;
                    }
                    else if (caseCategoryByName != null)
                    {
                        caseToClose.ClosureReasonDetails = caseCategoryByName.Code;
                        caseToClose.ClosureOtherReasonDetails = caseCategoryByName.Name;
                    }
                    else
                    {
                        caseToClose.ClosureReasonDetails = "OTHER"; //if not found in DB then it should be OTHER, set this value here , so while displaying in UI we can display proper reason
                        caseToClose.ClosureOtherReasonDetails = String.IsNullOrEmpty(otherReasonDetails) ? reasonDetails : otherReasonDetails;
                    }
                }

                caseToClose.SetCaseEvent(CaseEventType.CaseClosed, DateTime.UtcNow.ToMidnight());

                // now save our changes
                caseToClose = caseToClose.Save();

                // Update case assignee status
                UpdateCaseAssigneeStatus(caseToClose.Id, CaseStatus.Closed);

                if (Outcome != "Complete" && RunWorkflow)
                {
                    caseToClose.WfOnCaseClosed();
                }
                return caseToClose;
            }
        }


        /// <summary>
        /// put the list into the order that they can be applied.
        /// 
        /// These get sorted by root level first (ones with no dependants) by date
        /// then any that depend on those, etc all the way down. Then make sure
        /// no one is duplicated and they get pushed to the lowest possible level
        /// that they require
        /// </summary>
        /// <param name="toSort"></param>
        /// <returns></returns>
        public List<AppliedPolicy> AppliedPolicySort(List<AppliedPolicy> toSort)
        {

            // build the root level
            List<AppliedPolicy> theTop = toSort.Where(ts => ts.Policy.ConsecutiveTo.Count == 0).OrderBy(ts => ts.StartDate).ToList();

            // the next problem. We can have policies that can be consecutive too but thanks to 
            // whatever rules their parent policies are not selected. These orphans also
            // should sort to the root level as well
            foreach (AppliedPolicy ap in toSort)
            {
                if (theTop.Contains(ap))
                    continue;

                // because the applied policy is saved with the case cut the consecutive too's off
                // of it so that we don't try to process it at any other time
                if (!ap.Policy.ConsecutiveTo.Any(ct => toSort.Any(ts => ts.Policy.Code == ct)))
                {
                    ap.Policy.ConsecutiveTo = new List<string>();
                    theTop.Add(ap);
                }
            }

            // now from here get their children, and build up a path
            // this is the policy and the path.
            Dictionary<string, apSort> paths = new Dictionary<string, apSort>();

            // sort on the consecutive too
            Func<Policy, IEnumerable<string>> consecList = (polToSort) => { return polToSort.ConsecutiveTo.AsEnumerable<string>(); };

            foreach (AppliedPolicy ap in theTop)
            {
                paths[ap.Policy.Code] = new apSort() { path = string.Format("/{0}", ap.Policy.Code), appliedPolicy = ap };

                KeepSorting(toSort, paths, ap.Policy.Code, paths[ap.Policy.Code].path, 2, consecList);
            }

            // copy it to the consecutive to path and clear the path
            paths.ForEach(p => { p.Value.ConsecutiveToPath = p.Value.path; p.Value.path = ""; });


            // now our dictionary has the longest path names per policy, sort it, get the policies and return it
            List<AppliedPolicy> rtVal = paths.Select(p => new
            {
                path = p.Value.path,
                ap = p.Value.appliedPolicy,
                level = p.Value.level,
                ConsecutiveToPath = p.Value.ConsecutiveToPath
            })
                .OrderBy(o => o.level)
                .ThenBy(o => o.ap.StartDate)
                .ThenBy(o => o.ConsecutiveToPath)
                .Select(p => p.ap).ToList();

            return rtVal;
        }

        /// <summary>
        /// I know I could have used a tuple, but I find this more readable and easier to follow
        /// </summary>
        private class apSort
        {
            public string path { get; set; }
            public string ConsecutiveToPath { get; set; }
            public AppliedPolicy appliedPolicy { get; set; }
            public int level { get; set; }
        }

        /// <summary>
        /// drill down into the applied policies and build out the longest path to them
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="currentPolicy"></param>
        /// <param name="currentPath"></param>
        private void KeepSorting(List<AppliedPolicy> toSort, Dictionary<string, apSort> paths, string currentPolicy, string currentPath, int level,
                                    Func<Policy, IEnumerable<string>> sortingProperty)
        {
            // get all the policies where the current one is a parent
            //List<AppliedPolicy> theNextOnes = toSort.Where(ts => ts.Policy.ConsecutiveTo.Exists(ct => ct == currentPolicy)).ToList();
            List<AppliedPolicy> theNextOnes = toSort.Where(ts => sortingProperty(ts.Policy).Any(ct => ct == currentPolicy)).ToList();
            foreach (AppliedPolicy ap in theNextOnes)
            {
                // if there is already a key for this policy see if the key is greater than the
                // path we are currently building. If it is then add it (cuz we want to force it
                // down as far as possible)
                string newPath = string.Format("{0}/{1}", currentPath, ap.Policy.Code);
                if (paths.ContainsKey(ap.Policy.Code))
                {
                    if (paths[ap.Policy.Code].path.CompareTo(newPath) < 0)
                    {
                        paths[ap.Policy.Code].path = newPath;
                        paths[ap.Policy.Code].level = level;
                    }
                }
                else
                {
                    paths[ap.Policy.Code] = new apSort() { path = newPath, appliedPolicy = ap, level = level };
                }

                // and keep going down
                KeepSorting(toSort, paths, ap.Policy.Code, newPath, level + 1, sortingProperty);
            }
            return;
        }//KeepSorting


        /// <summary>
        /// Cancels a case given its case id and cancels its internal segments as well.
        /// </summary>
        /// <param name="caseId">The id of the case to cancel.</param>
        /// <param name="reason">The reason for cancellation</param>
        /// <returns></returns>
        public Case CancelCase(string caseId, CaseCancelReason reason = CaseCancelReason.Other)
        {
            using (new InstrumentationContext("CaseService.CancelCase"))
            {
                if (string.IsNullOrWhiteSpace(caseId))
                    throw new ArgumentNullException("caseId");

                // Get the case to cancel.
                Case cancel = Case.GetById(caseId);

                if (cancel == null)
                    throw new ArgumentException("Case not found", "caseId");
                if (User.Current != null && !User.Current.HasEmployerAccess(cancel.EmployerId, Permission.CancelCase))
                    throw new AbsenceSoftException("User does not have access to this case");

                // If the status is already cancelled, then !#@%, stop wasting the service layer's time darn-it! lol.
                if (cancel.Status == CaseStatus.Cancelled)
                    return cancel;

                // Set the status on the case and reason, along with the cancelled status on each of the segments.
                cancel.Status = CaseStatus.Cancelled;
                cancel.CancelReason = reason;
                cancel.Segments.ForEach(s => s.Status = CaseStatus.Cancelled);

                // set case cancelled event
                cancel.SetCaseEvent(CaseEventType.CaseCancelled, DateTime.UtcNow);

                // Update case assignee status
                UpdateCaseAssigneeStatus(cancel.Id, CaseStatus.Cancelled);

                // Raise: On Case Canceled event
                cancel.WfOnCaseCanceled();

                return cancel;
            }
        }//CancelCase

        /// <summary>
        /// Takes the case dates and types and makes the case accept this info,
        /// it then runs Eligibility and returns the case. The case is not
        /// saved.
        /// If changesCaseDates is true then the case dates will be updated to reflect the new
        /// start and end date, if it is false then the the past dates will be trimmed to be
        /// within the case
        /// </summary>
        /// <param name="theCase">The Case Object to update</param>
        /// <param name="startDate">the new start date</param>
        /// <param name="endDate">the new end date</param>
        /// <param name="changeStartDate">if set to <c>true</c> [change start date].</param>
        /// <param name="changeEndDate">if set to <c>true</c> [change end date].</param>
        /// <param name="newCaseType">New type of the case.</param>
        /// <param name="newSchedules">The new schedules.</param>
        /// <param name="modifyDecisions">if set to <c>true</c> [modify decisions].</param>
        /// <param name="modifyAccommodations">if set to <c>true</c> [modify accommodations].</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">
        /// Approvals have been applied. Entire case date range may not be changed
        /// or
        /// Information already sent to payroll, case start date cannot be changed.
        /// or
        /// Information already sent to payroll that is after new case end date. Date cannot be changed.
        /// </exception>
        public Case ChangeCase(Case theCase, DateTime startDate, DateTime endDate, bool changeStartDate, bool changeEndDate, CaseType newCaseType,
            bool modifyDecisions, bool modifyAccommodations, Schedule workSchedule = null, List<VariableScheduleTime> variableTime = null)
        {
            using (new InstrumentationContext("CaseService.ChangeCase"))
            {
                bool caseHasRelapse = theCase.HasRelapse();
                Relapse changedRelapse = null;

                // if there isn't anything to do then don't do it, OR, if you passed in an invalid case type selection
                if (theCase == null || theCase.Segments == null || theCase.Segments.Count == 0 || newCaseType == 0)
                    return theCase;     // you gave it bad, you get it back bad

                // next thing to verify, if the start and end are outside of the case date
                // and the change case is false then there is nothing to do here either
                if (!changeStartDate && !changeEndDate && !startDate.DateRangesOverLap(endDate, theCase.StartDate, theCase.EndDate))
                    throw new AbsenceSoftException("The dates requested fall outside of the bounds of the case. If this is intentional then change start date or change end date should be specified.");

                if (!changeStartDate && theCase.StartDate > startDate)
                    throw new AbsenceSoftException("The start date specified is before the start date of the case and new start date is not specified.");

                if (!changeEndDate && theCase.EndDate < endDate)
                    throw new AbsenceSoftException("The end date specified is after the end date of the case and new end date is not specified.");

                if (workSchedule != null && newCaseType == CaseType.Reduced)
                {
                    DateTime date = workSchedule.StartDate != null ? workSchedule.StartDate : DateTime.Now.ToMidnight();
                    date = date.GetFirstDayOfWeek();
                    workSchedule.Times.ForEach(x => { x.SampleDate = date; date = date.AddDays(1); });

                    if (theCase.Segments.FirstOrDefault(d => d.StartDate == startDate && d.EndDate == endDate) != null)
                    {
                        theCase.Segments.FirstOrDefault(d => d.StartDate == startDate && d.EndDate == endDate).LeaveSchedule = new List<Schedule> { workSchedule };
                    }
                }

                // Get a copy of the original case so that we can modify all the decisions and/oor accommodations.
                Case originalCase = modifyAccommodations || modifyDecisions ? theCase.Clone() : null;

                //Initialize relapse object If the case has relapse segment, to be used in furthur processing
                if (caseHasRelapse)
                {
                    changedRelapse = GetRelapses(theCase.Id).FirstOrDefault(r => r.Id == theCase.CurrentRelapseId);
                }

                // check to see if they are replacing the entire segment with a different type. If this happens then there are
                // differnt rules for the update
                CaseSegment csChangeOnly = theCase.Segments.FirstOrDefault(s => s.StartDate == startDate && s.EndDate == endDate);
                if (csChangeOnly != null && csChangeOnly.Type != newCaseType)
                {
                    // check to see if anything has been sent to payroll, if there are any dates that are locked
                    if (csChangeOnly.AppliedPolicies.Any(ap => ap.Usage.Any(u => u.IsLocked.HasValue && u.IsLocked.Value)))
                        throw new AbsenceSoftException("Approvals have been applied. Entire case date range may not be changed");

                    theCase = ChangeEntireCaseSegmentType(csChangeOnly, newCaseType, theCase);
                    
                    RunCalcs(theCase);
                    if (modifyDecisions)
                        ReApplyAndChangeAdjudicationDecisionsForPolicies(originalCase, theCase);
                    PaidLeaveRecalculateForPayScheduleChange(theCase);
                    //Update relapse oject as well, if the case has relapse segment
                    if (caseHasRelapse)
                    {
                        changedRelapse.CaseType = newCaseType;
                        changedRelapse.Save();
                    }
                    return theCase;
                }

                //did we extend the case? Check by seeing if the total span of the case has increased
                bool extended = false;
                if ((changeEndDate || changeStartDate) && (theCase.EndDate - theCase.StartDate) < ((changeEndDate ? endDate : theCase.EndDate) - (changeStartDate ? startDate : theCase.StartDate)))
                    extended = true;

                // there are some additional checks to perform on the case start date, for any policies that pay
                // if a policy has been sent to payroll then the case start date cannot change and the case end date
                // cannot be changed to a point where it would remove info already sent to payroll
                if (theCase.Segments.SelectMany(s => s.AppliedPolicies).SelectMany(ap => ap.Usage).Any(u => u.IsLocked.HasValue && u.IsLocked.Value))
                {
                    if (changeStartDate || theCase.StartDate != startDate)
                        throw new AbsenceSoftException("Information already sent to payroll, case start date cannot be changed.");

                    if (changeEndDate || theCase.EndDate != endDate)
                        if (theCase.Segments.SelectMany(s => s.AppliedPolicies).SelectMany(ap => ap.Usage).Count(u => u.IsLocked.HasValue && u.IsLocked.Value && u.DateUsed > endDate) > 0)
                            throw new AbsenceSoftException("Information already sent to payroll that is after new case end date. Date cannot be changed.");
                }

                // make sure the segments are sorted by start date
                theCase.Segments = theCase.Segments.OrderBy(o => o.StartDate).ToList();

                // find the segment that this applies too, that will be the first one where these dates
                // overlap and the case type matches
                CaseSegment theChangedOne = theCase.Segments.OrderByDescending(o => o.StartDate).FirstOrDefault(s => s.Type == newCaseType && s.StartDate.DateRangesOverLap(s.EndDate, startDate, endDate));

                // yeah! we found one, change the start and end dates on it to match
                if (theChangedOne != null && newCaseType != CaseType.Reduced)
                {
                    if (changeStartDate)
                    {
                        theChangedOne.StartDate = startDate;
                    }
                    if (changeEndDate)
                    {
                        theChangedOne.EndDate = endDate;

                    }
                }

                // if we didn't find an overlapping segment, then we need to do something else with it
                if (theChangedOne == null || newCaseType == CaseType.Reduced)
                {
                    // first see if the start and end dates match some other case type, if so
                    // change the type
                    bool changedType = false;
                    foreach (CaseSegment cs in theCase.Segments)
                    {
                        if (cs.StartDate == startDate && cs.EndDate == endDate)
                        {
                            cs.Type = newCaseType;
                            changedType = true;
                            theChangedOne = cs;
                            break;
                        }
                    } // foreach CaseSegment

                    // didn't find one there, then add a new one
                    if (!changedType)
                    {
                        // clone the first one in the list and the override the dates
                        // and type
                        theChangedOne = theCase.Segments[0].Clone();
                        theChangedOne.Id = Guid.NewGuid();

                        theChangedOne.StartDate = startDate;
                        theChangedOne.EndDate = endDate;
                        theChangedOne.Type = newCaseType;
                        theChangedOne.Status = theCase.Status;
                        if (workSchedule != null && newCaseType == CaseType.Reduced)
                        {
                            theChangedOne.LeaveSchedule = new List<Schedule> { workSchedule };
                        }
                        // Need to remove any applied policy usage and absences, 'cause this needs to get re-calced.
                        theChangedOne.AppliedPolicies.ForEach(p =>
                        {
                            p.Usage.Clear();
                            p.StartDate = startDate;
                            p.EndDate = endDate;
                        });
                        theChangedOne.Absences.Clear();
                        theChangedOne.UserRequests.Clear();
                        if (newCaseType != CaseType.Reduced)
                        {
                            theChangedOne.LeaveSchedule.Clear();
                        }

                        theCase.Segments.Add(theChangedOne);
                    } // if (!changedType)
                } // if (theChangedOne == null)

                // if we went through all that and didn't find anything to change, 
                // then something weird is going on so just return the case
                if (theChangedOne == null)
                    return theCase;

                //We need to Support all Case Type , but valid only if EndDate has changed 
                if (changeEndDate)
                {
                    //Below Section only valid when we change the CaseEndate
                    //Get all the policy in the case 
                    var listOfPolicy = theCase.Segments.SelectMany(s => s.AppliedPolicies).Distinct(p => p.Policy.Code);
                    //Check if any missing policy exists in the selected segment
                    var listOfPolicyCodeNotExists = listOfPolicy.Select(x => x.Policy.Code).Except(theChangedOne.AppliedPolicies.Select(x => x.Policy.Code));
                    if (listOfPolicyCodeNotExists != null)
                    {
                        //it could happen we have mulitpile policy missing , so need a loop
                        foreach (var policyCode in listOfPolicyCodeNotExists)
                        {
                            //get the last segment this policy exists;
                            CaseSegment lastSegmentOfPolicy = theCase.Segments.OrderByDescending(o => o.StartDate).
                             FirstOrDefault(s => s.AppliedPolicies.Exists(p => p.Policy.Code == policyCode));
                            if (lastSegmentOfPolicy != null)
                            {
                                //get the missing policy details from the last segment , so we can clone it
                                var missingPolicy = lastSegmentOfPolicy.AppliedPolicies.
                                    FirstOrDefault(p => p.Policy.Code == policyCode);

                                if (missingPolicy != null)
                                {
                                    //Now check what other segment exists which is greater than the last segment found
                                    //for the policy
                                    var segment = theCase.Segments.OrderByDescending(o => o.StartDate).
                                        Where(p => p.StartDate > lastSegmentOfPolicy.StartDate).FirstOrDefault();

                                    if (segment != null && missingPolicy.PolicyReason.CaseTypes.HasFlag(segment.Type))
                                    {
                                        //done the hardwork now and lets add the missing policy to those segment
                                        var policyToAdd = missingPolicy.Clone();
                                        policyToAdd.Usage.Clear();
                                        policyToAdd.StartDate = segment.StartDate;
                                        policyToAdd.EndDate = segment.EndDate;
                                        segment.AppliedPolicies.Add(policyToAdd);
                                    }
                                }
                            }
                        }
                        //We are done
                    }
                }
                // now that we have changed dates and possibly inserted segments
                // sort it all again
                theCase.Segments = theCase.Segments.OrderBy(o => o.StartDate).ToList();

                // Remove any segments that are completely engulfed by the new segment
                theCase.Segments.RemoveAll(s => s != theChangedOne && s.StartDate >= theChangedOne.StartDate && s.EndDate <= theChangedOne.EndDate);

                // Now we need to trim up any segments that this new segment overlaps 'n' stuff
                List<CaseSegment> toAdd = new List<CaseSegment>();
                List<CaseSegment> toRemove = new List<CaseSegment>();
                foreach (CaseSegment curSegment in theCase.Segments.Where(s => s != theChangedOne).OrderBy(s => s.StartDate))
                {
                    // Uh spaghetti-O's!!! Looks like this new segment is splitting the old one, awe schucks, we have to
                    //  split them out now into 3 segments instead of 2, gee whiz BatMan, what a conumdrum!
                    if (curSegment.StartDate < theChangedOne.StartDate && curSegment.EndDate > theChangedOne.EndDate)
                    {
                        // Get our calculated start and end dates for our current segment split (1 = before new, 2 = after new)
                        DateTime s1 = curSegment.StartDate;
                        DateTime e1 = theChangedOne.StartDate.AddDays(-1);
                        DateTime s2 = theChangedOne.EndDate.Value.AddDays(1);
                        DateTime e2 = curSegment.EndDate.Value;
                        // Now clone our current segment (the split)
                        CaseSegment clone = curSegment.Clone();
                        clone.Id = Guid.NewGuid();
                        // Assign the clone/copy's start and end date accordingly to occur after the newly added segment that
                        //  broke it into 2.
                        clone.StartDate = s2;
                        clone.EndDate = e2;
                        // Add the clone to our toAdd collection that will be placed into the segment collection of the case later.
                        toAdd.Add(clone);
                        // Now assign the current seegment's new start and end dates so they occur before our newly added segment
                        //  that broke it into 2.
                        curSegment.StartDate = s1;
                        curSegment.EndDate = e1;
                    }// if split?

                    // We need to trim the beginning of our current segment because the newly added segment starts before
                    //  this segment does and ends some point after this segment's start date, therefore this segment should
                    //  simply start later (called a segment push).
                    if (curSegment.StartDate >= theChangedOne.StartDate && curSegment.EndDate >= theChangedOne.EndDate && curSegment.StartDate <= theChangedOne.EndDate)
                    {
                        curSegment.StartDate = theChangedOne.EndDate.Value.AddDays(1);
                    }// if trim beginning

                    // We need to trim the end of our current segment because the newly added segment starts after
                    //  this segment does and ends some point after this segment's end date, therefore this segment should
                    //  simply end earlier (called a segment pull).
                    if (curSegment.StartDate <= theChangedOne.StartDate && curSegment.EndDate <= theChangedOne.EndDate && curSegment.EndDate >= theChangedOne.StartDate)
                    {
                        curSegment.EndDate = theChangedOne.StartDate.AddDays(-1);
                    }// if trim end

                    // When a segment falls out of a new case start date, that's bad mmm-kay, it gets lobbed off 'n' stuff mmm-kay
                    if (changeStartDate && curSegment.EndDate <= theChangedOne.StartDate && !theCase.HasRelapse())
                    {
                        toRemove.AddIfNotExists(curSegment);
                    }// if lob off start

                    // When a segment falls out of a new case end date, that's bad mmm-kay, it gets lobbed off 'n' stuff mmm-kay
                    if (changeEndDate && curSegment.StartDate >= theChangedOne.EndDate)
                    {
                        toRemove.AddIfNotExists(curSegment);
                    }// if lob off end

                }//foreach segment not current

                // Remove any bad ones
                if (toRemove.Any())
                    theCase.Segments.RemoveAll(s => toRemove.Contains(s));
                // Add any we need to here
                if (toAdd.Any())
                    theCase.Segments.AddRange(toAdd);

                // now fix up the dates
                // if we are told to move the dates, then move the dates, but it gets a litle wierd
                // we really only change the case dates if the change was at the begining or the end
                // of the case
                theCase.StartDate = theCase.Segments.Min(s => s.StartDate);
                theCase.EndDate = theCase.Segments.Max(s => s.EndDate);

                // now the the segment dates are all fixed up, drill down into the applied
                // policies and set their start and end dates to match the segment
                foreach (CaseSegment cs in theCase.Segments)
                {
                    foreach (AppliedPolicy ap in cs.AppliedPolicies)
                    {
                        ap.StartDate = cs.StartDate;
                        ap.EndDate = cs.EndDate;
                        // Lob off any usage that falls outside of our newly faceted segments
                        ap.Usage.RemoveAll(u => !u.DateUsed.DateInRange(ap.StartDate, ap.EndDate));
                    }
                }//foreach segment

                //Remove all out of range missed times entered by user for employee with FTE schedule
                if (theCase.MissedTimeOverride != null && theCase.MissedTimeOverride.Any())
                {
                    theCase.MissedTimeOverride.RemoveAll(m => !(m.SampleDate.DateInRange(theCase.StartDate, theCase.EndDate)));
                }

                theCase.Segments = theCase.Segments.OrderBy(o => o.StartDate).ToList();

                if (variableTime != null && variableTime.Any())
                {
                    StoreVariableTime(variableTime, theCase);
                }
                // rerun the calcs
                RunCalcs(theCase);

                // If we're modifying accommodations, then let's get to it then.
                if (modifyAccommodations)
                    ReApplyAndChangeAccommodations(originalCase, theCase, modifyDecisions);

                // If we're modifying decisions, then let's get to it then.
                if (modifyDecisions)
                    ReApplyAndChangeAdjudicationDecisionsForPolicies(originalCase, theCase);

                /// Even though Run calcs calls PaidLeaveCalculate
                /// We'll call this here to make sure if they make any changes Paid leave gets updated
                /// Since PaidLeaveCalculate doesn't do that
                PaidLeaveRecalculateForPayScheduleChange(theCase);

                //if the case was extended we need to start the certification process again.
                // Late bind our on case extended event on the case until after the case is actually saved.
                if (RunWorkflow && extended)
                    theCase.OnSavedNOnce((o, a) => a.Entity.WfOnCaseExtended());

                if (theCase.EndDate.HasValue)
                {
                    var times = new LeaveOfAbsence() { Case = theCase, Employee = theCase.Employee, Employer = theCase.Employer, Customer = theCase.Customer }
                        .MaterializeSchedule(theCase.EndDate.Value.AddDays(1), theCase.EndDate.Value.AddDays(60), true);
                    DateTime rtwDate = theCase.EndDate.Value;
                    foreach (var t in times.OrderBy(d => d.SampleDate))
                        if (t.TotalMinutes.HasValue && t.TotalMinutes.Value > 0)
                        {
                            rtwDate = t.SampleDate;
                            break;
                        }
                    theCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, rtwDate);
                }
                if (theCase.WorkRelated != null && theCase.EndDate.HasValue)
                {
                    theCase.WorkRelated.DaysAwayFromWork = Convert.ToInt32((Convert.ToDateTime(theCase.EndDate) - theCase.StartDate).TotalDays) + 1;
                }
                //Corresponding Relapse(if present) Dates will also need to be updated along with the case.
                if (changedRelapse != null)
                {
                    DateTime relapseStartDate = changedRelapse.StartDate < startDate ? changedRelapse.StartDate : startDate;
                    changedRelapse.StartDate = theCase.Segments.Where(s => s.StartDate >= relapseStartDate).Min(s => s.StartDate);
                    changedRelapse.EndDate = theCase.Segments.Where(s => s.StartDate >= relapseStartDate).Max(s => s.EndDate);
                    changedRelapse.Save();
                }
                return theCase;
            }
        }

        public void StoreVariableTime(List<VariableScheduleTime> variableTime, Case savedCase)
        {
            if (variableTime != null && variableTime.Any())
            {
                var bulk = VariableScheduleTime.Repository.Collection.InitializeUnorderedBulkOperation();
                var deletes = variableTime.Where(v => v.Time != null && v.Time.TotalMinutes == null || v.Time.TotalMinutes.Value <= 0).ToList();
                var upserts = variableTime.Where(v => v.Time != null && v.Time.TotalMinutes.HasValue && v.Time.TotalMinutes.Value > 0).ToList();
                bool any = false;
                if (deletes.Any())
                {
                    bulk.Find(VariableScheduleTime.Query.And(
                        VariableScheduleTime.Query.EQ(v => v.EmployeeCaseId, savedCase.Id),
                        VariableScheduleTime.Query.In(v => v.Time.SampleDate, deletes.Select(d => d.Time.SampleDate))
                    )).Update(VariableScheduleTime.Updates
                        .Set(c => c.ModifiedById, CurrentUser.Id)
                        .CurrentDate(c => c.ModifiedDate)
                        .Set(c => c.IsDeleted, true));
                    any = true;
                }
                if (upserts.Any())
                {
                    foreach (var up in upserts)
                    {
                        bulk.Find(VariableScheduleTime.Query.And(
                            VariableScheduleTime.Query.EQ(v => v.EmployeeCaseId, savedCase.Id),
                            VariableScheduleTime.Query.EQ(v => v.Time.SampleDate, up.Time.SampleDate)
                        ))
                        .Upsert()
                        .UpdateOne(VariableScheduleTime.Updates
                            .SetOnInsert(v => v.CustomerId, savedCase.CustomerId)
                            .SetOnInsert(v => v.EmployerId, savedCase.EmployerId)
                            .SetOnInsert(v => v.EmployeeId, savedCase.Employee.Id)
                            .SetOnInsert(v => v.CreatedById, CurrentUser.Id)
                            .SetOnInsert(v => v.CreatedDate, DateTime.UtcNow)
                            .SetOnInsert(v => v.Time.Id, Guid.NewGuid())
                            .SetOnInsert(v => v.Time.SampleDate, up.Time.SampleDate)
                            .Set(v => v.Time.TotalMinutes, up.Time.TotalMinutes)
                            .Set(v => v.ModifiedById, CurrentUser.Id)
                            .Set(c => c.IsDeleted, false)
                            .CurrentDate(v => v.ModifiedDate)
                        );
                    }
                    any = true;
                }
                if (any)
                    bulk.Execute();
            }
        }

        /// <summary>
        /// Return the list of all actual hours added in case.
        /// </summary>
        /// <param name="caseId">Case ID</param>
        /// <returns></returns>
        public List<VariableScheduleTime> GetReducedVariableSchedule(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentException("You must provide the case id");

            using (new InstrumentationContext("CaseService.ChangeCase"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(User.Current.BuildDataAccessFilters());
                ands.Add(VariableScheduleTime.Query.EQ(vst => vst.EmployeeCaseId, caseId));
                ands.Add(VariableScheduleTime.Query.EQ(vst => vst.IsDeleted, false));

                return VariableScheduleTime.Query.Find(VariableScheduleTime.Query.And(ands)).ToList();
            }
        }

        /// <summary>
        /// Calculate Days away from work
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="injuryDate"></param>
        /// <returns></returns>
        public int CalculateDaysAwayFromWork(Case theCase, DateTime? injuryDate = null)
        {
            var DayAwaysFromWorkDateList = new List<DateTime>();
            theCase.Segments
                .Where(s => s.Type == CaseType.Consecutive)
                .SelectMany(a => a.AppliedPolicies.SelectMany(u => u.Usage.Where(d => d.Determination == AdjudicationStatus.Approved)))
                .ForEach(p => DayAwaysFromWorkDateList.Add(p.DateUsed));

            injuryDate = injuryDate.HasValue ? injuryDate : theCase.WorkRelated?.IllnessOrInjuryDate;
            if (injuryDate.HasValue)
            {
                return DayAwaysFromWorkDateList.Distinct().Count(r => r > injuryDate.Value);
            }
            return DayAwaysFromWorkDateList.Distinct().Count();
        }

        /// <summary>
        /// Sets the policy end date explicitly, or clears the explicit end date if <paramref name="endDate" /> is <c>null</c>.
        /// </summary>
        /// <param name="theCase">The case to adjust the policy end date on.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="endDate">The explicit new end date for the policy, OR, <c>null</c> to clear the explicit end date and reset to the segment end date.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase
        /// or
        /// policyCode</exception>
        /// <exception cref="System.ArgumentException">The new end date for this policy must be within the range of the case start and end dates;endDate</exception>
        /// <exception cref="AbsenceSoftException">Unable to set this end date for this policy as this policy does not currently exist on the case</exception>
        public Case SetPolicyEndDate(Case theCase, string policyCode, DateTime? endDate = null)
        {
            // Some common-sense validation stuff
            if (theCase == null)
                throw new ArgumentNullException("theCase");
            if (string.IsNullOrWhiteSpace(policyCode))
                throw new ArgumentNullException("policyCode");
            // Duh.
            if (endDate.HasValue && !endDate.Value.DateInRange(theCase.StartDate, theCase.EndDate))
                throw new ArgumentException("The new end date for this policy must be within the range of the case start and end dates", "endDate");

            using (new InstrumentationContext("CaseService.SetPolicyEndDate"))
            {
                // If we have an end date passed in, that means they want to modify the end date. That takes some special smarticles.
                if (endDate.HasValue)
                {
                    // Get a list of all of the segments which could be impacted by this BS angry move by our UI user person dude.
                    var segments = theCase.Segments.Where(s => s.EndDate >= endDate).ToList();
                    foreach (var s in segments)
                    {
                        // We need to clean up the Time Off Requests, to ensure no time is being used on any of those user requests
                        foreach (var tor in s.UserRequests.Where(u => u.RequestDate > endDate.Value))
                            // This is simply removing the TOR detail lines for this specific policy, those TORs don't actually go away, 
                            //  we just stop tracking time for them, i.e. split determinations, etc. are stored here.
                            tor.Detail.RemoveAll(d => d.PolicyCode == policyCode);

                        // We need to remove all Time Off Requests where we no longer have any policy detail after removing this policy
                        //  NOTE: This could cause some issues/confusion with dissapearing TORs, especially if they move the end date back out
                        //  and then magically expect these time off requests to all-of-a-sudden reappear as if they didn't get wiped in the first
                        //  place, and so that is what it is, if you're reading this comment then, sorry, you'll just have to deliver the bad news
                        //  and curse Chad for making this decision.
                        s.UserRequests.RemoveAll(u => !u.Detail.Any());

                        // We need to remove outright any applied policies on this case that occurr completely in a segment past our new end date.
                        //  Cause any applied policy will count towards time, and a start and end date on the same day still *use* that same day, which
                        //  is not at all what we want, so stop questioning it. In the event we have an extended end date from our prior, yes we'll have
                        //  to create one of these SoB's from scratch just like we do on change case, which sucks but it is what it is.
                        //  NOTE: This may be an unrecoverable action, but it is what it is.
                        s.AppliedPolicies.RemoveAll(p => p.Policy.Code == policyCode && p.StartDate > endDate.Value);

                        // Get the applied policy from the impacted segment so we can do stuff to it and stuff
                        var ap = s.AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                        // Check to see if we already have an applied policy for this segment, which ends past our new end date.
                        if (ap == null)
                        {
                            // Ok, so we ain't got no current applied policy, that probably means we freaking removed it before by calling this same method
                            //  Man, I knew this would come back to bite us, oh well, here it is, we need to create a new applied policy for this policy and
                            //  get it back into this segment. We need to do that by copying it from another segmnet that has it (and one better Q@#$%^ing have it).
                            ap = theCase.Segments.SelectMany(r => r.AppliedPolicies).FirstOrDefault(r => r.Policy.Code == policyCode).Clone();
                            if (ap == null)
                                throw new AbsenceSoftException("Unable to set this end date for this policy as this policy does not currently exist on the case");

                            // Clean the non-entity (typically means reset the GUID id property
                            ap.Clean();
                            // Set the appropriate start date and new end date
                            ap.StartDate = s.StartDate;
                            ap.EndDate = endDate;
                            ap.PolicyDateOverridden.SetOverride(true, CurrentUser);
                            // Remove the usage records
                            ap.Usage.Clear();
                            // Add the applied policy to the segment
                            s.AppliedPolicies.Add(ap);

                        }
                        else
                        {
                            // Remove all Usage records for this policy that occur after our new end date; we won't need them anymore.
                            // NOTE: Now, when we do this, guess what, yep, you guessed it, we also lose the determination status for those dates we're removing
                            //  for this policy. My guess is, we'll get at least 1 ticket where the customer, end user or product support comes back and asks
                            //  why-come when I move the date earlier, then move it back, I lose my approvals/denials... and the answer is this, because we removed it, that's why-cause.
                            ap.Usage.RemoveAll(u => u.DateUsed > endDate.Value);

                            // Set the new end date for the applied policy; the calcs will actually respect this, which is awesome.
                            ap.EndDate = endDate;
                            ap.PolicyDateOverridden.SetOverride(true, CurrentUser);
                        }
                    }
                }
                else // There is no end date supplied, revert to default
                {
                    // the date is null, so let's default it to the case create behavior and match the segment
                    //  and then let denials for exhaustion work in there...
                    List<AppliedPolicy> policies = new List<AppliedPolicy>();
                    foreach (var s in theCase.Segments)
                    {
                        // Get the applied policy from the impacted segment so we can do stuff to it and stuff
                        var ap = s.AppliedPolicies.FirstOrDefault(p => p.Policy.Code == policyCode);
                        // Check to see if we already have an applied policy for this segment, which ends past our new end date.
                        if (ap == null)
                        {
                            // Ok, so we ain't got no current applied policy, that probably means we freaking removed it before by calling this same method
                            //  Man, I knew this would come back to bite us, oh well, here it is, we need to create a new applied policy for this policy and
                            //  get it back into this segment. We need to do that by copying it from another segmnet that has it (and one better Q@#$%^ing have it).
                            ap = theCase.Segments.SelectMany(r => r.AppliedPolicies).FirstOrDefault(r => r.Policy.Code == policyCode).Clone();
                            if (ap == null)
                                throw new AbsenceSoftException("Unable to reset this end date for this policy as this policy does not currently exist on the case");

                            // Clean the non-entity (typically means reset the GUID id property
                            ap.Clean();
                            // Set the appropriate start date and new end date
                            ap.StartDate = s.StartDate;
                            ap.EndDate = s.EndDate;
                            // Remove the usage records
                            ap.Usage.Clear();
                            // Add the applied policy to the segment
                            s.AppliedPolicies.Add(ap);
                        }
                        else
                        {
                            // Remove all Usage records for this policy that occur after our new end date; we won't need them anymore.
                            // NOTE: Now, when we do this, guess what, yep, you guessed it, we also lose the determination status for those dates we're removing
                            //  for this policy. My guess is, we'll get at least 1 ticket where the customer, end user or product support comes back and asks
                            //  why-come when I move the date earlier, then move it back, I lose my approvals/denials... and the answer is this, because we removed it, that's why-cause.
                            ap.Usage.RemoveAll(u => u.DateUsed > endDate.Value);

                            // Set the new end date for the applied policy; the calcs will actually respect this, which is awesome.
                            ap.EndDate = s.EndDate;
                        }

                        // Add the applied policy to our case events adjustment collection so we can re-adjust based on case events later on
                        policies.Add(ap);
                    }

                    // If we have any applied policies that are applicable then we need to re-apply any case events for those
                    if (policies.Any())
                        applyCaseEvents(theCase, policies);
                }

                // Run calcs, goes without saying this magic method does all the mysterious awesomeness we count on
                //  for things to work and return the result to the caller
                return RunCalcs(theCase);
            }
        }

        /// <summary>
        /// Reapplies and changes adjudication decisions for any and all policies, accommodations and eventually any other thing
        /// that requires adjudication, if the dates line up with the original case, after all changes are applied.
        /// </summary>
        /// <param name="originalCase">The original case.</param>
        /// <param name="theCase">The case.</param>
        private void ReApplyAndChangeAdjudicationDecisionsForPolicies(Case originalCase, Case theCase)
        {
            if (originalCase == null || theCase == null)
                return;

            // TODO: do this for policies and stuff...
        }

        /// <summary>
        /// Reapplies and changes accommodation dates if those original dates line up with the case
        /// dates that are changing.
        /// </summary>
        /// <param name="originalCase">The original case.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="keepDetermination">if set to <c>true</c> [keep determination].</param>
        private void ReApplyAndChangeAccommodations(Case originalCase, Case theCase, bool keepDetermination)
        {
            if (originalCase == null || theCase == null)
                return;

            if (!theCase.IsAccommodation)
                return;
            if (theCase.AccommodationRequest == null)
                return;
            if (theCase.AccommodationRequest.Accommodations == null || !theCase.AccommodationRequest.Accommodations.Any())
                return;

            foreach (var accomm in theCase.AccommodationRequest.Accommodations)
            {
                if (accomm.StartDate == originalCase.StartDate && accomm.StartDate != theCase.StartDate)
                {
                    // Adjust the start date
                    var minUsage = accomm.Usage.OrderBy(u => u.StartDate).FirstOrDefault();
                    if (minUsage == null) continue;
                    if (!keepDetermination && minUsage.StartDate > theCase.StartDate)
                        accomm.Usage.Insert(0, new AccommodationUsage()
                        {
                            StartDate = theCase.StartDate,
                            EndDate = minUsage.StartDate.AddDays(-1),
                            Determination = AdjudicationStatus.Pending
                        });
                    else
                    {
                        while (minUsage != null && minUsage.EndDate.HasValue && minUsage.EndDate.Value < theCase.StartDate)
                        {
                            accomm.Usage.Remove(minUsage);
                            minUsage = accomm.Usage.OrderBy(u => u.StartDate).FirstOrDefault();
                        }
                        if (minUsage != null)
                            minUsage.StartDate = theCase.StartDate;
                    }
                }
                if (accomm.EndDate == originalCase.EndDate && accomm.EndDate != theCase.EndDate)
                {
                    // Adjust the end date
                    var maxUsage = accomm.Usage.OrderByDescending(u => u.StartDate).FirstOrDefault();
                    if (maxUsage == null) continue;
                    if (!keepDetermination && maxUsage.EndDate.HasValue && theCase.EndDate.HasValue && maxUsage.EndDate < theCase.EndDate)
                        accomm.Usage.Add(new AccommodationUsage()
                        {
                            StartDate = maxUsage.StartDate.AddDays(1),
                            EndDate = theCase.EndDate,
                            Determination = AdjudicationStatus.Pending
                        });
                    else
                    {
                        if (!keepDetermination && maxUsage.EndDate.HasValue && theCase.EndDate == null)
                            accomm.Usage.Add(new AccommodationUsage()
                            {
                                StartDate = maxUsage.StartDate.AddDays(1),
                                EndDate = null,
                                Determination = AdjudicationStatus.Pending
                            });
                        else if (theCase.EndDate.HasValue)
                        {
                            while (maxUsage != null && maxUsage.StartDate > theCase.EndDate.Value)
                            {
                                accomm.Usage.Remove(maxUsage);
                                maxUsage = accomm.Usage.OrderByDescending(u => u.StartDate).FirstOrDefault();
                            }
                            if (maxUsage != null)
                                maxUsage.EndDate = theCase.EndDate ?? maxUsage.EndDate;
                        }
                        else if (maxUsage.EndDate.HasValue)
                            maxUsage.EndDate = theCase.EndDate ?? maxUsage.EndDate;
                    }
                }
                if (!accomm.Usage.Any())
                {
                    var oAccomm = !keepDetermination || originalCase.AccommodationRequest == null ? null : originalCase.AccommodationRequest.Accommodations == null ? null :
                        originalCase.AccommodationRequest.Accommodations.FirstOrDefault(a => a.Id == accomm.Id);
                    accomm.Usage.Add(new AccommodationUsage()
                    {
                        StartDate = theCase.StartDate,
                        EndDate = theCase.EndDate,
                        Determination = oAccomm == null ? AdjudicationStatus.Pending : oAccomm.Determination
                    });
                }
            }
        }

        /// <summary>
        /// Changes an entire case segment from one type to another
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="newSegmentType"></param>
        private Case ChangeEntireCaseSegmentType(CaseSegment theSegment, CaseType newSegmentType, Case theCase)
        {
            // make sure the old type and new type are different
            if (theSegment.Type == newSegmentType)
                return theCase;

            // if moving from consecutive to reduced or vice verse then there is also nothing 
            // special that needs to be done. Make the assignment and return
            if ((theSegment.Type == CaseType.Consecutive && newSegmentType == CaseType.Reduced) ||
                (theSegment.Type == CaseType.Reduced && newSegmentType == CaseType.Consecutive))
            {
                theSegment.Type = newSegmentType;
                return theCase;
            }

            // if the case is currently intermittent then process those rule
            if (theSegment.Type == CaseType.Intermittent)
            {
                CaseSegmentChangeFromIntermittent(theSegment, newSegmentType);
                return theCase;
            }

            // yes.... everything should fall in this if
            if (theSegment.Type == CaseType.Consecutive || theSegment.Type == CaseType.Reduced)
            {
                CaseSegmentChangeFromConsecutiveOrReduced(theSegment, newSegmentType);
                return theCase;
            }

            //Existing type of Segment is Administrative, and it has to be changed to a non administrative type
            if (theSegment.Type == CaseType.Administrative && newSegmentType != CaseType.Administrative)
            {
                theCase = CaseSegmentChangeFromAdministrative(theSegment, newSegmentType, theCase);
                return theCase;
            }
            return theCase;
        }

        /// <summary>
        /// Change the case away from intermittent. Throw errors if it cannot be done.
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="newSegmentType"></param>
        private void CaseSegmentChangeFromIntermittent(CaseSegment theSegment, CaseType newSegmentType)
        {
            // perform intermittent checks
            if (theSegment.UserRequests.Count > 0)
                throw new AbsenceSoftException("Cannot change from an intermittent type when there are existing user requests");

            // clear out usage
            theSegment.AppliedPolicies.ForEach(ap => ap.Usage.Clear());
            theSegment.Type = newSegmentType;
        }

        private void CaseSegmentChangeFromConsecutiveOrReduced(CaseSegment theSegment, CaseType newSegmentType)
        {
            // perform consecutive checks (yeah, when I started writing this I thought there would be
            // more, so I am leaving it for the time being just in case rules materialize
            // if not the just refactor these methods out


            // clear out usage
            theSegment.AppliedPolicies.ForEach(ap => ap.Usage.Clear());
            theSegment.Type = newSegmentType;

        }

        private Case CaseSegmentChangeFromAdministrative(CaseSegment theSegment, CaseType newSegmentType, Case theCase)
        {
            theSegment.AppliedPolicies.Clear();
            theSegment.Type = newSegmentType;
            theSegment.CreatedDate = DateTime.UtcNow;
            using (EligibilityService e = new EligibilityService())
            {
                LeaveOfAbsence leave = e.RunEligibility(theCase);
                return leave.Case;
            }
        }

        /// <summary>
        /// Updates the adjudication status on all pending segments.  Closes case.
        /// </summary>
        /// <param name="myCase">The case to update in the database.</param>
        /// <param name="status">The status to set the pending segements to (must be approved or denied)</param>
        /// <returns>The saved case.</returns>
        public Case AdjudicateAllPendingTimeOnCase(Case myCase, AdjudicationStatus status)
        {
            using (new InstrumentationContext("CaseService.AdjudicateAllPendingTimeOnCase"))
            {
                // set the adjudication status for all pending time on the case
                myCase.Segments.SelectMany(s => s.AppliedPolicies).SelectMany(p => p.Usage.Where(u => u.Determination == AdjudicationStatus.Pending))
                .ForEach(u =>
                {
                    u.Determination = status;
                    if (status == AdjudicationStatus.Denied)
                    {
                        u.DenialReasonCode = AdjudicationDenialReason.Other;
                        u.DenialReasonName = AdjudicationDenialReason.OtherDescription;
                        u.DenialExplanation = "Denied on case closure";
                    }
                    u.UserEntered = true;
                });

                myCase = this.RunCalcs(myCase);
                this.CaseClosed(myCase, DateTime.UtcNow, CaseClosureReason.ReturnToWork);
                return myCase;
            }
        }


        /// <summary>
        /// Reopens the case.
        /// </summary>
        /// <param name="caseToReopen">The case to reopen.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Case not found;caseToReopen</exception>
        public Case ReopenCase(Case caseToReopen)
        {
            using (new InstrumentationContext("CaseService.ReopenCase"))
            {
                // get it
                if (caseToReopen == null)
                    throw new ArgumentException("Case not found", "caseToReopen");

                // case status open in case segments
                if (caseToReopen.Segments != null)
                    foreach (CaseSegment cs in caseToReopen.Segments)
                        cs.Status = CaseStatus.Open;

                //change case status to open
                caseToReopen.Status = CaseStatus.Open;
                caseToReopen.ClearCaseEvent(CaseEventType.CaseClosed);
                // now save our changes
                caseToReopen = UpdateCase(caseToReopen);

                // Update case assignee status
                UpdateCaseAssigneeStatus(caseToReopen.Id, CaseStatus.Open);

                // Workflow
                caseToReopen.WfOnCaseReopened();
            }

            return caseToReopen;
        }

        /// <summary>
        /// Accepts an inquiry case.
        /// </summary>
        /// <param name="inquiryCase">The inquiry to accept.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Case not found</exception>
        public Case AcceptInquiryCase(Case inquiryCase)
        {
            using (new InstrumentationContext("CaseService.AcceptInquiryCase"))
            {
                // get it
                if (inquiryCase == null)
                    throw new ArgumentException("Case not found", "inquiryCase");

                // case status open in case segments
                if (inquiryCase.Segments != null)
                    foreach (CaseSegment cs in inquiryCase.Segments)
                        cs.Status = CaseStatus.Open;

                //change case status to open
                inquiryCase.Status = CaseStatus.Open;

                // now save our changes
                inquiryCase = UpdateCase(inquiryCase);

                // Update case assignee status
                UpdateCaseAssigneeStatus(inquiryCase.Id, CaseStatus.Open);

                // Workflow
                inquiryCase.WfOnInquiryAccepted();
            }

            return inquiryCase;
        }

        /// <summary>
        /// Close an inquiry case.
        /// </summary>
        /// <param name="inquiryCase">The inquiry to close.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Case not found</exception>
        public Case CloseInquiryCase(Case inquiryCase)
        {
            using (new InstrumentationContext("CaseService.CloseInquiryCase"))
            {
                // get it
                if (inquiryCase == null)
                    throw new ArgumentException("Case not found", "inquiryCase");

                // case status open in case segments
                if (inquiryCase.Segments != null)
                    foreach (CaseSegment cs in inquiryCase.Segments)
                        cs.Status = CaseStatus.Open;

                //change case status to closed
                inquiryCase.Status = CaseStatus.Closed;

                // Set the status on the case and reason, along with the cancelled status on each of the segments.
                inquiryCase.Status = CaseStatus.Cancelled;
                inquiryCase.CancelReason = CaseCancelReason.InquiryClosed;
                inquiryCase.Segments.ForEach(s => s.Status = inquiryCase.Status);

                // Raise: On Inquiry Closed event
                inquiryCase.WfOnInquiryClosed();

                // now save our changes
                inquiryCase = UpdateCase(inquiryCase);

                // Update case assignee status
                UpdateCaseAssigneeStatus(inquiryCase.Id, CaseStatus.Closed);
            }

            return inquiryCase;
        }


        /// <summary>
        /// Updates the passed in certification on the case.
        /// </summary>
        /// <param name="case">The case to update in the database.</param>
        /// <param name="Certification">The certification details</param>
        /// <returns>nada</returns>
        public Case CreateOrModifyCertification(Case myCase, Certification cert, bool isNewCert)
        {
            if (myCase == null) throw new ArgumentNullException("myCase");
            if (cert == null) throw new ArgumentNullException("cert");

            if ((cert.StartDate != DateTime.MinValue && cert.EndDate != DateTime.MinValue) && (cert.StartDate < myCase.StartDate || cert.EndDate > myCase.EndDate))
            {
                throw new AbsenceSoftException("Cert date range must fall within the date range of the case");
            }

            var overlap = myCase.Certifications
                .Where(w => isNewCert || w.Id != cert.Id)
                .FirstOrDefault(c => c.StartDate.CertificateDateRangesOverLap(c.EndDate, cert.StartDate, cert.EndDate) && (c.EndDate != cert.EndDate && c.StartDate != cert.StartDate));
            if (overlap != null)
            {
                throw new AbsenceSoftException(
                    string.Format(
                        "Existing certification with range, {0:d} -- {1:d}, overlaps with provided range, {2:d} -- {3:d}",
                        overlap.StartDate,
                        overlap.EndDate,
                        cert.StartDate,
                        cert.EndDate
                    )
                );
            }

            using (new InstrumentationContext("CaseService.CreateOrModifyCertification"))
            {
                if (!isNewCert)
                    myCase.Certifications.Remove(myCase.Certifications.First(x => x.Id == cert.Id));

                myCase.Certifications.Add(cert);
                return myCase.Save();
            }
        }

        /// <summary>
        /// Get list of work states for report depending on other criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetWorkStateForStatusReport(ListCriteria criteria, User user)
        {

            if (criteria == null)
                criteria = new ListCriteria();
            ListResults result = new ListResults(criteria);

            long? startDate = criteria.Get<long?>("StartDate");
            long? endDate = criteria.Get<long?>("EndDate");
            bool isAccommodation = criteria.Get<bool>("IsAccommodation");
            string absenceReason = criteria.Get<string>("AbsenceReason");
            long? caseStatus = criteria.Get<long?>("CaseStatus");
            long? determination = criteria.Get<long?>("Determination");
            long? caseType = criteria.Get<long?>("CaseType");

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(user.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, user.CustomerId));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            if (!startDate.HasValue)
                startDate = long.MinValue;

            if (!endDate.HasValue)
                endDate = long.MaxValue;

            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate.Value)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate.Value))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate.Value)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate.Value)))
            ));

            if (isAccommodation)
            {
                ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            }

            if (!string.IsNullOrWhiteSpace(absenceReason))
                ands.Add(Case.Query.EQ(e => e.Reason.Id, absenceReason));

            if (caseType.HasValue)
            {
                CaseType type = ((CaseType)caseType);
                ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Type, type)));
            }

            if (caseStatus.HasValue)
            {
                CaseStatus status = ((CaseStatus)caseStatus);
                ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Status, status)));
            }

            if (determination.HasValue)
            {
                AdjudicationStatus status = ((AdjudicationStatus)determination);
                ands.Add(Case.Query.EQ(c => c.Summary.Determination, status));
            }

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);

            result.Results = query.Where(m => m.Employee.WorkState != null && m.Employee.WorkState != "").ToList().Select(e => e.Employee.WorkState).Distinct().Select(c => new ListResult()
                    .Set("Text", c)
                    .Set("Value", c)
                );

            return result;
        }


        /// <summary>
        /// Get list of office location for report depending on other criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetOfficeLocationForStatusReport(ListCriteria criteria, User user)
        {

            if (criteria == null)
                criteria = new ListCriteria();
            ListResults result = new ListResults(criteria);

            long? startDate = criteria.Get<long?>("StartDate");
            long? endDate = criteria.Get<long?>("EndDate");
            bool isAccommodation = criteria.Get<bool>("IsAccommodation");
            string absenceReason = criteria.Get<string>("AbsenceReason");
            long? caseStatus = criteria.Get<long?>("CaseStatus");
            long? determination = criteria.Get<long?>("Determination");
            long? caseType = criteria.Get<long?>("CaseType");
            string workState = criteria.Get<string>("WorkState");

            List<IMongoQuery> ands = new List<IMongoQuery>();
            if (user != null)
                ands.Add(user.BuildDataAccessFilters(Permission.RunCaseManagerReport, "Employee._id"));
            else if (!string.IsNullOrWhiteSpace(user.CustomerId))
                ands.Add(Case.Query.EQ(e => e.CustomerId, user.CustomerId));

            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(Case.Query.Or(
                Case.Query.And(Case.Query.GTE(e => e.StartDate, new BsonDateTime(startDate.Value)), Case.Query.LTE(e => e.StartDate, new BsonDateTime(endDate.Value))),
                Case.Query.And(Case.Query.GTE(e => e.EndDate, new BsonDateTime(startDate.Value)), Case.Query.LTE(e => e.EndDate, new BsonDateTime(endDate.Value)))
            ));

            if (isAccommodation)
            {
                ands.Add(Case.Query.EQ(e => e.IsAccommodation, true));
            }

            if (!string.IsNullOrWhiteSpace(absenceReason))
                ands.Add(Case.Query.EQ(e => e.Reason.Id, absenceReason));

            if (caseType.HasValue)
            {
                CaseType type = ((CaseType)caseType);
                ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Type, type)));
            }

            if (caseStatus.HasValue)
            {
                CaseStatus status = ((CaseStatus)caseStatus);
                ands.Add(Case.Query.ElemMatch(c => c.Segments, q => q.EQ(s => s.Status, status)));
            }

            if (determination.HasValue)
            {
                AdjudicationStatus status = ((AdjudicationStatus)determination);
                ands.Add(Case.Query.EQ(c => c.Summary.Determination, status));
            }

            if (!string.IsNullOrWhiteSpace(workState))
                ands.Add(Case.Query.EQ(e => e.Employee.WorkState, workState));

            var query = Case.Query.Find(ands.Count > 0 ? Case.Query.And(ands) : null);

            result.Results = query.Where(m => m.Employee.Info.OfficeLocation != null && m.Employee.Info.OfficeLocation != "").ToList().Select(e => e.Employee.Info.OfficeLocation).Distinct().Select(c => new ListResult()
                    .Set("Text", c)
                    .Set("Value", c)
                );

            return result;
        }

        public List<Case> GetCasesForEmployee(string employeeId, params CaseStatus[] statuses)
        {
            using (new InstrumentationContext("EmployeeService.GetCasesForEmployee"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();

                ands.Add(Case.Query.EQ(e => e.Employee.Id, employeeId));

                ands.Add(Case.Query.In(e => e.Status, statuses));

                return Case.Query.Find(Case.Query.And(ands)).ToList();
            }
        }

        public int GetCountOfCasesForEmployee(string employeeId, params CaseStatus[] statuses)
        {
            return Case.AsQueryable().Where(c => c.Employee.Id == employeeId && statuses.Contains(c.Status)).Count();
        }


        public List<IntermittentTimeRequest> GenerateIntermittentRequests(Case c, PolicyAbsenceReason policyAbsenceReason, DateTime requestDate, List<IntermittentTimeRequest> intermittentTimeRequests,bool isEditTOR = false)
        {
            //Currently User can enter TOR for a single day only , but as per the requirement we need block min /maxinum no of days based on the policy setting.
            //we have to generate the list of UserRequests based on the IntermittentRestrictionsMinimum & IntermittentRestrictionsMaximum settings
            //once UserRequests are created and passed to RunCalcs it will automatically do the rest of the calculation.

            int timeUnitValue = 1;
            if (policyAbsenceReason.IntermittentRestrictionsMinimumTimeUnit.HasValue)
            {
                //Get  Minimum Time Unit  , we will use this to calculate the user requests days if Maximun is not mentioned.
                Unit timeUnit = GetTimeUnit(policyAbsenceReason.IntermittentRestrictionsMinimumTimeUnit.Value);
                if (policyAbsenceReason.IntermittentRestrictionsMinimum.HasValue)
                {
                    timeUnitValue = policyAbsenceReason.IntermittentRestrictionsMinimum.Value;
                }

                //Check if we have Maximum Value & Maximum Time Unit set , If yes use those value to calculate  the max occurange dates
                if (policyAbsenceReason.IntermittentRestrictionsMaximum.HasValue && policyAbsenceReason.IntermittentRestrictionsMaximumTimeUnit.HasValue)
                {
                    timeUnit = GetTimeUnit(policyAbsenceReason.IntermittentRestrictionsMaximumTimeUnit.Value);
                    timeUnitValue = policyAbsenceReason.IntermittentRestrictionsMaximum.Value;
                }

                double minutes = timeUnit.ConvertUnits(Unit.Minutes, timeUnitValue);

                //if timeUnit is in Hours , it will be always same date , so no need to do below check
                if (timeUnit != Unit.Hours && policyAbsenceReason.IntermittentRestrictionsCalcType == IntermittentRestrictions.BeginningOfPeriod)
                {
                    //gets the start date of week and calculate from it
                    requestDate = requestDate.AddDays(((int)requestDate.DayOfWeek * -1));
                }
                DateTime endRequestDate = requestDate.AddMinutes(minutes);


                //If Calculated endRequestDate is greater than Case end date , then it should be assigned to case end date.
                //as we can't go beyond  case end date

                if (endRequestDate > c.EndDate.Value)
                {
                    endRequestDate = c.EndDate.Value;
                }

                double noOfDays = (endRequestDate - requestDate).TotalDays;

                //We need to get the workschedule of Employee & any holiday details , so while generating UserRequest we can 
                //use those in our calculation
                LeaveOfAbsence los = new LeaveOfAbsence();
                los.Case = c;
                los.Employee = c.Employee;
                los.Employer = c.Employee.Employer;
                los.Customer = c.Employee.Customer;
                los.WorkSchedule = Employee.GetById(c.Employee.Id).WorkSchedules;

                // get their schedule excluding the holidays (cuz we are directly checking the holidays)
                List<Time> regularTimes = los.MaterializeSchedule(c.StartDate, c.EndDate.Value, true);

                if (noOfDays <= 1)
                {
                    if (!intermittentTimeRequests.Exists(x => x.RequestDate == requestDate))
                    {
                        intermittentTimeRequests.Add(new IntermittentTimeRequest() { RequestDate = requestDate, IsEditMode = isEditTOR });
                    }
                }
                else
                {
                    for (int i = 0; i < noOfDays; i++)
                    {
                        DateTime generatedRequestDate = requestDate.AddDays(i);
                        //Only add scheduled work day
                        Time rt = regularTimes.FirstOrDefault(t => t.SampleDate == generatedRequestDate);
                        if (rt != null && rt.TotalMinutes != 0)
                        {
                            //if Request Date Already Exists don't do anything.
                            if (!intermittentTimeRequests.Exists(x => x.RequestDate == generatedRequestDate))
                            {
                                intermittentTimeRequests.Add(new IntermittentTimeRequest() { RequestDate = generatedRequestDate,IsEditMode = isEditTOR });
                            }
                        }
                    }
                }
            }
            return intermittentTimeRequests;
        }

        private Unit GetTimeUnit(EntitlementType entitlementType)
        {
            switch (entitlementType)
            {
                case EntitlementType.WorkWeeks:
                case EntitlementType.CalendarWeeks:
                case EntitlementType.ReasonablePeriod:
                case EntitlementType.CalendarWeeksFromFirstUse:
                    return Unit.Weeks;
                case EntitlementType.CalendarMonths:
                case EntitlementType.WorkingMonths:
                case EntitlementType.CalendarMonthsFromFirstUse:
                    return Unit.Months;
                case EntitlementType.CalendarYears:
                case EntitlementType.CalendarYearsFromFirstUse:
                    return Unit.Years;
                case EntitlementType.CalendarDays:
                case EntitlementType.WorkDays:
                case EntitlementType.FixedWorkDays:
                case EntitlementType.CalendarDaysFromFirstUse:
                    return Unit.Days;
                case EntitlementType.WorkingHours:
                default:
                    return Unit.Hours;
            }
        }

        public int GetTorTime(Case c, PolicyAbsenceReason policyReason, int approvedTime)
        {
            int intermittentTime = 0;
            // if IntermittentRestrictionsMinimumTimeUnit is in Hrs , just use the IntermittentRestrictionsMinimum value
            if (policyReason.IntermittentRestrictionsMinimumTimeUnit.HasValue &&
                    policyReason.IntermittentRestrictionsMinimumTimeUnit.Value != EntitlementType.WorkingHours)
            {

                var schedules = c.Employee.WorkSchedules.FirstOrDefault();
                if (schedules != null && schedules.Times != null)
                {
                    //Get the per day workable time of a Employee,
                    var empTotalWorkedTime = schedules.Times.GroupBy(g => 1)
                  .Select(g => new
                  {
                      TotalMinutesWorked = g.Sum(t => t.TotalMinutes) / (g.Count(t => t.TotalMinutes > 0) > 0 ? g.Count(t => t.TotalMinutes > 0) : 1)
                  }).FirstOrDefault();

                    if (empTotalWorkedTime != null && empTotalWorkedTime.TotalMinutesWorked.HasValue)
                    {
                        intermittentTime = empTotalWorkedTime.TotalMinutesWorked.Value;
                    }
                }
            }
            else if (policyReason.IntermittentRestrictionsMinimum.HasValue)
            {
                intermittentTime = policyReason.IntermittentRestrictionsMinimum.Value * 60;
            }
            if (intermittentTime > approvedTime)
            {
                return intermittentTime;
            }
            return approvedTime;

        }

        private void AdjustFixedWorkDaysChanges(DayOfWeek startDayOfWeek, int fixedWorkDaysPerWeek, Time timeOut, Time normalWorkDay, ref int weekDaysCount, ref int totalDaysCount)
        {
            if (timeOut.SampleDate.DayOfWeek == startDayOfWeek)
            {
                //first day of current week
                weekDaysCount = 1;
                totalDaysCount = totalDaysCount + 1;
                //to maintain MinutesInDay, MinutesInDay and PercentOfDay 
                normalWorkDay.TotalMinutes = 1;
                timeOut.TotalMinutes = 1;
            }
            else
            {
                if (weekDaysCount < fixedWorkDaysPerWeek)
                {
                    weekDaysCount = weekDaysCount + 1;
                    totalDaysCount = totalDaysCount + 1;
                    //to maintain MinutesInDay, MinutesInDay and PercentOfDay 
                    normalWorkDay.TotalMinutes = 1;
                    timeOut.TotalMinutes = 1;
                }
                else
                {
                    //skip remaining days of the week when it has already reached FixedWorkDaysPerWeek
                    //If we are setting timeOut.TotalMinutes to 0 when it has valid value , we should
                    //set the schedule workday also 0
                    normalWorkDay.TotalMinutes = 0;
                    timeOut.TotalMinutes = 0;
                }                
            }
        }

        #region Case Category Administration


        /// <summary>
        /// Checks whether case category code already exists
        /// We need to pass in the CustomerId instead of using what is in context because the UI needs to tell whether we're looking at a core one or a customer one
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool CaseCategoryCodeIsInUse(string id, string customerId, string code)
        {
            return CaseCategory.AsQueryable().Count(nc => nc.Code == code && nc.Id != id && nc.EmployerId == EmployerId && nc.CustomerId == customerId) > 0;
        }

        public CaseCategory GetCaseCategoryByName(string caseCategory)
        {
            using (new InstrumentationContext("CaseService.GetCaseCategoryByName"))
            {
                CaseCategory category = GetAllCaseCategories().FirstOrDefault(p => p.Name == caseCategory);
                return category;
            }
        }

        /// <summary>
        /// Gets CaseCategory by code and employer/customer 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public CaseCategory GetCaseCategoryByCode(string code)
        {
            using (new InstrumentationContext("CaseService.GetCaseCategoryByCode"))
            {
                CaseCategory category = CaseCategory.GetByCode(code, CustomerId, EmployerId, true);
                return category;
            }
        }

        /// <summary>
        /// Toggles a case category by code and employer/customer depending on what is most specific
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public CaseCategory ToggleCaseCategoryByCode(string code)
        {
            using (new InstrumentationContext("CaseService.ToggleCaseCategoryByCode"))
            {
                CaseCategory category = CaseCategory.GetByCode(code, CustomerId, EmployerId, true);
                if (category != null)
                {
                    category.ToggleSuppression(CurrentCustomer, CurrentEmployer);
                }
                return category;
            }
        }

        /// <summary>
        /// Toggles a case category by id and employer/customer depending on what is most specific
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CaseCategory ToggleCaseCategoryById(string id)
        {
            using (new InstrumentationContext("CaseService.ToggleCaseCategoryById"))
            {
                CaseCategory category = CaseCategory.GetById(id);
                if (category != null)
                {
                    category.ToggleSuppression(CurrentCustomer, CurrentEmployer);
                    category.Suppressed = category.IsSuppressed(CurrentCustomer, CurrentEmployer);
                }
                return category;
            }

        }

        public List<CaseCategory> GetAllCaseCategories(bool includeSuppressed = false)
        {
            IEnumerable<CaseCategory> caseCategories = CaseCategory.DistinctFind(null, CustomerId, EmployerId);

            if (!includeSuppressed)
                caseCategories = caseCategories.Where(nc => !nc.IsSuppressed(CurrentCustomer, CurrentEmployer)).ToList();
            List<CaseCategory> allCaseCategories = caseCategories.ToList();
            return allCaseCategories;
        }


        public CaseCategory GetCaseCategoryById(string caseCategoryId)
        {
            using (new InstrumentationContext("CaseService.GetCaseCategoryById"))
            {
                return CaseCategory.AsQueryable().FirstOrDefault(nc => nc.Id == caseCategoryId);
            }
        }

        public CaseCategory SaveCaseCategory(CaseCategory category)
        {
            using (new InstrumentationContext("CaseService.SaveCaseCategory"))
            {
                if (category == null)
                    return null;

                CaseCategory savedCategory = CaseCategory.GetById(category.Id);

                if (savedCategory != null && (!savedCategory.IsCustom || savedCategory.EmployerId != EmployerId))
                    category.Clean();

                category.CustomerId = CustomerId;
                category.EmployerId = EmployerId;

                return category.Save();
            }
        }

        public void DeleteCaseCategory(CaseCategory category)
        {
            using (new InstrumentationContext("CaseService.DeleteCaseCategory"))
            {
                category.Delete();
            }
        }

        public int CalculateDaysOnJobTransferOrRestriction(Case c, DateTime? injuryDate = null)
        {
            int noOfDays = 0;
            var combinedList = new List<DateRange>();

            combinedList.AddRange(c.Segments.Where(s => s.Type == CaseType.Intermittent || s.Type == CaseType.Reduced).SelectMany(a => a.AppliedPolicies.SelectMany(u => u.Usage.Where(x => x.Determination == AdjudicationStatus.Approved).Select(r => new DateRange { StartDate = r.DateUsed, EndDate = r.DateUsed }))).GroupBy(o => o.StartDate).Select(i => i.FirstOrDefault()));

            if (c.AccommodationRequest != null)
            {
                var AccommodationsDateRange = c.AccommodationRequest.Accommodations.Where(x => x.Duration != AccommodationDuration.Permanent && x.Determination == AdjudicationStatus.Approved).Select(r => new DateRange { StartDate = r.MinApprovedFromDate ?? DateTime.Now, EndDate = r.MaxApprovedThruDate ?? r.MinApprovedFromDate ?? DateTime.Now }).ToList();
                //To handle Date Range for Accommodation Request w.r.t. IllnessOrInjuryDate, making similar to policies usages
                foreach (DateRange dr in AccommodationsDateRange)
                {
                    DateTime startingDate = dr.StartDate;
                    do
                    {
                        combinedList.Add(new DateRange { StartDate = startingDate, EndDate = startingDate });
                        startingDate = startingDate.AddDays(1);
                    } while (startingDate <= dr.EndDate);
                }
            }

            using (var demandService = new DemandService(CurrentUser))
            {
                if (c.Employee != null && !string.IsNullOrEmpty(c.Id))
                {
                    List<Data.Jobs.EmployeeRestriction> jobRestrictions = demandService.GetJobRestrictionsForEmployee(c.Employee.Id, c.Id);
                    if (jobRestrictions != null && jobRestrictions.Count > 0)
                    {
                        var jobRestrictionsDateRange = jobRestrictions.Where(r => r.Restriction.Values[0].ToString() != "None").Select(r => new DateRange { StartDate = r.Restriction.Dates.StartDate, EndDate = r.Restriction.Dates.EndDate ?? DateTime.Now }).ToList();
                        foreach (DateRange dr in jobRestrictionsDateRange)
                        {
                            DateTime startingDate = dr.StartDate;
                            do
                            {
                                combinedList.Add(new DateRange { StartDate = startingDate, EndDate = startingDate });
                                startingDate = startingDate.AddDays(1);
                            } while (startingDate <= dr.EndDate);
                        }
                    }
                }
            }

            if (combinedList.Any())
            {
                //Remove consecutive date range
                var consecutiveRange = c.Segments.Where(s => s.Type == CaseType.Consecutive).SelectMany(a => a.AppliedPolicies.SelectMany(u => u.Usage.Where(x => x.Determination == AdjudicationStatus.Approved).Select(r => new DateRange { StartDate = r.DateUsed, EndDate = r.DateUsed }))).GroupBy(o => o.StartDate).Select(i => i.FirstOrDefault());
                combinedList = combinedList.Except(consecutiveRange).ToList();

                injuryDate = injuryDate.HasValue ? injuryDate : c.WorkRelated?.IllnessOrInjuryDate;
                //If the date of injury is within the boundaries of the case dates, then start calculated days Job Transfer or Restriction starting the day after the date of injury
                if (injuryDate >= c.StartDate && injuryDate <= c.EndDate)
                {
                    combinedList = combinedList.Where(r => r.StartDate > injuryDate).ToList();
                }

                var overlappingEvents = combinedList.Where(e1 => combinedList.Where(e2 => e2 != e1).Any(e2 => e1.StartDate <= e2.EndDate && e1.EndDate >= e2.StartDate));
                if (overlappingEvents != null && overlappingEvents.Any())
                {
                    DateTime min = overlappingEvents.Min(p => p.StartDate < p.EndDate ? p.StartDate : p.EndDate);
                    DateTime max = overlappingEvents.Max(p => p.StartDate > p.EndDate ? p.StartDate : p.EndDate);
                    noOfDays += (int)max.Subtract(min).TotalDays + 1;
                }
                var nonOverlappingEvents = combinedList.Except(overlappingEvents);
                if (nonOverlappingEvents != null && nonOverlappingEvents.Any())
                {
                    noOfDays += nonOverlappingEvents.Sum(x => (int)(x.EndDate - x.StartDate).TotalDays + 1);
                }
            }

            return noOfDays;
        }
        #endregion

        /// <summary>
        /// Enqueues the recalc future cases.
        /// </summary>
        /// <param name="activeCase">The active case.</param>
        public void EnqueueRecalcFutureCases(Case activeCase)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseService.EnqueueRecalcFutureCases"))
            {

                //only be called if there are other open cases for that employee that overlap or go beyond the case end date
                var affectedCases = Case.AsQueryable().Where(c => c.Employee.Id == activeCase.Employee.Id
                && c.StartDate < activeCase.EndDate
                && c.Id != activeCase.Id
                && c.Status == CaseStatus.Open);
                if (affectedCases.Any())
                {

                    if (affectedCases.Count() > 1)
                    {
                        using (QueueService svc = new QueueService())
                        {
                            var data = new RecalcFutureCaseData(
                                activeCase.Id,
                                activeCase.Employee.Id,
                                activeCase.EndDate
                            );
                            svc.Add(MessageQueues.AtCommonQueue, MessageType.RecalcFutureCases, data);
                        }
                    }
                    else
                    {
                        //if we have only two cases to rectify then do to directly , don't try queue ,
                        foreach (var @case in affectedCases)
                        {
                            RunCalcs(@case);
                            UpdateCase(@case);
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Calculate gaps between the applied policy usages
        /// </summary>
        /// <param name="ap">AppliedPolicy</param>
        /// <param name="theCase">Case</param>
        /// <param name="put">List<PolicyUsageTotals></param>
        /// <param name="employeeId">EmployeeId</param>
        /// <returns>gapUsageCount</returns>
        public int CalculateGapUsage(AppliedPolicy ap, Case theCase, List<PolicyUsageTotals> put, string employeeId = null)
        {
            int gapUsageCount = 0;

            // get employee id
            employeeId = theCase != null? theCase.Employee.Id : employeeId;

            if (put.Any(p => p.PolicyCode == ap.Policy.Code))
            {
                // get policy usage total
                var policyTotal = put.FirstOrDefault(p => p.PolicyCode == ap.Policy.Code);

                if (policyTotal == null || employeeId == null)
                {
                    return gapUsageCount;
                }

                // get employee's all cases which are in range of measure start and end date
                var _allOtherCases = Case.AsQueryable().Where(c => c.Employee.Id == employeeId && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Inquiry &&
                                                                    c.StartDate < policyTotal.MeasureEndDate && policyTotal.MeasureStartDate < c.EndDate).OrderBy(o => o.StartDate).ToList();

                foreach (var appliedCase in _allOtherCases)
                {
                    AppliedPolicy prevPolicy = null;

                    if (appliedCase.Segments.Any(p => p.AppliedPolicies.Any(r => r.PolicyReason.ReasonCode == ap.PolicyReason.ReasonCode)))
                    {
                        // get applied policies
                        var _allAppliedPolicies = appliedCase.Segments.Where(s => s.Status != CaseStatus.Cancelled).SelectMany(s => s.AppliedPolicies.Where(a => a.PolicyReason.ReasonCode == ap.PolicyReason.ReasonCode)).Distinct().ToList();

                        // add current applying policy
                        if (_allAppliedPolicies.Any(a => a.Id == ap.Id) && !_allAppliedPolicies.Any(a => a.StartDate == ap.StartDate && a.EndDate == ap.EndDate))
                        {
                            _allAppliedPolicies.Add(ap);
                        }

                        if (_allAppliedPolicies.Count > 1)
                        {
                            // calculate gaps
                            foreach (var appliedPolicy in _allAppliedPolicies.OrderBy(p => p.StartDate))
                            {
                                if (prevPolicy == null)
                                {
                                    prevPolicy = appliedPolicy; // first policy
                                }
                                else if (appliedPolicy.StartDate >= prevPolicy.EndDate.Value)
                                {
                                    gapUsageCount = gapUsageCount + ((appliedPolicy.StartDate - prevPolicy.EndDate.Value).Days - 1); // add gaps
                                    prevPolicy = appliedPolicy; // set next as prev policy
                                }
                            }
                        }
                    }
                }

            }            

            return gapUsageCount;
        }

        /// <summary>
        /// Calculate Gap Usage In TimeTracker
        /// </summary>
        /// <param name="theAPforThisPolicy">AppliedPolicy</param>
        /// <param name="currentCase">Case</param>
        /// <param name="usageTotals">PolicyUsageTotals</param>
        /// <param name="empId">Employee Id</param>
        /// <param name="timeUsed">Time Used</param>
        /// <returns>Time Used after adding gaps</returns>
        private double CalculateGapUsageInTimeTracker(AppliedPolicy theAPforThisPolicy, Case currentCase, List<PolicyUsageTotals> usageTotals, string empId, double timeUsed)
        {
            int gapUsageCount = 0;
            decimal dayUsage = 0;
            AppliedPolicyUsage workUsage = null;

            // calcualte gaps
            gapUsageCount = CalculateGapUsage(theAPforThisPolicy, currentCase, usageTotals, empId);
            
            if (theAPforThisPolicy.Usage.Any(a => a.Determination == AdjudicationStatus.Pending || a.Determination == AdjudicationStatus.Approved))
            {
                // get work usage from totals
                workUsage = theAPforThisPolicy.Usage.FirstOrDefault(a => a.Determination == AdjudicationStatus.Pending || a.Determination == AdjudicationStatus.Approved);

                 // get days usage
                 dayUsage = workUsage != null ? Convert.ToDecimal(workUsage.DaysUsage(theAPforThisPolicy.PolicyReason.EntitlementType.Value)) : 0;
            }

            if (gapUsageCount > 0 && dayUsage > 0)
            {
                // add gap with unit conversion
                timeUsed = Convert.ToDouble(Math.Round(Convert.ToDecimal(timeUsed) + Convert.ToDecimal(gapUsageCount * dayUsage), 8));
            }

            return timeUsed;
        }

        public void SaveMissedTimeOverrides(string caseId, int totalMinutes, DateTime? date)
        {
            Case theCase = GetCaseById(caseId);
            var missedTimeList = theCase.MissedTimeOverride ?? new List<Time>();

            if (missedTimeList.Any(t => t.SampleDate == date.Value))
            {
                missedTimeList.FirstOrDefault(t => t.SampleDate == date.Value).TotalMinutes = totalMinutes;
            }
            else
            {
                missedTimeList.Add(new Time()
                {
                    TotalMinutes = totalMinutes,
                    SampleDate = date.Value.ToMidnight()
                });
            }
            theCase.MissedTimeOverride = missedTimeList;
            RunCalcs(theCase);
            UpdateCase(theCase);
        }
    } // public class CaseService : LogicService
    public class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

