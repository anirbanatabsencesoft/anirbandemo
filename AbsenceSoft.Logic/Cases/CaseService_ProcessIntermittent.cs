﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using System.Text;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson.Serialization;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Common;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// all the logic releated to implementing intermittent cases goes in here
    /// </summary>
    public partial class CaseService : LogicService
    {

        /// <summary>
        /// Creates or modifies a time off request for the supplied case, re-runs calcs and applies any determination for the given request date.
        /// This method does NOT save the case; it only provisions the appropriate time off and usage calcs, etc. and returns the result.
        /// Use CaseService.UpdateCase(...) in order to save the case.
        /// 
        /// Note: This now is a wrapper for a more specialized version, but this will create a time off request that meets the
        /// requirements.
        /// </summary>
        /// <param name="myCase">The case to apply the intermittent request for time off to</param>
        /// <param name="requestDate">The date for time off requested</param>
        /// <param name="minutesUsed">The total minutes used or taken off for that date</param>
        /// <param name="notes">Any notes to apply to the time off request</param>
        /// <param name="status">The adjudication status for the request for time off</param>
        /// <param name="reasonCode">The denial reason code for the adjudication status of denied</param>
        /// <param name="reasonName">The denial reason name for the adjudication status of denied</param>
        /// <returns>The modified, unsaved case after everying has been performed</returns>
        /// <exception cref="ArgumentNullException">myCase</exception>
        /// <exception cref="ArgumentException">minutesUsed must be greater than 0</exception>
        /// <exception cref="AbsenceSoftException">There are no active intermittent case segments for this case</exception>
        /// <exception cref="AbsenceSoftException">The requested date does not fall within any intermittent segment for this case</exception>
        [Obsolete("CreateOrModifyTimeOffRequest(case, date, minutes, etc.) is obsolete, please use the one that takes a IntermittentTimeRequest collection instead.", false)]
        public Case CreateOrModifyTimeOffRequest(Case myCase, DateTime requestDate, int minutesUsed, string notes, AdjudicationStatus status = AdjudicationStatus.Pending, string reasonCode = null, string reasonName = null)
        {
            using (new InstrumentationContext("CaseService.CreateOrModifyTimeOffRequest"))
            {
                reasonCode = string.IsNullOrWhiteSpace(reasonCode) ? reasonCode : reasonCode.ToUpperInvariant();
                // Validate the parameters are not null/invalid
                if (myCase == null)
                    throw new ArgumentNullException("myCase");
                if (minutesUsed <= 0)
                    throw new ArgumentException("minutesUsed must be greater than 0", "minutesUsed");

                if (status == AdjudicationStatus.Denied && string.IsNullOrWhiteSpace(reasonCode))
                    throw new ArgumentException("Denied status requires a reason.");

                // Get all intermittent case segments
                var intSegments = myCase.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && s.IsEligibleOrPending);
                // If there are no intermittent segments, that's bad, shouldn't allow an intermittent request for time off at that point, no sir.
                if (!intSegments.Any())
                    throw new AbsenceSoftException("There are no active intermittent case segments for this case");

                // Get the applicable segment, since they can't overlap, not active ones anyway, then we need to find the one that
                //  is appropriate for our request date.
                CaseSegment seg = intSegments.FirstOrDefault(s => requestDate.DateInRange(s.StartDate, s.EndDate));
                // Uh oh, there isn't one that covers our requested date, bail out and raise some red flags.
                if (seg == null)
                    throw new AbsenceSoftException("The requested date does not fall within any intermittent segment for this case");

                List<IntermittentTimeRequest> requests = new List<IntermittentTimeRequest>();
                IntermittentTimeRequest itr = new IntermittentTimeRequest()
                {
                    Notes = notes,
                    RequestDate = requestDate,
                    TotalMinutes = minutesUsed
                };
                requests.Add(itr);
                IntermittentTimeRequestDetail det = new IntermittentTimeRequestDetail();
                itr.Detail.Add(det);

                foreach (AppliedPolicy ap in seg.AppliedPolicies)
                {
                    det.PolicyCode = ap.Policy.Code;
                    switch (status)
                    {
                        case AdjudicationStatus.Pending:
                            det.Pending = minutesUsed;
                            break;
                        case AdjudicationStatus.Approved:
                            det.Approved = minutesUsed;
                            break;
                        case AdjudicationStatus.Denied:
                            det.Denied = minutesUsed;
                            det.DenialReasonCode = reasonCode;
                            det.DenialReasonName = reasonName;
                            break;
                        default:
                            throw new AbsenceSoftException(string.Format("Adjudication status: {0} is not allowed. Must be Pending, Approved or Denied", status.ToString()));
                    }
                }

                CreateOrModifyTimeOffRequest(myCase, requests);

                return myCase;
            }
        }//Case CreateOrModifyTimeOffRequest


        /// <summary>
        /// Set the user override info on a time off request
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="timeRequested"></param>
        /// <returns></returns>
        public Case CreateOrModifyTimeOffRequest(Case theCase, List<IntermittentTimeRequest> timeRequested)
        {
            using (new InstrumentationContext("CaseService.CreateOrModifyTimeOffRequest"))
            {
                // Validate the parameters are not null/invalid
                if (theCase == null)
                    throw new ArgumentNullException("myCase");

                // Get all intermittent case segments
                var intSegments = theCase.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && s.IsEligibleOrPending);
                // If there are no intermittent segments, that's bad, shouldn't allow an intermittent request for time off at that point, no sir.
                if (!intSegments.Any())
                    throw new AbsenceSoftException("There are no active intermittent case periods for this case");

                foreach (IntermittentTimeRequest requestDate in timeRequested)
                {
                    // Get the applicable segment, since they can't overlap, not active ones anyway, then we need to find the one that
                    //  is appropriate for our request date.
                    CaseSegment seg = intSegments.FirstOrDefault(s => requestDate.RequestDate.DateInRange(s.StartDate, s.EndDate));

                    // Uh oh, there isn't one that covers our requested date, bail out and raise some red flags.
                    if (seg == null)
                        throw new AbsenceSoftException(string.Format("The requested date ({0:MM-dd-yyyy}) does not fall within any intermittent period for this case", requestDate.RequestDate));

                    // see if this date is already used, and if so remove it then add in the new request
                   // seg.UserRequests.RemoveAll(a => a.RequestDate == requestDate.RequestDate && (!a.IntermittentType.HasValue || a.IntermittentType == requestDate.IntermittentType));
                }

                ValidateTimeOffRequest(theCase, timeRequested);

                // verify all of the info before setting anything
                foreach (IntermittentTimeRequest requestDate in timeRequested)
                {
                    // Stage the total minutes for our intermittent request date based on the max entered time for all policies.
                    requestDate.TotalMinutes = requestDate.Detail.Any() ? requestDate.Detail.Max(d => d.Approved + d.Pending + d.Denied) : 0;

                    // Get the applicable segment, since they can't overlap, not active ones anyway, then we need to find the one that
                    //  is appropriate for our request date.
                    CaseSegment seg = intSegments.FirstOrDefault(s => requestDate.RequestDate.DateInRange(s.StartDate, s.EndDate));

                    // Uh oh, there isn't one that covers our requested date, bail out and raise some red flags.
                    if (seg == null)
                        throw new AbsenceSoftException(string.Format("The requested date ({0:MM-dd-yyyy}) does not fall within any intermittent period for this case", requestDate.RequestDate));

                    // Track whether we should run the time adjustment logic(AT-4805)
                    var adjustTime = true;
                    // Enumerate the request detail for that day
                    foreach (var detail in requestDate.Detail)
                    {
                        // Get the applied policy for this request detail row
                        var appliedPolicy = seg.AppliedPolicies.FirstOrDefault(p => p.Policy?.Code == detail.PolicyCode);
                        if (appliedPolicy == null)
                        {
                            // If we didn't find one, the state of the request is invalid so break processing and 
                            //  ensure we don't any further with our new adjustment logic
                            adjustTime = false;
                            break;
                        }
                        // Determine whether the user is requesting approved time
                        var hasApprovedTime = detail.Approved > 0;
                        // Determine whether the user is requesting pending time
                        var hasPendingTime = detail.Pending > 0;

                        // Get the appropriate policy usage for this policy and date.
                        var usage = appliedPolicy.Usage?.FirstOrDefault(u => u.DateUsed.ToMidnight() == requestDate.RequestDate.ToMidnight());

                        // If the usage is null/not found (invalid scenario)
                        //  or if it has approved time requested but no allowed time for that day
                        //  or if it has pending time requested but no allowed or pending time for that day
                        // then it's invalid and we should not run our custom adjustment logic
                        if (usage == null
                            || (usage.IntermittentDetermination != IntermittentStatus.Allowed && hasApprovedTime)
                            || ((usage.IntermittentDetermination != IntermittentStatus.Allowed || usage.IntermittentDetermination != IntermittentStatus.Pending) && hasPendingTime))
                        {
                            adjustTime = false;
                        }
                        if (usage != null && (usage.IntermittentDetermination == IntermittentStatus.Allowed && hasApprovedTime))
                        {
                            adjustTime = true;
                            break;
                        }
                    }

                    // Run our requested time detail adjustment logic based on actual status for the policy for this day
                    //  versus what was requested by the user for all policies
                    if (adjustTime)
                    {
                        foreach (var d in requestDate.Detail)
                        {
                            var appPolicyUsage = seg.AppliedPolicies.FirstOrDefault(pol => pol.Policy.Code == d.PolicyCode)?.Usage?
                                .FirstOrDefault(u => u.DateUsed == requestDate.RequestDate && u.IntermittentDetermination.HasValue);
                            if (appPolicyUsage != null)
                            {
                                switch (appPolicyUsage.IntermittentDetermination)
                                {
                                    case IntermittentStatus.Denied:
                                        d.Denied = d.Approved + d.Pending + d.Denied;
                                        d.DenialReasonCode = d.DenialReasonCode ?? appPolicyUsage.DenialReasonCode;
                                        d.DenialReasonName = d.DenialReasonName ?? appPolicyUsage.DenialReasonName;
                                        d.DeniedReasonOther = d.DenialReasonCode == AdjudicationDenialReason.Other ? d.DeniedReasonOther ?? appPolicyUsage.DenialExplanation : "";
                                        d.Approved = 0;
                                        d.Pending = 0;
                                        break;
                                    case IntermittentStatus.Pending:
                                        d.Pending = d.Approved + d.Pending;
                                        d.Approved = 0;
                                        break;
                                }
                            }
                        }
                    }
                    // see if this date is already used, and if so remove it then add in the new request
                    seg.UserRequests.RemoveAll(a => a.RequestDate == requestDate.RequestDate && a.Id == requestDate.Id);

                    // check to see if an approved total is being put against a non-allowed time
                    // that would be, if any of the details have an approved, and then for the policies on this segment
                    // none of the usage objects are tagged with an allowed
                    if (requestDate.Detail.Any(rda => rda.Approved > 0 && seg.AppliedPolicies.Any(s => s.Policy.Code == rda.PolicyCode &&
                            (s.Usage.Any(u => u.DateUsed == requestDate.RequestDate && u.IntermittentDetermination != IntermittentStatus.Allowed) || !s.Usage.Any(u => u.DateUsed == requestDate.RequestDate)))))
                        throw new AbsenceSoftException(string.Format("Approved total for {0:MM/dd/yyyy} is being used against a day that is not approved for intermitent time.", requestDate.RequestDate));

                    // Only add this back in if the user actually has some time in there.
                    if (requestDate.TotalMinutes > 0)
                    {
                        seg.UserRequests.Add(requestDate);
                        if (!requestDate.IsEditMode)
                            theCase.OnSavedNOnce((o, e) => requestDate.WfOnTimeOffRequested(e.Entity, CurrentUser));
                    }
                }

                theCase = RunCalcs(theCase);

                return theCase;
            }
        }//Case CreateOrModifyTimeOffRequest


        public void ValidateTimeOffRequest(Case theCase, List<IntermittentTimeRequest> timeRequested)
        {
            using (new InstrumentationContext("CaseService.ValidateTimeOffRequest"))
            {
                /*
                 * 1) TOR’s should be allowed as pending or denied within the leave request range 
                 * 2) TOR’s may not be approved outside of the policy approval range
                 * 3) TOR’s may not be entered beyond the leave request range 
                 * 4) When a user tries to enter a TOR outside of these parameters text advising of each of the appropriate 
                 *    rule above may be displayed, but the orange pop-up error boxes should not appear.
                 * 5) When a TOR is entered - if the total hours entered for all TORs for a day is greater than 24, show a 
                 *    validation message and don't allow
                 */
                List<string> errors = new List<string>();

                // get the holidays for the employer, so we can 0 out their usage for that day
                HolidayService hs = new HolidayService();
                List<int> years = Enumerable.Range(0, (theCase.EndDate.Value.Year - theCase.StartDate.Year) + 1).Select(i => theCase.StartDate.Year + i).ToList();
                List<DateTime> holidays = hs.GetHolidayList(years, theCase.Employer);

                // check that this is a scheduled day, and by scheduled we mean that they are scheduled to work some 
                // amount of time that day
                LeaveOfAbsence los = new LeaveOfAbsence();
                los.Employee = theCase.Employee;
                los.Employer = theCase.Employee.Employer;
                los.Customer = theCase.Employee.Customer;
                los.WorkSchedule = Employee.GetById(theCase.Employee.Id).WorkSchedules;

                //check if the policy is of type FixedWorkDays then skip the holidays
                var appliedPolicyType = theCase.Segments.Any(s => s.AppliedPolicies.Any(ap => ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays
                                 || ap.PolicyReason.EntitlementType == EntitlementType.CalendarMonths
                                 || ap.PolicyReason.EntitlementType == EntitlementType.CalendarWeeks
                                 || ap.PolicyReason.EntitlementType == EntitlementType.CalendarDays
                                 || ap.PolicyReason.EntitlementType == EntitlementType.CalendarYears));

                //check if the PolicyBehaviorType is of type Ignored then skip the holidays
                var policyBehaviorType = theCase.Segments.Any(s => s.AppliedPolicies.Any(ap => ap.PolicyBehaviorType == PolicyBehavior.Ignored));

                // get their schedule excluding the holidays (cuz we are directly checking the holidays)
                
                List<Time> regularTimes = los.MaterializeSchedule(theCase.StartDate, theCase.EndDate.Value, false, null,
                    policyBehaviorType ? PolicyBehavior.Ignored : PolicyBehavior.ReducesWorkWeek);
                foreach (IntermittentTimeRequest req in timeRequested)
                {
                    if ((req.TotalMinutes + GetExistingTORMinutes(theCase, req)) > 1440)
                        errors.AddFormat("The total time '{0}' requested for '{1:MM/dd/yyyy}' is '{2}' more than 24h, this is not allowed.",
                            req.TotalMinutes.ToFriendlyTime(),
                            req.RequestDate,
                            (req.TotalMinutes - 1440).ToFriendlyTime());

                    // check the holiday list
                    if (holidays.Any(h => h == req.RequestDate) && !appliedPolicyType && !policyBehaviorType)
                    {
                        errors.AddFormat("Request date '{0:MM/dd/yyyy}' is a holiday and may not be requested", req.RequestDate);
                        continue;
                    }

                    Time rt = regularTimes.FirstOrDefault(t => t.SampleDate == req.RequestDate);
                    // Only do below Check if TotalMinutes requested is > 0
                    if ((rt == null || rt.TotalMinutes == 0) && !appliedPolicyType && !policyBehaviorType && req.TotalMinutes > 0)
                    {
                        if (los.TimeLimitForTheDay(req.RequestDate) < 1)
                        {
                            errors.AddFormat("Requested date '{0:MM/dd/yyyy}' is not a scheduled work day", req.RequestDate);
                        }
                    }
                }

                if (errors.Any())
                    throw new AbsenceSoftAggregateException(errors.ToArray());
            }//using
        } // ValidateTimeOffRequest

        private int GetExistingTORMinutes(Case theCase, IntermittentTimeRequest requestDate)
        {
            CaseSegment theSegment = theCase.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && s.IsEligibleOrPending).FirstOrDefault(s => requestDate.RequestDate.DateInRange(s.StartDate, s.EndDate));
            if (theSegment == null || !theSegment.UserRequests.Any())
                return 0;
            return theSegment.UserRequests.Where(a => a.RequestDate == requestDate.RequestDate).Sum(t=> t.TotalMinutes);
        }

        public List<ValidationMessage> PreValidateTimeOffRequest(string caseId, List<IntermittentTimeRequest> timeRequested)
        {
            using (new InstrumentationContext("CaseService.PreValidateTimeOffRequest"))
            {
                /*
                 * 1) TOR’s should be allowed as pending or denied within the leave request range 
                 * 2) TOR’s may not be approved outside of the policy approval range
                 * 3) TOR’s may not be entered beyond the leave request range 
                 * 4) When a user tries to enter a TOR outside of these parameters text advising of each of the appropriate 
                 *    rule above may be displayed, but the orange pop-up error boxes should not appear.
                 * 5) When a TOR is entered - if the total hours entered for all TORs for a day is greater than 24, show a 
                 *    validation message and don't allow
                 * 6) When a TOR is entered - if a TOR already exists for that day, provide a warning message to the user to let 
                 *    them know that a TOR already exists (Are you sure?) but let them do this if they confirm.
                 * 7) When a TOR is entered - If the total time entered for all TORs is greater than the employee scheduled hours 
                 *    for that day, give the warning (Are you sure?) and let them do it if they confirm.
                 */

                List<ValidationMessage> messages = new List<ValidationMessage>();

                Case theCase = Case.GetById(caseId);
                if (theCase == null)
                {
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Warning, Message = "Case not found" });
                    return messages;
                }

                LeaveOfAbsence los = new LeaveOfAbsence();
                los.Employee = theCase.Employee;
                

                // Get all intermittent case segments
                var intSegments = theCase.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled && s.IsEligibleOrPending);
                // If there are no intermittent segments, that's bad, shouldn't allow an intermittent request for time off at that point, no sir.
                if (!intSegments.Any())
                    throw new AbsenceSoftException("There are no active intermittent case periods for this case");

                foreach (IntermittentTimeRequest requestDate in timeRequested)
                {
                    // Get the applicable segment, since they can't overlap, not active ones anyway, then we need to find the one that
                    //  is appropriate for our request date.
                    CaseSegment seg = intSegments.FirstOrDefault(s => requestDate.RequestDate.DateInRange(s.StartDate, s.EndDate));

                    // Uh oh, there isn't one that covers our requested date, bail out and raise some red flags.
                    if (seg == null)
                        throw new AbsenceSoftException(string.Format("The requested date ({0:MM-dd-yyyy}) does not fall within any intermittent period for this case", requestDate.RequestDate));

                    // see if this date is already used, and if so remove it then add in the new request
                    seg.UserRequests.RemoveAll(a => a.RequestDate == requestDate.RequestDate && a.Id == requestDate.Id);

                    //add a check against the certification
                    CertificationCheckResult ccr = CheckIntermittentTimeRequest(theCase, requestDate);
                    if (!ccr.Pass)
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Warning, Message = ccr.ErrorMessage });
                }

                var otherTORs = Case.AsQueryable().Where(c => c.Employee.Id == theCase.Employee.Id && c.Id != theCase.Id).ToList()
                                        .SelectMany(c => c.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled))
                                        .SelectMany(s => s.UserRequests.Where(u => timeRequested.Any(t => t.RequestDate == u.RequestDate))).ToList();

                var ws = Employee.GetById(theCase.Employee.Id).WorkSchedules;
                var appliedPolicyType = theCase.Segments.Any(s => s.AppliedPolicies.Any(ap => ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays));
                var holidayBehaviorTypeIgnoreExist = theCase.Segments.Any(s => s.AppliedPolicies.Any(ap => ap.PolicyBehaviorType == PolicyBehavior.Ignored));

                foreach (var req in timeRequested)
                {
                    var time = new LeaveOfAbsence() { Case = theCase, Employee = theCase.Employee, Employer = theCase.Employer, WorkSchedule = ws }
                            .MaterializeSchedule(req.RequestDate, req.RequestDate,false, null, 
                             holidayBehaviorTypeIgnoreExist ? PolicyBehavior.Ignored : PolicyBehavior.ReducesWorkWeek);

                    var minutesScheduled = time.Any() ? time.Sum(t => t.TotalMinutes ?? 0) : 0;
                    double fteTimeForWeek = los.TimeLimitForTheDay(req.RequestDate);

                    if(fteTimeForWeek > 1)
                    {
                        var currentWeekTORs = Case.AsQueryable().Where(c => c.Employee.Id == theCase.Employee.Id).ToList()
                                        .SelectMany(c => c.Segments.Where(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled))
                                        .SelectMany(s => s.UserRequests.Where(u => u.RequestDate.AddDays(-(int)u.RequestDate.DayOfWeek) == req.RequestDate.AddDays(-(int)req.RequestDate.DayOfWeek))).Sum(u => u.TotalMinutes);
                        currentWeekTORs += req.TotalMinutes;
                        if (currentWeekTORs > fteTimeForWeek)
                        {
                            messages.Add(new ValidationMessage()
                            {
                                ValidationType = ValidationType.Warning,
                                Message = "The total time requested in this week is more than the employee's scheduled weekly hours.",
                            });
                        }
                    }

                    if (minutesScheduled < req.TotalMinutes && !appliedPolicyType && fteTimeForWeek < 1)
                    {
                        // remove duplicate messages
                        if (messages.Count > 0 && messages.Any(m => m.Message.Contains("applicable based on the employee's work schedule")))
                        {
                            messages.RemoveAll(m => m.Message.Contains("applicable based on the employee's work schedule"));
                        }

                        messages.Add(new ValidationMessage()
                        {
                            ValidationType = ValidationType.Warning,
                            Message = string.Format("The total time '{0}' requested for '{1:MM/dd/yyyy}' is '{2}' more than the employee's scheduled hours of '{3}'.",
                                req.TotalMinutes.ToFriendlyTime(),
                                req.RequestDate,
                                (req.TotalMinutes - minutesScheduled).ToFriendlyTime(),
                                minutesScheduled.ToFriendlyTime())
                        });
                    }

                    var match = otherTORs.FirstOrDefault(t => t.RequestDate == req.RequestDate);
                    if (match != null && fteTimeForWeek < 1)
                        messages.Add(new ValidationMessage()
                        {
                            ValidationType = ValidationType.Warning,
                            Message = string.Format("The requested date of '{0:MM/dd/yyyy}' already has a time off request on the same day for '{1}', please ensure this entry is valid.",
                                req.RequestDate,
                                match.TotalMinutes.ToFriendlyTime())
                        });
                }

                // now that every thing is verified create the request and recalc everything
                // use the existing validate time off request method, since that will already tell us if there are going to
                //  be any other errors, 1-5 in our list.
                try
                {
                    theCase = CreateOrModifyTimeOffRequest(theCase, timeRequested);
                }
                catch (AbsenceSoftAggregateException aggEx)
                {
                    foreach (string ex in aggEx.Messages)
                    {
                        messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = ex });
                    }
                }
                catch (AbsenceSoftException abEx)
                {
                    messages.Add(new ValidationMessage() { ValidationType = ValidationType.Error, Message = abEx.Message });
                }

                return messages;
            }//using
        } // PreValidateTimeOffRequest


        /// <summary>
        /// Process the intermittent segment. Basically, everything is set to pending and any statuses the
        /// user entered are copied.
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        /// <param name="userOverrides"></param>
        private void ProcessIntermittentSegment(CaseSegment theSegment, Case theCase, List<PolicyUsageTotals> policyTotals, List<PolicyUsageTotals> deniedTotals, SegmentUsage userOverrides)
        {
            // only apply the policy that have been approved (either pending or approved)
            foreach (AppliedPolicy ap in theSegment.AppliedPolicies)
                if (ap.Status != EligibilityStatus.Ineligible)
                    ApplyIntermittentPolicy(theSegment, theCase, userOverrides, ap, policyTotals, deniedTotals);

        }// ProcessIntermittentSegment

        /// <summary>
        /// Intermittent Type are handled differently , we evaluate only when TOR gets added . but that is not helping in scenario where 
        /// we have depdenent policy
        /// </summary>
        /// <param name="myCase"></param>
        /// <param name="policyCode"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="status"></param>
        /// <param name="reasonCode"></param>
        /// <param name="explanation"></param>
        /// <param name="reasonName"></param>
        private void AdjustIntermittentDeniedDates(Case myCase, string policyCode, DateTime startDate, DateTime endDate, AdjudicationStatus status, string reasonCode = null, string explanation = null, string reasonName = null)
        {
            startDate = startDate.ToMidnight();
            endDate = endDate.ToMidnight();
            reasonCode = string.IsNullOrWhiteSpace(reasonCode) ? reasonCode : reasonCode.ToUpperInvariant();

            if (myCase == null)
                throw new ArgumentNullException("myCase");
            if (endDate < startDate)
                throw new AbsenceSoftException("End Date occurrs before Start Date, did you intend to reverse them?");
            if (status == AdjudicationStatus.Denied && string.IsNullOrWhiteSpace(reasonCode))
                throw new AbsenceSoftException("Denial Reason must be specified if the approval decision is Denied");
            if (reasonCode == AdjudicationDenialReason.Other && string.IsNullOrWhiteSpace(explanation))
                throw new AbsenceSoftException("Denial Reason other must be specified if the approval decision is Denied and the Reason 'Other' is selected");
            if (User.Current != null && !User.Current.HasEmployerAccess(myCase.EmployerId, Permission.AdjudicateCase))
                throw new AbsenceSoftException("User does not have access to make an approval decision for a case with this employer");

            // Ensure if this is going to be denied, we don't have any approved intermittent time during that period, otherwise, watch out buster
            if (status == AdjudicationStatus.Denied)
            {

                var deniedWorkDays = myCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode))
                        .SelectMany(p => p.Usage).Count(u => u.DateUsed >= startDate && u.DateUsed <= endDate && u.MinutesInDay > 0);

                if (deniedWorkDays > 0)
                {
                    myCase.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode))
                        .SelectMany(p => p.Usage).Where(u => u.DateUsed < startDate || u.DateUsed > endDate)
                        .Where(u => !(u.IsLocked ?? false) && u.Determination == AdjudicationStatus.Denied && u.DenialReasonCode == AdjudicationDenialReason.Exhausted)
                        .OrderBy(u => u.DateUsed).ForEach(u =>
                        {
                            if (u.MinutesInDay > 0)
                            {
                                deniedWorkDays -= 1;
                            }
                            if (deniedWorkDays >= 0)
                            {
                                u.Determination = AdjudicationStatus.Pending;
                                u.DenialReasonCode = null;
                                u.DenialExplanation = "";
                                u.UserEntered = true;
                            }
                        });
                }
            }

            // do the update skipping any ones that are locked
            myCase.Segments.Where(s => s.Type != CaseType.Intermittent).SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode))
                .SelectMany(p => p.Usage.Where(u => u.DateUsed >= startDate && u.DateUsed <= endDate))
                .Where(u => !u.IsLocked.HasValue || !u.IsLocked.Value)
                .ForEach(u =>
                {
                    u.Determination = status;
                    u.DenialReasonCode = reasonCode;
                    u.DenialReasonName = reasonName;
                    u.DenialExplanation = explanation;
                    u.UserEntered = true;
                });

            // fix up the status, 
            AdjudicationStatus forIntStatus = status;
            if (status == AdjudicationStatus.Approved)
                forIntStatus = AdjudicationStatus.Pending;

            // do the update skipping any ones that are locked
            myCase.Segments.Where(s => s.Type == CaseType.Intermittent).SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode))
                .SelectMany(p => p.Usage.Where(u => u.DateUsed >= startDate && u.DateUsed <= endDate))
                .Where(u => !u.IsLocked.HasValue || !u.IsLocked.Value)
                .ForEach(u =>
                {
                    u.Determination = forIntStatus;
                    u.DenialReasonCode = reasonCode;
                    u.DenialReasonName = reasonName;
                    u.DenialExplanation = explanation;
                    u.UserEntered = true;
                    u.IntermittentDetermination = status.ToIntermittentStatus();
                });

            myCase.Save();
            if (RunWorkflow)
            {
                //Let's be sure we only invoke this for policy codes actually updated/changed by this process.
                myCase.Segments
                    .SelectMany(s => s.AppliedPolicies.Where(p => p.Policy.Code == policyCode))
                    .Distinct(p => p.Policy.Code)
                    .ForEach(p => p.WfOnPolicyAdjudicated(myCase, status, reasonCode, explanation, reasonName));
            }
        }
        /// <summary>
        /// Process the policy under an intermittent segment
        /// </summary>
        /// <param name="theSegment"></param>
        /// <param name="theCase"></param>
        /// <param name="userOverrides"></param>
        /// <param name="ap"></param>
        private void ApplyIntermittentPolicy(CaseSegment theSegment, Case theCase, SegmentUsage userOverrides, AppliedPolicy ap, List<PolicyUsageTotals> policyTotals, List<PolicyUsageTotals> deniedTotals)
        {
            //IS arileine request
           // var appliedPolicyType = theCase.Segments.Any(s => s.AppliedPolicies.Any(a => ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays));
            // find their schedule across this time period
            LeaveOfAbsence los = new LeaveOfAbsence();
            los.Employee = theCase.Employee;
            los.Employer = theCase.Employee.Employer;
            los.Customer = theCase.Employee.Customer;
            los.WorkSchedule = Employee.GetById(theCase.Employee.Id).WorkSchedules;

            // get their schedule including the holidays

            List<Time> regularTimes = los.MaterializeSchedule(ap.StartDate, ap.EndDate.Value, false, null, ap.PolicyBehaviorType);
            List<DateTime> fteDates = new List<DateTime>();

            // get the holidays for the employer, so we can 0 out their usage for that day
            HolidayService hs = new HolidayService();
            List<int> years = Enumerable.Range(0, (ap.EndDate.Value.Year - ap.StartDate.Year) + 1).Select(i => ap.StartDate.Year + i).ToList();
            List<DateTime> holidays = hs.GetHolidayList(years, theCase.Employer);

            // find out how much of the elimination period has been used up
            PolicyUsageTotals deniedTotal = deniedTotals.FirstOrDefault(f => f.PolicyCode == ap.Policy.Code);
            if (deniedTotal == null)
            {
                deniedTotal = new PolicyUsageTotals() { PolicyCode = ap.Policy.Code };
                deniedTotals.Add(deniedTotal);
            }

            // we need help (used for date calcs)
            AppliedPolicyUsageHelper theHelper = new AppliedPolicyUsageHelper(los, theCase.StartDate, theCase.EndDate.Value);

            // everything is in it's own units of measurements, could be minutes
            // days, weeks or months. We don't care we just calc to the 
            // unit
            double unitsToApply = ap.PolicyReason.Entitlement.Value;
            var policyReasonCode = !string.IsNullOrEmpty(ap.PolicyReason.ReasonCode) ? ap.PolicyReason.ReasonCode : ap.PolicyReason.Reason.Code;
            // get the units already used on this policy, if it doesn't exist then create it
            // this tracks our usage across all the segments in the case
            PolicyUsageTotals put = policyTotals.FirstOrDefault(f => f.PolicyCode == ap.Policy.Code);
            if (put == null)
            {
                put = new PolicyUsageTotals() { PolicyCode = ap.Policy.Code };
                policyTotals.Add(put);
            }

            // calc gap usages
            int gapUsageCount = ap.PolicyReason.EntitlementType.Value.CalendarTypeFromFirstUse() ? CalculateGapUsage(ap, theCase, policyTotals) : 0;

            // check to see if there are any regular times and if so, is the first day exhausted. If it is, then 
            // loop until we find the first non-exhausted day
            if (regularTimes !=null && regularTimes.Count > 0)
            {
                DateTime firstRealDay = regularTimes[0].SampleDate;
                foreach (Time normalWorkDay in regularTimes)
                {
                    // break down the usage into parts that can be measured against
                    List<AppliedPolicyUsageType> todaysTestUsage = theHelper.GetUsage(normalWorkDay.SampleDate);
                    
                    // the look back period need to be applied for each day. As the year progresses they can get back time 
                    // that has aged off. Each work day we need to recompute their prior usage across all cases 
                    // including this one to see if they still have time left
                  
                    CalculatePriorUsage priorRecalc = new CalculatePriorUsage(theCase, normalWorkDay.SampleDate, true, true, false, ap.Policy.Code, true,
                        AdjudicationStatus.Approved);

                    PolicyUsageHistory puh = priorRecalc.UsageHistory;
                    double minutesUsedForPolicyAndDay = puh[ap.Policy.Code].Allocated.Where(du => du.DateUsed == normalWorkDay.SampleDate).Sum(du => du.MinutesUsed); //intermittenAmt + currentAmt;

                    // calculate like the user has requested the entire day off
                    PolicyUsageTotals alreadyExhausted = new PolicyUsageTotals()
                    {
                        PolicyCode = put.PolicyCode,
                        Allocated = puh.UsageTotals.Where(p => p.PolicyCode == ap.Policy.Code).SelectMany(p => p.Allocated).Where(a => a.Determination == AdjudicationStatus.Approved).ToList()
                    };

                    // we don't need to request any minutes because we are only checking to see if it is already
                    // exhausted (usage for today does not matter because we only care about history)
                    AppliedPolicyUsage exhaustedUsage = new AppliedPolicyUsage()
                    {
                        MinutesUsed = 0,
                        MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                        Determination = AdjudicationStatus.Pending,
                        DateUsed = normalWorkDay.SampleDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysTestUsage,
                        IntermittentDetermination = IntermittentStatus.Pending
                    };

                    CalcUsage calcExhausted = new CalcUsage(exhaustedUsage, alreadyExhausted, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false,
                        theSegment.Type, gapUsageCount,null,los);

                    // now, if any amount is exhausted, create the row for that. If it is the entire day, then go do the
                    // next day (we don't care what the user says at that point. Exhausted is exhausted), if the entire 
                    // day is not exhausted then the calcs below should adjust the time based on what is left
                    if (calcExhausted.ReachedTheLimit || calcExhausted.WentOverTheLimit)
                    {
                        AppliedPolicyUsage itsUsedUpAlready = new AppliedPolicyUsage()
                        {
                            MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : calcExhausted.NewTotal,
                            MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                            Determination = AdjudicationStatus.Denied,
                            DateUsed = normalWorkDay.SampleDate,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode,
                            AvailableToUse = todaysTestUsage,
                            IntermittentDetermination = IntermittentStatus.Denied,
                            DenialReasonCode = AdjudicationDenialReason.Exhausted,
                            DenialReasonName = AdjudicationDenialReason.ExhaustedDescription
                        };
                        ap.Usage.Add(itsUsedUpAlready);

                        // it's exhausted so there is nothing left to do for today so go do the next day
                        continue;
                    }
                    else
                    {
                        firstRealDay = normalWorkDay.SampleDate;
                        break;
                    }

                }                       // foreach (Time normalWorkDay in regularTimes)

                // now, whatever our first 'real' day was, redo our regualr time and process the rest from there
                regularTimes = regularTimes.Where(rt => rt.SampleDate >= firstRealDay).ToList();

            }

            // intermittents are processed a little differently.
            // Instead of working through the abscence collection, we roll across their 
            // regular schedule. And then we do two things. First look to see if this has
            // been requested in the absences, and then second if it has been requested see
            // what the status of it was set to before (approved or denied)
            if (regularTimes != null)
            {
               
                foreach (Time normalWorkDay in regularTimes)
                {
                    //int xxxx = 0;
                    //if (ap.Policy.Code == "0076")
                    //{
                    //    xxxx = 1;
                    //    if (normalWorkDay.SampleDate == new DateTime(2018, 11, 30, 0, 0, 0, DateTimeKind.Utc))
                    //        xxxx = 1;

                    //}

                    double fteTimeForWeek = 0;
                    int? normalWorkDayMinutes = 0;

                    if (ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays)
                    {
                        normalWorkDay.TotalMinutes = 1;
                    }
                    // 0 out the hours on a holiday
                    if (holidays.Any(h => h == normalWorkDay.SampleDate) && ap.PolicyBehaviorType != PolicyBehavior.Ignored && ap.PolicyReason.EntitlementType != EntitlementType.FixedWorkDays)
                    {
                        normalWorkDay.TotalMinutes = 0;
                    }
                    // break down the usage into parts that can be measured against
                    List<AppliedPolicyUsageType> todaysUsage = theHelper.GetUsage(normalWorkDay.SampleDate);

                    // see if there is already a usage object that the user has interacted with (this would have been set with adjudication)
                    AppliedPolicyUsage userOverride = userOverrides.FindUsage(theSegment.Id, ap.Id, normalWorkDay.SampleDate);

                    // now see if there is a time off requst for today
                    IntermittentTimeRequest todaysRequest = theSegment.UserRequests.FirstOrDefault(ab => ab.RequestDate == normalWorkDay.SampleDate);
                    List<IntermittentTimeRequest> torRequest = theSegment.UserRequests.Where(ab => ab.RequestDate == normalWorkDay.SampleDate).ToList();

                    int torTotalMinutes = theSegment.UserRequests.Where(ab => ab.RequestDate == normalWorkDay.SampleDate).Sum(ab => ab.TotalMinutes);
                    IntermittentTimeRequestDetail todaysDetail = null;

                    int totalApprovedMinutes = torRequest.SelectMany(t => t.Detail.Where(r => r.PolicyCode == ap.Policy.Code)).Sum(p => p.Approved);
                    int overlapTime = GetOverLapTime(torRequest);
                    totalApprovedMinutes = (totalApprovedMinutes - overlapTime) < 0 ? 0 : totalApprovedMinutes - overlapTime;
                    int totalDeniedMinutes = torRequest.SelectMany(t => t.Detail.Where(r => r.PolicyCode == ap.Policy.Code)).Sum(p => p.Denied);
                    int totalPendingMinutes = torRequest.SelectMany(t => t.Detail.Where(r => r.PolicyCode == ap.Policy.Code)).Sum(p => p.Pending);
                    if (ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays)
                    {
                        totalApprovedMinutes = totalApprovedMinutes > 0 ? 1440 : totalApprovedMinutes;
                        totalDeniedMinutes = totalDeniedMinutes > 0 ? 1440 : totalDeniedMinutes;
                        totalPendingMinutes = totalPendingMinutes > 0 ? 1440 : totalPendingMinutes;
                    }
                    if (todaysRequest != null)
                    {
                        todaysDetail = todaysRequest.Detail.FirstOrDefault(rd => rd.PolicyCode == ap.Policy.Code);
                        if (torTotalMinutes <= 0)
                        {
                            foreach (IntermittentTimeRequest tor in torRequest)
                            {
                                theSegment.UserRequests.Remove(tor);
                            }
                            todaysDetail = null;
                            todaysRequest = null;
                        }

                    }

                    // do a check to see if today has been set to denied, if it has and there is a user
                    // request, then anything the user requests is denied
                    if (userOverride != null && userOverride.IntermittentDetermination == IntermittentStatus.Denied)
                    {
                        //user might have updated the TOR from denied to pending
                        //set determination to pending if so, 
                        //If DenialReason is present then user didn't updated from denied to pending , so keep the status
                        //as it is
                        AdjudicationStatus status = userOverride.Determination;
                        if (todaysDetail != null && totalPendingMinutes > 0 && string.IsNullOrEmpty(userOverride.DenialReasonCode))
                        {
                            status = AdjudicationStatus.Pending;
                        }
                        AppliedPolicyUsage shotDown = new AppliedPolicyUsage()
                        {
                            MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                            MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                            Determination = status,
                            DenialExplanation = userOverride.DenialExplanation,
                            AvailableToUse = todaysUsage,
                            DateUsed = normalWorkDay.SampleDate,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode,
                            DenialReasonCode = userOverride.DenialReasonCode,
                            DenialReasonName = userOverride.DenialReasonName,
                            DeterminedById = userOverride.DeterminedById,
                            IntermittentDetermination = userOverride.IntermittentDetermination,
                            UserEntered = userOverride.UserEntered
                        };

                        // if there is a request for today then assign all of it to denied (but doesn't exceed the hours in the day)
                        if (todaysDetail != null)
                            shotDown.MinutesUsed = Math.Min(totalPendingMinutes + totalApprovedMinutes + totalDeniedMinutes, (normalWorkDay.TotalMinutes ?? 0));

                        // add it to the usage and do the next day
                        ap.Usage.Add(shotDown);
                        continue;
                    }

                    // if there is no request for today, do a quick check to see if the status has already been set
                    // to Intermittent Allowed, create a row and do the next date
                    // or if there is a request for today but not for this policy
                    if (todaysRequest == null || todaysDetail == null || todaysRequest.TotalMinutes <= 0 || 
                        (totalApprovedMinutes + totalPendingMinutes + totalDeniedMinutes) <= 0)
                    {
                        // create the default item
                        AppliedPolicyUsage addingThisThing = new AppliedPolicyUsage()
                        {
                            MinutesUsed = 0,
                            MinutesInDay = (normalWorkDay.TotalMinutes ?? 0),
                            Determination = AdjudicationStatus.Pending,
                            DateUsed = normalWorkDay.SampleDate,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode,
                            AvailableToUse = todaysUsage,
                            IntermittentDetermination = IntermittentStatus.Pending,
                            UserEntered = false
                        };

                        // if the user created something before then copy it
                        if (userOverride != null)
                        {
                            addingThisThing.IntermittentDetermination = userOverride.IntermittentDetermination;
                            addingThisThing.Determination = userOverride.Determination;
                            addingThisThing.DenialReasonCode = userOverride.DenialReasonCode;
                            addingThisThing.DenialReasonName = userOverride.DenialReasonName;
                            addingThisThing.DenialExplanation = userOverride.DenialExplanation;
                            addingThisThing.UserEntered = true;
                        }

                        // add it to the list
                        ap.Usage.Add(addingThisThing);

                        // do the next work day
                        continue;
                    }

                    // step 1. reduce the request by any denied time (and deny the time)
                    // first check for denied time because this will reduce the requested total, and we can enter it
                    // straight into the data 
                    if (totalDeniedMinutes > 0)
                    {
                        // if the reason was exhausted, well, users aren't really allowed to use that one, so it will
                        // fall through to process and we will ignore they did it
                        if (todaysDetail.DenialReasonCode != AdjudicationDenialReason.Exhausted)
                        {
                            int deniedAmount = totalDeniedMinutes;

                            // make sure it doesn't exceed the amount allowed for today
                            if (deniedAmount > normalWorkDay.TotalMinutes)
                                deniedAmount = (normalWorkDay.TotalMinutes ?? 0);

                            AppliedPolicyUsage addingThisThing = new AppliedPolicyUsage()
                            {
                                MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : deniedAmount,
                                MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                                Determination = AdjudicationStatus.Denied,
                                DateUsed = normalWorkDay.SampleDate,
                                PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                AvailableToUse = todaysUsage,
                                IntermittentDetermination = IntermittentStatus.Pending
                            };
                            if (userOverride != null)
                            {
                                // We carry over the underlying denial reason/explanation if any, that's because
                                //  the intermittent one is stored in the user request itself, NOT on the usage
                                //  on the Usage we're storing the one provided by the user from the UI when denying the
                                //  actual policy itself (rather than a specific absence event).
                                addingThisThing.DenialReasonCode = userOverride.DenialReasonCode;
                                addingThisThing.DenialReasonName = userOverride.DenialReasonName;
                                addingThisThing.DenialExplanation = userOverride.DenialExplanation;
                                addingThisThing.IntermittentDetermination = userOverride.IntermittentDetermination;
                                addingThisThing.UserEntered = true;
                            }

                            ap.Usage.Add(addingThisThing);
                            deniedTotal.AddAllocated(normalWorkDay.SampleDate, deniedAmount, (normalWorkDay.TotalMinutes ?? 0),
                                policyReasonCode, AdjudicationStatus.Denied, todaysDetail.DenialReasonCode,
                                addingThisThing.IntermittentDetermination, todaysUsage,null,null,false,theCase.CaseNumber);

                            // if the user denied the entire day there is no reason to do anything else (even if there is more time
                            // entered in the list)(denied is denied) go do the next day
                            if (deniedAmount >= normalWorkDay.TotalMinutes)
                                continue;
                        }
                    } // if(userEnteredRequest && userRequested.Denied > 0)

                    // now we are left with pending and approved, so, for the moment we are going to combine them and treat them 
                    // as one (since for usage that is how the calcs look at them
                    // this does the work for us, and we pass it to the calc usage class
                    AppliedPolicyUsage workUsage = new AppliedPolicyUsage()
                    {
                        MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : totalPendingMinutes + totalApprovedMinutes,
                        MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                        Determination = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? AdjudicationStatus.Approved : AdjudicationStatus.Pending,
                        DateUsed = todaysRequest.RequestDate,
                        PolicyReasonCode = ap.PolicyReason.ReasonCode,
                        AvailableToUse = todaysUsage,
                        IntermittentDetermination = IntermittentStatus.Pending
                    };
                    if (userOverride != null)
                    {
                        // We carry over the underlying denial reason/explanation if any, that's because
                        //  the intermittent one is stored in the user request itself, NOT on the usage
                        //  on the Usage we're storing the one provided by the user from the UI when denying the
                        //  actual policy itself (rather than a specific absence event).
                        workUsage.DenialReasonCode = userOverride.DenialReasonCode;
                        workUsage.DenialReasonName = userOverride.DenialReasonName;
                        workUsage.DenialExplanation = userOverride.DenialExplanation;
                        workUsage.IntermittentDetermination = userOverride.IntermittentDetermination;
                        workUsage.UserEntered = true;
                    }
                    
                    // the look back period need to be applied for each day. As the year progresses they can get back time 
                    // that has aged off. Each work day we need to recompute their prior usage across all cases 
                    // including this one to see if they still have time left
                    CalculatePriorUsage priorRecalc = new CalculatePriorUsage(theCase, normalWorkDay.SampleDate, true, true, false, ap.Policy.Code, true, AdjudicationStatus.Approved);

                    PolicyUsageHistory puh = priorRecalc.UsageHistory;

                    // just a quick check, if the original amount entered is greater than the work day (who knows why)
                    // then fix it up to the work day, otherwise leave it alone
                    if (workUsage.MinutesUsed > normalWorkDay.TotalMinutes)
                    {
                        fteTimeForWeek = los.TimeLimitForTheDay(normalWorkDay.SampleDate);
                        if (fteTimeForWeek < 1)
                        {
                            workUsage.MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0);
                        }
                        else
                        {

                            normalWorkDayMinutes = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : normalWorkDay.TotalMinutes;
                            normalWorkDay.TotalMinutes = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (int)workUsage.MinutesUsed;
                            workUsage.MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : workUsage.MinutesUsed;
                            fteDates.Add(normalWorkDay.SampleDate);
                        }
                    }

                    double minutesUsedForPolicyAndDay = puh[ap.Policy.Code].Allocated.Where(du => du.DateUsed == normalWorkDay.SampleDate).Sum(du => du.MinutesUsed); //intermittenAmt + currentAmt;

                    PolicyUsageTotals calcLookBack = new PolicyUsageTotals()
                    {
                        PolicyCode = put.PolicyCode,
                        Allocated = puh.UsageTotals.Where(p => p.PolicyCode == ap.Policy.Code).SelectMany(p => p.Allocated).Where(a => a.Determination == AdjudicationStatus.Approved).ToList()
                    };

                    CalcUsage entitlementUsage = new CalcUsage(workUsage, calcLookBack, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, 
                        theSegment.Type, gapUsageCount, workUsage.DateUsed,los);

                    //This is specific to FTE schedule. If Employees FTE weekly hours is 40h and calculated schedule has 8h daily work hours(M-F)                    
                    //In reality, employee may be working 10h a day for 4 days in week(M-Th). Assuming Policy entitlement is of 60 days
                    //Last TOR(60th day) of 10h will get reduced to 8h. So,  we have to consider a day, in this scenario, instead of hours/minutes
                    if (entitlementUsage.DeniedCuzOverTheLimit > 0 && (int)entitlementUsage.NewTotal == (normalWorkDayMinutes ?? 0) && entitlementUsage.NewTotal > 0) 
                    {
                        workUsage.MinutesUsed = normalWorkDayMinutes ?? 0;
                        entitlementUsage = new CalcUsage(workUsage, calcLookBack, ap.PolicyReason.EntitlementType.Value, unitsToApply, ap, false, 
                            theSegment.Type, gapUsageCount, workUsage.DateUsed, los);
                        workUsage.MinutesUsed = normalWorkDay.TotalMinutes ?? 0;
                    }

                    // if they have minutes and the status is not denied (remember, we reset it to IntermittentAllowed if it was previously exhausted)
                    if (entitlementUsage.DeniedCuzOverTheLimit > 0 || (normalWorkDay.TotalMinutes >= (entitlementUsage.NewTotal) && normalWorkDay.TotalMinutes > 0))
                    {
                        double workTotalMins = (int)entitlementUsage.NewTotal == normalWorkDayMinutes.Value && normalWorkDayMinutes > 0 ? workUsage.MinutesUsed : entitlementUsage.NewTotal;
                        double amountWeAreUsing = 0;

                        // now go back to the oringal request and split it out
                        // pending wins first and approved gets what ever is left over
                        if (totalPendingMinutes > 0)
                        {
                            amountWeAreUsing = Math.Min(workTotalMins, totalPendingMinutes);
                            if (amountWeAreUsing > 0)
                            {
                                var myUsage = ap.Usage.AddFluid(new AppliedPolicyUsage()
                                {
                                    MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : amountWeAreUsing,
                                    MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 :  (normalWorkDay.TotalMinutes ?? 0),
                                    Determination = AdjudicationStatus.Pending,
                                    DateUsed = normalWorkDay.SampleDate,
                                    PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                    AvailableToUse = todaysUsage,
                                    IntermittentDetermination = IntermittentStatus.Pending
                                });
                                if (userOverride != null)
                                {
                                    // We carry over the underlying denial reason/explanation if any, that's because
                                    //  the intermittent one is stored in the user request itself, NOT on the usage
                                    //  on the Usage we're storing the one provided by the user from the UI when denying the
                                    //  actual policy itself (rather than a specific absence event).
                                    myUsage.DenialReasonCode = userOverride.DenialReasonCode;
                                    myUsage.DenialReasonName = userOverride.DenialReasonName;
                                    myUsage.DenialExplanation = userOverride.DenialExplanation;
                                    myUsage.IntermittentDetermination = userOverride.IntermittentDetermination;
                                    myUsage.UserEntered = true;
                                }

                                workTotalMins -= amountWeAreUsing;

                                // add the usage
                                put.AddAllocated(normalWorkDay.SampleDate, amountWeAreUsing, (normalWorkDay.TotalMinutes ?? 0),
                                    policyReasonCode, workUsage.Determination, null, myUsage.IntermittentDetermination,
                                    todaysUsage,null,null,false,theCase.CaseNumber);
                            }
                        }

                        // now the approved total get's its cut
                        if (totalApprovedMinutes > 0)
                        {
                            amountWeAreUsing = Math.Min(workTotalMins, totalApprovedMinutes);
                            if (amountWeAreUsing > 0)
                            {
                                var myUsage = ap.Usage.AddFluid(new AppliedPolicyUsage()
                                {
                                    MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : amountWeAreUsing,
                                    MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                                    Determination = AdjudicationStatus.Approved,
                                    DateUsed = normalWorkDay.SampleDate,
                                    PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                    AvailableToUse = todaysUsage,
                                    IntermittentDetermination = IntermittentStatus.Pending,
                                    IsIntermittentRestriction = todaysRequest.IsIntermittentRestriction

                                });
                                if (userOverride != null)
                                {
                                    // We carry over the underlying denial reason/explanation if any, that's because
                                    //  the intermittent one is stored in the user request itself, NOT on the usage
                                    //  on the Usage we're storing the one provided by the user from the UI when denying the
                                    //  actual policy itself (rather than a specific absence event).
                                    myUsage.DenialReasonCode = userOverride.DenialReasonCode;
                                    myUsage.DenialReasonName = userOverride.DenialReasonName;
                                    myUsage.DenialExplanation = userOverride.DenialExplanation;
                                    myUsage.IntermittentDetermination = userOverride.IntermittentDetermination;
                                    myUsage.UserEntered = true;
                                }

                                // add the usage
                                put.AddAllocated(normalWorkDay.SampleDate, amountWeAreUsing, (normalWorkDay.TotalMinutes ?? 0),
                                    policyReasonCode, workUsage.Determination, null, myUsage.IntermittentDetermination,
                                    todaysUsage, null, null, false, theCase.CaseNumber);

                                workTotalMins -= Math.Min(workTotalMins, totalApprovedMinutes);
                            }
                        }

                        // now, what if there are some left? It's obviously exhausted 'n' stuff,
                        //  so do that.
                        if (workTotalMins > 0)
                        {
                            var myUsage = ap.Usage.AddFluid(new AppliedPolicyUsage()
                            {
                                MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : Math.Min(workTotalMins, totalApprovedMinutes),
                                MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                                Determination = AdjudicationStatus.Denied,
                                DenialReasonCode = AdjudicationDenialReason.Exhausted,
                                DenialReasonName = AdjudicationDenialReason.ExhaustedDescription,
                                DateUsed = normalWorkDay.SampleDate,
                                PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                AvailableToUse = todaysUsage,
                                IntermittentDetermination = IntermittentStatus.Pending
                            });
                            if (userOverride != null)
                            {
                                // We carry over the underlying denial reason/explanation if any, that's because
                                //  the intermittent one is stored in the user request itself, NOT on the usage
                                //  on the Usage we're storing the one provided by the user from the UI when denying the
                                //  actual policy itself (rather than a specific absence event).
                                myUsage.DenialReasonCode = userOverride.DenialReasonCode;
                                myUsage.DenialReasonName = userOverride.DenialReasonName;
                                myUsage.DenialExplanation = userOverride.DenialExplanation;
                                myUsage.IntermittentDetermination = userOverride.IntermittentDetermination;
                                myUsage.UserEntered = true;
                            }
                            deniedTotal.AddAllocated(normalWorkDay.SampleDate, workTotalMins, (normalWorkDay.TotalMinutes ?? 0),
                                policyReasonCode, AdjudicationStatus.Denied, AdjudicationDenialReason.Exhausted,
                                myUsage.IntermittentDetermination, todaysUsage,null, null, false, theCase.CaseNumber);
                        }

                        // the amount requested could have been adjusted down because it was over the limit
                        // if it was it would be within the bounds of the number of hours in the day
                        // but we still need to record that a part of the request was denied
                        if (entitlementUsage.DeniedCuzOverTheLimit > 0)
                        {
                            var myUsage = ap.Usage.AddFluid(new AppliedPolicyUsage()
                            {
                                MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : entitlementUsage.DeniedCuzOverTheLimit,
                                MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                                DenialReasonCode = AdjudicationDenialReason.Exhausted,
                                DenialReasonName = AdjudicationDenialReason.ExhaustedDescription,
                                Determination = AdjudicationStatus.Denied,
                                DateUsed = normalWorkDay.SampleDate,
                                PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                AvailableToUse = todaysUsage,
                                IntermittentDetermination = IntermittentStatus.Pending
                            });
                            if (userOverride != null)
                            {
                                // We don't override the denial reason, because it's exhausted, which is special
                                myUsage.IntermittentDetermination = userOverride.IntermittentDetermination;
                                myUsage.UserEntered = true;
                            }

                            deniedTotal.AddAllocated(normalWorkDay.SampleDate, entitlementUsage.DeniedCuzOverTheLimit, (normalWorkDay.TotalMinutes ?? 0),
                                policyReasonCode, AdjudicationStatus.Denied, AdjudicationDenialReason.Exhausted, 
                                myUsage.IntermittentDetermination, todaysUsage,null,null,false,theCase.CaseNumber);
                        }
                    } // if (normalWorkDay.TotalMinutes >= (totalRequested + minutesUsedForPolicyAndDay))
                    else
                    {
                        // we are asking for more than is available, approve what is left and deny the rest
                        double amountToUse = (normalWorkDay.TotalMinutes ?? 0) - minutesUsedForPolicyAndDay;
                        double amountDenied = entitlementUsage.NewTotal - amountToUse;

                        AdjudicationStatus setStatusTo = workUsage.Determination;

                        // if the amount denied = the amount that we were going to pend, then flip it to denied
                        // otherwise the user has changed other stuff that is messing this all up so leave it pending
                        string denailReason = null;
                        string denailReasonName = null;
                        if (workUsage.Determination == AdjudicationStatus.Denied && workUsage.MinutesUsed == amountToUse)
                        {
                            setStatusTo = workUsage.Determination;
                            denailReason = workUsage.DenialReasonCode;
                            denailReasonName = workUsage.DenialReasonName;
                        }

                        if (normalWorkDay.TotalMinutes == 0 && workUsage.MinutesInDay == 0 && workUsage.MinutesUsed == 0
                                && setStatusTo == AdjudicationStatus.Pending && userOverride.UserEntered
                                && totalApprovedMinutes>0 )
                        {
                            setStatusTo = AdjudicationStatus.Approved;
                        }
                        var theUsage1 = ap.Usage.AddFluid(new AppliedPolicyUsage()
                        {
                            MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : amountToUse,
                            MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                            Determination = setStatusTo,
                            DenialReasonCode = denailReason,
                            DenialReasonName = denailReasonName,
                            DateUsed = normalWorkDay.SampleDate,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode,
                            AvailableToUse = todaysUsage,
                            IntermittentDetermination = IntermittentStatus.Pending
                        });
                        if (userOverride != null)
                        {   
                            // We carry over the underlying denial reason/explanation if any, that's because
                            //  the intermittent one is stored in the user request itself, NOT on the usage
                            //  on the Usage we're storing the one provided by the user from the UI when denying the
                            //  actual policy itself (rather than a specific absence event).
                            theUsage1.DenialReasonCode = userOverride.DenialReasonCode;
                            theUsage1.DenialReasonName = userOverride.DenialReasonName;
                            theUsage1.DenialExplanation = userOverride.DenialExplanation;
                            theUsage1.IntermittentDetermination = userOverride.IntermittentDetermination;
                            theUsage1.UserEntered = true;
                        }

                        if (setStatusTo == AdjudicationStatus.Approved)
                            put.AddAllocated(normalWorkDay.SampleDate, amountToUse, (normalWorkDay.TotalMinutes ?? 0), policyReasonCode,
                                setStatusTo, null, theUsage1.IntermittentDetermination, todaysUsage, null, null, false, theCase.CaseNumber);
                        else if (setStatusTo == AdjudicationStatus.Denied)
                            deniedTotal.AddAllocated(normalWorkDay.SampleDate, amountToUse, (normalWorkDay.TotalMinutes ?? 0), policyReasonCode,
                                setStatusTo, denailReason, theUsage1.IntermittentDetermination, todaysUsage,null, null, false, theCase.CaseNumber);

                        if (entitlementUsage.DeniedCuzOverTheLimit > 0)
                        {
                            var theUsage2 = ap.Usage.AddFluid(new AppliedPolicyUsage()
                            {
                                MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : amountDenied + entitlementUsage.DeniedCuzOverTheLimit,
                                MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                                DenialReasonCode = AdjudicationDenialReason.Exhausted,
                                DenialReasonName = AdjudicationDenialReason.ExhaustedDescription,
                                Determination = AdjudicationStatus.Denied,
                                DateUsed = normalWorkDay.SampleDate,
                                PolicyReasonCode = ap.PolicyReason.ReasonCode,
                                AvailableToUse = todaysUsage,
                                IntermittentDetermination = IntermittentStatus.Pending
                            });
                            if (userOverride != null)
                            {
                                // We carry over the underlying denial reason/explanation if any, that's because
                                //  the intermittent one is stored in the user request itself, NOT on the usage
                                //  on the Usage we're storing the one provided by the user from the UI when denying the
                                //  actual policy itself (rather than a specific absence event).
                                theUsage2.DenialReasonCode = userOverride.DenialReasonCode;
                                theUsage2.DenialReasonName = userOverride.DenialReasonName;
                                theUsage2.DenialExplanation = userOverride.DenialExplanation;
                                theUsage2.IntermittentDetermination = userOverride.IntermittentDetermination;
                                theUsage2.UserEntered = true;
                            }
                            deniedTotal.AddAllocated(normalWorkDay.SampleDate, amountDenied + entitlementUsage.DeniedCuzOverTheLimit, 
                                (normalWorkDay.TotalMinutes ?? 0), policyReasonCode, AdjudicationStatus.Denied, 
                                AdjudicationDenialReason.Exhausted, theUsage2.IntermittentDetermination, todaysUsage, null, null, false, theCase.CaseNumber);
                        }
                    } // else if (normalWorkDay.TotalMinutes >= (totalRequested + minutesUsedForPolicyAndDay))

                    // multiple denied reasons need to be consolidated for a day, check today usage and see if any need to be rolled up
                    if (ap.Usage.Count(apc => apc.DateUsed == normalWorkDay.SampleDate && apc.Determination == AdjudicationStatus.Denied) > 1)
                    {
                        List<AppliedPolicyUsage> tmpUsage = ap.Usage.Where(apc => apc.DateUsed == normalWorkDay.SampleDate && apc.Determination == AdjudicationStatus.Denied).ToList();

                        // remove the dups from the list
                        ap.Usage = ap.Usage.Where(apc => apc.DateUsed != normalWorkDay.SampleDate && apc.Determination != AdjudicationStatus.Denied).ToList();

                        // and create a single usage for the date
                        var multiPack = ap.Usage.AddFluid(new AppliedPolicyUsage()
                        {
                            AvailableToUse = todaysUsage,
                            DateUsed = normalWorkDay.SampleDate,
                            PolicyReasonCode = ap.PolicyReason.ReasonCode,
                            MinutesInDay = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : (normalWorkDay.TotalMinutes ?? 0),
                            MinutesUsed = ap.PolicyReason.EntitlementType == EntitlementType.FixedWorkDays ? 1 : tmpUsage.Sum(apc => apc.MinutesUsed),
                            Determination = AdjudicationStatus.Denied,
                            DenialReasonCode = AdjudicationDenialReason.Exhausted,
                            DenialReasonName = AdjudicationDenialReason.ExhaustedDescription
                        });
                        if (userOverride != null)
                        {
                            // Since it's exhausted, we don't override the denial reason, but we do carry over
                            //  the intermittent determination.
                            multiPack.IntermittentDetermination = userOverride.IntermittentDetermination;
                            multiPack.UserEntered = true;
                        }
                    }

                    //restore total minutes if it was modified to allow extra time, to support FTE schedule
                    if(fteDates.Contains(normalWorkDay.SampleDate))
                    {
                        if (put.Allocated.FirstOrDefault(a => a.DateUsed == normalWorkDay.SampleDate) != null)
                        {
                            put.Allocated.FirstOrDefault(a => a.DateUsed == normalWorkDay.SampleDate).MinutesInDay = normalWorkDayMinutes ?? 0;
                        }
                        ap.Usage.FirstOrDefault(a => a.DateUsed == normalWorkDay.SampleDate).MinutesInDay = normalWorkDayMinutes ?? 0;
                        normalWorkDay.TotalMinutes = normalWorkDayMinutes;
                    }
                } // foreach(Time timeOut in theSegment.Absences.OrderBy(o => o.SampleDate))
            }
            if (deniedTotal.Allocated.Any())
            {

                //Check if we hit any Denied , if yes then set rest of segment as denied so any dependent policy 
                //can trigger, We will only look for Exhausted Policy 
                //But we can't set denied blindly as between Intermittent cases we can have other type cases also
                //will handle all those complexitiy downstream
                var deniedDate = deniedTotal.Allocated.LastOrDefault(d => d.Determination == AdjudicationStatus.Denied
                && d.DenialReasonCode == AdjudicationDenialReason.Exhausted);
                if (deniedDate != null)
                {
                    //We can have Partial Dennied , In case of partial denied we need to set the denied from next date 
                    var deniedStartDate = deniedDate.DateUsed;
                    if (deniedDate.MinutesInDay > deniedDate.MinutesUsed)
                    {
                        deniedStartDate = deniedDate.DateUsed.AddDays(1);
                    }
                    //This needs to go back to Drawing board and Product decession needs to be made, as current changes creates lot of other issue

                    //AdjustIntermittentDeniedDates(theCase, ap.Policy.Code, deniedStartDate, theSegment.EndDate.Value,
                    //    AdjudicationStatus.Denied, AdjudicationDenialReason.Exhausted);
                }
            }
            if (fteDates.Any())
            {
                AdjustFteUsage(fteDates, ap, los);
            }
        } // private void ApplyIntermittentPolicy(CaseSegment theSegment, Case theCase, SegmentUsage userOverrides, AppliedPolicy ap)

        private int GetOverLapTime(List<IntermittentTimeRequest> iTorDetail)
        {
            int overlapTime = 0;
            DateTime? dateToCompareOverlap = null;
            iTorDetail.Where(t => t.StartTimeForLeave != null && t.EndTimeForLeave != null).OrderBy(t => DateTime.Parse(t.StartTimeForLeave.ToString())).ForEach(u =>
            {
                if (dateToCompareOverlap != null && DateTime.Parse(u.StartTimeForLeave.ToString()) < dateToCompareOverlap.Value)
                {
                    overlapTime += (int)(dateToCompareOverlap.Value - DateTime.Parse(u.StartTimeForLeave.ToString())).TotalMinutes;
                }
                dateToCompareOverlap = DateTime.Parse(u.EndTimeForLeave.ToString());

            });
            return overlapTime;
        }

        /// <summary>
        /// For Intermittent leaves based on variable schedule employees will use/lose time as requested. % of a work week is recorded as time lost for that week 
        /// (intermittent approved time for a policy) over (divided by) average hours per week (FTE %) or 1 (whichever is less).
        /// </summary>
        /// <param name="fteDates">Dates on which TOR is more then scheduled time</param>
        /// <param name="appliedPolicy">Policy whose usage need to be updated</param>
        /// <param name="put">Policy Usage Total</param>
        /// <param name="los">LeaveOfAbsence</param>
        private void AdjustFteUsage(List<DateTime> fteDates, AppliedPolicy appliedPolicy, LeaveOfAbsence los)
        {
            DateTime lastDayOfWeek = fteDates[0].AddDays(-1);
            double fteTimeForWeek = 0;
            double fteTimePerDay = 0;

            foreach (DateTime dateToProcess in fteDates)
            {
                if (dateToProcess <= lastDayOfWeek)
                {
                    continue;
                }

                lastDayOfWeek = dateToProcess.GetLastDayOfWeek();
                fteTimeForWeek = los.TimeLimitForTheDay(dateToProcess);

                if (fteTimeForWeek < 1)
                {
                    continue;
                }

                List<AppliedPolicyUsage> allocated = appliedPolicy.Usage.Where(p => dateToProcess.AddDays(-(int)dateToProcess.DayOfWeek) == p.DateUsed.AddDays(-(int)p.DateUsed.DayOfWeek)).OrderBy(a => a.DateUsed).ToList();

                fteTimePerDay = (double)(los.TimeScheduledPerDay(dateToProcess, los.Employee.StartDayOfWeek ?? DayOfWeek.Monday, appliedPolicy.PolicyBehaviorType) ?? 0);

                double workDays = los.TotalWorkDaysInWeek(dateToProcess, los.Employee.StartDayOfWeek ?? DayOfWeek.Monday, appliedPolicy.PolicyBehaviorType);

                allocated.Where(a => a.MinutesUsed > 0 && a.MinutesInDay <= 0)?.ForEach(f => f.MinutesInDay = fteTimePerDay);

                var timeused = allocated.Where(a => a.Determination == AdjudicationStatus.Approved)
                                .Sum(a => Convert.ToDecimal(a.DaysUsage(appliedPolicy.PolicyReason.EntitlementType.Value)));

                if (allocated.Sum(a => a.MinutesUsed) > fteTimeForWeek || timeused > 1)
                {
                    double minutesUsedInWeek = 0;
                    foreach (AppliedPolicyUsage allocation in allocated)
                    {
                        timeused = allocated.Where(a => a.Determination == AdjudicationStatus.Approved && a.DateUsed <= allocation.DateUsed)
                                .Sum(a => Convert.ToDecimal(a.DaysUsage(appliedPolicy.PolicyReason.EntitlementType.Value)));
                        if ((minutesUsedInWeek + allocation.MinutesUsed) > fteTimeForWeek || timeused > 1)
                        {
                            timeused = 1 - allocated.Where(a => a.Determination == AdjudicationStatus.Approved && a.DateUsed < allocation.DateUsed)
                                .Sum(a => Convert.ToDecimal(a.DaysUsage(appliedPolicy.PolicyReason.EntitlementType.Value)));

                            var minutesInDay = minutesUsedInWeek >= fteTimeForWeek ? 0 : (allocation.MinutesUsed / ((double)timeused * workDays));
                            appliedPolicy.Usage.FirstOrDefault(a => a.DateUsed == allocation.DateUsed).MinutesInDay = minutesInDay;
                        }
                        minutesUsedInWeek += allocation.MinutesUsed;
                    }
                }
            }
        }

        /// <summary>
        /// Saves a intermittent note
        /// </summary>
        /// <param name="note"></param>
        public CaseNote IntermittentAbsenceEntryNote(Case c, IntermittentTimeRequest r, User CurrentUser)
        {
            var noteQuery =
                                from n in CaseNote.AsQueryable()
                                where Query.EQ("RequestDate", new BsonDateTime(r.RequestDate)).Inject()
                                select n;

            CaseNote note = noteQuery.FirstOrDefault() ?? new CaseNote()
            {
                Category = NoteCategoryEnum.TimeOffRequest,
                CreatedBy = CurrentUser,
                CustomerId = c.CustomerId,
                EmployerId = c.EmployerId,
                Public = false,
                Case = c
            };
            note.ModifiedBy = CurrentUser;
            note.Notes = string.Format("[{0:MM/dd/yyyy}] Intermittent Absence Entry Note: {1}", r.RequestDate, r.Notes);
            note.Metadata.SetRawValue("RequestId", r.Id.ToString());
            note.Metadata.SetRawValue("RequestDate", r.RequestDate);
            note.Save();
            return note;
        }
    } // public partial class CaseService : LogicService
}
