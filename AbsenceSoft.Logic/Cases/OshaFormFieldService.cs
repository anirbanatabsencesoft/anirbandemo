﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public class OshaFormFieldService : LogicService
    {
        /// <summary>
        /// Represents the default code for Injured Body Part.
        /// </summary>
        public const string InjuredBodyPart = "POB";

        /// <summary>
        /// Represents the default code for Cause of Injury.
        /// </summary>
        public const string WhatHarmedTheEmployee = "CIJ";

        /// <summary>
        /// create instance of service
        /// </summary>
        public OshaFormFieldService()
        {

        }

        /// <summary>
        /// create instance of service and initialize with current user
        /// </summary> 
        /// <param name="currentUser"></param>
        public OshaFormFieldService(User currentUser)
            : base(currentUser)
        {

        }

        /// <summary>
        /// create instance of service and initialize with current user, current employer and current customer
        /// </summary>
        /// <param name="currentCustomer"></param>
        /// <param name="currentEmployer"></param>
        /// <param name="currentUser"></param>
        public OshaFormFieldService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Get Osha Form Field list
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetOshaFormFieldList(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults result = new ListResults(criteria);
            string name = criteria.Get<string>("Name");
            string code = criteria.Get<string>("Code");

            List<IMongoQuery> ands = OshaFormField.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            OshaFormField.Query.MatchesString(at => at.Name, name, ands);
            OshaFormField.Query.MatchesString(at => at.Code, code, ands);
            List<OshaFormField> types = OshaFormField.DistinctAggregation(ands);
            result.Total = types.Count;
            types = types
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = types.Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("CustomerId", c.CustomerId)
                .Set("EmployerId", c.EmployerId)
                .Set("Name", c.Name)
                .Set("Code", c.Code)
                .Set("Category", c.Category)
                .Set("Disabled", false)
                .Set("ModifiedDate", c.ModifiedDate)
            );

            return result;
        }

        /// <summary>
        /// Get Osha Form Field by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OshaFormField GetOshaFormFieldById(string id)
        {
            OshaFormField osha = OshaFormField.GetById(id);
            if (osha != null && osha.Options == null)
            {
                osha.Options = new List<string>();
            }
            return osha;
        }

        /// <summary>
        /// Get Osha Form Field by Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public OshaFormField GetOshaFormFieldByCode(string code)
        {
            OshaFormField osha = OshaFormField.GetByCode(code, CustomerId, EmployerId, true);
            if (osha != null && osha.Options == null)
            {
                osha.Options = new List<string>();
            }
            return osha;
        }

        /// <summary>
        /// Save Osha Form Field
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public OshaFormField SaveOshaFormField(OshaFormField type)
        {
            using (new InstrumentationContext("OshaFormFieldService.SaveOshaFormField"))
            {
                OshaFormField savedType = OshaFormField.GetById(type.Id);
                if (savedType != null && (savedType.CustomerId != CustomerId || savedType.EmployerId != EmployerId))
                {
                    type.Clean();
                }

                type.CustomerId = CustomerId;
                type.EmployerId = EmployerId;

                return type.Save();
            }
        }

    }//end: OshaFormFieldService
}//end: namespace
