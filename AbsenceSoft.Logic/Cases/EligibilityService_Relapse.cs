﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public partial class EligibilityService : LogicService
    {
        /// <summary>
        /// Runs policy selection if necessary as well as runs eligibility against
        /// all non-manual policies on the case and returns the Leave Of Absence object
        /// fully populated. This method does NOT persist the case.
        /// </summary>
        /// <param name="myCase">The case that should have policy selection and eligibility ran for it.</param>
        /// <param name="relapse">The Relapse Case</param>
        /// <param name="isRelapseDirty">Tells if relapse model in dirty from UI</param>
        /// <param name="isReRun">Tells if this method is called multiple times from UI</param>
        /// <returns>LeaveOfAbsence</returns>
        public LeaveOfAbsence RunEligibility(Case myCase, Relapse relapse, bool isRelapseDirty = false, bool isReRun = false)
        {
            using (new InstrumentationContext("EligibilityService.RunEligibility"))
            {
                string METATAGNAME = "UnSaved";
                if (myCase == null)
                {
                    throw new ArgumentNullException("myCase");
                }
                CaseSegment newSegment = null;

                if (relapse != null && (!isReRun || isRelapseDirty))
                {
                    newSegment = new CaseSegment
                    {
                        StartDate = relapse.StartDate,
                        Type = relapse.CaseType,
                        EndDate = relapse.EndDate,
                        Status = CaseStatus.Open
                    };
                    newSegment.Metadata.Add(new MongoDB.Bson.BsonElement(METATAGNAME, true));

                    if (relapse.LeaveSchedule != null && relapse.CaseType == CaseType.Reduced)
                    {
                        DateTime date = relapse.LeaveSchedule.StartDate;
                        date = date.GetFirstDayOfWeek();
                        relapse.LeaveSchedule.Times.ForEach(x => { x.SampleDate = date; date = date.AddDays(1); });
                        newSegment.LeaveSchedule = new List<Schedule> { relapse.LeaveSchedule };
                    }
                    if (isRelapseDirty)
                    {
                        myCase.EndDate = relapse.EndDate;
                        CaseSegment oldCaseSegment = myCase.Segments.OrderByDescending(c => c.StartDate).FirstOrDefault();
                        if (bool.TryParse(oldCaseSegment.Metadata.ValueByName(METATAGNAME).ToString(), out bool isUnsaved))
                        {
                            myCase.Segments.Remove(oldCaseSegment);
                        }
                    }
                }
                LeaveOfAbsence loa = GetLeaveOfAbsenceForRelapse(myCase, relapse);
                PerformPolicySelection(myCase, loa, newSegment);
                PerformEligibilityForRelapse(loa);
                EvaluateTimePayAndCrosswalkRules(loa);

                if (!isReRun || isRelapseDirty)
                {
                    loa.Case.Segments.Add(loa.ActiveSegment);
                }
                // If any policy comes out to be ineligibe, then, look for substitute policy that can be used in place of ineligible policy.
                if (SelectSubstitutePolicy(loa))
                {
                    RunEligibility(myCase, relapse, isReRun, true);
                }
                if (loa.Employee.WorkSchedules != null && loa.Employee.WorkSchedules.Count > 0)
                {
                    loa.Case = new CaseService().Using(c => c.RunCalcs(loa.Case));
                }
                return loa;
            }
        }

        /// <summary>
        /// Performs the policy selection process by evaluating policy selection rules for each policy that has them
        /// and applying them to the absence when applicable (passes selection criteria, if any).
        /// determine if a relapse window is set and the time-frame for the new segment 
        /// start date falls within that relapse period calculated from the 
        /// prior Return to Work date on the case 
        /// </summary>
        /// <param name="myCase">The Case</param>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="newCaseSegment">New case segment</param>
        protected void PerformPolicySelection(Case myCase, LeaveOfAbsence loa, CaseSegment newCaseSegment)
        {
            // If there are already policies applied, jump out, we can't run this again.
            if (newCaseSegment == null)
            {
                return;
            }
            foreach (var aPolicy in loa.ActiveSegment.AppliedPolicies)
            {
                if (aPolicy.PolicyReason != null && (aPolicy.PolicyReason.AllowRelapse ?? true))
                {
                    if (aPolicy.PolicyReason.RelapseAllowedTime.HasValue && aPolicy.PolicyReason.RelapseAllowedTimeUnit.HasValue)
                    {
                        DateTime? startRange = myCase.GetCaseEventDate(CaseEventType.ReturnToWork) ?? loa.ActiveSegment.EndDate;
                        if (!startRange.HasValue)
                        {
                            continue;
                        }
                        double duration = aPolicy.PolicyReason.RelapseAllowedTimeUnit.Value.ConvertUnits(Unit.Minutes, aPolicy.PolicyReason.RelapseAllowedTime.Value);
                        DateTime? endRange = startRange.HasValue ? startRange.Value.AddMinutes(duration) : new DateTime?();

                        if (newCaseSegment != null && !newCaseSegment.StartDate.DateInRange(startRange.Value, endRange))
                        {
                            continue;
                        }
                    }
                    //this can be a first check for every exiting policy
                    Policy policy = Policy.GetById(aPolicy.Policy.Id);
                    AppliedPolicy aPolicyClone = aPolicy.Clone();
                    aPolicyClone.StartDate = newCaseSegment.StartDate;
                    aPolicyClone.EndDate = newCaseSegment.EndDate;                  
                    aPolicyClone.Usage.Clear();
                    if (policy != null && policy.AbsenceReasons.Any(r => r.ReasonId == myCase.Reason.Id && r.RelapseUseLatestPolicyDefinition == true))
                    {
                        aPolicyClone.Policy = policy;                       
                        aPolicyClone.PolicyReason = policy.AbsenceReasons.FirstOrDefault(p => p.ReasonId == aPolicy.PolicyReason.ReasonId);
                        aPolicyClone.ReevaluateEligibility = true;
                    }

                    //check the new case type if allowed for that particular policy 
                    if (policy != null && policy.AbsenceReasons.Any(p => p.ReasonId == myCase.Reason.Id && p.CaseTypes.HasFlag(newCaseSegment.Type)))
                    {
                        PolicyEvent policyEvent = aPolicyClone.PolicyReason.PolicyEvents.FirstOrDefault(pe => pe.EventType == CaseEventType.BondingStartDate && pe.DateType == EventDateType.EndPolicyBasedOnEventDate);
                        CaseEvent ce = myCase.FindCaseEvent((policyEvent != null) ? policyEvent.EventType : CaseEventType.BondingStartDate);
                        if (!(loa.WillUseBonding && policyEvent != null && aPolicyClone.StartDate > ce.EventDate))
                        {
                            newCaseSegment.AppliedPolicies.Add(aPolicyClone);
                        }
                    }
                }
            }

            List<Policy> policies = PerformEmployeePolicySelection(loa.Employee, newCaseSegment.StartDate, loa.Case.Reason.Id)
                .Where(p => !newCaseSegment.AppliedPolicies.Any(a => a.Policy.Code == p.Code) && !(loa.Employer.SuppressPolicies ?? new List<string>(0)).Contains(p.Code)).ToList();
            foreach (var policy in policies.Where(p => p.AbsenceReasons.Any(r => (r.AllowRelapse ?? true) && r.RelapseAllowSelection == true && r.ReasonId == loa.Case.Reason.Id && r.CaseTypes.HasFlag(loa.Case.Segments.First().Type))))
            {
                List<PolicyRuleGroup> ruleGroups = GetRuleGroups(policy, loa);
                List<AppliedRuleGroup> appliedGroups = GetAppliedRuleGroups(ruleGroups);
                EvaluateRuleGroups(loa, appliedGroups, PolicyRuleGroupType.Selection);
                if (appliedGroups.Where(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Selection).All(g => g.Pass))
                {
                    // add the policy to the applied policy group to get ready for eligibility
                    AppliedPolicy aPolicy = new AppliedPolicy();
                    aPolicy.StartDate = newCaseSegment.StartDate;
                    aPolicy.EndDate = newCaseSegment.EndDate;
                    aPolicy.Policy = policy;
                    aPolicy.PolicyReason = policy.AbsenceReasons.First(a => a.ReasonId == loa.Case.Reason.Id);
                    aPolicy.Status = EligibilityStatus.Pending;
                    // Ensure our selection rules don't get copied over, that would be bad; they shouldn't show in the UI.
                    aPolicy.RuleGroups = appliedGroups.ToList(r => r.RuleGroup.RuleGroupType != PolicyRuleGroupType.Selection);
                    // Save our selection rules so we have proof of why we selected 'n' stuff
                    aPolicy.Selection = appliedGroups.ToList(r => r.RuleGroup.RuleGroupType == PolicyRuleGroupType.Selection);
                    foreach (var r in aPolicy.Policy.AbsenceReasons)
                    {
                        if (!r.PeriodType.HasValue)
                        {
                            r.PeriodType = loa.Employer.FMLPeriodType;
                        }
                    }
                    aPolicy.ReevaluateEligibility = true;
                    newCaseSegment.AppliedPolicies.Add(aPolicy);
                }
            }
            loa.ActiveSegment = newCaseSegment;
            // Need to make adjustment to these newly minted policies and ensure time/pay are correct.
            EvaluateTimePayAndCrosswalkRules(loa);
        }

        /// <summary>
        /// Gets a leave of absence object that contains all of the data necessary for evaluating rules, sending communications, etc.
        /// and provides the basis for tokenization and expression evaluation in the rules engine :-).
        /// </summary>
        /// <param name="caseId">The case Id for the leave of absence case that is the primary case we're evaluating.</param>
        /// <param name="relapse">Relpase case</param>
        /// <returns></returns>
        public LeaveOfAbsence GetLeaveOfAbsenceForRelapse(Case myCase, Relapse relapse)
        {
            using (new InstrumentationContext("EligibilityService.GetLeaveOfAbsence"))
            {
                if (myCase == null)
                {
                    return null;
                }
                LeaveOfAbsence leave = new LeaveOfAbsence() { Case = myCase };
                leave.ActiveSegment = myCase.Segments.OrderByDescending(c => c.StartDate).FirstOrDefault();
                leave.CaseHistory = Case.AsQueryable().Where(c => c.Id != myCase.Id && c.Employee.Id == myCase.Employee.Id).ToList();
                leave.Employee = relapse != null ? relapse.Employee : myCase.Employee;
                leave.Employer = leave.Employee.Employer;
                leave.Customer = leave.Employer.Customer;
                leave.Communications = Communication.AsQueryable().Where(c => c.CaseId == myCase.Id).ToList();
                leave.Tasks = Data.ToDo.ToDoItem.AsQueryable().Where(t => t.CaseId == myCase.Id).ToList();
                leave.WorkSchedule = Employee.GetById(myCase.Employee.Id).WorkSchedules;
                leave.Relapse = relapse;
                return leave;
            }
        }

        /// <summary>
        /// Performs the primary eligibility process against a leave of absence, evaluating each policy in the
        /// case and applies the eligibility status based on the rule group evaluations.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        protected void PerformEligibilityForRelapse(LeaveOfAbsence loa)
        {
            using (new InstrumentationContext("EligibilityService.PerformEligibility"))
            {
                // Enumerate our applied policies for eligibility (already placed on the active segment)
                foreach (var policy in loa.ActiveSegment.AppliedPolicies.Where(p => !p.ManuallyAdded && (p.PolicyReason.RelapseReevaluateEligibility == true || p.ReevaluateEligibility == true))) // RelapseReevaluateEligibility is true
                {
                    EvaluateRuleGroups(loa, policy.RuleGroups, PolicyRuleGroupType.Eligibility);
                    if (policy.RuleGroups.Where(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Eligibility).All(g => g.Pass))
                    {
                        policy.Status = EligibilityStatus.Eligible;
                    }
                    else
                    {
                        policy.Status = EligibilityStatus.Ineligible;
                    }
                }
            }
        }
    }
}
