﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Notification;
using AbsenceSoft.Logic.Rules;
using AbsenceSoft.Rendering.Templating.MailMerge;
using AT.Common.Core;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ADocument = Aspose.Words.Document;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Common;

namespace AbsenceSoft.Logic.Cases
{
    public class CommunicationService : LogicService, ICommunicationService
    {
        public bool SuppressWorkflow { get; set; }
        public CommunicationService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {
            strCommTopHTML.Append("<!DOCTYPE html>");
            strCommTopHTML.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            strCommTopHTML.Append("<head>");
            strCommTopHTML.Append("<style>");
            strCommTopHTML.Append("body, p, table, tr, td, th { font-family: 'Times New Roman' !important; font-size: 12px !important; }   h1, h2, h3, h4, h5 { font-family: 'Times New Roman' !important; } ");
            strCommTopHTML.Append("</style></head><body>");

            strCommBottomHTML.Append("</body></html>");

            // See: http://handlebarsjs.com/ for template syntax
            /* Additional tags:
             *  - {{#join collection}}      Outputs a collection as a comma delimited list where collection is an IEnumerable or Array
             *  - {{#eq value1 value2}}     Conditionally adds a block of text if value1 = value2
             *  - {{#lt value1 value2}}     Conditionally adds a block of text if value 1 is < value2
             *  - {{#lte value1 value2}}    Conditionally adds a block of text if value 1 is <= value2
             *  - {{#gt value1 value2}}     Conditionally adds a block of text if value 1 is > value2
             *  - {{#gte value1 value2}}    Conditionally adds a block of text if value 1 is >= value2
             *  
             * HTML support
             *  - {{#pagebreak}}            Inserts an HTML page break that tells the PDF engine to break the page after that point
             *  
             * Plain text support:
             *  - {{#newline}}              Adds a literal newline character, otherwise newlines are ignored in output (generally for plain text)
             *  - {{#formfeed}}             Inserts a Form Feed control character for use in plain text documents and raw TWAIN output
             */
        }

        public static StringBuilder strCommTopHTML = new StringBuilder();
        public static StringBuilder strCommBottomHTML = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the communication service.
        /// </summary>
        /// <remarks>
        /// <para>See: http://handlebarsjs.com/ for template syntax</para>
        /// <list type="bullet">
        /// <item>Additional tags:
        ///     <list type="bullet">
        ///         <item>{{#join collection}} - Outputs a collection as a comma delimited list where collection is an IEnumerable or Array</item>
        ///         <item>{{#eq value1 value2}} - Conditionally adds a block of text if value1 = value2</item>
        ///         <item>{{#lt value1 value2}} - Conditionally adds a block of text if value 1 is < value2</item>
        ///         <item>{{#lte value1 value2}} - Conditionally adds a block of text if value 1 is <= value2</item>
        ///         <item>{{#gt value1 value2}} - Conditionally adds a block of text if value 1 is > value2</item>
        ///         <item>{{#gte value1 value2}} - Conditionally adds a block of text if value 1 is >= value2</item>
        ///     </list>
        /// </item>
        /// <item>HTML support:
        ///     <list type="bullet">
        ///         <item>{{#pagebreak}} - Inserts an HTML page break that tells the PDF engine to break the page after that point</item>
        ///     </list>
        /// </item>
        /// <item>Plain text support:
        ///     <list type="bullet">
        ///         <item>{{#newline}} - Adds a literal newline character, otherwise newlines are ignored in output (generally for plain text)</item>
        ///         <item>{{#formfeed}} - Inserts a Form Feed control character for use in plain text documents and raw TWAIN output</item>
        ///     </list>
        /// </item>
        /// </list>
        /// </remarks>
        public CommunicationService()
            : this(null, null, null)
        {

        }

        public Communication GetCommunication(string id)
        {
            return Communication.GetById(id);
        }

        /// <summary>
        /// Gets a list of communications for display in the UI using
        /// the standard list criteria.
        /// </summary>
        /// <param name="criteria">The list criteria that must at least containt caseId.</param>
        /// <returns>A list results with a list of communication style objects.</returns>
        public ListResults CommunicationList(ListCriteria criteria)
        {
            using (new InstrumentationContext("CommunicationService.CommunicationList"))
            {
                if (criteria == null)
                {
                    criteria = new ListCriteria();
                }
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("CaseId");
                if (string.IsNullOrWhiteSpace(caseId))
                {
                    throw new AbsenceSoftException("caseId is a required list criteria for getting communications");
                }

                /// Criteria for filter
                string employerId = criteria.Get<string>("EmployerId");                
                long? modifiedDate = criteria.Get<long?>("ModifiedDate");

                List<IMongoQuery> ands = new List<IMongoQuery>
                {
                    CurrentUser.BuildDataAccessFilters(),
                    Communication.Query.EQ(c => c.CaseId, caseId),
                    BuildDraftQuery(criteria)
                };
                
                if (!string.IsNullOrEmpty(employerId))
                {
                    ands.Add(Communication.Query.EQ(c => c.EmployerId, employerId));
                }

                // Add ESS filter
                // Fetch user permission
                List<string> userPermissions;
                if (CurrentEmployer != null)
                {
                    userPermissions = User.Permissions.EmployerPermissions(CurrentEmployer.Id).ToList();
                }
                else
                {
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();
                }

                // if a user hasn't a permission to "ViewPublicCommunication" then show user's own communication otherwise all
                if (!userPermissions.Any(m => m == Permission.ViewConfidentialCommunications))
                {
                    ands.Add(Communication.Query.Or(Communication.Query.EQ(c => c.Public, true), Query.EQ("Public", BsonNull.Value), Communication.Query.EQ(e => e.CreatedById, CurrentUser.Id)));
                }                

                if (modifiedDate.HasValue)
                {
                    ands.Add(Communication.Query.GTE(e => e.ModifiedDate, Convert.ToDateTime(new BsonDateTime(modifiedDate.Value))));
                    // Window is 24 hours
                    ands.Add(Communication.Query.LT(e => e.ModifiedDate, Convert.ToDateTime(new BsonDateTime(modifiedDate.Value + 86400000))));
                }

                var query = Communication.Query.Find(ands.Count > 0 ? Communication.Query.And(ands) : null);
                query = criteria.SetSortAndSkip(query);

                result.Total = query.Count();
                result.Results = query.Select(c => new ListResult()
                    .Set("Id", c.Id)
                    .Set("Template", c.Template)
                    .Set("ToDoItemId", c.ToDoItemId)
                    .Set("CaseId", c.CaseId)
                    .Set("IsDraft", c.IsDraft)
                    .Set("Name", c.Name)
                    .Set("Subject", c.Subject)
                    .Set("RecipientsJSON", c.Recipients)
                    .Set("CommunicationType", c.CommunicationType.ToString().SplitCamelCaseString())
                    .Set("SentDate", c.SentDate)
                    .Set("SentDateText", string.Format("{0:MM/dd/yyyy}", c.SentDate))
                    .Set("CreatedByName", c.CreatedByName)
                    .Set("ModifiedDate", c.ModifiedDate)
                );

                // apply filter and sort
                result.Results = ApplyCommunicationFilters(criteria, result.Results);                

                return result;
            }
        }

        /// <summary>
        /// Builds query to check if the communication is a draft.  A communication is considered not a draft if the boolean value is false, or it doesn't have that value.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private IMongoQuery BuildDraftQuery(ListCriteria criteria)
        {
            bool isDraft = criteria.Get<bool>("IsDraft");
            IMongoQuery draftQuery = Communication.Query.EQ(c => c.IsDraft, isDraft);
            if (isDraft)
            {
                return draftQuery;
            }

            List<IMongoQuery> ors = new List<IMongoQuery>
            {
                draftQuery,
                Communication.Query.NotExists(c => c.IsDraft)
            };
            return Communication.Query.Or(ors);
        }

        public List<Communication> ExportCommunication(string employerId)
        {
            return Communication.AsQueryable().Where(m => m.EmployerId == employerId).ToList();
        }

        /// <summary>
        /// Creates a communication (not yet persisted) and populates it with all of the necessary goodies
        /// for use in the UI or by the todo engine in preparation for editing and then sending.
        /// </summary>
        /// <param name="caseId">The id of the case to create the communication for</param>
        /// <param name="templateKey">Either the template key or NULL</param>
        /// <returns></returns>
        public Communication CreateCommunication(string caseId, string templateKey = null, CommunicationType? type = null)
        {
            string errorMsg = null;
            if (string.IsNullOrWhiteSpace(caseId))
            {
                throw new ArgumentNullException("caseId");
            }
            

            using (new InstrumentationContext("CommunicationService.CreateCommunication.Manual"))
            {
                Case myCase = Case.GetById(caseId);
                if (myCase == null)
                {
                    errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - Case was not found:" , caseId);
                    Log.Error(errorMsg);
                    throw new AbsenceSoftException(errorMsg);
                }

                if (!string.IsNullOrWhiteSpace(templateKey) && CurrentUser != null && !CurrentUser.HasEmployerAccess(myCase.EmployerId, Permission.Communications(templateKey).ToArray()))
                {
                    errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - User does not have access to create a communication for this employer of this type");
                    Log.Error(errorMsg);
                    throw new AbsenceSoftException(errorMsg);
                }
                else if (CurrentUser != null && !CurrentUser.HasEmployerAccess(myCase.EmployerId, Permission.SendCommunication))
                {
                    errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - User does not have access to create a communication for this employer");
                    Log.Error(errorMsg);
                    throw new AbsenceSoftException(errorMsg);
                }

                Communication comm = new Communication()
                {
                    CustomerId = myCase.CustomerId,
                    CaseId = myCase.Id,
                    EmployeeId = myCase.Employee.Id,
                    EmployeeNumber = myCase.Employee.EmployeeNumber,
                    EmployerId = myCase.EmployerId,
                    CommunicationType = type ?? CommunicationType.Mail,
                    Template = templateKey,
                    CreatedById = CurrentUser.Id,
                    ModifiedById = CurrentUser.Id,
                    CreatedByName = CurrentUser.DisplayName,
                    CreatedByEmail = CurrentUser.Email
                };

                TemplateCommunication(comm);

                return comm;
            }
        }

        /// <summary>
        /// Creates a communication (not yet persisted) and populates it with all of the necessary goodies
        /// for use in the UI or by the todo engine in preparation for editing and then sending.
        /// </summary>
        /// <param name="wf">The todo item to use for buildilng the communication. It will contain metadata
        /// useful in determining which type of communication, template, etc.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">Creating communication for todo items other than communication types is not supported</exception>
        public Communication CreateCommunication(ToDoItem wf)
        {
            using (new InstrumentationContext("CommunicationService.CreateCommunication"))
            {
                if (wf.ItemType != ToDoItemType.Communication)
                {
                    throw new AbsenceSoftException("Creating communication for todo items other than communication types is not supported");
                }

                Case theCase = wf.Case;
                List<Contact> ccContacts = new List<Contact>();
                List<Contact> toContacts = new List<Contact>();
                try
                {
                    if (theCase != null && theCase.Employee != null && wf.WorkflowInstance != null && wf.WorkflowInstance.Workflow != null && wf.WorkflowInstance.Workflow.Activities != null)
                    {
                        List<string> toContactTypeCodes = GetContactCodesFromMetadata("To", wf.WorkflowInstance.Workflow.Activities.FirstOrDefault(o => o.ActivityId == "SendCommunicationActivity"));
                        List<string> ccContactTypeCodes = GetContactCodesFromMetadata("CC", wf.WorkflowInstance.Workflow.Activities.FirstOrDefault(o => o.ActivityId == "SendCommunicationActivity"));
                        string employeeId = theCase.Employee.Id;

                        toContacts = toContactTypeCodes != null ? GetContactsByCodes(employeeId, theCase, toContactTypeCodes) : new List<Contact>();
                        ccContacts = ccContactTypeCodes != null ? GetContactsByCodes(employeeId, theCase, ccContactTypeCodes) : new List<Contact>();
                    }
                }
                catch
                {//the exception can be ignored as not all todos will have pre defined TO and CC contacts or user can manually create the todos or workflows
                }

                Communication comm = new Communication()
                {
                    Recipients = toContacts,
                    CCRecipients = ccContacts,
                    CaseId = wf.CaseId,
                    EmployeeId = wf.EmployeeId,
                    EmployeeNumber = wf.EmployeeNumber,
                    EmployerId = wf.EmployerId,
                    CustomerId = wf.CustomerId,
                    Metadata = wf.Metadata,
                    Template = wf.Metadata.GetRawValue<string>("Template"),
                    CreatedById = CurrentUser.Id,
                    ModifiedById = CurrentUser.Id,
                    CreatedByName = CurrentUser.DisplayName,
                    CreatedByEmail = CurrentUser.Email
                };
                int? comType = wf.Metadata.GetRawValue<int?>("CommunicationType");
                if (comType.HasValue)
                {
                    comm.CommunicationType = (CommunicationType)comType.Value;
                }

                TemplateCommunication(comm, wf);
                return comm;
            }
        }//CreateCommunication

        /// <summary>
        /// Retrieves a comma separated list of values and turns them into a string array
        /// </summary>
        /// <param name="metadataKey"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        private List<string> GetContactCodesFromMetadata(string metadataKey, WorkflowActivity activity)
        {
            if (activity != null)
            {
                string csv = activity.Metadata.GetRawValue<string>(metadataKey);
                if (csv != null)
                {
                    string[] separator = { "," };
                    return csv.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the contacts by codes.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="contactTypeCodes">The contact type codes.</param>
        /// <returns></returns>
        private List<Contact> GetContactsByCodes(string employeeId, Case theCase, List<string> contactTypeCodes)
        {
            List<string> emails = contactTypeCodes.Where(s => s.Contains("@")).ToList();
            List<Contact> contacts = EmployeeContact.AsQueryable()
                .Where(e => e.EmployeeId == employeeId && contactTypeCodes.Contains(e.ContactTypeCode)).ToList()
                .Select(e => e.Contact).ToList();

            if (contactTypeCodes.Contains("SELF", StringComparer.OrdinalIgnoreCase))
            {
                string employeesEmail = theCase.Employee.Info.Email;

                if (!string.IsNullOrEmpty(theCase.Employee.Info.AltEmail))
                {
                    employeesEmail = theCase.Employee.Info.AltEmail;
                }

                contacts.Add(new Contact()
                {
                    Email = employeesEmail
                });
            }

            if (theCase.AuthorizedSubmitter != null && contactTypeCodes.Contains(theCase.AuthorizedSubmitter.ContactTypeCode))
            {
                contacts.Add(theCase.AuthorizedSubmitter.Contact);
            }

            foreach (string email in emails)
            {
                contacts.Add(new Contact()
                {
                    Email = email
                });
            }

            return contacts;
        }
        private void TemplateCommunication(Communication comm, ToDoItem todo = null)
        {
            if (string.IsNullOrEmpty(comm.Template))
            {
                return;
            }

            Template template = Template.GetByCode(comm.Template, comm.CustomerId, comm.EmployerId);
            if (template == null)
            {
                return;
            }

            TemplateRenderModel model = BuildTemplateRenderModel(comm, todo);
            comm.Name = template.Name;
            comm.Paperwork = GetCommunicationPaperwork(model, template, todo);
            AttachLatestPaperwork(template, model, comm);
            if (template.Document != null && template.DocType == DocumentType.MSWordDocument)
            {
                comm.Body = RenderTemplateFromWordDoc(template, comm.Paperwork, model);
            }
            else if (!string.IsNullOrWhiteSpace(template.Body))
            {
                comm.Body = AbsenceSoft.Rendering.Template.RenderTemplate(template.Body, model);
            }

            //User Can enter anything in Subject line from the Catalog and our TemplateRenderModel doesn't have 
            //all the data available from catalog , so instead set MailMergeDataObject which holds all data from
            //Catalog

            if (!string.IsNullOrWhiteSpace(template.Subject))
            {
                comm.Subject = AbsenceSoft.Rendering.Template.RenderTemplate(template.Subject, model.MailMergeData);
            }
        }

        private TemplateRenderModel BuildTemplateRenderModel(Communication comm, ToDoItem todo = null)
        {
            TemplateRenderModel model = new TemplateRenderModel();
            var employee = Employee.GetById(comm.EmployeeId);
            if (employee != null)
            {
                model.WorkSchedule = employee.WorkSchedules;
                model.AdminContacts = GetAdminContacts(employee.Id);
            }

            model.Case = comm.Case;
            model.EmailRepliesType = comm.EmailRepliesType;
            if (model.Case != null)
            {
                if (model.Case.Contact != null)
                {
                    var contactType = ContactType.GetByCode(model.Case.Contact?.ContactTypeCode, comm.CustomerId, comm.EmployerId);
                    if (contactType != null && !string.IsNullOrWhiteSpace(contactType.DisplayOnCommunicationsAs))
                    {
                        model.Case.Contact.ContactTypeName = contactType.DisplayOnCommunicationsAs;
                    }
                }

                if (model.Case.Summary.MaxApprovedThruDate.HasValue && model.Case.Summary.MinApprovedThruDate.HasValue)
                {
                    model.Case.Summary.MinDuration = (model.Case.Summary.MaxApprovedThruDate - model.Case.Summary.MinApprovedThruDate);
                    if (model.Case.Summary.MinDuration.HasValue)
                    {
                        model.Case.Summary.MinDurationString = model.Case.Summary.MinDuration.ToString().Split('.')[0];
                    }
                }

                
                
                model.Case.Summary.ApprovalNotificationDateSent = GetLastSentDate(model.Case.Id, ((comm.Template == "") ? "ELGNOTICE" : comm.Template));
                model.Case.Summary.MinDuration = (model.Case.Summary.MaxApprovedThruDate - model.Case.Summary.MinApprovedThruDate);
                model.Case.Summary.MinDurationString = model.Case.Summary.MinDuration.ToString().Split('.')[0];


                model.Employee = model.Case.Employee;
                model.Employer = model.Case.Employer;
                model.Customer = model.Case.Employer.Customer;
                model.ActiveSegment = model.Case.Segments.OrderByDescending(s => s.CreatedDate).First();

                if (model.Case.AccommodationRequest != null && model.Case.AccommodationRequest.Accommodations != null)
                {
                    Guid? activeAccommodationId = null;
                    if (todo != null)
                    {
                        activeAccommodationId = todo.Metadata.GetRawValue<Guid?>("ActiveAccommodationId") ?? todo.Metadata.GetRawValue<Guid?>("AccommodationId");
                    }

                    if (activeAccommodationId.HasValue)
                    {
                        model.ActiveAccommodation = model.Case.AccommodationRequest.Accommodations.FirstOrDefault(a => a.Id == activeAccommodationId);
                    }

                    if (model.ActiveAccommodation == null)
                    {
                        model.ActiveAccommodation = model.Case.AccommodationRequest.Accommodations.OrderByDescending(a => a.CreatedDate).FirstOrDefault();
                    }
                }
            }

            model.Communication = comm;
            model.CurrentUser = CurrentUser;
            if (CurrentUser != null)
            {
                var employerAccess = model.CurrentUser.Employers.FirstOrDefault(e => e.EmployerId == comm.EmployerId);
                // If we have employer access record, set the employee to that
                if (employerAccess != null)
                {
                    model.CurrentUserEmployee = employerAccess.Employee;
                }

                // If we still don't have an employee record, look it up via email
                if (model.CurrentUserEmployee == null)
                {
                    model.CurrentUserEmployee = Employee.AsQueryable().Where(e => e.EmployerId == comm.EmployerId && e.Info.Email == CurrentUser.Email).FirstOrDefault();
                }

                // If we STILL don't have an employee record, let's fake it
                if (model.CurrentUserEmployee == null)
                {
                    model.CurrentUserEmployee = new Employee()
                    {
                        CustomerId = comm.CustomerId,
                        EmployerId = comm.EmployerId,
                        EmployerName = comm.Employer.Name,
                        FirstName = CurrentUser.FirstName,
                        LastName = CurrentUser.LastName,
                        Info = new EmployeeInfo()
                        {
                            Email = CurrentUser.Email
                        }
                    };
                }
            }

            return model;
        }

        private void AttachLatestPaperwork(Template template, TemplateRenderModel model, Communication comm)
        {
            if (!template.AttachLatestIncompletePaperwork)
            {
                return;
            }

            CommunicationPaperwork c = GetLastIncompletePaperWork(model.Case.Id);
            if (c != null)
            {
                /// We actually need to convert the return attachment into new paperwork
                CommunicationPaperwork incompletePaperwork = new CommunicationPaperwork()
                {
                    AttachmentId = c.ReturnAttachmentId,
                    Paperwork = new Paperwork()
                    {
                        Name = c.ReturnAttachment.Description ?? c.ReturnAttachment.FileName
                    }
                };
                //If somehow GetComunicatioPaperWork function returns nothing then Paperwork will be null
                //and generate a exception here
                if (comm.Paperwork == null)
                {
                    comm.Paperwork = new List<CommunicationPaperwork>();
                }
                comm.Paperwork.Add(incompletePaperwork);
            }
        }

        /// <summary>
        /// Renders the template from word document.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="paperwork">The paperwork.</param>
        /// <param name="myCase">My case.</param>
        /// <returns></returns>
        private string RenderTemplateFromWordDoc(Template template, List<CommunicationPaperwork> paperwork, TemplateRenderModel model)
        {
            MailMergeData data = CopyCaseData(model, paperwork);
            model.MailMergeData = data;
            Stream docStream = template.Document.DownloadStream();
            return AbsenceSoft.Rendering.Template.RenderTemplateToHTML(docStream, data);
        }

        /// <summary>
        /// Renders the template from word document.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="paperwork">The paperwork.</param>
        /// <param name="myCase">My case.</param>
        /// <returns></returns>
        private byte[] RenderTemplateFromWordDoc(byte[] file, List<CommunicationPaperwork> paperwork, TemplateRenderModel model)
        {
            MailMergeData data = CopyCaseData(model, paperwork);
            return AbsenceSoft.Rendering.Template.RenderWordTemplate(file, Aspose.Words.SaveFormat.Pdf, data);
        }

        /// <summary>
        /// Renders the template from PDF.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="paperwork">The paperwork.</param>
        /// <param name="model">The model.</param>
        /// <param name="pdfFields">The PDF fields.</param>
        /// <returns></returns>
        private byte[] RenderTemplateFromPdf(byte[] file, List<CommunicationPaperwork> paperwork, TemplateRenderModel model, Dictionary<string, List<string>> pdfFields)
        {
            MailMergeData data = CopyCaseData(model, paperwork);
            return AbsenceSoft.Rendering.Template.RenderPdfTemplate(file, data, pdfFields);
        }

        /// <summary>
        /// Copies the case data.
        /// </summary>
        /// <param name="myCase">My case.</param>
        /// <param name="paperwork">The paperwork.</param>
        /// <returns></returns>
        private MailMergeData CopyCaseData(TemplateRenderModel model, List<CommunicationPaperwork> paperwork)
        {
            MailMergeData data = new MailMergeData();
            List<EmployeeJob> jobs = null;
            List<EmployeeRestriction> restrictions = null;
            List<Relapse> relapses = null;

            if (model.Employee != null)
            {
                jobs = EmployeeJob.AsQueryable().Where(ej => ej.EmployeeNumber == model.Employee.EmployeeNumber).ToList();
                restrictions = EmployeeRestriction.AsQueryable().Where(er => er.EmployeeId == model.Employee.Id && er.CaseId == model.Case.Id).ToList();
            }

            List<CaseNote> caseNotes = GetCaseNotes(model.Case.Id);
            List<Demand> demands = null;
            List<DemandType> demandTypes = null;
            if (restrictions != null && restrictions.Count > 0)
            {
                demands = Demand.DistinctFind(null, CustomerId, EmployerId).ToList();
                demandTypes = DemandType.DistinctFind(null, CustomerId, EmployerId).ToList(); ;
            }

            List<DateTime> paperworkDueDateList = new List<DateTime>();
            DateTime? _firstCasePaperworkDueDate = null;
            DateTime? _lastCasePaperworkDueDate = null;
            var communications = Communication.AsQueryable().Where(c => c.CaseId == model.Case.Id && c.Paperwork != null).ToList();
            if (communications.Any())
            {
                var todos = GetToDoItems(model.Case.Id);
                foreach (var paperworkList in communications.Where(c => c.Paperwork != null && c.Paperwork.Any(p => p.DueDate.HasValue)).SelectMany(c => c.Paperwork.Where(p => p != null && p.DueDate.HasValue)))
                {
                    //Pick the paperwork for which ToDoItem is not complete. If the Paperwork ToDo has been completed (marking Paperwork as either complete, incomplete, or not received) that should close that Paperwork.
                    //If a new set of paperwork is sent out, it creates a ToDoItem with a new due date based on the Return Date Adjustment configuration on the paperwork.
                    var todo = todos.FirstOrDefault(t => t.Metadata.GetRawValue<Guid?>("PaperworkId") == paperworkList.Id && t.DueDate == paperworkList.DueDate);
                    if (todo != null && todo.Status != ToDoItemStatus.Complete)
                    {
                        paperworkDueDateList.AddIfNotExists(paperworkList.DueDate.Value);
                        if (paperworkList.DueDateHistory != null && paperworkList.DueDateHistory.Any())
                        {
                            paperworkDueDateList.AddRangeIfNotExists(paperworkList.DueDateHistory.Select(h => h.DueDate));
                        }
                    }
                }
            }
            //Adding it to handle paperwork for the current communication being created and handling LastPaperworkDueDate and FirstPaperworkDueDate 
            if (paperwork != null)
            {
                foreach (var paperworkList in paperwork)
                {
                    if (paperworkList.DueDate.HasValue)
                    {
                        paperworkDueDateList.AddIfNotExists(paperworkList.DueDate.Value);
                    }
                }
            }

            if (paperworkDueDateList.Any())
            {
                _firstCasePaperworkDueDate = paperworkDueDateList.Min();
                _lastCasePaperworkDueDate = paperworkDueDateList.Max();

            }

            List<CaseAssignee> caseAssignees = null;
            using (var caseService = new CaseService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                relapses = caseService.GetRelapses(model.Case.Id);
                caseAssignees = caseService.GetCaseAssignees(model.Case.Id);
                if (model.Case.Summary != null)
                {
                    PolicySummary ps = caseService.GetEmployeePolicySummary(model.Case.Employee, DateTime.UtcNow, model.Case, true, "FMLA").FirstOrDefault();
                    if (ps != null)
                    {
                        model.Case.Summary.FmlaProjectedUsageText = string.Format("{0:N2} {1}", ps.TimeUsed, ps.Units);
                    }
                                      
                }
            }
            data.CopyCaseAssigneeData(caseAssignees);
            data.CopyCaseData(model.Case, model.Employee, model.CurrentUser, model.AdminContacts, paperwork, caseNotes, relapses);
            data.UpdatePolicyUtilization(GetPolicyUtilizationSummary(model.Case));
            data.CopyEmployeeData(model.Employee, model.AdminContacts, restrictions, jobs, model.Case, demands, demandTypes);
            data.CopyLogoData(GetLogo(CurrentCustomer.LogoId), GetLogo(model.Employer.LogoId));
            List<CommunicationPaperwork> duePaperWorks = GetCasePaperworkDue(model.Case);
            if (duePaperWorks != null && duePaperWorks.Any())
            {
                var paperworkReviewStatus = new[] { PaperworkReviewStatus.NotApplicable, PaperworkReviewStatus.Pending, PaperworkReviewStatus.NotReceived, PaperworkReviewStatus.Incomplete };
                data.CopyDuePaperworkData(duePaperWorks.Where(paperWork => paperworkReviewStatus.Contains(paperWork.Status)).ToList(), _firstCasePaperworkDueDate, _lastCasePaperworkDueDate);
            }
            else
            {
                //If this is the first time communication is sent and it has Paperwork use its duedate 
                if (paperwork != null && paperwork.Any())
                {
                    var paperworkReviewStatus = new[] { PaperworkReviewStatus.NotApplicable, PaperworkReviewStatus.Pending, PaperworkReviewStatus.NotReceived, PaperworkReviewStatus.Incomplete };
                    _firstCasePaperworkDueDate = paperwork.Where(p => p.DueDate.HasValue).Min(pd => pd.DueDate);
                    if (_lastCasePaperworkDueDate == null)
                    {
                        _lastCasePaperworkDueDate = paperwork.Where(p => p.DueDate.HasValue).Max(pd => pd.DueDate);
                    }
                    data.CopyDuePaperworkData(paperwork.Where(paperWork => paperworkReviewStatus.Contains(paperWork.Status)).ToList(), _firstCasePaperworkDueDate, _lastCasePaperworkDueDate);
                }
            }
            using (var serviceOptionService = new EmployerServiceOptionService(CurrentCustomer, CurrentEmployer, null))
            {
                EmployerServiceOption _serviceOption = serviceOptionService.GetEmployerServiceOption(model.Employer.Id);
                data.Employer.ServiceType = _serviceOption != null && _serviceOption.CusomerServiceOption != null && _serviceOption.CusomerServiceOption.Value != null ? _serviceOption.CusomerServiceOption.Value : string.Empty;
            }
            return data;
        }

        /// <summary>
        /// Gets the Case ToDoItems
        /// </summary>
        /// <param name="caseId">The case identifier</param>
        /// <returns></returns>
        private List<ToDoItem> GetToDoItems(string caseId)
        {
            return ToDoItem.AsQueryable().Where(c => c.CaseId == caseId).ToList();
        }

        /// <summary>
        /// Gets the Case Notes
        /// </summary>
        /// <param name="caseId">The case identifier</param>
        /// <returns></returns>
        private List<CaseNote> GetCaseNotes(string caseId)
        {
            return CaseNote.AsQueryable().Where(c => c.CaseId == caseId).ToList();
        }

        /// <summary>
        /// Get Case Paperworks
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private List<CommunicationPaperwork> GetCasePaperworkDue(Case employeeCase)
        {

            List<IMongoQuery> ands = new List<IMongoQuery>
            {
                Communication.Query.EQ(c => c.CustomerId, employeeCase.CustomerId),
                Communication.Query.EQ(c => c.EmployerId, employeeCase.EmployerId),
                Communication.Query.EQ(c => c.CaseId, employeeCase.Id),
                Communication.Query.ElemMatch(c => c.Paperwork, p => p.And(
                    Query<CommunicationPaperwork>.Exists(d => d.DueDate),
                    Query<CommunicationPaperwork>.NE(d => d.DueDate, null),
                    Query<CommunicationPaperwork>.EQ(d => d.Status, PaperworkReviewStatus.NotApplicable)
                    ))
            };
            return Communication.Query.Find(Query.And(ands)).SelectMany(c => c.Paperwork).ToList();
        }

        public List<ExpandoObject> GetPolicyUtilizationSummary(Case myCase)
        {
            var casePolicies = (myCase.Segments ?? new List<CaseSegment>(0))
                    .SelectMany(s => s.AppliedPolicies ?? new List<AppliedPolicy>(0))
                    .Where(p => p.Policy != null)
                    .Select(p => p.Policy.Code)
                    .ToArray();
            List<PolicySummary> policySummaries = new CaseService().GetEmployeePolicySummary(myCase.Employee, DateTime.Today, myCase, true, casePolicies);
            List<ExpandoObject> modelList = new List<ExpandoObject>();

            if (policySummaries != null)
            {
                foreach (var policySummary in policySummaries)
                {
                    dynamic policySummaryModel = new ExpandoObject();
                    policySummaryModel.PolicyCode = policySummary.PolicyCode;

                    var timeRemainingUnitsText = policySummary.Units.ToString().ToLowerInvariant();
                    if (policySummary.TimeRemaining == 1)
                    {
                        // remove the s since it is singular
                        timeRemainingUnitsText = timeRemainingUnitsText.Remove(timeRemainingUnitsText.Length - 1);
                    }
                    policySummaryModel.TimeRemainingText = String.Format("{0:N2} {1}", policySummary.TimeRemaining, timeRemainingUnitsText);
                    if (policySummary.TimeRemaining < 0 && policySummary.HoursRemaining == "reasonable amount")
                    {
                        policySummaryModel.TimeRemainingText = "reasonable amount";
                    
                    }
                    else if (policySummary.TimeRemaining < 0)
                    {
                        policySummaryModel.TimeRemainingText = String.Format("{0:N2} {1}", 0D, timeRemainingUnitsText);
                    }

                    var timeUsedUnitsText = policySummary.Units.ToString().ToLowerInvariant();
                    if (policySummary.TimeUsed == 1)
                    {
                        // remove the s since it is singular
                        timeUsedUnitsText = timeUsedUnitsText.Remove(timeUsedUnitsText.Length - 1);
                    }
                    policySummaryModel.TimeUsedText = String.Format("{0:N2} {1}", policySummary.TimeUsed, timeUsedUnitsText);

                    policySummaryModel.HoursUsedText = policySummary.HoursUsed;
                    policySummaryModel.HoursRemainingText = policySummary.HoursRemaining;

                    if (!string.IsNullOrWhiteSpace(policySummaryModel.HoursRemainingText) && policySummaryModel.HoursRemainingText != "reasonable amount")
                    {
                        string hrsRemainingText = policySummaryModel.HoursRemainingText;
                        int min = hrsRemainingText.ParseFriendlyTime();
                        if (min < 0)
                        {
                            policySummaryModel.HoursRemainingText = "0h";
                        }
                    }
                    policySummaryModel.ProjectedUsageText = "";
                    var policyfromCaseSummary = myCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == policySummary.PolicyCode);
                    if (policyfromCaseSummary != null)
                    {
                        policySummaryModel.ProjectedUsageText = policyfromCaseSummary.ProjectedUsageText;
                    }
                    modelList.Add(policySummaryModel);
                }
            }
            return modelList;
        }

        /// <summary>
        /// Gets the logo.
        /// </summary>
        /// <param name="logoId">The logo identifier.</param>
        /// <returns></returns>
        private byte[] GetLogo(string logoId)
        {
            Document logoDoc = Document.GetById(logoId);
            byte[] logo = null;
            if (logoDoc != null)
            {
                logo = logoDoc.Download().File;
            }
            return logo;
        }

        /// <summary>
        /// Gets the admin contacts.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        private List<EmployeeContact> GetAdminContacts(string employeeId)
        {
            List<string> contactTypes = ContactType.AsQueryable().Where(c => c.ContactCategory == ContactTypeDesignationType.Administrative).Select(c => c.Code).ToList();
            List<EmployeeContact> contacts = EmployeeContact.AsQueryable().Where(m => m.EmployeeId == employeeId).ToList();
            List<EmployeeContact> adminContacts = (from m in contactTypes
                                                   join n in contacts on m equals n.ContactTypeCode
                                                   select n).ToList();
            return adminContacts;
        }

        /// <summary>
        /// Gets the last incomplete paper work.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        private CommunicationPaperwork GetLastIncompletePaperWork(string caseId)
        {
            List<Communication> communications = Communication.AsQueryable().Where(c => c.CaseId == caseId && c.Paperwork.Count > 0).ToList();
            return communications.SelectMany(x => x.Paperwork)
                .LastOrDefault(y => (y.Status == PaperworkReviewStatus.Incomplete || y.Status == PaperworkReviewStatus.NotReceived)
                                && !string.IsNullOrEmpty(y.ReturnAttachmentId));
        }

        /// <summary>
        /// Gets the last incomplete paper work.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="templateKey"></param>
        /// <returns></returns>
        private DateTime? GetLastSentDate(string caseId, string templateKey)
        {
            List<Communication> communications = Communication.AsQueryable().Where(c => c.CaseId == caseId && c.Template == templateKey).ToList();
            if (communications.Any() && communications.SelectMany(p => p.SentDates).Any())
            {
                return communications.SelectMany(p => p.SentDates).Max();
            }
            return null;
        }

        /// <summary>
        /// Gets the communication paperwork.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="template">The template.</param>
        /// <param name="wf">The wf.</param>
        /// <returns></returns>
        protected virtual List<CommunicationPaperwork> GetCommunicationPaperwork(TemplateRenderModel model, Template template, ToDoItem wf = null)
        {
            List<CommunicationPaperwork> comms = null;
            if (template.PaperworkCodes != null && template.PaperworkCodes.Any())
            {
                DateTime? _firstCasePaperworkDueDate = null;
                var lstResult = Communication.AsQueryable().Where(c => c.CaseId == model.Case.Id && c.Paperwork != null).ToList();
                if (lstResult != null && lstResult.Any())
                {
                    var paperWork = lstResult.Select(c => c.Paperwork.Select(p => p.DueDate).Min()).FirstOrDefault();
                    if (paperWork != null)
                    {
                        _firstCasePaperworkDueDate = paperWork;
                    }
                }
                IMongoQuery paperworkMatchesCode = Paperwork.Query.And(Paperwork.Query.In(p => p.Code, template.PaperworkCodes));
                var allRelatedPaperworks = Paperwork.DistinctFind(paperworkMatchesCode, model.Customer.Id, model.Employer.Id);
                comms = new List<CommunicationPaperwork>(allRelatedPaperworks.Count());
                foreach (var p in allRelatedPaperworks)
                {
                    if (p != null)
                    {
                        if (p.Criteria != null && p.Criteria.Any())
                        {
                            bool success = true;
                            foreach (var group in p.Criteria)
                            {
                                int suc = 0;
                                foreach (var rule in group.Criteria)
                                {
                                    switch (rule.CriteriaType)
                                    {
                                        case PaperworkCriteriaType.None:
                                            suc++;
                                            break;
                                        case PaperworkCriteriaType.Policy:
                                            if (model.Case.Segments.Any(s => s.AppliedPolicies.Any(a => a.Status == EligibilityStatus.Eligible && a.Policy.Code == rule.Criteria)))
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.AbsenceReason:
                                            if (model.Case.Reason.Code.ToUpperInvariant() == rule.Criteria.ToUpperInvariant())
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.CaseType:
                                            if (model.Case.Segments.Any(s => s.Type.ToString() == rule.Criteria))
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.ContactType:
                                            if (model.Case.Contact != null && model.Case.Contact.ContactTypeCode == rule.Criteria)
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.MilitaryStatus:
                                            if (model.Case.Contact != null && model.Case.Contact.MilitaryStatus.ToString() == rule.Criteria)
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.LeaveOfAbsenceExpression:
                                            LeaveOfAbsence loa = new LeaveOfAbsence()
                                            {
                                                ActiveSegment = model.ActiveSegment,
                                                ActiveWorkFlowItem = wf,
                                                Case = model.Case,
                                                Customer = model.Customer,
                                                Employee = model.Employee,
                                                Employer = model.Employer,
                                                WorkSchedule = model.WorkSchedule
                                            };
                                            try
                                            {
                                                if (LeaveOfAbsenceEvaluator.Eval<bool>(rule.Criteria, loa))
                                                {
                                                    suc++;
                                                }
                                            }
                                            catch (NullReferenceException) { /*Only ignore Null refs, otherwise life goes on*/ }
                                            break;
                                        case PaperworkCriteriaType.Accommodation:
                                            if (model.ActiveAccommodation != null && model.ActiveAccommodation.Type != null && model.ActiveAccommodation.Type.Code == rule.Criteria)
                                            {
                                                suc++;
                                            }
                                            break;
                                        case PaperworkCriteriaType.WorkState:
                                            if (model.Employee != null && model.Employee.WorkState != null && string.Compare(model.Employee.WorkState, rule.Criteria, true) == 0)
                                            {
                                                suc++;
                                            }
                                            break;
                                    }
                                }
                                switch (group.SuccessType)
                                {
                                    default:
                                    case RuleGroupSuccessType.And:
                                        success &= suc == group.Criteria.Count;
                                        break;
                                    case RuleGroupSuccessType.Or:
                                        success &= suc > 0;
                                        break;
                                    case RuleGroupSuccessType.Not:
                                        success &= suc == 0;
                                        break;
                                    case RuleGroupSuccessType.Nor:
                                        success &= suc < group.Criteria.Count;
                                        break;
                                }
                                if (!success)
                                {
                                    break;
                                }
                            }
                            if (success)
                            {
                                comms.Add(new CommunicationPaperwork()
                                {
                                    Paperwork = p,
                                    FirstCasePaperworkDueDate = _firstCasePaperworkDueDate,
                                    Fields = p.Fields
                                }.SetDueDate(p.DueDateFromNow, CurrentUser));
                            }
                        }
                        else
                        {
                            comms.Add(new CommunicationPaperwork()
                            {
                                Paperwork = p,
                                FirstCasePaperworkDueDate = _firstCasePaperworkDueDate,
                                Fields = p.Fields
                            }.SetDueDate(p.DueDateFromNow, CurrentUser));
                        }
                    }
                }
            }
            return comms;
        }//GetCommunicationPaperwork

        public void DeleteDraft(string communicationId)
        {
            if (string.IsNullOrEmpty(communicationId))
            {
                return;
            }

            Communication draft = Communication.GetById(communicationId);
            if (!draft.IsDraft)
            {
                return;
            }
            draft.Delete();
        }

        public void BulkDraftDelete(params string[] communicationIds)
        {
            var isDraftAndIsInList = Communication.Query.And(
                Communication.Query.EQ(c => c.CustomerId, CustomerId),
                Communication.Query.EQ(c => c.IsDraft, true),
                Communication.Query.In(c => c.Id, communicationIds.ToList())
                );
            Communication.Delete(isDraftAndIsInList);
        }

        /// <summary>
        /// Sends a communication by generating the final output based on the provided communication info and type of communication.
        /// Performs any send operation for email, or for mail/fax will generate the final PDF document for download, etc.
        /// </summary>
        /// <param name="comm">The communication to send.</param>
        /// <param name="wf">[optional] The todo item the communication is in relation to (if any).</param>
        /// <returns>The sent communication with all of its dates updated, etc.</returns>
        public Communication SendCommunication(Communication comm, ToDoItem wf = null, string templateKey = null)
        {
            using (new InstrumentationContext("CommunicationService.SendCommunication"))
            {
                TemplateRenderModel model = BuildTemplateRenderModel(comm, wf);
                if (comm.Paperwork != null)
                {
                    Parallel.ForEach(comm.Paperwork.Where(p => p.Paperwork != null
                        && !string.IsNullOrWhiteSpace(p.Paperwork.FileId)
                        && string.IsNullOrWhiteSpace(p.AttachmentId)),
                        p =>
                        {
                            var attachment = new AbsenceSoft.Data.Communications.Attachment()
                            {
                                CaseId = comm.CaseId,
                                EmployeeId = comm.EmployeeId,
                                EmployeeNumber = comm.EmployeeNumber,
                                FileName = p.Paperwork.FileName,
                                Description = p.Paperwork.Description,
                                EmployerId = model.Employer.Id,
                                CustomerId = model.Customer.Id,
                                AttachmentType = AttachmentType.Paperwork
                            };

                            // Read the file document from GridFS into a memory stream so we can convert it to a byte array.
                            attachment.ContentType = p.Paperwork.Document.ContentType;
                            attachment.File = p.Paperwork.Document.Download().File;
                            switch (p.Paperwork.DocType)
                            {
                                case DocumentType.Pdf:
                                    //If its a Pdf and FillPdf is true and PdfFields is having value that we means we have 
                                    //mergetokens inside it , so need inboke the method else just do the normal attachement
                                    if (p.Paperwork.FillPdf && p.Paperwork.PdfFields != null && p.Paperwork.PdfFields.Any())
                                    {
                                        attachment.File = RenderTemplateFromPdf(attachment.File, comm.Paperwork, model, p.Paperwork.PdfFields);
                                    }
                                    break;
                                case DocumentType.Template:
                                    attachment.File = Rendering.Pdf.ConvertHtmlToPdf(Rendering.Template.RenderTemplate(Encoding.UTF8.GetString(attachment.File), model));
                                    attachment.ContentType = "application/pdf";
                                    attachment.FileName = Regex.Replace(attachment.FileName, @"\..{2,6}$", ".pdf");
                                    p.Paperwork.FileName = attachment.FileName;
                                    break;
                                case DocumentType.Html:
                                    attachment.File = Rendering.Pdf.ConvertHtmlToPdf(Encoding.UTF8.GetString(attachment.File));
                                    attachment.ContentType = "application/pdf";
                                    attachment.FileName = Regex.Replace(attachment.FileName, @"\..{2,6}$", ".pdf");
                                    p.Paperwork.FileName = attachment.FileName;
                                    break;
                                case DocumentType.MSWordDocument:
                                    try
                                    {
                                        attachment.File = RenderTemplateFromWordDoc(attachment.File, comm.Paperwork, model);
                                    }
                                    catch (Aspose.Words.UnsupportedFileFormatException)
                                    {
                                        //If the merge fails, we'll just use whatever file they originally uploaded
                                        attachment.File = p.Paperwork.Document.File;
                                    }
                                    attachment.ContentType = "application/pdf";
                                    attachment.FileName = Regex.Replace(attachment.FileName, @"\..{2,6}$", ".pdf");
                                    p.Paperwork.FileName = attachment.FileName;
                                    break;
                            }
                            p.Attachment = new AttachmentService().Using(a => a.CreateAttachment(attachment));
                        });
                }

                string errorMsg = null;
                switch (comm.CommunicationType)
                {
                    case CommunicationType.Mail:
                        Stream docStream = null;
                        if (!string.IsNullOrWhiteSpace(templateKey))
                        {
                            // For mail, we only need to convert the body of the communication to a PDF for printing, done.
                            Template template = Template.GetByCode(templateKey, model.Case.CustomerId, model.Case.EmployerId);
                            if (template == null)
                            {
                                errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - Failed to retrieve template for code: " , templateKey) ;
                                Log.Error(errorMsg);
                                throw new AbsenceSoftException(errorMsg);
                            }

                            if (template.DocType == DocumentType.MSWordDocument)
                            {
                                docStream = new MemoryStream();
                                template.Document.DownloadStream().CopyTo(docStream);
                            }
                        }
                        var document = Rendering.Pdf.ConvertHtmlToPdf(comm.Body, docStream);
                        comm.Attachment = new AttachmentService().Using(a => a.CreateAttachment(comm.EmployeeId, comm.CaseId, AttachmentType.Communication, comm.Name + ".pdf", document, "application/pdf", comm.Public, comm.Subject));
                        break;

                    case CommunicationType.Email:
                        // For email, we keep the body as HTML and embed it as the message body
                        MailMessage msg = new MailMessage();
                        comm.Recipients.ForEach(r => msg.To.Add(r.Email));
                        if (comm.CCRecipients != null && comm.CCRecipients.Any())
                        {
                            comm.CCRecipients.ForEach(r => msg.CC.Add(r.Email));
                        }

                        string msgBody = comm.Body;
                        msg.Subject = comm.Subject;
                        msg.BodyEncoding = Encoding.UTF8;
                        msg.IsBodyHtml = true;
                        msg.Priority = MailPriority.Normal;

                        int matchIndex = msgBody.IndexOf("src=\"data:image");
                        int imageCount = 0;
                        List<LinkedResource> linkedResources = new List<LinkedResource>();
                        while (matchIndex >= 0)
                        {
                            int startIndex = matchIndex + 5;
                            int base64EncodeStartIndex = msgBody.IndexOf(",", startIndex) + 1;
                            int endIndex = msgBody.IndexOf("\"", startIndex);
                            byte[] theLogo = Convert.FromBase64String(msgBody.Substring(base64EncodeStartIndex, endIndex - base64EncodeStartIndex));
                            if (theLogo != null)
                            {
                                imageCount++;
                                MemoryStream stream = new MemoryStream(theLogo);
                                LinkedResource logo = new LinkedResource(stream)
                                {
                                    ContentId = "logo" + imageCount
                                };
                                linkedResources.Add(logo);
                                msgBody = msgBody.Substring(0, startIndex) + ("cid:logo" + imageCount) + msgBody.Substring(endIndex, msgBody.Length - endIndex);
                                matchIndex = msgBody.IndexOf("src=\"data:image");
                            }
                            else
                            {
                                matchIndex = msgBody.IndexOf("src=\"data:image", endIndex);
                            }
                        }

                        AlternateView av1 = AlternateView.CreateAlternateViewFromString(msgBody, Encoding.UTF8, System.Net.Mime.MediaTypeNames.Text.Html);
                        linkedResources.ForEach<LinkedResource>(linkedResource => av1.LinkedResources.Add(linkedResource));
                        msg.AlternateViews.Add(av1);

                        //Get Customer/Employer Default From EmailAddress
                        // Send of behalf of current user.  Or use default address from Customer or Employer
                        string replyToAddress = null;
                        string replyToName = null;

                        EmailReplies emailReplyTo = GetDefaultEmailReplyTo(model);

                        var user = CurrentUser;
                        if (emailReplyTo == EmailReplies.CurrentUser && user != null && !string.IsNullOrWhiteSpace(user.Email) && user.Email.Contains("@") && user.Email.Contains("."))
                        {
                            replyToAddress = user.Email;
                            replyToName = user.DisplayName;
                        }
                        else
                        {
                            replyToAddress = GetDefaultEmailFromAddress(model.Employer);
                        }

                        //
                        // set "reply-to" header
                        if (replyToAddress != null)
                        {
                            if (replyToName != null)
                            {
                                // include friendly name if possible
                                msg.ReplyToList.Add(new MailAddress(replyToAddress, replyToName));
                            }
                            else
                            {
                                msg.ReplyToList.Add(new MailAddress(replyToAddress));
                            }
                        }
                        //
                        // if possible, set friendly name in "from" header
                        if (replyToName != null)
                        {
                            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                            msg.From = new MailAddress(smtpSection.From, replyToName);
                        }
                        msg.Body = msgBody;

                        string fromEmail = GetDefaultEmailFromAddress(model.Employer);
                        if (!String.IsNullOrWhiteSpace(fromEmail))
                        {
                            msg.From = new MailAddress(fromEmail);
                        }

                        if (comm.Paperwork != null && comm.Paperwork.Any())
                        {
                            foreach (var p in comm.Paperwork)
                            {
                                if (p != null && p.Attachment != null && !string.IsNullOrWhiteSpace(p.Attachment.FileId))
                                {
                                    msg.Attachments.Add(new System.Net.Mail.Attachment(p.Attachment.Document.DownloadStream(), p.Attachment.FileName, p.Attachment.ContentType));
                                }
                            }
                        }
                        if (msg.Attachments?.Sum(p => p.ContentStream.Length) > 10485760)
                        {
                            errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - File attachment(s) more than 10485760 bytes long. ");
                            Log.Error(errorMsg);
                            throw new AbsenceSoftException(errorMsg);
                        }
                        if (comm.CaseAttachments != null && comm.CaseAttachments.Any())
                        {
                            foreach (var p in comm.CaseAttachments)
                            {
                                if (!string.IsNullOrWhiteSpace(p.FileId))
                                {
                                    msg.Attachments.Add(new System.Net.Mail.Attachment(p.Document.DownloadStream(), p.FileName, p.ContentType));
                                }
                            }
                        }
                        if (msg.Attachments?.Sum(p => p.ContentStream.Length) > 10485760)
                        {
                            errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - File attachment(s) more than 10485760 bytes long. ");
                            Log.Error(errorMsg);
                            throw new AbsenceSoftException(errorMsg);
                        }
                        var result = NotificationApiHelper.SendEmailAsync(msg, CurrentUser, Utilities.GetTokenFromClaims()).GetAwaiter().GetResult();
                        if (result.StatusCode != System.Net.HttpStatusCode.OK || result.Success == false)
                        {
                            errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - ", result.Message);
                            Log.Error(errorMsg);
                            throw new AbsenceSoftException(errorMsg);
                        }
                        break;

                
                       
                    default:
                        errorMsg = string.Concat("Error ID: ", RandomString.Generate(8, true, true, true, false), " - This communication method is not yet supported, check back in a little bit, we'll get there");
                        Log.Error(errorMsg);
                        throw new AbsenceSoftException(errorMsg);
                }

                comm.SentDate = DateTime.UtcNow;
                comm.CreatedByName = CurrentUser.DisplayName;
                comm.CreatedByEmail = CurrentUser.Email;

                DeleteDraft(comm.Id);
                comm = comm.Save();
                if (!SuppressWorkflow)
                {
                    comm.WfOnCommunicationSent(wf);


                    if (comm.Paperwork != null)
                    {
                        if (comm.CommunicationType != CommunicationType.Mail)
                        {
                            comm.Paperwork.RemoveAll(p => p.Paperwork.RequiresReview == false && (p.Paperwork.ReturnDateAdjustment == null || p.Paperwork.ReturnDateAdjustmentDays == 0));                            
                        }
                        comm.Paperwork.ForEach(p => p.WfOnPaperworkSent(comm, wf));
                    }
                }
                return comm;
            }
        }

        public Communication SaveCommunication(Communication communication)
        {
            return communication.Save();
        }

        /// <summary>
        /// Get Customer/Employer Default From EmailAddress 
        /// </summary>
        /// <param name="employer"></param>
        /// <returns></returns>
        public string GetDefaultEmailFromAddress(Employer employer)
        {
            if (employer == null)
            {
                return null;
            }

            string fromAddress = employer.DefaultEmailFromAddress;

            if (!String.IsNullOrWhiteSpace(fromAddress))
            {
                return fromAddress;
            }
            if (employer.Customer != null)
            {
                fromAddress = employer.Customer.DefaultEmailFromAddress;
            }
            return fromAddress;

        }

        /// <summary>
        /// Gets the default email reply to.
        /// </summary>
        /// <param name="templateRenderModel">The template render model.</param>
        /// <returns></returns>
        private EmailReplies GetDefaultEmailReplyTo(TemplateRenderModel templateRenderModel)
        {
            EmailReplies emailReply = EmailReplies.CurrentUser;

            if (templateRenderModel.EmailRepliesType.HasValue)
            {
                return templateRenderModel.EmailRepliesType.Value;
            }

            if (templateRenderModel.Employer != null && templateRenderModel.Employer.EmailRepliesType.HasValue)
            {
                return templateRenderModel.Employer.EmailRepliesType.Value;
            }

            if (templateRenderModel.Employer.Customer != null && templateRenderModel.Employer.Customer.EmailRepliesType.HasValue)
            {
                return templateRenderModel.Employer.Customer.EmailRepliesType.Value;
            }
            return emailReply;
        }

        /// <summary>
        /// Creates the communication package from the letter and any/all attachments, which are hopefully all PDFs.
        /// </summary>
        /// <param name="communicationId">The communication id.</param>
        /// <returns></returns>
        public Document CreateCommunicationPackage(Communication comm)
        {
            // Get the communication, if it's not found, bail out and return null, doesn't exist.
            if (comm == null)
            {
                return null;
            }

            // Create our document that we're going to return as a result, this will not generally be a "stored"
            //  document and should be streamed, however in the future may want to, either way it has the structure
            //  we need, info we want to share already and could be saved I guess if you really wanted to, but really
            //  don't do it, just use it as a vehicle for returning this information and we'll be square.
            Document doc = new Document()
            {
                ContentType = "application/pdf",
                CaseId = comm.CaseId,
                CustomerId = comm.CustomerId,
                EmployeeId = comm.EmployeeId,
                EmployerId = comm.EmployerId,
                FileName = string.Format("{0}.pdf", comm.Name),
                ContentLength = 0L
            };

            // If we have the attachment for the communication (this is the generated PDF that would have been "printed")
            //  If we do, then just set the file bytes to that, it should already be a PDF so all done, awesome!
            if (comm.Attachment != null && comm.Attachment.Document != null)
            {
                doc.File = comm.Attachment.Document.File ?? comm.Attachment.Document.Download().File;
            }

            // If we still don't have a file but we have a communication body, must have been an email, in which case
            //  now since we're going to build a "packet" with other documents, it needs to be a PDF, so here we go.
            if (doc.File == null && !string.IsNullOrWhiteSpace(comm.Body))
            {
                doc.File = Rendering.Pdf.ConvertHtmlToPdf(comm.Body);
            }

            // Create a list of byte arrays to store each page of the document we need in order, first starting with the letter
            //  if applicable.
            List<byte[]> pdfs = new List<byte[]>(comm.Paperwork.Count + 1 + (comm.CaseAttachments != null && comm.CaseAttachments.Count > 0 ? comm.CaseAttachments.Count + 1 : 0));
            // Add the primary document/file/letter to the pdfs collection to start off our document (could be a cover page in
            //  the future too, who knows right).
            if (doc.File != null)
            {
                pdfs.Add(doc.File);
            }

            // Loop through any paperwork for the communication, this includes enclosures, forms, etc. These paperwork items should also have
            //  either a generated or an uploaded file associated with them as well, for now we have to assume it's an HTML document or PDF document
            //  and if the content type is anything else, no dice, they don't get it then.
            foreach (var p in comm.Paperwork)
            {
                if (p.Attachment != null && p.Attachment.Document != null)
                {
                    // Download the file from S3 (important I guess)
                    p.Attachment.Document.File = p.Attachment.Document.File ?? p.Attachment.Document.Download().File;
                    // If we have a file and the file has some meat to it, let's check out it's content type, again if the
                    //  file is HTML we need to convert it to PDF and if it's already a PDF, then GREAT!!!
                    if (p.Attachment.Document.File != null && p.Attachment.Document.File.LongLength > 0L)
                    {
                        if (p.Attachment.Document.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                        {
                            pdfs.Add(Rendering.Pdf.ConvertMsWordToPdf(p.Attachment.Document.File));
                        }
                        else if (p.Attachment.Document.ContentType == "text/html")
                        {
                            pdfs.Add(Rendering.Pdf.ConvertHtmlToPdf(Encoding.UTF8.GetString(p.Attachment.Document.File)));
                        }
                        else if (p.Attachment.Document.ContentType == "application/pdf")
                        {
                            pdfs.Add(p.Attachment.Document.File);
                        }
                    }
                }
            }
            // Loop through any case attachments            
            if (comm.CaseAttachments != null)
            {
                foreach (var a in comm.CaseAttachments)
                {
                    if (a != null && a.Document != null)
                    {
                        if (a.Document.ContentType == "application/msword" || a.Document.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                        {
                            ADocument document = new ADocument(a.Document.DownloadStream());
                            pdfs.Add(Rendering.Pdf.ConvertHtmlToPdf(document.ToString(Aspose.Words.SaveFormat.Html)));
                        }
                        else
                        {
                            // Download the file from S3 (important I guess)
                            a.Document.File = a.Document.File ?? a.Document.Download().File;
                            // If we have a file and the file has some meat to it, let's check out it's content type, again if the
                            //  file is HTML we need to convert it to PDF and if it's already a PDF, then GREAT!!!
                            if (a.Document.File != null && a.Document.File.LongLength > 0L)
                            {

                                if (a.Document.ContentType == "text/html")
                                {
                                    pdfs.Add(Rendering.Pdf.ConvertHtmlToPdf(Encoding.UTF8.GetString(a.Document.File)));
                                }
                                else if (a.Document.ContentType == "application/pdf")
                                {
                                    pdfs.Add(a.Document.File);
                                }
                            }
                        }
                    }
                }
            }

            // If we have any documents to render at all, cool, we need to merge them together into one big PDF.
            if (pdfs.Count > 0)
            {
                doc.File = Rendering.Pdf.MergePdfDocuments(pdfs.ToArray());
            }

            // If after all is said or done, we actually have a file to return, set the content length and return it.
            if (doc.File != null)
            {
                doc.ContentLength = doc.File.LongLength;
                return doc;
            }

            // Awe, no file bits, we'll just return null then. :(
            return null;
        }

        /// <summary>
        /// Resends the communication.
        /// </summary>
        /// <param name="comm">The comm.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">comm</exception>
        public Communication ResendCommunication(Communication comm)
        {
            if (comm == null)
            {
                throw new ArgumentNullException("comm");
            }
            Communication newComm = comm.Clone();
            newComm.Id = null;
            newComm.Clean();
            return SendCommunication(newComm);
        }

        /// <summary>
        /// Apply communication filters on serach results and the sort by sent date
        /// </summary>
        /// <param name="criteria">Search Crietria</param>
        /// <param name="results">Search Result</param>
        /// <returns></returns>
        private IEnumerable<ListResult> ApplyCommunicationFilters(ListCriteria criteria, IEnumerable<ListResult> results)
        {
            if (!string.IsNullOrEmpty(criteria.Get<string>("CommunicationName")))
            {
                string communicationName = criteria.Get<string>("CommunicationName").ToLower();
                results = results.Where(c => c["Name"] != null && c["Name"].ToString().ToLower().StartsWith(communicationName));
            }

            if (!string.IsNullOrEmpty(criteria.Get<string>("CommunicationSubject")))
            {
                string communicationSubject = criteria.Get<string>("CommunicationSubject").ToLower();
                results = results.Where(c => c["Subject"] != null && c["Subject"].ToString().ToLower().StartsWith(communicationSubject));
            }

            if (!string.IsNullOrEmpty(criteria.Get<string>("CommunicationType")))
            {
                string communicationType = criteria.Get<string>("CommunicationType").ToLower();
                results = results.Where(c => c["CommunicationType"] != null && c["CommunicationType"].ToString().ToLower().StartsWith(communicationType));
            }

            if (criteria.Get<long?>("SentDate") != null)
            {
                long? sentDate = criteria.Get<long?>("SentDate");
                results = results.Where(c => c["SentDate"] != null && Convert.ToDateTime(c["SentDate"]) >= Convert.ToDateTime(new BsonDateTime(sentDate.Value)) && Convert.ToDateTime(c["SentDate"]) <= Convert.ToDateTime(new BsonDateTime(sentDate.Value + 86400000)));
            }

            if (!string.IsNullOrEmpty(criteria.Get<string>("Recipients")))
            {
                string recipients = criteria.Get<string>("Recipients").ToLower();

                results = results.Where(c => c["RecipientsJSON"] != null && (c.Get<List<Contact>>("RecipientsJSON").Any(ct => (ct.FirstName != null && ct.FirstName.ToLower().StartsWith(recipients)) ||
                                                                                                                       (ct.LastName != null && ct.LastName.ToLower().StartsWith(recipients)) ||
                                                                                                                       (ct.FullName != null && ct.FullName.ToLower().StartsWith(recipients)) ||
                                                                                                                       (ct.LastName != null && ct.FirstName != null && string.Concat(ct.LastName.ToLower(), " ", ct.FirstName.ToLower()).Contains(recipients)) ||
                                                                                                                       (ct.Email != null && ct.Email.ToLower().StartsWith(recipients)))));
            }

            if (!string.IsNullOrEmpty(criteria.Get<string>("CreatedBy")))
            {
                string createdBy = criteria.Get<string>("CreatedBy").ToLower();
                results = results.Where(c => c["CreatedByName"] != null && c["CreatedByName"].ToString().ToLower().StartsWith(createdBy));
            }

            // sort by sent date
            results = results.OrderByDescending(r => r["SentDate"]);

            return results;
        }

       
    }
}

