﻿using System;

namespace AbsenceSoft.Logic.Cases
{
    public class MetricComparison
    {
        public double MaxValue { get; set; }
        public double ActualValue { get; set; }
        public double Difference { get; set; }

        public static MetricComparison Zero = new MetricComparison
        {
            MaxValue = 0,
            ActualValue = 0,
            Difference = 0
        };
    }
}