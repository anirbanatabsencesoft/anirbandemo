﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Rendering.Templating.MailMerge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
	[Serializable]
	public class TemplateRenderModel
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateRenderModel"/> class.
        /// </summary>
		public TemplateRenderModel() { }

        /// <summary>
        /// Gets or sets the communication that is being sent.
        /// </summary>
        /// <value>
        /// The communication.
        /// </value>
		public Communication Communication { get; set; }

        /// <summary>
        /// Gets or sets the case for this leave of absence.
        /// </summary>
        /// <value>
        /// The case.
        /// </value>
		public Case Case { get; set; }

        /// <summary>
        /// Gets or sets the active segment being used for tokenization, evaluation, etc.
        /// This is always the last segment created (for now).
        /// </summary>
        /// <value>
        /// The active segment.
        /// </value>
		public CaseSegment ActiveSegment { get; set; }

        /// <summary>
        /// Gets or sets the employee for this leave of absence.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
		public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the employer for this leave of absence.
        /// </summary>
        /// <value>
        /// The employer.
        /// </value>
		public Employer Employer { get; set; }

        /// <summary>
        /// Gets or sets the customer for this leave of absence.
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
		public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the admin contacts for the employee.
        /// </summary>
        /// <value>
        /// The admin contacts.
        /// </value>
		public List<EmployeeContact> AdminContacts { get; set; }

        /// <summary>
        /// Gets or sets the admin contacts for the employee.
        /// </summary>
        /// <value>
        /// The contacts.
        /// </value>
        public List<EmployeeContact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the type of the email replies.
        /// </summary>
        /// <value>
        /// The type of the email replies.
        /// </value>
         public EmailReplies? EmailRepliesType { get; set; }
        /// <summary>
        /// Gets or sets the active accommodation for this leave of absence.
        /// </summary>
        /// <value>
        /// The active accommodation.
        /// </value>
		public Accommodation ActiveAccommodation { get; set; }

        /// <summary>
        /// Gets or sets the user for this task
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
		public User CurrentUser { get; set; }

        /// <summary>
        /// Gets or sets the user employee history for this task
        /// </summary>
        /// <value>
        /// The current user employee.
        /// </value>
        public Employee CurrentUserEmployee { get; set; }

        /// <summary>
        /// Gets or sets the work schedules.
        /// </summary>
        /// <value>
        /// The work schedules.
        /// </value>
        public List<Schedule> WorkSchedule { get; set; }

        /// <summary>
        /// Gets the last date paperwork was receieved; if any.
        /// </summary>
        /// <value>
        /// The last paperwork received date.
        /// </value>
		public DateTime? LastPaperworkReceivedDate
		{
			get
			{
				if (Case == null) return null;
				var evt = Case.FindCaseEvent(Data.Enums.CaseEventType.PaperworkReceived);
				if (evt != null)
					return evt.EventDate;

				return null;
			}
		}

        /// <summary>
        /// Gets the last set return to work date; if any.
        /// </summary>
        /// <value>
        /// The return to work date.
        /// </value>
		public DateTime? ReturnToWorkDate
		{
			get
			{
				if (Case == null) return null;
				var evt = Case.FindCaseEvent(Data.Enums.CaseEventType.ReturnToWork);
				if (evt != null)
					return evt.EventDate;

				return null;
			}
		}

        public MailMergeData MailMergeData { get; set; }

        private List<Communication> comms = null;
        /// <summary>
        /// Gets or sets the prior communications.
        /// </summary>
        /// <value>
        /// The prior communications.
        /// </value>
        public List<Communication> PriorCommunications
        {
            get
            {
                if (comms != null) return comms;
                if (Case == null) return null;
                comms = Communication.AsQueryable().Where(c => c.CaseId == Case.Id).ToList();
                return comms;
            }
            set
            {
                comms = value;
            }
        }

        /// <summary>
        /// Gets the earliest date of exhaustion; if any.
        /// </summary>
        /// <value>
        /// The first exhaustion date.
        /// </value>
		public DateTime? FirstExhaustionDate
		{
			get
			{
				if (Case == null) return null;
				return Case.Segments.SelectMany(c => c.AppliedPolicies).Where(p => p.FirstExhaustionDate.HasValue).Select(p => p.FirstExhaustionDate).OrderBy(p => p).FirstOrDefault();
			}
		}

        /// <summary>
        /// Gets the illness or injury date; if any.
        /// </summary>
        /// <value>
        /// The illness or injury date.
        /// </value>
		public DateTime? IllnessOrInjuryDate
		{
			get
			{
				if (Case == null) return null;
				var evt = Case.FindCaseEvent(Data.Enums.CaseEventType.IllnessOrInjuryDate);
				if (evt != null)
					return evt.EventDate;

				return null;
			}
		}

        /// <summary>
        /// Gets the delivery date; if any.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
		public DateTime? DeliveryDate
		{
			get
			{
				if (Case == null) return null;
				var evt = Case.FindCaseEvent(Data.Enums.CaseEventType.DeliveryDate);
				if (evt != null)
					return evt.EventDate;

				return null;
			}
		}

        /// <summary>
        /// Gets the bonding start date; if any.
        /// </summary>
        /// <value>
        /// The bonding date.
        /// </value>
		public DateTime? BondingDate
		{
			get
			{
				if (Case == null) return null;
				var evt = Case.FindCaseEvent(Data.Enums.CaseEventType.BondingStartDate);
				if (evt != null)
					return evt.EventDate;

				return null;
			}
		}

        /// <summary>
        /// Gets the Approval Notification Sent Date; if any.
        /// </summary>
        /// <value>
        /// The approval notification sent date.
        /// </value>
		public DateTime? ApprovalNotificationSentDate
		{
			get
			{
				if (Case == null) return null;
				
				DateTime? dt = null;
				ListCriteria criteria = new ListCriteria();
				criteria.Set("CaseId", Case.Id);
				ListResults results = new CommunicationService().Using(c => c.CommunicationList(criteria));
				if(results.Results.Any(y => y.ExtensionData["Name"].ToString() == "Designation Notice Leave Approved"))
					dt = (DateTime?)results.Results.Last(y => y.ExtensionData["Name"].ToString() == "Designation Notice Leave Approved").ExtensionData["SentDate"];
				return dt;
			}
		}

        /// <summary>
        /// Gets the employee months worked.
        /// </summary>
        /// <value>
        /// The employee months worked.
        /// </value>
		public double? EmployeeMonthsWorked
		{
			get
			{
				if (Employee != null && Employee.ServiceDate.HasValue)
				{
					int compMonth = (DateTime.Today.Month + DateTime.Today.Year * 12) - (Employee.ServiceDate.Value.Month + Employee.ServiceDate.Value.Year * 12);
					double daysInEndMonth = (DateTime.Today - DateTime.Today.AddMonths(1)).Days;
					return compMonth + (Employee.ServiceDate.Value.Day - DateTime.Today.Day) / daysInEndMonth;
				}
				else return null;
			}
		}

        /// <summary>
        /// Gets the employee prior hours worked.
        /// </summary>
        /// <value>
        /// The employee prior hours worked.
        /// </value>
		public double? EmployeePriorHoursWorked
		{
			get
			{
				if (Case != null && Employee != null && Employee.PriorHours != null && Employee.PriorHours.Any())
				{
					PriorHours prioirHours = Employee.PriorHours.Where(h => h.AsOf <= Case.CreatedDate.Date).OrderByDescending(h => h.AsOf).FirstOrDefault();

					if (prioirHours != null)
						return prioirHours.HoursWorked;

				}
				
				return null;
			}
		}

        /// <summary>
        /// Gets the employee average minutes worked per week.
        /// </summary>
        /// <value>
        /// The employee average minutes worked per week.
        /// </value>
        public int? EmployeeAverageMinutesWorkedPerWeek
        {
            get
            {
                if (Case != null && Employee != null && Employee.MinutesWorkedPerWeek != null && Employee.MinutesWorkedPerWeek.Any())
                {
                    MinutesWorkedPerWeek workedPerWeek = Employee.MinutesWorkedPerWeek.Where(h => h.AsOf <= Case.CreatedDate.Date).OrderByDescending(h => h.AsOf).FirstOrDefault();
                    if (workedPerWeek != null)
                        return workedPerWeek.MinutesWorked;

                }

                return null;
            }
        }

        /// <summary>
        /// Gets FMLA policy in active segment
        /// </summary>
        /// <value>
        /// The FMLA policy.
        /// </value>
		public AppliedPolicy FMLAPolicy 
		{
			get
			{
				if (Case == null || ActiveSegment == null || ActiveSegment.AppliedPolicies == null)
					return null;

				return ActiveSegment.AppliedPolicies.Where(p => p.Policy.Code == "FMLA").FirstOrDefault();
			}
		}

        /// <summary>
        /// Gets a value indicating whether [FMLA rule worked12 month is not met].
        /// </summary>
        /// <value>
        /// <c>true</c> if [FMLA rule worked12 month is not met]; otherwise, <c>false</c>.
        /// </value>
		public bool FMLARuleWorked12MonthIsNotMet 
		{ 
			get
			{
				if (FMLAPolicy != null)
				{
					return FMLAPolicy.RuleGroups.SelectMany(r => r.Rules).Where(r => r.Fail == true && String.Compare(r.Rule.Name, "Worked 12 Months", true) == 0).Any();
				}
				else return false;		
			}
		}

        /// <summary>
        /// Gets a value indicating whether [FMLA rule1250 hours worked is not met].
        /// </summary>
        /// <value>
        /// <c>true</c> if [FMLA rule1250 hours worked is not met]; otherwise, <c>false</c>.
        /// </value>
		public bool FMLARule1250HoursWorkedIsNotMet
		{
			get
			{
				if (FMLAPolicy != null)
				{
					return FMLAPolicy.RuleGroups.SelectMany(r => r.Rules).Where(r => r.Fail == true && String.Compare(r.Rule.Name, "1,250 Hours Worked", true) == 0).Any();
				}
				else return false;
			}
		}

        /// <summary>
        /// Gets a value indicating whether [FMLA rule50 employeesin75 mile radius rule is not met].
        /// </summary>
        /// <value>
        /// <c>true</c> if [FMLA rule50 employeesin75 mile radius rule is not met]; otherwise, <c>false</c>.
        /// </value>
		public bool FMLARule50Employeesin75MileRadiusRuleIsNotMet
		{
			get
			{
				if (FMLAPolicy != null)
				{
					return FMLAPolicy.RuleGroups.SelectMany(r => r.Rules).Where(r => r.Fail == true && String.Compare(r.Rule.Name, "50 Employees in 75 Mile Radius Rule", true) == 0).Any();
				}
				else return false;
			}
		}


	}
}
