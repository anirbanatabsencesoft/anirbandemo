﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    [Serializable]
    public class PolicySummary
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the time used.
        /// </summary>
        /// <value>
        /// The time used.
        /// </value>
        public double TimeUsed { get; set; }

        /// <summary>
        /// Gets or sets the Minutes used.
        /// </summary>
        /// <value>
        /// The time used.
        /// </value>
        public double MinutesUsed { get; set; }

        /// <summary>
        /// Gets or sets the time remaining.
        /// </summary>
        /// <value>
        /// The time remaining.
        /// </value>
        public double TimeRemaining { get; set; }

        // These are easy conversion properties for displaying the time for intermittent
        //  cases as friendly hours + minutes for leave managers who don't want to do the
        //  math of what 0.23 work weeks equals, lol, lazy leave managers, if a computer can
        //  do it, so can you, but whatever!
        /// <summary>
        /// The friendly time display for total hours used from the raw minutes.
        /// </summary>
        /// <value>
        /// The hours used.
        /// </value>
        public string HoursUsed { get; set; }

        /// <summary>
        /// The friendly time display for total hours remaining from the raw minutes.
        /// </summary>
        /// <value>
        /// The hours remaining.
        /// </value>
        public string HoursRemaining { get; set; }

        /// <summary>
        /// Gets or sets the units.
        /// </summary>
        /// <value>
        /// The units.
        /// </value>
        public Unit Units { get; set; }

        // These get flattened out where we can, otherwise they designate 
        // a difference based on absence reason and are necessary
        public string AbsenceReason { get; set; }

        /// <summary>
        /// Gets or sets the absence reason identifier.
        /// </summary>
        /// <value>
        /// The absence reason identifier.
        /// </value>
        public string AbsenceReasonId { get; set; }

        /// <summary>
        /// Gets or sets the type of the policy.
        /// </summary>
        /// <value>
        /// The type of the policy.
        /// </value>
        public PolicyType PolicyType { get; set; }

        /// <summary>
        /// Gets or sets the whether the policy should exclude from time conversion.
        /// </summary>
        public bool ExcludeFromTimeConversion { get; set; }

    }
}
