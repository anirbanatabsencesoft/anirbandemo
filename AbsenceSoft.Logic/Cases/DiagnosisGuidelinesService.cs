﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Cases.Contracts;

namespace AbsenceSoft.Logic.Cases
{
    /// <summary>
    /// 
    /// 
    /// </summary>
    public class DiagnosisGuidelinesService : LogicService, ILogicService, IDiagnosisGuidelinesService
    {
        private static Object thisLock = new Object();
        public DiagnosisGuidelinesService()
        { 
        }

        /// <summary>
        /// Get Guidelines for specific Diagnosis
        /// </summary>
        /// <param name="medicalCode"></param>
        /// <returns></returns>
        public DiagnosisGuidelines GetDiagnosisGuidelines(string medicalCode)
        {
            DiagnosisGuidelines guidelines;

            using (new InstrumentationContext("DiagnosisGuidelinesService.GetDiagnosisGuidelines"))
            {
                //validate medical code
                if (string.IsNullOrWhiteSpace(medicalCode))
                    return null;

                //get guidelines 
                guidelines = DiagnosisGuidelinesService.GetThreadSafeDiagnosisGuidelines(medicalCode);
            }        

            
            return guidelines;
        }

        /// <summary>
        /// Returns the guideline getting the data from ODG
        /// </summary>
        /// <param name="medicalCode"></param>
        /// <returns></returns>
        private static DiagnosisGuidelines GetThreadSafeDiagnosisGuidelines(string medicalCode)
        {

            lock (thisLock)
            {
                DiagnosisGuidelines guidelines = DiagnosisGuidelines.AsQueryable().Where(d => d.Code == medicalCode).FirstOrDefault();

                //Validate expiration of guidelines data
                if (guidelines != null)
                {
                    //Validate ExpirationDate of data, remove if its expired
                    if (guidelines.ExpirationDate > DateTime.Today)
                    {
                        return guidelines;
                    }
                    else
                    {
                        guidelines.Delete();
                        guidelines = null;
                    }
                }

                //Get Diagnosis Guidelines from ODG
                ODGMedicalGuidelinesWebClient odgService = new ODGMedicalGuidelinesWebClient();
                guidelines = odgService.GetMedicalGuidelines(medicalCode);


                //Save guidelines data
                if (guidelines != null)
                    guidelines.Save();

                return guidelines;
            }
        }


        /// <summary>
        /// Get the Co-Morbidity Guideline
        /// </summary>
        /// <param name="caseId">Enter the case ID for which the Comorbidity guideline to be retrieved or save</param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public CoMorbidityGuideline GetCoMorbidityGuideline(string caseId, CoMorbidityGuideline.CoMorbidityArgs arg)
        {
            CoMorbidityGuideline guidelines;

            using (new InstrumentationContext("DiagnosisGuidelinesService.GetCoMorbidityGuideline"))
            {
                //validate medical code
                if (string.IsNullOrWhiteSpace(caseId) && arg == null)
                    return null;

                //get guidelines 
                guidelines = DiagnosisGuidelinesService.GetThreadSafeCoMorbidityGuideline(caseId, arg);
            }


            return guidelines;
        }

        /// <summary>
        /// Returns the guideline getting the data from ODG
        /// </summary>
        /// <param name="medicalCode"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static CoMorbidityGuideline GetThreadSafeCoMorbidityGuideline(string caseId, CoMorbidityGuideline.CoMorbidityArgs arg = null)
        {

            lock (thisLock)
            {
                Case _case = Case.GetById(caseId);

               if (_case == null || _case.Disability == null || _case.Status == Data.Enums.CaseStatus.Closed)
                    return null;

                //Get the JobActivity Level from associated Employee and apply to the case
                if (_case.Employee != null)
                    _case.Disability.EmployeeJobActivity = _case.Disability.EmployeeJobActivity ?? _case.Employee.JobActivity;

                using (CaseService caseService = new CaseService())
                {
                    //Get Diagnosis Guidelines from ODG
                    ODGMedicalGuidelinesWebClient odgService = new ODGMedicalGuidelinesWebClient();

                    //get the ODG Comorbidity guideline if requested
                    //return if data already exists in database
                    if (_case.Disability.CoMorbidityGuidelineDetail != null && (arg == null || !arg.ReCalculate))
                    {
                        var icd = GetDiagnosisCodeString(_case);

                        if (icd == _case.Disability.CoMorbidityGuidelineDetail.Args.ICDDiagnosisCodeString)
                        {
                            //Update Case should be only fired if anything has changed fro current state
                            //else don't waste a DB call. At the same time this will also solve the problem 
                            //of Lastupdated getting changed when opening case.
                            int jobActivityLevel = _case.Disability.EmployeeJobActivity != null ? (int)_case.Disability.EmployeeJobActivity : 0;
                            if (_case.Disability.CoMorbidityGuidelineDetail.Args.JobActivityLevel != jobActivityLevel)
                            {
                                _case.Disability.CoMorbidityGuidelineDetail.Args.JobActivityLevel = jobActivityLevel;
                                caseService.UpdateCase(_case);
                            }
                            return _case.Disability.CoMorbidityGuidelineDetail;
                        }
                        else
                        {
                            //re-calculate as there is a change in the diagnosis codes
                            arg = _case.Disability.CoMorbidityGuidelineDetail.Args;
                            arg.ReCalculate = true;
                            arg.ICDDiagnosisCodeString = icd;
                        }
                    }

                    //set default argument if co-morbidity is null
                    if (_case.Disability.CoMorbidityGuidelineDetail == null && arg == null)
                    {
                        var icd = GetDiagnosisCodeString(_case);

                        if (!string.IsNullOrWhiteSpace(icd))
                            arg = new CoMorbidityGuideline.CoMorbidityArgs() { ICDDiagnosisCodeString = icd, StateCode = _case.Employee.WorkState, JobActivityLevel = Convert.ToInt32(_case.Disability.EmployeeJobActivity) };

                    }

                    //get the data from ODG Service
                    if (arg != null && (_case.Disability.CoMorbidityGuidelineDetail == null || arg.ReCalculate))
                    {
                        //set age here
                        arg.Age = DateDiff(_case.Employee.DoB, option: "y");
                        arg.FullOutput = "Y";
                        arg.CountryCode = "US";

                        var result = odgService.GetComorbidityGuidelines(arg);
                        result.Args = arg;
                        _case.Disability.CoMorbidityGuidelineDetail = result;
                    }

                    _case.Disability.CoMorbidityGuidelineDetail.Args.JobActivityLevel = Convert.ToInt32(_case.Disability.EmployeeJobActivity);

                    //Save guidelines data
                    caseService.UpdateCase(_case);
                }
                
                return _case.Disability.CoMorbidityGuidelineDetail;
            }
        }

        /// <summary>
        /// Returns the ICD Codes based on primary and secondary diagnosis
        /// </summary>
        /// <param name="_case"></param>
        /// <returns></returns>
        private static string GetDiagnosisCodeString(Case _case)
        {
            try
            {
                var icd = _case.Disability.PrimaryDiagnosis.Code;
                if (_case.Disability.SecondaryDiagnosis != null && _case.Disability.SecondaryDiagnosis.Count > 0)
                {
                    icd += "," + String.Join(",", _case.Disability.SecondaryDiagnosis.Select(x => x.Code).ToArray());
                }

                return icd;
            }
            catch { throw new Exception("Expecting Primary Diagnosis Code"); }
        }


        /// <summary>
        /// Returns the datediff between two dates.
        /// If end date is missing, it's today's date.
        /// Options are d/m/y
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        private static int? DateDiff(DateTime? startDate, DateTime? endDate = null, string option="d")
        {
            try
            {
                if (endDate == null)
                    endDate = DateTime.Now;

                //get the diff
                TimeSpan? ts = endDate.Value - startDate;
                var days = ts.Value.Days;

                switch (option)
                {
                    case "m":
                        return Convert.ToInt32(Math.Round((decimal)(days / 30)));
                    case "y":
                        return Convert.ToInt32(Math.Round((decimal)(days / 365)));
                    default:
                        return days;
                }
            }
            catch { return null; }
        }

        /// <summary>
        /// Get by ICD9 Code
        /// </summary>
        /// <param name="code">The icd9 code</param>
        // <returns></returns>
        public DiagnosisCode GetByCode(string code)
        {
            DiagnosisCode rtVal = DiagnosisCode.AsQueryable().Where(i => i.Code.Equals(code)).FirstOrDefault();

            return rtVal ?? new DiagnosisCode();
        }
    }
}
