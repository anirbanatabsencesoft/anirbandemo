﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using System.Text;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson.Serialization;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Logic.Cases
{
    public partial class CaseService : LogicService
    {

        /// <summary>
        /// Take the work schedule based on the entitlement unit (like work weeks), and multiply remaining entitlement * units,
        /// i.e. 0.0 work weeks * 40h/w = 0h
        /// </summary>
        /// <param name="parForThis">The policy absence reason for this method to calculate against</param>
        /// <param name="projectedStartDate">The projected start date.</param>
        /// <param name="ps">The ps.</param>
        /// <param name="loa">The loa.</param>
        /// <returns></returns>
        double ConvertUnitsToMinutesBasedOnRemaining(PolicyAbsenceReason parForThis, DateTime projectedStartDate, PolicySummary ps, LeaveOfAbsence loa)
        {
            EntitlementType et = parForThis.EntitlementType.Value;
            if (et != EntitlementType.WorkDays && et != EntitlementType.FixedWorkDays && et != EntitlementType.WorkingMonths && et != EntitlementType.WorkWeeks)
            {
                ps.ExcludeFromTimeConversion = true;
                return 0;
            }
            double duration = parForThis.Entitlement.Value;
            if (duration == 0d)
                return duration;

            //We need to get the the startDate and End of the Week , so we can fetch the WorkSchedule for that Emp
            /* Projected out schedule from start to end date */
            DateTime startDate = DateTime.UtcNow;
            DateTime endDate = DateTime.UtcNow;
          
            switch (et)
            {
                case EntitlementType.WorkDays:
                    startDate = projectedStartDate;
                    endDate = projectedStartDate;
                    break;
                case EntitlementType.WorkWeeks:
                    startDate = projectedStartDate.GetFirstDayOfWeek();
                    endDate = projectedStartDate.GetLastDayOfWeek();
                    break;
                case EntitlementType.WorkingMonths:
                    startDate = projectedStartDate.GetFirstDayOfMonth();
                    endDate = projectedStartDate.GetLastDayOfMonth();
                    break;
            }
          
            //Get the Projected Schedule
            List<Time> projectedSchedule = loa.MaterializeSchedule(startDate, endDate, false);
            //Convert it to total Hours
            int? totalMinutesInSchedule = projectedSchedule.Sum(a => a.TotalMinutes);
            //We already have all the Details , Get the TimeRemaining & TimeUsed and multiply it with
            //the minutes derived from Schedule and We get the total entitlement in minutes , now we have 
            //MinutesUsed available with us , so deduct it and send it back now
            //Get the Remaining minutes
            if (ps.TimeRemaining > 0)
            {
                double remainingMinutes = ((ps.TimeRemaining + ps.TimeUsed) * totalMinutesInSchedule.Value) - ps.MinutesUsed;
                if (remainingMinutes < 0)
                {
                    remainingMinutes = Math.Round(ps.TimeRemaining * (ps.MinutesUsed / ps.TimeUsed));
                }
                // There are two more thing we have to consider: 
                //1. Variable work schedule: there may be less hours used up for approved leave on some dates
                //2. Holidays: If there were Hoildays in between approved leaves, then those holiday hours need to be reduced from above calculated remaining minutes.
                //We are saying to handling Variable schedule but not checking , if the employee really has or not
                ScheduleType activeScheduleType = ScheduleType.Weekly;
                if (loa.WorkSchedule.Any())
                {
                    activeScheduleType = loa.WorkSchedule.OrderBy(o => o.StartDate).FirstOrDefault().ScheduleType;
                }
                if (activeScheduleType != ScheduleType.Weekly && (ps.TimeUsed * totalMinutesInSchedule.Value) > ps.MinutesUsed)
                {
                    remainingMinutes -= (ps.TimeUsed * totalMinutesInSchedule.Value) - ps.MinutesUsed;
                }

                //Now we have remainingMinutes , send back
                return remainingMinutes;
            }
            else
            {
                return 0;
            }

        }

        // for now this single class converts the usage into the pieces we need
        // for entitlement and elimination (at some point you may want to make a base class, and then
        // inherited versions of these that can handle the different types and a factory to create them
        // for right now everything is crammed into this one type) (I'm mostly thinking when we 
        // implement calendar days / calendar months that will be the way to go)
        private class CalcUsage
        {
            public double TotalRequested { get; private set; }
            public double DeniedCuzOverTheLimit { get; private set; }
            public double NewTotal { get; private set; }
            public bool WentOverTheLimit { get; private set; }

            public bool ReachedTheLimit { get; private set; }

            public string Error { get; private set; }

            public CalcUsage(AppliedPolicyUsage workUsage, PolicyUsageTotals put, EntitlementType et, double maxAllowed, AppliedPolicy ap,
                bool denied, CaseType segmentType, int gapUsageCount = 0,DateTime? dtSampleDate =null, LeaveOfAbsence loa = null,int dayscount=0,
                params string[] deniedWhy)
            {
                

                TotalRequested = workUsage.MinutesUsed;
                NewTotal = workUsage.MinutesUsed;

                // doubles are not fun just because: .2 + .2 +.2 +.2 +.2 +.2 +.2 +.2 +.2 +.2 = 1.9999999999999998
                // I guess that is sort of like 2, kinda, maybe
                // and 4 places because on a 2000 hour year 1 hour = .0005 and if you round 1.9999999999999998 to 6 places you get 1.999998
                //double percentUsed = Math.Round(put.PercentUsed(et),4);
                decimal percentUsed = 0;
                if (denied)
                {
                    if (deniedWhy.Length == 0)
                    {
                        percentUsed = Math.Round(put.PercentUsed(et, AdjudicationStatus.Denied), 8);
                    }
                    else
                    {
                        foreach (string adr in deniedWhy)
                            percentUsed += Math.Round(put.PercentUsed(et, adr), 8);
                    }
                }
                else
                {
                    percentUsed = put.PercentUsed(et, AdjudicationStatus.Pending, true) +
                        put.PercentUsed(et, AdjudicationStatus.Approved,ap);
                }

                string denialReason = AdjudicationDenialReason.Other;
                if (deniedWhy.Length > 0)
                    denialReason = deniedWhy[0];

                decimal allowedMax = Convert.ToDecimal(maxAllowed);
                decimal currentUseAsPercent = Convert.ToDecimal(workUsage.DaysUsage(et));
                double workDaysInBaseUnit = workUsage[et].BaseUnit;          // how many days of work are in the time frame we are measuring e.g. days in week, days in month
                decimal remaining = Math.Round(allowedMax - percentUsed, 10);
                decimal whereWeAt = 0;
                //if (dtSampleDate.HasValue)
                //{
                //    System.Diagnostics.Debug.WriteLine(dtSampleDate+","+ percentUsed + "," + currentUseAsPercent);
                //}
                //In case when daily usage more than remaining then roundup
                if ((segmentType != CaseType.Intermittent) && remaining != 0 && currentUseAsPercent > remaining && denialReason == AdjudicationDenialReason.EliminationPeriod)
                    whereWeAt = allowedMax;
                else
                {
                    if (et.CalendarTypeFromFirstUse() && gapUsageCount > 0)
                    {
                        whereWeAt = CalculateUsageWithGaps(workUsage, put, ap, gapUsageCount, percentUsed, currentUseAsPercent); 
                    }
                    else
                    {
                        whereWeAt = Math.Round(percentUsed + currentUseAsPercent, 10);
                    }
                }

                DeniedCuzOverTheLimit = 0;

                // set the flag if we come out even
                if (!et.UnlimitedType() && whereWeAt == allowedMax)
                    ReachedTheLimit = true;                

                if (segmentType != CaseType.Intermittent && ap.PolicyReason.EntitlementType.Value.CalendarType())
                {
                    double? daysInPeriod;
                    // calculate policy exhausted after ? days
                    switch (et)
                    {
                        case EntitlementType.CalendarDays:
                        case EntitlementType.CalendarDaysFromFirstUse:
                            daysInPeriod = ap.PolicyReason.Entitlement.Value;
                            break;
                        case EntitlementType.CalendarWeeks:
                        case EntitlementType.CalendarWeeksFromFirstUse:
                            daysInPeriod = ap.PolicyReason.Entitlement.Value * 7;
                            break;
                        case EntitlementType.CalendarMonths:
                        case EntitlementType.CalendarMonthsFromFirstUse:
                            daysInPeriod = ap.PolicyReason.Entitlement.Value * 30;
                            break;
                        case EntitlementType.CalendarYears:
                        case EntitlementType.CalendarYearsFromFirstUse:
                            daysInPeriod = ap.PolicyReason.Entitlement.Value * 365.25;
                            break;
                        default:
                            daysInPeriod = 1;
                            break;
                    }

                    if (dayscount > daysInPeriod)
                    {
                        WentOverTheLimit = true;
                    }
                }

                // our units to apply equals 100 percent of our benefit
                // so check to make sure we are not about to go over our benefit
                if (!et.UnlimitedType() && whereWeAt > allowedMax)
                {
                    // works like this. What percent are we over (12.1 weeks - 12 weeks = .1 weeks)
                    // how many days make up the work week (5) and many hours in the work day 5
                    // so they get half a day off or 2.5 hours (why 8 decimal places, because that's where it works as a double)
                    if (segmentType != CaseType.Intermittent)
                    {
                        TotalRequested = 0;
                    }
                    else
                    {
                        if (et != EntitlementType.FixedWorkDays)
                        {
                            int? totalMinutesInSchedule = 0;
                            if (loa != null)
                            {
                                DateTime startDate = DateTime.UtcNow;
                                DateTime endDate = DateTime.UtcNow;

                                switch (et)
                                {
                                    case EntitlementType.WorkDays:
                                        startDate = workUsage.DateUsed;
                                        endDate = workUsage.DateUsed;
                                        break;
                                    case EntitlementType.WorkWeeks:
                                        startDate = workUsage.DateUsed.GetFirstDayOfWeek();
                                        endDate = workUsage.DateUsed.GetLastDayOfWeek();
                                        break;
                                    case EntitlementType.WorkingMonths:
                                        startDate = workUsage.DateUsed.GetFirstDayOfMonth();
                                        endDate = workUsage.DateUsed.GetLastDayOfMonth();
                                        break;
                                }

                                //Get the Projected Schedule
                                List<Time> projectedSchedule = loa.MaterializeSchedule(startDate, endDate, false);
                                //Convert it to total Hours
                                totalMinutesInSchedule = projectedSchedule.Sum(a => a.TotalMinutes);
                                DeniedCuzOverTheLimit = Math.Round(Convert.ToDouble(whereWeAt - allowedMax) * totalMinutesInSchedule.Value, 8);
                            }
                            else
                            {
                                DeniedCuzOverTheLimit = Math.Round(Convert.ToDouble(whereWeAt - allowedMax) * workDaysInBaseUnit * workUsage.MinutesInDay, 8);
                                NewTotal = TotalRequested - DeniedCuzOverTheLimit;
                            }
                        }
                        else
                        {
                            DeniedCuzOverTheLimit = Convert.ToDouble(whereWeAt - allowedMax);
                            NewTotal = DeniedCuzOverTheLimit;
                        }
                    }
                    ////////////////////////////////////////////////if (DeniedCuzOverTheLimit >= TotalRequested)
                    ////////////////////////////////////////////////    DeniedCuzOverTheLimit = TotalRequested;
                    
                    WentOverTheLimit = true;
                }
            }

            /// <summary>
            /// Interchange allocation hours of a holiday and a regular day to allow approval of leaves 
            /// </summary>
            /// <param name="put"></param>
            /// <param name="holidays"></param>
            /// <param name="policyTotals"></param>
            /// <param name="resetPoint"></param>
            /// <returns></returns>
            public bool AdjustCalcUsageForHolidays(PolicyUsageTotals put, List<DateTime>  holidays, PolicyUsageTotals policyTotals, DateTime resetPoint)
            {
                if (!put.Allocated.Any())
                {
                    return false;
                }

                var dateToRegain = put.Allocated.OrderBy(a => a.DateUsed).FirstOrDefault().DateUsed.AddDays(-1);
                var holidayInCurrentWeek = holidays.Any(h => AreInSameWeek(h, resetPoint));
                var holidayInFirstWeek = holidays.Any(h => AreInSameWeek(h, dateToRegain));
                
                if (holidayInCurrentWeek && holidayInFirstWeek && policyTotals.Allocated.Any(a => a.DateUsed == dateToRegain))
                {
                    var minutesUsed = policyTotals.Allocated.FirstOrDefault(a => a.DateUsed == dateToRegain).MinutesUsed;
                    if(minutesUsed <= 0d)
                    {
                        put.Allocated[0] = policyTotals.Allocated.FirstOrDefault(a => a.DateUsed == dateToRegain);
                        return true;
                    }
                }
                return false;
            }

            private bool AreInSameWeek(DateTime firstDate, DateTime secondDate)
            {
                return firstDate.AddDays(-(int)firstDate.DayOfWeek) == secondDate.AddDays(-(int)secondDate.DayOfWeek);
            }

            /// <summary>
            /// Calculate policy exhaustion including gaps(any) used.
            /// </summary>
            /// <param name="workUsage">AppliedPolicyUsage</param>
            /// <param name="put">PolicyUsageTotals</param>
            /// <param name="ap">AppliedPolicy</param>
            /// <param name="gapUsageCount">gapCount</param>
            /// <param name="percentUsed">percentUsed</param>
            /// <param name="currentUseAsPercent">currentUseAsPercent</param>
            /// <returns></returns>
            private decimal CalculateUsageWithGaps(AppliedPolicyUsage workUsage, PolicyUsageTotals put, AppliedPolicy ap, int? gapUsageCount, decimal percentUsed, decimal currentUseAsPercent)
            {
                decimal whereWeAt = 0;

                // get first usage                
                var calcFirstUsage = put.Allocated.OrderBy(p => p.DateUsed).FirstOrDefault(p => p.Determination == AdjudicationStatus.Approved || p.Determination == AdjudicationStatus.Pending);

                if (calcFirstUsage != null && workUsage.DateUsed > DateTime.MinValue)
                {
                    // calculate policy exhausted after ? days
                    double? daysToAdd = ap.PolicyReason.Entitlement ?? ap.PolicyReason.EntitlementType.Value.ToUnitDays(ap.PolicyReason.Entitlement.Value);

                    // calculate last date
                    DateTime calcLastUsage = calcFirstUsage.DateUsed.AddDays(daysToAdd.Value);

                    if (workUsage.DateUsed >= calcLastUsage)
                    {
                        // add gap percent used
                        whereWeAt = Math.Round(percentUsed + currentUseAsPercent + Convert.ToDecimal(gapUsageCount.Value * currentUseAsPercent), 10);
                    }       
                }                

                if (whereWeAt == 0)
                {
                    whereWeAt = Math.Round(percentUsed + currentUseAsPercent, 10); // calculate default
                }                

                return whereWeAt;
            }
        }
    }
}
