﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.DataExport;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Cases
{
    public class ExportService : LogicService, ILogicService
    {
        public ExportService()
        {

        }

        /// <summary>
        /// This method escap some chars from string
        /// </summary>
        /// <param name="s">string to be escaped from some chars</param>
        /// <returns>returns string with escaped chars</returns>
        internal static string Escape(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        private const string Comma = ",";
        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        /// <summary>
        /// This method is used to write array of data to csv file
        /// </summary>
        /// <param name="filePath">Path of file in which you want to write</param>
        /// <param name="arrData">written data</param>
        public void DoExport(string filePath, string[] arrData)
        {
            MemoryStream ms = new MemoryStream();
            StringBuilder sbData = new StringBuilder();

            for (int i = 0; i < arrData.Length; i++)
            {
                sbData.Append(Escape(arrData[i]));
                if (i != arrData.Length - 1)
                    sbData.Append(Comma);
            }

            StreamWriter sw = File.AppendText(filePath);
            sw.WriteLine(sbData.ToString());
            sw.Close();
        }

        public string UploadFileToAmazon(string filePath, string customerId, string employerId)
        {
            try
            {
                using (FileService client = new FileService())
                    return client.UploadFile(filePath, Settings.Default.S3BucketName_Exports, customerId, employerId);
            }
            catch (Exception ex)
            {
                Log.Error("Exception: " + ex.Message);
                throw;
            }
            finally
            {
                try
                {
                    if (File.Exists(filePath))
                        File.Delete(filePath);
                }
                catch { }
            }
        }

        public List<DataExport> GetTotalLinks(string employerId)
        {
            List<DataExport> data = DataExport.AsQueryable().Where(m => m.EmployerId == employerId && m.Status == ExportStatus.Complete).ToList();
            return data;
        }

        public string GetExportLink(string key)
        {
            try
            {
                var fileKey = DataExport.AsQueryable().FirstOrDefault(m => m.Id == key).FileReference;

                using (FileService client = new FileService())
                    return client.GetS3DownloadUrl(fileKey, Settings.Default.S3BucketName_Exports);
            }
            catch (Exception amazonS3Exception)
            {
                Log.Error("Error occurred. Exception: " + amazonS3Exception.ToString());
                throw;
            }
        }

        public bool Zip(string directory, string password, string dataExportId, string zipSaveFilePath)
        {
            try
            {
                IEnumerable<string> paths = Directory.GetFiles(directory);
                string[] includeDirectories = Directory.GetDirectories(directory);
                using (ZipFile zip = new ZipFile())
                {
                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.Level9;
                    zip.Password = password;
                    zip.Encryption = EncryptionAlgorithm.WinZipAes256;

                    foreach (string dir in includeDirectories)
                    {
                        zip.AddDirectory(dir, "/Attachments/");
                    }

                    zip.AddFiles(paths, false, "");

                    zip.Save(zipSaveFilePath);
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }

        public DataExport CreateExportRecord(User currentUser, string employerId)
        {
            if (!currentUser.HasEmployerAccess(employerId, Permission.ExportData))
                throw new AbsenceSoftException("User does not have access to create a data export request for this employer.");

            return new DataExport()
            {
                EmployerId = employerId,
                CustomerId = currentUser.CustomerId,
                Status = ExportStatus.Pending
            };
        }

        public void Update(DataExport export)
        {
            using (new InstrumentationContext("CustomerService.Update"))
            {
                export.Save();
            }
        } // Update

        public string GetPassword(string dataExportId)
        {
            DataExport de = DataExport.AsQueryable().Where(o => o.Id == dataExportId).FirstOrDefault();
            return Crypto.Decrypt(de.ZipFilePassword.Encrypted);
        }
    }
}
