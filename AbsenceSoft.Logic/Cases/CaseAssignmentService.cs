﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace AbsenceSoft.Logic.Cases
{
    public class CaseAssignmentService : LogicService
    {
        public CaseAssignmentService() { }
        public CaseAssignmentService(User currentUser)
        {
            this.CurrentUser = currentUser;
        }

        public User CurrentUser { get; set; }

        public void DeleteCaseAssignmentType(CaseAssignmentType assigneeType)
        {

            if (assigneeType.CustomerId != CurrentUser.CustomerId)
                throw new AbsenceSoftException("User does not have permission to delete type for specified customer");

            if (!CurrentUser.HasEmployerAccess(assigneeType.EmployerId))
                throw new AbsenceSoftException("User does not have permission to delete type for specified employer");

            assigneeType.Delete();
        }

        public CaseAssignmentType UpdateCaseAssignmentType(CaseAssignmentType assigneeType)
        {
            if (assigneeType.CustomerId != CurrentUser.CustomerId)
                throw new AbsenceSoftException("User does not have permission to add type for specified customer");

            if (!CurrentUser.HasEmployerAccess(assigneeType.EmployerId))
                throw new AbsenceSoftException("User does not have permission to add type for specified employer");

            assigneeType.Save();
            return assigneeType;
        }

        /// <summary>
        /// Gets a single case assignee type 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="caseAssignmentTypeId"></param>
        /// <returns></returns>
        public CaseAssignmentType GetCaseAssignmentType(string customerId, string caseAssignmentTypeId)
        {
            using (new InstrumentationContext("CaseAssignmentService.GetCaseAssigneeType"))
            {
                if (string.IsNullOrEmpty(caseAssignmentTypeId))
                    throw new ArgumentNullException("caseAssignmentTypeId");

                return CaseAssignmentType.GetById(caseAssignmentTypeId);
            }
        }

        /// <summary>
        /// Gets a list of Case Assignee types for the specified customer and optional employer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<CaseAssignmentType> GetCaseAssignmentTypes(string customerId, string employerId = null)
        {
            using (new InstrumentationContext("CaseAssignmentService.GetCaseAssigneeTypes"))
            {
                if (string.IsNullOrEmpty(customerId))
                    throw new ArgumentNullException("customerId");

                List<CaseAssignmentType> assigneeTypes = new List<CaseAssignmentType>();

                if (CurrentUser.CustomerId != customerId || !CurrentUser.HasEmployerAccess(employerId))
                    return assigneeTypes;

                assigneeTypes = CaseAssignmentType.AsQueryable().Where(cat => cat.CustomerId == customerId).ToList();

                if (!string.IsNullOrEmpty(employerId))
                    assigneeTypes = assigneeTypes.Where(cat => cat.EmployerId == employerId || cat.EmployerId == null).ToList();

                return assigneeTypes;
            }
        }

        /// <summary>
        /// Gets a list of users that can be assigned to the case
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        public List<User> GetUsersForCase(Case theCase)
        {
            List<string> caseAssigneeRoles = Role.AsQueryable().Where(r => r.CustomerId == theCase.CustomerId && r.Permissions.Contains("CaseAssignee")).ToList().Select(r => r.Id).ToList();
            return User.AsQueryable().Where(u => u.Id != theCase.AssignedToId && u.CustomerId == theCase.CustomerId && u.Roles.ContainsAny(caseAssigneeRoles)).ToList();
        }

        /// <summary>
        /// Assigns a user to a specific role for the case
        /// </summary>
        /// <param name="theCase">The Case</param>
        /// <param name="userId">The id of the user to be assigned to the case</param>
        /// <param name="caseAssignmentTypeId">The id of the role the user has for this case</param>
        /// <returns></returns>
        public Case AssignUserToCaseType(Case theCase, string userId, string caseAssignmentTypeId)
        {
            using (new InstrumentationContext("CaseAssignmentService.AssignUserToCaseRole"))
            {
                if (theCase == null)
                    throw new ArgumentNullException("theCase");

                if (string.IsNullOrEmpty(userId))
                    throw new ArgumentNullException("userId");

                if (string.IsNullOrEmpty(caseAssignmentTypeId))
                    throw new ArgumentNullException("caseAssignmentTypeId");

                User u = User.GetById(userId);
                if (u.CustomerId != CurrentUser.CustomerId)
                    throw new AbsenceSoftException("Cannot assign a user that does not belong to the current customer");

                if(theCase.AssignedUsers.Any(au => au.AssignedToId == userId))
                    throw new AbsenceSoftException("This user has already been assigned to this case");

                CaseAssignmentType role = CaseAssignmentType.GetById(caseAssignmentTypeId);
                if (role.CustomerId != CurrentUser.CustomerId)
                    throw new AbsenceSoftException("Cannot assign a role that does not belong to the current customer");

                if (role.MaxAllowedPerCase.HasValue && theCase.AssignedUsers.Where(p => p.AssignmentType.Id == role.Id).Count() >= role.MaxAllowedPerCase.Value)
                    throw new AbsenceSoftException("Max number of users for this role on this case has already been reached");

                Assignee assignee = new Assignee()
                {
                    AssignedToId = userId,
                    DisplayName = u.DisplayName,
                    AssignmentType = role,
                    AssignedById = CurrentUser.Id,
                    AssignedOn = DateTime.UtcNow
                };

                if (theCase.AssignedUsers == null)
                    theCase.AssignedUsers = new List<Assignee>();

                theCase.AssignedUsers.Add(assignee);

                return theCase;
            }

        }

        /// <summary>
        /// Removes a single user from the list of assigned users on a case
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Case RemoveUserFromCase(Case theCase, string userId)
        {
            using (new InstrumentationContext("CaseAssignmentService.RemoveUserFromCase"))
            {
                if (theCase == null)
                    throw new ArgumentNullException("theCase");

                if (string.IsNullOrEmpty(userId))
                    throw new ArgumentNullException("userId");
                
                if(theCase.AssignedUsers == null)
                {
                    theCase.AssignedUsers = new List<Assignee>();
                }
                else
                {
                    theCase.AssignedUsers = theCase.AssignedUsers.Where(p => p.AssignedToId != userId).ToList();
                }
                
                return theCase;
            }
        }

        /// <summary>
        /// Creates the 7 default roles for a customer.  Should only be called as part of the signup process
        /// </summary>
        /// <param name="customerId"></param>
        public void CreateDefaultCaseAssigneeTypesForCustomer(string customerId)
        {
            if (CurrentUser.CustomerId != customerId)
                throw new AbsenceSoftException("User does not have permission to add types for specified customer");

            using (new InstrumentationContext("CaseASsignmentService.CreateDefaultCaseAssigneeTypesForCustomer"))
            {
                CreateDefaultCaseAssigneeType("STD Case Manager", customerId).Save();
                CreateDefaultCaseAssigneeType("WC Case Manager", customerId).Save();
                CreateDefaultCaseAssigneeType("ADA Case Manager", customerId).Save();
                CreateDefaultCaseAssigneeType("Backup Case Manager", customerId).Save();
                CreateDefaultCaseAssigneeType("Clerical Support", customerId).Save();
                CreateDefaultCaseAssigneeType("Client Services Representative", customerId).Save();
            }

        }

        private CaseAssignmentType CreateDefaultCaseAssigneeType(string name, string customerId)
        {
            return new CaseAssignmentType()
            {
                Name = name,
                CustomerId = customerId,
                CreatedById = User.DefaultUserId
            };
        }


    }
}
