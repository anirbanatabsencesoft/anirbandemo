﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Logic.Chart
{
    public partial class ChartService : LogicService
    {
        public List<object[]> GetCaseLoadData(VisibilityListType viewType)
        {
            // Define our query
            DateTime startDate = DateTime.UtcNow.ToMidnight();
            DateTime endDate = startDate.AddDays(1);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters(employeeIdFieldName: "EmployeeId"));
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            //ands.Add(Case.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)));
            //ands.Add(Case.Query.LT(e => e.CreatedDate, new BsonDateTime(endDate)));
            ands.Add(Case.Query.EQ(c => c.Status, CaseStatus.Open));
            if (viewType == VisibilityListType.Individual)
                ands.Add(Case.Query.EQ(e => e.AssignedToId, CurrentUser.Id));
            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var c = this;");
            if (viewType == VisibilityListType.Individual)
            {
                map.AppendLine("    var ret = [c.Reason.Name, 1];");
                map.AppendLine("    emit(c.Reason._id, { val: ret });");
            }
            else
            {
                map.AppendLine("    var ret = [c.AssignedToName, 1];");
                map.AppendLine("    emit(c.AssignedToId, { val: ret });");
            }
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            List<object[]> data = new List<object[]>();
            var response = Case.Repository.Collection.MapReduce(mapReduce);
            if (response.Ok && string.IsNullOrWhiteSpace(response.ErrorMessage))
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        data.Add(rData);
                }

            return data;
        }

        public List<object[]> GetOpenToDosData(VisibilityListType viewType)
        {
            // Define our query
            DateTime startDate = DateTime.UtcNow.ToMidnight();
            DateTime endDate = startDate.AddDays(1);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters(employeeIdFieldName: "EmployeeId"));
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            //ands.Add(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)));
            //ands.Add(ToDoItem.Query.LT(e => e.CreatedDate, new BsonDateTime(endDate)));
            ands.Add(ToDoItem.Query.NotIn(t => t.Status, new ToDoItemStatus[] { ToDoItemStatus.Cancelled, ToDoItemStatus.Complete }));
            if (viewType == VisibilityListType.Individual)
                ands.Add(ToDoItem.Query.EQ(e => e.AssignedToId, CurrentUser.Id));
            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            if (viewType == VisibilityListType.Individual)
                map.AppendLine("    var ret = [types[todo.ItemType.toString()], 1];");
            else
                map.AppendLine("    var ret = [todo.AssignedToName, 1];");
            if (viewType == VisibilityListType.Individual)
                map.AppendLine("    emit(todo.ItemType, { val: ret });");
            else
                map.AppendLine("    emit(todo.AssignedToId, { val: ret });");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            if (viewType == VisibilityListType.Individual)
                mapReduce.Scope = new ScopeDocument(new Dictionary<string, object>() {
                    { "types", Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToDictionary(t => ((int)t).ToString(), t => t.ToString().SplitCamelCaseString()) }
                });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            List<object[]> data = new List<object[]>();
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            if (response.Ok && string.IsNullOrWhiteSpace(response.ErrorMessage))
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        data.Add(rData);
                }

            return data;
        }

        public List<object[]> GetOverdueToDosData(VisibilityListType viewType)
        {
            // Define our query
            DateTime startDate = DateTime.UtcNow.ToMidnight();
            DateTime endDate = startDate.AddDays(1);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters(employeeIdFieldName: "EmployeeId"));
            // Must match a range for dates, not just start and end within that range, it's
            //  any leave that intersects the date range selected by the user.
            ands.Add(ToDoItem.Query.Or(
                ToDoItem.Query.LT(e => e.DueDate, new BsonDateTime(startDate)),
                ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Overdue)
            ));
            //ands.Add(ToDoItem.Query.GTE(e => e.CreatedDate, new BsonDateTime(startDate)));
            //ands.Add(ToDoItem.Query.LT(e => e.CreatedDate, new BsonDateTime(endDate)));
            if (viewType == VisibilityListType.Individual)
                ands.Add(ToDoItem.Query.EQ(e => e.AssignedToId, CurrentUser.Id));
            IMongoQuery query = ands.Count > 0 ? ToDoItem.Query.And(ands) : null;

            // Define our Map function
            StringBuilder map = new StringBuilder();
            map.AppendLine("function() {");
            map.AppendLine("    var todo = this;");
            map.AppendLine("    var ret = [types[todo.ItemType.toString()], 1];");
            map.AppendLine("    emit(todo.ItemType, { val: ret });");
            map.AppendLine("}");

            // Define our reduce function
            StringBuilder reduce = new StringBuilder();
            reduce.AppendLine("function(key, values) {");
            reduce.AppendLine(" var result = { val: [values[0].val[0]] };");
            reduce.AppendLine(" values.forEach(function(value) {");
            reduce.AppendLine("     for (var i=1; i<value.val.length; i++) {");
            reduce.AppendLine("         result.val[i] = result.val[i] || 0;");
            reduce.AppendLine("         result.val[i] += (value.val[i] || 0);");
            reduce.AppendLine("     }");
            reduce.AppendLine(" });");
            reduce.AppendLine(" return result;");
            reduce.AppendLine("}");

            // Define our map reduce arguments
            MapReduceArgs mapReduce = new MapReduceArgs();
            mapReduce.OutputMode = MapReduceOutputMode.Inline;
            mapReduce.Query = query;
            mapReduce.Scope = new ScopeDocument(new Dictionary<string, object>() {
                { "types", Enum.GetValues(typeof(ToDoItemType)).OfType<ToDoItemType>().OrderBy(t => (int)t).ToDictionary(t => ((int)t).ToString(), t => t.ToString().SplitCamelCaseString()) }
            });
            mapReduce.MapFunction = new BsonJavaScript(map.ToString());
            mapReduce.ReduceFunction = new BsonJavaScript(reduce.ToString());

            List<object[]> data = new List<object[]>();
            var response = ToDoItem.Repository.Collection.MapReduce(mapReduce);
            if (response.Ok && string.IsNullOrWhiteSpace(response.ErrorMessage))
                foreach (var r in response.InlineResults)
                {
                    object[] rData = r.GetValue("value").AsBsonDocument.GetValue("val").AsBsonArray.Select(b => b.GetRawValue<object>()).ToArray();
                    if (rData != null)
                        data.Add(rData);
                }

            return data;
        }
    }
}

