﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Users
{
    public static class UserExtensions
    {
        /// <summary>
        /// Perform an action when a user does NOT have
        /// specified permission
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <param name="action"></param>
        /// <param name="otherwise"></param>
        public static void DoWithoutPermission(this User user, Permission permission, Action action, Action otherwise = null)
        {
            if (!user.HasPermission(permission))
            {
                action();
            }
            else
            {
                if (otherwise != null)
                {
                    otherwise();
                }
            }
        }

        /// <summary>
        /// Perform an action when a user does have
        /// specified permission
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <param name="action"></param>
        /// <param name="otherwise"></param>
        public static void DoWithPermission(this User user, Permission permission, Action action, Action otherwise = null)
        {
            if (user.HasPermission(permission))
            {
                action();
            }
            else
            {
                if (otherwise != null)
                {
                    otherwise();
                }
            }
        }

        public static bool HasPermission(this User user, Permission permission)
        {
            List<string> userPermissions;
            if (Employer.Current != null)
                userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
            else if (User.Current != null)
                userPermissions = User.Permissions.ProjectedPermissions.ToList();
            else
                userPermissions = User.Permissions.GetPermissions(user);

            return userPermissions.Any(m => m == permission);
        }
    }
}
