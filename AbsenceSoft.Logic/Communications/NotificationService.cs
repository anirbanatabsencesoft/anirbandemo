﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Notification;
using AT.Common.Core.NotificationEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Logic.Communications
{
    public class NotificationService : LogicService
    {
        public NotificationService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Saves to do notification for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="frequency">The frequency.</param>
        public void SaveToDoNotificationForUser(string userId, NotificationFrequencyType? frequency)
        {
            NotificationConfiguration todoConfiguration = GetConfigurationForUser(userId, NotificationCategory.ToDo);

            // They have elected not to have any notifications and we don't have anything saved for them, so nothing to do here
            if (frequency == null && todoConfiguration == null)
            {
                return;
            }

            // They don't have any saved configurations, so we need to create a new one
            if (todoConfiguration == null)
            {
                todoConfiguration = new NotificationConfiguration()
                {
                    UserId = userId,
                    CustomerId = CustomerId,
                    Category = NotificationCategory.ToDo
                };
            }

            if (frequency == null)
            {
                todoConfiguration.IsActive = false;
            }
            else
            {
                todoConfiguration.IsActive = true;
                todoConfiguration.FrequencyType = frequency.Value;
            }

            todoConfiguration.Save();
        }

        /// <summary>
        /// Gets to do notification for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public NotificationConfiguration GetToDoNotificationForUser(string userId)
        {
            return GetConfigurationForUser(userId, NotificationCategory.ToDo);
        }

        /// <summary>
        /// Gets the configuration for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="notificationCategory">The notification category.</param>
        /// <returns></returns>
        private NotificationConfiguration GetConfigurationForUser(string userId, NotificationCategory notificationCategory)
        {
            return NotificationConfiguration.AsQueryable().FirstOrDefault(nc => nc.UserId == userId && nc.Category == notificationCategory);
        }
    }
}

