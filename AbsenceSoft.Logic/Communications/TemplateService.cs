﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Customers;
using Newtonsoft.Json.Linq;
using System.IO;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Common;
using AbsenceSoft.Logic.Administration;
using System;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Cases;

namespace AbsenceSoft.Logic.Communications
{
    public class TemplateService:LogicService
    {
        public TemplateService(User currentUser)
            :base(currentUser)
        {

        }

        public TemplateService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            :base(currentCustomer, currentEmployer, currentUser)
        {

        }

        #region Communication Templates

        /// <summary>
        /// Returns a communication template by it's unique id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Template GetCommunicationTemplate(string id)
        {
            var template = Template.GetById(id);
            //don't set the value if template is a new one
            if (template != null)
                template.Suppressed = template.IsSuppressed(CurrentCustomer, CurrentEmployer);
            return template;
        }

        /// <summary>
        /// Retrieves customer specific Communications Templates from the database and sorts/filters them by specified criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="followCriteriaToFilterByEmployerId"></param>
        /// <returns></returns>
        public ListResults CommunicationsTemplateList(ListCriteria criteria)
        {
            using (new InstrumentationContext("TemplateService.CommunicationsTemplateList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults result = new ListResults(criteria);
                string name = criteria.Get<string>("Name");
                string code = criteria.Get<string>("Code");
                string docType = criteria.Get<string>("DocType");

                List<IMongoQuery> ands = Template.DistinctAnds(CurrentUser, CustomerId, EmployerId);
                Template.Query.MatchesString(t => t.Name, name, ands);
                Template.Query.MatchesString(t => t.Code, code, ands);
                if (!string.IsNullOrWhiteSpace(docType))
                {
                    DocumentType type = DocumentType.None;
                    if(DocumentType.TryParse(docType, out type))
                    {
                        if(type != DocumentType.None)
                            ands.Add(Template.Query.EQ(t => t.DocType, type));
                    }
                }

                List<Template> templates = Template.DistinctAggregation(ands);
                result.Total = templates.Count();
                templates = templates
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();

                result.Results = templates.Select(c => new ListResult()
                    .Set("Id", c.Id)
                    .Set("CustomerId", c.CustomerId)
                    .Set("EmployerId", c.EmployerId)
                    .Set("Name", c.Name)
                    .Set("Subject", c.Subject)
                    .Set("Code", c.Code)
                    .Set("DocType", c.DocType)
                    .Set("AttachLatestIncompletePaperwork", c.AttachLatestIncompletePaperwork)
                    .Set("IsLOASpecific", c.IsLOASpecific)
                    .Set("ModifiedDate", c.ModifiedDate)
                    .Set("Suppressed", c.IsSuppressed(CurrentCustomer, CurrentEmployer))
                );

                return result;
            }
        }

        

        /// <summary>
        /// Makes sure we aren't saving over a base template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public Template SaveCommunicationTemplate(Template template)
        {
            using (new InstrumentationContext("TemplateService.SaveCommunicationTemplate"))
            {
                Template savedTemplate = Template.GetById(template.Id);
                if (savedTemplate != null && 
                    (!savedTemplate.IsCustom || savedTemplate.EmployerId != EmployerId))
                {
                        template.Clean();
                }

                template.CustomerId = CustomerId;
                template.EmployerId = EmployerId;
                return template.Save();
            }
        }

        /// <summary>
        /// Deletes a communication template and the associated document, if the document isn't referenced by anything else
        /// </summary>
        /// <param name="template"></param>
        public void DeleteCommunicationTemplate(Template template)
        {
            if (template == null)
                return;

            /// Can't delete a default
            if (!template.IsCustom)
                return;

            template.Delete();

            Document doc = template.Document;
            if (doc != null)
            {
                if (Template.Query.Find(Template.Query.EQ(t => t.DocumentId, doc.Id)).Count() == 0)
                {
                    Log.Info("S3: Deleting template file \"{4}\" with key, \"{0}\", for Customer \"{1}\", Employer \"{2}\", Template \"{3}\"", doc.Locator, template.CustomerId, template.EmployerId, template.Code, doc.Id);
                    doc.Remove();
                }
            }
        }

        /// <summary>
        /// Toggles the suppression for a template selected by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Template ToggleCommunicationTemplateSuppressionById(string id)
        {
            var template = Template.GetById(id);
            template.ToggleSuppression(CurrentCustomer, CurrentEmployer);
            template.Suppressed = template.IsSuppressed(CurrentCustomer, CurrentEmployer);
            return template;
        }

        /// <summary>
        /// Get template by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Template GetTemplateByCode(string code)
        {
            var types = Template.AsQueryable().Where(cat => cat.Code == code).ToList();
            return types.FirstOrDefault();
        }
        #endregion

        #region Paperwork Templates

        /// <summary>
        /// Returns a paperwork template by it's unique id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Paperwork GetPaperworkTemplate(string id)
        {
            return Paperwork.GetById(id);
        }

        /// <summary>
        /// Retrieves customer specific Paperwork Templates from the database and sorts/filters them by specified criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults PaperworkTemplateList(ListCriteria criteria)
        {
            using (new InstrumentationContext("TemplateService.PaperworkTemplateList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);
                string name = criteria.Get<string>("Name");
                string docType = criteria.Get<string>("DocType");
                string code = criteria.Get<string>("Code");
                string description = criteria.Get<string>("Description");
                List<string> ids = new List<string>();
                JArray paperworkIds = criteria.Get<JArray>("PaperworkIds");

                List<IMongoQuery> ands = Paperwork.DistinctAnds(CurrentUser, CustomerId, EmployerId);
                Paperwork.Query.MatchesString(t => t.Name, name, ands);
                Paperwork.Query.MatchesString(t => t.Code, code, ands);
                if (!string.IsNullOrWhiteSpace(docType))
                {
                    DocumentType type = DocumentType.None;
                    if (DocumentType.TryParse(docType, out type))
                    {
                        if (type != DocumentType.None)
                            ands.Add(Template.Query.EQ(t => t.DocType, type));
                    }
                }

                List<Paperwork> paperwork = Paperwork.DistinctAggregation(ands);
                results.Total = paperwork.Count;

                paperwork = paperwork
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();
                results.Results = paperwork.Select(p => new ListResult()
                    .Set("Id", p.Id)
                    .Set("CustomerId", p.CustomerId)
                    .Set("EmployerId", p.EmployerId)
                    .Set("Name", p.Name)
                    .Set("Code", p.Code)
                    .Set("DocType", p.DocType)
                    .Set("FileName", p.FileName)
                    .Set("Description", p.Description)
                    .Set("EnclosureText", p.EnclosureText)
                    .Set("RequiresReview", p.RequiresReview)
                    .Set("IsForApprovalDecision", p.IsForApprovalDecision)
                    .Set("ReturnDateAdjustment", p.ReturnDateAdjustment)
                    .Set("ModifiedDate", p.ModifiedDate)
                );

                return results;
            }
        }

        public Paperwork SavePaperworkTemplate(Paperwork paperwork) {
            using (new InstrumentationContext("TemplateService.SavePaperworkTemplate"))
            {
                Paperwork savedPaperwork = Paperwork.GetById(paperwork.Id);
                if(savedPaperwork != null 
                    && (!savedPaperwork.IsCustom || savedPaperwork.EmployerId != EmployerId))
                {
                    paperwork.Clean();
                }

                paperwork.CustomerId = CustomerId;
                paperwork.EmployerId = EmployerId;
                return paperwork.Save();
            }
        }

        public void DeletePaperworkTemplate(Paperwork paperwork)
        {
            if (paperwork == null)
                return;

            if (!paperwork.IsCustom)
                return;

            paperwork.Delete();

            Document doc = paperwork.Document;
            if (doc != null)
            {
                if (Paperwork.Query.Find(Paperwork.Query.EQ(t => t.FileId, doc.Id)).Count() == 0)
                {
                    Log.Info("S3: Deleting paperwork file \"{4}\" with key, \"{0}\", for Customer \"{1}\", Employer \"{2}\", Template \"{3}\"", doc.Locator, paperwork.CustomerId, paperwork.EmployerId, paperwork.Code, doc.Id);
                    doc.Remove();
                }
            }
        }

        /// <summary>
        /// Get paperwork by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Paperwork GetPaperworkByCode(string code)
        {
            var types = Paperwork.AsQueryable().Where(cat => cat.Code == code).ToList();
            return types.FirstOrDefault();
        }

        #endregion


        public IEnumerable<Paperwork> GetAllPaperworkTemplates()
        {
            return Paperwork.DistinctFind(null, CustomerId, EmployerId);
        }

        /// <summary>
        /// Gets all communication templates for the current customer and employer.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Template> GetAllCommunicationTemplates()
        {
            return Template.DistinctFind(null, CustomerId, EmployerId);
        }

        public Stream DownloadTemplateCatalog()
        {
            List<CustomField> customFields = CustomField.DistinctFind(null, CustomerId, EmployerId).ToList();
            List<string> policyCodes = new PolicyService(CustomerId, EmployerId).GetPolicyCodes();
            List<DemandType> demandTypes = DemandType.DistinctFind(null, CustomerId, EmployerId).ToList();
            List<string> caseAssigneeTypeCode = new CaseService().GetCaseAssigneeTypeCode(CustomerId);
            return Rendering.Template.GenerateTemplate(customFields, policyCodes, demandTypes, caseAssigneeTypeCode);
        }

        
    }
}
