﻿
namespace AbsenceSoft.Logic.Communications
{
	public class TodoMessageData
	{
		public string Name { get; set; }

		public string CaseNumber { get; set; }

		public string ToDoTitle { get; set; }

		public string TodoDueDate { get; set; }

		public string CaseId { get; set; }

		public string CustomerSiteUrl { get; set; }
	}
}
