﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AbsenceSoft.Logic.Properties;
namespace AbsenceSoft.Logic.Embedded
{

    /// <summary>
    /// Returns Industry Description from NAICS/SIC code 
    /// </summary>
    public static class IndustryDescription
    {
        readonly static Dictionary<string, string> naicsAndSicDictionary = null;
     
        /// <summary>
        /// Initializes a new instance of the <see cref="IndustryDescription"/> class.
        /// </summary>
        static IndustryDescription()
        {
            naicsAndSicDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(Resources.NaicsAndSic);
        }

        /// <summary>
        /// Gets the industry description.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public static string GetIndustryDescription(string code)
        {
            return naicsAndSicDictionary.ContainsKey(code) ? naicsAndSicDictionary[code] : null;
        }
    }
}
