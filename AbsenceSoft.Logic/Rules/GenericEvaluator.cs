﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AbsenceSoft.Logic.Rules
{
    public static class GenericEvaluator
    {
        /// <summary>
        /// Stores a static list of enumeration types within the AppDomain that may be used for evaluating LeaveOfAbsence expresions.
        /// </summary>
        private static List<Type> enumTypes = Assembly.GetAssembly(typeof(AbsenceSoft.Data.BaseEntity<>)).GetTypes().Where(t => t.IsEnum).ToList();

        /// <summary>
        /// Gets a read-only enumerable collection of enum types that this evaluator knows about.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetEnumTypes() { return enumTypes.AsReadOnly(); }

        /// <summary>
        /// Evalutes a textual expression
        /// </summary>
        /// <param name="expression">The text expression to evaluate</param>
        /// <returns>The evaluated result of the expression</returns>
        public static object Eval(string expression, dynamic toBeEvaluated)
        {
            using (new InstrumentationContext("GenericEvaluator.Eval"))
            {
                try
                {
                    string modifiedExpression = expression;
                    // Determine if we need to prepend the expression with our Obj. thingy
                    if (!modifiedExpression.StartsWith("#") && !modifiedExpression.StartsWith("'") && !modifiedExpression.StartsWith("_") && !modifiedExpression.StartsWith("Obj."))
                        modifiedExpression = string.Concat("Obj.", modifiedExpression);
                    // Determine the appropriate expression prefix, if any, that should be prepended if one for a known instance type is not already
                    // Build a compiled expressiong from our expression instance name and expression text
                    CompiledExpression exp = new CompiledExpression(modifiedExpression);
                    // Register the default known types, simple types, etc.
                    exp.RegisterDefaultTypes();
                    // Register all of our Enums, 'cause those may be referenced. They should not be forced to namespace types either.
                    enumTypes.ForEach(e => exp.RegisterType(e.Name, e));
                    // Register our Leave of Absence variable using the expression instance name constant.
                    //  This works similar to know types but is necessary in building the expression tree, duh.
                    exp.RegisterType("Obj", toBeEvaluated);
                    // Parse the expression
                    exp.Parse();
                    // Compile the expression into our temporary assembly and compiled expression tree (allows evaluation).
                    exp.Compile();
                    // Evaluate the expression, YAY, and return the result, we're done yo!
                    return exp.Eval();
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error evaluating expression: {0}", expression), ex);
                    throw;
                }
            }
        }

        /// <summary>
        /// Evalutes a textual expression against the passed in leave of absence instance.
        /// </summary>
        /// <typeparam name="T">The expression return result type</typeparam>
        /// <param name="expression">The text expression to evaluate</param>s
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="wfItem">The todo item, if any, to also be evaulated</param>
        /// <returns>The evaluated result of the expression</returns>
        public static T Eval<T>(string expression, dynamic toBeEvaluated)
        {
            using (new InstrumentationContext("GenericEvaluator.Eval<{0}>", typeof(T).Name))
            {
                try
                {
                    // Determine the appropriate expression prefix, if any, that should be prepended if one for a known instance type is not already
                    var expString = expression;
                    // Determine if we need to prepend the expression with our Obj. thingy
                    if (!expString.StartsWith("'") && !expString.StartsWith("_") && !expString.StartsWith("Obj."))
                        expString = string.Concat("Obj.", expString);

                    // Build a compiled expressiong from our expression instance name and expression text
                    CompiledExpression<T> exp = new CompiledExpression<T>(expString);
                    // Register the default known types, simple types, etc.
                    exp.RegisterDefaultTypes();
                    // Register all of our Enums, 'cause those may be referenced. They should not be forced to namespace types either.
                    enumTypes.ForEach(e => exp.RegisterType(e.Name, e));
                    // Register our Leave of Absence variable using the expression instance name constant.
                    //  This works similar to know types but is necessary in building the expression tree, duh.
                    exp.RegisterType("Obj", toBeEvaluated);
                    // Parse the expression
                    exp.Parse();
                    // Compile the expression into our temporary assembly and compiled expression tree (allows evaluation).
                    exp.Compile();
                    // Evaluate the expression, YAY, and return the result, we're done yo!
                    return exp.Eval();
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error evaluating expression {0}", expression), ex);
                    throw;
                }
            }
        }
    }
}
