﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Rules
{

    /// <summary>
    /// Methods to run rules
    /// </summary>
    public class RuleService<T>:LogicService
    {
        /// <summary>
        /// Takes a rule group and a leave of absence object and evaluates all the rules
        /// in the rule group updating the RuleGroup's List or IRules in the process
        /// </summary>
        /// <param name="rg">The Rule Group to evaluate</param>
        /// <param name="toBeEvalue">The object to run the rules against</param>
        public bool EvaluateRuleGroup(RuleGroup rg, T toBeEvaluated)
        {
            // we started so count it as done
            rg.HasBeenEvaluated = true;
            foreach (Rule r in rg.Rules)
            {
                try
                {
                    r.ActualValue = GenericEvaluator.Eval(r.LeftExpression, toBeEvaluated);
                    r.Result = GenericEvaluator.Eval<bool>(r.ToString(), toBeEvaluated) ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail; // I like big bits and I cannot lie
                }
                catch(NullReferenceException)
                {
                    r.Result = AppliedRuleEvalResult.Unknown;
                    if (r.RightExpression == "null")
                        r.Result = r.Operator == "==" ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (Exception ex)
                {
                    r.ErrorMessages.AddFormat("An error occurred evaluating this rule, {0}", ex.Message);
                    r.Result = AppliedRuleEvalResult.Unknown;
                }
            }

            return rg.Pass;
        }
    }
}
