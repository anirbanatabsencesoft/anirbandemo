﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Logic.Common;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using AbsenceSoft.Logic.Customers.Contracts;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// The Employee service provides methods for creating new employees, employee record validation, 
    /// updating employees based on roster, upload, UI, etc., and employee search logic. Part of the 
    /// complexity and need for an employee service vs. direct CRUD is that employee updates may impact 
    /// an absence, affect eligibility, communications, etc. and may also trigger todo or additional 
    /// rules that have to be accounted for based on the status of that employee, such as terminating an 
    /// employee that has an open FMLA leave of absence, etc.
    /// </summary>
    public class EmployeeService : LogicService, ILogicService, IEmployeeService
    {
        // I'm guessing this should go somewhere else besides here
        private const string STATES_AND_TERRITORIES = "AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY,AS,GU,MP,PR,VI";

        public EmployeeService()
        {
        }

        // Employee Update
        //  1. Determine update rules
        //  2. Only update allowed properties based on employee state
        //  3. Merge appropriate properties, create any tasks based on affected leaves, etc.

        public List<Employee> ExportEmployeeList(string employerId)
        {
            using (new InstrumentationContext("EmployeeService.ExportEmployeeList"))
            {
                return Employee.AsQueryable().Where(m => m.EmployerId == employerId).ToList();
            }
        } // ExportEmployeeList

        public ListResults EmployeeList(ListCriteria criteria)
        {
            using (new InstrumentationContext("EmployeeService.EmployeeList"))
            {
                string[] includeFields = new[] { "_id", "FirstName", "LastName", "MiddleName", "EmployeeNumber", "HireDate", "EmployerId", "EmployerName" };

                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string name = criteria.Get<string>("Name");
                string lName = criteria.Get<string>("LastName");
                string fName = criteria.Get<string>("FirstName");
                string mName = criteria.Get<string>("MiddleName");

                string eeid = criteria.Get<string>("EmployeeNumber");
                long? hireDate = criteria.Get<long?>("HireDate");
                string nameOrEEid = criteria.Get<string>("NameOrEmployeeNumber");
                IEnumerable<string> includeAddl = criteria.GetList<string>("Include") ?? new string[0];
                string employeeNameNumberOrCase = criteria.Get<string>("EmployeeNameNumberOrCase");
                includeFields = includeFields.Union(includeAddl).ToArray();

                List<IMongoQuery> ands = new List<IMongoQuery>();
                List<IMongoQuery> ors = new List<IMongoQuery>();

                // Always add the employer id to the filter
                if (AbsenceSoft.Data.Security.User.Current != null)
                    ands.Add(User.Current.BuildDataAccessFilters(Permission.ViewEmployee, "_id"));

                // Check contact type based on roles
                if (!User.Permissions.GetPermissions(User.Current).Contains(Permission.ViewAllEmployees.Id))
                {
                    var currentRoles = Role.GetByIds(User.Current.Roles).Where(r => r.ContactTypes != null).Select(r => r.ContactTypes).ToList();
                    string[] currentContactTypes = currentRoles.SelectMany(r => r).Distinct().ToArray();
                    if(currentContactTypes != null)
                    {
                        var userEmployeeNumber = User.Current.Employers.Where(a => a.Employee != null && a.Employee.Id == User.Current.EmployeeId)
                                                                       .FirstOrDefault()?.Employee?.EmployeeNumber;
                        // Get employee contact only with matched contact types
                        if (!string.IsNullOrWhiteSpace(userEmployeeNumber))
                        {
                            var empContact = EmployeeContact.Repository.Where(e => currentContactTypes.Contains(e.ContactTypeCode)
                                                                                    && e.IsDeleted != true && e.CustomerId.Equals(User.Current.CustomerId)
                                                                                    && e.EmployerId.Equals(User.Current.EmployerId)
                                                                                    && e.RelatedEmployeeNumber != null && e.RelatedEmployeeNumber != string.Empty 
                                                                                    && e.RelatedEmployeeNumber.Equals(userEmployeeNumber))
                                                                            .Select(e => e.EmployeeId).Distinct().ToArray();
                           
                            ors.Add(Employee.Query.In(e => e.Id, empContact));
                        }
                    }
                }
           
                // Multi-Employer type filter 
                string employerId = criteria.Get<string>("EmployerId");

                //Check if user is limited to one employer
                if (string.IsNullOrWhiteSpace(employerId) && Employer.Current != null)
                    employerId = Employer.Current.Id;

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));

                if (!string.IsNullOrWhiteSpace(nameOrEEid))
                {
                    var term = Regex.Escape(nameOrEEid.Trim());
                    var searchExp = new BsonRegularExpression(new Regex("^" + term, RegexOptions.IgnoreCase));
                    ands.Add(Employee.Query.Or(
                        Employee.Query.Matches(e => e.LastName, searchExp),
                        Employee.Query.Matches(e => e.FirstName, searchExp),
                        Employee.Query.Matches(e => e.MiddleName, searchExp),
                        Employee.Query.Matches(e => e.EmployeeNumber, searchExp)
                    ));

                }

                if (!string.IsNullOrWhiteSpace(employeeNameNumberOrCase))
                {
                    var term = Regex.Escape(employeeNameNumberOrCase.Trim());
                    var searchExp = new BsonRegularExpression(new Regex("^" + term, RegexOptions.IgnoreCase));
                    ands.Add(Employee.Query.Or(
                        Employee.Query.Matches(e => e.LastName, searchExp),
                        Employee.Query.Matches(e => e.FirstName, searchExp),
                        Employee.Query.Matches(e => e.MiddleName, searchExp),
                        Employee.Query.Matches(e => e.EmployeeNumber, searchExp)
                    ));
                }

                // If the lastname and firstname are equal, then it's a "full name" filter, duh, so do an OR filter
                if (!string.IsNullOrWhiteSpace(name))
                {
                    if (name.Contains(','))
                    {
                        string[] arrName = name.Split(',');
                        if (arrName.Length > 0)
                        {
                            lName = arrName[0].Trim();
                            if (arrName.Length > 1)
                            {
                                fName = arrName[1].ToString().Trim();
                                if (arrName.Length > 2)
                                {
                                    mName = arrName[2].ToString().Trim();
                                }
                            }
                        }
                    }
                    else
                    {
                        //Added for Name Search
                        ands.Add(
                             Employee.Query.Or(
                                 Employee.Query.Matches(e => e.LastName, new BsonRegularExpression("^" + name, "i")),
                                 Employee.Query.Matches(e => e.FirstName, new BsonRegularExpression("^" + name, "i")),
                                 Employee.Query.Matches(e => e.MiddleName, new BsonRegularExpression("^" + name, "i"))
                         ));
                    }
                }

                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                if (!string.IsNullOrWhiteSpace(lName))
                    ands.Add(Employee.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(lName), RegexOptions.IgnoreCase))));

                if (!string.IsNullOrWhiteSpace(fName))
                    ands.Add(Employee.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(fName), RegexOptions.IgnoreCase))));

                if (!string.IsNullOrWhiteSpace(mName))
                    ands.Add(Employee.Query.Matches(e => e.MiddleName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(mName), RegexOptions.IgnoreCase))));

                if (!string.IsNullOrWhiteSpace(eeid))
                    ands.Add(Employee.Query.Matches(e => e.EmployeeNumber, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(eeid), RegexOptions.IgnoreCase))));
                if (hireDate.HasValue)
                {
                    ands.Add(Employee.Query.GTE(e => e.HireDate, new BsonDateTime(hireDate.Value)));
                    // Window is 24 hours
                    ands.Add(Employee.Query.LT(e => e.HireDate, new BsonDateTime(hireDate.Value + 86400000)));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                ands.Add(Employee.Query.IsNotDeleted());

                // Create main OR query with add AND query
                List<IMongoQuery> finalOrs = new List<IMongoQuery>();
                if (ands != null && ands.Count > 0)
                    finalOrs.Add(Employee.Query.And(ands));
                if (ors != null && ors.Count > 0)
                    finalOrs.Add(Employee.Query.Or(ors));

                var query = Employee.Repository.Collection.FindAs<BsonDocument>(finalOrs.Count > 0 ? Employee.Query.Or(finalOrs) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize)
                    .SetFields(Fields.Include(includeFields));

                // Build sort with Mongo Cursor
                var empListByLastName = name != null ? Employee.AsQueryable().Where(emp => emp.LastName.ToUpper() == name.ToUpper()).ToList() : null;

                var empListByFirstName = (empListByLastName != null && empListByLastName.Count > 0) 
                                         ? empListByLastName.Where(emp => emp.FirstName.ToUpper() == name.ToUpper()).ToList()
                                         : name != null ? Employee.AsQueryable().Where(emp => emp.FirstName.ToUpper() == name.ToUpper()).ToList() : null;

                var empCountsByMiddleName = empListByFirstName != null ? empListByFirstName.Count(emp => emp.MiddleName != null) : 0;

                var empCountByMiddleName = empListByLastName != null ? empListByLastName.Count(emp => emp.MiddleName != null) : 0;

                // Build sort by (try to use our index as much as possible)
                var sortBy = SortBy.Ascending("CustomerId", "EmployerId", "IsDeleted");

                var sortByLastName = SortBy.Ascending("LastName");

                var sortByFirstName = SortBy.Ascending("FirstName");

                var sortByMiddleName = SortBy.Ascending("MiddleName");

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLowerInvariant() == "name") // Need a compound order by because name is a psedo field.
                    {
                        // Use this if for checking criteria.SortDirection in Descending order
                        if (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Descending)
                        {
                            sortByLastName = SortBy.Descending("LastName");
                            sortByFirstName = SortBy.Descending("FirstName");
                            sortByMiddleName = SortBy.Descending("MiddleName");
                        }
                    }
                    else // Normal order by
                    {
                        sortBy = criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? sortBy.Ascending(criteria.SortBy)
                            : sortBy.Descending(criteria.SortBy);
                    }
                }
                else
                    sortBy = sortBy.Ascending("LastName", "EmployeeNumber");

                if (empListByLastName != null && empListByLastName.Count > 0)
                {
                    // Sort by Middle Name will be done if we have more records
                    query = (empCountByMiddleName > 2)
                        ? query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName).SetSortOrder(sortByMiddleName)
                        : query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName);                   
                }
                else if (empListByFirstName != null && empListByFirstName.Count > 2) 
                {
                    // Sort by First Name and Middle Name will be done if we have records
                    query = (empCountsByMiddleName > 2) 
                            ? query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName).SetSortOrder(sortByMiddleName) 
                            : query.SetSortOrder(sortBy).SetSortOrder(sortByLastName);                    
                }
                else
                {
                    // Sort by LastName , First Name will be done if we have records
                    if(lName != null && fName != null)
                    {
                        // Build sort with Mongo Cursor
                        if (mName != null)
                        {
                            query = query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName).SetSortOrder(sortByMiddleName);
                        }
                        else
                        {
                            var empListByLastFirstName = Employee.AsQueryable().Where(emp => emp.LastName.ToUpper() == lName.ToUpper() && emp.FirstName.ToUpper() == fName.ToUpper()).ToList();

                            var empCountWithMiddleName = empListByLastFirstName != null ? empListByLastFirstName.Count(emp => emp.MiddleName != null) : 0;

                            query = empCountWithMiddleName > 0 ? query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName).SetSortOrder(sortByMiddleName) : query.SetSortOrder(sortBy).SetSortOrder(sortByLastName).SetSortOrder(sortByFirstName);
                        }                        
                    }                    
                    else
                    {
                        // Sort by Last Name will be done if we have records
                        query = query.SetSortOrder(sortBy).SetSortOrder(sortByLastName);
                    }                    
                }


                result.Total = query.Count();

                var queryResult = query.ToList();
                string[] empIds = queryResult.Select(emp => emp.GetRawValue<string>("_id")).ToArray();
                List<Case> empCases = Case.Query.Find(Case.Query.In(c => c.Employee.Id, empIds)).ToList();
                //query.Explain(true);
                result.Results = queryResult.Select(e =>
                {
                    var id = e.GetRawValue<string>("_id");
                    var hd = e.GetRawValue<DateTime?>("HireDate");
                    var lr = new ListResult()
                        .Set("Id", e.GetRawValue<string>("_id"))
                        .Set("FirstName", e.GetRawValue<string>("FirstName"))
                        .Set("LastName", e.GetRawValue<string>("LastName"))
                        .Set("Name", string.Concat(e.GetRawValue<string>("LastName"), ", ", e.GetRawValue<string>("FirstName"), " ", e.GetRawValue<string>("MiddleName")))
                        .Set("EmployeeNumber", e.GetRawValue<string>("EmployeeNumber"))
                        .Set("EmployerId", e.GetRawValue<string>("EmployerId"))
                        .Set("EmployerName", e.GetRawValue<string>("EmployerName"))
                        .Set("HireDate", hd)
                        .Set("HireDateText", hd.HasValue ? hd.Value.ToString("MM/dd/yyyy") : "")
                        .Set("EmployeeCasesInfo", empCases.Where(c => c.Employee.Id == id).OrderBy(o => o.EndDate).ToList().Select(m => new EmployeeCaseInfo
                        {
                            CaseId = m.Id,
                            CaseNumber = m.CaseNumber,
                            Reason = m.Reason != null ? m.Reason.Name : string.Empty,
                            Description = m.Description,
                            StartDate = m.StartDate,
                            EndDate = m.EndDate,
                            Status = (m.Status == CaseStatus.Cancelled) ? "Denied" : m.Segments.Where(en => en.AppliedPolicies.Any(f => f.Usage.Any(k => k.Determination == AdjudicationStatus.Approved))).Count() > 0 ? "Approved" : "Pending",
                            CaseType = m.CaseType.ToString(),
                            CaseStatus = (int)m.Status
                        }).ToList());

                    foreach (var addl in includeAddl)
                    {
                        if (e.Contains(addl))
                        {
                            var value = e[addl];
                            if (value is BsonDocument)
                            {
                                lr.Set(addl, value.AsBsonDocument.ToDictionary());
                            }
                            else
                            {
                                lr.Set(addl, value.AsBsonValue);
                            }
                        }
                    }
                    return lr;
                });

                return result;
            } // using InstrumentationContext
        } // EmployeeList

        public ListResults EmployeeLastViewedList(ListCriteria criteria)
        {
            using (new InstrumentationContext("EmployeeService.EmployeeLastViewedList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (User.Current != null)
                    ands.Add(ItemViewHistory.Query.EQ(i => i.CreatedById, User.Current.Id));
                ands.Add(ItemViewHistory.Query.EQ(i => i.ItemType, "Employee"));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(ItemViewHistory.Query.EQ(e => e.EmployerId, employerId));

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = ItemViewHistory.Query.Find(ands.Count > 0 ? ItemViewHistory.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                // Build sort by (try to use our index as much as possible)
                query = query.SetSortOrder(SortBy<ItemViewHistory>.Descending(i => i.CreatedDate));

                result.Total = query.Count();
                //query.Explain(true);
                result.Results = query.ToList().Select(e =>
                {
                    var id = e.ItemId;
                    var hd = e.Metadata.GetRawValue<DateTime?>("HireDate");
                    var lr = new ListResult()
                        .Set("Id", id)
                        .Set("FirstName", e.Metadata.GetRawValue<string>("FirstName"))
                        .Set("LastName", e.Metadata.GetRawValue<string>("LastName"))
                        .Set("Name", e.Metadata.GetRawValue<string>("EmployeeName"))
                        .Set("EmployeeNumber", e.Metadata.GetRawValue<string>("EmployeeNumber"))
                        .Set("EmployerId", e.Metadata.GetRawValue<string>("EmployerId"))
                        .Set("EmployerName", e.Metadata.GetRawValue<string>("EmployerName"))
                        .Set("HireDate", hd)
                        .Set("HireDateText", hd.HasValue ? hd.Value.ToString("MM/dd/yyyy") : "");
                    return lr;
                });

                return result;
            }
        }

        public ListResults EmployeeQuickFind(ListCriteria criteria)
        {
            using (new InstrumentationContext("EmployeeService.EmployeeQuickFind"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string text = criteria.Get<string>("text");
                bool eeNumber = criteria.Get<bool>("eeNum");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (AbsenceSoft.Data.Security.User.Current != null)
                    ands.Add(User.Current.BuildDataAccessFilters(Permission.ViewEmployee, "_id"));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");
                if (string.IsNullOrWhiteSpace(employerId) && Employer.Current != null)
                    employerId = Employer.Current.Id;
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));

                // If the lastname and firstname are equal, then it's a "full name" filter, duh, so do an OR filter
                if (!string.IsNullOrWhiteSpace(text))
                {
                    var safeText = Regex.Escape(text);
                    var reggie = new BsonRegularExpression(new Regex("^" + safeText, RegexOptions.IgnoreCase));
                    ands.Add(Employee.Query.Or(
                        Employee.Query.Matches(e => e.LastName, reggie),
                        Employee.Query.Matches(e => e.FirstName, reggie),
                        Employee.Query.Matches(e => e.EmployeeNumber, reggie)
                    ));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                ands.Add(Employee.Query.IsNotDeleted());
                var query = Employee.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Employee.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize)
                    .SetFields(Fields.Include("_id", "FirstName", "LastName", "EmployeeNumber"));

                // Build sort by (try to use our index as much as possible)
                var sortBy = SortBy.Ascending("CustomerId", "EmployerId");

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLowerInvariant() == "name") // Need a compound order by because name is a psedo field.
                        sortBy = criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? sortBy.Ascending("LastName", "FirstName", "EmployeeNumber")
                            : sortBy.Descending("LastName", "FirstName").Ascending("EmployeeNumber");
                    else // Normal order by
                        sortBy = criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? sortBy.Ascending(criteria.SortBy)
                            : sortBy.Descending(criteria.SortBy);
                }
                else
                    sortBy = sortBy.Ascending("LastName", "FirstName", "EmployeeNumber");
                query = query.SetSortOrder(sortBy);

                result.Total = query.Count();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("id", eeNumber ? e.GetRawValue<string>("EmployeeNumber") : e.GetRawValue<string>("_id"))
                    .Set("text", string.Concat(e.GetRawValue<string>("LastName"), ", ", e.GetRawValue<string>("FirstName"), " ", e.GetRawValue<string>("EmployeeNumber")))
                );

                return result;
            }
        } // EmployeeQuickFind

        public ListResults ContactQuickFind(ListCriteria criteria)
        {
            using (new InstrumentationContext("EmployeeService.ContactQuickFind"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);
                string text = criteria.Get<string>("text");
                string employeeId = criteria.Get<string>("EmployeeId");

                List<IMongoQuery> ands = new List<IMongoQuery>();

                // Ensure the employee Id matches
                ands.Add(EmployeeContact.Query.EQ(c => c.EmployeeId, employeeId));
                EmployeeContact.Query.StringMatchesMultipleFields(text, ands, e => e.Contact.FirstName, e => e.Contact.LastName, e => e.Contact.Email);
                var query = EmployeeContact.Query.Find(ands.Count > 0 ? EmployeeContact.Query.And(ands) : null)
                   .SetFields(EmployeeContact.Query.IncludeFields(
                       e => e.Contact,
                       e => e.ContactTypeName
                   ));



                var queryResults = query.ToList();
                List<string> allContactTypeCodes = queryResults.Select(qr => qr.ContactTypeCode).ToList();
                List<ContactType> contactTypes = ContactType.Query.Find(
                    ContactType.Query.In(ct => ct.Code, allContactTypeCodes)
                ).ToList();
                result.Results = queryResults.Select(o => new ListResult()
                            .Set("id", o.Id)
                            .Set("typeId", o.ContactTypeCode)
                            .Set("type", o.ContactTypeName)
                            .Set("designation", (int)GetEmployeeContactTypeByCode(contactTypes, o.ContactTypeCode).ContactCategory)
                            .Set("designationText", GetEmployeeContactTypeByCode(contactTypes, o.ContactTypeCode).ContactCategory.ToString().SplitCamelCaseString())
                            .Set("email", o.Contact.Email)
                            .Set("homeEmail", o.Contact.AltEmail)
                            .Set("address", o.Contact.Address)
                            .Set("fax", o.Contact.Fax)
                            .Set("lastName", o.Contact.LastName)
                            .Set("firstName", o.Contact.FirstName)
                            .Set("text", string.Format("{0} {1} ({2})", o.Contact.FirstName, o.Contact.LastName, o.ContactTypeName)));

                return result;
            }
        } // ContactQuickFind

        private ContactType GetEmployeeContactTypeByCode(List<ContactType> contactTypes, string code)
        {
            return contactTypes.Where(ct => ct.Code == code).OrderBy(ct =>
                {
                    if (!string.IsNullOrWhiteSpace(ct.EmployerId))
                        return 0;
                    if (!string.IsNullOrWhiteSpace(ct.CustomerId))
                        return 1;
                    return 2;
                }).DefaultIfEmpty(new ContactType() { ContactCategory = ContactTypeDesignationType.None }).First();
        }

        /// <summary>
        /// Returns list of existing employee contacts based on employeeId and absenceReasonCode
        /// </summary>
        /// <param name="employeeId">Id of an employee</param>
        /// <param name="absenceReasonCode">code specific to absence reason</param>
        /// <returns>List of EmployeeContact</returns>
        public List<EmployeeContact> GetEmployeeContactsByAbsenceReasonCode(string employeeId, string absenceReasonCode)
        {
            List<string> contactTypesCodes = GetEmployeeContactTypesByAbsenceReasonCode(absenceReasonCode, employeeId).Select(c => c.Code).ToList();
            return EmployeeContact.AsQueryable().Where(ec => ec.EmployeeId == employeeId && contactTypesCodes.Contains(ec.ContactTypeCode)).ToList();
        }

        /// <summary>
        /// Returns list of employee contact types based on absenceReasonCode
        /// </summary>
        /// <param name="absenceReasonCode">code specific to absence reason</param>
        /// <param name="employeeId">current employee id</param>
        /// <returns>List of contactType</returns>
        public List<ContactType> GetEmployeeContactTypesByAbsenceReasonCode(string absenceReasonCode, string employeeId = null)
        {
            List<ContactType> contactTypes = new List<ContactType>();
            using (var contactTypeService = new ContactTypeService())
            {
                switch (absenceReasonCode)
                {
                    case "EXIGENCY":
                    case "MILITARY":
                    case "RESERVETRAIN":
                    case "CEREMONY":
                    case "DOM":
                        contactTypes = contactTypeService.GetContactTypes(CurrentUser.CustomerId, null, ContactTypeDesignationType.Self, ContactTypeDesignationType.Personal).ToList();
                        break;
                    case "FHC":
                    case "BEREAVEMENT":
                        contactTypes = contactTypeService.GetContactTypes(CurrentUser.CustomerId, null, ContactTypeDesignationType.Personal).ToList();
                        break;
                    case "EDUCATIONAL":
                    case "PARENTAL":
                    case "BONDING":
                    case "PARENTALLEAVE":
                        contactTypes.Add(ContactType.GetByCode("CHILD"));
                        contactTypes.Add(ContactType.GetByCode("NEXTOFKIN"));
                        contactTypes.Add(ContactType.GetByCode("STEPCHILD"));
                        contactTypes.Add(ContactType.GetByCode("FOSTERCHILD"));
                        contactTypes.Add(ContactType.GetByCode("ADOPTEDCHILD"));
                        contactTypes.Add(ContactType.GetByCode("LEGALWARD"));
                        contactTypes.Add(ContactType.GetByCode("DOMESTICPARTNERSCHILD"));
                        contactTypes.Add(ContactType.GetByCode("CIVILUNIONPARTNERSCHILD"));
                        contactTypes.Add(ContactType.GetByCode("CHILDLIVINGWITHEMPLOYEE"));
                        contactTypes.Add(ContactType.GetByCode("GRANDCHILD"));
                        contactTypes.Add(ContactType.GetByCode("NIECE"));
                        contactTypes.Add(ContactType.GetByCode("NEPHEW"));
                        contactTypes.Add(ContactType.GetByCode("SIBLING"));
                        break;     
                    case "ADOPT":
                        contactTypes.Add(ContactType.GetByCode("FOSTERCHILD"));
                        contactTypes.Add(ContactType.GetByCode("ADOPTEDCHILD"));
                        contactTypes.Add(ContactType.GetByCode("LEGALWARD"));
                        contactTypes.Add(ContactType.GetByCode("OTHER"));
                        break;

                    default:
                        contactTypes = contactTypeService.GetContactTypes(CurrentUser.CustomerId, null, ContactTypeDesignationType.Personal).ToList();

                        //set work state restriction
                        var states = new List<string>();
                        var employee = Employee.GetById(employeeId);

                        if (employee != null)
                        {
                            if (!string.IsNullOrWhiteSpace(employee.WorkState))
                                states.Add(employee.WorkState);

                            if (employee.Info != null && employee.Info.Address != null && !string.IsNullOrWhiteSpace(employee.Info.Address.State))
                                states.Add(employee.Info.Address.State);

                            //filter out by state
                            if (states.Any())
                                contactTypes = contactTypes.Where(c => c.WorkStateRestrictions == null || c.WorkStateRestrictions.Intersect(states).Any()).ToList();
                        }
                        break;
                }
            }
            return contactTypes;
        }

        /// <summary>
        /// Sets the employee work schedule, ensuring no gaps are left when the new schedule is applied
        /// and it either correctly replaces, augments or corrects the current schedule(s) where appropriate.
        /// </summary>
        /// <param name="emp">The employee whos work schedule it is to modify</param>
        /// <param name="newWorkSchedule">The new work schedule to apply</param>
        /// <param name="variableTime">The variable time.</param>
        public void SetWorkSchedule(Employee emp, Schedule newWorkSchedule, List<VariableScheduleTime> variableTime)
        {
            if (newWorkSchedule != null && !emp.WorkSchedules.Any(w => w.Id == newWorkSchedule.Id))
                emp.WorkSchedules.Add(newWorkSchedule);
            else if (newWorkSchedule != null)
            {
                var oldSched = emp.WorkSchedules.FirstOrDefault(w => w.Id == newWorkSchedule.Id);
                if (oldSched != null && (
                    oldSched.StartDate != newWorkSchedule.StartDate ||
                    oldSched.EndDate != newWorkSchedule.EndDate ||
                    oldSched.Times.Count != newWorkSchedule.Times.Count ||
                    oldSched.ScheduleType != newWorkSchedule.ScheduleType ||
                    oldSched.Times.Except(newWorkSchedule.Times).Any()))
                {
                    newWorkSchedule.Id = Guid.NewGuid();
                    emp.WorkSchedules.Add(newWorkSchedule);
                }
            }

            // Level work schedules
            if (newWorkSchedule != null)
            {
                var overlap = emp.WorkSchedules.Where(w => w.Id != newWorkSchedule.Id &&
                    (w.StartDate.DateRangesOverLap(w.EndDate, newWorkSchedule.StartDate, newWorkSchedule.EndDate) ||
                    newWorkSchedule.StartDate.DateRangesOverLap(newWorkSchedule.EndDate, w.StartDate, w.EndDate))
                    ).ToList();
                foreach (var fix in overlap)
                {
                    if (fix.StartDate < newWorkSchedule.StartDate)
                    {
                        if (newWorkSchedule.EndDate.HasValue && (!fix.EndDate.HasValue || fix.EndDate > newWorkSchedule.EndDate))
                        {
                            // We're applying a schedule change in the middle somewhere for a fixed period, so
                            //  we need to copy the old schedule to make the EE's schedule perpetual.
                            Schedule copy = new Schedule();
                            copy.StartDate = newWorkSchedule.EndDate.Value.AddDays(1);
                            copy.EndDate = fix.EndDate;
                            copy.Times = fix.Times.Select(t => new Time()
                            {
                                SampleDate = t.SampleDate,
                                TotalMinutes = t.TotalMinutes
                            }).ToList();
                            emp.WorkSchedules.Add(copy);
                        }
                        fix.EndDate = newWorkSchedule.StartDate.AddDays(-1);
                    }
                    else if (newWorkSchedule.EndDate.HasValue && !fix.EndDate.HasValue)
                    {
                        fix.StartDate = newWorkSchedule.EndDate.Value.AddDays(1);
                        fix.Times.ForEach(t => t.SampleDate = fix.StartDate.AddDays((t.SampleDate - fix.StartDate).TotalDays));
                    }
                    else
                    {
                        // this is a replacement. Remove it from the work schedules collection
                        emp.WorkSchedules.Remove(fix);
                    }
                }
            }

            // Store variable time
            if (variableTime != null && variableTime.Any() && emp.Id !=null)
            {
                var bulk = VariableScheduleTime.Repository.Collection.InitializeUnorderedBulkOperation();
                var deletes = variableTime.Where(v => v.Time != null && v.Time.TotalMinutes == null || v.Time.TotalMinutes.Value <= 0).ToList();
                var upserts = variableTime.Where(v => v.Time != null && v.Time.TotalMinutes.HasValue && v.Time.TotalMinutes.Value > 0).ToList();
                bool any = false;
                if (deletes.Any())
                {
                    bulk.Find(VariableScheduleTime.Query.And(
                        VariableScheduleTime.Query.EQ(v => v.EmployeeId, emp.Id),
                        VariableScheduleTime.Query.NotExists(v => v.EmployeeCaseId),
                        VariableScheduleTime.Query.In(v => v.Time.SampleDate, deletes.Select(d => d.Time.SampleDate)))
                    ).Update(VariableScheduleTime.Updates
                        .Set(c => c.ModifiedById, (User.Current ?? User.System).Id)
                        .CurrentDate(c => c.ModifiedDate)
                        .Set(c => c.IsDeleted, true));
                    any = true;
                }
                if (upserts.Any())
                {
                    foreach (var up in upserts)
                    {
                        bulk.Find(VariableScheduleTime.Query.And(
                            VariableScheduleTime.Query.EQ(v => v.EmployeeId, emp.Id),
                            VariableScheduleTime.Query.NotExists(v => v.EmployeeCaseId),
                            VariableScheduleTime.Query.EQ(v => v.Time.SampleDate, up.Time.SampleDate)
                        ))
                        .Upsert()
                        .UpdateOne(VariableScheduleTime.Updates
                            .SetOnInsert(v => v.CustomerId, emp.CustomerId)
                            .SetOnInsert(v => v.EmployerId, emp.EmployerId)
                            .SetOnInsert(v => v.EmployeeId, emp.Id)
                            .SetOnInsert(v => v.EmployeeNumber, emp.EmployeeNumber)
                            .SetOnInsert(v => v.CreatedById, (User.Current ?? User.System).Id)
                            .SetOnInsert(v => v.CreatedDate, DateTime.UtcNow)
                            .SetOnInsert(v => v.Time.Id, Guid.NewGuid())
                            .SetOnInsert(v => v.Time.SampleDate, up.Time.SampleDate)
                            .Set(v => v.Time.TotalMinutes, up.Time.TotalMinutes)
                            .Set(v => v.ModifiedById, (User.Current ?? User.System).Id)
                            .Set(c => c.IsDeleted, false)
                            .CurrentDate(v => v.ModifiedDate)
                        );
                    }
                    any = true;
                }
                if (any)
                    bulk.Execute();
            }
        } // SetWorkSchedule

        /// <summary>
        /// Validates and then saves an employee to the database
        /// </summary>
        /// <param name="emp">The Employee</param>
        public Employee Update(Employee emp, Schedule newWorkSchedule = null, List<VariableScheduleTime> variableTimes = null)
        {
            using (new InstrumentationContext("EmployeeService.Update"))
            {
                // Set the EE work schedule.
                SetWorkSchedule(emp, newWorkSchedule, variableTimes);

                // this will throw an error if the save is invalid, just let it
                // flow up from here
                OptionalOut<string> outResult = new OptionalOut<string>();
                ValidateBeforeSave(emp, outResult);
                if (outResult != null && !string.IsNullOrEmpty(outResult.Result))
                    emp.Id = outResult.Result;

                // Set IsExempt to true if IsKeyEmployee is set to true. This does NOT apply to the converse situation
                if (emp.IsKeyEmployee && !emp.IsExempt)
                    emp.IsExempt = true;

                // we've made it this far we can save (the spouse method may save contacts)
                emp.Save();

                // from here down no more exceptions
                // next validate the spouse data
                ValidateSpouseData(emp);

                // If we have any schedule updates/new schedule, then we need to re-calc
                ProcessUpdates(emp, reCalc: newWorkSchedule != null || variableTimes != null);

                return emp;
            } // using InstrumentationContext
        } // Update

        /// <summary>
        /// Processes the updates.
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <param name="cases">The cases.</param>
        /// <param name="user">The user.</param>
        /// <param name="reCalc">if set to <c>true</c> [re calculate]. Typically used if work schedules have changed.</param>
        public void ProcessUpdates(Employee emp, List<Case> cases = null, User user = null, bool reCalc = false)
        {
            if (!reCalc)
                return;
            cases = cases ?? Case.AsQueryable()
                .Where(c => c.CustomerId == emp.CustomerId && c.EmployerId == emp.EmployerId && c.Employee.Id == emp.Id && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Closed)
                .ToList()
                .OrderBy(c => c.StartDate)
                .ToList();
            if (!cases.Any())
                return;

            foreach (var c in cases)
            {
                bool any = false;
                if (reCalc && c.Segments != null)
                {
                    new CaseService().Using(s => s.RunCalcs(c));
                    any = true;
                }
                if (any)
                {
                    c.ModifiedById = (user ?? User.Current ?? User.System).Id;
                    c.Save();
                }
            }
        }

        public Employee GetEmployeeByEmployeeNumber(string employeeNumber, string customerId, string employerID)
        {
            return Employee.AsQueryable().Where(e => e.EmployeeNumber == employeeNumber && e.EmployerId == employerID && e.CustomerId == customerId).FirstOrDefault();
        }
        /// <summary>
        /// Validate that the employee can be saved
        /// 
        ///     Ensure ServiceDate >= HireDate & RehireDate (if not null)
        ///     Ensure TerminationDate, if not null, >= ServiceDate & HireDate
        ///     Ensure RehireDate, if not null, > HireDate
        ///     Ensure WorkState is a valid US State Abbreviation if WorkCountry = ‘US’ or WorkCountry is null
        ///     
        /// </summary>
        /// <param name="emp"></param>
        public void ValidateBeforeSave(Employee emp, OptionalOut<string> outResult = null)
        {
            // make sure we are not breaking our uniqueness rules (wow - I can't splel)
            ValidateUniqueEmployee(emp, outResult);

            // then validate the dates
            ValidateEmployeeDates(emp);

            // if they work in the U.S. then their state must be in the U.S.
            if (emp.WorkCountry == "US" && (string.IsNullOrWhiteSpace(emp.WorkState) || STATES_AND_TERRITORIES.IndexOf(emp.WorkState) == -1))
                throw new AbsenceSoftException("Employee works in the U.S. but has an invalid or missing U.S. work state");

            if (!string.IsNullOrWhiteSpace(emp.SpouseEmployeeNumber) && (!Employee.AsQueryable().Where(e => e.CustomerId == emp.CustomerId && e.EmployerId == emp.EmployerId && e.EmployeeNumber == emp.SpouseEmployeeNumber).Any()))
            {
                throw new AbsenceSoftException($"Employee data for given Spouse number {emp.SpouseEmployeeNumber} not found under same employer.");
            }
        } // ValidateBeforeSave

        public void ValidateEmployeeDates(Employee emp)
        {
            if (emp.TerminationDate.HasValue && (!emp.HireDate.HasValue || emp.TerminationDate < emp.HireDate.Value))
                throw new AbsenceSoftException("Termination date occurs before employee hire dateor hire date is missing.");

            if (emp.RehireDate.HasValue && (!emp.HireDate.HasValue || emp.RehireDate.Value < emp.HireDate.Value))
                throw new AbsenceSoftException("Rehire Date occurs before the hire date or hire date is missing");
        } // ValidateEmployeeDates

        /// <summary>
        /// Validate the spouse info with the following rules:
        /// 
        /// If SpouseEmployeeNumber is not null/blank
        ///     Lookup Employee by SpouseEmployeeNumber + EmployerId
        ///     
        ///     If not exists for that combo, NULL out the SpouseEmployeeNumber field
        ///         Then, do a lookup just by SpouseEmployeeNumber (without EmployerId), if that exists, then the spouse works for someone else 
        ///         and we need to NULL out their SpouseEmployeeNumber field IF it == the current Employee’s Id, otherwise they 
        ///         really get around and have already married someone else and we should leave their record alone
        ///         
        ///     If you’re nulling out the EE record, do a search to ensure they don’t also have a corresponding Contact of type Spouse 
        ///     with RelatedEmployeeId = SpouseEmployeeNumber, if so, remove them
        /// 
        ///     Otherwise set the spouse’s employee records’ SpouseEmployeeNumber to this one’s and save it; now they’re data-married, 
        ///         See if the employee already has a Contact of type Spouse
        ///         Set the RelatedEmployeeId of the existing or new contact to the SpouseEmployeeNumber
        ///         Copy the name, etc. from the Employee record of that spouse
        ///    
        ///     Calc the YearsOfAge from the Employee record of that spouse’s DOB, if available, otherwise leave null
        ///         Do this for each Employee record, the employee and the spouse’s (fun right?)
        ///         Save the contact records
        ///         
        /// </summary>
        /// <param name="emp">The employee to test</param>
        public void ValidateSpouseData(Employee emp)
        {
            // is there spouse data, if not, then just return
            if (string.IsNullOrWhiteSpace(emp.SpouseEmployeeNumber))
                return;

            Employee theSpouse = Employee.AsQueryable().Where(e => e.CustomerId == emp.CustomerId && e.EmployerId == emp.EmployerId && e.EmployeeNumber == emp.SpouseEmployeeNumber).FirstOrDefault();

            // if the spouse is not found then they are no longer the spouse
            if (theSpouse == null)
            {
                emp.SpouseEmployeeNumber = null;
                emp.Save();
                return;
            }

            // employee has spouse with the same employer
            // make sure the spouses are spouses
            if (string.IsNullOrWhiteSpace(theSpouse.SpouseEmployeeNumber))
            {
                // found the spouse and the spouses aren't spouses so make them spouses
                theSpouse.SpouseEmployeeNumber = emp.EmployeeNumber;
                theSpouse.Save();

                // now see if the spouse has a contact type of spouse and that it matches this spouse
                // and if not then set it and save it
                List<EmployeeContact> spouseContacts = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == theSpouse.Id).ToList(); // can't use this, sorta circular theSpouse.GetContacts();

                // get the employee contact with a type of spouse
                EmployeeContact sfc = spouseContacts.Where(c => c.ContactTypeCode == "SPOUSE").FirstOrDefault();

                // if the contact is correct then there is nothing to do
                // if this contact does not match this employee delete it and create a new one
                if (sfc == null || sfc.RelatedEmployeeNumber != emp.EmployeeNumber)
                {
                    if (sfc != null)
                        sfc.Delete();

                    sfc = new EmployeeContact()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        Employee = theSpouse,
                        EmployeeId = theSpouse.Id,
                        EmployeeNumber = theSpouse.EmployeeNumber,
                        RelatedEmployeeNumber = emp.EmployeeNumber,
                        ContactTypeCode = "SPOUSE",
                        ContactTypeName = "Spouse",
                        YearsOfAge = emp.DoB.HasValue ? (double?)emp.DoB.Value.AgeInYears() : null
                    };

                    sfc.Save();

                } // if(ec == null || ec.RelatedEmployeeId !=  emp.Id)

                // check the employee to see if it has a spouse contact record
                List<EmployeeContact> eec = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == emp.Id).ToList();  // see above emp.GetContacts();
                EmployeeContact spouseContact = eec.Where(c => c.ContactTypeCode == "Spouse").FirstOrDefault();

                // the current spouse contact is blank or does not equal this one
                if (spouseContact == null || spouseContact.RelatedEmployeeNumber != emp.EmployeeNumber)
                {
                    if (spouseContact != null)
                        spouseContact.Delete();

                    spouseContact = new EmployeeContact()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeId = emp.EmployerId,
                        EmployeeNumber = emp.EmployeeNumber,
                        Employee = emp,
                        RelatedEmployeeNumber = theSpouse.EmployeeNumber,
                        ContactTypeCode = "SPOUSE",
                        ContactTypeName = "Spouse",
                        YearsOfAge = theSpouse.DoB.HasValue ? (double?)theSpouse.DoB.Value.AgeInYears() : null
                    };

                    spouseContact.Save();
                }
            }
        } // ValidateSpouseData

        /// <summary>
        /// Validate an employee performs the following:
        /// 
        /// Check to ensure it is not a duplicate employee record, check the following fields to ensure they don’t duplicate another Employee in the DB:
        ///     EmployerId + EmployeeNumber (if EmployeeNumber is not null)
        ///     EmployerId + FirstName + LastName + DOB (if DOB is not null)
        ///     EmployerId + Ssn (if Ssn is not null)
        ///     EmployerId + EmployeeInfo.Email
        ///     
        /// </summary>
        /// <param name="emp">The employee being added</param>
        public void ValidateUniqueEmployee(Employee emp, OptionalOut<string> outResult = null)
        {
            // make sure they have an employer, can't validate without out
            if (string.IsNullOrWhiteSpace(emp.EmployerId))
                throw new AbsenceSoftException("Employer was not able to be identified.");

            var query = Query.And(Employee.Query.IsNotDeleted(), Query.EQ("EmployeeNumber", emp.EmployeeNumber), Query.EQ("EmployerId", new BsonObjectId(new ObjectId(emp.EmployerId))), Query.EQ("CustomerId", new BsonObjectId(new ObjectId(emp.CustomerId))));
            var existingEmployee = Employee.Repository.Collection.Find(query).FirstOrDefault();
            if (existingEmployee != null && !existingEmployee.IsDeleted && existingEmployee.Id != emp.Id)
                throw new AbsenceSoftException("An employee with this Employee Number already exists.");
            else if (existingEmployee != null && existingEmployee.IsDeleted && outResult != null)
            {
                outResult.Result = existingEmployee.Id;
            }

            string fName = (emp.FirstName ?? "").ToLowerInvariant();
            string lName = (emp.LastName ?? "").ToLowerInvariant();

            if (emp.DoB.HasValue)
            {
                if (Employee.AsQueryable()
                    .Where(e => e.EmployerId == emp.EmployerId &&
                                e.FirstName.ToLowerInvariant() == fName &&
                                e.LastName.ToLowerInvariant() == lName &&
                                e.DoB == emp.DoB &&
                                e.Id != emp.Id &&
                                e.EmployeeNumber == emp.EmployeeNumber).Count() > 0)
                {
                    throw new AbsenceSoftException("An employee with this First Name, Last Name and DoB already exits.");
                }
            }
           

            // verify by ssn (if there is an ssn. apparently it is OK to not have an ssn)
            if (emp.Ssn != null && !string.IsNullOrWhiteSpace(emp.Ssn.PlainText) && Employee.AsQueryable().Where(e => e.EmployerId == emp.EmployerId && e.Id != emp.Id && e.Ssn.Hashed == emp.Ssn.Hashed).Count() > 0)
                throw new AbsenceSoftException("An employee with this Federal Identification/Social Security Number already eixsts.");

        } // ValidateNewEmployee

        /// <summary>
        /// Takes a list of schedules and makes sure that a schedule
        /// creates a circle (sun - sat, tues - mon, etc), that the 
        /// start and end dates don't overlap
        /// </summary>
        /// <param name="sched">The Schedule list</param>
        /// <returns>true if valid, false if not</returns>
        public bool AreSchedulesValid(List<Schedule> sched)
        {
            // first assumption, they should have a work schedule
            if (sched == null || !sched.Any())
                return true;

            // track the start and end dates on the schedules
            DateTime beginDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            // make sure the schedule is in start date order
            sched = sched.OrderBy(o => o.StartDate).ToList();

            // check that there are times for each scheudle and then check that each
            // schedule makes sense
            foreach (Schedule s in sched)
            {
                if ((s.Times == null || s.Times.Count == 0) && s.ScheduleType != ScheduleType.FteVariable)
                    return false;

                // check that the start date of this schedule does not overlap with any other
                // compare the previous start and end with this start and end
                if ((s.StartDate >= beginDate && s.StartDate <= endDate) || (s.EndDate >= beginDate && s.EndDate <= endDate))
                    return false;

                // make sure there is one day between the previous end date and the next start date
                // i.e. no gaps on the schedule
                // on the first pass endate is mindate so don't test
                if (endDate != DateTime.MinValue && endDate.AddDays(1) != s.StartDate)
                    return false;

                // then set the dates for the next pass and to use in the following times check
                beginDate = s.StartDate;
                endDate = s.EndDate ?? DateTime.MaxValue;

                // if it is a weekly type/variable schedule then we have some other rules
                // first there must be 7 days and it must start on a Sunday
                if ((s.ScheduleType == Data.Enums.ScheduleType.Weekly || s.ScheduleType == ScheduleType.Variable) &&
                    (s.Times.Count != 7 || s.Times[0].SampleDate.DayOfWeek != DayOfWeek.Sunday))
                    return false;

                // if there is only one in the list then it must be good (
                if (s.Times.Count == 1 || s.ScheduleType == ScheduleType.FteVariable)
                    continue;

                // now make sure all the days are in a row, we'll sort them just in case
                List<Time> checkThese = s.Times.OrderBy(o => o.SampleDate).ToList();
                DateTime checkDate = checkThese[0].SampleDate;
                for (int x = 1; x < checkThese.Count; x++)
                    if (checkThese[x].SampleDate != checkDate.AddDays(x))
                        return false;

            } // foreach (Schedule s in sched)

            // made it to the end so it must be a good set of schedules
            return true;
        } // AreSchedulesValid

        /// <summary>
        /// Gets the list of contacts of an employee.
        /// </summary>
        /// <param name="employeeId">The employee Id to get the contacts for</param>
        /// <returns>A list of contacts of this employee</returns>
        /// <exception cref="System.Exception">Thrown when the user does not have access to the create employee contact.</exception>
        public List<EmployeeContact> GetContacts(string employeeId, string сontactTypeCode = null)
        {
            using (new InstrumentationContext("EmployeeService.GetContacts"))
            {
                if (сontactTypeCode == null)
                {
                    return EmployeeContact.AsQueryable()
                     .Where(a => a.EmployeeId == employeeId)
                     .OrderBy(a => a.CreatedDate)
                     .ToList();
                }
                else
                {
                    return EmployeeContact.AsQueryable()
                    .Where(a => a.EmployeeId == employeeId && a.ContactTypeCode == сontactTypeCode)
                    .OrderBy(a => a.CreatedDate)
                    .ToList();
                }

            }
        } // GetContacts

        /// <summary>
        /// Returns the specific employee contact
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public EmployeeContact GetContact(string contactId)
        {
            return EmployeeContact.GetById(contactId);
        }

        /// <summary>
        /// Saves the specific employee contact
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public EmployeeContact SaveContact(EmployeeContact contact)
        {
            return contact.Save();
        }


        /// <summary>
        /// Deletes the specific employee contact
        /// </summary>
        /// <param name="contactId"></param>
        public void DeleteContact(string contactId)
        {
            EmployeeContact contact = EmployeeContact.GetById(contactId);
            if (contact != null)
                contact.Delete();
        }

        public List<EmployeeContact> GetMedicalContacts(string customerId, string employeeId)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new AbsenceSoftException("GetContactTypes: customer is null");
            if (string.IsNullOrEmpty(employeeId))
                throw new AbsenceSoftException("GetContactTypes: employee is null");

            using (new InstrumentationContext("ContactTypeService.GetMedicalDesignationTypeContacts"))
            {
                // get medical contact types
                var medicalContactTypesQuery = ContactType.Query.Find(ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Medical));
                var medicalContactTypeCodes = medicalContactTypesQuery.ToList<ContactType>().Select(ct => ct.Code).ToArray();

                // get employee medical contacts
                var employeeContactsQuery = EmployeeContact.Query.Find(
                    EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, customerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, employeeId),
                        EmployeeContact.Query.In(e => e.ContactTypeCode, medicalContactTypeCodes)
                ));
                return employeeContactsQuery.OrderBy(a => a.CreatedDate).ToList();
            }
        }

        public Employee GetEmployee(string employeeId)
        {
            return Employee.GetById(employeeId);
        } // GetEmployee

        /// <summary>
        /// Deletes an employee and all related data. This is a HUGE deal, so this shouldn't be taken lightly.
        /// We keep deleted data via audit, but still, it would be a pain to restore that data, and be sure you got
        /// all of it and the correct version of it, etc.
        /// </summary>
        /// <param name="employeeId">The Id of the employee to be </param>
        public void DeleteEmployee(string employeeId)
        {
            using (new InstrumentationContext("EmployeeService.DeleteEmployee"))
            {
                if (string.IsNullOrWhiteSpace(employeeId))
                    throw new ArgumentNullException("employeeId");
                if (User.Current != null && employeeId == User.Current.Id)
                    throw new AbsenceSoftException("You cannot delete yourself");

                Employee emp = Employee.GetById(employeeId);

                if (emp == null)
                    throw new ArgumentNullException("employeeId", "Employee was not found");
                if (User.Current != null && !User.Current.HasEmployerAccess(emp.EmployerId, Permission.DeleteEmployee))
                    throw new AbsenceSoftException("You do not have access to this employee to remove them");

                // This is a NO-NO, no matter what, so throw a single exception here first.
                if (Case.Exists(c => c.Employee.Id == employeeId))
                    throw new AbsenceSoftException("You may not delete an employee with any cases");

                List<string> errors = new List<string>();

                // While not the end of the world, if the employee is a self service user, or a power user with access, we don't want to nuke them out for no good reason.
                var users = User.AsQueryable().Where(u => u.Employers.Any(e => e.EmployeeId == employeeId) && u.DisabledDate == null).ToList();
                if (users.Any())
                    errors.AddRange(users.Select(u => string.Format("This employee has thier own user account that is still active, '{0}', please deactivate that user account first.", u.Email)));

                // Make sure there are no spouses for this employee.... why a list, dunno, maybe the employee is in Utah.
                var spouses = Employee.AsQueryable().Where(e => e.CustomerId == emp.CustomerId && e.EmployerId == emp.EmployerId && e.SpouseEmployeeNumber == emp.EmployeeNumber).ToList();
                if (spouses.Any())
                    errors.AddRange(spouses.Select(s => string.Format("Employee is set as the spouse of {0}, Employee # {1}. Please edit that employee first, removing this employee as the spouse.", s.FullName, s.EmployeeNumber)));

                // Make sure there are no other employee contacts where this employee is named as a contact, such as an HR, Supervisor, etc.
                var caseContacts = EmployeeContact.AsQueryable().Where(c => c.CustomerId == emp.CustomerId && c.EmployerId == emp.EmployerId && c.RelatedEmployeeNumber == emp.EmployeeNumber).ToList();
                if (caseContacts.Any())
                    errors.AddRange(caseContacts.Select(c => string.Format("Employee is a '{2}' contact of {0}, Employee # {1}. Please edit that employee first, removing this employee as the '{2}' contact.", c.Employee.FullName, c.Employee.EmployeeNumber, c.ContactTypeCode)));

                // If any errors occurred, then let's throw our aggregate exception.
                if (errors.Any())
                    throw new AbsenceSoftAggregateException(errors.ToArray());

                // Delete our employee.
                emp.Delete();
                // BOOM! Done! Employee is GONE!

                // Now let's delete or modify anything that could even remotely be possibly related to the employee
                AbsenceSoft.Data.Customers.EmployeeContact.AsQueryable().Where(e => e.CustomerId == emp.CustomerId && e.EmployerId == emp.EmployerId && (e.EmployeeId == employeeId || e.RelatedEmployeeNumber == emp.EmployeeNumber)).ToList().ForEach(e => e.Delete());
                AbsenceSoft.Data.Notes.EmployeeNote.AsQueryable().Where(e => e.EmployeeId == employeeId).ToList().ForEach(e => e.Delete());
                User.AsQueryable().Where(u => u.Employers.Any(e => e.EmployeeId == employeeId)).ToList().ForEach(e => { if (e.Employers != null && e.Employers.Count > 0) e.Employers.ForEach(r => r.EmployeeId = null); e.Save(); });
            }
        } // DeleteEmployee

        /// <summary>
        /// Before creating case for employee, check he has work schedule and work state
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public List<string> IsEmployeeInfoComplete(string employeeId)
        {
            List<string> incompleteFields = new List<string>();

            var emp = Employee.GetById(employeeId);
            if (emp == null || emp.WorkSchedules == null || !emp.WorkSchedules.Any())
                incompleteFields.Add("Work Schedule");
            if (emp == null || string.IsNullOrWhiteSpace(emp.WorkState))
                incompleteFields.Add("Work State");

            return incompleteFields;
        } // IsEmployeeInfoComplete

        /// <summary>
        /// Get Employee by email address 
        /// </summary>
        /// <param name="employerId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public Employee GetEmployeeByEmail(string employerId, string email)
        {
            if (string.IsNullOrWhiteSpace(employerId))
                throw new ArgumentNullException("employerId");

            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email");

            using (new InstrumentationContext("EmployeeService.GetEmployeeByEmail"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));

                ands.Add(Employee.Query.EQ(e => e.Info.Email, email.ToLowerInvariant()));

                var employee = Employee.Query.Find(Employee.Query.And(ands)).FirstOrDefault();

                return employee;
            }
        }

        /// <summary>
        /// Get Employee by only email address 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Boolean GetEmployeeOnlyByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email");

            using (new InstrumentationContext("EmployeeService.GetEmployeeByEmail"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();

                ands.Add(Employee.Query.EQ(e => e.Info.Email, email.ToLowerInvariant()));

                var employee = Employee.Query.Find(Employee.Query.And(ands)).FirstOrDefault();
                if (employee != null && employee.Info != null)
                    return employee.EmployerId == Employer.CurrentId;
                return false;
            }
        }

        public List<VariableScheduleTime> GetEmployeeVariableSchedule(string employeeId, DateTime startDate, DateTime endDate)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
                throw new ArgumentException("You must provide the employee id");

            using (new InstrumentationContext("EmployeeService.GetEmployeeVariableSchedule"))
            {
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(User.Current.BuildDataAccessFilters());

                ands.Add(VariableScheduleTime.Query.NotExists(vst => vst.EmployeeCaseId));
                ands.Add(VariableScheduleTime.Query.EQ(vst => vst.IsDeleted, false));
                ands.Add(VariableScheduleTime.Query.EQ(vst => vst.EmployeeId, employeeId));
                ands.Add(VariableScheduleTime.Query.GTE(vst => vst.Time.SampleDate, startDate));
                ands.Add(VariableScheduleTime.Query.LTE(vst => vst.Time.SampleDate, endDate));

                return VariableScheduleTime.Query.Find(VariableScheduleTime.Query.And(ands)).ToList();
            }
        }

        public void UpdateEmployeeNumber(Employee emp, string originalEmployeeNumber, string newEmployeeNumber)
        {
            var derivedTypes = from t in System.Reflection.Assembly.GetAssembly(typeof(Employee)).GetTypes()
                               where (t.BaseType != null && (t.BaseType.Name == "BaseEmployeeEntity`1" || t.BaseType.Name == "BaseEmployeeNumberEntity`1"))
                               select t;
            foreach (Type type in derivedTypes)
            {
                var getMethod = type.GetMethod("UpdateEmployeeNumber", System.Reflection.BindingFlags.FlattenHierarchy | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
                switch (type.BaseType.Name)
                {
                    case "BaseEmployeeEntity`1":
                        getMethod.Invoke(null, new object[] { emp.Id, originalEmployeeNumber, newEmployeeNumber, CurrentUser.Id, emp.CustomerId, emp.EmployerId });
                        break;
                    case "BaseEmployeeNumberEntity`1":
                        getMethod.Invoke(null, new object[] { originalEmployeeNumber, newEmployeeNumber, CurrentUser.Id, emp.CustomerId, emp.EmployerId });
                        break;
                }
            }
            Employee.UpdateEmployeeNumber(originalEmployeeNumber, newEmployeeNumber, CurrentUser.Id, emp.CustomerId, emp.EmployerId);
            Case.UpdateEmployeeNumber(emp.Id, originalEmployeeNumber, newEmployeeNumber, CurrentUser.Id, emp.CustomerId, emp.EmployerId);
            EmployeeOrganization.UpdateEmployeeNumber(originalEmployeeNumber, newEmployeeNumber, CurrentUser.Id, emp.CustomerId, emp.EmployerId);
        }

        public List<VariableScheduleTime> GetVariableScheduleTime(string employeeId)
        {
            return VariableScheduleTime.AsQueryable().Where(e => e.EmployeeId == employeeId).ToList();
        }

    } // public class EmployeeService : LogicService, ILogicService
} // namespace
