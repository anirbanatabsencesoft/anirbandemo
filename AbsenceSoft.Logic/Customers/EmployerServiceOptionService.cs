﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// This class handles employer service options.
    /// </summary>
    public class EmployerServiceOptionService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerServiceOptionService"/> class.
        /// </summary>
        public EmployerServiceOptionService()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerServiceOptionService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public EmployerServiceOptionService(User currentUser)
            : base(currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerServiceOptionService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public EmployerServiceOptionService(string customerId, string employerId, User currentUser = null)
            : base(customerId, employerId, currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerServiceOptionService"/> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="u">The u.</param>
        public EmployerServiceOptionService(Customer customer, Employer employer, User u)
            : base(customer, employer, u)
        {

        }

        /// <summary>
        /// Determines whether or not the customer has multi-employer access (a.k.a TPA)
        /// </summary>
        /// <returns>true if the client has multiple employers, false if not.</returns>
        public bool IsTpaCustomerWithEmployerServiceOptions()
        {
            return CurrentCustomer.HasFeature(Data.Enums.Feature.MultiEmployerAccess) 
                && CurrentCustomer.HasFeature(Data.Enums.Feature.EmployerServiceOptions);
        }

        /// <summary>
        /// Get the employer specific service option by key.
        /// </summary>
        /// <param name="employerId">The employer to find.</param>
        /// <param name="key">The key of the service option.</param>
        /// <returns>An EmployerServiceOption object.</returns>
        public EmployerServiceOption GetEmployerServiceOption(string employerId, string key = "ServiceOptionIndicator")
        {
            if (string.IsNullOrWhiteSpace(employerId)
                || string.IsNullOrWhiteSpace(key)
                || !IsTpaCustomerWithEmployerServiceOptions())
                return null;

            return EmployerServiceOption.AsQueryable()
                    .SingleOrDefault(p => p.EmployerId == employerId
                        && p.Key == key);
        }
        
        /// <summary>
        /// Get the employer service option value by key.
        /// </summary>
        /// <param name="employerId">The employer to find.</param>
        /// <param name="key">The key of the service option.</param>
        /// <returns>The value of the service option.</returns>
        public string GetCustomerServiceOptionId(string employerId, string key = "ServiceOptionIndicator")
        {
            var employerServiceOption = GetEmployerServiceOption(employerId, key);
            return employerServiceOption == null
                ? null
                : employerServiceOption.CusomerServiceOptionId;
        }

        /// <summary>
        /// Gets a dictionary of all customer service option id's by employer id.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <returns>A Dictionary.</returns>
        public Dictionary<string, string> GetCustomerServiceOptionIdDictionary(string key = "ServiceOptionIndicator")
        {
            if (string.IsNullOrWhiteSpace(key)
                || !IsTpaCustomerWithEmployerServiceOptions())
                return new Dictionary<string, string>();

            return EmployerServiceOption.AsQueryable()
                    .Where(p => p.Key == key)
                    .ToDictionary(p => p.EmployerId, p => p.CusomerServiceOptionId);
        }

        /// <summary>
        /// Inserts or updates the employer service option and customer service option id for an employer.
        /// </summary>
        /// <param name="employerId">The employer to find.</param>
        /// <param name="key">The key of the service option.</param>
        /// <param name="cusomerServiceOptionId">The id of the customer service option.</param>
        public void UpsertEmployerServiceOption(string employerId, string key, string cusomerServiceOptionId)
        {
            using (new InstrumentationContext("EmployerServiceOptionService.UpsertEmployerServiceOptionValue"))
			{
                if (!IsTpaCustomerWithEmployerServiceOptions())
                    return;

                var employer = Employer.GetById(employerId);
                if (employer == null)
                    throw new Exception("Invalid employee passed into method.");

                var employerServiceOption = GetEmployerServiceOption(employerId, key)
                    ?? new EmployerServiceOption
                    {
                        Employer = employer,
                        Customer = CurrentUser.Customer,
                        CreatedById = CurrentUser.Customer.CreatedById,
                        ModifiedById = CurrentUser.Customer.ModifiedById
                    };
                employerServiceOption.Key = key;
                employerServiceOption.CusomerServiceOptionId = cusomerServiceOptionId;
                employerServiceOption.Save();
            }
        }
    }
}
