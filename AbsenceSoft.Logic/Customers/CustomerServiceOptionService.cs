﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// This class handles customer service options.
    /// </summary>
    public class CustomerServiceOptionService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerServiceOptionService"/> class.
        /// </summary>
        public CustomerServiceOptionService()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerServiceOptionService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public CustomerServiceOptionService(User currentUser)
            : base(currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerServiceOptionService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public CustomerServiceOptionService(string customerId, string employerId, User currentUser = null)
            : base(customerId, employerId, currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerServiceOptionService"/> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="u">The u.</param>
        public CustomerServiceOptionService(Customer customer, Employer employer, User u)
            : base(customer, employer, u)
        {

        }

        /// <summary>
        /// Determines whether or not the customer has employer service options feature access.
        /// </summary>
        /// <returns>true if the client has access, false if not.</returns>
        public bool CanAccessServiceOptions()
        {
            return CurrentUser.Customer.HasFeature(Data.Enums.Feature.EmployerServiceOptions);
        }

        /// <summary>
        /// Get the customer specific service option by key.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <returns>A list of CustomerServiceOption objects.</returns>
        public List<CustomerServiceOption> GetCustomerServiceOptions(string key = "ServiceOptionIndicator")
        {
            if (string.IsNullOrWhiteSpace(key)
                || !CanAccessServiceOptions())
                return new List<CustomerServiceOption>(0);

            return CustomerServiceOption.AsQueryable()
                    .Where(p => p.Key == key)
                    .ToList();
        }

        /// <summary>
        /// Get the customer specific customer service option by key and id.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <param name="id">The id of the service option.</param>
        /// <returns>An CustomerServiceOption object.</returns>
        public CustomerServiceOption GetCustomerServiceOptionById(string key, string id)
        {
            if (string.IsNullOrWhiteSpace(id)
                    || !CanAccessServiceOptions())
                return null;

            var customer = CurrentUser.Customer;
            if (customer == null)
                throw new Exception("Invalid customer initialized with service.");

            return CustomerServiceOption.AsQueryable()
                .FirstOrDefault(p => p.Key == key && p.Id == id);
        }

        /// <summary>
        /// Get the customer specific customer service option by key and value.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <param name="value">The value of the service option.</param>
        /// <returns>An CustomerServiceOption object.</returns>
        public CustomerServiceOption GetCustomerServiceOption(string key, string value)
        {
            return GetCustomerServiceOptions(key)
                .FirstOrDefault(p => p.Value == value);
        }

        /// <summary>
        /// Gets list results from a set of criteria.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <param name="criteria">The criteria of the service option.</param>
        /// <returns></returns>
        public ListResults ListCustomerServiceOptions(string key, ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            var results = new ListResults(criteria);

            var ands = new List<IMongoQuery> { CurrentUser.BuildDataAccessFilters() };
            var value = criteria.Get<string>("Value");

            CustomerServiceOption.Query.MatchesString(p => p.Key, key, ands);
            CustomerServiceOption.Query.MatchesString(p => p.Value, value, ands);

            var query = CustomerServiceOption.Query.Find(CustomerServiceOption.Query.And(ands));
            query = criteria.SetSortAndSkip(query);

            results.Total = query.Count();
            results.Results = query.ToList()
                .Select(p => new ListResult()
                    .Set("Id", p.Id)
                    .Set("Key", p.Key)
                    .Set("Value", p.Value)
                    .Set("Order", p.Order));

            return results;
        }

        /// <summary>
        /// Inserts or updates the customer service option for a customer.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <param name="value">The value of the service option.</param>
        /// <param name="order">The order the value should be displayed in a user interface.</param>
        public void UpsertCustomerServiceOptionValue(string key, string value, int order)
        {
            using (new InstrumentationContext("CustomerServiceOptionService.UpsertCustomerServiceOptionValue"))
			{
                if (!CanAccessServiceOptions())
                    return;

                var customer = CurrentUser.Customer;
                if (customer == null)
                    throw new Exception("Invalid customer initialized with service.");

                var customerServiceOption = GetCustomerServiceOption(key, value)
                    ?? new CustomerServiceOption
                    {
                        Customer = CurrentUser.Customer,
                        CreatedById = CurrentUser.Customer.CreatedById,
                        ModifiedById = CurrentUser.Customer.ModifiedById
                    };
                customerServiceOption.Key = key;
                customerServiceOption.Value = value;
			    customerServiceOption.Order = order;
                customerServiceOption.Save();
            }
        }

        /// <summary>
        /// Deletes the customer service option for a customer.
        /// </summary>
        /// <param name="key">The key of the service option.</param>
        /// <param name="id">The id of the service option.</param>
        public void DeleteCustomerServiceOptionValue(string key, string id)
        {
            using (new InstrumentationContext("CustomerServiceOptionService.DeleteCustomerServiceOptionValue"))
            {
                var customerServiceOption = GetCustomerServiceOptionById(key, id);
                if (customerServiceOption == null)
                    return;

                customerServiceOption.Delete();
            }
        }
    }
}
