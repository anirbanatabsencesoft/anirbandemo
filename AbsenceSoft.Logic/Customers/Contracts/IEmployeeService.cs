﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Customers.Contracts
{
    public interface IEmployeeService
    {
        Employee Update(Employee emp, Schedule newWorkSchedule = null, List<VariableScheduleTime> variableTimes = null);

        Employee GetEmployeeByEmployeeNumber(string employeeNumber, string customerId, string employerId);

        void DeleteEmployee(string employeeId);

        List<EmployeeContact> GetContacts(string employeeId, string сontactTypeCode = null);

        EmployeeContact GetContact(string contactId);

        EmployeeContact SaveContact(EmployeeContact contact);

        void DeleteContact(string contactId);
        void SetWorkSchedule(Employee emp, Schedule newWorkSchedule, List<VariableScheduleTime> variableTime);

        List<VariableScheduleTime> GetVariableScheduleTime(string employeeId);

        List<EmployeeContact> GetMedicalContacts(string customerId, string employeeId);       
    }
}
