﻿using System.Collections.Generic;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Logic.Customers.Contracts
{
    public interface IOrganizationService
    {
        List<EmployeeOrganization> GetEmployeeOrganizationsList(Employee employee);
        EmployeeOrganization UpdateOrganization(EmployeeOrganization organization);
        EmployeeOrganization GetEmployeeOrganizationByCode(Employee employee, string orgCode);

        IList<Organization> GetOrganizations(string employerId, string customerId, string code = null, bool inactive = false);

        OrganizationType GetOrganizationType(string employerId, string orgTypeCode);

        Organization SaveOrgnization(Organization organization);
    }
}
