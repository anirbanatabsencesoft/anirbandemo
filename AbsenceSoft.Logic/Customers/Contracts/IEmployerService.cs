﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Customers.Contracts
{
    public interface IEmployerService
    {
        List<CustomField> GetCustomFields(EntityTarget? target = null, bool? collectedAtIntake = null);
        Employer GetById(string id);
        Employer GetEmployerByReferenceCode(string employerReferenceCode);
        string GetEmployerIdByReferenceCode(string employerReferenceCode);
    }
}