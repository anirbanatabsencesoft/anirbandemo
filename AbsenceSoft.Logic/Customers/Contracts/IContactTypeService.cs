﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Customers.Contracts
{
    public interface IContactTypeService
    {
        List<ContactType> GetContactTypes(string customerId, string employerId, params ContactTypeDesignationType[] category);

        List<ContactType> GetMedicalContactTypes(string employerId);

        void UpdateMedicalDesignationTypeContactsIsPrimary(string customerId, Guid? contactId, string employeeId);
    }
}
