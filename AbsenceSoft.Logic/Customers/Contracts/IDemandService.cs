﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers.Contracts
{
    public interface IDemandService
    {
        List<EmployeeRestriction> GetJobRestrictionsForEmployee(string employeeId, string caseId = null);
        void SaveJobRestriction(EmployeeRestriction employeeRestriction);
        void DeleteJobRestriction(string workRestrictionId);
        EmployeeRestriction GetEmployeeRestrictionById(string id);
        List<DemandType> GetDemandTypes(string CustomerId, string EmployerId);

        Demand GetDemandByCode(string demandCode, string customerId);

        DemandType GetDemandTypeByCode(string demandTypeCode, string customerId);
    }
}