﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Customers
{
    public class SearchService : LogicService, ILogicService
    {
        public SearchResults EmployeeCaseSearch(ListCriteria criteria)
        {
            var searchTerm = criteria.Get<String>("SearchText");
            var employers = criteria.Get<String[]>("Employers");
            var lastName = criteria.Get<String>("LastName");
            var dateOfBirth = criteria.Get<DateTime?>("DateOfBirth");
            // AT-6262 - Add employee's middle initial in search results - search results show up in alphabetical order by first name
            var sortBy = criteria.SortBy == null ? "fn" : criteria.SortBy;

            var lowerSearchTerm = searchTerm.Trim().ToLowerInvariant();
            List<IMongoQuery> ands = new List<IMongoQuery>();
            int caseNumber;
            bool isMultiEmployer = User.Current.Employers.Count > 1;

            SearchResults results = new SearchResults(criteria);

            ands = ApplyFilters(ands, employers, lastName, dateOfBirth);

            Int32.TryParse(searchTerm, out caseNumber);

            ObjectId oId;
            SearchResults searchResults = new SearchResults(criteria);
            if (ObjectId.TryParse(lowerSearchTerm, out oId))
            {
                if (results.Criteria.PageNumber == 1)
                {
                    searchResults = AddCaseIdResults(lowerSearchTerm, searchResults, ands, isMultiEmployer, true);
                    results.FacetResults = searchResults.Results;
                }
                ands.Clear();
                ands = ApplyFilters(ands, employers, lastName, dateOfBirth);
                results = AddCaseIdResults(lowerSearchTerm, results, ands, isMultiEmployer, false);
                results.Total = results.FacetResults.Count();
            }
            else
            {
                if (results.Criteria.PageNumber == 1)
                {
                    searchResults = AddEmployeeResults(lowerSearchTerm, searchResults, ands, true);
                    results.FacetResults = searchResults.Results;
                }
                ands.Clear();
                ands = ApplyFilters(ands, employers, lastName, dateOfBirth);
                results = AddEmployeeResults(lowerSearchTerm, results, ands, false);
                results.Total = results.FacetResults.Count();
            }

            if (searchResults.Results == null || searchResults.Results.Count() == 0)
            {
                results.Total = results.Results.Count();
            }

            results.Results = SortCasesAndEmployees(results.Results.ToList(), sortBy); ;

            return results;
        }

        /// <summary>
        /// Applies filters based on the options selected
        /// </summary>
        /// <param name="ands"></param>
        /// <param name="employers"></param>
        /// <param name="lastName"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        private List<IMongoQuery> ApplyFilters(List<IMongoQuery> ands, string[] employers, string lastName, DateTime? dateOfBirth)
        {
            if (employers != null)
                ands.Add(Employee.Query.In(e => e.EmployerName, employers));

            if (!string.IsNullOrEmpty(lastName))
                ands.Add(Employee.Query.EQIgnoreCase(e => e.LastName, lastName));

            if (dateOfBirth != null)
                ands.Add(Employee.Query.EQ(e => e.DoB, dateOfBirth));

            return ands;
        }

        /// <summary>
        /// Searches by case id
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="results"></param>
        /// <param name="ands"></param>
        /// <param name="isMultiEmployer"></param>
        /// <returns></returns>
        private SearchResults AddCaseIdResults(string searchTerm, SearchResults results, List<IMongoQuery> ands, bool isMultiEmployer, bool countFlag)
        {
            // Always add the employer id to the filter
            if (AbsenceSoft.Data.Security.User.Current != null)
                ands.Add(User.Current.BuildDataAccessFilters(Permission.ViewCase, "Employee._id"));

            ands.Add(Case.Query.EQ(c => c.Id, searchTerm));
            ands.Add(Case.Query.IsNotDeleted());

            var caseList = new List<BsonDocument>();
            if (!countFlag)
            {
                ListCriteria criteria = results.Criteria;
                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = Case.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Case.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);
                caseList = query.ToList();
            }
            else
            {
                var query = Case.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Case.Query.And(ands) : null);
                caseList = query.ToList();
            }

            results.Results = caseList.Select(e =>
            {
                var eCase = BsonSerializer.Deserialize<Case>(e);
                var lr = new SearchResult();
                lr.SearchText = searchTerm;
                lr.Id = eCase.Id;
                lr.CaseNumber = eCase.CaseNumber;
                lr.EmployeeId = eCase.Employee.Id;
                lr.EmployerId = eCase.EmployerId;
                lr.Employer = eCase.EmployerName;
                lr.CustomerId = eCase.CustomerId;
                lr.Title = isMultiEmployer ? string.Format("Case: {0} | {1}", eCase.Title, eCase.EmployerName) : string.Format("Case: {0}", eCase.Title);
                lr.Description = eCase.Employee.GetDescriptionForSearchResult(includeTitle: true);
                lr.SetUrl(SearchResultType.Case);
                return lr;
            });

            return results;
        }

        /// <summary>
        /// Searches by employee
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="result"></param>
        /// <param name="ands"></param>
        /// <returns></returns>
        private SearchResults AddEmployeeResults(string searchTerm, SearchResults result, List<IMongoQuery> ands, bool countFlag)
        {
            // Always add the employer id to the filter
            if (AbsenceSoft.Data.Security.User.Current != null)
                ands.Add(User.Current.BuildDataAccessFilters(Permission.ViewEmployee, "_id"));

            string lName = string.Empty;
            string fName = string.Empty;
            string lName_2 = string.Empty;
            string fName_2 = string.Empty;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                if (searchTerm.Contains(',') || searchTerm.Contains(' '))
                {
                    string[] arrName = searchTerm.Split(',');
                    if (arrName.Length > 1)
                    {
                        lName = arrName[0].Trim();
                        fName = arrName[1].Trim();

                        fName_2 = arrName[0].Trim();
                        lName_2 = arrName[1].Trim();
                    }
                    else
                    {
                        arrName = searchTerm.Split(' ');
                        if (arrName.Length > 1)
                        {
                            lName = arrName[0].Trim();
                            fName = arrName[1].Trim();

                            fName_2 = arrName[0].Trim();
                            lName_2 = arrName[1].Trim();
                        }
                    }
                    ands.Add(Employee.Query.Or(
                       Employee.Query.And(
                       Employee.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(lName), RegexOptions.IgnoreCase))),
                       Employee.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(fName), RegexOptions.IgnoreCase)))),
                       Employee.Query.And(
                       Employee.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(lName_2), RegexOptions.IgnoreCase))),
                       Employee.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + Regex.Escape(fName_2), RegexOptions.IgnoreCase)))
                       )));
                }
                else
                {
                    string safeSearchTerm = Regex.Escape(searchTerm);
                    ands.Add(Employee.Query.Or(
                        Employee.Query.Matches(e => e.LastName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + safeSearchTerm, RegexOptions.IgnoreCase))),
                        Employee.Query.Matches(e => e.FirstName, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + safeSearchTerm, RegexOptions.IgnoreCase))),
                        Employee.Query.Matches(e => e.Info.Email, new MongoDB.Bson.BsonRegularExpression(new Regex("^" + safeSearchTerm, RegexOptions.IgnoreCase))),
                        Employee.Query.Matches(e => e.EmployeeNumber, new MongoDB.Bson.BsonRegularExpression(new Regex(safeSearchTerm, RegexOptions.IgnoreCase)))
                    ));
                }
            }
            ands.Add(Employee.Query.IsNotDeleted());

            var employeeList = new List<BsonDocument>();

            if (!countFlag)
            {
                ListCriteria criteria = result.Criteria;
                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var employeeQuery = Employee.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Employee.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);
                employeeList = employeeQuery.ToList();
            }
            else
            {
                var employeeQuery = Employee.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Employee.Query.And(ands) : null);
                employeeList = employeeQuery.ToList();
            }

            string[] employeeIds = employeeList.Select(e => e.GetRawValue<string>("_id")).ToArray();

            ands.Clear();
            // Always add the employer id to the filter
            if (AbsenceSoft.Data.Security.User.Current != null)
                ands.Add(User.Current.BuildDataAccessFilters());

            ands.Add(Case.Query.Or(
                Case.Query.Matches(c => c.CaseNumber, searchTerm),
                Case.Query.In(c => c.Employee.Id, employeeIds)
                ));
            ands.Add(Case.Query.IsNotDeleted());

            var caseQuery = Case.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Employee.Query.And(ands) : null);
            var caseList = caseQuery.ToList();

            List<string> employees = new List<string>();
            employees.AddRange(employeeList.Select(e => BsonSerializer.Deserialize<Employee>(e).EmployeeNumber));
            employees.AddRange(caseList.Select(c => BsonSerializer.Deserialize<Case>(c).Employee.EmployeeNumber));

            List<EmployeeOrganization> employeeOrganizations = EmployeeOrganization.AsQueryable().Where(o => o.CustomerId == CurrentUser.CustomerId && employees.Contains(o.EmployeeNumber) && o.TypeCode == OrganizationType.OfficeLocationTypeCode).ToList();

            result.Results = employeeList.Select(e =>
            {
                var emp = BsonSerializer.Deserialize<Employee>(e);
                emp.SetOfficeLocationDisplayName(employeeOrganizations.Where(o => o.EmployeeNumber == emp.EmployeeNumber && o.EmployerId == emp.EmployerId && o.CustomerId == emp.CustomerId).ToList());
                var lr = new SearchResult();
                lr.SearchText = searchTerm;
                lr.Id = emp.Id;
                lr.EmployeeId = emp.Id;
                lr.EmployerId = emp.EmployerId;
                lr.Employer = emp.EmployerName;
                lr.CustomerId = emp.CustomerId;
                lr.CaseNumber = "NA";
                lr.DOB = emp.DoB;
                lr.FirstName = emp.FirstName;
                lr.LastName = emp.LastName;
                lr.EmployeeNumber = emp.EmployeeNumber;
                lr.Title = string.Format("{0} | {1}", emp.SearchTitle, emp.EmployerName);
                lr.Description = emp.GetDescriptionForSearchResult();
                lr.SetUrl(SearchResultType.Employee);
                return lr;
            });

            result.Results = result.Results.Concat(caseList.Select(c =>
            {
                var eCase = BsonSerializer.Deserialize<Case>(c);
                eCase.Employee.SetOfficeLocationDisplayName(employeeOrganizations.Where(o => o.EmployeeNumber == eCase.Employee.EmployeeNumber && o.EmployerId == eCase.Employee.EmployerId && o.CustomerId == eCase.Employee.CustomerId).ToList());
                var lr = new SearchResult();
                lr.SearchText = searchTerm;
                lr.Id = eCase.Id;
                lr.CaseNumber = eCase.CaseNumber;
                lr.EmployeeId = eCase.Employee.Id;
                lr.EmployerId = eCase.EmployerId;
                lr.Employer = eCase.EmployerName;
                lr.CustomerId = eCase.CustomerId;
                lr.Title = string.Format("Case: {0}  |  {1} | {2}  |  {3} - {4}  |  {5}",
                        eCase.CaseNumber,
                        eCase.Reason.Name,
                        eCase.Status,
                        eCase.StartDate.ToShortDateString(),
                        eCase.EndDate.HasValue ? eCase.EndDate.Value.ToShortDateString() : string.Empty,
                        eCase.EmployerName);
                lr.Description = eCase.Employee.GetDescriptionForSearchResult(includeTitle: true);
                lr.SetUrl(SearchResultType.Case);
                return lr;
            }));

            return result;
        }

        /// <summary>
        /// Sorts search results
        /// </summary>
        /// <param name="SearchResult"></param>
        /// <param name="SortBy"></param>
        /// <returns>Sorted list based on the SortBy param</returns>
        private List<SearchResult> SortCasesAndEmployees(List<SearchResult> SearchResult, string SortBy)
        {
            IOrderedEnumerable<SearchResult> orderedResult = null;

            if (SortBy != null)
                SortBy = SortBy.Trim().ToLowerInvariant();

            if (string.IsNullOrEmpty(SortBy) || SortBy == "relevance")
                orderedResult = SearchResult.Distinct().OrderBy(e => e.Title);
            else if (SortBy == "ln")
                orderedResult = SearchResult.Distinct().OrderBy(e => e.LastName);
            else if (SortBy == "fn")
                orderedResult = SearchResult.Distinct().OrderBy(e => e.FirstName);
            else if (SortBy == "cn")
                orderedResult = SearchResult.Distinct().OrderBy(e => e.CaseNumber);
            else if (SortBy == "em")
                orderedResult = SearchResult.Distinct().OrderBy(e => e.Employer);
            else
                orderedResult = SearchResult.Distinct().OrderBy(e => e.Title);

            return orderedResult.ToList();
        }
    }
}
