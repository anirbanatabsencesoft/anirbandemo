﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.SSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AbsenceSoft.Logic.Customers
{
    
    public class SsoService:LogicService
    {
        public SsoService(Customer currentCustomer, Employer currentEmployer, User user)
            :base(currentCustomer, currentEmployer, user)
        {
            
        }

        /// <summary>
        /// Gets the SSO Profile based on the employer or customer in context
        /// </summary>
        /// <returns></returns>
        public SsoProfile GetProfile()
        {
            // No known customer?  Go away
            if (string.IsNullOrEmpty(CustomerId))
                return null;

            // Get all profiles for current customer and all their employers
            List<SsoProfile> profiles = SsoProfile.AsQueryable().Where(sp => sp.CustomerId == CustomerId).ToList();

            // No profiles, nothing to return
            if (profiles.Count == 0)
                return null;

            // Get the profile that applies to this customer
            SsoProfile customerProfile = profiles.FirstOrDefault(sp => string.IsNullOrEmpty(sp.EmployerId));

            // No employer in context?  Just return the customer profile
            if (string.IsNullOrEmpty(EmployerId))
                return customerProfile;
            
            // Get the profile that matches the employer in context
            SsoProfile employerProfile = profiles.FirstOrDefault(sp => sp.EmployerId == EmployerId);

            // Return a null coalescence of employer first, then customer if we don't have one for this employer
            return !string.IsNullOrEmpty(EmployerId)? employerProfile: customerProfile;
        }

        /// <summary>
        /// Gets the SSO profile in context and whether or not it's for ESS or Portal
        /// </summary>
        /// <param name="isPortal"></param>
        /// <returns></returns>
        public SsoProfile GetProfile(bool isPortal)
        {
            SsoProfile profile = GetProfile();
            if (profile == null)
                return null;

            if (isPortal && profile.IsPortal)
                return profile;

            if (!isPortal && profile.IsEss)
                return profile;

            return null;
        }

        /// <summary>
        /// Saves a profile
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public SsoProfile SaveProfile(SsoProfile profile)
        {
            if (profile == null)
                throw new ArgumentNullException("profile");

            // store a copy of the current profile in case something goes wrong while we're trying to update the SSO
            var currentProfile = GetProfile();

            try
            {
                profile.CustomerId = CustomerId;
                profile.EmployerId = EmployerId;
                if (profile.LocalCertificate == null)
                    profile.LocalCertificate = CurrentCustomer.GeneratePfx();
                profile.Save();

                SsoConfiguration.UpdateSso(profile);
            }
            catch (CryptographicException)
            {
                // Cryptography errors are BAD and can take down the entire site, so we need to rollback to the previous SSO config
                // They didn't have a profile and are trying to setup a new one, let's delete
                if (currentProfile == null)
                {
                    SsoProfile.Repository.Delete(profile.Id);
                }
                // We're going to save the old one and since it has the same Id Mongo will just overwrite the one we just saved
                else
                {
                    currentProfile.Save();
                }
                
                throw;
            }
            

            return profile;
        }

        /// <summary>
        /// Exports the SP metadata on the profile
        /// </summary>
        /// <param name="ess"></param>
        /// <returns></returns>
        public byte[] ExportSPMetadata(bool ess, bool regenerateCertificate)
        {
            SsoProfile profile = GetProfile();
            if (regenerateCertificate)
            {
                profile.LocalCertificate = CurrentCustomer.GeneratePfx();
                profile = SaveProfile(profile);
            }

            string spMetadata = SsoConfiguration.ExportMetadata(profile, ess);
            return Encoding.UTF8.GetBytes(spMetadata);
        }

        /// <summary>
        /// Takes a file and imports it to create or update saml settings
        /// </summary>
        /// <param name="idpMetadata"></param>
        /// <returns></returns>
        public SsoProfile ImportIdPMetadata(HttpPostedFileBase idpMetadata)
        {
            if (idpMetadata == null)
                throw new ArgumentNullException("idpMetadata");

            string metadata = Encoding.UTF8.GetString(idpMetadata.InputStream.ReadAllBytes());
            SsoProfile profile = GetProfile();
            if (profile == null)
                profile = new SsoProfile();
            SsoConfiguration.ImportMetadata(metadata, profile);
            return SaveProfile(profile);
        }
    }
}
