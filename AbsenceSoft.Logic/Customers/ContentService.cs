﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Rendering.Templating.MailMerge;
using MongoDB.Driver.Builders;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic.Customers
{
    public sealed class ContentService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService" /> class.
        /// </summary>
        public ContentService() : this(null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService" /> class.
        /// </summary>
        /// <param name="u">The u.</param>
        public ContentService(User u = null) : this((string)null, null, u) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="u">The u.</param>
        public ContentService(string customerId, string employerId, User u) : base(customerId, employerId, u) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentService" /> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="u">The u.</param>
        public ContentService(Customer customer, Employer employer, User u) : base(customer, employer, u) { }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public Content Get(string key, string language = null)
        {
            using (new InstrumentationContext("ContentService.Get", key))
            {
                Content content = Content
                    .AsQueryable()
                    .Where(r => r.CustomerId == CustomerId || r.CustomerId == null)
                    .Where(r => r.EmployerId == EmployerId || r.EmployerId == null)
                    .Where(r => r.Key == key)
                    .Where(r => r.Language == language || r.Language == null)
                    .ToList()
                    .OrderBy(r =>
                    {
                        if (!string.IsNullOrWhiteSpace(r.EmployerId))
                            return 1;
                        if (!string.IsNullOrWhiteSpace(r.CustomerId))
                            return 2;
                        return 3;
                    })
                    .FirstOrDefault();

                return content;
            }
        }

        /// <summary>
        /// Gets the specified dictionary.
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public string Get(Hashtable dict, string key, string language = null)
        {
            if (dict == null)
                return null;

            return dict[BuildDictionaryKey(key, language, EmployerId)] as string
                ?? dict[BuildDictionaryKey(key, null, EmployerId)] as string
                ?? dict[BuildDictionaryKey(key, language, null)] as string
                ?? dict[BuildDictionaryKey(key, null, null)] as string;
        }

        /// <summary>
        /// Runs a plain text merge on the text
        /// </summary>
        /// <param name="text"></param>
        /// <param name="theCase"></param>
        /// <param name="theEmployee"></param>
        /// <returns></returns>
        public string PerformTextMerge(string text, Case theCase, Employee theEmployee)
        {
            /// Nothing to do if there is no provided data
            if (text == null || (theCase == null && theEmployee == null))
                return text;

            MailMergeData data = new MailMergeData();
            data.Case.CopyCaseData(theCase, null);
            DateTime caseCreatedDate = theCase != null ? theCase.CreatedDate : DateTime.Now;
            data.Employee.CopyEmployeeData(theEmployee, theCase);

            return AbsenceSoft.Rendering.Template.RenderTemplate(text, data);
        }

        /// <summary>
        /// Gets all content.
        /// </summary>
        /// <returns></returns>
        public Hashtable GetAllContent()
        {
            using (new InstrumentationContext("ContentService.GetAllContent"))
            {
                var query = Content.Query.Find(Content.Query.Or(
                    Content.Query.EQ(c => c.CustomerId, CustomerId),
                    Content.Query.EQ(c => c.CustomerId, null)
                )).ToList();

                Hashtable content = new Hashtable(query.Count);
                foreach (var item in query)
                {
                    if (item == null)
                        continue;

                    string key = BuildDictionaryKey(item.Key, item.Language, item.EmployerId);

                    if (content.ContainsKey(key))
                        continue;

                    content[key] = item.Value;
                }

                return content;
            }
        }

        /// <summary>
        /// Builds the dictionary key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="lang">The language.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        private string BuildDictionaryKey(string key, string lang, string employerId)
        {
            string dictKey = string.Join("_", new[] { key, lang, employerId }.Where(r => !string.IsNullOrWhiteSpace(r)));
            return dictKey;
        }

        /// <summary>
        /// Sets the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public Content Set(string key, string text, string language)
        {
            using (new InstrumentationContext("ContentService.Set", key))
            {
                Content c = Get(key, language);
                if (c == null)
                    c = new Content() { Key = key };

                if (c.CustomerId != CustomerId || c.EmployerId != EmployerId)
                    c.Clean();

                c.CustomerId = CustomerId;
                c.EmployerId = EmployerId;
                c.Language = language;
                c.Value = text;

                return c.Save();
            }
        }

        public ListResults GetContent(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults results = new ListResults(criteria);
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters());
            var query = Content.Query.Find(Content.Query.And(ands));

            var sortBy = SortBy.Ascending("Key", "Value");

            if (!string.IsNullOrWhiteSpace(criteria.SortBy))
            {
                string sortByCheck = criteria.SortBy.ToLowerInvariant();

                sortBy = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                    ? SortBy.Ascending(criteria.SortBy)
                    : SortBy.Descending(criteria.SortBy));
            }

            query = query.SetSortOrder(sortBy);

            results.Total = query.Count();
            results.Results = query.ToList().Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("Name", c.Key)
                .Set("Employer", c.Employer != null ? c.Employer.Name : "")
                .Set("Value", c.Value));

            return results;

        }

        public Content GetById(string id)
        {
            return Content.GetById(id);
        }

        public void DeleteContent(Content content)
        {
            if (content != null)
                content.Delete();
        }

        public Content SaveContent(Content content)
        {
            content.CustomerId = this.CustomerId;
            content.Save();
            return content;
        }

        public string GetValueByKey(string key)
        {
            Content content = Get(key);
            return content == null ? "" : (content.Value == null ? "" : content.Value);
        }
    }
}
