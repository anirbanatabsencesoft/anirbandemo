﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers
{
    public class HolidayService
    {
        /// <summary>
        /// If the holiday does not contain a specific date then call this with the year to build
        /// the date. If the year is outside the bounds of the start and end dates or if there is
        /// an error then it returns false
        /// </summary>
        /// <param name="h">Holiday Object</param>
        /// <param name="year">Target Year</param>
        /// <returns>true if it can create a date, false if it failed (like int.TryParse)</returns>
        public bool MakeDate(Holiday h, int year, out DateTime holiday)
        {
            // set a default value on holiday, cuz .net is funny that way
            holiday = DateTime.MinValue;

            // if there is a specific date on the holiday object
            // then check if the holiday date is within the start date & end date range
            if (h.Date.HasValue)
            {
                if (h.Date >= h.StartDate && (h.Date <= (h.EndDate ?? DateTime.MaxValue)))
                {
                    holiday = h.Date.Value;
                    return true;
                }
                else
                    return false;
            }            

            // now we have to have a month
            if (!h.Month.HasValue)
                return false;

            // if we have a day of month, then this is a specific date e.g. July 4, build it and return
            if (h.DayOfMonth.HasValue)
            {
                holiday = new DateTime(year, (int)h.Month, (int)h.DayOfMonth, 0, 0, 0, DateTimeKind.Utc);

                // check start and end dates
                if (holiday >= h.StartDate && (holiday <= (h.EndDate ?? DateTime.MaxValue)))
                    return true;

                holiday = DateTime.MinValue;
                return false;
            }

            // that leaves us with the WeekOfMonth - which is still named wrong because it is
            // the x occurance of that day in the month
            // if bothed aren't defined then that is an error
            if ((h.DayOfWeek.HasValue && !h.WeekOfMonth.HasValue) ||
                    !h.DayOfWeek.HasValue && h.WeekOfMonth.HasValue)
                return false;

            if (h.WeekOfMonth > 6)
                return false;

            int cnt = 0;                                            // counter for the loop below
            bool dtCreated = false;                                 // did we make a date
            int direction = 1;                                      // going forward / backward through the month
            int targetCount = Math.Abs((int)h.WeekOfMonth);         // the instances to count off
            holiday = new DateTime(year, (int)h.Month, (int)1, 0, 0, 0, DateTimeKind.Utc);

            // fix it up if we are going backwards
            if (h.WeekOfMonth < 0)
            {
                direction = -1;
                holiday = holiday.GetLastDayOfMonth();
            }

            // start with our date, and while we are in the month
            // count the instances of that date going in the 
            // appropriate direction for the month
            while (holiday.Month == (int)h.Month)
            {
                if (holiday.DayOfWeek == h.DayOfWeek)
                {
                    cnt++;
                    if (cnt == targetCount)
                    {
                        dtCreated = true;
                        break;
                    }
                }
                holiday = holiday.AddDays(direction);
            }

            if (!dtCreated)
            {
                holiday = DateTime.MinValue;
                return false;
            }

            // make sure our date is within the start and end
            if (holiday >= h.StartDate && (holiday <= (h.EndDate ?? DateTime.MaxValue)))
                return true;

            // if we get here and we haven't returned something then
            // that is bad
            holiday = DateTime.MinValue;
            return false;
        }

        /// <summary>
        /// Pass the holiday list and the year to gen the holdidays
        /// </summary>
        /// <param name="hs">The list of Holiday objexts</param>
        /// <param name="year">the year to generate</param>
        /// <returns></returns>
        public List<DateTime> HolidaysForYear(List<Holiday> hs, int year)
        {
            List<DateTime> rtVal = new List<DateTime>();
            foreach (Holiday h in hs)
            {
                DateTime nh;
                if (MakeDate(h, year, out nh))
                {
                    if (h.OrClosestWeekday && nh.IsWeekend())
                        rtVal.Add(nh.ClosestWeekday());
                    else
                        rtVal.Add(nh);
                }
            }

            return rtVal;
        }

        /// <summary>
        /// Gets a list of holidays for the given years supplied for the given employer.
        /// </summary>
        /// <param name="years">A list of years to calculate holidays for.</param>
        /// <param name="emp">The employer that contains the holiday definitions.</param>
        /// <returns>A list of <see cref="T:System.DateTime"/> of specific instances of each holiday for the years provided.</returns>
        public List<DateTime> GetHolidayList(List<int> years, Employer emp)
        {
            using (new InstrumentationContext("HolidayService.GetHolidayList(years,emp)"))
            {
                List<DateTime> holidays = new List<DateTime>();

                foreach (int i in years)
                    holidays.AddRange(HolidaysForYear(emp.Holidays, i));

                return holidays;
            }
        }

        /// <summary>
        /// Gets a list of holidays between 2 specific dates (inclusive of start and end date).
        /// </summary>
        /// <param name="startDate">The inclusive start date to begin measurement from.</param>
        /// <param name="endDate">The inclusive end date to end measurement on.</param>
        /// <param name="emp">The employer that contains the holiday definitions.</param>
        /// <returns>A list of <see cref="T:System.DateTime"/> of specific instances of each holiday between the start and end date provided.</returns>
        public List<DateTime> GetHolidayList(DateTime startDate, DateTime endDate, Employer emp)
        {
            using (new InstrumentationContext("HolidayService.GetHolidayList(startDate,endDate,emp)"))
            {
                List<int> years = new List<int>();
                int y = years.AddFluid(startDate.Year);
                while (y <= endDate.Year)
                    years.AddIfNotExists(y++);
                return GetHolidayList(years, emp).Where(h => h >= startDate && h <= endDate).ToList();
            }
        }
    }
}
