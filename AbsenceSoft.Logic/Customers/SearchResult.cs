﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers
{
    public enum SearchResultType
    {
        Case,
        Employee,
    }

    [Serializable]
    public sealed class SearchResults
    {
        private DateTime _init;
        private IEnumerable<SearchResult> _results;
        private IEnumerable<SearchResult> _facetResults;
        private long _total;

        public SearchResults()
        {
            _init = DateTime.UtcNow;
            Total = 0;
            Elapsed = 0;
            Results = new List<SearchResult>();
            FacetResults = new List<SearchResult>();
        }
        public SearchResults(ListCriteria criteria) : this() { Criteria = criteria; }

        /// <summary>
        /// Gets or sets the original list criteria that was used to generate the list result.
        /// </summary>
        public ListCriteria Criteria { get; set; }

        /// <summary>
        ///  Gets or sets the total number of results that match the criteria.
        /// </summary>
        public long Total
        {
            get { return _total; }
            set
            {
                _total = value;
                MarkTime();
            }
        }

        /// <summary>
        /// Gets or sets the total time elapsed in miliseconds that the list generation took.
        /// </summary>
        public double Elapsed { get; set; }

        public IEnumerable<SearchResult> FacetResults
        {
            get
            {
                if (_facetResults == null)
                {
                    _facetResults = new List<SearchResult>();
                }
                return _facetResults;
            }
            set
            {
                _facetResults = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of results themselves for display/usage
        /// </summary>
        public IEnumerable<SearchResult> Results
        {
            get
            {
                if (_results == null)
                {
                    _results = new List<SearchResult>();
                }
                return _results;
            }
            set
            {
                _results = value;
                MarkTime();
            }
        }

        /// <summary>
        /// Updates the elapsed time in milliseconds for completion.
        /// </summary>
        public void MarkTime()
        {
            this.Elapsed = this._init.Elapsed();
        }
    }

    [Serializable]
    public class SearchResult
    {
        public SearchResult()
        {
            this.Id = String.Empty;
            this.SearchText = String.Empty;
            this.Title = String.Empty;
            this.Description = String.Empty;
            this.Url = String.Empty;
            this.Employer = String.Empty;
            this.LastName = String.Empty;
            this.FirstName = string.Empty;
            this.DOB = null;
            this.CaseNumber = String.Empty;
            this.EmployeeId = string.Empty;
            this.EmployerId = string.Empty;
            this.CustomerId = string.Empty;
            this.EmployeeNumber = string.Empty;
        }

        public void SetUrl(SearchResultType searchType)
        {
            this.Url = searchType == SearchResultType.Case ? "/cases/" + Id + "/view"
                                                           : "employees/" + Id + "/view";
        }

        public string Id { get; set; }

        public string SearchText { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string Employer { get; set; }

        public string EmployerId { get; set; }

        public string CustomerId { get; set; }

        public string EmployeeId { get; set; }

        public string EmployeeNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DOB { get; set; }

        public string CaseNumber { get; set; }
    }
}
