﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// The OrganizationAnnualInfoService is used to create and update Organization annual info
    /// </summary>
    public class OrganizationAnnualInfoService : LogicService
    {
        public OrganizationAnnualInfoService()
        {

        }

        public OrganizationAnnualInfoService(User currentUser)
            : base(currentUser)
        {

        }

        public OrganizationAnnualInfoService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Gets the organization annual information employer.
        /// </summary>
        /// <returns></returns>
        public List<OrganizationAnnualInfo> GetSavedAnnualInfo(int startYear, int endYear)
        {
            return OrganizationAnnualInfo.AsQueryable()
                .Where(oAi => oAi.EmployerId == EmployerId && oAi.Year >= startYear && oAi.Year <= endYear)
                .ToList();
        }

     
        /// <summary>
        /// Ges the years.
        /// </summary>
        /// <param name="yearBack">The year back.</param>
        /// <returns></returns>
        public IEnumerable<int> GetYears(int numberOfYearsBack)
        {
            var last5Years = from n in Enumerable.Range(0, 5)
                             select (DateTime.Now.Year- numberOfYearsBack) - n;
            return last5Years;
        }

        public void BulkUploadAnnualInfo(IEnumerable<OrganizationAnnualInfo> organizationAnnualInfos)
        {
            /// Make sure they actually passed us a list of things to do
            if (organizationAnnualInfos == null ||
                organizationAnnualInfos.Count() == 0 ||
                organizationAnnualInfos.Where(ec => ec.OrganizationCode != null).Count() == 0)
                return;

            BulkWriteOperation<OrganizationAnnualInfo> bulkOrganizationAnnualInfo = OrganizationAnnualInfo.Repository.Collection.InitializeUnorderedBulkOperation();
            foreach (var organizationAnnualInfo in organizationAnnualInfos.Where(ec => ec.OrganizationCode != null))
            {
                bulkOrganizationAnnualInfo.Find(
                    Query.And(
                        OrganizationAnnualInfo.Query.EQ(oai => oai.OrganizationCode, organizationAnnualInfo.OrganizationCode),
                        OrganizationAnnualInfo.Query.EQ(oai => oai.Year, organizationAnnualInfo.Year),
                        OrganizationAnnualInfo.Query.EQ(oai => oai.CustomerId, CustomerId),
                        OrganizationAnnualInfo.Query.EQ(oai => oai.EmployerId, EmployerId)
                    )).Upsert().UpdateOne(OrganizationAnnualInfo.Updates
                        .Set(oai => oai.OrganizationCode, organizationAnnualInfo.OrganizationCode)
                        .Set(oai => oai.Year, organizationAnnualInfo.Year)
                        .Set(oai => oai.AverageEmployeeCount, organizationAnnualInfo.AverageEmployeeCount)
                        .Set(oai => oai.TotalHoursWorked, organizationAnnualInfo.TotalHoursWorked)
                        .Set(oai => oai.IsDeleted, false)
                        .Set(oai => oai.ModifiedById, CurrentUser.Id)
                        .Set(oai => oai.ModifiedDate, DateTime.UtcNow)
                        .SetOnInsert(ec => ec.CreatedById, CurrentUser.Id)
                        .SetOnInsert(ec => ec.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(ec => ec.CustomerId, CustomerId)
                        .SetOnInsert(ec => ec.EmployerId, EmployerId)
                    );
            }

            bulkOrganizationAnnualInfo.Execute();
        }

    }//end: OrganizationAnnualInfoService
}//end: namespace
