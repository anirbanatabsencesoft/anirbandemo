﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// The EmployerService is used to create and update Employers. It can validate Employer information
    /// and provide other functionality related to the Employer class
    /// </summary>
    public class EmployerService : LogicService, IEmployerService
    {
        public EmployerService()
        {

        }

        public EmployerService(User currentUser)
            : base(currentUser)
        {

        }

        public EmployerService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Creates a default instance of an employer from information available on the customer record
        /// as well as some defaults for holidays, options, etc.
        /// </summary>
        /// <param name="customer">The customer to create the employer for</param>
        /// <returns></returns>
        public Employer CreateEmployer(Customer customer)
        {
            Employer emp = new Employer()
            {
                Name = customer.Name,
                Customer = customer,
                AllowIntermittentForBirthAdoptionOrFosterCare = true,
                EnableSpouseAtSameEmployerRule = false,
                Contact = customer.Contact,
                CreatedById = customer.CreatedById,
                ModifiedById = customer.ModifiedById,
                Enable50In75MileRule = true,
                FMLPeriodType = PeriodType.RollingBack,
                FTWeeklyWorkHours = 40,
                //ReferenceCode = customer.Url,				
                IsPubliclyTradedCompany = false,
                Holidays = new List<Holiday>()
                {
                    new Holiday() { Name = "New Year’s Day",                Month = Month.January,      DayOfMonth = 1,                                     StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Martin Luther King’s Birthday", Month = Month.January,      WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday,   StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "George Washington's Birthday",  Month = Month.February,     WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday,   StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Memorial Day",                  Month = Month.May,          WeekOfMonth = -1,   DayOfWeek = DayOfWeek.Monday,   StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Independence Day",              Month = Month.July,         DayOfMonth = 4,                                     StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Labor Day",                     Month = Month.September,    WeekOfMonth = 1,    DayOfWeek = DayOfWeek.Monday,   StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Columbus Day",                  Month = Month.October,      WeekOfMonth = 2,    DayOfWeek = DayOfWeek.Monday,   StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Veterans Day",                  Month = Month.November,     DayOfMonth = 11,                                    StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                    new Holiday() { Name = "Thanksgiving Day",              Month = Month.November,     WeekOfMonth = 4,    DayOfWeek = DayOfWeek.Thursday, StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc) },
                    new Holiday() { Name = "Christmas Day",                 Month = Month.December,     DayOfMonth = 25,                                    StartDate = new DateTime(1970,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true }
                },
                Url = GenerateEmployerUrlSuggestion(customer.Name)
            };

            return emp;
        }// CreateEmployer

        /// <summary>
        /// Validate and then update the employer.
        /// 
        /// Update allows the exceptions from the validation methods to bubble up. 
        /// Please catch them.
        /// </summary>
        /// <param name="employer"></param>
        public void Update(Employer employer)
        {
            using (new InstrumentationContext("EmployerService.Update"))
            {
                ValidateInfo(employer);
                /// Need to keep a copy of the original value, because it will be false once we call save
                bool isNew = employer.IsNew;
                employer.Save();

                /// If they've got this flag set, let's go ahead and add a new EmployerAccess record to the customers users
                if (isNew && CurrentCustomer != null && CurrentCustomer.EnableAllEmployersByDefault)
                {
                    User.Update(
                            Query.And(User.Query.EQ(u => u.CustomerId, CustomerId), User.Query.NE(u => u.UserType, UserType.SelfService)),
                            User.Updates.AddToSet(u => u.Employers, new EmployerAccess() { EmployerId = employer.Id }),
                            UpdateFlags.Multi
                        );
                }
            }
        }

        /// <summary>
        /// Gets the Employer by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public Employer GetById(string id)
        {
            return Employer.GetById(id);
        }

        /// <summary>
        /// Verify the fields on the employer pass all the rules
        /// </summary>
        /// <param name="employer">The Employer to check</param>
        public void ValidateInfo(Employer employer)
        {
            // is there a customer
            if (string.IsNullOrWhiteSpace(employer.CustomerId))
                throw new AbsenceSoftException("Employer Service: No customer ID");

            // is it a valid customer
            if (Customer.AsQueryable().Where(c => c.Id == employer.CustomerId).Count() != 1)
                throw new AbsenceSoftException("Employer Service: Error with customer ID");

            CheckForDuplicate(employer);

            if (employer.Holidays != null && employer.Holidays.Count > 0 && !AreHolidaysValid(employer.Holidays))
                throw new AbsenceSoftException("Employer Service: Invalid Holidays");

            if (IsSubDomainInUse(employer.Url, employer.Id))
                throw new AbsenceSoftException("Employer Service: Employer or Customer exists with the same URL");
        }

        /// <summary>
        /// Verify the name and / or the EIN is not already assigned to this customer
        /// </summary>
        /// <param name="employer"></param>
        public void CheckForDuplicate(Employer employer)
        {
            if (NameIsInUse(employer))
                throw new AbsenceSoftException("Employer Service: Employer for that customer already exists for that name");

            if (ReferenceCodeIsInUse(employer))
                throw new AbsenceSoftException("Employer Service: Employer for that customer already exists with this reference code");
        }

        public bool NameIsInUse(Employer employer)
        {
            return Employer.AsQueryable()
                .Where(e => e.CustomerId == employer.CustomerId
                && e.Name == employer.Name
                && e.Id != employer.Id).Count() > 0;
        }

        public bool NameIsInUse(string name, string employerId = null)
        {
            employerId = employerId ?? EmployerId;

            return Employer.AsQueryable()
                .Where(e => e.CustomerId == CustomerId
                && e.Id != employerId
                && e.Name == name).Count() > 0;
        }

        public bool ReferenceCodeIsInUse(Employer employer)
        {
            return Employer.AsQueryable()
                .Where(e => e.CustomerId == employer.CustomerId
                && employer.ReferenceCode != null
                && e.ReferenceCode == employer.ReferenceCode
                && e.Id != employer.Id).Count() > 0;
        }

        public bool ReferenceCodeIsInUse(string referenceCode)
        {
            return Employer.AsQueryable()
                .Where(e => e.CustomerId == CustomerId
                && e.ReferenceCode != null
                && e.ReferenceCode == referenceCode
                && e.Id != EmployerId).Count() > 0;
        }

        public bool AreHolidaysValid(List<Holiday> holidays)
        {
            HolidayService hs = new HolidayService();
            DateTime testHoliday;
            foreach (Holiday h in holidays)
                if (!h.Date.HasValue && !hs.MakeDate(h, h.StartDate.Year, out testHoliday))
                    return false;

            // made it to the end so all the holiday are valid
            return true;
        }//end: AreHolidaysValid

        /// <summary>
        /// Get employers for a customer
        /// </summary>
        /// <param name="customerId"></param>
        public List<Employer> GetEmployersForCustomer(string customerId)
        {
            using (new InstrumentationContext("EmployerService.GetEmployersForCustomer"))
            {
                return Employer.AsQueryable().Where(e => e.CustomerId == customerId && !e.IsDeleted).ToList();
            }
        }//end: GetEmployersForCustomer

        /// <summary>
        /// Gets a list of employers for a given user (based on user data access) and optionally
        /// given the "intent" of what the user may be trying to do with that list to further filter
        /// </summary>
        /// <param name="u">The user account to pull employers for</param>
        /// <param name="permissions">The intended permissions of what the list is or may be needed for</param>
        /// <returns>A list of employers for the given user and optionally any future intent</returns>
        public List<Employer> GetEmployersForUser(User u, params string[] permissions)
        {
            List<Employer> savedEmployers;
            using (new InstrumentationContext("EmployerService.GetEmployersForUser"))
            {
                // Short-circuit validation, must have a user
                if (u == null) return new List<Employer>(0);
                // User must belong to a customer
                if (string.IsNullOrWhiteSpace(u.CustomerId)) return new List<Employer>(0);
                // If the user is a System Administrator, they have access to ALL employers, so return all for this customer
                if (u.Roles != null && u.Roles.Any(r => r == Role.SystemAdministrator.Id))
                {
                    savedEmployers = Employer.AsQueryable().Where(e => e.CustomerId == u.CustomerId).ToList();
                    return savedEmployers.Where(emp => UserHasPermissionOnThisEmployer(u, emp, permissions)).OrderBy(emp => emp.Name).ToList();
                }

                // User must have access to at least 1 employer
                if (u.Employers == null || !u.Employers.Any(e => e.IsActive))
                {
                    return new List<Employer>(0);
                }

                // Filter out any that are not active
                savedEmployers = u.Employers.Where(e => e.IsActive)
                    // Ensure the permissions passed in are empty (meaning just all active) or that 
                    //  if we have explicit intent, that the user has the appropriate permissions for that function for that employer
                    .Where(e => UserHasPermissionOnThisEmployer(u, e.Employer, permissions))
                    // Project our Employer entity
                    .Select(e => e.Employer)
                    // Filter out deleted ones
                    .Where(e => e != null && !e.IsDeleted).ToList();

                // Return the final list sorted by employer name
                return savedEmployers.OrderBy(emp => emp.Name).ToList();
            }
        }//end: GetEmployersForUser

        private bool UserHasPermissionOnThisEmployer(User user, Employer employer, params string[] permissions)
        {
            // Unknown user, just allow it
            if (user == null)
            {
                return true;
            }

            // No employer, just allow it
            if (employer == null)
            {
                return true;
            }

            // no specified permissions, just allow it
            if (!permissions.Any())
            {
                return true;
            }

            //Get the intersection of the requested permissions and the actual permissions, and if they have the same count,
            // then the user has all the requested permissions
            var usersActualPermissions = User.Permissions.GetPermissions(user, employer.Id);
            var intersect = permissions.Intersect(usersActualPermissions);
            return intersect.Count() == permissions.Length;
        }

        /// <summary>
        /// Get employers for a customer
        /// </summary>
        /// <param name="customerId"></param>
        public List<CustomField> GetCustomFields(EntityTarget? target = null, bool? collectedAtIntake = null)
        {
            using (new InstrumentationContext("EmployerService.GetCustomFields"))
            {
                var customFields = CustomField.DistinctFind(null, CustomerId, EmployerId).ToList();
                if (target.HasValue)
                {
                    customFields = customFields.Where(cf => cf.Target.HasFlag(target.Value)).ToList();
                }

                if (collectedAtIntake.HasValue)
                {
                    customFields = customFields.Where(cf => cf.IsCollectedAtIntake == collectedAtIntake.Value).ToList();
                }

                return customFields.OrderBy(k => k.FileOrder).ToList();
            }

        }//end: GetCustomFields

        /// <summary>
        /// Get employers for a customer
        /// </summary>
        /// <param name="customerId"></param>
        public List<CustomField> GetCustomFieldsForCustomer(EntityTarget? target = null, bool? collectedAtIntake = null)
        {
            using (new InstrumentationContext("EmployerService.GetCustomFields"))
            {
                var customFields = CustomField.Repository.Where(c=>c.CustomerId == CustomerId && c.IsDeleted == false && c.Target != EntityTarget.None).OrderBy(c=>c.Target).ToList();
                if (target.HasValue)
                {
                    customFields = customFields.Where(cf => cf.Target.HasFlag(target.Value)).ToList();
                }

                if (collectedAtIntake.HasValue)
                {
                    customFields = customFields.Where(cf => cf.IsCollectedAtIntake == collectedAtIntake.Value).ToList();
                }
                List<CustomField> customFieldListForBoth = new List<CustomField>();
                customFields.ForEach(c=>{
                    int targetForCustomField = (int)c.Target;
                    //3 is for custom fields which are applicable for both case and employee and 3 is not defined in the Entity Target
                    if (targetForCustomField == 3)
                    {
                        CustomField cf = c.Clone();
                        c.Target = EntityTarget.Case;
                        cf.Target = EntityTarget.Employee;
                        customFieldListForBoth.Add(cf);
                    }
                });
                customFields.AddRange(customFieldListForBoth);
                customFields.ForEach(c => {                    
                    string targetType = string.Empty;
                    if (c.Target == EntityTarget.Case)
                    {
                        targetType = "Case Custom - ";
                    }
                    else if (c.Target == EntityTarget.Employee)
                    {
                        targetType = "Employee Custom - ";
                    }                    
                    c.Name = string.Concat(targetType, c.Name, "(",c.Employer!=null?c.Employer.Name:c.Customer.Name,")");
                });
                return customFields.OrderBy(k => k.EmployerId).ThenBy(k => k.Target).ThenBy(k => k.FileOrder).ToList();
            }

        }//end: GetCustomFields
        /// <summary>
        /// Get list of Features with their status
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public IDictionary<Feature, bool> GetFeatures(string customerId, string employerId)
        {
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }


            using (new InstrumentationContext("EmployerService.GetFeatures"))
            {
                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    Employer employer = Employer.GetById(employerId);

                    if (employer != null && !employer.IsDeleted)
                        return employer.GetFeatureListWithStatus();
                }
                else
                {
                    Customer cust = Customer.GetById(customerId);
                    if (cust != null && !cust.IsDeleted)
                        return cust.GetFeatureListWithStatus();
                }
            }

            return new Dictionary<Feature, bool>();
        }


        /// <summary>
        /// Gets a unique, unused customer URL suggestions for use by the UI when creating a new employer.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GenerateEmployerUrlSuggestion(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return RandomString.GenerateUniqueId();

            string sug = Regex.Replace(name, @"[^A-Za-z_\-]", string.Empty).ToLowerInvariant();
            if (IsSubDomainInUse(sug))
                sug = sug + "-ss";
            while (IsSubDomainInUse(sug))
                sug = string.Concat(sug, RandomString.Generate(1, false, true, false, false)).ToLowerInvariant();

            return sug;
        } // GenerateEmployerUrlSuggestion


        /// <summary>
        /// Checks to see if an employer subdomain is currently in use by a different customer (is not available).
        /// </summary>
        /// <param name="subDomain">The sub-domain to check for</param>
        /// <param name="customerId">The current customer ID if any</param>
        /// <returns><c>true</c> if the domain is NOT available (in use); otherwise <c>false</c>.</returns>
        public bool IsSubDomainInUse(string subDomain, string employerId = null)
        {
            using (new InstrumentationContext("EmployerService.IsSubDomainInUse"))
            {
                if (string.IsNullOrWhiteSpace(subDomain))
                    return false;

                var lowered = subDomain.ToLowerInvariant();
                return Customer.AsQueryable().Where(c => c.Url == lowered).Any() || Employer.AsQueryable().Where(e => e.Url == lowered && e.Id != employerId).Any();
            }
        } // IsSubDomainInUse


        #region Employer Contacts CRUD

        /// <summary>
        /// Returns a list of employer contacts
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults ListEmployerContacts(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults results = new ListResults(criteria);

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters());

            string firstName = criteria.Get<string>("FirstName");
            string lastName = criteria.Get<string>("LastName");
            string email = criteria.Get<string>("Email");
            string employerId = criteria.Get<string>("EmployerId");
            string text = criteria.Get<string>("text");
            string contactTypeCode = criteria.Get<string>("ContactTypeCode");
            bool excludeInactive = criteria.Get<bool>("ExcludeInactive");
            if (!string.IsNullOrEmpty(employerId))
            {
                ands.Add(EmployerContact.Query.EQ(ec => ec.EmployerId, employerId));
            }
            if (excludeInactive)
            {
                ands.Add(EmployerContact.Query.Or(
                   EmployerContact.Query.EQ(ec => ec.Contact.EndDate, null),
                   EmployerContact.Query.GTE(ec => ec.Contact.EndDate, DateTime.UtcNow.Date)
                   ));
            }
            EmployerContact.Query.MatchesString(ec => ec.Contact.FirstName, firstName, ands);
            EmployerContact.Query.MatchesString(ec => ec.Contact.LastName, lastName, ands);
            EmployerContact.Query.MatchesString(ec => ec.Contact.Email, email, ands);
            EmployerContact.Query.MatchesString(ec => ec.ContactTypeCode, contactTypeCode, ands);
            EmployerContact.Query.StringMatchesMultipleFields(text, ands, ec => ec.Contact.FirstName, ec => ec.Contact.LastName);

            var query = EmployerContact.Query.Find(EmployerContact.Query.And(ands));
            query = criteria.SetSortAndSkip<EmployerContact>(query);

            results.Total = query.Count();
            var employerContacts = query.ToList();

            // apply sorting
            if (!string.IsNullOrWhiteSpace(criteria.SortBy))
            {
                if (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending)
                {
                    employerContacts = employerContacts.OrderBy(q => criteria.SortBy).ToList();
                }
                else
                {
                    employerContacts = employerContacts.OrderByDescending(q => criteria.SortBy).ToList();
                }

            }

            results.Results = employerContacts.Select(ec => new ListResult()
                .Set("Id", ec.Id)
                .Set("EmployerId", ec.EmployerId)
                .Set("FirstName", ec.Contact.FirstName)
                .Set("LastName", ec.Contact.LastName)
                .Set("text", string.Format("{0} {1} ({2})", ec.Contact.FirstName, ec.Contact.LastName, ec.ContactTypeName))
                .Set("PhoneNumber", ec.Contact.WorkPhone)
                .Set("Email", ec.Contact.Email)
                .Set("ContactTypeCode", ec.ContactTypeCode)
                .Set("EndDate", ec.Contact.EndDate)
                );

            return results;
        }

        /// <summary>
        /// Returns a contact by a specific Id
        /// </summary>
        /// <param name="employerContactId"></param>
        /// <returns></returns>
        public EmployerContact GetContactById(string employerContactId)
        {
            return EmployerContact.GetById(employerContactId);
        }

        public List<EmployerContact> GetEmployerContacts(string employerId)
        {
            if (string.IsNullOrWhiteSpace(employerId)
                || !CurrentUser.Customer.HasFeature(Feature.EmployerContact))
                return new List<EmployerContact>();

            List<IMongoQuery> ands = new List<IMongoQuery>();

            ands.Add(EmployerContact.Query.EQ(ec => ec.EmployerId, employerId));
            ands.Add(EmployerContact.Query.EQ(ec => ec.CustomerId, CurrentUser.Customer.Id));
            ands.Add(EmployerContact.Query.Or(
                    EmployerContact.Query.EQ(ec => ec.Contact.EndDate, null),
                    EmployerContact.Query.GTE(ec => ec.Contact.EndDate, DateTime.UtcNow.Date)
                  ));
            var query = EmployerContact.Query.Find(EmployerContact.Query.And(ands));
            return query.ToList();

        }

        /// <summary>
        /// Get Employer Contacts By Contact Type Code
        /// </summary>
        /// <param name="employerId">employerId</param>
        /// <param name="contactTypeCode">contactTypeCode</param>
        /// <returns>Employer Contact List</returns>
        public List<EmployerContact> GetEmployerContactsByContactType(string employerId,string contactTypeCode)
        {
            if (string.IsNullOrWhiteSpace(employerId) && string.IsNullOrWhiteSpace(contactTypeCode)
                || !CurrentUser.Customer.HasFeature(Feature.EmployerContact))
            {
                return new List<EmployerContact>();
            }

            List<IMongoQuery> ands = new List<IMongoQuery>();

            ands.Add(EmployerContact.Query.EQ(ec => ec.EmployerId, employerId));
            ands.Add(EmployerContact.Query.EQ(ec => ec.CustomerId, CurrentUser.Customer.Id));
            ands.Add(EmployerContact.Query.EQ(ec => ec.ContactTypeCode, contactTypeCode));
            ands.Add(EmployerContact.Query.Or(
                    EmployerContact.Query.EQ(ec => ec.Contact.EndDate, null),
                    EmployerContact.Query.GTE(ec => ec.Contact.EndDate, DateTime.UtcNow.Date)
                  ));

            var query = EmployerContact.Query.Find(EmployerContact.Query.And(ands));
            return query.ToList();
        }

        /// <summary>
        /// Saves a contact
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public EmployerContact SaveContact(EmployerContact contact)
        {
            if (contact == null)
                throw new ArgumentNullException("contact");

            contact.EmployerId = this.EmployerId;
            contact.CustomerId = this.CustomerId;

            return contact.Save();
        }

        /// <summary>
        /// Deletes a contact
        /// </summary>
        /// <param name="contact"></param>
        public void DeleteContact(EmployerContact contact)
        {
            if (contact == null)
                throw new ArgumentNullException("contact");

            contact.Delete();
        }


        #endregion


        public void BulkUploadContacts(IEnumerable<EmployerContact> employerContacts)
        {
            /// Make sure they actually passed us a list of things to do
            if (employerContacts == null ||
                !employerContacts.Any() ||
                employerContacts.All(ec => string.IsNullOrWhiteSpace(ec.EmployerId)))
                return;

            BulkWriteOperation<EmployerContact> bulkEmployerContact = EmployerContact.Repository.Collection.InitializeUnorderedBulkOperation();
            foreach (var contact in employerContacts.Where(ec => ec.EmployerId != null))
            {
                bulkEmployerContact.Find(
                    Query.And(
                        EmployerContact.Query.EQ(ec => ec.Contact.Email, contact.Contact.Email),
                        EmployerContact.Query.EQ(ec => ec.CustomerId, CurrentCustomer.Id),
                        EmployerContact.Query.EQ(ec => ec.EmployerId, contact.EmployerId)
                    )).Upsert().UpdateOne(EmployerContact.Updates
                        .Set(ec => ec.Contact.FirstName, contact.Contact.FirstName)
                        .Set(ec => ec.Contact.LastName, contact.Contact.LastName)
                        .Set(ec => ec.Contact.WorkPhone, contact.Contact.WorkPhone)
                        .Set(ec => ec.Contact.Fax, contact.Contact.Fax)
                        .Set(ec => ec.Contact.EndDate, contact.Contact.EndDate)
                        .Set(ec => ec.IsDeleted, false)
                        .Set(ec => ec.ContactTypeCode, contact.ContactTypeCode)
                        .Set(ec => ec.ModifiedById, CurrentUser.Id)
                        .Set(ec => ec.ModifiedDate, DateTime.UtcNow)
                        .SetOnInsert(ec => ec.Contact.Id, Guid.NewGuid())
                        .SetOnInsert(ec => ec.Contact.Address, new Address())
                        .SetOnInsert(ec => ec.Contact.Email, contact.Contact.Email)
                        .SetOnInsert(ec => ec.CreatedById, CurrentUser.Id)
                        .SetOnInsert(ec => ec.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(ec => ec.CustomerId, CurrentCustomer.Id)
                        .SetOnInsert(ec => ec.EmployerId, contact.EmployerId)
                    );
            }

            bulkEmployerContact.Execute();
        }

        /// <summary>
        /// Returns the employer Id 
        /// </summary>
        /// <param name="employerReferenceCode"></param>
        /// <returns></returns>
        public string GetEmployerIdByReferenceCode(string employerReferenceCode)
        {
            if (string.IsNullOrWhiteSpace(employerReferenceCode))
                throw new ArgumentNullException("employerReferenceCode");

            Employer emp = GetEmployerByReferenceCode(employerReferenceCode);
            if (emp == null)
                return null;

            return emp.Id;
        }

        public Employer GetEmployerByReferenceCode(string employerReferenceCode)
        {
            if (string.IsNullOrWhiteSpace(employerReferenceCode))
                throw new ArgumentNullException("employerReferenceCode");

            return Employer.AsQueryable().FirstOrDefault(e => e.ReferenceCode == employerReferenceCode);
        }






    }//end: EmployerService
}//end: namespace
