﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace AbsenceSoft.Logic.Customers
{
    public class TeamService : LogicService
    {
        public TeamService() { }

        public TeamService(User currentUser)
        {
            this.CurrentUser = currentUser;
        }

        public User CurrentUser { get; set; }

        /// <summary>
        /// Retrieves all teams for the specified customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<Team> GetTeamsForCustomer(string customerId)
        {
            if (customerId != CurrentUser.CustomerId)
                throw new AbsenceSoftException("User does not have permission to access specified customer");

            return Team.AsQueryable().Where(t => t.CustomerId == customerId).ToList();
        }

        public bool NameIsInUse(string teamName, string id)
        {
            return Team.AsQueryable().Count(t => t.Name == teamName && t.Id != id) > 0;
        }

        public List<Team> GetTeamsForUser()
        {
            List<string> teamMembers = TeamMember.AsQueryable().Where(t => t.UserId == CurrentUser.Id).ToList().Select(tm => tm.TeamId).ToList();
            return Team.AsQueryable().Where(t => teamMembers.Contains(t.Id)).ToList();
        }

        public Team GetTeamById(string teamId)
        {
            return CheckTeam(teamId);
        }

        /// <summary>
        /// Saves or updates a new team
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public Team SaveTeam(Team t)
        {
            if (!t.IsNew)
                CheckTeam(t.Id);

            t.Save();
            return t;
        }

        /// <summary>
        /// Deletes a team and all team members associated with that team
        /// </summary>
        /// <param name="t"></param>
        public void DeleteTeam(Team t)
        {
            CheckTeam(t.Id);

            List<TeamMember> teamMembers = GetTeamMembers(t.Id);
            foreach (TeamMember tm in teamMembers)
            {
                tm.Delete();
            }

            t.Delete();
        }

        /// <summary>
        /// Gets the team members for a specified team
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns></returns>
        public List<TeamMember> GetTeamMembers(string teamId)
        {
            CheckTeam(teamId);
            return TeamMember.AsQueryable().Where(t => t.TeamId == teamId).ToList();
        }

        /// <summary>
        /// Given a list of team members to save, deletes any ones that are no longer part of the team and saves the rest
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="teamMembers"></param>
        /// <returns></returns>
        public List<TeamMember> SaveTeamMembers(string teamId, List<TeamMember> teamMembers)
        {
            List<TeamMember> currentMembers = GetTeamMembers(teamId);
            List<TeamMember> membersToDelete = currentMembers.Where(cm => !teamMembers.Any(tm => tm.UserId == cm.UserId)).ToList();
            foreach (TeamMember member in membersToDelete)
                RemoveTeamMember(member);

            foreach (TeamMember member in teamMembers)
                SaveTeamMember(member);

            return teamMembers;
        }

        /// <summary>
        /// Saves a single team member to the database, assigning them to the specified team
        /// </summary>
        /// <param name="tm"></param>
        /// <returns></returns>
        public TeamMember SaveTeamMember(TeamMember tm)
        {
            CheckTeam(tm.TeamId);

            if (tm.IsNew)
            {
                tm.AssignedById = CurrentUser.Id;
                tm.AssignedOn = DateTime.UtcNow;
            }
            
            tm.ModifiedById = CurrentUser.Id;
            tm.ModifiedOn = DateTime.UtcNow;

            tm.Save();
            return tm;
        }

        /// <summary>
        /// Removes a team member from the database, removing them from the team they are on
        /// </summary>
        /// <param name="tm"></param>
        public void RemoveTeamMember(TeamMember tm)
        {
            if (tm.CustomerId != CurrentUser.CustomerId)
                throw new AbsenceSoftException("User does not have permissions to remove team members from specified customer");

            tm.Delete();
        }

        /// <summary>
        /// Gets all the users that can be assigned to a team by the current user
        /// </summary>
        /// <returns></returns>
        public List<User> GetUsersForTeamAssignmentByTeamLead()
        {
            List<string> nonESSPermissions = Permission.All().Where(p => !p.Category.StartsWith("Self Service")).Select(p => p.Id).ToList();
            List<string> nonESSRoles = Role.AsQueryable().Where(r => r.CustomerId == CurrentUser.CustomerId && r.Permissions.ContainsAny(nonESSPermissions)).Select(r => r.Id).ToList();
            nonESSRoles.Add(Role.SystemAdministrator.Id);
            List<User> users = User.AsQueryable().Where(u => u.CustomerId == CurrentUser.CustomerId && !u.IsDeleted && u.Roles.ContainsAny(nonESSRoles)).ToList();
            return users;
        }

        public List<string> GetTeamMemberIdsForCurrentUser()
        {
            List<string> myTeamsIds = TeamMember.AsQueryable().Where(t => t.UserId == CurrentUser.Id && t.IsTeamLead).Select(tm => tm.TeamId).ToList();
            List<string> myTeamsUserIds = TeamMember.AsQueryable().Where(tm => myTeamsIds.Contains(tm.TeamId)).ToList().Select(tm => tm.UserId).ToList();
            ///Edge case - if they don't belong to a team, myTeamsUserIds will be empty and they won't even see their stuff
            if (myTeamsUserIds.Count == 0)
                myTeamsUserIds.Add(CurrentUser.Id);
            return myTeamsUserIds;
        }



        /// <summary>
        /// Checks that the specified team exists and belongs to the current customer
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns>The team</returns>
        private Team CheckTeam(string teamId)
        {
            Team t = Team.GetById(teamId);
            if (t == null)
                throw new AbsenceSoftException("Unable to find specified team");

            if (t.CustomerId != CurrentUser.CustomerId)
                throw new AbsenceSoftException("User does not have permission to access specified customer");

            return t;
        }


    }
}
