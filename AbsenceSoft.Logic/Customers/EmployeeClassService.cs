﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Expressions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Customers
{
    public class EmployeeClassService : BaseExpressionService
    {
        public EmployeeClassService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }
        public EmployeeClassService(string customerId, string employerId, string userId)
            : base(customerId, employerId, User.GetById(userId))
        {

        }
        /// <summary>
        /// Checks whether Employee Class already exists
        /// We need to pass in the CustomerId instead of using what is in context because the UI needs to tell whether we're looking at a core one or a customer one
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool EmployeeClassIsInUse(string id, string code)
        {
            if(String.IsNullOrWhiteSpace(id) || String.IsNullOrWhiteSpace(code))
            {
                return false;
            }
            return EmployeeClass.AsQueryable().Any(nc => nc.Code == code.ToUpperInvariant() && nc.Id != id && nc.EmployerId == EmployerId && nc.CustomerId == CustomerId);
        }

        /// <summary>
        /// Gets Employee Class by code and employer/customer 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public EmployeeClass GetEmployeeClassByCode(string code)
        {
            if (String.IsNullOrWhiteSpace(code))
            {
                return null;
            }
            using (new InstrumentationContext("EmployeeClassService.GetEmployeeClassByCode"))
            {
                EmployeeClass employeeClass = EmployeeClass.GetByCode(code.ToUpperInvariant(), CustomerId, EmployerId, true);
                return employeeClass;
            }
        }

        /// <summary>
        /// Toggles a Employee Class by code and employer/customer depending on what is most specific
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public EmployeeClass ToggleEmployeeClassByCode(string code)
        {
            if (String.IsNullOrWhiteSpace(code))
            {
                return null;
            }
            using (new InstrumentationContext("CaseService.ToggleEmployeeClassByCode"))
            {
                EmployeeClass EmployeeClass = EmployeeClass.GetByCode(code.ToUpperInvariant(), CustomerId, EmployerId, true);

                if (EmployeeClass != null)
                {
                    SuppressedEntity suppressedEntity = new SuppressedEntity { EntityName = "EmployeeClass", EntityCode = EmployeeClass.Code.ToUpperInvariant(), EntityModifiedDate = DateTime.UtcNow };
                    if (CurrentEmployer != null)
                    {
                        CurrentEmployer.SuppressedEntities = AddOrRemoveSuppressedEntity(CurrentEmployer.SuppressedEntities, suppressedEntity);
                        CurrentEmployer.Save();
                    }
                    else if (CurrentCustomer != null)
                    {
                        CurrentCustomer.SuppressedEntities = AddOrRemoveSuppressedEntity(CurrentCustomer.SuppressedEntities, suppressedEntity);
                        CurrentCustomer.Save();
                    }
                }
                return EmployeeClass;
            }
        }

        /// <summary>
        ///  Gets all Employee Classs filtered by target and employer/ customer
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public List<EmployeeClass> GetAllEmployeeClasses()
        {
            IEnumerable<EmployeeClass> employeeClasses = EmployeeClass.DistinctFind(null, CustomerId, EmployerId);

            return employeeClasses.Where(nc => !IsEmployeeClassDisabledInCurrentContext(nc.Code)).ToList();
        }

        /// <summary>
        /// To list Employee Class for search
        /// </summary>
        /// <param name="criteria"></param> 
        public ListResults EmployeeClassList(ListCriteria criteria)
        {
            if (criteria == null)
            {
                criteria = new ListCriteria();
            }

            ListResults result = new ListResults(criteria);
            string description = criteria.Get<string>("Description");
            string code = criteria.Get<string>("Code");
            string name = criteria.Get<string>("Name");

            List<IMongoQuery> ands = EmployeeClass.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            EmployeeClass.Query.MatchesString(at => at.Description, description, ands);
            EmployeeClass.Query.MatchesString(at => at.Code, code, ands);
            EmployeeClass.Query.MatchesString(at => at.Name, name, ands);
            List<EmployeeClass> types = EmployeeClass.DistinctAggregation(ands);
            result.Total = types.Count;
            types = types
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = types.Select(c => new ListResult()
                .Set("Id", c.Id)
                .Set("CustomerId", c.CustomerId)
                .Set("EmployerId", c.EmployerId)                
                .Set("Description", c.Description)
                .Set("Name", c.Name)
                .Set("Code", c.Code)
                .Set("Order", c.Order)
                .Set("Disabled", IsDisabledInContext(c.Code))
                .Set("ModifiedDate", c.ModifiedDate)
            );

            return result;
        }

        public bool IsDisabledInContext(string code)
        {
            bool returnValue = false;

            if (String.IsNullOrWhiteSpace(code))
            {
                return returnValue;
            }

            if (CurrentEmployer != null)
            {
                var EmployeeClass = CurrentEmployer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "EmployeeClass" && p.EntityCode == code.ToUpperInvariant());
                if (EmployeeClass != null)
                {
                    returnValue = true;
                }
            }
            if (CurrentCustomer != null && !returnValue)
            {
                var EmployeeClass = CurrentCustomer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "EmployeeClass" && p.EntityCode == code.ToUpperInvariant());
                if (EmployeeClass != null)
                {
                    returnValue = true;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Check if Employee Class is disabled in current context(employer/customer)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool IsEmployeeClassDisabledInCurrentContext(string code)
        {
            bool returnValue = false;

            if (String.IsNullOrWhiteSpace(code))
            {
                return returnValue;
            }

            if (CurrentEmployer != null)
            {
                var EmployeeClass = CurrentCustomer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "EmployeeClass" && p.EntityCode == code.ToUpperInvariant());
                if (EmployeeClass != null)
                {
                    returnValue = true;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Get Employee Class by Id
        /// </summary>
        /// <param name="employeeClassId"></param>
        /// <returns></returns>
        public EmployeeClass GetEmployeeClassById(string employeeClassId)
        {
            using (new InstrumentationContext("CaseService.GetEmployeeClassById"))
            {
                return EmployeeClass.GetById(employeeClassId);
            }
        }

        /// <summary>
        /// Save Employee Class
        /// </summary>
        /// <param name="employeeClass"></param>
        /// <returns></returns>
        public EmployeeClass SaveEmployeeClass(EmployeeClass employeeClass)
        {
            using (new InstrumentationContext("CaseService.SaveEmployeeClass"))
            {
                if (employeeClass == null)
                    return null;

                EmployeeClass savedEmployeeClass = EmployeeClass.GetById(employeeClass.Id);

                if (savedEmployeeClass != null && (!savedEmployeeClass.IsCustom || savedEmployeeClass.EmployerId != EmployerId))
                    employeeClass.Clean();

                employeeClass.CustomerId = CustomerId;
                employeeClass.EmployerId = EmployerId;

                return employeeClass.Save();
            }
        }

        /// <summary>
        /// Delete Employee Class
        /// </summary>
        /// <param name="employeeClass"></param>
        public void DeleteEmployeeClass(EmployeeClass employeeClass)
        {
            if(employeeClass == null)
            {
                return;
            }
            using (new InstrumentationContext("CaseService.DeleteEmployeeClass"))
            {
                employeeClass.Delete();
            }
        }

        /// <summary>
        /// Add or remove code from supress entity
        /// </summary>
        /// <param name="suppressedEntities"></param>
        /// <param name="suppressedEntity"></param>
        /// <returns></returns>
        private List<SuppressedEntity> AddOrRemoveSuppressedEntity(List<SuppressedEntity> suppressedEntities, SuppressedEntity suppressedEntity)
        {
            SuppressedEntity entity = suppressedEntities
                .FirstOrDefault(se => se.EntityCode == suppressedEntity.EntityCode && se.EntityName == suppressedEntity.EntityName);
            if (entity == null)
            {
                suppressedEntities.Add(suppressedEntity);
            }
            else
            {
                suppressedEntities.Remove(entity);
            }

            return suppressedEntities;
        }

    }
}
