﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft;
using System.Text;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Data.Notes;
using MongoDB.Bson.Serialization;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Rules;

namespace AbsenceSoft.Logic.Customers
{
    public class PayScheduleService
    {
        /// <summary>
        /// get a list of payschedules for an employer
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<PaySchedule> PayScheduleForEmployer(string employerId)
        {
            return PaySchedule.AsQueryable().Where(ps => ps.EmployerId == employerId).ToList();
        }

        /// <summary>
        /// update a pay schedule
        /// Will update the employer 
        /// </summary>
        /// <param name="ps"></param>
        /// <returns>success</returns>
        public void PayscheduleUpdatesert(PaySchedule ps)
        {
            if (!string.IsNullOrWhiteSpace(ps.EmployerId) && string.IsNullOrWhiteSpace(ps.CustomerId) && ps.Employer != null)
                ps.CustomerId = ps.Employer.CustomerId;
            ps.Save();

            /// If we've set it as the default, we need to update the employer
            if (ps.Default)
            {
                Employer e = ps.Employer;
                /// Before bothering to update the employer document, lets make sure we're not just saving the 
                /// the already existing default pay schedule
                if (e.DefaultPayScheduleId != ps.Id)
                {
                    e.DefaultPayScheduleId = ps.Id;
                    e.Save();
                }
            }
        }

        /// <summary>
        /// No tricks here, deletes the pay schedule
        /// </summary>
        /// <param name="ps"></param>
        public void PayScheduleDelete(PaySchedule ps)
        {
            ps.Delete();
        }

        /// <summary>
        /// Return the dates that are on a schedule that include the passed in date
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        public List<DateTime> PayDatesForSchedule(PaySchedule theSchedule, DateTime workDate)
        {
            List<DateTime> theDates = new List<DateTime>();

            switch(theSchedule.PayPeriodType)
            {
                case Data.Enums.PayPeriodType.BiWeekly:
                    theDates = BiWeeklySchedule(theSchedule, workDate);
                    break;
                case Data.Enums.PayPeriodType.Weekly:
                    theDates = WeeklySchedule(theSchedule, workDate);
                    break;
                case Data.Enums.PayPeriodType.Monthly:
                    theDates = MonthlySchedule(theSchedule, workDate);
                    break;
                case Data.Enums.PayPeriodType.SemiMonthly:
                    theDates = SemiMonthlySchedule(theSchedule, workDate);
                    break;
                default:
                    throw new AbsenceSoftException(string.Format("Unsupported pay schedule type: {0}", Enum.GetName(typeof(Data.Enums.PayPeriodType), theSchedule.PayPeriodType)));
            }

            return theDates;
        }


        /// <summary>
        /// build a weekly schedule around the work date
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private List<DateTime> WeeklySchedule(PaySchedule theSchedule, DateTime workDate)
        {
            // if called in error....
            if(theSchedule.PayPeriodType != PayPeriodType.Weekly)
                return null;

            // find the day of the week that we are looking for
            int dow = (int)workDate.DayOfWeek;

            // schedule start on a day other than Sunday and this one needs to wrap back around
            if (dow < theSchedule.DayStart)
                dow += 7;

            dow = (dow - theSchedule.DayStart);
            DateTime startDate = workDate.AddDays(-dow);

            List<DateTime> rtVal = Enumerable.Range(0, 7).Select(r => startDate.AddDays(r)).ToList();

            return rtVal;
        }

        /// <summary>
        /// given a schedule start date and a target date return the 
        /// dates that are in that pay date range
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private List<DateTime> BiWeeklySchedule(PaySchedule theSchedule, DateTime workDate)
        {
            // if called in error....
            if (theSchedule.PayPeriodType != PayPeriodType.BiWeekly)
                return null;

            int days = (theSchedule.StartDate - workDate).Days;
            int pos = Math.Abs(days) % 14;
            if (days > 0)
            {
                pos = 14 - pos;
                if (pos == 14)
                    pos = 0;
            }

            DateTime startDate = workDate.AddDays(-pos);

            List<DateTime> rtVal = Enumerable.Range(0, 14).Select(r => startDate.AddDays(r)).ToList();

            return rtVal;

        }

        /// <summary>
        /// you get paid once a month
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private List<DateTime> MonthlySchedule(PaySchedule theSchedule, DateTime workDate)
        {
            // if called in error....
            if (theSchedule.PayPeriodType != PayPeriodType.Monthly)
                return null;

            int days = (workDate.GetLastDayOfMonth() - workDate.GetFirstDayOfMonth()).Days + 1;

            DateTime startDate = workDate.GetFirstDayOfMonth();

            List<DateTime> rtVal = Enumerable.Range(0, days).Select(r => startDate.AddDays(r)).ToList();

            return rtVal;

        }

        /// <summary>
        /// paid twice a month, the first through x and x + 1 through the end of the month
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private List<DateTime> SemiMonthlySchedule(PaySchedule theSchedule, DateTime workDate)
        {
            // if called in error....
            if (theSchedule.PayPeriodType != PayPeriodType.SemiMonthly)
                return null;

            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if(workDate.Day > theSchedule.DayEnd)
            {
                startDate = new DateTime(workDate.Year, workDate.Month, theSchedule.DayEnd + 1, 0, 0, 0, DateTimeKind.Utc);
                endDate = startDate.GetLastDayOfMonth();
            }
            else
            {
                startDate = workDate.GetFirstDayOfMonth();
                endDate = new DateTime(workDate.Year, workDate.Month, theSchedule.DayEnd, 0, 0, 0, DateTimeKind.Utc);
            }

            int days = (endDate - startDate).Days + 1;
        
            List<DateTime> rtVal = Enumerable.Range(0, days).Select(r => startDate.AddDays(r)).ToList();

            return rtVal;

        }                                   // private List<DateTime> SemiMonthlySchedule(PaySchedule theSchedule, DateTime workDate)


        /// <summary>
        /// Take a pay schedule, and then from the start around this date, find the begining of the
        /// schedule for that date, and then generate a list of dates for payments that end
        /// durring a time that include the end date
        /// </summary>
        /// <param name="theSchedule"></param>
        /// <param name="startAroundThisDate"></param>
        /// <param name="endAroundThisDate"></param>
        /// <returns></returns>
        public List<PaySchedulePayments> PaymentDateRangesForSchedule(PaySchedule theSchedule, DateTime startAroundThisDate, DateTime endAroundThisDate)
        {
            if (startAroundThisDate > endAroundThisDate)
                throw new AbsenceSoftException("PaymentDateRangesForSchedule start date must be before end date.");

            List<PaySchedulePayments> payDates = new List<PaySchedulePayments>();

            // get the first schedule
            List<DateTime> dates = PayDatesForSchedule(theSchedule, startAroundThisDate);

            // get the first date
            PaySchedulePayments psp = new PaySchedulePayments()
            {
                StartDate = dates[0],
                EndDate = dates[dates.Count - 1],
                PayPeriodType = theSchedule.PayPeriodType,
                ProcessingDays = theSchedule.ProcessingDays
            };
            payDates.Add(psp);

            // and go until we have reached or passed our end date
            while(psp.EndDate < endAroundThisDate)
            {
                // get the next set of dates
                dates = PayDatesForSchedule(theSchedule, dates[dates.Count - 1].AddDays(1));

                // create our new range
                psp = new PaySchedulePayments()
                {
                    StartDate = dates[0],
                    EndDate = dates[dates.Count - 1],
                    PayPeriodType = theSchedule.PayPeriodType,
                    ProcessingDays = theSchedule.ProcessingDays
                };
                payDates.Add(psp);
            }

            // and here are your dates
            return payDates;
        }

        /// <summary>
        /// This method takes an employee, checks to see if the payschedule is null and then runs the rules engine
        /// against the rules on the payscedules 
        /// </summary>
        /// <param name="theEmployee"></param>
        public void SetDefaultEmployeePaySchedule(Employee theEmployee)
        {
            using (new InstrumentationContext("PaySceduleService.SetEmployeePaySchedule"))
            {
                // if the payschedule is set then there is nothing to do
                if (!string.IsNullOrWhiteSpace(theEmployee.PayScheduleId))
                    return;

                List<PaySchedule> schedules = this.PayScheduleForEmployer(theEmployee.EmployerId);

                // find the default if (if there is one)
                PaySchedule defaultSched = theEmployee.Employer.DefaultPaySchedule;
 
                // now... run any rules
                PaySchedule fromRule = null;

                LeaveOfAbsence loa = new LeaveOfAbsence();
                loa.Employee = theEmployee;
                loa.Employer = theEmployee.Employer;
                loa.WorkSchedule = theEmployee.WorkSchedules;

                // loop across all the payschedules, evaluate any that have rules, and stop at the first
                // on that has a passing rule group
                foreach (PaySchedule ps in schedules)
                {
                    if (ps.AssignmentRules == null)
                        continue;

                    foreach (Rule ar in ps.AssignmentRules.Rules)
                        EvaluateRule(loa, ar);

                    ps.AssignmentRules.HasBeenEvaluated = true;
                    if(ps.AssignmentRules.Pass)
                    {
                        fromRule = ps;
                        break;
                    }
                }

                // try and assign something
                if(fromRule == null && defaultSched == null)
                    throw new AbsenceSoftException("No default or rule avaible to set employee pay schedule");

                theEmployee.PayScheduleId = (fromRule ?? defaultSched).Id;
            }
        }

        private void EvaluateRule(LeaveOfAbsence loa, Rule rule)
        {
            using (new InstrumentationContext("PayScheduleService.EvaluateRule"))
            {
                // If the rule has been overridden, we still calculate because we need to show the user the original evaluation 

                AppliedRuleEvalResult ruleEvalResult = AppliedRuleEvalResult.Unknown;
                try
                {
                    rule.ActualValue = LeaveOfAbsenceEvaluator.Eval(rule.LeftExpression, loa);
                    bool result = LeaveOfAbsenceEvaluator.Eval<bool>(rule.ToString(), loa);
                    ruleEvalResult = result ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (NullReferenceException)
                {
                    // If the result was NULL and it threw an exception, that's OK, let's check
                    //  though to see if we were actually expecting null.
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                    if (rule.RightExpression == "null")
                        ruleEvalResult = rule.Operator == "==" ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (Exception)
                {
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                }

                rule.Result = ruleEvalResult;
            }
        }


    }                                               // public class PayScheduleService
}
