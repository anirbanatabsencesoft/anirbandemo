﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic.Customers
{
    /// <summary>
    /// The customer service is used to create and update customer information.
    /// 
    /// It provides validation and other related services for the customer data
    /// </summary>
    public class CustomerService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        public CustomerService() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public CustomerService(User currentUser) : base(currentUser) { }

        /// <summary>
        /// Gets the employers for current customer.
        /// </summary>
        /// <returns></returns>
        public List<Employer> GetEmployersForCurrentCustomer()
        {
            return Employer.AsQueryable().Where(e => e.CustomerId == CurrentCustomer.Id).ToList();
        }

        /// <summary>
        /// Creates a new customer but does not save it. Must call Update to save it.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subDomain"></param>
        /// <param name="phone"></param>
        /// <param name="address"></param>
        /// <param name="creator"></param>
        /// <returns></returns>
        public Customer CreateCustomer(string name, string subDomain, string phone, string fax, Address address, User creator)
        {
            return new Customer()
            {
                Name = name,
                Url = subDomain,
                CreatedById = creator.Id,
                ModifiedById = creator.Id,
                Contact = new Contact()
                {
                    Address = address,
                    Email = creator.Email,
                    FirstName = creator.FirstName,
                    LastName = creator.LastName,
                    WorkPhone = phone,
                    Fax = fax
                },
                BillingContact = new Contact()
                {
                    Address = address,
                    Email = creator.Email,
                    FirstName = creator.FirstName,
                    LastName = creator.LastName,
                    WorkPhone = phone,
                    Fax = fax
                },
                Features = new List<AppFeature>() {
					//Set Feature's default values
					new AppFeature() { Feature = Feature.ADA, Enabled = false },
                    new AppFeature() { Feature = Feature.ShortTermDisability, Enabled = false },
                    new AppFeature() { Feature = Feature.MultiEmployerAccess, Enabled = false },
                    new AppFeature() { Feature = Feature.GuidelinesData, Enabled = false },
                    new AppFeature() { Feature = Feature.PolicyConfiguration, Enabled = false },
                    new AppFeature() { Feature = Feature.CommunicationConfiguration, Enabled = true }, //On by default
					new AppFeature() { Feature = Feature.EmployeeSelfService, Enabled = false },
                    new AppFeature() { Feature = Feature.LOA, Enabled = true }, //On by default
					new AppFeature() { Feature = Feature.ShortTermDisablityPay, Enabled = false },
                    new AppFeature() { Feature = Feature.ESSDataVisibilityInPortal, Enabled = false },
                    new AppFeature() { Feature = Feature.WorkflowConfiguration, Enabled = false },
                    new AppFeature() { Feature = Feature.CaseReporter, Enabled = false },
                    new AppFeature() { Feature = Feature.WorkRelatedReporting, Enabled = false },
                    new AppFeature() { Feature = Feature.ESSWorkRelatedReporting, Enabled = false },
                    new AppFeature() { Feature = Feature.IPRestrictions, Enabled = false },
                    new AppFeature() { Feature = Feature.EmployeeConsults, Enabled = false },
                    new AppFeature() { Feature = Feature.InquiryCases, Enabled = false },
                    new AppFeature() { Feature = Feature.OrgDataVisibility, Enabled = false },
                    new AppFeature() { Feature = Feature.JSA, Enabled = false },
                    new AppFeature() { Feature = Feature.CustomContent, Enabled = false },
                    new AppFeature() { Feature = Feature.AdHocReporting, Enabled = false },
                    new AppFeature() { Feature = Feature.EmployerContact, Enabled = false },
                    new AppFeature() { Feature = Feature.EmployerServiceOptions, Enabled = false },
                    new AppFeature() { Feature = Feature.OshaReporting, Enabled = false },
                    new AppFeature() { Feature = Feature.RiskProfile, Enabled = false },
                    new AppFeature() { Feature = Feature.JobConfiguration, Enabled = false },
                    new AppFeature() { Feature = Feature.WorkRestriction, Enabled = false },
                    new AppFeature() { Feature = Feature.BusinessIntelligenceReport, Enabled = false },
                    new AppFeature() { Feature = Feature.ContactPreferences, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminPaySchedules, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminPhysicalDemands, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminCustomFields, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminNecessities, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminNoteCategories, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminConsultations, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminAssignTeams, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminOrganizations, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminDataUpload, Enabled = true }, //On by default
					new AppFeature() { Feature = Feature.AdminEmployers, Enabled = true },
                    new AppFeature() { Feature = Feature.AdminSecuritySettings, Enabled = false },
                    new AppFeature() { Feature = Feature.AccommodationTypeCategories, Enabled = false },
                    new AppFeature() { Feature = Feature.AdjustEmployeeStartDayOfWeek, Enabled = false },
                    new AppFeature() { Feature = Feature.SingleSignOn, Enabled = false },
                    new AppFeature() { Feature = Feature.PolicyCrosswalk, Enabled = false },
                    new AppFeature() { Feature = Feature.CaseBulkDownload, Enabled = false },
                    new AppFeature() { Feature = Feature.ReportingTwoBeta, Enabled = true },
                    new AppFeature() { Feature = Feature.CaseClosureCategories, Enabled = false },
                    new AppFeature() { Feature = Feature.ESSWorkSchedule, Enabled = false },
                    new AppFeature() { Feature = Feature.FlightCrew, Enabled = false },
                    new AppFeature() { Feature = Feature.AdminDenialReason, Enabled = false }
                },
            };
        } // CreateCustomer

        /// <summary>
        /// Whether a given email matches any of our customers domains
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string MatchesDomainContent(string email)
        {
            if (email == null)
                return null;

            email = email.ToLowerInvariant();

            var matchingDomainCustomer = Customer.AsQueryable().Where(c => c.EmailDomain != null).ToList()
                .FirstOrDefault(c => !string.IsNullOrEmpty(c.EmailDomain) && email.EndsWith(c.EmailDomain));

            if (matchingDomainCustomer == null)
                return null;

            using (ContentService svc = new ContentService(matchingDomainCustomer, null, null))
            {
                Content content = svc.Get("Web.SignUp.Account.MatchesDomain");
                if (content == null || string.IsNullOrEmpty(content.Value))
                    return "This email address matches an already registered customer's domain.";

                return content.Value;
            }
        }


        /// <summary>
        /// Gets a unique, unused customer URL suggestions for use by the UI when creating a new customer account.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GenerateCustomerUrlSuggestion(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return RandomString.GenerateUniqueId();

            string sug = Regex.Replace(name, @"[^A-Za-z_\-]", string.Empty);
            while (IsSubDomainInUse(sug))
                sug = string.Concat(sug, RandomString.Generate(1, false, true, false, false));

            return sug;
        } // GenerateCustomerUrlSuggestion


        /// <summary>
        /// Checks to see if a customer subdomain is currently in use by a different customer (is not available).
        /// </summary>
        /// <param name="subDomain">The sub-domain to check for</param>
        /// <param name="customerId">The current customer ID if any</param>
        /// <returns><c>true</c> if the domain is NOT available (in use); otherwise <c>false</c>.</returns>
        public bool IsSubDomainInUse(string subDomain, string customerId = null)
        {
            using (new InstrumentationContext("CustomerService.IsSubDomainInUse"))
            {
                if (string.IsNullOrWhiteSpace(subDomain))
                    return true;

                var lowered = subDomain.ToLowerInvariant();
                return Customer.AsQueryable().Where(c => c.Url == lowered && c.Id != customerId).Any() || Employer.AsQueryable().Where(e => e.Url == lowered).Any();
            }
        }

        /// <summary>
        /// Validate a customer can be saved before saving it
        /// </summary>
        /// <param name="cust">The Customer</param>
        public void Update(Customer cust)
        {
            using (new InstrumentationContext("CustomerService.Update"))
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                ValidateInfo(cust);
                bool isNew = cust.IsNew;
                cust.Save();
                if (isNew)
                {
                    EnsureDefaultRolesForCustomer(cust.Id);
                    demandService.SetupDemandData(cust.Id);
                }

            }

        } // Update


        /// <summary>
        /// Ensures the default roles for customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        public void EnsureDefaultRolesForCustomer(string customerId)
        {
            Customer customer = Customer.GetById(customerId);
            if (customer == null) return;
            bool LOAFeatureEnabled = customer.HasFeature(Feature.LOA);
            bool ADAFeatureEnabled = customer.HasFeature(Feature.ADA);
            bool STDFeatureEnabled = customer.HasFeature(Feature.ShortTermDisability);
            bool ESSFeatureEnabled = customer.HasFeature(Feature.EmployeeSelfService);

            // for all customer
            EnsureRole(customerId, "Absence Manager Supervisor (AMS)", new List<Permission>()
            {
                Permission.ADADetail,
                Permission.AdjudicateCase,
                Permission.AttachFile,
                Permission.CancelCase,
                Permission.CancelToDo,
                Permission.CompleteToDo,
                Permission.ChangeToDoDueDate,
                Permission.CreateCase,
                Permission.AddCertification,
                Permission.EditCertification,
                Permission.DeleteCertification,
                Permission.ViewCertificationDetail,
                Permission.ViewSTDDetail,
                Permission.ViewWorkRestrictionDetail,
                Permission.ViewContact,
                Permission.ViewPersonalContact,
                Permission.ViewMedicalContact,
                Permission.ViewAdministrativeContact,
                Permission.CreateContact,
                Permission.ViewEmployee,
                Permission.ViewEmployeeInfo,
                Permission.ViewEmployeeContactInfo,
                Permission.ViewEmployeeJobInfo,
                Permission.ViewEmployeeJobDetails,
                Permission.ViewEmployeeWorkSchedule,
                Permission.ViewEmployeeTimeTracker,
                Permission.ViewEmployeeAbsenceHistory,
                Permission.EditEmployeeInfo,
                Permission.EditEmployeeContactInfo,
                Permission.EditEmployeeJobInfo,
                Permission.EditEmployeeJobDetails,
                Permission.EditEmployeeWorkSchedule,
                Permission.CreateEmployee,
                Permission.CreateNotes,
                Permission.CreateToDo,
                Permission.DeleteContact,
                Permission.DeleteEmployee,
                Permission.DeleteNotes,
                Permission.EditCase,
                Permission.EditContact,
                Permission.EditEmployee,
                Permission.EditNotes,
                Permission.EditSalary,
                Permission.EditSSN,
                Permission.ReAssignCase,
                Permission.CaseAssignee,
                Permission.ReAssignToDo,
                Permission.RunAdHocReport,
                Permission.ExportReportDefinition,
                Permission.ImportReportDefinition,
                Permission.RunOshaReport,
                Permission.RunCaseManagerReport,
                Permission.RunSelfServiceReport,
                Permission.SendCommunication,
                Permission.TeamDashboard,
                Permission.SearchCases,
                Permission.ViewAttachment,
                Permission.ViewCaseDescription,
                Permission.ViewSalary,
                Permission.ViewSSN,
                Permission.ViewTeam ,
                Permission.ViewITOR,
                Permission.AddITOR,
                Permission.EditITOR,
                Permission.ApproveITOR,
                Permission.ViewCase,
                Permission.EditCaseReporterAndPhone
            }
            .AddRangeFluid(Permission.AllCommunications())
            .AddRangeFluid(Permission.AllToDos()));


            // If customer has LOA feature enabled
            if (LOAFeatureEnabled)
            {
                EnsureRole(customerId, "Absence Manager (AM)", new List<Permission>()
                {
                    Permission.TeamDashboard,
                    Permission.SearchCases,
                    Permission.ADADetail,
                    Permission.AdjudicateCase,
                    Permission.AttachFile,
                    Permission.CancelCase,
                    Permission.CancelToDo,
                    Permission.CompleteToDo,
                    Permission.ChangeToDoDueDate,
                    Permission.CreateCase,
                    Permission.AddCertification,
                    Permission.EditCertification,
                    Permission.DeleteCertification,
                    Permission.ViewCertificationDetail,
                    Permission.ViewSTDDetail,
                    Permission.ViewWorkRestrictionDetail,
                    Permission.ViewContact,
                    Permission.ViewPersonalContact,
                    Permission.ViewMedicalContact,
                    Permission.ViewAdministrativeContact,
                    Permission.CreateContact,
                    Permission.ViewEmployee,
                    Permission.ViewEmployeeInfo,
                    Permission.ViewEmployeeContactInfo,
                    Permission.ViewEmployeeJobInfo,
                    Permission.ViewEmployeeJobDetails,
                    Permission.ViewEmployeeWorkSchedule,
                    Permission.ViewEmployeeTimeTracker,
                    Permission.ViewEmployeeAbsenceHistory,
                    Permission.EditEmployeeInfo,
                    Permission.EditEmployeeContactInfo,
                    Permission.EditEmployeeJobInfo,
                    Permission.EditEmployeeJobDetails,
                    Permission.EditEmployeeWorkSchedule,
                    Permission.CreateEmployee,
                    Permission.CreateNotes,
                    Permission.CreateToDo,
                    Permission.DeleteContact,
                    Permission.DeleteEmployee,
                    Permission.DeleteNotes,
                    Permission.EditCase,
                    Permission.EditContact,
                    Permission.EditEmployee,
                    Permission.EditNotes,
                    Permission.EditSalary,
                    Permission.EditSSN,
                    Permission.ReAssignCase,
                    Permission.CaseAssignee,
                    Permission.ReAssignToDo,
                    Permission.SendCommunication,
                    Permission.ViewCaseDescription,
                    Permission.ViewAttachment,
                    Permission.ViewSalary,
                    Permission.ViewSSN,
                    Permission.ViewITOR,
                    Permission.AddITOR,
                    Permission.EditITOR,
                    Permission.ApproveITOR,
                    Permission.ViewCase,
                    Permission.EditCaseReporterAndPhone
                }
                .AddRangeFluid(Permission.AllCommunications())
                .AddRangeFluid(Permission.AllToDos()));

                // if customer has STD feature enabled
                if (STDFeatureEnabled)
                {
                    EnsureRole(customerId, "Short Term Disability Case Manager (STDCM)", new List<Permission>()
                    {
                        Permission.TeamDashboard,
                        Permission.SearchCases,
                        Permission.ADADetail,
                        Permission.AdjudicateCase,
                        Permission.AttachFile,
                        Permission.CancelCase,
                        Permission.CancelToDo,
                        Permission.CompleteToDo,
                        Permission.ChangeToDoDueDate,
                        Permission.CreateCase,
                        Permission.ViewSTDDetail,
                        Permission.ViewWorkRestrictionDetail,
                        Permission.ViewContact,
                        Permission.ViewPersonalContact,
                        Permission.ViewMedicalContact,
                        Permission.ViewAdministrativeContact,
                        Permission.CreateContact,
                        Permission.ViewEmployee,
                        Permission.ViewEmployeeInfo,
                        Permission.ViewEmployeeContactInfo,
                        Permission.ViewEmployeeJobInfo,
                        Permission.ViewEmployeeJobDetails,
                        Permission.ViewEmployeeWorkSchedule,
                        Permission.ViewEmployeeTimeTracker,
                        Permission.ViewEmployeeAbsenceHistory,
                        Permission.EditEmployeeInfo,
                        Permission.EditEmployeeContactInfo,
                        Permission.EditEmployeeJobInfo,
                        Permission.EditEmployeeJobDetails,
                        Permission.EditEmployeeWorkSchedule,
                        Permission.CreateEmployee,
                        Permission.CreateNotes,
                        Permission.CreateToDo,
                        Permission.DeleteContact,
                        Permission.DeleteEmployee,
                        Permission.DeleteNotes,
                        Permission.EditCase,
                        Permission.EditContact,
                        Permission.EditEmployee,
                        Permission.EditNotes,
                        Permission.EditSalary,
                        Permission.EditSSN,
                        Permission.ReAssignCase,
                        Permission.CaseAssignee,
                        Permission.ReAssignToDo,
                        Permission.SendCommunication,
                        Permission.ViewCaseDescription,
                        Permission.ViewAttachment,
                        Permission.ViewSalary,
                        Permission.ViewSSN,
                        Permission.ViewITOR,
                        Permission.AddITOR,
                        Permission.EditITOR,
                        Permission.ApproveITOR,
                        Permission.ViewCase,
                        Permission.EditCaseReporterAndPhone
                    }
                    .AddRangeFluid(Permission.AllCommunications())
                    .AddRangeFluid(Permission.AllToDos()));
                }

                EnsureRole(customerId, "Customer Service Representative (CSR)", new List<Permission>()
                {
                    Permission.TeamDashboard,
                    Permission.SearchCases,
                    Permission.ADADetail,
                    Permission.CompleteToDo,
                    Permission.ChangeToDoDueDate,
                    Permission.CreateCase,
                    Permission.ViewSTDDetail,
                    Permission.ViewWorkRestrictionDetail,
                    Permission.ViewContact,
                    Permission.ViewPersonalContact,
                    Permission.ViewMedicalContact,
                    Permission.ViewAdministrativeContact,
                    Permission.ViewEmployee,
                    Permission.ViewEmployeeInfo,
                    Permission.ViewEmployeeContactInfo,
                    Permission.ViewEmployeeJobInfo,
                    Permission.ViewEmployeeJobDetails,
                    Permission.ViewEmployeeWorkSchedule,
                    Permission.ViewEmployeeTimeTracker,
                    Permission.ViewEmployeeAbsenceHistory,
                    Permission.EditEmployeeInfo,
                    Permission.EditEmployeeContactInfo,
                    Permission.EditEmployeeJobInfo,
                    Permission.EditEmployeeJobDetails,
                    Permission.EditEmployeeWorkSchedule,
                    Permission.CreateContact,
                    Permission.CreateEmployee,
                    Permission.CreateNotes,
                    Permission.CreateToDo,
                    Permission.EditContact,
                    Permission.EditEmployee,
                    Permission.SendCommunication,
                    Permission.ViewCaseDescription,
                    Permission.CaseAssignee,
                    Permission.EditCase,
                    Permission.ViewITOR,
                    Permission.AddITOR,
                    Permission.EditITOR,
                    Permission.ApproveITOR,
                    Permission.ViewCase
                }
                .AddRangeFluid(Permission.Communications("ELGNOTICE", "HRNOTICE", "MGRNOTICE"))
                .AddRangeFluid(Permission.ToDos(ToDoItemType.Communication, ToDoItemType.Manual)));
            }

            // if customer has ADA feature enabled
            if (ADAFeatureEnabled)
            {
                EnsureRole(customerId, "Accommodations Case Manager (ACM)", new List<Permission>()
                {
                    Permission.TeamDashboard,
                    Permission.SearchCases,
                    Permission.ADADetail,
                    Permission.AdjudicateCase,
                    Permission.AttachFile,
                    Permission.CancelCase,
                    Permission.CancelToDo,
                    Permission.CompleteToDo,
                    Permission.ChangeToDoDueDate,
                    Permission.CreateCase,
                    Permission.ViewSTDDetail,
                    Permission.ViewWorkRestrictionDetail,
                    Permission.ViewContact,
                    Permission.ViewPersonalContact,
                    Permission.ViewMedicalContact,
                    Permission.ViewAdministrativeContact,
                    Permission.CreateContact,
                    Permission.ViewEmployee,
                    Permission.ViewEmployeeInfo,
                    Permission.ViewEmployeeContactInfo,
                    Permission.ViewEmployeeJobInfo,
                    Permission.ViewEmployeeJobDetails,
                    Permission.ViewEmployeeWorkSchedule,
                    Permission.ViewEmployeeTimeTracker,
                    Permission.ViewEmployeeAbsenceHistory,
                    Permission.EditEmployeeInfo,
                    Permission.EditEmployeeContactInfo,
                    Permission.EditEmployeeJobInfo,
                    Permission.EditEmployeeJobDetails,
                    Permission.EditEmployeeWorkSchedule,
                    Permission.CreateEmployee,
                    Permission.CreateNotes,
                    Permission.CreateToDo,
                    Permission.DeleteContact,
                    Permission.DeleteEmployee,
                    Permission.DeleteNotes,
                    Permission.EditCase,
                    Permission.EditContact,
                    Permission.EditEmployee,
                    Permission.EditNotes,
                    Permission.RunAdHocReport,
                    Permission.ExportReportDefinition,
                    Permission.ImportReportDefinition,
                    Permission.RunOshaReport,
                    Permission.ReAssignCase,
                    Permission.CaseAssignee,
                    Permission.ReAssignToDo,
                    Permission.SendCommunication,
                    Permission.ViewCaseDescription,
                    Permission.ViewAttachment,
                    Permission.ADAInteractiveProcess,
                    Permission.ADAEditInteractiveProcess,
                    Permission.AddAccommodation,
                    Permission.ViewCase,
                    Permission.EditCaseReporterAndPhone
                }
                .AddRangeFluid(Permission.AllCommunications())
                .AddRangeFluid(Permission.AllToDos()));
            }

            if (ESSFeatureEnabled)
                EnsureDefaultSelfServiceRolesForCustomer(customer);

        }//end: EnsureDefaultRolesForCustomer		

        public void EnsureDefaultSelfServiceRolesForCustomer(Customer customer)
        {
            EnsureRole(customer.Id, "Employee", new List<Permission>()
            { 
				//Employee
				Permission.ViewEmployee,
                Permission.ViewEmployeeInfo,
                Permission.ViewEmployeeContactInfo,
                Permission.ViewEmployeeWorkSchedule,
                Permission.ViewEmployeeAbsenceHistory,
                Permission.EditEmployee,
                Permission.EditEmployeeContactInfo,

				//Contact
				Permission.ViewContact,
                Permission.ViewPersonalContact,
                Permission.ViewMedicalContact,
                Permission.CreateContact,
                Permission.EditContact,
                Permission.DeleteContact,

				//Case
				Permission.ViewCase,
                Permission.CreateCase,
                Permission.CancelCase,
                Permission.AttachFile,
                Permission.ViewAttachment,
                Permission.ViewCaseDescription,
                Permission.ViewWorkRestrictionDetail,
                Permission.ViewCertificationDetail,

                Permission.ViewITOR,

				//ToDo
				Permission.ViewTodo,
                Permission.CompleteToDo,

				//Notes
				Permission.ViewConfidentialNotes,
                Permission.CreateNotes,
                Permission.EditNotes,

				//SelfService
				Permission.EmployeeDashboard,

				//Communications
			},
            RoleType.SelfService,
            true, false, false);

            EnsureRole(customer.Id, "Supervisor", new List<Permission>()
            { 
				//Employee
				Permission.ViewEmployee,
                Permission.ViewEmployeeInfo,
                Permission.ViewEmployeeContactInfo,
                Permission.ViewEmployeeJobInfo,
                Permission.ViewEmployeeJobDetails,
                Permission.ViewEmployeeWorkSchedule,
                Permission.ViewEmployeeAbsenceHistory,
                Permission.EditEmployee,
                Permission.EditEmployeeJobDetails,
                Permission.EditEmployeeWorkSchedule,

				//Contact
				Permission.ViewContact,
                Permission.ViewAdministrativeContact,
                Permission.CreateContact,
                Permission.EditContact,

				//Case
				Permission.ViewCase,
                Permission.AttachFile,
                Permission.ViewAttachment,
                Permission.ViewCaseDescription,
                Permission.ViewCertificationDetail,

                Permission.ViewITOR,

				//ToDo
				Permission.ReAssignTodo,

				//Notes
				Permission.CreateNotes,
                Permission.EditNotes,

				//Reports
				Permission.RunSelfServiceReport, 

				//SelfService
				Permission.MyEmployeesDashboard,
                Permission.EmployeeDashboard

            }
            .AddRangeFluid(Permission.ToDos
            (
                //Todos
                ToDoItemType.ReturnToWork,
                ToDoItemType.VerifyReturnToWork,
                ToDoItemType.ScheduleErgonomicAssessment,
                ToDoItemType.CompleteErgonomicAssessment,
                ToDoItemType.EquipmentSoftwareInstalled,
                ToDoItemType.EquipmentSoftwareOrdered,
                ToDoItemType.OtherAccommodation
            )),
            RoleType.SelfService,
            false, false, true, new List<string>{"SUPERVISOR"});

            EnsureRole(customer.Id, "Human Resource", new List<Permission>()
            { 
				//Employee
				Permission.ViewEmployee,
                Permission.ViewEmployeeInfo,
                Permission.ViewEmployeeContactInfo,
                Permission.ViewEmployeeJobInfo,
                Permission.ViewEmployeeJobDetails,
                Permission.ViewEmployeeWorkSchedule,
                Permission.ViewEmployeeTimeTracker,
                Permission.ViewEmployeeAbsenceHistory,
                Permission.EditEmployee,
                Permission.EditEmployeeContactInfo,
                Permission.EditEmployeeJobDetails,
                Permission.EditEmployeeWorkSchedule,					

				//Contact
				Permission.ViewContact,
                Permission.ViewAdministrativeContact,
                Permission.CreateContact,
                Permission.EditContact,

				//Case
				Permission.ViewCase,
                Permission.AttachFile,
                Permission.ViewCaseDescription,
                Permission.ViewAttachment,
                Permission.ViewCertificationDetail,
                Permission.CreateCase,
                Permission.EditAttachment,
                Permission.DeleteAttachment,

                Permission.ViewITOR,

				//ToDo
				Permission.ReAssignTodo,

				//Notes
				Permission.CreateNotes,
                Permission.EditNotes,

				//Reports
				Permission.RunSelfServiceReport, 

				//SelfService
				Permission.MyEmployeesDashboard,
                Permission.EmployeeDashboard
            }
            .AddRangeFluidIf(new List<Permission>(2) { Permission.ADAInteractiveProcess, Permission.ADADetail }, () => customer.HasFeature(Feature.ADA))
            .AddRangeFluid(Permission.ToDos
            (
                //Todos
                ToDoItemType.ReturnToWork,
                ToDoItemType.VerifyReturnToWork,
                ToDoItemType.ScheduleErgonomicAssessment,
                ToDoItemType.CompleteErgonomicAssessment,
                ToDoItemType.EquipmentSoftwareInstalled,
                ToDoItemType.EquipmentSoftwareOrdered,
                ToDoItemType.OtherAccommodation
            )),
            RoleType.SelfService,
            false, false, true, new List<string> { "HR"});
        }


        private Role EnsureRole(string customerId, string roleName, List<Permission> permissions, RoleType roleType = RoleType.Portal, bool defaulRole = false, bool alwaysAssign = false, bool revokeAccess = true,  List<string> contactType = null)
        {
            Role role = Role.AsQueryable().Where(r => r.CustomerId == customerId && r.Name == roleName).FirstOrDefault();
#if DEBUG || DEVELOPMENT
            if (role != null)
                return role;
#endif
            role = role ?? new Role();
            role.Name = roleName;
            role.CustomerId = customerId;
            role.CreatedBy = User.Current ?? User.System;
            role.ModifiedBy = User.Current ?? User.System;
            role.Permissions = permissions.Select(p => p.Id).ToList();
            role.Type = roleType;
            role.DefaultRole = defaulRole;
            role.AlwaysAssign = alwaysAssign;
            role.RevokeAccessIfNoContactsOfTypes = revokeAccess;

            if (contactType != null)
            {
                foreach(string contact in contactType)
                    role.ContactTypes.AddIfNotExists(contact);
            }

            return role.Save();
        }//end: EnsureRole


        // perform all the checks on the customer
        public void ValidateInfo(Customer cust)
        {
            if (IsSubDomainInUse(cust.Url, cust.Id))
                throw new AbsenceSoftException("Customer Service: Customer or Employer exists with the same URL");
        } // ValidateInfo
    }
}
