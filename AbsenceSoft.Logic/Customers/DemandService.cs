﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers.Contracts;

namespace AbsenceSoft.Logic.Customers
{
    public class DemandService : LogicService, IDemandService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DemandService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public DemandService(User currentUser) : base(currentUser) { }

        public DemandService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            :base(currentCustomer, currentEmployer, currentUser)
        {

        }

        #region Demand Logic

        /// <summary>
        /// Gets a list of demands based on the specified criteria.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetDemandList(ListCriteria criteria)
        {
            using (new InstrumentationContext("DemandService.GetDemandList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                var results = new ListResults(criteria);

                var name = criteria.Get<string>("Name");
                var description = criteria.Get<string>("Description");
                var code = criteria.Get<string>("Code");

                var ands = Demand.DistinctAnds(CurrentUser, CustomerId, EmployerId);

                if (!string.IsNullOrEmpty(name))
                    Demand.Query.MatchesString(d => d.Name, name, ands);
                if (!string.IsNullOrEmpty(description))
                    Demand.Query.MatchesString(d => d.Description, description, ands);
                if (!string.IsNullOrEmpty(code))
                    Demand.Query.MatchesString(d => d.Code, code, ands);

                var demands = Demand.DistinctAggregation(ands);
                results.Total = demands.Count;
                demands = demands
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();

                results.Results = demands
                    .Select(d => new ListResult()
                        .Set("Id", d.Id)
                        .Set("Name", d.Name)
                        .Set("Code", d.Code)
                        .Set("Description", d.Description)
                        .Set("Order", d.Order));

                return results;
            }
        }

        /// <summary>
        /// Returns a list of all demands for a specified customer and employer.
        /// </summary>
        /// <returns></returns>
        public List<Demand> GetDemands()
        {
            return Demand.DistinctFind(null, CustomerId, EmployerId).ToList();
        }

        private bool IsValidCustomerOrEmployer()
        {
            return CurrentCustomer != null
                && (CurrentCustomer == null || !CurrentCustomer.IsDeleted)
                && (CurrentEmployer == null || !CurrentEmployer.IsDeleted);
        }

        /// <summary>
        /// Saves a Physical Demand.
        /// </summary>
        /// <param name="demand">The demand to save.</param>
        /// <returns></returns>
        public Demand SaveDemand(Demand demand)
        {
            if (demand == null || !IsValidCustomerOrEmployer())
                return null;

            if (!string.IsNullOrEmpty(demand.Id))
            {
                var existingDemand = Demand.GetById(demand.Id);
                if (existingDemand != null
                    && (!existingDemand.IsCustom || existingDemand.EmployerId != EmployerId))
                {
                    demand.Clean();
                }
            }

            demand.CustomerId = CustomerId;
            demand.EmployerId = EmployerId;
            return demand.Save();
        }

        /// <summary>
        /// Deletes a demand.
        /// </summary>
        /// <param name="demand">The demand to delete.</param>
        public void DeleteDemand(Demand demand)
        {
            if (demand == null || !IsValidCustomerOrEmployer())
                return;

            var existingDemand = DemandType.GetById(demand.Id);
            if (existingDemand != null
                && (existingDemand.IsCustom || existingDemand.EmployerId != EmployerId))
            {
                return; // Cannot delete custom default or customer based demand from employer level.
            }

            demand.Delete();
        }
        
        #endregion 

        #region Demand Type Logic

        /// <summary>
        /// Gets a list of demand types based on the specified criteria.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetDemandTypeList(ListCriteria criteria)
        {
            using (new InstrumentationContext("DemandService.GetDemandTypeList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                var results = new ListResults(criteria);

                var name = criteria.Get<string>("Name");
                var description = criteria.Get<string>("Description");
                var code = criteria.Get<string>("Code");

                var ands = DemandType.DistinctAnds(CurrentUser, CustomerId, EmployerId);

                if (!string.IsNullOrEmpty(name))
                    DemandType.Query.MatchesString(d => d.Name, name, ands);
                if (!string.IsNullOrEmpty(description))
                    DemandType.Query.MatchesString(d => d.Description, description, ands);
                if (!string.IsNullOrEmpty(code))
                    DemandType.Query.MatchesString(d => d.Code, code, ands);

                var demandTypes = DemandType.DistinctAggregation(ands);
                results.Total = demandTypes.Count;
                demandTypes = demandTypes
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();

                results.Results = demandTypes
                    .Select(d => new ListResult()
                        .Set("Id", d.Id)
                        .Set("Name", d.Name)
                        .Set("Description", d.Description)
                        .Set("Code", d.Code)
                        .Set("Order", d.Order));

                return results;
            }
        }

        /// <summary>
        /// Saves a Demand Type.
        /// </summary>
        /// <param name="demandType">The demand type to save.</param>
        /// <returns></returns>
        public DemandType SaveDemandType(DemandType demandType)
        {
            if (demandType == null || !IsValidCustomerOrEmployer())
                return null;

            if (!string.IsNullOrEmpty(demandType.Id))
            {
                var existingDemandType = DemandType.GetById(demandType.Id);
                if (existingDemandType != null
                    && (!existingDemandType.IsCustom || existingDemandType.EmployerId != EmployerId))
                {
                    demandType.Clean();
                }
            }

            demandType.CustomerId = CustomerId;
            demandType.EmployerId = EmployerId;
            return demandType.Save();
        }

        /// <summary>
        /// Deletes a demand type.
        /// </summary>
        /// <param name="demandType">The demand type to delete.</param>
        public void DeleteDemandType(DemandType demandType)
        {
            if (demandType == null || !IsValidCustomerOrEmployer())
                return;

            var existingDemandType = DemandType.GetById(demandType.Id);

            if (existingDemandType != null && (!demandType.Customer.IsDeleted 
                && (demandType.Employer == null || !demandType.Employer.IsDeleted) 
                && !string.IsNullOrEmpty(demandType.Id) && !string.IsNullOrEmpty(demandType.CustomerId) 
                && (demandType.Employer == null || !string.IsNullOrEmpty(demandType.EmployerId))
                )) {
                demandType.Delete();
            }
            else
                return;
        }
        
        /// <summary>
        /// Gets a list of demand types for a specified customer and employer.
        /// </summary>
        /// <returns></returns>
        public List<DemandType> GetDemandTypes()
        {
            return DemandType.DistinctFind(null, CustomerId, EmployerId).ToList();                
        }

        /// <summary>
        /// Gets a list of demand types for a specified customer and employer.
        /// </summary>
        /// <returns></returns>
        public List<DemandType> GetDemandTypes(string CustomerId, string EmployerId)
        {
            return DemandType.DistinctFind(null, CustomerId, EmployerId).ToList();
        }

        /// <summary>
        /// Gets a list of demand types for a demand.
        /// </summary>
        /// <param name="demandId">The demand identifier.</param>
        /// <returns></returns>
        public List<DemandType> GetDemandTypesForDemand(string demandId)
        {
            var demand = Demand.GetById(demandId);
            return DemandType.AsQueryable()
                .Where(dt => demand.Types.Contains(dt.Id))
                .ToList();
        }

        #endregion
        
        #region Demand Setup Logic 

        /// <summary>
        /// Sets up the demand data for the specified customer
        /// </summary>
        /// <param name="customerId"></param>
        public void SetupDemandData(string customerId)
        {
            CreateDemandTypes(customerId);
        }

        /// <summary>
        /// Creates the Demand Types for a specific customer
        /// </summary>
        /// <param name="customerId"></param>
        private static void CreateDemandTypes(string customerId)
        {
            var restriction = CreateDemandType(customerId, "Restrictions", "RESTRICTIONS", "The impact or scope of the restriction(s)", DemandValueType.Value, CreateRestrictionsItems(), 0, "Required", "Restrictions");
            var side = CreateDemandType(customerId, "Side", "SIDE", "The employee is required or restricted on one or both sides.", DemandValueType.Value, CreateSideDemandItems(), 1);
            var weight = CreateDemandType(customerId, "Weight Restrictions (lbs)", "WEIGHT", "The employee is required or restricted by the amount of weight", DemandValueType.Value, CreateWeightRestrictionDemandItems(), 2, "Weight Requirements (lbs)");
            var time = CreateDemandType(customerId, "Time Restrictions (hours)", "TIME", "The employee is required or restricted by the length of time", DemandValueType.Value, CreateTimeRestrictionDemandItems(), 3, "Time Requirements (hours)");
            var emptyText = CreateDemandType(customerId, "Text", "TEXT", "A text field", DemandValueType.Text);
            var yesNo = CreateDemandType(customerId, "Yes/No", "YESNO", "A Yes/No question", DemandValueType.Boolean);
            var schedule = CreateDemandType(customerId, "Schedule", "SCHEDULE", "The employee schedule is required or restricted", DemandValueType.Schedule);
            CreateDemands(customerId, side, weight, time, emptyText, yesNo, schedule, restriction);
        }

        private static List<DemandItem> CreateRestrictionsItems()
        {
            var restrictions = new List<DemandItem>
            {
                new DemandItem
                {
                    Name = "None",
                    Value = 0
                },
                new DemandItem
                {
                    Name = "Fully",
                    Value = 2
                },
                new DemandItem
                {
                    Name = "Partially",
                    Value = 1
                }
            };

            return restrictions;
        }

        /// <summary>
        /// Creates a list of sides values for a demand item
        /// </summary>
        /// <returns></returns>
        private static List<DemandItem> CreateSideDemandItems()
        {
            var sides = new List<DemandItem>
            {
                new DemandItem
                {
                    Name = "Left",
                    Value = 1
                },
                new DemandItem
                {
                    Name = "Right",
                    Value = 1
                },
                new DemandItem
                {
                    Name = "Both",
                    Value = 2
                }
            };

            return sides;
        }

        /// <summary>
        /// Creates a list of weight values for a demand item
        /// </summary>
        /// <returns></returns>
        private static List<DemandItem> CreateWeightRestrictionDemandItems()
        {
            var weightRestrictions = new List<DemandItem>
            {
                new DemandItem
                {
                    Name = "5 - 10 lbs",
                    Value = 5,
                    Classification = JobClassification.Light
                },
                new DemandItem
                {
                    Name = "11 - 20 lbs",
                    Value = 11,
                    Classification = JobClassification.Medium
                },
                new DemandItem
                {
                    Name = "21 - 30 lbs",
                    Value = 21,
                    Classification = JobClassification.Heavy
                },
                new DemandItem
                {
                    Name = "31 - 50 lbs",
                    Value = 31,
                    Classification = JobClassification.VeryHeavy
                }
            };

            return weightRestrictions;
        }

        /// <summary>
        /// Creates a list of time values for a demand item
        /// </summary>
        /// <returns></returns>
        private static List<DemandItem> CreateTimeRestrictionDemandItems()
        {
            var timeRestrictions = new List<DemandItem>
        {
                new DemandItem
                {
                    Name = "0 - 2.5 hrs",
                    Value = 1
                },
                new DemandItem
                {
                    Name = "2.5 - 5 hrs",
                    Value = 2
                },
                new DemandItem
                {
                    Name = "5 - 7.5 hrs",
                    Value = 3
                },
                new DemandItem
                {
                    Name = "7.5 - 10 hrs",
                    Value = 4
                }
            };

            return timeRestrictions;
        }

        /// <summary>
        /// Creates saves and returns a demand type
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="demandValueType"></param>
        /// <param name="items"></param>
        /// <param name="order"></param>
        /// <param name="requirementLabel"></param>
        /// <param name="restrictionLabel"></param>
        /// <returns></returns>
        private static DemandType CreateDemandType(
            string customerId,
            string name,
            string code,
            string description,
            DemandValueType demandValueType,
            List<DemandItem> items = null,
            int? order = null, 
            string requirementLabel = null,
            string restrictionLabel = null)
        {
            var demandType = new DemandType
            {
                CustomerId = customerId,
                Name = name,
                Code = code,
                Description = description,
                Type = demandValueType,
                Items = items,
                Order = order,
                RequirementLabel = requirementLabel,
                RestrictionLabel = restrictionLabel
            }.Save();

            return demandType;
        }

        /// <summary>
        /// Creates the default demands
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="side"></param>
        /// <param name="weight"></param>
        /// <param name="time"></param>
        /// <param name="emptyText"></param>
        /// <param name="yesNo"></param>
        /// <param name="schedule"></param>
        /// <param name="restriction"></param>
        private static void CreateDemands(
            string customerId,
            DemandType side,
            DemandType weight,
            DemandType time,
            DemandType emptyText,
            DemandType yesNo,
            DemandType schedule,
            DemandType restriction)
        {
            CreateDemand(customerId, "Carrying without handles", "The employee is restricted in what they can carry without handles", "CARRYWOH", 1, restriction, side, weight, time);
            CreateDemand(customerId, "Carrying with handles", "The employee is restricted in what they can carry with handles", "CARRYWH", 2, restriction, side, weight, time);
            CreateDemand(customerId, "Lifting Floor to Knee", "The employee is restricted in lifting floor to knee", "LFTK", 3, restriction, side, weight, time);
            CreateDemand(customerId, "Lifting Knee to Waist", "The employee is restricted in lifting knee to waist", "LKTW", 4, restriction, side, weight, time);
            CreateDemand(customerId, "Lifting Waist to Shoulder", "The employee is restricted in lifting waist to shoulder", "LWTS", 5, restriction, side, weight, time);
            CreateDemand(customerId, "Lifting Above Shoulder", "The employee is restricted in lifting above shoulder", "LAS", 6, restriction, side, weight, time);
            CreateDemand(customerId, "Push/Pull", "The employee is restricted in what they can push or pull", "PP", 7, restriction, side, weight, time);
            CreateDemand(customerId, "Fine finger dexterity", "The employee is restricted in their fine finger dexterity", "FFD", 8, restriction, side, time);
            CreateDemand(customerId, "Power grip", "The employee is restricted in their power grip", "POWG", 9, restriction, side, weight, time);
            CreateDemand(customerId, "Pinch grip", "The employee is restricted in their pinch grip", "PING", 10, restriction, side, weight, time);
            CreateDemand(customerId, "Simple grasping", "The employee is restricted in their simple grasping", "SG", 11, restriction, side, time);
            CreateDemand(customerId, "Forceful grasping", "The employee is restricted in their forceful grasping", "FG", 12, restriction, side, time);
            CreateDemand(customerId, "Overhead Reach Arms > 90 degrees", "The employee is restricted in their overhead reach", "ORA", 13, restriction, side, weight, time);
            CreateDemand(customerId, "At Shoulder Reach Arms > 90 degrees", "The employee is restricted in their reach at their shoulder", "ATSA", 14, restriction, side, weight, time);
            CreateDemand(customerId, "Below Shoulder Reach < 90 degrees", "The employee is restricted in their reach below their shoulder", "BSR", 15, restriction, side, weight, time);
            CreateDemand(customerId, "Head/Neck Flex/Extend > 20 degrees", "The employee is restricted in their head or neck flexibility", "HNFE", 16, restriction, side, time);
            CreateDemand(customerId, "Bend and stoop", "The employee is restricted in their bending or stooping", "BNS", 17, restriction, time);
            CreateDemand(customerId, "Climb Step Stool (up to 3 steps", "The employee is restricted in climbing a step stool", "CSS", 18, restriction, time);
            CreateDemand(customerId, "Climb Stairs", "The employee is restricted in climbing stairs", "CS", 19, restriction, time);
            CreateDemand(customerId, "Climb Ladder (over 3 steps", "The employee is restricted in climbing a ladder", "CL", 20, restriction, time);
            CreateDemand(customerId, "Crouch", "The employee is restricted in crouching", "CROU", 21, restriction, time);
            CreateDemand(customerId, "Kneel/Squat", "The employee is restricted in kneeling", "KNEEL", 22, restriction, time);
            CreateDemand(customerId, "Sit", "The employee is restricted in sitting", "SIT", 23, restriction, time);
            CreateDemand(customerId, "Stand", "The employee is restricted in standing", "STAND", 24, restriction, time);
            CreateDemand(customerId, "Walk", "The employee is restricted in walking", "WALK", 25, restriction, time);
            CreateDemand(customerId, "Pivot", "The employee is restricted in pivoting", "PIV", 26, restriction, time);
            CreateDemand(customerId, "Vision Restrictions", "The employee is restricted in their vision", "VR", 27, emptyText);
            CreateDemand(customerId, "Hearing Restrictions", "The employee is restricted in their hearing", "HR", 28, emptyText);
            CreateDemand(customerId, "Talking Restrictions", "The employee is restricting in their talking", "TR", 29, emptyText);
            CreateDemand(customerId, "Can the patient work more than 40 hours within a week?", "The employee is restricted in how much they can work", "MT40", 30, yesNo);
            CreateDemand(customerId, "Indicate the number of hours/days the patient can work", "The follow up to how much the employee is restricted if they can work", "INHPCW", 31, schedule);
            CreateDemand(customerId, "Can the patient drive commercial machinery such as a delivery van, forklift, reach truck, scissor lift, or truck", "The employee is restricted what machinery they can operate", "DRIV", 32, yesNo);
            CreateDemand(customerId, "Does the patient have restrictions not covered above, including physical restrictions, sensory restrictions, and those related to a psychological or mental condition?", "The employee is otherwise restricted", "MISC", 33, yesNo);
            CreateDemand(customerId, "Please list any applicable restrictions not included on the list above, including physical restrictions, sensory restrictions, and psychological/mental restrictions and expected duration:", "The employee has other not listed restrictions", "OTHER", 34, emptyText);
            CreateDemand(customerId, "Other comments", "Any other information", "OC", 35, emptyText);
        }

        /// <summary>
        /// Creates and saves a demand
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="code"></param>
        /// <param name="order"></param>
        /// <param name="types"></param>
        private static void CreateDemand(
            string customerId,
            string name,
            string description,
            string code,
            int? order,
            params DemandType[] types)
        {
            new Demand
            {
                CustomerId = customerId,
                Name = name,
                Description = description,
                Code=code,
                Order = order,
                Types = types.Select(s => s.Id).ToList()
            }.Save();
        }
        #endregion

        #region Job Restrictions

        /// <summary>
        /// Gets the job restrictions for employee.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<EmployeeRestriction> GetJobRestrictionsForEmployee(
            string employeeId,
            string caseId = null)
        {
            return EmployeeRestriction.AsQueryable()
                .Where(er => er.EmployeeId == employeeId && er.CaseId == caseId)
                .ToList();
        }

        /// <summary>
        /// Saves the job restriction.
        /// </summary>
        /// <param name="employeeRestriction">The employee restriction.</param>
        public void SaveJobRestriction(EmployeeRestriction employeeRestriction)
        {
            if (employeeRestriction.IsNew)
                employeeRestriction.WfOnWorkRestrictionCreated(employeeRestriction.Case);

            employeeRestriction.Save();
        }

        /// <summary>
        /// Deletes the job restriction.
        /// </summary>
        /// <param name="workRestrictionId">The work restriction identifier.</param>
        public void DeleteJobRestriction(string workRestrictionId)
        {
            var restriction = EmployeeRestriction.GetById(workRestrictionId);
            if (restriction != null)
                restriction.Delete();
        }

        /// <summary>
        /// Evaluates the jobs for an employee optionally by case and specific date.
        /// Query all Jobs for the employee's office location and dates of restrictions to
        /// determine which jobs are applicable/qualified based on this list of restrictions
        /// and reasons why they're allowed.
        /// </summary>
        /// <param name="employeeId">The employee to use for an evaluation basis.</param>
        /// <param name="caseId">The case identifier if running against a case (which you generally would).</param>
        /// <param name="asOf">As of date to specify which specific date is applicable in the inquiry (defaults to Today).</param>
        /// <returns></returns>
        public List<JobEvaluationResult> EvaluateJobsForEmployee(
            string employeeId,
            string caseId = null,
            DateTime? asOf = null)
        {
            return EvaluateJobsForEmployee(Employee.GetById(employeeId), caseId, asOf);
        }

        /// <summary>
        /// Evaluates the jobs for an employee optionally by case and specific date. 
        /// Query all Jobs for the employee's office location and dates of restrictions to 
        /// determine which jobs are applicable/qualified based on this list of restrictions 
        /// and reasons why they're allowed.
        /// </summary>
        /// <param name="employee">The employee to use for an evaluation basis.</param>
        /// <param name="caseId">The case identifier if running against a case (which you generally would).</param>
        /// <param name="asOf">As of date to specify which specific date is applicable in the inquiry (defaults to Today).</param>
        /// <returns></returns>
        public List<JobEvaluationResult> EvaluateJobsForEmployee(
            Employee employee,
            string caseId = null,
            DateTime? asOf = null)
        {
            // If we have no employee, wtf is there to evaluate then? Nothing, so return an empty list
            if (employee == null)
                return new List<JobEvaluationResult>(0);

            // Fetch the employee's current office location (as of today)
            var officeLocation = (asOf.HasValue 
                ? employee.GetOfficeLocations()
                    .FirstOrDefault(o => o.Dates == null || o.Dates.DateInRange(asOf.Value))
                    ?? employee.GetOfficeLocation()
                    ?? new EmployeeOrganization()
                : employee.GetOfficeLocation()
                    ?? new EmployeeOrganization()).Code;

            // Fetch the employee's current necessities (as of today)
            var necessities = employee.GetNecessities()
                .Where(n => n.Dates == null || n.Dates.DateInRange(asOf ?? DateTime.UtcNow.ToMidnight()))
                .ToList();

            // Fetch the employee's current restrictions (as of today)
            var restrictions = GetJobRestrictionsForEmployee(employee.Id, caseId)
                .Select(r => r.Restriction)
                .Where(r => r.Dates == null || r.Dates.DateInRange(asOf ?? DateTime.UtcNow.ToMidnight()))
                .ToList();

            // Fetch all of the potential jobs for that employee's office location
            var possibleJobs = new JobService(employee.CustomerId, employee.EmployerId, CurrentUser)
                .Using(j => j.GetJobsForLocation(officeLocation));

            // Return a list of evaluation results (while evaluating inline) based on the restrictions and necessities
            //  against the job
            return possibleJobs
                .Select(j => j.EvaluateRestrictions(restrictions, necessities))
                .ToList();
        }

        #endregion

        public Demand GetDemandById(string id)
        {
            return Demand.GetById(id);
        }
        
        public EmployeeRestriction GetEmployeeRestrictionById(string id)
        {
            return EmployeeRestriction.GetById(id);
        }

        /// <summary>
        /// Get demand by demand code
        /// </summary>
        /// <param name="demandCode"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public Demand GetDemandByCode(string demandCode, string customerId)
        {            
            var theDemand = Demand.AsQueryable().Where(d => d.Code == demandCode && d.CustomerId == customerId).FirstOrDefault();
            return theDemand;
        }

        /// <summary>
        /// Get demand type by demand type code
        /// </summary>
        /// <param name="demandTypeCode"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public DemandType GetDemandTypeByCode(string demandTypeCode, string customerId)
        {
            var theDemandType = DemandType.AsQueryable().Where(d => d.Code == demandTypeCode && d.CustomerId == customerId).FirstOrDefault();
            return theDemandType;
        }                
    }
}
