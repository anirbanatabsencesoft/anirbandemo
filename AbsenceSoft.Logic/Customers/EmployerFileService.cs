﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using System;
using System.IO;
using System.Web;

namespace AbsenceSoft.Logic.Customers
{
    public class EmployerFileService : LogicService, ILogicService
    {
        /// <summary>
        /// Gets the actual document underneath an employer file in order to download that document to the browser
        /// or file system as a byte array and returns the populated attachment instance with <c>File</c> populated.
        /// </summary>
        /// <param name="fileId">The GridFS file identifier.</param>
        /// <returns>A customer file with its File byte array representing the document populated.</returns>
        public string DownloadFile(string employerId, string fileId)
        {
            if (string.IsNullOrWhiteSpace(fileId))
                throw new ArgumentNullException("fileId");

            Document file = Document.GetById(fileId);
            if (file == null)
                return null;
            //if (User.Current != null && !string.IsNullOrWhiteSpace(User.Current.CustomerId) && (User.Current.CustomerId != file.CustomerId || User.Current.CustomerId != customerId))
            //    throw new AbsenceSoftException("User is not authorized for this file");
            if (employerId != file.EmployerId)
                throw new AbsenceSoftException("Employer mismatch");

            return file.DownloadUrl();
        }//DownloadFile


        /// <summary>
        /// Creates a file, uploads and stores the file and returns the saved result.
        /// </summary>
        /// <param name="employerId">The employer Id to save the file for</param>
        /// <param name="fileName">The name of the file that should be uploaded for the attachment</param>
        /// <param name="file">The file contents in a byte array</param>
        /// <param name="contentType">The content type of the file being stored</param>
        /// <returns>The saved file with the appropriate Id and FileId populated</returns>       
        public Document UploadFile(string employerId, string fileName, byte[] file, string contentType)
        {
            if (string.IsNullOrWhiteSpace(employerId))
                throw new ArgumentNullException("employerId");
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentNullException("fileName");
            if (file == null || file.Length == 0)
                throw new ArgumentNullException("file");

            Document customerFile = new Document()
            {
                EmployerId = employerId,
                ContentLength = file.LongLength,
                ContentType = contentType,
                FileName = fileName,
                File = file
            };

            return UploadFile(customerFile);
        }//UploadFile


        /// <summary>
        /// Creates a file, uploads and stores the file and returns the saved result.
        /// </summary>
        /// <param name="employerId">The employer Id that owns the file</param>
        /// <param name="file">The posted file of the attachment</param>
        /// <returns>The saved attachment with the appropriate Id and FileId populated</returns>
        public Document UploadFile(string employerId, HttpPostedFileBase file)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            return new Document()
            {
                EmployerId = employerId,
                ContentLength = file.ContentLength,
                ContentType = file.ContentType,
                FileName = file.FileName,
                File = file.InputStream.ReadAllBytes()
            }.Upload();
        }//UploadFile


        /// <summary>
        /// Creates an employer file, uploads and stores the file in the <c>File</c> property and returns the saved result.
        /// </summary>
        /// <param name="customerFile">The fully popluated customer file to save</param>
        /// <returns>The saved attachment with the appropriate Id and FileId populated</returns>
        public Document UploadFile(Document employerFile)
        {
            if (employerFile == null)
                throw new ArgumentNullException("employerFile");
            if (employerFile.File == null || employerFile.File.Length == 0)
                throw new ArgumentNullException("employerFile.File");

            return employerFile.Upload();
        }//UploadFile


        public void DeleteFile(string fileId)
        {
            if (string.IsNullOrWhiteSpace(fileId))
                throw new ArgumentNullException("fileId");

            Document doc = Document.GetById(fileId);
            if (doc == null)
                return;

            doc.Remove();
        }//DeleteFile
    }
}
