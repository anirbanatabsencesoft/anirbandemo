﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;

namespace AbsenceSoft.Logic.Customers
{
    public class ContactTypeService : LogicService, IContactTypeService
    {
        public ContactTypeService()
        {

        }

        public ContactTypeService(User currentUser)
            :base(currentUser)
        {

        }

        public List<ContactType> GetContactTypesForEmployee(Employee employee, ContactTypeDesignationType? category = null)
        {
            if (employee == null)
                throw new AbsenceSoftException("GetContactTypes: employee is null");

            List<ContactType> contactTypes = new List<ContactType>();

            if (category != null)
                contactTypes = GetContactTypes(employee.CustomerId, employee.EmployerId, category.Value);
            else
                contactTypes = GetContactTypes(employee.CustomerId, employee.EmployerId);

            var states = new List<string>();

            if (!string.IsNullOrWhiteSpace(employee.WorkState))
                states.Add(employee.WorkState);

            if (employee.Info != null && employee.Info.Address != null && !string.IsNullOrWhiteSpace(employee.Info.Address.State))
                states.Add(employee.Info.Address.State);

            //filter out by state
            if (states.Any())
                contactTypes = contactTypes.Where(c => c.WorkStateRestrictions == null || c.WorkStateRestrictions.Intersect(states).Any()).ToList();

            return contactTypes;
        }

        /// <summary>
        /// Get List of Medical Contact Type Codes.
        /// </summary>
        /// <returns>A List of Contact Type Codes.</returns>
        public List<string> GetMedicalContactTypeCodes()
        {
            using (new InstrumentationContext("ContactTypeService.GetMedicalContactTypes"))
            {
                var medicalContactTypesQuery = ContactType.Query.Find(ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Medical));
                return medicalContactTypesQuery.ToList<ContactType>().Select(ct => ct.Code).ToList();
            }
        }

        public List<ContactType> GetMedicalContactTypes(string employerId)
        {
            using (new InstrumentationContext("ContactTypeService.GetMedicalContactTypes"))
            {
                List<IMongoQuery> ands = ContactType.DistinctAnds(CurrentUser, CustomerId, employerId);
                ands.Add(ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Medical));               
                var medicalContactTypesQuery = ContactType.DistinctAggregation(ands);
                return medicalContactTypesQuery.ToList<ContactType>();
            }
        }

        public List<ContactType> GetEmployeeContactTypes()
        {
            using (new InstrumentationContext("ContactTypeService.GetEmployeeContactTypes"))
            {

                var employeeContactTypesQuery= ContactType.Query.Find(ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Personal));
              
                return employeeContactTypesQuery.ToList<ContactType>();
            }
        }

        /// <summary>
        /// Get List of Medical Contact Type Codes.
        /// </summary>
        /// <param name="customerId">The Customer Id</param>
        /// <param name="contactId">The Optional Contact Id to filter exclude</param>
        /// <param name="employeeId">The Employee Id</param>
        /// <returns>A List of Contact Type Codes.</returns>
        public void UpdateMedicalDesignationTypeContactsIsPrimary(string customerId, Guid? contactId, string employeeId)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new AbsenceSoftException("GetContactTypes: customer is null");
            if (string.IsNullOrEmpty(employeeId))
                throw new AbsenceSoftException("GetContactTypes: employee is null");

            using (new InstrumentationContext("ContactTypeService.UpdateMedicalDesignationTypeContactsIsPrimary"))
            {
                // get medical contact types
                var medicalContactTypesQuery = ContactType.Query.Find(ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Medical));
                var medicalContactTypeCodes = medicalContactTypesQuery.ToList<ContactType>().Select(ct => ct.Code).ToArray();

                // get employee medical contacts
                var employeeContactsQuery = EmployeeContact.Query.Find(
                    EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, customerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, employeeId),
                        EmployeeContact.Query.In(e => e.ContactTypeCode, medicalContactTypeCodes)
                ));

                List<string> employeeContactIds = null;
                if (contactId.HasValue)
                    employeeContactIds = employeeContactsQuery.ToList().Where(c => c.Contact.Id != contactId).Select(c => c.Id).ToList();
                else
                    employeeContactIds = employeeContactsQuery.ToList().Select(c => c.Id).ToList();

                // update employee contacts IsPrimary to false (except contactId param filtered out)
                EmployeeContact.Update(EmployeeContact.Query.In(c => c.Id, employeeContactIds), EmployeeContact.Updates.Set(c => c.Contact.IsPrimary, false).Set(u => u.ModifiedDate, DateTime.UtcNow), UpdateFlags.Multi);
            }
        }

        /// <summary>
        /// Get List of Contact Types
        /// </summary>
        /// <param name="category"></param>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<ContactType> GetContactTypes(string customerId, string employerId, params ContactTypeDesignationType[] category)
        {
            using (new InstrumentationContext("ContactTypeService.GetContactTypesByCategory"))
            {
                var criteria = new List<IMongoQuery>
                {
                    ContactType.Query.Or(
                        ContactType.Query.NotExists(r => r.CustomerId),
                        ContactType.Query.EQ(r => r.CustomerId, null),
                        ContactType.Query.EQ(r => r.CustomerId, customerId))
                };
                if (employerId != null)
                {
                    criteria.Add(ContactType.Query.Or(
                        ContactType.Query.EQ(r => r.EmployerId, employerId),
                        ContactType.Query.EQ(r => r.EmployerId, null),
                        ContactType.Query.NotExists(r => r.EmployerId)
                    ));
                }
                var qry = ContactType.Query.Find(ContactType.Query.And(criteria));
                var list = qry.ToList() ?? new List<ContactType>();

                if (category != null && category.Any())
                {
                    list = list.Where(c => category.Contains(c.ContactCategory)).ToList();
                }

                return list.OrderBy(c => c.Name).ToList();
            }

        }

        #region Employer Contact Types CRUD

        public ListResults ListEmployerContactTypes(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults results = new ListResults(criteria);

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(CurrentUser.BuildDataAccessFilters());

            string name = criteria.Get<string>("Name");
            string code = criteria.Get<string>("Code");
            EmployerContactType.Query.MatchesString(ect => ect.Name, name, ands);
            EmployerContactType.Query.MatchesString(ect => ect.Code, code, ands);

            var query = EmployerContactType.Query.Find(EmployerContactType.Query.And(ands));
            query = criteria.SetSortAndSkip<EmployerContactType>(query);

            results.Total = query.Count();
            results.Results = query.ToList().Select(ect => new ListResult()
                .Set("Id", ect.Id)
                .Set("Name", ect.Name)
                .Set("Code", ect.Code));

            return results;
        }

        public EmployerContactType GetContactTypeById(string id)
        {
            return EmployerContactType.GetById(id);
        }

        public EmployerContactType SaveContactType(EmployerContactType contactType)
        {
            if (contactType == null)
                throw new ArgumentNullException("contactType");

            contactType.CustomerId = CurrentCustomer.Id;

            return contactType.Save();
        }



        public void DeleteContactType(EmployerContactType contactType)
        {
            if (contactType == null)
                throw new ArgumentNullException("contactType");

            contactType.Delete();
        }

        public List<EmployerContactType> GetContactTypesForCustomer()
        {
            if (CurrentCustomer == null || !CurrentCustomer.HasFeature(Feature.EmployerContact))
                return new List<EmployerContactType>(0);
            return EmployerContactType.AsQueryable().Where(ect => ect.CustomerId == CurrentCustomer.Id).ToList();
        }

        #endregion
    }
}
