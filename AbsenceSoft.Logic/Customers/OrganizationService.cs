﻿using AbsenceSoft.Data;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AbsenceSoft.Logic.Customers.Contracts;

namespace AbsenceSoft.Logic.Customers
{
    public class OrganizationService : LogicService, IOrganizationService
    {
        public OrganizationService(User currentUser) : base(currentUser)
        {

        }

        public ListResults ListOrganizations(ListCriteria criteria)
        {
            using (new InstrumentationContext("OrganizationService.ListOrganizations"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);

                string name = criteria.Get<string>("Name");
                string description = criteria.Get<string>("Description");
                string code = criteria.Get<string>("Code");
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());

                if (!string.IsNullOrEmpty(name))
                {
                    ands.Add(Organization.Query.Matches(o => o.Name, new BsonRegularExpression(name, "i")));
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ands.Add(Organization.Query.Matches(o => o.Description, new BsonRegularExpression(description, "i")));
                }

                if (!string.IsNullOrEmpty(code))
                {
                    ands.Add(Organization.Query.Matches(o => o.Code, new BsonRegularExpression(code, "i")));
                }

                var query = Organization.Query.Find(Organization.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(o => new ListResult()
                    .Set("Id", o.Id)
                    .Set("EmployerId", o.EmployerId)
                    .Set("Name", o.Name)
                    .Set("Description", o.Description)
                    .Set("Code", o.Code)
                );

                return results;
            }
        }

        public EmployeeOrganization UpdateOrganization(EmployeeOrganization organization)
        {
            if (organization == null)
                return organization;

            organization.Save();
            // Only key this workflow if it's actually an office location
            if (organization.TypeCode == OrganizationType.OfficeLocationTypeCode)
            {
                // And if it is, and we know the employee (which we should) then we should  also update any cases
                if (organization.Employee != null)
                    Data.Cases.Case.UpdateCasesEmployeeCurrentOfficeLocation(organization.Employee.Id, organization.Code);
                organization.WfOnEmployeeOfficeLocationChanged(CurrentUser);
            }
            return organization;
        }

        public ListResults ListOrganizationTypes(ListCriteria criteria)
        {
            using (new InstrumentationContext("OrganizationService.ListOrganizationTypes"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);
                string employerId = criteria.Get<string>("EmployerId");
                string name = criteria.Get<string>("Name");
                string description = criteria.Get<string>("Description");
                string code = criteria.Get<string>("Code");
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());

                if (!string.IsNullOrEmpty(employerId))
                {
                    ands.Add(Query.EQ("EmployerId", new BsonObjectId(new ObjectId(employerId))));
                }

                if (!string.IsNullOrEmpty(name))
                {
                    ands.Add(OrganizationType.Query.Matches(o => o.Name, new BsonRegularExpression(name, "i")));
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ands.Add(OrganizationType.Query.Matches(o => o.Description, new BsonRegularExpression(description, "i")));
                }

                if (!string.IsNullOrEmpty(code))
                {
                    ands.Add(OrganizationType.Query.Matches(o => o.Code, new BsonRegularExpression(code, "i")));
                }

                var query = OrganizationType.Query.Find(OrganizationType.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(o => new ListResult()
                    .Set("Id", o.Id)
                    .Set("EmployerId", o.EmployerId)
                    .Set("Name", o.Name)
                    .Set("Description", o.Description)
                    .Set("Code", o.Code)
                );

                return results;
            }
        }

        /// <summary>
        /// Get organization types for a customer
        /// </summary>
        /// <param name="customerId"></param>
        public List<OrganizationType> GetOrganizationTypes(string userId)
        {
            using (new InstrumentationContext("OrganizationService.GetOrganizationTypes"))
            {
                EmployerAccess empAccess = User.GetById(userId).Employers.FirstOrDefault(emp => emp.EmployeeId != null);
                if (empAccess == null) return null;
                return OrganizationType.AsQueryable().Where(e => e.CustomerId == CurrentUser.CustomerId && e.EmployerId == empAccess.EmployerId).OrderBy(e => e.Name).ToList();
            }
        }//end: GetEmployersForCustomer

        public List<OrganizationVisibilityTree> GetOrganizationTreeForEmployee(Employee emp, string orgType, string searchTerm)
        {
            List<HierarchyPath> paths = null;
            List<EmployeeOrganization> assignedOrgs = EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == emp.EmployeeNumber && org.CustomerId == emp.CustomerId && org.EmployerId == emp.EmployerId).ToList();
            string employerId = emp.EmployerId;
            if (employerId == null) return null;
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Organization.Query.Where(org => org.CustomerId == emp.CustomerId &&
                org.EmployerId == employerId));
            if (!string.IsNullOrWhiteSpace(orgType))
                query.Add(Organization.Query.EQ(org => org.TypeCode, orgType));
            if (!string.IsNullOrWhiteSpace(searchTerm))
                query.Add(Organization.Query.Matches(org => org.Name, BsonRegularExpression.Create(new Regex(Regex.Escape(searchTerm), RegexOptions.IgnoreCase))));
            paths = Organization.Query.Find(Organization.Query.And(query)).ToList().Select(org => org.Path).ToList();
            List<OrganizationVisibilityTree> tree = BuildOrganizationTree(null, paths, emp.EmployerId, emp.CustomerId);
            if (assignedOrgs != null) AssignOrganizationVisiblity(null, tree, assignedOrgs);
            if (!string.IsNullOrWhiteSpace(searchTerm)) tree.ForEach(orgTree => orgTree.Expand(searchTerm));
            return tree;
        }
        public List<OrganizationVisibilityTree> GetOrganizationTreeForUser(User user, string orgType, string searchTerm)
        {
            List<HierarchyPath> paths = null;
            List<EmployerAccess> empAccess = user.Employers ?? null;
            if (empAccess == null) return null;
            List<EmployeeOrganization> assignedOrgs = new List<EmployeeOrganization>();
            List<string> employerIds = new List<string>();
            foreach (EmployerAccess emp in empAccess)
            {
                employerIds.Add(emp.EmployerId);
                var orgAccess = emp.OrgCodes.Any() ? emp.OrgCodes.Distinct().ToList() : null;
                var orgs = orgAccess != null ? EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == emp.Employee.EmployeeNumber && org.EmployerId == emp.EmployerId && org.CustomerId == user.CustomerId && orgAccess.Contains(org.Code)).ToList():
                    EmployeeOrganization.AsQueryable().Where(org => org.EmployerId == emp.EmployerId && org.CustomerId == user.CustomerId).ToList();
                assignedOrgs.AddRange(orgs);
            }
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Organization.Query.Where(org => org.CustomerId == user.CustomerId));
            query.Add(Organization.Query.In(org => org.EmployerId, employerIds));

            if (!string.IsNullOrWhiteSpace(orgType))
                query.Add(Organization.Query.EQ(org => org.TypeCode, orgType));
            if (!string.IsNullOrWhiteSpace(searchTerm))
                query.Add(Organization.Query.Matches(org => org.Name, BsonRegularExpression.Create(new Regex(Regex.Escape(searchTerm), RegexOptions.IgnoreCase))));
            paths = Organization.Query.Find(Organization.Query.And(query)).ToList().Select(org => org.Path).ToList();
            List<OrganizationVisibilityTree> tree = BuildOrganizationTree(user, paths);
            if (assignedOrgs != null) AssignOrganizationVisiblity(user, tree, assignedOrgs);
            return tree;
        }
        public List<OrganizationVisibilityTree> GetCurrentOrganizationTreeForEmployee(Employee emp)
        {
            List<HierarchyPath> paths = null;
            List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == emp.EmployeeNumber && org.CustomerId == emp.CustomerId && org.EmployerId == emp.EmployerId).ToList();
            if (empOrgs == null) return new List<OrganizationVisibilityTree>();
            paths = empOrgs.Select(org => org.Path).ToList();
            List<OrganizationVisibilityTree> tree = BuildOrganizationTree(null, paths, emp.EmployerId, emp.CustomerId);
            AssignOrganizationVisiblity(null, tree, empOrgs);
            return tree;
        }
        public List<OrganizationVisibilityTree> GetCurrentOrganizationTreeForUser(User user)
        {
            List<HierarchyPath> paths = null;
            List<EmployeeOrganization> assignedOrgs = GetUserOrganizations(user);
            if (assignedOrgs == null) return null;
            paths = assignedOrgs.Select(org => org.Path).ToList();
            List<OrganizationVisibilityTree> tree = BuildOrganizationTree(user, paths);
            AssignOrganizationVisiblity(user, tree, assignedOrgs);
            return tree;
        }

        private List<EmployeeOrganization> GetUserOrganizations(User user)
        {
            EmployerAccess empAccess = GetUserAssignedEmployerAccess(user);
            if (empAccess == null) return null;
            Employee assignedEmp = empAccess.Employee;
            if (assignedEmp == null)
            {
                return null;
            }
            var orgAccess = empAccess.OrgCodes.Any() ? empAccess.OrgCodes.Distinct().ToList():null;

            List<EmployeeOrganization> assignedOrgs = orgAccess != null ? EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == assignedEmp.EmployeeNumber && org.EmployerId == empAccess.EmployerId && org.CustomerId == user.CustomerId && orgAccess.Contains(org.Code)).ToList():
                EmployeeOrganization.AsQueryable().Where(org => org.EmployeeNumber == assignedEmp.EmployeeNumber && org.EmployerId == empAccess.EmployerId && org.CustomerId == user.CustomerId).ToList();
            return assignedOrgs;
        }

        private EmployerAccess GetUserAssignedEmployerAccess(User user)
        {
            EmployerAccess empAccess = user.Employers.FirstOrDefault(emp => !string.IsNullOrWhiteSpace(emp.EmployeeId));
            if (empAccess == null) return null;
            return empAccess;
        }

        private List<OrganizationVisibilityTree> BuildOrganizationTree(User user, List<HierarchyPath> paths, string employerId = null, string customerId = null)
        {
            List<string> pathList = new List<string>();
            List<string> lvlOneParentPathList = new List<string>();
            foreach (HierarchyPath path in paths)
            {
                if (!pathList.Contains(path.Path)) pathList.Add(path.Path);
                if (path.GetParentPath() == HierarchyPath.Null && (!lvlOneParentPathList.Contains(path.Path))) lvlOneParentPathList.Add(path.Path);
                HierarchyPath ancestor = path.GetParentPath();
                while (ancestor != HierarchyPath.Null)
                {
                    pathList.Add(ancestor.Path);
                    if (ancestor.GetParentPath() == HierarchyPath.Null && (!lvlOneParentPathList.Contains(ancestor.Path))) lvlOneParentPathList.Add(ancestor.Path);
                    ancestor = ancestor.GetParentPath();
                }
            }

            List<IMongoQuery> hierarchialQuery = new List<IMongoQuery>();
            hierarchialQuery.Add(Query.In("Path", pathList.Select(path => (BsonValue)path).ToList()));
            foreach (HierarchyPath path in paths)
            {
                string pattern = string.Format(@"^{0}(.*)", path.Path);
                hierarchialQuery.Add(Query.Matches("Path", new BsonRegularExpression(pattern, "i")));
            }
            List<EmployerAccess> employersAccess = user?.Employers ?? null;
            List<string> employerIds = new List<string>();
            if (employersAccess != null)
            {
                foreach (EmployerAccess emp in employersAccess)
                {
                    employerIds.Add(emp.EmployerId);
                }
            }

            var query = employerIds.Any() ? Organization.Query.And(
                Organization.Query.EQ(org => org.CustomerId, customerId == null ? user.CustomerId : customerId),
                Organization.Query.In(org => org.EmployerId, employerIds),
                Organization.Query.Or(hierarchialQuery)) :
                Organization.Query.And(
                Organization.Query.EQ(org => org.CustomerId, customerId == null ? user.CustomerId : customerId),
                Organization.Query.EQ(org => org.EmployerId, employerId == null ? GetUserAssignedEmployerAccess(user).EmployerId : employerId),
                Organization.Query.Or(hierarchialQuery));

            List<Organization> orgs = Organization.Query.Find(query).ToList();
            List<OrganizationVisibilityTree> orgVisbilityTree = new List<OrganizationVisibilityTree>();
            foreach (string lvlOneParentPath in lvlOneParentPathList)
            {
                Organization parentOrg = orgs.FirstOrDefault(org => org.Path.Path == lvlOneParentPath);
                if (parentOrg != null)
                {
                    OrganizationVisibilityTree tree = new OrganizationVisibilityTree(parentOrg, string.Empty, 1);
                    tree.CreateChildOrganizationTree(orgs);
                    orgVisbilityTree.Add(tree);
                }
            }
            return orgVisbilityTree.OrderBy(tree => tree.OrganizationName).ToList();
        }

        private void AssignOrganizationVisiblity(User user, List<OrganizationVisibilityTree> tree, List<EmployeeOrganization> empOrgs = null)
        {
            foreach (OrganizationVisibilityTree orgTree in tree)
            { orgTree.AssignVisibility(empOrgs); }
        }

        public void SaveOrganizationVisibilityForUser(User user, List<OrganizationVisibilityTree> assignedOrgTree)
        {
            EmployerAccess empAccess = GetUserAssignedEmployerAccess(user);
            if (empAccess == null) throw new InvalidOperationException("User " + user.DisplayName + " is not an employee.");
            Employee emp = Employee.GetById(empAccess.EmployeeId);
            var bwo = EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();

            List<string> orgCodes = new List<string>();
            foreach (OrganizationVisibilityTree orgTree in assignedOrgTree)
            {
                orgTree.UpdateUserOrganization(emp, bwo, CurrentUser);
                orgCodes.Add(orgTree.OrganizationCode);
            }
            bwo.Execute();
            var userToUpdate = user.Employers.FirstOrDefault(e => e.EmployeeId != null);
            if (orgCodes.Any() && userToUpdate != null)
            {
                userToUpdate.OrgCodes.AddRange(orgCodes);
                user.Save();
            }
        }
        public void SaveOrganizationVisibilityForEmployee(Employee emp, List<OrganizationVisibilityTree> assignedOrgTree)
        {
            var bwo = EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();
            foreach (OrganizationVisibilityTree orgTree in assignedOrgTree)
            {
                orgTree.UpdateUserOrganization(emp, bwo, CurrentUser);
            }
            bwo.Execute();
        }

        public List<OrganizationVisibilityTree> GetEmployerOrganizations(string employerId, string orgSearchTerm = null)
        {
            List<IMongoQuery> queryList = new List<IMongoQuery>();
            queryList.Add(Organization.Query.EQ(org => org.EmployerId, employerId));
            if (!string.IsNullOrWhiteSpace(orgSearchTerm))
            {
                queryList.Add(Organization.Query.Matches(org => org.Name, BsonRegularExpression.Create(new Regex(Regex.Escape(orgSearchTerm), RegexOptions.IgnoreCase))));
            }
            List<HierarchyPath> paths = Organization.Query.Find(Organization.Query.And(queryList)).ToList().Select(org => org.Path).ToList();
            List<string> pathList = new List<string>();
            List<string> lvlOneParentPathList = new List<string>();
            foreach (HierarchyPath path in paths)
            {
                if (!pathList.Contains(path.Path)) pathList.Add(path.Path);
                if (path.GetParentPath() == HierarchyPath.Null && (!lvlOneParentPathList.Contains(path.Path))) lvlOneParentPathList.Add(path.Path);
                HierarchyPath ancestor = path.GetParentPath();
                while (ancestor != HierarchyPath.Null)
                {
                    pathList.Add(ancestor.Path);
                    if (ancestor.GetParentPath() == HierarchyPath.Null && (!lvlOneParentPathList.Contains(ancestor.Path))) lvlOneParentPathList.Add(ancestor.Path);
                    ancestor = ancestor.GetParentPath();
                }
            }
            List<IMongoQuery> hierarchialQuery = new List<IMongoQuery>();
            hierarchialQuery.Add(Query.In("Path", pathList.Select(path => (BsonValue)path).ToList()));
            foreach (HierarchyPath path in paths)
            {
                string pattern = string.Format(@"^{0}(.*)", path.Path);
                hierarchialQuery.Add(Query.Matches("Path", new BsonRegularExpression(pattern, "i")));
            }
            var query = Organization.Query.And(
                Organization.Query.EQ(org => org.EmployerId, employerId),
                Organization.Query.Or(hierarchialQuery));

            List<Organization> orgs = Organization.Query.Find(query).ToList();
            List<OrganizationVisibilityTree> orgVisbilityTree = new List<OrganizationVisibilityTree>();
            // Where clause is to make sure that files that get uploaded for conversion with bad data don't throw any exceptions
            foreach (string lvlOneParentPath in lvlOneParentPathList.Where(p => orgs.Any(o => o.Path.Path == p)))
            {
                OrganizationVisibilityTree tree = new OrganizationVisibilityTree(orgs.FirstOrDefault(org => org.Path.Path == lvlOneParentPath), string.Empty, 1);
                tree.CreateChildOrganizationTree(orgs);
                orgVisbilityTree.Add(tree);
            }
            if (!string.IsNullOrWhiteSpace(orgSearchTerm))
            {
                orgVisbilityTree.ForEach(tree => tree.Expand(orgSearchTerm));
            }
            return orgVisbilityTree.OrderBy(tree => tree.OrganizationName).ToList();
        }

        public List<OrganizationType> GetEmployerOrganizationTypes(string employerId)
        {
            return OrganizationType.AsQueryable().Where(orgType => orgType.EmployerId == employerId).OrderBy(orgType => orgType.Name).ToList();
        }

        public List<Organization> GetChildOrganizations(string employerId, HierarchyPath path)
        {
            string pattern = string.Format(@"^{0}(.*)", path);
            var query = Organization.Query.And(
                Organization.Query.EQ(childOrg => childOrg.EmployerId, employerId),
                Organization.Query.EQ(childOrg => childOrg.CustomerId, CurrentCustomer.Id),
                Organization.Query.And(Query.Matches("Path", new BsonRegularExpression(pattern, "i"))));
            return Organization.Query.Find(query).ToList();
        }
        public List<EmployeeOrganization> GetChildEmployeeOrganizations(string employerId, HierarchyPath path)
        {
            string pattern = string.Format(@"^{0}(.*)", path);
            var query = EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(childOrg => childOrg.EmployerId, employerId),
                EmployeeOrganization.Query.EQ(childOrg => childOrg.CustomerId, CurrentCustomer.Id),
                EmployeeOrganization.Query.And(Query.Matches("Path", new BsonRegularExpression(pattern, "i"))));
            return EmployeeOrganization.Query.Find(query).ToList();
        }
        public void DeleteOrganization(string employerId, string orgId)
        {
            var curUserId = CurrentUser == null ? User.DefaultUserId : CurrentUser.Id;
            Organization organization = Organization.GetById(orgId);
            string pattern = string.Format(@"^{0}(.*)", organization.Path.Path);
            var queryOrganization = Organization.Query.Or(Organization.Query.And(
                Organization.Query.EQ(childOrg => childOrg.EmployerId, employerId),
                Organization.Query.EQ(childOrg => childOrg.CustomerId, CurrentCustomer.Id),
                Organization.Query.And(Query.Matches("Path", new BsonRegularExpression(pattern, "i")))),
                Organization.Query.EQ(childOrg => childOrg.Id, orgId));
            var updateOrganization = Organization.Updates.Set(o => o.ModifiedById, curUserId)
                .Set(o => o.ModifiedDate, DateTime.UtcNow)
                .Set(o => o.IsDeleted, true);
            Organization.Update(queryOrganization, updateOrganization, UpdateFlags.Multi);

            var queryEmployeeOrganization = EmployeeOrganization.Query.Or(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(childOrg => childOrg.EmployerId, employerId),
                EmployeeOrganization.Query.EQ(childOrg => childOrg.CustomerId, CurrentCustomer.Id),
                EmployeeOrganization.Query.And(Query.Matches("Path", new BsonRegularExpression(pattern, "i")))),
                EmployeeOrganization.Query.EQ(childOrg => childOrg.Id, orgId));
            var updateEmployeeOrganization = EmployeeOrganization.Updates.Set(o => o.ModifiedById, curUserId)
                .Set(o => o.ModifiedDate, DateTime.UtcNow)
                .Set(o => o.IsDeleted, true);
            EmployeeOrganization.Update(queryEmployeeOrganization, updateEmployeeOrganization, UpdateFlags.Multi);
        }

        public bool IsOrganizationCodeExisting(string employerId, string orgId, string orgCode)
        {
            List<IMongoQuery> queryAnds = new List<IMongoQuery>();
            string pattern = string.Format(@"^{0}$", orgCode);
            queryAnds.Add(Organization.Query.EQ(org => org.EmployerId, employerId));
            queryAnds.Add(Organization.Query.Matches(org => org.Code, new BsonRegularExpression(pattern, "i")));
            if (!string.IsNullOrWhiteSpace(orgId))
            {
                queryAnds.Add(Organization.Query.NE(org => org.Id, orgId));
            }
            if (Organization.Query.Find(Query.And(queryAnds)).Count() > 0) return true;
            return false;
        }

        public List<string> GetCurrentLevelOneEmployeeOrganizations(Employee employee)
        {
            //Get all EmployeeOrganizations by CustomerId, EmployerId, EmployeeNumber, and TypeCode NOT office location
            List<EmployeeOrganization> empOrgs = EmployeeOrganization.AsQueryable().Where(emp => employee.CustomerId == emp.CustomerId
           && employee.EmployerId == emp.EmployerId && employee.EmployeeNumber == emp.EmployeeNumber
           && emp.TypeCode.ToUpper() != OrganizationType.OfficeLocationTypeCode.ToUpper()).ToList();

            List<string> orgNames = new List<string>();

            //Loop through each of the Employee's organization, and only pick out the records that are NOT an ancestor of any other Organization that the employee is a part of
            foreach (EmployeeOrganization org in empOrgs)
            {
                var IsAncestor = false; //Start off by assuming organization won't be an ancestor
                var path1 = HierarchyPath.Parse(org.Path); //Parse the path into a HierarchyPath struct

                //Loop through all of the employee's organizations again
                foreach (EmployeeOrganization org2 in empOrgs)
                {
                    //Check if the outer-loop path is an ancestor of the current inner-looped Path 
                    if (path1.IsAncestorOf(org2.Path))
                    {
                        IsAncestor = true;
                    }

                }//end inner for loop

                //By the time we complete the inner-loop above, we will have checked if current out-loop Path is an ancestor of any of the employee's organization records
                if (!IsAncestor && (org.Dates == null || !org.Dates.EndDate.HasValue || org.Dates.EndDate.Value >= DateTime.Now))
                {
                    orgNames.Add(org.Name); //add to returned list
                }

            }//end outer-forloop
            return orgNames;
        }

        /// <summary>
        /// Return the list of all organization for a Employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public List<EmployeeOrganization> GetEmployeeOrganizationsList(Employee employee)
        {
            return EmployeeOrganization.AsQueryable().Where(emp => employee.CustomerId == emp.CustomerId
           && employee.EmployerId == emp.EmployerId && employee.EmployeeNumber == emp.EmployeeNumber
           && emp.TypeCode.ToUpper() != OrganizationType.OfficeLocationTypeCode.ToUpper()).ToList();
        }        

        public IList<Organization> GetOrganizations(string employerId, string customerId, string code = null, bool inactive = false)
        {
            var orgnizations = Organization.AsQueryable()
                .Where(org => org.CustomerId == customerId
                    && org.EmployerId == employerId);


            if (!string.IsNullOrEmpty(code))
            {
                orgnizations = orgnizations.Where(org => org.Code == code);
            }

            if(inactive)
            {
                orgnizations = orgnizations.Where(org => org.IsDeleted);
            }

            return orgnizations.ToList();
        }

        public Organization SaveOrgnization(Organization organization)
        {
            organization.Save();

            return organization;
        }
        public EmployeeOrganization GetEmployeeOrganizationByCode(Employee employee, string orgCode)
        {
            return EmployeeOrganization.AsQueryable()
                .Where(emp => employee.CustomerId == emp.CustomerId
                    && employee.EmployerId == emp.EmployerId
                    && employee.EmployeeNumber == emp.EmployeeNumber
                    && orgCode == emp.Code
                    && emp.TypeCode.ToUpper() != OrganizationType.OfficeLocationTypeCode.ToUpper())
                .FirstOrDefault();
        }


        public OrganizationType GetOrganizationType(string employerId, string orgTypeCode)
        {
            return OrganizationType.AsQueryable()
                .Where(orgType =>
                    orgType.EmployerId == employerId
                    && orgType.Code.ToUpper() == orgTypeCode.ToUpper())
                .FirstOrDefault();
        }

        /// <summary>
        /// Returns the list of all organizations which matches path
        /// </summary>
        /// <param name="employee">Employee</param>
        /// <param name="paths">paths</param>
        /// <returns>Organizations</returns>
        public List<Organization> GetParentOrganizationList(Employee employee, List<HierarchyPath> paths)
        {
            var query = Organization.Query.And(
                Organization.Query.EQ(org => org.CustomerId, employee.CustomerId),
                Organization.Query.EQ(org => org.EmployerId, employee.EmployerId),
                Organization.Query.And(Query.In("Path", paths.Select(path => (BsonValue) path.Path))));

            List<Organization> orgs = Organization.Query.Find(query).ToList();

            return orgs;
        }
    }
}
