﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Pay
{
    [Serializable]
    public class CasePayPeriodModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CasePayPeriodModel"/> class.
        /// </summary>
        public CasePayPeriodModel()
        {
            Detail = new List<CasePayDetailModel>();
        }

        /// <summary>
        /// The CaseId that this pay period belongs to
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the pay period identifier.
        /// </summary>
        /// <value>
        /// The pay period identifier.
        /// </value>
        public Guid PayPeriodId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public PayrollStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the pay date.
        /// </summary>
        /// <value>
        /// The pay date.
        /// </value>
        public DateTime PayDate { get; set; }

        /// <summary>
        /// Gets or sets the pay from.
        /// </summary>
        /// <value>
        /// The pay from.
        /// </value>
        public DateTime PayFrom { get; set; }

        /// <summary>
        /// Gets or sets the pay thru.
        /// </summary>
        /// <value>
        /// The pay thru.
        /// </value>
        public DateTime PayThru { get; set; }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        /// <value>
        /// The percentage.
        /// </value>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the pay period override.
        /// </summary>
        /// <value>
        /// The pay period override.
        /// </value>
        public bool PayPeriodHasOverride { get; set; }

        /// <summary>
        /// Gets whether this pay period or any of its detail rows were overriden
        /// </summary>
        public bool HasUserOverride
        {
            get
            {
                return this.PayPeriodHasOverride || Detail.Any(p => p.HasUserOverride);
            }
        }

        public decimal PayPeriodSalary { get; set; }

        /// <summary>
        /// Gets or sets the detail row models for this pay period model.
        /// </summary>
        /// <value>
        /// The detail.
        /// </value>
        public List<CasePayDetailModel> Detail { get; set; }
    }
}
