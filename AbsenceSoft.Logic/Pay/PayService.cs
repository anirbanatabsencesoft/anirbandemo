﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Pay.Contracts;

namespace AbsenceSoft.Logic.Pay
{
    public class PayService : IPayService
    {
        #region Members

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        private User CurrentUser { get; set; }

        #endregion

        #region .ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="PayService"/> class.
        /// </summary>
        public PayService() : this(User.Current) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PayService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public PayService(User currentUser)
        {
            CurrentUser = currentUser ?? User.System;
        }

        #endregion

        #region Policy Pay Order

        /// <summary>
        /// Retrieves the order policies need to be paid in for a specific employer
        /// Will generate and save the default order if none exists
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<PolicyPayOrder> GetEmployerPolicyPayOrder(string employerId)
        {
            using (new InstrumentationContext("PayService.GetEmployerPolicyPayOrder"))
            {
                List<PolicyPayOrder> employerPolicies = new List<PolicyPayOrder>();
                if (string.IsNullOrEmpty(employerId))
                    throw new AbsenceSoftException("EmployerId is required for retrieving policy pay order");

                if (!CurrentUser.HasEmployerAccess(employerId))
                    return employerPolicies;

                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(PolicyPayOrder.Query.EQ(m => m.EmployerId, employerId));
                var query = PolicyPayOrder.Query.Find(PolicyPayOrder.Query.And(ands));
                query.SetSortOrder(SortBy.Ascending("Order"));

                /// If there are no records for this employer, we just generate the default list and return it
                if (query.Count() == 0)
                    return GenerateDefaultPolicyPayOrder(employerId);

                /// Now we see if the number of pay order records 
                List<Policy> paidPolicies = GetEmployerPaidPolicies(employerId);
                List<PolicyPayOrder> currentPayOrder = query.ToList();
                if(paidPolicies.Count == query.Count())
                    return currentPayOrder;

                /// edge case
                /// some idiot messed with the default order, saved it, then created a new paid policy 
                /// and didn't tell us what order it should be paid out in
                List<Policy> missingPolices = paidPolicies.Where(p => !currentPayOrder.Any(c => c.Code == p.Code)).ToList();
                int startOrder = currentPayOrder.Max(cpo => cpo.Order) + 1;
                AddPoliciesToPayOrder(missingPolices, currentPayOrder, employerId, startOrder);
                SaveEmployerPolicyPayOrder(currentPayOrder);
                return currentPayOrder;
            }
        }

        /// <summary>
        /// Takes a list of Policy Pay Orders and saves them to the database or updates them
        /// </summary>
        /// <param name="employerId"></param>
        /// <param name="payOrder"></param>
        public void SaveEmployerPolicyPayOrder(List<PolicyPayOrder> payOrder)
        {
            //var bulkUpdateQuery = PolicyPayOrder.Repository.Collection.InitializeUnorderedBulkOperation();
            //foreach (var item in payOrder)
            //{
            //    bulkUpdateQuery.Insert<PolicyPayOrder>(item);
            //}

            //bulkUpdateQuery.Execute();

            foreach (PolicyPayOrder policyPayOrder in payOrder)
            {
                policyPayOrder.Save();
            }
        }

        /// <summary>
        /// Creates a policy pay order list based on the specific employer id passed in
        /// Employer Id is important because we need to include any employer specific paid policies in their default list
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        internal List<PolicyPayOrder> GenerateDefaultPolicyPayOrder(string employerId)
        {
            using (new InstrumentationContext("PayService.GenerateDefaultPolicyPayOrder"))
            {
                List<PolicyPayOrder> policiesPayOrder = new List<PolicyPayOrder>();
                if (string.IsNullOrEmpty(employerId))
                    throw new AbsenceSoftException("EmployerId is required for retrieving policy pay order");

                if (!CurrentUser.HasEmployerAccess(employerId))
                    return policiesPayOrder;

                List<Policy> policies = GetEmployerPaidPolicies(employerId);
                AddPoliciesToPayOrder(policies, policiesPayOrder, employerId);

                return policiesPayOrder;
            }
        }

        private void AddPoliciesToPayOrder(List<Policy> policiesToAdd, List<PolicyPayOrder> currentPolicyPayOrder, string employerId, int startOrder = 1)
        {
            foreach (Policy policy in policiesToAdd)
            {
                currentPolicyPayOrder.Add(new PolicyPayOrder()
                {
                    Name = policy.Name,
                    Code = policy.Code,
                    Order = startOrder,
                    EmployerId = employerId
                });
                startOrder++;
            }
        }

        /// <summary>
        /// Retrieves the list of employer paid policies
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns></returns>
        private List<Policy> GetEmployerPaidPolicies(string employerId)
        {
            return Policy.AsQueryable().Where(p => (p.EmployerId == null || p.EmployerId == employerId) && p.AbsenceReasons.Any(ar => ar.Paid)).ToList()
                    .OrderBy(p =>
                    {
                        switch (p.PolicyType)
                        {
                            case PolicyType.StateDisability:
                                return 1;
                            case PolicyType.StateFML:
                                return 2;
                            case PolicyType.WorkersComp:
                                return 3;
                            case PolicyType.STD:
                                return 4;
                            case PolicyType.LTD:
                                return 5;
                            case PolicyType.Company:
                                return 6;
                            case PolicyType.Other:
                                return 7;
                            default:
                                return 8;
                        }
                    }).ToList();
        }

        #endregion Policy Pay Order

        #region Pay Info

        /// <summary>
        /// take an amount that is a weekly period amount and convert it to
        /// some sort of pay period
        /// </summary>
        /// <param name="payamount">The weekly amount</param>
        /// <param name="periodType">The desitination period</param>
        /// <returns>The total</returns>
        public decimal WeeklyPayAmountToPayPeriod(decimal payAmount, PayPeriodType periodType)
        {
            decimal rtAmount = 0m;
            switch (periodType)
            {
                case PayPeriodType.Weekly:
                    rtAmount = payAmount;
                    break;
                case PayPeriodType.BiWeekly:
                    rtAmount = payAmount * 2m;
                    break;
                case PayPeriodType.SemiMonthly:
                    rtAmount = (payAmount * 52) / 24;
                    break;
                case PayPeriodType.Monthly:
                    rtAmount = (payAmount * 52) / 12;
                    break;
                default:
                    throw new AbsenceSoftException("WeeklyPayAmountToPayPeriod: Unhandled pay period type");
            }

            return rtAmount;
        }

        public decimal GetMaxPayPerPayPeriod(Case theCase, string policyCode, DateTime? date = null)
        {
            var theDate = date ?? DateTime.UtcNow.ToMidnight();
            var policyDetail = theCase.Pay.PayPeriods.Where(p => theDate.DateInRange(p.StartDate, p.EndDate)).SelectMany(p => p.Detail)
                .FirstOrDefault(d => d.PolicyCode == policyCode && theDate.DateInRange(d.StartDate, d.EndDate) && d.MaxPaymentAmount.HasValue);
            
            return policyDetail?.MaxPaymentAmount ?? 0M;
        }

        /// <summary>
        /// Gets the minimum pay per pay period.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public decimal GetMinPayPerPayPeriod(Case theCase, string policyCode, DateTime? date = null)
        {
            var theDate = date ?? DateTime.UtcNow.ToMidnight();
            var policyDetail = theCase.Pay.PayPeriods.Where(p => theDate.DateInRange(p.StartDate, p.EndDate)).SelectMany(p => p.Detail)
                .FirstOrDefault(d => d.PolicyCode == policyCode && theDate.DateInRange(d.StartDate, d.EndDate) && d.MinPaymentAmount.HasValue);
            
            return policyDetail?.MinPaymentAmount ?? 0M;
        }

        /// <summary>
        /// Gets the total paid for policy for this employee given the last x amount of time defined by the lookback date.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="lookbackDate">The lookback date, default is the start of the week as of 52 weeks ago (1 year).</param>
        /// <returns>
        /// The total dollar amount already paid (is locked/reported) from any pay periods inclusive
        /// of the lookback date forward to the most current.
        /// </returns>
        public decimal GetTotalPaidForPolicy(string employeeId, string policyCode, DateTime? lookbackDate = null)
        {
            var cases = Case.AsQueryable().Where(c => c.Employee.Id == employeeId).ToList();
            return GetTotalPaidForPolicy(cases, policyCode, lookbackDate);
        }

        /// <summary>
        /// Gets the total paid for policy for the provided cases given the last x amount of time defined by the lookback date.
        /// </summary>
        /// <param name="cases">The cases to look for payments within.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="lookbackDate">The lookback date, default is the start of the week as of 52 weeks ago (1 year).</param>
        /// <returns>
        /// The total dollar amount already paid (is locked/reported) from any pay periods inclusive
        /// of the lookback date forward to the most current (of only the cases provided in the list of cases).
        /// </returns>
        public decimal GetTotalPaidForPolicy(List<Case> cases, string policyCode, DateTime? lookbackDate = null)
        {
            if (cases == null || !cases.Any())
                return 0M;

            if (string.IsNullOrWhiteSpace(policyCode))
                return 0M;

            var backDate = lookbackDate ?? DateTime.UtcNow.ToMidnight().AddYears(-1).GetFirstDayOfWeek();
            var payments = cases
                .Where(c => c.Pay != null && c.Pay.PayPeriods != null && c.Pay.PayPeriods.Any(p => p.IsLocked))
                .SelectMany(c => c.Pay.PayPeriods.Where(p => p.IsLocked).SelectMany(p => p.Detail))
                .Where(d => d.PolicyCode == policyCode && d.PayAmount != null && d.PayAmount.Value > 0M && backDate.DateRangesOverLap(null, d.StartDate, d.EndDate))
                .ToList();

            if (!payments.Any())
                return 0M;

            return payments.Sum(c => c.PayAmount.Value);
        }

        /// <summary>
        /// Gets the payment percentage for the given policy as of the specified date (or today).
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policyCode">The policy identifier.</param>
        /// <param name="date">The date, if <c>null</c> or not included will use today as a default.</param>
        /// <returns></returns>
        public decimal GetPaymentPercentageForPolicy(Case theCase, string policyCode, DateTime? date = null)
        {
            var theDate = date ?? DateTime.UtcNow.ToMidnight();
            var policyDetail = theCase.Pay.PayPeriods.Where(p => theDate.DateInRange(p.StartDate, p.EndDate)).SelectMany(p => p.Detail)
                .FirstOrDefault(d => d.PolicyCode == policyCode && theDate.DateInRange(d.StartDate, d.EndDate));

            if (policyDetail == null || policyDetail.PayPercentage == null)
            {
                return 0M;
            }
            return Math.Min(policyDetail.PayPercentage.Value, 1M);
        }

        /// <summary>
        /// Gets the maximum payment percentage for policy.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="date">The date, if <c>null</c> or not included will use today as a default.</param>
        /// <returns></returns>
        public decimal GetMaxPaymentPercentageForPolicy(Case theCase, string policyCode, DateTime? date = null)
        {
            var theDate = date ?? DateTime.UtcNow.ToMidnight();
            var policyDetail = theCase.Pay.PayPeriods.Where(p => theDate.DateInRange(p.StartDate, p.EndDate)).SelectMany(p => p.Detail)
                .FirstOrDefault(d => d.PolicyCode == policyCode && theDate.DateInRange(d.StartDate, d.EndDate));

            if (policyDetail == null || policyDetail.PayPercentage == null) return 1M;
            return Math.Min(policyDetail.PaymentTierPercentage, 1M);
        }
        
        #endregion Pay Info

        #region Model Stuff

        /// <summary>
        /// Gets the case pay model.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        public CasePayModel GetCasePayModel(Case theCase)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            using (InstrumentationContext context = new InstrumentationContext("PayService.GetCasePayModel"))
            {
                List<Case> allCases = Case.AsQueryable().Where(c => c.Employee.Id == theCase.Employee.Id && c.Id != theCase.Id).ToList();
                allCases.Add(theCase);

                CasePayModel pay = new CasePayModel
                {
                    EmployerId = theCase.EmployerId,
                    EmployeeId = theCase.Employee.Id,
                    EmployeeName = theCase.Employee.FullName,
                    CaseId = theCase.Id,
                    CaseNumber = theCase.CaseNumber,
                    AbsenceReason = theCase.Reason.Name,
                    ApplyOffsetsByDefault = theCase.Pay.ApplyOffsetsByDefault
                };
                var approvedPolicies = theCase.Summary.Policies.SelectMany(p => p.Detail).Where(d => d.Determination == AdjudicationSummaryStatus.Approved);
                pay.ApprovedFrom = approvedPolicies.Any() ? approvedPolicies.Min(p => p.StartDate) : new DateTime?();
                pay.ApprovedThru = approvedPolicies.Any() ? approvedPolicies.Max(p => p.EndDate) : new DateTime?();
                pay.BasePay = theCase.Employee.Salary;
                pay.Pay = theCase.Employee.PayType;
                pay.StartDate = theCase.StartDate;
                pay.EndDate = theCase.EndDate;
                pay.ScheduledHoursPerWeek = Convert.ToDecimal(new LeaveOfAbsence()
                {
                    Case = theCase,
                    Employee = theCase.Employee,
                    Employer = theCase.Employer,
                    Customer = theCase.Customer
                }.HoursWorkedPerWeek());
                pay.TotalPaid = theCase.Pay.TotalPaid;

                pay.PayScheduleId = theCase.Pay.PayScheduleId;
                pay.PayScheduleName = theCase.Pay.PaySchedule?.Name;
                pay.WaiveWaitingPeriod = theCase.Pay.WaiveWaitingPeriod.Value;
                
                foreach (var period in theCase.Pay.PayPeriods.OrderBy(p => p.StartDate))
                {
                    CasePayPeriodModel pp = new CasePayPeriodModel()
                    {
                        PayPeriodId = period.Id,
                        Amount = period.Total,
                        PayDate = period.PayRollDateOverride ?? period.PayrollDate.Value,
                        PayFrom = period.StartDate,
                        PayThru = period.EndDateOverride ?? period.EndDate,
                        Percentage = period.Percentage,
                        Status = period.Status,
                        PayPeriodSalary = period.Detail.Any() ? period.Detail.Max(d => d.SalaryTotal) : 0M,
                        PayPeriodHasOverride = period.PayRollDateOverride != null || period.EndDateOverride != null,
                        Detail = new List<CasePayDetailModel>(period.Detail.Count)
                    };
                    foreach (var detail in period.Detail.OrderBy(d => d.StartDate))
                    {
                        AppliedPolicy ap = null;
                        var seg = theCase.Segments.Where(s => detail.StartDate.DateRangesOverLap(detail.EndDate, s.StartDate, s.EndDate)).FirstOrDefault();
                        if (seg != null)
                            ap = seg.AppliedPolicies.Where(p => p.Policy.Code == detail.PolicyCode).FirstOrDefault();

                        if (ap != null && ap.PolicyReason.Paid && ap.PolicyReason.PaymentTiers.Any(t => t.IsWaitingPeriod))
                            pay.ShowWaiveWaitingPeriod = true;

                        CasePayDetailModel dd = new CasePayDetailModel()
                        {
                            PayDetailId = detail.Id,
                            PolicyCode = detail.PolicyCode,
                            PolicyName = detail.PolicyName,
                            AllowOffset = detail.IsOffset || (ap != null && ap.PolicyReason.AllowOffset.HasValue && ap.PolicyReason.AllowOffset.Value),
                            BenefitPercentage = detail.PayPercentage.Value,
                            IsOffset = detail.IsOffset,
                            PayFrom = detail.StartDate,
                            PayThru = detail.EndDate,
                            TotalPaid = detail.IsOffset ? -detail.PayAmount.Value : detail.PayAmount.Value,
                            HasUserOverride = detail.PayAmount.HasOverride || detail.PayPercentage.HasOverride
                        };

                        pp.Detail.Add(dd);

                        if (!pay.Policies.Any(p => p.PolicyCode == detail.PolicyCode))
                            pay.Policies.Add(new CasePayPolicyModel()
                            {
                                PolicyCode = detail.PolicyCode,
                                PolicyName = detail.PolicyName,
                                TotalPaid = GetTotalPaidForPolicy(new List<Case>(1) { theCase }, detail.PolicyCode, theCase.StartDate),
                                TotalPaidLast52Weeks = GetTotalPaidForPolicy(allCases, detail.PolicyCode),
                                MaxBenefitAmount = GetMaxPayPerPayPeriod(theCase, detail.PolicyCode),
                                MinBenefitAmount = GetMinPayPerPayPeriod(theCase, detail.PolicyCode),
                                BenefitPercentage = GetPaymentPercentageForPolicy(theCase, detail.PolicyCode)
                            });
                    }

                    pay.PayPeriods.Add(pp);
                }

                return pay;
            }
        }

        #endregion Model Stuff

        /// <summary>
        /// Set the submit to payroll flag. Use this one if you know the specific dates
        /// otherwise use the other one that will generate the payschedule date range for you
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriodId">The pay period identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns>The modified case after being adjusted and calcs re-ran.</returns>
        /// <exception cref="AbsenceSoftException">
        /// The selected pay period does not exist.
        /// or
        /// You must make a selection to either submit or exclude all previous pay-periods prior to making a selection for this pay-period.
        /// </exception>
        public Case UpdatePayPeriodStatus(Case theCase, Guid payPeriodId, PayrollStatus status)
        {
            using (new InstrumentationContext("CaseService.SubmitToPayroll"))
            {
                var myPeriod = theCase.Pay.PayPeriods.FirstOrDefault(p => p.Id == payPeriodId);

                if (myPeriod == null)
                    throw new AbsenceSoftException("The selected pay period does not exist.");

                if (theCase.Pay.PayPeriods.Any(p => p.EndDate < myPeriod.StartDate && !p.IsLocked && p.Id != payPeriodId))
                    throw new AbsenceSoftException("You must make a selection to either submit or exclude all previous pay-periods prior to making a selection for this pay-period.");

                myPeriod.SetStatus(status, CurrentUser);

                using (var cs = new CaseService(false))
                    cs.PaidLeaveCalculate(theCase);

                return theCase;
            }
        }

        /// <summary>
        /// Updates the pay schedule and recalculates any pay periods that have not been submitted or excluded
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="newPayScheduleId"></param>
        /// <returns></returns>
        public Case UpdatePaySchedule(Case theCase, string newPayScheduleId)
        {
            using (new InstrumentationContext("CaseService.UpdatePaySchedule"))
            {
                /// If they didn't actually change the pay schedule, we have nothing to do
                if(theCase.Pay.PayScheduleId == newPayScheduleId)
                    return theCase;

                theCase.Pay.PayScheduleId = newPayScheduleId;
                CaseService cs = new CaseService();
                cs.PaidLeaveRecalculateForPayScheduleChange(theCase);
                cs.UpdateCase(theCase);
                return theCase;
            }
        }

        /// <summary>
        /// Changes the benefit amount.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriodDetailId">The pay period detail identifier.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        /// <exception cref="AbsenceSoftException">The pay period detail record could not be located</exception>
        public Case ChangeBenefitAmount(Case theCase, Guid payPeriodDetailId, decimal amount)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            var amt = Math.Abs(amount);

            var detail = theCase.Pay.PayPeriods.SelectMany(p => p.Detail).FirstOrDefault(d => d.Id == payPeriodDetailId);
            if (detail == null)
                throw new AbsenceSoftException("The pay period detail record could not be located");

            detail.PayAmount.SetOverride(amt, CurrentUser);
            detail.PayPercentage.SetOverride(Math.Round(detail.PayAmount.Value / detail.SalaryTotal, 2), CurrentUser);

            using (var cs = new CaseService(false))
                cs.PaidLeaveCalculate(theCase);

            return theCase;
        }

        /// <summary>
        /// Changes the benefit percentage.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriodDetailId">The pay period detail identifier.</param>
        /// <param name="percentage">The percentage.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        /// <exception cref="AbsenceSoftException">The pay period detail record could not be located</exception>
        public Case ChangeBenefitPercentage(Case theCase, Guid payPeriodDetailId, decimal percentage)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            var pct = Math.Abs(percentage);

            var detail = theCase.Pay.PayPeriods.SelectMany(p => p.Detail).FirstOrDefault(d => d.Id == payPeriodDetailId);
            if (detail == null)
                throw new AbsenceSoftException("The pay period detail record could not be located");

            detail.PayPercentage.SetOverride(pct, CurrentUser);
            detail.PayAmount.SetOverride(detail.SalaryTotal * detail.PayPercentage.Value, CurrentUser);

            using (var cs = new CaseService(false))
                cs.PaidLeaveCalculate(theCase);

            return theCase;
        }

        /// <summary>
        /// Changes the overral benefit percentage for a given policy for all non-submitted/future
        /// pay period detail rows.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriodDetailId">The pay period detail identifier.</param>
        /// <param name="percentage">The percentage.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        /// <exception cref="AbsenceSoftException">The pay period detail record could not be located</exception>
        public Case ChangeBenefitPercentage(Case theCase, string policyCode, decimal percentage)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            var pct = Math.Abs(percentage);

            List<PayPeriodDetail> detail = theCase.Pay.PayPeriods.Where(p => !p.IsLocked).SelectMany(p => p.Detail).Where(d => d.PolicyCode == policyCode).ToList();
            if (!detail.Any())
                return theCase;

            foreach (var d in detail)
            {
                d.PayPercentage = d.PayPercentage ?? new UserOverrideableValue<decimal>(d.PaymentTierPercentage);
                d.PayPercentage.SetOverride(pct, CurrentUser);
                d.PayAmount.SetOverride(d.SalaryTotal * d.PayPercentage.Value, CurrentUser);
            }

            using (var cs = new CaseService(false))
                cs.PaidLeaveCalculate(theCase);

            return theCase;
        }

        /// <summary>
        /// Changes the detail row and set whether that policy detail row is an offset.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriodDetailId">The pay period detail identifier.</param>
        /// <param name="isOffset">if set to <c>true</c> [is offset].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        /// <exception cref="AbsenceSoftException">The pay period detail record could not be located</exception>
        public Case ChangeDetailIsOffset(Case theCase, Guid payPeriodDetailId, bool isOffset)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            var detail = theCase.Pay.PayPeriods.SelectMany(p => p.Detail).FirstOrDefault(d => d.Id == payPeriodDetailId);
            if (detail == null)
                throw new AbsenceSoftException("The pay period detail record could not be located");

            detail.IsOffset = isOffset;

            using (var cs = new CaseService(false))
                cs.PaidLeaveCalculate(theCase);

            return theCase;
        }

        /// <summary>
        /// Intelligently applies offsets by default on a case
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="applyOffsetsByDefault">Whether we are Applying all offsets by default, or choosing</param>
        /// <returns></returns>
        public Case ChangeApplyOffsetsByDefault(Case theCase, bool applyOffsetsByDefault)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            if (theCase.Pay.ApplyOffsetsByDefault == applyOffsetsByDefault)
                return theCase;

            theCase.Pay.ApplyOffsetsByDefault = applyOffsetsByDefault;

            foreach (PayPeriodDetail detail in theCase.Pay.PayPeriods.SelectMany(p => p.Detail))
            {
                PolicyAbsenceReason absenceReason = theCase.Segments.SelectMany(p => p.AppliedPolicies)
                    .FirstOrDefault(p => p.Policy.Code == detail.PolicyCode).PolicyReason;

                detail.IsOffset = absenceReason.AllowOffset.HasValue && absenceReason.AllowOffset.Value && applyOffsetsByDefault;
            }

            using (var cs = new CaseService(false))
                cs.PaidLeaveCalculate(theCase);

            return theCase;
        }

        /// <summary>
        /// Changes the employee base pay.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="basePay">The base pay.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        public Case ChangeEmployeeBasePay(Case theCase, double basePay)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            theCase.Employee.Salary = basePay;

            using (var cs = new CaseService(false))
            {
                cs.RunCalcs(theCase); // Force re-building the usage salary amounts per day of usage
                cs.PaidLeaveRecalculateForPayScheduleChange(theCase);
            }

            return theCase;
        }

        /// <summary>
        /// Sets the waiting period.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="waiveWaitingPeriod">if set to <c>true</c> [waive waiting period].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">theCase</exception>
        public Case SetWaitingPeriod(Case theCase, bool waiveWaitingPeriod)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            if (waiveWaitingPeriod)
                theCase.Pay.WaiveWaitingPeriod.SetOverride(waiveWaitingPeriod, CurrentUser);
            else
                theCase.Pay.WaiveWaitingPeriod.ClearOverride();

            using (var cs = new CaseService(false))
                cs.PaidLeaveRecalculateForPayScheduleChange(theCase);

            return theCase;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="payPeriod"></param>
        /// <returns></returns>
        public Case UpdatePayPeriodDates(Case theCase, Guid payPeriodId, DateTime? payDate, DateTime? payThru)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase");

            PayPeriod payPeriod = theCase.Pay.PayPeriods.FirstOrDefault(pp => pp.Id == payPeriodId);
            if (payPeriod == null)
                throw new AbsenceSoftException("Pay Period was not found");

            if(payThru.HasValue)
                payPeriod.AdjustEndDate(payThru.Value, CurrentUser);

            if (payDate.HasValue)
                payPeriod.AdjustPayDate(payDate.Value, CurrentUser);

            return theCase;
        }
    }
}
