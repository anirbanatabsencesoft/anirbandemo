﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Pay
{
    [Serializable]
    public class CasePayDetailModel
    {
        /// <summary>
        /// Gets or sets the CaseId that this detail row belongs to
        /// </summary>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the pay detail identifier.
        /// </summary>
        /// <value>
        /// The pay detail identifier.
        /// </value>
        public Guid PayDetailId { get; set; }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the pay from.
        /// </summary>
        /// <value>
        /// The pay from.
        /// </value>
        public DateTime PayFrom { get; set; }

        /// <summary>
        /// Gets or sets the pay thru.
        /// </summary>
        /// <value>
        /// The pay thru.
        /// </value>
        public DateTime PayThru { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is offset.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is offset; otherwise, <c>false</c>.
        /// </value>
        public bool IsOffset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [allow offset].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow offset]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowOffset { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets the benefit percentage.
        /// </summary>
        /// <value>
        /// The benefit percentage.
        /// </value>
        public decimal BenefitPercentage { get; set; }

        /// <summary>
        /// Gets or sets the user override.
        /// </summary>
        /// <value>
        /// The user override.
        /// </value>
        public bool HasUserOverride { get; set; }
    }
}
