﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Pay
{
    [Serializable]
    public class CasePayModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CasePayModel"/> class.
        /// </summary>
        public CasePayModel()
        {
            Policies = new List<CasePayPolicyModel>();
            PayPeriods = new List<CasePayPeriodModel>();
        }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the employee.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the absence reason.
        /// </summary>
        /// <value>
        /// The absence reason.
        /// </value>
        public string AbsenceReason { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the approved from.
        /// </summary>
        /// <value>
        /// The approved from.
        /// </value>
        public DateTime? ApprovedFrom { get; set; }

        /// <summary>
        /// Gets or sets the approved thru.
        /// </summary>
        /// <value>
        /// The approved thru.
        /// </value>
        public DateTime? ApprovedThru { get; set; }

        /// <summary>
        /// Gets or sets the pay schedule identifier.
        /// </summary>
        /// <value>
        /// The pay schedule identifier.
        /// </value>
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the name of the pay schedule.
        /// </summary>
        /// <value>
        /// The name of the pay schedule.
        /// </value>
        public string PayScheduleName { get; set; }

        /// <summary>
        /// Gets or sets the base pay.
        /// </summary>
        /// <value>
        /// The base pay.
        /// </value>
        public double? BasePay { get; set; }

        /// <summary>
        /// Gets or sets the pay type
        /// </summary>
        /// <value>
        /// The pay type
        /// </value>
        public PayType? Pay { get; set; }

        /// <summary>
        /// Returns the Base Pay formatted for display
        /// </summary>
        public string BasePayDisplay
        {
            get
            {
                return string.Format("{0:C}{1}", BasePay, Pay == PayType.Hourly ? "/hour" : "/year");
            }
        }

        /// <summary>
        /// Gets or sets the scheduled hours per week.
        /// </summary>
        /// <value>
        /// The scheduled hours per week.
        /// </value>
        public decimal ScheduledHoursPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to apply offsets by default.
        /// </summary>
        /// <value>
        /// <c>true</c> if apply offsets by default; otherwise, <c>false</c>.
        /// </value>
        public bool ApplyOffsetsByDefault { get; set; }

        /// <summary>
        /// Gets or sets the policies.
        /// </summary>
        /// <value>
        /// The policies.
        /// </value>
        public List<CasePayPolicyModel> Policies { get; set; }

        /// <summary>
        /// Gets or sets the pay periods.
        /// </summary>
        /// <value>
        /// The pay periods.
        /// </value>
        public List<CasePayPeriodModel> PayPeriods { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [waive waiting period].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [waive waiting period]; otherwise, <c>false</c>.
        /// </value>
        public bool WaiveWaitingPeriod { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show waive waiting period].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show waive waiting period]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowWaiveWaitingPeriod { get; set; }
    }
}
