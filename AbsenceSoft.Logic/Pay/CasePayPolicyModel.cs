﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Pay
{
    [Serializable]
    public class CasePayPolicyModel
    {
        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the policy.
        /// </summary>
        /// <value>
        /// The name of the policy.
        /// </value>
        public string PolicyName { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        /// <value>
        /// The total paid.
        /// </value>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets the benefit percentage.
        /// </summary>
        /// <value>
        /// The benefit percentage.
        /// </value>
        public decimal BenefitPercentage { get; set; }

        /// <summary>
        /// Gets or sets the maximum benefit amount.
        /// </summary>
        /// <value>
        /// The maximum benefit amount.
        /// </value>
        public decimal MaxBenefitAmount { get; set; }

        /// <summary>
        /// Gets or sets the minimum benefit amount.
        /// </summary>
        /// <value>
        /// The minimum benefit amount.
        /// </value>
        public decimal MinBenefitAmount { get; set; }

        /// <summary>
        /// Gets or sets the total paid last52 weeks.
        /// </summary>
        /// <value>
        /// The total paid last52 weeks.
        /// </value>
        public decimal TotalPaidLast52Weeks { get; set; }
    }
}
