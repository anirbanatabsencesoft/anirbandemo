﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Pay.Contracts
{
    public interface IPayService
    {
        CasePayModel GetCasePayModel(Case theCase);
        Case UpdatePayPeriodStatus(Case theCase, Guid payPeriodId, PayrollStatus status);
    }
}