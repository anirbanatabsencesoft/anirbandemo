﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    public class OptionalOut<Type>
    {
        public Type Result { get; set; }
    }
}
