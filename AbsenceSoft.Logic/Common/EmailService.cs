﻿using AbsenceSoft.Common;
using AbsenceSoft.Logic.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    public class EmailService : LogicService, ILogicService
    {
        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_ForgotPassword = "ForgotPassword";

        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_LockedOutEmail = "LockedOutEmail";


        /// <summary>
        /// The create account email template name.
        /// </summary>
        public const string EmailTemplate_CreateAccount = "CreateAccount";

        /// <summary>
        /// The change password email template name.
        /// </summary>
        public const string EmailTemplate_ChangePassword = "ChangePassword";

        /// <summary>
        /// The add new user template name
        /// </summary>
        public const string EmailTemplate_AddNewUser = "AddNewUser";

        /// <summary>
        /// User need access email template name
        /// </summary>
        public const string EmailTemplate_UserNeedAccess = "UserNeedAccessEmail";

        /// <summary>
        /// Welcome message to approved user
        /// </summary>
        public const string EmailTemplate_WelcomeEmail = "WelcomeEmail";

        public const string EmailTemplate_EmployeeValidationEmail = "EmployeeValidationEmail";
        public const string EmailTemplate_EmployeeAssociationEmail = "EmployeeAssociationEmail";

		public const string EmailTemplate_TodoNotification = "ToDoEmailNotification";

        public const string EmailTemplate_SelfAccountCreationEmail = "SelfAccountCreationEmail";

        public const string EmailTemplate_UserExistsEmailToUser = "UserExistsEmailToUser";

        public const string EmailTemplate_SelfRegistrationEmailToAdmin = "SelfRegistrationEmailToAdmin";

        public const string EmailTemplate_EmployeeNotUniqueEmailToAdmin = "EmployeeNotUniqueEmailToAdmin";


        public EmailService()
        {

        }


        /// <summary>
        /// Gets an email template text directly from an embedded resource of the logic assembly. This is only
        /// for site-wide email templates such as forgot password, welcome, etc. All other emails (e.g. communications)
        /// should use the CommunicationService in order to get template text.
        /// </summary>
        /// <param name="name">The email template name</param>
        /// <returns>The specified email template text or <c>null</c> if the named resource wasn't found.</returns>
        public string GetEmailTemplate(string name)
        {
            return Resources.ResourceManager.GetString(name);
        }//GetEmailTemplate


        /// <summary>
        /// Sends the specified message to an SMTP server for delivery.
        /// </summary>
        /// <param name="message">A <see cref="System.Net.Mail.MailMessage"/> that contains the message to send.</param>
        /// <returns><c>true</c> if the message was sent; otherwise <c>false</c>.</returns>
        /// <exception cref="System.ArgumentNullException">message is null.</exception>
        public bool SendEmail(MailMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            try
            {
                using (SmtpClient smtpClient = new SmtpClient())
                {
                    message.IsBodyHtml = true;
                    smtpClient.Send(message);
                    Log.Info(string.Format("Email message successfully sent, {0}", message.Subject, message.To.ToString()));
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Error sending email", ex);
                return false;
            }
        }//SendEmail

        /// <summary>
        /// Sends the specified message to an SMTP server for delivery. Returns smtp exceptions if there is a delivery error.
        /// </summary>
        /// <param name="message">A <see cref="System.Net.Mail.MailMessage"/> that contains the message to send.</param>
        /// <exception cref="System.ArgumentNullException">the message is null.</exception>
        /// <exception cref="System.InvalidOperationException">the TO or FROM recipients are invalid/missing, network host invalid, or port invalid.</exception>
        /// <exception cref="System.Net.Mail.SmtpException">the connection to the SMTP server failed. A timeout, authentication failure, or SSL failure.</exception>
        /// <exception cref="System.Net.Mail.SmtpFailedRecipientsException">the message could not be delivered to one or more of the recipients.</exception>
        public void SendEmailWithExceptions(MailMessage message)
        {
            try
            {
                using (SmtpClient smtpClient = new SmtpClient())
                {
                    message.IsBodyHtml = true;
                    smtpClient.Send(message);
                    Log.Info(string.Format("Email message successfully sent, {0}", message.Subject, message.To.ToString()));
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error sending email", ex);
                throw ex;
            }
        }//SendEmail

        /// <summary>
        /// Sends the specified e-mail message to an SMTP server for delivery. The message
        /// sender, recipients, subject, and message body are specified using System.String
        /// objects.
        /// </summary>
        /// <param name="from">A <see cref="System.String"/> that contains the address information of the message sender.</param>
        /// <param name="recipients">A <see cref="System.String"/> that contains the addresses that the message is sent to.</param>
        /// <param name="subject">A <see cref="System.String"/> that contains the subject line for the message.</param>
        /// <param name="body">A <see cref="System.String"/> that contains the message body.</param>
        /// <returns><c>true</c> if the message was sent; otherwise <c>false</c>.</returns>
        /// <exception cref="System.ArgumentNullException">from is null.-or-recipients is null.</exception>
        /// <exception cref="System.ArgumentException">from is System.String.Empty.-or-recipients is System.String.Empty.</exception>
        public bool SendEmail(string from, string recipients, string subject, string body)
        {
            return SendEmail(new MailMessage(from, recipients, subject, body));
        }//SendEmail
    }
}
