﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    [Serializable]
    public class UploadPolicy
    {
        public string s3PolicyBase64 { get; set; }
        public string s3Signature { get; set; }
        public string s3Key { get; set; }
        public object s3Policy { get; set; }
        public string fileName { get; set; }
        public string postUrl { get; set; }
        public string acl { get; set; }
    }
}
