﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    public static class MessageType
    {
        public const string EligibililtyLoad = "el";

        public const string CaseRecalcRequest = "case-recalc";

        public const string CaseExportRequest = "case-export";

        public const string ReportRequest = "report-request";

        public const string RecalcFutureCases = "case-recalc-future";
    }
}
