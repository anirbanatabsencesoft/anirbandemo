﻿using AbsenceSoft.Common.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    public class MessageQueues
    {
        public static string AtCommonQueue { get { return Settings.Default.SQSQueueNameEligibilityUpload ?? "eligibility_upload_debug"; } }
    }
}
