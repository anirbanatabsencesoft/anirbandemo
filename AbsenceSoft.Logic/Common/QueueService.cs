﻿using AbsenceSoft.Common.Properties;
using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    public class QueueService : LogicService
    {
        /// <summary>
        /// Add a message to the named queue and returns the message receipt.
        /// </summary>
        /// <param name="queueName">The name of the queue to add the item to.</param>
        /// <param name="message">The message text of the item to add to the queue.</param>
        /// <param name="attributes">Any additional attributes for the message to be stored in the payload.
        /// Default is <c>null</c>.</param>
        /// <returns>The message receipt id of the added item.</returns>
        public string Add(string queueName, string messageType, string message, Dictionary<string, string> attributes = null)
        {
            if (string.IsNullOrWhiteSpace(messageType))
                throw new ArgumentNullException("messageType");
            if (string.IsNullOrWhiteSpace(queueName))
                throw new ArgumentNullException("queueName");
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException("message");

            SendMessageRequest request = new SendMessageRequest()
            {
                QueueUrl = QueueUrl(queueName),
                MessageBody = message
            };
            request.MessageAttributes.Add(QueueItem.CommonAttributes.MessageType, new MessageAttributeValue() { StringValue = messageType, DataType = "String" });
            if (attributes != null && attributes.Count > 0)
                foreach (var att in attributes)
                    request.MessageAttributes.Add(att.Key, new MessageAttributeValue() { StringValue = att.Value, DataType = "String" });

            using (var client = SQSClient())
            {
                var response = client.SendMessage(request);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                    throw new AbsenceSoftException("Error adding item to the queue");

                return response.MessageId;
            }
        } // Add

        /// <summary>
        /// Add a message to the named queue and returns the message receipt.
        /// </summary>
        /// <param name="queueName">The name of the queue to add the item to.</param>
        /// <param name="message">The item to add to the queue.</param>
        /// <param name="attributes">Any additional attributes for the message to be stored in the payload.
        /// Default is <c>null</c>.</param>
        /// <returns>The message receipt id of the added item.</returns>
        public string Add(string queueName, string messageType, object message, Dictionary<string, string> attributes = null)
        {
            if (string.IsNullOrWhiteSpace(queueName))
                throw new ArgumentNullException("queueName");
            if (message == null)
                throw new ArgumentNullException("message");

            return Add(queueName, messageType, JsonConvert.SerializeObject(message, Formatting.None), attributes);
        } // Add

        public IEnumerable<string> AddBatch(string queueName, params QueueItem[] items)
        {
            if (string.IsNullOrWhiteSpace(queueName))
                throw new ArgumentNullException("queueName");
            if (items == null || items.Length == 0)
                throw new ArgumentNullException("items");

            int id = 0;
            SendMessageBatchRequest batchReq = new SendMessageBatchRequest()
            {
                QueueUrl = QueueUrl(queueName),
                Entries = items.Select(i => new SendMessageBatchRequestEntry()
                {
                    MessageBody = i.Body,
                    MessageAttributes = i.Attributes.ToDictionary(a => a.Key, a => new MessageAttributeValue() { DataType = "String", StringValue = a.Value }),
                    Id = (id++).ToString()
                }).ToList()
            };

            using (var client = SQSClient())
            {
                var response = client.SendMessageBatch(batchReq);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                    throw new AbsenceSoftException("Error adding item batch to the queue");

                if (response.Failed != null && response.Failed.Any())
                    throw new AbsenceSoftAggregateException(response.Failed.Select(f => f.Message).ToArray());

                return response.Successful.Select(r => r.MessageId);
            }
        }//AddBatch

        /// <summary>
        /// Long-polling receive w/ wait up to 20 seconds with up to n messages returned
        /// specified by maxItems, default is 1.
        /// </summary>
        /// <param name="queueName">The name of the queue to receive messages from.</param>
        /// <param name="maxItems">The maximum number of messages to be received from the queue.
        /// The actul number returned may be zero or more up to that maximum.</param>
        /// <returns></returns>
        public List<QueueItem> Receive(string queueName, int maxItems = 1)
        {
            ReceiveMessageRequest request = new ReceiveMessageRequest()
            {
                QueueUrl = QueueUrl(queueName),
                MaxNumberOfMessages = maxItems,
                WaitTimeSeconds = 20
            };
            request.AttributeNames.Add("ApproximateReceiveCount");
            request.MessageAttributeNames.Add("All");

            using (var client = SQSClient())
            {
                var response = client.ReceiveMessage(request);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                    throw new AbsenceSoftException("Error receiving items from the queue");
                if (response.HttpStatusCode == System.Net.HttpStatusCode.Forbidden)
                    throw new AbsenceSoftException("Violation of queue limits");

                if (response.Messages == null || !response.Messages.Any())
                    return new List<QueueItem>(0);

                return response.Messages.Select(m => new QueueItem()
                {
                    QueueName = queueName,
                    MessageId = m.MessageId,
                    ReceiptHandle = m.ReceiptHandle,
                    Body = m.Body,
                    Attributes = m.MessageAttributes.ToDictionary(a => a.Key, a => a.Value.StringValue),
                    ReceiveCount = int.Parse(m.Attributes.ContainsKey("ApproximateReceiveCount") ? m.Attributes["ApproximateReceiveCount"] ?? "0" : "0")
                }).ToList();
            }
        } // Receive

        /// <summary>
        /// Deletes an item from the specified queue by it's receipt handle. A message
        /// cannot be deleted from the queue using it's message Id, it must be the
        /// handle used when the message was received.s
        /// </summary>
        /// <param name="queueName">The name of the queue to delete the item from.</param>
        /// <param name="receiptHandle">The receipt handle of the message to delete.</param>
        public void Delete(string queueName, string receiptHandle)
        {
            DeleteMessageRequest request = new DeleteMessageRequest()
            {
                QueueUrl = QueueUrl(queueName),
                ReceiptHandle = receiptHandle
            };

            using (var client = SQSClient())
            {
                var response = client.DeleteMessage(request);
                if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                    throw new AbsenceSoftException("Error adding item to the queue");
            }
        } // Delete

        /// <summary>
        /// Renews the message visibility timeout so that it is not redelivered for another n
        /// <paramref name="seconds"/> specified.
        /// </summary>
        /// <param name="item">The queue item to renew.</param>
        /// <param name="seconds">The total number of seconds to add to the visibility timeout.</param>
        public void Renew(QueueItem item, int seconds = 60)
        {
            if (item != null)
            {
                ChangeMessageVisibilityRequest request = new ChangeMessageVisibilityRequest()
                {
                    QueueUrl = QueueUrl(item.QueueName),
                    ReceiptHandle = item.ReceiptHandle,
                    VisibilityTimeout = seconds
                };
                using (var client = SQSClient())
                {
                    var response = client.ChangeMessageVisibility(request);
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                        throw new AbsenceSoftException("Error renewing queue item");
                }
            }
        } // Renew

        /// <summary>
        /// Renews the message visibility timeout so that it is not redelivered for the
        /// <paramref name="duration"/> specified.
        /// </summary>
        /// <param name="item">The queue item to renew.</param>
        /// <param name="duration">The total duration to add to the visibility timeout.</param>
        public void Renew(QueueItem item, TimeSpan duration)
        {
            int seconds = Convert.ToInt32(Math.Ceiling(duration.TotalSeconds));
            Renew(item, seconds);
        } // Renew

        /// <summary>
        /// Gets a queue URL for a given queue name based on the AWS account
        /// configuration and root AWS SQS queue URL format.
        /// </summary>
        /// <param name="queueName">The name of the queue to get the URL for.</param>
        /// <returns>The fully qualified SQS queue URL.</returns>
        private string QueueUrl(string queueName)
        {
            return string.Format("https://queue.amazonaws.com/{0}/{1}",
                Settings.Default.AWSAccountNumber,
                queueName);
        } // QueueUrl

        /// <summary>
        /// Determines whether the file service (S3) is alive asynchronously.
        /// </summary>
        /// <returns><c>true</c> if the file service is alive; otherwise <c>false</c>.</returns>
        public async Task<bool> IsAliveAsync()
        {
            using (IAmazonSQS client = SQSClient())
            {
                var res = await client.GetQueueAttributesAsync(new GetQueueAttributesRequest()
                {
                    QueueUrl = QueueUrl(MessageQueues.AtCommonQueue),
                    AttributeNames = new List<string>() { "All" }
                });
                return res.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
        }

        /// <summary>
        /// Gets a new instance of an Amazon AWS SQS Client
        /// </summary>
        /// <returns>A new instance of an Amazon AWS SQS Client</returns>
        private IAmazonSQS SQSClient()
        {
            AmazonSQSConfig config = new AmazonSQSConfig()
            {
                RegionEndpoint = RegionEndpoint.GetBySystemName(Settings.Default.AWSRegion),
                ProxyHost = null
            };
            return AWSClientFactory.CreateAmazonSQSClient(
                Settings.Default.AWSAccessId,
                Settings.Default.AWSSecretKey,
                config);
        } // SQSClient
    } // QueueService
} // namespace
