﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using Ionic.Zip;
using Ionic.Crc;
using AbsenceSoft.Data;
using System.Threading.Tasks;
using AbsenceSoft.Common.HttpEncoding;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using System.Web;

namespace AbsenceSoft.Logic.Common
{
    public class FileService : LogicService
    {
        public FileService()
        {

        }

        public FileService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            :base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Gets the base path.
        /// </summary>
        /// <value>
        /// The base path.
        /// </value>
        private string _basePath { get { return AbsenceSoft.Common.Properties.Settings.Default.S3BasePath ?? ""; } }
        
        /// <summary>
        /// Gets the file upload policy specifically for the processing upload file bucket and properly
        /// formats the path for upload.
        /// </summary>
        /// <param name="originalFileName">The original file name</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetProcessingFileUploadPolicy(string originalFileName, string customerId, string employerId)
        {
            string path = string.Concat(customerId, "/", employerId, "/");
            string bucketName = Settings.Default.S3BucketName_Processing;
            return GetS3UploadPolicy(originalFileName, bucketName, path);
        } // GetEligibilityFileUploadPolicy

        /// <summary>
        /// Gets the file upload policy specifically for the customer upload file bucket and properly
        /// formats the path for upload.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetCustomerUploadPolicy(string originalFileName, string customerId)
        {
            string path = string.Concat(customerId, "/");
            string bucketName = Settings.Default.S3BucketName_Files;
            return GetS3UploadPolicy(originalFileName, bucketName, path);
        } // GetCustomerUploadPolicy

        /// <summary>
        /// Gets the file upload policy specifically for the customer logo upload file bucket and properly
        /// formats the path for upload as well as the public-read ACL.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetCustomerLogoUploadPolicy(string originalFileName, string customerId)
        {
            string path = string.Concat(customerId, "/logo/");
            string bucketName = Settings.Default.S3BucketName_Files;
            return GetS3UploadPolicy(originalFileName, bucketName, path, "public-read");
        } // GetCustomerUploadPolicy

        /// <summary>
        /// Gets the file upload policy specifically for the employer upload file bucket and properly
        /// formats the path for upload.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetEmployerUploadPolicy(string originalFileName, string customerId, string employerId)
        {
            string path = string.Concat(customerId, "/", employerId, "/");
            string bucketName = Settings.Default.S3BucketName_Files;
            return GetS3UploadPolicy(originalFileName, bucketName, path);
        } // GetEmployerUploadPolicy

        /// <summary>
        /// Gets the file upload policy specifically for the employee attachments upload file bucket and properly
        /// formats the path for upload.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <param name="employeeId">The employee Id for the employee which this file is being uploaded to/for.</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetEmployeeUploadPolicy(string originalFileName, string customerId, string employerId, string employeeId)
        {
            string path = string.Concat(customerId, "/", employerId, "/employees/", employeeId, "/");
            string bucketName = Settings.Default.S3BucketName_Files;
            return GetS3UploadPolicy(originalFileName, bucketName, path);
        } // GetEmployeeUploadPolicy

        /// <summary>
        /// Gets the file upload policy specifically for the case attachments, communications, etc. upload file bucket and properly
        /// formats the path for upload.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <param name="employeeId">The employee Id for the employee which this file is being uploaded to/for.</param>
        /// <param name="caseId">The case Id for the case which this file is being attached to.</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetCaseUploadPolicy(string originalFileName, string customerId, string employerId, string employeeId, string caseId)
        {
            string path = string.Concat(customerId, "/", employerId, "/employees/", employeeId, "/cases/", caseId, "/");
            string bucketName = Settings.Default.S3BucketName_Files;
            return GetS3UploadPolicy(originalFileName, bucketName, path);
        } // GetCaseUploadPolicy


        /// <summary>
        /// Generates S3 Policy Credentials so we can manipulate files with limited scope.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="bucketName">The name of the S3 bucket that the upload is intended for.</param>
        /// <param name="path">The path within the bucket to store the file, defaults to null or the root path.</param>
        /// <param name="acl">[Optional] Provides the name of the ACL policy to apply to the file as a criteria, defaults to "private".</param>
        /// <returns>A populated file upload policy that can be used directly in JavaScript by the client for uploading straight to S3.</returns>
        public UploadPolicy GetS3UploadPolicy(string originalFileName, string bucketName, string path = null, string acl = "private")
        {
            string fileKey = GetFileKey(originalFileName, path).Trim();            
            string expiryDate = DateTime.UtcNow.AddSeconds(300).ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            var s3Policy = new
            {
                expiration = expiryDate,
                conditions = Settings.Default.S3UploadLimitInBytes > 0L ? new object[5]
                {
                    // Set the appropriate bucket name
                    new { bucket = bucketName },
                    // Sets the match condition for the key to the exact fileName we're expecting to upload, anything
                    //  else will get rejected
                    new string[3] { "eq", "$key", fileKey },
                    // Set our ACL to private, this should never be accessible unless an explicit policy is created for it
                    new { acl = acl ?? "private" },
                    // Limit the upload to configurable limit, any more than this is just ridiculous, really, they should ZIP/Compress it
                    new object[3] { "content-length-range", 0, Settings.Default.S3UploadLimitInBytes },
                    // Allow any content types, however make it possible for upstream filtering of content type if we wanted to
                    new string[3] { "starts-with", "$Content-Type", "" }
                } : new object[4] 
                {
                    // Set the appropriate bucket name
                    new { bucket = bucketName },
                    // Sets the match condition for the key to the exact fileName we're expecting to upload, anything
                    //  else will get rejected
                    new string[3] { "eq", "$key", fileKey },
                    // Set our ACL to private, this should never be accessible unless an explicit policy is created for it
                    new { acl = acl ?? "private" },
                    // Allow any content types, however make it possible for upstream filtering of content type if we wanted to
                    new string[3] { "starts-with", "$Content-Type", "" }
                }
            };
            string policyString = s3Policy.ToJSON();
            string policyBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(policyString), Base64FormattingOptions.None);
            string signature = null;
            using (var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(Settings.Default.AWSSecretKey)))
                signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(policyBase64)), Base64FormattingOptions.None);

            UploadPolicy credentials = new UploadPolicy()
            {
                s3PolicyBase64 = policyBase64,
                s3Signature = signature,
                s3Key = Settings.Default.AWSAccessId,
                s3Policy = s3Policy,
                fileName = fileKey,
                postUrl = string.Format("https://s3.amazonaws.com/{0}/", bucketName),
                acl = acl ?? "private"
            };

            return credentials;
        } // GetS3UploadPolicy

        /// <summary>
        /// Gets a temporary S3 download URL with a short expiration to allow temporary access to an S3 bucket resource.
        /// </summary>
        /// <param name="resource">The generated file name/resource key for the file in S3</param>
        /// <param name="bucketName">The name of the S3 bucket the resource is located in</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// A URL good for only 30 seconds to grant temporary access of the resource
        /// </returns>
        /// <exception cref="AbsenceSoftException">S3 error occurred. Exception:  + amazonS3Exception.ToString()</exception>
        public string GetS3DownloadUrl(string resource, string bucketName, string fileName = null)
        {
            try
            {
                using (IAmazonS3 client = S3Client())
                {
                    GetPreSignedUrlRequest requests = new GetPreSignedUrlRequest();
                    requests.BucketName = bucketName;
                    requests.Key = resource;
                    requests.Expires = DateTime.UtcNow.AddMinutes(2);
                    requests.Protocol = Protocol.HTTPS;
                    if (!string.IsNullOrWhiteSpace(fileName))
                    {
                        var urlEncoder = new UrlEncoder();
                        var encodedFileName = urlEncoder.UrlEncodeNonAscii(fileName, Encoding.UTF8);
                        requests.ResponseHeaderOverrides.ContentDisposition = string.Format("attachment; filename=\"{0}\"", encodedFileName);
                    }
                        
                    return client.GetPreSignedURL(requests);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Log.Error("S3 error occurred. Exception: " + amazonS3Exception.ToString());
                throw new AbsenceSoftException("S3 error occurred. Exception: " + amazonS3Exception.ToString());
            }
        } // GetS3DownloadUrl

        /// <summary>
        /// Deletes the specified resource from the specified S3 bucket.
        /// </summary>
        /// <param name="resource">The generated file name/resource key for the file in S3</param>
        /// <param name="bucketName">The name of the S3 bucket the resource is located in</param>
        public void DeleteS3Object(string resource, string bucketName)
        {
#warning HACK: Need to disable deleting files from the default files bucket because all hell has broken loose in production
            if (string.Equals(bucketName, Settings.Default.S3BucketName_Files, StringComparison.InvariantCultureIgnoreCase))
                return;

            try
            {
                using (IAmazonS3 client = S3Client())
                    client.DeleteObject(new DeleteObjectRequest()
                    {
                        BucketName = bucketName,
                        Key = resource
                    });
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Log.Error("S3 error occurred. Exception: " + amazonS3Exception.ToString());
                throw new AbsenceSoftException("S3 error occurred. Exception: " + amazonS3Exception.ToString());
            }
        } // DeleteS3Object

        /// <summary>
        /// Uploads a document using a byte array stored in the document to S3 with a default private ACL and returns the resulting
        /// saved Document. User can optionally provide customer, employer, employee and case information as well in that cascading order
        /// if the document is for a more specific purpose.
        /// </summary>
        /// <param name="doc">The document containing the parameters and byte[] array to upload, save and return.</param>
        /// <param name="bucketName">The bucket name to upload that document for.</param>
        /// <returns>Returns the saved document</returns>
        public Document UploadDocument(Document doc, string bucketName)
        {
            try
            {
                string fileKey = GetFileKey(doc.FileName, null, doc.CustomerId, doc.EmployerId, doc.EmployeeId, doc.CaseId);
                using (IAmazonS3 client = S3Client())
                {
                    PutObjectRequest request = new PutObjectRequest();
                    request.BucketName = bucketName;
                    request.Key = fileKey;
                    request.CannedACL = S3CannedACL.Private;
                    using (MemoryStream ms = new MemoryStream(doc.File))
                    {
                        request.InputStream = ms;
                        client.PutObject(request);
                    }
                }
                doc.Locator = fileKey;
                doc.Store = DocumentStore.S3;
                doc.Save();
                return doc;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Log.Error("S3 error occurred. Exception: " + amazonS3Exception.Message, amazonS3Exception);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("Error uploading file to S3", ex);
                throw;
            }
        } // UploadFile

        public Document UploadAdministrationDocument(HttpPostedFileBase file)
        {
            Document newDocument = CreateDocumentFromFile(file);
            newDocument.CustomerId = CustomerId;
            newDocument.EmployerId = EmployerId;
            return newDocument.Upload();
        }

        public Document CreateDocumentFromFile(HttpPostedFileBase file)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            return new Document()
            {
                ContentLength = file.ContentLength,
                ContentType = file.ContentType,
                FileName = file.FileName,
                File = file.InputStream.ReadAllBytes()
            };
        }

        /// <summary>
        /// Uploads a physical file from the provided stream to S3 with a default private ACL and returns the resulting
        /// file name. User can optionally provide customer, employer, employee and case information as well in that cascading order
        /// if the file is for a more specific purpose.
        /// </summary>
        /// <param name="stream">The stream containing the file bytes to read for the upload to S3.</param>
        /// <param name="fileName">The name of the file that is to be uploaded to the S3 bucket.</param>
        /// <param name="bucketName">The name of the S3 bucket the file should be uploaded to.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <param name="employeeId">The employee Id for the employee which this file is being uploaded to/for.</param>
        /// <param name="caseId">The case Id for the case which this file is being attached to.</param>
        /// <returns></returns>
        public string UploadFile(Stream stream, string fileName, string bucketName, string customerId = null, string employerId = null, string employeeId = null, string caseId = null)
        {
            try
            {
                string fileKey = GetFileKey(fileName, null, customerId, employerId, employeeId, caseId);
                using (IAmazonS3 client = S3Client())
                {
                    PutObjectRequest request = new PutObjectRequest();
                    request.BucketName = bucketName;
                    request.Key = fileKey;
                    request.CannedACL = S3CannedACL.Private;
                    request.InputStream = stream;
                    client.PutObject(request);
                }

                return fileKey;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Log.Error("S3 error occurred. Exception: " + amazonS3Exception.Message, amazonS3Exception);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("Error uploading file to S3", ex);
                throw;
            }
        }

        /// <summary>
        /// Used for saving a document after we have saved it in S3
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public Document SaveUploadedDocument(Document doc)
        {
            doc.Save();
            return doc;
        }

        /// <summary>
        /// Uploads a physical file from the local server file system to S3 with a default private ACL and returns the resulting
        /// file name. User can optionally provide customer, employer, employee and case information as well in that cascading order
        /// if the file is for a more specific purpose.
        /// </summary>
        /// <param name="filePath">The physical file path for the file that is to be uploaded to the S3 bucket.</param>
        /// <param name="bucketName">The name of the S3 bucket the file should be uploaded to.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to</param>
        /// <param name="employeeId">The employee Id for the employee which this file is being uploaded to/for.</param>
        /// <param name="caseId">The case Id for the case which this file is being attached to.</param>
        /// <returns></returns>
        public string UploadFile(string filePath, string bucketName, string customerId = null, string employerId = null, string employeeId = null, string caseId = null)
        {
            try
            {
                string fileKey = GetFileKey(filePath, null, customerId, employerId, employeeId, caseId);
                using (IAmazonS3 client = S3Client())
                {
                    PutObjectRequest request = new PutObjectRequest();
                    request.BucketName = bucketName;
                    request.Key = fileKey;
                    request.CannedACL = S3CannedACL.Private;
                    request.FilePath = filePath;
                    client.PutObject(request);
                }

                return fileKey;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Log.Error("S3 error occurred. Exception: " + amazonS3Exception.Message, amazonS3Exception);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("Error uploading file to S3", ex);
                throw;
            }
        } // UploadFile

        public IEnumerable<string> UploadFiles(IEnumerable<string> fileNames, string bucketName, string customerId = null, string employerId = null, string employeeId = null, string caseId = null)
        {
            if (fileNames == null || !fileNames.Any()) return new string[0];
            if (fileNames.Count() == 1)
                return new string[1] { UploadFile(fileNames.First(), bucketName, customerId, employerId, employeeId, caseId) };
            return fileNames.AsParallel().Select(f => UploadFile(f, bucketName, customerId, employerId, employeeId, caseId));
        } // UploadFiles

        /// <summary>
        /// Gets a byte array for the given resource in the provided bucket from S3.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <returns>The entire byte array from the S3 bucket for the resource.</returns>
        public byte[] DownloadFile(string resource, string bucketName)
        {
            using (IAmazonS3 client = S3Client())
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = bucketName;
                request.Key = resource;
                var response = client.GetObject(request);

                using (Stream amazonS3Stream = response.ResponseStream)
                using (MemoryStream fs = new MemoryStream())
                {
                    amazonS3Stream.CopyTo(fs);
                    return fs.ToArray();
                }
            }
        } // DownloadFile

        /// <summary>
        /// Gets a download stream for the given resource in the provided bucket from S3.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <returns>A readable response stream from the S3 bucket for the resource.</returns>
        public Stream DownloadStream(string resource, string bucketName)
        {
            Stream ms = new MemoryStream();
            using (IAmazonS3 client = S3Client())
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = bucketName;
                request.Key = resource;
                var response = client.GetObject(request);

                response.ResponseStream.CopyTo(ms);
                
            }
            ms.Position = 0;
            return ms;
        } // DownloadStream

        /// <summary>
        /// Downloads a file from S3 given the resource locator in the provided bucket to disk
        /// directly and returns the temp file path. This is the FASTEST and most efficient way 
        /// of dealing with larger files directly rather than in-memory (so they can be streamed).
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <returns>The temp file path</returns>
        public string DownloadFileToDisk(string resource, string bucketName)
        {
            string tempFileName = Path.GetTempFileName();

            using (IAmazonS3 client = S3Client())
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = bucketName;
                request.Key = resource;
                var response = client.GetObject(request);

                using (Stream amazonS3Stream = response.ResponseStream)
                using (FileStream fs = File.OpenWrite(tempFileName))
                    amazonS3Stream.CopyTo(fs);
            }

            return tempFileName;
        } // DownloadFileToDisk

        /// <summary>
        /// Downloads a file from S3 given the resource locator in the provided bucket to disk
        /// directly and returns the temp file path. This is the FASTEST and most efficient way 
        /// of dealing with larger files directly rather than in-memory (so they can be streamed).
        /// If the file downloaded is a ZIP file, it will automatically be extracted and the zip
        /// file deleted and the unziped text file inside's path returned instead.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <returns>The temp file path</returns>
        public string DownloadFileToDiskAsText(string resource, string bucketName)
        {
            using (var fc = new S3FileContext(this, bucketName, resource))
            {
                if (!ZipFile.IsZipFile(fc.TempFileName, false))
                {
                    fc.Keep = true;
                    return fc.TempFileName;
                }

                string unzipFile = Path.GetTempFileName();
                using (ZipFile zip = ZipFile.Read(fc.TempFileName))
                    foreach (ZipEntry entry in zip)
                        using (FileStream stream = File.OpenWrite(unzipFile))
                        {
                            entry.Extract(stream);
                            break;
                        }

                return unzipFile;
            }
        } // DownloadFileToDiskAsText

        /// <summary>
        /// Reads a file stream from S3 and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file. This may spawn multiple threads
        /// and is not guaranteed to deliver all lines in order.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <param name="handleRow">An action that takes a row number/line # and row text which is invoked for
        /// each line in the file.</param>
        public void ReadFileByLine(string resource, string bucketName, Action<long, string> handleRow)
        {
            using (var fc = new S3FileContext(this, bucketName, resource))
                ReadFileByLine(fc.TempFileName, handleRow);
        } // ReadFileByLine

        /// <summary>
        /// Reads a file stream from a file on disk and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file.
        /// </summary>
        /// <param name="path">The physical file path on disk to find the file, relative or absolute.</param>
        /// <param name="handleRow">An action that takes a row number/line # and row text which is invoked for
        /// each line in the file.</param>
        public void ReadFileByLine(string path, Action<long, string> handleRow)
        {
            int lineNumber = 0;
            string line = null;
            using (StreamReader reader = new StreamReader(path))
                while ((line = reader.ReadLine()) != null)
                {
                    lineNumber++;
                    handleRow(lineNumber, line);
                }
        } // ReadFileByLine

        /// <summary>
        /// Reads a file stream from S3 and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file. This may spawn multiple threads
        /// and is not guaranteed to deliver all lines in order.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        public void ReadDelimitedFileByLine(string resource, string bucketName, Action<long, string[], char, string> handleRow)
        {
            using (var fc = new S3FileContext(this, bucketName, resource))
                ReadDelimitedFileByLine(fc.TempFileName, handleRow);
        } // ReadDelimitedFileByLine

        /// <summary>
        /// Reads the delimited file by line.
        /// </summary>
        /// <param name="path">The physical file path to the zip file resource.</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        public void ReadDelimitedFileByLine(string path, Action<long, string[], char, string> handleRow)
        {
            using (FileStream reader = File.OpenRead(path))
                ReadDelimitedFile(reader, handleRow);
        } // ReadDelimitedFileByLine

        /// <summary>
        /// Reads the delimited file and calls the handleRow callback for each row's parsed set of values, if any.
        /// </summary>
        /// <param name="fileStream">The file stream to read from.</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        private void ReadDelimitedFile(Stream fileStream, Action<long, string[], char, string> handleRow)
        {
            long lineNumber = 0;
            char delimiter = GetDelimiter(fileStream);
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(fileStream))
            {   
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters(delimiter.ToString());
                while (!parser.EndOfData)
                {
                    string lineText = string.Empty;
                    string[] _parts = null;
                    lineNumber++;

                    try
                    {
                        lineText = parser.PeekChars(int.MaxValue);
                        _parts = parser.ReadFields();                        
                    }
                    catch (Exception ex)
                    {   
                        Log.Info(string.Format("Error while processing eligibility file upload '{0}'", ex.Message), ex);
                    }
                    finally
                    {
                        handleRow(lineNumber, _parts, delimiter, lineText);                        
                    }
                }
            }
        } // ReadDelimitedFile

        /// <summary>
        /// The zip extensions
        /// </summary>
        private static string[] zipExtensions = new string[] { ".zip", ".zipx" };

        /// <summary>
        /// Compresses the file and returns the corresponding destination output file path.
        /// </summary>
        /// <param name="sourceFile">The source file.</param>
        /// <param name="destinationFile">The destination file. If not provided will use the source file name + ".zip"</param>
        /// <returns></returns>
        public string CompressFile(string sourceFile, string destinationFile = null)
        {
            destinationFile = destinationFile ?? string.Concat(sourceFile, ".zip");
            using (FileStream orig = new FileStream(sourceFile, FileMode.Open))
            using (ZipFile zip = new ZipFile(destinationFile))
            {
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.CompressionMethod = CompressionMethod.Deflate;
                zip.AddFile(sourceFile);
                zip.Save();
            }
            return destinationFile;
        }

        /// <summary>
        /// Reads a zip file stream from S3 and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file. This may spawn multiple threads
        /// and is not guaranteed to deliver all lines in order. This method attempts to
        /// read a zip file from the S3 stream and decompresses it on the fly while reading as necessary.
        /// If the file is not a zip file, will instead abort operation and call the normal ReadFileByLine
        /// method automatically.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <param name="handleRow">An action that takes a row number/line # and row text which is invoked for
        /// each line in the file.</param>
        public void ReadZipFileByLine(string resource, string bucketName, Action<long, string> handleRow)
        {
            using (var fc = new S3FileContext(this, bucketName, resource))
                ReadZipFileByLine(fc.TempFileName, handleRow);
        } // ReadZipFileByLine

        /// <summary>
        /// Reads a zip file stream from the given path and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file. This may spawn multiple threads
        /// and is not guaranteed to deliver all lines in order. This method attempts to
        /// read a zip file from the file system and decompresses it on the fly while reading as necessary.
        /// If the file is not a zip file, will instead abort operation and call the normal ReadFileByLine
        /// method automatically.
        /// </summary>
        /// <param name="path">The physical file path to the zip file resource.</param>
        /// <param name="handleRow">An action that takes a row number/line # and row text which is invoked for
        /// each line in the file.</param>
        public void ReadZipFileByLine(string path, Action<long, string> handleRow)
        {
            int lineNumber = 0;
            string line = null;
            if (!ZipFile.IsZipFile(path, false))
            {
                ReadFileByLine(path, handleRow);
                return;
            }
            using (ZipFile zip = ZipFile.Read(path))
                foreach (ZipEntry entry in zip)
                {
                    using (CrcCalculatorStream entryReader = entry.OpenReader())
                    using (StreamReader reader = new StreamReader(entryReader))
                        while ((line = reader.ReadLine()) != null)
                        {
                            lineNumber++;
                            handleRow(lineNumber, line);
                        }
                }
        } // ReadZipFileByLine

        /// <summary>
        /// Reads a zip file stream from S3 and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file. This may spawn multiple threads
        /// and is not guaranteed to deliver all lines in order. This method attempts to
        /// read a zip file from the S3 stream and decompresses it on the fly while reading as necessary.
        /// If the file is not a zip file, will instead abort operation and call the normal ReadFileByLine
        /// method automatically.
        /// </summary>
        /// <param name="resource">The file key/resource of the item in S3</param>
        /// <param name="bucketName">The bucket name where the resource is located</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        public void ReadDelimitedZipFileByLine(string resource, string bucketName, Action<long, string[], char, string> handleRow)
        {
            using (var fc = new S3FileContext(this, bucketName, resource))
                ReadDelimitedZipFileByLine(fc.TempFileName, handleRow);
        } // ReadDelimitedZipFileByLine

        /// <summary>
        /// Reads a zip file stream from the given path and attempts to read it textually line by line and invokes
        /// the passed in handleRow action for each line in the file as it is parsed to get its delimiters.
        /// This may spawn multiple threads and is not guaranteed to deliver all lines in order. This method attempts to
        /// read a zip file from the file system and decompresses it on the fly while reading as necessary.
        /// If the file is not a zip file, will instead abort operation and call the normal ReadFileByLine
        /// method automatically.
        /// </summary>
        /// <param name="path">The physical file path to the zip file resource.</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        public void ReadDelimitedZipFileByLine(string path, Action<long, string[], char, string> handleRow)
        {
            if (!ZipFile.IsZipFile(path, false))
            {
                ReadDelimitedFileByLine(path, handleRow);
                return;
            }
            using (ZipFile zip = ZipFile.Read(path))
                foreach (ZipEntry entry in zip)
                {
                    string tempFileName = Path.GetTempFileName();
                    using (FileStream fs = File.Create(tempFileName))
                    {
                        entry.Extract(fs);
                        fs.Flush(true);
                        fs.Seek(0, SeekOrigin.Begin);
                        ReadDelimitedFile(fs, handleRow);
                    }
                }
        } // ReadDelimitedZipFileByLine

        /// <summary>
        /// Gets the delimiter.
        /// </summary>
        /// <param name="fileStream">The file stream.</param>
        /// <returns></returns>
        private char GetDelimiter(Stream fileStream)
        {
            string line = null;
            char delimiter = ',';

            using (StreamReader reader = new StreamReader(fileStream, Encoding.Default, true, 1024, true))
                while ((line = reader.ReadLine()) != null)
                {
                    // If they included blank lines at the beginning, WTF, but it's whatever, skip on with life
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    // Determine the delimiter (test for tabs, then for pipes, otherwise assume commas)
                    if (line.Contains('\t')) delimiter = '\t';
                    else if (line.Contains('|')) delimiter = '|';
                    else delimiter = ',';
                    break;
                }

            // Stream reset
            if (fileStream.CanSeek)
                fileStream.Seek(0L, SeekOrigin.Begin);

            // Return the delimiter
            return delimiter;
        } // GetDelimiter

        /// <summary>
        /// Gets the proper file name and path based on the original file name, customer, employer, employee and case Id, all past
        /// the original file name are optional, however must be provided in cascading order, meaning if supplying caseId, ALL values
        /// are required.
        /// </summary>
        /// <param name="originalFileName">The original file name.</param>
        /// <param name="path">The amended path for where the file should be stored.</param>
        /// <param name="customerId">The customer Id for the customer the employer belongs to and which owns this upload request.</param>
        /// <param name="employerId">The employer Id for the employer which this file upload belongs to.</param>
        /// <param name="employeeId">The employee Id for the employee which this file is being uploaded to/for.</param>
        /// <param name="caseId">The case Id for the case which this file is being attached to.</param>
        /// <returns>The fully qualified S3 file name path to use for the Key parameter.</returns>
        private string GetFileKey(string originalFileName, string path = null, string customerId = null, string employerId = null, string employeeId = null, string caseId = null)
        {
            path = path ?? string.Empty;
            if (!string.IsNullOrWhiteSpace(customerId))
            {
                path = string.Concat(path, customerId, "/");
                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    path = string.Concat(path, employerId, "/");
                    if (!string.IsNullOrWhiteSpace(employeeId))
                    {
                        path = string.Concat(path, "employees/", employeeId, "/");
                        if (!string.IsNullOrWhiteSpace(caseId))
                        {
                            path = string.Concat(path, "cases/", caseId, "/");
                        }
                    }
                }
            }

            string myPath = _basePath;
            if (!string.IsNullOrWhiteSpace(path))
            {
                if (path.StartsWith("/"))
                    path = path.Substring(1);
                if (!string.IsNullOrWhiteSpace(path))
                    myPath = string.Concat(myPath, path, path.EndsWith("/") ? "" : "/");
            }
            string fileName = string.Format("{0:N}{1}", Guid.NewGuid(), Path.GetExtension(originalFileName).ToLowerInvariant());
            if (!string.IsNullOrWhiteSpace(myPath))
            {
                if (myPath.StartsWith("/"))
                    myPath = myPath.Substring(1);
                if (!string.IsNullOrWhiteSpace(myPath))
                    fileName = string.Concat(myPath, myPath.EndsWith("/") ? "" : "/", fileName);
            }

            return fileName;
        } // GetFileKey

        /// <summary>
        /// Determines whether the file service (S3) is alive asynchronously.
        /// </summary>
        /// <returns><c>true</c> if the file service is alive; otherwise <c>false</c>.</returns>
        public async Task<bool> IsAliveAsync()
        {
            using (IAmazonS3 client = S3Client())
            {
                var res = await client.GetBucketLocationAsync(new GetBucketLocationRequest()
                {
                    BucketName = Settings.Default.S3BucketName_Files
                });
                return res.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
        }

        /// <summary>
        /// Gets a new instance of an Amazon AWS S3 Client
        /// </summary>
        /// <returns>A new instance of an Amazon AWS S3 Client</returns>
        private IAmazonS3 S3Client()
        {
            AmazonS3Config config = new AmazonS3Config()
            {
                //RegionEndpoint = RegionEndpoint.GetBySystemName(Settings.Default.AWSRegion),
                ServiceURL = "https://s3.amazonaws.com/",
                ProxyHost = null
            };
            return AWSClientFactory.CreateAmazonS3Client(
                Settings.Default.AWSAccessId,
                Settings.Default.AWSSecretKey,
                config);
        } // S3Client

        private class S3FileContext : IDisposable
        {
            #region S3FileContext

            private readonly string _fileName;
            /// <summary>
            /// Gets the name of the temporary file on disk.
            /// </summary>
            /// <value>
            /// The name of the temporary file on disk.
            /// </value>
            public string TempFileName { get { return _fileName; } }
            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="S3FileContext"/> is a keeper.
            /// </summary>
            /// <value>
            ///   <c>true</c> if keep; otherwise, <c>false</c>.
            /// </value>
            public bool Keep { get; set; }
            /// <summary>
            /// Initializes a new instance of the <see cref="S3FileContext"/> class.
            /// </summary>
            /// <param name="fs">The file service instance this is being used for.</param>
            /// <param name="bucketName">Name of the bucket to get the file from.</param>
            /// <param name="resource">The resource name of the file, or the File Key.</param>
            public S3FileContext(FileService fs, string bucketName, string resource)
            {
                try
                {
                    _fileName = fs.DownloadFileToDisk(resource, bucketName);
                }
                catch (Exception s3Ex)
                {
                    Log.Error(string.Format("Error getting file from S3, '{1}' in bucket '{2}', and writing to temporary file, '{0}'", _fileName, resource, bucketName), s3Ex);
                    try { File.Delete(_fileName); }
                    catch (Exception ex) { Log.Error(string.Format("Error deleting temporary file, '{0}'", _fileName), ex); }
                    throw;
                }
            }
            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                if (Keep) return;
                try { if (File.Exists(_fileName)) File.Delete(_fileName); }
                catch (Exception ex) { Log.Error(string.Format("Error deleting temporary file, '{0}'", _fileName), ex); }
            }

            #endregion S3FileContext
        } // S3FileContext
    }
}

namespace AbsenceSoft.Data
{
    using AbsenceSoft.Logic.Common;

    public static class FileServiceExtensions
    {
        /// <summary>
        /// Uploads the specified document to the given bucket.
        /// </summary>
        /// <param name="doc">The document to upload.</param>
        /// <param name="bucketName">Name of the bucket to upload to.</param>
        /// <returns>The uploaded document with file key set.</returns>
        public static Document Upload(this Document doc, string bucketName = null)
        {
            if (doc == null || doc.File == null)
                return doc;
            using (FileService fs = new FileService())
                return fs.UploadDocument(doc, bucketName ?? Settings.Default.S3BucketName_Files);
        }

        /// <summary>
        /// Removes the specified document from the given bucket.
        /// </summary>
        /// <param name="doc">The document to remove from the bucket.</param>
        /// <param name="bucketName">Name of the bucket to remove the document from.</param>
        /// <returns>The removed document record.</returns>
        public static Document Remove(this Document doc, string bucketName = null)
        {
            if (!string.IsNullOrWhiteSpace(doc.Locator))
                using (FileService fs = new FileService())
                    fs.DeleteS3Object(doc.Locator, bucketName ?? Settings.Default.S3BucketName_Files);

            if (!doc.IsNew)
                doc.Delete();

            Log.Info("Deleted file \"{0}\" from bucket \"{1}\" with name \"{2}\"{3}", doc.Locator, bucketName ?? Settings.Default.S3BucketName_Files, doc.FileName,
                doc.IsNew ? "" : string.Format(" for document _id \"{0}\".", doc.Id));

            return doc;
        }

        /// <summary>
        /// Downloads the specified document from the given bucket.
        /// </summary>
        /// <param name="doc">The document to download.</param>
        /// <param name="bucketName">Name of the bucket to download the document from.</param>
        /// <returns>The document record that contains the File property populated.</returns>
        public static Document Download(this Document doc, string bucketName = null)
        {
            if (doc == null || string.IsNullOrWhiteSpace(doc.Locator))
                return doc;
            if (doc.File != null && doc.File.LongLength > 0D)
                return doc;
            using (FileService fs = new FileService())
                doc.File = fs.DownloadFile(doc.Locator, bucketName ?? Settings.Default.S3BucketName_Files);
            return doc;
        }

        /// <summary>
        /// Downloads the given document as a read-only, forward-only stream for reading.
        /// </summary>
        /// <param name="doc">The document to download as a stream.</param>
        /// <param name="bucketName">Name of the bucket to download the document from.</param>
        /// <returns>A read-only, forward-only stream to download the document from.</returns>
        public static Stream DownloadStream(this Document doc, string bucketName = null)
        {
            if (doc == null || string.IsNullOrWhiteSpace(doc.Locator))
                return new MemoryStream();
            if (doc.File != null && doc.File.LongLength > 0D)
                return new MemoryStream(doc.File);
            using (FileService fs = new FileService())
                return fs.DownloadStream(doc.Locator, bucketName ?? Settings.Default.S3BucketName_Files);
        }

        /// <summary>
        /// Gets the download URL for a document that contains a temporary read policy for that document
        /// so that it may be downloaded directly from the store provider rather than intermediarily
        /// through the server.
        /// </summary>
        /// <param name="doc">The document record which to get the download URL for.</param>
        /// <param name="bucketName">Name of the bucket the document is located.</param>
        /// <returns>A fully qualified, secure URL to download the document with a temporary access policy embedded.</returns>
        public static string DownloadUrl(this Document doc, string bucketName = null)
        {
            if (doc == null || string.IsNullOrWhiteSpace(doc.Locator))
                return null;
            using (FileService fs = new FileService())
            {
                return fs.GetS3DownloadUrl(doc.Locator, bucketName ?? Settings.Default.S3BucketName_Files, doc.FileName);
            }
                
        }
    }
}
