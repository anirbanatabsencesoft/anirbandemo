﻿using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Common
{
    public static class RegexEvaluators
    {
        //public static string Wrap(Match m, string original, string format)
        public static string Wrap(Match m, string original, string format, bool exactMatch)
        {
            // doesn't match the entire string, otherwise it is a match
            if (m.Length != original.Length && exactMatch)
            {
                // has a preceding letter or digit (i.e., not a real match).
                if (m.Index != 0 && char.IsLetterOrDigit(original[m.Index - 1]))
                {
                    return m.Value;
                }
                // has a trailing letter or digit (i.e., not a real match).
                if (m.Index + m.Length != original.Length && char.IsLetterOrDigit(original[m.Index + m.Length]))
                {
                    return m.Value;
                }
            }
            // it is a match, apply the format
            return string.Format(format, m.Value);
        }
    }
}
