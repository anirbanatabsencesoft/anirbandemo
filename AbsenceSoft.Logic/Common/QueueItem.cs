﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Common
{
    [Serializable]
    public class QueueItem
    {
        public static class CommonAttributes
        {
            public const string MessageType = "messagetype";
        }

        public QueueItem() { Attributes = new Dictionary<string, string>(); }
        public string QueueName { get; set; }
        public string MessageId { get; set; }
        public string ReceiptHandle { get; set; }
        public string Body { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
        public int ReceiveCount { get; set; }

        public T GetMessage<T>()
        {
            return JsonConvert.DeserializeObject<T>(Body);
        }

        public string GetMessageType()
        {
            if (Attributes.ContainsKey(CommonAttributes.MessageType))
            {
                return Attributes[CommonAttributes.MessageType];
            }

            return null;
        }

        public override string ToString()
        {
            return string.Format("MessageType={0}; Body={1}", GetMessageType(), Body);
        }
    }
}
