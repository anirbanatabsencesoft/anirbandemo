﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Bson;
using System;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// Stages the event.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private static Event StageEvent(EventType type, string customerId, string employerId, User user = null)
        {
            Event e = new Event();
            e.CustomerId = customerId;
            e.EmployerId = employerId;
            e.CreatedBy = user ?? User.Current ?? User.System;
            e.ModifiedBy = user ?? User.Current ?? User.System;
            e.Name = type.ToString().SplitCamelCaseString();
            e.EventType = type;
            e.IsSelfService = User.IsEmployeeSelfServicePortal;
            return e;
        }

        /// <summary>
        /// Stages the employee event.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private static Event StageEmployeeEvent(EventType type, string customerId, string employerId, string employeeId, User user = null)
        {
            Event e = StageEvent(type, customerId, employerId, user);
            e.EmployeeId = employeeId;
            return e;
        }

        /// <summary>
        /// Stages the case event.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private static Event StageCaseEvent(EventType type, string customerId, string employerId, string employeeId, string caseId, User user = null)
        {
            Event e = StageEmployeeEvent(type, customerId, employerId, employeeId, user);
            e.CaseId = caseId;
            return e;
        }

        /// <summary>
        /// Stages a ToDo event.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="toDoItem">The ToDo item.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private static Event StageToDoEvent(EventType type, string customerId, string employerId, string employeeId, string caseId, ToDoItem toDoItem, User user = null)
        {
            Event e = StageCaseEvent(type, customerId, employerId, employeeId, caseId, user);
            e.Metadata.SetRawValue("ToDoItemId", toDoItem.Id);
            e.Metadata = e.Metadata.Merge(toDoItem.Metadata ?? new BsonDocument());
            return e;
        }

        /// <summary>
        /// Builds the context.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <returns></returns>
        private static RuleEvaluatorContext<ToDoItem> BuildContext(ToDoItem todo)
        {
            RuleEvaluatorContext<ToDoItem> context = new RuleEvaluatorContext<ToDoItem>(todo)
            {
                Customer = todo.Customer,
                Employer = todo.Employer,
                Employee = todo.Employee,
                Case = todo.Case,
                ActiveWorkFlowItem = todo,
                WorkflowInstance = todo.WorkflowInstance,
                Event = todo.WorkflowInstance == null ? null : todo.WorkflowInstance.Event
            };
            return context;
        }

        /// <summary>
        /// Builds the context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="theCase">The case.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        private static RuleEvaluatorContext<T> BuildContext<T>(Case theCase, T target) where T : class, new()
        {
            RuleEvaluatorContext<T> context = new RuleEvaluatorContext<T>(target)
            {
                Customer = theCase.Customer,
                Employer = theCase.Employer,
                Employee = theCase.Employee,
                Case = theCase
            };
            return context;
        }

        /// <summary>
        /// Builds the context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="employee">The employee.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        private static RuleEvaluatorContext<T> BuildContext<T>(Employee employee, T target) where T : class, new()
        {
            RuleEvaluatorContext<T> context = new RuleEvaluatorContext<T>(target)
            {
                Customer = employee.Customer,
                Employer = employee.Employer,
                Employee = employee
            };
            return context;
        }

        /// <summary>
        /// Builds the context from the current workflow instance using the instance state and metadata, and original event.
        /// </summary>
        /// <param name="wf">The workflow instance to build the context from.</param>
        /// <returns></returns>
        public static object BuildContext(this WorkflowInstance wf)
        {
            switch (wf.EventType)
            {
                case EventType.EmployeeCreated:
                case EventType.EmployeeChanged:
                case EventType.EmployeeTerminated:
                case EventType.EmployeeDeleted:
                    return new RuleEvaluatorContext<Employee>(wf);
                case EventType.WorkScheduleChanged:
                case EventType.CaseReducedScheduleChanged:
                    return new RuleEvaluatorContext<Schedule>(wf);
                case EventType.PayScheduleChanged:
                    return new RuleEvaluatorContext<PaySchedule>(wf);
                case EventType.CaseTypeChanged:
                    return new RuleEvaluatorContext<CaseSegment>(wf);
                case EventType.AccommodationCreated:
                case EventType.AccommodationChanged:
                case EventType.AccommodationDeleted:
                case EventType.AccommodationAdjudicated:
                    return new RuleEvaluatorContext<Accommodation>(wf);
                case EventType.TimeOffRequested:
                case EventType.TimeOffAdjudicated:
                    return new RuleEvaluatorContext<IntermittentTimeRequest>(wf);
                case EventType.PolicyExhausted:
                case EventType.PolicyAdjudicated:
                case EventType.PolicyManuallyAdded:
                case EventType.PolicyManuallyDeleted:
                    return new RuleEvaluatorContext<AppliedPolicy>(wf);
                case EventType.AttachmentCreated:
                case EventType.AttachmentDeleted:
                    return new RuleEvaluatorContext<Attachment>(wf);
                case EventType.CommunicationSent:
                case EventType.CommunicationDeleted:
                case EventType.PaperworkSent:
                    return new RuleEvaluatorContext<Communication>(wf);
                case EventType.CommunicationNoteCreated:
                case EventType.CommunicationNoteChanged:
                    return new RuleEvaluatorContext<CommunicationNote>(wf);
                case EventType.CaseNoteCreated:
                case EventType.CaseNoteChanged:
                    return new RuleEvaluatorContext<CaseNote>(wf);
                case EventType.EmployeeNoteCreated:
                case EventType.EmployeeNoteChanged:
                    return new RuleEvaluatorContext<EmployeeNote>(wf);
                case EventType.EmployeeConsultCreated:
                    return new RuleEvaluatorContext<EmployeeConsultation>(wf);
                case EventType.ToDoItemCreated:
                case EventType.ToDoItemCompleted:
                case EventType.ToDoItemChanged:
                case EventType.ToDoItemCanceled:
                case EventType.ToDoItemDeleted:
                    return new RuleEvaluatorContext<ToDoItem>(wf);
                case EventType.PayPeriodSubmitted:
                    return new RuleEvaluatorContext<PayPeriod>(wf);
                case EventType.CertificationCreated:
                case EventType.CertificationsChanged:
                    return new RuleEvaluatorContext<Certification>(wf);
                case EventType.DiagnosisCreated:
                case EventType.DiagnosisChanged:
                case EventType.DiagnosisDeleted:
                    return new RuleEvaluatorContext<DiagnosisCode>(wf);
                case EventType.WorkRestrictionCreated:
                case EventType.WorkRestrictionChanged:
                    return new RuleEvaluatorContext<WorkRestriction>(wf);
                case EventType.EmployeeJobChanged:
                case EventType.EmployeeJobApproved:
                case EventType.EmployeeJobDenied:
                    return new RuleEvaluatorContext<EmployeeJob>(wf);
                case EventType.EmployeeOfficeLocationChanged:
                    return new RuleEvaluatorContext<EmployeeOrganization>(wf);
                case EventType.EmployeeNecessityChanged:
                    return new RuleEvaluatorContext<EmployeeNecessity>(wf);
                case EventType.Manual:
                case EventType.InquiryCreated:
                case EventType.InquiryAccepted:
                case EventType.InquiryClosed:
                case EventType.CaseCreated:
                case EventType.CaseClosed:
                case EventType.CaseCanceled:
                case EventType.CaseAdjudicated:
                case EventType.CaseExtended:
                case EventType.CaseReopened:
                case EventType.WorkRelatedReportingChanged:
                default:
                    return new RuleEvaluatorContext<Case>(wf);
            }
        }
    }
}
