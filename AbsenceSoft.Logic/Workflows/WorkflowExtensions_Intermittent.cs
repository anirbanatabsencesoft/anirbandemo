﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on time off requested.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="tor">The tor.</param>
        /// <param name="user">The user.</param>
        public static void WfOnTimeOffRequested(this Case theCase, IntermittentTimeRequest tor, User user = null)
        {
            Event e = StageCaseEvent(EventType.TimeOffRequested, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("IntermittentTimeRequestId", tor.Id);
            using (var caseService = new CaseService())
            {
                bool passedCertification = caseService.CheckIntermittentTimeRequest(theCase, tor).Pass;
                e.Metadata.SetRawValue("PassedCertification", passedCertification);
                tor.PassedCertification = passedCertification;
            }
                
            
            e.Save();
            var context = BuildContext<IntermittentTimeRequest>(theCase, tor);
            using (WorkflowService wf = new WorkflowService(theCase.Customer, theCase.Employer, user))
                wf.RaiseEvent<IntermittentTimeRequest>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on time off requested.
        /// </summary>
        /// <param name="tor">The tor.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnTimeOffRequested(this IntermittentTimeRequest tor, Case theCase, User user = null) { theCase.WfOnTimeOffRequested(tor, user); }

        /// <summary>
        /// WORKFLOW: Called on time off adjudicated.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="tor">The tor.</param>
        /// <param name="user">The user.</param>
        public static void WfOnTimeOffAdjudicated(this Case theCase, IntermittentTimeRequest tor, User user = null)
        {
            Event e = StageCaseEvent(EventType.TimeOffAdjudicated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("IntermittentTimeRequestId", tor.Id);
            e.Save();
            var context = BuildContext<IntermittentTimeRequest>(theCase, tor);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<IntermittentTimeRequest>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on time off adjudicated.
        /// </summary>
        /// <param name="tor">The tor.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnTimeOffAdjudicated(this IntermittentTimeRequest tor, Case theCase, User user = null) { theCase.WfOnTimeOffAdjudicated(tor, user); }

        /// <summary>
        /// WORKFLOW: Called on certification created.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="cert">The cert.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCertificationCreated(this Case theCase, Certification cert, User user = null)
        {
            Event e = StageCaseEvent(EventType.CertificationCreated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("CertificationId", cert.Id);
            e.Save();
            var context = BuildContext<Certification>(theCase, cert);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Certification>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on certification created.
        /// </summary>
        /// <param name="cert">The cert.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCertificationCreated(this Certification cert, Case theCase, User user = null) { theCase.WfOnCertificationCreated(cert, user); }

        /// <summary>
        /// WORKFLOW: Called on certifications changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="cert">The cert.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCertificationsChanged(this Case theCase, Certification cert, User user = null)
        {
            Event e = StageCaseEvent(EventType.CertificationsChanged, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("CertificationId", cert.Id);
            e.Save();
            var context = BuildContext<Certification>(theCase, cert);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Certification>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on certifications changed.
        /// </summary>
        /// <param name="cert">The cert.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCertificationsChanged(this Certification cert, Case theCase, User user = null) { theCase.WfOnCertificationsChanged(cert, user); }
    }
}
