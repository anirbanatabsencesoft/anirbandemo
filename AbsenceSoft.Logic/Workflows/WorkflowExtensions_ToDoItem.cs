﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using System;
using System.Linq;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// Determines whether this instance is tied to a workflow.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <returns></returns>
        public static bool IsWorkflow(this ToDoItem todo)
        {
            return !string.IsNullOrWhiteSpace(todo.WorkflowInstanceId) && todo.WorkflowInstance != null;
        }

        /// <summary>
        /// Gets the last active known activity history for the given <see cref="T:AbsenceSoft.Data.ToDo.ToDoItem" /> related workflow instance it's running
        /// against, if any. If none, then <c>null</c> is returned.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <returns>
        /// The last known working/pending activity history for the given ToDoItem.
        /// </returns>
        public static ActivityHistory LastHistory(this ToDoItem todo)
        {
            if (string.IsNullOrWhiteSpace(todo.WorkflowInstanceId))
                return null;

            Guid wfAid = todo.Metadata.GetRawValue<Guid>("WorkflowActivityId");
            if (wfAid == default(Guid) || wfAid == Guid.Empty)
                return null;

            var history = ActivityHistory.AsQueryable().Where(h => 
                h.WorkflowInstanceId == todo.WorkflowInstanceId && 
                h.WorkflowActivityId == wfAid &&
                h.IsWaitingOnUserInput &&
                h.Outcome == null)
                .ToList();
            if (!history.Any())
                return null;

            return history.OrderByDescending(h => h.ModifiedDate).FirstOrDefault();
        }

        /// <summary>
        /// WORKFLOW: Called on to do item created.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>
        public static void WfOnToDoItemCreated(this ToDoItem todo, User user = null)
        {
            Event e = StageToDoEvent(EventType.ToDoItemCreated, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on to do item completed.
        /// </summary>
        /// <param name="todo">The todo to raise the event for.</param>
        /// <param name="user">The user that is raising the event.</param>
        /// <param name="outcome">The outcome to set to the todo item, if applicable.</param>
        /// <param name="save">if set to <c>true</c> will save the todo item rather than assume it has already been saved.</param>
        public static void WfOnToDoItemCompleted(this ToDoItem todo, User user = null, string outcome = null, bool save = false, bool checkIncomplete = false)
        {
            // If it's a new todo item, AND it's not been saved, then WTF, no no buddy, buzz off.
            if (todo == null || (todo.IsNew && !save))
                return;

            // Set the result text based on the outcome specified, if any.
            todo.ResultText = outcome ?? todo.ResultText;

            // If we should save it, then let's do that.
            if (save)
            {
                if (checkIncomplete)
                {
                    todo.Status = Data.Enums.ToDoItemStatus.Pending;
                }
                else
                {
                    todo.Status = Data.Enums.ToDoItemStatus.Complete;
                }
                todo.ResultText = todo.ResultText ?? Activity.CompleteOutcomeValue;
                todo.ModifiedById = (user ?? User.Current ?? User.System).Id;                
                todo.Save();
            }

            if (todo.IsWorkflow())
            {
                using (WorkflowService wf = new WorkflowService(user))
                {
                    var history = todo.LastHistory();
                    if (history != null)
                    {
                        var instance = todo.WorkflowInstance;
                        if (instance != null && !todo.IsNew)
                        {
                            //Add New Attribute "ReturnToWork" for Jira Work Item-EZW-3 : RTW not reflecting properly 
                            DateTime? returnToWorkDate=null;
                            if (todo.ItemType == Data.Enums.ToDoItemType.ReturnToWork)
                                {
                                  returnToWorkDate = !string.IsNullOrEmpty(todo.Metadata.GetRawValue<DateTime?>("ReturnToWork").ToString()) ?todo.Metadata.GetRawValue<DateTime?>("ReturnToWork") :null;
                                if(!string.IsNullOrEmpty(returnToWorkDate.ToString()))
                                todo.Metadata.SetRawValue("ReturnToWork", returnToWorkDate);
                                }
                            instance.State.Apply(todo.Metadata);
                            instance.Metadata.Merge(todo.Metadata);
                            instance.Event.Case.HasWorkRestrictions = instance.Case.HasWorkRestrictions;
                            instance.Save();
                            history.Metadata.SetRawValue("ToDoId", todo.Id);
                            history.Metadata.SetRawValue("ToDoItemId", todo.Id);
                            history.Outcome =(history.ActivityId == "QuestionActivity"?outcome:null);
                            wf.CompleteActivity(instance, history);
                            return;
                        }
                    }
                }
            }
            Event e = StageToDoEvent(EventType.ToDoItemCompleted, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }


        /// <summary>
        /// WORKFLOW: Called on to do item completed.
        /// </summary>
        /// <param name="todo">The todo to raise the event for.</param>
        /// <param name="user">The user that is raising the event.</param>
        /// <param name="outcome">The outcome to set to the todo item, if applicable.</param>
        /// <param name="save">if set to <c>true</c> will save the todo item rather than assume it has already been saved.</param>
        public static void WfOnToDoItemExtended(this ToDoItem todo, DateTime NewDueDate, DateTime OldDueDate, User user = null, string outcome = null, bool save = false)
        {
            // If it's a new todo item, AND it's not been saved, then WTF, no no buddy, buzz off.
            if (todo == null || (todo.IsNew && !save))
                return;

            // Set the result text based on the outcome specified, if any.
            todo.ResultText = outcome ?? todo.ResultText;

            // If we should save it, then let's do that.
            if (save)
            {
                todo.Status = Data.Enums.ToDoItemStatus.Pending;
                todo.ResultText = todo.ResultText ?? Activity.CompleteOutcomeValue;
                todo.ModifiedById = (user ?? User.Current ?? User.System).Id; 
                todo.OriginalDueDate = OldDueDate;
                todo.DueDate = NewDueDate;
                todo.Save();
            }

            if (todo.IsWorkflow())
            {
                using (WorkflowService wf = new WorkflowService(user))
                {
                    var history = todo.LastHistory();
                    if (history != null)
                    {
                        var instance = todo.WorkflowInstance;
                        if (instance != null && !todo.IsNew)
                        {
                            instance.State.Apply(todo.Metadata);
                            instance.Metadata.Merge(todo.Metadata);
                            instance.Save();
                            history.Metadata.SetRawValue("ToDoId", todo.Id);
                            history.Metadata.SetRawValue("ToDoItemId", todo.Id);
                            wf.CompleteActivity(instance, history);
                            return;
                        }
                    }
                }
            }
            Event e = StageToDoEvent(EventType.ToDoItemCompleted, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on to do item changed.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>        
        public static void WfOnToDoItemChanged(this ToDoItem todo, User user = null)
        {
            Event e = StageToDoEvent(EventType.ToDoItemChanged, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on to do item canceled.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>
        /// <param name="save">if set to <c>true</c> [save].</param>
        public static void WfOnToDoItemCanceled(this ToDoItem todo, User user = null, bool save = false)
        {
            if (save)
            {
                todo.Status = Data.Enums.ToDoItemStatus.Cancelled;
                todo.ModifiedById = (user ?? User.Current ?? User.System).Id;
                todo.Save();
            }
            if (todo.IsWorkflow())
            {
                using (WorkflowService wf = new WorkflowService(user))
                {
                    var history = todo.LastHistory();
                    if (history != null)
                        wf.CompleteActivity(todo.WorkflowInstance, history);
                }
            }
            Event e = StageToDoEvent(EventType.ToDoItemCanceled, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on to do item deleted.
        /// </summary>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>        
        public static void WfOnToDoItemDeleted(this ToDoItem todo, User user = null)
        {
            Event e = StageToDoEvent(EventType.ToDoItemDeleted, todo.CustomerId, todo.EmployerId, todo.EmployeeId, todo.CaseId, todo, user);
            e.Save();
            var context = BuildContext(todo);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<ToDoItem>(e, context);
        }
    }
}
