﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Bson;
using System;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on employee job changed.
        /// </summary>
        /// <param name="job">The employee job.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeJobChanged(this EmployeeJob job, User user = null)
        {
            if (job == null || job.Employee == null)
                return;
            Event e = StageEmployeeEvent(EventType.EmployeeJobChanged, job.CustomerId, job.EmployerId, job.Employee.Id, user);
            e.Metadata.SetRawValue("EmployeeJobId", job.Id);
            e.Metadata.SetRawValue("JobCode", job.JobCode);
            if (job.Status != null)
                e.Metadata.SetRawValue("Status", job.Status.Value);
            e.CaseId = job.CaseId; // If this is in relation to a case
            e.Save();
            var context = BuildContext<EmployeeJob>(job.Employee, job);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeJob>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee job status changed.
        /// </summary>
        /// <param name="job">The employee job.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeJobStatusChanged(this EmployeeJob job, User user = null)
        {
            if (job.Status == null)
                return;
            EventType et = EventType.EmployeeJobChanged;
            switch (job.Status.Value)
            {
                case AdjudicationStatus.Approved:
                    et = EventType.EmployeeJobApproved;
                    break;
                case AdjudicationStatus.Denied:
                    et = EventType.EmployeeJobDenied;
                    break;
                default:
                    return;
            }
            Event e = StageEmployeeEvent(et, job.CustomerId, job.EmployerId, job.Employee.Id, user);
            e.Metadata.SetRawValue("EmployeeJobId", job.Id);
            e.Metadata.SetRawValue("JobCode", job.JobCode);
            e.Metadata.SetRawValue("Status", job.Status.Value);
            e.CaseId = job.CaseId; // If this is in relation to a case
            e.Save();
            var context = BuildContext<EmployeeJob>(job.Employee, job);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeJob>(e, context);
        }
    }
}
