﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Workflows
{
    [Serializable]
    public class WorkflowCriteria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowCriteria"/> class.
        /// </summary>
        public WorkflowCriteria()
        {
            Options = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets or sets the formal name of the field/criteria item, typically without spaces, camel-case, e.g. FirstName.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the textual label or prompt used to gather the criteria, e.g. "First Name".
        /// </summary>
        public string Prompt { get; set; }

        /// <summary>
        /// Gets or sets the type of control for displaying the criteria prompt in the UI.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Gets or sets the list of options, if any, for the criteria prompt in the UI to choose from
        /// for select types, checkbox lists, etc.
        /// </summary>
        public Dictionary<string, object> Options { get; set; }

        /// <summary>
        /// Gets or sets whether or not this field is required by the user to be populated in order
        /// to query/run the report.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Gets or sets the value (either default value or populated by the user, etc.) for this criteria item.
        /// This value may be a Date, a string, a number, a Boolean, etc.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets the criteria value cast as type T.
        /// </summary>
        /// <typeparam name="T">The type of criteria value expected to be cast and return as</typeparam>
        /// <returns>The current value cast as type T or default(T).</returns>
        public T ValueAs<T>()
        {
            return convert<T>(Value);
        } // ValueAs<T>

        /// <summary>
        /// Converts the specified value to a type of T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="v">The v.</param>
        /// <returns></returns>
        private T convert<T>(object v)
        {
            // If the value is null, return the default of type T
            if (v == null)
                return default(T);

            // If the type matches, just type-cast it and return the result
            if (v is T)
                return (T)v;

            // Get our type of T so we can use it for other tests and ensure .NET reflection caches the result
            //  as an instance on our memory heap for performance reasons
            Type typeOfT = typeof(T);

            // If it is a nullable type, we have to get the base type
            if (typeOfT.IsGenericType && typeOfT.GetGenericTypeDefinition() == typeof(Nullable<>))
                typeOfT = typeOfT.GetGenericArguments().First();

            // If the type matches, just type-cast it and return the result
            if (v.GetType() == typeOfT) return (T)v;

            // If it is an enum, we can't explicitly use Convert.ChangeType, so we have to use the Enum helpers
            //  for this instead.
            if (typeOfT.IsEnum)
            {
                int iVal;
                if (v is string && int.TryParse(v.ToString(), out iVal))
                {
                    if (!Enum.IsDefined(typeOfT, iVal))
                        return default(T);
                    return (T)Enum.ToObject(typeOfT, iVal);
                }
                else if (v is long)
                    v = Convert.ToInt32(v);

                if (!Enum.IsDefined(typeOfT, v))
                    return default(T);
                return (T)Enum.ToObject(typeOfT, v);
            }

            // If it is an assignable type, then use the ChangeType method.
            if (typeOfT.IsAssignableFrom(v.GetType()))
                return (T)Convert.ChangeType(v, typeOfT);

            // Can't figure anything else out, so just return the default of T again. Maybe this
            //  should attempt to throw an exception instead, will need to look into that.
            return default(T);
        }
    }
}
