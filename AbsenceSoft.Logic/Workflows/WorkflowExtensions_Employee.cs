﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Bson;
using System;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on employee created.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeCreated(this Employee employee, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeCreated, employee.CustomerId, employee.EmployerId, employee.Id, user);
            e.Save();
            var context = BuildContext<Employee>(employee, employee);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Employee>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee changed.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="user">The user.</param>
        /// <param name="metaBuilder">The meta builder.</param>
        public static void WfOnEmployeeChanged(this Employee employee, User user = null, Action<BsonDocument> metaBuilder = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeChanged, employee.CustomerId, employee.EmployerId, employee.Id, user);
            if (metaBuilder != null)
                metaBuilder(e.Metadata);
            e.Save();
            var context = BuildContext<Employee>(employee, employee);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Employee>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on work schedule changed.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="newWorkSchedule">The new work schedule.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkScheduleChanged(this Employee employee, Schedule newWorkSchedule, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.WorkScheduleChanged, employee.CustomerId, employee.EmployerId, employee.Id, user);
            e.Metadata.SetRawValue("WorkScheduleId", newWorkSchedule.Id);
            e.Save();
            var context = BuildContext<Schedule>(employee, newWorkSchedule);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Schedule>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on work schedule changed.
        /// </summary>
        /// <param name="newWorkSchedule">The new work schedule.</param>
        /// <param name="employee">The employee.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkScheduleChanged(this Schedule newWorkSchedule, Employee employee, User user = null) { employee.WfOnWorkScheduleChanged(newWorkSchedule, user); }

        /// <summary>
        /// WORKFLOW: Called on pay schedule changed.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="newPaySchedule">The new pay schedule.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPayScheduleChanged(this Employee employee, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.PayScheduleChanged, employee.CustomerId, employee.EmployerId, employee.Id, user);
            e.Save();
            var context = BuildContext<Employee>(employee, employee);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Employee>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee terminated.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeTerminated(this Employee employee, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeTerminated, employee.CustomerId, employee.EmployerId, employee.Id, user);
            e.Save();
            var context = BuildContext<Employee>(employee, employee);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Employee>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee deleted.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeDeleted(this Employee employee, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeDeleted, employee.CustomerId, employee.EmployerId, employee.Id, user);
            e.Save();
            var context = BuildContext<Employee>(employee, employee);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Employee>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee note created.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeNoteCreated(this EmployeeNote note, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeNoteCreated, note.CustomerId, note.EmployerId, note.EmployeeId, user);
            e.Metadata.SetRawValue("EmployeeNoteId", note.Id);
            e.Save();
            var context = BuildContext<EmployeeNote>(note.Employee, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee note changed.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeNoteChanged(this EmployeeNote note, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeNoteChanged, note.CustomerId, note.EmployerId, note.EmployeeId, user);
            e.Metadata.SetRawValue("EmployeeNoteId", note.Id);
            e.Save();
            var context = BuildContext<EmployeeNote>(note.Employee, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on employee consult created.
        /// </summary>
        /// <param name="note">The employee consult.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeConsultCreated(this EmployeeConsultation consult, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeConsultCreated, consult.CustomerId, consult.EmployerId, consult.Employee.Id, user);
            e.Metadata.SetRawValue("EmployeeConsultationId", consult.Id);
            e.Metadata.SetRawValue("ConsultId", consult.ConsultationTypeId);
            e.Save();
            var context = BuildContext<EmployeeConsultation>(consult.Employee, consult);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeConsultation>(e, context);
        }

    }
}
