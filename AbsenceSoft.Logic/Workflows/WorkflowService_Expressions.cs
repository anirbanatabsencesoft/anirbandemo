﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Communications;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Rules;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace AbsenceSoft.Logic.Workflows
{
    public partial class WorkflowService
    {
        /// <summary>
        /// Pass in an event type and it returns a list of RuleExpressions for the UI to build off of
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public List<RuleExpressionGroup> GetExpressionsByEventType(EventType eventType)
        {
            IEnumerable<RuleExpressionGroup> groups = GetRuleExpressions();

            //foreach (RuleExpressionGroup group in groups)
            //    group.Expressions = group.Expressions.Where(e => (e.AvailableForEventTypes & eventType) == eventType).ToList();

            return groups.ToList();
        }

        /// <summary>
        /// Ruleses to rule expressions.
        /// </summary>
        /// <param name="rules">The rules.</param>
        /// <returns></returns>
        public List<RuleExpression> RulesToRuleExpressions(List<Rule> rules)
        {
            List<RuleExpression> expressions = new List<RuleExpression>();
            foreach (Rule rule in rules)
            {
                var expression = GetExpression(rule);
                if (expression != null)
                    expressions.Add(expression);
            }
            return expressions;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftAggregateException"></exception>
        /// <exception cref="AbsenceSoftException"></exception>
        public RuleExpression GetExpression(Rule rule)
        {
            List<string> validation = new List<string>();
            if (string.IsNullOrWhiteSpace(rule.Name))
                validation.Add("A name must be supplied for this rule");
            if (string.IsNullOrWhiteSpace(rule.Operator))
                validation.Add("A valid operator was not selected. Please select a valid operator");
            if (string.IsNullOrWhiteSpace(rule.LeftExpression))
                validation.Add("The rule is missing the left expression (core name) of the property or routine it maps to");
            if (validation.Any())
                throw new AbsenceSoftAggregateException(validation.ToArray());

            string name = rule.LeftExpression;
            string[] pars = null;
            if (!string.IsNullOrWhiteSpace(name) && name.Contains("("))
            {
                name = Regex.Replace(name, @"\(.*\)$", "");
                int idx = rule.LeftExpression.IndexOf('(');
                pars = rule.LeftExpression.Substring(idx + 1, rule.LeftExpression.Length - idx - 2).Split(',').Select(p => p.Trim()).ToArray();
            }

            RuleExpression expression = GetRuleExpressions().SelectMany(g => g.Expressions).FirstOrDefault(r => r.Name == name);
            if (expression == null && pars != null && pars.Length > 0)
            {
                //Custom Field
                expression = GetRuleExpressions().SelectMany(g => g.Expressions).FirstOrDefault(r => r.Name == name + pars[0].Replace(" ", string.Empty).Replace("'", string.Empty) && r.IsCustom);
            }
            if (expression == null)
                return null;

            RuleExpression copy = expression.Clone<RuleExpression>();
            copy.RuleId = rule.Id;
            copy.RuleName = rule.Name;
            copy.RuleDescription = rule.Description;
            copy.RuleOperator = rule.Operator;
            copy.StringValue = rule.RightExpression;
            if (pars != null && pars.Length == copy.Parameters.Count)
                for (var i = 0; i < copy.Parameters.Count; i++)
                    copy.Parameters[i].StringValue = pars[i];

            return copy;
        }

        /// <summary>
        /// Validates and converts any passed in rule expressions from the UI to rules that the code can understand
        /// </summary>
        /// <param name="ruleExpressions"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public List<IRule> RuleExpressionsToRules(List<RuleExpression> ruleExpressions, EventType eventType)
        {
            var rules = new List<IRule>();
            if (ruleExpressions == null || ruleExpressions.Count == 0)
                return rules;

            rules.AddRange(ruleExpressions.Select(GetRule));

            return rules;
        }

        /// <summary>
        /// Gets the rule.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftAggregateException"></exception>
        public IRule GetRule(RuleExpression expression)
        {
            List<string> validation = new List<string>();
            if (string.IsNullOrWhiteSpace(expression.RuleName))
                validation.Add("A name must be supplied for this rule");
            if (string.IsNullOrWhiteSpace(expression.RuleOperator))
                validation.Add("A valid operator was not selected. Please select a valid operator");
            if (!string.IsNullOrWhiteSpace(expression.RuleOperator) && !expression.Operators.ContainsKey(expression.RuleOperator))
                validation.Add("The operator provided is not valid for this rule or expression");
            if (validation.Any())
                throw new AbsenceSoftAggregateException(validation.ToArray());

            string left = expression.Name;
            if (expression.IsCustom)
            {

                left = "CustomField";
                if (expression.Name.Contains("CaseCustomNumberField"))
                    left = "CaseCustomNumberField";
                else if (expression.Name.Contains("CaseCustomDateField"))
                    left = "CaseCustomDateField";
                else if (expression.Name.Contains("CaseCustomBoolField"))
                    left = "CaseCustomBoolField";
                else if (expression.Name.Contains("CustomNumberField"))
                    left = "CustomNumberField";
                else if (expression.Name.Contains("CustomDateField"))
                    left = "CustomDateField";
                else if (expression.Name.Contains("CustomBoolField"))
                    left = "CustomBoolField";
                else if (expression.Name.Contains("CaseCustomField"))
                    left = "CaseCustomField";
            }
            if (expression.IsFunction)
            {
                left += "(";
                if (expression.Parameters != null && expression.Parameters.Any())
                    left += string.Join(", ", expression.Parameters.Select(p => p.StringValue));
                left += ")";
            }

            return new Rule()
            {
                Id = expression.RuleId ?? Guid.NewGuid(),
                Name = expression.RuleName,
                Description = expression.RuleDescription,
                LeftExpression = left,
                Operator = expression.RuleOperator,
                //RightExpression = expression.IsCustom ? string.Format("'{0}'", expression.Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'")) : expression.IsFunction && (expression.Parameters == null || expression.Parameters.Count == 0) ? "true" : expression.StringValue
                RightExpression = expression.IsCustom && expression.ControlType == ControlType.Date ? string.Format("#{0}#", expression.Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'"))
                    : expression.IsCustom && expression.ControlType == ControlType.Numeric ? string.Format("{0}", expression.Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'"))
                    : expression.IsCustom && expression.ControlType == ControlType.TextBox ? string.Format("'{0}'", expression.Value.ToString().Replace("\\", "\\\\").Replace("'", "\\'"))
                    : expression.StringValue
            };
        }



        /// <summary>
        /// Gets the enum options.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns></returns>
        private List<RuleExpressionOption> getEnumOptions<TEnum>()
        {
            return Enum
                .GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .Select(m => new RuleExpressionOption()
                {
                    Value = m,
                    Text = m.ToString().SplitCamelCaseString()
                })
                .ToList();
        }

        /// <summary>
        /// Gets the Employee Class options.
        /// </summary>
        /// <returns></returns>
        private List<RuleExpressionOption> getEmployeeClassOptions()
        {
            var employeeClasses = (new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser)).GetAllEmployeeClasses();
            return employeeClasses
                .Select(m => new RuleExpressionOption()
                {
                    Value = m.Code,
                    Text = m.Name
                })
                .ToList();
        }

        /// <summary>
        /// Gets the rule expressions.
        /// </summary>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        private IEnumerable<RuleExpressionGroup> GetRuleExpressions()
        {

            var myGroups = new List<RuleExpressionGroup>();

            #region employeeGroup
            var employeeGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Employee Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's demographic, eligibility, relationship and other attributed data",
                Expressions = new List<RuleExpression>()
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.EmployeeNumber",
                Prompt = "Employee Number",
                HelpText = "Matches an explicit employee number either to be inclusive or to exclude that employee number from consideration (i.e. everyone but the CEO gets this policy).",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Employee Number",
                RuleDescription = "Employee number is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.OfficeLocation",
                Prompt = "Office Location Name",
                HelpText = "Matches an explicit office location name for the employee to be inclusive or exclude that office location from consideration (i.e. West Region Office).",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Office Location Name",
                RuleDescription = "Office location name is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.Address.City",
                Prompt = "City of Residence",
                HelpText = "Matches on the employee's address city (residence address, not office)",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "City of Residence",
                RuleDescription = "Employee's city of residence is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.Address.State",
                Prompt = "State of Residence (Non-US)",
                HelpText = "Matches on the employee's address state (residence address, not office). Use the 2-character ISO code for US states and territories",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "State of Residence",
                RuleDescription = "Employee's state of residence is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.Address.PostalCode",
                Prompt = "Postal Code of Residence",
                HelpText = "Matches on the employee's address postal code (residence address, not office)",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Postal Code of Residence",
                RuleDescription = "Employee's postal code of residence is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.Address.Country",
                Prompt = "Country of Residence",
                HelpText = "Matches on the employee's address country (residence address, not office). Use the 2-character ISO country code, e.g. US, CA, MX, etc.",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Country of Residence",
                RuleDescription = "Employee's country of residence is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.DoB",
                Prompt = "Date of Birth",
                HelpText = "Compares a provided value to the employee's date of birth, if provided",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee's Date of Birth",
                RuleDescription = "Compares the employee's date of birth to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.MilitaryStatus",
                Prompt = "Military Status",
                HelpText = "Matches on the employee's military status",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                Options = getEnumOptions<MilitaryStatus>(),
                RuleName = "Military Status",
                RuleDescription = "Employee's military status is or is not",
                RuleOperator = "==",
                TypeName = typeof(MilitaryStatus).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Meets50In75MileRule",
                Prompt = "50 Employees in 75 Mile Radius Rule",
                HelpText = "Determines whether or not the employee meets or does not meet the 50 employees in 75 mile rule. If this is disabled in the employer configuration, the result of this rule always returns 'true'",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                IsFunction = true,
                RuleName = "50 in 75 Mile Rule",
                RuleDescription = "Employee meets the 50 employees in 75 mile rule",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "AgeInYears",
                Prompt = "Employee Age in Years",
                HelpText = "Gets the employee's age in years from the case start date for checking min, max or age equality",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                IsFunction = true,
                RuleName = "Employee's Age in Years",
                RuleDescription = "The employee's age in years meets the specified criteria",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Int32).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.ConsultationTypeId",
                Prompt = "Consult Type",
                HelpText = "Gets the consult type to see if a specific consult was created",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                Options = ConsultationType.AsQueryable().ToList().Select(c => new RuleExpressionOption()
                {
                    Text = c.Consult,
                    Value = c.Id
                }).ToList(),
                RuleName = "Consult Type",
                RuleDescription = "The type of the consult the employeee has",
                RuleOperator = "=="
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.RiskProfile.Code",
                Prompt = "Risk Profile",
                HelpText = "Compares the employee risk profile to the value provided",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Employee Risk Profile",
                RuleDescription = "The employee risk profile matches a specific profile",
                RuleOperator = "==",
                Options = GetRiskProfileOptions(EntityTarget.Employee),
                Value = null,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.IsFlightCrew",
                Prompt = "Is Airline Flight Crew",
                HelpText = "Determines whether or not the employee is an Airline Flight Crew employee",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Airline Flight Crew",
                RuleDescription = "Ensures that an employee is an airline flight crew employee",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.WorkState",
                Prompt = "Work State (Non-US)",
                HelpText = "Matches on the employee's work state (work state, not state of residence). Use the 2-character ISO code for US states and territories",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Work State",
                RuleDescription = "Employee's work state is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });

            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.EmailAlert",
                Prompt = "Is Email Alert",
                HelpText = "Determines whether employee is an Email Alert employee on ",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Email Alert",
                RuleDescription = "Ensures that an employee is an Email Alert on ",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });

            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.TextAlert",
                Prompt = "Is Text Alert",
                HelpText = "Determines whether employee is an Text Alert employee on ",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Email Alert",
                RuleDescription = "Ensures that an employee is an Text Alert on ",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });

            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.PhoneAlert",
                Prompt = "Is Phone Alert",
                HelpText = "Determines whether employee is an Phone Alert employee on ",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Email Alert",
                RuleDescription = "Ensures that an employee is an Phone Alert on ",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Info.Address.StateUS",
                Prompt = "State of Residence (US)",
                HelpText = "Matches on the employee's US address state (residence address, not office). Use the 2-character ISO code for US states and territories",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                Options = GetStates("US").Select(a => new RuleExpressionOption() { Value = a.Key, Text = a.Value }).ToList(),
                Value = null,
                RuleName = "US State of Residence",
                RuleDescription = "Employee's state of residence is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            employeeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.WorkStateUS",
                Prompt = "Work State (US)",
                HelpText = "Matches on the employee's US work state (work state, not state of residence). Use the 2-character ISO code for US states and territories",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                Options = GetStates("US").Select(a => new RuleExpressionOption() { Value = a.Key, Text = a.Value }).ToList(),
                Value = null,
                RuleName = "US Work State",
                RuleDescription = "Employee's work state is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            //CustomField(name) --> lookup Employer custom fields...
            var fields = CustomField.AsQueryable().Where(f => (f.CustomerId == CurrentUser.CustomerId && f.EmployerId == EmployerId) || (f.CustomerId == CurrentUser.CustomerId && f.EmployerId == null)).AsEnumerable().Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();
            if (fields.Any())
            {
                employeeGroup.Expressions.AddRange(fields.Select(f =>
                {
                    var exp = new RuleExpression()
                    {
                        ControlType = f.DataType == CustomFieldType.Flag ? ControlType.CheckBox : f.DataType == CustomFieldType.Number ?
                                                ControlType.Numeric : f.DataType == CustomFieldType.Date ? ControlType.Date : ControlType.TextBox,
                        Operators = (f.DataType == CustomFieldType.Number || f.DataType == CustomFieldType.Date) ?
                                                getOperators(true, true, true, true, true, true) : getOperators(),
                        Name = f.DataType == CustomFieldType.Date ? "CustomDateField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Number) ? "CustomNumberField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Flag) ? "CustomBoolField" + f.Name.Replace(" ", string.Empty) : "CustomField" + f.Name.Replace(" ", string.Empty),
                        Prompt = f.Name,
                        HelpText = f.HelpText ?? f.Description,
                        RuleName = f.Name,
                        RuleDescription = f.Description,
                        RuleOperator = "==",
                        IsFunction = true,
                        IsCustom = true,
                        Parameters = new List<RuleExpressionParameter>()
                        {
                                        new RuleExpressionParameter()
                                        {
                                            ControlType = ControlType.Hidden,
                                            Name = "name",
                                            Value = f.Name,
                                            TypeName = typeof(String).AssemblyQualifiedName
                                        }
                        },
                        Value = f.SelectedValue
                    };
                    switch (exp.ControlType)
                    {
                        case ControlType.Date:
                            exp.TypeName = typeof(DateTime).AssemblyQualifiedName;
                            break;
                        case ControlType.Minutes:
                            exp.TypeName = typeof(Int32).AssemblyQualifiedName;
                            break;
                        case ControlType.Numeric:
                            exp.TypeName = typeof(Double).AssemblyQualifiedName;
                            break;
                        case ControlType.CheckBox:
                            exp.TypeName = typeof(Boolean).AssemblyQualifiedName;
                            break;
                        default:
                            exp.TypeName = typeof(String).AssemblyQualifiedName;
                            break;
                    }
                    if (f.ValueType == CustomFieldValueType.SelectList)
                    {
                        exp.ControlType = ControlType.SelectList;
                        exp.Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                    }

                    return exp;
                }));
            }//end: CustomField 
            #endregion

            #region scheduleGroup
            var scheduleGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Schedule Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's work schedule and work history information",
                Expressions = new List<RuleExpression>()
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.ServiceDate",
                Prompt = "Service Date",
                HelpText = "Compares a provided value to the employee's service date, if provided",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee's Service Date",
                RuleDescription = "Compares the employee's service date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.HireDate",
                Prompt = "Hire Date",
                HelpText = "Compares a provided value to the employee's hire date, if provided",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee's Hire Date",
                RuleDescription = "Compares the employee's hire date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.RehireDate",
                Prompt = "Rehire Date",
                HelpText = "Compares a provided value to the employee's rehire date, if provided",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee's Rehire Date",
                RuleDescription = "Compares the employee's rehire date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.TerminationDate",
                Prompt = "Termination Date",
                HelpText = "Compares a provided value to the employee's termination date, if provided",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee's Termination Date",
                RuleDescription = "Compares the employee's termination date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "TotalHoursWorkedLast12Months",
                Prompt = "Total Hours Worked in the Last 12 Months",
                HelpText = "Used to compare the total number of hours this employee has worked in the last 12 months (measured from the start date of the case)",
                ControlType = ControlType.Numeric,
                IsFunction = true,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Total Hours Worked in the Last 12 Months",
                RuleDescription = "The total hours worked in the last 12 months conditions have been met",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Double).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HoursWorkedPerWeek",
                Prompt = "Hours Worked per Week",
                HelpText = "Used to compare the average hours the employee works per week (measured as average over the course of the last year from the start date of the case)",
                ControlType = ControlType.Numeric,
                IsFunction = true,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Hours Worked per Week",
                RuleDescription = "Employee's average hours worked per week requirements are met",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Double).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "AverageHoursWorkedPerWeekOverPeriod",
                Prompt = "Average hours worked per week",
                HelpText = "Used to compare the average hours the employee works per week over a period (e.g. days, months, years, etc.)",
                ControlType = ControlType.Numeric,
                IsFunction = true,
                Operators = getOperators(true, true, true, true, true, true),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                 new RuleExpressionParameter()
                                {
                                    Name = "minAvergeHoursWorkedPerWeek",
                                    Prompt = "Hours Worked per Week",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Minimum Average Hours Worked Per Week .",
                                    Value = 0,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                 },
                                new RuleExpressionParameter()
                                {
                                    Name = "minLengthOfService",
                                    Prompt = "Minimum Length of Service",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of service (i.e. for 3 months, enter in '3' here).",
                                    Value = 12,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of service (i.e. for 3 months, select 'Months').",
                                    Value = Unit.Months,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                }
                },
                RuleName = "Average hours worked per week over a period",
                RuleDescription = "Average hours worked per week has been met",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Boolean).AssemblyQualifiedName

            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "TotalMonthsWorked",
                Prompt = "Total Months Worked",
                HelpText = "Used to compare the total number of months that the employee has worked (measured from the start date of the case)",
                ControlType = ControlType.Numeric,
                IsFunction = true,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Total Months Worked",
                RuleDescription = "Employee has worked the required number of months before the start of the case",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Double).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Has1YearOfService",
                Prompt = "Has Minimum of 1 Year of Service",
                HelpText = "Used to determine if the employee has at least one year of service from the service/hire date",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                RuleName = "Has minimum of 1 Year of Service",
                RuleDescription = "The employee has at least 1 year (12 months) of service",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasMinLengthOfService",
                Prompt = "Minimum Length of Service",
                HelpText = "Minimum length of service has been met from their service/hire date; provide the length of service and applicable unit type for that duration (e.g. days, months, years, etc.)",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "minLengthOfService",
                                    Prompt = "Minimum Length of Service",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of service (i.e. for 3 months, enter in '3' here).",
                                    //Value = 12,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of service (i.e. for 3 months, select 'Months').",
                                    Value = Unit.Months,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                }
                            },
                RuleName = "Minimum length of service",
                RuleDescription = "Minimum length of service has been met",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            scheduleGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasLesserOfHalfFTHoursOrNHoursPerWeek",
                Prompt = "Has Lesser of ½ FT or # Hours per Week",
                HelpText = "Minimum hours worked per week is the lesser of n hrs/week or ½ of employers FT equivalency (i.e. used for Minnesota Parenting Leave)",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "nHours",
                                    Prompt = "Hours",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "The number of hours to measure against, OR the employer's FT hours / 2, whichever is less.",
                                    Value = 20,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                }
                            },
                RuleName = "Minimum hours per week",
                RuleDescription = "Minimum hours worked per week is the lesser of n hrs/week or ½ of employers FT equivalency",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            #endregion

            #region jobGroup
            var jobGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Job Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's job information",
                Expressions = new List<RuleExpression>()
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.IsKeyEmployee",
                Prompt = "Is Key Employee",
                HelpText = "Determines whether or not the employee is a key employee",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Key Employee",
                RuleDescription = "Employee is or is not a key employee",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.IsExempt",
                Prompt = "Is Exempt",
                HelpText = "Determines whether or not the employee is a federally exempt employee",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Exempt",
                RuleDescription = "Employee is a federally exempt employee",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Status",
                Prompt = "Employment Status",
                HelpText = "Compares the employment status",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Employment Status",
                RuleDescription = "Employee is or is not the selected employment status",
                RuleOperator = "==",
                Options = getEnumOptions<EmploymentStatus>(),
                Value = EmploymentStatus.Active,
                TypeName = typeof(EmploymentStatus).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.PayType",
                Prompt = "Pay Type",
                HelpText = "Compares the employee's type of pay",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Pay Type",
                RuleDescription = "Employee does or does not have the selected pay type",
                RuleOperator = "==",
                Options = getEnumOptions<PayType>(),
                Value = PayType.Salary,
                TypeName = typeof(PayType).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Salary",
                Prompt = "Employee Pay",
                HelpText = "Compares the employee's pay rate/salary",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee Pay",
                RuleDescription = "The employee's pay rate/salary requirements are met",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Double).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EffectiveAnnualPayFromLeaveStartDate",
                Prompt = "Effective Annual Pay",
                HelpText = "Compares the employee's effective annual pay (measured from the start date of the case)",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Employee Pay",
                RuleDescription = "The employee's effective annual pay from the leave start date requirements are met",
                RuleOperator = ">=",
                Value = 0,
                IsFunction = true,
                TypeName = typeof(Double).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.EmployeeClassCode",
                Prompt = "Employee Class",
                HelpText = "Compares the employee's type of work frequency",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Employee Class",
                RuleDescription = "Employee does or does not have the selected work type",
                RuleOperator = "==",
                Options = getEmployeeClassOptions(),
                Value = WorkType.FullTime,
                TypeName = typeof(WorkType).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.JobTitle",
                Prompt = "Job Title",
                HelpText = "Compares the employee's job title via an exact match to a provided value",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Job Title",
                RuleDescription = "The employee has or does not have the entered in job title",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Job.Classification",
                Prompt = "Job Classification",
                HelpText = "Compares the job classification for the employee's current position. Represents the Social Security Administration's defintions of the 5 job classifications and related restrictions when relating to social security claims, but are also used for worker's comp and other benefits and legal definitions around disability",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Job Classification",
                RuleDescription = "Employee meets the job classification requirement",
                RuleOperator = "==",
                Options = getEnumOptions<JobClassification>(),
                Value = JobClassification.Light,
                TypeName = typeof(JobClassification).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Job.CensusJobCategoryCode",
                Prompt = "Census Job Category Code",
                HelpText = "Compares the US Equal Employment Opportunity Commission EEO-1 Job Classification Guide code",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Census Job Category Code",
                RuleDescription = "Employee meets the requirement for the US Equal Employment Opportunity Commission EEO-1 Job Classification Guide code",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employee.Job.SOCCode",
                Prompt = "SOC Code",
                HelpText = "Compares the Standard Occupational Classification Manual code for the employee's current job",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "SOC Code",
                RuleDescription = "Employee meets the requirement for the Standard Occupational Classification Manual code",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            jobGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.IsTemporary",
                Prompt = "Is Temporary",
                HelpText = "Determines if the job that the employee was just changed to is a temporary job",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Temporary",
                RuleDescription = "Employee Job Is Temporary",
                RuleOperator = "==",
                TypeName = typeof(bool).AssemblyQualifiedName
            });
            #endregion

            #region caseGroup
            var caseGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Case Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the case/absence, typically through intake",
                Expressions = new List<RuleExpression>()
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Status",
                Prompt = "Case Status Open",
                HelpText = "Compares the case status to the value provided for open status",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Case Status",
                RuleDescription = "The case status matches an open status",
                RuleOperator = "==",
                Options = getEnumOptions<CaseStatus>(),
                Value = CaseStatus.Open,
                TypeName = typeof(CaseStatus).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Status",
                Prompt = "Case Status Inquiry",
                HelpText = "Compares the case status to the value provided for inquiry status",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Case Status",
                RuleDescription = "The case status matches an inquiry status",
                RuleOperator = "==",
                Options = getEnumOptions<CaseStatus>(),
                Value = CaseStatus.Inquiry,
                TypeName = typeof(CaseStatus).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.RiskProfile.Code",
                Prompt = "Risk Profile",
                HelpText = "Compares the case risk profile to the value provided",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Case Risk Profile",
                RuleDescription = "The case risk profile matches a specific profile",
                RuleOperator = "==",
                Options = GetRiskProfileOptions(EntityTarget.Case),
                Value = null,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "NumberOfChildrenForLeave",
                Prompt = "Number of Children for Case",
                HelpText = "Compares the number of children related to the case (i.e. for pregnancy/maternity or adoption of n children)",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Number of Children for Case",
                RuleDescription = "The number of children related to the case requirements are met",
                RuleOperator = ">=",
                Value = 0,
                IsFunction = true,
                TypeName = typeof(Int32).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.StartDate",
                Prompt = "Start Date",
                HelpText = "Compares a provided value to the case's start date",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Case's Start Date",
                RuleDescription = "Compares the case's start date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.EndDate",
                Prompt = "End Date",
                HelpText = "Compares a provided value to the case's end date",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Case's End Date",
                RuleDescription = "Compares the case's end date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "Case.DaysInCase",
                    Prompt = "Calendar Days In Case",
                    HelpText = "Compares the total number of days in the case to the value provided",
                    ControlType = ControlType.Numeric,
                    Operators = getOperators(true, true, true, true, true, true),
                    RuleName = "Days In Case",
                    RuleDescription = "The days in case value compares to a provided value",
                    RuleOperator = "==",
                    TypeName = typeof(int).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Reason.Code",
                Prompt = "Absence Reason",
                HelpText = "Compares a provided value to the case's absence reason",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                Options = new CaseService(CustomerId, EmployerId, CurrentUser).Using(s => s.GetDistinctAbsenceReasons().Select(a => new RuleExpressionOption() { Value = a.Code, Text = a.Name }).ToList()),
                RuleName = "Case's Absence Reason",
                RuleDescription = "Compares the case's absence reason to this value",
                RuleOperator = "==",
                TypeName = typeof(string).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasPolicyDenialReason",
                Prompt = "Policy Denial Reason",
                HelpText = "Checks the provided value is part of case's policy denial reason",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "PolicyReason",
                                    TypeName = typeof(string).AssemblyQualifiedName,
                                    ControlType = ControlType.MultiSelectList,
                                    Options = new CaseService(CustomerId, EmployerId, CurrentUser).Using(s => s.GetAllDenialReasons(DenialReasonTarget.Policy).Select(a => new RuleExpressionOption() { Value = a.Code, Text = a.Description }).ToList()),
                                    Value = null,
                                    HelpText = "The reason to check",
                                    Prompt = "PolicyReason"
                                }
                },
                RuleName = "Case's Policy Denial Reason",
                RuleDescription = "Compares the case's policy denial reason with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasAccomodationDenialReason",
                Prompt = "Accommodation Denial Reason",
                HelpText = "Checks the provided value is part of case's accommodation denial reason",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "AccomodationReason",
                                    TypeName = typeof(string).AssemblyQualifiedName,
                                    ControlType = ControlType.MultiSelectList,
                                    Options = new CaseService(CustomerId, EmployerId, CurrentUser).Using(s => s.GetAllDenialReasons(DenialReasonTarget.Accommodation).Select(a => new RuleExpressionOption() { Value = a.Code, Text = a.Description }).ToList()),
                                    Value = null,
                                    HelpText = "The reason to check",
                                    Prompt = "AccomodationReason"
                                }
                },
                RuleName = "Case's Accomodation Denial Reason",
                RuleDescription = "Compares the case's accommodation denial reason with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.RelatedCaseType",
                Prompt = "Related Case Type",
                HelpText = "When a case has a related case, matches the type of relationship that case shares (i.e. is it a recurring condition/relapse, or some other related reason)",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Related Case Type",
                RuleDescription = "The related case is of the proper type",
                RuleOperator = "==",
                Options = getEnumOptions<RelatedCaseType>(),
                Value = RelatedCaseType.Recurring,
                TypeName = typeof(RelatedCaseType).AssemblyQualifiedName
            });

            var contactTypes = new ContactTypeService().Using(s => s.GetContactTypes(CurrentUser.CustomerId, null, ContactTypeDesignationType.Personal, ContactTypeDesignationType.Self));
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "CaseRelationshipType",
                Prompt = "Related Person",
                HelpText = "Select the type of case's related person allowed or not allowed for this rule",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Related Person",
                RuleDescription = "Relationship to employee is met",
                RuleOperator = "==",
                Options = contactTypes.Select(t => new RuleExpressionOption() { Value = t.Code, Text = t.Name }).ToList(),
                Value = "SELF",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Contact.ContactTypeOther",
                Prompt = "Related Person's Contact Type (when other)",
                HelpText = "Matches a case's related contact type when the type selected is 'Other' and free-form text is provided. Matches against the free-form text itself, not the fact that it's 'Other'.",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Related Person's Contact Type (when other)",
                RuleDescription = "The related contact type is 'x'",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Contact.YearsOfAge",
                Prompt = "Related Person's Age",
                HelpText = "Compares the case's related person's age, if provided",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Related Person's Age",
                RuleDescription = "The related contact's age is at least n",
                RuleOperator = ">=",
                Value = 0,
                TypeName = typeof(Int32).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Contact.MilitaryStatus",
                Prompt = "Related Person's Military Status",
                HelpText = "Compares the person related to this case's military status to the value provided",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Related Person's Military Status",
                RuleDescription = "The related contact's military status matches the value",
                RuleOperator = "==",
                Options = getEnumOptions<MilitaryStatus>(),
                Value = MilitaryStatus.Civilian,
                TypeName = typeof(MilitaryStatus).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.IsAccommodation",
                Prompt = "Is Accommodation",
                HelpText = "Determines whether or not the case is an accommodation case",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Accommodation",
                RuleDescription = "This is an accommodation case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.IsInformationOnly",
                Prompt = "Is Information Only",
                HelpText = "Determines whether or not the case is information only",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Information Only",
                RuleDescription = "This is an information only case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "IsIntermittent",
                Prompt = "Is Intermittent",
                HelpText = "Determines whether or not the case or any portion thereof is intermittent",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                RuleName = "Is Intermittent",
                RuleDescription = "This is an intermittent case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.IsSTD",
                Prompt = "Is STD",
                HelpText = "Determines whether or not the case is a short term disability case",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is STD",
                RuleDescription = "This is a short term disability case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasAnyPolicyType",
                Prompt = "Has Any Policy Type",
                HelpText = "A specific policy type that this case must have",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "policyType",
                                    Prompt = "Policy Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Select the specific policy type that this case must have",
                                    Value = null,
                                    Options = getEnumOptions<PolicyType>(),
                                    TypeName = typeof(PolicyType).AssemblyQualifiedName
                                }
                            },
                RuleName = "Has Policy Type",
                RuleDescription = "Specific policy type that a case has",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.Policy.PolicyType",
                Prompt = "Policy Type",
                HelpText = "Determines whether or not the policy is of a specific policy type",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Is Policy",
                RuleDescription = "This is a specific type of policy",
                RuleOperator = "==",
                Options = getEnumOptions<PolicyType>(),
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.IsReportedByAuthorizedSubmitter",
                Prompt = "Is Reported By Authorized Submitter",
                HelpText = "Determines whether or not the case is reported by an authorized submitter",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Reported By Authorized Submitter",
                RuleDescription = "This is a case that is reported by an authorized submitter",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Disability.PrimaryDiagnosis",
                Prompt = "Primary Diagnosis Code",
                HelpText = "Compares the primary diagnosis code with the value provided. No auto-complete is provided, please properly lookup the exact intended ICD9 or 10 code (they are not interchangeable)",
                ControlType = ControlType.TextBox,
                Operators = getOperators(),
                RuleName = "Primary Diagnosis Code",
                RuleDescription = "The primary diagnosis code for the employee's disability is equal or not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Disability.MedicalComplications",
                Prompt = "Medical Complications",
                HelpText = "Determines whether or not the employee had any medical complications in relation to the case",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Medical Complications",
                RuleDescription = "The employee had medical complications due to the illness or injury related to this case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Disability.Hospitalization",
                Prompt = "Hospitalization",
                HelpText = "Determines whether or not the employee had any hospitalizations in relation to the case",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Hospitalization",
                RuleDescription = "The employee was hospitalized due to the illness or injury related to this case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.Disability.ConditionStartDate",
                Prompt = "Condition Start Date",
                HelpText = "Compares a provided value to the case's disability condition's start date",
                ControlType = ControlType.Date,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Condition Start Date",
                RuleDescription = "Compares the case's disability condition's start date to this value",
                RuleOperator = ">=",
                TypeName = typeof(DateTime).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "IsWorkRelated",
                Prompt = "Is Work Related",
                HelpText = "Determines whether or not the case is work related",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Case Work Related",
                RuleDescription = "This is a work related case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasRelatedSpouseCase",
                Prompt = "Spouse Has Related Case",
                HelpText = "Determines whether or not the case is related to a case the employee's spouse at the same employer also has open",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Has Related Spouse Case",
                RuleDescription = "Has a related spouse case",
                RuleOperator = "==",
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });

            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "IsEligible",
                Prompt = "Is Eligible",
                HelpText = "Determines whether or not the case has any eligible policies",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                RuleName = "Has Eligible Policy",
                RuleDescription = "Has an eligible policy",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Case.IsRelapse",
                Prompt = "Is Relapse",
                HelpText = "Determines whether or not the case is relapse",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Case Relapsed",
                RuleDescription = "This is a relapsed case",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "IsMetadataValueTrue",
                Prompt = "Is Metadata Value True",
                HelpText = "[WARNING]: ***This is an advanced rule*** Please consult your integration/usage guide, or your AbsenceTracker representative if you feel you need to use this rule for your own policies; These metadata values are stored on the case itself, not the employee. These are NOT the same thing as custom fields and are dynamic, system generated properties.",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "fieldName",
                                    Prompt = "Field Name",
                                    ControlType = ControlType.TextBox,
                                    HelpText = "The field name to lookup in the metadata that is expected to be a Boolean value, and should be true",
                                    Value = "",
                                    TypeName = typeof(String).AssemblyQualifiedName
                                }
                            },
                RuleName = "Is Metadata Value True",
                RuleDescription = "",
                RuleOperator = "==",
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.AccommodationRequest.HasAccommodations",
                    Prompt = "Does Case Have Accommodations",
                    HelpText = "Determines whether or not the case has at least one accommodation or more",
                    ControlType = ControlType.CheckBox,
                    IsFunction = true,
                    Operators = getOperators(),
                    RuleName = "Does Case Have Accommodations",
                    RuleDescription = "Case has at least one accommodation or more",
                    RuleOperator = "==",
                    Value = true,
                    TypeName = typeof(Boolean).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "Case.HasWorkRestrictions",
                    Prompt = "Does Have Workplace Restrictions",
                    HelpText = "Determines whether or not the case has at least one work restriction or more",
                    ControlType = ControlType.CheckBox,
                    Operators = getOperators(),
                    RuleName = "Does Case Have Work Restrictions",
                    RuleDescription = "Case has at least one work restriction or more",
                    RuleOperator = "==",
                    Value = true,
                    TypeName = typeof(Boolean).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.AccommodationRequest.FirstCreatedAccommodationDuration",
                    Prompt = "First Created Accommodation Duration",
                    HelpText = "Compares the case's first created accommodation duration value to the value provided for accommodation duration",
                    ControlType = ControlType.SelectList,
                    Operators = getOperators(),
                    RuleName = "Case First Created Accommodation Duration",
                    RuleDescription = "The case's first created accommodation has a duration value that matches an accommodation duration",
                    RuleOperator = "==",
                    Options = getEnumOptions<AccommodationDuration>(),
                    TypeName = typeof(AccommodationDuration).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.AccommodationRequest.LastCreatedAccommodationDuration",
                    Prompt = "Last Created Accommodation Duration",
                    HelpText = "Compares the case's last created accommodation duration value to the value provided for accommodation duration",
                    ControlType = ControlType.SelectList,
                    Operators = getOperators(),
                    RuleName = "Case Last Created Accommodation Duration",
                    RuleDescription = "The case's last created accommodation has a duration value that matches an accommodation duration",
                    RuleOperator = "==",
                    Options = getEnumOptions<AccommodationDuration>(),
                    TypeName = typeof(AccommodationDuration).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.AccommodationRequest.IsAllAccommodationDurationsTemporary",
                    Prompt = "Is All Case Accommodation Durations Temporary",
                    HelpText = "Determines whether or not the case has at least one accommodation or more, and that they are all of temporary acommodation duration",
                    ControlType = ControlType.CheckBox,
                    IsFunction = true,
                    Operators = getOperators(),
                    RuleName = "Is All Case Accommodation Durations Temporary",
                    RuleDescription = "Case has at least one accommodation or more, and that they are all of temporary acommodation duration",
                    RuleOperator = "==",
                    Value = true,
                    TypeName = typeof(Boolean).AssemblyQualifiedName
                });
            caseGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.AccommodationRequest.IsAllAccommodationDurationsPermanent",
                    Prompt = "Is All Case Accommodation Durations Permanent",
                    HelpText = "Determines whether or not the case has at least one accommodation or more, and that they are all of permanent acommodation duration.",
                    ControlType = ControlType.CheckBox,
                    IsFunction = true,
                    Operators = getOperators(),
                    RuleName = "Is All Case Accommodation Durations Permanent",
                    RuleDescription = "Case has at least one accommodation or more, and that they are all of permanent acommodation duration.",
                    RuleOperator = "==",
                    Value = true,
                    TypeName = typeof(Boolean).AssemblyQualifiedName
                });
            //CaseCustomField(name) --> lookup Employer custom fields...
            var caseCustomfields = CustomField.AsQueryable().Where(f => (f.CustomerId == CurrentUser.CustomerId && f.EmployerId == EmployerId) || (f.CustomerId == CurrentUser.CustomerId && f.EmployerId == null)).AsEnumerable().Where(f => (f.Target & EntityTarget.Case) == EntityTarget.Case).ToList();
            if (caseCustomfields.Any())
            {
                caseGroup.Expressions.AddRange(caseCustomfields.Select(f =>
                {
                    var exp = new RuleExpression()
                    {
                        ControlType = f.DataType == CustomFieldType.Flag ? ControlType.CheckBox : f.DataType == CustomFieldType.Number ?
                                                ControlType.Numeric : f.DataType == CustomFieldType.Date ? ControlType.Date : ControlType.TextBox,
                        Operators = (f.DataType == CustomFieldType.Number || f.DataType == CustomFieldType.Date) ?
                                                getOperators(true, true, true, true, true, true) : getOperators(true, true),
                        Name = f.DataType == CustomFieldType.Date ? "CaseCustomDateField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Number) ? "CaseCustomNumberField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Flag) ? "CaseCustomBoolField" + f.Name.Replace(" ", string.Empty) : "CaseCustomField" + f.Name.Replace(" ", string.Empty),
                        Prompt = f.Name,
                        HelpText = f.HelpText ?? f.Description,
                        RuleName = f.Name,
                        RuleDescription = f.Description,
                        RuleOperator = "==",
                        IsFunction = true,
                        IsCustom = true,
                        Parameters = new List<RuleExpressionParameter>()
                        {
                                        new RuleExpressionParameter()
                                        {
                                            ControlType = ControlType.Hidden,
                                            Name = "name",
                                            Value = f.Name,
                                            TypeName = typeof(String).AssemblyQualifiedName
                                        }
                        },
                        Value = f.SelectedValue
                    };
                    switch (exp.ControlType)
                    {
                        case ControlType.Date:
                            exp.TypeName = typeof(DateTime).AssemblyQualifiedName;
                            break;
                        case ControlType.Minutes:
                            exp.TypeName = typeof(Int32).AssemblyQualifiedName;
                            break;
                        case ControlType.Numeric:
                            exp.TypeName = typeof(Double).AssemblyQualifiedName;
                            break;
                        case ControlType.CheckBox:
                            exp.TypeName = typeof(Boolean).AssemblyQualifiedName;
                            break;
                        default:
                            exp.TypeName = typeof(String).AssemblyQualifiedName;
                            break;
                    }
                    if (f.ValueType == CustomFieldValueType.SelectList)
                    {
                        exp.ControlType = ControlType.SelectList;
                        exp.Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                    }

                    return exp;
                }));
            }//end: CaseCustomField  


            var policyListCriteria = new ListCriteria()
            {
                PageSize = int.MaxValue
            };
            caseGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasSpecificPolicy",
                Prompt = "Has Policy",
                HelpText = "A specific policy that this case must have",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "policyCode",
                                    Prompt = "Policy Code",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Select the specific policy that this case must have",
                                    Value = null,
                                    Options = new PolicyService(CurrentCustomer, CurrentEmployer, CurrentUser)
                                        .Using(ps => ps.PolicyList(policyListCriteria)
                                        .Results.Select(r => new RuleExpressionOption() {Text = r["Name"].ToString(), Value = r["Code"].ToString() })
                                        .OrderBy(r => r.Text).ToList()),
                                    TypeName = typeof(string).AssemblyQualifiedName
                                }
                            },
                RuleName = "Has Policy",
                RuleDescription = "Specific polic that a case has",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            #endregion

            #region employerGroup
            var employerGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Employer Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employer (such as number of employees, etc.)",
                Expressions = new List<RuleExpression>()
            });
            employerGroup.Expressions.Add(new RuleExpression()
            {
                Name = "TotalEmployeesAtEmployer",
                Prompt = "Total Employees at Employer",
                HelpText = "Compares the total number of employees employed by this employer (in the AbsenceTracker database) against a provided value.",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Total Employees at Employer",
                RuleDescription = "The total employees at this employer are compared to this value",
                RuleOperator = ">=",
                IsFunction = true,
                Value = 0,
                TypeName = typeof(Int32).AssemblyQualifiedName
            });
            employerGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employer.IsPubliclyTradedCompany",
                Prompt = "Employer is a Publicly Traded Company",
                HelpText = "Determines whether or not the employer is a publicly traded company",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Employer is a Publicly Traded Company",
                RuleDescription = "Employer is or is not a publicly traded company",
                RuleOperator = "==",
                Value = false,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            });
            employerGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Employer.HasFeature",
                Prompt = "Employer has a specific feature",
                HelpText = "Determines whether or not the employer has a given feature, or if absent the customer has it enabled",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                IsFunction = true,
                RuleName = "Employer has Feature",
                RuleDescription = "Employer has a specific feature enabled",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(bool).AssemblyQualifiedName,
                Parameters = new List<RuleExpressionParameter>(1)
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "feature",
                                    TypeName = typeof(Feature).AssemblyQualifiedName,
                                    ControlType = ControlType.SelectList,
                                    HelpText = "The feature to check",
                                    Prompt = "Feature",
                                    Options = getEnumOptions<Feature>()
                                }
                            }
            });
            using (var customerServiceOptionService = new CustomerServiceOptionService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                if (CurrentUser != null && CurrentUser.Customer != null && customerServiceOptionService.CanAccessServiceOptions())
                {
                    var serviceOptions = customerServiceOptionService.GetCustomerServiceOptions();
                    if (serviceOptions != null && serviceOptions.Count > 0)
                    {
                        employerGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "EmployerServiceOption",
                            Prompt = "Service Type",
                            HelpText = "Employer Service Type",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(true, true),
                            Options = serviceOptions.Select(so => new RuleExpressionOption()
                            {
                                Text = so.Value,
                                Value = so.Id
                            }).ToList(),
                            RuleName = "EmployerServiceOption",
                            RuleDescription = "Employer's Service Type",
                            RuleOperator = "==",
                            TypeName = typeof(EmployerServiceOption).AssemblyQualifiedName
                        });

                    }
                }
            }
            #endregion

            #region accommodationGroup
            var accommodationGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Accommodation",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the accommodation",
                Expressions = new List<RuleExpression>()
            });
            accommodationGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.Type.Code",
                Prompt = "Accommodation Type",
                HelpText = "Compares the accommodation type code against a provided value.",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Accommodation Type Code",
                RuleDescription = "The accommodation type code is compared to this value",
                RuleOperator = "==",
                TypeName = typeof(String).AssemblyQualifiedName,
                Options = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser).Using(s => s.GetAccommodationTypes()).Select(a => new RuleExpressionOption()
                {
                    Value = a.Code,
                    Text = a.Name
                }).ToList(),
                AvailableForEventTypes = new List<EventType>()
                            {
                                EventType.AccommodationAdjudicated,
                                EventType.AccommodationChanged,
                                EventType.AccommodationCreated,
                                EventType.AccommodationDeleted
                            }
            });
            accommodationGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasAccomodationType",
                Prompt = "Has Accommodation Type",
                HelpText = "Checks the provided value is part of case's accommodation type",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "AccomodationType",
                                    TypeName = typeof(string).AssemblyQualifiedName,
                                    ControlType = ControlType.SelectList,
                                    Options = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser).Using(s => s.GetAccommodationTypes()).Select(a => new RuleExpressionOption()
                                        {
                                            Value = a.Code,
                                            Text = a.Name
                                        }).ToList(),
                                    Value = null,
                                    HelpText = "The type to check",
                                    Prompt = "AccomodationType"
                                }
                },
                RuleName = "Case's Accomodation Type",
                RuleDescription = "Compares the case's accommodation type with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            accommodationGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.Duration",
                    Prompt = "Accommodation Duration",
                    HelpText = "Compares the accommodation value to the value provided for accommodation duration",
                    ControlType = ControlType.SelectList,
                    Operators = getOperators(),
                    RuleName = "Accommodation Duration",
                    RuleDescription = "The accommodation duration value matches an accommodation duration",
                    RuleOperator = "==",
                    Options = getEnumOptions<AccommodationDuration>(),
                    TypeName = typeof(AccommodationDuration).AssemblyQualifiedName,
                    AvailableForEventTypes = new List<EventType>
                    {
                        EventType.AccommodationAdjudicated,
                        EventType.AccommodationChanged,
                        EventType.AccommodationCreated,
                        EventType.AccommodationDeleted
                    }
                });
            accommodationGroup.Expressions.Add(
                new RuleExpression
                {
                    Name = "EventTarget.DaysInAccommodation",
                    Prompt = "Days In Accommodation",
                    HelpText = "Compares the total number of days in the accommodation to the value provided",
                    ControlType = ControlType.Numeric,
                    Operators = getOperators(true, true, true, true, true, true),
                    RuleName = "Days In Accommodation",
                    RuleDescription = "The days in accommodation value compares to a provided value",
                    RuleOperator = "==",
                    TypeName = typeof(int).AssemblyQualifiedName,
                    AvailableForEventTypes = new List<EventType>
                    {
                        EventType.AccommodationAdjudicated,
                        EventType.AccommodationChanged,
                        EventType.AccommodationCreated,
                        EventType.AccommodationDeleted
                    }
                });
            accommodationGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasAccomodationDenialReasonGroup",
                Prompt = "Accommodation Denial Reason",
                HelpText = "Checks the provided value is part of case's accommodation denial reason",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "AccomodationReason",
                                    TypeName = typeof(string).AssemblyQualifiedName,
                                    ControlType = ControlType.MultiSelectList,
                                    Options = new CaseService(CustomerId, EmployerId, CurrentUser).Using(s => s.GetAllDenialReasons(DenialReasonTarget.Accommodation).Select(a => new RuleExpressionOption() { Value = a.Code, Text = a.Description }).ToList()),
                                    Value = null,
                                    HelpText = "The reason to check",
                                    Prompt = "AccomodationReason"
                                }
                },
                RuleName = "Case's Accomodation Denial Reason",
                RuleDescription = "Compares the case's accommodation denial reason with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            accommodationGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasAccomodationStatus",
                Prompt = "Accommodation Status",
                HelpText = "Checks the provided value is part of case's Accomodation adjudication status",
                ControlType = ControlType.CheckBox,
                DisplayStyle = "none",
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                {
                    new RuleExpressionParameter()
                    {
                        Name = "accomodationType",
                        Prompt = "Accomodation Type",
                        ControlType = ControlType.SelectList,
                        HelpText = "Select the specific accomodation type that this case may have",
                        Value = null,
                        Options = new AccommodationService(CurrentCustomer, CurrentEmployer, CurrentUser).Using(s => s.GetAccommodationTypes()).Select(a => new RuleExpressionOption()
                                        {
                                            Value = a.Code,
                                            Text = a.Name
                                        }).ToList(),
                        TypeName = typeof(string).AssemblyQualifiedName
                    },
                    new RuleExpressionParameter()
                    {
                        Name = "adjudicationStatuses",
                        TypeName = typeof(string).AssemblyQualifiedName,
                        ControlType = ControlType.MultiSelectList,
                        Options = getEnumOptions<AdjudicationStatus>(),
                        Value = null,
                        HelpText = "The policies to check",
                        Prompt = "AdjudicationStatuses"
                    }
                },
                RuleName = "Case's accomodation adjudication status",
                RuleDescription = "Compares the case's accomodation adjudication status with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            #endregion

            #region determinationGroup
            var determinationGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Determination",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of a determination",
                Expressions = new List<RuleExpression>()
            });
            determinationGroup.Expressions.Add(new RuleExpression()
            {
                Name = "IsDetermination",
                IsFunction = true,
                Prompt = "Determination",
                HelpText = "Compares the determination decision against a provided value.",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Determination",
                RuleDescription = "The determination decision is compared to this value",
                RuleOperator = "==",
                TypeName = typeof(bool).AssemblyQualifiedName,
                AvailableForEventTypes = new List<EventType>()
                            {
                                EventType.AccommodationAdjudicated,
                                EventType.CaseAdjudicated
                            },
                Parameters = new List<RuleExpressionParameter>(1)
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "status",
                                    TypeName = typeof(AdjudicationStatus).AssemblyQualifiedName,
                                    ControlType = ControlType.SelectList,
                                    HelpText = "The determination status to compare",
                                    Prompt = "Determination",
                                    Options = getEnumOptions<AdjudicationStatus>()
                                }
                            }
            });
            #endregion

            #region customerGroup
            var customerGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Customer",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the customer",
                Expressions = new List<RuleExpression>()
            });
            customerGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Event.IsSelfService",
                IsFunction = false,
                Prompt = "Is Self-Service (ESS)",
                HelpText = "Checks whether the current customer context is within employee self-service (ESS)",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Is Self Service",
                RuleDescription = "Is Self Service",
                RuleOperator = "==",
                TypeName = typeof(bool).AssemblyQualifiedName,
                Value = true
            });
            customerGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Customer.HasFeature",
                IsFunction = true,
                Prompt = "Has Feature",
                HelpText = "Checks whether the customer has a specific feature enabled.",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Has Feature",
                RuleDescription = "The customer feature set is checked",
                RuleOperator = "==",
                TypeName = typeof(bool).AssemblyQualifiedName,
                Parameters = new List<RuleExpressionParameter>(1)
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "feature",
                                    TypeName = typeof(Feature).AssemblyQualifiedName,
                                    ControlType = ControlType.SelectList,
                                    HelpText = "The feature to check for",
                                    Prompt = "Feature",
                                    Options = getEnumOptions<Feature>()
                                }
                            }
            });
            #endregion

            #region communicationsGroup
            var communicationsGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Communications",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of a sent communication or paperwork",
                Expressions = new List<RuleExpression>()
            });
            communicationsGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.Template",
                IsFunction = false,
                Prompt = "Template",
                HelpText = "Checks whether the current communication is of a specific communication template",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Template Code Match",
                RuleDescription = "Template Code Matches",
                RuleOperator = "==",
                TypeName = typeof(string).AssemblyQualifiedName,
                Options = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser).CommunicationsTemplateList(
                    new ListCriteria() { PageNumber = 0, PageSize = int.MaxValue })
                    .Results
                    .Where(r => !string.IsNullOrWhiteSpace(r.Get<string>("Code")))
                    .OrderBy(r => r.Get<string>("Code"))
                    .Select(r => new RuleExpressionOption()
                    {
                        Value = r.Get<string>("Code"),
                        Text = r.Get<string>("Name") ?? r.Get<string>("Code")
                    }).ToList(),
                AvailableForEventTypes = new List<EventType>()
                            {
                                EventType.CommunicationSent,
                                EventType.CommunicationDeleted,
                                EventType.PaperworkSent
                            }
            });

            communicationsGroup.Expressions.Add(new RuleExpression()
            {
                Name = "Paperwork.Paperwork.Id",
                IsFunction = false,
                Prompt = "Paperwork",
                HelpText = "Checks whether the current paperwork is of a specific paperwork template",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Paperwork Id Match",
                RuleDescription = "Paperwwork Id Matches",
                RuleOperator = "==",
                TypeName = typeof(string).AssemblyQualifiedName,
                Options = new TemplateService(CurrentCustomer, CurrentEmployer, CurrentUser).PaperworkTemplateList(
                    new ListCriteria() { PageNumber = 0, PageSize = int.MaxValue })
                    .Results
                    .Select(r => new RuleExpressionOption()
                    {
                        Value = r.Get<string>("Id"),
                        Text = r.Get<string>("Name")
                    }).ToList(),
                AvailableForEventTypes = new List<EventType>()
                {
                    EventType.PaperworkSent
                }
            });

            #endregion

            #region TimeOffRequestGroup
            var timeOffRequestGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Time Off Request",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of a Time Off Request",
                Expressions = new List<RuleExpression>()
            });
            timeOffRequestGroup.Expressions.Add(new RuleExpression()
            {
                Name = "EventTarget.PassedCertification",
                Prompt = "Passed Certification",
                HelpText = "Determines whether or not the Time Off Request has a valid passed certification",
                ControlType = ControlType.CheckBox,
                Operators = getOperators(),
                RuleName = "Passed Certification",
                RuleDescription = "Has a valid passed certification",
                RuleOperator = "==",
                TypeName = typeof(Boolean).AssemblyQualifiedName,
                AvailableForEventTypes = new List<EventType>()
                {
                    EventType.TimeOffRequested,
                    EventType.TimeOffAdjudicated
                }
            });
            #endregion

            #region policyTypeGroup
            var policyTypeGroup = myGroups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Policy Information",
                Description = "Used as filters, rules and expressions that evaluate properties and conditions of the policy type and its associated status",
                Expressions = new List<RuleExpression>()
            });
            policyTypeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasPolicyStatus",
                Prompt = "Policy Status",
                HelpText = "Checks the provided value is part of case's policy eligibility or adjudication status",
                ControlType = ControlType.CheckBox,
                DisplayStyle = "none",
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                {
                    new RuleExpressionParameter()
                    {
                        Name = "policyType",
                        Prompt = "Policy Type",
                        ControlType = ControlType.SelectList,
                        HelpText = "Select the specific policy type that this case must have",
                        Value = null,
                        Options = getEnumOptions<PolicyType>(),
                        TypeName = typeof(PolicyType).AssemblyQualifiedName
                    },
                    new RuleExpressionParameter()
                    {
                        Name = "policyStatuses",
                        TypeName = typeof(string).AssemblyQualifiedName,
                        ControlType = ControlType.MultiSelectList,
                        Options = getEnumOptions<PolicyStatus>(),
                        Value = null,
                        HelpText = "The policies to check",
                        Prompt = "PolicyStatuses"
                    }
                },
                RuleName = "Case's policy eligibility or adjudication status",
                RuleDescription = "Compares the case's policy eligibility or adjudication status with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            policyTypeGroup.Expressions.Add(new RuleExpression()
            {
                Name = "HasPolicyDenialReasonGroup",
                Prompt = "Policy Denial Reason",
                HelpText = "Checks the provided value is part of case's policy denial reason",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "PolicyReason",
                                    TypeName = typeof(string).AssemblyQualifiedName,
                                    ControlType = ControlType.MultiSelectList,
                                    Options = new CaseService(CustomerId, EmployerId, CurrentUser).Using(s => s.GetAllDenialReasons(DenialReasonTarget.Policy).Select(a => new RuleExpressionOption() { Value = a.Code, Text = a.Description }).ToList()),
                                    Value = null,
                                    HelpText = "The reason to check",
                                    Prompt = "PolicyReason"
                                }
                },
                RuleName = "Case's Policy Denial Reason",
                RuleDescription = "Compares the case's policy denial reason with selected values",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(string).AssemblyQualifiedName
            });
            #endregion

            CreateAttachmentGroup(myGroups);

            /// Alphabetize here, so it doesn't matter what order we put them in.
            foreach (var group in myGroups)
            {
                group.Expressions = group.Expressions.OrderBy(e => e.Prompt).ToList();
            }


            return myGroups.OrderBy(g => g.Title);
        }//end: GetRuleExpressions

        private RuleExpression CreateIsAnyIneligibleExpression()
        {
            return new RuleExpression()
            {
                Name = "IsAnyIneligible",
                Prompt = "Is Any Ineligible",
                HelpText = "Determines whether or not the case has any ineligible policies",
                ControlType = ControlType.CheckBox,
                IsFunction = true,
                Operators = getOperators(),
                RuleName = "Has Ineligible Policy",
                RuleDescription = "Has an ineligible policy",
                RuleOperator = "==",
                Value = true,
                TypeName = typeof(Boolean).AssemblyQualifiedName
            };
        }

        private void CreateAttachmentGroup(List<RuleExpressionGroup> groups)
        {
            var attachmentGroup = groups.AddFluid(new RuleExpressionGroup()
            {
                Title = "Attachment Information",
                Description = "Rules for handling attachments",
                Expressions = new List<RuleExpression>()
            });

            attachmentGroup.Expressions.Add(CreateAttachmentTypeExpression());
        }

        private RuleExpression CreateAttachmentTypeExpression()
        {
            return new RuleExpression()
            {
                Name = "EventTarget.AttachmentType",
                Prompt = "Attachment Type",
                HelpText = "Matches an attachment type against the actual type of the attachment",
                ControlType = ControlType.SelectList,
                Options = getEnumOptions<AttachmentType>(),
                Operators = getOperators(),
                RuleName = "Attachment Type",
                RuleDescription = "Attachment Type is or is not equal to this value",
                RuleOperator = "==",
                TypeName = typeof(AttachmentType).AssemblyQualifiedName,
                AvailableForEventTypes = new List<EventType>()
                {
                    EventType.AttachmentCreated,
                    EventType.AttachmentDeleted
                }
            };
        }

        /// <summary>
        /// Gets the operators.
        /// </summary>
        /// <param name="eq">if set to <c>true</c> [eq].</param>
        /// <param name="neq">if set to <c>true</c> [neq].</param>
        /// <param name="lt">if set to <c>true</c> [lt].</param>
        /// <param name="lte">if set to <c>true</c> [lte].</param>
        /// <param name="gt">if set to <c>true</c> [gt].</param>
        /// <param name="gte">if set to <c>true</c> [gte].</param>
        /// <returns></returns>
        private Dictionary<string, string> getOperators(bool eq = true, bool neq = true, bool lt = false, bool lte = false, bool gt = false, bool gte = false)
        {
            Dictionary<string, string> ops = new Dictionary<string, string>();
            if (eq) ops.Add("==", "Equals");
            if (neq) ops.Add("!=", "Does Not Equal");
            if (lt) ops.Add("<", "Less Than");
            if (lte) ops.Add("<=", "Less Than Or Equal To");
            if (gt) ops.Add(">", "Greater Than");
            if (gte) ops.Add(">=", "Greater Than Or Equal To");
            return ops;
        }//end: getOperators

        private List<RuleExpressionOption> GetRiskProfileOptions(EntityTarget target)
        {
            return RiskProfile.AsQueryable().ToList()
                .Where(rp => (rp.Target & target) == target)
                .Select(rp =>
                    new RuleExpressionOption()
                    {
                        Text = rp.Name,
                        Value = rp.Code
                    }
                ).ToList();
        }
        /// <summary>
        /// Get states for country
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetStates(string countryCode)
        {
            Dictionary<string, string> states = new Dictionary<string, string>();
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/states.xml"));
                XmlNodeList xmlNodeList = doc.SelectNodes("//state");
                if (xmlNodeList != null)
                {
                    foreach (XmlNode node in xmlNodeList)
                    {
                        if (node.Attributes["country"].Value != countryCode)
                        {
                            continue;
                        }

                        var code = string.Empty;
                        if (node.Attributes != null && node.Attributes["code"].Value != null)
                        {
                            code = node.Attributes["code"].Value;
                        }

                        states.Add(code, node.InnerText);
                    }
                }
                return states;
            }
            catch (Exception ex)
            {
                Log.Error("Error loading US states in WF from WorkflowInstance + Metadata", ex);
            }
            return new Dictionary<string, string>();
        }
    }
}
