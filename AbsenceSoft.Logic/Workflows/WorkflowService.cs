﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Rules;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Workflows.Contracts;

namespace AbsenceSoft.Logic.Workflows
{
    public partial class WorkflowService : LogicService, ILogicService, IWorkflowService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowService"/> class.
        /// </summary>
        /// <param name="u">The u.</param>
        public WorkflowService(User u = null)
            : base(u)
        {
        }

        public WorkflowService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        #region Properties

        /// <summary>
        /// Gets or sets the available expressionGroups
        /// </summary>
        private static Dictionary<string, List<RuleExpressionGroup>> _expressionGroups = new Dictionary<string, List<RuleExpressionGroup>>();
        private static object _sync = new object();

        #endregion

        #region CRUD

        /// <summary>
        /// Uses reflection to get all activity classes in the code base
        /// </summary>
        /// <returns></returns>
        public List<Activity> GetAllActivities()
        {
            Type[] typesInAsm;
            try
            {
                typesInAsm = Assembly.GetAssembly(this.GetType()).GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                typesInAsm = ex.Types;
            }

            var activities = typesInAsm
                .Where(r => r != null && !r.IsAbstract && !string.IsNullOrWhiteSpace(r.Namespace) && r.Namespace.StartsWith("AbsenceSoft.Logic.Workflows.Activities") && r.IsSubclassOf(typeof(Activity)))
                .Where(r =>
                {
                    if (CurrentUser == null || CurrentUser.Id == User.System.Id)
                        return true;
                    var atts = r.GetCustomAttributes(typeof(FeatureRestrictionAttribute), true);
                    if (atts == null) return true;
                    if (!atts.Any()) return true;
                    var customer = CurrentUser.Customer;
                    if (customer == null) return true;
                    var fat = atts.First() as FeatureRestrictionAttribute;
                    if (fat == null) return true;
                    if (fat.Feature == Feature.None) return true;
                    return customer.HasFeature(fat.Feature);
                })
                .ToList();

            return activities.Select(r => Activator.CreateInstance(r) as Activity).OrderByDescending(r => r.IsRunFirst).ThenBy(r => r.Category).ThenBy(r => r.Name).ToList();
        }

        /// <summary>
        /// Gets all activity classes in the code base that match a specific event type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Activity> GetActivitiesByEventType(EventType type)
        {
            return GetAllActivities().Where(p => p.GetPossibleEventTypes().Contains(type)).ToList();
        }

        /// <summary>
        /// Gets a specific activity based on its id.
        /// </summary>
        /// <param name="activityId">The activity identifier.</param>
        /// <returns>The activity specified by the id.</returns>
        public Activity GetActivity(string activityId)
        {
            return GetAllActivities().FirstOrDefault(a => a.Id == activityId);
        }

        /// <summary>
        /// Updates a workflow
        /// </summary>
        /// <param name="workflow"></param>
        /// <returns></returns>
        public Workflow Update(Workflow workflow)
        {
            using (new InstrumentationContext("WorkflowService.Update"))
            {
                Workflow savedWorkflow = Workflow.GetById(workflow.Id);
                if (savedWorkflow != null &&
                    (!savedWorkflow.IsCustom || savedWorkflow.EmployerId != EmployerId))
                {
                    workflow.Clean();
                }

                workflow.CustomerId = CustomerId;
                workflow.EmployerId = EmployerId;
                return workflow.Save();
            }
        }

        /// <summary>
        /// Deletes a customer workflow or suppresses a default one
        /// </summary>
        /// <param name="wf"></param>
        public void Delete(Workflow wf)
        {
            using (new InstrumentationContext("WorkflowService.Delete"))
            {
                if (!CurrentUser.HasEmployerAccess(wf.EmployerId))
                    throw new AbsenceSoftException("User does not have permission to delete workflows for this employer.");

                ///Check if it's a default workflow
                if (!wf.IsCustom)
                {
                    CurrentUser.Customer.SuppressedEntities.AddIfNotExists(new SuppressedEntity { EntityName = "Workflow", EntityCode = wf.Code });
                    CurrentUser.Customer.Save();
                }
                else
                {
                    wf.Delete();
                }
            }
        }

        /// <summary>
        /// Toggles the suppression for a template selected by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Workflow ToggleWorkflowSuppressionById(string workflowId)
        {
            var workflow = Workflow.GetById(workflowId);
            workflow.ToggleSuppression(CurrentCustomer, CurrentEmployer);
            workflow.Suppressed = workflow.IsSuppressed(CurrentCustomer, CurrentEmployer);
            return workflow;
        }

        /// <summary>
        /// Allows a user to copy an existing workflow
        /// </summary>
        /// <param name="code"></param>
        /// <param name="newCode"></param>
        /// <returns></returns>
        public Workflow Copy(Workflow workflowToCopy, string newCode, string newName, string newDescription, string employerId)
        {
            if (workflowToCopy == null)
                throw new ArgumentNullException("workflowToCopy");

            workflowToCopy.Id = null;
            workflowToCopy.Code = newCode;
            workflowToCopy.Name = newName;
            workflowToCopy.Description = newDescription;
            workflowToCopy.EmployerId = employerId;

            return Update(workflowToCopy);
        }

        #endregion

        /// <summary>
        /// Raises the event.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e">The e.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="System.ArgumentNullException">
        /// e
        /// or
        /// context
        /// </exception>
        public void RaiseEvent<T>(Event e, RuleEvaluatorContext<T> context) where T : class, new()
        {
            if (e == null)
                throw new ArgumentNullException("e");
            if (context == null)
                throw new ArgumentNullException("context");

            using (new InstrumentationContext("WorkflowService.RaiseEvent<T>"))
            {
                if (e.IsNew) e.Save();
                context.Metadata = context.Metadata.Merge(e.Metadata);
                context.Event = context.Event ?? e;
                var workflows = GetWorkflowsByEventType<T>(e, context);
                if (workflows == null || !workflows.Any())
                    return;

                foreach (var wf in workflows.OrderByDescending(wf => wf.Order.HasValue).ThenBy(wf => wf.Order))
                {
                    WorkflowInstance instance = null;
                    if (!WorkflowCriteriaPasses(wf, context))
                        continue;

                    try
                    {
                        instance = CreateInstance(wf, e);
                        if (instance != null)
                        {
                            instance.Save();
                            Run(instance);
                            UpdateContextFromInstance(context, instance);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error running workflow instance", ex);
                        if (instance != null)
                        {
                            instance.Status = WorkflowInstanceStatus.Failed;
                            instance.Save();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks if workflow criteria passes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="wf"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool WorkflowCriteriaPasses<T>(Workflow wf, RuleEvaluatorContext<T> context) where T : class, new()
        {
            using (var ruleService = new RuleService<RuleEvaluatorContext<T>>())
            {
                var ruleGroup = new RuleGroup()
                {
                    SuccessType = wf.CriteriaSuccessType,
                    Rules = wf.Criteria
                };

                return ruleService.EvaluateRuleGroup(ruleGroup, context);
            }
        }

        /// <summary>
        /// Updates the case and employee as they might have changed in the running of the instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="instance"></param>
        private void UpdateContextFromInstance<T>(RuleEvaluatorContext<T> context, WorkflowInstance instance) where T : class, new()
        {
            context.Case = instance.Case;
            context.Employee = instance.Employee;
        }

        /// <summary>
        /// Gets all workflows that need to respond to a specific event type
        /// Also runs any configured rules for the event context (employee, employer, case, etc.) and only returns passed workflows
        /// </summary>
        /// <typeparam name="T">The context type to run rules against.</typeparam>
        /// <param name="e">The event which triggered the workflow.</param>
        /// <param name="context">The context of type <typeparamref name="T"/> to evaluate any conditions for the event.</param>
        /// <returns>A list of workflows that meet the event and condition criteria for the given event context.</returns>
        public List<Workflow> GetWorkflowsByEventType<T>(Event e, RuleEvaluatorContext<T> context) where T : class, new()
        {
            using (new InstrumentationContext("WorkflowService.GetWorkflowsByEventType"))
            {
                // Start by getting the workflows that are either not customized or belong to this customer
                var workflowQuery = Workflow.Query.And(
                    // must belong to this event type
                    Workflow.Query.EQ(w => w.TargetEventType, e.EventType),
                    // Must not be a default one surpressed by this customer
                    Workflow.Query.NotInIgnoreCase(w => w.Code, e.Customer.SuppressedEntities.Where(p => p.EntityName == "Workflow").Select(p => p.EntityCode))
                );

                return Workflow.DistinctFind(workflowQuery, CustomerId, EmployerId)
                    .Where(p => !p.EffectiveDate.HasValue || DateTime.Now >= p.EffectiveDate.Value).ToList();
            }
        }

        public Workflow GetByCode(string code, string customerId = null, string employerId = null, bool includeSuppressed = false)
        {
            return Workflow.GetByCode(code, customerId, employerId, includeSuppressed);
        }

        /// <summary>
        /// Creates an instance of the given workflow based on the event that triggered it and context/scope
        /// of that event.
        /// </summary>
        /// <param name="wf">The workflow to create an instance of.</param>
        /// <param name="e">The event which triggered the workflow.</param>
        /// <param name="state">The starting state to set into the workflow instance.</param>
        /// <returns>
        /// An instance of a workflow given the current event and it's context.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wf
        /// or
        /// e
        /// </exception>
        public WorkflowInstance CreateInstance(Workflow wf, Event e, DynamicAwesome state = null)
        {
            if (wf == null)
                throw new ArgumentNullException("wf");
            if (e == null)
                throw new ArgumentNullException("e");

            WorkflowInstance instance = null;
            if (!IsWorkflowDisabledInCurrentContext(wf.Code))
            {
                using (new InstrumentationContext("WorkflowService.CreateInstance"))
                {
                    string createdById = e.CreatedById ?? CurrentUser.Id;
                    string customerId = e.CustomerId ?? wf.CustomerId ?? CurrentUser.CustomerId;
                    string employerId = e.EmployerId ?? wf.EmployerId ?? Employer.CurrentId;

                    instance = new WorkflowInstance()
                    {
                        CustomerId = customerId,
                        EmployerId = employerId,
                        WorkflowId = wf.Id,
                        Workflow = wf,
                        WorkflowName = wf.Name,
                        EventId = e.Id,
                        Event = e,
                        EventType = e.EventType,
                        EmployeeId = e.EmployeeId,
                        CaseId = e.CaseId,
                        Messages = new List<string>(),
                        State = state ?? new DynamicAwesome(),
                        Status = WorkflowInstanceStatus.Pending,
                        CreatedById = createdById,
                        ModifiedById = createdById
                    };

                    instance.Metadata.Merge(e.Metadata).Merge(instance.State.AsBsonDocument);
                    instance.State.Apply(e.Metadata);
                }
            }
            return instance;
        }

        /// <summary>
        /// Runs the specified instance; handling the activity coordination, transitions and state.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        /// <exception cref="AbsenceSoftException">The workflow instance is not in a state where it can be run. It must be pending.</exception>
        public void Run(WorkflowInstance instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");
            if (instance.Status != WorkflowInstanceStatus.Pending)
                throw new AbsenceSoftException("The workflow instance is not in a state where it can be run. It must be pending.");

            using (new InstrumentationContext("WorkflowService.Run"))
            {
                instance.Status = WorkflowInstanceStatus.Running;
                instance.Save();
                Continue(instance);
            }
        }

        /// <summary>
        /// Determines whether the specified instance is complete.
        /// </summary>
        /// <param name="instance">The workflow instance to check for completion.</param>
        /// <returns><c>true</c> if the instance is complete, otherwise <c>false</c>.</returns>
        private bool IsComplete(WorkflowInstance instance)
        {
            if (instance.Status == WorkflowInstanceStatus.Completed)
                return true;
            if (instance.Status == WorkflowInstanceStatus.Canceled)
                return true;
            if (instance.Status == WorkflowInstanceStatus.Failed)
                return true;

            var history = ActivityHistory.AsQueryable().Where(h => h.WorkflowInstanceId == instance.Id).ToList();
            if (history.Any(h => h.IsWaitingOnUserInput && string.IsNullOrWhiteSpace(h.Outcome)))
                return false;
            if (history.Any(h => string.IsNullOrWhiteSpace(h.Outcome)))
                return false;
            if (history.Any(h =>
            {
                // Get which activities would be next
                var next = GetNextActivities(instance, h);
                // If there are no more for this, then we're done with this history branch
                if (next == null || !next.Any())
                    return false;
                // If there are some, however all of them are already in our history
                //  then we're done with this history branch as well
                if (!next.Any(n => !history.Any(a => a.WorkflowActivityId == n.Id)))
                    return false;
                return true;
            }))
                return false;

            // Well, we've eliminated any possability that we're not done, so we are 
            //  done and that is our assumption that this workflow is really complete.
            return true;
        }

        /// <summary>
        /// Marks the workflow instance as complete, saves it and returns the saves instance.
        /// </summary>
        /// <param name="instance">The workflow instance to mark complete.</param>
        /// <returns>The saved workflow instance marked as complete.</returns>
        private WorkflowInstance MarkComplete(WorkflowInstance instance)
        {
            if (instance.Status == WorkflowInstanceStatus.Completed)
                return instance;
            if (instance.Status == WorkflowInstanceStatus.Canceled)
                return instance;
            if (instance.Status == WorkflowInstanceStatus.Failed)
                return instance;

            instance.Status = WorkflowInstanceStatus.Completed;
            return instance.Save();
        }

        /// <summary>
        /// Continues the specified instance optionally starting from the last history item.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="lastHistory">The last history.</param>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        private void Continue(WorkflowInstance instance, ActivityHistory lastHistory = null)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            using (new InstrumentationContext("WorkflowService.Continue"))
            {
                try
                {
                    var activities = GetNextActivities(instance, lastHistory);
                    // If we have no new activities from this one, then we need to check to see if this 
                    //  workflow instance is complete, and if so, mark it as such
                    if (activities == null || !activities.Any())
                        if (IsComplete(instance))
                            MarkComplete(instance);

                    // Define our conditional, recursive lambda actions (yay recursion)
                    Action<List<WorkflowActivity>> runAll = null;
                    Action<WorkflowActivity> runMe = null;

                    // Now assignn these self-calling recursive lambda expressions
                    runAll = new Action<List<WorkflowActivity>>(listOfActivities =>
                    {
                        // Nothing to do, check if complete, just return
                        if (listOfActivities == null || !listOfActivities.Any())
                        {
                            if (IsComplete(instance))
                                MarkComplete(instance);
                            return;
                        }

                        // Loop through and run each of the activities, NOT in parallel so there aren't
                        //  conflicts in referencing the same entities/records, etc. 
                        foreach (var activity in listOfActivities.Where(a => a != null).OrderByDescending(x=> GetActivity(x.ActivityId).IsRunFirst))
                            runMe(activity);

                        //// If there's just one, just run that one
                        //if (listOfActivities.Count == 1)
                        //    runMe(listOfActivities.First());
                        //else
                        //    // 'cause why not right... parallel 'n' stuff
                        //    listOfActivities.AsParallel().ForAll(activity => runMe(activity));
                    });
                    // This runs a specific activity and handles the activity history output.
                    runMe = new Action<WorkflowActivity>(activity =>
                    {
                        ActivityHistory history = null;
                        try
                        {
                            history = RunActivity(instance, activity);
                            history.Save();
                            if (!history.IsWaitingOnUserInput)
                                runAll(GetNextActivities(instance, history));
                        }
                        catch (Exception ex)
                        {
                            if (history != null)
                            {
                                history.Outcome = Activity.ErrorOutcomeValue;
                                history.Error = ex.Message;
                                history.IsWaitingOnUserInput = false;
                                history.Save();
                                // There could be an ERROR outcome path on the activity
                                runAll(GetNextActivities(instance, history));
                            }
                            Log.Error("Error running workflow activity", ex);
                        }
                    });
                    runAll(activities);
                }
                catch (Exception ex)
                {
                    instance.Status = WorkflowInstanceStatus.Failed;
                    instance.AddError(ex);
                    instance.State.Add("Error", ex.Message);
                    instance.State.Add("Exception", ex.ToString());
                    instance.Save();
                    Log.Error(string.Format("Error continuing workflow instance, {0}", instance.Id), ex);
                }
            }
        }

        /// <summary>
        /// Gets the next set of activities for the current workflow instance, if any, based on the last activity completed
        /// either <paramref name="lastActivity" /> passed in or from the instance history data store if
        /// <paramref name="lastActivity" /> is <c>null</c>.
        /// </summary>
        /// <param name="instance">The currently running instance of a workflow to get the next activity for.</param>
        /// <param name="lastActivity">The last activity that was completed, if any, otherwise <c>null</c> or missing.</param>
        /// <returns>
        /// The next workflow activity to perform or <c>null</c> if there is nothing else to do.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        /// <exception cref="System.ArgumentException">The workflow instance is not currently running;instance</exception>
        public List<WorkflowActivity> GetNextActivities(WorkflowInstance instance, ActivityHistory lastActivity = null)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");
            if (instance.Status != WorkflowInstanceStatus.Running)
                throw new ArgumentException("The workflow instance is not currently running", "instance");

            using (new InstrumentationContext("WorkflowService.GetNextActivity"))
            {
                var transitions = GetNextTransitions(instance, lastActivity);
                if ((transitions == null || !transitions.Any()) && lastActivity != null)
                    return new List<WorkflowActivity>(0);

                if (lastActivity == null)
                {
                    List<Guid> targets = instance.Workflow.Transitions.Where(t => t.SourceActivityId.HasValue).Select(t => t.TargetActivityId).ToList();
                    return instance.Workflow.Activities.Where(a => !targets.Contains(a.Id)).ToList();
                }

                return instance.Workflow.Activities.Where(a => transitions.Any(t => t.TargetActivityId == a.Id)).ToList();
            }
        }

        /// <summary>
        /// Gets the next transitions for the current workflow instance, if any, based on the last activity completed
        /// either <paramref name="lastActivity" /> passed in or from the instance history data store if
        /// <paramref name="lastActivity" /> is <c>null</c>.
        /// </summary>
        /// <param name="instance">The currently running instance of a workflow to get the next transitions for.</param>
        /// <param name="lastActivity">The last activity that was completed, if any, otherwise <c>null</c> or missing.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        /// <exception cref="System.ArgumentException">The workflow instance is not currently running;instance</exception>
        protected List<Transition> GetNextTransitions(WorkflowInstance instance, ActivityHistory lastActivity = null)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");
            if (instance.Status != WorkflowInstanceStatus.Running)
                throw new ArgumentException("The workflow instance is not currently running", "instance");

            using (new InstrumentationContext("WorkflowService.GetNextActivity"))
            {
                if (lastActivity == null)
                    lastActivity = ActivityHistory.AsQueryable().Where(h => h.WorkflowInstanceId == instance.Id).OrderByDescending(h => h.ModifiedDate).FirstOrDefault();
                if (lastActivity == null)
                {
                    var tranny = instance.Workflow.Transitions.FirstOrDefault(t => t.IsStart);
                    var tList = new List<Transition>(1);
                    tList.AddFluidIf(tranny, () => tranny != null);
                    return tList;
                }

                // By nature of the outcome model, if no outcome has been reached, we'll return an empty list.
                return instance.Workflow.Transitions.Where(t => t.SourceActivityId == lastActivity.WorkflowActivityId && t.ForOutcome == lastActivity.Outcome).ToList();
            }
        }

        /// <summary>
        /// Runs the activity for the given workflow instance.
        /// </summary>
        /// <param name="instance">The currently running instance of a workflow to run this activity for.</param>
        /// <param name="activity">The workflow activity that should be run.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">instance is null</exception>
        /// <exception cref="System.ArgumentException">The workflow instance is not currently running;instance</exception>
        /// <exception cref="AbsenceSoftException">Unable to locate the implementation for this workflow activity with activity id</exception>
        public ActivityHistory RunActivity(WorkflowInstance instance, WorkflowActivity activity)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");
            if (instance.Status != WorkflowInstanceStatus.Running)
                throw new ArgumentException("The workflow instance is not currently running", "instance");

            using (new InstrumentationContext("WorkflowService.RunActivity"))
            {
                /// Get the actual activity to run
                Activity a = GetActivity(activity.ActivityId);
                if (a == null)
                    throw new AbsenceSoftException(string.Format("Unable to locate the implementation for this workflow activity with activity id \"{0}\".", activity.ActivityId));

                try
                {
                    if (a.IsValidToRun(instance, activity, CurrentUser))
                        return a.Run(instance, activity, CurrentUser);

                    return a.Error(instance, activity, string.Format("Activity {0} is not in a valid state to run", activity.Name), CurrentUser);
                }
                catch (Exception ex)
                {
                    return a.Error(instance, activity, ex.Message, CurrentUser);
                }
            }
        }

        /// <summary>
        /// Completes the activity (generally after some user input).
        /// </summary>
        /// <param name="instance">The currently running instance of a workflow to complete this activity for.</param>
        /// <param name="activityHistory">The activity history to complete.</param>
        /// <returns>
        /// The completed activity history record.
        /// </returns>
        public void CompleteActivity(WorkflowInstance instance, ActivityHistory activityHistory)
        {
            var activity = GetActivity(activityHistory.ActivityId);
            if (activity == null)
                Continue(instance);
            else
            {
                var history = activity.Complete(activityHistory, instance, CurrentUser);
                history.Save();
                Continue(instance, history);
            }
        }

        /// <summary>
        /// Pauses the specified instance
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public WorkflowInstance Pause(WorkflowInstance instance)
        {
            instance.Status = WorkflowInstanceStatus.Paused;
            return instance;
        }

        /// <summary>
        /// Resumes the specified instance
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public WorkflowInstance Resume(WorkflowInstance instance)
        {
            instance.Status = WorkflowInstanceStatus.Running;
            return instance;
        }

        /// <summary>
        /// Cancels the specified instance
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public WorkflowInstance Cancel(WorkflowInstance instance)
        {
            instance.Status = WorkflowInstanceStatus.Canceled;
            return instance;
        }

        /// <summary>
        /// Restarts the specified instance
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public WorkflowInstance Restart(WorkflowInstance instance)
        {
            instance.Status = WorkflowInstanceStatus.Canceled;
            instance.Save();
            instance.Id = null;
            instance.Status = WorkflowInstanceStatus.Pending;
            instance.Save();
            Run(instance);
            return instance;
        }

        /// <summary>
        /// Gets all the workflows for specified customer. Performs necessary security checks
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">
        /// Current user does not have permission to get workflows for this customer
        /// or
        /// Current user does not have permission to get workflows for this employer
        /// </exception>
        public ListResults WorkflowList(ListCriteria criteria)
        {
            using (new InstrumentationContext("WorkflowService.WorkflowList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);

                string name = criteria.Get<string>("Name");
                string code = criteria.Get<string>("Code");
                string caseEvent = criteria.Get<string>("Event");

                List<IMongoQuery> ands = Workflow.DistinctAnds(CurrentUser, CustomerId, EmployerId);
                Workflow.Query.MatchesString(w => w.Name, name, ands);
                Workflow.Query.MatchesString(w => w.Code, code, ands);
                if (!string.IsNullOrEmpty(caseEvent))
                {
                    EventType caseEventId = EventType.CaseCreated;
                    if (EventType.TryParse(caseEvent, out caseEventId))
                    {
                        ands.Add(Workflow.Query.EQ(e => e.TargetEventType, caseEventId));
                    };
                }

                List<Workflow> workflow = Workflow.DistinctAggregation(ands);
                results.Total = workflow.Count;

                workflow = workflow
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();

                results.Results = workflow.Select(w => new ListResult()
                    .Set("Id", w.Id)
                    .Set("CustomerId", w.CustomerId)
                    .Set("EmployerId", w.EmployerId)
                    .Set("Code", w.Code)
                    .Set("Name", w.Name)
                    .Set("Description", w.Description)
                    .Set("EventTypeString", w.EventTypeString)
                    .Set("EventType", w.TargetEventType)
                    .Set("IsCustom", w.IsCustom)
                    .Set("ModifiedDate", w.ModifiedDate)
                    .Set("Suppressed", IsWorkflowDisabledInCurrentContext(w.Code))
                    ).ToList();

                return results;
            }
        }

        /// <summary>
        /// Gets the workflow by id and populates the IsDisabled property.  Also performs necessary security checks
        /// </summary>
        /// <param name="workflowId">The workflow identifier.</param>
        /// <returns></returns>
        public Workflow GetWorkflowById(string workflowId)
        {
            Workflow wf = Workflow.GetById(workflowId);
            if (wf != null)
            {
               wf.IsDisabled = wf.Suppressed = IsWorkflowDisabledInCurrentContext(wf.Code);
            }
            return wf;
        }

        /// <summary>
        /// Lists the workflow instances.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults ListWorkflowInstances(ListCriteria criteria)
        {
            using (new InstrumentationContext("WorkflowService.GetCaseWorkflows"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults result = new ListResults(criteria);
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());
                string caseId = criteria.Get<string>("CaseId");
                string workflowName = criteria.Get<string>("WorkflowName");
                long? workflowStatus = criteria.Get<long?>("Status");
                long? workflowEventType = criteria.Get<long?>("EventType");
                long? lastModifiedDate = criteria.Get<long?>("ModifiedDate");

                if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(WorkflowInstance.Query.EQ(e => e.CaseId, caseId));

                if (!string.IsNullOrEmpty(workflowName))
                    ands.Add(WorkflowInstance.Query.Matches(e => e.WorkflowName, new BsonRegularExpression(workflowName, "i")));

                if (workflowStatus.HasValue)
                {
                    WorkflowInstanceStatus status = (WorkflowInstanceStatus)workflowStatus;
                    ands.Add(WorkflowInstance.Query.EQ(e => e.Status, status));
                }

                if (workflowEventType.HasValue)
                {
                    EventType eventType = (EventType)workflowEventType;
                    ands.Add(WorkflowInstance.Query.EQ(e => e.EventType, eventType));
                }

                if (lastModifiedDate.HasValue)
                {
                    ands.Add(WorkflowInstance.Query.GTE(e => e.ModifiedDate, new BsonDateTime(lastModifiedDate.Value)));
                    ands.Add(WorkflowInstance.Query.LT(e => e.ModifiedDate, new BsonDateTime(lastModifiedDate.Value + 86400000)));
                }

                var query = WorkflowInstance.Query.Find(ands.Count > 0 ? WorkflowInstance.Query.And(ands) : null)
                    .SetFields(WorkflowInstance.Query.IncludeFields(
                        w => w.Id,
                        w => w.CaseId,
                        w => w.WorkflowId,
                        w => w.WorkflowName,
                        w => w.ModifiedDate,
                        w => w.Status,
                        w => w.EventType
                    ));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                        ? SortBy.Ascending(criteria.SortBy)
                        : SortBy.Descending(criteria.SortBy));
                }

                int skip = Math.Max(((criteria.PageNumber - 1) * criteria.PageSize), 0);

                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                result.Total = query.Count();
                result.Results = query.ToList().Select(wf => new ListResult()
                    .Set("Id", wf.Id)
                    .Set("CaseId", wf.CaseId)
                    .Set("CaseNumber", wf.Case == null ? "" : wf.Case.CaseNumber)
                    .Set("WorkflowId", wf.WorkflowId)
                    .Set("WorkflowName", wf.WorkflowName)
                    .Set("ModifiedDate", wf.ModifiedDate)
                    .Set("Status", wf.Status.ToString())
                    .Set("EventType", wf.EventType.ToString().SplitCamelCaseString())
                ).ToList();

                return result;
            }
        }

        /// <summary>
        /// Checks if the Workflow is disabled in current context
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool IsWorkflowDisabledInCurrentContext(string code)
        {
            bool returnValue = false;
            if (CurrentEmployer != null)
            {
                returnValue = CurrentEmployer.SuppressWorkflows.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            if (CurrentCustomer != null && !returnValue)
            {
                returnValue = CurrentCustomer.SuppressWorkflows.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            return returnValue;
        }

        /// <summary>
        /// Gets the workflow instance by identifier.
        /// </summary>
        /// <param name="workflowInstanceId">The workflow instance identifier.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">
        /// Unable to retrieved specified workflow
        /// or
        /// Specified workflow does not belong to current customer
        /// or
        /// Current user does not have permission to access specified workflow
        /// </exception>
        public WorkflowInstance GetWorkflowInstanceById(string workflowInstanceId)
        {
            using (new InstrumentationContext("WorkflowService.GetWorkflowInstanceById"))
            {
                WorkflowInstance wfi = WorkflowInstance.GetById(workflowInstanceId);
                if (wfi == null)
                    throw new AbsenceSoftException("Unable to retrieved specified workflow");

                if (wfi.CustomerId != CurrentUser.CustomerId)
                    throw new AbsenceSoftException("Specified workflow does not belong to current customer");

                if (!CurrentUser.HasEmployerAccess(wfi.EmployerId))
                    throw new AbsenceSoftException("Current user does not have permission to access specified workflow");

                return wfi;
            }
        }

        /// <summary>
        /// Updates the workflow instance.
        /// </summary>
        /// <param name="wfi">The wfi.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">User does not have permission to edit workflows for this employer.</exception>
        public WorkflowInstance UpdateWorkflowInstance(WorkflowInstance wfi)
        {
            using (new InstrumentationContext("WorkflowService.UpdateWorkflowInstance"))
            {
                if (!CurrentUser.HasEmployerAccess(wfi.EmployerId))
                    throw new AbsenceSoftException("User does not have permission to edit workflows for this employer.");

                return wfi.Save();
            }
        }


        /// <summary>
        /// Runs the workflow instance for given workflow code, actiivityID and case Id
        /// </summary>
        /// <param name="workflowCode">The workflow code</param>
        /// <param name="caseId">The case identifier</param>
        /// <param name="activityId">An optional activity identifier</param> 
        /// <exception cref="AbsenceSoftException">Unable to find workflow having code</exception>
        /// <exception cref="AbsenceSoftException">Activity Id is not in a proper format, please supply a valid GUID format</exception>
        /// <exception cref="AbsenceSoftException">Error in Workflow run for case</exception>
        public void Run(string workflowCode, string caseId, Guid? activityId)
        {

            var workflow = Workflow.GetByCode(workflowCode, CustomerId, EmployerId);
            if (workflow == null)
            {
                throw new AbsenceSoftException(string.Format("Unable to find workflow having code \"{0}\".", workflowCode));
            }
            using (var manualWorkflowService = new ManualWorkflowService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                Event e = manualWorkflowService.BuildEvent(workflow.Id, null, caseId);
                WorkflowInstance instance = CreateInstance(workflow, e);
                if (instance != null)
                {
                    if (activityId == null)
                    {
                        Run(instance);
                    }
                    else
                    {
                        WorkflowActivity wfActivity = workflow.Activities.FirstOrDefault(a => a.Id == activityId);
                        if (wfActivity == null)
                        {
                            throw new AbsenceSoftException(string.Format("The Activity with ID \"{0}\" was not found within the workflow, \"{1}\" or does not exist, or was invalid.", activityId, workflowCode));
                        }
                        instance.Status = WorkflowInstanceStatus.Running;
                        instance.Save();
                        ActivityHistory history = RunActivity(instance, wfActivity);
                        history.Save();
                        if (!history.IsWaitingOnUserInput)
                        {
                            Continue(instance, history);
                        }
                    }
                }
            }

        }

        public Workflow ToggleWorkflowByCode(string code)
        {
            using (new InstrumentationContext("WorkflowService.ToggleWorkflowByCode"))
            {
                Workflow workflow = Workflow.GetByCode(code, CustomerId, EmployerId, true);
                if (CurrentEmployer != null)
                {
                    return ToggleWorkflowForEmployer(workflow);
                }
                if (CurrentCustomer != null)
                {
                    return ToggleWorkflowForCustomer(workflow);
                }
                /// This should not be reachable, as we should always have a customer or employer
                /// But just in case it is, we're going to return the absence reason as is
                return workflow;
            }
        }

        private Workflow ToggleWorkflowForEmployer(Workflow workflow)
        {
            if (CurrentEmployer.SuppressWorkflows.Contains(workflow.Code))
            {
                CurrentEmployer.SuppressWorkflows.RemoveAll(w => w == workflow.Code);
            }
            else
            {
                CurrentEmployer.SuppressWorkflows.Add(workflow.Code);
            }

            CurrentEmployer.Save();
            return workflow;
        }

        private Workflow ToggleWorkflowForCustomer(Workflow workflow)
        {
            if (CurrentCustomer.SuppressWorkflows.Contains(workflow.Code, StringComparer.InvariantCultureIgnoreCase))
            {
                CurrentCustomer.SuppressWorkflows.RemoveAll(w => string.Equals(w, workflow.Code, StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                CurrentCustomer.SuppressWorkflows.Add(workflow.Code.ToUpperInvariant());
            }

            CurrentCustomer.Save();

            return workflow;
        }

     
    
       
    }
}
