﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {

        private static void WfOnAccommodationEvent(Case theCase, Accommodation accomm, EventType eventType, User user = null)
        {
            Event e = StageCaseEvent(eventType, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            CopyAccommodationMetadata(accomm, e);
            e.Save();
            RaiseAccommodationEvent(theCase, accomm, e, user);
        }

        private static void CopyAccommodationMetadata(Accommodation accomm, Event e)
        {
            e.Metadata.SetRawValue("AccommodationId", accomm.Id);
            e.Metadata.SetRawValue("ActiveAccommodationId", accomm.Id);
            e.Metadata.SetRawValue("AccommodationTypeId", accomm.Type.Id);
            e.Metadata.SetRawValue("AccommodationTypeCode", accomm.Type.Code);
            e.Metadata.SetRawValue("StartDate", accomm.StartDate);
            e.Metadata.SetRawValue("EndDate", accomm.EndDate);
        }

        private static void RaiseAccommodationEvent(Case theCase, Accommodation accomm, Event e, User user = null)
        {
            var context = BuildContext<Accommodation>(theCase, accomm);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Accommodation>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on accommodation created.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accomm">The accomm.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationCreated(this Case theCase, Accommodation accomm, User user = null)
        {
            WfOnAccommodationEvent(theCase, accomm, EventType.AccommodationCreated, user);
            WfOnAccommodationEvent(theCase, accomm, EventType.AccommodationChanged, user);
        }
        /// <summary>
        /// WORKFLOW: Called on accommodation created.
        /// </summary>
        /// <param name="accomm">The accomm.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationCreated(this Accommodation accomm, Case theCase, User user = null) { theCase.WfOnAccommodationCreated(accomm, user); }

        /// <summary>
        /// WORKFLOW: Called on accommodation changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accomm">The accomm.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationChanged(this Case theCase, Accommodation accomm, User user = null)
        {
            WfOnAccommodationEvent(theCase, accomm, EventType.AccommodationChanged, user);
        }
        /// <summary>
        /// WORKFLOW: Called on accommodation changed.
        /// </summary>
        /// <param name="accomm">The accomm.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationChanged(this Accommodation accomm, Case theCase, User user = null) { theCase.WfOnAccommodationChanged(accomm, user); }

        /// <summary>
        /// WORKFLOW: Called on accommodation deleted.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accomm">The accomm.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationDeleted(this Case theCase, Accommodation accomm, User user = null)
        {
            WfOnAccommodationEvent(theCase, accomm, EventType.AccommodationDeleted, user);
        }
        /// <summary>
        /// WORKFLOW: Called on accommodation deleted.
        /// </summary>
        /// <param name="accomm">The accomm.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationDeleted(this Accommodation accomm, Case theCase, User user = null) { theCase.WfOnAccommodationDeleted(accomm, user); }

        /// <summary>
        /// WORKFLOW: Called on accommodation adjudicated.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="accomm">The accomm.</param>
        /// <param name="status">The status.</param>
        /// <param name="reasonCode">The reason code.</param>
        /// <param name="denialReasonOther">The denial reason other.</param>
        /// <param name="reasonName">The reason name.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationAdjudicated(this Case theCase, Accommodation accomm, AdjudicationStatus status, string reasonCode, string denialReasonOther, string reasonName, User user = null)
        {
            Event e = StageCaseEvent(EventType.AccommodationAdjudicated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            CopyAccommodationMetadata(accomm, e);
            e.Metadata.SetRawValue("Determination", status);
            e.Metadata.SetRawValue("DenialReason", reasonCode);
            e.Metadata.SetRawValue("DenialReasonName", reasonName);
            e.Metadata.SetRawValue("DenialExplanation", denialReasonOther);
            e.Save();
            RaiseAccommodationEvent(theCase, accomm, e);
        }
        /// <summary>
        /// WORKFLOW: Called on accommodation adjudicated.
        /// </summary>
        /// <param name="accomm">The accomm.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="status">The status.</param>
        /// <param name="reason">The reason.</param>
        /// <param name="denialReasonOther">The denial reason other.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAccommodationAdjudicated(this Accommodation accomm, Case theCase, AdjudicationStatus status, string reason, string denialReasonOther, string reasonName, User user = null)
        { theCase.WfOnAccommodationAdjudicated(accomm, status, reason, denialReasonOther, reasonName, user); }
    }
}
