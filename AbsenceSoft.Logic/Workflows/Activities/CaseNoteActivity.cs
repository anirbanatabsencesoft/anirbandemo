﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    [Serializable]
    public sealed class CaseNoteActivity : Activity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "CaseNoteActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "Case Note"; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "Case"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates a case note based on a note template or static notes."; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.Action; } }

        /// <summary>
        /// Gets the possible event types for a CaseNoteActivity
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            return base.GetCaseEventTypes();
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            if (user == null || wf.State == null || activity.Metadata == null)
                return false;

            if (string.IsNullOrWhiteSpace(wf.CaseId))
                return false;

            // Category and Public are optional
            if (!activity.MetadataHasProperties("Template"/*, "Category", "Public"*/))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            dynamic state = wf.State;
            bool isSpouse = activity.Metadata.GetRawValue<bool?>("Spouse") == true;

            if (isSpouse && (wf.Case == null || wf.Case.SpouseCase == null))
                return Complete(RunEnd(wf, activity, user, CompleteOutcomeValue, 
                    error: "Unable to add a case note to spouse case, related spouse case not found or not applicable."), wf, user);

            CaseNote note = new CaseNote()
            {
                CaseId = isSpouse ? wf.Case.SpouseCaseId : wf.CaseId,
                Notes = activity.Metadata.GetRawValue<string>("Template"),
                Public = !activity.Metadata.GetRawValue<bool?>("Confidential") ?? true,
                Category = activity.Metadata.GetRawValue<NoteCategoryEnum?>("Category"),
                EmployerId = wf.EmployerId,
                CustomerId = wf.CustomerId,
                CreatedById = user.Id,
                ModifiedById = user.Id,
                EnteredByName = (user ?? User.Current ?? User.System).DisplayName
            };

            if (note.Notes != null && note.Notes.Contains("{{"))
            {
                // Need to tokenize this thing
                try
                {
                    var scope = wf.BuildContext();
                    note.Notes = Rendering.Template.RenderTemplate(note.Notes, scope);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Workflow instance '{0}', for activity '{1}', failed to tokenize notes: \"{2}\".", wf.Id, activity.Name, note.Notes), ex);
                }
            }

            note.Save();
            
            ActivityHistory end = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(end, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            /// Noting to do at the moment
            return activity;
        }
    }
}
