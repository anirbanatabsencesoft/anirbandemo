﻿using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StartWorkflowActivity : Activity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "StartWorkflowActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Start Workflow"; }
        }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category
        {
            get { return "Control"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Starts another configured workflow, passing in the state from the currently running workflow"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.Action; }
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            if (wf.State == null || activity.Metadata == null || user == null)
                return false;

            if (!activity.MetadataHasProperties("WorkflowCode"))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            using (var svc = new WorkflowService(user))
            {
                string workflowCode = activity.Metadata.GetRawValue<string>("WorkflowCode");

                Workflow workflow = Workflow.GetByCode(workflowCode, wf.CustomerId, wf.EmployerId);

                /// Still didn't find it?  They need to fix the workflow 
                if (workflow == null)
                    return base.RunEnd(wf, activity, user, ErrorOutcomeValue, "Unable to find the specified workflow");

                /// Create a manual event type
                Event e = new Event() {
                    EventType = EventType.Manual,
                    CustomerId = wf.CustomerId,
                    EmployerId = wf.EmployerId,
                    EmployeeId = wf.EmployeeId,
                    CaseId = wf.CaseId,
                    IsSelfService = wf.Event.IsSelfService,
                    Name = activity.Name
                }.Save();
                
                WorkflowInstance newInstance = svc.CreateInstance(workflow, e, wf.State.Clone());
                if (newInstance != null)
                {
                    newInstance.State.Add("SourceWorkflowInstanceId", wf.Id);
                    newInstance.State.Add("SourceWorkflowActivityId", activity.Id);
                    newInstance.State.Add("SourceWorkflowActivityActivityId", activity.ActivityId);
                    newInstance.Save();
                    svc.Run(newInstance);
                    wf.State["WorkflowInstanceId"] = newInstance.Id;
                }
            }
            
            ActivityHistory ah = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(ah, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            return activity;
        }
    }
}
