﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Rules;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    [Serializable]
    public sealed class ConditionActivity : Activity
    {
        /// <summary>
        /// The yes outcome value
        /// </summary>
        public const string YesOutcomeValue = "YES";
        /// <summary>
        /// The no outcome value
        /// </summary>
        public const string NoOutcomeValue = "NO";

        public const string NAOutcomeValue = "NA";

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ConditionActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Condition"; }
        }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category
        {
            get { return "Control"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Evaluates a rule group to be a yes or no response"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.Condition; }
        }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action. Default is only 2, Complete and ERROR.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return YesOutcomeValue;
            yield return NoOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        /// <summary>
        /// Gets the UI Template
        /// </summary>
        /// <value>
        /// The activities UI template
        /// </value>
        public override string Template
        {
            get { return "Question"; }
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            return activity.Rules != null;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            if (activity.Rules == null)
                return Error(wf, activity, "No rules defined for this condition", user);

            try
            {
                using (var svc = new RuleService<dynamic>())
                    svc.EvaluateRuleGroup(activity.Rules, wf.BuildContext());
            }
            catch (Exception ex)
            {
                var err = Error(wf, activity, ex.Message, user);
                err.Metadata.SetRawValue("Rules", activity.Rules.ToBsonDocument());
                return err;
            }

            var ah = RunEnd(wf, activity, user, activity.Rules.Pass ? YesOutcomeValue : NoOutcomeValue);
            ah.Metadata.SetRawValue("Rules", activity.Rules.ToBsonDocument());
            return Complete(ah, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            /// At the moment, nothing to do but to return
            return activity;
        }
    }
}
