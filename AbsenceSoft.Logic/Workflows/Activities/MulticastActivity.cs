﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    [Serializable]
    public sealed class MulticastActivity : Activity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "MulticastActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "Multicast"; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "Control"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Always returns Complete, allows breakout workflow and ease of maintenance."; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.None; } }

        /// <summary>
        /// Gets the possible event types for a MulticastActivity
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            return base.GetCaseEventTypes();
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// Always returns true.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            ActivityHistory end = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(end, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            /// Noting to do at the moment
            return activity;
        }
    }
}
