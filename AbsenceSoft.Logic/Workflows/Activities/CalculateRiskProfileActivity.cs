﻿using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.RiskProfiles;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    public class CalculateRiskProfileActivity : Activity
    {
        public override ActivityType ActivityType { get { return ActivityType.Action; } }

        public override string Category { get { return "Control"; } }

        public override string Description { get { return "Automatically calculates a Risk Profile for an employee and/or case"; } }

        public override string Id { get { return "CalculateRiskProfileActivity"; } }

        public override string Name { get { return "Calculate Risk Profile Activity";  } }

        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            if (user == null || wf.State == null || (string.IsNullOrEmpty(wf.EmployeeId) && string.IsNullOrEmpty(wf.CaseId)))
                return false;

            return true;
        }

        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            using (var riskProfileService = new RiskProfileService(wf.Customer, wf.Employer, user))
            {
                riskProfileService.CalculateAndSaveRiskProfile(wf.Employee, wf.Case);
            }

            ActivityHistory end = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            
            return Complete(end, wf, user);
        }

        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            return activity;
        }

        

        
    }
}
