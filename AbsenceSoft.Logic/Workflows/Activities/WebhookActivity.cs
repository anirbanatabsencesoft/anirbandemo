﻿using AbsenceSoft.Common.Extensions;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Customers;
using AT.Common.Log;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace AbsenceSoft.Logic.Workflows.Activities
{
    [Serializable]
    public sealed class WebhookActivity : Activity
    {
        ///// <summary>
        ///// The URL to which data has to be posted
        ///// </summary>
        //public string EndpointUrl { get; set; }

        ///// <summary>
        ///// The name of the event given by the consumer
        ///// </summary>
        //public string EventName { get; set; }

        ///// <summary>
        ///// A friendly description that can be provided for understanding
        ///// </summary>
        //public string FriendlyDescription { get; set; }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "WebhookActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "Webhook"; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "Case"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Sends data of a particular event to an external system over HTTP(s)"; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.Action; } }

        /// <summary>
        /// Gets the possible event types for activity
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            return base.GetCaseEventTypes();
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            Logger.Info("Webhook activity:Validation started for " + activity.Name);
            if (user == null || wf.State == null || activity.Metadata == null)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(wf.CaseId))
            {
                return false;
            }

            if (!activity.Metadata.TryGetValue("EndpointUrl", out var endpointUrl))
            {
                return false;
            }

            if (endpointUrl == null)
            {
                return false;
            }

            if (!endpointUrl.ToString().IsValidUrl())
            {
                return false;
            }
            Logger.Info("Webhook activity:Validation successed for " + activity.Name);
            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            Logger.Info("Webhook activity:run started " +activity.Name);
            var metadata = activity.Metadata;
            var endpointUrl = metadata.GetRawValue<string>("EndpointUrl");

            var employerAccess = user.Employers.Where(x => x.Employer.Id == wf.EmployerId);
            var customeHeaders = metadata.GetRawValue<string>("CustomHeaders");
            var headers = JsonConvert.DeserializeObject<Dictionary<string, string>>(customeHeaders);

            dynamic contextData = new
            {
                Description = metadata.GetRawValue<string>("FriendlyDescription"),
                Name = metadata.GetRawValue<string>("EventName"),
                SystemEvent = wf.Event.Name,
                wf.WorkflowName,
                WorkflowCode = wf.Workflow.Code,
                EmployerReferenceCode = employerAccess?.FirstOrDefault().Employer.ReferenceCode,
                wf.Employee?.EmployeeNumber,
                wf.Case.CaseNumber,
                UserEmail = user.Email,
                OriginalEventDate = wf.Event.CreatedDate
            };
            using (var httpClient = new HttpClient())
            {
                var jsonData = JsonConvert.SerializeObject(
                    contextData,
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });

                foreach (var item in headers)
                {
                    httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);

                }

                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                Logger.Info($"Webhook: posting data to the endpoint. Url: {endpointUrl}");
                // Need to call .GetAwaiter().GetResult() (vs. .Result) so we unwrap exceptions
                //  and avoid deadlocks, which are bad in a web context.
                // By the same token, we don't need the bloat of context switching for sync vs. async, so
                //  we need to configure await as false.
                var response = httpClient.PostAsync(endpointUrl, content).ConfigureAwait(false).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                {
                    return Error(wf, activity, "Error sending data to endpoint", user);
                }

                Logger.Info("Webhook activity: response is " + response.StatusCode);
            }

            Logger.Info($"Webhook activity successful for case '{wf.CaseId}'");

            ActivityHistory end = RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(end, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            /// Noting to do at the moment
            return activity;
        }
    }
}
