﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Communications;
using MongoDB.Bson;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ReviewCaseAttachmentActivity : BaseToDoActivity
    {
        /// <summary>
        /// The attachment reviewed outcome value
        /// </summary>
        public const string AttachmentReviewedOutcomeValue = "Attachment Reviewed";
        /// <summary>
        /// The attachment updated outcome value
        /// </summary>
        public const string AttachmentUpdatedOutcomeValue = "Attachment Updated";
        /// <summary>
        /// The attachment deleted outcome value
        /// </summary>
        public const string AttachmentDeletedOutcomeValue = "Attachment Deleted";


        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.ReviewCaseAttachment; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ReviewCaseAttachmentActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Review Case Attachment"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a ToDo to review a specific case attachment"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Gets a value indicating whether [automatic save].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic save]; otherwise, <c>false</c>.
        /// </value>
        public override bool AutoSave
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return AttachmentReviewedOutcomeValue;
            yield return AttachmentUpdatedOutcomeValue;
            yield return AttachmentDeletedOutcomeValue;
            yield return CanceledOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        /// <summary>
        /// Returns the event types for a Cases
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            yield return EventType.AttachmentCreated;
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            return base.IsValidToRun(wf, activity, user) && wf.StateHasProperties("AttachmentId");
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            dynamic wfState = wf.State;
            string attachmentId = wfState.AttachmentId;
            ToDoItem newAttachmentReview = CreateCaseToDo(wf, activity, user);
            newAttachmentReview.Metadata = new BsonDocument().Add("Attachment", Attachment.GetById(attachmentId).ToBsonDocument());
            newAttachmentReview.Save();
            var activityHistory = RunEnd(wf, activity, user);
            return AutoCompleteToDo(activityHistory, activity, newAttachmentReview, user);
        }
    }
}
