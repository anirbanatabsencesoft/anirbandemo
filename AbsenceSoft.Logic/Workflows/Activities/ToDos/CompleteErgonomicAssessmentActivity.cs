﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class CompleteErgonomicAssessmentActivity : BaseToDoActivity
    {
        public const string ErgonomicAssessmentCompletedOutcome = "ERGONOMIC ASSESSMENT COMPLETED";
        public const string ErgonomicAssessmentNotCompletedOutcome = "ERGONOMIC ASSESSMENT NOT COMPLETED";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.CompleteErgonomicAssessment; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "CompleteErgonomicAssessmentActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Complete Ergonomic Assessment Activity"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Complete Ergonomic Assessment Activity ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Returns the possible outcomes
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return ErgonomicAssessmentCompletedOutcome;
            yield return ErgonomicAssessmentNotCompletedOutcome;
            yield return ErrorOutcomeValue;
        }

        /// <summary>
        /// Is Valid To Run if we have a CaseId and an AccommodationId
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="activity"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            return base.IsValidToRun(wf, activity, user) && wf.StateHasProperties("AccommodationId");
        }
    }
}
