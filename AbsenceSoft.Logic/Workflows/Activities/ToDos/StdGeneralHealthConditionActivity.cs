﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    [FeatureRestriction(Feature.ShortTermDisability)]
    public sealed class StdGeneralHealthConditionActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType { get { return ToDoItemType.STDGenHealthCond; } }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "StdGeneralHealthConditionActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "STD General Health Condition"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates and handles an STD General Health Condition ToDo"; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.User; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "STD"; } }

        /// <summary>
        /// Completes the specified activity.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="wf">The wf.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            ToDoItem todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            if (!string.IsNullOrEmpty(todo.ResultText))
                activity.Outcome = todo.ResultText;
            else if (todo.Status == ToDoItemStatus.Complete || todo.Status == ToDoItemStatus.Cancelled)
                activity.Outcome = todo.Status == ToDoItemStatus.Complete ? CompleteOutcomeValue : CanceledOutcomeValue;

            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);

            if (!activity.IsWaitingOnUserInput 
                && todo.Status == ToDoItemStatus.Complete 
                && todo.Case != null 
                && todo.Case.Disability != null
                && wf.StateHasProperties("GeneralHealthCondition")
                && wf.DynamicState.GeneralHealthCondition != null
                && todo.Case.Disability.GeneralHealthCondition != wf.DynamicState.GeneralHealthCondition)
            {
                todo.Case.Disability.GeneralHealthCondition = wf.DynamicState.GeneralHealthCondition;
                todo.Case.Save();
            }

            return activity;
        }
    }
}
