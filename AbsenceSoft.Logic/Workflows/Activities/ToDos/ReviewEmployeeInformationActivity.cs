﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ReviewEmployeeInformationActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.ReviewEmployeeInformation; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ReviewEmployeeInformationActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Review Employee Information"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Review Employee Information ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Determines whether [is valid to run] [the specified wf].
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            /// These values have to be populated
            if (user == null || wf.State == null || activity.Metadata == null)
                return false;

            /// we need case id on WorkflowInstance.State
            if (string.IsNullOrWhiteSpace(wf.EmployeeId))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            var todo = CreateEmployeeToDo(wf, activity, user);
            return base.RunEnd(wf, activity, user);
        }
    }
}
