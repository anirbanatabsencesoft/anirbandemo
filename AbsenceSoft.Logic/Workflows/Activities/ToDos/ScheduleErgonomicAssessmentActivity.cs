﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ScheduleErgonomicAssessmentActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.ScheduleErgonomicAssessment; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ScheduleErgonomicAssessmentActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Schedule Ergonomic Assessment"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Schedule Ergonomic Assessment ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Is Valid To Run if we have a CaseId and an AccommodationId
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="activity"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            return base.IsValidToRun(wf, activity, user) && wf.StateHasProperties("AccommodationId");
        }
    }
}
