﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class PaperworkReviewActivity : BaseToDoActivity
    {
        /// <summary>
        /// The approved outcome value
        /// </summary>
        public const string ApprovedOutcomeValue = "Approved";
        /// <summary>
        /// The incomplete outcome value
        /// </summary>
        public const string IncompleteOutcomeValue = "Incomplete";
        /// <summary>
        /// The denied outcome value
        /// </summary>
        public const string DeniedOutcomeValue = "Denied";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.PaperworkReview; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "PaperworkReviewActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Paperwork Review"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a ToDo to review a specific piece of paperwork"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return ApprovedOutcomeValue;
            yield return IncompleteOutcomeValue;
            yield return DeniedOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            yield return EventType.PaperworkSent;
            yield return EventType.CaseCreated;
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            ToDoItem todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            PaperworkReviewStatus status = PaperworkReviewStatus.Pending;
            if (!string.IsNullOrEmpty(todo.ResultText) && todo.Status == ToDoItemStatus.Complete)
            {
                switch (todo.ResultText)
                {
                    case IncompleteOutcomeValue:
                        status = PaperworkReviewStatus.Incomplete;
                        break;
                    case ApprovedOutcomeValue:
                        status = PaperworkReviewStatus.Complete;
                        break;
                    case DeniedOutcomeValue:
                        status = PaperworkReviewStatus.Denied;
                        break;
                }

                string commId = todo.Metadata.GetRawValue<string>("CommunicationId");
                var pId = todo.Metadata.GetRawValue<Guid?>("PaperworkId");
                Communication comm = Communication.GetById(commId);
                if (comm != null && pId.HasValue)
                {
                    CommunicationPaperwork pw = comm.Paperwork.FirstOrDefault(p => p.Id == pId);
                    if (pw != null)
                    {
                        pw.Status = status;
                        pw.StatusDate = DateTime.UtcNow;
                        comm.Save();
                    }
                }
            }

            activity.Outcome = todo.ResultText;
            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);

            return activity;
        }
    }
}
