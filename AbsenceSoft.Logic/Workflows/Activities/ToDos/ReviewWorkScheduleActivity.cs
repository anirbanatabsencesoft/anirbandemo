﻿using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ReviewWorkScheduleActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ReviewWorkScheduleActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Review Work Schedule"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Review Work Schedule ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.ReviewWorkSchedule; }
        }

        /// <summary>
        /// Returns the possible event types for a Review Work Schedule activity
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            yield return EventType.WorkScheduleChanged;
        }

        /// <summary>
        /// Determines whether [is valid to run] [the specified wf].
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            /// These values have to be populated
            if (user == null || wf.State == null || activity.Metadata == null)
                return false;

            /// we need case id on WorkflowInstance.State
            if (string.IsNullOrWhiteSpace(wf.EmployeeId))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            CreateEmployeeToDo(wf, activity, user);
            return base.RunEnd(wf, activity, user);
        }

        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            ToDoItem todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            var wfState = wf.DynamicState;

            if (!string.IsNullOrEmpty(todo.ResultText))
                activity.Outcome = todo.ResultText;
            else if (todo.Status == ToDoItemStatus.Complete || todo.Status == ToDoItemStatus.Cancelled)
                activity.Outcome = todo.Status == ToDoItemStatus.Complete ? CompleteOutcomeValue : CanceledOutcomeValue;

            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);
            if (!activity.IsWaitingOnUserInput
                && todo.Status == ToDoItemStatus.Complete
                && todo.Case != null
                && todo.ResultText == CompleteOutcomeValue
                && wf.StateHasProperties("WorkSchedule")
                && wfState.ReturnToWorkDate != null)
            {
                todo.Employee.WorkSchedules.Add(wfState.WorkSchedule);
                todo.Employee.Save();
            }

            return activity;
        }
    }
}
