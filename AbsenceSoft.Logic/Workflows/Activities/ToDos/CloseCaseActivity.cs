﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class CloseCaseActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.CloseCase; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "CloseCaseActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Close Case"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Checks if the case can be automatically closed and does so, or creates a Close Case ToDo if the case can not be automatically closed"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            bool closeAutomatically = activity.Metadata.GetRawValue<bool?>("CloseAutomatically") ?? false;

            if (closeAutomatically)
            {
                ActivityHistory ah = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
                return Complete(ah, wf, user);
            }

            return base.Run(wf, activity, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            DateTime closeDate = wf.Case.EndDate ?? wf.Case.Segments.Max(s => s.EndDate) ?? DateTime.UtcNow;
            if (closeDate < wf.Case.StartDate)
                closeDate = wf.Case.StartDate;
            if (wf.Case.EndDate.HasValue && closeDate > wf.Case.EndDate)
                closeDate = wf.Case.EndDate.Value;

            CaseClosureReason reason = activity.Metadata.GetRawValue<CaseClosureReason?>("Reason") ?? CaseClosureReason.Other;
            string closureReason = wf.Metadata.GetRawValue<string>("ReasonCause");
            string otherReasonText = wf.Metadata.GetRawValue<string>("OtherReasonText");


            // See whether or not we have a ToDo that we're completing
            if (wf.StateHasProperties("ToDoId"))
            {
                ToDoItem todo = GetToDoItem(activity, wf);
                if (todo != null && todo.ItemType == ToDoItemType.CloseCase)
                {
                    var closedDate = todo.Metadata.GetRawValue<DateTime?>("CaseClosedDate");
                    closeDate = closedDate ?? wf.Case.EndDate ?? wf.Case.Segments.Max(s => s.EndDate) ?? todo.DueDate;
                    if (closeDate < todo.Case.StartDate)
                        closeDate = todo.Case.StartDate;
                    if (!closedDate.HasValue && todo.DueDate < closeDate && todo.DueDate > todo.Case.StartDate)
                        closeDate = todo.DueDate;
                    if (todo.Case.EndDate.HasValue && closeDate > todo.Case.EndDate)
                        closeDate = todo.Case.EndDate.Value;

                    // Get the new reason from the ToDo context if it exists
                    reason = todo.Metadata.GetRawValue<CaseClosureReason?>("Reason") ?? reason;
                }
            }

            // This is a hack, as it were, the date we're passing into the case service CaseClosed() method
            //  is the date the employee will RTW, therefore, it will subtract a day to get the final end date of the
            //  case per logic, so when we pass that date in, we need to add a day
            closeDate = closeDate.AddDays(1);

            // Actually close the case
            using (CaseService svc = new CaseService())
            {
                Case c = wf.Case;
                string Outcome = activity.Outcome;
                svc.CaseClosed(c, closeDate, reason, closureReason, otherReasonText, Outcome);
            }

            return base.Complete(activity, wf, user);
        }
    }
}
