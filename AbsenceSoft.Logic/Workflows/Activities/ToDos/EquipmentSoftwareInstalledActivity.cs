﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class EquipmentSoftwareInstalledActivity: BaseToDoActivity
    {
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.EquipmentSoftwareInstalled; }
        }

        public override string Id
        {
            get { return "EquipmentSoftwareInstalledActivity"; }
        }

        public override string Name
        {
            get { return "Accommodation Equipment or Software Installed"; }
        }

        public override string Description
        {
            get { return "Creates an Accommodation Equipment of Software Ordered ToDo"; }
        }

        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }
    }
}
