﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    [Serializable]
    public abstract class BaseToDoActivity : Activity
    {
        /// <summary>
        /// The canceled outcome value
        /// </summary>
        public const string CanceledOutcomeValue = "Canceled";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public abstract ToDoItemType ToDoType { get; }

        /// <summary>
        /// Gets a value indicating whether to automatically save the ToDo item once it's created, 
        /// otherwise if additional changes are needed to be made before saving, <c>false</c>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic save]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool AutoSave { get { return true; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "ToDo"; } }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return CompleteOutcomeValue;
            yield return CanceledOutcomeValue;
            yield return Activity.ErrorOutcomeValue;
        }


        /// <summary>
        /// Returns the event types for a Cases
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            return base.GetCaseEventTypes();
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            /// These values have to be populated
            if (user == null || wf.State == null || activity.Metadata == null)
                return false;

            /// we need case id on WorkflowInstance.State
            if (string.IsNullOrWhiteSpace(wf.CaseId))
                return false;

            /// We need the number of time and units for the todo, whether it is optional or not, the priority, and if it should be hidden
            /// In the future we will need a type of person to assign to, but for now it is just the case
            //if (!activity.MetadataHasProperties("Time", "Title", "Units"))
            //    return false;
            // All options currently are optional; 'cause we have smarticle defaults 'n' stuff

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            if (activity.Metadata.GetRawValue<bool?>("Spouse") == true && (wf.Case == null || string.IsNullOrWhiteSpace(wf.Case.SpouseCaseId)))
                return base.RunEnd(wf, activity, user, CompleteOutcomeValue,
                    error: "Unable to create ToDo for spouse case, related spouse case not found or not applicable.");

            var todo = CreateCaseToDo(wf, activity, user);
            var activityHistory = base.RunEnd(wf, activity, user);
            return AutoCompleteToDo(activityHistory, activity, todo, user);
        }

        /// <summary>
        /// Allows ToDoActivities that override Run to handle AutoComplete
        /// </summary>
        /// <param name="activityHistory"></param>
        /// <param name="activity"></param>
        /// <param name="todo"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        internal ActivityHistory AutoCompleteToDo(ActivityHistory activityHistory, WorkflowActivity activity, ToDoItem todo, User user = null)
        {
            if (IsAutocompleted(activity, todo, user))
                return UpdateActivityHistoryByToDo(todo, activityHistory);

            return activityHistory;
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            var todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            return UpdateActivityHistoryByToDo(todo, activity);
        }

        /// <summary>
        /// Will automatically complete a todo if the user triggering this portion of the workflow is the same person who the todo is assigned to
        /// </summary>
        /// <returns></returns>
        public bool IsAutocompleted(WorkflowActivity activity, ToDoItem todo, User user = null)
        {
            // No user to compare to, it's not auto completed
            if (user == null)
                return false;

            // not sure how this can happen, but better safe than sorry
            if (todo == null)
                return false;

            // Didn't tell us to auto complete, it's not auto completed
            bool? autocomplete = activity.Metadata.GetRawValue<bool?>("Autocomplete");
            if (!autocomplete.HasValue || !autocomplete.Value)
                return false;

            // We only auto complete if it's assigned to the same person who triggered it
            if (user.Id != todo.AssignedToId)
                return false;

            todo.Status = ToDoItemStatus.Complete;
            todo.Save();
            return true;
        }

        /// <summary>
        /// Updates the ActivityHistory based on the ToDo status
        /// </summary>
        /// <param name="todo"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        private ActivityHistory UpdateActivityHistoryByToDo(ToDoItem todo, ActivityHistory activity)
        {
            if (!string.IsNullOrEmpty(todo.ResultText))           
                activity.Outcome = todo.ResultText;            
            else if (todo.Status == ToDoItemStatus.Complete || todo.Status == ToDoItemStatus.Cancelled)
                activity.Outcome = todo.Status == ToDoItemStatus.Complete ? CompleteOutcomeValue : CanceledOutcomeValue;

            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);

            return activity;
        }

        /// <summary>
        /// Gets to do item.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="wf">The wf.</param>
        /// <returns></returns>
        protected ToDoItem GetToDoItem(ActivityHistory activity, WorkflowInstance wf)
        {
            var todo = ToDoItem.Query.Find(ToDoItem.Query.And(ToDoItem.Query.EQ(t => t.CustomerId, wf.CustomerId),
                ToDoItem.Query.EQ(t => t.EmployerId, wf.EmployerId),
                ToDoItem.Query.EQ(t => t.EmployeeId, wf.EmployeeId),
                ToDoItem.Query.In(t => t.CaseId, new[] { null, wf.CaseId }),
                ToDoItem.Query.EQ(t => t.WorkflowInstanceId, wf.Id),
                Query.EQ("WorkflowActivityId", activity.WorkflowActivityId)
                )).OrderByDescending(t=>t.ModifiedDate).FirstOrDefault();

            dynamic wfState = wf.State;
            if (todo == null && wf.StateHasProperties("ToDoId"))
                todo = ToDoItem.GetById(wfState.ToDoId);
            if (todo == null && wf.StateHasProperties("ToDoItemId"))
                todo = ToDoItem.GetById(wfState.ToDoItemId);
            if (todo == null && activity.Metadata.GetRawValue<string>("ToDoId") != null)
                todo = ToDoItem.GetById(activity.Metadata.GetRawValue<string>("ToDoId"));
            if (todo == null && activity.Metadata.GetRawValue<string>("ToDoItemId") != null)
                todo = ToDoItem.GetById(activity.Metadata.GetRawValue<string>("ToDoItemId"));

            return todo;
        }

        /// <summary>
        /// Creates the case to do.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        internal ToDoItem CreateCaseToDo(WorkflowInstance wf, WorkflowActivity activity, User user)
        {
            Case c = activity.Metadata.GetRawValue<bool?>("Spouse") == true ? wf.Case.SpouseCase : wf.Case;
            ToDoItem newCaseTodo = CreateBaseTodo(wf, activity, user);
            newCaseTodo.CaseId = c.Id;
            newCaseTodo.CustomerId = c.CustomerId;
            newCaseTodo.EmployeeId = c.Employee.Id;
            newCaseTodo.EmployeeNumber = c.Employee.EmployeeNumber;
            newCaseTodo.EmployeeName = c.Employee.FullName;
            newCaseTodo.EmployerId = c.EmployerId;
            newCaseTodo.AssignedToId = c.AssignedToId;
            newCaseTodo.AssignedToName = c.AssignedToName;
            string assigneeType = activity.Metadata.GetRawValue<string>("CaseAssigneeTypeCode");
            if (!string.IsNullOrWhiteSpace(assigneeType))
            {
                var assigneeTypeCodesIn = new[] { assigneeType, assigneeType.ToUpperInvariant(), assigneeType.ToLowerInvariant() };
                CaseAssignee caseAssignee = CaseAssignee.AsQueryable()
                    .Where(ca => 
                        ca.CustomerId == wf.CustomerId 
                        && ca.CaseId == wf.CaseId 
                        && assigneeTypeCodesIn.Contains(ca.Code))
                    .OrderByDescending(ca => ca.CreatedDate)
                    .Take(1)
                    .FirstOrDefault();

                if (caseAssignee == null && wf.EventType == EventType.PolicyExhausted)
                {
                    using (var caseService = new CaseService())
                    {
                        caseService.EvaluateCaseAssignmentRules(wf.Case, assigneeType, user);
                        caseAssignee = CaseAssignee.AsQueryable()
                        .Where(ca =>
                            ca.CustomerId == wf.CustomerId
                            && ca.CaseId == wf.CaseId
                            && assigneeTypeCodesIn.Contains(ca.Code))
                        .OrderByDescending(ca => ca.CreatedDate)
                        .Take(1)
                        .FirstOrDefault();
                    }
                }

                if (caseAssignee != null)
                {
                    newCaseTodo.AssignedToId = caseAssignee.UserId;
                    newCaseTodo.AssignedToName = User.GetById(caseAssignee.UserId).DisplayName;
                }
            }
            if (AutoSave)
                newCaseTodo.Save();

            return newCaseTodo;
        }

        /// <summary>
        /// Creates the employee to do.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        internal ToDoItem CreateEmployeeToDo(WorkflowInstance wf, WorkflowActivity activity, User user)
        {
            Employee e = Employee.GetById(wf.EmployeeId);
            ToDoItem newEmployeeTodo = CreateBaseTodo(wf, activity, user);
            newEmployeeTodo.CustomerId = e.CustomerId;
            newEmployeeTodo.EmployeeId = e.Id;
            newEmployeeTodo.EmployeeNumber = e.EmployeeNumber;
            newEmployeeTodo.EmployeeName = e.FullName;
            newEmployeeTodo.EmployerId = e.EmployerId;
            newEmployeeTodo.AssignedToId = user.Id;
            newEmployeeTodo.AssignedToName = user.DisplayName;

            if (AutoSave)
                newEmployeeTodo.Save();

            return newEmployeeTodo;
        }

        /// <summary>
        /// Creates the base todo.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private ToDoItem CreateBaseTodo(WorkflowInstance wf, WorkflowActivity activity, User user)
        {
            var configuration = activity.Metadata;
            dynamic wfState = wf.State;
            int timeToRespond = configuration.GetRawValue<int?>("Time") ?? 0;
            ToDoResponseTime responseTime = configuration.GetRawValue<ToDoResponseTime?>("Units") ?? ToDoResponseTime.Hours;
            if (timeToRespond == 0 && responseTime == ToDoResponseTime.Hours)
                timeToRespond = 1;

            bool isSpouse = activity.Metadata.GetRawValue<bool?>("Spouse") == true;
            Case c = wf.Case == null ? null : isSpouse ? wf.Case.SpouseCase : wf.Case;

            ToDoItem newTodo = new ToDoItem()
            {
                CustomerId = wf.CustomerId,
                EmployerId = wf.EmployerId,
                EmployeeId = wf.EmployeeId,
                EmployeeNumber = c == null ? null : c.Employee.EmployeeNumber,
                CaseId = c == null ? wf.CaseId : c.Id,
                ItemType = ToDoType,
                Optional = configuration.GetRawValue<bool?>("Optional") ?? false,
                Title = configuration.GetRawValue<string>("Title") ?? ToDoType.ToString().SplitCamelCaseString(),
                Priority = configuration.GetRawValue<ToDoItemPriority?>("Priority") ?? ToDoItemPriority.Normal,
                DueDate = CalculateDueDate(wf, activity, timeToRespond, responseTime, isSpouse ? c.Employee.Id : wf.EmployeeId),
                HideUntil = CalculateHideUntil(c, configuration, wf),
                AssignedToId = c == null ? user.Id : c.AssignedToId,
                AssignedToName = c == null ? user.DisplayName : c.AssignedToName,
                CaseNumber = c == null ? null : c.CaseNumber,
                EmployeeName = (c == null ? null : c.Employee.FullName) ?? (wf.Employee == null ? null : wf.Employee.FullName),
                EmployerName = wf.Employer.Name,
                Status = ToDoItemStatus.Pending,
                WorkflowInstance = wf,
                CreatedById = user.Id,
                ModifiedById = user.Id,
                Metadata = wf.Metadata.DeepClone().AsBsonDocument,
                Question = activity.Question != null ? activity.Question:null,
            };
          
            if (newTodo.Title != null && newTodo.Title.Contains("{{"))
            {
                // Need to tokenize this thing
                try
                {
                    var scope = wf.BuildContext();
                    newTodo.Title = Rendering.Template.RenderTemplate(newTodo.Title, scope);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Workflow instance '{0}', for activity '{1}', failed to tokenize title of ToDo item: \"{2}\".", wf.Id, activity.Name, newTodo.Title), ex);
                }
            }

            newTodo.WorkflowInstanceId = wf.Id;
            newTodo.Metadata.Set("WorkflowActivityId", activity.Id);

            if (configuration.GetRawValue<bool?>("Unique") == true)
            {
                newTodo.OnSavedNOnce((o, args) =>
                {
                    List<ToDoItem> duplicates = ToDoItem.AsQueryable().Where(t =>
                        t.Id != args.Entity.Id &&
                        t.CaseId == args.Entity.CaseId &&
                        t.ItemType == args.Entity.ItemType &&
                        t.Title == args.Entity.Title &&
                        (t.Status == ToDoItemStatus.Pending || t.Status == ToDoItemStatus.Open || t.Status == ToDoItemStatus.Overdue)
                    ).ToList();

                    if (!duplicates.Any())
                        return;

                    //Allow duplicate comm todos
                    List<ToDoItem> toRemove = new List<ToDoItem>(duplicates);
                    var isNewCommToDo = new Func<ToDoItem, bool>(t =>
                    {
                        //check for item type = communication and different template
                        string templateOld = string.Empty;
                        string templateNew = t.Metadata.GetRawValue<string>("Template");
                        Guid? accommIdOld = null;
                        Guid? accommIdNew = t.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
                        accommIdOld = t.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
                        templateOld = t.Metadata.GetRawValue<string>("Template");

                        //this is an accomm approval/denial letter for a different accomm - allow the duplicate
                        // Freaking HACK!
                        if (accommIdNew.HasValue &&
                            accommIdOld.HasValue &&
                            (templateNew.Equals("ACCOM-APPROVED") || templateNew.Equals("ACCOM-DENIED")) &&
                            (accommIdNew != accommIdOld))
                        {
                            return true;
                        }

                        //this is a communicatin todo with a different template allow duplicate
                        else if (templateNew != templateOld)
                            return true;

                        return false;
                    });
                    foreach (ToDoItem t in duplicates.Where(x => x.ItemType == ToDoItemType.Communication))
                        if (isNewCommToDo(t))
                            toRemove.Remove(t);

                    //cancel any duplicates
                    toRemove.ForEach(t => t.WfOnToDoItemCanceled(user, true));
                });
            }
            return newTodo;
        }

        /// <summary>
        /// Calculates the due date.
        /// </summary>
        /// <param name="timeToRespond">The time to respond.</param>
        /// <param name="responseTime">The response time.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        private DateTime CalculateDueDate(WorkflowInstance wf, WorkflowActivity activity, int timeToRespond, ToDoResponseTime responseTime, string employeeId)
        {
            DateTime dueDate = DateTime.UtcNow;
            DateTime? changedDueDate = null;
            CaseEventType? dueFrom = activity.Metadata.GetRawValue<CaseEventType?>("TargetDueDate");
            string dueFromMetaName = activity.Metadata.GetRawValue<string>("TargetDueDateMeta");
            //check for return to work date 
            //Checked value of "ReturnToWork" for Jira Work Item-EZW-3 : RTW not reflecting properly 
            changedDueDate = !string.IsNullOrEmpty( wf.Metadata.GetRawValue<DateTime?>("ReturnToWork").ToString())? wf.Metadata.GetRawValue<DateTime?>("ReturnToWork"):null;

            if (!string.IsNullOrWhiteSpace(dueFromMetaName))
                dueDate = wf.Metadata.GetRawValue<DateTime?>(dueFromMetaName) ??
                    wf.Event.Metadata.GetRawValue<DateTime?>(dueFromMetaName) ??
                    (wf.Case == null ? null : wf.Case.Metadata.GetRawValue<DateTime?>(dueFromMetaName)) ??
                    (wf.StateHasProperties(dueFromMetaName) ? wf.State[dueFromMetaName] as DateTime? : null) ??
                    dueDate;

            if (dueFrom.HasValue && wf.Case != null)
            {
                var caseEvent = wf.Case.FindCaseEvent(dueFrom.Value);
                if (caseEvent != null)
                    dueDate = caseEvent.EventDate;
            }
            if (wf.Event.EventType == EventType.PolicyExhausted)
            {
                dueDate = wf.Event.Metadata.GetRawValue<DateTime?>("FirstExhaustionDate") ?? dueDate;
            }
            //Returning "ReturnToWork" value if any for Jira Work Item-EZW-3 : RTW not reflecting properly 
            if (dueFrom == CaseEventType.EstimatedReturnToWork && changedDueDate !=null)
            {
                dueDate = changedDueDate.Value;
                return dueDate;
            }
            if (responseTime == ToDoResponseTime.Hours)
                return dueDate.AddHours(timeToRespond);

            if (responseTime == ToDoResponseTime.WorkingDays)
                return CalculateWorkingDays(dueDate, timeToRespond, employeeId);

            if (responseTime == ToDoResponseTime.CalendarDays)
                return dueDate.AddDays(timeToRespond);

            if (responseTime == ToDoResponseTime.Months)
                return dueDate.AddMonths(timeToRespond);

            return dueDate;
        }

        /// <summary>
        /// Calculates the working days.
        /// </summary>
        /// <param name="dueDate">When the ToDo might be due.</param>
        /// <param name="timeToRespond">The time to respond.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        private DateTime CalculateWorkingDays(DateTime dueDate, int timeToRespond, string employeeId)
        {
            var dates = GetEmployeeSchedule(dueDate, timeToRespond, employeeId)
                .Where(t => t.TotalMinutes > 0);

            if (timeToRespond > 0)
                dates = dates.OrderBy(s => s.SampleDate); // They are calculating a date in the future
            else
                dates = dates.OrderByDescending(s => s.SampleDate); // We need to count backwards

            return dates.Skip(Math.Abs(timeToRespond)) // since we've flipped it, just count the absolute value of days
                .Select(s => s.SampleDate)
                .DefaultIfEmpty(DateTime.UtcNow).First();
        }

        /// <summary>
        /// Gets an employee's schedule for the necessary length of time
        /// </summary>
        /// <param name="dueDate"></param>
        /// <param name="timeToRespond"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        private List<Time> GetEmployeeSchedule(DateTime dueDate, int timeToRespond, string employeeId)
        {
            Employee emp = Employee.GetById(employeeId);
            LeaveOfAbsence leave = new LeaveOfAbsence()
            {
                Employee = emp,
                Employer = emp.Employer
            };

            DateTime endDate = dueDate.AddDays(timeToRespond * 5);
            // This can happen when they create a ToDo that is -x days from an event
            if (dueDate > endDate)
            {
                DateTime tempDate = endDate;
                endDate = dueDate;
                dueDate = tempDate;
            }

            return leave.MaterializeSchedule(dueDate, endDate);
        }

        /// <summary>
        /// Calculates the hide until.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        private DateTime? CalculateHideUntil(Case c, BsonDocument configuration, WorkflowInstance wf)
        {
            if (configuration == null)
                return null;

            string hideFromMetaName = configuration.GetRawValue<string>("TargetHideDateMeta");
            if (!string.IsNullOrWhiteSpace(hideFromMetaName))
                return wf.Metadata.GetRawValue<DateTime?>(hideFromMetaName) ??
                    wf.Event.Metadata.GetRawValue<DateTime?>(hideFromMetaName) ??
                    (c == null ? null : c.Metadata.GetRawValue<DateTime?>(hideFromMetaName));

            int? hideDays = configuration.GetRawValue<int?>("HideDays");
            CaseEventType? eventType = configuration.GetRawValue<CaseEventType?>("TargetHideDate");
            if (hideDays == null)
                return null;
            if (eventType == null)
                return DateTime.UtcNow.AddDays(hideDays.Value);

            if (c == null)
                return null;

            var caseEvent = c.CaseEvents.FirstOrDefault(w => w.EventType == eventType.Value);
            if (caseEvent == null)
                return null;

            return caseEvent.EventDate.AddDays(hideDays.Value);
        }
    }
}
