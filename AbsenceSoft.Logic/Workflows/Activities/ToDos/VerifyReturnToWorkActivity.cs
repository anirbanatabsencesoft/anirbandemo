﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class VerifyReturnToWorkActivity : BaseToDoActivity
    {
        /// <summary>
        /// The RTW verified yes outcome
        /// </summary>
        public const string RtwVerifiedYesOutcome = "RTW Verified";
        /// <summary>
        /// The RTW verified no outcome
        /// </summary>
        public const string RtwVerifiedNoOutcome = "RTW Not Verified";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType { get { return ToDoItemType.VerifyReturnToWork; } }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "VerifyReturnToWorkActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "Verify RTW"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates and handles a Verify Return to Work ToDo"; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.User; } }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return RtwVerifiedYesOutcome;
            yield return RtwVerifiedNoOutcome;
            yield return CanceledOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        /// <summary>
        /// Completes the specified activity.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="wf">The wf.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            ToDoItem todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            var wfState = wf.DynamicState;

            if (!string.IsNullOrEmpty(todo.ResultText))
                activity.Outcome = todo.ResultText;
            else if (todo.Status == ToDoItemStatus.Complete || todo.Status == ToDoItemStatus.Cancelled)
                activity.Outcome = todo.Status == ToDoItemStatus.Complete ? CompleteOutcomeValue : CanceledOutcomeValue;

            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);
            if (!activity.IsWaitingOnUserInput
                && todo.Status == ToDoItemStatus.Complete
                && todo.Case != null
                && todo.ResultText == RtwVerifiedYesOutcome
                && wf.StateHasProperties("ReturnToWorkDate")
                && wfState.ReturnToWorkDate != null)
            {
                todo.Case.SetCaseEvent(CaseEventType.ReturnToWork, wfState.ReturnToWorkDate);
                todo.Case.Save();
            }

            return activity;
        }
    }
}