﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class UpdateToDosActivity : Activity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "UpdateToDosActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Update ToDos"; }
        }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category
        {
            get { return "ToDo"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Updates the ToDos associated with this workflow, or optionally updates all ToDos associated with the case or employee"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.Action; }
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            if (wf == null || activity == null || user == null)
                return false;

            if (string.IsNullOrWhiteSpace(wf.CaseId) && string.IsNullOrEmpty(wf.EmployeeId))
                return false;

            if (!activity.MetadataHasProperties("Status"))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            bool updateAll = activity.Metadata.GetRawValue<bool?>("UpdateAll") ?? false;
            ToDoItemStatus? whereStatus = activity.Metadata.GetRawValue<ToDoItemStatus?>("WhereStatus");
            string whereResultText = activity.Metadata.GetRawValue<string>("WhereResultText");
            ToDoItemStatus status = activity.Metadata.GetRawValue<ToDoItemStatus>("Status");

            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(ToDoItem.Query.EQ(t => t.CustomerId, wf.CustomerId));
            ands.Add(ToDoItem.Query.EQ(t => t.EmployerId, wf.EmployerId));

            if (!string.IsNullOrWhiteSpace(wf.CaseId))
                ands.Add(ToDoItem.Query.EQ(t => t.CaseId, wf.CaseId));
            else
                ands.Add(ToDoItem.Query.EQ(t => t.EmployeeId, wf.EmployeeId));

            if (!updateAll)
                ands.Add(ToDoItem.Query.EQ(t => t.WorkflowInstanceId, wf.Id));
            if (whereStatus.HasValue)
            {
                ToDoItemStatus whereStatusValue = whereStatus.Value;
                if (whereStatusValue != ToDoItemStatus.Overdue)
                {
                    ands.Add(ToDoItem.Query.EQ(t => t.Status, whereStatusValue));
                }
                else
                {
                    ands.Add(ToDoItem.Query.Or(
                            ToDoItem.Query.EQ(t => t.Status, whereStatusValue),
                            ToDoItem.Query.And(
                                ToDoItem.Query.LTE(t => t.DueDate, DateTime.UtcNow),
                                ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Complete),
                                ToDoItem.Query.NE(t => t.Status, ToDoItemStatus.Cancelled)
                                )
                            )
                        );
                }
                    
            }
                
            if (!string.IsNullOrWhiteSpace(whereResultText))
                ands.Add(ToDoItem.Query.EQ(t => t.ResultText, whereResultText));

            List<ToDoItem> toDosToUpdate = ToDoItem.Query.Find(ToDoItem.Query.And(ands)).ToList();

            foreach (ToDoItem todo in toDosToUpdate)
            {
                todo.Status = status;
                todo.ResultText = activity.Metadata.GetRawValue<string>("ResultText");
                todo.ModifiedById = user.Id;
                todo.Save();
            }

            ActivityHistory ah = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(ah, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, User user = null)
        {
            return activity;
        }
    }
}
