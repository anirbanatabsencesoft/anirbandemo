﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class PaperworkDueActivity : BaseToDoActivity
    {
        /// <summary>
        /// The paperwork received outcome value
        /// </summary>
        public const string PaperworkReceivedOutcomeValue = "Paperwork Received";
        /// <summary>
        /// The paperwork not received outcome value
        /// </summary>
        public const string PaperworkNotReceivedOutcomeValue = "Paperwork Not Received";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.PaperworkDue; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "PaperworkDueActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Paperwork Due"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Paperwork Due ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return CanceledOutcomeValue;
            yield return PaperworkReceivedOutcomeValue;
            yield return PaperworkNotReceivedOutcomeValue;
        }

        /// <summary>
        /// Returns the event types for a Cases
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            yield return EventType.PaperworkSent;
            yield return EventType.CaseCreated;
            yield return EventType.AccommodationCreated;
            yield return EventType.Manual;
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            ToDoItem todo = GetToDoItem(activity, wf);
            if (todo == null)
                return activity;

            string commId = todo.Metadata.GetRawValue<string>("CommunicationId");
            Guid? paperworkId = todo.Metadata.GetRawValue<Guid?>("PaperworkId");
            bool? extendGracePeriod = todo.Metadata.GetRawValue<bool?>("ExtendGracePeriod");
            Communication comm = Communication.GetById(commId);

            if (todo.ResultText == PaperworkNotReceivedOutcomeValue)
            {
                MarkPaperworkNotReceived(comm, paperworkId, extendGracePeriod, todo, user);
            }


            if (todo.Status == ToDoItemStatus.Complete
                && todo.Case != null
                && todo.ResultText == PaperworkReceivedOutcomeValue
                )
            {
                AddReturnAttachmentId(comm, paperworkId, todo);
                AddPaperworkReceivedCaseEvent(todo);
            }

            activity.Outcome = todo.ResultText;
            activity.IsWaitingOnUserInput = string.IsNullOrEmpty(activity.Outcome);

            return activity;
        }

        private void AddReturnAttachmentId(Communication comm, Guid? paperworkId, ToDoItem todo)
        {
            if (comm == null)
                return;

            CommunicationPaperwork pw = GetMatchingPaperwork(comm, paperworkId);
            if (pw != null)
            {
                pw.ReturnAttachmentId = todo.Metadata.GetRawValue<string>("ReturnAttachmentId");
            }

            comm.Save();
        }

        private void AddPaperworkReceivedCaseEvent(ToDoItem todo)
        {
            CaseEvent ce = new CaseEvent() { EventType = CaseEventType.PaperworkReceived };
            ce.EventDate = DateTime.Today;
            todo.Case.CaseEvents.AddFluid(ce);
            todo.Case.Save();
        }

        private void MarkPaperworkNotReceived(Communication comm, Guid? paperworkId, bool? extendGracePeriod, ToDoItem todo, Data.Security.User user = null)
        {
            CommunicationPaperwork pw = GetMatchingPaperwork(comm, paperworkId);
            if (pw != null)
            {
                pw.Status = PaperworkReviewStatus.NotReceived;
                pw.StatusDate = DateTime.UtcNow;
            }

            if (extendGracePeriod.HasValue && extendGracePeriod.Value)
            {
                todo.DueDate = todo.Metadata.GetRawValue<DateTime?>("NewDueDate") ?? todo.DueDate.AddDays(1);
                if (pw != null)
                {
                    pw.Status = PaperworkReviewStatus.Pending;
                    pw.SetDueDate(todo.DueDate, user);
                }
                todo.Status = ToDoItemStatus.Pending;
                todo.DueDateChangeReason = "Paperwork not received, Extend Grace Period";
                todo.ResultText = null; // getting extended, no result yet
                todo.Save();
            }

            if (comm != null)
            {
                comm.Save();
            }                
        }

        private CommunicationPaperwork GetMatchingPaperwork(Communication comm, Guid? paperworkId)
        {
            if (comm == null || !paperworkId.HasValue)
                return null;

            return comm.Paperwork.FirstOrDefault(p => p.Id == paperworkId);
        }
    }
}
