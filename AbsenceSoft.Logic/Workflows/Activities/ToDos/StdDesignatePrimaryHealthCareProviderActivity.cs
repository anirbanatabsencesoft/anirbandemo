﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    [FeatureRestriction(Feature.ShortTermDisability)]
    public sealed class StdDesignatePrimaryHealthCareProviderActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType { get { return ToDoItemType.STDDesignatePrimaryHCP; } }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "StdDesignatePrimaryHealthCareProviderActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "STD Designate Primary Health Care Provider"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates and handles an STD Designate Primary Health Care Provider ToDo"; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.User; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "STD"; } }
    }
}
