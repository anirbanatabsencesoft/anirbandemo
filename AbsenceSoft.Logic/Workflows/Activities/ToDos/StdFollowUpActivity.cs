﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    [FeatureRestriction(Feature.ShortTermDisability)]
    public sealed class StdFollowUpActivity : BaseToDoActivity
    {
        /// <summary>
        /// The standard leave complete outcome
        /// </summary>
        public const string StdLeaveCompleteOutcome = "STD Leave Complete";
        /// <summary>
        /// The standard leave not complete outcome
        /// </summary>
        public const string StdLeaveNotCompleteOutcome = "STD Leave Not Complete";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType { get { return ToDoItemType.STDFollowUp; } }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "StdFollowUpActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "STD Follow Up"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates and handles an STD Follow Up ToDo"; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.User; } }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category { get { return "STD"; } }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            // TODO: Need to do stuff with these outcomes.
            yield return StdLeaveCompleteOutcome;
            yield return StdLeaveNotCompleteOutcome;
            yield return CanceledOutcomeValue;
            yield return Activity.ErrorOutcomeValue;
        }
    }
}
