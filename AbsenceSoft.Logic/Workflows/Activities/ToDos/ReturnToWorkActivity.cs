﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ReturnToWorkActivity : BaseToDoActivity
    {
        /// <summary>
        /// The return to work outcome value
        /// </summary>
        public const string ReturnToWorkOutcomeValue = "Returning to work";
        /// <summary>
        /// The not returning to work outcome value
        /// </summary>
        public const string NotReturningToWorkOutcomeValue = "Not returning to work";
        /// <summary>
        /// The recertify outcome value
        /// </summary>
        public const string RecertifyOutcomeValue = "Recertify";

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.ReturnToWork; }
        }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ReturnToWorkActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Return To Work"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Creates a Return To Work ToDo"; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        /// <summary>
        /// Gets the possible outcomes for the activity. If no outcomes are
        /// returned, then this is a terminating action.
        /// </summary>
        /// <returns>
        /// A collection of possible outcomes, codes or names, ids, etc. that
        /// uniquely identify each outcome an activity may have.
        /// </returns>
        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return ReturnToWorkOutcomeValue;
            yield return NotReturningToWorkOutcomeValue;
            yield return RecertifyOutcomeValue;
            yield return ErrorOutcomeValue;
        }
    }
}
