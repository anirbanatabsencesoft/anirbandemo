﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    [Serializable]
    public sealed class SendCommunicationActivity : BaseToDoActivity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id { get { return "SendCommunicationActivity"; } }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name { get { return "Send Communication"; } }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description { get { return "Creates a communication ToDo, OR, automatically generates (and/or sends) the communication given the parameters provided."; } }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType { get { return ActivityType.User; } }

        /// <summary>
        /// Gets the type of to do.
        /// </summary>
        /// <value>
        /// The type of to do.
        /// </value>
        public override ToDoItemType ToDoType { get { return ToDoItemType.Communication; } }

        /// <summary>
        /// Gets a value indicating whether [automatic save].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic save]; otherwise, <c>false</c>.
        /// </value>
        public override bool AutoSave { get { return false; } }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            return base.IsValidToRun(wf, activity, user) && activity.MetadataHasProperties("TemplateCode"/*, "DefaultSendMethod"*/);
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            /// First, we create the ToDo!
            ToDoItem sendCommunicationToDo = base.CreateCaseToDo(wf, activity, user);
            sendCommunicationToDo.Metadata.SetRawValue<string>("Template", activity.Metadata.GetRawValue<string>("TemplateCode"));
            sendCommunicationToDo.Metadata.SetRawValue<int?>("CommunicationType", activity.Metadata.GetRawValue<int?>("DefaultSendMethod") ?? (int)CommunicationType.Mail);
            sendCommunicationToDo.Save();

            /// Technically this Activity is now done, but we might want to automatically complete
            ActivityHistory history = base.RunEnd(wf, activity, user);
            if (TrySendCommunication(sendCommunicationToDo, wf, activity))
                return Complete(history, wf, user);


            return history;
        }

        /// <summary>
        /// Will check if we need to send a communication and update the communication accordingly
        /// </summary>
        /// <param name="sendCommunicationToDo">The communication to send</param>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The configured activity</param>
        /// <returns>
        /// Whether the communication was sent or not
        /// </returns>
        private bool TrySendCommunication(ToDoItem sendCommunicationToDo, WorkflowInstance wf, WorkflowActivity activity)
        {
            CommunicationType defaultSendMethod = (CommunicationType?)activity.Metadata.GetRawValue<int?>("DefaultSendMethod") ?? CommunicationType.Mail;
            bool sendAutomatically = activity.Metadata.GetRawValue<bool?>("SendAutomatically") ?? false;
            dynamic wfState = wf.State;
            if (defaultSendMethod != CommunicationType.Email || !sendAutomatically)
                return false;


            using (var svc = new CommunicationService())
            {
                Communication comms = svc.CreateCommunication(sendCommunicationToDo);
                Case theCase = wf.Case;
                List<string> toContactTypeCodes = GetContactCodesFromMetadata("To", activity);
                List<string> ccContactTypeCodes = GetContactCodesFromMetadata("CC", activity);
                string employeeId = theCase.Employee.Id;
                string employerId = theCase.EmployerId;
                List<Contact> toContacts = toContactTypeCodes != null ? GetContactsByCodes(employeeId, theCase, toContactTypeCodes) : new List<Contact>();
                List<Contact> ccContacts = ccContactTypeCodes != null ? GetContactsByCodes(employeeId, theCase, ccContactTypeCodes) : new List<Contact>();

                /// Nobody to send to? Failed to send the communications
                if (toContacts.Count == 0 && ccContacts.Count == 0)
                    return false;

                comms.Recipients = toContacts;
                comms.CCRecipients = ccContacts;
                svc.SendCommunication(comms, sendCommunicationToDo);
            }
            sendCommunicationToDo.Status = ToDoItemStatus.Complete;
            sendCommunicationToDo.Save();
            return true;

        }

        /// <summary>
        /// Retrieves a comma separated list of values and turns them into a string array
        /// </summary>
        /// <param name="metadataKey"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        private List<string> GetContactCodesFromMetadata(string metadataKey, WorkflowActivity activity)
        {
            if (activity != null && activity.Metadata != null)
            {
                string csv = activity.Metadata.GetRawValue<string>(metadataKey);
                string[] separator = { "," };
                return !string.IsNullOrWhiteSpace(csv) ? csv.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
            }
            return null;
        }

        /// <summary>
        /// Gets the contacts by codes.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="contactTypeCodes">The contact type codes.</param>
        /// <returns></returns>
        private List<Contact> GetContactsByCodes(string employeeId, Case theCase, List<string> contactTypeCodes)
        {
            List<string> emails = contactTypeCodes.Where(s => s.Contains("@")).ToList();
            List<Contact> contacts = EmployeeContact.AsQueryable()
                .Where(e => e.EmployeeId == employeeId && contactTypeCodes.Contains(e.ContactTypeCode)).ToList()
                .Select(e => e.Contact).ToList();
            if (contacts == null) contacts = new List<Contact>();
            if (contactTypeCodes.Contains("SELF", StringComparer.OrdinalIgnoreCase))
            {
                string employeesEmail = theCase.Employee.Info.Email;

                if (!string.IsNullOrEmpty(theCase.Employee.Info.AltEmail))
                    employeesEmail = theCase.Employee.Info.AltEmail;

                contacts.Add(new Contact()
                {
                    Email = employeesEmail
                });
            }

            if (theCase.AuthorizedSubmitter != null && contactTypeCodes.Contains(theCase.AuthorizedSubmitter.ContactTypeCode))
            {
                contacts.Add(theCase.AuthorizedSubmitter.Contact);
            }

            foreach (string email in emails)
            {
                contacts.Add(new Contact()
                {
                    Email = email
                });
            }

            return contacts;
        }
    }
}
