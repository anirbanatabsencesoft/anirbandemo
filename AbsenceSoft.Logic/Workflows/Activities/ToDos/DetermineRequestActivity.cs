﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public class DetermineRequestActivity:BaseToDoActivity
    {
        /// <summary>
        /// The approved outcome value
        /// </summary>
        public const string ApprovedOutcomeValue = "Approved";
        /// <summary>
        /// The denied outcome value
        /// </summary>
        public const string DeniedOutcomeValue = "Denied";

        public override ToDoItemType ToDoType
        {
            get { return ToDoItemType.DetermineRequest; }
        }

        public override string Id
        {
            get { return "DetermineRequestActivity"; }
        }

        public override string Name
        {
            get { return "Determine Request"; }
        }

        public override string Description
        {
            get { return "Creates a Determine Request ToDo"; }
        }

        public override ActivityType ActivityType
        {
            get { return ActivityType.User; }
        }

        public override IEnumerable<string> GetPossibleOutcomes()
        {
            yield return ApprovedOutcomeValue;
            yield return DeniedOutcomeValue;
            yield return CompleteOutcomeValue;
            yield return ErrorOutcomeValue;
        }

        public override IEnumerable<EventType> GetPossibleEventTypes()
        {
            yield return EventType.TimeOffRequested;
        }

        public override bool AutoSave { get { return false; }
        }

        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            return base.IsValidToRun(wf, activity, user) && wf.StateHasProperties("IntermittentTimeRequestId") && wf.StateHasProperties("PassedCertification");
        }

        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            ToDoItem determineRequestTodo = base.CreateCaseToDo(wf, activity, user);
            dynamic wfState = wf.State;
            determineRequestTodo.Metadata.SetRawValue<Guid>("IntermittentTimeRequestId", (Guid)wfState.IntermittentTimeRequestId);
            determineRequestTodo.Save();
            ActivityHistory history = base.RunEnd(wf, activity, user);
            if (TryApproveRequest(determineRequestTodo, wf, activity, user))
                return base.Complete(history, wf, user);

            return history;
        }

        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            ToDoItem determineRequestTodo = ToDoItem.GetById(activity.Metadata.GetRawValue<string>("ToDoItemId"));
            Guid intermittentRequestId = determineRequestTodo.Metadata.GetRawValue<Guid>("IntermittentTimeRequestId");
            Case theCase = determineRequestTodo.Case;
            IntermittentTimeRequest request = theCase.Segments.SelectMany(s => s.UserRequests).FirstOrDefault(ur => ur.Id == intermittentRequestId);
            
            if (determineRequestTodo.ResultText == DeniedOutcomeValue)
                DenyIntermittentTimeRequest(theCase, request, user);
            else if (determineRequestTodo.ResultText == ApprovedOutcomeValue)
                ApproveIntermittentTimeRequest(theCase, request, user);

            using (var caseService = new CaseService())
            {
                caseService.UpdateCase(theCase);
            }

            return base.Complete(activity, wf, user);
        }

        private bool TryApproveRequest(ToDoItem determineRequestTodo, WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            bool approveAutomatically = activity.Metadata.GetRawValue<bool?>("ApproveAutomatically") ?? false;
            dynamic wfState = wf.State;
            if (!approveAutomatically || !wfState.PassedCertification)
                return false;

            Case theCase = Case.GetById(wf.CaseId);
            Guid intermittentRequestId = wfState.IntermittentTimeRequestId;
            IntermittentTimeRequest requestToApprove = theCase.Segments.SelectMany(s => s.UserRequests).FirstOrDefault(itr => itr.Id == intermittentRequestId);
            if (requestToApprove == null)
                return false;

            ApproveIntermittentTimeRequest(theCase, requestToApprove, user);
            using (var caseService = new CaseService())
            {
                caseService.UpdateCase(theCase);
            }
            determineRequestTodo.Status = ToDoItemStatus.Complete;
            determineRequestTodo.Save();

            return true;
        }

        private void ApproveIntermittentTimeRequest(Case theCase, IntermittentTimeRequest requestToApprove, Data.Security.User user = null)
        {
            requestToApprove.ManagerApproved = true;
            requestToApprove.ManagerApprovedDate = DateTime.UtcNow;
            IEnumerable<AppliedPolicyUsage> appliedPolicyUsage = GetMatchingPolicyUsages(theCase, requestToApprove);
            foreach (var usage in appliedPolicyUsage)
                usage.Determination = AdjudicationStatus.Approved;
            
            if (user == null)
            {
                requestToApprove.ManagerId = Data.Security.User.System.Id;
            }
            else
            {
                requestToApprove.ManagerId = user.Id;
            }

            foreach (var detail in requestToApprove.Detail)
            {
                detail.Approved = detail.Pending;
                detail.Pending = 0;
                detail.Denied = 0;
            }
        }

        private void DenyIntermittentTimeRequest(Case theCase, IntermittentTimeRequest requestToDeny, Data.Security.User user = null)
        {
            IEnumerable<AppliedPolicyUsage> appliedPolicyUsage = GetMatchingPolicyUsages(theCase, requestToDeny);
            foreach (var usage in appliedPolicyUsage)
                usage.Determination = AdjudicationStatus.Denied;
            
            foreach (var detail in requestToDeny.Detail)
            {
                detail.Denied = detail.Pending;
                detail.Approved = 0;
                detail.Pending = 0;
                
            }
        }

        private IEnumerable<AppliedPolicyUsage> GetMatchingPolicyUsages(Case theCase, IntermittentTimeRequest theRequest)
        {
            CaseSegment segmentToApprove = theCase.Segments.FirstOrDefault(s => s.UserRequests.Any(ur => ur.Id == theRequest.Id));
            IEnumerable<AppliedPolicyUsage> appliedPolicyUsage = segmentToApprove.AppliedPolicies
                .Where(ap => theRequest.Detail.Select(d => d.PolicyCode).Contains(ap.Policy.Code)).SelectMany(ap => ap.Usage);

            return appliedPolicyUsage;
        }
    }
}
