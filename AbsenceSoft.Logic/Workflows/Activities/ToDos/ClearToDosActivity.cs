﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Workflows.Activities.ToDos
{
    public sealed class ClearToDosActivity : Activity
    {
        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>
        /// The activity identifier.
        /// </value>
        public override string Id
        {
            get { return "ClearToDosActivity"; }
        }

        /// <summary>
        /// Gets the name of the activity.
        /// </summary>
        /// <value>
        /// The name of the activity.
        /// </value>
        public override string Name
        {
            get { return "Clear ToDos"; }
        }

        /// <summary>
        /// Gets the category of the activity.
        /// </summary>
        /// <value>
        /// The category of the activity.
        /// </value>
        public override string Category
        {
            get { return "ToDo"; }
        }

        /// <summary>
        /// Gets the activity description.
        /// </summary>
        /// <value>
        /// The activity description.
        /// </value>
        public override string Description
        {
            get { return "Clears the ToDos associated with this workflow, or optionally clears all ToDos associated with the case"; }
        }

        /// <summary>
        /// Tells if this activity has to run first.
        /// </summary>
        public override bool IsRunFirst
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the type of the activity.
        /// </summary>
        /// <value>
        /// The type of the activity.
        /// </value>
        public override ActivityType ActivityType
        {
            get { return ActivityType.Action; }
        }

        /// <summary>
        /// Determines whether the current activity is valid to run given the workflow instance, activity instance and user.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        ///   <c>true</c> if valid to run, otherwise <c>false</c>.
        /// </returns>
        public override bool IsValidToRun(WorkflowInstance wf, WorkflowActivity activity, User user = null)
        {
            if (wf == null || activity == null || user == null)
                return false;

            if (string.IsNullOrWhiteSpace(wf.CaseId))
                return false;

            return true;
        }

        /// <summary>
        /// Runs the specified activity on the provided workflow instance.
        /// </summary>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="activity">The activity instance/configuration that is used to store configuration
        /// level data about this specific instance of the activity to be run.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Run(WorkflowInstance wf, WorkflowActivity activity, Data.Security.User user = null)
        {
            bool closeAll = activity.Metadata.GetRawValue<bool?>("CloseAll") ?? false;
            ToDoItemType? type = activity.Metadata.GetRawValue<ToDoItemType?>("ToDoItemType");
            string title = activity.Metadata.GetRawValue<string>("Title");
            //
            // Get todo query
            IQueryable<ToDoItem> toDosToClear = ToDoItem.AsQueryable().Where(t => t.Status != ToDoItemStatus.Complete && t.Status != ToDoItemStatus.Cancelled);
            // filter by type if provided
            if (type.HasValue)
                toDosToClear = toDosToClear.Where(t => t.ItemType == type.Value);
            // if close all, then filter by case id, otherwise just this workflow
            if (closeAll)
                toDosToClear = toDosToClear.Where(t => t.CaseId == wf.CaseId);
            else
                toDosToClear = toDosToClear.Where(t => t.WorkflowInstanceId == wf.Id);
            if (!string.IsNullOrWhiteSpace(title))
                toDosToClear = toDosToClear.Where(t => t.Title == title);
            // enumerate the collection all at once
            var myList = toDosToClear.ToList();
            // loop through and modify all the todos
            foreach (ToDoItem todo in myList)
            {
                todo.Status = ToDoItemStatus.Cancelled;
                todo.ResultText = activity.Metadata.GetRawValue<string>("ResultText");
                todo.ModifiedById = user.Id;
                todo.Save();
            }

            ActivityHistory ah = base.RunEnd(wf, activity, user, CompleteOutcomeValue);
            return Complete(ah, wf, user);
        }

        /// <summary>
        /// Completes the specified activity; generally called after Run if no outcome is specified; meaning
        /// there is another completion step necessary, often after user input (like completing a ToDo, clicking a button,
        /// confirming some shit, etc.).
        /// </summary>
        /// <param name="activity">The activity history record that is to be completed/finished.</param>
        /// <param name="wf">The active workflow instance to run the activity against.</param>
        /// <param name="user">The user to which the activity is being run as (default is system user).</param>
        /// <returns>
        /// An activity history result, <c>saved</c>, with the given outcome for the activity.
        /// </returns>
        public override ActivityHistory Complete(ActivityHistory activity, WorkflowInstance wf, Data.Security.User user = null)
        {
            return activity;
        }
    }
}
