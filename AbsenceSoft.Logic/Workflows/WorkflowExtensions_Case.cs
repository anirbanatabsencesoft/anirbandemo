﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Bson;
using System;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on inquiry created.
        /// </summary>
        /// <param name="theIquiry">The inquiry.</param>
        /// <param name="user">The user.</param>
        public static void WfOnInquiryCreated(this Case theIquiry, User user = null)
        {
            theIquiry.WfOnRaiseInquiryEvent(EventType.InquiryCreated, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case created.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseCreated(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseCreated, user);
            if (theCase.WorkRelated != null && theCase.WorkRelated.IllnessOrInjuryDate.HasValue)
                theCase.WfOnWorkRelatedReportingChanged(user);
        }

        /// <summary>
        /// WORKFLOW: Called on case events updates.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseEventsUpdated(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseEventsUpdated, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case closed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseClosed(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseClosed, user);
        }

        /// <summary>
        /// WORKFLOW: Called on inquiry closed.
        /// </summary>
        /// <param name="theInquiry">The inquiry case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnInquiryClosed(this Case theInquiry, User user = null)
        {
            theInquiry.WfOnRaiseInquiryEvent(EventType.InquiryClosed, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case canceled.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseCanceled(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseCanceled, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case adjudicated.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="status">The status.</param>
        /// <param name="reasonCode">The reason.</param>
        /// <param name="denialReasonOther">The denial reason other.</param>
        /// <param name="reasonName">The reason.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseAdjudicated(this Case theCase, AdjudicationStatus status, string reasonCode, string denialReasonOther, string reasonName, User user = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseAdjudicated, user, m => m
                .SetRawValue("Determination", status)
                .SetRawValue("DenialReason", reasonCode)
                .SetRawValue("DenialReasonName", reasonName)
                .SetRawValue("DenialExplanation", denialReasonOther)
                .SetRawValue("StartDate", startDate)
                .SetRawValue("EndDate", endDate));
        }

        /// <summary>
        /// WORKFLOW: Called on case extended.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseExtended(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseExtended, user);
        }

        /// <summary>
        /// WORKFLOW: Called on inquiry accepted.
        /// </summary>
        /// <param name="theIquiry">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnInquiryAccepted(this Case theIquiry, User user = null)
        {
            theIquiry.WfOnRaiseInquiryEvent(EventType.CaseCreated, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case reopened.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseReopened(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseReopened, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case reduced schedule changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="newLeaveSchedule">The new leave schedule.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseReducedScheduleChanged(this Case theCase, Schedule newLeaveSchedule, User user = null)
        {
            Event e = StageCaseEvent(EventType.CaseReducedScheduleChanged, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("ScheduleId", newLeaveSchedule.Id);
            e.Save();
            var context = BuildContext<Schedule>(theCase, newLeaveSchedule);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Schedule>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on case reduced schedule changed.
        /// </summary>
        /// <param name="newLeaveSchedule">The new leave schedule.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseReducedScheduleChanged(this Schedule newLeaveSchedule, Case theCase, User user = null) { theCase.WfOnCaseReducedScheduleChanged(newLeaveSchedule, user); }

        /// <summary>
        /// WORKFLOW: Called on case type changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="segment">The segment.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseTypeChanged(this Case theCase, CaseSegment segment, User user = null)
        {
            Event e = StageCaseEvent(EventType.CaseTypeChanged, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("CaseSegmentId", segment.Id);
            e.Metadata.SetRawValue("Type", segment.Type);
            e.Save();
            var context = BuildContext<CaseSegment>(theCase, segment);
            context.ActiveSegment = segment;
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<CaseSegment>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on case type changed.
        /// </summary>
        /// <param name="segment">The segment.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseTypeChanged(this CaseSegment segment, Case theCase, User user = null) { theCase.WfOnCaseTypeChanged(segment, user); }

        /// <summary>
        /// WORKFLOW: Called on policy exhausted.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyExhausted(this Case theCase, AppliedPolicy policy, User user = null)
        {
            Event e = StageCaseEvent(EventType.PolicyExhausted, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("AppliedPolicyId", policy.Id);
            e.Metadata.SetRawValue("PolicyId", policy.Policy.Id);
            e.Metadata.SetRawValue("PolicyCode", policy.Policy.Code);
            e.Metadata.SetRawValue("FirstExhaustionDate", policy.FirstExhaustionDate);
            e.Save();
            var context = BuildContext<AppliedPolicy>(theCase, policy);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<AppliedPolicy>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on policy exhausted.
        /// </summary>
        /// <param name="policy">The policy.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyExhausted(this AppliedPolicy policy, Case theCase, User user = null) { theCase.WfOnPolicyExhausted(policy, user); }

        /// <summary>
        /// WORKFLOW: Called on policy adjudicated.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="status">The status.</param>
        /// <param name="reasonCode">The reason code.</param>
        /// <param name="denialReasonOther">The denial reason other.</param>
        /// <param name="reasonName">The reason name.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyAdjudicated(this Case theCase, AppliedPolicy policy, AdjudicationStatus status, string reasonCode, string denialReasonOther, string reasonName, User user = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            Event e = StageCaseEvent(EventType.PolicyAdjudicated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("AppliedPolicyId", policy.Id);
            e.Metadata.SetRawValue("PolicyId", policy.Policy.Id);
            e.Metadata.SetRawValue("PolicyCode", policy.Policy.Code);
            e.Metadata.SetRawValue("Determination", status);
            e.Metadata.SetRawValue("DenialReason", reasonCode);
            e.Metadata.SetRawValue("DenialReasonName", reasonName);
            e.Metadata.SetRawValue("DenialExplanation", denialReasonOther);
            e.Metadata.SetRawValue("StartDate", startDate);
            e.Metadata.SetRawValue("EndDate", endDate);
            e.Save();
            var context = BuildContext<AppliedPolicy>(theCase, policy);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<AppliedPolicy>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on policy adjudicated.
        /// </summary>
        /// <param name="policy">The policy.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="status">The status.</param>
        /// <param name="reasonCode">The reason code.</param>
        /// <param name="denialReasonOther">The denial reason other.</param>
        /// <param name="reasonName">The reason name.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyAdjudicated(this AppliedPolicy policy, Case theCase, AdjudicationStatus status, string reasonCode, string denialReasonOther, string reasonName, User user = null, DateTime? startDate = null, DateTime? endDate = null)
        { theCase.WfOnPolicyAdjudicated(policy, status, reasonCode, denialReasonOther, reasonName, user, startDate, endDate); }

        /// <summary>
        /// WORKFLOW: Called on policy manually added.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyManuallyAdded(this Case theCase, AppliedPolicy policy, User user = null)
        {
            Event e = StageCaseEvent(EventType.PolicyManuallyAdded, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("AppliedPolicyId", policy.Id);
            e.Metadata.SetRawValue("PolicyId", policy.Policy.Id);
            e.Metadata.SetRawValue("PolicyCode", policy.Policy.Code);
            e.Save();
            var context = BuildContext<AppliedPolicy>(theCase, policy);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<AppliedPolicy>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on policy manually added.
        /// </summary>
        /// <param name="policy">The policy.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyManuallyAdded(this AppliedPolicy policy, Case theCase, User user = null) { theCase.WfOnPolicyManuallyAdded(policy, user); }

        /// <summary>
        /// WORKFLOW: Called on policy manually deleted.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="policy">The policy.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyManuallyDeleted(this Case theCase, AppliedPolicy policy, User user = null)
        {
            Event e = StageCaseEvent(EventType.PolicyManuallyDeleted, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("AppliedPolicyId", policy.Id);
            e.Metadata.SetRawValue("PolicyId", policy.Policy.Id);
            e.Metadata.SetRawValue("PolicyCode", policy.Policy.Code);
            e.Save();
            var context = BuildContext<AppliedPolicy>(theCase, policy);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<AppliedPolicy>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on policy manually deleted.
        /// </summary>
        /// <param name="policy">The policy.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPolicyManuallyDeleted(this AppliedPolicy policy, Case theCase, User user = null) { theCase.WfOnPolicyManuallyDeleted(policy, user); }

        /// <summary>
        /// WORKFLOW: Called on case note created.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseNoteCreated(this CaseNote note, User user = null)
        {
            Event e = StageCaseEvent(EventType.CaseNoteCreated, note.CustomerId, note.EmployerId, note.Case.Employee.Id, note.CaseId, user);
            e.Metadata.SetRawValue("CaseNoteId", note.Id);
            e.Save();
            var context = BuildContext<CaseNote>(note.Case, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<CaseNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on case note changed.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseNoteChanged(this CaseNote note, User user = null)
        {
            Event e = StageCaseEvent(EventType.CaseNoteChanged, note.CustomerId, note.EmployerId, note.Case.Employee.Id, note.CaseId, user);
            e.Metadata.SetRawValue("CaseNoteId", note.Id);
            e.Save();
            var context = BuildContext<CaseNote>(note.Case, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<CaseNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on holiday changed/added.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnHolidayChanged(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.HolidayChanged, user);
        }

        /// <summary>
        /// WORKFLOW: Called on work related reporting changed/added.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkRelatedReportingChanged(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.WorkRelatedReportingChanged, user, d => d.SetRawValue("IllnessOrInjuryDate", theCase.WorkRelated.IllnessOrInjuryDate));
        }

        /// <summary>
        /// WORKFLOW: Raises a case event given the event type and provided user.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="type">The type.</param>
        /// <param name="user">The user.</param>
        public static void WfOnRaiseCaseEvent(this Case theCase, EventType type, User user = null, Action<BsonDocument> metaBuilder = null)
        {
            Event e = StageCaseEvent(type, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            if (metaBuilder != null)
                metaBuilder(e.Metadata);
            e.Save();
            var context = BuildContext<Case>(theCase, theCase);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Case>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Raises a case event given the event type and provided user.
        /// </summary>
        /// <param name="theInquiry">The case.</param>
        /// <param name="type">The type.</param>
        /// <param name="user">The user.</param>
        public static void WfOnRaiseInquiryEvent(this Case theInquiry, EventType type, User user = null, Action<BsonDocument> metaBuilder = null)
        {
            WfOnRaiseCaseEvent(theInquiry, type, user, metaBuilder);
        }

        /// <summary>
        /// WORKFLOW: Raises a case event given the event type and provided user.
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="user"></param>
        public static void WfOnCustomFieldChangeEvent(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseCustomFieldChanged, user);
        }

        /// <summary>
        /// Raises a case event for OnContactInfoChangeEvent
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="user"></param>
        public static void WfOnContactInfoChangeEvent(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseContactInfoChanged, user);
        }

        /// <summary>
        /// WORKFLOW: Called on case relapsed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCaseRelapsed(this Case theCase, User user = null)
        {
            theCase.WfOnRaiseCaseEvent(EventType.CaseRelapsed, user);            
        }
    }
}
