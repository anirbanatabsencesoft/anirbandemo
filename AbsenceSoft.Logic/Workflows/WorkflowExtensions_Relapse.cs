﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        public static void WfOnRelapse(this Case theCase, Relapse relapse, User user = null)
        {
            Event e = StageCaseEvent(EventType.CaseRelapsed, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("RelapseId", relapse.Id);
            e.Metadata.SetRawValue("RelapseStartDate", relapse.StartDate);
            e.Metadata.SetRawValue("RelapseEndDate", relapse.EndDate);
            e.Save();
            var context = BuildContext<Relapse>(theCase, relapse);
            using (WorkflowService wf = new WorkflowService(user))
            {
                wf.RaiseEvent<Relapse>(e, context);
            }
        }
    }
}
