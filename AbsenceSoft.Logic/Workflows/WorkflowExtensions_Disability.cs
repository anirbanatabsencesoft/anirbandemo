﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on pay period submitted.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="payPeriod">The pay period.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPayPeriodSubmitted(this Case theCase, PayPeriod payPeriod, User user = null)
        {
            Event e = StageCaseEvent(EventType.PayPeriodSubmitted, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("PayPeriodId", payPeriod.Id);
            e.Save();
            var context = BuildContext<PayPeriod>(theCase, payPeriod);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<PayPeriod>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on pay period submitted.
        /// </summary>
        /// <param name="payPeriod">The pay period.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPayPeriodSubmitted(this PayPeriod payPeriod, Case theCase, User user = null) { theCase.WfOnPayPeriodSubmitted(payPeriod, user); }

        /// <summary>
        /// WORKFLOW: Called on diagnosis created.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisCreated(this Case theCase, DiagnosisCode diagnosis, User user = null)
        {
            Event e = StageCaseEvent(EventType.DiagnosisCreated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("DiagnosisId", diagnosis.Id);
            e.Metadata.SetRawValue("DiagnosisCode", diagnosis.Code);
            e.Save();
            var context = BuildContext<DiagnosisCode>(theCase, diagnosis);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<DiagnosisCode>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on diagnosis created.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisCreated(this DiagnosisCode diagnosis, Case theCase, User user = null) { theCase.WfOnDiagnosisCreated(diagnosis, user); }

        /// <summary>
        /// WORKFLOW: Called on diagnosis changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisChanged(this Case theCase, DiagnosisCode diagnosis, User user = null)
        {
            Event e = StageCaseEvent(EventType.DiagnosisChanged, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("DiagnosisId", diagnosis.Id);
            e.Metadata.SetRawValue("DiagnosisCode", diagnosis.Code);
            e.Save();
            var context = BuildContext<DiagnosisCode>(theCase, diagnosis);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<DiagnosisCode>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on diagnosis changed.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisChanged(this DiagnosisCode diagnosis, Case theCase, User user = null) { theCase.WfOnDiagnosisChanged(diagnosis, user); }

        /// <summary>
        /// WORKFLOW: Called on diagnosis deleted.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisDeleted(this Case theCase, DiagnosisCode diagnosis, User user = null)
        {
            Event e = StageCaseEvent(EventType.DiagnosisDeleted, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("DiagnosisId", diagnosis.Id);
            e.Metadata.SetRawValue("DiagnosisCode", diagnosis.Code);
            e.Save();
            var context = BuildContext<DiagnosisCode>(theCase, diagnosis);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<DiagnosisCode>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on diagnosis deleted.
        /// </summary>
        /// <param name="diagnosis">The diagnosis.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnDiagnosisDeleted(this DiagnosisCode diagnosis, Case theCase, User user = null) { theCase.WfOnDiagnosisDeleted(diagnosis, user); }

        /// <summary>
        /// WORKFLOW: Called on work restriction created.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="restriction">The restriction.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkRestrictionCreated(this Case theCase, EmployeeRestriction restriction, User user = null)
        {
            Event e = StageCaseEvent(EventType.WorkRestrictionCreated, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("WorkRestrictionId", restriction.Id);
            e.Save();
            var context = BuildContext<EmployeeRestriction>(theCase, restriction);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeRestriction>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on work restriction created.
        /// </summary>
        /// <param name="restriction">The restriction.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkRestrictionCreated(this EmployeeRestriction restriction, Case theCase, User user = null) { theCase.WfOnWorkRestrictionCreated(restriction, user); }

        /// <summary>
        /// WORKFLOW: Called on work restriction changed.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="restriction">The restriction.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkRestrictionChanged(this Case theCase, WorkRestriction restriction, User user = null)
        {
            Event e = StageCaseEvent(EventType.WorkRestrictionChanged, theCase.CustomerId, theCase.EmployerId, theCase.Employee.Id, theCase.Id, user);
            e.Metadata.SetRawValue("WorkRestrictionId", restriction.Id);
            e.Save();
            var context = BuildContext<WorkRestriction>(theCase, restriction);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<WorkRestriction>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on work restriction changed.
        /// </summary>
        /// <param name="restriction">The restriction.</param>
        /// <param name="theCase">The case.</param>
        /// <param name="user">The user.</param>
        public static void WfOnWorkRestrictionChanged(this WorkRestriction restriction, Case theCase, User user = null) { theCase.WfOnWorkRestrictionChanged(restriction, user); }
    }
}
