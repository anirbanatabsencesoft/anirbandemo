﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;
using MongoDB.Bson;
using System;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on employee necessity changed.
        /// </summary>
        /// <param name="necessity">The employee necessity.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeNecessityChanged(this EmployeeNecessity necessity, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeNecessityChanged, necessity.CustomerId, necessity.EmployerId, necessity.Employee.Id, user);
            e.Metadata.SetRawValue("EmployeeNecessityId", necessity.Id);
            e.Metadata.SetRawValue("NecessityId", necessity.NecessityId);
            e.CaseId = necessity.CaseId; // If this is in relation to a case
            e.Save();
            var context = BuildContext<EmployeeNecessity>(necessity.Employee, necessity);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeNecessity>(e, context);
        }
    }
}
