﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on employee job changed.
        /// </summary>
        /// <param name="organization">The employee job.</param>
        /// <param name="user">The user.</param>
        public static void WfOnEmployeeOfficeLocationChanged(this EmployeeOrganization organization, User user = null)
        {
            Event e = StageEmployeeEvent(EventType.EmployeeOfficeLocationChanged, organization.CustomerId, organization.EmployerId, organization.Employee.Id, user);
            e.Metadata.SetRawValue("EmployeeOrganizationId", organization.Id);
            e.Metadata.SetRawValue("OrganizationCode", organization.Code);
            e.Save();
            var context = BuildContext<EmployeeOrganization>(organization.Employee, organization);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<EmployeeOrganization>(e, context);
        }
    }
}
