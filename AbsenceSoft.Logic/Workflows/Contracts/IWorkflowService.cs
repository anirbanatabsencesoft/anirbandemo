﻿using System;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Workflows.Contracts
{
    public interface IWorkflowService : IDisposable
    {
        Workflow GetWorkflowById(string workflowId);
        Workflow GetByCode(string code, string customerId = null, string employerId = null, bool includeSuppressed = false);
        WorkflowInstance CreateInstance(Workflow wf, Event e, DynamicAwesome state = null);
        WorkflowInstance UpdateWorkflowInstance(WorkflowInstance wfi);
        void Run(WorkflowInstance instance);
        void Run(string workflowCode, string caseId, Guid? activityId);
    }
}
