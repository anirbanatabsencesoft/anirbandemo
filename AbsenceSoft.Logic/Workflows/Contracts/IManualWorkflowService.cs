﻿using System;
using System.Collections.Generic;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Workflows.Contracts
{
    public interface IManualWorkflowService : IDisposable
    {
        Event BuildEvent(string workflowId, string employeeId = null, string caseId = null, List<WorkflowCriteria> criteria = null);
    }
}
