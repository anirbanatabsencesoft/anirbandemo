﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Workflows
{
    [Serializable]
    public class RuleEvaluatorContext<T> : LeaveOfAbsence where T : class, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}"/> class.
        /// </summary>
        public RuleEvaluatorContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}" /> class.
        /// </summary>
        /// <param name="metadata">The metadata document(s) which should be merged into this instance's.</param>
        public RuleEvaluatorContext(params BsonDocument[] metadata) : this()
        {
            foreach (BsonDocument doc in metadata.Where(m => m != null))
                this.Metadata.Merge(doc);
            this.State = new DynamicAwesome(this.Metadata);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}"/> class.
        /// </summary>
        /// <param name="stateData">The state data.</param>
        public RuleEvaluatorContext(DynamicAwesome stateData) : this(stateData.AsBsonDocument) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}"/> class.
        /// </summary>
        /// <param name="eventTarget">The event target that this rule evaluator context is being created for.</param>
        public RuleEvaluatorContext(T eventTarget) : this() { EventTarget = eventTarget; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}" /> class.
        /// </summary>
        /// <param name="wf">The workflow instance to build the evaluator context from.</param>
        public RuleEvaluatorContext(WorkflowInstance wf)
            : this((wf == null ? null : wf.Metadata), (wf == null ? null : wf.State == null ? null : wf.State.AsBsonDocument))
        {
            if (wf == null)
                return;

            WorkflowInstance = wf;
            LoadFromWorkflowInstanceAndMetadata(wf.Event);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleEvaluatorContext{T}"/> class.
        /// </summary>
        /// <param name="e">The event instance to build the evaluator context from.</param>
        public RuleEvaluatorContext(Event e) 
            : this(e == null ? null : e.Metadata)
        {
            if (e == null)
                return;

            Event = e;
            LoadFromWorkflowInstanceAndMetadata(e); 
        }

        /// <summary>
        /// Gets or sets the current event target.
        /// </summary>
        /// <value>
        /// The current event target.
        /// </value>
        public T EventTarget { get; set; }

        /// <summary>
        /// Gets or sets the Paperwork
        /// </summary>
        public CommunicationPaperwork Paperwork { get; set; }

        /// <summary>
        /// Gets or sets the event.
        /// </summary>
        /// <value>
        /// The event.
        /// </value>
        public Event Event { get; set; }

        /// <summary>
        /// Gets or sets the workflow instance.
        /// </summary>
        /// <value>
        /// The workflow instance.
        /// </value>
        public WorkflowInstance WorkflowInstance { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public dynamic State { get; set; }

        /// <summary>
        /// Determines whether [has related spouse case].
        /// </summary>
        /// <returns></returns>
        public bool HasRelatedSpouseCase()
        {
            return Case == null ? false : !string.IsNullOrWhiteSpace(Case.SpouseCaseId);
        }

        /// <summary>
        /// Determines whether this instance is eligible.
        /// </summary>
        /// <returns></returns>
        public bool IsEligible()
        {
            return AnyMatchesEligibilityStatus(EligibilityStatus.Eligible);
        }

        /// <summary>
        /// Determines whether any policy is ineligible.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is any ineligible]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAnyIneligible()
        {
            return AnyMatchesEligibilityStatus(EligibilityStatus.Ineligible);
        }

        /// <summary>
        /// Determines whether any policy matches 
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        private bool AnyMatchesEligibilityStatus(EligibilityStatus status)
        {
            if (Case == null || Case.Segments == null || !Case.Segments.Any())
                return false;
            return Case.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Status == status);
        }

        /// <summary>
        /// Determines whether the leave has specific policy applied.
        /// </summary>
        /// <returns></returns>
        public bool HasSpecificPolicy(string policyCode)
        {
            if (Case == null || Case.Segments == null || !Case.Segments.Any())
                return false;
            return (Case.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.Code == policyCode && p.Status == EligibilityStatus.Eligible));
        }

        /// <summary>
        /// Determines whether the case is intermittent irrespective of any status
        /// </summary>
        public bool IsIntermittent()
        {
            if (Case == null || Case.Segments == null || !Case.Segments.Any())
            {
                return false;
            }
            return Case.Segments.Any(s => s.Type == CaseType.Intermittent && s.Status != CaseStatus.Cancelled);
        }

        public bool HasAnyPolicyType(PolicyType type)
        {
            if (Case == null || Case.Segments == null || !Case.Segments.Any())
                return false;

            return Case.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.PolicyType == type);
        }

        /// <summary>
        /// Determines whether accomodation denial reason contains any/one of the selected denial reason from rule/condition
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasAccomodationDenialReason(string reasons)
        {
            if (Case == null || Case.AccommodationRequest == null || Case.AccommodationRequest.Accommodations == null || !Case.AccommodationRequest.Accommodations.Any() || string.IsNullOrWhiteSpace(reasons))
                return false;

            string[] selectedreasons = reasons.Split(new char[] { '~' });
            if (selectedreasons.Length > 0)
            {
                foreach (string reason in selectedreasons)
                {
                    DateTime? startDate = Event.Metadata.GetRawValue<DateTime?>("StartDate");
                    DateTime? endDate = Event.Metadata.GetRawValue<DateTime?>("EndDate");
                    Guid accommodationId = Event.Metadata.GetRawValue<Guid>("AccommodationId");
                    if (accommodationId == Guid.Empty &&
                        Case.AccommodationRequest.Accommodations.OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).OrderByDescending(d => d.DeterminationDate).FirstOrDefault().DenialReasonCode == reason) return true;

                    if (Case.AccommodationRequest.Accommodations.Where(a => a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Any(o => o.StartDate >= startDate && o.EndDate <= endDate) &&
                        Case.AccommodationRequest.Accommodations.Where(a => a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Where(o => o.StartDate >= startDate && o.EndDate <= endDate).OrderByDescending(d => d.DeterminationDate).FirstOrDefault().DenialReasonCode == reason) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether accomodation denial reason contains any/one of the selected denial reason from rule/condition
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasAccomodationDenialReasonGroup(string reasons)
        {
            return HasAccomodationDenialReason(reasons);
        }

        /// <summary>
        /// Determines whether policy denial reason contains any/one of the selected denial reason from rule/condition
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasPolicyDenialReason(string reasons)
        {
            if (Case == null || Case.Segments == null || !Case.Segments.Any() || string.IsNullOrWhiteSpace(reasons))
                return false;

            string[] selectedreasons = reasons.Split(new char[] { '~' });
            if (selectedreasons.Length > 0)
            {
                foreach (string reason in selectedreasons)
                {
                    DateTime? startDate = Event.Metadata.GetRawValue<DateTime?>("StartDate");
                    DateTime? endDate = Event.Metadata.GetRawValue<DateTime?>("EndDate");
                    if (startDate != null && endDate != null)
                    {
                        if (Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Usage.Any(o => o.DenialReasonCode == reason && o.DateUsed >= startDate && o.DateUsed <= endDate))) return true;
                    }
                    else
                    {
                        if (Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Usage.Any(o => o.DenialReasonCode == reason))) return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether policy denial reason contains any/one of the selected denial reason from rule/condition
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasPolicyDenialReasonGroup(string reasons)
        {
            return HasPolicyDenialReason(reasons);
        }

        /// <summary>
        /// Determines whether policy is exhausted based of the adjudication status and adjudication denial reason
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasPolicyBeenExhausted(string policycode)
        {
            if (Case == null || Case.Summary == null || !Case.Summary.Policies.Any() || string.IsNullOrWhiteSpace(policycode))
                return false;

            return Case.Summary.Policies.FirstOrDefault(e => e.PolicyCode == policycode)
                    .Detail.Any(d => d.Determination == AdjudicationSummaryStatus.Denied && d.DenialReasonCode == AdjudicationDenialReason.Exhausted);
        }

        /// <summary>
        /// Determines whether policy status contains one of selected statuses
        /// </summary>
        /// <param name="policyType"></param>
        /// <param name="policyStatuses"></param>
        /// <returns></returns>
        public bool HasPolicyStatus(PolicyType policyType, object policyStatuses)
        {
            if (Case == null || Case.Summary == null || !Case.Summary.Policies.Any() || policyStatuses == null)
                return false;

            bool returnStatus = false;
            string[] selectedstatuses = policyStatuses.ToString().Split(new char[] { '~' });
            if (selectedstatuses.Length > 0)
            {
                DateTime? startDate = Event.Metadata.GetRawValue<DateTime?>("StartDate");
                DateTime? endDate = Event.Metadata.GetRawValue<DateTime?>("EndDate");
                foreach (string status in selectedstatuses)
                {
                    switch (int.Parse(status))
                    {
                        case -1:
                            returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Status == EligibilityStatus.Ineligible);
                            break;
                        case 0:
                            returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Status == EligibilityStatus.Pending);
                            break;
                        case 1:
                            returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Status == EligibilityStatus.Eligible);
                            break;
                        case 2:
                            returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Status == EligibilityStatus.Approved);
                            break;
                        case 3:
                            if (startDate != null && endDate != null)
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Pending && o.DateUsed >= startDate && o.DateUsed <= endDate));
                            }
                            else
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Pending));
                            }
                            break;
                        case 4:
                            if (startDate != null && endDate != null)
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Approved && o.DateUsed >= startDate && o.DateUsed <= endDate));
                            }
                            else
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Approved));
                            }
                            break;
                        case 5:
                            if (startDate != null && endDate != null)
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Denied && o.DateUsed >= startDate && o.DateUsed <= endDate));
                            }
                            else
                            {
                                returnStatus = Case.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault().AppliedPolicies.Any(p => p.Policy.PolicyType == policyType && p.Usage.Any(o => o.Determination == AdjudicationStatus.Denied));
                            }
                            break;
                        case 6:
                            returnStatus = HasPolicyBeenExhausted(Case.Segments.SelectMany(p => p.AppliedPolicies).Where(p => p.Policy.PolicyType == policyType).Select(o => o.Policy.Code).FirstOrDefault());
                            break;
                    }

                    if (returnStatus) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether accomodation type's adjudication status contains the selected status
        /// </summary>
        /// <param name="accomodationType"></param>
        /// <param name="adjudicationStatuses"></param>
        /// <returns></returns>
        public bool HasAccomodationStatus(string accomodationType, object adjudicationStatuses)
        {
            if (Case == null || Case.AccommodationRequest == null || Case.AccommodationRequest.Accommodations == null || !Case.AccommodationRequest.Accommodations.Any() || (adjudicationStatuses == null) || string.IsNullOrWhiteSpace(accomodationType))
                return false;

            string[] selectedStatuses = adjudicationStatuses.ToString().Split(new char[] { '~' });
            if (selectedStatuses.Length > 0)
            {
                DateTime? startDate = Event.Metadata.GetRawValue<DateTime?>("StartDate");
                DateTime? endDate = Event.Metadata.GetRawValue<DateTime?>("EndDate");
                Guid accommodationId = Event.Metadata.GetRawValue<Guid>("AccommodationId");

                foreach (string status in selectedStatuses)
                {
                    if (accommodationId == Guid.Empty && Case.AccommodationRequest.Accommodations.Where(a => a.Type.Code == accomodationType).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).OrderByDescending(d => d.DeterminationDate).FirstOrDefault().Determination == (AdjudicationStatus)Enum.Parse(typeof(AdjudicationStatus), status)) return true;
                    
                    if ((AdjudicationStatus)Enum.Parse(typeof(AdjudicationStatus), status) != AdjudicationStatus.Pending)
                    {
                        if (Case.AccommodationRequest.Accommodations.Where(a => a.Type.Code == accomodationType && a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Any(o => o.StartDate >= startDate && o.EndDate <= endDate) && Case.AccommodationRequest.Accommodations.Where(a => a.Type.Code == accomodationType && a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Where(o => o.StartDate >= startDate && o.EndDate <= endDate).OrderByDescending(d => d.DeterminationDate).FirstOrDefault().Determination == (AdjudicationStatus)Enum.Parse(typeof(AdjudicationStatus), status)) return true;
                    }
                    else
                    {
                        if (Case.AccommodationRequest.Accommodations.Where(a => a.Type.Code == accomodationType && a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Any(o => o.StartDate >= startDate && o.EndDate <= endDate) && Case.AccommodationRequest.Accommodations.Where(a => a.Type.Code == accomodationType && a.Id == accommodationId).OrderByDescending(a => a.CreatedDate).SelectMany(p => p.Usage).Where(o => o.StartDate >= startDate && o.EndDate <= endDate).OrderBy(d => d.DeterminationDate).FirstOrDefault().Determination == (AdjudicationStatus)Enum.Parse(typeof(AdjudicationStatus), status)) return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether accomodation type equal to the selected accommodation type from rule/condition
        /// </summary>
        /// <param name="reasons"></param>
        /// <returns></returns>
        public bool HasAccomodationType(string accommodationType)
        {
            if (Case == null || Case.AccommodationRequest == null || Case.AccommodationRequest.Accommodations == null || !Case.AccommodationRequest.Accommodations.Any() || string.IsNullOrWhiteSpace(accommodationType))
                return false;

            Guid accommodationId = Event.Metadata.GetRawValue<Guid>("AccommodationId");

            if (accommodationId == Guid.Empty && Case.AccommodationRequest.Accommodations.OrderByDescending(a => a.CreatedDate).FirstOrDefault().Type.Code == accommodationType) return true;

            if (Case.AccommodationRequest.Accommodations.Any(a => a.Type.Code == accommodationType && a.Id == accommodationId)) return true;

            return false;
        }

        /// <summary>
        /// Determines whether this instance is eligible.
        /// </summary>
        /// <returns></returns>
        public bool IsPolicyPending()
        {
            if (Case == null || Case.Summary == null || !Case.Summary.Policies.Any())
                return false;

            return Case.Summary.Policies.SelectMany(p => p.Detail).Any(d => d.Determination == AdjudicationSummaryStatus.Pending);

            //return Case.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Status == EligibilityStatus.Eligible);
            
            //if (Case != null && Case.Summary != null && Case.Summary.Determination == AdjudicationStatus.Pending)
            //    return true;

            //return false;
        }

        /// <summary>
        /// Metadatas the value as.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public object MetadataValueAs(string propertyName, string typeName)
        {
            Type T = Type.GetType(typeName);
            if (T == null)
                return null;
            return this.Metadata.GetRawValue(propertyName, T);
        }

        /// <summary>
        /// Metadatas the value is.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="compare">The compare.</param>
        /// <returns></returns>
        public bool MetadataValueIs(string propertyName, string typeName, object compare = null)
        {
            object val = MetadataValueAs(propertyName, typeName);
            if (val == null && compare == null)
                return true;
            if (val == null || compare == null)
                return false;
            return val.Equals(compare);
        }

        /// <summary>
        /// Case Metadatas the value is.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="compare">The compare.</param>
        /// <returns></returns>
        public bool CaseMetadataValueIs(string propertyName, string typeName, object compare)
        {
            Type T = Type.GetType(typeName);
            if (T == null)
                return compare == null;
            var val = Case.Metadata.GetRawValue(propertyName, T);
            if (val == null)
                return compare == null;
            return val.Equals(compare);
        }

        /// <summary>
        /// Gets the determination.
        /// </summary>
        /// <returns></returns>
        public bool IsDetermination(AdjudicationStatus status)
        {
            return this.Metadata.GetRawValue<AdjudicationStatus?>("Determination") == status;
        }

        /// <summary>
        /// Determines whether the employee's nth birthday occurrs during the case date range.
        /// </summary>
        /// <param name="nthBirthday">The nth birthday to check for.</param>
        /// <returns></returns>
        public bool IsEmployeeNthBirthdayDuringCase(int nthBirthday)
        {
            if (Employee != null && Employee.DoB.HasValue)
            {
                var age65Date = Employee.DoB.Value.AddYears(nthBirthday);
                return age65Date.DateInRange(Case.StartDate, Case.EndDate);
            }
            return false;
        }

        /// <summary>
        /// Loads all of the instance properties and stuff from the workflow instance and
        /// metadata super-magically with voodoo magic potions 'n' stuff.
        /// </summary>
        /// <param name="e">The event instance to build the evaluator context from.</param>
        private void LoadFromWorkflowInstanceAndMetadata(Event e)
        {
            try
            {
                Event = e;
                string todoId = Metadata.GetRawValue<string>("ToDoItemId");
                if (!string.IsNullOrWhiteSpace(todoId))
                {
                    ToDoItem todo = ToDoItem.GetById(todoId);
                    if (todo != null)
                    {
                        Customer = todo.Customer;
                        Employer = todo.Employer;
                        Employee = todo.Employee;
                        Case = todo.Case;
                        ActiveWorkFlowItem = todo;
                        WorkflowInstance = WorkflowInstance ?? todo.WorkflowInstance;
                        State = new DynamicAwesome(todo.Metadata.Merge(this.Metadata));
                    }
                }

                Customer = Customer ?? e.Customer;
                Employer = Employer ?? e.Employer;
                Employee = Employee ?? e.Employee;
                Case = Case ?? e.Case;
                
                // Get the Case or Employee out first, those are the easy ones
                EventTarget = Case as T;
                if (EventTarget != null) return;
                EventTarget = Employee as T;
                if (EventTarget != null) return;

                if (Case != null && ActiveSegment == null)
                {
                    Guid? segmentId = Metadata.GetRawValue<Guid?>("CaseSegmentId");
                    if (segmentId.HasValue && Case.Segments != null)
                        ActiveSegment = Case.Segments.FirstOrDefault(s => s.Id == segmentId);
                    EventTarget = ActiveSegment as T;
                }
                if (EventTarget != null) return;

                if (Employee != null)
                {
                    Guid? scheduleId = Metadata.GetRawValue<Guid?>("WorkScheduleId");
                    if (scheduleId.HasValue && Employee.WorkSchedules != null)
                        EventTarget = Employee.WorkSchedules.FirstOrDefault(s => s.Id == scheduleId) as T;
                }
                if (EventTarget != null) return;

                string noteId = Metadata.GetRawValue<string>("EmployeeNoteId");
                if (!string.IsNullOrWhiteSpace(noteId))
                {
                    EmployeeNote note = EmployeeNote.GetById(noteId);
                    if (note != null)
                    {
                        Customer = Customer ?? note.Customer;
                        Employer = Employer ?? note.Employer;
                        Employee = Employee ?? note.Employee;
                        EventTarget = note as T;
                    }
                }
                if (EventTarget != null) return;

                if (Case != null)
                {
                    Guid? scheduleId = Metadata.GetRawValue<Guid?>("ScheduleId");
                    if (scheduleId.HasValue && Case.Segments != null)
                        EventTarget = Case.Segments.SelectMany(s => s.LeaveSchedule.Where(l => l.Id == scheduleId)).FirstOrDefault() as T;
                    if (EventTarget != null) return;

                    Guid? policyId = Metadata.GetRawValue<Guid?>("AppliedPolicyId");
                    if (policyId.HasValue && Case.Segments != null)
                        EventTarget = Case.Segments.SelectMany(s => s.AppliedPolicies.Where(p => p.Id == policyId)).FirstOrDefault() as T;
                    if (EventTarget != null) return;

                    Guid? torId = Metadata.GetRawValue<Guid?>("IntermittentTimeRequestId");
                    if (torId.HasValue && Case.Segments != null)
                        EventTarget = Case.Segments.SelectMany(s => s.UserRequests.Where(i => i.Id == torId)).FirstOrDefault() as T;
                    if (EventTarget != null) return;

                    Guid? certId = Metadata.GetRawValue<Guid?>("CertificationId");
                    if (certId.HasValue && Case.Certifications != null)
                        EventTarget = Case.Certifications.FirstOrDefault(c => c.Id == certId) as T;
                    if (EventTarget != null) return;

                    Guid? payPeriodId = Metadata.GetRawValue<Guid?>("PayPeriodId");
                    if (payPeriodId.HasValue && Case.Pay != null && Case.Pay.PayPeriods != null)
                        EventTarget = Case.Pay.PayPeriods.FirstOrDefault(p => p.Id == payPeriodId) as T;
                    if (EventTarget != null) return;

                    string diagId = Metadata.GetRawValue<string>("DiagnosisId");
                    if (!string.IsNullOrWhiteSpace(diagId))
                        EventTarget = DiagnosisCode.GetById(diagId) as T;
                    if (EventTarget != null) return;

                    string wrId = Metadata.GetRawValue<string>("WorkRestrictionId");
                    if (!string.IsNullOrWhiteSpace(wrId))
                        EventTarget = EmployeeRestriction.GetById(wrId) as T;
                    if (EventTarget != null) return;

                    Guid? accomId = Metadata.GetRawValue<Guid?>("AccommodationId");
                    if (accomId.HasValue && Case.AccommodationRequest != null && Case.AccommodationRequest.Accommodations != null)
                        EventTarget = Case.AccommodationRequest.Accommodations.FirstOrDefault(w => w.Id == accomId) as T;
                    if (EventTarget != null) return;
                }

                noteId = Metadata.GetRawValue<string>("CaseNoteId");
                if (!string.IsNullOrWhiteSpace(noteId))
                {
                    CaseNote note = CaseNote.GetById(noteId);
                    if (note != null)
                    {
                        Customer = Customer ?? note.Customer;
                        Employer = Employer ?? note.Employer;
                        Case = Case ?? note.Case;
                        Employee = Employee ?? Data.Customers.Employee.GetById(note.Case.Employee.Id) ?? note.Case.Employee;
                        EventTarget = note as T;
                    }
                }
                if (EventTarget != null) return;

                noteId = Metadata.GetRawValue<string>("CommunicationNoteId");
                if (!string.IsNullOrWhiteSpace(noteId))
                {
                    CommunicationNote note = CommunicationNote.GetById(noteId);
                    if (note != null)
                    {
                        Customer = Customer ?? note.Customer;
                        Employer = Employer ?? note.Employer;
                        Case = Case ?? note.Communication.Case;
                        Employee = Employee ?? Data.Customers.Employee.GetById(note.Communication.Case.Employee.Id) ?? note.Communication.Case.Employee;
                        EventTarget = note as T;
                    }
                }
                if (EventTarget != null) return;

                string commId = Metadata.GetRawValue<string>("CommunicationId");
                if (!string.IsNullOrWhiteSpace(commId))
                {
                    Communication comm = Communication.GetById(commId);
                    if (comm != null)
                    {
                        Customer = Customer ?? comm.Customer;
                        Employer = Employer ?? comm.Employer;
                        Case = Case ?? comm.Case;
                        Employee = Employee ?? comm.Employee ?? (comm.Case == null ? null : comm.Case.Employee);
                        EventTarget = comm as T;
                    }
                }
                if (EventTarget != null) return;

                string attId = Metadata.GetRawValue<string>("AttachmentId");
                if (!string.IsNullOrWhiteSpace(attId))
                {
                    Attachment att = Attachment.GetById(attId);
                    if (att != null)
                    {
                        Customer = Customer ?? att.Customer;
                        Employer = Employer ?? att.Employer;
                        Case = Case ?? att.Case;
                        Employee = Employee ?? att.Employee ?? (att.Case == null ? null : att.Case.Employee);
                        EventTarget = att as T;
                    }
                }
                if (EventTarget != null) return;

                string orgId = Metadata.GetRawValue<string>("EmployeeOrganizationId");
                if (!string.IsNullOrWhiteSpace(orgId))
                {
                    EmployeeOrganization org = EmployeeOrganization.GetById(orgId);
                    if (org != null)
                    {
                        Customer = Customer ?? org.Customer;
                        Employer = Employer ?? org.Employer;
                        Case = Case ?? null;
                        Employee = Employee ?? org.Employee;
                        EventTarget = org as T;
                    }
                }
                if (EventTarget != null) return;

                string jobId = Metadata.GetRawValue<string>("EmployeeJobId");
                if (!string.IsNullOrWhiteSpace(jobId))
                {
                    EmployeeJob job = EmployeeJob.GetById(jobId);
                    if (job != null)
                    {
                        Customer = Customer ?? job.Customer;
                        Employer = Employer ?? job.Employer;
                        Case = Case ?? job.Case;
                        Employee = Employee ?? job.Employee ?? (job.Case != null ? job.Case.Employee : null);
                        EventTarget = job as T;
                    }
                }
                if (EventTarget != null) return;

                string necessityId = Metadata.GetRawValue<string>("EmployeeNecessityId");
                if (!string.IsNullOrWhiteSpace(necessityId))
                {
                    EmployeeNecessity nec = EmployeeNecessity.GetById(necessityId);
                    if (nec != null)
                    {
                        Customer = Customer ?? nec.Customer;
                        Employer = Employer ?? nec.Employer;
                        Case = Case ?? nec.Case;
                        Employee = Employee ?? nec.Employee ?? (nec.Case != null ? nec.Case.Employee : null);
                        EventTarget = nec as T;
                    }
                }
                if (EventTarget != null) return;
            }
            catch (Exception ex)
            {
                Log.Error("Error loading rule evaluator context in WF from WorkflowInstance + Metadata", ex);
            }
        }
    }
}
