﻿using AbsenceSoft.Common;
using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Rules;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Pay;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Workflows.Contracts;

namespace AbsenceSoft.Logic.Workflows
{
    public class ManualWorkflowService : LogicService, ILogicService, IManualWorkflowService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManualWorkflowService"/> class.
        /// </summary>
        /// <param name="u">The u.</param>
        public ManualWorkflowService(User u = null) : base(u) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualWorkflowService"/> class.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="employer"></param>
        /// <param name="user"></param>
        public ManualWorkflowService(Customer customer, Employer employer, User user): base(customer, employer, user) { }

        /// <summary>
        /// Builds the event based on some keys and the criteria selected by the user for manually triggering a workflow.
        /// </summary>
        /// <param name="workflowId">The workflow identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">workflowId</exception>
        /// <exception cref="AbsenceSoftException">
        /// Workflow was not found
        /// or
        /// Employee was not found
        /// or
        /// Case was not found
        /// </exception>
        public Event BuildEvent(string workflowId, string employeeId = null, string caseId = null, List<WorkflowCriteria> criteria = null)
        {
            if (string.IsNullOrWhiteSpace(workflowId))
                throw new ArgumentNullException("workflowId");

            using (new InstrumentationContext("WorkflowService.BuildEvent"))
            {
                criteria = criteria ?? new List<WorkflowCriteria>(0);
                Workflow wf = null;
                Employee employee = null;
                Case theCase = null;
                string customerId = null;
                string employerId = null;

                // Get and check to ensure we have a workflow we're targeting
                wf = Workflow.GetById(workflowId);
                if (wf == null)
                    throw new AbsenceSoftException("Workflow was not found");

                // Get and ensure if any employee ID is passed in, it's a valid one
                if (!string.IsNullOrWhiteSpace(employeeId))
                {
                    employee = Employee.GetById(employeeId);
                    if (employee == null)
                        throw new AbsenceSoftException("Employee was not found");
                }

                // Get and ensure if any case ID is passed in, it's a valid one
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    theCase = Case.GetById(caseId);
                    if (theCase == null)
                        throw new AbsenceSoftException("Case was not found");
                    if (employee == null)
                        employee = Employee.GetById(theCase.Employee.Id);
                }

                customerId = theCase == null && employee == null ? CurrentUser.CustomerId : theCase == null ? employee.CustomerId : theCase.CustomerId;
                employerId = theCase == null && employee == null ? (CurrentUser.Employers.FirstOrDefault() ?? new EmployerAccess()).EmployerId : theCase == null ? employee.EmployerId : theCase.EmployerId;

                Event e = new Event() { EventType = EventType.Manual };
                e.CustomerId = customerId;
                e.EmployerId = employerId;
                e.EmployeeId = employee == null ? null : employee.Id;
                e.CaseId = theCase == null ? null : theCase.Id;
                e.IsSelfService = User.IsEmployeeSelfServicePortal;
                e.Name = wf.Name;

                ApplyAccommodationAdjudicationCriteria(e, criteria);
                ApplyAccommodationCriteria(e, criteria);
                ApplyAdjudicationStatusCriteria(e, criteria);
                ApplyAppliedPolicyCriteria(e, criteria);
                ApplyAttachmentCriteria(e, criteria);
                ApplyCaseAdjudicationReasonCriteria(e, criteria);
                ApplyCaseCriteria(e, criteria);
                ApplyCaseNoteCriteria(e, criteria);
                ApplyCaseSegmentCriteria(e, criteria);
                ApplyCertificationCriteria(e, criteria);
                ApplyCommunicationCriteria(e, criteria);
                ApplyCommunicationNoteCriteria(e, criteria);
                ApplyDiagnosisCodeCriteria(e, criteria);
                ApplyEmployeeNoteCriteria(e, criteria);
                ApplyPaperworkCriteria(e, criteria);
                ApplyPayPeriodCriteria(e, criteria);
                ApplyScheduleCriteria(e, criteria);
                ApplyTimeOffCriteria(e, criteria);
                ApplyToDoItemCriteria(e, criteria);
                ApplyWorkRestrictionCriteria(e, criteria);
                ApplyEmployeeJobCriteria(e, criteria);
                ApplyEmployeeNecessityCriteria(e, criteria);
                
                e.Save();

                return e;
            }
        }

        /// <summary>
        /// Gets the criteria.
        /// </summary>
        /// <param name="workflowId">The workflow identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">workflowId</exception>
        /// <exception cref="AbsenceSoftException">
        /// Workflow was not found
        /// or
        /// Employee was not found
        /// or
        /// Case was not found
        /// </exception>
        public List<WorkflowCriteria> GetCriteria(string workflowId, string employeeId = null, string caseId = null)
        {
            if (string.IsNullOrWhiteSpace(workflowId))
                throw new ArgumentNullException("workflowId");

            if (string.IsNullOrWhiteSpace(employeeId) && string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("employeeId", "employeeId and caseId can't both be null");

            using (new InstrumentationContext("WorkflowService.GetCriteria"))
            {
                Workflow wf = null;
                Employee employee = null;
                Case theCase = null;

                // Get and check to ensure we have a workflow we're targeting
                wf = Workflow.GetById(workflowId);
                if (wf == null)
                    throw new AbsenceSoftException("Workflow was not found");

                // Get and ensure if any employee ID is passed in, it's a valid one
                if (!string.IsNullOrWhiteSpace(employeeId))
                {
                    employee = Employee.GetById(employeeId);
                    if (employee == null)
                        throw new AbsenceSoftException("Employee was not found");
                }

                // Get and ensure if any case ID is passed in, it's a valid one
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    theCase = Case.GetById(caseId);
                    if (theCase == null)
                        throw new AbsenceSoftException("Case was not found");
                    if (employee == null)
                        employee = Employee.GetById(theCase.Employee.Id);
                }

                // Create our return criteria collection that will go back to the caller
                List<WorkflowCriteria> criteria = new List<WorkflowCriteria>();

                // Based on the event type, we're going to build our criteria using the various
                //  criteria builder methods, etc. Combining Enum EventType case statements throught the same
                //  break if they build the same type of criteria (duh) simply for better readability.
                switch (wf.TargetEventType)
                {
                    case EventType.EmployeeCreated:
                    case EventType.EmployeeChanged:
                    case EventType.EmployeeDeleted:
                    case EventType.EmployeeTerminated:
                    case EventType.PayScheduleChanged:
                    default:
                        // All of these simply take the current employee id, there's nothing more to collect
                        break;
                    case EventType.WorkScheduleChanged:
                    case EventType.CaseReducedScheduleChanged:
                        // list of work schedule Schedule class
                        criteria.Add(BuildScheduleCriteria(employee));
                        break;
                    case EventType.InquiryCreated:
                    case EventType.InquiryAccepted:
                    case EventType.InquiryClosed:
                    case EventType.CaseCreated:
                    case EventType.CaseClosed:
                    case EventType.CaseCanceled:
                    case EventType.CaseExtended:
                    case EventType.CaseReopened:
                    case EventType.CaseEventsUpdated:
                    case EventType.WorkRelatedReportingChanged:
                        if (theCase == null)
                            criteria.Add(BuildCaseCriteria(employee));
                        break;
                    case EventType.CaseAdjudicated:
                        if (theCase == null)
                            criteria.Add(BuildCaseCriteria(employee));

                        criteria.Add(BuildAdjudicationStatusCriteria());
                        criteria.Add(BuildAdjudicationDenialReasonCriteria());
                        criteria.Add(BuildDenialExplanationReasonCriteria());
                        break;

                    case EventType.CaseTypeChanged:
                        criteria.Add(BuildCaseSegmentCriteria(employee, theCase));
                        break;
                    case EventType.AccommodationCreated:
                    case EventType.AccommodationChanged:
                    case EventType.AccommodationDeleted:
                        // list of accommodations
                        criteria.Add(BuildAccommodationCriteria(employee, theCase));
                        break;
                    case EventType.AccommodationAdjudicated:
                        // list of accommodations
                        criteria.Add(BuildAccommodationCriteria(employee, theCase));
                        criteria.Add(BuildAdjudicationStatusCriteria());
                        criteria.Add(BuildAccommodationAdjudicationDenialReasonCriteria());
                        criteria.Add(BuildDenialExplanationReasonCriteria());
                        break;
                    case EventType.TimeOffRequested:
                    case EventType.TimeOffAdjudicated:
                        // list of intermittent time requests
                        criteria.Add(BuildIntermittentTimeOffRequestCriteria(employee, theCase));
                        break;
                    case EventType.PolicyExhausted:
                    case EventType.PolicyManuallyAdded:
                    case EventType.PolicyManuallyDeleted:
                        // list of applied policies
                        criteria.Add(BuildAppliedPolicyCriteria(employee, theCase));
                        break;
                    case EventType.PolicyAdjudicated:
                        // list of applied policies, list of AdjudicationStatus, list of AdjudicationDenialReason, DenialExplanationTextbox
                        criteria.Add(BuildAppliedPolicyCriteria(employee, theCase));
                        criteria.Add(BuildAdjudicationStatusCriteria());
                        criteria.Add(BuildAdjudicationDenialReasonCriteria());
                        criteria.Add(BuildDenialExplanationReasonCriteria());
                        break;
                    case EventType.AttachmentCreated:
                    case EventType.AttachmentDeleted:
                        // list of attachments
                        criteria.Add(BuildAttachmentCriteria(employee, theCase));
                        break;
                    case EventType.CommunicationSent:
                    case EventType.CommunicationDeleted:
                        // list of communications
                        criteria.Add(BuildCommunicationCriteria(employee, theCase));
                        break;
                    case EventType.CommunicationNoteCreated:
                    case EventType.CommunicationNoteChanged:
                        criteria.Add(BuildCommunicationNoteCriteria(employee, theCase));
                        break;
                    case EventType.PaperworkSent:
                        // list of paperwork
                        criteria.Add(BuildPaperworkCriteria(employee, theCase));
                        break;
                    case EventType.CaseNoteCreated:
                    case EventType.CaseNoteChanged:
                        criteria.Add(BuildCaseNoteCriteria(employee, theCase));
                        break;
                    case EventType.EmployeeConsultCreated:
                        criteria.Add(BuildEmployeeConsultCriteria(employee));
                        break;
                    case EventType.EmployeeNoteCreated:
                    case EventType.EmployeeNoteChanged:
                        criteria.Add(BuildEmployeeNoteCriteria(employee));
                        break;
                    case EventType.ToDoItemCreated:
                    case EventType.ToDoItemCompleted:
                    case EventType.ToDoItemChanged:
                    case EventType.ToDoItemCanceled:
                    case EventType.ToDoItemDeleted:
                        // list of ToDoItems
                        criteria.Add(BuildToDoItemCriteria(employee, theCase));
                        break;
                    case EventType.PayPeriodSubmitted:
                        criteria.Add(BuildPayPeriodCriteria(employee, theCase));
                        break;
                    case EventType.CertificationCreated:
                    case EventType.CertificationsChanged:
                        // list of certifications
                        criteria.Add(BuildCertificationCriteria(employee, theCase));
                        break;
                    case EventType.DiagnosisCreated:
                    case EventType.DiagnosisChanged:
                    case EventType.DiagnosisDeleted:
                        // list of diagnosis codes
                        criteria.Add(BuildDiagnosisCodesCriteria());
                        break;
                    case EventType.WorkRestrictionCreated:
                    case EventType.WorkRestrictionChanged:
                        // list of work restrictions
                        criteria.Add(BuildWorkRestrictionsCriteria(employee, theCase));
                        break;
                    case EventType.EmployeeJobApproved:
                    case EventType.EmployeeJobChanged:
                    case EventType.EmployeeJobDenied:
                        criteria.AddRange(BuildEmployeeJobCriteria(employee));
                        break;
                    case EventType.EmployeeNecessityChanged:
                        criteria.AddRange(BuildEmployeeNecessityCriteria(employee));
                        break;
                }

                return criteria;
            }
        }

        #region Criteria Builders
        /// <summary>
        ///  Builds the criteria to select a specific case
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildCaseCriteria(Employee employee)
        {
            WorkflowCriteria caseCriteria = new WorkflowCriteria()
            {
                Name = "CaseId",
                Prompt = "Case",
                Required = true,
                ControlType = ControlType.SelectList
            };
            Case.AsQueryable().Where(c => c.Employee.Id == employee.Id).ToList()
                .ForEach(c =>
                    caseCriteria.Options.Add(c.Id, c.Title)
                );

            return caseCriteria;
        }
        /// <summary>
        /// Applies the case criteria.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyCaseCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "CaseId");
            if (myCriteria != null)
                e.CaseId = myCriteria.ValueAs<string>();
        }

        /// <summary>
        /// Builds the criteria to select a specific case segment
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="theCase"></param>
        /// <returns></returns>
        private WorkflowCriteria BuildCaseSegmentCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria caseSegmentCriteria = new WorkflowCriteria()
            {
                Name = "SegmentId",
                Prompt = "Segment",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);

            foreach (Case aCase in employeeCases)
            {
                foreach (CaseSegment segment in aCase.Segments)
                {
                    string key = string.Format("{0}|{1}", aCase.Id, segment.Id);
                    string name = FormatCaseSegmentName(aCase, segment);
                    caseSegmentCriteria.Options.Add(key, name);
                }
            }

            return caseSegmentCriteria;
        }
        /// <summary>
        /// Applies the case segment criteria.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyCaseSegmentCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "SegmentId");
            if (myCriteria != null)
            {
                var val = (myCriteria.ValueAs<string>() ?? "").Split('|');
                if (val.Length == 2 && !val.Any(v => string.IsNullOrWhiteSpace(v)))
                {
                    e.CaseId = e.CaseId ?? val[0];
                    Case theCase = Case.GetById(e.CaseId);
                    e.Metadata.SetRawValue("CaseSegmentId", val[1]);
                    e.Metadata.SetRawValue("Type", theCase.Segments.First(s => s.Id == new Guid(val[1])).Type);
                }
            }
        }

        /// <summary>
        /// Builds the criteria to select a specific schedule
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildScheduleCriteria(Employee employee)
        {
            WorkflowCriteria scheduleCriteria = new WorkflowCriteria()
            {
                Name = "ScheduleId",
                Prompt = "Work Schedule",
                Required = true,
                ControlType = ControlType.SelectList
            };

            foreach (var schedule in employee.WorkSchedules)
            {
                string scheduleTitle = string.Format("{0}: {1}", schedule.ScheduleType.ToString().SplitCamelCaseString(), schedule.StartDate.ToShortDateString());
                scheduleCriteria.Options.Add(schedule.Id.ToString(), scheduleTitle);
            }

            return scheduleCriteria;
        }

        /// <summary>
        /// Applies the schedule criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyScheduleCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "ScheduleId");
            if (myCriteria != null)
                e.Metadata.SetRawValue("ScheduleId", myCriteria.ValueAs<string>());
        }

        /// <summary>
        /// Builds the criteria to select a specific adjudication status
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAdjudicationStatusCriteria()
        {
            return new WorkflowCriteria()
            {
                Name = "Determination",
                Prompt = "Determination",
                Required = true,
                ControlType = ControlType.SelectList,
                Options = Enums<AdjudicationStatus>.GetOptions()
            };
        }

        /// <summary>
        /// Applies the adjudication status criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyAdjudicationStatusCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "Determination");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("Determination", myCriteria.ValueAs<AdjudicationStatus>());
            }
        }

        /// <summary>
        /// Builds the criteria to select a specific adjudication denial reason
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAdjudicationDenialReasonCriteria()
        {
            Dictionary<string, object> denialReasons = null;
            using (var caseService = new CaseService())
            {
                denialReasons = caseService.GetAllDenialReasons(DenialReasonTarget.Policy).ToDictionary(p => p.Code, p => (object)p.Description);
            }

            // NOTE: This shouldn't be required, if they don't select one, oh well, just choose Other upon conversion
            return new WorkflowCriteria()
            {
                Name = "DenialReason",
                Prompt = "Denial Reason",
                Required = false,
                ControlType = ControlType.SelectList,
                Options = denialReasons
            };
        }
        /// <summary>
        /// Builds the criteria to select a specific accommodation adjudication denial reason
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAccommodationAdjudicationDenialReasonCriteria()
        {
            Dictionary<string, object> denialReasons = null;
            using (var caseService = new CaseService())
            {
                denialReasons = caseService.GetAllDenialReasons(DenialReasonTarget.Accommodation).ToDictionary(p => p.Code, p => (object)p.Description);
            }

            // NOTE: This shouldn't be required, if they don't select one, oh well, just choose Other upon conversion
            return new WorkflowCriteria()
            {
                Name = "AccommodationDenialReason",
                Prompt = "Denial Reason",
                Required = false,
                ControlType = ControlType.SelectList,
                Options = denialReasons
            };
        }

        /// <summary>
        /// Builds the criteria to specify the denial explanation reason
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildDenialExplanationReasonCriteria()
        {
            // NOTE: This shouldn't be required, if they don't enter it and the status is denied, 
            //  AND for reason of Other, oh well, just enter "Not Provided" upon conversion
            return new WorkflowCriteria()
            {
                Name = "DenialExplanation",
                Prompt = "Explanation",
                Required = false,
                ControlType = ControlType.TextBox
            };
        }

        /// <summary>
        /// Applies the case adjudication reason
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyCaseAdjudicationReasonCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var reasonCriteria = criteria.FirstOrDefault(c => c.Name == "DenialReason");
            var explanationCriteria = criteria.FirstOrDefault(c => c.Name == "DenialExplanation");
            if (reasonCriteria != null && explanationCriteria != null)
            {
                var reason = reasonCriteria.ValueAs<string>();
                string explanation = explanationCriteria.ValueAs<string>();
                if (!string.IsNullOrWhiteSpace(reason) && string.IsNullOrWhiteSpace(explanation))
                    explanation = "Not Provided";

                e.Metadata.SetRawValue("DenialReason", reason);
                e.Metadata.SetRawValue("DenialExplanation", explanation);
            }
        }

        /// <summary>
        /// Applies the accommodation adjudication reason
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyAccommodationAdjudicationCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var reasonCriteria = criteria.FirstOrDefault(c => c.Name == "AccommodationDenialReason");
            var explanationCriteria = criteria.FirstOrDefault(c => c.Name == "DenialExplanation");
            if (reasonCriteria != null && explanationCriteria != null)
            {
                var reason = reasonCriteria.ValueAs<string>();
                string explanation = explanationCriteria.ValueAs<string>();
                if (!string.IsNullOrWhiteSpace(reason) && string.IsNullOrWhiteSpace(explanation))
                    explanation = "Not Provided";

                e.Metadata.SetRawValue("DenialReason", reason);
                e.Metadata.SetRawValue("DenialExplanation", explanation);
            }
        }

        /// <summary>
        /// Builds the criteria to specify the accommodation
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAccommodationCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria accommodationCriteria = new WorkflowCriteria()
            {
                Name = "AccommodationId",
                Prompt = "Accommodation",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);

            foreach (Case aCase in employeeCases.Where(c => c.IsAccommodation))
            {
                foreach (Accommodation accomm in aCase.AccommodationRequest.Accommodations)
                {
                    string key = string.Format("{0}|{1}", aCase.Id, accomm.Id);
                    string name = string.Format("Case #: {0}, {1}: {2:MM/dd/yyyy}", aCase.CaseNumber, accomm.Type.Name, accomm.StartDate);
                    if (accomm.EndDate.HasValue)
                        name = string.Format("{0} - {1:MM/dd/yyyy}", name, accomm.EndDate.Value);

                    accommodationCriteria.Options.Add(key, name);
                }
            }

            return accommodationCriteria;
        }

        /// <summary>
        /// Appliyes the Accommodation Criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyAccommodationCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "AccommodationId");
            if (myCriteria != null)
            {
                var val = (myCriteria.ValueAs<string>() ?? "").Split('|');
                if (val.Length == 2 && !val.Any(v => string.IsNullOrWhiteSpace(v)))
                {
                    e.CaseId = e.CaseId ?? val[0];
                    Case theCase = Case.GetById(e.CaseId);
                    Accommodation theAccomm = theCase.AccommodationRequest.Accommodations.First(p => p.Id == new Guid(val[1]));
                    e.Metadata.SetRawValue("AccommodationId", theAccomm.Id);
                    e.Metadata.SetRawValue("AccommodationTypeId", theAccomm.Type.Id);
                    e.Metadata.SetRawValue("AccommodationTypeCode", theAccomm.Type.Code);
                }
            }
        }

        /// <summary>
        /// Builds the criteria to specify the intermittent time off request
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildIntermittentTimeOffRequestCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria timeOffCriteria = new WorkflowCriteria()
            {
                Name = "TimeOffCriteriaId",
                Prompt = "Time Off Request",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                foreach (var segment in aCase.Segments)
                {
                    foreach (var timeOffRequest in segment.UserRequests)
                    {
                        string key = string.Format("{0}|{1}|{2}", aCase.Id, segment.Id, timeOffRequest.Id);
                        string name = string.Format("{0}: {1:MM/dd/yyyy}", FormatCaseSegmentName(aCase, segment), timeOffRequest.RequestDate);
                        timeOffCriteria.Options.Add(key, name);
                    }
                }
            }

            return timeOffCriteria;
        }

        /// <summary>
        /// Applies the selected time off criteria to the event
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyTimeOffCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "TimeOffCriteriaId");
            if (myCriteria != null)
            {
                var val = (myCriteria.ValueAs<string>() ?? "").Split('|');
                if (val.Length == 3 && !val.Any(v => string.IsNullOrWhiteSpace(v)))
                {
                    e.CaseId = e.CaseId ?? val[0];
                    e.Metadata.SetRawValue("IntermittentTimeRequestId", val[2]);
                }
            }
        }

        /// <summary>
        /// Builds the criteria to specify the applied policy
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAppliedPolicyCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria appliedPolicyCriteria = new WorkflowCriteria()
            {
                Name = "AppliedPolicyId",
                Prompt = "Policy",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                foreach (var segment in aCase.Segments)
                {
                    foreach (var policy in segment.AppliedPolicies)
                    {
                        string key = string.Format("{0}|{1}|{2}", aCase.Id, segment.Id, policy.Id);
                        string name = string.Format("{0}: {1}", FormatCaseSegmentName(aCase, segment), policy.Policy.Name);
                        appliedPolicyCriteria.Options.Add(key, name);
                    }
                }
            }

            return appliedPolicyCriteria;
        }

        /// <summary>
        /// Applies the selected policy to the event
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyAppliedPolicyCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "AppliedPolicyId");
            if (myCriteria != null)
            {
                var val = (myCriteria.ValueAs<string>() ?? "").Split('|');
                if (val.Length == 3 && !val.Any(v => string.IsNullOrWhiteSpace(v)))
                {
                    e.CaseId = e.CaseId ?? val[0];
                    Case theCase = Case.GetById(e.CaseId);
                    CaseSegment theSegment = theCase.Segments.First(c => c.Id == new Guid(val[1]));
                    AppliedPolicy thePolicy = theSegment.AppliedPolicies.First(p => p.Id == new Guid(val[2]));
                    e.Metadata.SetRawValue("AppliedPolicyId", thePolicy.Id);
                    e.Metadata.SetRawValue("PolicyId", thePolicy.Policy.Id);
                    e.Metadata.SetRawValue("PolicyCode", thePolicy.Policy.Code);
                    e.Metadata.SetRawValue("FirstExhaustionDate", thePolicy.FirstExhaustionDate);
                }
            }
        }

        /// <summary>
        /// Builds the criteria to specify the attachment
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildAttachmentCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria attachmentCriteria = new WorkflowCriteria()
            {
                Name = "AttachmentId",
                Prompt = "Attachment",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<Attachment> employeeAttachments = Attachment.AsQueryable().Where(a => a.EmployeeId == employee.Id && a.CaseId == theCase.Id).ToList();
                foreach (var attachment in employeeAttachments)
                {
                    string key = attachment.Id;
                    string name = string.Format("Case #:{0} - {1}", aCase.CaseNumber, attachment.Description);
                    attachmentCriteria.Options.Add(key, name);
                }
            }

            return attachmentCriteria;
        }

        /// <summary>
        /// Applies the attachment criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyAttachmentCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "AttachmentId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("AttachmentId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the communication
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildCommunicationCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria communicationCriteria = new WorkflowCriteria()
            {
                Name = "CommunicationId",
                Prompt = "Communication",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<Communication> employeeCommunications = Communication.AsQueryable().Where(c => c.EmployeeId == employee.Id && c.CaseId == aCase.Id).ToList();
                foreach (var communication in employeeCommunications)
                {
                    string key = communication.Id;
                    string name = string.Format("Case #:{0} - {1}", aCase.CaseNumber, communication.Name);
                    communicationCriteria.Options.Add(key, name);
                }
            }

            return communicationCriteria;
        }

        /// <summary>
        /// Applies the communication criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyCommunicationCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "CommunicationId");
            if (myCriteria != null)
            {
                Communication comm = Communication.GetById(myCriteria.ValueAs<string>());
                e.Metadata.SetRawValue("AttachmentId", comm.AttachmentId);
                e.Metadata.SetRawValue("CommunicationId", comm.Id);
                e.Metadata.SetRawValue("Template", comm.Template);
            }
        }

        /// <summary>
        /// Builds the criteria to specify the communication note
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildCommunicationNoteCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria communicationNoteCriteria = new WorkflowCriteria()
            {
                Name = "CommunicationNoteId",
                Prompt = "CommunicationNote",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<Communication> employeeCommunications = Communication.AsQueryable().Where(c => c.EmployeeId == employee.Id && c.CaseId == aCase.Id).ToList();
                foreach (var comm in employeeCommunications)
                {
                    List<CommunicationNote> employeeCommunicationNotes = CommunicationNote.AsQueryable().Where(c => c.CommunicationId == comm.Id).ToList();
                    foreach (var commNote in employeeCommunicationNotes)
                    {
                        string key = commNote.Id;
                        string name = string.Format("Case #: {0} - {1} - {2}", aCase.CaseNumber, comm.Name, commNote.Notes);
                        communicationNoteCriteria.Options.Add(key, name);
                    }
                }
            }

            return communicationNoteCriteria;
        }
        /// <summary>
        /// Applies the communication note criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyCommunicationNoteCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "CommunicationNoteId");
            if (myCriteria != null)
            {
                CommunicationNote note = CommunicationNote.GetById(myCriteria.ValueAs<string>());
                e.Metadata.SetRawValue("AttachmentId", note.Communication.AttachmentId);
                e.Metadata.SetRawValue("CommunicationId", note.CommunicationId);
                e.Metadata.SetRawValue("CommunicationNoteId", note.Id);
                e.Metadata.SetRawValue("Template", note.Communication.Template);
            }
        }

        /// <summary>
        /// Builds the criteria to specify the communication note
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildPaperworkCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria paperworkCriteria = new WorkflowCriteria()
            {
                Name = "PaperworkId",
                Prompt = "Paperwork",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<Communication> employeeCommunications = Communication.AsQueryable().Where(c => c.EmployeeId == employee.Id && c.CaseId == aCase.Id).ToList();
                foreach (var communication in employeeCommunications)
                {
                    foreach (var paperwork in communication.Paperwork)
                    {
                        string key = string.Format("{0}|{1}", communication.Id, paperwork.Id);
                        string name = string.Format("Case #: {0} - {1} - {2}", aCase.CaseNumber, communication.Name, paperwork.Paperwork.Name);
                        paperworkCriteria.Options.Add(key, name);
                    }
                }
            }

            return paperworkCriteria;
        }

        /// <summary>
        /// Applies paperwork criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyPaperworkCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "PaperworkId");
            if (myCriteria != null)
            {
                var val = (myCriteria.ValueAs<string>() ?? "").Split('|');
                if (val.Length == 2 && !val.Any(v => string.IsNullOrWhiteSpace(v)))
                {
                    Communication comm = Communication.GetById(val[0]);
                    CommunicationPaperwork paperwork = comm.Paperwork.First(p => p.Id == new Guid(val[1]));
                    e.Metadata.SetRawValue("AttachmentId", comm.AttachmentId);
                    e.Metadata.SetRawValue("CommunicationId", comm.Id);
                    e.Metadata.SetRawValue("CommunicationPaperworkId", paperwork.Id);
                    e.Metadata.SetRawValue("CommunicationPaperworkAttachmentId", paperwork.AttachmentId);
                    e.Metadata.SetRawValue("Template", comm.Template);
                }
            }
        }

        /// <summary>
        /// Builds the criteria to specify the case note
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildCaseNoteCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria caseNoteCriteria = new WorkflowCriteria()
            {
                Name = "CaseNoteId",
                Prompt = "Case Note",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<CaseNote> caseNotes = CaseNote.AsQueryable().Where(c => c.CaseId == aCase.Id).ToList();
                foreach (var caseNote in caseNotes)
                {
                    string key = caseNote.Id;
                    string name = string.Format("Case #: {0} - {1}", aCase.CaseNumber, caseNote.Notes);
                    caseNoteCriteria.Options.Add(key, name);
                }
            }

            return caseNoteCriteria;
        }

        /// <summary>
        /// Applies the case note criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyCaseNoteCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "CaseNoteId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("CaseNoteId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the employee note
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildEmployeeNoteCriteria(Employee employee)
        {
            WorkflowCriteria employeeNoteCriteria = new WorkflowCriteria()
            {
                Name = "EmployeeNoteId",
                Prompt = "Employee Note",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<EmployeeNote> employeeNotes = EmployeeNote.AsQueryable().Where(n => n.EmployeeId == employee.Id).ToList();
            foreach (var note in employeeNotes)
            {
                employeeNoteCriteria.Options.Add(note.Id, note.Notes);
            }

            return employeeNoteCriteria;
        }

        /// <summary>
        /// Builds the criteria to specify the employee consult
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildEmployeeConsultCriteria(Employee employee)
        {
            WorkflowCriteria employeeConsultCriteria = new WorkflowCriteria()
            {
                Name = "EmployeeConsultationId",
                Prompt = "Employee Consult",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<EmployeeConsultation> consultList = EmployeeConsultation.AsQueryable().Where(n => n.EmployeeNumber == employee.Id).ToList();
            foreach (var consult in consultList)
            {
                employeeConsultCriteria.Options.Add(consult.Id, consult.Comments);
            }

            return employeeConsultCriteria;
        }

        /// <summary>
        /// Applies the employee note criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyEmployeeNoteCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "EmployeeNoteId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("EmployeeNoteId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the ToDoItem
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildToDoItemCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria toDoItemCriteria = new WorkflowCriteria()
            {
                Name = "ToDoItemId",
                Prompt = "ToDo Item",
                Required = true,
                ControlType = ControlType.SelectList
            };

            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                List<ToDoItem> toDoItems = ToDoItem.AsQueryable().Where(t => t.CaseId == aCase.Id).ToList();
                foreach (var item in toDoItems)
                {
                    string key = item.Id;
                    string name = string.Format("Case #: {0} - {1}", aCase.CaseNumber, item.Title);
                    toDoItemCriteria.Options.Add(key, name);
                }
            }

            return toDoItemCriteria;
        }

        /// <summary>
        /// Applies the todoitem criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyToDoItemCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "ToDoItemId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("ToDoItemId", myCriteria.ValueAs<string>());
            }
        }


        /// <summary>
        /// Builds the criteria to specify the pay period
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildPayPeriodCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria payPeriodCriteria = new WorkflowCriteria()
            {
                Name = "PayPeriodId",
                Prompt = "Pay Period",
                Required = true,
                ControlType = ControlType.SelectList
            };
            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                foreach (var payPeriod in aCase.Pay.PayPeriods)
                {
                    string key = payPeriod.Id.ToString();
                    string name = string.Format("Case #: {0} - {1:MM/dd/yyyy} - {2:MM/dd/yyyy}", aCase.CaseNumber, payPeriod.StartDate, payPeriod.EndDate);
                    payPeriodCriteria.Options.Add(key, name);
                }
            }

            return payPeriodCriteria;
        }

        /// <summary>
        /// Applies the pay period criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyPayPeriodCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "PayPeriodId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("PayPeriodId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the certification
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildCertificationCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria certificationCriteria = new WorkflowCriteria()
            {
                Name = "CertificationId",
                Prompt = "Certification",
                Required = true,
                ControlType = ControlType.SelectList
            };
            List<Case> employeeCases = GetEmployeeCases(employee.Id, theCase);
            foreach (var aCase in employeeCases)
            {
                foreach (var cert in aCase.Certifications)
                {
                    string key = cert.Id.ToString();
                    string name = string.Format("Case #: {0} - {1:MM/dd/yyyy} - {2:MM/dd/yyyy}", aCase.CaseNumber, cert.StartDate, cert.EndDate);
                    certificationCriteria.Options.Add(key, name);
                }
            }

            return certificationCriteria;
        }

        /// <summary>
        /// Applies the certification criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyCertificationCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "CertificationId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("CertificationId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the diagnosis code
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildDiagnosisCodesCriteria()
        {
            WorkflowCriteria diagnosisCriteria = new WorkflowCriteria()
            {
                Name = "DiagnosisId",
                Prompt = "Diagnosis",
                Required = true,
                ControlType = ControlType.SelectList,
                
            };
            DiagnosisCode.AsQueryable().ToList().ForEach(dc => diagnosisCriteria.Options.Add(dc.Id, dc.UIDescription));

            return diagnosisCriteria;
        }

        /// <summary>
        /// Applies the Diagnosis code criteria
        /// </summary>
        /// <param name="e"></param>
        /// <param name="criteria"></param>
        private void ApplyDiagnosisCodeCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "DiagnosisId");
            if (myCriteria != null)
            {
                DiagnosisCode diagnosis = DiagnosisCode.GetById(myCriteria.ValueAs<string>());
                e.Metadata.SetRawValue("DiagnosisId", diagnosis.Id);
                e.Metadata.SetRawValue("DiagnosisCode", diagnosis.Code);
            }
        }

        /// <summary>
        /// Builds the criteria to specify the work restriction
        /// </summary>
        /// <returns></returns>
        private WorkflowCriteria BuildWorkRestrictionsCriteria(Employee employee, Case theCase)
        {
            WorkflowCriteria workRestrictionCriteria = new WorkflowCriteria()
            {
                Name = "WorkRestrictionId",
                Prompt = "Work Restriction",
                Required = true,
                ControlType = ControlType.SelectList
            };
            using (var demandService = new DemandService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                List<EmployeeRestriction> employeeRestrictions = demandService.GetJobRestrictionsForEmployee(employee.Id);
                foreach (var restriction in employeeRestrictions)
                {
                    string key = restriction.Id;
                    string name = string.Format("Restriction: {0}, Dates: {1}", restriction.Restriction.Demand.Name, restriction.Restriction.Dates);
                    workRestrictionCriteria.Options.Add(key, name);
                }
            }

            return workRestrictionCriteria;
        }

        /// <summary>
        /// Applies the work restriction criteria
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyWorkRestrictionCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "WorkRestrictionId");
            if (myCriteria != null)
            {
                e.Metadata.SetRawValue("WorkRestrictionId", myCriteria.ValueAs<string>());
            }
        }

        /// <summary>
        /// Builds the criteria to specify the employee job
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        private List<WorkflowCriteria> BuildEmployeeJobCriteria(Employee employee)
        {
            WorkflowCriteria criteria = new WorkflowCriteria()
            {
                Name = "EmployeeJobId",
                Prompt = "Employee Job",
                Required = true,
                ControlType = ControlType.SelectList
            };
            List<EmployeeJob> jobs = employee.GetJobs();
            foreach (var j in jobs)
            {
                string key = string.Format("{0}:{1}", j.Id, j.JobCode);
                string name = string.Format("{0}: {1}, {2}, {3}", 
                    j.JobName, 
                    j.JobTitle ?? j.JobName, 
                    j.OfficeLocationCode ?? "(no location)",
                    j.Dates == null ? "(perpetual)" : j.Dates.ToString());

                criteria.Options.Add(key, name);
            }

            WorkflowCriteria status = new WorkflowCriteria()
            {
                Name = "EmployeeJobStatus",
                Prompt = "Job Status",
                Required = false,
                ControlType = ControlType.SelectList,
                Options = Enums<AdjudicationStatus>.GetOptions()
            };

            return new List<WorkflowCriteria>(2) { criteria, status };
        }

        /// <summary>
        /// Applies the employee job criteria
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyEmployeeJobCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "EmployeeJobId");
            if (myCriteria != null)
            {
                var crit = myCriteria.ValueAs<string>().Split(':');
                if (crit.Length != 2)
                    return;
                e.Metadata.SetRawValue("EmployeeJobId", crit[0]);
                e.Metadata.SetRawValue("JobCode", crit[1]);
            }
            var statusCritera = criteria.FirstOrDefault(c => c.Name == "EmployeeJobStatus");
            if (statusCritera != null)
            {
                e.Metadata.SetRawValue("Status", statusCritera.ValueAs<AdjudicationStatus>());
            }
        }



        /// <summary>
        /// Builds the criteria to specify the employee necessity
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        private List<WorkflowCriteria> BuildEmployeeNecessityCriteria(Employee employee)
        {
            WorkflowCriteria criteria = new WorkflowCriteria()
            {
                Name = "EmployeeNecessityId",
                Prompt = "Employee Necessity",
                Required = true,
                ControlType = ControlType.SelectList
            };
            List<EmployeeNecessity> necs = employee.GetNecessities();
            foreach (var n in necs)
            {
                string key = string.Format("{0}:{1}", n.Id, n.NecessityId);
                string name = string.Format("{0}: {1}, {2}",
                    n.Name,
                    n.Type.ToString().SplitCamelCaseString(),
                    n.Dates == null ? "(perpetual)" : n.Dates.ToString());
                criteria.Options.Add(key, name);
            }

            return new List<WorkflowCriteria>(1) { criteria };
        }

        /// <summary>
        /// Applies the employee necessity criteria
        /// </summary>
        /// <param name="e">The event.</param>
        /// <param name="criteria">The criteria.</param>
        private void ApplyEmployeeNecessityCriteria(Event e, List<WorkflowCriteria> criteria)
        {
            var myCriteria = criteria.FirstOrDefault(c => c.Name == "EmployeeNecessityId");
            if (myCriteria != null)
            {
                var crit = myCriteria.ValueAs<string>().Split(':');
                if (crit.Length != 2)
                    return;
                e.Metadata.SetRawValue("EmployeeNecessityId", crit[0]);
                e.Metadata.SetRawValue("NecessityId", crit[1]);
            }
        }

        #endregion

        /// <summary>
        /// Gets the employee cases.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="theCase">The case.</param>
        /// <returns></returns>
        private List<Case> GetEmployeeCases(string employeeId, Case theCase)
        {
            List<Case> employeeCases = new List<Case>();
            if (theCase == null)
                employeeCases = Case.AsQueryable().Where(c => c.Employee.Id == employeeId).ToList();
            else
                employeeCases.Add(theCase);

            return employeeCases;
        }

        /// <summary>
        /// Formats the name of the case segment.
        /// </summary>
        /// <param name="theCase">The case.</param>
        /// <param name="segment">The segment.</param>
        /// <returns></returns>
        private string FormatCaseSegmentName(Case theCase, CaseSegment segment)
        {
            string name = string.Format("Case #: {0}, {1}: {2:MM/dd/yyyy}", theCase.CaseNumber, segment.Type.ToString().SplitCamelCaseString(), segment.StartDate);
            if (segment.EndDate.HasValue)
                name = string.Format("{0} - {1:MM/dd/yyyy}", name, segment.EndDate.Value);

            return name;
        }
    }
}
