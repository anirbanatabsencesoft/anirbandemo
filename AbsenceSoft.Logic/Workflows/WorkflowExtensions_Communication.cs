﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows;

namespace AbsenceSoft
{
    public static partial class WorkflowExtensions
    {
        /// <summary>
        /// WORKFLOW: Called on attachment created.
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAttachmentCreated(this Attachment attachment, User user = null)
        {
            Event e = StageCaseEvent(EventType.AttachmentCreated, attachment.CustomerId, attachment.EmployerId, attachment.EmployeeId, attachment.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", attachment.Id);
            e.Save();
            var context = BuildContext<Attachment>(attachment.Case, attachment);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Attachment>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on attachment deleted.
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        /// <param name="user">The user.</param>
        public static void WfOnAttachmentDeleted(this Attachment attachment, User user = null)
        {
            Event e = StageCaseEvent(EventType.AttachmentDeleted, attachment.CustomerId, attachment.EmployerId, attachment.EmployeeId, attachment.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", attachment.Id);
            e.Save();
            var context = BuildContext<Attachment>(attachment.Case, attachment);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Attachment>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on communication sent.
        /// </summary>
        /// <param name="comm">The comm.</param>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCommunicationSent(this Communication comm, ToDoItem todo = null, User user = null)
        {
            if (todo != null)
                todo.WfOnToDoItemCompleted(user, todo.ResultText, true);

            Event e = StageCaseEvent(EventType.CommunicationSent, comm.CustomerId, comm.EmployerId, comm.EmployeeId, comm.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", comm.AttachmentId);
            e.Metadata.SetRawValue("CommunicationId", comm.Id);
            string templateKey = comm.Template;
            if (!string.IsNullOrWhiteSpace(templateKey))
                e.Metadata.SetRawValue("Template", templateKey);
            if (todo != null)
                e.Metadata.Merge(todo.Metadata);
            e.Save();
            var context = BuildContext<Communication>(comm.Case, comm);
            context.ActiveWorkFlowItem = todo;
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Communication>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on communication deleted.
        /// </summary>
        /// <param name="comm">The comm.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCommunicationDeleted(this Communication comm, User user = null)
        {
            Event e = StageCaseEvent(EventType.CommunicationDeleted, comm.CustomerId, comm.EmployerId, comm.EmployeeId, comm.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", comm.AttachmentId);
            e.Metadata.SetRawValue("CommunicationId", comm.Id);
            string templateKey = comm.Template;
            if (!string.IsNullOrWhiteSpace(templateKey))
                e.Metadata.SetRawValue("Template", templateKey);
            e.Save();
            var context = BuildContext<Communication>(comm.Case, comm);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Communication>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on communication note created.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCommunicationNoteCreated(this CommunicationNote note, User user = null)
        {
            Event e = StageCaseEvent(EventType.CommunicationNoteCreated, note.CustomerId, note.EmployerId, note.Communication.EmployeeId, note.Communication.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", note.Communication.AttachmentId);
            e.Metadata.SetRawValue("CommunicationId", note.CommunicationId);
            e.Metadata.SetRawValue("CommunicationNoteId", note.Id);
            string templateKey = note.Communication.Template;
            if (!string.IsNullOrWhiteSpace(templateKey))
                e.Metadata.SetRawValue("Template", templateKey);
            e.Save();
            var context = BuildContext<CommunicationNote>(note.Communication.Case, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<CommunicationNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on communication note changed.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="user">The user.</param>
        public static void WfOnCommunicationNoteChanged(this CommunicationNote note, User user = null)
        {
            Event e = StageCaseEvent(EventType.CommunicationNoteChanged, note.CustomerId, note.EmployerId, note.Communication.EmployeeId, note.Communication.CaseId, user);
            e.Metadata.SetRawValue("AttachmentId", note.Communication.AttachmentId);
            e.Metadata.SetRawValue("CommunicationId", note.CommunicationId);
            e.Metadata.SetRawValue("CommunicationNoteId", note.Id);
            string templateKey = note.Communication.Template;
            if (!string.IsNullOrWhiteSpace(templateKey))
                e.Metadata.SetRawValue("Template", templateKey);
            e.Save();
            var context = BuildContext<CommunicationNote>(note.Communication.Case, note);
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<CommunicationNote>(e, context);
        }

        /// <summary>
        /// WORKFLOW: Called on paperwork sent.
        /// </summary>
        /// <param name="comm">The comm.</param>
        /// <param name="paperwork">The paperwork.</param>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPaperworkSent(this Communication comm, CommunicationPaperwork paperwork, ToDoItem todo = null, User user = null)
        {
            if (todo != null)
                todo.WfOnToDoItemCompleted(user);

            Event e = StageCaseEvent(EventType.PaperworkSent, comm.CustomerId, comm.EmployerId, comm.EmployeeId, comm.CaseId, user);
            e.Metadata
                .SetRawValue("AttachmentId", comm.AttachmentId)
                .SetRawValue("CommunicationId", comm.Id)
                .SetRawValue("PaperworkId", paperwork.Id)
                .SetRawValue("PaperworkAttachmentId", paperwork.AttachmentId)
                .SetRawValue("PaperworkDueDate", paperwork.DueDate?.AddDays(-(paperwork.Paperwork.ReturnDateAdjustmentDays ?? 0)));
            string templateKey = comm.Template;
            if (!string.IsNullOrWhiteSpace(templateKey))
                e.Metadata.SetRawValue("Template", templateKey);
            e.Metadata.SetRawValue("PaperworkName", paperwork.Paperwork.Name);
            e.Metadata.SetRawValue("RequiresReview", paperwork.Paperwork.RequiresReview);
            e.Metadata.SetRawValue("IsForApprovalDecision", paperwork.Paperwork.IsForApprovalDecision);
            if (todo != null)
                e.Metadata.Merge(todo.Metadata);
            e.Save();
            var context = BuildContext<Communication>(comm.Case, comm);
            context.Paperwork = paperwork;
            context.ActiveWorkFlowItem = todo;
            using (WorkflowService wf = new WorkflowService(user))
                wf.RaiseEvent<Communication>(e, context);
        }
        /// <summary>
        /// WORKFLOW: Called on paperwork sent.
        /// </summary>
        /// <param name="paperwork">The paperwork.</param>
        /// <param name="comm">The comm.</param>
        /// <param name="todo">The todo.</param>
        /// <param name="user">The user.</param>
        public static void WfOnPaperworkSent(this CommunicationPaperwork paperwork, Communication comm, ToDoItem todo = null, User user = null) { comm.WfOnPaperworkSent(paperwork, todo, user); }
    }
}
