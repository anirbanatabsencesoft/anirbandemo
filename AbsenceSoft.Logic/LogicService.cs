﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic
{
    public abstract class LogicService : ILogicService
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        internal string CustomerId { get; set; }


        private Customer _currentCustomer;
        /// <summary>
        /// Gets or sets the current customer.
        /// </summary>
        /// <value>
        /// The current customer.
        /// </value>
        public Customer CurrentCustomer
        {
            get
            {
                return _currentCustomer
                    ?? (_currentCustomer = Customer.GetById(CustomerId));
            }
            set
            {
                _currentCustomer = value;
                CustomerId = value == null
                    ? null
                    : value.Id;
            }
        }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        internal string EmployerId { get; set; }

        private Employer _currentEmployer;
        /// <summary>
        /// Gets or sets the current employer.
        /// </summary>
        /// <value>
        /// The current employer.
        /// </value>
        public Employer CurrentEmployer
        {
            get
            {
                if (_currentEmployer == null && EmployerId != null)
                    _currentEmployer = Employer.GetById(EmployerId);

                return _currentEmployer;
            }
            set
            {
                _currentEmployer = value;
                EmployerId = value == null
                    ? null
                    : value.Id;
            }
        }

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        public User CurrentUser { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicService"/> class.
        /// </summary>
        protected LogicService() : this((string)null, (string)null, null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        protected LogicService(User currentUser) : this((string)null, (string)null, currentUser) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        protected LogicService(string customerId, string employerId, User currentUser = null)
        {
            CustomerId = customerId;
            EmployerId = employerId;
            CurrentUser = currentUser;

            if (string.IsNullOrWhiteSpace(CustomerId))
                CustomerId = (Customer.Current ?? new Customer()).Id;

            if (string.IsNullOrWhiteSpace(EmployerId))
                EmployerId = (Employer.Current ?? new Employer()).Id;

            if (CurrentUser == null)
                CurrentUser = User.Current;

            if (CurrentUser == null)
                CurrentUser = User.System;
            else
            {
                if (string.IsNullOrWhiteSpace(CustomerId))
                    CustomerId = CurrentUser.CustomerId;
                if (string.IsNullOrWhiteSpace(EmployerId) && CurrentUser.Employers != null && CurrentUser.Employers.Count == 1 && CurrentUser.Employers[0].IsActive)
                    EmployerId = CurrentUser.Employers[0].EmployerId;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicService"/> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="u">The u.</param>
        protected LogicService(Customer customer, Employer employer, User u)
        {
            if (customer == null)
                customer = Customer.Current;

            if (customer == null)
                customer = new Customer();
            CustomerId = customer.Id;

            if (u == null)
                u = User.Current;

            if (u == null)
                u = User.System;

            _currentCustomer = customer;
            _currentEmployer = employer;
            if (_currentEmployer != null)
                EmployerId = _currentEmployer.Id;
            CurrentUser = u;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicService"/> class.
        /// </summary>
        public virtual void Dispose() { /* Should be implemented, but may not have to be */ }
    }
}
