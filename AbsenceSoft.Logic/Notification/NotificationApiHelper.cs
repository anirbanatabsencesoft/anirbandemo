﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Properties;
using AT.Api.Core.Common;
using AT.Api.Notification.Controllers;
using AT.Common.Core;
using AT.Common.Core.NotificationEnums;
using AT.Data.Notification.Arguments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Notification
{
    public static class NotificationApiHelper
    {
        #region Template constants
        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_ForgotPassword = "ForgotPassword";

        /// <summary>
        /// The forgot password email template name.
        /// </summary>
        public const string EmailTemplate_LockedOutEmail = "LockedOutEmail";

        /// <summary>
        /// The create account email template name.
        /// </summary>
        public const string EmailTemplate_CreateAccount = "CreateAccount";

        /// <summary>
        /// The change password email template name.
        /// </summary>
        public const string EmailTemplate_ChangePassword = "ChangePassword";

        /// <summary>
        /// The add new user template name
        /// </summary>
        public const string EmailTemplate_AddNewUser = "AddNewUser";

        /// <summary>
        /// User need access email template name
        /// </summary>
        public const string EmailTemplate_UserNeedAccess = "UserNeedAccessEmail";

        /// <summary>
        /// Welcome message to approved user
        /// </summary>
        public const string EmailTemplate_WelcomeEmail = "WelcomeEmail";

        public const string EmailTemplate_EmployeeValidationEmail = "EmployeeValidationEmail";
        public const string EmailTemplate_EmployeeAssociationEmail = "EmployeeAssociationEmail";

        public const string EmailTemplate_TodoNotification = "ToDoEmailNotification";

        public const string EmailTemplate_SelfAccountCreationEmail = "SelfAccountCreationEmail";

        public const string EmailTemplate_UserExistsEmailToUser = "UserExistsEmailToUser";

        public const string EmailTemplate_SelfRegistrationEmailToAdmin = "SelfRegistrationEmailToAdmin";

        public const string EmailTemplate_EmployeeNotUniqueEmailToAdmin = "EmployeeNotUniqueEmailToAdmin";

        #endregion

        /// <summary>
        /// Sends Notification in Sync fashion
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="createdByUserId"></param>
        /// </summary>
        public static bool SendEmail(this MailMessage message, User currentUser, string token)
        {
            var saveNotificationArgs = SetNotificationArgument(message, currentUser);
            AT.Common.Core.ResultSet<AT.Entities.Notification.Notification> result = CallApi(saveNotificationArgs, token).GetAwaiter().GetResult();
            return result.Success;
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="createdByUserId"></param>
        public async static Task<ResultSet<AT.Entities.Notification.Notification>> SendEmailAsync(this MailMessage message, User currentUser, string token)
        {
            var saveNotificationArgs = SetNotificationArgument(message, currentUser);
            AT.Common.Core.ResultSet<AT.Entities.Notification.Notification> result = await CallApiAsync(saveNotificationArgs, token);
            return result;
        }

        /// <summary>
        /// Sends Notification with given parameters
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileName"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="createdByUserId"></param>
        public static void Notify(this MailMessage message, User currentUser, string token, NotificationType notificationTypeSelected, string fileNames, int? lookupNotificationTypeId)
        {
            var saveNotificationArgs = SetNotificationArgument(currentUser, message.From.ToString(), message.Subject, message.Body, (message.IsBodyHtml ? MessageFormat.HTML : MessageFormat.Text), notificationTypeSelected, fileNames,
                                    lookupNotificationTypeId, message.To.ToString());
            SendNotification(saveNotificationArgs, token);
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileName"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        public async static Task<bool> NotifyAsync(this MailMessage message, User currentUser, string token, NotificationType notificationTypeSelected, string fileNames, int? lookupNotificationTypeId)
        {
            if (currentUser == null && token.Length == 0)
            {
                var saveNotificationArgs = SetNotificationArgument(message.From.ToString(), message.Subject, message.Body, (message.IsBodyHtml ? MessageFormat.HTML : MessageFormat.Text), notificationTypeSelected,
                                          lookupNotificationTypeId, message.To.ToString());
                return await SendNotificationAsync(saveNotificationArgs);
            }
            else
            {
                var saveNotificationArgs = SetNotificationArgument(currentUser, message.From.ToString(), message.Subject, message.Body, (message.IsBodyHtml ? MessageFormat.HTML : MessageFormat.Text), notificationTypeSelected, fileNames,
                                           lookupNotificationTypeId, message.To.ToString());
                return await SendNotificationAsync(saveNotificationArgs, token);
            }
        }

        /// <summary>
        /// Set the attachments to a notification
        /// </summary>
        /// <param name="fileNames"></param>
        /// <param name="saveNotificationArgs"></param>
        private static void SetAttachments(string fileNames, SaveNotificationArgs saveNotificationArgs)
        {
            if (!string.IsNullOrWhiteSpace(fileNames))
            {
                return;
            }

            var nameList = fileNames.Split(new string[1] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();

            saveNotificationArgs.Attachments = new List<AT.Entities.Notification.NotificationAttachment>();
            foreach (var name in nameList)
            {
                byte[] fileContentBytes = System.IO.File.ReadAllBytes(name);
                saveNotificationArgs.Attachments.Add(new AT.Entities.Notification.NotificationAttachment()
                {
                    AttachmentHeader = $"CustomerKey - {saveNotificationArgs.Notification.CustomerKey}, EmployerId - {saveNotificationArgs.Notification.EmployerId}",
                    FileContent = fileContentBytes,
                    FileName = System.IO.Path.GetFileName(name)
                });
            }
        }

        /// <summary>
        /// Gets an email template text directly from an embedded resource of the logic assembly. This is only
        /// for site-wide email templates such as forgot password, welcome, etc. All other emails (e.g. communications)
        /// should use the CommunicationService in order to get template text.
        /// </summary>
        /// <param name="name">The email template name</param>
        /// <returns>The specified email template text or <c>null</c> if the named resource wasn't found.</returns>
        public static string GetEmailTemplate(string name)
        {
            return Resources.ResourceManager.GetString(name);
        }

        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="currentUser"></param>
        /// <param name="mailSender"></param>
        /// <param name="mailSubject"></param>
        /// <param name="mailMessage"></param>
        /// <param name="mailMessageFormat"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="fileNamePassed"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="createdByUserId"></param>
        /// <param name="fileContentBytes"></param>
        /// <param name="mailRecipientAddress"></param>
        /// <returns></returns>
        private static SaveNotificationArgs SetNotificationArgument(User currentUser, string mailSender, string mailSubject, string mailMessage, MessageFormat mailMessageFormat, NotificationType notificationTypeSelected, string fileNamePassed,
                                                                    int? lookupNotificationTypeId, string mailRecipientAddress)
        {
            //Build Notification   
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = currentUser.CustomerId,
                From = mailSender,
                NotificationType = notificationTypeSelected,
                CreatedById = 0,
                CreatedByKey = currentUser.Id,
                ModifiedByKey = currentUser.Id,
                MessageFormat = mailMessageFormat,
                Subject = mailSubject,
                Message = mailMessage
            };
            if (lookupNotificationTypeId != null)
            {
                notification.LookupNotificationTypeId = lookupNotificationTypeId.Value;
            }

            //Recipient List Recipient
            AT.Entities.Notification.NotificationRecipient recipient = new AT.Entities.Notification.NotificationRecipient()
            {
                RecipientAddress = mailRecipientAddress,
                RecipientType = RecipientType.To
            };

            // Create SaveNotificationArgs
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = new List<AT.Entities.Notification.NotificationRecipient>
                                {
                                    recipient
                                }
            };
            SetAttachments(fileNamePassed, saveNotificationArgs);
            return saveNotificationArgs;
        }

        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="mailSender"></param>
        /// <param name="mailSubject"></param>
        /// <param name="mailMessage"></param>
        /// <param name="mailMessageFormat"></param>
        /// <param name="notificationTypeSelected"></param>
        /// <param name="lookupNotificationTypeId"></param>
        /// <param name="createdByUserId"></param>
        /// <param name="fileContentBytes"></param>
        /// <param name="mailRecipientAddress"></param>
        /// <returns></returns>
        private static SaveNotificationArgs SetNotificationArgument(string mailSender, string mailSubject, string mailMessage, MessageFormat mailMessageFormat, NotificationType notificationTypeSelected,
                                                                    int? lookupNotificationTypeId, string mailRecipientAddress)
        {
            //Build Notification   
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = "0",
                From = mailSender,
                NotificationType = notificationTypeSelected,
                CreatedById = 0,
                CreatedByKey = "0",
                ModifiedById = 0,
                ModifiedByKey = "0",
                MessageFormat = mailMessageFormat,
                Subject = mailSubject,
                Message = mailMessage
            };
            if (lookupNotificationTypeId != null)
            {
                notification.LookupNotificationTypeId = lookupNotificationTypeId.Value;
            }

            //Recipient List Recipient
            AT.Entities.Notification.NotificationRecipient recipient = new AT.Entities.Notification.NotificationRecipient()
            {
                RecipientAddress = mailRecipientAddress,
                RecipientType = RecipientType.To
            };

            // Create SaveNotificationArgs
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = new List<AT.Entities.Notification.NotificationRecipient>
                                {
                                    recipient
                                }
            };
            return saveNotificationArgs;
        }


        /// <summary>
        /// Sets the notification arguments
        /// </summary>
        /// <param name="message"></param>
        /// <param name="currentUser"></param>
        /// <param name="createdByUserId"></param>
        /// <returns></returns>
        private static SaveNotificationArgs SetNotificationArgument(MailMessage message, User currentUser)
        {
            //Build Notification
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification
            {
                CustomerKey = currentUser.CustomerId,
                From = message.From.ToString(),
                NotificationType = NotificationType.Email,
                CreatedById = 0,
                CreatedByKey = currentUser.Id,
                ModifiedByKey = currentUser.Id,
                MessageFormat = MessageFormat.HTML,
                Subject = message.Subject,
                Message = message.Body
            };

            List<AT.Entities.Notification.NotificationAttachment> notificationAttachments = new List<AT.Entities.Notification.NotificationAttachment>();

            //Build Attachment
            if (message.Attachments.Count > 0)
            {
                foreach (var attachment in message.Attachments)
                {
                    AT.Entities.Notification.NotificationAttachment notificationAttachment = new AT.Entities.Notification.NotificationAttachment()
                    {
                        FileName = attachment.Name,
                        FileContent = Utilities.ReadStreamFully(attachment.ContentStream)
                    };
                    notificationAttachments.Add(notificationAttachment);
                }
            }

            List<AT.Entities.Notification.NotificationRecipient> NotificationRecipients = new List<AT.Entities.Notification.NotificationRecipient>();

            foreach (var recipient in message.To)
            {
                AT.Entities.Notification.NotificationRecipient notificationRecipient = new AT.Entities.Notification.NotificationRecipient()
                {
                    RecipientAddress = recipient.Address,
                    RecipientType = RecipientType.To
                };
                NotificationRecipients.Add(notificationRecipient);
            }

            foreach (var ccRecipient in message.CC)
            {
                AT.Entities.Notification.NotificationRecipient notificationCCRecipient = new AT.Entities.Notification.NotificationRecipient()
                {
                    RecipientAddress = ccRecipient.Address,
                    RecipientType = RecipientType.CC
                };
                NotificationRecipients.Add(notificationCCRecipient);
            }

            notification.RecipientList = NotificationRecipients;

            // Create SaveNotificationArgs
            SaveNotificationArgs saveNotificationArgs = new SaveNotificationArgs()
            {
                Notification = notification,
                Recipients = NotificationRecipients
            };

            if (notificationAttachments.Count > 0)
            {
                saveNotificationArgs.Attachments = notificationAttachments;
            }

            saveNotificationArgs.MailMessage = message;

            return saveNotificationArgs;
        }

        /// <summary>
        /// Calls the Notification Api using Service Factory
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private async static Task<AT.Common.Core.ResultSet<AT.Entities.Notification.Notification>> CallApi(SaveNotificationArgs saveNotificationArgs, string token)
        {
            ServiceFactory factory = new ServiceFactory(token);
            return await factory.ExecuteService<NotificationController, SaveNotificationArgs, AT.Entities.Notification.Notification>(saveNotificationArgs, nc => nc.Save(saveNotificationArgs));
        }

        /// <summary>
        /// Calls the Notification Api using Service Factory in Async mode
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private static async Task<AT.Common.Core.ResultSet<AT.Entities.Notification.Notification>> CallApiAsync(SaveNotificationArgs saveNotificationArgs, string token)
        {
            ServiceFactory factory = new ServiceFactory(token);
            return await factory.ExecuteService<NotificationController, SaveNotificationArgs, AT.Entities.Notification.Notification>(saveNotificationArgs, nc => nc.Save(saveNotificationArgs));
        }

        /// <summary>
        /// Calls the Notification Api using Service Factory in Async mode
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private static async Task<ResultSet<AT.Entities.Notification.Notification>> CallApiAsync(AT.Entities.Notification.Notification notificationArgs)
        {
            ServiceFactory factory = new ServiceFactory();
            return await factory.ExecuteService<NotificationController, AT.Entities.Notification.Notification, AT.Entities.Notification.Notification>(notificationArgs, nc => nc.NotifyOnly(notificationArgs));
        }

        /// <summary>
        /// Sends Notification 
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private static void SendNotification(SaveNotificationArgs saveNotificationArgs, string token)
        {
            CallApi(saveNotificationArgs, token).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private async static Task<bool> SendNotificationAsync(SaveNotificationArgs saveNotificationArgs, string token)
        {
            AT.Common.Core.ResultSet<AT.Entities.Notification.Notification> result = await CallApiAsync(saveNotificationArgs, token);

            return result.Success;
        }

        /// <summary>
        /// Sends Notification in Async fashion
        /// </summary>
        /// <param name="saveNotificationArgs"></param>
        /// <returns></returns>
        private async static Task<bool> SendNotificationAsync(SaveNotificationArgs saveNotificationArgs)
        {
            AT.Entities.Notification.Notification notification = new AT.Entities.Notification.Notification()
            {
                From = saveNotificationArgs.Notification.From,
                RecipientList = saveNotificationArgs.Recipients,
                AttachmentList = saveNotificationArgs.Attachments,
                Subject = saveNotificationArgs.Notification.Subject,
                Message = saveNotificationArgs.Notification.Message,
                NotificationType = saveNotificationArgs.Notification.NotificationType,
                CreatedByKey = saveNotificationArgs.Notification.CreatedByKey,
                ModifiedByKey = saveNotificationArgs.Notification.ModifiedByKey,
                CreatedById = saveNotificationArgs.Notification.CreatedById
            };
            AT.Common.Core.ResultSet<AT.Entities.Notification.Notification> result = await CallApiAsync(notification);

            return result.Success;
        }
    }
}