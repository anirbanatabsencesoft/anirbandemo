﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic
{
    [Serializable]
    public sealed class ListCriteria
    {
        public ListCriteria()
        {
            this.ExtensionData = new Dictionary<string, object>();
            this.SortDirection = Data.Enums.SortDirection.Ascending;
            this.PageNumber = 1;
            this.PageSize = 30;
            this.OnlyPageRecords = true;
        }

        /// <summary>
        /// The sort direction that the results should be sorted using the sort by field name
        /// </summary>
        public Data.Enums.SortDirection SortDirection { get; set; }

        /// <summary>
        /// The name of the field to sort by
        /// </summary>
        public String SortBy { get; set; }

        /// <summary>
        /// The page number to start the paging, if applicable. One based (not zero based).
        /// The default is 1.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// The size of the result set, or Max count to return in the actual results.
        /// Default value is 10.
        /// </summary>
        [Range(20, int.MaxValue)]
        public int PageSize { get; set; }

        /// <summary>
        /// The boolean value to indicate whether records for all pages upto the Page Number should be fetched.
        /// False indicates all records upto the Page Number should be fetched staring from Page Number 1
        /// True indicates, records only from the given Page Number should be fetched.
        /// </summary>
        public bool OnlyPageRecords { get; set; }

        /// <summary>
        /// Gets or sets the extension data.
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> ExtensionData { get; set; }

        /// <summary>
        /// Gets a filter value based on the filter name of a certain type, or the defualt value
        /// of that type if the name is not found in the filter object graph.
        /// </summary>
        /// <typeparam name="T">The type of the return value expected by the property name.</typeparam>
        /// <param name="name">The name of the filter property to get the value of</param>
        /// <returns>A strongly typed value of type T for the given named property, or default(T) if name is not found.</returns>
        /// <exception cref="System.InvalidCastException">Thrown when the value specified by name is of a difference type than the type of T expected.</exception>
        public T Get<T>(string name)
        {
            if (!ExtensionData.ContainsKey(name))
                return default(T);

            var obj = ExtensionData[name];
            return (T)obj;
        }//Filter<T>

        /// <summary>
        /// Gets an array-based value based on the filter name of a certain type, or the defualt value
        /// </summary>
        /// <typeparam name="T">The type of the return value expected by the property name.</typeparam>
        /// <param name="name">The name of the filter property to get the value of</param>
        /// <returns>A strongly typed value of type IEnumerable of T for the given named property, or empty IEnumerable of T if name is not found.</returns>
        public IEnumerable<T> GetList<T>(string name)
        {
            if (!ExtensionData.ContainsKey(name))
                return new List<T>();

            return (ExtensionData[name] as JArray).Select(s => s.Value<T>());
        }

        /// <summary>
        /// Fluid interface for setting values inline.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ListCriteria Set(string name, object value)
        {
            this[name] = value;
            return this;
        }

        /// <summary>
        /// Gets or sets the extension data property as a shortcut by name (safe).
        /// </summary>
        /// <param name="name">The name of the property to get or set</param>
        /// <returns>The value of that extension data property, or null</returns>
        public object this[string name]
        {
            get { return ExtensionData.ContainsKey(name) ? ExtensionData[name] : null; }
            set
            {
                if (value == null)
                    ExtensionData.Remove(name);
                else
                    ExtensionData[name] = value;
            }
        }

        /// <summary>
        /// A convenience method to call both SetSort and SetSkip on a MongoCursor
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        internal MongoDB.Driver.MongoCursor<T> SetSortAndSkip<T>(MongoDB.Driver.MongoCursor<T> query) where T : class
        {
            SetSort(query);
            SetSkip(query);
            return query;
        }


        /// <summary>
        /// Sets the sort, if any, on a mongo cursor based on the current criteria
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        internal MongoDB.Driver.MongoCursor<T> SetSort<T>(MongoDB.Driver.MongoCursor<T> query) where T : class
        {
            if (string.IsNullOrWhiteSpace(this.SortBy))
                return query;

            var sortOrder = this.SortDirection == Data.Enums.SortDirection.Ascending ?
                MongoDB.Driver.Builders.SortBy.Ascending(this.SortBy) :
                MongoDB.Driver.Builders.SortBy.Descending(this.SortBy);

            query.SetSortOrder(sortOrder);

            return query;
        }

        /// <summary>
        /// Sets the skip and limit on a mongo cursor based on the current criteria
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        internal MongoDB.Driver.MongoCursor<T> SetSkip<T>(MongoDB.Driver.MongoCursor<T> query) where T : class
        {
            return query.SetSkip(SkipCount).SetLimit(this.PageSize);
        }

        internal int SkipCount
        {
            get
            {
                return Math.Max(0, ((this.PageNumber - 1) * this.PageSize));
            }
        }
    }
}
