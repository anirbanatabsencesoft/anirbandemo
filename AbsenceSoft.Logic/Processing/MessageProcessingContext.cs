﻿using System;

namespace AbsenceSoft.Logic.Processing
{
    public class MessageProcessingContext
    {
        public MessageProcessingContext()
        {
        }

        public ProcessResult? EarlyCompletionResult { get; private set; }

        public void CompleteEarly(ProcessResult result)
        {
            EarlyCompletionResult = result;
        }
    }
}