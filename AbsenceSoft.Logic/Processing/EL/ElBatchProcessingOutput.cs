﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Processing.EL
{
    /// <summary>
    /// Used as method output
    /// </summary>
    public class ElBatchProcessingOutput
    {
        public long ValidRows { get; set; }

        public long InvalidRows { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsCancelled { get; set; }
        
    }
}
