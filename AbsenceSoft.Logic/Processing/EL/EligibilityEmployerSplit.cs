﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Processing.EL
{
    internal class EligibilityEmployerSplit : IDisposable
    {
        private Employer _employer = null;
        private string _fileName = null;
        private TextWriter _writer = null;
        private string _oldUploadId = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="EligibilityEmployerSplit"/> class.
        /// </summary>
        public EligibilityEmployerSplit()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EligibilityEmployerSplit"/> class.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <param name="employerReferenceCode">The employer reference code.</param>
        public EligibilityEmployerSplit(EligibilityUpload upload, string employerReferenceCode) : this()
        {
            EmployerReferenceCode = employerReferenceCode;
            if (upload == null || string.IsNullOrWhiteSpace(EmployerReferenceCode))
                return;
            _oldUploadId = upload.Id;
            Upload = upload.Clone();
            Upload.Clean();
            Upload.Status = Data.Enums.ProcessingStatus.Pending;
            Upload.Employer = Employer.Query.Find(Employer.Query.And(Employer.Query.EQ(e => e.CustomerId, Upload.CustomerId),
                Employer.Query.EQIgnoreCase(e => e.ReferenceCode, EmployerReferenceCode))).FirstOrDefault();
        }

        /// <summary>
        /// Gets or sets the employer reference code.
        /// </summary>
        /// <value>
        /// The employer reference code.
        /// </value>
        public string EmployerReferenceCode { get; set; }

        /// <summary>
        /// Gets or sets the upload.
        /// </summary>
        /// <value>
        /// The upload.
        /// </value>
        public EligibilityUpload Upload { get; set; }

        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return Upload != null && !string.IsNullOrWhiteSpace(EmployerReferenceCode) && Upload.Employer != null;
        }

    

        /// <summary>
        /// Writes the specified record text.
        /// </summary>
        /// <param name="recordText">The record text.</param>
        public void Write(string recordText)
        {
            if (!IsValid())
                return;
            if (string.IsNullOrWhiteSpace(_fileName))
                _fileName = Path.GetTempFileName();
            if (_writer == null)
                _writer = new StreamWriter(_fileName);
            _writer.WriteLine(recordText);
        }

       

        /// <summary>
        /// Completes this instance by compressing the final file, uploading to S3, saving the eligibility upload record
        /// and then queueing the processing of that split chunk.
        /// </summary>
        public void Complete()
        {
            if (!IsValid())
                return;
            // If the writer is still open, close that thing, jeeze
            if (_writer != null)
            {
                _writer.Close();
                _writer.Dispose();
                _writer = null;
            }
            // Now we need to zip up the file and clean up everything.
            string zipFile = null;
            try
            {
                using (FileService fs = new FileService(Upload.Customer, Upload.Employer, Upload.CreatedBy))
                using (EligibilityUploadService svc = new EligibilityUploadService() { CurrentCustomer = Upload.Customer, CurrentEmployer = Upload.Employer, CurrentUser = Upload.CreatedBy })
                {
                    zipFile = fs.CompressFile(_fileName);
                    Upload.FileName = Path.GetFileName(zipFile);
                    Upload.Size = new FileInfo(zipFile).Length;
                    Upload.FileKey = fs.UploadFile(zipFile, Settings.Default.S3BucketName_Processing, Upload.CustomerId, Upload.EmployerId);
                    Upload.LinkedEligibilityUploadId = _oldUploadId;
                    Upload.Status = Data.Enums.ProcessingStatus.Pending;
                    Upload = svc.CreateEligibilityDataFileUpload(Upload);
                }
            }
            catch (Exception ex)
            {
                // If the status is already failed, then it's already been reported, saved and logged.
                if (Upload.Status != Data.Enums.ProcessingStatus.Failed)
                {
                    Log.Error(ex);
                    Upload.Status = Data.Enums.ProcessingStatus.Failed;
                    Upload.Errors = 1;
                    Upload.Save();

                    using (EligibilityUploadService svc = new EligibilityUploadService() { CurrentCustomer = Upload.Customer, CurrentEmployer = Upload.Employer, CurrentUser = Upload.CreatedBy ?? User.System })
                        svc.ReportFailed(Upload.Id, ex.Message);
                }
            }
            finally
            {
                try
                {
                    if (File.Exists(zipFile))
                        File.Delete(zipFile);
                }
                catch { }
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (_writer != null)
                _writer.Dispose();
            _writer = null;
            if (!string.IsNullOrWhiteSpace(_fileName))
                try
                {
                    File.Delete(_fileName);
                }
                catch { }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="EligibilityEmployerSplit"/> class.
        /// </summary>
        ~EligibilityEmployerSplit()
        {
            Dispose();
        }
    }
}
