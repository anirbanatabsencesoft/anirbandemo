﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Processing.EL.Params;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Processing.EL
{
    public class EligibilityUploadBatchProcessingService : LogicService
    {
        #region Properties to reduce database round trips

        /// <summary>
        /// Returns the list of countries instead of repeated database call.
        /// </summary>
        List<Country> _countries;
        public List<Country> Countries
        {
            get
            {
                if (_countries == null)
                {
                    using (var administrationService = new AdministrationService(CurrentUser))
                    {
                        _countries = administrationService.GetCountriesFromXML(includeRegions: false);
                    }
                }
                return _countries;
            }
        }

        #endregion

        #region Member variables

        string _userId;
        JobService _jobService;
        EligibilityUploadService _elService;
       
        BulkWriteOperation<Employee> _bulkEmployee = null;
        BulkWriteOperation<EmployeeContact> _bulkEmployeeContact = null;
        BulkWriteOperation<Case> _bulkCase = null;
        BulkWriteOperation<VariableScheduleTime> _bulkVariableTime = null;
        BulkWriteOperation<ToDoItem> _bulkToDo = null;
        BulkWriteOperation<EmployeeContact> _bulkEmployeeContactCleanup = null;
        BulkWriteOperation<Organization> _bulkOrganization = null;
        BulkWriteOperation<EmployeeOrganization> _bulkEmployeeOrganization = null;
        BulkWriteOperation<Job> _bulkJob = null;
        BulkWriteOperation<EmployeeJob> _bulkEmployeeJob = null;

        EligibilityRowGroupProcessingParams _gp = null;
        EligibilityRowProcessingParams _ep = null;

        EligibilityRow _row = null;
        IGrouping<string, EligibilityUploadResult> _rowGroup = null;
        BatchProcessingArguments _args = null;
        ElBatchProcessingOutput _elpOutput = null;
        IList<EligibilityRow> _rows = null;
        Employee _emp = null;
        IList<Case> _cases = null;
        Employer _employer = null;
        int _calctimeout ;
        #endregion

        /// <summary>
        /// Constructor for injections
        /// </summary>
        public EligibilityUploadBatchProcessingService()
        {
            _elService = new EligibilityUploadService();
            _calctimeout = 300; //value defaulted to 5 mins if the config read fails value will be 5 mins else it will be the one specified in config
            Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["CalculationTimeout"], out _calctimeout);
        }


        #region Private methods
        
        /// <summary>
        /// Check if an upload is cancelled
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CheckIfCancelled(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return true;
            var el = EligibilityUpload.GetById(id) ?? new EligibilityUpload() { Status = ProcessingStatus.Canceled };
            return el.Status == ProcessingStatus.Canceled;
        }

        /// <summary>
        /// Normalize string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string Normalize(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            data = data.Replace("\\n", Environment.NewLine);
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }

        /// <summary>
        /// Validate row
        /// </summary>
        /// <param name="row"></param>
        /// <param name="refCode"></param>
        private void ValidateRow(EligibilityRow row, string refCode)
        {
            if (!string.IsNullOrWhiteSpace(row.EmployerReferenceCode) && !row.EmployerReferenceCode.Equals(refCode, StringComparison.InvariantCultureIgnoreCase))
                row.AddError("'{0}' is not a valid Employer Reference Code. This employer expects a reference code of '{1}' and no other employer matches '{0}'.", row.EmployerReferenceCode, refCode ?? "<none>");
            if (!string.IsNullOrWhiteSpace(row.Country))
            {
                var countryReturned = Countries.Any(x => x.Code == row.Country.ToUpperInvariant());
                if (!countryReturned)
                    row.AddError("Residence Country '{0}' is not a valid 2-character ISO standard country code.", row.Country);
            }
            if (!string.IsNullOrWhiteSpace(row.WorkCountry))
            {
                var countryReturned = Countries.Any(x => x.Code == row.WorkCountry.ToUpperInvariant());
                if (!countryReturned)
                    row.AddError("Work Country '{0}' is not a valid 2-character ISO standard country code.", row.WorkCountry);
            }
        }

        /// <summary>
        /// Returns the Eligibility rows for a group
        /// </summary>
        /// <returns></returns>
        private IList<EligibilityRow> GetEligibilityRowListFromRowGroup()
        {
            // Do the actual translation/copy in groups to minimize memory impact perhaps
            List<EligibilityRow> rows = _rowGroup.Select(u =>
            {
                var row = new EligibilityRow()
                {
                    RowNumber = u.RowNumber,
                    RowResult = u,
                    CustomerId = _args.UploadedDoc.CustomerId,
                    EmployerId = _args.UploadedDoc.EmployerId,
                    EmployeeClassTypes = _args.EmployeeClassTypes
                };

                try
                {
                    //parse
                    row.Parse(u.RowParts, _args.CustomFields);
                    _employer = null;

                    var refCode = "";
                    if(EligibilityRow.IsMultiEmployer(u.RowParts, out refCode))
                    {
                        _employer = Employer.Query.Find(Employer.Query.And(Employer.Query.EQ(e => e.CustomerId, _args.UploadedDoc.CustomerId),
                                        Employer.Query.EQIgnoreCase(e => e.ReferenceCode, refCode))).FirstOrDefault();
                    }
                    else
                    {
                        _employer = _args.UploadedDoc.Employer;
                    }

                    if (_employer != null)
                    {
                        row.EmployerId = _employer.Id;

                        //validate
                        ValidateRow(row, _args.UploadedDoc.Employer == null ? refCode : _args.UploadedDoc.Employer.ReferenceCode);
                    }
                    else
                    {
                        row.AddError("Employer Reference Code {0} is not valid", refCode);
                    }
                }
                catch(Exception ex)
                {
                    if (row != null)
                        row.AddError("An unhandled error occurred. The details: {0}", ex.Message);
                }

                return row;
            }).ToList();

            // Let's get all of our errors out of the way first, so we're not dealing with them later on.
            List<EligibilityRow> errors = rows.Where(row => row.IsError).ToList();

            if (errors.Any())
            {
                long errCount = errors.LongCount();
                _args.UploadedDoc.Processed += errCount;
                _args.UploadedDoc.Errors += errCount;
                
                var bulkErrors = EligibilityUploadResult.Repository.Collection.InitializeUnorderedBulkOperation();
                foreach (var e in errors)
                {
                    rows.Remove(e);
                    var result = e.RowResult;
                    result.Error = e.Error;
                    bulkErrors.Insert(result);
                }
                bulkErrors.Execute();

                // Update our upload with the error count
                _args.UploadedDoc.Save();
            }

            return rows;
        }

        /// <summary>
        /// Returns the employee whether to update or create new
        /// </summary>
        /// <returns></returns>
        private Employee GetEmployee()
        {
            Employee emp = _args.Employees.FirstOrDefault(e => e.EmployeeNumber == _rowGroup.Key && e.IsDeleted == false);
            if (emp == null)
            {
                if (!_rows.Any(x => x.RecType == EligibilityRow.RecordType.Basic || x.RecType == EligibilityRow.RecordType.Eligibility))
                {
                    _rows.ForEach(x => x.AddError("Cannot load a record of type '{0}' when the employee does not already exist or is not defined in the file", x.RecType));
                    _ep.failAll = true;
                    return null;
                }
                else
                {
                    emp = new Employee()
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        CustomerId = _args.UploadedDoc.CustomerId,
                        EmployerId = _employer.Id,

                        CreatedById = _args.UploadedDoc.CreatedById,
                        EmployeeNumber = _rowGroup.Key,
                        CustomFields = _args.CustomFields.Clone(),
                        EmployerName = _employer.Name
                    }.SetCreatedDate(DateTime.UtcNow);

                    _ep.isNew = true;
                }
            }

            return emp;
        }

        /// <summary>
        /// Returns the case list
        /// </summary>
        /// <returns></returns>
        private List<Case> GetCaseListForEmployee()
        {
            if (_ep.failAll || _ep.isNew || _emp == null || string.IsNullOrWhiteSpace(_emp.Id))
                return new List<Case>(0);

            return Case.AsQueryable().Where(c => c.CustomerId == _args.UploadedDoc.CustomerId
                                && c.EmployerId == _args.UploadedDoc.EmployerId
                                && c.Employee.Id == _emp.Id
                                && c.Status != CaseStatus.Cancelled && c.Status != CaseStatus.Closed)
                                .ToList();
        }

        
        /// <summary>
        /// Returns the last row key
        /// </summary>
        /// <returns></returns>
        private string GetLastRowKey()
        {
            string lastRowKey = "LastERow";
            switch (_row.RecType)
            {
                case EligibilityRow.RecordType.Eligibility:
                    lastRowKey = "LastERow";
                    break;
                case EligibilityRow.RecordType.Spouse:
                    lastRowKey = "LastSRow";
                    break;
                case EligibilityRow.RecordType.Basic:
                    lastRowKey = "LastBRow";
                    break;
                case EligibilityRow.RecordType.Supervisor:
                    lastRowKey = "LastURow";
                    break;
                case EligibilityRow.RecordType.HR:
                    lastRowKey = "LastHRow";
                    break;
                case EligibilityRow.RecordType.Custom:
                    lastRowKey = "LastCRow";
                    break;
                case EligibilityRow.RecordType.Schedule:
                    lastRowKey = "LastWRow";
                    break;
                case EligibilityRow.RecordType.Job:
                    lastRowKey = "LastJRow";
                    break;
                default:
                    lastRowKey = null;
                    break;
            }

            return lastRowKey;
        }

        /// <summary>
        /// Inserts the scheduled time for an employee
        /// </summary>
        private void InsertScheduledTime()
        {
            try
            {
                if (_ep.isNew)
                {
                    if (!_row.TimeScheduled.HasValue || _row.TimeScheduled.Value <= 0)
                        return;

                    var time = new VariableScheduleTime()
                    {
                        CreatedById = _userId,
                        ModifiedById = _userId,
                        CustomerId = _emp.CustomerId,
                        EmployerId = _emp.EmployerId,
                        EmployeeId = _emp.Id,
                        EmployeeNumber = _emp.EmployeeNumber,
                        Time = new Time()
                        {
                            SampleDate = _row.DateScheduled.Value,
                            TotalMinutes = _row.TimeScheduled
                        }
                    };
                    time.SetCreatedDate(DateTime.UtcNow);
                    time.SetModifiedDate(DateTime.UtcNow);
                    _bulkVariableTime.Insert(time);
                    _gp.bulkVariableTimeOps++;
                }
                else
                {
                    var b = _bulkVariableTime.Find(VariableScheduleTime.Query.And(
                        VariableScheduleTime.Query.EQ(v => v.EmployeeId, _emp.Id),
                        VariableScheduleTime.Query.NotExists(v => v.EmployeeCaseId),
                        VariableScheduleTime.Query.EQ(v => v.Time.SampleDate, _row.DateScheduled.Value)
                    ));
                    if (_row.TimeScheduled == null || _row.TimeScheduled.Value <= 0)
                        b.RemoveOne();
                    else
                        b.Upsert().UpdateOne(VariableScheduleTime.Updates
                            .Set(v => v.Time.TotalMinutes, _row.TimeScheduled)
                            .Set(v => v.ModifiedById, _userId)
                            .Set(v => v.ModifiedDate, DateTime.UtcNow)
                            .SetOnInsert(v => v.CreatedById, _userId)
                            .SetOnInsert(v => v.CreatedDate, DateTime.UtcNow)
                            .SetOnInsert(v => v.CustomerId, _emp.CustomerId)
                            .SetOnInsert(v => v.EmployerId, _emp.EmployerId)
                            .SetOnInsert(v => v.EmployeeNumber, _emp.EmployeeNumber)
                            .SetOnInsert(v => v.EmployeeId, _emp.Id)
                            .SetOnInsert(v => v.Time.SampleDate, _row.DateScheduled.Value)
                            .SetOnInsert(v => v.Time.Id, Guid.NewGuid())
                        );
                    _gp.bulkVariableTimeOps++;
                    _ep.reCalcOpenCases = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Time Scheduled row", ex);
                _row.AddError("Error processing Time Scheduled row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Check if job exists
        /// </summary>
        /// <returns></returns>
        private bool CheckJobExists()
        {
            bool exists = false;
            if (_rows.Any(x => x.RecType == EligibilityRow.RecordType.Job))
            {
                var newRow = _rows.Where(i => i.JobCode == _row.JobCode && i.RowResult.RowParts[i.RowResult.RowParts.Length - 1] == "1").ToList();
                if (newRow.Count > 0)
                {
                    if (_row.RowResult.RowParts[_row.RowResult.RowParts.Length - 1] == "0")
                    {
                        var newEndDate = newRow[0].JobStartDate;
                        _row.JobEndDate = newEndDate.Value.AddDays(-1);
                    }
                    else
                    {
                        exists = true;
                    }
                }
            }
            return exists;
        }

        /// <summary>
        /// Insert supervisor
        /// </summary>
        /// <returns></returns>
        private bool InsertSupervisor()
        {
            try
            {
                // Only try to match up the Supervisor/Manager IF no name is passed in
                if (!string.IsNullOrWhiteSpace(_row.ManagerEmployeeNumber) && string.IsNullOrWhiteSpace(_row.ManagerLastName) && string.IsNullOrWhiteSpace(_row.ManagerFirstName))
                {
                    var cEmp = _args.Employees.FirstOrDefault(e => e.EmployeeNumber == _row.ManagerEmployeeNumber);
                    if (cEmp == null)
                    {
                        var rEmp = _rows.FirstOrDefault(e => e.EmployeeNumber == _row.ManagerEmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                        if (rEmp != null)
                        {
                            _row.ManagerLastName = rEmp.LastName ?? _row.ManagerLastName;
                            _row.ManagerFirstName = rEmp.FirstName ?? _row.ManagerFirstName;
                            _row.ManagerPhone = rEmp.PhoneWork ?? _row.ManagerPhone;
                            _row.ManagerEmail = StringUtils.LowercaseFirstNotNullString(rEmp.Email, _row.ManagerEmail);
                        }
                        else
                        {
                            _row.ManagerLastName = _row.ManagerLastName ?? _row.ManagerEmployeeNumber;
                            _row.ManagerFirstName = _row.ManagerFirstName ?? "Supervisor";
                        }
                    }
                    else
                    {
                        _row.ManagerLastName = cEmp.LastName ?? _row.ManagerLastName;
                        _row.ManagerFirstName = cEmp.FirstName ?? _row.ManagerFirstName;
                        _row.ManagerPhone = cEmp.Info.WorkPhone ?? _row.ManagerPhone;
                        _row.ManagerEmail = StringUtils.LowercaseFirstNotNullString(cEmp.Info.Email, _row.ManagerEmail);
                    }
                }

                if (_ep.isNew)
                {
                    _bulkEmployeeContact.Insert(new EmployeeContact()
                    {
                        CustomerId = _emp.CustomerId,
                        EmployerId = _emp.EmployerId,
                        EmployeeId = _emp.Id,
                        EmployeeNumber = _emp.EmployeeNumber,
                        MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                        CreatedById = _userId,
                        ModifiedById = _userId,
                        ContactTypeCode = "SUPERVISOR",
                        ContactTypeName = "Supervisor",
                        Contact = new Contact()
                        {
                            LastName = _row.ManagerLastName,
                            FirstName = _row.ManagerFirstName,
                            WorkPhone = _row.ManagerPhone,
                            Email = _row.ManagerEmail == null ? null : _row.ManagerEmail.ToLowerInvariant()
                        },
                        RelatedEmployeeNumber = _row.ManagerEmployeeNumber
                    }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                }
                else
                {
                    _bulkEmployeeContact.Find(EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.NE(e => e.IsDeleted, true),
                        EmployeeContact.Query.EQ(e => e.ContactTypeCode, "SUPERVISOR")
                    ))
                    .Upsert()
                    .UpdateOne(EmployeeContact.Updates
                        .Set(c => c.ModifiedById, _userId)
                        .Set(c => c.ModifiedDate, DateTime.UtcNow)
                        .Set(c => c.RelatedEmployeeNumber, _row.ManagerEmployeeNumber)
                        .Set(c => c.Contact.LastName, _row.ManagerLastName)
                        .Set(c => c.Contact.FirstName, _row.ManagerFirstName)
                        .Set(c => c.Contact.WorkPhone, _row.ManagerPhone)
                        .Set(c => c.Contact.Email, _row.ManagerEmail == null ? null : _row.ManagerEmail.ToLowerInvariant())
                        .SetOnInsert(c => c.CustomerId, _emp.CustomerId)
                        .SetOnInsert(c => c.EmployerId, _emp.EmployerId)
                        .SetOnInsert(c => c.EmployeeId, _emp.Id)
                        .SetOnInsert(c => c.EmployeeNumber, _emp.EmployeeNumber)
                        .SetOnInsert(c => c.CreatedById, _userId)
                        .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(c => c.ContactTypeCode, "SUPERVISOR")
                        .SetOnInsert(c => c.ContactTypeName, "Supervisor")
                        .SetOnInsert(c => c.MilitaryStatus, AbsenceSoft.Data.Enums.MilitaryStatus.Civilian)
                        .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.Contact.Address.Country, "US")
                        .Combine(Update.Unset("Source"))
                    );
                }
                _gp.bulkEmployeeContactOps++;
                _gp.contactTypeCodes.Add("SUPERVISOR");
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Manager/Supervisor row", ex);
                _row.AddError("Error processing Manager/Supervisor row, {0}", ex.Message);
                if (_row.RecType == EligibilityRow.RecordType.Eligibility)
                    _ep.updateEE = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update basic record
        /// </summary>
        /// <returns></returns>
        private void UpdateBasicRecord()
        {
            try
            {
                _elService.UpdateBasicRecordType(_userId, _bulkOrganization, _bulkEmployeeOrganization, _ep.isNew, ref _ep.updateEE, ref _ep.reCalcOpenCases, _emp, _row, ref _gp.bulkOrgOps, ref _gp.bulkEmpOrgOps, ref _ep.updateCasesOfficeLocation);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Basic row", ex);
                _row.AddError("Error processing Basic row, {0}", ex.Message);
                _ep.updateEE = false;
            }
        }

        /// <summary>
        /// Insert Hr
        /// </summary>
        /// <param name="row"></param>
        /// <param name="rows"></param>
        /// <param name="emp"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private bool InsertHr()
        {
            try
            {
                // Only try to match up the HR peep IF no name is passed in
                if (!string.IsNullOrWhiteSpace(_row.HREmployeeNumber) && string.IsNullOrWhiteSpace(_row.HRLastName) && string.IsNullOrWhiteSpace(_row.HRLastName))
                {
                    var cEmp = _args.Employees.FirstOrDefault(e => e.EmployeeNumber == _row.HREmployeeNumber);
                    if (cEmp == null)
                    {
                        var rEmp = _rows.FirstOrDefault(e => e.EmployeeNumber == _row.HREmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                        if (rEmp != null)
                        {
                            _row.HRLastName = rEmp.LastName ?? _row.HRLastName;
                            _row.HRFirstName = rEmp.FirstName ?? _row.HRFirstName;
                            _row.HRPhone = rEmp.PhoneWork ?? _row.HRPhone;
                            _row.HREmail = StringUtils.LowercaseFirstNotNullString(rEmp.Email, _row.HREmail);
                        }
                        else
                        {
                            _row.HRLastName = _row.HRLastName ?? _row.HREmployeeNumber;
                            _row.HRFirstName = _row.HRFirstName ?? "HR";
                        }
                    }
                    else
                    {
                        _row.HRLastName = cEmp.LastName ?? _row.HRLastName;
                        _row.HRFirstName = cEmp.FirstName ?? _row.HRFirstName;
                        _row.HRPhone = cEmp.Info.WorkPhone ?? _row.HRPhone;
                        _row.HREmail = StringUtils.LowercaseFirstNotNullString(cEmp.Info.Email, _row.HREmail);
                    }
                }

                if (_ep.isNew)
                {
                    _bulkEmployeeContact.Insert(new EmployeeContact()
                    {
                        CustomerId = _emp.CustomerId,
                        EmployerId = _emp.EmployerId,
                        EmployeeId = _emp.Id,
                        EmployeeNumber = _emp.EmployeeNumber,
                        MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                        CreatedById = _userId,
                        ModifiedById = _userId,
                        ContactTypeCode = "HR",
                        ContactTypeName = "HR",
                        Contact = new Contact()
                        {
                            LastName = _row.HRLastName,
                            FirstName = _row.HRFirstName,
                            WorkPhone = _row.HRPhone,
                            Email = _row.HREmail == null ? null : _row.HREmail.ToLowerInvariant()
                        },
                        RelatedEmployeeNumber = _row.HREmployeeNumber
                    }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                }
                else
                {
                    _bulkEmployeeContact.Find(EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.NE(e => e.IsDeleted, true),
                        EmployeeContact.Query.EQ(e => e.ContactTypeCode, "HR")
                    ))
                    .Upsert()
                    .UpdateOne(EmployeeContact.Updates
                        .Set(c => c.ModifiedById, _userId)
                        .Set(c => c.ModifiedDate, DateTime.UtcNow)
                        .Set(c => c.RelatedEmployeeNumber, _row.HREmployeeNumber)
                        .Set(c => c.Contact.LastName, _row.HRLastName)
                        .Set(c => c.Contact.FirstName, _row.HRFirstName)
                        .Set(c => c.Contact.WorkPhone, _row.HRPhone)
                        .Set(c => c.Contact.Email, _row.HREmail == null ? null : _row.HREmail.ToLowerInvariant())
                        .SetOnInsert(c => c.CustomerId, _emp.CustomerId)
                        .SetOnInsert(c => c.EmployerId, _emp.EmployerId)
                        .SetOnInsert(c => c.EmployeeId, _emp.Id)
                        .SetOnInsert(c => c.EmployeeNumber, _emp.EmployeeNumber)
                        .SetOnInsert(c => c.CreatedById, _userId)
                        .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(c => c.ContactTypeCode, "HR")
                        .SetOnInsert(c => c.ContactTypeName, "HR")
                        .SetOnInsert(c => c.MilitaryStatus, AbsenceSoft.Data.Enums.MilitaryStatus.Civilian)
                        .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.Contact.Address.Country, "US")
                        .Combine(Update.Unset("Source"))
                    );
                }
                _gp.contactTypeCodes.Add("HR");
                _gp.bulkEmployeeContactOps++;
            }
            catch (Exception ex)
            {
                Log.Error("Error processing HR row", ex);
                _row.AddError("Error processing HR row, {0}", ex.Message);
                if (_row.RecType == EligibilityRow.RecordType.Eligibility)
                    _ep.updateEE = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Insert Spouse
        /// </summary>
        /// <param name="row"></param>
        /// <param name="rows"></param>
        /// <param name="emp"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private bool InsertSpouse()
        {
            try
            {
                // Deal with the Spouse
                if (_row.SpouseEmployeeNumber != _emp.SpouseEmployeeNumber)
                {
                    _emp.SpouseEmployeeNumber = _row.SpouseEmployeeNumber;
                    _ep.updateEE = true;

                    string sFirst = null, sLast = null;
                    MilitaryStatus sMil = Data.Enums.MilitaryStatus.Civilian;

                    var cEmp = _args.Employees.FirstOrDefault(e => e.EmployeeNumber == _row.SpouseEmployeeNumber);
                    if (cEmp == null)
                    {
                        var rEmp = _rows.FirstOrDefault(e => e.EmployeeNumber == _row.SpouseEmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                        if (rEmp != null)
                        {
                            sLast = rEmp.LastName;
                            sFirst = rEmp.FirstName;
                            if (rEmp.MilitaryStatus.HasValue)
                                sMil = (MilitaryStatus)rEmp.MilitaryStatus.Value;
                        }
                    }
                    else
                    {
                        sLast = cEmp.LastName;
                        sFirst = cEmp.FirstName;
                        sMil = cEmp.MilitaryStatus;
                    }

                    sFirst = sFirst ?? "Spouse";
                    sLast = sLast ?? _row.SpouseEmployeeNumber;

                    if (_ep.isNew)
                    {
                        _bulkEmployeeContact.Insert(new EmployeeContact()
                        {
                            CustomerId = _emp.CustomerId,
                            EmployerId = _emp.EmployerId,
                            EmployeeId = _emp.Id,
                            EmployeeNumber = _emp.EmployeeNumber,
                            CreatedById = _userId,
                            ModifiedById = _userId,
                            ContactTypeCode = "SPOUSE",
                            ContactTypeName = "Spouse",
                            Contact = new Contact()
                            {
                                LastName = sLast,
                                FirstName = sFirst
                            },
                            RelatedEmployeeNumber = _row.SpouseEmployeeNumber,
                            MilitaryStatus = sMil
                        }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                    }
                    else
                    {
                        _bulkEmployeeContact.Find(EmployeeContact.Query.And(
                            EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                            EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                            EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                            EmployeeContact.Query.NE(e => e.IsDeleted, true),
                            EmployeeContact.Query.EQ(e => e.ContactTypeCode, "SPOUSE")
                        ))
                        .Upsert()
                        .UpdateOne(EmployeeContact.Updates
                            .Set(c => c.ModifiedById, _userId)
                            .Set(c => c.ModifiedDate, DateTime.UtcNow)
                            .Set(c => c.RelatedEmployeeNumber, _row.SpouseEmployeeNumber)
                            .Set(c => c.Contact.LastName, sLast)
                            .Set(c => c.Contact.FirstName, sFirst)
                            .Set(c => c.MilitaryStatus, sMil)
                            .SetOnInsert(c => c.CustomerId, _emp.CustomerId)
                            .SetOnInsert(c => c.EmployerId, _emp.EmployerId)
                            .SetOnInsert(c => c.EmployeeNumber, _emp.EmployeeNumber)
                            .SetOnInsert(c => c.EmployeeId, _emp.Id)
                            .SetOnInsert(c => c.CreatedById, _userId)
                            .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                            .SetOnInsert(c => c.ContactTypeCode, "SPOUSE")
                            .SetOnInsert(c => c.ContactTypeName, "Spouse")
                            .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                            .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                            .SetOnInsert(c => c.Contact.Address.Country, "US")
                        );
                    }
                    _gp.bulkEmployeeContactOps++;
                    _gp.contactTypeCodes.Add("SPOUSE");
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Eligibility row", ex);
                _row.AddError("Error processing Eligibility row, {0}", ex.Message);
                if (_row.RecType == EligibilityRow.RecordType.Eligibility)
                    _ep.updateEE = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Insert the employee contact
        /// </summary>
        /// <param name="row"></param>
        /// <param name="emp"></param>
        /// <param name="args"></param>
        private void InsertContact()
        {
            try
            {
                string contactTypeName = _args.ContactTypes.Any(c => c.Key == _row.ContactTypeCode) ? _args.ContactTypes[_row.ContactTypeCode].Name :  IsEmployerContactType(_row.ContactTypeCode) ? GetEmployerContactTypeByCode(_row.ContactTypeCode).Name : null;

                string email = string.IsNullOrWhiteSpace(_row.Email) ? null : _row.Email.ToLowerInvariant();
                List<IMongoQuery> matchOR = new List<IMongoQuery>();
                if (!string.IsNullOrWhiteSpace(_row.ContactEmployeeNumber))
                    matchOR.Add(EmployeeContact.Query.EQ(e => e.RelatedEmployeeNumber, _row.ContactEmployeeNumber));
                if (!string.IsNullOrWhiteSpace(email))
                    matchOR.Add(EmployeeContact.Query.EQ(e => e.Contact.Email, email));
                else if (string.IsNullOrWhiteSpace(_row.ContactEmployeeNumber) && !string.IsNullOrWhiteSpace(_row.FirstName) || !string.IsNullOrWhiteSpace(_row.LastName))
                    matchOR.Add(Query.And(EmployeeContact.Query.EQ(e => e.Contact.FirstName, _row.FirstName), EmployeeContact.Query.EQ(e => e.Contact.LastName, _row.LastName)));

                if (_row.ContactExclusive ?? false)
                {
                    var q = EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.NE(e => e.IsDeleted, true),
                        EmployeeContact.Query.EQ(e => e.ContactTypeCode, _row.ContactTypeCode)
                    );
                    _bulkEmployeeContactCleanup.Find(q).Update(EmployeeContact.Updates
                        .Set(c => c.ModifiedById, _userId)
                        .CurrentDate(c => c.ModifiedDate)
                        .Set(c => c.IsDeleted, true)
                        .Combine(Update
                        .Set("EL", _args.UploadedDoc.FileName)
                        .Set("ELPart", "ContactExclusive")
                        .Set("ELPartQuery", q.ToString())
                        .Unset("Source")));
                    _gp.bulkEmployeeContactCleanupOps++;

                }
                if (_row.ContactPosition.HasValue && matchOR.Any())
                {
                    var q = EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.NE(e => e.IsDeleted, true),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.EQ(e => e.ContactTypeCode, _row.ContactTypeCode),
                        Query.Or(matchOR),
                        EmployeeContact.Query.NE(e => e.ContactPosition, _row.ContactPosition)
                    );
                    _bulkEmployeeContactCleanup.Find(q).Update(EmployeeContact.Updates
                        .Set(c => c.ModifiedById, _userId)
                        .CurrentDate(c => c.ModifiedDate)
                        .Set(c => c.IsDeleted, true)
                        .Combine(Update
                        .Set("EL", _args.UploadedDoc.FileName)
                        .Set("ELPart", "ContactPosition")
                        .Set("ELPartQuery", q.ToString())
                        .Unset("Source")));
                    _gp.bulkEmployeeContactCleanupOps++;
                }

                if (_ep.isNew || _row.ClearOtherContacts)
                {
                    var empContact = new EmployeeContact()
                    {
                        CustomerId = _emp.CustomerId,
                        EmployerId = _emp.EmployerId,
                        EmployeeId = _emp.Id,
                        EmployeeNumber = _emp.EmployeeNumber,
                        MilitaryStatus = ((MilitaryStatus?)_row.MilitaryStatus) ?? MilitaryStatus.Civilian,
                        CreatedById = _userId,
                        ModifiedById = _userId,
                        ContactTypeCode = _row.ContactTypeCode,
                        ContactTypeName = _row.ContactTypeName ?? contactTypeName ?? _row.ContactTypeCode,
                        Contact = new Contact()
                        {
                            LastName = _row.LastName,
                            FirstName = _row.FirstName,
                            WorkPhone = _row.PhoneWork,
                            CellPhone = _row.PhoneMobile,
                            HomePhone = _row.PhoneHome,
                            Fax = _row.PhoneFax,
                            Email = email,
                            AltEmail = string.IsNullOrWhiteSpace(_row.EmailAlt) ? null : _row.EmailAlt.ToLowerInvariant(),
                            Address = new Address()
                            {
                                Address1 = _row.Address,
                                Address2 = _row.Address2,
                                City = _row.City,
                                State = _row.State,
                                Country = _row.Country ?? "US",
                                PostalCode = _row.PostalCode
                            }
                        },
                        RelatedEmployeeNumber = _row.ContactEmployeeNumber,
                        ContactPosition = _row.ContactPosition
                    }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                    empContact.Metadata.SetRawValue("EL", _args.UploadedDoc.FileName);
                    empContact.Metadata.SetRawValue("ELPart", "Insert");
                    _bulkEmployeeContact.Insert(empContact);
                }
                else
                {
                    var q = EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.EQ(e => e.ContactTypeCode, _row.ContactTypeCode),
                        !(_row.ContactExclusive ?? false) && _row.ContactPosition.HasValue ?
                            EmployeeContact.Query.EQ(e => e.ContactPosition, _row.ContactPosition) :
                            Query.Or(matchOR)
                    );
                    _bulkEmployeeContact.Find(q)
                    .Upsert()
                    .UpdateOne(EmployeeContact.Updates
                        .SetOnInsert(c => c.CustomerId, _emp.CustomerId)
                        .SetOnInsert(c => c.EmployerId, _emp.EmployerId)
                        .SetOnInsert(c => c.EmployeeId, _emp.Id)
                        .SetOnInsert(c => c.EmployeeNumber, _emp.EmployeeNumber)
                        .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                        .SetOnInsert(c => c.CreatedById, _userId)
                        .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                        .Set(c => c.ModifiedById, _userId)
                        .CurrentDate(c => c.ModifiedDate)
                        .Set(c => c.RelatedEmployeeNumber, _row.ContactEmployeeNumber)
                        .Set(c => c.Contact.LastName, _row.LastName)
                        .Set(c => c.Contact.FirstName, _row.FirstName)
                        .Set(c => c.Contact.WorkPhone, _row.PhoneWork)
                        .Set(c => c.Contact.HomePhone, _row.PhoneHome)
                        .Set(c => c.Contact.Fax, _row.PhoneFax)
                        .Set(c => c.Contact.CellPhone, _row.PhoneMobile)
                        .Set(c => c.Contact.Email, email)
                        .Set(c => c.Contact.AltEmail, _row.EmailAlt?.ToLowerInvariant())
                        .Set(c => c.ContactTypeCode, _row.ContactTypeCode)
                        .Set(c => c.ContactTypeName, _row.ContactTypeName ?? contactTypeName ?? _row.ContactTypeCode)
                        .Set(c => c.MilitaryStatus, ((MilitaryStatus?)_row.MilitaryStatus) ?? MilitaryStatus.Civilian)
                        .Set(c => c.Contact.Address.Address1, _row.Address)
                        .Set(c => c.Contact.Address.Address2, _row.Address2)
                        .Set(c => c.Contact.Address.City, _row.City)
                        .Set(c => c.Contact.Address.State, _row.State)
                        .Set(c => c.Contact.Address.Country, _row.Country ?? "US")
                        .Set(c => c.Contact.Address.PostalCode, _row.PostalCode)
                        .Set(c => c.ContactPosition, _row.ContactPosition)
                        .Set(c => c.IsDeleted, false)
                        .Combine(Update
                            .Set("EL", _args.UploadedDoc.FileName)
                            .Set("ELPart", "Upsert")
                            .Set("ELPartQuery", q.ToString())
                            .Unset("Source"))
                    );
                }
                _gp.bulkEmployeeContactOps++;
                _gp.contactTypeCodes.Add(_row.ContactTypeCode);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error processing '{0}' employee contact row", _row.ContactTypeCode), ex);
                _row.AddError("Error processing '{0}' employee contact row, {1}", _row.ContactTypeCode, ex.Message);
            }
        }

        /// <summary>
        /// Check if contact type code exists in Employer Contact Types
        /// </summary>
        /// <param name="contactTypeCode">Contact Type Code</param>
        /// <returns>TrueorFalse</returns>
        private bool IsEmployerContactType(string contactTypeCode)
        {
            return EmployerContactType.AsQueryable().Any(e => (e.CustomerId == null || e.CustomerId == _args.UploadedDoc.CustomerId) && e.Code == contactTypeCode);
        }

        /// <summary>
        /// Gets Employer Contact Type By contact type Code
        /// </summary>
        /// <param name="contactTypeCode">Contact Type Code</param>
        /// <returns>EmployerContactType</returns>
        private EmployerContactType GetEmployerContactTypeByCode(string contactTypeCode)
        {
            return EmployerContactType.AsQueryable().FirstOrDefault(e => (e.CustomerId == null || e.CustomerId == _args.UploadedDoc.CustomerId) && e.Code == contactTypeCode);
        }

        /// <summary>
        /// Inserts schedule
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="row"></param>
        private void InsertSchedule()
        {
            try
            {
                if (_row.HoursWorkedIn12Months.HasValue)
                {
                    _emp.PriorHours = _emp.PriorHours ?? new List<PriorHours>();
                    _emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)_row.HoursWorkedIn12Months.Value, AsOf = (_row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                    _ep.updateEE = true;
                }

                if (_row.AverageMinutesWorkedPerWeek.HasValue)
                {
                    _emp.MinutesWorkedPerWeek = _emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                    _emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = _row.AverageMinutesWorkedPerWeek.Value, AsOf = (_row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                    _ep.updateEE = true;
                }


                Schedule workSched = new Schedule() { ScheduleType = _row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                workSched.StartDate = (_row.ScheduleEffectiveDate ?? (_ep.isNew && _emp.HireDate.HasValue ? _emp.HireDate.Value : DateTime.UtcNow)).ToMidnight();

                if (_row.FtePercentage.HasValue)
                {
                    workSched.FteMinutesPerWeek = (int)Math.Round(((decimal)(_employer.FTWeeklyWorkHours ?? 0 ) * (_row.FtePercentage.Value / 100)) * 60);
                    workSched.FteTimePercentage = _row.FtePercentage.Value;
                    workSched.ScheduleType = ScheduleType.FteVariable;
                    workSched.FteWeeklyDuration = FteWeeklyDuration.FtePercentage;
                }
                else if (_row.AverageMinutesWorkedPerWeek.HasValue && workSched.ScheduleType == ScheduleType.Variable)
                {
                    workSched.FteMinutesPerWeek = _row.AverageMinutesWorkedPerWeek.Value;
                    workSched.ScheduleType = ScheduleType.FteVariable;
                    workSched.FteWeeklyDuration = FteWeeklyDuration.FteTimePerWeek;
                }
                else if (!_row.WorkTimeSun.HasValue &&
                    !_row.WorkTimeMon.HasValue &&
                    !_row.WorkTimeTue.HasValue &&
                    !_row.WorkTimeWed.HasValue &&
                    !_row.WorkTimeThu.HasValue &&
                    !_row.WorkTimeFri.HasValue &&
                    !_row.WorkTimeSat.HasValue)
                {
                    if (!_row.MinutesPerWeek.HasValue)
                        workSched = null;
                    else
                    {
                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                        int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)_row.MinutesPerWeek.Value / 5m));
                        int leftOverMinute = _row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                        for (var d = 0; d < 7; d++)
                        {
                            if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                            else
                                workSched.Times.Add(new Time() { SampleDate = sample });
                            sample = sample.AddDays(1);
                        }
                        // Add our leftover minute to the first date, clugey, but it's whatever.
                        workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                    }
                }
                else
                {
                    DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                    for (var d = 0; d < 7; d++)
                    {
                        int? workTime = null;
                        switch (sample.DayOfWeek)
                        {
                            case DayOfWeek.Sunday: workTime = _row.WorkTimeSun; break;
                            case DayOfWeek.Monday: workTime = _row.WorkTimeMon; break;
                            case DayOfWeek.Tuesday: workTime = _row.WorkTimeTue; break;
                            case DayOfWeek.Wednesday: workTime = _row.WorkTimeWed; break;
                            case DayOfWeek.Thursday: workTime = _row.WorkTimeThu; break;
                            case DayOfWeek.Friday: workTime = _row.WorkTimeFri; break;
                            case DayOfWeek.Saturday: workTime = _row.WorkTimeSat; break;
                        }
                        workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                        sample = sample.AddDays(1);
                    }
                }

                bool lastScheduleUserEntered = false;
                if (!_ep.isNew && workSched != null)
                {
                    Schedule dup = _emp.WorkSchedules == null ? null : _emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                    if (dup != null && dup.Metadata.GetRawValue<bool?>("SystemEntered") == true)
                        lastScheduleUserEntered = true;
                    if (dup != null && dup.ScheduleType == ScheduleType.FteVariable && workSched.ScheduleType == ScheduleType.FteVariable)
                    {
                        if (dup.FteMinutesPerWeek == workSched.FteMinutesPerWeek)
                        {
                            workSched = null;
                        }
                    }
                    else if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                    {
                        // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                        bool isMatch = true;
                        foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                            if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                            {
                                isMatch = false;
                                break;
                            }
                        // See if the schedules still look the same
                        if (isMatch)
                            workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                    }
                }

                if (workSched != null && (!(_row.NoOverwriteFlag == true) || lastScheduleUserEntered))
                {
                    workSched.Metadata.SetRawValue("SystemEntered", true);
                    new EmployeeService().Using(s => s.SetWorkSchedule(_emp, workSched, null));
                    _ep.updateEE = true;
                    _ep.reCalcOpenCases = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error("Error processing Work Schedule row", ex);
                _row.AddError("Error processing Work Schedule row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Insert custom data
        /// </summary>        
        private void InsertCustom()
        {
            try
            {
                if (_emp.CustomFields == null)
                    _emp.CustomFields = _row.CustomFields.ToList();
                else if (!_emp.CustomFields.Any())
                    _emp.CustomFields.AddRange(_row.CustomFields);
                else
                {
                    foreach (var c in _row.CustomFields)
                    {
                        var m = _emp.CustomFields.FirstOrDefault(x => x.Name == c.Name || x.Id == c.Id);
                        if (m == null)
                            _emp.CustomFields.AddFluid(c).ModifiedById = _userId;
                        else
                        {
                            m.DataType = c.DataType;
                            m.Description = c.Description;
                            m.FileOrder = c.FileOrder;
                            m.HelpText = c.HelpText;
                            m.IsRequired = c.IsRequired;
                            m.Label = c.Label;
                            m.ListValues = c.ListValues;
                            m.ValueType = c.ValueType;
                            m.SelectedValue = c.SelectedValue;
                            m.ModifiedById = _userId;
                        }
                    }
                }
                _ep.updateEE = true;
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Custom Fields row", ex);
                _row.AddError("Error processing Custom Fields row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Insert intermittent records
        /// </summary>      
        private void InsertIntermittent()
        {
            try
            {
                var myCase = _cases.FirstOrDefault(c => c.CaseNumber == _row.CaseNumber);

                if (myCase == null)
                {
                    // query to find is case status closed
                    myCase = Case.AsQueryable().FirstOrDefault(c => c.CustomerId == _args.UploadedDoc.CustomerId && c.EmployerId == _args.UploadedDoc.EmployerId && c.Employee.Id == _emp.Id && c.CaseNumber == _row.CaseNumber);

                    if (myCase != null && myCase.Status == CaseStatus.Closed)
                    {
                        _row.AddError("Case number '{0}' is closed. Case must have an open status to load time off request.", _row.CaseNumber);
                        return;
                    }
                    else
                    {
                        _row.AddError("A case with number '{0}' was not found for the employee with number {1}", _row.CaseNumber, _row.EmployeeNumber);
                        return;
                    }
                }

                using (CaseService svc = new CaseService(_row.Workflow == true))
                {
                    string pId = null;
                    if (!string.IsNullOrWhiteSpace(_row.PolicyCode))
                    {
                        var policy = Policy.GetByCode(_row.PolicyCode, myCase.CustomerId, myCase.EmployerId);
                        if (policy == null)
                        {
                            _row.AddError("The policy code '{0}' supplied for intermittent time on  '{2:MM/dd/yyyy}' for case '{1}' was not found in the system. Please ensure this policy is properly configured.", _row.PolicyCode, myCase.CaseNumber, _row.DateOfAbsence);
                            return;
                        }
                        pId = policy.Code;
                    }
                    var intSeg = myCase.Segments.FirstOrDefault(s => s.Type == CaseType.Intermittent && _row.DateOfAbsence.Value.DateInRange(s.StartDate, s.EndDate));
                    if (intSeg == null)
                    {
                        _row.AddError("No intermittent period for this case was found for date '{0:MM/dd/yyyy}'.", _row.DateOfAbsence);
                        return;
                    }
                    List<IntermittentTimeRequest> times = new List<IntermittentTimeRequest>();
                    IntermittentTimeRequest t = new IntermittentTimeRequest()
                    {
                        EmployeeEntered = false,
                        RequestDate = _row.DateOfAbsence.Value,
                        Notes = "Created through Case Migration",
                        IntermittentType = _row.IntermittentTimeOutRequestType
                    };
                    if (_row.StartTimeForLeave != null && _row.EndTimeForLeave != null)
                    {
                        t.StartTimeForLeave = _row.StartTimeForLeave;
                        t.EndTimeForLeave = _row.EndTimeForLeave;
                    }
                    times.Add(t);
                    t.TotalMinutes = (_row.MinutesApproved ?? 0) + (_row.MinutesDenied ?? 0) + (_row.MinutesPending ?? 0);
                    if (!string.IsNullOrWhiteSpace(pId) && !intSeg.AppliedPolicies.Any(p => p.Policy.Code == pId))
                    {
                        _row.AddError("The policy '{0}' was not found covering any intermittent period of time for the case and may not be used.", _row.PolicyCode);
                        return;
                    }
                    List<IntermittentTimeRequestDetail> detail = new List<IntermittentTimeRequestDetail>();
                    detail.AddRange(intSeg.AppliedPolicies.Where(p => (pId == null || pId == p.Policy.Code) && p.Status == EligibilityStatus.Eligible)
                        .Select(p => new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = p.Policy.Code,
                            Approved = _row.MinutesApproved ?? 0,
                            Denied = _row.MinutesDenied ?? 0,
                            Pending = _row.MinutesPending ?? 0,
                            DenialReasonCode = _row.MinutesDenied.HasValue && _row.MinutesDenied > 0 ? AdjudicationDenialReason.Other : null,
                            DeniedReasonOther = _row.MinutesDenied.HasValue && _row.MinutesDenied > 0 ? "Denied through Case Migration" : null
                        }));
                    detail.AddRange(intSeg.AppliedPolicies.Where(p => (pId != null && pId != p.Policy.Code) && p.Status == EligibilityStatus.Eligible)
                        .Select(p => new IntermittentTimeRequestDetail()
                        {
                            PolicyCode = p.Policy.Code,
                            Approved = 0,
                            Denied = 0,
                            Pending = 0,
                            DenialReasonCode = _row.MinutesDenied.HasValue && _row.MinutesDenied > 0 ? AdjudicationDenialReason.Other : null,
                            DeniedReasonOther = _row.MinutesDenied.HasValue && _row.MinutesDenied > 0 ? "Denied through Case Migration" : null
                        }));
                    foreach (var dt in detail)
                    {
                        var deet = t.Detail.FirstOrDefault(d => d.PolicyCode == dt.PolicyCode);
                        if (deet == null)
                            t.Detail.Add(dt);
                        else
                        {
                            deet.Approved = dt.Approved;
                            deet.Denied = dt.Denied;
                            deet.DenialReasonCode = dt.DenialReasonCode;
                            deet.DeniedReasonOther = dt.DeniedReasonOther;
                            deet.Pending = dt.Pending;
                        }
                    }

                    myCase = svc.CreateOrModifyTimeOffRequest(myCase, times);
                    _ep.reCalcOpenCases = true;
                }
            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                Log.Error("Error processing Intermittent row", aggEx);
                _row.AddError(string.Join("; ", aggEx.Messages));
            }
            catch (AbsenceSoftException abEx)
            {
                Log.Error("Error processing Intermittent row", abEx);
                _row.AddError(abEx.Message);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Intermittent row", ex);
                _row.AddError("Error processing Intermittent row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Insert job
        /// </summary>
        /// <param name="jobExists"></param>
        private void InsertJob(bool jobExists)
        {
            try
            {
                // Upsert this job
                _jobService.UpsertJob(_bulkJob, _row.JobCode, _row.JobName);

                // Upsert the employee job
                _jobService.UpsertEmployeeJob(_bulkEmployeeJob, _row.EmployeeNumber, _row.JobCode, _row.JobName, _row.JobTitle, _row.JobStartDate ?? DateTime.UtcNow.ToMidnight(), _row.JobEndDate, _row.JobLocation, _row.JobPosition, _row.JobStatus, jobExists);
                _gp.bulkJobOps++;
            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                Log.Error("Error processing Job row", aggEx);
                _row.AddError(string.Join("; ", aggEx.Messages));
            }
            catch (AbsenceSoftException abEx)
            {
                Log.Error("Error processing Job row", abEx);
                _row.AddError(abEx.Message);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Job row", ex);
                _row.AddError("Error processing Job row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Insert employee organization
        /// </summary>
        private void InsertEmployeeOrganization()
        {
            try
            {
                _elService.UpdateEmployeeOrganization(_emp, _row, _bulkOrganization, _bulkEmployeeOrganization, ref _gp.bulkOrgOps, ref _gp.bulkEmpOrgOps, ref _ep.updateCasesOfficeLocation);
            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                Log.Error("Error processing Employee Organization row", aggEx);
                _row.AddError(string.Join("; ", aggEx.Messages));
            }
            catch (AbsenceSoftException abEx)
            {
                Log.Error("Error processing Employee Organization row", abEx);
                _row.AddError(abEx.Message);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Employee Organization row", ex);
                _row.AddError("Error processing Employee Organization row, {0}", ex.Message);
            }
        }

        /// <summary>
        /// Insert custom fields
        /// </summary>
        /// <returns></returns>
        private bool InsertCustomFields()
        {
            // Hard work has already been done by .Parse
            // Just set that we need to update the EE and continue on to the next row
            if (string.IsNullOrWhiteSpace(_row.CaseNumber) && _row.Target == EntityTarget.Case)
            {
                var allCasesForEmployee = _cases.AsQueryable().Where(c => c.Employee.EmployeeNumber == _row.EmployeeNumber && c.Status == CaseStatus.Open);
                foreach (var myCase in allCasesForEmployee)
                {
                    _elService.UpdateCaseCustomFields(myCase, _row);
                    _ep.updateCases.Add(myCase.Id);
                }
            }
            else if (!string.IsNullOrWhiteSpace(_row.CaseNumber))
            {
                var myCase = _cases.FirstOrDefault(c => c.CaseNumber == _row.CaseNumber && c.Employee.EmployeeNumber == _row.EmployeeNumber);
                if (myCase == null && _row.Target == EntityTarget.Case)
                {
                    //check for closed case
                    myCase =  Case.AsQueryable().Where(c => c.CustomerId == _args.UploadedDoc.CustomerId
                               && c.EmployerId == _args.UploadedDoc.EmployerId
                               && c.Employee.Id == _emp.Id 
                               && c.Status == CaseStatus.Closed
                               && c.CaseNumber == _row.CaseNumber).FirstOrDefault();

                    if (myCase == null)
                    {
                        _row.AddError("A case with number '{0}' was not found for the employee with number {1}", _row.CaseNumber, _row.EmployeeNumber);
                        return false;
                    }
                    _cases.Add(myCase);
                }
                if (myCase == null && _row.Target == EntityTarget.Employee)
                {
                    _row.AddError("A case with number '{0}' was not found for the employee with number {1}", _row.CaseNumber, _row.EmployeeNumber);
                    return false;
                }
                _elService.UpdateCaseCustomFields(myCase, _row);
                _ep.updateCases.Add(myCase.Id);
            }
            
            _ep.updateEE = true;
            return false;
        }

        /// <summary>
        /// Insert eligibility
        /// </summary> 
        private bool InsertEligibility()
        {
            try
            {
                if (_ep.isNew)
                    _ep.updateEE = true;

                // Setup
                _emp.Info = _emp.Info ?? new EmployeeInfo();
                _emp.Info.Address = _emp.Info.Address ?? new Address();

                // Set Basic Properties (These are the easy ones)
                _emp.ModifiedById = _userId;
                _emp.SetModifiedDate(DateTime.UtcNow);
                _emp.LastName = _row.LastName;
                _emp.FirstName = _row.FirstName;
                _emp.MiddleName = _row.MiddleName;
                _emp.JobTitle = _row.JobTitle;
                _emp.WorkState = _row.WorkState;
                _emp.WorkCountry = _row.WorkCountry ?? "US";
                _emp.WorkCounty = _row.WorkCounty;
                _emp.ResidenceCounty = _row.ResidenceCounty;
                _emp.WorkCity = _row.WorkCity;
                _emp.Info.HomePhone = _row.PhoneHome;
                _emp.Info.WorkPhone = _row.PhoneWork;
                _emp.Info.CellPhone = _row.PhoneMobile;
                _emp.Info.AltPhone = _row.PhoneAlt;
                _emp.Info.Email = StringUtils.LowercaseFirstNotNullString(_row.Email);
                _emp.Info.AltEmail = StringUtils.LowercaseFirstNotNullString(_row.EmailAlt);
                _emp.Info.Address.Address1 = _row.Address;
                _emp.Info.Address.Address2 = _row.Address2;
                _emp.Info.Address.City = _row.City;
                _emp.Info.Address.State = _row.State;
                _emp.Info.Address.PostalCode = _row.PostalCode;
                _emp.Info.Address.Country = _row.Country;
                _emp.EmployeeClassCode = _row.EmploymentType;
                _emp.EmployeeClassName = _row.EmploymentTypeName;
                _emp.DoB = _row.DateOfBirth;
                _emp.Gender = (Gender?)_row.Gender;
                _emp.IsExempt = _row.ExemptionStatus == 'E';
                _emp.Meets50In75MileRule = _row.Meets50In75 ?? false;
                _emp.IsKeyEmployee = _row.KeyEmployee ?? false;
                _emp.MilitaryStatus = (MilitaryStatus)(_row.MilitaryStatus ?? 0);
                _emp.TerminationDate = _row.TerminationDate;
                _emp.Salary = (double?)_row.PayRate;
                _emp.PayType = (PayType?)_row.PayType;
                _emp.HireDate = _row.HireDate;
                _emp.RehireDate = _row.RehireDate;
                _emp.ServiceDate = _row.AdjustedServiceDate;
                _emp.Department = _row.Department;
                _emp.CostCenterCode = _row.CostCenterCode;
                _emp.IsFlightCrew = _row.AirlineFlightCrew;
                if (!string.IsNullOrWhiteSpace(_row.SSN) && !_elService.SSNExists(_row.SSN, _emp.Id))
                {
                    _emp.Ssn = new CryptoString(_row.SSN).Hash().Encrypt();
                }
                if (_row.JobClassification.HasValue)
                    _emp.JobActivity = (JobClassification)_row.JobClassification.Value;
                if (!string.IsNullOrWhiteSpace(_row.PayScheduleName) && _args.PaySchedules.ContainsKey(_row.PayScheduleName))
                    _emp.PayScheduleId = _args.PaySchedules[_row.PayScheduleName];
                _emp.StartDayOfWeek = _row.StartDayOfWeek;

                // Create TODO items for any open cases if this employee is being marked as terminated.
                if (!_ep.isNew && _emp.Status != Data.Enums.EmploymentStatus.Terminated && _row.EmploymentStatus == EmploymentStatusValue.Terminated)
                {
                    _emp.WfOnEmployeeTerminated();

                    // Only do this for cases that are NOT cancelled or closed (like open, requested, inquiry, etc.)
                    foreach (var c in _cases.Where(c => c.Status != CaseStatus.Closed && c.Status != CaseStatus.Cancelled))
                    {
                        _bulkToDo.Insert(new ToDoItem()
                        {
                            Title = "Employee Terminated; Review Open Case",
                            CreatedById = _userId,
                            ModifiedById = _userId,
                            CustomerId = _emp.CustomerId,
                            EmployerId = _emp.EmployerId,
                            AssignedToId = c.AssignedToId,
                            CaseId = c.Id,
                            EmployeeId = _emp.Id,
                            EmployeeNumber = _emp.EmployeeNumber,
                            EmployeeName = _emp.FullName,
                            CaseNumber = c.CaseNumber,
                            DueDate = DateTime.UtcNow.AddDays(1),
                            ItemType = ToDoItemType.CaseReview,
                            Optional = false,
                            Priority = ToDoItemPriority.High,
                            Status = ToDoItemStatus.Pending,
                            Weight = 0,
                            AssignedToName = c.AssignedToName,
                            EmployerName = c.EmployerName
                        }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                        _gp.bulkToDoOps++;
                    }
                }
                _emp.Status = (EmploymentStatus)(_row.EmploymentStatus ?? EmploymentStatusValue.Active);

                if (_row.HoursWorkedIn12Months.HasValue)
                {
                    _emp.PriorHours = _emp.PriorHours ?? new List<PriorHours>();
                    _emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)_row.HoursWorkedIn12Months.Value, AsOf = (_row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                }

                if (_row.AverageMinutesWorkedPerWeek.HasValue)
                {
                    _emp.MinutesWorkedPerWeek = _emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                    _emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = _row.AverageMinutesWorkedPerWeek.Value, AsOf = (_row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                }

                Schedule workSched = new Schedule() { ScheduleType = _row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                workSched.StartDate = (_row.ScheduleEffectiveDate ?? (_ep.isNew && _emp.HireDate.HasValue ? _emp.HireDate.Value : DateTime.UtcNow)).ToMidnight();

                if(_row.FtePercentage.HasValue)
                {
                    workSched.FteMinutesPerWeek = (int)Math.Round(((decimal)(_employer.FTWeeklyWorkHours ?? 0) * (_row.FtePercentage.Value / 100)) * 60);
                    workSched.FteTimePercentage = _row.FtePercentage.Value;
                    workSched.ScheduleType = ScheduleType.FteVariable;
                    workSched.FteWeeklyDuration = FteWeeklyDuration.FtePercentage;
                }
                else if (_row.AverageMinutesWorkedPerWeek.HasValue && workSched.ScheduleType == ScheduleType.Variable)
                {
                    workSched.FteMinutesPerWeek = _row.AverageMinutesWorkedPerWeek.Value;
                    workSched.ScheduleType = ScheduleType.FteVariable;
                    workSched.FteWeeklyDuration = FteWeeklyDuration.FteTimePerWeek;
                }
                else if (!_row.WorkTimeSun.HasValue &&
                    !_row.WorkTimeMon.HasValue &&
                    !_row.WorkTimeTue.HasValue &&
                    !_row.WorkTimeWed.HasValue &&
                    !_row.WorkTimeThu.HasValue &&
                    !_row.WorkTimeFri.HasValue &&
                    !_row.WorkTimeSat.HasValue)
                {
                    if (!_row.MinutesPerWeek.HasValue)
                        workSched = null;
                    else
                    {
                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                        int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)_row.MinutesPerWeek.Value / 5m));
                        int leftOverMinute = _row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                        for (var d = 0; d < 7; d++)
                        {
                            if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                            else
                                workSched.Times.Add(new Time() { SampleDate = sample });
                            sample = sample.AddDays(1);
                        }
                        // Add our leftover minute to the first date, clugey, but it's whatever.
                        workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                    }
                }
                else
                {
                    DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                    for (var d = 0; d < 7; d++)
                    {
                        int? workTime = null;
                        switch (sample.DayOfWeek)
                        {
                            case DayOfWeek.Sunday: workTime = _row.WorkTimeSun; break;
                            case DayOfWeek.Monday: workTime = _row.WorkTimeMon; break;
                            case DayOfWeek.Tuesday: workTime = _row.WorkTimeTue; break;
                            case DayOfWeek.Wednesday: workTime = _row.WorkTimeWed; break;
                            case DayOfWeek.Thursday: workTime = _row.WorkTimeThu; break;
                            case DayOfWeek.Friday: workTime = _row.WorkTimeFri; break;
                            case DayOfWeek.Saturday: workTime = _row.WorkTimeSat; break;
                        }
                        workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                        sample = sample.AddDays(1);
                    }
                }

                if (!_ep.isNew && workSched != null)
                {
                    Schedule dup = _emp.WorkSchedules == null ? null : _emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                    
                    if (dup != null && dup.ScheduleType == ScheduleType.FteVariable && workSched.ScheduleType == ScheduleType.FteVariable)
                    {
                        if (dup.FteMinutesPerWeek == workSched.FteMinutesPerWeek && workSched.StartDate == dup.StartDate)
                        {
                            workSched = null;
                        }
                    }
                    else if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                    {
                        // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                        bool isMatch = true;
                        foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                            if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                            {
                                isMatch = false;
                                break;
                            }
                        if (dup.EndDate == null && workSched.StartDate != dup.StartDate)
                        {
                            isMatch = false;
                        }
                        // See if the schedules still look the same
                        if (isMatch)
                            workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                    }
                }

                if (workSched != null)
                {
                    _ep.reCalcOpenCases = true;
                    workSched.Metadata.SetRawValue("SystemEntered", true);
                    new EmployeeService().Using(s => s.SetWorkSchedule(_emp, workSched, null));
                }

                if (!string.IsNullOrEmpty(_row.JobLocation))
                {
                    _elService.UpdateOrganization(_emp, _row.JobLocation, _bulkOrganization, _bulkEmployeeOrganization, ref _gp.bulkOrgOps, ref _gp.bulkEmpOrgOps, ref _ep.updateCasesOfficeLocation);
                }

                _ep.updateEE = true;
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Eligibility row", ex);
                _row.AddError("Error processing Eligibility row, {0}", ex.Message);
                _ep.updateEE = false;
            }

            return true;
        }

        /// <summary>
        /// Update Todo
        /// </summary>
        private void UpdateToDoForCaseSegments()
        {
            
            if (!_ep.failAll && _cases.Any(c => c.Status != CaseStatus.Closed))
            {
                foreach (var c in _cases.Where(c => c.Status != CaseStatus.Closed))
                {
                    if (_ep.reCalcOpenCases)
                    {
                        if (c.Segments != null && c.Segments.Any())
                        {
                            var oldExhuastDate = c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.FirstExhaustionDate.HasValue).Any() ?
                                c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.FirstExhaustionDate.HasValue).Min(p => p.FirstExhaustionDate) : null;

                            var handlers = new CaseChangeHandlers(CasePropertyChanged.MinimumLeaveExhaustionDate, (changedCase, o) =>
                            {
                                if (changedCase.GetMinLeaveExhaustionDate().HasValue)
                                {
                                    var now = DateTime.UtcNow;
                                    _bulkToDo.Insert(new Tasks.ToDoService().MakeLeaveExhaustedTodo(
                                        @case: changedCase,
                                        modifiedById: _userId,
                                        createdById: _userId,
                                        modified: now,
                                        created: now
                                    ));
                                    _gp.bulkToDoOps++;
                                }
                            });
                            new CaseService().Using(s => s.RunCalcs(c, handlers));

                            _ep.updateCases.Add(c.Id);
                        }
                    }
                    if (_ep.updateCasesOfficeLocation && _emp != null && _emp.Info != null && !string.IsNullOrWhiteSpace(_emp.Info.OfficeLocation))
                    {
                        c.CurrentOfficeLocation = _emp.Info.OfficeLocation;
                        _ep.updateCases.Add(c.Id);
                    }
                }
            }
        }


        /// <summary>
        /// Update case
        /// </summary>
        private void UpdateCase()
        {
            if (!_ep.failAll && _ep.updateCases.Any())
            {
                foreach (var c in _cases.Where(p => _ep.updateCases.Any(u => u == p.Id)))
                {
                    c.ModifiedById = _userId;
                    c.SetModifiedDate(DateTime.UtcNow);
                    _bulkCase.Find(Case.Query.EQ(x => x.Id, c.Id)).ReplaceOne(c);
                    _gp.bulkCaseOps++;
                }
            }
        }

        /// <summary>
        /// Update employee modified date
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="rows"></param>
        /// <param name="bulkEmployee"></param>
        private void UpdateEmployeeModifiedDate()
        {
            //If reCalcOpenCases is true and while executing runcalcs any issue occurs
            //it will throw an exception , and we don't want to update an employee record if runcalcs fails
            if (!_ep.failAll && _ep.updateEE)
            {
                _emp.SetModifiedDate(DateTime.UtcNow);
                _emp.ModifiedById = _userId;
                _emp.CustomFields = _elService.UpdateEmployeeCustomFields(_emp, _rows.ToList());


                if (_ep.isNew)
                    _bulkEmployee.Insert(_emp);
                else
                    _bulkEmployee.Find(Employee.Query.EQ(e => e.Id, _emp.Id)).ReplaceOne(_emp);

                _gp.bulkEmployeeOps++;
            }
        }

        /// <summary>
        /// Cleanup employee contacts
        /// </summary>
        private void CleanupEmployeeContact()
        {
            if (!_ep.failAll && !_ep.isNew && _rows.Any(x => x.ClearOtherContacts))
            {
                var codes = _gp.contactTypeCodes.Where(c => !string.IsNullOrWhiteSpace(c)).Distinct();

                // HACK: This is hard-coded for Amazon, DO NOT remove this and DO NOT copy this logic/approach anywhere else in the code-base
                if (_emp.EmployerId == "546e52cda32aa00f78086269")
                    codes = codes.Where(c => !"SUPERVISOR".Equals(c, StringComparison.InvariantCultureIgnoreCase) && !"WC".Equals(c, StringComparison.InvariantCultureIgnoreCase)).ToList();
                _bulkEmployeeContactCleanup.Find(Query.And(
                        EmployeeContact.Query.EQ(e => e.CustomerId, _emp.CustomerId),
                        EmployeeContact.Query.EQ(e => e.EmployerId, _emp.EmployerId),
                        EmployeeContact.Query.EQ(e => e.EmployeeId, _emp.Id),
                        EmployeeContact.Query.In(e => e.ContactTypeCode, codes),
                        EmployeeContact.Query.NE(e => e.IsDeleted, true)
                )).Update(Update
                    .Set("mby", _userId)
                    .CurrentDate("mdt")
                    .Set("EL", _args.UploadedDoc.FileName)
                    .Set("IsDeleted", true)
                    .Unset("Source"));
                _gp.bulkEmployeeContactCleanupOps++;
            }
        }

        #endregion
 

        /// <summary>
        /// Process EL Records in Batch
        /// </summary>
        /// <param name="args"></param>
        public ElBatchProcessingOutput ProcessRecordsUsingBatch(BatchProcessingArguments args)
        {
            this._elpOutput = new ElBatchProcessingOutput();
            this._args = args;

            #region  Step 1: Check there are records

            if (args.UploadedDoc != null && args.UploadedDoc.Status != ProcessingStatus.Canceled && args.Items != null && args.Items.Count > 0)
            {
                Log.Info($"Processing records/rows for {args.UploadedDoc.FileName}, eligibility upload id {args.UploadedDoc.Id}");
            }

            #endregion

            #region  Step 2: Close if cancelled
            if (CheckIfCancelled(args.UploadedDoc.Id))
            {
                _elpOutput.IsCancelled = true;
                return _elpOutput;
            }

            #endregion

            //set the global variable
            _userId = args.UploadedDoc.CreatedById ?? User.DefaultUserId;
            _jobService = new JobService(args.UploadedDoc.CustomerId, args.UploadedDoc.EmployerId, args.UploadedDoc.CreatedBy);

            //Step 3 : Process now
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ProcessRecords"))
            {
                // Now we need to group our rows by Employee Number and start going to work on each.
                // This WILL spike CPU utilization, and should really be run on dedicated machines, so yeah, fair warning
                foreach (var rGroup in args.Items.Where(u => u.RowParts.Length > 1).GroupBy(u => Normalize(u.RowParts[1])).ToList().Batch(3000).ToList())
                {
                    #region Foreach Batch

                    #region Regenerate Local variables to be used
                    
                    _bulkEmployee = Employee.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkEmployeeContact = EmployeeContact.Repository.Collection.InitializeOrderedBulkOperation();
                    _bulkCase = Case.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkVariableTime = VariableScheduleTime.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkToDo = ToDoItem.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkEmployeeContactCleanup = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkOrganization = Organization.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkEmployeeOrganization = EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkJob = Job.Repository.Collection.InitializeUnorderedBulkOperation();
                    _bulkEmployeeJob = EmployeeJob.Repository.Collection.InitializeUnorderedBulkOperation();

                    _gp = new EligibilityRowGroupProcessingParams();

                    #endregion

                    foreach (var r in rGroup)
                    {
                        #region Foreach Employee Number

                        this._rowGroup = r;

                        //get eligibility rows
                        this._rows = GetEligibilityRowListFromRowGroup();

                        //regenerate processing param object
                        this._ep = new EligibilityRowProcessingParams();
                        
                        //Get Employee
                        this._emp = GetEmployee();
                        

                        //check if deleted
                        if (_emp != null && _emp.IsDeleted)
                        {
                            _emp.IsDeleted = false;
                            _ep.updateEE = true;
                        }
                        
                        //get cases for the employee
                        this._cases = GetCaseListForEmployee();

                        if (!_ep.failAll)
                        {
                            //Loop through the eligibility rows
                            foreach (var row in _rows)
                            {
                                this._row = row;
                                _rows = _rows.OrderBy(t => t.EmployeeNumber).ThenBy(t => t.JobStartDate).ToList();
                                                                
                                //insert schedule
                                if (_row.RecType == EligibilityRow.RecordType.TimeScheduled)
                                {
                                    InsertScheduledTime();
                                    continue;
                                }

                                //get last row key
                                var lastRowKey = GetLastRowKey();

                                if (!string.IsNullOrWhiteSpace(lastRowKey) && row.Hash == _emp.Metadata.GetRawValue<string>(lastRowKey))
                                    continue;

                                // Set the last row key now
                                if (!string.IsNullOrWhiteSpace(lastRowKey))
                                {
                                    _emp.Metadata.SetRawValue(lastRowKey, row.Hash);
                                    _ep.updateEE = true;
                                }

                                #region Supervisor
                                if (row.RecType == EligibilityRow.RecordType.Supervisor || (row.RecType == EligibilityRow.RecordType.Eligibility &&
                                    (!string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerLastName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerPhone) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerEmail))) ||
                                        (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber)))
                                {
                                    
                                    if (!string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerLastName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerPhone) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerEmail))
                                    {
                                        //insert supervisor
                                        var toContinue = InsertSupervisor();
                                        if (toContinue)
                                            continue;
                                    }

                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber))
                                        {
                                            UpdateBasicRecord();
                                        }
                                        continue;
                                    }
                                }

                                #endregion Supervisor
                                
                                #region HR
                                if (row.RecType == EligibilityRow.RecordType.HR || (row.RecType == EligibilityRow.RecordType.Eligibility &&
                                    (!string.IsNullOrWhiteSpace(row.HREmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.HRLastName) ||
                                        !string.IsNullOrWhiteSpace(row.HRFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.HRPhone) ||
                                        !string.IsNullOrWhiteSpace(row.HREmail))) ||
                                        (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.HREmployeeNumber)))
                                {
                                    
                                    if (!string.IsNullOrWhiteSpace(row.HREmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.HRLastName) ||
                                        !string.IsNullOrWhiteSpace(row.HRFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.HRPhone) ||
                                        !string.IsNullOrWhiteSpace(row.HREmail))
                                    {
                                        var toContinue = InsertHr();
                                        if (toContinue)
                                            continue;
                                    }
                                    
                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.HREmployeeNumber))
                                        {
                                            UpdateBasicRecord();
                                        }
                                        continue;
                                    }
                                }

                                #endregion HR

                                #region Spouse
                                if (row.RecType == EligibilityRow.RecordType.Spouse || (row.RecType == EligibilityRow.RecordType.Eligibility && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber))
                                        || (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber)))
                                {
                                    

                                    var toContinue = InsertSpouse();
                                    if (toContinue)
                                        continue;

                                    
                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber))
                                        {
                                            UpdateBasicRecord();
                                        }
                                        continue;
                                    }
                                }

                                #endregion Spouse

                                #region Contact
                                if (row.RecType == EligibilityRow.RecordType.Contact)
                                {

                                    if (!args.ContactTypes.ContainsKey(row.ContactTypeCode) && !IsEmployerContactType(row.ContactTypeCode))
                                    {
                                        row.AddError("The contact type of '{0}' is not valid or is not configured for this customer/employer.", row.ContactTypeCode);
                                    }
                                    else
                                    {
                                        //insert contact
                                        InsertContact();
                                    }                                    
                                    
                                    continue;
                                }

                                #endregion Contact

                                #region Custom
                                if (row.RecType == EligibilityRow.RecordType.Custom)
                                {
                                    if (row.CustomFields == null || row.CustomFields.Length == 0)
                                    {
                                        row.AddError("This row contains custom fields which are not configured for the employer.");
                                    }

                                    InsertCustom();
                                    continue;
                                }
                                #endregion Custom

                                #region Schedule
                                if (row.RecType == EligibilityRow.RecordType.Schedule)
                                {
                                    InsertSchedule();
                                    continue;
                                }

                                #endregion

                                #region Basic
                                if (row.RecType == EligibilityRow.RecordType.Basic)
                                {
                                    UpdateBasicRecord();
                                    continue;
                                }

                                #endregion

                                #region Intermittent
                                if (row.RecType == EligibilityRow.RecordType.Intermittent)
                                {
                                    InsertIntermittent();
                                    continue;
                                }

                                #endregion

                                #region Job
                                if (row.RecType == EligibilityRow.RecordType.Job)
                                {
                                    //check whether job exists
                                    bool alreadyExists = CheckJobExists();

                                    InsertJob(alreadyExists);
                                    continue;
                                }

                                #endregion Job

                                #region Employee Organization
                                if (row.RecType == EligibilityRow.RecordType.Organization)
                                {   
                                    InsertEmployeeOrganization();
                                    continue;   
                                }

                                #endregion Employee Organization

                                #region Custom fields
                                if (row.RecType == EligibilityRow.RecordType.CustomField)
                                {
                                    var isBreak = InsertCustomFields();
                                    if (isBreak)
                                        break;

                                    continue;
                                }

                                #endregion

                                #region Eligibility
                                
                                var isContinue = InsertEligibility();
                                if (isContinue)
                                    continue;

                                #endregion Eligibility


                            } // foreach Row
                        } //if not failed


                        //For each row group
                        try
                        {
                            //update to do
                            UpdateToDoForCaseSegments();

                            //Update case modified date
                            UpdateCase();

                            //Update employee modified date
                            UpdateEmployeeModifiedDate();

                            //clean up contacts
                            CleanupEmployeeContact();
                            
                        }
                        catch (MongoBulkWriteException bulkEx)
                        {
                            var errorKey = Guid.NewGuid().ToString("D");
                            Log.Error(string.Format("{1}: Error processing employee '{0}'", _emp.EmployeeNumber, errorKey), bulkEx);
                            foreach (var row in _rows)
                                foreach (var err in bulkEx.WriteErrors)
                                    row.AddError("{1}: Service exception processing employee: {0}", err.Message, errorKey);
                        }
                        catch (Exception ex)
                        {
                            var errorKey = Guid.NewGuid().ToString("D");
                            Log.Error(string.Format("{1}: Error processing employee '{0}'", _emp.EmployeeNumber, errorKey), ex);
                            foreach (var row in _rows)
                                row.AddError("{1}: Service exception processing employee: {0}",ex.Message, errorKey);
                        }

                        #endregion

                        _gp.additionalErrors += _rows.LongCount(x => x.IsError);
                        _gp.adjustedCount += _rows.LongCount();

                        _rows.Clear();
                        _rows = null;

                    } //foreach: Employee Number

                    //if operation cancelled return output
                    if (CheckIfCancelled(args.UploadedDoc.Id))
                    {
                        _elpOutput.IsCancelled = true;
                        return _elpOutput;
                    }

                    #region Execute
                    try
                    {
                        if (_gp.bulkEmployeeOps > 0)
                            _bulkEmployee.Execute();
                        if (_gp.bulkEmployeeContactCleanupOps > 0)
                            _bulkEmployeeContactCleanup.Execute();
                        if (_gp.bulkEmployeeContactOps > 0)
                            _bulkEmployeeContact.Execute();
                        if (_gp.bulkVariableTimeOps > 0)
                            _bulkVariableTime.Execute();
                        if (_gp.bulkCaseOps > 0)
                            _bulkCase.Execute();
                        if (_gp.bulkToDoOps > 0)
                            _bulkToDo.Execute();
                        if (_gp.bulkOrgOps > 0)
                            _bulkOrganization.Execute();
                        if (_gp.bulkEmpOrgOps > 0)
                            _bulkEmployeeOrganization.Execute();
                        if (_gp.bulkJobOps > 0)
                        {
                            _bulkJob.Execute();
                            _bulkEmployeeJob.Execute();
                        }
                    }
                    catch (MongoBulkWriteException bulkEx)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error processing employee batch", errorKey), bulkEx);
                        foreach (var row in rGroup.SelectMany(g => g))
                            foreach (var err in bulkEx.WriteErrors)
                                row.Error = string.Format("{0}{1}{2}: Service exception processing employee: {3}",
                                    row.Error,
                                    string.IsNullOrWhiteSpace(row.Error) ? null : Environment.NewLine,
                                    errorKey, err.Message);
                    }
                    catch (Exception ex)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error processing employee batch", errorKey), ex);
                        foreach (var row in rGroup.SelectMany(g => g))
                            row.Error = string.Format("{0}{1}{2}: Service exception processing employee",
                                row.Error,
                                    string.IsNullOrWhiteSpace(row.Error) ? null : Environment.NewLine,
                                    errorKey);
                    }

                    #endregion

                    #region Handle errors
                    try
                    {
                        var bulkResults = EligibilityUploadResult.Repository.Collection.InitializeUnorderedBulkOperation();
                        bool hasResults = false;
                        rGroup.SelectMany(g => g.Where(r => string.IsNullOrWhiteSpace(r.Id))).ForEach(e =>
                        {
                            if (!e.IsError)
                                e.RowSource = null;
                            hasResults = true;
                            bulkResults.Insert(e);
                        });
                        if (hasResults)
                            bulkResults.Execute();
                    }
                    catch (MongoBulkWriteException bulkEx)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error recording row results for employee batch", errorKey), bulkEx);
                        foreach (var err in bulkEx.WriteErrors)
                            Log.Error(string.Format("{1}: Error recording row results for employee batch: {0}", err.Message, errorKey));
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error recording row results for employee batch", ex);
                    }

                    #endregion

                    #region Finalization
                    try
                    {
                        // Update our upload with the error count
                        args.UploadedDoc.Errors += _gp.additionalErrors;
                        args.UploadedDoc.Processed += _gp.adjustedCount;
                        args.UploadedDoc.Save();

                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("Error setting final counts for upload id '{0}' for Employee batch", args.UploadedDoc.Id), ex);
                    }

                    #endregion

                    #endregion Foreach Batch
                } //foreach: Employee Batch


                if (CheckIfCancelled(args.UploadedDoc.Id))
                {
                    _elpOutput.IsCancelled = true;
                    return _elpOutput;
                }

                //return output
                return _elpOutput;
            }
        }
      
        /// <summary>
        /// Dispose
        /// </summary>
        public override void Dispose()
        {
            _args = null;
            _bulkCase = null;
            _bulkEmployee = null;
            _bulkEmployeeContact = null;
            _bulkEmployeeContactCleanup = null;
            _bulkEmployeeJob = null;
            _bulkEmployeeOrganization = null;
            _bulkJob = null;
            _bulkOrganization = null;
            _bulkToDo = null;
            _bulkVariableTime = null;
            _cases = null;
            _countries = null;
            _elpOutput = null;
            _elService = null;
            _emp = null;
            _ep = null;
            _gp = null;
            _jobService = null;
            _row = null;
            _rowGroup = null;
            _rows = null;
            _userId = null;
            _employer = null;

            base.Dispose();
        }
}
}
