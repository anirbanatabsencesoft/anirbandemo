﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Customers;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Processing.EL
{
    internal sealed class EligibilityRow : BaseRow
    {
        #region Const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;

        private const int DateScheduledOrdinal = 2;
        private const int TimeScheduledOrdinal = 3;
        private const int M_EmployerReferenceCodeOrdinal = 4;

        private const int SpouseSpouseEmployeeNumberOrdinal = 2;
        private const int SpouseEmployerReferenceCodeOrdinal = 3;

        private const int ContactEmployeeNumberOrdinal = 2;
        private const int ContactEmailOrdinal = 3;
        private const int ContactFirstNameOrdinal = 4;
        private const int ContactLastNameOrdinal = 5;
        private const int ContactPhoneOrdinal = 6;
        private const int ContactEmployerReferenceCodeOrdinal = 7;

        private const int LastNameOrdinal = 2;
        private const int FirstNameOrdinal = 3;
        private const int MiddleNameOrdinal = 4;
        private const int JobTitleOrdinal = 5;
        private const int JobLocationOrdinal = 6;
        private const int WorkStateOrdinal = 7;
        private const int WorkCountryOrdinal = 8;
        private const int PhoneHomeOrdinal = 9;
        private const int PhoneWorkOrdinal = 10;
        private const int PhoneMobileOrdinal = 11;
        private const int PhoneAltOrdinal = 12;
        private const int EmailOrdinal = 13;
        private const int EmailAltOrdinal = 14;
        private const int AddressOrdinal = 15;
        private const int Address2Ordinal = 16;
        private const int CityOrdinal = 17;
        private const int StateOrdinal = 18;
        private const int PostalCodeOrdinal = 19;
        private const int CountryOrdinal = 20;
        private const int EmploymentTypeOrdinal = 21;
        private const int ManagerEmployeeNumberOrdinal = 22;
        private const int ManagerLastNameOrdinal = 23;
        private const int ManagerFirstNameOrdinal = 24;
        private const int ManagerPhoneOrdinal = 25;
        private const int ManagerEmailOrdinal = 26;
        private const int HREmployeeNumberOrdinal = 27;
        private const int HRLastNameOrdinal = 28;
        private const int HRFirstNameOrdinal = 29;
        private const int HRPhoneOrdinal = 30;
        private const int HREmailOrdinal = 31;
        private const int SpouseEmployeeNumberOrdinal = 32;
        private const int DateOfBirthOrdinal = 33;
        private const int GenderOrdinal = 34;
        private const int ExemptionStatusOrdinal = 35;
        private const int Meets50In75Ordinal = 36;
        private const int KeyEmployeeOrdinal = 37;
        private const int MilitaryStatusOrdinal = 38;
        private const int EmploymentStatusOrdinal = 39;
        private const int TerminationDateOrdinal = 40;
        private const int PayRateOrdinal = 41;
        private const int PayTypeOrdinal = 42;
        private const int HireDateOrdinal = 43;
        private const int RehireDateOrdinal = 44;
        private const int AdjustedServiceDateOrdinal = 45;
        private const int MinutesPerWeekOrdinal = 46;
        private const int HoursWorkedIn12MonthsOrdinal = 47;
        private const int WorkTimeSunOrdinal = 48;
        private const int WorkTimeMonOrdinal = 49;
        private const int WorkTimeTueOrdinal = 50;
        private const int WorkTimeWedOrdinal = 51;
        private const int WorkTimeThuOrdinal = 52;
        private const int WorkTimeFriOrdinal = 53;
        private const int WorkTimeSatOrdinal = 54;
        private const int VariableScheduleOrdinal = 55;
        private const int JobDescriptionOrdinal = 56;
        private const int JobClassificationOrdinal = 57;
        private const int DepartmentOrdinal = 58;
        private const int CostCenterCodeOrdinal = 59;
        private const int ScheduleEffectiveDateOrdinal = 60;
        private const int EmployerReferenceCodeOrdinal = 61;
        private const int E_SSNOrdinal = 62;
        private const int E_PayScheduleNameOrdinal = 63;
        private const int E_StartDayOfWeekOrdinal = 64;
        private const int E_AverageMinutesWorkedPerWeekOrdinal = 65;
        private const int E_WorkCountyOrdinal = 66;
        private const int E_ResidenceCountyOrdinal = 67;
        private const int E_WorkCityOrdinal = 68;
        private const int E_AirlineFlightCrewOrdinal = 69;
        private const int E_FtePercentageOrdinal = 70;

        private const int W_MinutesPerWeekOrdinal = 2;
        private const int W_HoursWorkedIn12MonthsOrdinal = 3;
        private const int W_WorkTimeSunOrdinal = 4;
        private const int W_WorkTimeMonOrdinal = 5;
        private const int W_WorkTimeTueOrdinal = 6;
        private const int W_WorkTimeWedOrdinal = 7;
        private const int W_WorkTimeThuOrdinal = 8;
        private const int W_WorkTimeFriOrdinal = 9;
        private const int W_WorkTimeSatOrdinal = 10;
        private const int W_VariableScheduleOrdinal = 11;
        private const int W_ScheduleEffectiveDateOrdinal = 12;
        private const int W_EmployerReferenceCodeOrdinal = 13;
        private const int W_NoOverwriteFlagOrdinal = 14;
        private const int W_AverageMinutesWorkedPerWeekOrdinal = 15;
        private const int W_FtePercentageOrdinal = 16;


        private const int BasicLastNameOrdinal = 2;
        private const int BasicFirstNameOrdinal = 3;
        private const int BasicMiddleNameOrdinal = 4;
        private const int BasicWorkStateOrdinal = 5;
        private const int BasicPhoneHomeOrdinal = 6;
        private const int BasicPhoneWorkOrdinal = 7;
        private const int BasicPhoneMobileOrdinal = 8;
        private const int BasicEmailOrdinal = 9;
        private const int BasicAddressOrdinal = 10;
        private const int BasicAddress2Ordinal = 11;
        private const int BasicCityOrdinal = 12;
        private const int BasicStateOrdinal = 13;
        private const int BasicPostalCodeOrdinal = 14;
        private const int BasicManagerEmployeeNumberOrdinal = 15;
        private const int BasicGenderOrdinal = 16;
        private const int BasicAdjustedServiceDateOrdinal = 17;
        private const int BasicMinutesPerWeekOrdinal = 18;
        private const int BasicHoursWorkedIn12MonthsOrdinal = 19;
        private const int BasicWorkTimeSunOrdinal = 20;
        private const int BasicWorkTimeMonOrdinal = 21;
        private const int BasicWorkTimeTueOrdinal = 22;
        private const int BasicWorkTimeWedOrdinal = 23;
        private const int BasicWorkTimeThuOrdinal = 24;
        private const int BasicWorkTimeFriOrdinal = 25;
        private const int BasicWorkTimeSatOrdinal = 26;
        private const int BasicVariableScheduleOrdinal = 27;
        private const int BasicEmployerReferenceCodeOrdinal = 28;
        private const int BasicWorkCountyOrdinal = 29;
        private const int BasicResidenceCountyOrdinal = 30;
        private const int BasicWorkCityOrdinal = 31;
        private const int B_AirlineFlightCrewOrdinal = 32;
        private const int BasicFtePercentageOrdinal = 33;
        private const int BasicAverageMinutesWorkedPerWeekOrdinal = 34;

        private const int OTypeOrdinal = 2;
        private const int ORoleOrdinal = 3;
        private const int OExclusiveOrdinal = 4;
        private const int OPositionOrdinal = 5;
        private const int OEmployeeNumberOrdinal = 6;
        private const int OLastNameOrdinal = 7;
        private const int OFirstNameOrdinal = 8;
        private const int OEmailOrdinal = 9;
        private const int OHomeEmailOrdinal = 10;
        private const int OPhoneOrdinal = 11;
        private const int OHomePhoneOrdinal = 12;
        private const int OMobilePhoneOrdinal = 13;
        private const int OFaxOrdinal = 14;
        private const int OAddressOrdinal = 15;
        private const int OAddress2Ordinal = 16;
        private const int OCityOrdinal = 17;
        private const int OStateOrdinal = 18;
        private const int OPostalCodeOrdinal = 19;
        private const int OMilitaryStatusOrdinal = 20;
        private const int OClearOtherContactsOrdinal = 21;
        private const int OEmployerReferenceCodeOrdinal = 22;

        // Intermittent Time Used
        private const int I_EmployeeLastNameOrdinal = 2;
        private const int I_EmployeeFirstNameOrdinal = 3;
        private const int I_CaseNumberOrdinal = 4;
        private const int I_DateOfAbsenceOrdinal = 5;
        private const int I_MinutesApprovedOrdinal = 6;
        private const int I_MinutesDeniedOrdinal = 7;
        private const int I_MinutesPendingOrdinal = 8;
        private const int I_PolicyCodeOrdinal = 9;
        private const int I_WorkflowOrdinal = 10;
        private const int I_EmployerReferenceCodeOrdinal = 11;
        private const int I_IntermittentTimeOutRequestTypeOrdinal = 12;
        private const int I_StartTimeOfLeaveOrdinal = 13;
        private const int I_EndTimeOfLeaveOrdinal = 14;

        // Job
        private const int J_JobCodeOrdinal = 2;
        private const int J_JobNameOrdinal = 3;
        private const int J_JobTitleOrdinal = 4;
        private const int J_JobStartDateOrdinal = 5;
        private const int J_JobEndDateOrdinal = 6;
        private const int J_JobLocationOrdinal = 7;
        private const int J_EmployerReferenceCodeOrdinal = 8;
        private const int J_JobPositionOrdinal = 9;
        private const int J_JobStatusOrdinal = 10;
        private const int J_JobPositionDefault = 1;

        //Organization
        private const int O_OrgCodeOrdinal = 2;
        private const int O_OrgNameOrdinal = 3;
        private const int O_OrgTypeCodeOrdinal = 4;
        private const int O_OrgStartDateOrdinal = 5;
        private const int O_OrgEndDateOrdinal = 6;
        private const int O_OrgAffiliationOrdinal = 7;
        private const int O_ParentOrgCodeOrdinal = 8;
        private const int O_ParentOrgNameOrdinal = 9;
        private const int O_ParentOrgTypeCodeOrdinal = 10;
        private const int O_EmployerReferenceCodeOrdinal = 11;
        private const int O_SicCodeOrdinal = 12;
        private const int O_NaicsCodeOrdinal = 13;
        private const int O_Address1Ordinal = 14;
        private const int O_Address2Ordinal = 15;
        private const int O_CityOrdinal = 16;
        private const int O_PostalCodeOrdinal = 17;
        private const int O_StateOrdinal = 18;
        private const int O_CountryOrdinal = 19;


        // Custom Field ("F")
        private const int F_FieldCodeOrdinal = 2;
        private const int F_FieldValueOrdinal = 3;
        private const int F_EmployerReferenceCodeOrdinal = 4;
        private const int F_EntityTargetOrdinal = 5;
        private const int F_CaseNumberOrdinal = 6;
        private const int F_UpdateBehaviourOrdinal = 7;
        #endregion Const

        #region .ctor

        public EligibilityRow() : base()
        {
        }
        public EligibilityRow(string[] rowSource, IEnumerable<CustomField> customFields = null)
            : base(rowSource, customFields)
        {
        }
        public EligibilityRow(Employee emp)
            : base()
        {
            RecType = RecordType.Eligibility;
            if (emp == null)
                return;
            CustomerId = emp.CustomerId;
            EmployerId = emp.EmployerId;
            Parse(emp);
        }

        #endregion .ctor

        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public List<EmployeeClass> EmployeeClassTypes { get; set; }
        internal enum RecordType
        {
            Eligibility = 'E',
            TimeScheduled = 'M',
            Spouse = 'S',
            Basic = 'B',
            Supervisor = 'U',
            HR = 'H',
            Custom = 'C',
            Schedule = 'W',
            Contact = 'O',
            Intermittent = 'I',
            Job = 'J',
            Organization = 'G',
            CustomField = 'F'
        }

        #region Fields

        public long RowNumber { get; set; }
        public EligibilityUploadResult RowResult { get; set; }
        public string Hash { get { return Convert.ToBase64String(System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(Regex.Replace(ToString().ToLowerInvariant(), @"[^a-zA-Z0-9,]", "")))); } }

        public RecordType RecType { get; set; }
        public string EmployeeNumber { get; set; }

        public string EmployerReferenceCode { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string JobLocation { get; set; }
        public string WorkState { get; set; }
        public string WorkCountry { get; set; }
        public string WorkCounty { get; set; }
        public string WorkCity { get; set; }
        public string ResidenceCounty { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneAlt { get; set; }
        public string Email { get; set; }
        public string EmailAlt { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string EmploymentType { get; set; }
        public string EmploymentTypeName { get; set; }
        public string ManagerEmployeeNumber { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public string HREmployeeNumber { get; set; }
        public string HRLastName { get; set; }
        public string HRFirstName { get; set; }
        public string HRPhone { get; set; }
        public string HREmail { get; set; }
        public string SpouseEmployeeNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public char? Gender { get; set; }
        public char? ExemptionStatus { get; set; }
        public bool? Meets50In75 { get; set; }
        public bool? KeyEmployee { get; set; }
        public byte? MilitaryStatus { get; set; }
        public char? EmploymentStatus { get; set; }
        public DateTime? TerminationDate { get; set; }
        public decimal? PayRate { get; set; }
        public byte? PayType { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? RehireDate { get; set; }
        public DateTime? AdjustedServiceDate { get; set; }
        public int? MinutesPerWeek { get; set; }
        public decimal? HoursWorkedIn12Months { get; set; }
        public int? AverageMinutesWorkedPerWeek { get; set; }
        public decimal? FtePercentage { get; set; }
        public int? WorkTimeSun { get; set; }
        public int? WorkTimeMon { get; set; }
        public int? WorkTimeTue { get; set; }
        public int? WorkTimeWed { get; set; }
        public int? WorkTimeThu { get; set; }
        public int? WorkTimeFri { get; set; }
        public int? WorkTimeSat { get; set; }
        public bool? VariableSchedule { get; set; }
        public bool? NoOverwriteFlag { get; set; }
        public string JobDescription { get; set; }
        public byte? JobClassification { get; set; }
        public string Department { get; set; }
        public string CostCenterCode { get; set; }
        public DateTime? ScheduleEffectiveDate { get; set; }
        public string SSN { get; set; }
        public string PayScheduleName { get; set; }
        public DayOfWeek? StartDayOfWeek { get; set; }
        public bool? AirlineFlightCrew { get; set; }
        // Time Scheduled Record Detail Only
        public DateTime? DateScheduled { get; set; }
        public int? TimeScheduled { get; set; }

        public CustomField[] CustomFields { get; set; }

        // Contact Record Only
        public string ContactTypeCode { get; set; }
        public string ContactTypeName { get; set; }
        public bool? ContactExclusive { get; set; }
        public int? ContactPosition { get; set; }
        public string ContactEmployeeNumber { get; set; }
        public string PhoneFax { get; set; }
        public bool ClearOtherContacts { get; set; }

        // Intermittent Record Only
        public string CaseNumber { get; set; }
        public DateTime? DateOfAbsence { get; set; }
        public int? MinutesApproved { get; set; }
        public int? MinutesDenied { get; set; }
        public int? MinutesPending { get; set; }
        public string PolicyCode { get; set; }
        public bool? Workflow { get; set; }
        public IntermittentType IntermittentTimeOutRequestType { get; set; }

        public TimeOfDay? StartTimeForLeave { get; set; }

        public TimeOfDay? EndTimeForLeave { get; set; }

        // Job Records Only
        public string JobCode { get; set; }
        public string JobName { get; set; }
        public DateTime? JobStartDate { get; set; }
        public DateTime? JobEndDate { get; set; }
        public int? JobPosition { get; set; }
        public AdjudicationStatus JobStatus { get; set; }

        // Organization Records Only
        public string OrgCode { get; set; }
        public string OrgName { get; set; }
        public string OrgTypeCode { get; set; }
        public DateTime? OrgStartDate { get; set; }
        public DateTime? OrgEndDate { get; set; }
        public byte? OrgAffiliation { get; set; }
        public string ParentOrgCode { get; set; }
        public string ParentOrgName { get; set; }
        public string ParentOrgTypeCode { get; set; }
        public string SicCode { get; set; }
        public string NaicsCode { get; set; }
        public string OrgAddress1 { get; set; }
        public string OrgAddress2 { get; set; }
        public string OrgCity { get; set; }
        public string OrgPostalCode { get; set; }
        public string OrgState { get; set; }
        public string OrgCountry { get; set; }

        // Custom Field Records Only
        public string FieldCode { get; set; }
        public string FieldValue { get; set; }
        public EntityTarget Target { get; set; }
        /// <summary>
        /// 0 = Passive Update (default) and 1 = Forced Overwrite
        /// </summary>
        public CustomFieldUpdateBehavior CustomFieldUpdateBehaviour { get; set; } = CustomFieldUpdateBehavior.Passive;
        #endregion Fields

        #region Parsing

        public bool Parse(Employee emp)
        {
            if (emp == null)
                return false;
            EmployeeNumber = emp.EmployeeNumber;
            LastName = emp.LastName;
            FirstName = emp.FirstName;
            MiddleName = emp.MiddleName;
            JobTitle = emp.JobTitle;
            JobLocation = emp.GetOfficeLocationDisplay();
            WorkState = emp.WorkState;
            WorkCountry = emp.WorkCountry;
            WorkCounty = emp.WorkCounty;
            WorkCity = emp.WorkCity;
            ResidenceCounty = emp.ResidenceCounty;
            PhoneHome = emp.Info.HomePhone;
            PhoneWork = emp.Info.WorkPhone;
            PhoneMobile = emp.Info.CellPhone;
            PhoneAlt = emp.Info.AltPhone;
            Email = emp.Info.Email;
            EmailAlt = emp.Info.AltEmail;
            Department = emp.Department;
            CostCenterCode = emp.CostCenterCode;
            if (emp.Info.Address != null)
            {
                Address = emp.Info.Address.Address1;
                Address2 = emp.Info.Address.Address2;
                City = emp.Info.Address.City;
                State = emp.Info.Address.State;
                PostalCode = emp.Info.Address.PostalCode;
                Country = emp.Info.Address.Country;
            }
            EmploymentType = emp.EmployeeClassCode;
            DateOfBirth = emp.DoB;
            Gender = (char?)emp.Gender;
            ExemptionStatus = emp.IsExempt ? 'E' : 'N';
            Meets50In75 = emp.Meets50In75MileRule;
            KeyEmployee = emp.IsKeyEmployee;
            MilitaryStatus = (byte)emp.MilitaryStatus;
            EmploymentStatus = (char)emp.Status;
            TerminationDate = emp.TerminationDate;
            PayRate = (decimal?)emp.Salary;
            PayType = (byte?)emp.PayType;
            HireDate = emp.HireDate;
            RehireDate = emp.RehireDate;
            AdjustedServiceDate = emp.ServiceDate;
            AirlineFlightCrew = emp.IsFlightCrew;
            if (emp.SpouseEmployee != null)
                SpouseEmployeeNumber = emp.SpouseEmployee.EmployeeNumber;
            List<EmployeeContact> contacts = EmployeeContact.AsQueryable().Where(e => e.EmployeeId == emp.Id).ToList();
            EmployeeContact mgr = contacts.FirstOrDefault(c => c.ContactTypeCode == "SUPERVISOR");
            if (mgr != null)
            {
                ManagerLastName = mgr.Contact.LastName;
                ManagerFirstName = mgr.Contact.FirstName;
                ManagerPhone = mgr.Contact.WorkPhone;
                ManagerEmail = mgr.Contact.Email;
                if (mgr.RelatedEmployee != null)
                    ManagerEmployeeNumber = mgr.RelatedEmployee.EmployeeNumber;
            }
            EmployeeContact hr = contacts.FirstOrDefault(c => c.ContactTypeCode == "HR");
            if (hr != null)
            {
                HRLastName = hr.Contact.LastName;
                HRFirstName = hr.Contact.FirstName;
                HRPhone = hr.Contact.WorkPhone;
                HREmail = hr.Contact.Email;
                if (hr.RelatedEmployee != null)
                    HREmployeeNumber = hr.RelatedEmployee.EmployeeNumber;
            }

            Cases.LeaveOfAbsence loa = new Cases.LeaveOfAbsence() { Employee = emp, Employer = emp.Employer, Customer = emp.Customer, WorkSchedule = emp.WorkSchedules };
            HoursWorkedIn12Months = (decimal)loa.TotalHoursWorkedLast12Months();
            DateTime startDate = DateTime.UtcNow.GetFirstDayOfWeek();
            DateTime endDate = startDate.AddDays(6);
            VariableSchedule = emp.WorkSchedules.Any(s => DateTime.UtcNow.DateInRange(s.StartDate, s.EndDate) && s.ScheduleType == ScheduleType.Variable);
            if (VariableSchedule != true)
            {
                var sched = loa.MaterializeSchedule(startDate, endDate, true);
                MinutesPerWeek = sched.Sum(t => t.TotalMinutes ?? 0);
                WorkTimeSun = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Sunday) ?? new Time()).TotalMinutes;
                WorkTimeMon = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Monday) ?? new Time()).TotalMinutes;
                WorkTimeTue = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Tuesday) ?? new Time()).TotalMinutes;
                WorkTimeWed = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Wednesday) ?? new Time()).TotalMinutes;
                WorkTimeThu = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Thursday) ?? new Time()).TotalMinutes;
                WorkTimeFri = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Friday) ?? new Time()).TotalMinutes;
                WorkTimeSat = (sched.SingleOrDefault(t => t.SampleDate.DayOfWeek == DayOfWeek.Saturday) ?? new Time()).TotalMinutes;
            }

            return true;
        }

        public static bool GetRecordType(string[] row, out string employerRecordType)
        {
            if (row == null || row.Length <= 1)
            {
                employerRecordType = "E";
                return false;
            }
            employerRecordType = row[RecordTypeOrdinal];
            return true;
        }
        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (_parts == null || _parts.Length == 1)
                return AddError("No delimiters or properly formatted information found in this record");

            //
            // Record Type
            var recTypes = Enums<RecordType>.GetCharOptions().Values.Select(c => c.ToString()).ToArray();
            if (Required(RecordTypeOrdinal, "Record Type") && In(RecordTypeOrdinal, "Record Type", recTypes))
                RecType = (RecordType)Convert.ToChar(_parts[RecordTypeOrdinal]);
            else
                return false;

            // =============
            // Shared Fields
            // =============
            //
            // Employee Number
            if (Required(EmployeeNumberOrdinal, "Employee Number"))
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);

            // =========================
            // Time Scheduled Record Fields
            // =========================
            if (RecType == RecordType.TimeScheduled)
            {
                #region TimeScheduled

                //
                // Date Worked
                if (Required(DateScheduledOrdinal, "Date Scheduled"))
                    DateScheduled = ParseDate(DateScheduledOrdinal, "Date Scheduled");
                //
                // Time Worked
                if (Required(TimeScheduledOrdinal, "Time Scheduled"))
                    TimeScheduled = ParseInt(TimeScheduledOrdinal, "Time Scheduled");
                if (TimeScheduled.HasValue && TimeScheduled.Value > 1440)
                    AddError("Time scheduled must be no more than 1,440 minutes (24 hours), however {0:N0} minutes was passed for {1:MM/dd/yyyy}", TimeScheduled, DateScheduled);
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(M_EmployerReferenceCodeOrdinal);

                #endregion
            }
            else if (RecType == RecordType.Eligibility)
            {
                #region Eligibility
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(EmployerReferenceCodeOrdinal);

                //
                // Employee Last Name
                if (Required(LastNameOrdinal, "Employee Last Name"))
                    LastName = Normalize(LastNameOrdinal);
                //
                // Employee First Name
                if (Required(FirstNameOrdinal, "Employee First Name"))
                    FirstName = Normalize(FirstNameOrdinal);
                //
                // Employee Middle Name
                MiddleName = Normalize(MiddleNameOrdinal);
                //
                // Job Title               
                JobTitle = Normalize(JobTitleOrdinal);
                //
                // Job Location
                JobLocation = Normalize(JobLocationOrdinal);
                //
                // Work State
                // Work Country
                if (Required(WorkStateOrdinal, "Work State"))
                {
                    WorkState = Normalize(WorkStateOrdinal).ToUpperInvariant();
                    WorkCountry = (Normalize(WorkCountryOrdinal) ?? "US").ToUpperInvariant();
                    if (WorkCountry == "US")
                        ValidUSState(WorkState, "Work State");
                }
                //
                // Employee Phone Home
                PhoneHome = Normalize(PhoneHomeOrdinal);
                //
                // Employee Phone Work
                PhoneWork = Normalize(PhoneWorkOrdinal);
                //
                // Employee Phone Mobile
                PhoneMobile = Normalize(PhoneMobileOrdinal);
                //
                // Employee Phone Alt
                PhoneAlt = Normalize(PhoneAltOrdinal);
                //
                // Employee Email
                Email = Normalize(EmailOrdinal);
                ValidEmail(Email, "Employee Email");
                //
                // Employee Alt Email
                EmailAlt = Normalize(EmailAltOrdinal);
                ValidEmail(EmailAlt, "Employee Alt Email");
                //
                // Mailing Address
                Address = Normalize(AddressOrdinal);
                //
                // Mailing Address 2
                Address2 = Normalize(Address2Ordinal);
                //
                // Mailing City
                City = Normalize(CityOrdinal);
                //
                // Mailing State
                State = Normalize(StateOrdinal);
                //
                // Mailing Postal Code
                PostalCode = Normalize(PostalCodeOrdinal);
                //
                // Mailing Country
                Country = Normalize(CountryOrdinal);
                if (!string.IsNullOrWhiteSpace(State) && (string.IsNullOrWhiteSpace(Country) || Country == "US"))
                    ValidUSState(State, "Mailing State");
                if (string.IsNullOrWhiteSpace(Country) || Country == "US")
                    if (!string.IsNullOrWhiteSpace(PostalCode) && PostalCode.Length == 4)
                        PostalCode = string.Concat("0", PostalCode);
                //
                // PT/FT Status
                EmploymentType = Normalize(EmploymentTypeOrdinal);
                if (!String.IsNullOrWhiteSpace(EmploymentType) && !ValidateEmployementType(EmploymentType))
                {
                    AddError("The Employement type '{0}' does not exist. It should be matching to Employee Class configuration under Admin", EmploymentType);
                }
                EmploymentType = GetEmployementType(EmploymentType);

                EmploymentTypeName = GetEmploymentTypeName(EmploymentType);
                //
                // Manager Employee Number
                ManagerEmployeeNumber = Normalize(ManagerEmployeeNumberOrdinal);
                //
                // Manager Last Name
                ManagerLastName = Normalize(ManagerLastNameOrdinal);
                //
                // Manager First Name
                ManagerFirstName = Normalize(ManagerFirstNameOrdinal);
                //
                // Manager Phone
                ManagerPhone = Normalize(ManagerPhoneOrdinal);
                //
                // Manager Email
                ManagerEmail = Normalize(ManagerEmailOrdinal);
                ValidEmail(ManagerEmail, "Manager Email");
                //
                // HR Contact Employee Number
                HREmployeeNumber = Normalize(HREmployeeNumberOrdinal);
                //
                // HR Contact Last Name
                HRLastName = Normalize(HRLastNameOrdinal);
                //
                // HR Contact First Name
                HRFirstName = Normalize(HRFirstNameOrdinal);
                //
                // HR Contact Phone
                HRPhone = Normalize(HRPhoneOrdinal);
                //
                // HR Contact Email
                HREmail = Normalize(HREmailOrdinal);
                ValidEmail(HREmail, "HR Contact Email");
                //
                // Spouse Employee Number
                SpouseEmployeeNumber = Normalize(SpouseEmployeeNumberOrdinal);
                //
                // Date of Birth
                if (Required(DateOfBirthOrdinal, "Date of Birth"))
                    DateOfBirth = ParseDate(DateOfBirthOrdinal, "Date of Birth");
                //
                // Gender
                if (Required(GenderOrdinal, "Gender"))
                {
                    Gender = ParseChar(GenderOrdinal, "Date of Birth");
                    if (Gender.HasValue && Gender != 'F' && Gender != 'M' && Gender != 'U')
                        AddError("'{0}' is not a recognized employee gender assignment code, please provide the value 'F' (Female), 'M' (Male) or 'U' (Unknown/Not Collected)", Gender);
                }
                //
                // Exemption Status
                if (Required(ExemptionStatusOrdinal, "Exemption Status"))
                {
                    ExemptionStatus = ParseChar(ExemptionStatusOrdinal, "Exemption Status");
                    if (ExemptionStatus.HasValue && ExemptionStatus != 'E' && ExemptionStatus != 'N')
                        AddError("'{0}' is not a recognized employee exemption status indicator, please provide the value 'E' (Exempt) or 'N' (Non-Exempt)", ExemptionStatus);
                }
                //
                // Meets 50 in 75
                Meets50In75 = ParseBool(Meets50In75Ordinal, "Meets 50 in 75");
                //
                // Key Employee
                KeyEmployee = ParseBool(KeyEmployeeOrdinal, "Key Employee");
                //
                // Military Status
                MilitaryStatus = ParseByte(MilitaryStatusOrdinal, "Military Status");
                if (MilitaryStatus.HasValue && (MilitaryStatus < 0 || MilitaryStatus > 2))
                    AddError("'{0}' is not a valid value for 'Military Status'. Valid values are bytes 0 (Civilian), 1 (Active Duty) or 2 (Veteran)", MilitaryStatus);
                //
                // Employment Status
                if (Required(EmploymentStatusOrdinal, "Employment Status"))
                {
                    EmploymentStatus = ParseChar(EmploymentStatusOrdinal, "Employment Status");
                    if (EmploymentStatus.HasValue && !EmploymentStatusValue.IsValidValue(EmploymentStatus.Value))
                        AddError("'{0}' is not a valid value for 'Employment Status'. Valid values: {1}",
                            EmploymentStatus,
                            EmploymentStatusValue.GetCodeLookupAsString()
                        );
                }
                //
                // Termination Date
                TerminationDate = ParseDate(TerminationDateOrdinal, "Termination Date");
                //
                // Pay Rate
                PayRate = ParseDecimal(PayRateOrdinal, "Pay Rate");
                //
                // Pay Type
                PayType = ParseByte(PayTypeOrdinal, "Pay Type");
                if (PayType.HasValue && PayType != 1 && PayType != 2)
                    AddError("'{0}' is not a valid value for 'Pay Type'. Valid values are bytes 1 (Annual Salary) or 2 (Hourly)", PayType);
                //
                HireDate = ParseDate(HireDateOrdinal, "Hire Date");
                //
                // Rehire Date
                RehireDate = ParseDate(RehireDateOrdinal, "Rehire Date");
                //
                // Adjusted Service Date
                if (Required(AdjustedServiceDateOrdinal, "Adjusted Service Date"))
                {
                    AdjustedServiceDate = ParseDate(AdjustedServiceDateOrdinal, "Adjusted Service Date");
                }


                //
                // Scheduled Minutes per Week

                MinutesPerWeek = ParseInt(MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);
                //
                // Hours Worked in 12 Months

                HoursWorkedIn12Months = ParseDecimal(HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);

                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(VariableScheduleOrdinal, "Variable Schedule Flag");

                //
                //Percentage of Emplyees weekly work time
                FtePercentage = ParseDecimal(E_FtePercentageOrdinal, "Percentage of Employer's Time per week");
                //
                // Job Description
                JobDescription = Normalize(JobDescriptionOrdinal);
                //
                // Job Classification
                JobClassification = ParseByte(JobClassificationOrdinal, "Job Classification");
                if (JobClassification.HasValue && (JobClassification < 1 || JobClassification > 5))
                    AddError("The value '{0}' is not a valid 'Job Classification'. Valid values are bytes 1 (Sedentary Work), 2 (Light Work), 3 (Medium Work), 4 (Heavy Work) or 5 (Very Heavy Work)", JobClassification);
                //
                // Job Department
                Department = Normalize(DepartmentOrdinal);
                //
                // Cost Center Code
                CostCenterCode = Normalize(CostCenterCodeOrdinal);
                //
                // Schedule Effective Date
                ScheduleEffectiveDate = ParseDate(ScheduleEffectiveDateOrdinal, "Schedule Effective Date");
                //
                // SSN
                SSN = Normalize(E_SSNOrdinal);
                //
                // Pay Schedule Name
                PayScheduleName = Normalize(E_PayScheduleNameOrdinal);
                //
                // Start Day of Week
                byte? dow = ParseByte(E_StartDayOfWeekOrdinal, "Start Day of Week");
                if (dow.HasValue)
                {
                    if (dow < 0 || dow > 6)
                        AddError("The value'{0}' is not a valid 'Day of Week'. Valid values are bytes 0 (Sunday) through 6 (Saturday)", dow);
                    else
                        StartDayOfWeek = (DayOfWeek)dow.Value;
                }

                AverageMinutesWorkedPerWeek = ParseInt(E_AverageMinutesWorkedPerWeekOrdinal, "Average minutes worked per week");
                if (AverageMinutesWorkedPerWeek.HasValue && (AverageMinutesWorkedPerWeek > 10080 || AverageMinutesWorkedPerWeek < 0))
                    AddError("The 'Average minutes worked per week' of '{0}' is invalid. Value may not be less than zero and must be less than 10080 minutes", AverageMinutesWorkedPerWeek);


                WorkCounty = Normalize(E_WorkCountyOrdinal);

                ResidenceCounty = Normalize(E_ResidenceCountyOrdinal);

                WorkCity = Normalize(E_WorkCityOrdinal);

                // Airline Flight Crew
                AirlineFlightCrew = ParseBool(E_AirlineFlightCrewOrdinal, "Airline Flight Crew");
                #endregion
            }
            else if (RecType == RecordType.Basic)
            {
                #region Basic

                //
                // Employee Last Name
                if (Required(BasicLastNameOrdinal, "Employee Last Name"))
                    LastName = Normalize(BasicLastNameOrdinal);
                //
                // Employee First Name
                if (Required(BasicFirstNameOrdinal, "Employee First Name"))
                    FirstName = Normalize(BasicFirstNameOrdinal);
                //
                // Employee Middle Name
                MiddleName = Normalize(BasicMiddleNameOrdinal);
                //
                // Work State
                // Work Country
                if (Required(BasicWorkStateOrdinal, "Work State"))
                {
                    WorkState = Normalize(BasicWorkStateOrdinal).ToUpperInvariant();
                    WorkCountry = "US";
                    ValidUSState(WorkState, "Work State");
                }
                //
                // Employee Phone Home
                PhoneHome = Normalize(BasicPhoneHomeOrdinal);
                //
                // Employee Phone Work
                PhoneWork = Normalize(BasicPhoneWorkOrdinal);
                //
                // Employee Phone Mobile
                PhoneMobile = Normalize(BasicPhoneMobileOrdinal);
                //
                // Employee Email
                Email = Normalize(BasicEmailOrdinal);
                ValidEmail(Email, "Employee Email");
                //
                // Mailing Address
                Address = Normalize(BasicAddressOrdinal);
                //
                // Mailing Address 2
                Address2 = Normalize(BasicAddress2Ordinal);
                //
                // Mailing City
                City = Normalize(BasicCityOrdinal);
                //
                // Mailing State
                State = Normalize(BasicStateOrdinal);
                //
                // Mailing Postal Code
                PostalCode = Normalize(BasicPostalCodeOrdinal);
                //
                // Mailing Country
                Country = "US";
                if (!string.IsNullOrWhiteSpace(State))
                    ValidUSState(State, "Mailing State");
                if (!string.IsNullOrWhiteSpace(PostalCode) && PostalCode.Length == 4)
                    PostalCode = string.Concat("0", PostalCode);
                //
                // Manager Employee Number
                ManagerEmployeeNumber = Normalize(BasicManagerEmployeeNumberOrdinal);
                //
                // Gender
                Gender = ParseChar(BasicGenderOrdinal, "Date of Birth");
                if (Gender.HasValue && Gender != 'F' && Gender != 'M' && Gender != 'U')
                    AddError("'{0}' is not a recognized employee gender assignment code, please provide the value 'F' (Female), 'M' (Male) or 'U' (Unknown/Not Collected)", Gender);
                //
                // Adjusted Service Date
                if (Required(BasicAdjustedServiceDateOrdinal, "Adjusted Service Date"))
                    AdjustedServiceDate = ParseDate(BasicAdjustedServiceDateOrdinal, "Adjusted Service Date");
                //
                // Scheduled Minutes per Week
                MinutesPerWeek = ParseInt(BasicMinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);
                //
                // Hours Worked in 12 Months
                //if (Required(HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months"))
                //{
                HoursWorkedIn12Months = ParseDecimal(BasicHoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);
                //}
                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(BasicWorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(BasicWorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(BasicWorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(BasicWorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(BasicWorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(BasicWorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(BasicWorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(BasicVariableScheduleOrdinal, "Variable Schedule Flag");
                //
                //Percentage of Emplyees weekly work time
                FtePercentage = ParseDecimal(BasicFtePercentageOrdinal, "Percentage of Employer's Time per week");

                AverageMinutesWorkedPerWeek = ParseInt(BasicAverageMinutesWorkedPerWeekOrdinal, "Average minutes worked per week");
                if (AverageMinutesWorkedPerWeek.HasValue && (AverageMinutesWorkedPerWeek > 10080 || AverageMinutesWorkedPerWeek < 0))
                    AddError("The 'Average minutes worked per week' of '{0}' is invalid. Value may not be less than zero and must be less than 10080 minutes", AverageMinutesWorkedPerWeek);
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(BasicEmployerReferenceCodeOrdinal);

                WorkCounty = Normalize(BasicWorkCountyOrdinal);

                ResidenceCounty = Normalize(BasicResidenceCountyOrdinal);

                WorkCity = Normalize(BasicWorkCityOrdinal);
                // Airline Flight Crew
                AirlineFlightCrew = ParseBool(B_AirlineFlightCrewOrdinal, "Airline Flight Crew");
                #endregion
            }
            else if (RecType == RecordType.Spouse)
            {
                #region Spouse

                //
                // Spouse Employee Number
                if (Required(SpouseSpouseEmployeeNumberOrdinal, "Spouse Employee Number"))
                    SpouseEmployeeNumber = Normalize(SpouseSpouseEmployeeNumberOrdinal);
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(SpouseEmployerReferenceCodeOrdinal);

                #endregion
            }
            else if (RecType == RecordType.HR)
            {
                #region HR

                //
                // HR Contact Employee Number
                HREmployeeNumber = Normalize(ContactEmployeeNumberOrdinal);
                //
                // HR Contact Last Name
                HRLastName = Normalize(ContactLastNameOrdinal);
                //
                // HR Contact First Name
                HRFirstName = Normalize(ContactFirstNameOrdinal);
                //
                // HR Contact Phone
                HRPhone = Normalize(ContactPhoneOrdinal);
                //
                // HR Contact Email
                HREmail = Normalize(ContactEmailOrdinal);
                ValidEmail(HREmail, "HR Contact Email");
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(ContactEmployerReferenceCodeOrdinal);

                #endregion
            }
            else if (RecType == RecordType.Supervisor)
            {
                #region Supervisor

                //
                // Manager Employee Number
                ManagerEmployeeNumber = Normalize(ContactEmployeeNumberOrdinal);
                //
                // Manager Last Name
                ManagerLastName = Normalize(ContactLastNameOrdinal);
                //
                // Manager First Name
                ManagerFirstName = Normalize(ContactFirstNameOrdinal);
                //
                // Manager Phone
                ManagerPhone = Normalize(ContactPhoneOrdinal);
                //
                // Manager Email
                ManagerEmail = Normalize(ContactEmailOrdinal);
                ValidEmail(ManagerEmail, "Manager Email");
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(ContactEmployerReferenceCodeOrdinal);

                #endregion
            }
            else if (RecType == RecordType.Custom)
            {
                #region Custom Fields

                if (customFields == null && string.IsNullOrWhiteSpace(CustomerId))
                    AddError("Customer is missing from the file process and is required for reading configured custom fields");
                if (customFields == null && string.IsNullOrWhiteSpace(EmployerId))
                    AddError("Employer is missing from the file process and is required for reading configured custom fields");

                if (IsError)
                    return false;

                CustomFields = (customFields ?? CustomField.AsQueryable().Where(f => f.CustomerId == CustomerId && f.EmployerId == EmployerId)).ToList()
                    .Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee)
                    .Select(f => f.Clone())
                    .OrderBy(f => f.FileOrder ?? int.MaxValue)
                    .ToArray();

                for (var idx = 0; idx < CustomFields.Length; idx++)
                {
                    var colIdx = idx + 2;
                    var field = CustomFields[idx];

                    if (field.IsRequired && !Required(colIdx, field.Label))
                        continue;

                    object val = field.SelectedValue = null;
                    switch (field.DataType)
                    {
                        case CustomFieldType.Number:
                            val = ParseDecimal(colIdx, field.Label);
                            break;
                        case CustomFieldType.Flag:
                            val = ParseBool(colIdx, field.Label);
                            break;
                        case CustomFieldType.Date:
                            val = ParseDate(colIdx, field.Label);
                            break;
                        case CustomFieldType.Text:

                        default:
                            val = Normalize(colIdx);
                            break;
                    }
                    if (val != null)
                    {
                        switch (field.ValueType)
                        {
                            case CustomFieldValueType.SelectList:
                                string stringVal = val.ToString();
                                var item = field.ListValues.FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Key) && l.Key.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)
                                    || (!string.IsNullOrWhiteSpace(l.Value) && l.Value.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase))));
                                if (item == null)
                                {
                                    AddError("'{0}' is not a valid value for '{1}'. Valid values are {2}",
                                        val,
                                        field.Label,
                                        field.ListValues.Count > 10
                                        ? "defined under employer custom fields"
                                        : string.Join(", ", field.ListValues.Select(l => string.Concat(string.IsNullOrWhiteSpace(l.Value) ? l.Key : l.Value,
                                            string.IsNullOrWhiteSpace(l.Key) ? "" : " (",
                                            string.IsNullOrWhiteSpace(l.Key) ? "" : l.Key,
                                            string.IsNullOrWhiteSpace(l.Key) ? "" : ")"))));
                                }
                                else
                                {
                                    field.SelectedValue = val;
                                }
                                break;
                            case CustomFieldValueType.UserEntered:
                            default:
                                field.SelectedValue = val;
                                break;
                        }
                    }
                }

                #endregion
            }
            else if (RecType == RecordType.Schedule)
            {
                #region Schedule
                //
                // Scheduled Minutes per Week
                MinutesPerWeek = ParseInt(W_MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);
                //
                // Hours Worked in 12 Months
                HoursWorkedIn12Months = ParseDecimal(W_HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);
                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(W_WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(W_WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(W_WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(W_WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(W_WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(W_WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(W_WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(W_VariableScheduleOrdinal, "Variable Schedule Flag");
                //Percentage of Emplyees weekly work time
                FtePercentage = ParseDecimal(W_FtePercentageOrdinal, "Percentage of Employer's Time per week");
                //
                //
                // Schedule Effective Date
                ScheduleEffectiveDate = ParseDate(W_ScheduleEffectiveDateOrdinal, "Schedule Effective Date");
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(W_EmployerReferenceCodeOrdinal);
                //
                // No Overwrite Flag
                NoOverwriteFlag = ParseBool(W_NoOverwriteFlagOrdinal, "No Overwrite Flag");

                AverageMinutesWorkedPerWeek = ParseInt(W_AverageMinutesWorkedPerWeekOrdinal, "Average minutes worked per week");
                if (AverageMinutesWorkedPerWeek.HasValue && (AverageMinutesWorkedPerWeek > 10080 || AverageMinutesWorkedPerWeek < 0))
                    AddError("The 'Average minutes worked per week' of '{0}' is invalid. Value may not be less than zero and must be less than 10,080 minutes", AverageMinutesWorkedPerWeek);

                #endregion
            }
            else if (RecType == RecordType.Contact)
            {
                #region Contact

                //
                // Contact Type Code
                if (Required(OTypeOrdinal, "Contact Type Code"))
                    ContactTypeCode = Normalize(OTypeOrdinal);
                //
                // Contact Type Name
                ContactTypeName = Normalize(ORoleOrdinal);
                //
                // Exclusive
                ContactExclusive = ParseBool(OExclusiveOrdinal, "Exclusive");
                //
                // Contact Position
                ContactPosition = ParseInt(OPositionOrdinal, "Contact Position");
                //
                // Contact Employee Number
                ContactEmployeeNumber = Normalize(OEmployeeNumberOrdinal);
                //
                // Contact Last Name
                if (Required(OLastNameOrdinal, "Contact Last Name"))
                    LastName = Normalize(OLastNameOrdinal);
                //
                // Contact First Name
                if (Required(OFirstNameOrdinal, "Contact First Name"))
                    FirstName = Normalize(OFirstNameOrdinal);
                //
                // Contact Email
                Email = Normalize(OEmailOrdinal);
                ValidEmail(Email, "Contact Email");
                //
                // Contact Home Email
                EmailAlt = Normalize(OHomeEmailOrdinal);
                ValidEmail(EmailAlt, "Contact Home Email");
                //
                // Contact Phone
                PhoneWork = Normalize(OPhoneOrdinal);
                //
                // Contact Home Phone
                PhoneHome = Normalize(OHomePhoneOrdinal);
                //
                // Contact Mobile Phone
                PhoneMobile = Normalize(OMobilePhoneOrdinal);
                //
                // Contact Fax
                PhoneFax = Normalize(OFaxOrdinal);
                //
                // Contact Address
                Address = Normalize(OAddressOrdinal);
                //
                // Contact Address Line 2
                Address2 = Normalize(OAddress2Ordinal);
                //
                // Contact City
                City = Normalize(OCityOrdinal);
                //
                // Contact State/Province
                State = Normalize(OStateOrdinal);
                //
                // Contact Country
                Country = "US";
                //
                // Contact Postal Code
                PostalCode = Normalize(OPostalCodeOrdinal);
                //
                // Contact Military Status
                MilitaryStatus = ParseByte(OMilitaryStatusOrdinal, "Military Status");
                if (MilitaryStatus.HasValue && (MilitaryStatus < 0 || MilitaryStatus > 2))
                    AddError("'{0}' is not a valid value for 'Military Status'. Valid values are bytes 0 (Civilian), 1 (Active Duty) or 2 (Veteran)", MilitaryStatus);
                //
                // Clear Other Contacts
                ClearOtherContacts = ParseBool(OClearOtherContactsOrdinal, "Clear Other Contacts") ?? false;
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(OEmployerReferenceCodeOrdinal);

                #endregion Contact
            }
            else if (RecType == RecordType.Intermittent)
            {
                #region Intermittent

                //
                // Last Name
                LastName = Normalize(I_EmployeeLastNameOrdinal);
                //
                // First Name
                FirstName = Normalize(I_EmployeeFirstNameOrdinal);
                //
                // Case Number
                if (Required(I_CaseNumberOrdinal, "Unique Case Number"))
                    CaseNumber = Normalize(I_CaseNumberOrdinal);
                //
                // Date of Absence
                if (Required(I_DateOfAbsenceOrdinal, "Date of Absence"))
                    DateOfAbsence = ParseDate(I_DateOfAbsenceOrdinal, "Date of Absence");
                //
                // Minutes Approved
                MinutesApproved = ParseInt(I_MinutesApprovedOrdinal, "Minutes Approved");
                //
                // Minutes Denied
                MinutesDenied = ParseInt(I_MinutesDeniedOrdinal, "Minutes Denied");
                //
                // Minutes Pending
                MinutesPending = ParseInt(I_MinutesPendingOrdinal, "Minutes Pending");
                if (((MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0)) <= 0)
                    AddError("Total minutes of absence recorded for '{0:MM/dd/yyyy}' is '{1}'. The sum of minutes approved, pending and denied must be at least 1 minute.",
                        DateOfAbsence, (MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0));
                //
                // Policy Code
                PolicyCode = Normalize(I_PolicyCodeOrdinal);
                //
                // Workflow
                Workflow = ParseBool(I_WorkflowOrdinal, "Time Off Request Workflow");
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(I_EmployerReferenceCodeOrdinal);

                //ITOR type
                int? itor = ParseInt(I_IntermittentTimeOutRequestTypeOrdinal, "ITOR Type");
                if (itor != null)
                {
                    if (Enum.IsDefined(typeof(IntermittentType), itor))
                    {
                        IntermittentTimeOutRequestType = (IntermittentType)itor;
                    }
                    else
                    {
                        AddError("The 'ITOR Type' of '{0}' is invalid. Value must be 0 or 1", itor);
                    }
                }

                // Start Time of Leave
                StartTimeForLeave = ParseTime(I_StartTimeOfLeaveOrdinal, "Start Time for Leave");

                // End Time of Leave
                EndTimeForLeave = ParseTime(I_EndTimeOfLeaveOrdinal, "End Time for Leave");
                #endregion
            }
            else if (RecType == RecordType.Job)
            {
                #region Job

                //
                // Job Code
                if (Required(J_JobCodeOrdinal, "Job Code"))
                    JobCode = Normalize(J_JobCodeOrdinal);

                //
                // Job Name
                JobName = Normalize(J_JobNameOrdinal);

                //
                // Job Title
                JobTitle = Normalize(J_JobTitleOrdinal);

                //
                // Job Start Date
                JobStartDate = ParseDate(J_JobStartDateOrdinal, "Job Start Date") ?? DateTime.UtcNow.ToMidnight();

                //
                // Job End Date
                JobEndDate = ParseDate(J_JobEndDateOrdinal, "Job End Date");

                //
                // Job Location (Office Location)
                JobLocation = Normalize(J_JobLocationOrdinal);
                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(J_EmployerReferenceCodeOrdinal);

                //
                // Job Position
                JobPosition = ParseInt(J_JobPositionOrdinal, "Job Position") ?? J_JobPositionDefault;

                //
                // Job Status
                JobStatus = (AdjudicationStatus)(ParseInt(J_JobStatusOrdinal, "Job Status") ?? (int)AdjudicationStatus.Approved);
                #endregion Job
            }
            else if (RecType == RecordType.Organization)
            {
                #region Organization
                //
                // Organization Code
                if (Required(O_OrgCodeOrdinal, "Organization Code"))
                    OrgCode = Normalize(O_OrgCodeOrdinal);

                //
                // Organization Name
                OrgName = Normalize(O_OrgNameOrdinal);

                //
                // Organization Type Code
                OrgTypeCode = Normalize(O_OrgTypeCodeOrdinal);

                //
                // Organization Start Date
                OrgStartDate = ParseDate(O_OrgStartDateOrdinal, "Organization Start Date");

                //
                // Organization End Date
                OrgEndDate = ParseDate(O_OrgEndDateOrdinal, "Organization End Date");

                //
                // Organization End Date
                OrgAffiliation = ParseByte(O_OrgAffiliationOrdinal, "Organization Affiliation");

                //
                // Parent Organization Code
                ParentOrgCode = Normalize(O_ParentOrgCodeOrdinal);

                //
                // Parent Organization Name
                ParentOrgName = Normalize(O_ParentOrgNameOrdinal);

                //
                // Parent Organization Name
                ParentOrgTypeCode = Normalize(O_ParentOrgTypeCodeOrdinal);

                //
                // Employer Reference Code
                EmployerReferenceCode = Normalize(O_EmployerReferenceCodeOrdinal);

                SicCode = Normalize(O_SicCodeOrdinal);
                NaicsCode = Normalize(O_NaicsCodeOrdinal);
                OrgAddress1 = Normalize(O_Address1Ordinal);
                OrgAddress2 = Normalize(O_Address2Ordinal);
                OrgCity = Normalize(O_CityOrdinal);
                OrgPostalCode = Normalize(O_PostalCodeOrdinal);
                OrgState = Normalize(O_StateOrdinal);
                OrgCountry = Normalize(O_CountryOrdinal);

                #endregion Organization
            }
            else if (RecType == RecordType.CustomField)
            {
                #region CustomField
                //
                // Field Update Behaviour
                int updateBehaviour = ParseInt(F_UpdateBehaviourOrdinal, "Update Behaviour") ?? 0;
                CustomFieldUpdateBehavior updateBehaviourCon;
                if (Enum.TryParse<CustomFieldUpdateBehavior>(updateBehaviour.ToString(), out updateBehaviourCon))
                { 
                    CustomFieldUpdateBehaviour = updateBehaviourCon;
                }
                else
                { 
                    CustomFieldUpdateBehaviour = CustomFieldUpdateBehavior.Passive;
                }
                //
                // Field Code
                if (Required(F_FieldCodeOrdinal, "Custom Field Code"))
                    FieldCode = Normalize(F_FieldCodeOrdinal);
                //
                // Field Value
                if (CustomFieldUpdateBehaviour == CustomFieldUpdateBehavior.Force || Required(F_FieldValueOrdinal, "Custom Field Value"))
                    FieldValue = Normalize(F_FieldValueOrdinal);

                CaseNumber = Normalize(F_CaseNumberOrdinal);
                EmployerReferenceCode = Normalize(F_EmployerReferenceCodeOrdinal);
                
                int nc = ParseInt(F_EntityTargetOrdinal, "Entity Target") ?? 1;
                EntityTarget ncCon;
                if (Enum.TryParse<EntityTarget>(nc.ToString(), out ncCon))
                    Target = ncCon;
                else
                    Target = EntityTarget.Employee;

                if (IsError)
                    return false;

                var customFieldToUpdate = (CustomField.DistinctFind(null, CustomerId, EmployerId, true)).ToList()
                     .Where(f => (f.Target & Target) == Target)
                    .FirstOrDefault(cf => cf.Code == FieldCode)?.Clone();
                if (customFieldToUpdate != null)
                {
                    UpdateCustomFieldValue(ParseCustomFieldValue(customFieldToUpdate), customFieldToUpdate);
                    CustomFields = new CustomField[1] { customFieldToUpdate };
                }
                else
                {
                    CustomFields = new CustomField[0];
                }

                #endregion

            }
            return !IsError;
        }

        private void UpdateCustomFieldValue(object val, CustomField customField)
        {
            if (val == null)
                return;

            switch (customField.ValueType)
            {
                case CustomFieldValueType.SelectList:
                    string stringVal = val.ToString();
                    var item = GetMatchingCustomFieldValue(customField, stringVal);
                    if (item == null)
                    {
                        AddError(FormatCustomFieldError(customField, stringVal));
                    }
                    else
                    {
                        customField.SelectedValue = stringVal;
                    }
                    break;
                case CustomFieldValueType.UserEntered:
                default:
                    customField.SelectedValue = val;
                    break;
            }
        }

        private ListItem GetMatchingCustomFieldValue(CustomField customField, string stringVal)
        {
            ListItem matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Key) && l.Key.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));

            if (matchingItem != null)
                return matchingItem;

            return customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Value) && l.Value.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));
        }

        private string FormatCustomFieldError(CustomField customField, string val)
        {
            StringBuilder fieldError = new StringBuilder();
            fieldError.AppendFormat("'{0}' is not a valid value for '{1}'. ", val, customField.Label);
            if (customField.ListValues.Count > 10)
            {
                fieldError.AppendFormat("Valid values are defined under Custom Fields.");
            }
            else
            {
                fieldError.AppendFormat("Valid values are {0}", string.Join(", ", customField.ListValues.Select(l => FormatListValueForError(l))));
            }

            return fieldError.ToString();
        }

        private string FormatListValueForError(ListItem item)
        {
            return string.Format("{0} ({0})", item.Key, item.Value);
        }

        private object ParseCustomFieldValue(CustomField customField)
        {
            object val = customField.SelectedValue = null;
            switch (customField.DataType)
            {
                case CustomFieldType.Number:
                    val = ParseDecimal(F_FieldValueOrdinal, FieldValue);
                    break;
                case CustomFieldType.Flag:
                    val = ParseBool(F_FieldValueOrdinal, FieldValue);
                    break;
                case CustomFieldType.Date:
                    val = ParseDate(F_FieldValueOrdinal, FieldValue);
                    break;
                case CustomFieldType.Text:
                default:
                    val = Normalize(F_FieldValueOrdinal);
                    break;
            }

            return val;
        }

        #endregion Parsing

        #region ToString

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            int cols = 67;
            switch (RecType)
            {
                case RecordType.TimeScheduled:
                    cols = 5;
                    break;
                case RecordType.Spouse:
                    cols = 4;
                    break;
                case RecordType.HR:
                case RecordType.Supervisor:
                    cols = 8;
                    break;
                case RecordType.Basic:
                    cols = 35;
                    break;
                case RecordType.Eligibility:
                default:
                    cols = 71;
                    break;
                case RecordType.Custom:
                    cols = 2;
                    cols += CustomFields.Count();
                    break;
                case RecordType.Schedule:
                    cols = 17;
                    break;
                case RecordType.Contact:
                    cols = 23;
                    break;
                case RecordType.Intermittent:
                    cols = 12;
                    break;
                case RecordType.Job:
                    cols = 11;
                    break;
                case RecordType.CustomField:
                    cols = 5;
                    break;
                case RecordType.Organization:
                    cols = 12;
                    break;
            }

            string[] fields = new string[cols];

            fields[RecordTypeOrdinal] = Emit((char)RecType);
            fields[EmployeeNumberOrdinal] = Emit(EmployeeNumber);

            if (RecType == RecordType.TimeScheduled)
            {
                fields[DateScheduledOrdinal] = Emit(DateScheduled);
                fields[TimeScheduledOrdinal] = Emit(TimeScheduled);
                fields[M_EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
            }
            else if (RecType == RecordType.Spouse)
            {
                fields[SpouseSpouseEmployeeNumberOrdinal] = Emit(SpouseEmployeeNumber);
                fields[SpouseEmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
            }
            else if (RecType == RecordType.Supervisor)
            {
                fields[ContactEmployeeNumberOrdinal] = Emit(ManagerEmployeeNumber);
                fields[ContactLastNameOrdinal] = Emit(ManagerLastName);
                fields[ContactFirstNameOrdinal] = Emit(ManagerFirstName);
                fields[ContactPhoneOrdinal] = Emit(ManagerPhone);
                fields[ContactEmailOrdinal] = Emit(ManagerEmail);
                fields[ContactEmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
            }
            else if (RecType == RecordType.HR)
            {
                fields[ContactEmployeeNumberOrdinal] = Emit(HREmployeeNumber);
                fields[ContactLastNameOrdinal] = Emit(HRLastName);
                fields[ContactFirstNameOrdinal] = Emit(HRFirstName);
                fields[ContactPhoneOrdinal] = Emit(HRPhone);
                fields[ContactEmailOrdinal] = Emit(HREmail);
                fields[ContactEmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
            }
            else if (RecType == RecordType.Custom)
            {
                for (var i = 0; i < CustomFields.Count(); i++)
                {
                    var c = i + 2;
                    fields[c] = Emit(CustomFields[i].SelectedValue);
                }
            }
            else if (RecType == RecordType.CustomField)
            {
                for (var i = 0; i < CustomFields.Count(); i++)
                {
                    var c = i + 2;
                    fields[c] = Emit(CustomFields[i].SelectedValue);
                }
            }
            else if (RecType == RecordType.Schedule)
            {
                fields[W_MinutesPerWeekOrdinal] = Emit(MinutesPerWeek);
                fields[W_HoursWorkedIn12MonthsOrdinal] = Emit(HoursWorkedIn12Months);
                fields[W_WorkTimeSunOrdinal] = Emit(WorkTimeSun);
                fields[W_WorkTimeMonOrdinal] = Emit(WorkTimeMon);
                fields[W_WorkTimeTueOrdinal] = Emit(WorkTimeTue);
                fields[W_WorkTimeWedOrdinal] = Emit(WorkTimeWed);
                fields[W_WorkTimeThuOrdinal] = Emit(WorkTimeThu);
                fields[W_WorkTimeFriOrdinal] = Emit(WorkTimeFri);
                fields[W_WorkTimeSatOrdinal] = Emit(WorkTimeSat);
                fields[W_VariableScheduleOrdinal] = Emit(VariableSchedule);
                fields[W_ScheduleEffectiveDateOrdinal] = Emit(ScheduleEffectiveDate);
                fields[W_EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[W_NoOverwriteFlagOrdinal] = Emit(NoOverwriteFlag);
                fields[W_AverageMinutesWorkedPerWeekOrdinal] = Emit(AverageMinutesWorkedPerWeek);
                fields[W_FtePercentageOrdinal] = Emit(FtePercentage);
            }
            else if (RecType == RecordType.Contact)
            {
                fields[OTypeOrdinal] = Emit(ContactTypeCode);
                fields[ORoleOrdinal] = Emit(ContactTypeName);
                fields[OExclusiveOrdinal] = Emit(ContactExclusive);
                fields[OPositionOrdinal] = Emit(ContactPosition);
                fields[OEmployeeNumberOrdinal] = Emit(ContactEmployeeNumber);
                fields[OLastNameOrdinal] = Emit(LastName);
                fields[OFirstNameOrdinal] = Emit(FirstName);
                fields[OEmailOrdinal] = Emit(Email);
                fields[OHomeEmailOrdinal] = Emit(EmailAlt);
                fields[OPhoneOrdinal] = Emit(PhoneWork);
                fields[OHomePhoneOrdinal] = Emit(PhoneHome);
                fields[OMobilePhoneOrdinal] = Emit(PhoneMobile);
                fields[OFaxOrdinal] = Emit(PhoneFax);
                fields[OAddressOrdinal] = Emit(Address);
                fields[OAddress2Ordinal] = Emit(Address2);
                fields[OCityOrdinal] = Emit(City);
                fields[OStateOrdinal] = Emit(State);
                fields[OPostalCodeOrdinal] = Emit(PostalCode);
                fields[OMilitaryStatusOrdinal] = Emit(MilitaryStatus);
                fields[OClearOtherContactsOrdinal] = Emit(ClearOtherContacts);
                fields[OEmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
            }
            else if (RecType == RecordType.Basic)
            {
                fields[BasicLastNameOrdinal] = Emit(LastName);
                fields[BasicFirstNameOrdinal] = Emit(FirstName);
                fields[BasicMiddleNameOrdinal] = Emit(MiddleName);
                fields[BasicWorkStateOrdinal] = Emit(WorkState);
                fields[BasicPhoneHomeOrdinal] = Emit(PhoneHome);
                fields[BasicPhoneWorkOrdinal] = Emit(PhoneWork);
                fields[BasicPhoneMobileOrdinal] = Emit(PhoneMobile);
                fields[BasicEmailOrdinal] = Emit(Email);
                fields[BasicAddressOrdinal] = Emit(Address);
                fields[BasicAddress2Ordinal] = Emit(Address2);
                fields[BasicCityOrdinal] = Emit(City);
                fields[BasicStateOrdinal] = Emit(State);
                fields[BasicPostalCodeOrdinal] = Emit(PostalCode);
                fields[BasicManagerEmployeeNumberOrdinal] = Emit(ManagerEmployeeNumber);
                fields[BasicGenderOrdinal] = Emit(Gender);
                fields[BasicAdjustedServiceDateOrdinal] = Emit(AdjustedServiceDate);
                fields[BasicMinutesPerWeekOrdinal] = Emit(MinutesPerWeek);
                fields[BasicHoursWorkedIn12MonthsOrdinal] = Emit(HoursWorkedIn12Months);
                fields[BasicWorkTimeSunOrdinal] = Emit(WorkTimeSun);
                fields[BasicWorkTimeMonOrdinal] = Emit(WorkTimeMon);
                fields[BasicWorkTimeTueOrdinal] = Emit(WorkTimeTue);
                fields[BasicWorkTimeWedOrdinal] = Emit(WorkTimeWed);
                fields[BasicWorkTimeThuOrdinal] = Emit(WorkTimeThu);
                fields[BasicWorkTimeFriOrdinal] = Emit(WorkTimeFri);
                fields[BasicWorkTimeSatOrdinal] = Emit(WorkTimeSat);
                fields[BasicVariableScheduleOrdinal] = Emit(VariableSchedule);
                fields[BasicEmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[BasicWorkCountyOrdinal] = Emit(WorkCounty);
                fields[BasicResidenceCountyOrdinal] = Emit(ResidenceCounty);
                fields[BasicWorkCityOrdinal] = Emit(WorkCity);
                fields[B_AirlineFlightCrewOrdinal] = Emit(AirlineFlightCrew);
                fields[BasicFtePercentageOrdinal] = Emit(FtePercentage);
                fields[BasicAverageMinutesWorkedPerWeekOrdinal] = Emit(AverageMinutesWorkedPerWeek);
            }
            else if (RecType == RecordType.Intermittent)
            {
                fields[I_EmployeeLastNameOrdinal] = Emit(LastName);
                fields[I_EmployeeFirstNameOrdinal] = Emit(FirstName);
                fields[I_CaseNumberOrdinal] = Emit(CaseNumber);
                fields[I_DateOfAbsenceOrdinal] = Emit(DateOfAbsence);
                fields[I_MinutesApprovedOrdinal] = Emit(MinutesApproved);
                fields[I_MinutesDeniedOrdinal] = Emit(MinutesDenied);
                fields[I_MinutesPendingOrdinal] = Emit(MinutesPending);
                fields[I_PolicyCodeOrdinal] = Emit(PolicyCode);
                fields[I_WorkflowOrdinal] = Emit(Workflow);
                fields[I_EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[I_StartTimeOfLeaveOrdinal] = Emit(StartTimeForLeave);
                fields[I_EndTimeOfLeaveOrdinal] = Emit(EndTimeForLeave);
            }
            else if (RecType == RecordType.Job)
            {
                fields[J_JobCodeOrdinal] = Emit(JobCode);
                fields[J_JobNameOrdinal] = Emit(JobName);
                fields[J_JobTitleOrdinal] = Emit(JobTitle);
                fields[J_JobStartDateOrdinal] = Emit(JobStartDate);
                fields[J_JobEndDateOrdinal] = Emit(JobEndDate);
                fields[J_JobLocationOrdinal] = Emit(JobLocation);
                fields[J_EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[J_JobPositionOrdinal] = Emit(JobPosition);
                fields[J_JobStatusOrdinal] = Emit(JobStatus);
            }
            else if (RecType == RecordType.Organization)
            {
                fields[O_OrgCodeOrdinal] = Emit(OrgCode);
                fields[O_OrgNameOrdinal] = Emit(OrgName);
                fields[O_OrgTypeCodeOrdinal] = Emit(OrgTypeCode);
                fields[O_OrgStartDateOrdinal] = Emit(OrgStartDate);
                fields[O_OrgEndDateOrdinal] = Emit(OrgEndDate);
                fields[O_OrgAffiliationOrdinal] = Emit(OrgAffiliation);
                fields[O_ParentOrgCodeOrdinal] = Emit(ParentOrgCode);
                fields[O_ParentOrgNameOrdinal] = Emit(ParentOrgName);
                fields[O_ParentOrgTypeCodeOrdinal] = Emit(ParentOrgTypeCode);
                fields[O_EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[O_SicCodeOrdinal] = Emit(SicCode);
                fields[O_NaicsCodeOrdinal] = Emit(NaicsCode);
                fields[O_Address1Ordinal] = Emit(OrgAddress1);
                fields[O_Address2Ordinal] = Emit(OrgAddress2);
                fields[O_CityOrdinal] = Emit(OrgCity);
                fields[O_PostalCodeOrdinal] = Emit(OrgPostalCode);
                fields[O_StateOrdinal] = Emit(OrgState);
                fields[O_CountryOrdinal] = Emit(OrgCountry);

            }
            else
            {
                fields[LastNameOrdinal] = Emit(LastName);
                fields[FirstNameOrdinal] = Emit(FirstName);
                fields[MiddleNameOrdinal] = Emit(MiddleName);
                fields[JobTitleOrdinal] = Emit(JobTitle);
                fields[JobLocationOrdinal] = Emit(JobLocation);
                fields[WorkStateOrdinal] = Emit(WorkState);
                fields[WorkCountryOrdinal] = Emit(WorkCountry);
                fields[PhoneHomeOrdinal] = Emit(PhoneHome);
                fields[PhoneWorkOrdinal] = Emit(PhoneWork);
                fields[PhoneMobileOrdinal] = Emit(PhoneMobile);
                fields[PhoneAltOrdinal] = Emit(PhoneAlt);
                fields[EmailOrdinal] = Emit(Email);
                fields[EmailAltOrdinal] = Emit(EmailAlt);
                fields[AddressOrdinal] = Emit(Address);
                fields[Address2Ordinal] = Emit(Address2);
                fields[CityOrdinal] = Emit(City);
                fields[StateOrdinal] = Emit(State);
                fields[PostalCodeOrdinal] = Emit(PostalCode);
                fields[CountryOrdinal] = Emit(Country);
                fields[EmploymentTypeOrdinal] = Emit(EmploymentType);
                fields[ManagerEmployeeNumberOrdinal] = Emit(ManagerEmployeeNumber);
                fields[ManagerLastNameOrdinal] = Emit(ManagerLastName);
                fields[ManagerFirstNameOrdinal] = Emit(ManagerFirstName);
                fields[ManagerPhoneOrdinal] = Emit(ManagerPhone);
                fields[ManagerEmailOrdinal] = Emit(ManagerEmail);
                fields[HREmployeeNumberOrdinal] = Emit(HREmployeeNumber);
                fields[HRLastNameOrdinal] = Emit(HRLastName);
                fields[HRFirstNameOrdinal] = Emit(HRFirstName);
                fields[HRPhoneOrdinal] = Emit(HRPhone);
                fields[HREmailOrdinal] = Emit(HREmail);
                fields[SpouseEmployeeNumberOrdinal] = Emit(SpouseEmployeeNumber);
                fields[DateOfBirthOrdinal] = Emit(DateOfBirth);
                fields[GenderOrdinal] = Emit(Gender);
                fields[ExemptionStatusOrdinal] = Emit(ExemptionStatus);
                fields[Meets50In75Ordinal] = Emit(Meets50In75);
                fields[KeyEmployeeOrdinal] = Emit(KeyEmployee);
                fields[MilitaryStatusOrdinal] = Emit(MilitaryStatus);
                fields[EmploymentStatusOrdinal] = Emit(EmploymentStatus);
                fields[TerminationDateOrdinal] = Emit(TerminationDate);
                fields[PayRateOrdinal] = Emit(PayRate);
                fields[PayTypeOrdinal] = Emit(PayType);
                fields[HireDateOrdinal] = Emit(HireDate);
                fields[RehireDateOrdinal] = Emit(RehireDate);
                fields[AdjustedServiceDateOrdinal] = Emit(AdjustedServiceDate);
                fields[MinutesPerWeekOrdinal] = Emit(MinutesPerWeek);
                fields[HoursWorkedIn12MonthsOrdinal] = Emit(HoursWorkedIn12Months);
                fields[WorkTimeSunOrdinal] = Emit(WorkTimeSun);
                fields[WorkTimeMonOrdinal] = Emit(WorkTimeMon);
                fields[WorkTimeTueOrdinal] = Emit(WorkTimeTue);
                fields[WorkTimeWedOrdinal] = Emit(WorkTimeWed);
                fields[WorkTimeThuOrdinal] = Emit(WorkTimeThu);
                fields[WorkTimeFriOrdinal] = Emit(WorkTimeFri);
                fields[WorkTimeSatOrdinal] = Emit(WorkTimeSat);
                fields[VariableScheduleOrdinal] = Emit(VariableSchedule);
                fields[JobDescriptionOrdinal] = Emit(JobDescription);
                fields[JobClassificationOrdinal] = Emit(JobClassification);
                fields[DepartmentOrdinal] = Emit(Department);
                fields[CostCenterCodeOrdinal] = Emit(CostCenterCode);
                fields[ScheduleEffectiveDateOrdinal] = Emit(ScheduleEffectiveDate);
                fields[EmployerReferenceCodeOrdinal] = Emit(EmployerReferenceCode);
                fields[E_SSNOrdinal] = Emit(SSN);
                fields[E_PayScheduleNameOrdinal] = Emit(PayScheduleName);
                fields[E_StartDayOfWeekOrdinal] = Emit((byte?)StartDayOfWeek);
                fields[E_WorkCountyOrdinal] = Emit(WorkCounty);
                fields[E_ResidenceCountyOrdinal] = Emit(ResidenceCounty);
                fields[E_WorkCityOrdinal] = Emit(WorkCity);
                fields[E_AirlineFlightCrewOrdinal] = Emit(AirlineFlightCrew);
                fields[E_FtePercentageOrdinal] = Emit(FtePercentage);
            }

            string record = string.Join(Delimiter.ToString(), fields);
            return record;
        } // ToString

        #endregion ToString

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public override bool AddError(string error, params object[] args)
        {
            var res = base.AddError(error, args);
            if (this.RowResult != null)
                this.RowResult.Error = base.Error;
            return res;
        }


        /// <summary>
        /// Determines whether [is multi employer] [the specified row].
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="employerReferenceCode">The employer reference code.</param>
        /// <returns></returns>
        public static bool IsMultiEmployer(string[] row, out string employerReferenceCode)
        {
            if (row == null || row.Length <= 1)
            {
                employerReferenceCode = null;
                return false;
            }
            char recordType;
            if (!Char.TryParse(row[RecordTypeOrdinal], out recordType))
            {
                employerReferenceCode = null;
                return false;
            }

            int ordinal = int.MaxValue;
            var recType = (RecordType)recordType;

            switch (recType)
            {
                case RecordType.Eligibility:
                    ordinal = EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.TimeScheduled:
                    ordinal = M_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Spouse:
                    ordinal = SpouseEmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Basic:
                    ordinal = BasicEmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Supervisor:
                case RecordType.HR:
                    ordinal = ContactEmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Schedule:
                    ordinal = W_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Contact:
                    ordinal = OEmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Intermittent:
                    ordinal = I_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Job:
                    ordinal = J_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Organization:
                    ordinal = O_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.CustomField:
                    ordinal = F_EmployerReferenceCodeOrdinal;
                    break;
                case RecordType.Custom:
                default:
                    // These don't have the ability to pass employer reference code
                    employerReferenceCode = null;
                    return false;
            }
            employerReferenceCode = row.Length > ordinal ? row[ordinal] : null;
            return !string.IsNullOrWhiteSpace(employerReferenceCode);
        }

        private bool ValidateEmployementType(string employeMentType)
        {
            if (employeMentType == "1" || employeMentType == "2" || employeMentType == "3")
            {
                return true;
            }
            if (EmployeeClassTypes.Any(e => e.Code == employeMentType))
            {
                return true;
            }
            return false;
        }

        private string GetEmployementType(string employeMentType)
        {
            if (employeMentType == "1")
            {
                return WorkType.FullTime;
            }
            if (employeMentType == "2")
            {
                return WorkType.PartTime;
            }
            if (employeMentType == "3")
            {
                return WorkType.PerDiem;
            }

            return employeMentType;
        }

        private string GetEmploymentTypeName(string employmentType)
        {
            if (EmployeeClassTypes !=null && EmployeeClassTypes.Any(e => e.Code == employmentType))
            {
                return EmployeeClassTypes.FirstOrDefault(e => e.Code == employmentType).Name;
            }
            return employmentType;
        }

    }
}
