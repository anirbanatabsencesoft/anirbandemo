﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Jobs;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Processing.EL
{
    public partial class EligibilityUploadService : LogicService
    {
        public EligibilityUploadService(User u = null)
            : base(u)
        {

        }

        public EligibilityUploadService(Customer customer, Employer employer = null, User u = null)
            : base(customer, employer, u)
        {

        }
        #region CRUD

        /// <summary>
        /// Creates the eligibility data file upload.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        /// <exception cref="AbsenceSoftException">
        /// No eligibility upload file information was provided.
        /// or
        /// The eligibility upload original file name was not provided and is required.
        /// or
        /// The eligibility upload file key was not provided and is required.
        /// or
        /// The eligibility upload employer was not provided and is required.
        /// or
        /// The user does not have permissions to upload an eligibility file for this employer
        /// </exception>
        public EligibilityUpload CreateEligibilityDataFileUpload(EligibilityUpload upload)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.CreateEligibilityDataFileUpload"))
            {
                if (upload == null)
                    throw new AbsenceSoftException("No eligibility upload file information was provided.");
                if (string.IsNullOrWhiteSpace(upload.FileName))
                    throw new AbsenceSoftException("The eligibility upload original file name was not provided and is required.");
                if (string.IsNullOrWhiteSpace(upload.FileKey))
                    throw new AbsenceSoftException("The eligibility upload file key was not provided and is required.");

                upload.CustomerId = upload.CustomerId ?? CurrentUser.CustomerId ?? (CurrentUser.Id == User.System.Id ? upload.CustomerId : null);
                upload.CreatedById = CurrentUser == null ? User.System.Id : CurrentUser.Id;
                upload.ModifiedById = CurrentUser == null ? User.System.Id : CurrentUser.Id;
                upload.EmployerId = string.IsNullOrEmpty(upload.EmployerId) ? null : upload.EmployerId;
                upload.Processed = 0;
                upload.Records = 0;
                upload.Errors = 0;
                upload = upload.Save();

                try
                {
                    QueueFile(upload);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("There was an error uploading the file '{0}' for processing.", upload.Id), ex);
                    upload.Status = ProcessingStatus.Failed;
                    upload.Save();
                    ReportFailed(upload.Id,
                        string.Format("There was an error queueing the file, '{2}', for processing. Please try uploading the file again or contact your system administrator.{0}{1}",
                        Environment.NewLine,
                        ex.Message,
                        upload.FileName));
                    throw;
                }

                return upload;
            }
        } // CreateEligibilityDataFileUpload

        /// <summary>
        /// Lists the eligibility data files.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public List<EligibilityUpload> ListEligibilityDataFiles()
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ListEligibilityDataFiles"))
            {
                if (CurrentEmployer != null)
                {
                    return EligibilityUpload.AsQueryable().Where(u => u.EmployerId == EmployerId && u.CustomerId == CustomerId)
                        .OrderByDescending(u => u.CreatedDate).ToList();
                }
                else
                {
                    return EligibilityUpload.AsQueryable().Where(u => u.CustomerId == CustomerId && !u.EmployerId.Any()).OrderByDescending(u => u.CreatedDate).ToList();
                }
            }
        } // ListEligibilityDataFiles

        public List<EligibilityUpload> ListLinkEligibilityDataFiles(string linkedEligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ListLinkEligibilityDataFiles"))
            {
                if (string.IsNullOrEmpty(linkedEligibilityUploadId))
                    throw new AbsenceSoftException("Linked eligibility id not found.");

                return EligibilityUpload.AsQueryable().Where(u => u.LinkedEligibilityUploadId == linkedEligibilityUploadId).OrderByDescending(u => u.CreatedDate).ToList();
            }
        } // ListLinkEligibilityDataFiles


        /// <summary>
        /// Lists the eligibility data file errors.
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <returns></returns>
        public List<EligibilityUploadResult> ListEligibilityDataFileErrors(string eligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ListEligibilityDataFileErrors"))
            {
                return EligibilityUploadResult.AsQueryable().Where(u => u.EligibilityUploadId == eligibilityUploadId && u.IsError).ToList()
                    .OrderBy(u => u.RowNumber).ThenByDescending(u => u.CreatedDate).ToList();
            }
        } // ListEligibilityDataFileErrors


        /// <summary>
        /// 
        /// Deletes the eligibility data file.
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <param name="currentUser">The current user.</param>
        /// <exception cref="AbsenceSoftException">
        /// Uploaded file was not found.
        /// or
        /// User does not have access to remove this eligibility file upload.
        /// </exception>
        public void DeleteEligibilityDataFile(string eligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.DeleteEligibilityDataFile"))
            {
                EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
                if (upload == null)
                    throw new AbsenceSoftException("Uploaded file was not found.");

                if (!CurrentUser.HasEmployerAccess(upload.EmployerId, Permission.UploadEligibilityFile))
                    throw new AbsenceSoftException("User does not have access to remove this eligibility file upload.");

                using (FileService svc = new FileService())
                    svc.DeleteS3Object(upload.FileKey, Settings.Default.S3BucketName_Processing);

                upload.Delete();
                EligibilityUploadResult.Delete(EligibilityUploadResult.Query.EQ(q => q.EligibilityUploadId, upload.Id));
            }
        } // DeleteEligibilityDataFile

        /// <summary>
        /// Cancels the eligibility data file.
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <param name="currentUser">The current user.</param>
        /// <exception cref="AbsenceSoftException">
        /// Uploaded file was not found.
        /// or
        /// User does not have access to cancel this eligibility file upload.
        /// </exception>
        public void CancelEligibilityDataFile(string eligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.CancelEligibilityDataFile"))
            {
                EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
                if (upload == null)
                    throw new AbsenceSoftException("Uploaded file was not found.");

                if (!CurrentUser.HasEmployerAccess(upload.EmployerId, Permission.UploadEligibilityFile))
                    throw new AbsenceSoftException("User does not have access to cancel this eligibility file upload.");

                if (upload.Status != ProcessingStatus.Pending && upload.Status != ProcessingStatus.Processing)
                    return;

                EligibilityUpload.Update(EligibilityUpload.Query.EQ(u => u.Id, eligibilityUploadId),
                    EligibilityUpload.Updates.Set(u => u.Status, ProcessingStatus.Canceled)
                    .Set(u => u.ModifiedDate, DateTime.UtcNow));
            }
        } // CancelEligibilityDataFile

        /// <summary>
        /// Determines whether [is eligibility data file canceled or deleted] [the specified eligibility upload identifier].
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <returns></returns>
        public bool IsEligibilityDataFileCanceledOrDeleted(string eligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.IsEligibilityDataFileCanceledOrDeleted"))
            {
                return EligibilityUpload.AsQueryable().Count(u => u.Id == eligibilityUploadId && u.Status != ProcessingStatus.Canceled) <= 0;
            }
        }

        /// <summary>
        /// Reports the failed.
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <param name="error">The error.</param>
        public void ReportFailed(string eligibilityUploadId, string error, long rowNumber = -1)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ReportFailed"))
            {
                var eg = EligibilityUpload.GetById(eligibilityUploadId);
                if (eg == null) return;

                new EligibilityUploadResult()
                {
                    EligibilityUploadId = eligibilityUploadId,
                    RowNumber = rowNumber,
                    Error = error,
                    RowSource = null,
                    CreatedById = eg.CreatedById,
                    ModifiedById = eg.ModifiedById
                }.Save();
                EligibilityUpload.Update(
                    EligibilityUpload.Query.EQ(u => u.Id, eligibilityUploadId),
                    EligibilityUpload.Updates
                    .Set(u => u.Status, ProcessingStatus.Failed)
                    .Set(u => u.ModifiedDate, DateTime.UtcNow));
            }
        } // ReportFailed

        /// <summary>
        /// Reports the results.
        /// </summary>
        /// <param name="results">The results.</param>
        public void ReportResults(List<EligibilityUploadResult> results)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ReportResults"))
            {
                if (!results.Any()) return;

                var bwo = EligibilityUploadResult.Repository.Collection.InitializeUnorderedBulkOperation();
                foreach (var row in results)
                {
                    var update = EligibilityUploadResult.Updates
                            .SetOnInsert(r => r.EligibilityUploadId, row.EligibilityUploadId)
                            .SetOnInsert(r => r.RowNumber, row.RowNumber)
                            .SetOnInsert(r => r.CreatedById, row.CreatedById)
                            .SetOnInsert(r => r.CreatedDate, DateTime.UtcNow)
                            .Set(r => r.IsError, row.IsError)
                            .Set(r => r.IsRetry, row.IsRetry)
                            .Set(r => r.ModifiedById, row.ModifiedById)
                            .Set(r => r.ModifiedDate, DateTime.UtcNow);
                    if (row.IsError)
                    {
                        update = update.Set(r => r.Error, row.Error);
                        update = update.SetOnInsert(r => r.RowSource, row.RowSource);
                    }
                    else
                    {
                        update = update.Unset(r => r.Error);
                        update = update.Unset(r => r.RowSource);
                    }

                    bwo.Find(EligibilityUploadResult.Query.And(
                            EligibilityUploadResult.Query.EQ(e => e.EligibilityUploadId, row.EligibilityUploadId),
                            EligibilityUploadResult.Query.EQ(e => e.RowNumber, row.RowNumber)
                        ))
                        .Upsert()
                        .UpdateOne(update);
                }
                bwo.Execute();
            }
        } // ReportResults

        /// <summary>
        /// Generates the CSV.
        /// </summary>
        /// <param name="eligibilityUploadId">The eligibility upload identifier.</param>
        /// <returns></returns>
        public byte[] GenerateCSV(string eligibilityUploadId)
        {
            EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
            var fileUploadErrors = new EligibilityUploadService().Using(s => s.ListEligibilityDataFileErrors(eligibilityUploadId));

            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, Encoding.UTF8);

            // File Information
            writer.Write("File Name");
            writer.Write(",");
            writer.Write(upload.FileName);
            writer.Write(",");
            writer.Write(",");
            writer.Write(",");
            writer.Write("Upload Date");
            writer.Write(",");
            writer.Write(upload.CreatedDate.ToString("MM/dd/yyyy"));
            writer.Write("\n");


            writer.Write("Status");
            writer.Write(",");
            writer.Write(upload.Status);
            writer.Write(",");
            writer.Write(",");
            writer.Write(",");
            writer.Write("Total Records");
            writer.Write(",");
            writer.Write(upload.Records);
            writer.Write("\n");

            writer.Write("Processing Time");
            writer.Write(",");
            writer.Write(upload.Elapsed);
            writer.Write(",");
            writer.Write(",");
            writer.Write(",");
            writer.Write("Records Processed");
            writer.Write(",");
            writer.Write(upload.Processed);
            writer.Write("\n");


            writer.Write("Uploaded By");
            writer.Write(",");
            writer.Write(upload.CreatedByUserName);
            writer.Write(",");
            writer.Write(",");
            writer.Write(",");
            writer.Write("Progress");
            writer.Write(",");
            writer.Write(upload.Progress);
            writer.Write("\n");


            writer.Write("#Errors");
            writer.Write(",");
            writer.Write(upload.Errors);
            writer.Write("\n\r");
            writer.Write("\n\r");
            writer.Write("\n\r");

            writer.Write("Row #");
            writer.Write(",");
            writer.Write("Error Message");
            //writer.Write(",");
            //writer.Write("Row Source");
            writer.Write("\n");
            // Error Information
            foreach (var error in fileUploadErrors)
            {
                writer.Write(error.RowNumber);
                writer.Write(",");
                writer.Write(Escape(error.Error));
                //writer.Write(",");
                //writer.Write(Escape(error.RowSource));
                writer.Write("\n");
            }


            writer.Flush();
            output.Position = 0;

            return output.ToArray();

        }

        /// <summary>
        /// This method escap some chars from string
        /// </summary>
        /// <param name="s">string to be escaped from some chars</param>
        /// <returns>returns string with escaped chars</returns>
        private static string Escape(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        private const string Comma = ",";
        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        #endregion

        #region Process

        /// <summary>
        /// Adds an eligibility file upload to the queue for processing to be picked up by an out of band
        /// queue processor thingy.
        /// </summary>
        /// <param name="eligibilityUploadId">The id of the eligibility upload to queue.</param>
        /// <param name="customerId">The customer id for the eligibility upload</param>
        /// <param name="employerId">The employer id for the eligibility upload</param>
        /// <returns>The queue item identifier for the message that was created (not typically needed)</returns>
        public string QueueFile(EligibilityUpload upload)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.QueueFile"))
            {
                using (QueueService svc = new QueueService())
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("EligibilityUploadId", upload.Id);
                    parameters.Add("CustomerId", upload.CustomerId);
                    if (!string.IsNullOrWhiteSpace(upload.EmployerId))
                    {
                        parameters.Add("EmployerId", upload.EmployerId);
                    }

                    return svc.Add(MessageQueues.AtCommonQueue, MessageType.EligibililtyLoad, upload.Id, parameters);

                }
            }

        } // QueueFile



        /// <summary>
        /// Takes a queue item from the eligibility upload file queue, sets the status of the eligibility upload
        /// to processing, breaks it down reading it line by line and creates
        /// a record queue item for each row of text in the file; then sets the total number of records and removes
        /// the item from the queue.
        /// </summary>
        /// <param name="item">The queue item to process.</param>
        public void ProcessFile(QueueItem item)
        {
            Stopwatch watcher = new Stopwatch();
            watcher.Start();
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ProcessFile"))
            {
                // Simple common-sense validation
                if (item == null)
                    throw new ArgumentNullException("item");
                if (item.QueueName != MessageQueues.AtCommonQueue)
                    throw new AbsenceSoftException(string.Format("The queue item provided from queue '{0}' is not from the expected queue '{1}' and may not be of the correct type.",
                        item.QueueName, MessageQueues.AtCommonQueue));

                // Get our Id from the message body
                string eligibilityUploadId = item.Body;
                // Get the eligibility upload item from the database.
                EligibilityUpload upload = EligibilityUpload.GetById(eligibilityUploadId);
                if (upload == null)
                    throw new AbsenceSoftException("Eligibility file upload not found");
                // This is not pending a read, so we need to skip it.
                if (upload.Status != ProcessingStatus.Pending)
                    return;
                // Set the status of our eligibilitiy upload to processing, this is a must in order to properly process
                //  any of the rows.
                upload.Status = ProcessingStatus.Processing;
                upload.Save();

                // Used to track the total number of records the file has.
                List<EligibilityEmployerSplit> altUploads = null;

                try
                {

                    // Read the file line by line, assume it's a zip file but this method will also perform normal processing
                    //  if the file in question is not a zip file (hooray for wicked smaht logic).

                    long validRows = 0, invalidRows = 0, totalRows = 0; //This is only Required for Company Wise EL to display Records Failed & Processed
                    using (EligibilityUploadPostgresHandler ph = new EligibilityUploadPostgresHandler(upload.FileKey, upload.Id, upload.CustomerId, upload.EmployerId))
                    {
                        using (FileService fs = new FileService())
                        {
                            fs.ReadDelimitedZipFileByLine(upload.FileKey, Settings.Default.S3BucketName_Processing, (rowNumber, parts, delimiter, recordText) =>
                            {
                                string refCode;
                                string refRecType;
                                bool isSplit = false;

                                if (parts != null)
                                {
                                    EligibilityRow.IsMultiEmployer(parts, out refCode);

                                    altUploads = altUploads ?? new List<EligibilityEmployerSplit>();
                                    if (upload.Employer == null)
                                    {
                                        if (upload.Employer == null && EligibilityRow.GetRecordType(parts, out refRecType) && refRecType == "C")
                                        {
                                            ReportFailed(eligibilityUploadId, "cannot contain  'C' Record", rowNumber);
                                            invalidRows += 1;
                                        }

                                        if (upload.Employer == null && String.IsNullOrEmpty(refCode))
                                        {
                                            ReportFailed(eligibilityUploadId, "does not contains reference code", rowNumber);
                                            invalidRows += 1;
                                        }


                                        var split = altUploads.FirstOrDefault(u => string.Equals(u.EmployerReferenceCode, refCode, StringComparison.InvariantCultureIgnoreCase)) ??
                                                altUploads.AddFluid(new EligibilityEmployerSplit(upload, refCode));

                                        if (split.IsValid())
                                        {
                                            validRows += 1;
                                            split.Write(recordText);
                                            isSplit = true;
                                        }
                                        else if (!string.IsNullOrEmpty(refCode))
                                        {
                                            //Already handled null or empty refcode above , so this will only handle invalid refcode
                                            //this is only valid for Company wise EL 
                                            ReportFailed(eligibilityUploadId, refCode + " is not a valid Employer Reference code, no employer matches with " + refCode, rowNumber);
                                            invalidRows += 1;
                                        }
                                    }

                                    if (upload.Employer != null && upload.Employer.ReferenceCode != null && !upload.Employer.ReferenceCode.Equals(refCode, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        var split = altUploads.FirstOrDefault(u => string.Equals(u.EmployerReferenceCode, refCode, StringComparison.InvariantCultureIgnoreCase)) ??
                                        altUploads.AddFluid(new EligibilityEmployerSplit(upload, refCode));
                                        if (split.IsValid())
                                        {
                                            split.Write(recordText);
                                            return;
                                        }
                                    }

                                    if (upload.Employer != null)
                                    {
                                        if (parts.Length == 1)
                                        {
                                            ReportFailed(eligibilityUploadId, "No delimiters or properly formatted information found in this record", rowNumber);
                                            invalidRows += 1;
                                            upload.Errors += 1;
                                        }
                                        else
                                        {
                                            // Here we have record text, so we need to push it to the record processing queue to get picked up by another thread.
                                            EligibilityUploadResult result = new EligibilityUploadResult()
                                            {
                                                EligibilityUploadId = eligibilityUploadId,
                                                RowNumber = rowNumber,
                                                RowParts = parts,
                                                RowSource = recordText,
                                                Error = null,
                                                IsRetry = false,
                                                CreatedById = upload.CreatedById,
                                                ModifiedById = upload.ModifiedById,
                                                IsSplit = isSplit,
                                                RefCode = refCode
                                            };

                                            //set employee number if available
                                            if (parts.Length > 0)
                                            {
                                                result.RecordType = parts[0];
                                                result.EmployeeNumber = parts[1];
                                            }

                                            ph.Add(result);
                                            validRows += 1;
                                        }
                                    }
                                }
                                else
                                {
                                    ReportFailed(eligibilityUploadId, "cannot be parsed using the current Delimiters.", rowNumber);
                                    invalidRows += 1;
                                    upload.Errors += 1;
                                }

                                totalRows += 1;
                            });
                        }

                        //update snapshot
                        ph.FinalizeParsingSnapshot(invalidRows);
                    }

                    // We're all done so we need to update the eligibility upload's # of records.
                    upload.Records = totalRows;
                    upload.ParsedRecords += validRows;
                    upload.ParsingErrors += invalidRows;
                    upload.Save();

                    //
                    // Handle the other files, if any before moving forward.
                    if (altUploads != null && altUploads.Any(u => u.IsValid()))
                    {
                        foreach (var alty in altUploads.Where(u => u.IsValid()))
                        {
                            using (alty)
                            {
                                // ZIP the file
                                // Update the file name
                                // Re-Upload the new file chunk/part for processing independently
                                alty.Complete();
                            }
                        }
                    }                   

                    Log.Info("Finished reading records/rows for '{0}', eligibility upload id {1}", upload.FileName, upload.Id);
                }
                catch (MongoBulkWriteException bulkEx)
                {
                    var errorKey = Guid.NewGuid().ToString("D");
                    Log.Error(string.Format("{0}: Error processing eligibility file upload '{1}'", errorKey, upload.Id), bulkEx);
                    foreach (var err in bulkEx.WriteErrors)
                        Log.Error(string.Format("{0}: Error processing eligibility file upload '{1}'", errorKey, err.Message));

                    StringBuilder msg = new StringBuilder();
                    msg.AppendLine(bulkEx.Message);
                    bulkEx.WriteErrors.ForEach(e => msg.AppendLine(e.Message));
                    ReportFailed(upload.Id, msg.ToString());
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error processing eligibility file upload '{0}'", upload.Id), ex);
                    ReportFailed(upload.Id, ex.Message);
                }
                finally
                {
                    upload = null;
                    try
                    {
                        if (altUploads != null && altUploads.Any())
                            foreach (var alty in altUploads.Where(a => a != null))
                                alty.Dispose();
                    }
                    catch { }
                    GC.Collect();
                }
            }

            watcher.Stop();
            var ts = watcher.Elapsed;
            Console.WriteLine($"Time taken {ts.Hours} hrs, {ts.Minutes} mins, {ts.Seconds} secs, {ts.Milliseconds} ms");
            watcher = null;
        } // ProcessFile


        /// <summary>
        /// Takes a list of queue items from the eligibility upload record queue, then in parallel, parses each row
        /// and performs the employee "upsert", reports any errors or success and removes each applicable item from the queue.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <param name="uploadItems">The eligibility upload items to process.</param>
        public void ProcessRecords(EligibilityUpload upload, List<EligibilityUploadResult> uploadItems)
        {
            // Simple common-sense validation
            if (upload == null || uploadItems == null || !uploadItems.Any() || upload.Status == ProcessingStatus.Canceled)
                return;
            Log.Info("Processing records/rows for '{0}', eligibility upload id {1}", upload.FileName, upload.Id);

            List<Country> allCountries = null;
            using (var administrationService = new AdministrationService(CurrentUser))
            {
                allCountries = administrationService.GetCountriesFromXML(includeRegions: false);
            }
            // Quick function to test whether the current process is cancelled or not.
            Func<bool> isCancelled = new Func<bool>(() => (EligibilityUpload.GetById(upload.Id) ?? new EligibilityUpload() { Status = ProcessingStatus.Canceled }).Status == ProcessingStatus.Canceled);

            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.ProcessRecords"))
            {

                var refCode = upload.Employer.ReferenceCode;
                var employersCustomFields = CustomField.DistinctFind(null, upload.CustomerId, upload.EmployerId, true)
                    .ToList().Where(cf => (cf.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();

                var employeeClassTypes = EmployeeClass.DistinctFind(null, upload.CustomerId, upload.EmployerId).ToList();

                var allContactTypes = new Dictionary<string, ContactType>();
                ContactType.AsQueryable()
                    .Where(e => (e.CustomerId == null || e.CustomerId == upload.CustomerId) && (e.EmployerId == null || e.EmployerId == upload.EmployerId))
                    .ToList()
                    .OrderBy(a => !string.IsNullOrWhiteSpace(a.EmployerId) ? 0 : !string.IsNullOrWhiteSpace(a.CustomerId) ? 1 : 2)
                    .ForEach(t =>
                    {
                        if (allContactTypes.ContainsKey(t.Code))
                            return;
                        allContactTypes.Add(t.Code, t);
                    });

                var query = Employee.Query.And(Employee.Query.EQ(e => e.CustomerId, upload.CustomerId), Employee.Query.EQ(e => e.EmployerId, upload.EmployerId));
                var allEmployees = Employee.Repository.Collection.Find(query)
                // see: http://stackoverflow.com/questions/14053803/mongodb-c-sharp-driver-cursor-not-found
                  .SetFlags(QueryFlags.NoCursorTimeout).ToList();
                var paySchedules = PaySchedule.AsQueryable()
                 .Where(s => s.CustomerId == upload.CustomerId && s.EmployerId == upload.EmployerId && s.Name != null)
                 .ToDictionary(s => s.Name, s => s.Id);

                // Build a normalization function to ensure we're grouping by rational employee numbers
                Func<string, string> normalize = new Func<string, string>(s =>
                {
                    if (string.IsNullOrWhiteSpace(s))
                        return null;
                    string data = s;
                    data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
                    data = data.Replace("\"\"", "\"");
                    data = data.Replace("\\n", Environment.NewLine);
                    if (string.IsNullOrWhiteSpace(data))
                        return null;
                    return data;
                });

                if (isCancelled())
                    return;

                JobService jobService = new JobService(upload.CustomerId, upload.EmployerId, upload.CreatedBy);

                // Now we need to group our rows by Employee Number and start going to work on each.
                // This WILL spike CPU utilization, and should really be run on dedicated machines, so yeah, fair warning
                foreach (var rGroup in uploadItems.Where(u => u.RowParts.Length > 1).GroupBy(u => normalize(u.RowParts[1])).ToList().Batch(3000).ToList())
                {
                    #region Foreach Batch

                    if (isCancelled())
                        return;

                    var userId = upload.CreatedById ?? User.DefaultUserId;

                    var bulkEmployee = Employee.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkEmployeeContact = EmployeeContact.Repository.Collection.InitializeOrderedBulkOperation();
                    var bulkCase = Case.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkVariableTime = VariableScheduleTime.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkToDo = ToDoItem.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkEmployeeContactCleanup = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkOrganization = Organization.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkEmployeeOrganization = EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkJob = Job.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkEmployeeJob = EmployeeJob.Repository.Collection.InitializeUnorderedBulkOperation();
                    int bulkEmployeeOps = 0;
                    int bulkEmployeeContactOps = 0;
                    int bulkCaseOps = 0;
                    int bulkVariableTimeOps = 0;
                    int bulkToDoOps = 0;
                    int bulkEmployeeContactCleanupOps = 0;
                    int bulkJobOps = 0;
                    int bulkOrgOps = 0;
                    int bulkEmpOrgOps = 0;

                    long additionalErrors = 0;
                    long adjustedCount = 0;

                    foreach (var r in rGroup)
                    {
                        #region Foreach Employee Number

                        // Do the actual translation/copy in groups to minimize memory impact perhaps
                        List<EligibilityRow> rows = r.Select(u =>
                        {
                            var row = new EligibilityRow()
                            {
                                RowNumber = u.RowNumber,
                                RowResult = u,
                                CustomerId = upload.CustomerId,
                                EmployerId = upload.EmployerId,
                                EmployeeClassTypes = employeeClassTypes
                            };
                            row.Parse(u.RowParts, employersCustomFields);

                            if (!string.IsNullOrWhiteSpace(row.EmployerReferenceCode) && !row.EmployerReferenceCode.Equals(refCode, StringComparison.InvariantCultureIgnoreCase))
                                row.AddError("'{0}' is not a valid Employer Reference Code. This employer expects a reference code of '{1}' and no other employer matches '{0}'.", row.EmployerReferenceCode, refCode ?? "<none>");
                            if (!string.IsNullOrWhiteSpace(row.Country))
                            {
                                var countryReturned = allCountries.Any(x => x.Code == row.Country.ToUpperInvariant());
                                if (!countryReturned)
                                    row.AddError("Residence Country '{0}' is not a valid 2-character ISO standard country code.", row.Country);
                            }
                            if (!string.IsNullOrWhiteSpace(row.WorkCountry))
                            {
                                var countryReturned = allCountries.Any(x => x.Code == row.WorkCountry.ToUpperInvariant());
                                if (!countryReturned)
                                    row.AddError("Work Country '{0}' is not a valid 2-character ISO standard country code.", row.WorkCountry);
                            }

                            return row;
                        }).ToList();

                        // Let's get all of our errors out of the way first, so we're not dealing with them later on.
                        List<EligibilityRow> errors = rows.Where(row => row.IsError).ToList();

                        if (errors.Any())
                        {
                            long errCount = errors.LongCount();
                            upload.Processed += errCount;
                            upload.Errors += errCount;

                            var bulkErrors = EligibilityUploadResult.Repository.Collection.InitializeUnorderedBulkOperation();
                            foreach (var e in errors)
                            {
                                rows.Remove(e);
                                var result = e.RowResult;
                                result.Error = e.Error;
                                bulkErrors.Insert(result);
                            }
                            bulkErrors.Execute();

                            // Update our upload with the error count
                            upload.Save();
                        }
                        errors.Clear();
                        errors = null;

                        List<string> contactTypeCodes = new List<string>();

                        bool isNew = false,
                            updateEE = false,
                            reCalcOpenCases = false,
                             updateCases = false,
                             failAll = false,
                            updateCasesOfficeLocation = false;
                        Employee emp = allEmployees.FirstOrDefault(e => e.EmployeeNumber == r.Key);
                        if (emp == null)
                        {
                            if (!rows.Any(x => x.RecType == EligibilityRow.RecordType.Basic || x.RecType == EligibilityRow.RecordType.Eligibility))
                            {
                                rows.ForEach(x => x.AddError("Cannot load a record of type '{0}' when the employee does not already exist or is not defined in the file", x.RecType));
                                failAll = true;
                            }
                            else
                            {
                                emp = new Employee()
                                {
                                    Id = ObjectId.GenerateNewId().ToString(),
                                    CustomerId = upload.CustomerId,
                                    EmployerId = upload.EmployerId,

                                    CreatedById = upload.CreatedById,
                                    EmployeeNumber = r.Key,
                                    CustomFields = employersCustomFields.Clone(),
                                    EmployerName = upload.Employer.Name
                                }.SetCreatedDate(DateTime.UtcNow);
                                isNew = updateEE = true;
                            }
                        }
                        else
                        {
                            query = Employee.Query.And(Employee.Query.EQ(e => e.CustomerId, upload.CustomerId), Employee.Query.EQ(e => e.EmployerId, upload.EmployerId), Employee.Query.EQ(e => e.EmployeeNumber, r.Key));
                            emp = Employee.Repository.Collection.Find(query).FirstOrDefault();
                            if (emp.IsDeleted)
                            {
                                emp.IsDeleted = false;
                                updateEE = true;
                            }

                        }

                        // Get all cases, except cancelled ones
                        List<Case> cases = failAll || isNew || emp == null || string.IsNullOrWhiteSpace(emp.Id)
                           ? new List<Case>(0)
                           : Case.Query.Find(
                               Case.Query.And(
                                    Case.Query.And(
                                    Case.Query.EQ(e => e.CustomerId, upload.CustomerId),
                                    Case.Query.EQ(e => e.EmployerId, upload.EmployerId),
                                    Case.Query.EQ(e => e.Employee.Id, emp.Id),
                                    Case.Query.NE(e => e.Status, CaseStatus.Cancelled)))).ToList();


                        // Create a master List<string> to store case numbers for this employee(or each employee number group / batch).
                        List<string> caseMasterList = new List<string>();

                        foreach (var caseNumber in rows.Where(rw => !string.IsNullOrWhiteSpace(rw.CaseNumber)).Select(rw => rw.CaseNumber).Distinct().ToList())
                        {
                           caseMasterList.AddIfNotExists(caseNumber);
                        }

                        if (!failAll)
                        {
                            foreach (var row in rows.OrderBy(u =>
                            {
                                switch (u.RecType)
                                {
                                    case EligibilityRow.RecordType.Eligibility: return 1M;
                                    case EligibilityRow.RecordType.Basic: return 2M;
                                    case EligibilityRow.RecordType.Spouse: return 3M;
                                    case EligibilityRow.RecordType.Schedule: return 3.5M;
                                    case EligibilityRow.RecordType.TimeScheduled: return 4M;
                                    case EligibilityRow.RecordType.Supervisor: return 5M;
                                    case EligibilityRow.RecordType.HR: return 6M;
                                    case EligibilityRow.RecordType.Custom: return 7M;
                                    case EligibilityRow.RecordType.CustomField: return 7.5M;
                                    case EligibilityRow.RecordType.Intermittent: return 8M;
                                    case EligibilityRow.RecordType.Organization: return 9M;
                                    default: return 999M;
                                }
                            }).ToList())
                            {
                                rows = rows.OrderBy(t => t.EmployeeNumber).ThenBy(t => t.JobStartDate).ToList();
                                bool alreadyExists = false;
                                if (rows.Any(x => x.RecType == EligibilityRow.RecordType.Job))
                                {
                                    var newRow = rows.Where(i => i.JobCode == row.JobCode && i.RowResult.RowParts[i.RowResult.RowParts.Length - 1] == "1").ToList();
                                    if (newRow.Count > 0)
                                    {
                                        if (row.RowResult.RowParts[row.RowResult.RowParts.Length - 1] == "0")
                                        {
                                            var newEndDate = newRow[0].JobStartDate;
                                            row.JobEndDate = newEndDate.Value.AddDays(-1);
                                        }
                                        else
                                        {
                                            alreadyExists = true;
                                        }
                                    }
                                }

                                string lastRowKey = "LastERow";
                                switch (row.RecType)
                                {
                                    case EligibilityRow.RecordType.Eligibility:
                                        lastRowKey = "LastERow";
                                        break;
                                    case EligibilityRow.RecordType.Spouse:
                                        lastRowKey = "LastSRow";
                                        break;
                                    case EligibilityRow.RecordType.Basic:
                                        lastRowKey = "LastBRow";
                                        break;
                                    case EligibilityRow.RecordType.Supervisor:
                                        lastRowKey = "LastURow";
                                        break;
                                    case EligibilityRow.RecordType.HR:
                                        lastRowKey = "LastHRow";
                                        break;
                                    case EligibilityRow.RecordType.Custom:
                                        lastRowKey = "LastCRow";
                                        break;
                                    case EligibilityRow.RecordType.Schedule:
                                        lastRowKey = "LastWRow";
                                        break;
                                    case EligibilityRow.RecordType.Job:
                                        lastRowKey = "LastJRow";
                                        break;
                                    default:
                                        lastRowKey = null;
                                        break;
                                }
                                if (!string.IsNullOrWhiteSpace(lastRowKey) && row.Hash == emp.Metadata.GetRawValue<string>(lastRowKey))
                                    continue;

                                if (row.RecType == EligibilityRow.RecordType.TimeScheduled)
                                {
                                    #region TimeScheduled
                                    try
                                    {
                                        if (isNew)
                                        {
                                            if (row.TimeScheduled == null || row.TimeScheduled.Value <= 0)
                                                continue;

                                            var time = new VariableScheduleTime()
                                            {
                                                CreatedById = userId,
                                                ModifiedById = userId,
                                                CustomerId = emp.CustomerId,
                                                EmployerId = emp.EmployerId,
                                                EmployeeId = emp.Id,
                                                EmployeeNumber = emp.EmployeeNumber,
                                                Time = new Time()
                                                {
                                                    SampleDate = row.DateScheduled.Value,
                                                    TotalMinutes = row.TimeScheduled
                                                }
                                            };
                                            time.SetCreatedDate(DateTime.UtcNow);
                                            time.SetModifiedDate(DateTime.UtcNow);
                                            bulkVariableTime.Insert(time);
                                            bulkVariableTimeOps++;
                                        }
                                        else
                                        {
                                            var b = bulkVariableTime.Find(VariableScheduleTime.Query.And(
                                                VariableScheduleTime.Query.EQ(v => v.EmployeeId, emp.Id),
                                                VariableScheduleTime.Query.NotExists(v => v.EmployeeCaseId),
                                                VariableScheduleTime.Query.EQ(v => v.Time.SampleDate, row.DateScheduled.Value)
                                            ));
                                            if (row.TimeScheduled == null || row.TimeScheduled.Value <= 0)
                                                b.RemoveOne();
                                            else
                                                b.Upsert().UpdateOne(VariableScheduleTime.Updates
                                                    .Set(v => v.Time.TotalMinutes, row.TimeScheduled)
                                                    .Set(v => v.ModifiedById, userId)
                                                    .Set(v => v.ModifiedDate, DateTime.UtcNow)
                                                    .SetOnInsert(v => v.CreatedById, userId)
                                                    .SetOnInsert(v => v.CreatedDate, DateTime.UtcNow)
                                                    .SetOnInsert(v => v.CustomerId, emp.CustomerId)
                                                    .SetOnInsert(v => v.EmployerId, emp.EmployerId)
                                                    .SetOnInsert(v => v.EmployeeNumber, emp.EmployeeNumber)
                                                    .SetOnInsert(v => v.EmployeeId, emp.Id)
                                                    .SetOnInsert(v => v.Time.SampleDate, row.DateScheduled.Value)
                                                    .SetOnInsert(v => v.Time.Id, Guid.NewGuid())
                                                );
                                            bulkVariableTimeOps++;
                                            reCalcOpenCases = true;
                                        }

                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Time Scheduled row", ex);
                                        row.AddError("Error processing Time Scheduled row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion TimeScheduled
                                }

                                // Set the last row key now
                                if (!string.IsNullOrWhiteSpace(lastRowKey))
                                {
                                    emp.Metadata.SetRawValue(lastRowKey, row.Hash);
                                    updateEE = true;
                                }

                                if (row.RecType == EligibilityRow.RecordType.Supervisor || (row.RecType == EligibilityRow.RecordType.Eligibility &&
                                    (!string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerLastName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerPhone) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerEmail))) ||
                                        (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber)))
                                {
                                    #region Supervisor
                                    if (!string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerLastName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerPhone) ||
                                        !string.IsNullOrWhiteSpace(row.ManagerEmail))
                                    {
                                        try
                                        {
                                            // Only try to match up the Supervisor/Manager IF no name is passed in
                                            if (!string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber) && string.IsNullOrWhiteSpace(row.ManagerLastName) && string.IsNullOrWhiteSpace(row.ManagerFirstName))
                                            {
                                                var cEmp = allEmployees.FirstOrDefault(e => e.EmployeeNumber == row.ManagerEmployeeNumber);
                                                if (cEmp == null)
                                                {
                                                    var rEmp = rows.FirstOrDefault(e => e.EmployeeNumber == row.ManagerEmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                                                    if (rEmp != null)
                                                    {
                                                        row.ManagerLastName = rEmp.LastName ?? row.ManagerLastName;
                                                        row.ManagerFirstName = rEmp.FirstName ?? row.ManagerFirstName;
                                                        row.ManagerPhone = rEmp.PhoneWork ?? row.ManagerPhone;
                                                        row.ManagerEmail = StringUtils.LowercaseFirstNotNullString(rEmp.Email, row.ManagerEmail);
                                                    }
                                                    else
                                                    {
                                                        row.ManagerLastName = row.ManagerLastName ?? row.ManagerEmployeeNumber;
                                                        row.ManagerFirstName = row.ManagerFirstName ?? "Supervisor";
                                                    }
                                                }
                                                else
                                                {
                                                    row.ManagerLastName = cEmp.LastName ?? row.ManagerLastName;
                                                    row.ManagerFirstName = cEmp.FirstName ?? row.ManagerFirstName;
                                                    row.ManagerPhone = cEmp.Info.WorkPhone ?? row.ManagerPhone;
                                                    row.ManagerEmail = StringUtils.LowercaseFirstNotNullString(cEmp.Info.Email, row.ManagerEmail);
                                                }
                                            }

                                            if (isNew)
                                            {
                                                bulkEmployeeContact.Insert(new EmployeeContact()
                                                {
                                                    CustomerId = emp.CustomerId,
                                                    EmployerId = emp.EmployerId,
                                                    EmployeeId = emp.Id,
                                                    EmployeeNumber = emp.EmployeeNumber,
                                                    MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                                                    CreatedById = userId,
                                                    ModifiedById = userId,
                                                    ContactTypeCode = "SUPERVISOR",
                                                    ContactTypeName = "Supervisor",
                                                    Contact = new Contact()
                                                    {
                                                        LastName = row.ManagerLastName,
                                                        FirstName = row.ManagerFirstName,
                                                        WorkPhone = row.ManagerPhone,
                                                        Email = row.ManagerEmail == null ? null : row.ManagerEmail.ToLowerInvariant()
                                                    },
                                                    RelatedEmployeeNumber = row.ManagerEmployeeNumber
                                                }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                                            }
                                            else
                                            {
                                                bulkEmployeeContact.Find(EmployeeContact.Query.And(
                                                    EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                    EmployeeContact.Query.EQ(e => e.ContactTypeCode, "SUPERVISOR")
                                                ))
                                                .Upsert()
                                                .UpdateOne(EmployeeContact.Updates
                                                    .Set(c => c.ModifiedById, userId)
                                                    .Set(c => c.ModifiedDate, DateTime.UtcNow)
                                                    .Set(c => c.RelatedEmployeeNumber, row.ManagerEmployeeNumber)
                                                    .Set(c => c.Contact.LastName, row.ManagerLastName)
                                                    .Set(c => c.Contact.FirstName, row.ManagerFirstName)
                                                    .Set(c => c.Contact.WorkPhone, row.ManagerPhone)
                                                    .Set(c => c.Contact.Email, row.ManagerEmail == null ? null : row.ManagerEmail.ToLowerInvariant())
                                                    .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                                    .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                                    .SetOnInsert(c => c.EmployeeId, emp.Id)
                                                    .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                                    .SetOnInsert(c => c.CreatedById, userId)
                                                    .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                                    .SetOnInsert(c => c.ContactTypeCode, "SUPERVISOR")
                                                    .SetOnInsert(c => c.ContactTypeName, "Supervisor")
                                                    .SetOnInsert(c => c.MilitaryStatus, AbsenceSoft.Data.Enums.MilitaryStatus.Civilian)
                                                    .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Country, "US")
                                                    .Combine(Update.Unset("Source"))
                                                );
                                            }
                                            bulkEmployeeContactOps++;
                                            contactTypeCodes.Add("SUPERVISOR");
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Error("Error processing Manager/Supervisor row", ex);
                                            row.AddError("Error processing Manager/Supervisor row, {0}", ex.Message);
                                            if (row.RecType == EligibilityRow.RecordType.Eligibility)
                                                updateEE = false;
                                            continue;
                                        }
                                    }

                                    #endregion Supervisor
                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.ManagerEmployeeNumber))
                                        {
                                            #region Basic

                                            try
                                            {
                                                UpdateBasicRecordType(userId, bulkOrganization, bulkEmployeeOrganization, isNew, ref updateEE, ref reCalcOpenCases, emp, row, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Error("Error processing Basic row", ex);
                                                row.AddError("Error processing Basic row, {0}", ex.Message);
                                                updateEE = false;
                                                continue;
                                            }

                                            #endregion
                                        }
                                        continue;
                                    }
                                }

                                if (row.RecType == EligibilityRow.RecordType.HR || (row.RecType == EligibilityRow.RecordType.Eligibility &&
                                    (!string.IsNullOrWhiteSpace(row.HREmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.HRLastName) ||
                                        !string.IsNullOrWhiteSpace(row.HRFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.HRPhone) ||
                                        !string.IsNullOrWhiteSpace(row.HREmail))) ||
                                        (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.HREmployeeNumber)))
                                {
                                    #region HR
                                    if (!string.IsNullOrWhiteSpace(row.HREmployeeNumber) ||
                                        !string.IsNullOrWhiteSpace(row.HRLastName) ||
                                        !string.IsNullOrWhiteSpace(row.HRFirstName) ||
                                        !string.IsNullOrWhiteSpace(row.HRPhone) ||
                                        !string.IsNullOrWhiteSpace(row.HREmail))
                                    {
                                        try
                                        {
                                            // Only try to match up the HR peep IF no name is passed in
                                            if (!string.IsNullOrWhiteSpace(row.HREmployeeNumber) && string.IsNullOrWhiteSpace(row.HRLastName) && string.IsNullOrWhiteSpace(row.HRLastName))
                                            {
                                                var cEmp = allEmployees.FirstOrDefault(e => e.EmployeeNumber == row.HREmployeeNumber);
                                                if (cEmp == null)
                                                {
                                                    var rEmp = rows.FirstOrDefault(e => e.EmployeeNumber == row.HREmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                                                    if (rEmp != null)
                                                    {
                                                        row.HRLastName = rEmp.LastName ?? row.HRLastName;
                                                        row.HRFirstName = rEmp.FirstName ?? row.HRFirstName;
                                                        row.HRPhone = rEmp.PhoneWork ?? row.HRPhone;
                                                        row.HREmail = StringUtils.LowercaseFirstNotNullString(rEmp.Email, row.HREmail);
                                                    }
                                                    else
                                                    {
                                                        row.HRLastName = row.HRLastName ?? row.HREmployeeNumber;
                                                        row.HRFirstName = row.HRFirstName ?? "HR";
                                                    }
                                                }
                                                else
                                                {
                                                    row.HRLastName = cEmp.LastName ?? row.HRLastName;
                                                    row.HRFirstName = cEmp.FirstName ?? row.HRFirstName;
                                                    row.HRPhone = cEmp.Info.WorkPhone ?? row.HRPhone;
                                                    row.HREmail = StringUtils.LowercaseFirstNotNullString(cEmp.Info.Email, row.HREmail);
                                                }
                                            }

                                            if (isNew)
                                            {
                                                bulkEmployeeContact.Insert(new EmployeeContact()
                                                {
                                                    CustomerId = emp.CustomerId,
                                                    EmployerId = emp.EmployerId,
                                                    EmployeeId = emp.Id,
                                                    EmployeeNumber = emp.EmployeeNumber,
                                                    MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                                                    CreatedById = userId,
                                                    ModifiedById = userId,
                                                    ContactTypeCode = "HR",
                                                    ContactTypeName = "HR",
                                                    Contact = new Contact()
                                                    {
                                                        LastName = row.HRLastName,
                                                        FirstName = row.HRFirstName,
                                                        WorkPhone = row.HRPhone,
                                                        Email = row.HREmail == null ? null : row.HREmail.ToLowerInvariant()
                                                    },
                                                    RelatedEmployeeNumber = row.HREmployeeNumber
                                                }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                                            }
                                            else
                                            {
                                                bulkEmployeeContact.Find(EmployeeContact.Query.And(
                                                    EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                    EmployeeContact.Query.EQ(e => e.ContactTypeCode, "HR")
                                                ))
                                                .Upsert()
                                                .UpdateOne(EmployeeContact.Updates
                                                    .Set(c => c.ModifiedById, userId)
                                                    .Set(c => c.ModifiedDate, DateTime.UtcNow)
                                                    .Set(c => c.RelatedEmployeeNumber, row.HREmployeeNumber)
                                                    .Set(c => c.Contact.LastName, row.HRLastName)
                                                    .Set(c => c.Contact.FirstName, row.HRFirstName)
                                                    .Set(c => c.Contact.WorkPhone, row.HRPhone)
                                                    .Set(c => c.Contact.Email, row.HREmail == null ? null : row.HREmail.ToLowerInvariant())
                                                    .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                                    .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                                    .SetOnInsert(c => c.EmployeeId, emp.Id)
                                                    .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                                    .SetOnInsert(c => c.CreatedById, userId)
                                                    .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                                    .SetOnInsert(c => c.ContactTypeCode, "HR")
                                                    .SetOnInsert(c => c.ContactTypeName, "HR")
                                                    .SetOnInsert(c => c.MilitaryStatus, AbsenceSoft.Data.Enums.MilitaryStatus.Civilian)
                                                    .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Country, "US")
                                                    .Combine(Update.Unset("Source"))
                                                );
                                            }
                                            bulkEmployeeContactOps++;
                                            contactTypeCodes.Add("HR");
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Error("Error processing HR row", ex);
                                            row.AddError("Error processing HR row, {0}", ex.Message);
                                            if (row.RecType == EligibilityRow.RecordType.Eligibility)
                                                updateEE = false;
                                            continue;
                                        }
                                    }
                                    #endregion HR
                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.HREmployeeNumber))
                                        {
                                            #region Basic

                                            try
                                            {
                                                UpdateBasicRecordType(userId, bulkOrganization, bulkEmployeeOrganization, isNew, ref updateEE, ref reCalcOpenCases, emp, row, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Error("Error processing Basic row", ex);
                                                row.AddError("Error processing Basic row, {0}", ex.Message);
                                                updateEE = false;
                                                continue;
                                            }

                                            #endregion
                                        }
                                        continue;
                                    }
                                }

                                if (row.RecType == EligibilityRow.RecordType.Spouse || (row.RecType == EligibilityRow.RecordType.Eligibility && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber))
                                        || (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber)))
                                {
                                    #region Spouse
                                    try
                                    {
                                        // Deal with the Spouse
                                        if (row.SpouseEmployeeNumber != emp.SpouseEmployeeNumber)
                                        {
                                            emp.SpouseEmployeeNumber = row.SpouseEmployeeNumber;
                                            updateEE = true;

                                            string sFirst = null, sLast = null;
                                            MilitaryStatus sMil = Data.Enums.MilitaryStatus.Civilian;

                                            var cEmp = allEmployees.FirstOrDefault(e => e.EmployeeNumber == row.SpouseEmployeeNumber);
                                            if (cEmp == null)
                                            {
                                                var rEmp = rows.FirstOrDefault(e => e.EmployeeNumber == row.SpouseEmployeeNumber && (e.RecType == EligibilityRow.RecordType.Eligibility || e.RecType == EligibilityRow.RecordType.Basic));
                                                if (rEmp != null)
                                                {
                                                    sLast = rEmp.LastName;
                                                    sFirst = rEmp.FirstName;
                                                    if (rEmp.MilitaryStatus.HasValue)
                                                        sMil = (MilitaryStatus)rEmp.MilitaryStatus.Value;
                                                }
                                            }
                                            else
                                            {
                                                sLast = cEmp.LastName;
                                                sFirst = cEmp.FirstName;
                                                sMil = cEmp.MilitaryStatus;
                                            }

                                            sFirst = sFirst ?? "Spouse";
                                            sLast = sLast ?? row.SpouseEmployeeNumber;

                                            if (isNew)
                                            {
                                                bulkEmployeeContact.Insert(new EmployeeContact()
                                                {
                                                    CustomerId = emp.CustomerId,
                                                    EmployerId = emp.EmployerId,
                                                    EmployeeId = emp.Id,
                                                    EmployeeNumber = emp.EmployeeNumber,
                                                    CreatedById = userId,
                                                    ModifiedById = userId,
                                                    ContactTypeCode = "SPOUSE",
                                                    ContactTypeName = "Spouse",
                                                    Contact = new Contact()
                                                    {
                                                        LastName = sLast,
                                                        FirstName = sFirst
                                                    },
                                                    RelatedEmployeeNumber = row.SpouseEmployeeNumber,
                                                    MilitaryStatus = sMil
                                                }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                                            }
                                            else
                                            {
                                                bulkEmployeeContact.Find(EmployeeContact.Query.And(
                                                    EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                    EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                    EmployeeContact.Query.EQ(e => e.ContactTypeCode, "SPOUSE")
                                                ))
                                                .Upsert()
                                                .UpdateOne(EmployeeContact.Updates
                                                    .Set(c => c.ModifiedById, userId)
                                                    .Set(c => c.ModifiedDate, DateTime.UtcNow)
                                                    .Set(c => c.RelatedEmployeeNumber, row.SpouseEmployeeNumber)
                                                    .Set(c => c.Contact.LastName, sLast)
                                                    .Set(c => c.Contact.FirstName, sFirst)
                                                    .Set(c => c.MilitaryStatus, sMil)
                                                    .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                                    .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                                    .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                                    .SetOnInsert(c => c.EmployeeId, emp.Id)
                                                    .SetOnInsert(c => c.CreatedById, userId)
                                                    .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                                    .SetOnInsert(c => c.ContactTypeCode, "SPOUSE")
                                                    .SetOnInsert(c => c.ContactTypeName, "Spouse")
                                                    .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                                    .SetOnInsert(c => c.Contact.Address.Country, "US")
                                                );
                                            }
                                            bulkEmployeeContactOps++;
                                            contactTypeCodes.Add("SPOUSE");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Eligibility row", ex);
                                        row.AddError("Error processing Eligibility row, {0}", ex.Message);
                                        if (row.RecType == EligibilityRow.RecordType.Eligibility)
                                            updateEE = false;
                                        continue;
                                    }
                                    #endregion Spouse
                                    if (row.RecType != EligibilityRow.RecordType.Eligibility)
                                    {
                                        if (row.RecType == EligibilityRow.RecordType.Basic && !string.IsNullOrWhiteSpace(row.SpouseEmployeeNumber))
                                        {
                                            #region Basic

                                            try
                                            {
                                                UpdateBasicRecordType(userId, bulkOrganization, bulkEmployeeOrganization, isNew, ref updateEE, ref reCalcOpenCases, emp, row, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Error("Error processing Basic row", ex);
                                                row.AddError("Error processing Basic row, {0}", ex.Message);
                                                updateEE = false;
                                                continue;
                                            }

                                            #endregion
                                        }
                                        continue;
                                    }
                                }

                                if (row.RecType == EligibilityRow.RecordType.Contact)
                                {
                                    #region Contact

                                    try
                                    {
                                        if (!allContactTypes.ContainsKey(row.ContactTypeCode))
                                        {
                                            row.AddError("The contact type of '{0}' is not valid or is not configured for this customer/employer.", row.ContactTypeCode);
                                            continue;
                                        }

                                        var ct = allContactTypes[row.ContactTypeCode];
                                        string email = string.IsNullOrWhiteSpace(row.Email) ? null : row.Email.ToLowerInvariant();
                                        List<IMongoQuery> matchOR = new List<IMongoQuery>();
                                        if (!string.IsNullOrWhiteSpace(row.ContactEmployeeNumber))
                                            matchOR.Add(EmployeeContact.Query.EQ(e => e.RelatedEmployeeNumber, row.ContactEmployeeNumber));
                                        if (!string.IsNullOrWhiteSpace(email))
                                            matchOR.Add(EmployeeContact.Query.EQ(e => e.Contact.Email, email));
                                        else if (string.IsNullOrWhiteSpace(row.ContactEmployeeNumber) && !string.IsNullOrWhiteSpace(row.FirstName) || !string.IsNullOrWhiteSpace(row.LastName))
                                            matchOR.Add(Query.And(EmployeeContact.Query.EQ(e => e.Contact.FirstName, row.FirstName), EmployeeContact.Query.EQ(e => e.Contact.LastName, row.LastName)));

                                        if (row.ContactExclusive ?? false)
                                        {
                                            var q = EmployeeContact.Query.And(
                                                EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                EmployeeContact.Query.EQ(e => e.ContactTypeCode, row.ContactTypeCode)
                                            );
                                            bulkEmployeeContact.Find(q).Update(EmployeeContact.Updates
                                                .Set(c => c.ModifiedById, userId)
                                                .CurrentDate(c => c.ModifiedDate)
                                                .Set(c => c.IsDeleted, true)
                                                .Combine(Update
                                                .Set("EL", upload.FileName)
                                                .Set("ELPart", "ContactExclusive")
                                                .Set("ELPartQuery", q.ToString())
                                                .Unset("Source")));
                                        }
                                        if (row.ContactPosition.HasValue && matchOR.Any())
                                        {
                                            var q = EmployeeContact.Query.And(
                                                EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                EmployeeContact.Query.EQ(e => e.ContactTypeCode, row.ContactTypeCode),
                                                Query.Or(matchOR),
                                                EmployeeContact.Query.NE(e => e.ContactPosition, row.ContactPosition)
                                            );
                                            bulkEmployeeContact.Find(q).Update(EmployeeContact.Updates
                                                .Set(c => c.ModifiedById, userId)
                                                .CurrentDate(c => c.ModifiedDate)
                                                .Set(c => c.IsDeleted, true)
                                                .Combine(Update
                                                .Set("EL", upload.FileName)
                                                .Set("ELPart", "ContactPosition")
                                                .Set("ELPartQuery", q.ToString())
                                                .Unset("Source")));
                                        }

                                        if (isNew || row.ClearOtherContacts)
                                        {
                                            var empContact = new EmployeeContact()
                                            {
                                                CustomerId = emp.CustomerId,
                                                EmployerId = emp.EmployerId,
                                                EmployeeId = emp.Id,
                                                EmployeeNumber = emp.EmployeeNumber,
                                                MilitaryStatus = ((MilitaryStatus?)row.MilitaryStatus) ?? MilitaryStatus.Civilian,
                                                CreatedById = userId,
                                                ModifiedById = userId,
                                                ContactTypeCode = row.ContactTypeCode,
                                                ContactTypeName = row.ContactTypeName ?? ct.Name ?? row.ContactTypeCode,
                                                Contact = new Contact()
                                                {
                                                    LastName = row.LastName,
                                                    FirstName = row.FirstName,
                                                    WorkPhone = row.PhoneWork,
                                                    CellPhone = row.PhoneMobile,
                                                    HomePhone = row.PhoneHome,
                                                    Fax = row.PhoneFax,
                                                    Email = email,
                                                    AltEmail = string.IsNullOrWhiteSpace(row.EmailAlt) ? null : row.EmailAlt.ToLowerInvariant(),
                                                    Address = new Address()
                                                    {
                                                        Address1 = row.Address,
                                                        Address2 = row.Address2,
                                                        City = row.City,
                                                        State = row.State,
                                                        Country = row.Country ?? "US",
                                                        PostalCode = row.PostalCode
                                                    }
                                                },
                                                RelatedEmployeeNumber = row.ContactEmployeeNumber,
                                                ContactPosition = row.ContactPosition
                                            }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                                            empContact.Metadata.SetRawValue("EL", upload.FileName);
                                            empContact.Metadata.SetRawValue("ELPart", "Insert");
                                            bulkEmployeeContact.Insert(empContact);
                                        }
                                        else
                                        {
                                            var q = EmployeeContact.Query.And(
                                                EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                                EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                                EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                                EmployeeContact.Query.EQ(e => e.ContactTypeCode, row.ContactTypeCode),
                                                !(row.ContactExclusive ?? false) && row.ContactPosition.HasValue ?
                                                    EmployeeContact.Query.EQ(e => e.ContactPosition, row.ContactPosition) :
                                                    Query.Or(matchOR)
                                            );
                                            bulkEmployeeContact.Find(q)
                                            .Upsert()
                                            .UpdateOne(EmployeeContact.Updates
                                                .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                                .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                                .SetOnInsert(c => c.EmployeeId, emp.Id)
                                                .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                                .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                                .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                                .SetOnInsert(c => c.CreatedById, userId)
                                                .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                                .Set(c => c.ModifiedById, userId)
                                                .CurrentDate(c => c.ModifiedDate)
                                                .Set(c => c.RelatedEmployeeNumber, row.ContactEmployeeNumber)
                                                .Set(c => c.Contact.LastName, row.LastName)
                                                .Set(c => c.Contact.FirstName, row.FirstName)
                                                .Set(c => c.Contact.WorkPhone, row.PhoneWork)
                                                .Set(c => c.Contact.HomePhone, row.PhoneHome)
                                                .Set(c => c.Contact.Fax, row.PhoneFax)
                                                .Set(c => c.Contact.CellPhone, row.PhoneMobile)
                                                .Set(c => c.Contact.Email, email)
                                                .Set(c => c.Contact.AltEmail, row.EmailAlt?.ToLowerInvariant())
                                                .Set(c => c.ContactTypeCode, row.ContactTypeCode)
                                                .Set(c => c.ContactTypeName, row.ContactTypeName ?? ct.Name ?? row.ContactTypeCode)
                                                .Set(c => c.MilitaryStatus, ((MilitaryStatus?)row.MilitaryStatus) ?? MilitaryStatus.Civilian)
                                                .Set(c => c.Contact.Address.Address1, row.Address)
                                                .Set(c => c.Contact.Address.Address2, row.Address2)
                                                .Set(c => c.Contact.Address.City, row.City)
                                                .Set(c => c.Contact.Address.State, row.State)
                                                .Set(c => c.Contact.Address.Country, row.Country ?? "US")
                                                .Set(c => c.Contact.Address.PostalCode, row.PostalCode)
                                                .Set(c => c.ContactPosition, row.ContactPosition)
                                                .Set(c => c.IsDeleted, false)
                                                .Combine(Update
                                                    .Set("EL", upload.FileName)
                                                    .Set("ELPart", "Upsert")
                                                    .Set("ELPartQuery", q.ToString())
                                                    .Unset("Source"))
                                            );
                                        }
                                        bulkEmployeeContactOps++;
                                        contactTypeCodes.Add(row.ContactTypeCode);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(string.Format("Error processing '{0}' employee contact row", row.ContactTypeCode), ex);
                                        row.AddError("Error processing '{0}' employee contact row, {1}", row.ContactTypeCode, ex.Message);
                                    }
                                    continue;

                                    #endregion Contact
                                }

                                if (row.RecType == EligibilityRow.RecordType.Custom)
                                {
                                    #region Custom
                                    if (row.CustomFields == null || row.CustomFields.Length == 0)
                                    {
                                        row.AddError("This row contains custom fields which are not configured for the employer.");
                                        continue;
                                    }
                                    try
                                    {
                                        if (emp.CustomFields == null)
                                            emp.CustomFields = row.CustomFields.ToList();
                                        else if (!emp.CustomFields.Any())
                                            emp.CustomFields.AddRange(row.CustomFields);
                                        else
                                        {
                                            foreach (var c in row.CustomFields)
                                            {
                                                var m = emp.CustomFields.FirstOrDefault(x => x.Name == c.Name || x.Id == c.Id);
                                                if (m == null)
                                                    emp.CustomFields.AddFluid(c).ModifiedById = userId;
                                                else
                                                {
                                                    m.DataType = c.DataType;
                                                    m.Description = c.Description;
                                                    m.FileOrder = c.FileOrder;
                                                    m.HelpText = c.HelpText;
                                                    m.IsRequired = c.IsRequired;
                                                    m.Label = c.Label;
                                                    m.ListValues = c.ListValues;
                                                    m.ValueType = c.ValueType;
                                                    m.SelectedValue = c.SelectedValue;
                                                    m.ModifiedById = userId;
                                                }
                                            }
                                        }
                                        updateEE = true;

                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Custom Fields row", ex);
                                        row.AddError("Error processing Custom Fields row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion Custom
                                }

                                if (row.RecType == EligibilityRow.RecordType.Schedule)
                                {
                                    #region Schedule
                                    try
                                    {
                                        if (row.HoursWorkedIn12Months.HasValue)
                                        {
                                            emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
                                            emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)row.HoursWorkedIn12Months.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                                            updateEE = true;
                                        }

                                        if (row.AverageMinutesWorkedPerWeek.HasValue)
                                        {
                                            emp.MinutesWorkedPerWeek = emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                                            emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = row.AverageMinutesWorkedPerWeek.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                                            updateEE = true;
                                        }


                                        Schedule workSched = new Schedule() { ScheduleType = row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                                        workSched.StartDate = (row.ScheduleEffectiveDate ?? (isNew && emp.HireDate.HasValue ? emp.HireDate.Value : DateTime.UtcNow)).ToMidnight();

                                        if (!row.WorkTimeSun.HasValue &&
                                            !row.WorkTimeMon.HasValue &&
                                            !row.WorkTimeTue.HasValue &&
                                            !row.WorkTimeWed.HasValue &&
                                            !row.WorkTimeThu.HasValue &&
                                            !row.WorkTimeFri.HasValue &&
                                            !row.WorkTimeSat.HasValue)
                                        {
                                            if (!row.MinutesPerWeek.HasValue)
                                                workSched = null;
                                            else
                                            {
                                                DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                                                int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)row.MinutesPerWeek.Value / 5m));
                                                int leftOverMinute = row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                                                for (var d = 0; d < 7; d++)
                                                {
                                                    if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                                        workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                                                    else
                                                        workSched.Times.Add(new Time() { SampleDate = sample });
                                                    sample = sample.AddDays(1);
                                                }
                                                // Add our leftover minute to the first date, clugey, but it's whatever.
                                                workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                                            }
                                        }
                                        else
                                        {
                                            DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                                            for (var d = 0; d < 7; d++)
                                            {
                                                int? workTime = null;
                                                switch (sample.DayOfWeek)
                                                {
                                                    case DayOfWeek.Sunday: workTime = row.WorkTimeSun; break;
                                                    case DayOfWeek.Monday: workTime = row.WorkTimeMon; break;
                                                    case DayOfWeek.Tuesday: workTime = row.WorkTimeTue; break;
                                                    case DayOfWeek.Wednesday: workTime = row.WorkTimeWed; break;
                                                    case DayOfWeek.Thursday: workTime = row.WorkTimeThu; break;
                                                    case DayOfWeek.Friday: workTime = row.WorkTimeFri; break;
                                                    case DayOfWeek.Saturday: workTime = row.WorkTimeSat; break;
                                                }
                                                workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                                                sample = sample.AddDays(1);
                                            }
                                        }

                                        bool lastScheduleUserEntered = false;
                                        if (!isNew && workSched != null)
                                        {
                                            Schedule dup = emp.WorkSchedules == null ? null : emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                                            if (dup != null && dup.Metadata.GetRawValue<bool?>("SystemEntered") == true)
                                                lastScheduleUserEntered = true;
                                            if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                                            {
                                                // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                                                bool isMatch = true;
                                                foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                                                    if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                                                    {
                                                        isMatch = false;
                                                        break;
                                                    }
                                                // See if the schedules still look the same
                                                if (isMatch)
                                                    workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                                            }
                                        }

                                        if (workSched != null && (!(row.NoOverwriteFlag == true) || lastScheduleUserEntered))
                                        {
                                            workSched.Metadata.SetRawValue("SystemEntered", true);
                                            new EmployeeService().Using(s => s.SetWorkSchedule(emp, workSched, null));
                                            updateEE = true;

                                            foreach (var caseNumber in cases.Where(rw => rw.Status == CaseStatus.Open).Select(rw => rw.CaseNumber).Distinct().ToList())
                                            {
                                                caseMasterList.AddIfNotExists(caseNumber);
                                            }
                                            reCalcOpenCases = true;
                                        }

                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Work Schedule row", ex);
                                        row.AddError("Error processing Work Schedule row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion
                                }

                                if (row.RecType == EligibilityRow.RecordType.Basic)
                                {
                                    #region Basic

                                    try
                                    {
                                        UpdateBasicRecordType(userId, bulkOrganization, bulkEmployeeOrganization, isNew, ref updateEE, ref reCalcOpenCases, emp, row, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);

                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Basic row", ex);
                                        row.AddError("Error processing Basic row, {0}", ex.Message);
                                        updateEE = false;
                                        continue;
                                    }

                                    #endregion
                                }

                                if (row.RecType == EligibilityRow.RecordType.Intermittent)
                                {
                                    #region Intermittent
                                    try
                                    {
                                        var myCase = cases.FirstOrDefault(c => c.CaseNumber == row.CaseNumber && c.Status != CaseStatus.Closed);
                                        if (myCase == null)
                                        {
                                            row.AddError("A case with number '{0}' was not found for the employee with number {1}", row.CaseNumber, row.EmployeeNumber);
                                            continue;
                                        }
                                        using (CaseService svc = new CaseService(row.Workflow == true))
                                        {
                                            string pId = null;
                                            if (!string.IsNullOrWhiteSpace(row.PolicyCode))
                                            {
                                                var policy = Policy.GetByCode(row.PolicyCode, myCase.CustomerId, myCase.EmployerId);
                                                if (policy == null)
                                                {
                                                    row.AddError("The policy code '{0}' supplied for intermittent time on  '{2:MM/dd/yyyy}' for case '{1}' was not found in the system. Please ensure this policy is properly configured.", row.PolicyCode, myCase.CaseNumber, row.DateOfAbsence);
                                                    continue;
                                                }
                                                pId = policy.Code;
                                            }
                                            var intSeg = myCase.Segments.FirstOrDefault(s => s.Type == CaseType.Intermittent && row.DateOfAbsence.Value.DateInRange(s.StartDate, s.EndDate));
                                            if (intSeg == null)
                                            {
                                                row.AddError("No intermittent period for this case was found for date '{0:MM/dd/yyyy}'.", row.DateOfAbsence);
                                                continue;
                                            }
                                            List<IntermittentTimeRequest> times = new List<IntermittentTimeRequest>();
                                            IntermittentTimeRequest t = null;
                                            if (intSeg.UserRequests != null)
                                                t = intSeg.UserRequests.FirstOrDefault(q => q.RequestDate == row.DateOfAbsence);
                                            if (t == null)
                                                t = new IntermittentTimeRequest()
                                                {
                                                    EmployeeEntered = false,
                                                    RequestDate = row.DateOfAbsence.Value,
                                                    Notes = "Created through Case Migration"
                                                };
                                            times.Add(t);
                                            t.TotalMinutes = (row.MinutesApproved ?? 0) + (row.MinutesDenied ?? 0) + (row.MinutesPending ?? 0);
                                            if (!string.IsNullOrWhiteSpace(pId) && !intSeg.AppliedPolicies.Any(p => p.Policy.Code == pId))
                                            {
                                                row.AddError("The policy '{0}' was not found covering any intermittent period of time for the case and may not be used.", row.PolicyCode);
                                                continue;
                                            }
                                            List<IntermittentTimeRequestDetail> detail = new List<IntermittentTimeRequestDetail>();
                                            detail.AddRange(intSeg.AppliedPolicies.Where(p => (pId == null || pId == p.Policy.Code) && p.Status == EligibilityStatus.Eligible)
                                                .Select(p => new IntermittentTimeRequestDetail()
                                                {
                                                    PolicyCode = p.Policy.Code,
                                                    Approved = row.MinutesApproved ?? 0,
                                                    Denied = row.MinutesDenied ?? 0,
                                                    Pending = row.MinutesPending ?? 0,
                                                    DenialReasonCode = row.MinutesDenied.HasValue && row.MinutesDenied > 0 ? AdjudicationDenialReason.Other : AdjudicationDenialReason.Exhausted,
                                                    DenialReasonName = row.MinutesDenied.HasValue && row.MinutesDenied > 0 ? AdjudicationDenialReason.OtherDescription : AdjudicationDenialReason.ExhaustedDescription,
                                                    DeniedReasonOther = row.MinutesDenied.HasValue && row.MinutesDenied > 0 ? "Denied through Case Migration" : null,
                                                    TotalMinutes = (row.MinutesApproved ?? 0) + (row.MinutesDenied ?? 0) + (row.MinutesPending ?? 0)
                                                }));

                                            foreach (var dt in detail)
                                            {
                                                var deet = t.Detail.FirstOrDefault(d => d.PolicyCode == dt.PolicyCode);
                                                if (deet == null)
                                                    t.Detail.Add(dt);
                                                else
                                                {
                                                    deet.Approved = dt.Approved;
                                                    deet.Denied = dt.Denied;
                                                    deet.DenialReasonCode = dt.DenialReasonCode;
                                                    deet.DenialReasonName = dt.DenialReasonName;
                                                    deet.DeniedReasonOther = dt.DeniedReasonOther;
                                                    deet.Pending = dt.Pending;
                                                    deet.TotalMinutes = dt.TotalMinutes;
                                                }
                                            }

                                            myCase = svc.CreateOrModifyTimeOffRequest(myCase, times);                                         
                                            reCalcOpenCases = true;
                                            continue;
                                        }
                                    }
                                    catch (AbsenceSoftAggregateException aggEx)
                                    {
                                        Log.Error("Error processing Intermittent row", aggEx);
                                        row.AddError(string.Join("; ", aggEx.Messages));
                                        continue;
                                    }
                                    catch (AbsenceSoftException abEx)
                                    {
                                        Log.Error("Error processing Intermittent row", abEx);
                                        row.AddError(abEx.Message);
                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Intermittent row", ex);
                                        row.AddError("Error processing Intermittent row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion
                                }

                                if (row.RecType == EligibilityRow.RecordType.Job)
                                {
                                    #region Job
                                    try
                                    {
                                        // Upsert this job
                                        jobService.UpsertJob(bulkJob, row.JobCode, row.JobName);

                                        // Upsert the employee job
                                        jobService.UpsertEmployeeJob(bulkEmployeeJob, row.EmployeeNumber, row.JobCode, row.JobName, row.JobTitle, row.JobStartDate ?? DateTime.UtcNow.ToMidnight(), row.JobEndDate, row.JobLocation, row.JobPosition, row.JobStatus, alreadyExists);
                                        bulkJobOps++;
                                        continue;
                                    }
                                    catch (AbsenceSoftAggregateException aggEx)
                                    {
                                        Log.Error("Error processing Job row", aggEx);
                                        row.AddError(string.Join("; ", aggEx.Messages));
                                        continue;
                                    }
                                    catch (AbsenceSoftException abEx)
                                    {
                                        Log.Error("Error processing Job row", abEx);
                                        row.AddError(abEx.Message);
                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Job row", ex);
                                        row.AddError("Error processing Job row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion Job
                                }

                                if (row.RecType == EligibilityRow.RecordType.Organization)
                                {
                                    #region Employee Organization
                                    try
                                    {
                                        UpdateEmployeeOrganization(emp, row, bulkOrganization, bulkEmployeeOrganization, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
                                        continue;
                                    }
                                    catch (AbsenceSoftAggregateException aggEx)
                                    {
                                        Log.Error("Error processing Employee Organization row", aggEx);
                                        row.AddError(string.Join("; ", aggEx.Messages));
                                        continue;
                                    }
                                    catch (AbsenceSoftException abEx)
                                    {
                                        Log.Error("Error processing Employee Organization row", abEx);
                                        row.AddError(abEx.Message);
                                        continue;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Error processing Employee Organization row", ex);
                                        row.AddError("Error processing Employee Organization row, {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion Employee Organization
                                }

                                if (row.RecType == EligibilityRow.RecordType.CustomField)
                                {
                                    // Hard work has already been done by .Parse
                                    // Just set that we need to update the EE and continue on to the next row
                                    if (string.IsNullOrWhiteSpace(row.CaseNumber) && row.Target == EntityTarget.Case)
                                    {
                                        updateCases = true;
                                        var allCasesForEmployee = cases.AsQueryable().Where(c => c.Employee.EmployeeNumber == row.EmployeeNumber);
                                        foreach (var myCase in allCasesForEmployee)
                                        {
                                            UpdateCaseCustomFields(myCase, row);
                                        }
                                    }
                                    else if (!string.IsNullOrWhiteSpace(row.CaseNumber))
                                    {
                                        updateCases = true;
                                        var myCase = cases.FirstOrDefault(c => c.CaseNumber == row.CaseNumber && c.Employee.EmployeeNumber == row.EmployeeNumber);
                                        if (myCase == null && row.Target == EntityTarget.Case)
                                        {
                                            row.AddError("A case with number '{0}' was not found for the employee with number {1}", row.CaseNumber, row.EmployeeNumber);
                                            break;
                                        }
                                        UpdateCaseCustomFields(myCase, row);
                                    }                                  

                                    updateEE = true;
                                    continue;
                                }

                                #region Eligibility

                                // Now process the row as an eligibility row
                                try
                                {
                                    if (isNew) updateEE = true;

                                    // Setup
                                    emp.Info = emp.Info ?? new EmployeeInfo();
                                    emp.Info.Address = emp.Info.Address ?? new Address();

                                    // Set Basic Properties (These are the easy ones)
                                    emp.ModifiedById = userId;
                                    emp.SetModifiedDate(DateTime.UtcNow);
                                    emp.LastName = row.LastName;
                                    emp.FirstName = row.FirstName;
                                    emp.MiddleName = row.MiddleName;
                                    emp.JobTitle = row.JobTitle;
                                    emp.WorkState = row.WorkState;
                                    emp.WorkCountry = row.WorkCountry ?? "US";
                                    emp.WorkCounty = row.WorkCounty;
                                    emp.ResidenceCounty = row.ResidenceCounty;
                                    emp.WorkCity = row.WorkCity;
                                    emp.Info.HomePhone = row.PhoneHome;
                                    emp.Info.WorkPhone = row.PhoneWork;
                                    emp.Info.CellPhone = row.PhoneMobile;
                                    emp.Info.AltPhone = row.PhoneAlt;
                                    emp.Info.Email = StringUtils.LowercaseFirstNotNullString(row.Email);
                                    emp.Info.AltEmail = StringUtils.LowercaseFirstNotNullString(row.EmailAlt);
                                    emp.Info.Address.Address1 = row.Address;
                                    emp.Info.Address.Address2 = row.Address2;
                                    emp.Info.Address.City = row.City;
                                    emp.Info.Address.State = row.State;
                                    emp.Info.Address.PostalCode = row.PostalCode;
                                    emp.Info.Address.Country = row.Country;
                                    emp.EmployeeClassCode = row.EmploymentType;
                                    emp.EmployeeClassName = row.EmploymentTypeName;
                                    emp.DoB = row.DateOfBirth;
                                    emp.Gender = (Gender?)row.Gender;
                                    emp.IsExempt = row.ExemptionStatus == 'E';
                                    emp.Meets50In75MileRule = row.Meets50In75 ?? false;
                                    emp.IsKeyEmployee = row.KeyEmployee ?? false;
                                    emp.MilitaryStatus = (MilitaryStatus)(row.MilitaryStatus ?? 0);
                                    emp.TerminationDate = row.TerminationDate;
                                    emp.Salary = (double?)row.PayRate;
                                    emp.PayType = (PayType?)row.PayType;
                                    emp.HireDate = row.HireDate;
                                    emp.RehireDate = row.RehireDate;
                                    emp.ServiceDate = row.AdjustedServiceDate;
                                    emp.Department = row.Department;
                                    emp.CostCenterCode = row.CostCenterCode;
                                    emp.IsFlightCrew = row.AirlineFlightCrew;
                                    if (!string.IsNullOrWhiteSpace(row.SSN) && !SSNExists(row.SSN, emp.Id))
                                    {
                                        emp.Ssn = new CryptoString(row.SSN).Hash().Encrypt();
                                    }
                                    if (row.JobClassification.HasValue)
                                        emp.JobActivity = (JobClassification)row.JobClassification.Value;
                                    if (!string.IsNullOrWhiteSpace(row.PayScheduleName) && paySchedules.ContainsKey(row.PayScheduleName))
                                        emp.PayScheduleId = paySchedules[row.PayScheduleName];
                                    emp.StartDayOfWeek = row.StartDayOfWeek;

                                    // Create TODO items for any open cases if this employee is being marked as terminated.
                                    if (!isNew && emp.Status != Data.Enums.EmploymentStatus.Terminated && row.EmploymentStatus == EmploymentStatusValue.Terminated)
                                    {
                                        emp.WfOnEmployeeTerminated();

                                        // Only do this for cases that are NOT cancelled or closed (like open, requested, inquiry, etc.)
                                        foreach (var c in cases.Where(c => c.Status != CaseStatus.Closed && c.Status != CaseStatus.Cancelled))
                                        {
                                            bulkToDo.Insert(new ToDoItem()
                                            {
                                                Title = "Employee Terminated; Review Open Case",
                                                CreatedById = userId,
                                                ModifiedById = userId,
                                                CustomerId = emp.CustomerId,
                                                EmployerId = emp.EmployerId,
                                                AssignedToId = c.AssignedToId,
                                                CaseId = c.Id,
                                                EmployeeId = emp.Id,
                                                EmployeeNumber = emp.EmployeeNumber,
                                                EmployeeName = emp.FullName,
                                                CaseNumber = c.CaseNumber,
                                                DueDate = DateTime.UtcNow.AddDays(1),
                                                ItemType = ToDoItemType.CaseReview,
                                                Optional = false,
                                                Priority = ToDoItemPriority.High,
                                                Status = ToDoItemStatus.Pending,
                                                Weight = 0,
                                                AssignedToName = c.AssignedToName,
                                                EmployerName = c.EmployerName
                                            }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                                            bulkToDoOps++;
                                        }
                                    }
                                    emp.Status = (EmploymentStatus)(row.EmploymentStatus ?? EmploymentStatusValue.Active);

                                    if (row.HoursWorkedIn12Months.HasValue)
                                    {
                                        emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
                                        emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)row.HoursWorkedIn12Months.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                                    }

                                    if (row.AverageMinutesWorkedPerWeek.HasValue)
                                    {
                                        emp.MinutesWorkedPerWeek = emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                                        emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = row.AverageMinutesWorkedPerWeek.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                                    }

                                    Schedule workSched = new Schedule() { ScheduleType = row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                                    workSched.StartDate = (row.ScheduleEffectiveDate ?? (isNew && emp.HireDate.HasValue ? emp.HireDate.Value : DateTime.UtcNow)).ToMidnight();

                                    if (!row.WorkTimeSun.HasValue &&
                                        !row.WorkTimeMon.HasValue &&
                                        !row.WorkTimeTue.HasValue &&
                                        !row.WorkTimeWed.HasValue &&
                                        !row.WorkTimeThu.HasValue &&
                                        !row.WorkTimeFri.HasValue &&
                                        !row.WorkTimeSat.HasValue)
                                    {
                                        if (!row.MinutesPerWeek.HasValue)
                                            workSched = null;
                                        else
                                        {
                                            DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                                            int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)row.MinutesPerWeek.Value / 5m));
                                            int leftOverMinute = row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                                            for (var d = 0; d < 7; d++)
                                            {
                                                if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                                    workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                                                else
                                                    workSched.Times.Add(new Time() { SampleDate = sample });
                                                sample = sample.AddDays(1);
                                            }
                                            // Add our leftover minute to the first date, clugey, but it's whatever.
                                            workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                                        }
                                    }
                                    else
                                    {
                                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                                        for (var d = 0; d < 7; d++)
                                        {
                                            int? workTime = null;
                                            switch (sample.DayOfWeek)
                                            {
                                                case DayOfWeek.Sunday: workTime = row.WorkTimeSun; break;
                                                case DayOfWeek.Monday: workTime = row.WorkTimeMon; break;
                                                case DayOfWeek.Tuesday: workTime = row.WorkTimeTue; break;
                                                case DayOfWeek.Wednesday: workTime = row.WorkTimeWed; break;
                                                case DayOfWeek.Thursday: workTime = row.WorkTimeThu; break;
                                                case DayOfWeek.Friday: workTime = row.WorkTimeFri; break;
                                                case DayOfWeek.Saturday: workTime = row.WorkTimeSat; break;
                                            }
                                            workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                                            sample = sample.AddDays(1);
                                        }
                                    }

                                    if (!isNew && workSched != null)
                                    {
                                        Schedule dup = emp.WorkSchedules == null ? null : emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                                        if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                                        {
                                            // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                                            bool isMatch = true;
                                            foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                                                if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                                                {
                                                    isMatch = false;
                                                    break;
                                                }
                                            // See if the schedules still look the same
                                            if (isMatch)
                                                workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                                        }
                                    }

                                    if (workSched != null)
                                    {
                                        reCalcOpenCases = true;
                                        workSched.Metadata.SetRawValue("SystemEntered", true);
                                        new EmployeeService().Using(s => s.SetWorkSchedule(emp, workSched, null));
                                    }

                                    if (!string.IsNullOrEmpty(row.JobLocation))
                                    {
                                        UpdateOrganization(emp, row.JobLocation, bulkOrganization, bulkEmployeeOrganization, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
                                    }

                                    updateEE = true;
                                    continue;
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("Error processing Eligibility row", ex);
                                    row.AddError("Error processing Eligibility row, {0}", ex.Message);
                                    updateEE = false;
                                    continue;
                                }
                                #endregion Eligibility
                            } // foreach Row
                        } //if not failed

                        try
                        {
                           
                            if (!failAll && cases.Any(c => c.Status != CaseStatus.Closed))
                            {
                                foreach (var c in cases.Where(c => c.Status != CaseStatus.Closed))
                                {
                                    if (reCalcOpenCases)
                                    {
                                        if (c.Segments != null && c.Segments.Any())
                                        {
                                            var oldExhuastDate = c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.FirstExhaustionDate.HasValue).Any() ?
                                                c.Segments.SelectMany(s => s.AppliedPolicies).Where(p => p.FirstExhaustionDate.HasValue).Min(p => p.FirstExhaustionDate) : null;

                                            var handlers = new CaseChangeHandlers(CasePropertyChanged.MinimumLeaveExhaustionDate, (changedCase, o) =>
                                            {
                                                if (changedCase.GetMinLeaveExhaustionDate().HasValue)
                                                {
                                                    var now = DateTime.UtcNow;
                                                    bulkToDo.Insert(new Tasks.ToDoService().MakeLeaveExhaustedTodo(
                                                        @case: changedCase,
                                                        modifiedById: userId,
                                                        createdById: userId,
                                                        modified: now,
                                                        created: now
                                                    ));
                                                    bulkToDoOps++;
                                                }
                                            });

                                            new CaseService().Using(s => s.RunCalcs(c, handlers));
                                            updateCases = true;
                                        }
                                    }
                                    if (updateCasesOfficeLocation && emp != null && emp.Info != null && !string.IsNullOrWhiteSpace(emp.Info.OfficeLocation))
                                    {
                                        c.CurrentOfficeLocation = emp.Info.OfficeLocation;
                                        updateCases = true;
                                    }
                                }
                            }

                            if (!failAll && updateCases)
                            {
                                foreach (var c in cases)
                                {
                                    // 8.Only Save() the cases who's case number exists in that list
                                    if (caseMasterList.Contains(c.CaseNumber))
                                    {
                                        c.ModifiedById = userId;
                                        c.SetModifiedDate(DateTime.UtcNow);
                                        bulkCase.Find(Case.Query.EQ(x => x.Id, c.Id)).ReplaceOne(c);
                                        bulkCaseOps++;
                                    }
                                }
                            }
                            //If reCalcOpenCases is true and while executing runcalcs any issue occurs
                            //it will throw an exception , and we don't want to update an employee record if runcalcs fails
                            if (!failAll && updateEE)
                            {
                                emp.SetModifiedDate(DateTime.UtcNow);
                                emp.ModifiedById = userId;
                                emp.CustomFields = UpdateEmployeeCustomFields(emp, rows);


                                if (isNew)
                                    bulkEmployee.Insert(emp);
                                else
                                    bulkEmployee.Find(Employee.Query.EQ(e => e.Id, emp.Id)).ReplaceOne(emp);

                                bulkEmployeeOps++;
                            }

                            if (!failAll && !isNew && rows.Any(x => x.ClearOtherContacts))
                            {
                                #region Clean Up Contacts
                                var codes = contactTypeCodes.Where(c => !string.IsNullOrWhiteSpace(c)).Distinct();

                                // HACK: This is hard-coded for Amazon, DO NOT remove this and DO NOT copy this logic/approach anywhere else in the code-base
                                if (emp.EmployerId == "546e52cda32aa00f78086269")
                                    codes = codes.Where(c => !"SUPERVISOR".Equals(c, StringComparison.InvariantCultureIgnoreCase) && !"WC".Equals(c, StringComparison.InvariantCultureIgnoreCase)).ToList();
                                bulkEmployeeContactCleanup.Find(Query.And(
                                        EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                        EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                        EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                        EmployeeContact.Query.In(e => e.ContactTypeCode, codes)
                                )).Update(Update
                                    .Set("mby", userId)
                                    .CurrentDate("mdt")
                                    .Set("EL", upload.FileName)
                                    .Set("IsDeleted", true)
                                    .Unset("Source"));
                                bulkEmployeeContactCleanupOps++;
                                #endregion
                            }
                        }
                        catch (MongoBulkWriteException bulkEx)
                        {
                            var errorKey = Guid.NewGuid().ToString("D");
                            Log.Error(string.Format("{1}: Error processing employee '{0}'", emp.EmployeeNumber, errorKey), bulkEx);
                            foreach (var row in rows)
                                foreach (var err in bulkEx.WriteErrors)
                                    row.AddError("{1}: Service exception processing employee: {0}", err.Message, errorKey);
                        }
                        catch (Exception ex)
                        {
                            var errorKey = Guid.NewGuid().ToString("D");
                            Log.Error(string.Format("{1}: Error processing employee '{0}'", emp.EmployeeNumber, errorKey), ex);
                            foreach (var row in rows)
                                row.AddError("{0}: Service exception processing employee", errorKey);
                        }

                        #endregion

                        additionalErrors += rows.LongCount(x => x.IsError);
                        adjustedCount += rows.LongCount();

                        rows.Clear();
                        rows = null;

                    } //foreach: Employee Number

                    if (isCancelled())
                        return;

                    try
                    {
                        if (bulkEmployeeOps > 0)
                            bulkEmployee.Execute();
                        if (bulkEmployeeContactCleanupOps > 0)
                            bulkEmployeeContactCleanup.Execute();
                        if (bulkEmployeeContactOps > 0)
                            bulkEmployeeContact.Execute();
                        if (bulkVariableTimeOps > 0)
                            bulkVariableTime.Execute();
                        if (bulkCaseOps > 0)
                            bulkCase.Execute();
                        if (bulkToDoOps > 0)
                            bulkToDo.Execute();
                        if (bulkOrgOps > 0)
                            bulkOrganization.Execute();
                        if (bulkEmpOrgOps > 0)
                            bulkEmployeeOrganization.Execute();
                        if (bulkJobOps > 0)
                        {
                            bulkJob.Execute();
                            bulkEmployeeJob.Execute();
                        }
                    }
                    catch (MongoBulkWriteException bulkEx)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error processing employee batch", errorKey), bulkEx);
                        foreach (var row in rGroup.SelectMany(g => g))
                            foreach (var err in bulkEx.WriteErrors)
                                row.Error = string.Format("{0}{1}{2}: Service exception processing employee: {3}",
                                    row.Error,
                                    string.IsNullOrWhiteSpace(row.Error) ? null : Environment.NewLine,
                                    errorKey, err.Message);
                    }
                    catch (Exception ex)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error processing employee batch", errorKey), ex);
                        foreach (var row in rGroup.SelectMany(g => g))
                            row.Error = string.Format("{0}{1}{2}: Service exception processing employee",
                                row.Error,
                                    string.IsNullOrWhiteSpace(row.Error) ? null : Environment.NewLine,
                                    errorKey);
                    }
                    try
                    {
                        var bulkResults = EligibilityUploadResult.Repository.Collection.InitializeUnorderedBulkOperation();
                        bool hasResults = false;
                        rGroup.SelectMany(g => g.Where(r => string.IsNullOrWhiteSpace(r.Id))).ForEach(e =>
                        {
                            if (!e.IsError)
                                e.RowSource = null;
                            hasResults = true;
                            bulkResults.Insert(e);
                        });
                        if (hasResults)
                            bulkResults.Execute();
                    }
                    catch (MongoBulkWriteException bulkEx)
                    {
                        var errorKey = Guid.NewGuid().ToString("D");
                        Log.Error(string.Format("{0}: Error recording row results for employee batch", errorKey), bulkEx);
                        foreach (var err in bulkEx.WriteErrors)
                            Log.Error(string.Format("{1}: Error recording row results for employee batch: {0}", err.Message, errorKey));
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error recording row results for employee batch", ex);
                    }
                    try
                    {
                        // Update our upload with the error count
                        upload.Errors += additionalErrors;
                        upload.Processed += adjustedCount;
                        upload.Save();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("Error setting final counts for upload id '{0}' for Employee batch", upload.Id), ex);
                    }

                    #endregion Foreach Batch
                } //foreach: Employee Batch

                // Cleanup
                allEmployees.Clear();
                allEmployees = null;
                employersCustomFields.Clear();
                employersCustomFields = null;
                jobService.Dispose();
                jobService = null;
                GC.Collect();

                if (isCancelled())
                    return;
            }
        }

        
        internal void UpdateBasicRecordType(string userId, BulkWriteOperation<Organization> bulkOrganization, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, bool isNew, ref bool updateEE, ref bool reCalcOpenCases, Employee emp, EligibilityRow row, ref int bulkOrgOps, ref int bulkEmpOrgOps, ref bool updateCasesOfficeLocation)
        {
            if (isNew) updateEE = true;

            // Setup
            emp.Info = emp.Info ?? new EmployeeInfo();
            emp.Info.Address = emp.Info.Address ?? new Address();

            // Set Basic Properties (These are the easy ones)
            emp.ModifiedById = userId;
            emp.SetModifiedDate(DateTime.UtcNow);
            emp.LastName = row.LastName;
            emp.FirstName = row.FirstName;
            emp.MiddleName = row.MiddleName;
            emp.WorkState = row.WorkState;
            emp.WorkCountry = emp.WorkCountry ?? "US";
            emp.WorkCounty = row.WorkCounty;
            emp.ResidenceCounty = row.ResidenceCounty;
            emp.WorkCity = row.WorkCity;
            emp.Info.HomePhone = row.PhoneHome;
            emp.Info.WorkPhone = row.PhoneWork;
            emp.Info.CellPhone = row.PhoneMobile;
            emp.Info.Email = StringUtils.LowercaseFirstNotNullString(row.Email);
            emp.Info.Address.Address1 = row.Address;
            emp.Info.Address.Address2 = row.Address2;
            emp.Info.Address.City = row.City;
            emp.Info.Address.State = row.State;
            emp.Info.Address.PostalCode = row.PostalCode;
            emp.Info.Address.Country = emp.Info.Address.Country ?? "US";
            emp.Gender = (Gender?)row.Gender;
            emp.ServiceDate = row.AdjustedServiceDate;
            emp.IsFlightCrew = row.AirlineFlightCrew;
            if (row.HoursWorkedIn12Months.HasValue)
            {
                emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
                emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)row.HoursWorkedIn12Months.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
            }
            
            if (row.AverageMinutesWorkedPerWeek.HasValue)
            {
                emp.MinutesWorkedPerWeek = emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = row.AverageMinutesWorkedPerWeek.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
            }
            Schedule workSched = new Schedule() { ScheduleType = row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
            workSched.StartDate = (row.ScheduleEffectiveDate ?? (isNew ? (emp.ServiceDate ?? emp.HireDate ?? DateTime.UtcNow) : DateTime.UtcNow)).ToMidnight();

            if (row.FtePercentage.HasValue)
            {
                EmployerId = emp.EmployerId;
                workSched.FteMinutesPerWeek = (int)Math.Round(((decimal)(CurrentEmployer.FTWeeklyWorkHours ?? 0) * (row.FtePercentage.Value / 100)) * 60);
                workSched.FteTimePercentage = row.FtePercentage.Value;
                workSched.ScheduleType = ScheduleType.FteVariable;
                workSched.FteWeeklyDuration = FteWeeklyDuration.FtePercentage;
            }
            else if (row.AverageMinutesWorkedPerWeek.HasValue && workSched.ScheduleType == ScheduleType.Variable)
            {
                workSched.FteMinutesPerWeek = row.AverageMinutesWorkedPerWeek.Value;
                workSched.ScheduleType = ScheduleType.FteVariable;
                workSched.FteWeeklyDuration = FteWeeklyDuration.FteTimePerWeek;
            }
            else if (!row.WorkTimeSun.HasValue &&
                !row.WorkTimeMon.HasValue &&
                !row.WorkTimeTue.HasValue &&
                !row.WorkTimeWed.HasValue &&
                !row.WorkTimeThu.HasValue &&
                !row.WorkTimeFri.HasValue &&
                !row.WorkTimeSat.HasValue)
            {
                if (!row.MinutesPerWeek.HasValue)
                    workSched = null;
                else
                {
                    DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                    int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)row.MinutesPerWeek.Value / 5m));
                    int leftOverMinute = row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                    for (var d = 0; d < 7; d++)
                    {
                        if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                            workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                        else
                            workSched.Times.Add(new Time() { SampleDate = sample });
                        sample = sample.AddDays(1);
                    }
                    // Add our leftover minute to the first date, clugey, but it's whatever.
                    workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                }
            }
            else
            {
                DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                for (var d = 0; d < 7; d++)
                {
                    int? workTime = null;
                    switch (sample.DayOfWeek)
                    {
                        case DayOfWeek.Sunday: workTime = row.WorkTimeSun; break;
                        case DayOfWeek.Monday: workTime = row.WorkTimeMon; break;
                        case DayOfWeek.Tuesday: workTime = row.WorkTimeTue; break;
                        case DayOfWeek.Wednesday: workTime = row.WorkTimeWed; break;
                        case DayOfWeek.Thursday: workTime = row.WorkTimeThu; break;
                        case DayOfWeek.Friday: workTime = row.WorkTimeFri; break;
                        case DayOfWeek.Saturday: workTime = row.WorkTimeSat; break;
                    }
                    workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                    sample = sample.AddDays(1);
                }
            }

            bool lastScheduleUserEntered = false;
            if (!isNew && workSched != null)
            {
                Schedule dup = emp.WorkSchedules == null ? null : emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                if (dup != null && dup.Metadata.GetRawValue<bool?>("SystemEntered") == true)
                    lastScheduleUserEntered = true;
                if (dup != null && dup.ScheduleType == ScheduleType.FteVariable && workSched.ScheduleType == ScheduleType.FteVariable)
                {
                    if (dup.FteMinutesPerWeek == workSched.FteMinutesPerWeek)
                    {
                        workSched = null;
                    }
                }
                else if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                {
                    // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                    bool isMatch = true;
                    foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                        if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                        {
                            isMatch = false;
                            break;
                        }
                    // See if the schedules still look the same
                    if (isMatch)
                        workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                }
            }

            if (workSched != null && (!(row.NoOverwriteFlag == true) || lastScheduleUserEntered))
            {
                reCalcOpenCases = true;
                workSched.Metadata.SetRawValue("SystemEntered", true);
                new EmployeeService().Using(s => s.SetWorkSchedule(emp, workSched, null));
            }

            if (!string.IsNullOrEmpty(row.JobLocation))
            {
                UpdateOrganization(emp, row.JobLocation, bulkOrganization, bulkEmployeeOrganization, ref bulkOrgOps, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
            }
            updateEE = true;
        }

        internal List<CustomField> UpdateEmployeeCustomFields(Employee emp, List<EligibilityRow> rows)
        {
            var rowsToUpdate = rows.Where(er => er.RecType == EligibilityRow.RecordType.CustomField && er.CustomFields.Count() > 0 && er.CaseNumber == null)
                .SelectMany(r => r.CustomFields).ToList();
            var customFieldsNotChanged = emp.CustomFields.Where(cf => !rowsToUpdate.Any(r => r.Code == cf.Code)).ToList();
            customFieldsNotChanged.AddRange(rowsToUpdate);
            return customFieldsNotChanged;
        }

        internal void UpdateOrganization(Employee emp, string officeLocation, BulkWriteOperation<Organization> bulkOrganization, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, ref int bulkOrgOps, ref int bulkEmpOrgOps, ref bool updateCasesOfficeLocation)
        {
            string code = officeLocation.ToUpperInvariant();
            bulkOrganization.Find(Organization.Query.And(
                Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
                Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
                Organization.Query.EQ(o => o.Code, code)
            )).Upsert().UpdateOne(
                Organization.Updates
                    .SetOnInsert(o => o.CustomerId, emp.CustomerId)
                    .SetOnInsert(o => o.EmployerId, emp.EmployerId)
                    .SetOnInsert(o => o.Code, code)
                    .SetOnInsert(o => o.Path, HierarchyPath.Root(code))
                    .SetOnInsert(o => o.TypeCode, OrganizationType.OfficeLocationTypeCode)
                    .SetOnInsert(o => o.TypeName, "Office Location")
                    .SetOnInsert(o => o.Name, officeLocation)
                    .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                    .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                    .SetOnInsert(o => o.ModifiedById, User.DefaultUserId)
                    .SetOnInsert(o => o.ModifiedDate, DateTime.UtcNow)
                    .SetOnInsert(o => o.Address.Id, Guid.NewGuid())
                    .Set(o => o.IsDeleted, false)
            );
            bulkOrgOps++;
            updateCasesOfficeLocation = true;
            UpdateOfficeLocation(emp, officeLocation, bulkEmployeeOrganization, ref bulkEmpOrgOps, ref updateCasesOfficeLocation);
        }

        private void UpdateOfficeLocation(Employee emp, string officeLocation, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, ref int bulkEmpOrgOps, ref bool updateCasesOfficeLocation)
        {
            string code = officeLocation.ToUpperInvariant();
            emp.Info.OfficeLocation = code;
            bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                )).Upsert().UpdateOne(
                    EmployeeOrganization.Updates
                    .SetOnInsert(eo => eo.CustomerId, emp.CustomerId)
                    .SetOnInsert(eo => eo.EmployerId, emp.EmployerId)
                    .SetOnInsert(eo => eo.EmployeeNumber, emp.EmployeeNumber)
                    .SetOnInsert(eo => eo.Code, code)
                    .SetOnInsert(eo => eo.TypeCode, OrganizationType.OfficeLocationTypeCode)
                    .SetOnInsert(eo => eo.Name, officeLocation)
                    .SetOnInsert(eo => eo.Path, HierarchyPath.Root(code))
                    .SetOnInsert(eo => eo.CreatedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.CreatedDate, DateTime.UtcNow)
                    .SetOnInsert(eo => eo.ModifiedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.ModifiedDate, DateTime.UtcNow)
                    .Set(eo => eo.Dates.StartDate, DateTime.UtcNow)
                    .Set(eo => eo.Dates.EndDate, null)
                );
            updateCasesOfficeLocation = true;
            bulkEmpOrgOps++;
            UpdateOfficeLocationEndDate(emp, officeLocation, bulkEmployeeOrganization);
            UpdateOfficeLocationIsDeleted(emp, officeLocation, bulkEmployeeOrganization);
        }

        internal void UpdateEmployeeOrganization(Employee emp, EligibilityRow row, BulkWriteOperation<Organization> bulkOrganization, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, ref int bulkOrgOps, ref int bulkEmpOrgOps, ref bool updateCasesOfficeLocation)
        {
            string code = row.OrgCode.ToUpperInvariant();
            if (!string.IsNullOrWhiteSpace(row.ParentOrgCode))
            {
                bulkOrganization.Find(Organization.Query.And(
                  Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
                  Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
                  Organization.Query.EQ(o => o.Code, row.ParentOrgCode)
              )).Upsert().UpdateOne(
                  Organization.Updates
                      .SetOnInsert(o => o.CustomerId, emp.CustomerId)
                      .SetOnInsert(o => o.EmployerId, emp.EmployerId)
                      .SetOnInsert(o => o.Code, row.ParentOrgCode)
                      .SetOnInsert(o => o.SicCode, row.SicCode)
                      .SetOnInsert(o => o.NaicsCode, row.NaicsCode)
                      .SetOnInsert(o => o.Address.Id, Guid.NewGuid())
                      .SetOnInsert(o => o.Address.Address1, row.OrgAddress1)
                      .SetOnInsert(o => o.Address.Address2, row.OrgAddress2)
                      .SetOnInsert(o => o.Address.City, row.OrgCity)
                      .SetOnInsert(o => o.Address.PostalCode, row.OrgPostalCode)
                      .SetOnInsert(o => o.Address.State, row.OrgState)
                      .SetOnInsert(o => o.Address.Country, row.OrgCountry)
                      .SetOnInsert(o => o.Path, HierarchyPath.Root(row.ParentOrgCode))
                      .SetOnInsert(o => o.TypeCode, (row.ParentOrgTypeCode ?? string.Empty) == OrganizationType.OfficeLocationTypeCode ? "Office Location" : null)
                      .SetOnInsert(o => o.TypeName, "Office Location")
                      .SetOnInsert(o => o.Name, row.ParentOrgName)
                      .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                      .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                      .Set(o => o.ModifiedById, User.DefaultUserId)
                      .Set(o => o.ModifiedDate, DateTime.UtcNow)
              );
                bulkOrgOps++;
            }

            bulkOrganization.Find(Organization.Query.And(
            Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
            Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
            Organization.Query.EQ(o => o.IsDeleted, false),
            Organization.Query.EQ(o => o.Code, code.Trim()))).Upsert().UpdateOne(
            Organization.Updates
                .SetOnInsert(o => o.CustomerId, emp.CustomerId)
                .SetOnInsert(o => o.EmployerId, emp.EmployerId)
                .SetOnInsert(o => o.Code, code)
                .Set(o => o.SicCode, row.SicCode)
                .Set(o => o.NaicsCode, row.NaicsCode)
                .SetOnInsert(o => o.Address.Id, Guid.NewGuid())
                .Set(o => o.Address.Address1, row.OrgAddress1)
                .Set(o => o.Address.Address2, row.OrgAddress2)
                .Set(o => o.Address.City, row.OrgCity)
                .Set(o => o.Address.PostalCode, row.OrgPostalCode)
                .Set(o => o.Address.State, row.OrgState)
                .Set(o => o.Address.Country, row.OrgCountry)
                .SetOnInsert(o => o.Path, row.ParentOrgCode == null ? HierarchyPath.Root(code) : HierarchyPath.Root(row.ParentOrgCode).AddDescendant(code))
                .SetOnInsert(o => o.TypeCode, string.IsNullOrEmpty(row.OrgTypeCode) ? OrganizationType.OfficeLocationTypeCode : row.OrgTypeCode)
                .SetOnInsert(o => o.TypeName, (row.OrgTypeCode ?? string.Empty) == OrganizationType.OfficeLocationTypeCode ? "Office Location" : null)
                .Set(o => o.Name, string.IsNullOrEmpty(row.OrgName) ? code : row.OrgName)
                .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                .Set(o => o.ModifiedById, User.DefaultUserId)
                .Set(o => o.ModifiedDate, DateTime.UtcNow));
            bulkOrgOps++;
            UpdateEmployeeOrganization(emp, row, bulkEmployeeOrganization, ref bulkEmpOrgOps);
            if (string.IsNullOrWhiteSpace(row.OrgTypeCode) || row.OrgTypeCode == OrganizationType.OfficeLocationTypeCode)
            {
                updateCasesOfficeLocation = true;
            }
        }
        internal void UpdateCaseCustomFields(Case myCase, EligibilityRow eRow)
        {
            foreach (var customField in eRow.CustomFields)
            {
                if (eRow.Target == EntityTarget.Case)
                {
                    int objIndex = myCase.CustomFields.FindIndex(cf => cf.Code == customField.Code);
                    if (objIndex != -1)
                    {
                        myCase.CustomFields[objIndex] = customField;
                    }
                    else
                    {
                        myCase.CustomFields.Add(customField);
                    }
                }
                else
                {
                    int objIndex = myCase.Employee.CustomFields.FindIndex(cf => cf.Code == customField.Code);
                    if (objIndex != -1)
                    {
                        myCase.Employee.CustomFields[objIndex] = customField;
                    }
                    else
                    {
                        myCase.Employee.CustomFields.Add(customField);
                    }
                }
            }
        }

        /// <summary>
        /// To check SSN exist for other employee
        /// </summary>
        /// <param name="SSN">SSN number</param>
        /// <param name="empId">Emp Id</param>
        /// <returns></returns>
        internal bool SSNExists(string SSN, string empId)
        {
            if (SSN != null)
            {
                var ssn = Employee.AsQueryable().Where(ca => ca.Ssn.Hashed == new CryptoString(SSN.Replace("-", "")).Hash().Hashed && ca.Id != empId).ToList();
                return ssn.Any();
            }
            return false;
        }

        private void UpdateEmployeeOrganization(Employee emp, EligibilityRow row, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, ref int bulkEmpOrgOps)
        {
            string code = row.OrgCode.ToUpperInvariant();
            #region AT-6690-Get Org type code from existing Org record
            var OrgTypeCode = row.OrgTypeCode;
            string OrgName = string.Empty;
            if (OrgTypeCode == null)
            {
                var query = Organization.Query.And(
                Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
                Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
                Organization.Query.EQ(o => o.IsDeleted, false),
                Organization.Query.EQ(o => o.Code, code.Trim()),
                Organization.Query.EQ(o => o.TypeCode, "OFFICELOCATION")
                );
                var allOrganizations = Organization.Repository.Collection.Find(query).FirstOrDefault();
                if (allOrganizations != null)
                {
                    OrgTypeCode = allOrganizations.TypeCode;
                    OrgName = allOrganizations.Name;
                }
            }
            #endregion

            bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                )).Upsert().UpdateOne(
                    EmployeeOrganization.Updates
                    .SetOnInsert(eo => eo.CustomerId, emp.CustomerId)
                    .SetOnInsert(eo => eo.EmployerId, emp.EmployerId)
                    .SetOnInsert(eo => eo.EmployeeNumber, emp.EmployeeNumber)
                    .SetOnInsert(eo => eo.Code, code)
                    .Set(eo => eo.TypeCode, OrgTypeCode)
                    .Set(eo => eo.Name, string.IsNullOrEmpty(row.OrgName) ? string.IsNullOrEmpty(OrgName) ? null : OrgName : row.OrgName)
                    .SetOnInsert(eo => eo.Path, row.ParentOrgCode == null ? HierarchyPath.Root(code) : HierarchyPath.Root(row.ParentOrgCode).AddDescendant(code))
                    .Set(eo => eo.Dates, row.OrgStartDate.HasValue ? new DateRange(row.OrgStartDate ?? DateTime.Now, row.OrgEndDate) : DateRange.Null)
                    .Set(eo => eo.Affiliation, row.OrgAffiliation == null ? OrganizationAffiliation.Member : (OrganizationAffiliation)row.OrgAffiliation)
                    .SetOnInsert(eo => eo.CreatedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.CreatedDate, DateTime.UtcNow)
                    .Set(eo => eo.ModifiedById, User.DefaultUserId)
                    .Set(eo => eo.ModifiedDate, DateTime.UtcNow)
                );
            bulkEmpOrgOps++;
        }
        /// <summary>
        /// AT-6271-To update the office location end date to one day before the upload date
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="officeLocation"></param>
        /// <param name="bulkEmployeeOrganization"></param>
        private void UpdateOfficeLocationEndDate(Employee emp, string officeLocation, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization)
        {
            string code = officeLocation.ToUpperInvariant();
            emp.Info.OfficeLocation = code;
            #region Update existing records
            //To update existing records that were inserted via EL upload, 
            //because start date and end date were not handled previously and were generating runtime error.
            //So updating start date as created date
            EmployeeOrganization.AsQueryable().Where(eo => eo.CustomerId == emp.CustomerId && eo.EmployerId == emp.EmployerId && eo.EmployeeNumber == emp.EmployeeNumber && eo.Dates == null && eo.Code != code).ToList().ForEach(eo =>
            {
                var createdDate = eo.CreatedDate;
                bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(e => e.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(e => e.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(e => e.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(e => e.Dates, null),
                EmployeeOrganization.Query.NE(e => e.Code, code)
                )).Update(
                    EmployeeOrganization.Updates
                    .SetOnInsert(e => e.ModifiedById, User.DefaultUserId)
                    .SetOnInsert(e => e.ModifiedDate, DateTime.UtcNow)
                    .Set(e => e.Dates.StartDate, createdDate)
                );
            });
            #endregion
            bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Dates.EndDate, null),
                EmployeeOrganization.Query.NE(eo => eo.Dates.StartDate, DateTime.UtcNow),
                EmployeeOrganization.Query.NE(eo => eo.Code, code)
                )).Update(
                    EmployeeOrganization.Updates
                    .SetOnInsert(eo => eo.ModifiedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.ModifiedDate, DateTime.UtcNow)
                    .Set(eo => eo.Dates.EndDate, DateTime.UtcNow.AddDays(-1))
                    .Set(e => e.IsDeleted, false)
                );
        }
        #endregion Process // ProcessRecords

        private void UpdateOfficeLocationIsDeleted(Employee emp, string officeLocation, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization)
        {
            string code = officeLocation.ToUpperInvariant();
            var q = bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                ));
            bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                )).Upsert().UpdateOne(
                    EmployeeOrganization.Updates
                    .Set(eo => eo.IsDeleted, false)
                );
        }        
    } // EligibilityUploadService
} // namespace
