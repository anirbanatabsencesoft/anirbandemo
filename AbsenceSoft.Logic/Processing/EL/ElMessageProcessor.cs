﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Logic.Common;
using System;
using System.Linq;

namespace AbsenceSoft.Logic.Processing.EL
{
    public class ElMessageProcessor : IMessageProcessor
    {
        private readonly EligibilityUploadService elUploadService = new EligibilityUploadService();

        public bool IsProcessorFor(QueueItem item)
        {
            return item.GetMessageType() == MessageType.EligibililtyLoad;
        }

        public void BeforeProcess(QueueItem item, MessageProcessingContext context)
        {
            // Do a quick check to determine if our file is actually processing, if so, then we're good, 
            //  otherwise it's been cancelled, has failed, was deleted, or already marked as complete (which would be super odd)
            //  and of course may not be marked as processing, which would indicate some other failure, so remove it from the queue.
            if (IsEligibilityDataFileCanceledOrDeleted(item.Body))
            {
                context.CompleteEarly(ProcessResult.Success);
            }
        }

        public void AfterProcess(QueueItem item, MessageProcessingContext messageProcessingContext)
        {
            // There is nothing to do in this method
        }

        public void HandleException(QueueItem item, Exception ex)
        {
            elUploadService.ReportFailed(item.Body, ex.Message);
        }

        public void Process(QueueItem item, MessageProcessingContext context)
        {
            elUploadService.ProcessFile(item);
        }

        private bool IsEligibilityDataFileCanceledOrDeleted(string eligibilityUploadId)
        {
            using (InstrumentationContext context = new InstrumentationContext("EligibilityUploadService.IsEligibilityDataFileCanceledOrDeleted"))
            {
                return EligibilityUpload.AsQueryable().Count(u => u.Id == eligibilityUploadId && u.Status != ProcessingStatus.Canceled) <= 0;
            }
        }

    }
}