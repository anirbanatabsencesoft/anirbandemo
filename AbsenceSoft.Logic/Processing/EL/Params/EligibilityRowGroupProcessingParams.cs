﻿using System.Collections.Generic;

namespace AbsenceSoft.Logic.Processing.EL.Params
{
    /// <summary>
    /// Encapsulating all parameters to be used in row group processing
    /// </summary>
    internal class EligibilityRowGroupProcessingParams
    {
        public int bulkEmployeeOps = 0;
        public int bulkEmployeeContactOps = 0;
        public int bulkCaseOps = 0;
        public int bulkVariableTimeOps = 0;
        public int bulkToDoOps = 0;
        public int bulkEmployeeContactCleanupOps = 0;
        public int bulkJobOps = 0;
        public int bulkOrgOps = 0;
        public int bulkEmpOrgOps = 0;

        public long additionalErrors = 0;
        public long adjustedCount = 0;

        public List<string> contactTypeCodes = new List<string>();
    }
}
