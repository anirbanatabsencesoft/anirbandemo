﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Processing.EL.Params
{
    /// <summary>
    /// Validation and other processing params
    /// </summary>
    internal class EligibilityRowProcessingParams
    {
        public bool isNew = false;
        public bool updateEE = false;
        public bool reCalcOpenCases = false;
        public List<string> updateCases = new List<string>();
        public bool failAll = false;
        public bool updateCasesOfficeLocation = false;
    }
}
