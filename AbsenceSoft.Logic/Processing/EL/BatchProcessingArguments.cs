﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Processing.EL
{

    /// <summary>
    /// Arguments for batch processing of EL records
    /// </summary>
    public class BatchProcessingArguments
    {
        /// <summary>
        /// The reference of Uplaoded document
        /// </summary>
        public EligibilityUpload UploadedDoc { get; set; }

        /// <summary>
        /// The list of records to be parsed
        /// </summary>
        public IList<EligibilityUploadResult> Items { get; set; }

        /// <summary>
        /// Pass list of custom fields for customer as param
        /// </summary>
        public List<CustomField> CustomFields { get; set; }

        /// <summary>
        /// List of all employee employement types
        /// </summary>
        public List<EmployeeClass> EmployeeClassTypes { get; set; }
        /// <summary>
        /// All contact types for the employer to be listed
        /// </summary>
        public Dictionary<string, ContactType> ContactTypes { get; set; }

        /// <summary>
        /// List of Pay schedules
        /// </summary>
        public Dictionary<string, string> PaySchedules { get; set; }

        /// <summary>
        /// Pass list of employees
        /// </summary>
        public List<Employee> Employees { get; set; }


    }
}
