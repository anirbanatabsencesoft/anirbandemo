﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using static AbsenceSoft.Logic.Processing.CaseRow;

namespace AbsenceSoft.Logic.Processing
{
    internal class CaseRowForRelapse : BaseRow
    {
        #region Fields

        public RecordType RecType;

        /// <summary>
        /// Mandatory. Associate ID number or unique identifier, either from your HRIS or other system; if numeric may contain leading zeros, however these leading zeros 
        /// will then always be required when identifying this associate.
        /// </summary>
        public string EmployeeNumber;

        /// <summary>
        /// Mandatory. The unique case number assigned to the case from the system the case is being converted from. This may be in any format, any length and any composition. 
        /// AbsenceTracker will keep this literal case number value as "the" case number within AbsenceTracker as well.
        ///This case number must also match all other conversion records that impact this case such as Create Case("C") records, Time Off Requests, etc.
        /// </summary>
        public string UniqueCaseNumber;

        /// <summary>
        /// Mandatory. The start date for the relapse.
        /// </summary>
        public DateTime? StartDate;

        /// <summary>
        /// The end date for the relapse. 
        ///*This is required for Consecutive, Intermittent and Reduced leaves.
        ///*This is optional for Administrative cases. If no end date is supplied will be assumed perpetual.
        /// </summary>
        public DateTime? EndDate;

        /// <summary>
        /// Mandatory. Provides the date of re-injury, re-illness or recurrence that resulted in a relapse/recurrence event. This may or may not be the same date of the start of the relapse event.
        /// </summary>
        public DateTime? DateofRelapse;

        /// <summary>
        /// Optional. Overrides or sets the prior case RTW date for when the employee had originally returned to work before this relapse/recurrence.
        /// </summary>
        public DateTime? PriorRTWDate;

        /// <summary>
        /// Optional. Sets the new actual return to work date that occurred in the past for this relapse event.
        /// </summary>
        public DateTime? NewRTWDate;

        /// <summary>
        /// Optional. Sets the new estimated return to work date for an open case/relapse based on when the employee is anticipated to return to work.
        /// </summary>
        public DateTime? NewEstimatedRTWDate;

        /// <summary>
        /// Optional. Overrides the prior case closed date,or sets the prior case closed date before this case was re-opened for this relapse/recurrence. 
        /// This value should be less than the Date of Relapse and less than the Date Re-Opened value, if provided.
        /// </summary>
        public DateTime? PriorCaseClosedDate;

        /// <summary>
        /// Optional. The date the case was re-opened as a relapse (not necessarily the date the employee relapsed). This is a case management tracking/audit date field.
        /// default: If not provided, the Date of Relapse will be used.
        /// </summary>
        public DateTime? DateReOpened;

        /// <summary>
        /// Optional. Indicates the new case closed date from this relapse event.
        /// </summary>
        public DateTime? NewCaseClosedDate;

        #endregion

        #region const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;
        private const int UniqueCaseNumberOrdinal = 2;
        private const int StartDateOrdinal = 3;
        private const int EndDateOrdinal = 4;
        private const int DateofRelapseOrdinal = 5;
        private const int PriorRTWDateOrdinal = 6;
        private const int NewRTWDateOrdinal = 7;
        private const int NewEstimatedRTWDateOrdinal = 8;
        private const int PriorCaseClosedDateOrdinal = 9;
        private const int DateReOpenedOrdinal = 10;
        private const int NewCaseClosedDateOrdinal = 11;

        #endregion

        public CaseRowForRelapse(string[] rowSource) : base(rowSource) { }

        /// <summary>
        /// To Parse the Ordinal value from csv file
        /// </summary>
        /// <param name="rowSource">Sorce row form csv</param>
        /// <param name="customFields">Custom Fields value</param>
        /// <returns></returns>
        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (Required(EmployeeNumberOrdinal, "Employee Number"))
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);

            if (Required(UniqueCaseNumberOrdinal, "Case Number"))
                UniqueCaseNumber = Normalize(UniqueCaseNumberOrdinal);

            if (Required(StartDateOrdinal, "Start Date"))
                StartDate = ParseDate(StartDateOrdinal, "Start Date");

            if (Required(DateofRelapseOrdinal, "Date of Relapse"))
                DateofRelapse = ParseDate(DateofRelapseOrdinal, "Date of Relapse");

            EndDate = ParseDate(EndDateOrdinal, "End Date");
            PriorRTWDate = ParseDate(PriorRTWDateOrdinal, "Prior RTW Date");
            NewRTWDate = ParseDate(NewRTWDateOrdinal, "New RTW Date");
            NewEstimatedRTWDate = ParseDate(NewEstimatedRTWDateOrdinal, "New Estimated RTW Date");
            PriorCaseClosedDate = ParseDate(PriorCaseClosedDateOrdinal, "Prior Case Closed Date");
            DateReOpened = ParseDate(DateReOpenedOrdinal, "Date Re-Opened");
            NewCaseClosedDate = ParseDate(NewCaseClosedDateOrdinal, "New Case Closed Date");

            return true;
        }

        /// <summary>
        /// Apply and update relapse case
        /// </summary>
        /// <param name="myCase">Case matched from csv case number</param>
        /// <param name="emp">Case matched from csv case number</param>
        /// <returns></returns>
        internal ApplicationResult Apply(Case myCase, Employee emp)
        {
            if (myCase == null || string.IsNullOrWhiteSpace(myCase.Id))
            {
                AddError("Cannot create Relapse, case does not exists. Please use the AbsenceTracker application to modify/create this case.");
                return ApplicationResult.Fail;
            }

            if (myCase.Segments.Any(s => s.Type != CaseType.Administrative) && EndDate == null)
            {
                AddError("Relapse End Date is required for Consecutive, Intermittent and Reduced leaves.");
                return ApplicationResult.Fail;
            }
            // Build Relapse  Case
            Relapse relapse = new Relapse
            {
                CaseId = myCase.Id,
                StartDate = StartDate.Value,
                EndDate = EndDate,
                CaseType = myCase.Segments.OrderByDescending(s => s.StartDate).FirstOrDefault().Type,
                Employee = emp,
                EmployerId = emp.Employer.Id,
                CustomerId = emp.CustomerId
            };
            using (CaseService cs = new CaseService())
            {
                LeaveOfAbsence loa = cs.ReopenCase(myCase, relapse);

                if (DateofRelapse.HasValue)
                {
                    myCase.SetCaseEvent(CaseEventType.Relapse, DateofRelapse.Value);
                }

                if (PriorRTWDate.HasValue)
                {
                    myCase.SetCaseEvent(CaseEventType.ReturnToWork, PriorRTWDate.Value);
                }

                if (PriorCaseClosedDate.HasValue && DateofRelapse.HasValue && PriorCaseClosedDate.Value < DateofRelapse.Value && PriorCaseClosedDate.Value < DateReOpened.Value)
                {
                    myCase.SetCaseEvent(CaseEventType.CaseClosed, PriorCaseClosedDate.Value);
                }

                myCase.Save();

                if (NewRTWDate.HasValue)
                {
                    myCase.SetCaseEvent(CaseEventType.ReturnToWork, NewRTWDate.Value);
                }

                if (NewCaseClosedDate.HasValue)
                {
                    myCase.SetCaseEvent(CaseEventType.CaseClosed, NewCaseClosedDate.Value);
                }

                if (NewEstimatedRTWDate.HasValue)
                {
                    myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, NewEstimatedRTWDate.Value);
                }

                cs.UpdateRelapse(loa.Case, relapse);

                myCase.Save();
            }
            return ApplicationResult.Success;
        }
    }
}
