﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Common;

namespace AbsenceSoft.Logic.Processing
{
    /// <summary>
    /// A processor for messages.
    /// </summary>
    public interface IMessageProcessor
    {
        /// <summary>
        /// Return true if this processor should
        /// be fed target item to process.
        /// </summary>
        /// <param name="item">The QueueItem to process.</param>
        /// <returns>true if it should process the item</returns>
        bool IsProcessorFor(QueueItem item);

        /// <summary>
        /// Perform actions prior to 
        /// processing.
        /// </summary>
        /// <remarks>
        /// Use context.CompleteEarly() to skip
        /// processing and allow the QueueListener to handle and
        /// delete the message.
        /// </remarks>
        /// <param name="item"></param>
        /// <param name="context"></param>
        void BeforeProcess(QueueItem item, MessageProcessingContext context);

        /// <summary>
        /// Peforms actions after processing
        /// </summary>
        /// <param name="item"></param>
        /// <param name="context"></param>
        void AfterProcess(QueueItem item, MessageProcessingContext context);

        /// <summary>
        /// Provide additional exception handling should this processor
        /// be determined to be the target processor.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ex"></param>
        void HandleException(QueueItem item, Exception ex);

        /// <summary>
        /// Do the real processing work
        /// </summary>
        /// <param name="item"></param>
        /// <param name="context"></param>
        void Process(QueueItem item, MessageProcessingContext context);
    }
}
