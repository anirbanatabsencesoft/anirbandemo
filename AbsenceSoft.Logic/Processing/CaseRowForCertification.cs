﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using static AbsenceSoft.Logic.Processing.CaseRow;

namespace AbsenceSoft.Logic.Processing
{
    internal class CaseRowForCertification : BaseRow
    {
        #region Fields
        
        public RecordType RecType;

        /// <summary>
        /// Mandatory. Associate ID number or unique identifier, either from your HRIS or other system; if numeric may contain leading zeros, however these leading zeros 
        /// will then always be required when identifying this associate.
        /// </summary>
        public string EmployeeNumber;

        /// <summary>
        /// Mandatory. The unique case number assigned to the case from the system the case is being converted from. This may be in any format, any length and any composition. 
        /// AbsenceTracker will keep this literal case number value as "the" case number within AbsenceTracker as well.
        ///This case number must also match all other conversion records that impact this case such as Create Case("C") records, Time Off Requests, etc.
        /// </summary>
        public string UniqueCaseNumber;

        /// <summary>
        /// Mandatory. The start date for the certificate.
        /// </summary>
        public DateTime? StartDate;

        /// <summary>
        /// Mandatory. The end date for the certificate. 
        /// </summary>
        public DateTime? EndDate;

        /// <summary>
        /// Optional. The date the intermittent certification was created in the system originally (recorded). 
        /// This is a case management tracking/audit date field.
        /// default: If not provided, the Case Created date is used.
        /// </summary>
        public DateTime? DateCreated;

        /// <summary>
        /// Mandatory. Specifies the type of intermittent absence.
        /// either: Office Visit - 0 Incapacity - 1
        /// </summary>
        public int? Categorization;

        /// <summary>
        /// Mandatory. Specifies the number of occurrences that are expected to occur through the specified frequency.
        /// </summary>
        public int? Occurances;

        /// <summary>
        /// Mandatory. Specifies the frequency of the occurrences as they are certified/expected to occur.
        /// </summary>
        public int? Frequency;

        /// <summary>
        /// Mandatory. Specifies the units or basis for how the frequency is measured. 
        /// i.e. frequency + type (8 weeks or 2 days, where 8 and 2 are frequencies and weeks/days would be the type).         
        /// </summary>
        public int? FrequencyUnit;

        /// <summary>
        /// Optional. Specifies how those units of the frequency are applied to the certification as it applies to "Days" or "Weeks" (comprised of days).
        /// Work Day - 0 (default) | Calendar Day - 1
        /// </summary>
        public int? FrequencyUnitType;

        /// <summary>
        /// Mandatory. Specifies the certified duration for each occurrence to occur within the bounds of
        /// </summary>
        public int? Duration;

        /// <summary>
        /// Mandatory. Specifies the units used to measure duration of each occurrence.
        /// </summary>
        public int? DurationType;

        /// <summary>
        /// Optional. Indicated whether or not the certification is complete or incomplete. 
        /// True = Certification is incomplete/not approved, False (or null/blank) then it is approved/complete.
        /// default: FALSE (Complete/Approved)
        /// </summary>
        public bool? IsIncomplete;

        #endregion

        #region const

        private const int X_RecordTypeOrdinal = 0;
        private const int X_EmployeeNumberOrdinal = 1;
        private const int X_UniqueCaseNumberOrdinal = 2;
        private const int X_StartDateOrdinal = 3;
        private const int X_EndDateOrdinal = 4;
        private const int X_DateCreatedOrdinal = 5;
        private const int X_CategorizationOrdinal = 6;
        private const int X_OccurancesOrdinal = 7;
        private const int X_FrequencyOrdinal = 8;
        private const int X_FrequencyUnitOrdinal = 9;
        private const int X_FrequencyUnitTypeOrdinal = 10;
        private const int X_DurationOrdinal = 11;
        private const int X_DurationTypeOrdinal = 12;
        private const int X_IsIncompleteOrdinal = 13;

        public CaseRowForCertification(string[] rowSource) : base(rowSource) { }

        #endregion

        /// <summary>
        /// Parse and validate record fields
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        /// <param name="customFields">The custom fields.</param>
        /// <returns>Is Valid</returns>
        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (Required(X_EmployeeNumberOrdinal, "Employee Number"))
            {
                EmployeeNumber = Normalize(X_EmployeeNumberOrdinal);
            }                

            if (Required(X_UniqueCaseNumberOrdinal, "Case Number"))
            {
                UniqueCaseNumber = Normalize(X_UniqueCaseNumberOrdinal);
                
            }

            if (Required(X_StartDateOrdinal, "Start Date"))
            {
                StartDate = ParseDate(X_StartDateOrdinal, "Start Date");
            }

            if (Required(X_EndDateOrdinal, "End Date"))
            {
                EndDate = ParseDate(X_EndDateOrdinal, "End Date");
            }                

            DateCreated = ParseDate(X_DateCreatedOrdinal, "Date Created");
            
            if (Required(X_CategorizationOrdinal, "Categorization"))
            {
                Categorization = ParseInt(X_CategorizationOrdinal, "Categorization");
            }
            
            if (Required(X_OccurancesOrdinal, "Occurances"))
            {
                Occurances = ParseInt(X_OccurancesOrdinal, "Occurances");
            }
            
            if (Required(X_FrequencyOrdinal, "Frequency"))
            {
                Frequency = ParseInt(X_FrequencyOrdinal, "Frequency");
            }
                        
            if (Required(X_FrequencyUnitOrdinal, "Frequency Unit"))
            {
                FrequencyUnit = ParseInt(X_FrequencyUnitOrdinal, "Frequency Unit");
            }
            
            FrequencyUnitType = ParseInt(X_FrequencyUnitTypeOrdinal, "Frequency Unit Type");
                        
            if (Required(X_DurationOrdinal, "Duration"))
            {
                Duration = ParseInt(X_DurationOrdinal, "Duration");
            }
            
            if (Required(X_DurationTypeOrdinal, "Duration Type"))
            {
                DurationType = ParseInt(X_DurationTypeOrdinal, "Duration Type");
            }
            
            IsIncomplete = ParseBool(X_IsIncompleteOrdinal, "Is Incomplete");

            return true;
        }

        /// <summary>
        /// Applies the properties in this record to the target case, employing any necessary additional validation
        /// </summary>
        /// <param name="myCase">Case</param>
        /// <param name="emp">Employee</param>
        /// <returns>ApplicationResult</returns>
        internal ApplicationResult Apply(Case myCase, Employee emp)
        {
            if (myCase == null || string.IsNullOrWhiteSpace(myCase.Id))
            {
                AddError("Cannot create Certification, case does not exists. Please use the AbsenceTracker application to modify/create this case.");
                return ApplicationResult.Fail;
            }

            if (!myCase.Segments.Any(s => s.Type == CaseType.Intermittent) && !myCase.Certifications.Any())
            {
                AddError("Cannot create Certification, Intermittent segment or previous certification does not exists on case. Please use the AbsenceTracker application to modify/create this case.");                
                return ApplicationResult.Fail;
            }

            Certification cert = new Certification
            {                
                StartDate = StartDate.Value,
                EndDate = EndDate.Value,
                CertificationCreateDate = DateCreated ?? myCase.CreatedDate,
                IntermittentType = Categorization == 1 ? IntermittentType.Incapacity : IntermittentType.OfficeVisit,
                Occurances = Occurances ?? 0,
                Frequency = Frequency ?? 0,
                FrequencyType = GetFrequencyUnit(FrequencyUnit),
                FrequencyUnitType = FrequencyUnitType == 1 ? DayUnitType.CalendarDay : DayUnitType.Workday,
                Duration = Duration ?? 0d,
                DurationType = GetFrequencyUnit(DurationType),
                IsCertificationIncomplete = IsIncomplete.HasValue ? IsIncomplete.Value : false
            };

            using (var service = new CaseService())
            {
                service.CreateOrModifyCertification(myCase, cert, true); // add certificate
            }
            
            return ApplicationResult.Success;
        }

        /// <summary>
        /// Get Frequency Unit by type
        /// </summary>
        /// <param name="frequencyUnitType">UnitType</param>
        /// <returns>Unit</returns>
        private Unit GetFrequencyUnit(int? frequencyUnitType)
        {
            switch (frequencyUnitType)
            {
                case 0:
                    return Unit.Minutes;
                case 1:
                    return Unit.Hours;
                case 3:
                    return Unit.Weeks;
                case 4:
                    return Unit.Months;
                case 5:
                    return Unit.Years;
                case 2:
                default:
                    return Unit.Days;
            }
        }
    }
}