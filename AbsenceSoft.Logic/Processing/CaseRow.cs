﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Logic.Processing
{
    internal sealed class CaseRow : BaseRow
    {
        #region Const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;
        private const int EmployeeLastNameOrdinal = 2;
        private const int EmployeeFirstNameOrdinal = 3;
        private const int E_EmployeeMiddleNameOrdinal = 4;

        private const int CaseNumberOrdinal = 4;

        // Eligibility Row Ordinals
        private const int LeaveIntervalOrdinal = 5;
        private const int ReasonForLeaveOrdinal = 6;
        private const int FamilyRelationshipOrdinal = 7;
        private const int IntermittentFrequencyOrdinal = 8;
        private const int IntermittentFrequencyUnitOrdinal = 9;
        private const int IntermittentOccurrencesOrdinal = 10;
        private const int IntermittentDurationOrdinal = 11;
        private const int IntermittentDurationUnitOrdinal = 12;
        private const int DateCaseOpenedOrdinal = 13;
        private const int ExpectedStartDateOrdinal = 14;
        private const int ExpectedRTWDateOrdinal = 15;
        private const int ConfirmedRTWDateOrdinal = 16;
        private const int CaseClosedDateOrdinal = 17;
        private const int CaseStatusOrdinal = 18;
        private const int DeliveryDateOrdinal = 19;
        private const int BondingStartDateOrdinal = 20;
        private const int BondingEndDateOrdinal = 21;
        private const int IllnessOrInjuryDateOrdinal = 22;
        private const int HospitalAdmissionDateOrdinal = 23;
        private const int HospitalReleaseDateOrdinal = 24;
        private const int RTWReleaseDateOrdinal = 25;
        private const int AutoPoliciesOrdinal = 26;
        private const int ReducedScheduleOrdinal = 27;
        private const int CaseCreateWorkflowOrdinal = 28;

        // Adjudication Detail
        private const int AdjudicationStartDateOrdinal = 5;
        private const int AdjudicationEndDateOrdinal = 6;
        private const int AdjudicationStatusOrdinal = 7;
        private const int MinutesUsedOrdinal = 8;

        // Intermittent Time Used
        private const int DateOfAbsenceOrdinal = 5;
        private const int MinutesApprovedOrdinal = 6;
        private const int MinutesDeniedOrdinal = 7;
        private const int MinutesPendingOrdinal = 8;

        // Adjudication + Intermittent Shared Ordinals
        private const int PolicyCodeOrdinal = 9;
        private const int WorkflowOrdinal = 10;

        // case notes
        private const int NOTE_NoteDate = 5;
        private const int NOTE_Category = 6;
        private const int NOTE_Text = 7;
        private const int NOTE_EnteredByName = 8;

        // work related
        private const int WC_Reportable = 5;
        private const int WC_HealthcarePrivate = 6;
        private const int WC_WhereOccurred = 7;
        private const int WC_CaseClassification = 8;
        private const int WC_TypeOfInjury = 9;
        private const int WC_DaysAwayfromWork = 10;
        private const int WC_DaysontheJobTransferorRestriction = 11;
        private const int WC_HealthcareProviderTypeCode = 12;
        private const int WC_HealthcareProviderFirstName = 13;
        private const int WC_HealthcareProviderLastName = 14;
        private const int WC_HealthcareProviderCompany = 15;
        private const int WC_HealthcareProviderAddress1 = 16;
        private const int WC_HealthcareProviderAddress2 = 17;
        private const int WC_HealthcareProviderCity = 18;
        private const int WC_HealthcareProviderState = 19;
        private const int WC_HealthcareProviderCountry = 20;
        private const int WC_HealthcareProviderPostalCode = 21;
        private const int WC_EmergencyRoom = 22;
        private const int WC_HospitalizedOvernight = 23;
        private const int WC_TimeEmployeeBeganWork = 24;
        private const int WC_TimeOfEvent = 25;
        private const int WC_ActivityBeforeIncident = 26;
        private const int WC_WhatHappened = 27;
        private const int WC_InjuryOrIllness = 28;
        private const int WC_WhatHarmedTheEmployee = 29;
        private const int WC_DateOfDeath = 30;
        private const int WC_Sharps = 31;
        private const int WC_SharpsInjuryLocation = 32;
        private const int WC_TypeOfSharp = 33;
        private const int WC_BrandOfSharp = 34;
        private const int WC_ModelOfSharp = 35;
        private const int WC_BodyFluidInvolved = 36;
        private const int WC_HaveEngineeredInjuryProtection = 37;
        private const int WC_WasProtectiveMechanismActivated = 38;
        private const int WC_WhenDidtheInjuryOccur = 39;
        private const int WC_JobClassification = 40;
        private const int WC_JobClassificationOther = 41;
        private const int WC_LocationandDepartment = 42;
        private const int WC_LocationandDepartmentOther = 43;
        private const int WC_SharpsProcedure = 44;
        private const int WC_SharpsProcedureOther = 45;
        private const int WC_ExposureDetail = 46;
        private const int WC_InjuryLocation = 47;
        private const int WC_InjuredBodyPart = 48;


        // Attachement


        private const int AT_CreatedDate = 5;
        private const int AT_AttacheMentType = 6;
        private const int AT_FileName = 7;
        private const int AT_FileDescription = 8;
        private const int AT_FileSource = 9;
        private const int AT_IsPublic = 10;
        private const int AT_RepresentativeName = 11;


        // Custom_Fields

        private const int CF_CustomFieldCodeOrdinal = 2;
        private const int CF_CustomFieldValueOrdinal = 3;
        private const int CF_EmployerReferenceCodeOrdinal = 4;
        private const int CF_EntityTargetOrdinal = 5;
        private const int CF_CaseNumberOrdinal = 6;
        public CustomField[] CustomFields { get; set; }

        // Employee
        private const int E_JobTitleOrdinal = 5;
        private const int E_JobLocationOrdinal = 6;
        private const int E_WorkStateOrdinal = 7;
        private const int E_WorkCountryOrdinal = 8;
        private const int E_PhoneHomeOrdinal = 9;
        private const int E_PhoneWorkOrdinal = 10;
        private const int E_PhoneMobileOrdinal = 11;
        private const int E_PhoneAltOrdinal = 12;
        private const int E_EmailOrdinal = 13;
        private const int E_EmailAltOrdinal = 14;
        private const int E_AddressOrdinal = 15;
        private const int E_Address2Ordinal = 16;
        private const int E_CityOrdinal = 17;
        private const int E_StateOrdinal = 18;
        private const int E_PostalCodeOrdinal = 19;
        private const int E_CountryOrdinal = 20;
        private const int E_EmploymentTypeOrdinal = 21;
        private const int E_ManagerEmployeeNumberOrdinal = 22;
        private const int E_ManagerLastNameOrdinal = 23;
        private const int E_ManagerFirstNameOrdinal = 24;
        private const int E_ManagerPhoneOrdinal = 25;
        private const int E_ManagerEmailOrdinal = 26;
        private const int E_HREmployeeNumberOrdinal = 27;
        private const int E_HRLastNameOrdinal = 28;
        private const int E_HRFirstNameOrdinal = 29;
        private const int E_HRPhoneOrdinal = 30;
        private const int E_HREmailOrdinal = 31;
        private const int E_SpouseEmployeeNumberOrdinal = 32;
        private const int E_DateOfBirthOrdinal = 33;
        private const int E_GenderOrdinal = 34;
        private const int E_ExemptionStatusOrdinal = 35;
        private const int E_Meets50In75Ordinal = 36;
        private const int E_KeyEmployeeOrdinal = 37;
        private const int E_MilitaryStatusOrdinal = 38;
        private const int E_EmploymentStatusOrdinal = 39;
        private const int E_TerminationDateOrdinal = 40;
        private const int E_PayRateOrdinal = 41;
        private const int E_PayTypeOrdinal = 42;
        private const int E_HireDateOrdinal = 43;
        private const int E_RehireDateOrdinal = 44;
        private const int E_AdjustedServiceDateOrdinal = 45;
        private const int E_MinutesPerWeekOrdinal = 46;
        private const int E_HoursWorkedIn12MonthsOrdinal = 47;
        private const int E_WorkTimeSunOrdinal = 48;
        private const int E_WorkTimeMonOrdinal = 49;
        private const int E_WorkTimeTueOrdinal = 50;
        private const int E_WorkTimeWedOrdinal = 51;
        private const int E_WorkTimeThuOrdinal = 52;
        private const int E_WorkTimeFriOrdinal = 53;
        private const int E_WorkTimeSatOrdinal = 54;

        private const int E_VariableScheduleOrdinal = 55;
        private const int E_JobDescriptionOrdinal = 56;
        private const int E_JobClassificationOrdinal = 57;
        private const int E_USCensusJobCategoryCodeOrdinal = 58;
        private const int E_SOCCodeOrdinal = 59;
        private const int E_ScheduleEffectiveDateOrdinal = 60;
        private const int E_EmployerReferenceCodeOrdinal = 61;
        private const int E_SSNOrdinal = 62;
        private const int E_PayScheduleNameOrdinal = 63;
        private const int E_StartDayOfWeekOrdinal = 64;
        private const int E_CaseNumberOrdinal = 65;
        private const int E_AverageMinutesWorkedPerWeekOrdinal = 66;

        // Certification
        private const int X_UniqueCaseNumberOrdinal = 2;

        //Accommodation
        private const int D_UniqueCaseNumberOrdinal = 2;

        // Relapse
        private const int R_UniqueCaseNumberOrdinal = 2;

        // Segment
        private const int S_UniqueCaseNumberOrdinal = 2;

        #endregion Const

        #region .ctor

        public CaseRow() : base() { }
        public CaseRow(string rowSource) : base(rowSource) { }
        public CaseRow(string[] rowSource) : base(rowSource) { }

        #endregion .ctor

        private CaseRowForCertification caseRowForCertification = null;

        private CaseRowForAccommodation caseRowForAccommodation = null;
        private CaseRowForSegment caseRowForSegment = null;

        private CaseRowForRelapse caseRowForRelapse = null;

        internal enum RecordType
        {
            Case = 'C',
            Adjudication = 'A',
            Intermittent = 'I',
            Note = 'N',
            
            CaseAttachement = 'T',
            // TODO: Need to implement this row per specs.
            WorkRelated = 'W',
            CaseConversionEligibility = 'E',
            CustomField = 'F',
            Relapse = 'R',
            Certification = 'X',
            Accommodation = 'D',
            Segment = 'S'
        }

        #region Fields


        public RecordType RecType { get; set; }
        public string EmployeeNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string CaseNumber { get; set; }

        // Eligibility Row Ordinals
        public CaseType? LeaveInterval { get; set; }
        public string ReasonForLeave { get; set; }
        public ContactType FamilyRelationship { get; set; }
        public int? IntermittentFrequency { get; set; }
        public Unit? IntermittentFrequencyUnit { get; set; }
        public int? IntermittentOccurrences { get; set; }
        public double? IntermittentDuration { get; set; }
        public Unit? IntermittentDurationUnit { get; set; }
        public DateTime? DateCaseOpened { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public DateTime? ExpectedRTWDate { get; set; }
        public DateTime? ConfirmedRTWDate { get; set; }
        public DateTime? CaseClosedDate { get; set; }
        public CaseStatus? CaseStatus { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public DateTime? IllnessOrInjuryDate { get; set; }
        public DateTime? HospitalAdmissionDate { get; set; }
        public DateTime? HospitalReleaseDate { get; set; }
        public DateTime? RTWReleaseDate { get; set; }
        public bool? AutoPolicies { get; set; }
        public int? ReducedSchedule { get; set; }

        // Adjudication Detail
        public DateTime? AdjudicationStartDate { get; set; }
        public DateTime? AdjudicationEndDate { get; set; }
        public AdjudicationStatus? AdjudicationStatus { get; set; }
        public int? MinutesUsed { get; set; }

        // Intermittent Time Used
        public DateTime? DateOfAbsence { get; set; }
        public int? MinutesApproved { get; set; }
        public int? MinutesDenied { get; set; }
        public int? MinutesPending { get; set; }

        // Shared
        public string PolicyCode { get; set; }
        public bool? Workflow { get; set; }

        // notes
        public DateTime? NoteDate { get; set; }
        public NoteCategoryEnum? CaseNoteCategory { get; set; }
        public string NoteEnteredByName { get; set; }
        public string NoteEnteredByEmployeeNumber { get; set; }
        public string NoteText { get; set; }


        // work related
        public WorkRelatedInfo WorkRelated { get; set; }

        // Attachement 
        public DateTime? AttachmentDate { get; set; }
        public AttachmentType? AttachmentTypeValue { get; set; }
        public string FileName { get; set; }
        public string FileDescription { get; set; }
        public string FileSource { get; set; }
        public string RepresentativeName { get; set; }



        public byte[] File { get; set; }

        public long ContentLength { get; set; }
        public string ContentType { get; set; }



        public bool IsPublic { get; set; }

        // Custom Fields
        public string CustomFieldCode { get; set; }
        public string CustomFieldValue { get; set; }
        public string EmployerReferenceCode { get; set; }
        public EntityTarget EntityTargetValue { get; set; }

        // Employee Demographic
        public string JobTitle { get; set; }
        public string JobLocation { get; set; }
        public string WorkState { get; set; }
        public string WorkCountry { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneAlt { get; set; }
        public string Email { get; set; }
        public string EmailAlt { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string EmploymentType { get; set; }
        public string ManagerEmployeeNumber { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public string HREmployeeNumber { get; set; }
        public string HRLastName { get; set; }
        public string HRFirstName { get; set; }
        public string HRPhone { get; set; }
        public string HREmail { get; set; }
        public string SpouseEmployeeNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public char? Gender { get; set; }
        public char? ExemptionStatus { get; set; }
        public bool? Meets50In75 { get; set; }
        public bool? KeyEmployee { get; set; }
        public byte? MilitaryStatus { get; set; }
        public char? EmploymentStatus { get; set; }
        public DateTime? TerminationDate { get; set; }
        public decimal? PayRate { get; set; }
        public byte? PayType { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? RehireDate { get; set; }
        public DateTime? AdjustedServiceDate { get; set; }
        public int? MinutesPerWeek { get; set; }
        public decimal? HoursWorkedIn12Months { get; set; }
        public int? AverageMinutesWorkedPerWeek { get; set; }
        public int? WorkTimeSun { get; set; }
        public int? WorkTimeMon { get; set; }
        public int? WorkTimeTue { get; set; }
        public int? WorkTimeWed { get; set; }
        public int? WorkTimeThu { get; set; }
        public int? WorkTimeFri { get; set; }
        public int? WorkTimeSat { get; set; }
        public bool? VariableSchedule { get; set; }
        public bool? NoOverwriteFlag { get; set; }
        public string JobDescription { get; set; }
        public byte? JobClassification { get; set; }
        public string USCensusJobCategoryCode { get; set; }
        public string SOCCode { get; set; }
        public DateTime? ScheduleEffectiveDate { get; set; }
        public string SSN { get; set; }
        public string PayScheduleName { get; set; }
        public DayOfWeek? StartDayOfWeek { get; set; }


        #endregion Fields

        #region Parsing

        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (rowSource == null || rowSource.Length == 0)
                return AddError("No row source to parse");

            //
            // Record Type
            if (Required(RecordTypeOrdinal, "Record Type"))
            {
                if (!new string[] { "C", "A", "I", "N", "W", "T", "F", "R","E", "X", "D", "S" }.Contains(_parts[RecordTypeOrdinal]))
                    return AddError("Unknown Record Type '{0}' passed. The first value in each record must either be 'C', 'A', 'N', 'I','T','F','R','E' or 'W', 'X','S'", _parts[RecordTypeOrdinal]);
                RecType = (RecordType)Convert.ToChar(_parts[RecordTypeOrdinal]);
            }
            else
                return false;

            // =============
            // Shared Fields
            // =============
            //
            // Employee Number
            if (Required(EmployeeNumberOrdinal, "Employee Number"))
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);            
                LastName = Normalize(EmployeeLastNameOrdinal);
                FirstName = Normalize(EmployeeFirstNameOrdinal);            
            
            if (RecType == RecordType.CustomField)
            {
                if (Required(CF_CaseNumberOrdinal, "Unique Case Number"))
                    CaseNumber = Normalize(CF_CaseNumberOrdinal);
            }
            else if (RecType == RecordType.CaseConversionEligibility)
            {
                if (Required(E_CaseNumberOrdinal, "Unique Case Number"))
                    CaseNumber = Normalize(E_CaseNumberOrdinal);
            }
            else if (RecType == RecordType.Accommodation)
            {
                if (Required(D_UniqueCaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(D_UniqueCaseNumberOrdinal);
                }
            }
            else
            {
                if (Required(CaseNumberOrdinal, "Unique Case Number"))
                    CaseNumber = Normalize(CaseNumberOrdinal);
            }

            // =========================
            // Type Specific Record Fields
            // =========================
            if (RecType == RecordType.Case)
            {
                #region Case
                //
                // Leave Interval / Case Type
                if (Required(LeaveIntervalOrdinal, "Leave Interval"))
                {
                    int? leaveInterval = ParseInt(LeaveIntervalOrdinal, "Leave Interval");
                    if (leaveInterval.HasValue)
                    {
                        if (leaveInterval != 1 && leaveInterval != 2 && leaveInterval != 4 && leaveInterval != 8)
                            AddError("Leave interval of '{0}' is not valid, it must be 1, 2, 4 or 8.", leaveInterval);
                        else
                            LeaveInterval = (CaseType)leaveInterval.Value;
                    }
                }
                //
                // Reason for Leave
                if (Required(ReasonForLeaveOrdinal, "Reason for Leave"))
                    ReasonForLeave = Normalize(ReasonForLeaveOrdinal);
                //
                // Family Relationship
                int? famRel = ParseInt(FamilyRelationshipOrdinal, "Family Relationship");
                if (famRel.HasValue)
                {
                    if (famRel == 0 || (famRel >= 5 && famRel <= 32))
                        FamilyRelationship = ContactType.GetById(String.Format("{0:000000000000000000000000}", famRel.Value));
                    else
                        AddError("Family Relationship of '{0}' is not valid, it must be a valid value from the list of contact types, 0, or between 5 and 32.", famRel);
                }
                //
                // Intermittent leaves
                if (LeaveInterval == CaseType.Intermittent)
                {
                    //
                    // Intermittent Frequency
                    IntermittentFrequency = ParseInt(IntermittentFrequencyOrdinal, "Intermittent Frequency");
                    //
                    // Intermittent Frequency Unit/Type
                    int? intFreqU = ParseInt(IntermittentFrequencyUnitOrdinal, "Intermittent Frequency Unit/Type");
                    if (intFreqU.HasValue && (intFreqU < 0 || intFreqU > 5))
                        AddError("Intermittent Frequency Unit/Type value of '{0}' is not valid, value must be an integer value 0 through 5.", intFreqU);
                    else if (intFreqU.HasValue)
                        IntermittentFrequencyUnit = (Unit)intFreqU.Value;
                    //
                    // Intermittent Occurrences
                    IntermittentOccurrences = ParseInt(IntermittentOccurrencesOrdinal, "Intermittent Occurrences");
                    //
                    // Intermittent Duration
                    IntermittentDuration = ParseDouble(IntermittentDurationOrdinal, "Intermittent Duration");
                    //
                    // Intermittent Duration Unit/Type
                    int? intDurU = ParseInt(IntermittentDurationUnitOrdinal, "Intermittent Frequency Unit/Type");
                    if (intDurU.HasValue && (intDurU < 0 || intDurU > 5))
                        AddError("Intermittent Duration Unit/Type value of '{0}' is not valid, value must be an integer value 0 through 5.", intDurU);
                    else if (intDurU.HasValue)
                        IntermittentDurationUnit = (Unit)intDurU.Value;
                    //
                }
                //
                // Date Case Opened
                if (Required(DateCaseOpenedOrdinal, "Date Case Opened"))
                    DateCaseOpened = ParseDate(DateCaseOpenedOrdinal, "Date Case Opened");
                //
                // Expected Start Date
                if (Required(ExpectedStartDateOrdinal, "Expected Start Date"))
                    ExpectedStartDate = ParseDate(ExpectedStartDateOrdinal, "Expected Start Date");
                //
                // Expected RTW Date
                //if (Required(ExpectedRTWDateOrdinal, "Expected RTW"))
                ExpectedRTWDate = ParseDate(ExpectedRTWDateOrdinal, "Expected RTW");
                //
                // Confirmed RTW Date
                ConfirmedRTWDate = ParseDate(ConfirmedRTWDateOrdinal, "Confirmed RTW Date");
                //
                // Date Case Closed
                CaseClosedDate = ParseDate(CaseClosedDateOrdinal, "Date Case Closed");
                //
                // Validate Non-Admin Case End Date
                if (LeaveInterval != CaseType.Administrative && !ExpectedRTWDate.HasValue && !ConfirmedRTWDate.HasValue && !CaseClosedDate.HasValue)
                    AddError("You must provide either an Expected RTW Date, Confirmed RTW Date or the Case Closed Date for Non-Administrative cases.");
                ExpectedRTWDate = ExpectedRTWDate ?? ConfirmedRTWDate ?? CaseClosedDate;
                //
                // Case Status
                int? cSt = ParseInt(CaseStatusOrdinal, "Case Status");
                if (cSt.HasValue && (cSt < 0 || cSt > 2))
                    AddError("Case Status of '{0}' is not valid. Must be 0 (Open), 1 (Closed) or 2 (Cancelled).", cSt);
                else
                    CaseStatus = (CaseStatus)(cSt ?? 0);
                //
                // Delivery Date
                DeliveryDate = ParseDate(DeliveryDateOrdinal, "Delivery Date");
                //
                // Bonding Start Date
                BondingStartDate = ParseDate(BondingStartDateOrdinal, "Bonding Start Date");
                //
                // Bonding End Date
                BondingEndDate = ParseDate(BondingEndDateOrdinal, "Bonding End Date");
                if (BondingStartDate.HasValue && BondingEndDate.HasValue && BondingStartDate > BondingEndDate)
                    AddError("The bonding start date of '{0:MM/dd/yyyy}' is after the end date of '{1:MM/dd/yyyy}'.", BondingStartDate, BondingEndDate);
                //
                // Illness or Injury Date
                IllnessOrInjuryDate = ParseDate(IllnessOrInjuryDateOrdinal, "Illness or Injury Date");
                //
                // Hospital Admission Date
                HospitalAdmissionDate = ParseDate(HospitalAdmissionDateOrdinal, "Hospital Admission Date");
                //
                // Hospital Release Date
                HospitalReleaseDate = ParseDate(HospitalReleaseDateOrdinal, "Hospital Release Date");
                if (HospitalAdmissionDate.HasValue && HospitalReleaseDate.HasValue && HospitalAdmissionDate > HospitalReleaseDate)
                    AddError("The hospital admission date of '{0:MM/dd/yyyy}' is after the release date of '{1:MM/dd/yyyy}'. If this is a relapse/re-admission, please use the original hospital admission date.", HospitalAdmissionDate, HospitalReleaseDate);
                //
                // RTW Release Date
                RTWReleaseDate = ParseDate(RTWReleaseDateOrdinal, "RTW Release Date");
                //
                // Auto Policies
                AutoPolicies = ParseBool(AutoPoliciesOrdinal, "Auto Policies");
                //
                // Reduced Schedule
                if (LeaveInterval == CaseType.Reduced)
                    if (Required(ReducedScheduleOrdinal, "Reduced Schedule"))
                        ReducedSchedule = ParseInt(ReducedScheduleOrdinal, "Reduced Schedule");
                //
                // Case Create Workflow
                Workflow = ParseBool(CaseCreateWorkflowOrdinal, "Case Create Workflow");
                #endregion
            }
            else if (RecType == RecordType.Adjudication)
            {
                #region Adjudication
                //
                // Start Date
                if (Required(AdjudicationStartDateOrdinal, "Start Date"))
                    AdjudicationStartDate = ParseDate(AdjudicationStartDateOrdinal, "Start Date");
                //
                // End Date
                AdjudicationEndDate = ParseDate(AdjudicationEndDateOrdinal, "End Date");
                //
                // Status / Adjudication
                int? st = ParseInt(AdjudicationStatusOrdinal, "Status / Adjudication");
                if (st.HasValue && (st < 0 || st > 2))
                    AddError("Status / Adjudication of '{0}' is not valid. Must be 0 (Pending), 1 (Approved) or 2 (Denied).", st);
                else
                    AdjudicationStatus = (AdjudicationStatus)(st ?? 0);
                //
                // Minutes Used
                MinutesUsed = ParseInt(MinutesUsedOrdinal, "Minutes Used");
                if (MinutesUsed.HasValue && MinutesUsed < 0)
                    AddError("Minutes Used must be a positive integer. Value supplied was '{0}'.", MinutesUsed);
                if (AdjudicationStatus != Data.Enums.AdjudicationStatus.Approved && MinutesUsed.HasValue && MinutesUsed.Value != 0)
                    AddError("You may not specify Minutes Used for a period when not approving that period.; leave blank or 0");
                if (MinutesUsed.HasValue && AdjudicationEndDate.HasValue && AdjudicationStartDate.HasValue)
                {
                    var days = (AdjudicationEndDate.Value.ToMidnight() - AdjudicationStartDate.Value.ToMidnight()).TotalDays + 1;
                    var minutesPerDay = (double)MinutesUsed.Value / days;
                    if (minutesPerDay > 1440D)
                        AddError("{0:N0} Minutes Used is too large of a value over {1:N0} days; Equates to {2:N0} minutes per day, which is greater than the max per day usage of 1,440 minutes (24 hours).",
                            MinutesUsed, days, minutesPerDay);
                }
                //
                // Policy Code
                PolicyCode = Normalize(PolicyCodeOrdinal);
                //
                // Workflow
                Workflow = ParseBool(WorkflowOrdinal, "Adjudication Workflow");
                #endregion
            }
            else if (RecType == RecordType.Intermittent)
            {
                #region Intermittent
                //
                // Date of Absence
                if (Required(DateOfAbsenceOrdinal, "Date of Absence"))
                    DateOfAbsence = ParseDate(DateOfAbsenceOrdinal, "Date of Absence");
                //
                // Minutes Approved
                MinutesApproved = ParseInt(MinutesApprovedOrdinal, "Minutes Approved");
                //
                // Minutes Denied
                MinutesDenied = ParseInt(MinutesDeniedOrdinal, "Minutes Denied");
                //
                // Minutes Pending
                MinutesPending = ParseInt(MinutesPendingOrdinal, "Minutes Pending");
                if (((MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0)) <= 0)
                    AddError("Total minutes of absence recorded for '{0:MM/dd/yyyy}' is '{1}'. The sum of minutes approved, pending and denied must be at least 1 minute.",
                        DateOfAbsence, (MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0));
                //
                // Policy Code
                PolicyCode = Normalize(PolicyCodeOrdinal);
                //
                // Workflow
                Workflow = ParseBool(WorkflowOrdinal, "Time Off Request Workflow");
                #endregion
            }
            else if (RecType == RecordType.Note)
            {
                #region Note

                NoteDate = ParseDate(NOTE_NoteDate, "Note Date");
                int nc = ParseInt(NOTE_Category, "Note Category") ?? 0;
                NoteCategoryEnum ncCon;
                if (Enum.TryParse<NoteCategoryEnum>(nc.ToString(), out ncCon))
                    CaseNoteCategory = ncCon;
                else
                    AddError("Case Note Category {0} cannot be parsed", nc);

                NoteEnteredByName = Normalize(NOTE_EnteredByName);
                NoteText = Normalize(NOTE_Text);

                if (string.IsNullOrWhiteSpace(NoteText))
                    AddError("Note is blank");

                #endregion Not
            }
            else if (RecType == RecordType.WorkRelated)
            {
                #region Work Related

                WorkRelated = new WorkRelatedInfo();

                WorkRelated.Reportable = ParseBool(WC_Reportable, "Reportable") ?? true;
                WorkRelated.HealthCarePrivate = ParseBool(WC_HealthcarePrivate, "Healthcare Private");
                WorkRelated.WhereOccurred = Normalize(WC_WhereOccurred);

                int? cl = ParseInt(WC_CaseClassification, "Case Classification");

                if (cl < 0 || cl > 3)
                    AddError("'{0}' is not a valid case classification, it must be 0, 1, 2 or 3", Normalize(WC_CaseClassification));
                else if (cl.HasValue)
                    WorkRelated.Classification = (WorkRelatedCaseClassification)cl.Value;


                int? c2 = ParseInt(WC_TypeOfInjury, "Type of Injury");
                if (c2 < 0 || c2 > 5)
                    AddError("'{0}' is not a valid type of injury, it must be 0, 1, 2, 3, 4 or 5", Normalize(WC_TypeOfInjury));
                else if (cl.HasValue)
                    WorkRelated.TypeOfInjury = (WorkRelatedTypeOfInjury)c2.Value;


                WorkRelated.DaysAwayFromWork = ParseInt(WC_DaysAwayfromWork, "Days Away from Work");
                WorkRelated.DaysOnJobTransferOrRestriction = ParseInt(WC_DaysontheJobTransferorRestriction, "Days on the Job Transfer or Restriction");
                string providerType = Normalize(WC_HealthcareProviderTypeCode);
                if (!string.IsNullOrWhiteSpace(providerType))
                {
                    WorkRelated.Provider = new EmployeeContact() { ContactTypeCode = providerType };
                    WorkRelated.Provider.Contact.FirstName = Normalize(WC_HealthcareProviderFirstName);
                    WorkRelated.Provider.Contact.LastName = Normalize(WC_HealthcareProviderLastName);
                    WorkRelated.Provider.Contact.CompanyName = Normalize(WC_HealthcareProviderCompany);
                    WorkRelated.Provider.Contact.Address.Address1 = Normalize(WC_HealthcareProviderAddress1);
                    WorkRelated.Provider.Contact.Address.Address2 = Normalize(WC_HealthcareProviderAddress2);
                    WorkRelated.Provider.Contact.Address.City = Normalize(WC_HealthcareProviderCity);
                    WorkRelated.Provider.Contact.Address.State = Normalize(WC_HealthcareProviderState);
                    WorkRelated.Provider.Contact.Address.Country = Normalize(WC_HealthcareProviderCountry);
                    WorkRelated.Provider.Contact.Address.PostalCode = Normalize(WC_HealthcareProviderPostalCode);
                }
                WorkRelated.EmergencyRoom = ParseBool(WC_EmergencyRoom, "Emergency Room");
                WorkRelated.HospitalizedOvernight = ParseBool(WC_HospitalizedOvernight, "Hospitalized Overnight");
                string tebw = Normalize(WC_TimeEmployeeBeganWork);
                if (!string.IsNullOrWhiteSpace(tebw))
                {
                    TimeOfDay tod;
                    if (TimeOfDay.TryParse(tebw, out tod))
                        WorkRelated.TimeEmployeeBeganWork = tod;
                    else
                        AddError("'{0}' is not a valid time of day. Please provide a valid value for 'Time Employee Began Work' using the 'HH:mm' or 'hh:mm tt' format", tebw);
                }
                string toe = Normalize(WC_TimeOfEvent);
                if (!string.IsNullOrWhiteSpace(toe))
                {
                    TimeOfDay tod;
                    if (TimeOfDay.TryParse(toe, out tod))
                        WorkRelated.TimeEmployeeBeganWork = tod;
                    else
                        AddError("'{0}' is not a valid time of day. Please provide a valid value for 'Time Of Event' using the 'HH:mm' or 'hh:mm tt' format", toe);
                }
                WorkRelated.ActivityBeforeIncident = Normalize(WC_ActivityBeforeIncident);
                WorkRelated.WhatHappened = Normalize(WC_WhatHappened);
                WorkRelated.InjuryOrIllness = Normalize(WC_InjuryOrIllness);
                WorkRelated.InjuryLocation = Normalize(WC_InjuryLocation);
                WorkRelated.InjuredBodyPart = Normalize(WC_InjuredBodyPart);
                WorkRelated.WhatHarmedTheEmployee = Normalize(WC_WhatHarmedTheEmployee);
                WorkRelated.DateOfDeath = ParseDate(WC_DateOfDeath, "Date of Death");
                WorkRelated.Sharps = ParseBool(WC_Sharps, "Sharps");
                if (WorkRelated.Sharps == true)
                {
                    WorkRelated.SharpsInfo = new WorkRelatedSharpsInfo();

                    cl = ParseInt(WC_SharpsInjuryLocation, "Sharps Injury Location");
                    if (cl < 0 || cl > 10)
                        AddError("'{0}' is not a valid sharps injury location, it must be an integer of 1 through 10", Normalize(WC_SharpsInjuryLocation));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.InjuryLocation = new List<WorkRelatedSharpsInjuryLocation>(1) { (WorkRelatedSharpsInjuryLocation)cl.Value };


                    WorkRelated.SharpsInfo.TypeOfSharp = Normalize(WC_TypeOfSharp);
                    WorkRelated.SharpsInfo.BrandOfSharp = Normalize(WC_BrandOfSharp);
                    WorkRelated.SharpsInfo.ModelOfSharp = Normalize(WC_ModelOfSharp);
                    WorkRelated.SharpsInfo.BodyFluidInvolved = Normalize(WC_BodyFluidInvolved);
                    WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection = ParseBool(WC_HaveEngineeredInjuryProtection, "Have Engineered Injury Protection");

                    if (WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection == true)
                    {
                        WorkRelated.SharpsInfo.WasProtectiveMechanismActivated = ParseBool(WC_WasProtectiveMechanismActivated, "Was Protective Mechanism Activated");
                        cl = ParseInt(WC_WhenDidtheInjuryOccur, "When Did the Injury Occur");
                        if (cl < 0 || cl > 3)
                            AddError("'{0}' is not a valid sharps injury timeframe, it must be an integer of 0, 1, 2 or 3", Normalize(WC_WhenDidtheInjuryOccur));
                        else if (cl.HasValue)
                            WorkRelated.SharpsInfo.WhenDidTheInjuryOccur = (WorkRelatedSharpsWhen)cl.Value;
                    }

                    cl = ParseInt(WC_JobClassification, "Job Classification");
                    if (cl < 0 || cl > 10)
                        AddError("'{0}' is not a valid sharps job classification, it must be an integer of 0 through 10", Normalize(WC_JobClassification));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.JobClassification = (WorkRelatedSharpsJobClassification)cl.Value;

                    if (WorkRelated.SharpsInfo.JobClassification == WorkRelatedSharpsJobClassification.Other)
                        WorkRelated.SharpsInfo.JobClassificationOther = Normalize(WC_JobClassificationOther);


                    cl = ParseInt(WC_LocationandDepartment, "Location and Department");
                    if (cl < 0 || cl > 8)
                        AddError("'{0}' is not a valid sharps job location/department, it must be an integer of 0 through 8", Normalize(WC_LocationandDepartment));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.LocationAndDepartment = (WorkRelatedSharpsLocation)cl.Value;

                    if (WorkRelated.SharpsInfo.LocationAndDepartment == WorkRelatedSharpsLocation.Other)
                        WorkRelated.SharpsInfo.JobClassificationOther = Normalize(WC_LocationandDepartmentOther);

                    cl = ParseInt(WC_SharpsProcedure, "Sharps Procedure");
                    if (cl < 0 || cl > 8)
                        AddError("'{0}' is not a valid sharps procedure, it must be an integer of 0 through 8", Normalize(WC_SharpsProcedure));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.Procedure = (WorkRelatedSharpsProcedure)cl.Value;

                    if (WorkRelated.SharpsInfo.Procedure == WorkRelatedSharpsProcedure.Other)
                    {
                        WorkRelated.SharpsInfo.ProcedureOther = Normalize(WC_SharpsProcedureOther);
                    }

                    WorkRelated.SharpsInfo.ExposureDetail = Normalize(WC_ExposureDetail);
                }

                #endregion

            }
            else if (RecType == RecordType.CaseAttachement)
            {
                #region Case Attachement


                EmployeeNumber = Normalize(EmployeeNumberOrdinal);
                LastName = Normalize(EmployeeLastNameOrdinal);
                FirstName = Normalize(EmployeeFirstNameOrdinal);
                CaseNumber = Normalize(CaseNumberOrdinal);
                AttachmentDate = ParseDate(AT_CreatedDate, "Attachment Created Date");
                int nc = ParseInt(AT_AttacheMentType, "Attachement Type") ?? 0;
                AttachmentType ncCon;
                if (Enum.TryParse<AttachmentType>(nc.ToString(), out ncCon))
                    AttachmentTypeValue = ncCon;
                else
                    AttachmentTypeValue = AttachmentType.Other;
                FileName = Normalize(AT_FileName);
                FileDescription = Normalize(AT_FileDescription);
                FileSource = Normalize(AT_FileSource);
                IsPublic = ParseBool(AT_IsPublic, "Is Public") ?? false;
                RepresentativeName = Normalize(AT_RepresentativeName);



                #endregion
            }
            else if (RecType == RecordType.CustomField)
            {
                #region Case Custom Fields


                CustomFieldCode = Normalize(CF_CustomFieldCodeOrdinal);
                CustomFieldValue = Normalize(CF_CustomFieldValueOrdinal);
                EmployerReferenceCode = Normalize(CF_EmployerReferenceCodeOrdinal);

                int nc = ParseInt(CF_EntityTargetOrdinal, "Entity Target") ?? 1;
                EntityTarget ncCon;
                if (Enum.TryParse<EntityTarget>(nc.ToString(), out ncCon))
                    EntityTargetValue = ncCon;
                else
                    EntityTargetValue = EntityTarget.Employee;

                #endregion
            }
            else if (RecType == RecordType.CaseConversionEligibility)
            {
                #region CaseConversionEligibility
                //
                // Employer Reference Code
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);

                //
                // Employee Last Name
                if (Required(EmployeeLastNameOrdinal, "Employee Last Name"))
                    LastName = Normalize(EmployeeLastNameOrdinal);
                //
                // Employee First Name
                if (Required(EmployeeFirstNameOrdinal, "Employee First Name"))
                    FirstName = Normalize(EmployeeFirstNameOrdinal);
                //
                // Employee Middle Name
                MiddleName = Normalize(E_EmployeeMiddleNameOrdinal);
                //
                // Job Title
                if (Required(E_JobTitleOrdinal, "Job Title"))
                    JobTitle = Normalize(E_JobTitleOrdinal);
                //
                // Job Location
                JobLocation = Normalize(E_JobLocationOrdinal);
                //
                // Work State
                // Work Country
                if (Required(E_WorkStateOrdinal, "Work State"))
                {
                    WorkState = Normalize(E_WorkStateOrdinal).ToUpperInvariant();
                    WorkCountry = (Normalize(E_WorkCountryOrdinal) ?? "US").ToUpperInvariant();
                    if (WorkCountry == "US")
                        ValidUSState(WorkState, "Work State");
                }
                //
                // Employee Phone Home
                PhoneHome = Normalize(E_PhoneHomeOrdinal);
                //
                // Employee Phone Work
                PhoneWork = Normalize(E_PhoneWorkOrdinal);
                //
                // Employee Phone Mobile
                PhoneMobile = Normalize(E_PhoneMobileOrdinal);
                //
                // Employee Phone Alt
                PhoneAlt = Normalize(E_PhoneAltOrdinal);
                //
                // Employee Email
                Email = Normalize(E_EmailOrdinal);
                ValidEmail(Email, "Employee Email");
                //
                // Employee Alt Email
                EmailAlt = Normalize(E_EmailAltOrdinal);
                ValidEmail(EmailAlt, "Employee Alt Email");
                //
                // Mailing Address
                Address = Normalize(E_AddressOrdinal);
                //
                // Mailing Address 2
                Address2 = Normalize(E_Address2Ordinal);
                //
                // Mailing City
                City = Normalize(E_CityOrdinal);
                //
                // Mailing State
                State = Normalize(E_StateOrdinal);
                //
                // Mailing Postal Code
                PostalCode = Normalize(E_PostalCodeOrdinal);
                //
                // Mailing Country
                Country = Normalize(E_CountryOrdinal);
                if (!string.IsNullOrWhiteSpace(State) && (string.IsNullOrWhiteSpace(Country) || Country == "US"))
                    ValidUSState(State, "Mailing State");
                if (string.IsNullOrWhiteSpace(Country) || Country == "US")
                    if (!string.IsNullOrWhiteSpace(PostalCode) && PostalCode.Length == 4)
                        PostalCode = string.Concat("0", PostalCode);
                //
                // PT/FT Status
                EmploymentType = Normalize(E_EmploymentTypeOrdinal);
                //
                // Manager Employee Number
                ManagerEmployeeNumber = Normalize(E_ManagerEmployeeNumberOrdinal);
                //
                // Manager Last Name
                ManagerLastName = Normalize(E_ManagerLastNameOrdinal);
                //
                // Manager First Name
                ManagerFirstName = Normalize(E_ManagerFirstNameOrdinal);
                //
                // Manager Phone
                ManagerPhone = Normalize(E_ManagerPhoneOrdinal);
                //
                // Manager Email
                ManagerEmail = Normalize(E_ManagerEmailOrdinal);
                ValidEmail(ManagerEmail, "Manager Email");
                //
                // HR Contact Employee Number
                HREmployeeNumber = Normalize(E_HREmployeeNumberOrdinal);
                //
                // HR Contact Last Name
                HRLastName = Normalize(E_HRLastNameOrdinal);
                //
                // HR Contact First Name
                HRFirstName = Normalize(E_HRFirstNameOrdinal);
                //
                // HR Contact Phone
                HRPhone = Normalize(E_HRPhoneOrdinal);
                //
                // HR Contact Email
                HREmail = Normalize(E_HREmailOrdinal);
                ValidEmail(HREmail, "HR Contact Email");
                //
                // Spouse Employee Number
                SpouseEmployeeNumber = Normalize(E_SpouseEmployeeNumberOrdinal);
                //
                // Date of Birth
                if (Required(E_DateOfBirthOrdinal, "Date of Birth"))
                    DateOfBirth = ParseDate(E_DateOfBirthOrdinal, "Date of Birth");
                //
                // Gender
                if (Required(E_GenderOrdinal, "Gender"))
                {
                    Gender = ParseChar(E_GenderOrdinal, "Date of Birth");
                    if (Gender.HasValue && Gender != 'F' && Gender != 'M' && Gender != 'U')
                        AddError("'{0}' is not a recognized employee gender assignment code, please provide the value 'F' (Female), 'M' (Male) or 'U' (Unknown/Not Collected)", Gender);
                }
                //
                // Exemption Status
                if (Required(E_ExemptionStatusOrdinal, "Exemption Status"))
                {
                    ExemptionStatus = ParseChar(E_ExemptionStatusOrdinal, "Exemption Status");
                    if (ExemptionStatus.HasValue && ExemptionStatus != 'E' && ExemptionStatus != 'N')
                        AddError("'{0}' is not a recognized employee exemption status indicator, please provide the value 'E' (Exempt) or 'N' (Non-Exempt)", ExemptionStatus);
                }
                //
                // Meets 50 in 75
                Meets50In75 = ParseBool(E_Meets50In75Ordinal, "Meets 50 in 75");
                //
                // Key Employee
                KeyEmployee = ParseBool(E_KeyEmployeeOrdinal, "Key Employee");
                //
                // Military Status
                MilitaryStatus = ParseByte(E_MilitaryStatusOrdinal, "Military Status");
                if (MilitaryStatus.HasValue && (MilitaryStatus < 0 || MilitaryStatus > 2))
                    AddError("'{0}' is not a valid value for 'Military Status'. Valid values are bytes 0 (Civilian), 1 (Active Duty) or 2 (Veteran)", MilitaryStatus);
                //
                // Employment Status
                if (Required(E_EmploymentStatusOrdinal, "Employment Status"))
                {
                    EmploymentStatus = ParseChar(E_EmploymentStatusOrdinal, "Employment Status");
                    if (EmploymentStatus.HasValue && !EmploymentStatusValue.IsValidValue(EmploymentStatus.Value))
                        AddError("'{0}' is not a valid value for 'Employment Status'. Valid values: {1}",
                            EmploymentStatus,
                            EmploymentStatusValue.GetCodeLookupAsString()
                        );
                }
                //
                // Termination Date
                TerminationDate = ParseDate(E_TerminationDateOrdinal, "Termination Date");
                //
                // Pay Rate
                PayRate = ParseDecimal(E_PayRateOrdinal, "Pay Rate");
                //
                // Pay Type
                PayType = ParseByte(E_PayTypeOrdinal, "Pay Type");
                if (PayType.HasValue && PayType != 1 && PayType != 2)
                    AddError("'{0}' is not a valid value for 'Pay Type'. Valid values are bytes 1 (Annual Salary) or 2 (Hourly)", PayType);
                //
                // Hire Date
                if (Required(E_HireDateOrdinal, "Hire Date"))
                {
                    HireDate = ParseDate(E_HireDateOrdinal, "Hire Date");
                    if (TerminationDate.HasValue && HireDate.HasValue && TerminationDate < HireDate)
                        AddError("The Termination Date, '{0:MM/dd/yyyy}', may not come before or on the Hire Date, '{1:MM/dd/yyyy}'", TerminationDate, HireDate);
                }
                //
                // Rehire Date
                RehireDate = ParseDate(E_RehireDateOrdinal, "Rehire Date");
                if (RehireDate.HasValue && HireDate.HasValue && RehireDate < HireDate)
                    AddError("The Rehire Date, '{0:MM/dd/yyyy}', may not come before the Hire Date, '{1:MM/dd/yyyy}'", RehireDate, HireDate);
                //
                // Adjusted Service Date
                if (Required(E_AdjustedServiceDateOrdinal, "Adjusted Service Date"))
                {
                    AdjustedServiceDate = ParseDate(E_AdjustedServiceDateOrdinal, "Adjusted Service Date");
                    if (TerminationDate.HasValue && AdjustedServiceDate.HasValue && TerminationDate < AdjustedServiceDate)
                        AddError("The Termination Date, '{0:MM/dd/yyyy}', may not come before or on the Adjusted Service Date, '{1:MM/dd/yyyy}'", TerminationDate, AdjustedServiceDate);
                }
                //
                // Scheduled Minutes per Week

                MinutesPerWeek = ParseInt(E_MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);

                // Hours Worked in 12 Months

                HoursWorkedIn12Months = ParseDecimal(E_HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);

                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(E_WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(E_WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(E_WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(E_WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(E_WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(E_WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(E_WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(E_VariableScheduleOrdinal, "Variable Schedule Flag");
                //
                // Job Description
                JobDescription = Normalize(E_JobDescriptionOrdinal);
                //
                // Job Classification
                JobClassification = ParseByte(E_JobClassificationOrdinal, "Job Classification");
                if (JobClassification.HasValue && (JobClassification < 1 || JobClassification > 5))
                    AddError("The value '{0}' is not a valid 'Job Classification'. Valid values are bytes 1 (Sedentary Work), 2 (Light Work), 3 (Medium Work), 4 (Heavy Work) or 5 (Very Heavy Work)", JobClassification);
                //
                // US Census Job Category Code
                USCensusJobCategoryCode = Normalize(E_USCensusJobCategoryCodeOrdinal);
                //
                // SOC Code
                SOCCode = Normalize(E_SOCCodeOrdinal);
                //
                // Schedule Effective Date
                ScheduleEffectiveDate = ParseDate(E_ScheduleEffectiveDateOrdinal, "Schedule Effective Date");
                //
                // SSN
                SSN = Normalize(E_SSNOrdinal);
                //
                // Pay Schedule Name
                PayScheduleName = Normalize(E_PayScheduleNameOrdinal);


                //
                // Start Day of Week
                byte? dow = ParseByte(E_StartDayOfWeekOrdinal, "Start Day of Week");
                if (dow.HasValue)
                {
                    if (dow < 0 || dow > 6)
                        AddError("The value'{0}' is not a valid 'Day of Week'. Valid values are bytes 0 (Sunday) through 6 (Saturday)", dow);
                    else
                        StartDayOfWeek = (DayOfWeek)dow.Value;
                }

                CaseNumber = Normalize(E_CaseNumberOrdinal);

                // Scheduled Hours Worked

                AverageMinutesWorkedPerWeek = ParseInt(E_AverageMinutesWorkedPerWeekOrdinal, "Average minutes worked per week");
                if (AverageMinutesWorkedPerWeek.HasValue && (AverageMinutesWorkedPerWeek > 10080 || AverageMinutesWorkedPerWeek < 0))
                    AddError("The 'Average minutes worked per week' of '{0}' is invalid. Value may not be less than zero and must be less than 10,080 minutes", AverageMinutesWorkedPerWeek);
                #endregion
            }
            else if (RecType == RecordType.Relapse)
            {
                if (Required(R_UniqueCaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(R_UniqueCaseNumberOrdinal);
                }
                caseRowForRelapse = new CaseRowForRelapse(rowSource);

            }
            else if (RecType == RecordType.Certification)
            {
                if (Required(X_UniqueCaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(X_UniqueCaseNumberOrdinal);
                }

                caseRowForCertification = new CaseRowForCertification(rowSource);
            }
            else if (RecType == RecordType.Accommodation)
            {
                caseRowForAccommodation = new CaseRowForAccommodation(rowSource);
                if (caseRowForAccommodation.Error != null)
                {
                    caseRowForAccommodation.Error.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList().ForEach(
                        err =>
                        {
                            _errors.Add(err);
                        });
                }
            }
            else if (RecType == RecordType.Segment)
            {
                if (Required(S_UniqueCaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(S_UniqueCaseNumberOrdinal);
                }
                caseRowForSegment = new CaseRowForSegment(rowSource);
            }

                return IsError;

        }

        #endregion Parsing

        #region Apply
        public ApplicationResult ApplyEmployee(string customerId, string employerId, string userId, List<Employee> lstEmp, List<EmployeeClass> employeeClassTypes)
        {


            if (IsError)
                return ApplicationResult.Fail;
            userId = userId ?? "000000000000000000000000";
            try
            {
                UpdateCaseConversionEligibility(employerId, customerId, userId, lstEmp, employeeClassTypes);
                return ApplicationResult.Success;
            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                Log.Error("Error processing Case Elgibility row", aggEx);
                foreach (var msg in aggEx.Messages)
                    AddError("Error processing Case Elgibility row, {0}", msg);
                AddError("Aggregate Error processing Case Elgibility row, {0}", aggEx.ToString());

                return ApplicationResult.Fail;
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Case Elgibility row", ex);
                AddError("Error processing Case Elgibility row, {0}", ex.ToString());
                return ApplicationResult.Fail;
            }
        }//end: ApplyEmployee


        /// <summary>
        /// Applies the properties in this record to the target case, employing any necessary additional validation
        /// special workflow or other application rules for migrating the case.
        /// </summary>
        /// <returns></returns>
        public ApplicationResult Apply(string customerId, string employerId, string userId = "000000000000000000000000", string attachmentfilePath = null, IEnumerable<CustomField> customFields = null, List<Employee> lstEmp = null)
        {
            if (IsError)
                return ApplicationResult.Fail;
            userId = userId ?? "000000000000000000000000";


            try
            {
                // Try to find the case.
                List<IMongoQuery> query = new List<IMongoQuery>();
                query.Add(Case.Query.EQ(c => c.CustomerId, customerId));
                query.Add(Case.Query.EQ(c => c.EmployerId, employerId));
                query.Add(Case.Query.EQ(c => c.CaseNumber, CaseNumber));

                Case myCase = Case.Query.Find(Case.Query.And(query.ToArray())).FirstOrDefault();
                if (RecType != RecordType.Case && myCase == null)
                {
                    AddError("The case #{0} could not be found or does not exist", CaseNumber);
                    return ApplicationResult.Fail;
                }
                Employee emp = null;

                if (lstEmp != null)
                {
                    // Variable name speaks for itself.
                    Action bsUglyAssSharedIEnumerableHackThatIsTooMuchToRefactorRightNowSoFuckIt = null;
                    bsUglyAssSharedIEnumerableHackThatIsTooMuchToRefactorRightNowSoFuckIt = () =>
                    {
                        try
                        {
                            /*
                             * May Throw:
                             * Error processing Case row, System.InvalidOperationException: Collection was modified; enumeration operation may not execute.
                             *    at System.Collections.Generic.List`1.Enumerator.MoveNextRare()
                             *    at System.Linq.Enumerable.WhereListIterator`1.MoveNext()
                             *    at System.Linq.Enumerable.FirstOrDefault[TSource](IEnumerable`1 source)
                             *    at AbsenceSoft.Logic.Processing.CaseRow.Apply(String customerId, String employerId, String userId, String attachmentfilePath, 
                             *      IEnumerable`1 customFields, List`1 lstEmp) in ..\AbsenceSoft.Logic\Processing\CaseRow.cs:line 1174
                             * See: https://stackoverflow.com/questions/11103779/are-ienumerable-linq-methods-thread-safe
                            */
                            emp = lstEmp.Where(e => e.EmployerId == employerId && e.CustomerId == customerId && e.EmployeeNumber == EmployeeNumber).FirstOrDefault();
                        }
                        catch (InvalidOperationException opEx)
                        {
                            if (opEx.Message.StartsWith("Collection was modified"))
                                bsUglyAssSharedIEnumerableHackThatIsTooMuchToRefactorRightNowSoFuckIt();
                            else
                                throw;
                        }
                    };
                    bsUglyAssSharedIEnumerableHackThatIsTooMuchToRefactorRightNowSoFuckIt();
                }

                if (emp == null)
                {
                    emp = Employee.AsQueryable().Where(e => e.EmployerId == employerId && e.CustomerId == customerId && e.EmployeeNumber == EmployeeNumber).FirstOrDefault();
                    if (emp == null)
                    {
                        AddError("Employee with employee number of '{0}' was not found.", EmployeeNumber);
                        return ApplicationResult.Fail;
                    }
                }
                switch (RecType)
                {
                    case RecordType.Case:
                    default:
                        #region Case
                        if (myCase != null && !string.IsNullOrWhiteSpace(myCase.Id))
                        {
                            AddError("Cannot create case, case already exists. Please use the AbsenceTracker application to modify/change this case.");
                            return ApplicationResult.Fail;
                        }
                        //
                        // Stage Case
                        myCase = new Case()
                        {
                            CustomerId = customerId,
                            EmployerId = employerId,
                            Employee = emp,
                            CreatedById = userId,
                            ModifiedById = userId,
                            EmployerCaseNumber = CaseNumber,
                            CaseNumber = CaseNumber,
                            Description = "Migrated to AbsenceTracker",
                            StartDate = ExpectedStartDate.Value,
                            EndDate = (ConfirmedRTWDate ?? CaseClosedDate ?? ExpectedRTWDate),
                            Status = CaseStatus ?? Data.Enums.CaseStatus.Open
                        };
                        //
                        // Date Opened
                        myCase.SetCaseEvent(CaseEventType.CaseCreated, DateCaseOpened ?? DateTime.UtcNow.ToMidnight());
                        //
                        // Absence Reason
                        AbsenceReason reason = AbsenceReason.GetByCode(ReasonForLeave, customerId, employerId, true);
                        if (reason == null)
                        {
                            AddError("The Reason for Leave code provided of '{0}' was not found, is invalid or is not yet configured for the employer.", ReasonForLeave);
                            return ApplicationResult.Fail;
                        }
                        myCase.Reason = reason;

                        // if this is an accommodation case then set the flag                        
                        myCase.IsAccommodation = Convert.ToBoolean(AbsenceReasonFlag.ShowAccommodation);

                        //
                        // Family Relationship / Case Contact
                        if (FamilyRelationship != null)
                            myCase.Contact = EmployeeContact.AsQueryable().Where(c => c.EmployeeId == emp.Id && c.ContactTypeCode == FamilyRelationship.Code).FirstOrDefault() ?? new EmployeeContact()
                            {
                                Employee = emp,
                                EmployeeId = emp.Id,
                                EmployeeNumber = emp.EmployeeNumber,
                                EmployerId = employerId,
                                CustomerId = customerId,
                                CreatedById = userId,
                                ModifiedById = userId,
                                MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                                ContactTypeCode = FamilyRelationship.Code,
                                ContactTypeName = FamilyRelationship.Name,
                                Contact = new Data.Contact()
                                {
                                    FirstName = string.Concat(emp.FirstName, emp.FirstName.EndsWith("s") ? "'" : "'s"),
                                    LastName = FamilyRelationship.Name
                                }
                            };
                        //
                        // Case Segment
                        CaseSegment seg = myCase.Segments.AddFluid(new CaseSegment());
                        seg.Type = LeaveInterval.Value;
                        seg.Status = CaseStatus ?? Data.Enums.CaseStatus.Open;
                        seg.StartDate = myCase.StartDate;
                        seg.EndDate = myCase.EndDate;
                        //
                        // Intermittent Certification/Schedule
                        if (seg.Type == CaseType.Intermittent)
                        {
                            if (IntermittentFrequency.HasValue || IntermittentOccurrences.HasValue || IntermittentDuration.HasValue)
                            {
                                Certification cert = myCase.Certifications.AddFluid(new Certification());
                                cert.StartDate = myCase.StartDate;
                                cert.EndDate = myCase.EndDate ?? DateTime.MaxValue;
                                cert.Frequency = IntermittentFrequency ?? 0;
                                cert.FrequencyType = IntermittentFrequencyUnit ?? Unit.Days;
                                cert.FrequencyUnitType = DayUnitType.Workday;
                                cert.Occurances = IntermittentOccurrences ?? 0;
                                cert.Duration = IntermittentDuration ?? 0;
                                cert.DurationType = IntermittentDurationUnit ?? Unit.Hours;
                            }
                        }
                        //
                        // Reduced Schedule
                        if (seg.Type == CaseType.Reduced)
                        {
                            LeaveOfAbsence loa = new LeaveOfAbsence()
                            {
                                Case = myCase,
                                Employer = myCase.Employer,
                                Customer = myCase.Employer.Customer,
                                ActiveSegment = seg,
                                Employee = emp,
                                WorkSchedule = emp.WorkSchedules
                            };

                            DateTime startDate = myCase.StartDate.GetFirstDayOfWeek();
                            DateTime endDate = startDate.AddDays(7);

                            var times = loa.MaterializeSchedule(startDate, endDate, true);
                            int diff = times.Sum(t => t.TotalMinutes ?? 0) - ReducedSchedule.Value;
                            int offset = Convert.ToInt32(Math.Floor(((decimal)diff / 7M)));
                            int adder = diff % 7;

                            seg.LeaveSchedule = new List<Data.Schedule>(1);
                            var sched = seg.LeaveSchedule.AddFluid(new Data.Schedule()
                            {
                                StartDate = startDate,
                                EndDate = myCase.EndDate,
                                ScheduleType = ScheduleType.Weekly
                            });
                            foreach (var t in times)
                                sched.Times.Add(new Data.Time()
                                {
                                    SampleDate = t.SampleDate,
                                    TotalMinutes = t.TotalMinutes.HasValue && t.TotalMinutes > 0 ? t.TotalMinutes.Value - offset : t.TotalMinutes
                                });
                            if (adder > 0)
                            {
                                var lastTimeWithTime = sched.Times.LastOrDefault(t => t.TotalMinutes.HasValue && t.TotalMinutes >= adder);
                                if (lastTimeWithTime != null)
                                    lastTimeWithTime.TotalMinutes = lastTimeWithTime.TotalMinutes.Value - adder;
                            }
                        }
                        //
                        // Date Closed
                        if (CaseClosedDate.HasValue || myCase.Status == Data.Enums.CaseStatus.Closed || myCase.Status == Data.Enums.CaseStatus.Cancelled || ConfirmedRTWDate.HasValue)
                        {
                            if (myCase.Status == Data.Enums.CaseStatus.Cancelled)
                            {
                                myCase.ClosureReason = CaseClosureReason.LeaveCancelled;
                                myCase.CancelReason = CaseCancelReason.Other;
                                myCase.SetCaseEvent(CaseEventType.CaseCancelled, CaseClosedDate ?? DateTime.UtcNow.ToMidnight());
                            }
                            else
                                myCase.ClosureReason = ConfirmedRTWDate.HasValue ? CaseClosureReason.ReturnToWork : CaseClosureReason.Other;

                            if (myCase.Status == Data.Enums.CaseStatus.Closed)
                            {
                                myCase.SetCaseEvent(CaseEventType.CaseClosed, CaseClosedDate ?? ConfirmedRTWDate ?? DateTime.UtcNow.ToMidnight());
                            }
                            else
                            {
                                myCase.SetCaseEvent(CaseEventType.CaseClosed, ConfirmedRTWDate ?? CaseClosedDate ?? DateTime.UtcNow.ToMidnight());
                            }

                        }
                        //
                        // Event Dates
                        if (ConfirmedRTWDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.ReturnToWork, ConfirmedRTWDate.Value);
                        if (DeliveryDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.DeliveryDate, DeliveryDate.Value);
                        if (BondingStartDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.BondingStartDate, BondingStartDate.Value);
                        if (BondingEndDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.BondingEndDate, BondingEndDate.Value);
                        if (IllnessOrInjuryDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, IllnessOrInjuryDate.Value);
                        if (HospitalAdmissionDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.HospitalAdmissionDate, HospitalAdmissionDate.Value);
                        if (HospitalReleaseDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.HospitalReleaseDate, HospitalReleaseDate.Value);
                        if (RTWReleaseDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.ReleaseReceived, RTWReleaseDate.Value);

                        //
                        // Auto Policies
                        if (AutoPolicies == true)
                        {
                            try
                            {
                                using (EligibilityService svc = new EligibilityService())
                                    myCase = svc.RunEligibility(myCase).Case;
                            }
                            catch (Exception ex)
                            {
                                AddError("Unable to automatically calculate eligibility.");
                                AddError(ex.ToString());
                                Log.Error(string.Format("Unable to automatically calculate eligibility for case migration of case '{0}'", myCase.EmployerCaseNumber), ex);
                                return ApplicationResult.Fail;
                            }
                        }

                        //
                        // Save Case Contact
                        if (myCase.Contact != null && myCase.Contact.IsNew)
                            myCase.Contact.Save();

                        //
                        // Save Case
                        using (CaseService svc = new CaseService(Workflow == true))
                            myCase = svc.UpdateCase(myCase, CaseEventType.CaseCreated);

                        //
                        // Create a migration case note
                        new CaseNote()
                        {
                            CustomerId = customerId,
                            EmployerId = employerId,
                            CaseId = myCase.Id,
                            Category = NoteCategoryEnum.CaseSummary,
                            CreatedById = userId,
                            ModifiedById = userId,
                            Public = false,
                            Notes = "Case migrated into AbsenceTracker from external source"
                        }.Save();

                        //
                        // Case Review ToDo Item
                        new ToDoItem()
                        {
                            Case = myCase,
                            CaseId = myCase.Id,
                            AssignedToId = myCase.AssignedToId,
                            AssignedToName = myCase.AssignedToName,
                            AssignedTo = myCase.AssignedTo,
                            CaseNumber = myCase.CaseNumber,
                            CreatedById = userId,
                            CustomerId = customerId,
                            Employee = myCase.Employee,
                            EmployeeNumber = myCase.Employee.EmployeeNumber,
                            EmployeeId = emp.Id,
                            EmployeeName = emp.FullName,
                            EmployerId = employerId,
                            ModifiedById = userId,
                            Optional = true,
                            Priority = ToDoItemPriority.High,
                            Status = ToDoItemStatus.Pending,
                            Weight = 0M,
                            DueDate = DateTime.UtcNow.AddDays(1),
                            ItemType = ToDoItemType.CaseReview,
                            Title = "Migration Case Review"
                        }.Save();
                        break;
                    #endregion Case
                    case RecordType.Adjudication:
                        #region Adjudication
                        DateTime adjStartDate = AdjudicationStartDate.Value;
                        DateTime adjEndDate = AdjudicationEndDate ?? myCase.EndDate ?? DateTime.MaxValue;
                        Data.Enums.AdjudicationStatus status = AdjudicationStatus ?? Data.Enums.AdjudicationStatus.Pending;
                        string denialReason = status == Data.Enums.AdjudicationStatus.Denied ? AdjudicationDenialReason.Other : null;
                        string explanation = status == Data.Enums.AdjudicationStatus.Denied ? "Case Migrated as Denied" : null;

                        seg = myCase.Segments.FirstOrDefault(s => adjStartDate.DateInRange(s.StartDate, s.EndDate));
                        if (seg == null)
                        {
                            AddError("The adjudication date(s) '{0:MM/dd/yyyy} - {1:MM/dd/yyyy}' provided are outside of the valid range for the case.", adjStartDate, adjEndDate);
                            return ApplicationResult.Fail;
                        }

                        if (!string.IsNullOrWhiteSpace(PolicyCode))
                        {
                            var policy = Policy.GetByCode(PolicyCode, customerId, employerId);
                            if (policy == null)
                            {
                                AddError("The policy code '{0}' supplied for adjudication on case '{1}' was not found in the system. Please ensure this policy is properly configured.", PolicyCode, myCase.CaseNumber);
                                return ApplicationResult.Fail;
                            }
                            if (!myCase.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.Code == policy.Code))
                            {
                                // Add the manual policy
                                myCase = new EligibilityService().Using(s => s.AddManualPolicy(myCase, policy.Code, null));
                                // And then we also need to re-run calcs
                                myCase = new CaseService().Using(s => s.RunCalcs(myCase));
                            }
                        }

                        using (CaseService svc = new CaseService(Workflow == true))
                        {
                            try
                            {
                                myCase = svc.ApplyDetermination(myCase, PolicyCode, adjStartDate, adjEndDate, status, denialReason, explanation);
                                if (MinutesUsed.HasValue && MinutesUsed.Value > 0 && AdjudicationStatus == Data.Enums.AdjudicationStatus.Approved)
                                {
                                    // We have to modify the work schedule; if it's a consecutive leave, then the base work schedule, if it's a reduced
                                    //  schedule leave, then we have to modify both the consecutive and reduced schedule
                                    switch (seg.Type)
                                    {
                                        case CaseType.Consecutive:
                                        case CaseType.Reduced:

                                            // TODO: Build a List<Time> representing a full 3 year spread around the case dates of the schedule.
                                            //      Essentially need to MaterializeSchedule from a LOA for the case, then adjust those dates in
                                            //      that materialized schedule that properly represent the minutes used passed in.

                                            // Get our schedule start date (it's a manual schedule so we build every day)
                                            DateTime curDate = adjStartDate;
                                            foreach (var policy in seg.AppliedPolicies.Where(a => string.IsNullOrWhiteSpace(PolicyCode) || a.Policy.Code == PolicyCode)
                                                .Where(a => a.Status != EligibilityStatus.Ineligible))
                                            {
                                                // Get the Usage collection for our date range
                                                var usages = policy.Usage.Where(u => u.DateUsed.DateInRange(adjStartDate, adjEndDate)).ToList();

                                                // Determine the total number of days the segment spans
                                                int days = usages.Count;
                                                if (days <= 0)
                                                    continue;

                                                // Get our minutes per day setting
                                                int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)(MinutesUsed.Value / days)));
                                                // Get the final diff to be distributed and front-loaded on the schedule for any remainder minutes
                                                int diff = MinutesUsed.Value - (dailyMinutes * days);
                                                // Loop through each day
                                                foreach (var usage in usages)
                                                {
                                                    usage.Determination = Data.Enums.AdjudicationStatus.Approved;
                                                    usage.DenialReasonCode = null;
                                                    usage.DenialReasonName = null;
                                                    usage.DenialExplanation = null;
                                                    usage.MinutesUsed = dailyMinutes;
                                                    usage.IsLocked = true;
                                                    usage.UserEntered = true;
                                                    // Front-load our odd-ball time 1 minutes at a time per day
                                                    if (diff > 0)
                                                    {
                                                        usage.MinutesUsed += 1;
                                                        diff--;
                                                    }
                                                    usage.MinutesInDay = usage.MinutesInDay < usage.MinutesUsed ? usage.MinutesUsed : usage.MinutesInDay;
                                                }
                                                // Ensure we've accounted for all of our diff (we should have 'cause that's how math works, but just in case)
                                                while (diff > 0)
                                                {
                                                    // Evenly distribute usages over the trailing parts of the usage collection.
                                                    for (var i = usages.Count - 1; i >= 0; i--)
                                                    {
                                                        var lastUsage = usages[i];
                                                        lastUsage.MinutesUsed += 1;
                                                        lastUsage.MinutesInDay = lastUsage.MinutesInDay < lastUsage.MinutesUsed ? lastUsage.MinutesUsed : lastUsage.MinutesInDay;
                                                        diff--;
                                                    }
                                                }

                                                // now that the data as been applied to the usage collection we need to fix up
                                                // the part the tells us how to use that usage 
                                                FixUpAppliedPoicyUsage(usages, myCase);
                                            }
                                            break;
                                        case CaseType.Administrative:
                                            // If it's Administrative return an error
                                            AddError("You may not supply Minutes Used for Administrative cases, these cases do not track 'time/usage' only duration.");
                                            return ApplicationResult.Fail;
                                        case CaseType.Intermittent:
                                        default:
                                            // If it's intermittent then these are TOR's; return an error
                                            AddError("You may not supply Minutes Used for intermittent cases, use the Intermittent Time Used record(s) instead for this purpose.");
                                            return ApplicationResult.Fail;
                                    }

                                    // Re-Calc with our saved usage
                                    myCase = svc.RunCalcs(myCase).Save();
                                }
                            }
                            catch (AbsenceSoftAggregateException aggEx)
                            {
                                if (aggEx.Messages != null && aggEx.Messages.Any())
                                    foreach (var msg in aggEx.Messages)
                                        AddError(msg);
                                else
                                    AddError(aggEx.Message);
                                return ApplicationResult.Fail;
                            }
                            catch (AbsenceSoftException absEx)
                            {
                                AddError(absEx.Message);
                                return ApplicationResult.Fail;
                            }
                            catch (Exception ex)
                            {
                                AddError(ex.Message);
                                Log.Error(string.Format("Error applying adjudication during case migration for case '{0}'", myCase.Id), ex);
                                return ApplicationResult.Fail;
                            }
                        }
                        break;
                    #endregion Adjudication
                    case RecordType.Intermittent:
                        #region Intermittent
                        using (CaseService svc = new CaseService(Workflow == true))
                        {
                            string pId = null;
                            if (!string.IsNullOrWhiteSpace(PolicyCode))
                            {
                                var policy = Policy.GetByCode(PolicyCode, customerId, employerId);
                                if (policy == null)
                                {
                                    AddError("The policy code '{0}' supplied for intermittent time on  '{2:MM/dd/yyyy}' for case '{1}' was not found in the system. Please ensure this policy is properly configured.", PolicyCode, myCase.CaseNumber, DateOfAbsence);
                                    return ApplicationResult.Fail;
                                }
                                pId = policy.Code;
                            }
                            var intSeg = myCase.Segments.FirstOrDefault(s => s.Type == CaseType.Intermittent && DateOfAbsence.Value.DateInRange(s.StartDate, s.EndDate));
                            if (intSeg == null)
                            {
                                AddError("No intermittent period for this case was found for date '{0:MM/dd/yyyy}'.", DateOfAbsence);
                                return ApplicationResult.Fail;
                            }
                            List<IntermittentTimeRequest> times = new List<IntermittentTimeRequest>();
                            IntermittentTimeRequest t = null;
                            if (intSeg.UserRequests != null)
                                t = intSeg.UserRequests.FirstOrDefault(r => r.RequestDate == DateOfAbsence);
                            if (t == null)
                                t = new IntermittentTimeRequest()
                                {
                                    EmployeeEntered = false,
                                    RequestDate = DateOfAbsence.Value,
                                    Notes = "Created through Case Migration"
                                };
                            times.Add(t);
                            t.TotalMinutes = (MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0);
                            if (!string.IsNullOrWhiteSpace(pId) && !intSeg.AppliedPolicies.Any(p => p.Policy.Code == pId))
                            {
                                AddError("The policy '{0}' was not found covering any intermittent period of time for the case and may not be used.", PolicyCode);
                                return ApplicationResult.Fail;
                            }
                            List<IntermittentTimeRequestDetail> detail = new List<IntermittentTimeRequestDetail>();
                            detail.AddRange(intSeg.AppliedPolicies.Where(p => (pId == null || pId == p.Policy.Code) && p.Status == EligibilityStatus.Eligible)
                                .Select(p => new IntermittentTimeRequestDetail()
                                {
                                    PolicyCode = p.Policy.Code,
                                    Approved = MinutesApproved ?? 0,
                                    Denied = MinutesDenied ?? 0,
                                    Pending = MinutesPending ?? 0,
                                    DenialReasonCode = MinutesDenied.HasValue && MinutesDenied > 0 ? AdjudicationDenialReason.Other : null,
                                    DenialReasonName = MinutesDenied.HasValue && MinutesDenied > 0 ? AdjudicationDenialReason.OtherDescription : null,
                                    DeniedReasonOther = MinutesDenied.HasValue && MinutesDenied > 0 ? "Denied through Case Migration" : null
                                }));
                            detail.AddRange(intSeg.AppliedPolicies.Where(p => (pId != null && pId != p.Policy.Code) && p.Status == EligibilityStatus.Eligible)
                                .Select(p => new IntermittentTimeRequestDetail()
                                {
                                    PolicyCode = p.Policy.Code,
                                    Approved = 0,
                                    Denied = 0,
                                    Pending = MinutesPending ?? 0,
                                    DenialReasonCode = MinutesDenied.HasValue && MinutesDenied > 0 ? AdjudicationDenialReason.Other : null,
                                    DenialReasonName = MinutesDenied.HasValue && MinutesDenied > 0 ? AdjudicationDenialReason.OtherDescription : null,
                                    DeniedReasonOther = MinutesDenied.HasValue && MinutesDenied > 0 ? "Denied through Case Migration" : null
                                }));
                            foreach (var dt in detail)
                            {
                                var deet = t.Detail.FirstOrDefault(d => d.PolicyCode == dt.PolicyCode);
                                if (deet == null)
                                    t.Detail.Add(dt);
                                else
                                {
                                    deet.Approved = dt.Approved;
                                    deet.Denied = dt.Denied;
                                    deet.DenialReasonCode = dt.DenialReasonCode;
                                    deet.DenialReasonName = dt.DenialReasonName;
                                    deet.DeniedReasonOther = dt.DeniedReasonOther;
                                    deet.Pending = dt.Pending;
                                }
                            }

                            myCase = svc.CreateOrModifyTimeOffRequest(myCase, times);

                            svc.UpdateCase(myCase);
                        }
                        break;
                    #endregion Intermittent
                    case RecordType.Note:
                        #region Note
                        if (myCase == null || string.IsNullOrWhiteSpace(myCase.Id))
                        {
                            AddError("Cannot add note. Case does not exists.");
                            return ApplicationResult.Fail;
                        }

                        CaseNote cn = new CaseNote()
                        {
                            CaseId = myCase.Id,
                            Category = CaseNoteCategory,
                            CustomerId = customerId,
                            EmployerId = employerId,
                            Notes = NoteText,
                            Public = true,
                            EnteredByEmployeeNumber = NoteEnteredByEmployeeNumber,
                            EnteredByName = NoteEnteredByName,
                            CreatedById = userId,
                            ModifiedById = userId
                        };
                        cn.SetCreatedDate(NoteDate.Value);
                        cn.SetModifiedDate(NoteDate.Value);

                        cn.Save();

                        break;

                    #endregion Note
                    case RecordType.WorkRelated:
                        #region Work Related
                        if (WorkRelated != null)
                            myCase.Metadata.SetRawValue("IsWorkRelated", true);
                        //
                        // Determine if we need to set the illness or injury date
                        if (WorkRelated != null && WorkRelated.IllnessOrInjuryDate.HasValue)
                            myCase.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, WorkRelated.IllnessOrInjuryDate.Value);
                        else if (WorkRelated != null)
                            WorkRelated.IllnessOrInjuryDate = myCase.GetMostRecentEventDate(CaseEventType.IllnessOrInjuryDate);

                        //
                        // Determine if we need to save the provider contact
                        if (WorkRelated != null && WorkRelated.Provider != null && !string.IsNullOrWhiteSpace(WorkRelated.Provider.ContactTypeCode))
                        {
                            WorkRelated.Provider.CustomerId = myCase.CustomerId;
                            WorkRelated.Provider.EmployerId = myCase.EmployerId;
                            WorkRelated.Provider.EmployeeId = myCase.Employee.Id;
                            WorkRelated.Provider.Save();
                        }
                        //
                        // assign the Work related info
                        myCase.WorkRelated = WorkRelated;
                        //
                        // Save Case
                        using (CaseService svc = new CaseService(Workflow == true))
                            myCase = svc.UpdateCase(myCase, CaseEventType.CaseCreated);
                        break;
                    #endregion Work Related
                    case RecordType.CaseAttachement:
                        #region CaseAttachement
                        if (!string.IsNullOrWhiteSpace(FileName) && !IsValidFilename(FileName))
                        {
                            AddError("'{0}' is invalid", FileName);
                            return ApplicationResult.Fail;
                        }

                        if (!string.IsNullOrWhiteSpace(FileSource))
                        {
                            if (!CheckFilePresent(attachmentfilePath))
                            {
                                AddError("'{0}' File mentioned in FileSource not found or corrup", "<none>");
                                return ApplicationResult.Fail;
                            }
                        }

                        if (!AttachmentDate.HasValue)
                            AttachmentDate = DateTime.UtcNow;


                        UpdateAttachementConversion(employerId, customerId, myCase.Id, emp.Id);
                        break;
                    #endregion
                    case RecordType.CustomField:
                        #region CaseCustomFields
                        if (string.IsNullOrWhiteSpace(CustomFieldCode))
                        {
                            AddError("Custom Fields Code can't be blank");
                            return ApplicationResult.Fail;
                        }
                        if (string.IsNullOrWhiteSpace(CustomFieldValue))
                        {
                            AddError("Custom Fields Value can't be blank");
                            return ApplicationResult.Fail;
                        }

                        var customFieldToUpdate = customFields.Where(f => f.Target == EntityTargetValue)
                                  .FirstOrDefault(cf => cf.Code == CustomFieldCode)?.Clone();

                        if (customFieldToUpdate != null)
                        {
                            UpdateCustomFieldValue(ParseCustomFieldValue(customFieldToUpdate), customFieldToUpdate);

                            if (EntityTargetValue == EntityTarget.Employee)
                            {
                                int objIndex = myCase.Employee.CustomFields.FindIndex(cf => cf.Code == customFieldToUpdate.Code);
                                if (objIndex != -1)
                                {
                                    myCase.Employee.CustomFields[objIndex] = customFieldToUpdate;
                                }
                                else
                                {
                                    myCase.Employee.CustomFields.Add(customFieldToUpdate);
                                }

                            }
                            else
                            {
                                int objIndex = myCase.CustomFields.FindIndex(cf => cf.Code == customFieldToUpdate.Code);
                                if (objIndex != -1)
                                {
                                    myCase.CustomFields[objIndex] = customFieldToUpdate;
                                }
                                else
                                {
                                    myCase.CustomFields.Add(customFieldToUpdate);
                                }
                            }
                            new CaseService().Using(m => m.UpdateCase(myCase));
                        }
                        else
                        {
                            AddError("This row contains custom fields which are not configured");
                            return ApplicationResult.Fail;
                        }
                        break;
                    #endregion

                    case RecordType.Relapse:
                        return caseRowForRelapse.Apply(myCase, emp);
                        break;

                    case RecordType.Certification:
                        return caseRowForCertification.Apply(myCase, emp);
                        break;
                    case RecordType.Accommodation:
                        var accomodationResult = caseRowForAccommodation.Apply(myCase, emp);
                        if (accomodationResult == ApplicationResult.Fail && caseRowForAccommodation.Error != null)
                        {
                            _errors = caseRowForAccommodation.Error.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                        }
                        return accomodationResult;
                    case RecordType.Segment:
                        return caseRowForSegment.Apply(myCase, emp);
                        break;
                }
                return ApplicationResult.Success;
            }
            catch (AbsenceSoftAggregateException aggEx)
            {
                Log.Error("Error processing Case row", aggEx);
                foreach (var msg in aggEx.Messages)
                    AddError("Error processing Case row, {0}", msg);
                AddError("Aggregate Error processing Case row, {0}", aggEx.ToString());

                return ApplicationResult.Fail;
            }
            catch (Exception ex)
            {
                Log.Error("Error processing Case row", ex);
                AddError("Error processing Case row, {0}", ex.ToString());
                return ApplicationResult.Fail;
            }
        }//end: Apply

        #endregion Apply

        #region ToString

        public override string ToString()
        {
            int cols = 29;
            switch (RecType)
            {
                case RecordType.Adjudication:
                case RecordType.Intermittent:
                    cols = 11;
                    break;
                case RecordType.Case:
                default:
                    cols = 29;
                    break;
                case RecordType.Note:
                    cols = 9;
                    break;
                case RecordType.CaseConversionEligibility:
                    cols = 65;
                    break;
                case RecordType.WorkRelated:
                    cols = 48;
                    break;
            }

            string[] fields = new string[cols];

            fields[RecordTypeOrdinal] = Emit((char)RecType);
            fields[EmployeeNumberOrdinal] = Emit(EmployeeNumber);
            fields[EmployeeLastNameOrdinal] = Emit(LastName);
            fields[EmployeeFirstNameOrdinal] = Emit(FirstName);
            fields[CaseNumberOrdinal] = Emit(CaseNumber);

            switch (RecType)
            {
                case RecordType.Adjudication:
                    fields[AdjudicationStartDateOrdinal] = Emit(AdjudicationStartDate);
                    fields[AdjudicationEndDateOrdinal] = Emit(AdjudicationEndDate);
                    fields[AdjudicationStatusOrdinal] = Emit((int?)AdjudicationStatus);
                    fields[MinutesUsedOrdinal] = Emit(MinutesUsed);
                    fields[PolicyCodeOrdinal] = Emit(PolicyCode);
                    fields[WorkflowOrdinal] = Emit(Workflow);
                    break;
                case RecordType.Intermittent:
                    fields[DateOfAbsenceOrdinal] = Emit(DateOfAbsence);
                    fields[MinutesApprovedOrdinal] = Emit(MinutesApproved);
                    fields[MinutesDeniedOrdinal] = Emit(MinutesDenied);
                    fields[MinutesPendingOrdinal] = Emit(MinutesPending);
                    fields[PolicyCodeOrdinal] = Emit(PolicyCode);
                    fields[WorkflowOrdinal] = Emit(Workflow);
                    break;
                case RecordType.Note:
                    fields[NOTE_NoteDate] = Emit(NoteDate);
                    fields[NOTE_Category] = Emit(CaseNoteCategory);
                    fields[NOTE_EnteredByName] = Emit(NoteEnteredByName);
                    fields[NOTE_Text] = Emit(NoteText);
                    break;
                case RecordType.WorkRelated:
                    fields[WC_Reportable] = Emit(WorkRelated.Reportable);
                    fields[WC_HealthcarePrivate] = Emit(WorkRelated.HealthCarePrivate);
                    fields[WC_WhereOccurred] = Emit(WorkRelated.WhereOccurred);
                    fields[WC_CaseClassification] = Emit((int?)WorkRelated.Classification);
                    fields[WC_TypeOfInjury] = Emit((int?)WorkRelated.TypeOfInjury);
                    fields[WC_DaysAwayfromWork] = Emit(WorkRelated.DaysAwayFromWork);
                    fields[WC_DaysontheJobTransferorRestriction] = Emit(WorkRelated.DaysOnJobTransferOrRestriction);
                    if (WorkRelated.Provider != null)
                    {
                        fields[WC_HealthcareProviderTypeCode] = Emit(WorkRelated.Provider.ContactTypeCode);
                        if (WorkRelated.Provider.Contact != null)
                        {
                            fields[WC_HealthcareProviderFirstName] = Emit(WorkRelated.Provider.Contact.FirstName);
                            fields[WC_HealthcareProviderLastName] = Emit(WorkRelated.Provider.Contact.LastName);
                            fields[WC_HealthcareProviderCompany] = Emit(WorkRelated.Provider.Contact.CompanyName);
                            if (WorkRelated.Provider.Contact.Address != null)
                            {
                                fields[WC_HealthcareProviderAddress1] = Emit(WorkRelated.Provider.Contact.Address.Address1);
                                fields[WC_HealthcareProviderAddress2] = Emit(WorkRelated.Provider.Contact.Address.Address2);
                                fields[WC_HealthcareProviderCity] = Emit(WorkRelated.Provider.Contact.Address.City);
                                fields[WC_HealthcareProviderState] = Emit(WorkRelated.Provider.Contact.Address.State);
                                fields[WC_HealthcareProviderCountry] = Emit(WorkRelated.Provider.Contact.Address.Country);
                                fields[WC_HealthcareProviderPostalCode] = Emit(WorkRelated.Provider.Contact.Address.PostalCode);
                            }
                        }
                    }
                    fields[WC_EmergencyRoom] = Emit(WorkRelated.EmergencyRoom);
                    fields[WC_HospitalizedOvernight] = Emit(WorkRelated.HospitalizedOvernight);
                    fields[WC_TimeEmployeeBeganWork] = Emit(WorkRelated.TimeEmployeeBeganWork);
                    fields[WC_TimeOfEvent] = Emit(WorkRelated.TimeOfEvent);
                    fields[WC_ActivityBeforeIncident] = Emit(WorkRelated.ActivityBeforeIncident);
                    fields[WC_WhatHappened] = Emit(WorkRelated.WhatHappened);
                    fields[WC_InjuryOrIllness] = Emit(WorkRelated.InjuryOrIllness);
                    fields[WC_WhatHarmedTheEmployee] = Emit(WorkRelated.WhatHarmedTheEmployee);
                    fields[WC_DateOfDeath] = Emit(WorkRelated.DateOfDeath);
                    fields[WC_Sharps] = Emit(WorkRelated.Sharps);
                    if (WorkRelated.SharpsInfo != null)
                    {
                        if (WorkRelated.SharpsInfo.InjuryLocation != null && WorkRelated.SharpsInfo.InjuryLocation.Any())
                            fields[WC_SharpsInjuryLocation] = Emit((int?)WorkRelated.SharpsInfo.InjuryLocation.First());
                        fields[WC_TypeOfSharp] = Emit(WorkRelated.SharpsInfo.TypeOfSharp);
                        fields[WC_BrandOfSharp] = Emit(WorkRelated.SharpsInfo.BrandOfSharp);
                        fields[WC_ModelOfSharp] = Emit(WorkRelated.SharpsInfo.ModelOfSharp);
                        fields[WC_BodyFluidInvolved] = Emit(WorkRelated.SharpsInfo.BodyFluidInvolved);
                        fields[WC_HaveEngineeredInjuryProtection] = Emit(WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection);
                        fields[WC_WasProtectiveMechanismActivated] = Emit(WorkRelated.SharpsInfo.WasProtectiveMechanismActivated);
                        fields[WC_WhenDidtheInjuryOccur] = Emit((int?)WorkRelated.SharpsInfo.WhenDidTheInjuryOccur);
                        fields[WC_JobClassification] = Emit(WorkRelated.SharpsInfo.JobClassification);
                        fields[WC_JobClassificationOther] = Emit(WorkRelated.SharpsInfo.JobClassificationOther);
                        fields[WC_LocationandDepartment] = Emit(WorkRelated.SharpsInfo.JobClassification);
                        fields[WC_LocationandDepartmentOther] = Emit(WorkRelated.SharpsInfo.JobClassificationOther);
                        fields[WC_SharpsProcedure] = Emit(WorkRelated.SharpsInfo.Procedure);
                        fields[WC_SharpsProcedureOther] = Emit(WorkRelated.SharpsInfo.ProcedureOther);
                        fields[WC_ExposureDetail] = Emit(WorkRelated.SharpsInfo.ExposureDetail);
                    }
                    break;
                case RecordType.Case:
                default:
                    fields[LeaveIntervalOrdinal] = Emit((int?)LeaveInterval);
                    fields[ReasonForLeaveOrdinal] = Emit(ReasonForLeave);
                    fields[FamilyRelationshipOrdinal] = Emit(FamilyRelationship != null ? Convert.ToInt16(FamilyRelationship.Id) : (int?)null);
                    fields[IntermittentFrequencyOrdinal] = Emit(IntermittentFrequency);
                    fields[IntermittentFrequencyUnitOrdinal] = Emit((int?)IntermittentFrequencyUnit);
                    fields[IntermittentOccurrencesOrdinal] = Emit(IntermittentOccurrences);
                    fields[IntermittentDurationOrdinal] = Emit(IntermittentDuration);
                    fields[IntermittentDurationUnitOrdinal] = Emit((int?)IntermittentDurationUnit);
                    fields[DateCaseOpenedOrdinal] = Emit(DateCaseOpened);
                    fields[ExpectedStartDateOrdinal] = Emit(ExpectedStartDate);
                    fields[ExpectedRTWDateOrdinal] = Emit(ExpectedRTWDate);
                    fields[ConfirmedRTWDateOrdinal] = Emit(ConfirmedRTWDate);
                    fields[CaseClosedDateOrdinal] = Emit(CaseClosedDate);
                    fields[CaseStatusOrdinal] = Emit((int?)CaseStatus);
                    fields[DeliveryDateOrdinal] = Emit(DeliveryDate);
                    fields[BondingStartDateOrdinal] = Emit(BondingStartDate);
                    fields[BondingEndDateOrdinal] = Emit(BondingEndDate);
                    fields[IllnessOrInjuryDateOrdinal] = Emit(IllnessOrInjuryDate);
                    fields[HospitalAdmissionDateOrdinal] = Emit(HospitalAdmissionDate);
                    fields[HospitalReleaseDateOrdinal] = Emit(HospitalReleaseDate);
                    fields[RTWReleaseDateOrdinal] = Emit(RTWReleaseDate);
                    fields[AutoPoliciesOrdinal] = Emit(AutoPolicies);
                    fields[ReducedScheduleOrdinal] = Emit(ReducedSchedule);
                    fields[CaseCreateWorkflowOrdinal] = Emit(Workflow);
                    break;
            }

            string record = string.Join(Delimiter.ToString(), fields);
            return record;
        }

        #endregion
        #region Create Case Attachement Via Conversion Process
        private bool ValidateEmployementType(string employeMentType, List<EmployeeClass> employeeClassTypes)
        {
            if (employeMentType == "1" || employeMentType == "2" || employeMentType == "3")
            {
                return true;
            }
            if (employeeClassTypes.Any(e => e.Code == employeMentType))
            {
                return true;
            }
            return false;
        }

        private string GetEmployementType(string employeMentType)
        {
            if (employeMentType == "1")
            {
                return WorkType.FullTime;
            }
            if (employeMentType == "2")
            {
                return WorkType.PartTime;
            }
            if (employeMentType == "3")
            {
                return WorkType.PerDiem;
            }

            return employeMentType;

        }

        private string GetEmploymentTypeName(string employmentType, List<EmployeeClass> employeeClassTypes)
        {
            if (employeeClassTypes != null && employeeClassTypes.Any(e => e.Code == employmentType))
            {
                return employeeClassTypes.FirstOrDefault(e => e.Code == employmentType).Name;
            }
            return employmentType;
        }

        Func<string, string> normalize = new Func<string, string>(s =>
        {
            if (string.IsNullOrWhiteSpace(s))
                return null;
            string data = s;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            data = data.Replace("\\n", Environment.NewLine);
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        });
        private bool IsValidFilename(string fileName)
        {
            string strTheseAreInvalidFileNameChars = new string(System.IO.Path.GetInvalidFileNameChars());
            Regex regInvalidFileName = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            if (regInvalidFileName.IsMatch(fileName)) { return false; };

            return true;
        }
        private bool CheckFilePresent(string attachmentFilePath)
        {

            string pathUptoAttachementDirectory = Path.Combine(attachmentFilePath, FileSource);
            if (System.IO.File.Exists(pathUptoAttachementDirectory))
            {
                if (string.IsNullOrEmpty(FileName))
                {
                    FileName = Path.GetFileName(pathUptoAttachementDirectory);
                }
                ContentType = System.Web.MimeMapping.GetMimeMapping(FileName);
                try
                {
                    using (FileStream fs = System.IO.File.OpenRead(pathUptoAttachementDirectory))
                    {
                        ContentLength = fs.Length;
                        byte[] buffer = new byte[fs.Length];
                        fs.Read(buffer, 0, buffer.Length);
                        fs.Close();
                        File = buffer;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("{1}: Failed to get file details updating  Case Attachement  '{0}'", FileName), ex);
                    return false;
                }
                return true;
            }


            return false;
        }

        private ApplicationResult UpdateAttachementConversion(string employerId, string customerId, string caseId, string employeeId)
        {

            Attachment attachMent = new Attachment();

            if (!AttachmentTypeValue.HasValue)
                attachMent.AttachmentType = AttachmentType.Other;
            else
                attachMent.AttachmentType = AttachmentTypeValue.Value;

            attachMent.CreatedByName = RepresentativeName;
            attachMent.Description = FileDescription;
            attachMent.FileName = FileName;
            attachMent.Public = IsPublic;
            attachMent.CaseId = caseId;
            attachMent.File = File;
            attachMent.ContentLength = ContentLength;
            attachMent.EmployeeNumber = EmployeeNumber;
            attachMent.EmployeeId = employeeId;
            attachMent.EmployerId = employerId;
            attachMent.CustomerId = customerId;
            attachMent.ContentType = ContentType;

            new AttachmentService().Using(svc => svc.CreateAttachment(attachMent));

            return ApplicationResult.Success;
        }

        #endregion

        /* OK,
        *
        * Here is what is going on and how we can go about fixing it. Take this with a grain of 
        * salt but I think this should work.
        *
        * Usage is measured as a percentage. Everything is a percentage. The system calcs the 
        * base units based on the employee's schedule. So if they are scheduled to work 40 hours 
        * and they take 8 hours off they have taken .2 of a week. This is how it works with all 
        * the pieces (even hourly). 
        *
        * Each days usage is stored in AppliedPolicyUsage (which you already know). On there is 
        * a collection called AvailabeToUse. This contains all the denominator value for each 
        * measure type. That way each day can figure out what percentage of usage it is.
        *
        * The problem is coming when you override the minutes used on the policy. It's in the 
        * 800s (I forgot the exact total.) The problem is all those denominators are all based 
        * on a 40 hour week. So you are basically telling it that each work day they took off 
        * 1.8 days and then you are locking it so it can't fix it up.
        *
        * What you will need to do is fix up the AvailableToUse collection and you will need to 
        * base it off of the data that you are trying to set. Calc out how many minutes you're saying 
        * they have over a day, week, month and year. 
        * 
        * You can see how this list is generated in AbsenceSoft.Logic.Cases.AppliedPolicyUsageHelper. 
        * It's really really simple and straight forward. 
        * 
        * I think that will fix it.  (and if you are reading this then it did)
        */
        private void FixUpAppliedPoicyUsage(List<AppliedPolicyUsage> usage, Case theCase)
        {
            // first we need to get a full schedule so, that we can calc out years and months that are 
            // not covered by the current usage
            // find their schedule across this time period
            LeaveOfAbsence los = new LeaveOfAbsence();
            los.Employee = theCase.Employee;
            los.Employer = theCase.Employee.Employer;
            los.Customer = theCase.Employee.Customer;
            los.WorkSchedule = Employee.GetById(theCase.Employee.Id).WorkSchedules;

            DateTime calcEndDate = theCase.EndDate.HasValue ? theCase.EndDate.Value.AddYears(2) : DateTime.UtcNow.AddYears(2);
            List<Time> schedule = los.MaterializeSchedule(theCase.StartDate.AddYears(-2), calcEndDate, false);

            // once we have the schedule, we only need to fix up the ones where the usage is locked (cuz
            // those are the ones we just messed with)
            List<AppliedPolicyUsage> theUsed = usage.Where(u => u.IsLocked.HasValue && u.IsLocked.Value).ToList();

            // we need a list that is the intersection of the the schedule and what was assigned
            // so that we can calculate long run totals (like yearly)
            List<Time> actual = new List<Time>();
            foreach (Time t in schedule)
            {
                AppliedPolicyUsage apolu = theUsed.FirstOrDefault(fd => fd.DateUsed == t.SampleDate);

                if (apolu == null)
                    actual.Add(t);
                else
                    actual.Add(new Time() { SampleDate = t.SampleDate, TotalMinutes = (int)apolu.MinutesInDay });
            }

            foreach (AppliedPolicyUsage apu in theUsed)
            {
                // only need to fix the ones that don't match the schedule
                Time theDay = schedule.FirstOrDefault(fd => fd.SampleDate == apu.DateUsed);

                // if it isn't there then it should be a 0
                if (theDay == null)
                    theDay = new Time() { TotalMinutes = 0, SampleDate = apu.DateUsed };

                // and if amount defined equals the amount asked for then we are good
                if (theDay.TotalMinutes == apu.MinutesInDay)
                    continue;

                int workMinutes = (int)apu.MinutesInDay;

                List<AppliedPolicyUsageType> newUsage = new List<AppliedPolicyUsageType>();

                AppliedPolicyUsageType wh = new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.WorkingHours,
                    BaseUnit = 0
                };

                // so we have to calculate out each of our types
                wh.BaseUnit = 1d / (workMinutes / 60d);
                newUsage.Add(wh);

                // work day is just 1 (it's always 1) (but we have to add it so the code will work)
                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.WorkDays,
                    BaseUnit = 1
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.FixedWorkDays,
                    BaseUnit = 1
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarDays,
                    BaseUnit = 1
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarDaysFromFirstUse,
                    BaseUnit = 1
                });

                // not sure what to do with this yet
                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.ReasonablePeriod,
                    BaseUnit = 1
                });

                // shortcut
                DateTime sampleDate = theDay.SampleDate;

                // take the sample date, get the first day of the week and then find out
                // how many work days there are that week
                DateTime startDate = sampleDate.GetFirstDayOfWeek();
                DateTime endDate = startDate.AddDays(6);
                int daysCounted = actual.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);

                // until explained what the dif is these are the same
                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.WorkWeeks,
                    BaseUnit = daysCounted
                });


                // there are 7 days in the week... nothing really to think about there
                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarWeeks,
                    BaseUnit = 7
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarWeeksFromFirstUse,
                    BaseUnit = 7
                });

                startDate = sampleDate.GetFirstDayOfMonth();
                endDate = sampleDate.GetLastDayOfMonth();
                daysCounted = actual.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.WorkingMonths,
                    BaseUnit = daysCounted
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarMonths,
                    BaseUnit = (endDate - startDate).Days + 1
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarMonthsFromFirstUse,
                    BaseUnit = (endDate - startDate).Days + 1
                });

                startDate = sampleDate.GetFirstDayOfYear();
                endDate = sampleDate.GetLastDayOfYear();
                //daysCounted = _schedule.Count(twd => twd.SampleDate >= startDate && twd.SampleDate <= endDate && twd.TotalMinutes != 0);

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarYears,
                    BaseUnit = (endDate - startDate).Days + 1
                });

                newUsage.Add(new AppliedPolicyUsageType()
                {
                    UnitType = EntitlementType.CalendarYearsFromFirstUse,
                    BaseUnit = (endDate - startDate).Days + 1
                });

                apu.AvailableToUse = newUsage;
            }
        }
        #region Case Custom Fields
        private void UpdateCustomFieldValue(object val, CustomField customField)
        {
            if (val == null)
                return;

            switch (customField.ValueType)
            {
                case CustomFieldValueType.SelectList:
                    string stringVal = val.ToString();
                    var item = GetMatchingCustomFieldValue(customField, stringVal);
                    if (item == null)
                    {
                        AddError(FormatCustomFieldError(customField, stringVal));
                    }
                    else
                    {
                        customField.SelectedValue = stringVal;
                    }
                    break;
                case CustomFieldValueType.UserEntered:
                default:
                    customField.SelectedValue = val;
                    break;
            }
        }

        private ListItem GetMatchingCustomFieldValue(CustomField customField, string stringVal)
        {
            ListItem matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Key) && l.Key.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));

            if (matchingItem != null)
                return matchingItem;

            return customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Value) && l.Value.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));
        }

        private string FormatCustomFieldError(CustomField customField, string val)
        {
            StringBuilder fieldError = new StringBuilder();
            fieldError.AppendFormat("'{0}' is not a valid value for '{1}'. ", val, customField.Label);
            if (customField.ListValues.Count > 10)
            {
                fieldError.AppendFormat("Valid values are defined under Custom Fields.");
            }
            else
            {
                fieldError.AppendFormat("Valid values are {0}", string.Join(", ", customField.ListValues.Select(l => FormatListValueForError(l))));
            }

            return fieldError.ToString();
        }

        private string FormatListValueForError(ListItem item)
        {
            return string.Format("{0} ({0})", item.Key, item.Value);
        }

        private object ParseCustomFieldValue(CustomField customField)
        {
            object val = customField.SelectedValue = null;
            switch (customField.DataType)
            {
                case CustomFieldType.Number:
                    val = ParseDecimal(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                case CustomFieldType.Flag:
                    val = ParseBool(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                case CustomFieldType.Date:
                    val = ParseDate(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                case CustomFieldType.Text:
                default:
                    val = Normalize(CF_CustomFieldValueOrdinal);
                    break;
            }

            return val;
        }
        #endregion

        #region Case Conversion Eligibility
        private void UpdateCaseConversionEligibility(string employerId, string customerId, string userId, List<Employee> lstEmp, List<EmployeeClass> employeeClassTypes)
        {
            #region Case Conversion Eligibility

            // Now process the row as an eligibility row
            try
            {
                var paySchedules = PaySchedule.AsQueryable()
                       .Where(s => s.CustomerId == customerId && s.EmployerId == employerId && s.Name != null)
                       .ToDictionary(s => s.Name, s => s.Id);



                Employee emp = Employee.AsQueryable().Where(e => e.EmployerId == employerId && e.CustomerId == customerId && e.EmployeeNumber == EmployeeNumber).FirstOrDefault();
                if (emp == null)
                    emp = new Employee();

                emp.EmployerId = employerId;
                emp.CustomerId = customerId;
                // Setup
                emp.Info = emp.Info ?? new EmployeeInfo();
                emp.Info.Address = emp.Info.Address ?? new Address();
                emp.EmployeeNumber = EmployeeNumber;
                // Set Basic Properties (These are the easy ones)
                emp.ModifiedById = userId;
                emp.SetModifiedDate(DateTime.UtcNow);
                emp.LastName = LastName;
                emp.FirstName = FirstName;
                emp.MiddleName = MiddleName;
                emp.JobTitle = JobTitle;
                emp.WorkState = WorkState;
                emp.WorkCountry = WorkCountry ?? "US";
                emp.Info.HomePhone = PhoneHome;
                emp.Info.WorkPhone = PhoneWork;
                emp.Info.CellPhone = PhoneMobile;
                emp.Info.AltPhone = PhoneAlt;
                emp.Info.Email = Email;
                emp.Info.AltEmail = EmailAlt;
                emp.Info.Address.Address1 = Address;
                emp.Info.Address.Address2 = Address2;
                emp.Info.Address.City = City;
                emp.Info.Address.State = State;
                emp.Info.Address.PostalCode = PostalCode;
                emp.Info.Address.Country = Country;
                emp.EmployeeClassCode = GetEmployementType(EmploymentType);
                if (!String.IsNullOrWhiteSpace(EmploymentType) && !ValidateEmployementType(EmploymentType,employeeClassTypes))
                {
                    AddError("'{0}' is not a valid value for EmploymentType. It should be matching to Employee Class configuration under Admin", EmploymentType);
                }
                emp.EmployeeClassName = GetEmploymentTypeName(EmploymentType, employeeClassTypes);
                emp.DoB = DateOfBirth;
                emp.Gender = (Gender?)Gender;
                emp.IsExempt = ExemptionStatus == 'E';
                emp.Meets50In75MileRule = Meets50In75 ?? false;
                emp.IsKeyEmployee = KeyEmployee ?? false;
                emp.MilitaryStatus = (MilitaryStatus)(MilitaryStatus ?? 0);
                emp.TerminationDate = TerminationDate;
                emp.Salary = (double?)PayRate;
                emp.PayType = (PayType?)PayType;
                emp.HireDate = HireDate;
                emp.RehireDate = RehireDate;
                emp.ServiceDate = AdjustedServiceDate;
                if (!string.IsNullOrWhiteSpace(SSN))
                    emp.Ssn = new CryptoString(SSN).Hash().Encrypt();
                if (JobClassification.HasValue)
                    emp.JobActivity = (JobClassification)JobClassification.Value;
                if (!string.IsNullOrWhiteSpace(PayScheduleName) && paySchedules.ContainsKey(PayScheduleName))
                    emp.PayScheduleId = paySchedules[PayScheduleName];
                emp.StartDayOfWeek = StartDayOfWeek;

                // Create TODO items for any open cases if this employee is being marked as terminated.

                emp.Status = (EmploymentStatus)(EmploymentStatus ?? EmploymentStatusValue.Active);

                if (HoursWorkedIn12Months.HasValue)
                {
                    emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
                    emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)HoursWorkedIn12Months.Value, AsOf = (ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                }
                else
                    emp.PriorHours = new List<PriorHours>(0);


                if (AverageMinutesWorkedPerWeek.HasValue)
                {
                    emp.MinutesWorkedPerWeek = emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
                    emp.MinutesWorkedPerWeek.Add(new MinutesWorkedPerWeek() { MinutesWorked = AverageMinutesWorkedPerWeek.Value, AsOf = (ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                }
                else
                    emp.MinutesWorkedPerWeek = new List<MinutesWorkedPerWeek>(0);


                Schedule workSched = new Schedule() { ScheduleType = VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                workSched.StartDate = (emp.HireDate.Value).ToMidnight();

                if (!WorkTimeSun.HasValue &&
                    !WorkTimeMon.HasValue &&
                    !WorkTimeTue.HasValue &&
                    !WorkTimeWed.HasValue &&
                    !WorkTimeThu.HasValue &&
                    !WorkTimeFri.HasValue &&
                    !WorkTimeSat.HasValue)
                {
                    if (!MinutesPerWeek.HasValue)
                        workSched = null;
                    else
                    {
                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                        int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)MinutesPerWeek.Value / 5m));
                        int leftOverMinute = MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                        for (var d = 0; d < 7; d++)
                        {
                            if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                            else
                                workSched.Times.Add(new Time() { SampleDate = sample });
                            sample = sample.AddDays(1);
                        }
                        // Add our leftover minute to the first date, clugey, but it's whatever.
                        workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                    }
                }
                else
                {
                    DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                    for (var d = 0; d < 7; d++)
                    {
                        int? workTime = null;
                        switch (sample.DayOfWeek)
                        {
                            case DayOfWeek.Sunday: workTime = WorkTimeSun; break;
                            case DayOfWeek.Monday: workTime = WorkTimeMon; break;
                            case DayOfWeek.Tuesday: workTime = WorkTimeTue; break;
                            case DayOfWeek.Wednesday: workTime = WorkTimeWed; break;
                            case DayOfWeek.Thursday: workTime = WorkTimeThu; break;
                            case DayOfWeek.Friday: workTime = WorkTimeFri; break;
                            case DayOfWeek.Saturday: workTime = WorkTimeSat; break;
                        }
                        workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                        sample = sample.AddDays(1);
                    }
                }

                if (workSched != null)
                {
                    Schedule dup = emp.WorkSchedules == null ? null : emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                    if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                    {
                        // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                        bool isMatch = true;
                        foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                            if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                            {
                                isMatch = false;
                                break;
                            }
                        // See if the schedules still look the same
                        if (isMatch)
                            workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                    }
                }

                if (workSched != null)
                {

                    workSched.Metadata.SetRawValue("SystemEntered", true);

                }
                if (emp.IsNew)
                {
                    emp = new EmployeeService().Using(s => s.Update(emp, workSched, null));
                }

                lstEmp.Add(emp);

            }
            catch (Exception ex)
            {
                Log.Error("Error processing Case Conversion Eligibility row", ex);
                AddError("Error processing Case Conversion Eligibility row, {0}", ex.Message);

            }
            #endregion 
        }
        #endregion
    }
}
