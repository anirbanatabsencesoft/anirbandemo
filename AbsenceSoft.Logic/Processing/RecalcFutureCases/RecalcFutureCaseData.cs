﻿using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Logic.Processing.CaseRecalcFutureCases
{
    public class RecalcFutureCaseData
    {

        public RecalcFutureCaseData(string caseId,string employeeId,DateTime? caseEndDate)
        {
            CaseId = caseId;
            EmployeeId = employeeId;
            CaseEndDate = caseEndDate;
        }

        [Required]
        public string CaseId { get; set; }

        [Required]
        public string EmployeeId { get; set; }

        [Required]
        public DateTime? CaseEndDate { get; set; }

    }
}