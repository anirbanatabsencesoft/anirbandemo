﻿using System;
using AbsenceSoft.Logic.Common;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Logic.Queries;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Processing.CaseRecalcFutureCases;
using System.Linq;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;

namespace AbsenceSoft.Logic.Processing.RecalcFutureCases
{
    public class RecalcFutureCaseMessageProcessor : IMessageProcessor
    {
        public bool IsProcessorFor(QueueItem item)
        {
            return item.GetMessageType() == MessageType.RecalcFutureCases;
        }

        public void AfterProcess(QueueItem item, MessageProcessingContext context)
        {
        }

        public void BeforeProcess(QueueItem item, MessageProcessingContext context)
        {
        }

        public void HandleException(QueueItem item, Exception ex)
        {
        }

        public void Process(QueueItem item, MessageProcessingContext context)
        {
            var msg = item.GetMessage<RecalcFutureCaseData>();
            List<ValidationResult> validations = new List<ValidationResult>();
            if (!Validator.TryValidateObject(msg, new ValidationContext(msg), validations))
            {
                throw new AbsenceSoftValidationException("Invalid message: " + item.Body, validations);
            }
            Log.Info("BEGIN  RecalcFutureCase Message processor case: Id={0}", msg.CaseId);

            var affectedCases = Case.AsQueryable().Where(c => c.Employee.Id == msg.EmployeeId
            && c.StartDate < msg.CaseEndDate.Value
            && c.Id != msg.CaseId
            && c.Status == CaseStatus.Open).OrderBy(o => o.StartDate).ToList();

          
            using (var caseService = new CaseService())
            {
                foreach (var @case in affectedCases)
                {
                    Log.Info("BEGIN RunCalcs case: CaseNumber={0}; Id={1}", @case.CaseNumber, @case.Id);
                    caseService.RunCalcs(@case);
                    caseService.UpdateCase(@case);
                    Log.Info("End RunCalcs case: CaseNumber={0}; Id={1}", @case.CaseNumber, @case.Id);
                }
            }
            Log.Info("End  RecalcFutureCase Message processor case: Id={0}", msg.CaseId);
        }

    }
}