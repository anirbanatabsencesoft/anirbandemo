﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Processing
{
    public enum ProcessResult
    {
        Success = 0,
        Failure = 1,
        Retry = -1
    }
}
