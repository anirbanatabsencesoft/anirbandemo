﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using static AbsenceSoft.Logic.Processing.CaseRow;

namespace AbsenceSoft.Logic.Processing
{
    internal class CaseRowForAccommodation : BaseRow
    {
        #region Fields

        public RecordType RecType;

        /// <summary>
        /// Mandatory. Associate ID number or unique identifier, either from your HRIS or other system; if numeric may contain leading zeros, however these leading zeros 
        /// will then always be required when identifying this associate.
        /// </summary>
        public string EmployeeNumber;

        /// <summary>
        /// Mandatory. The unique case number assigned to the case from the system the case is being converted from. This may be in any format, any length and any composition. 
        /// AbsenceTracker will keep this literal case number value as "the" case number within AbsenceTracker as well.
        ///This case number must also match all other conversion records that impact this case such as Create Case("C") records, Time Off Requests, etc.
        /// </summary>
        public string UniqueCaseNumber;

        /// <summary>
        /// Mandatory. The type code for accommodation. This item is configurable and outside of our core types (listed below) may also include any customer configured types.Please inquire with your account manager or implementations specialist to obtain a list of valid type codes. These are also accessible through the administrative user interface of AbsenceTracker.
        /// either: EA = Ergonomic Assessment, EOS = Equipment Or Software, SC = Schedule Change, LEV = Leave, OTR = Other
        /// </summary>
        public string AccommodationTypeCode;

        /// <summary>
        /// Mandatory. The start date for the accommodation.
        /// </summary>
        public DateTime? StartDate;

        /// <summary>
        /// Optional. The end date for the accommodation. 
        /// </summary>
        public DateTime? EndDate;

        /// <summary>
        /// Optional. A short description of the accommodation, if provided
        /// </summary>
        public string Description;

        /// <summary>
        /// Optional. Indicates whether or not the accommodation has been resolved
        /// </summary>
        public bool? IsResolved;

        /// <summary>
        /// Optional. Provides a description of the resolution or proposed resolution	
        /// </summary>
        public string Resolution;

        /// <summary>
        /// Optional. The date in which the accommodation was resolved. This field corresponds to the Is Resolved and Resolution fields
        /// </summary>
        public DateTime? ResolvedDate;

        /// <summary>
        /// Optional. Indicates whether or not the accommodation is/was granted
        /// </summary>
        public bool? IsGranted;

        /// <summary>
        /// Optional. The date in which the accommodation was granted. This field corresponds to the Is Granted field	
        /// </summary>
        public DateTime? GrantedDate;

        /// <summary>
        /// Optional. The total cost of the accommodation (equipment, services, etc.).	
        /// </summary>
        public decimal? Cost;

        /// <summary>
        /// Optional. This is the actual determination status of the accommodation for the given date range:
        /// 0 = Pending (* Default), 1 = Approved, 2 = Denied 
        /// </summary>
        public int? Determination;

        /// <summary>
        /// Optional. One of the following values indicating the overall status of the accommodation:
        /// 0 = Open (* Default), 1 = Closed, 2 = Canceled
        /// </summary>
        public int? AccommodationStatus;

        /// <summary>
        /// Optional. The date the accommodation was originally created. If not provided the Case Created date will be used.	
        /// </summary>
        public DateTime? DateCreated;

        /// <summary>
        /// Optional. Indicates whether or not the accommodation is due to a work related illness or injury.
        /// default: FALSE
        /// </summary>
        public bool? IsWorkRelated;

        #endregion

        #region const

        private const int D_RecordTypeOrdinal = 0;
        private const int D_EmployeeNumberOrdinal = 1;
        private const int D_UniqueCaseNumberOrdinal = 2;
        private const int D_AccommodationTypeCodeOrdinal = 3;
        private const int D_StartDateOrdinal = 4;
        private const int D_EndDateOrdinal = 5;
        private const int D_DescriptionOrdinal = 6;
        private const int D_IsResolvedOrdinal = 7;
        private const int D_ResolutionOrdinal = 8;
        private const int D_ResolvedDateOrdinal = 9;
        private const int D_IsGrantedOrdinal = 10;
        private const int D_GrantedDateOrdinal = 11;
        private const int D_CostOrdinal = 12;
        private const int D_DeterminationOrdinal = 13;
        private const int D_AccommodationStatusOrdinal = 14;
        private const int D_CreatedDateOrdinal = 15;
        private const int D_IsWorkRelatedOrdinal = 16;

        public CaseRowForAccommodation(string[] rowSource) : base(rowSource) { }

        #endregion

        /// <summary>
        /// Parse and validate record fields
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        /// <param name="customFields">The custom fields.</param>
        /// <returns>Is Valid</returns>
        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();              

            if (Required(D_AccommodationTypeCodeOrdinal, "Accommodation Status"))
            {
                AccommodationTypeCode = Normalize(D_AccommodationTypeCodeOrdinal);
            }

            if (Required(D_StartDateOrdinal, "Start Date"))
            {
                StartDate = ParseDate(D_StartDateOrdinal, "Start Date");
            }

            EndDate = ParseDate(D_EndDateOrdinal, "End Date");
            Description = Normalize(D_DescriptionOrdinal);
            IsResolved = ParseBool(D_IsResolvedOrdinal, "Is Resolved");
            Resolution = Normalize(D_ResolutionOrdinal);
            ResolvedDate = ParseDate(D_ResolvedDateOrdinal, "Resolved Date");
            IsGranted = ParseBool(D_IsGrantedOrdinal, "Is Granted");
            GrantedDate = ParseDate(D_GrantedDateOrdinal, "Granted Date");
            Cost = ParseDecimal(D_CostOrdinal, "Cost");
            Determination = ParseInt(D_DeterminationOrdinal, "Determination");
            AccommodationStatus = ParseInt(D_AccommodationStatusOrdinal, "Accommodation Status");
            DateCreated = ParseDate(D_CreatedDateOrdinal, "Date Created");
            IsWorkRelated = ParseBool(D_IsWorkRelatedOrdinal, "Is WorkRelated");

            return true;
        }

        /// <summary>
        /// Applies the properties in this record to the target case, employing any necessary additional validation
        /// </summary>
        /// <param name="myCase">Case</param>
        /// <param name="emp">Employee</param>
        /// <returns>ApplicationResult</returns>
        internal ApplicationResult Apply(Case myCase, Employee emp)
        {
            if (myCase == null || string.IsNullOrWhiteSpace(myCase.Id))
            {
                AddError("Cannot create accommodation, case does not exists. Please use the AbsenceTracker application to modify/create this case.");
                return ApplicationResult.Fail;
            }

            var accommodationType = AccommodationType.GetByCode(this.AccommodationTypeCode, myCase.CustomerId, myCase.EmployerId);
            if (accommodationType == null)
            {
                AddError("'Accommodation Status' is not in the correct format. Please provide proper value for it.");
                return ApplicationResult.Fail;
            }

            using (var service = new AccommodationService(myCase.Customer, myCase.Employer, Data.Security.User.System))
            {
                if (myCase.AccommodationRequest != null && myCase.AccommodationRequest.Accommodations != null)
                {
                    var errorMsg = "An accommodation request with the same date range already exists";

                    //check if the new accomodation is for temporary duration, since it will have both start & end date
                    if (EndDate.HasValue)
                    {
                        if ((myCase.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(accommodationType.Name)
                                                                            && (StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate)
                                                                                || EndDate.Value.DateInRange(x.StartDate.Value, x.EndDate))))
                            || (myCase.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(accommodationType.Name)
                                                                            && (StartDate <= x.StartDate
                                                                                && EndDate >= x.EndDate))))
                        {
                            AddError(errorMsg);
                        }
                    }
                    else // permanent duration
                    {
                        if ((myCase.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(accommodationType.Name)
                                                                            && StartDate.Value.DateInRange(x.StartDate.Value, x.EndDate)))
                                || (myCase.AccommodationRequest.Accommodations.Any(x => x.Type.Name.Equals(accommodationType.Name)
                                                                            && (StartDate <= x.StartDate || x.EndDate >= StartDate))))
                        {
                            AddError(errorMsg);
                        }
                    }
                }

                if (IsError)
                    return ApplicationResult.Fail;

                AccommodationType selectedType = service.GetAccommodationTypeById(accommodationType.Id);
                // Commented for EZW-1148 for now. Would be added later.
                //if (selectedType.Categories != null && selectedType.Categories.Count > 0)
                //{
                //    selectedType.Name = selectedType.Categories.LastOrDefault().Value;
                //}
                List<Accommodation> theAccomms;
                if (myCase.AccommodationRequest != null && myCase.AccommodationRequest.Accommodations.Count > 0)
                {
                    theAccomms = myCase.AccommodationRequest.Accommodations;
                }
                else
                {
                    theAccomms = new List<Accommodation>(1);
                }
                var accom = new Accommodation()
                {
                    Status = GetAccommodationStatus(this.AccommodationStatus),
                    Type = selectedType,
                    Duration = this.EndDate.HasValue ? AccommodationDuration.Temporary : AccommodationDuration.Permanent,
                    Resolved = this.IsResolved.HasValue ? this.IsResolved.Value : false,
                    Resolution = this.Resolution,
                    ResolvedDate = this.ResolvedDate,
                    Granted = this.IsGranted.HasValue ? this.IsGranted.Value : false,
                    GrantedDate = this.GrantedDate,
                    Cost = this.Cost,
                    IsWorkRelated = this.IsWorkRelated.HasValue ? this.IsWorkRelated.Value : false,
                    Description = this.Description,
                    Usage = new List<AccommodationUsage>()
                        {
                            new AccommodationUsage()
                            {
                                StartDate = this.StartDate.Value,
                                EndDate = this.EndDate,
                                Determination = GetDetermination(this.Determination)
                            }
                        }
                };
                theAccomms.Add(accom);
                myCase.AccommodationRequest = service.CreateOrModifyAccommodationRequest(myCase, this.Description, theAccomms);
                // Updates the cancelled accomodation request status if the added accomodation is not of status 'Cancelled' to 'Open'
                if (accom.Status != CaseStatus.Cancelled && myCase.AccommodationRequest.Status == CaseStatus.Cancelled)
                {
                    myCase.AccommodationRequest.Status = CaseStatus.Open;
                    myCase.AccommodationRequest.CancelReason = (AccommodationCancelReason?)null;
                    myCase.AccommodationRequest.OtherReasonDesc = string.Empty;
                }
                using (CaseService svc = new CaseService())
                {
                    svc.UpdateCase(myCase, CaseEventType.AccommodationAdded);
                }

            }
            return ApplicationResult.Success;
        }

        /// <summary>
        /// Get Determination Unit by type
        /// </summary>
        /// <param name="determination">int</param>
        /// <returns>AdjudicationStatus</returns>
        private AdjudicationStatus GetDetermination(int? determination)
        {
            switch (determination)
            {
                case 1:
                    return AdjudicationStatus.Approved;
                case 2:
                    return AdjudicationStatus.Denied;
                case 0:
                default:
                    return AdjudicationStatus.Pending;
            }
        }

        /// <summary>
        /// Get Accommodation by type
        /// </summary>
        /// <param name="accommodationStatus">int</param>
        /// <returns>CaseStatus</returns>
        private CaseStatus GetAccommodationStatus(int? accommodationStatus)
        {
            switch (accommodationStatus)
            {
                case 1:
                    return CaseStatus.Closed;
                case 2:
                    return CaseStatus.Cancelled;
                case 0:
                default:
                    return CaseStatus.Open;
            }
        }
    }
}