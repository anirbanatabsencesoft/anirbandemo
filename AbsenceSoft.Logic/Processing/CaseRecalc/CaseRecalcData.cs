﻿using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;

namespace AbsenceSoft.Logic.Processing.CaseRecalc
{
    public class CaseRecalcData
    {

        public CaseRecalcData(CaseEventType caseEventType, IMongoQuery mongoQuery, string initiatedByUserId)
        {
            CaseEventType = caseEventType;
            AffectedCasesFilter = mongoQuery.ToJson();
            this.InitiatedByUserId = initiatedByUserId;
        }

        [Required]
        public CaseEventType? CaseEventType { get; set; }

        [Required]
        public string AffectedCasesFilter { get; set; }

        [Required]
        public string InitiatedByUserId { get; set; }
    }
}