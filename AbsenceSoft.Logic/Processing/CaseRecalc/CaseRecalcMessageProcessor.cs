﻿using System;
using AbsenceSoft.Logic.Common;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using MongoDB.Driver;
using MongoDB.Bson;
using AbsenceSoft.Logic.Queries;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Logic.Processing.CaseRecalc
{
    public class CaseRecalcMessageProcessor : IMessageProcessor
    {
        public bool IsProcessorFor(QueueItem item)
        {
            return item.GetMessageType() == MessageType.CaseRecalcRequest;
        }

        public void AfterProcess(QueueItem item, MessageProcessingContext context)
        {
        }

        public void BeforeProcess(QueueItem item, MessageProcessingContext context)
        {
        }

        public void HandleException(QueueItem item, Exception ex)
        {
        }

        public void Process(QueueItem item, MessageProcessingContext context)
        {
            var msg = item.GetMessage<CaseRecalcData>();
            List<ValidationResult> validations = new List<ValidationResult>();
            if (!Validator.TryValidateObject(msg, new ValidationContext(msg), validations))
            {
                throw new AbsenceSoftValidationException("Invalid message: " + item.Body, validations);
            }

            var affectedCases = QueryCases.That()
                .AreActive()
                .ApplyFilter(msg.AffectedCasesFilter)
                .AsEnumerable();
            var caseEvent = msg.CaseEventType;
            var initiatedByUser = User.GetById(msg.InitiatedByUserId);
            foreach (var @case in affectedCases)
            {
                Log.Info("BEGIN Recalculate case: CaseNumber={0}; Id={1}", @case.CaseNumber, @case.Id);
                @case.WfOnHolidayChanged(initiatedByUser);
                Log.Info("END Recalculate case: CaseNumber={0}; Id={1}", @case.CaseNumber, @case.Id);
            }
        }

    }
}