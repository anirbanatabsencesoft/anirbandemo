﻿using System;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Data.Processing;
using System.Linq;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic.Processing.CaseExport
{
    public class CaseExportMessageProcessor : IMessageProcessor
    {
        private CaseExportService caseExportService = new CaseExportService();
    
        public bool IsProcessorFor(QueueItem item)
        {
            return item.GetMessageType() == MessageType.CaseExportRequest;
        }

        public void AfterProcess(QueueItem item, MessageProcessingContext context)
        {
        }

        public void BeforeProcess(QueueItem item, MessageProcessingContext context)
        {

            if (IsCaseExportFileCanceledOrDeleted(item.Body))
            {
                context.CompleteEarly(ProcessResult.Success);
            }
        }

        public void HandleException(QueueItem item, Exception ex)
        {
            caseExportService.ReportFailed(item.Body, ex.Message);
        }

        public void Process(QueueItem item, MessageProcessingContext context)
        {
            caseExportService.ProcessCaseExport(item);
        }

        private bool IsCaseExportFileCanceledOrDeleted(string caseExportRequestId)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.IsCaseExportFileCanceledOrDeleted"))
            {
                return CaseExportRequest.AsQueryable().Count(u => u.Id == caseExportRequestId && u.Status != ProcessingStatus.Canceled) <= 0;
            }
        }

    }
}