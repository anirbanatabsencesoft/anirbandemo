﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Extensions;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Enums;
using AbsenceSoft.Logic.Users;
using AbsenceSoft.Rendering;
using Ionic.Zip;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ADocument = Aspose.Words.Document;

namespace AbsenceSoft.Logic.Processing.EL
{
    public class CaseExportService : LogicService
    {
        

        public CaseExportService() : base() { }

        public CaseExportService(Customer currentCustomer, Employer currentEmployer, User currentUser) :
            base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CaseExportService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public CaseExportService(User currentUser) : base(currentUser) { }


        #region CRUD
        public CaseExportRequest CreateCaseExportRecord(string caseId, CaseExportFormat downloadType)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.CreateCaseExportRecord"))
            {

                if (string.IsNullOrWhiteSpace(caseId))
                    throw new AbsenceSoftException("The case id was not provided and is required.");

                Case theCase = Case.GetById(caseId);
                if (theCase == null)
                    throw new AbsenceSoftException("Case does not exist or was deleted");

                CaseExportRequest caseExportRequest = new CaseExportRequest();
                caseExportRequest.CustomerId = theCase.CustomerId;
                caseExportRequest.CreatedById = CurrentUser.Id;
                caseExportRequest.EmployerId = theCase.EmployerId;
                caseExportRequest.EmployeeNumber = theCase.Employee.EmployeeNumber;
                caseExportRequest.Employee = theCase.Employee;
                caseExportRequest.ModifiedById = CurrentUser.Id;
                caseExportRequest.CaseId = caseId;
                caseExportRequest.Errors = new List<string>();
                caseExportRequest.DownloadType = downloadType;

                switch (downloadType)
                {
                    case CaseExportFormat.Pdf:
                        caseExportRequest.ContentType = "application/pdf";
                        break;
                    case CaseExportFormat.Zip:
                        caseExportRequest.ContentType = "application/zip";
                        break;
                }

                caseExportRequest.Status = ProcessingStatus.Pending;
                caseExportRequest.Save();
                
                try
                {
                    QueueFile(caseExportRequest.Id);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("There was an error exporting the case  '{0}' for processing.", caseExportRequest.Id), ex);
                    ReportFailed(caseExportRequest, "There was an error exporting the case   for processing.");
                    throw;
                }

                return caseExportRequest;
            }
        }

        /// <summary>
        /// Create case export request for employee & adds Queue
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="downloadType"></param>
        /// <returns></returns>
        public CaseExportRequest CreateCaseExportRecordByEmployeeId(string employeeId, string caseIds, CaseExportFormat downloadType)
        {
            using (var context = new InstrumentationContext("CaseExportService.CreateCaseExportRecordByEmployeeId"))
            {
                using (var caseService = new CaseService())
                {
                    if (string.IsNullOrWhiteSpace(employeeId))
                    {
                        throw new AbsenceSoftException("The employee id was not provided and is required.");
                    }
                    
                    var employee = new EmployeeService().GetEmployee(employeeId);

                    if (employee == null)
                    {
                        throw new AbsenceSoftException("Employee does not exist or was deleted");
                    }

                    var caseExportRequest = new CaseExportRequest
                    {
                        CustomerId = employee.CustomerId,
                        CreatedById = CurrentUser.Id,
                        EmployerId = employee.EmployerId,
                        EmployeeNumber = employee.EmployeeNumber,
                        Employee = employee,
                        ModifiedById = CurrentUser.Id,
                        CaseId = null,
                        CaseIds = caseIds,
                        Errors = new List<string>(),
                        DownloadType = downloadType
                    };

                    caseExportRequest.ContentType = ExportContentTypeExtensions.GetExportContentType(downloadType);

                    caseExportRequest.Status = ProcessingStatus.Pending;
                    caseExportRequest.Save();

                    try
                    {
                        QueueFile(caseExportRequest.Id);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("There was an error exporting the cases of employee '{0}' for processing.", caseExportRequest.Id), ex);
                        ReportFailed(caseExportRequest, "There was an error exporting the cases of employee for processing.");
                        throw;
                    }

                    return caseExportRequest;
                }
            }
        }

        /// <summary>
        /// Gets all the bulk downloads for a case
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public List<CaseExportRequest> GetCaseBulkDownloads(string caseId)
        {
            if (caseId == null)
                throw new ArgumentNullException("caseId");

            return CaseExportRequest.AsQueryable().Where(cer => cer.CaseId == caseId)
                .OrderByDescending(cer => cer.CreatedDate).ToList();
        }


        public List<CaseExportRequest> GetCaseBulkDownloadsForEmployee(string employeeId)
        {
            if (employeeId == null)
                throw new ArgumentNullException("employeeId");

            using (var employeeService = new EmployeeService())
            {
                var employee = employeeService.GetEmployee(employeeId);

                if (employee == null)
                    return null;

                return CaseExportRequest.AsQueryable().Where(cer => cer.EmployeeNumber == employee.EmployeeNumber && cer.CaseId == null)
                    .OrderByDescending(cer => cer.CreatedDate).ToList();
            }           
        }

        /// <summary>
        /// Gets all the case bulk downloads by ids
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="downloadIds"></param>
        /// <returns></returns>
        public List<CaseExportRequest> GetCaseBulkDownloadsByIds(string caseId, IEnumerable<string> downloadIds)
        {
            return CaseExportRequest.AsQueryable().Where(cer => cer.CaseId == caseId && downloadIds.Contains(cer.Id)).ToList();
        }

        /// <summary>
        /// Gets the download key 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetExportLink(string key)
        {
            try
            {
                using (FileService client = new FileService())
                    return client.GetS3DownloadUrl(key, Settings.Default.S3BucketName_Files);
            }
            catch (Exception amazonS3Exception)
            {
                Log.Error("Error occurred. Exception: ", amazonS3Exception);
                throw;
            }
        }

        /// <summary>
        /// Queues a file for export
        /// </summary>
        /// <param name="caseExportRequestId"></param>
        /// <returns></returns>
        public string QueueFile(string caseExportRequestId)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.QueueFile"))
            {
                using (QueueService svc = new QueueService())
                {
                    return svc.Add(MessageQueues.AtCommonQueue, MessageType.CaseExportRequest, caseExportRequestId);
                }
            }
        } // QueueFile

        /// <summary>
        /// Determines whether [is case export file canceled or deleted] [the specified case export identifier].
        /// </summary>
        /// <param name="caseExportRequestId">The case export identifier.</param>
        /// <returns></returns>
        public bool IsCaseExportFileCanceledOrDeleted(string caseExportRequestId)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.IsCaseExportFileCanceledOrDeleted"))
            {
                return CaseExportRequest.AsQueryable().Any(u => u.Id == caseExportRequestId && u.Status != ProcessingStatus.Canceled);
            }
        }

        /// <summary>
        /// Reports the failed.
        /// </summary>
        /// <param name="caseExportRequestId">The case export identifier.</param>
        /// <param name="error">The error.</param>
        public void ReportFailed(string caseExportRequestId, string error)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.ReportFailed"))
            {

                CaseExportRequest.Update(
                    CaseExportRequest.Query.EQ(u => u.Id, caseExportRequestId),
                    CaseExportRequest.Updates.Push(u => u.Errors, error)
                    .Set(u => u.Status, ProcessingStatus.Failed)
                    .Set(u => u.ModifiedDate, DateTime.UtcNow));
            }
        } // ReportFailed

        public void ReportFailed(CaseExportRequest caseExportRequest, string error)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.ReportFailed"))
            {
                caseExportRequest.Errors.Add(error);
                caseExportRequest.Status = ProcessingStatus.Failed;
                caseExportRequest.Save();

            }
        } // ReportFailed

        #endregion

        #region Process

        private List<string> CurrentUserPermissions { get; set; }

        /// <summary>
        /// Takes a queue item from the case export queue, sets the status of the case export
        /// to processing,
        /// </summary>
        /// <param name="item">The queue item to process.</param>
        public bool ProcessCaseExport(QueueItem item)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.ProcessCaseExport"))
            {
                // Simple common-sense validation
                if (item == null)
                    throw new ArgumentNullException("item");
                if (item.QueueName != MessageQueues.AtCommonQueue)
                    throw new AbsenceSoftException(string.Format("The queue item provided from queue '{0}' is not from the expected queue '{1}' and may not be of the correct type.",
                        item.QueueName, MessageQueues.AtCommonQueue));

                // Get our Id from the message body
                string caseExportRequestId = item.Body;

                // Get the case export item from the database.
                CaseExportRequest caseExport = CaseExportRequest.GetById(caseExportRequestId);
                if (caseExport == null)
                    throw new AbsenceSoftException("Case Export file upload not found");
                // This is not pending a read, so we need to skip it.
                if (caseExport.Status != ProcessingStatus.Pending)
                    return false;
                // Set the status of our case export download to processing, this is a must in order to properly process
                //  any of the rows.
                caseExport.Status = ProcessingStatus.Processing;
                caseExport.Save();
                try
                {
                    CurrentUserPermissions = User.Permissions.GetPermissions(caseExport.CreatedBy);
                    var document = (string.IsNullOrWhiteSpace(caseExport.CaseId) && !string.IsNullOrWhiteSpace(caseExport.CaseIds) && !string.IsNullOrWhiteSpace(caseExport.EmployeeNumber)) ? BuildCasesForEmployeeExportRequest(caseExport) : ExportCaseAndDocuments(caseExport);

                    if (document != null && document.ContentLength > 0)
                    {
                        caseExport.FileName = document.FileName;
                        caseExport.FileKey = document.Locator;
                        caseExport.Size = document.ContentLength;
                        caseExport.Status = ProcessingStatus.Complete;
                        caseExport.Document = document;
                    }
                    else
                    {
                        ReportFailed(caseExport, "No attachment file found");
                        caseExport.Status = ProcessingStatus.Failed;
                    }
                    caseExport.Save();
                }
                catch (Exception ex)
                {
                    ReportFailed(caseExport, ex.Message);
                    return false;
                }
                return true;
            }
        } // ProcessCaseExport


        /// <summary>
        /// Gets the case's communications, attachments, and notes then builds a document so that they can be downloaded
        /// </summary>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        public Document ExportCaseAndDocuments(CaseExportRequest caseExport)
        {
            if (string.IsNullOrEmpty(caseExport.CaseId))
            {
                caseExport.Errors.Add("Missing Case Id");
                return null;
            }
            using (InstrumentationContext context = new InstrumentationContext("CaseExportService.ExportCaseComunicationAndAttachement"))
            {

                try
                {
                    List<IMongoQuery> query = new List<IMongoQuery>();
                    query.Add(Case.Query.EQ(c => c.CustomerId, caseExport.CustomerId));
                    query.Add(Case.Query.EQ(c => c.EmployerId, caseExport.EmployerId));
                    query.Add(Case.Query.EQ(c => c.Id, caseExport.CaseId));

                    Case theCase = Case.Query.Find(Case.Query.And(query)).FirstOrDefault();
                    if (theCase == null)
                    {
                        caseExport.Errors.Add("Case does not exist or was deleted");
                        return null;
                    }

                    List<Attachment> attachments = Attachment.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                && c.EmployerId == theCase.EmployerId).ToList();

                    List<Communication> communications = Communication.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                && c.EmployerId == theCase.EmployerId).ToList();

                    List<CaseNote> caseNotes = CaseNote.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                && c.EmployerId == theCase.EmployerId).ToList();

                    List<ToDoItem> todoItems = ToDoItem.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                && c.EmployerId == theCase.EmployerId).ToList();
                                

                    return BuildCaseExportRequest(theCase, attachments, communications, caseNotes, todoItems, caseExport);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Error processing case export file  '{0}'", caseExport.Id), ex);
                    ReportFailed(caseExport, ex.Message);
                }

            }

            return null;
        }

        /// <summary>
        /// Create document for the employee with his/hers all cases & returns it
        /// </summary>
        /// <param name="caseExport"></param>
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        private Document BuildCasesForEmployeeExportRequest(CaseExportRequest caseExport)
        {
            using (var employeeService = new EmployeeService())
            {
                var employee = employeeService.GetEmployeeByEmployeeNumber(caseExport.EmployeeNumber, caseExport.CustomerId, caseExport.EmployerId);

                if (employee == null)
                {
                    caseExport.Errors.Add("Missing employee");
                    return null;
                }

                var doc = new Document()
                {
                    CustomerId = caseExport.CustomerId,
                    EmployerId = caseExport.EmployerId,
                    ContentLength = 0,
                    EmployeeId = employee.Id,
                    CaseId = null,
                    ContentType = ExportContentTypeExtensions.GetExportContentType(caseExport.DownloadType),
                    FileName = string.Format("{0}_{1}_{2}.{3}", caseExport.EmployeeNumber, "AllCases", DateTime.UtcNow.ToYearMonthDayWithoutSeperator(), caseExport.DownloadType.ToString().ToLower())
                };

                switch (caseExport.DownloadType)
                {
                    case CaseExportFormat.Pdf:                        
                        doc.File = MergeAllCasesAsPdfForEmployee(caseExport, employee);
                        break;
                    case CaseExportFormat.Zip:
                        doc.File = MergeAllCasesAsZipForEmployee(caseExport, employee);
                        break;
                }

                if (doc.File != null)
                    doc.ContentLength = doc.File.LongLength;

                if (doc.ContentLength > 0)
                    doc.Upload();

                return doc;
            }
        }

        /// <summary>
        /// It will merge all cases as zip & returns same
        /// </summary>
        /// <param name="caseExport"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        private byte[] MergeAllCasesAsZipForEmployee(CaseExportRequest caseExport, Employee employee)
        {
            if (employee == null)
                return null;

            using (var caseService = new CaseService())
            {
                var cases = caseService.GetCasesForEmployee(employee.Id, CaseStatus.Open, CaseStatus.Closed, CaseStatus.Requested, CaseStatus.Inquiry).Where(o => caseExport.CaseIds.Contains(o.Id));

                using (var zipStream = new MemoryStream())
                {
                    var zip = new ZipFile();

                    foreach (var theCase in cases.OrderBy(o => o.StartDate))
                    {
                        var attachments = Attachment.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                    && c.EmployerId == theCase.EmployerId).ToList();

                        var communications = Communication.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                    && c.EmployerId == theCase.EmployerId).ToList();

                        var caseNotes = CaseNote.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                    && c.EmployerId == theCase.EmployerId).ToList();

                        var todoItems = ToDoItem.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                                    && c.EmployerId == theCase.EmployerId).ToList();

                        AddCaseToZipFileForEmployee(zip, theCase, caseNotes, todoItems, communications, attachments, caseExport);
                        AddCommunicationsToZipFileForEmployee(zip, communications, caseExport, theCase);
                        AddAttachmentsToZipFileForEmployee(zip, attachments, caseExport, theCase);
                    }

                    zip.Save(zipStream);
                    return zipStream.ToArray();
                }
            } 
        }

        /// <summary>
        /// Merge All Cases into pdf for a Employee
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <returns></returns>

        private byte[] MergeAllCasesAsPdfForEmployee(CaseExportRequest caseExport, Employee employee)
        {
            if (employee == null)
                return null;
            using (var caseService = new CaseService())
            {
                var cases = caseService.GetCasesForEmployee(employee.Id, CaseStatus.Open, CaseStatus.Closed, CaseStatus.Requested, CaseStatus.Inquiry).Where(o => caseExport.CaseIds.Contains(o.Id));

                var allCasesPdfs = new List<byte[]>();

                foreach (var theCase in cases.OrderBy(o => o.StartDate))
                {
                    var casePdf = CreateCasePdf(caseExport, theCase, out List<byte[]> communicationsPdf, out List<byte[]> attachmentsPdf);
                    if (casePdf != null)
                    {
                        allCasesPdfs.Add(casePdf);
                        if (communicationsPdf != null)
                        {
                            allCasesPdfs.AddRange(communicationsPdf);
                        }

                        if (attachmentsPdf != null)
                        {
                            allCasesPdfs.AddRange(attachmentsPdf);
                        }
                    }
                }
                var sourcePdf = Pdf.MergePdfDocumentsWithPageNumbers(allCasesPdfs.ToArray());
                return Pdf.AddPdfMetadata(sourcePdf, CreatePdfMetadataFromEmployee(employee));
            }
        }

        /// <summary>
        /// Creates a PDF of all the case data for employee
        /// </summary>
        /// <param name="caseExport"></param>
        /// <param name="case"></param>
        /// <param name="communicationsPdf"></param>
        /// <param name="attachmentsPdf"></param>
        /// <returns></returns>
        private byte[] CreateCasePdf(CaseExportRequest caseExport, Case @case, out List<byte[]> communicationsPdf, out List<byte[]> attachmentsPdf)
        {
            communicationsPdf = null;
            attachmentsPdf = null;
            try
            {
                var query = new List<IMongoQuery>
                    {
                        Case.Query.EQ(c => c.CustomerId, caseExport.CustomerId),
                        Case.Query.EQ(c => c.EmployerId, caseExport.EmployerId),
                        Case.Query.EQ(c => c.Id, @case.Id)
                    };

                var theCase = Case.Query.Find(Case.Query.And(query)).FirstOrDefault();

                if (theCase == null)
                {
                    caseExport.Errors.Add("Case does not exist or was deleted");
                    return null;
                }

                var attachments = Attachment.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                            && c.EmployerId == theCase.EmployerId).ToList();

                var communications = Communication.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                            && c.EmployerId == theCase.EmployerId).ToList();

                var caseNotes = CaseNote.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                            && c.EmployerId == theCase.EmployerId).ToList();

                var todoItems = ToDoItem.AsQueryable().Where(c => c.CaseId == theCase.Id && c.CustomerId == theCase.CustomerId
                            && c.EmployerId == theCase.EmployerId).ToList();

                communicationsPdf = CreateCommunicationsPdfs(communications, caseExport);
                attachmentsPdf = CreateAttachmentsPdfs(attachments, caseExport);

                return CreateCasePdf(theCase, caseNotes, todoItems, communications, attachments, caseExport);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error processing case export file  '{0}'", caseExport.Id), ex);
                ReportFailed(caseExport, ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Creates the document from the case, attachments and communications
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="attachments"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        private Document BuildCaseExportRequest(Case theCase, List<Attachment> attachments, List<Communication> communications, List<CaseNote> caseNotes, List<ToDoItem> todoItems, CaseExportRequest caseExport)
        {
            Document doc = new Document()
            {
                CustomerId = caseExport.CustomerId,
                EmployerId = caseExport.EmployerId,
                ContentLength = 0,
                EmployeeId = theCase.Employee.Id,
                CaseId = theCase.Id,
                FileName = string.Format("{0}_{1:yyyy-MM-dd HHmmss}.{2}", caseExport.CaseId, DateTime.UtcNow, caseExport.DownloadType.ToString().ToLower())
            };

            switch (caseExport.DownloadType)
            {
                case CaseExportFormat.Pdf:
                    doc.ContentType = "application/pdf";
                    doc.File = ExportToPdf(theCase, communications, attachments, caseNotes, todoItems, caseExport);
                    break;
                case CaseExportFormat.Zip:
                    doc.ContentType = "application/zip";
                    doc.File = ExportToZipFile(theCase, communications, attachments, caseNotes, todoItems, caseExport);
                    break;
            }

            if (doc.File != null)
                doc.ContentLength = doc.File.LongLength;

            if (doc.ContentLength > 0)
                doc.Upload();

            return doc;
        }

        /// <summary>
        /// Creates a zip file from a list of communications and attachments
        /// </summary>
        /// <param name="communications"></param>
        /// <param name="attachments"></param>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        private byte[] ExportToZipFile(Case theCase, List<Communication> communications, List<Attachment> attachments, List<CaseNote> caseNotes, List<ToDoItem> todoItems, CaseExportRequest caseExport)
        {
            using (MemoryStream zipStream = new MemoryStream())
            {
                ZipFile zip = new ZipFile();
                AddCaseToZipFile(zip, theCase, caseNotes, todoItems, communications, attachments, caseExport);
                AddCommunicationsToZipFile(zip, communications, caseExport);
                AddAttachmentsToZipFile(zip, attachments, caseExport);
                zip.Save(zipStream);
                return zipStream.ToArray();
            }
        }

        /// <summary>
        /// Adds each communication in a list to the passed in zip file
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        private void AddCommunicationsToZipFile(ZipFile zip, List<Communication> communications, CaseExportRequest caseExport)
        {
            foreach (var communication in communications.Where(c => 
                    !string.IsNullOrWhiteSpace(c.Body) 
                    && c.CommunicationType == CommunicationType.Email))
            {
                string fileName = string.Format("{0}_{1:yyyy-MM-dd HHmmss}.pdf", communication.Name,  communication.CreatedDate);
                try
                {
                    byte[] pdf = ConvertCommunicationEmailToPdf(communication, caseExport.CreatedBy);
                    zip.AddEntry(fileName, pdf);
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} as ZIP", fileName));
                }
            }
        }

        /// <summary>
        /// Adds each communication in a list to the passed in zip file for employee
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        private void AddCommunicationsToZipFileForEmployee(ZipFile zip, List<Communication> communications, CaseExportRequest caseExport, Case theCase)
        {
            var filteredCommunications = communications.Where(c => !string.IsNullOrWhiteSpace(c.Body) && c.CommunicationType == CommunicationType.Email);

            foreach (var communication in filteredCommunications)
            {
                var fileName = CreateNewFileNameInZip(zip, theCase.CaseNumber, communication.Name, communication.CreatedDate, "pdf");

                try
                {
                    var pdf = ConvertCommunicationEmailToPdf(communication, caseExport.CreatedBy);                    
                    zip.AddEntry(fileName, pdf);
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} Communication as ZIP & Exception Message : {1}", fileName, ex.Message));
                }
            }
        }


        private string CreateNewFileNameInZip(ZipFile zip, string caseNumber, string documentName, DateTime date, string fileExtension)
        {
            var fileName = string.Format("{0}_{1}_{2}", caseNumber, documentName ?? "NoNameDocument", date.ToYearMonthDayWithoutSeperator());

            if (zip.ContainsEntry(fileName + "." + fileExtension))
            {
                var nameParts = fileName.Split('_');
                var newName = string.Empty;

                if (nameParts[nameParts.Length - 1] == date.ToYearMonthDayWithoutSeperator())
                    newName = fileName + "_1";

                if (zip.ContainsEntry(newName + "." + fileExtension))
                {
                    var allKeys = zip.EntryFileNames.Where(o => o.Contains(fileName)).ToList();
                    allKeys.Sort();
                    var newNameParts = allKeys.LastOrDefault().Split('_');
                    var lastNumber = 0;
                    newNameParts[newNameParts.Length - 1] = newNameParts[newNameParts.Length - 1].Replace("." + fileExtension, "");
                    var isNumber = int.TryParse(newNameParts[newNameParts.Length - 1], out lastNumber);
                    newNameParts[newNameParts.Length - 1] = isNumber ? Convert.ToString(lastNumber + 1) : newNameParts[newNameParts.Length - 1] + "_1";
                    fileName = string.Join("_", newNameParts);
                    return fileName + "." + fileExtension;
                }
                else
                    return newName + "." + fileExtension;
            }

            return fileName + "." + fileExtension;
        }

        /// <summary>
        /// Appends a list of contacts to a stringbuilder
        /// </summary>
        /// <param name="email"></param>
        /// <param name="recipients"></param>
        private void AppendRecipients(StringBuilder email, List<Contact> recipients)
        {
            foreach (var recipient in recipients)
            {
                email.AppendFormat("{0} <{1}><br />", recipient.FullName, recipient.Email);
            }
        }

        /// <summary>
        /// Adds each attachment in a list to the passed in zip file
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="attachments"></param>
        /// <param name="caseExport"></param>
        private void AddAttachmentsToZipFile(ZipFile zip, List<Attachment> attachments, CaseExportRequest caseExport)
        {
            foreach (var attachment in attachments.Where(a => a.Document != null))
            {
                try
                {
                    var document = attachment.Document.Download();
                    if (document.File != null)
                    {
                        string fileName = string.Format("{0:yyyy-MM-dd HHmmss}_{1}", document.CreatedDate, document.FileName);
                        zip.AddEntry(fileName, document.File);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} as ZIP", attachment.Document.FileName));
                }
            }
        }

        /// <summary>
        /// Adds each attachment in a list to the passed in zip file for employee
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="attachments"></param>
        /// <param name="caseExport"></param>
        private void AddAttachmentsToZipFileForEmployee(ZipFile zip, List<Attachment> attachments, CaseExportRequest caseExport, Case theCase)
        {
            foreach (var attachment in attachments.Where(a => a.Document != null))
            {
                try
                {
                    var document = attachment.Document.Download();
                    if (document.File != null)
                    {
                        var file = document.FileName.Split('.');
                        var extension = file[file.Length - 1];
                        var fileName = CreateNewFileNameInZip(zip, theCase.CaseNumber, document.FileName.Replace("." + extension, ""), document.CreatedDate, extension);
                        zip.AddEntry(fileName, document.File);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} Attachment as ZIP & Exception Message : {1}", attachment.Document.FileName, ex.Message));
                }
            }
        }

        /// <summary>
        /// Adds each communication in a list to the passed in zip file
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        private void AddCaseToZipFile(ZipFile zip, Case theCase, List<CaseNote> caseNotes, List<ToDoItem> todoItems, List<Communication> communications, List<Attachment> attachments, CaseExportRequest caseExport)
        {
            try
            {
                byte[] pdf = CreateCasePdf(theCase, caseNotes, todoItems, communications, attachments, caseExport);
                zip.AddEntry("Case.pdf", pdf);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing case export file ", ex);
                ReportFailed(caseExport, "Failed to process case as ZIP");
            }
        }

        /// <summary>
        /// Adds each communication in a list to the passed in zip file for employee
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        private void AddCaseToZipFileForEmployee(ZipFile zip, Case theCase, List<CaseNote> caseNotes, List<ToDoItem> todoItems, List<Communication> communications, List<Attachment> attachments, CaseExportRequest caseExport)
        {
            try
            {
                var pdf = CreateCasePdf(theCase, caseNotes, todoItems, communications, attachments, caseExport);
                var fileName = CreateNewFileNameInZip(zip, theCase.CaseNumber, "Case", theCase.CreatedDate, "pdf");
                zip.AddEntry(fileName, pdf);
            }
            catch (Exception ex)
            {
                Log.Error("Error processing case export file ", ex);
                ReportFailed(caseExport, "Failed to process case as ZIP & Error Message : " + ex.Message);
            }
        }

        /// <summary>
        /// Creates a pdf from a list of communications and attachments
        /// </summary>
        /// <param name="communications"></param>
        /// <param name="attachments"></param>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        private byte[] ExportToPdf(Case theCase, List<Communication> communications, List<Attachment> attachments, List<CaseNote> caseNotes, List<ToDoItem> todoItems, CaseExportRequest caseExport)
        {
            byte[] casePdf = CreateCasePdf(theCase, caseNotes, todoItems, communications, attachments, caseExport);
            List<byte[]> communicationsPdfs = CreateCommunicationsPdfs(communications, caseExport);
            List<byte[]> attachmentsPdfs = CreateAttachmentsPdfs(attachments, caseExport);
            List<byte[]> allPdfs = new List<byte[]>(1 + communicationsPdfs.Count + attachmentsPdfs.Count)
            {
                casePdf
            };
            allPdfs.AddRange(communicationsPdfs);
            allPdfs.AddRange(attachmentsPdfs);
            byte[] sourcePdf =  Pdf.MergePdfDocumentsWithPageNumbers(allPdfs.ToArray());
            return Pdf.AddPdfMetadata(sourcePdf, CreatePdfMetadataFromCase(theCase));
        }

        /// <summary>
        /// Takes a list of communications and converts them to a PDF.
        /// </summary>
        /// <param name="pdf"></param>
        /// <param name="communications"></param>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        private List<byte[]> CreateCommunicationsPdfs(List<Communication> communications, CaseExportRequest caseExport)
        {
            List<byte[]> communicationPdfs = new List<byte[]>();

            // Went with a for each loop instead of aggregate because I'd prefer to call out to MergePdfDocuments ONCE per list instead of once per communication
            foreach (var communication in communications
                .Where(c => 
                        !string.IsNullOrEmpty(c.Body) 
                        && c.CommunicationType == CommunicationType.Email)
                .OrderBy(c => c.CreatedDate))
            {
                try
                {
                    byte[] communicationPdf = ConvertCommunicationEmailToPdf(communication, caseExport.CreatedBy);
                    communicationPdfs.Add(communicationPdf);
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} Communication as PDF & Exception Message : {1}", communication.Name, ex.Message));
                }
            }

            return communicationPdfs;
        }

        /// <summary>
        /// Takes a pdf and adds each attachment to the pdf
        /// </summary>
        /// <param name="pdf"></param>
        /// <param name="attachments"></param>
        /// <param name="caseExport"></param>
        /// <returns></returns>
        private List<byte[]> CreateAttachmentsPdfs(List<Attachment> attachments, CaseExportRequest caseExport)
        {
            List<byte[]> attachmentPdfs = new List<byte[]>();
            
            // Went with a for each loop instead of aggregate because I'd prefer to call out to MergePdfDocuments ONCE per list instead of once per attachment
            foreach (var attachment in attachments
                .Where(a => a.Document != null)
                .OrderBy(a => a.CreatedDate))
            {
                try
                {
                    var convertedDocument = ConvertDocumentToPdf(attachment.Document);
                    if (convertedDocument != null)
                        attachmentPdfs.Add(convertedDocument);
                }
                catch (Exception ex)
                {
                    Log.Error("Error processing case export file ", ex);
                    ReportFailed(caseExport, string.Format("Failed to process {0} Attachment as PDF & Exception Message : {1}", attachment.Document.FileName, ex.Message));
                }
            }

            return attachmentPdfs;
        }

        /// <summary>
        /// Converts a document to a pdf
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private byte[] ConvertDocumentToPdf(Document doc)
        {
            doc.Download();
            if (doc.File == null)
                return null;

            if (doc.ContentType == "application/msword" || doc.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                return Pdf.ConvertMsWordToPdf(doc.File);
            else if (doc.ContentType == "text/html")
                return Pdf.ConvertHtmlToPdf(Encoding.UTF8.GetString(doc.File));
            else if (doc.ContentType == "application/pdf")
                return doc.File;

            return null;
        }

        /// <summary>
        /// Converts a communication email to PDF
        /// </summary>
        /// <param name="communication"></param>
        /// <returns></returns>
        private byte[] ConvertCommunicationEmailToPdf(Communication communication, User user)
        {
            if (communication.CommunicationType != CommunicationType.Email)
                throw new ArgumentException("This method can only be called on emails");

            StringBuilder email = new StringBuilder();
            if(UserHasPermissionToThisCommunication(communication, user))
            {
                email.AppendLine("<p> Send To:");
                AppendRecipients(email, communication.Recipients);
                email.AppendLine("</p>");
                if (communication.CCRecipients.Any())
                {
                    email.AppendLine("<p> CC To:");
                    AppendRecipients(email, communication.CCRecipients);
                    email.AppendLine("</p>");
                }

                email.AppendFormat("<p>Email Sent: {0:f}</p>", communication.SentDate);
                email.Append(communication.Body);
            }
            else
            {
                email.AppendFormat("<p>Email Sent on {0:f} redacted due to permissions", communication.SentDate);
            }

            

            return Pdf.ConvertHtmlToPdf(email.ToString());
        }

        /// <summary>
        /// Determines whether a user can view this communications
        /// </summary>
        /// <param name="communication"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool UserHasPermissionToThisCommunication(Communication communication, User user)
        {
            if (string.IsNullOrEmpty(communication.Template))
                return true;

            return  CurrentUserPermissions.Has(communication.Template);
        }

        /// <summary>
        /// Determines whether a user can view this ToDo item
        /// </summary>
        /// <param name="todo"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool UserHasPermissionToThisToDoItem(ToDoItem todo, User user)
        {
            return CurrentUserPermissions.Has(todo.ItemType);
        }

        /// <summary>
        /// Whether a user has permission to access a case note
        /// </summary>
        /// <param name="note"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool UserHasPermissionToThisCaseNote(CaseNote note, User user)
        {
            if (note.Public)
                return true;

            return user.HasPermission(Permission.ViewConfidentialNotes);
        }

        /// <summary>
        /// Creates a PDF of all the case data
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="caseNotes"></param>
        /// <param name="todoItems"></param>
        /// <param name="communications"></param>
        /// <param name="attachments"></param>
        /// <returns></returns>
        private byte[] CreateCasePdf(Case theCase, List<CaseNote> caseNotes, List<ToDoItem> todoItems, List<Communication> communications, List<Attachment> attachments, CaseExportRequest caseExport)
        {
            byte[] coverSheet = CreateCaseCoverSheetPdf(theCase);
            byte[] caseSummary = CreateCaseSummaryPdf(theCase, caseExport.CreatedBy);
            byte[] employeeSummary = CreateEmployeeSummaryPdf(theCase.Employee, caseExport.CreatedBy, theCase);
            byte[] caseHistory = CreateCaseHistoryPdf(theCase);
            byte[] policySummary = CreatePolicySummaryPdf(theCase);
            byte[] caseActivity = CreateCaseActivityPdf(theCase, caseNotes, todoItems, communications, attachments, caseExport.CreatedBy);

            // If we're exporting it as PDF, we'll do the final step of stamping it with page numbers and adding metadata later
            if(caseExport.DownloadType == CaseExportFormat.Pdf)
                return Pdf.MergePdfDocuments(coverSheet, caseSummary, employeeSummary, caseHistory, policySummary, caseActivity);

            byte[] sourcePdf = Pdf.MergePdfDocumentsWithPageNumbers(coverSheet, caseSummary, employeeSummary, caseHistory, policySummary, caseActivity);
            return Pdf.AddPdfMetadata(sourcePdf, CreatePdfMetadataFromCase(theCase));
        }

        private byte[] CreateCaseActivityPdf(Case theCase, List<CaseNote> caseNotes, List<ToDoItem> todoItems, List<Communication> communications, List<Attachment> attachments, User user)
        {
            StringBuilder summary = new StringBuilder();
            int totalCapacity = caseNotes.Count + todoItems.Count + communications.Count + attachments.Count;
            foreach (var toDoItem in todoItems.Where(t => !UserHasPermissionToThisToDoItem(t, user)))
            {
                toDoItem.Title = "REDACTED";
            }

            foreach (var note in caseNotes.Where(n => !UserHasPermissionToThisCaseNote(n, user)))
            {
                note.Notes = "REDACTED";
            }

            List<CaseActivity> caseActivity = new List<CaseActivity>(totalCapacity);
            caseActivity.AddRange(caseNotes.Select(c => new CaseActivity(c)));
            caseActivity.AddRange(todoItems.Select(t => new CaseActivity(t)));
            caseActivity.AddRange(communications.Select(c => new CaseActivity(c)));
            caseActivity.AddRange(attachments.Select(a => new CaseActivity(a)));
            caseActivity = caseActivity.OrderBy(a => a.CreatedDate).ToList();

            summary.AppendLine("<h2>Case Activity</h2>");
            foreach (var activity in caseActivity)
            {
                summary.AppendFormat("<p>Created Date: {0:d}<br />" +
                    "Category: {1}<br />" +
                    "Created By: {2}<br />" +
                    "Type: {3}<br />" +
                    "Description: {4}<br /></p>",
                    activity.CreatedDate, activity.Category, activity.CreatedBy, activity.Type, activity.Description);
            }

            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        private byte[] CreatePolicySummaryPdf(Case theCase)
        {
            StringBuilder summary = new StringBuilder();
            summary.Append("<h2>Policy Summary</h2>");
            CaseSegment activeSegment = theCase.Segments.OrderByDescending(c => c.CreatedDate).FirstOrDefault();
            IEnumerable<AppliedPolicy> automaticPolicies = activeSegment.AppliedPolicies.Where(ap => ap.ManuallyAdded);
            IEnumerable<AppliedPolicy> manualPolicies = activeSegment.AppliedPolicies.Where(ap => !ap.ManuallyAdded);
            if(!theCase.Summary.Policies.Any() && !automaticPolicies.Any() && !manualPolicies.Any())
            {
                summary.Append("No Policies On Record<br />");
            }

            foreach (var policy in automaticPolicies)
            {
                AppendAppliedPolicy(summary, policy, theCase);
            }

            foreach (var policy in manualPolicies)
            {
                AppendAppliedPolicy(summary, policy, theCase);
            }

            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        private void AppendAppliedPolicy(StringBuilder summary, AppliedPolicy policy, Case theCase)
        {
            string eligiblePrefix = "";
            if (policy.Status != EligibilityStatus.Eligible)
                eligiblePrefix = "Not ";

            summary.AppendFormat("<h3>{0} - {1}Eligible</h3>", policy.Policy.Name, eligiblePrefix);
            var policySummary = theCase.Summary.Policies.FirstOrDefault(p => p.PolicyCode == policy.Policy.Code);
            if (policySummary == null)
                return;


            foreach (var detail in policySummary.Detail)
            {
                summary.AppendFormat("{0} : {1:d} - {2:d} {3}", detail.CaseType, detail.StartDate, detail.EndDate, detail.Determination);
            }
        }

        private byte[] CreateCaseHistoryPdf(Case theCase)
        {
            StringBuilder summary = new StringBuilder();
            List<Case> employeeCaseHistory = Case.AsQueryable()
                .Where(c => c.Employee.Id == theCase.Employee.Id && c.Id != theCase.Id)
                .OrderBy(c => c.CreatedDate).ToList();

            summary.Append("<h2>Case History</h2>");
            if (!employeeCaseHistory.Any())
            {
                summary.Append("Employee has no other cases.<br />");

            }
            else
            {
                foreach (var employeeCase in employeeCaseHistory)
                {
                    summary.AppendFormat("<p>Case Number: {0}<br />" +
                        "Reason: {1}<br />" +
                        "Start Date: {2:d}<br />" +
                        "End Date: {3:d}<br />" +
                        "Status: {4}<br /></p>",
                        employeeCase.CaseNumber, employeeCase.Reason.Name, employeeCase.StartDate, employeeCase.EndDate, employeeCase.Status);
                }
            }

            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        /// <summary>
        /// Creates a PDF Cover Sheet for Export
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        private byte[] CreateCaseCoverSheetPdf(Case theCase)
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("<h1>Cover Sheet</h1>");
            summary.AppendLine("<h2>CONFIDENTIAL</h2>");
            summary.AppendLine("<hr />");
            summary.AppendFormat("Case Number: {0}<br />", theCase.CaseNumber);
            summary.AppendFormat("Employee Number: {0}<br />", theCase.Employee.EmployeeNumber);
            summary.AppendFormat("Employee Name: {0}<br />", theCase.Employee.FullName);
            summary.AppendFormat("Absence Reason: {0}<br />", theCase.Reason.Name);
            summary.AppendFormat("Start Date: {0:d}<br />", theCase.StartDate);
            AppendCaseEndDate(summary, theCase);
            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        /// <summary>
        /// Creates a PDF case summary page to be 
        /// </summary>
        /// <param name="theCase"></param>
        /// <returns></returns>
        private byte[] CreateCaseSummaryPdf(Case theCase, User user)
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("<h1>Case Summary</h1>");
            summary.AppendLine("<hr />");
            summary.AppendFormat("Case Number: {0}<br />", theCase.CaseNumber);
            summary.AppendFormat("Absence Reason: {0}<br />", theCase.Reason.Name);
            summary.AppendFormat("Type: {0}<br />", theCase.CaseType);
            summary.AppendFormat("Status: {0}<br />", theCase.Status.ToString().SplitCamelCaseString());
            AppendRiskProfile(summary, theCase.RiskProfile, theCase.Employer);

            summary.AppendLine("<h2>Case Dates</h2>");
            summary.AppendFormat("Opened Date: {0:d}<br />", theCase.CreatedDate);
            summary.AppendFormat("Last Updated: {0:d}<br />", theCase.ModifiedDate);
            summary.AppendFormat("Start Date: {0:d}<br />", theCase.StartDate);
            AppendCaseEndDate(summary, theCase);
            AppendCaseClosedDate(summary, theCase);
            AppendCaseClosureReason(summary, theCase);
            AppendAdditionalCaseEvents(summary, theCase);

            summary.AppendLine("<h2>Case Details</h2>");
            summary.AppendFormat("<p>Summary: {0}</p>", theCase.Narrative);
            summary.AppendFormat("<p>Description: {0}</p>", theCase.Description);
            AppendCaseAssignees(summary, theCase);
            AppendCaseReporter(summary, theCase);
            AppendAuthorizedSubmitter(summary, theCase);
            AppendContact(summary, theCase);

            summary.AppendLine("<h2>Contact Preferences</h2>");
            AppendECommunication(summary, theCase, user);
            summary.AppendFormat("Email: {0}<br />", theCase.Employee.Info.AltEmail);
            AppendPhoneNumber(summary, "Phone Number", theCase.Employee.Info.AltPhone);
            AppendEmployeeAddress(summary, theCase);

            summary.AppendLine("<h2>Custom Fields</h2>");
            AppendCustomFields(summary, theCase.CustomFields, user);

            summary.AppendLine("<h2>Additional Data</h2>");
            AppendMetadata(summary, theCase);

            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        /// <summary>
        /// Appends all metadata stored on the case
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendMetadata(StringBuilder caseSummary, Case theCase)
        {
            foreach (var metadata in theCase.Metadata)
            {
                caseSummary.AppendFormat("{0}: {1}<br />", metadata.Name, metadata.Value);
            }
        }

        /// <summary>
        /// Appends all the custom fields and their value to the pdf
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendCustomFields(StringBuilder caseSummary, List<CustomField> customFields, User user)
        {
            if (customFields == null)
                return;

            foreach (var customField in customFields)
            {
                AppendPermissionControlledString(caseSummary, customField.Name, customField.SelectedValueText, user, Permission.ViewCustomFields);
            }
        }

        /// <summary>
        /// Appends whether an employee has requested e communications or not
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendECommunication(StringBuilder caseSummary, Case theCase, User user)
        {
            string eCommunications = "Not Provided";
            if (theCase.SendECommunication.HasValue)
            {
                if (theCase.SendECommunication.Value)
                    eCommunications = "Yes";
                else
                    eCommunications = "No";
            }


            AppendPermissionControlledString(caseSummary, "E-Communications", eCommunications, user, Permission.EditECommunicationRequest);
        }

        /// <summary>
        /// Appends the employee's address
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendEmployeeAddress(StringBuilder caseSummary, Case theCase)
        {
            if (theCase.Employee.Info.AltAddress == null)
                return;

            caseSummary.AppendFormat("Address: {0}", theCase.Employee.Info.AltAddress.ToString());
        }

        /// <summary>
        /// Appends all case events but Case Closed, since that gets a specific location
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendAdditionalCaseEvents(StringBuilder caseSummary, Case theCase)
        {
            foreach (var caseEvent in theCase.CaseEvents.Where(c => c.EventType != CaseEventType.CaseClosed))
            {
                AppendCaseEvent(caseSummary, theCase, caseEvent.EventType);
            }
        }

        /// <summary>
        /// Appends the case closed reason and details, if any
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendCaseClosureReason(StringBuilder caseSummary, Case theCase)
        {
            if (theCase.ClosureReason.HasValue)
                caseSummary.AppendFormat("Closure Reason: {0}<br />", theCase.ClosureReason.Value.ToString().SplitCamelCaseString());

            if(!string.IsNullOrEmpty(theCase.ClosureReasonDetails))
                caseSummary.AppendFormat("Closure Reason Details: {0}<br />", theCase.ClosureReasonDetails);
        }

        /// <summary>
        /// Appends the case closed date, if any
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendCaseClosedDate(StringBuilder caseSummary, Case theCase)
        {
            AppendCaseEvent(caseSummary, theCase, CaseEventType.CaseClosed);
        }

        /// <summary>
        /// Appends a case event type
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        /// <param name="eventType"></param>
        private void AppendCaseEvent(StringBuilder caseSummary, Case theCase, CaseEventType eventType)
        {
            DateTime? eventDate = theCase.GetCaseEventDate(eventType);
            if (eventDate == null)
                return;

            caseSummary.AppendFormat("{0}: {1:d}<br />", eventType.ToString().SplitCamelCaseString(), eventDate);
        }

        /// <summary>
        /// Appends a contact to the html, 
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendContact(StringBuilder caseSummary, Case theCase)
        {
            if (theCase.Contact == null || theCase.Contact.Contact == null)
                return;

            caseSummary.AppendFormat("Contact: {0} ", theCase.Contact.Contact.FullName);

            if (theCase.Contact.Contact.DateOfBirth.HasValue)
                caseSummary.AppendFormat("Contact Date Of Birth: {0:d}", theCase.Contact.Contact.DateOfBirth);
        }

        /// <summary>
        /// Appends the authorized submitter, if any
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendAuthorizedSubmitter(StringBuilder caseSummary, Case theCase)
        {
            if (theCase.AuthorizedSubmitter == null || theCase.AuthorizedSubmitter.Contact == null)
                return;

            caseSummary.AppendFormat("Authorized Submitter: {0} | {1}<br />", theCase.AuthorizedSubmitter.Contact.FullName, theCase.AuthorizedSubmitter.ContactTypeName);
        }

        /// <summary>
        /// Appends the CaseReporter, if any
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendCaseReporter(StringBuilder caseSummary, Case theCase)
        {
            if (theCase.CaseReporter == null || theCase.CaseReporter.Contact == null)
                return;

            caseSummary.AppendFormat("Case Reporter: {0} | {1}<br />", theCase.CaseReporter.Contact.FullName, theCase.CaseReporter.ContactTypeName);
        }

        /// <summary>
        /// Appends either the person assigned to the case, or a list of auto assigned people if applicable
        /// </summary>
        /// <param name="caseSummary"></param>
        /// <param name="theCase"></param>
        private void AppendCaseAssignees(StringBuilder caseSummary, Case theCase)
        {
            List<CaseAssignee> caseAssignees = CaseAssignee.AsQueryable().Where(ca => ca.CaseId == theCase.Id).ToList();
            List<string> distinctUserIds = caseAssignees.Distinct(c => c.UserId).Select(c => c.UserId).ToList();
            List<User> users = User.AsQueryable().Where(u => distinctUserIds.Contains(u.Id)).ToList();
            foreach (var assignee in caseAssignees)
            {
                var user = users.FirstOrDefault(u => u.Id == assignee.UserId);
                if (user == null)
                    continue;

                caseSummary.AppendFormat("Case Assignee: {0} ({1})<br />", user.DisplayName, assignee.Code);
            }

            if(caseAssignees.Count == 0 && !string.IsNullOrEmpty(theCase.AssignedToName))
            {
                caseSummary.AppendFormat("Assigned To: {0}<br />", theCase.AssignedToName);
            }
        }

        /// <summary>
        /// Appends a risk profile if the employer has the feature enabled, or None if there is no risk profile set
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="riskProfile"></param>
        private void AppendRiskProfile(StringBuilder summary, RiskProfile riskProfile, Employer employer)
        {
            if (!employer.HasFeature(Feature.RiskProfile))
                return;

            if (riskProfile == null)
            {
                summary.AppendFormat("Risk Profile: None<br />");
                return;
            }

            summary.AppendFormat("Risk Profile: {0}<br />", riskProfile.Name);
        }

        /// <summary>
        /// Appends the end date if it's not null, or just an empty placeholder if it isn't.
        /// </summary>
        /// <param name="stringBuilder"></param>
        /// <param name="theCase"></param>
        private void AppendCaseEndDate(StringBuilder stringBuilder, Case theCase)
        {
            if (!theCase.EndDate.HasValue)
            {
                stringBuilder.AppendLine("End Date: None<br />");
                return;
            }

            stringBuilder.AppendFormat("End Date: {0:d}<br />", theCase.EndDate.Value);
        }

        private byte[] CreateEmployeeSummaryPdf(Employee employee, User user, Case @case)
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("<h1>Employee Summary</h1>");
            summary.AppendLine("<hr />");
            summary.AppendFormat("Employee Name: {0}<br />", employee.FullName);
            summary.AppendFormat("Employee Number: {0}<br />", employee.EmployeeNumber);
            summary.AppendFormat("Employer Name: {0}<br />", employee.EmployerName);
            AppendRiskProfile(summary, employee.RiskProfile, employee.Employer);
            
            AppendHoursWorked(summary, employee, @case);
            
            AppendNullableDateTime(summary, "Date of Birth", employee.DoB, user, Permission.ViewEmployeeInfo);
            summary.AppendFormat("Date Of Birth: {0:d}<br />", employee.DoB);
            AppendEnum(summary, "Gender", employee.Gender, user, Permission.ViewEmployeeInfo);
            AppendSsn(summary, employee, user);

            summary.AppendLine("<h2>Contact Info</h2>");
            AppendPermissionControlledString(summary, "Address", employee.Info.Address.ToString(), user, Permission.ViewEmployeeContactInfo);
            AppendPhoneNumber(summary, "Work Phone", employee.Info.WorkPhone);
            AppendPhoneNumber(summary, "Cell Phone", employee.Info.CellPhone);
            AppendPhoneNumber(summary, "Home Phone", employee.Info.HomePhone);
            summary.AppendFormat("Work Email: {0}<br />", employee.Info.Email);
            summary.AppendFormat("Alt Email: {0}<br />", employee.Info.AltEmail);
            AppendPhoneNumber(summary, "Alt Phone", employee.Info.AltPhone);
            summary.AppendFormat("Alternate Mailing Address: {0}<br />", employee.Info.AltAddress);

            summary.AppendLine("<h2>Job Info</h2>");
            AppendPermissionControlledString(summary, "Work State", employee.WorkState, user, Permission.ViewEmployeeJobInfo);
            AppendEnum(summary, "Pay Type", employee.PayType, user, Permission.ViewEmployeeJobInfo);
            AppendEnum(summary, "Employement Status", employee.Status, user, Permission.ViewEmployeeJobInfo);
            AppendSalary(summary, employee, user);
            AppendNullableDateTime(summary, "Hire Date", employee.HireDate, user, Permission.ViewEmployeeJobInfo);
            AppendPermissionControlledString(summary, "Office Location", employee.GetOfficeLocationDisplay(), user, Permission.ViewEmployeeJobInfo);
            AppendBool(summary, "Exempt", employee.IsExempt, user, Permission.ViewEmployeeJobInfo);
            AppendBool(summary, "Key Employee", employee.IsKeyEmployee, user, Permission.ViewEmployeeJobInfo);
            AppendBool(summary, "Meets 50 in 75 Miles", employee.Meets50In75MileRule, user, Permission.ViewEmployeeJobInfo);
            AppendNullableDateTime(summary, "Re-hire Date", employee.RehireDate, user, Permission.ViewEmployeeJobInfo);
            AppendNullableDateTime(summary, "Service Date", employee.ServiceDate, user, Permission.ViewEmployeeJobInfo);
            AppendSpouse(summary, employee, user);
            AppendEnum(summary, "Military Status", employee.MilitaryStatus, user, Permission.ViewEmployeeJobInfo);
            AppendPaySchedule(summary, employee, user);
            summary.AppendFormat("Cost Center: {0}<br />", employee.CostCenterCode);
            AppendPermissionControlledString(summary, "Cost Center", employee.CostCenterCode, user, Permission.ViewEmployeeJobInfo);
            AppendPermissionControlledString(summary, "Employee Class", employee.EmployeeClassName, user, Permission.ViewEmployeeJobInfo);

            
            AppendJob(summary, employee, user);
            AppendJobHistory(summary, employee, user);

            AppendOrganizations(summary, employee, user);

            summary.AppendLine("<h2>Custom Fields</h2>");
            AppendCustomFields(summary, employee.CustomFields, user);
            AppendWorkSchedule(summary, employee, user);
            AppendLocationHistory(summary, employee, user);
            return Pdf.ConvertHtmlToPdf(summary.ToString());
        }

        /// <summary>
        /// Appends a nullable date time
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        /// <param name="user"></param>
        private void AppendNullableDateTime(StringBuilder summary, string label, DateTime? nullableDateTime, User user, Permission requiredPermission)
        {
            string formattedDateTime = "";
            if (nullableDateTime.HasValue)
                formattedDateTime = nullableDateTime.ToString("d");

            AppendPermissionControlledString(summary, label, formattedDateTime, user, requiredPermission);
        }

        /// <summary>
        /// Appends an employee's SSN
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        /// <param name="user"></param>
        private void AppendSsn(StringBuilder summary, Employee employee, User user)
        {
            string ssn = "";
            if (employee.Ssn != null)
                ssn = employee.Ssn.PlainText;

            AppendPermissionControlledString(summary, "SSN", ssn, user, Permission.ViewSSN);
        }

        /// <summary>
        /// Appends the salary
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        /// <param name="user"></param>
        private void AppendSalary(StringBuilder summary, Employee employee, User user)
        {
            string formattedSalary = "None";
            if (employee.Salary.HasValue)
                formattedSalary = employee.Salary.Value.ToString("0.00");

            AppendPermissionControlledString(summary, "Salary", formattedSalary, user, Permission.ViewSalary);
        }

        private void AppendPermissionControlledString(StringBuilder summary, string label, string value, User user, Permission requiredPermission)
        {
            if (!user.HasPermission(requiredPermission))
                value = "REDACTED";

            summary.AppendFormat("{0}: {1}<br />", label, value);
        }

        /// <summary>
        /// Appends the list of employee's location history
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendLocationHistory(StringBuilder summary, Employee employee, User user)
        {
            List<EmployeeOrganization> employeeLocationHistory =
                EmployeeOrganization.AsQueryable()
                .Where(eo => eo.EmployeeNumber == employee.EmployeeNumber
                    && eo.EmployerId == employee.EmployerId
                    && eo.TypeCode.Equals(OrganizationType.OfficeLocationTypeCode)).ToList();

            if (!employeeLocationHistory.Any())
                return;

            summary.AppendLine("<h2>Location History</h2>");
            if (!user.HasPermission(Permission.ViewEmployeeJobInfo))
            {
                summary.Append("Location History REDACTED");
                return;
            }

            foreach (var location in employeeLocationHistory)
            {
                DateTime? effectiveFromDate = null, effectiveToDate = null;
                if (location.Dates != null)
                {
                    effectiveFromDate = location.Dates.StartDate;
                    effectiveToDate = location.Dates.EndDate;
                }
                summary.AppendFormat("<p>Effective From: {0:d}<br />" +
                        "Effective To: {1:d}<br />" +
                        "Location: {2}<br /></p>",
                        effectiveFromDate, effectiveToDate, location.Name);
            }
        }

        /// <summary>
        /// Appends the list of employee's work schedules
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendWorkSchedule(StringBuilder summary, Employee employee, User user)
        {
            summary.Append("<h2>Schedule");
            if (!user.HasPermission(Permission.ViewEmployeeWorkSchedule))
            {
                summary.Append(" REDACTED</h2");
                return;
            }
            if (employee.StartDayOfWeek.HasValue)
                summary.AppendFormat(" - Week Starts On {0}", employee.StartDayOfWeek.Value);

            summary.Append("</h2>");
            if (employee.WorkSchedules == null)
                return;

            foreach (var schedule in employee.WorkSchedules)
            {
                string endDate = null;
                if (schedule.EndDate.HasValue)
                {
                    endDate = schedule.EndDate.Value.ToString("d");
                }
                else
                {
                    endDate = "(perpetual)";
                }

                summary.AppendFormat("<p>Effective From: {0:d}<br />" + 
                    "Effective To: {1}<br />" +
                    "Schedule: {2}</p>",
                    schedule.StartDate, endDate, schedule);
            }
        }


        /// <summary>
        /// Appends all the employee's level one organizations
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendOrganizations(StringBuilder summary, Employee employee, User user)
        {
            using (var organizationService = new OrganizationService(CurrentUser))
            {
                List<string> employeeOrganizations = organizationService.GetCurrentLevelOneEmployeeOrganizations(employee);
                if (!employeeOrganizations.Any())
                    return;

                summary.AppendLine("<h2>Organizations</h2>");
                foreach (var org in employeeOrganizations)
                {
                    AppendPermissionControlledString(summary, "Organization Name", org, user, Permission.ViewEmployeeJobInfo);
                }
            }
        }

        /// <summary>
        /// Appends any employee job history
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendJobHistory(StringBuilder summary, Employee employee, User user)
        {
            List<EmployeeJob> jobHistory = employee.GetJobs();
            if (!jobHistory.Any())
                return;

            if (!user.HasPermission(Permission.ViewEmployeeJobDetails))
            {
                summary.Append("Job History REDACTED<br />");
                return;
            }

            foreach (var job in jobHistory)
            {
                string officeLocation = "None";
                if (job.OfficeLocation != null)
                    officeLocation = job.OfficeLocation.Name;

                summary.AppendFormat("<p>Starting: {0:d}<br />" + 
                    "Ending: {1:d}<br />" + 
                    "Job: {2} ({3})<br />" + 
                    "Title: {4}<br />" +
                    "Location: {5}<br />" +
                    "Status: {6}<br /></p>",
                    job.Dates.StartDate, job.Dates.EndDate, job.JobName, job.JobCode, job.JobTitle, officeLocation, job.Status.Value.ToString().SplitCamelCaseString());
            }
        }

        /// <summary>
        /// Appends an employee's job, if they have one
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendJob(StringBuilder summary, Employee employee, User user)
        {
            var employeeJob = employee.GetCurrentJob();
            if (employeeJob == null || employeeJob.Job == null)
                return;

            summary.AppendLine("<h2>Job Details</h2>");
            var theActualJob = employeeJob.Job;
            AppendPermissionControlledString(summary, "Job", string.Format("{0} ({1})", theActualJob.Name, theActualJob.Code), user, Permission.ViewEmployeeJobDetails);

            if (!string.IsNullOrEmpty(employeeJob.JobTitle))
                AppendPermissionControlledString(summary, "Title", employeeJob.JobTitle, user, Permission.ViewEmployeeJobDetails);

            if (theActualJob.Activity.HasValue)
                AppendEnum(summary, "Activity", theActualJob.Activity, user, Permission.ViewEmployeeJobDetails);

            if(employeeJob.Dates != null && !employeeJob.Dates.IsNull)
            {
                AppendNullableDateTime(summary, "Start Date", employeeJob.Dates.StartDate, user, Permission.ViewEmployeeJobDetails);
                AppendNullableDateTime(summary, "End Date", employeeJob.Dates.EndDate, user, Permission.ViewEmployeeJobDetails);
            }
        }

        /// <summary>
        /// Appends the name of the pay schedule to the summary
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendPaySchedule(StringBuilder summary, Employee employee, User user)
        {
            string payScheduleName = "None";
            if (employee.PaySchedule != null)
                payScheduleName = employee.PaySchedule.Name;

            summary.AppendFormat("Pay Schedule: {0}<br />", payScheduleName);
            AppendPermissionControlledString(summary, "Pay Schedule", payScheduleName, user, Permission.ViewEmployeeJobInfo);
        }

        /// <summary>
        /// Appends a nullable enum text to the summary
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="label"></param>
        /// <param name="theEnum"></param>
        private void AppendEnum(StringBuilder summary, string label, Enum theEnum)
        {
            AppendEnum(summary, label, theEnum, null, new Permission());
        }

        /// <summary>
        /// Appends an enum
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="label"></param>
        /// <param name="theEnum"></param>
        /// <param name="user"></param>
        /// <param name="requiredPermission"></param>
        private void AppendEnum(StringBuilder summary, string label, Enum theEnum, User user, Permission requiredPermission)
        {
            string enumText = "N/A";
            if (theEnum != null)
                enumText = theEnum.ToString().SplitCamelCaseString();

            if(user == null || string.IsNullOrEmpty(requiredPermission.Id))
            {
                summary.AppendFormat("{0}: {1}<br />", label, enumText);
            }
            else
            {
                AppendPermissionControlledString(summary, label, enumText, user, requiredPermission);
            }
        }


        /// <summary>
        /// Appends the spouse info, if any
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="employee"></param>
        private void AppendSpouse(StringBuilder summary, Employee employee, User user)
        {
            if (employee.SpouseEmployee == null)
                return;

            AppendPermissionControlledString(summary, "Employee's Spouse", string.Format("{0} {1}", employee.SpouseEmployee.FirstName, employee.SpouseEmployee.LastName), user, Permission.ViewEmployeeJobInfo);
        }

        /// <summary>
        /// Converts a boolean to yes/no
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="label"></param>
        /// <param name="toTest"></param>
        private void AppendBool(StringBuilder summary, string label, bool toTest)
        {
            AppendBool(summary, label, toTest, null, new Permission());
        }

        /// <summary>
        /// Converts a boolean to yes/no
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="label"></param>
        /// <param name="toTest"></param>
        private void AppendBool(StringBuilder summary, string label, bool toTest, User user, Permission requiredPermission)
        {
            string boolString = "No";
            if (toTest)
                boolString = "Yes";

            if(user == null)
            {
                summary.AppendFormat("{0}: {1}<br />", label, boolString);
            }
            else
            {
                AppendPermissionControlledString(summary, label, boolString, user, requiredPermission);
            }
            
        }

        /// <summary>
        /// Appends a phone number with a specified label
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="label"></param>
        /// <param name="phoneNumber"></param>
        private void AppendPhoneNumber(StringBuilder summary, string label, string phoneNumber)
        {

            string formattedPhoneNumber = "None";
            if (!string.IsNullOrEmpty(phoneNumber))
                formattedPhoneNumber = phoneNumber.FormatPhone();

            summary.AppendFormat("{0}: {1}<br />", label, formattedPhoneNumber);
        }

        /// <summary>
        /// Appends the prior hours worked for an employee in the last 12 months.
        /// </summary>
        /// <param name="summary">The summary.</param>
        /// <param name="employee">The employee.</param>
        /// <param name="case">The case; could be null.</param>
        private void AppendHoursWorked(StringBuilder summary, Employee employee, Case @case)
        {
            if (employee.PriorHours != null && employee.PriorHours.Any())
            {
                // Appends the prior hours worked in last 12 months to the document with the as of date.
                void appendPriorHoursWorked(PriorHours prior)
                {
                    summary.Append($"Prior Hours Worked: {prior.HoursWorked} as of {prior.AsOf:d}<br />");
                }

                var priorHoursImportantToThisCase = employee.PriorHours
                    // Get any Prior Hours Worked during the case
                    .Where(h => @case != null && h.AsOf.DateInRange(@case.StartDate, @case.EndDate))
                    // Get the latest Prior Hours Worked as of the Case Created Date
                    .Union(employee.PriorHours.Where(h => @case != null && h.AsOf <= @case.CreatedDate).Take(1))
                    // Get the latest Prior Hours Worked as of the Case Start Date
                    .Union(employee.PriorHours.Where(h => @case != null && h.AsOf < @case.StartDate).Take(1))
                    // Ensure we only have a distinct list of Prior Hours Worked as we may have added duplicates above
                    .Distinct(h => h.Id)
                    // Order them by the AsOf date as we're going to list them in order
                    .OrderBy(h => h.AsOf);

                // Append these out to the document HTML
                priorHoursImportantToThisCase.ForEach(appendPriorHoursWorked);

                // If that didn't capture any because the case was null or there wasn't any known
                //  prior hours worked in the last 12 months before or during the case, just get
                //  the latest one.
                if (!priorHoursImportantToThisCase.Any())
                {
                    // Get and append the latest prior hours worked (what would be displayed in the UI)
                    appendPriorHoursWorked(employee.PriorHours.OrderBy(h => h.AsOf).Last());
                }

                // If the case is not null, then get the Case copy of prior hours and label it as such
                if (@case != null)
                {
                    // Get and append the latest prior hours worked (what would be displayed in the UI)
                    var prior = @case.Employee.PriorHours.OrderBy(h => h.AsOf).Last();
                    summary.Append($"Prior Hours Worked: {prior.HoursWorked} as of {prior.AsOf:d} (at time of case creation)<br />");
                }
            }

            if(employee.WorkSchedules != null && employee.WorkSchedules.Any())
            {
                double? totalHoursWorked = LeaveOfAbsence.TotalHoursWorkedLast12Months(employee);
                summary.AppendFormat("Prior Hours Worked: {0} (calculated)<br />", totalHoursWorked);
            }
        }

        private Dictionary<string, string> CreatePdfMetadataFromCase(Case theCase)
        {
            Dictionary<string, string> caseMetadata = new Dictionary<string, string>();
            caseMetadata.Add("CaseNumber", theCase.CaseNumber);
            caseMetadata.Add("EmployeeNumber", theCase.Employee.EmployeeNumber);
            caseMetadata.Add("EmployeeName", theCase.Employee.FullName);
            caseMetadata.Add("AbsenceReason", theCase.Reason.Name);
            caseMetadata.Add("CaseStartDate", theCase.StartDate.ToString("MM/dd/yyyy"));
            if (theCase.EndDate.HasValue)
            {
                caseMetadata.Add("CaseEndDate", theCase.EndDate.ToString("MM/dd/yyyy"));
            }

            return caseMetadata;
        }

        /// <summary>
        /// Create metadata for bulk PDF created for employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private Dictionary<string, string> CreatePdfMetadataFromEmployee(Employee employee)
        {
            var caseMetadata = new Dictionary<string, string>
            {
                { "EmployeeNumber", employee.EmployeeNumber },
                { "EmployeeName", employee.FullName },
                { "CustomerName", employee.Customer.Name },
                { "EmployerName", employee.Employer.Name },
                { "CreateTime", DateTime.Now.ToString("MM/dd/yyyy") }
            };

            return caseMetadata;
        }

        #endregion
    } // CaseExportService
} // namespace
