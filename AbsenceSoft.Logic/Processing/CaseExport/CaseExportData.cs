﻿using AbsenceSoft.Data.Enums;
using System.ComponentModel.DataAnnotations;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using System;

namespace AbsenceSoft.Logic.Processing.CaseExport
{
    [Serializable]
    public class CaseExportData
    {

        public CaseExportData( string initiatedByUserId, string caseExportRequestId)
        {
            this.InitiatedByUserId = initiatedByUserId;
            this.CaseExportRequestId = caseExportRequestId;
        }

    
        [Required]
        public string CaseExportRequestId { get; set; }
        
   
        [Required]
        public string InitiatedByUserId { get; set; }
    }
}