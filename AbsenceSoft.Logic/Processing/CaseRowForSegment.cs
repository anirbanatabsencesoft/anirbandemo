﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using static AbsenceSoft.Logic.Processing.CaseRow;

namespace AbsenceSoft.Logic.Processing
{
    internal class CaseRowForSegment : BaseRow
    {
        #region Fields

        public RecordType RecType;

        /// <summary>
        /// Mandatory. Associate ID number or unique identifier, either from your HRIS or other system; if numeric may contain leading zeros, however these leading zeros 
        /// will then always be required when identifying this associate.
        /// </summary>
        public string EmployeeNumber;

        /// <summary>
        /// Mandatory. The unique case number assigned to the case from the system the case is being converted from. This may be in any format, any length and any composition. 
        /// AbsenceTracker will keep this literal case number value as "the" case number within AbsenceTracker as well.
        ///This case number must also match all other conversion records that impact this case such as Create Case("C") records, Time Off Requests, etc.
        /// </summary>
        public string UniqueCaseNumber;

        /// <summary>
        /// Mandatory.Absence interval for leave or case type should be one of 
        /// 1 = Consecutive, 2 = Intermittent, 4 = Reduced Schedule, 8 = Administrative
        /// </summary>
        public int? LeaveInterval;

        /// <summary>
        /// Mandatory. The start date for this case type segment.
        /// </summary>
        public DateTime? StartDate;

        /// <summary>
        /// Optional.The end date for this case type segment. if no end date is supplied will be assumed perpetual through the end of the case 
        /// </summary>
        public DateTime? EndDate;

        /// <summary>
        /// Optional. The scheduled minutes per week which should be divided equally between M-F to represent the employee's weekly schedule.
        /// Useful if an employee works M-F and individual days of the week cannot be provided, OR, a variable schedule is being used 
        /// and the average weekly schedule simply amounts to a set weekly amount of time on average.
        /// This value is always divided between M-F of the week, for example "480" would be split M-F as a M-F 8h per day work schedule.
        /// </summary>
        public int? MinutesPerWeek;

        /// <summary>
        /// Optional. As part of the weekly schedule, 
        /// the employee is scheduled for x number of minutes on every Sunday.
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? SundayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Monday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? MondayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Tuesday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? TuesdayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Wednesday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? WednesdayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Thursday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? ThursdayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Friday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? FridayScheduledMinutes;

        /// <summary>
        /// Optional. As part of the weekly schedule, the employee is scheduled for x number of minutes on every Saturday. 
        /// Leave this field blank if providing Minutes per Week (IDX 02). Max value is 1440. Min value is 0.
        /// </summary>
        public int? SaturdayScheduledMinutes;

        /// <summary>
        /// Optional. If set to 1, Y, y, T or t will specify that this schedule is an average weekly schedule and that "M" records 
        /// may be supplied to specify actual scheduled time historically or as known or these  values are to be manually input into the UI at a later time.
        /// Default is false
        /// </summary>
        public bool? VariableScheduleFlag;

        #endregion

        #region const

        private const int S_RecordTypeOrdinal = 0;
        private const int S_EmployeeNumberOrdinal = 1;
        private const int S_UniqueCaseNumberOrdinal = 2;
        private const int S_LeaveIntervalOrdinal = 3;
        private const int S_StartDateOrdinal = 4;
        private const int S_EndDateOrdinal = 5;
        private const int S_MinutesPerWeekOrdinal = 6;
        private const int S_SundayScheduledMinutesOrdinal = 7;
        private const int S_MondayScheduledMinutesOrdinal = 8;
        private const int S_TuesdayScheduledMinutesOrdinal = 9;
        private const int S_WednesdayScheduledMinutesOrdinal = 10;
        private const int S_ThursdayScheduledMinutesOrdinal = 11;
        private const int S_FridayScheduledMinutesOrdinal = 12;
        private const int S_SaturdayScheduledMinutesOrdinal = 13;
        private const int S_VariableScheduleFlagOrdinal = 14;

        public CaseRowForSegment(string[] rowSource) : base(rowSource) { }

        #endregion

        #region Parse

        /// <summary>
        /// Parse and validate record fields
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        /// <param name="customFields">The custom fields.</param>
        /// <returns>Is Valid</returns>
        public override bool Parse(string[] rowSource = null, IEnumerable<CustomField> customFields = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (Required(S_EmployeeNumberOrdinal, "Employee Number"))
            {
                EmployeeNumber = Normalize(S_EmployeeNumberOrdinal);
            }

            if (Required(S_UniqueCaseNumberOrdinal, "Case Number"))
            {
                UniqueCaseNumber = Normalize(S_UniqueCaseNumberOrdinal);
            }

            if (Required(S_LeaveIntervalOrdinal, "Leave Interval"))
            {
                LeaveInterval = ParseInt(S_LeaveIntervalOrdinal, "Leave Interval");
            }

            if (Required(S_StartDateOrdinal, "Start Date"))
            {
                StartDate = ParseDate(S_StartDateOrdinal, "Start Date");
            }

            EndDate = ParseDate(S_EndDateOrdinal, "End Date");

            MinutesPerWeek = ParseInt(S_MinutesPerWeekOrdinal, "Minutes Per Week");

            SundayScheduledMinutes = ParseInt(S_SundayScheduledMinutesOrdinal, "Sunday Scheduled Minutes");
            MondayScheduledMinutes = ParseInt(S_MondayScheduledMinutesOrdinal, "Monday Scheduled Minutes");
            TuesdayScheduledMinutes = ParseInt(S_TuesdayScheduledMinutesOrdinal, "Tuesday Scheduled Minutes");
            WednesdayScheduledMinutes = ParseInt(S_WednesdayScheduledMinutesOrdinal, "Wednesday Scheduled Minutes");
            ThursdayScheduledMinutes = ParseInt(S_ThursdayScheduledMinutesOrdinal, "Thursday Scheduled Minutes");
            FridayScheduledMinutes = ParseInt(S_FridayScheduledMinutesOrdinal, "Friday Scheduled Minutes");
            SaturdayScheduledMinutes = ParseInt(S_SaturdayScheduledMinutesOrdinal, "Saturday Scheduled Minutes");
            VariableScheduleFlag = ParseBool(S_VariableScheduleFlagOrdinal, "Is Incomplete");

            return true;
        }

        /// <summary>
        /// Parses the int.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected override int? ParseInt(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            int n;
            if (int.TryParse(data, out n))
            {
                return n;
            }
                

            AddError("'{0}' is not a valid integer. Please provide a valid integer value for '{1}'.", data, name);

            if (name.Contains("Scheduled Minutes") && n < 0 || n > 1440)
            {
                AddError("The '{0}' value of '{1}' is invalid. It may not be less than zero or more than 1,440 minutes.", data, name);
            }

            return null;
        }

        #endregion

        #region Apply Service

        /// <summary>
        /// Applies the properties in this record to the target case, employing any necessary additional validation
        /// </summary>
        /// <param name="myCase">Case</param>
        /// <param name="emp">Employee</param>
        /// <returns>ApplicationResult</returns>
        internal ApplicationResult Apply(Case myCase, Employee emp)
        {
            if (myCase == null || string.IsNullOrWhiteSpace(myCase.Id))
            {
                AddError("Cannot create Segment, case does not exists. Please use the AbsenceTracker application to modify/create this case.");
                return ApplicationResult.Fail;
            }

            // Leave Interval or case type should be 1 = Consecutive | 2 = Intermittent | 4 = Reduced | 8 = Administrative
            if (LeaveInterval != 1 && LeaveInterval != 2 && LeaveInterval != 4 && LeaveInterval != 8)
            {
                AddError("Cannot create Segment, Leave Interval in invalid. Please use valid Leave Interval.");
                return ApplicationResult.Fail;
            }

            Schedule reducedSchedule = null;

            // set case type
            CaseType caseType = (CaseType)(LeaveInterval);

            if (caseType == CaseType.Reduced)
            {
                reducedSchedule = GetReduedSchedule();
            }

            // set IsNew flag
            bool isNewStartDate = StartDate < myCase.StartDate;
            bool isNewEndDate = EndDate > myCase.EndDate;


            using (CaseService svc = new CaseService())
            {
                myCase = svc.ChangeCase(myCase, StartDate.Value, EndDate.Value, isNewStartDate, isNewEndDate, caseType, false, false, reducedSchedule);
                svc.UpdateCase(myCase);
            }

            return ApplicationResult.Success;
        }

        /// <summary>
        /// Create and return reduced schedule based on day sheduled times
        /// </summary>
        /// <returns>Schedule</returns>
        private Schedule GetReduedSchedule()
        {
            Schedule reduced = new Schedule()
            {
                StartDate = StartDate.Value,
                EndDate = EndDate ?? null,
                ScheduleType = ScheduleType.Weekly,
            };

            DateTime sample = StartDate.Value.GetFirstDayOfWeek();

            for (var d = 0; d < 7; d++)
            {
                int? workTime = null;
                switch (sample.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        workTime = SundayScheduledMinutes;
                        break;
                    case DayOfWeek.Monday:
                        workTime = MinutesPerWeek.HasValue ? MinutesPerWeek / 5 : MondayScheduledMinutes;
                        break;
                    case DayOfWeek.Tuesday:
                        workTime = MinutesPerWeek.HasValue ? MinutesPerWeek / 5 : TuesdayScheduledMinutes;
                        break;
                    case DayOfWeek.Wednesday:
                        workTime = MinutesPerWeek.HasValue ? MinutesPerWeek / 5 : WednesdayScheduledMinutes;
                        break;
                    case DayOfWeek.Thursday:
                        workTime = MinutesPerWeek.HasValue ? MinutesPerWeek / 5 : ThursdayScheduledMinutes;
                        break;
                    case DayOfWeek.Friday:
                        workTime = MinutesPerWeek.HasValue ? MinutesPerWeek / 5 : FridayScheduledMinutes;
                        break;
                    case DayOfWeek.Saturday:
                        workTime = SaturdayScheduledMinutes;
                        break;
                }

                reduced.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                sample = sample.AddDays(1);
            }

            return reduced;
        }

        #endregion
    }
}
