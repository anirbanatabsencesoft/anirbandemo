﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Logic.Processing.EL;
using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using Ionic.Zip;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data;
using AbsenceSoft.Common;
using static AbsenceSoft.Logic.Processing.CaseRow;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Logic.Processing
{
    public class CaseMigrationService : LogicService
    {
        TextWriter report = null;
        Stopwatch watch = null;


        string AttachmentFilePath { get; set; }

        public CaseMigrationService() : base() { watch = new Stopwatch(); }
        public CaseMigrationService(string employerId) : this()
        {
            EmployerId = employerId;
        }
        public CaseMigrationService(string employerId, string reportPath, string attachementPath = null) : this(employerId)
        {
            SetReportOutput(reportPath);
            AttachmentFilePath = attachementPath;
        }


        public void SetReportOutput(string path)
        {
            if (report != null) report.Dispose();
            report = null;
            if (string.IsNullOrWhiteSpace(path))
                return;
            report = new StreamWriter(path, true, Encoding.UTF8) { AutoFlush = true };
        }
        public void SetReportOutput(TextWriter writer) => report = writer;

        public override void Dispose()
        {
            if (report != null)
                report.Dispose();
            base.Dispose();
        }

        #region Process

        [Serializable]
        public class ConversionFileRow
        {
            public long rowNumber { get; set; }
            public string[] recordText { get; set; }
            public char delimiter { get; set; }
            public string lineText { get; set; }
        }

        public bool ProcessFile(string path)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ProcessFile"))
            {
                ReportHeader();
                try
                {
                    // Simple common-sense validation
                    if (string.IsNullOrWhiteSpace(path))
                        return ReportFailed("No file path was specified.");

                    if (string.IsNullOrWhiteSpace(EmployerId))
                        return ReportFailed("Employer Id was not specified, this is required.");

                    Employer employer = Employer.GetById(EmployerId);
                    if (employer == null)
                        return ReportFailed(string.Format("No Employer was not found with the Id '{0}'.", EmployerId));

                    if (string.IsNullOrWhiteSpace(AttachmentFilePath))
                        AttachmentFilePath = Path.GetDirectoryName(path);

                    CustomerId = employer.CustomerId;

                    var customFieldToUpdate = CustomField.DistinctFind(null, CustomerId, EmployerId, true).ToList();
                    var employeeClassTypes = EmployeeClass.DistinctFind(null, CustomerId, EmployerId).ToList();
                    List<Employee> lstEmployee = new List<Employee>();
                    // Read the file line by line, assume it's a zip file but this method will also perform normal processing
                    //  if the file in question is not a zip file (hooray for wicked smaht logic).
                    List<ConversionFileRow> records = new List<ConversionFileRow>();
                    using (FileService fs = new FileService())
                        fs.ReadDelimitedZipFileByLine(path, (rowNumber, recordText, delimiter, lineText) => records.Add(new ConversionFileRow()
                        {
                            rowNumber = rowNumber,
                            recordText = recordText,
                            delimiter = delimiter,
                            lineText = lineText
                        }));
                    records
                        .GroupBy(r => r.recordText[1])
                        .AsParallel()
                        .WithDegreeOfParallelism(3)
                        .ForAll(g => g.OrderBy(o => o.recordText[0] == "E" ? 1 : o.recordText[0] == "C" ? 2 : 3).ForEach(r => ProcessRecord(r.rowNumber, r.recordText, r.delimiter, r.lineText, customFieldToUpdate, lstEmployee, employeeClassTypes)));

                    return true;
                }
                catch (Exception ex)
                {
                    ReportFailed(ex.ToString());
                }
             
                return false;
            }//end: InstrumentationContext
        }





        #region Export Data

        /// <summary>
        /// Our case record does not have a concept of multiple segments
        /// The character array is so that, given the index of a segment, the case number can be prepended with the appropriate character
        /// </summary>
        private static char[] base26Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

        /// <summary>
        /// A lazy loaded list of Contact Types, used when setting the Contact Type for Family Relationship
        /// </summary>
        private List<ContactType> CustomerContactTypes { get; set; }

        public bool ExportFile(string path)
        {
            ReportStart(path);
            try
            {
                // Simple common-sense validation
                if (string.IsNullOrWhiteSpace(path))
                    return ReportFailed("No file path was specified.");

                if (string.IsNullOrWhiteSpace(EmployerId))
                    return ReportFailed("Employer Id was not specified, this is required.");

                Employer employer = Employer.GetById(EmployerId);
                if (employer == null)
                    return ReportFailed(string.Format("No Employer was found with the Id '{0}'.", EmployerId));

                CustomerId = employer.CustomerId;

                Stream textStream = new MemoryStream();
                TextWriter writer = new StreamWriter(textStream);
                StreamReader reader = new StreamReader(textStream);
                ZipFile zip = new ZipFile(path);

                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.Comment = string.Format("This compressed, password protected archive is for '{0}' only and is for use with the AbsenceTracker system.", employer.Name);
                var cases = Case.AsQueryable().Where(c => c.CustomerId == CustomerId && c.EmployerId == EmployerId).ToList(); // Get all cases, hooray
                (report ?? Console.Out).WriteLine("Found {0} cases to export", cases.Count());
                foreach (var rec in cases)
                {
                    (report ?? Console.Out).WriteLine("Writing rows for case {0}", rec.CaseNumber);
                    bool hasMoreThanOneSegment = rec.Segments.Count > 1;
                    for (int i = 0; i < rec.Segments.Count; i++)
                    {
                        var currentSegment = rec.Segments[i];
                        WriteRow(CopyCaseRecord(rec, currentSegment, hasMoreThanOneSegment, i), writer);
                        WriteDetailRows(rec, currentSegment, hasMoreThanOneSegment, i, writer);
                        WriteIntermittentRows(rec, currentSegment, hasMoreThanOneSegment, i, writer);
                    }

                    List<CaseNote> caseNotes = CaseNote.AsQueryable().Where(cn => cn.CaseId == rec.Id).ToList();
                    WriteCaseNoteRows(rec, writer);
                    AddAttachmentsToZipFile(rec, zip);
                }

                (report ?? Console.Out).WriteLine("Creating csv");
                textStream.Position = 0;
                zip.AddEntry("Case Data Export.csv", reader.ReadToEnd());
                zip.Save();

                return true;
            }
            catch (Exception ex)
            {
                ReportFailed(ex.ToString());
            }
            finally
            {
                ReportEnd();
            }
            return false;
        }

        private void AddAttachmentsToZipFile(Case theCase, ZipFile zip)
        {
            List<Attachment> caseAttachments = Attachment.AsQueryable().Where(a => a.CaseId == theCase.Id).ToList();
            if (caseAttachments.Count() < 1)
                return;

            (report ?? Console.Out).WriteLine("Found {0} Attachments for {1}", caseAttachments.Count, theCase.CaseNumber);
            string directoryName = string.Format("{0} - Attachments", theCase.CaseNumber);
            zip.AddDirectoryByName(directoryName);
            foreach (var attachment in caseAttachments)
            {
                try
                {
                    (report ?? Console.Out).WriteLine("Adding Attachment {0} for {1}", attachment.FileName, theCase.CaseNumber);
                    string fileName = attachment.FileName;
                    var matchingAttachments = caseAttachments.Where(a => a.FileName == attachment.FileName).ToList();
                    if (matchingAttachments.Count() > 1)
                        fileName = string.Format("{0} - {1}", attachment.FileName, matchingAttachments.IndexOf(a => a.Id == attachment.Id) + 1);

                    zip.AddEntry(Path.Combine(directoryName, attachment.FileName), attachment.Document.Download().File);
                }
                catch (Exception oex)
                {
                    (report ?? Console.Out).WriteLine("Failed adding Attachment {0} for {1}", attachment.FileName, theCase.CaseNumber);
                    ReportError(0, string.Format("{0} - {1}", theCase.CaseNumber, attachment.FileName), oex.ToString());
                }
            }
        }

        private void WriteRow(CaseRow row, TextWriter writer)
        {
            if (row == null)
                return;

            writer.Write(row.ToString());
            writer.Write(writer.NewLine);
        }

        private void WriteDetailRows(Case theCase, CaseSegment currentSegment, bool hasMoreThanOneSegment, int index, TextWriter writer)
        {
            foreach (var policy in currentSegment.AppliedPolicies)
            {
                var groupedUsages = policy.Usage.GroupBy(u => u.Determination);
                foreach (var usage in groupedUsages)
                {
                    WriteRow(CopyDetailRecord(theCase, currentSegment, policy, usage.Key, usage.ToList(), hasMoreThanOneSegment, index), writer);

                }
            }
        }

        private void WriteIntermittentRows(Case theCase, CaseSegment currentSegment, bool hasMoreThanOneSegment, int index, TextWriter writer)
        {
            foreach (var request in currentSegment.UserRequests)
            {
                foreach (var detail in request.Detail)
                {
                    WriteRow(CopyIntermittentRecord(theCase, currentSegment, request, detail, hasMoreThanOneSegment, index), writer);
                }
            }
        }

        private void WriteCaseNoteRows(Case theCase, TextWriter writer)
        {
            List<CaseNote> caseNotes = CaseNote.AsQueryable().Where(cn => cn.CaseId == theCase.Id).ToList();
            foreach (var note in caseNotes)
            {
                WriteRow(CopyCaseNoteRecord(theCase, note), writer);
            }
        }

        private CaseRow CreateBaseRow(CaseRow.RecordType recordType, Case theCase, bool hasMoreThanOneSegment = false, int currentIndex = 0)
        {
            CaseRow row = new CaseRow()
            {
                RecType = recordType,
                CaseNumber = CreateCaseNumber(theCase, hasMoreThanOneSegment, currentIndex)
            };

            CopyEmployeeInformation(row, theCase.Employee);

            return row;
        }

        private CaseRow CopyCaseRecord(Case theCase, CaseSegment currentSegment, bool hasMoreThanOneSegment, int currentIndex)
        {
            CaseRow row = CreateBaseRow(CaseRow.RecordType.Case, theCase, hasMoreThanOneSegment, currentIndex);

            row.LeaveInterval = currentSegment.Type;
            row.DateCaseOpened = theCase.CreatedDate;
            row.ExpectedStartDate = theCase.StartDate;
            row.ExpectedRTWDate = theCase.EndDate;
            row.CaseStatus = theCase.Status;
            row.CaseClosedDate = CopyCaseEventDate(theCase, CaseEventType.CaseClosed);
            row.ConfirmedRTWDate = CopyCaseEventDate(theCase, CaseEventType.ReturnToWork);
            row.DeliveryDate = CopyCaseEventDate(theCase, CaseEventType.DeliveryDate);
            row.BondingStartDate = CopyCaseEventDate(theCase, CaseEventType.BondingStartDate);
            row.BondingEndDate = CopyCaseEventDate(theCase, CaseEventType.BondingEndDate);
            row.IllnessOrInjuryDate = CopyCaseEventDate(theCase, CaseEventType.IllnessOrInjuryDate);
            row.HospitalAdmissionDate = CopyCaseEventDate(theCase, CaseEventType.HospitalAdmissionDate);
            row.HospitalReleaseDate = CopyCaseEventDate(theCase, CaseEventType.HospitalReleaseDate);
            row.RTWReleaseDate = CopyCaseEventDate(theCase, CaseEventType.ReleaseReceived);

            CopyReasonInformation(row, theCase.Reason);
            CopyRelationshipInformation(row, theCase.Contact);
            CopyIntermittentInformation(row, theCase, currentSegment);

            return row;
        }

        private CaseRow CopyDetailRecord(Case theCase, CaseSegment currentSegment, AppliedPolicy policy, AdjudicationStatus determination, List<AppliedPolicyUsage> usage, bool hasMoreThanOneSegment, int currentIndex)
        {
            CaseRow row = CreateBaseRow(CaseRow.RecordType.Adjudication, theCase, hasMoreThanOneSegment, currentIndex);

            row.AdjudicationStartDate = currentSegment.StartDate;
            row.AdjudicationEndDate = currentSegment.EndDate;
            row.AdjudicationStatus = determination;
            row.MinutesUsed = Convert.ToInt32(usage.Sum(u => u.MinutesUsed));
            row.PolicyCode = policy.Policy.Code;

            return row;
        }

        private CaseRow CopyIntermittentRecord(Case theCase, CaseSegment currentSegment, IntermittentTimeRequest intermittentRequest, IntermittentTimeRequestDetail intermittentDetail, bool hasMoreThanOneSegment, int currentIndex)
        {
            if (currentSegment.Type != CaseType.Intermittent)
                return null;

            CaseRow row = CreateBaseRow(CaseRow.RecordType.Intermittent, theCase, hasMoreThanOneSegment, currentIndex);
            row.DateOfAbsence = intermittentRequest.RequestDate;
            row.PolicyCode = intermittentDetail.PolicyCode;
            row.MinutesApproved = intermittentDetail.Approved;
            row.MinutesDenied = intermittentDetail.Pending;
            row.MinutesPending = intermittentDetail.Pending;

            return row;
        }

        private string CreateCaseNumber(Case theCase, bool hasMoreThanOneSegment, int currentIndex)
        {
            if (!hasMoreThanOneSegment)
                return theCase.CaseNumber;

            return string.Format("{0} - {1}", base26Chars[currentIndex], theCase.CaseNumber);
        }

        private DateTime? CopyCaseEventDate(Case theCase, CaseEventType eventType)
        {
            var theEvent = theCase.FindCaseEvent(eventType);
            if (theEvent == null)
                return null;

            return theEvent.EventDate;
        }

        private void CopyEmployeeInformation(CaseRow row, Employee employee)
        {
            if (employee == null)
                return;

            row.EmployeeNumber = employee.EmployeeNumber;
            row.LastName = employee.LastName;
            row.FirstName = employee.FirstName;
        }

        private void CopyReasonInformation(CaseRow row, AbsenceReason reason)
        {
            if (reason == null)
                return;

            row.ReasonForLeave = reason.Code;
        }

        private void CopyRelationshipInformation(CaseRow row, EmployeeContact contact)
        {
            if (contact == null)
                return;

            if (CustomerContactTypes == null)
                CustomerContactTypes = ContactType.DistinctFind(null, CustomerId, EmployerId).ToList();

            row.FamilyRelationship = CustomerContactTypes.Where(ct => ct.Code == contact.ContactTypeCode).FirstOrDefault();
        }

        private void CopyIntermittentInformation(CaseRow row, Case theCase, CaseSegment currentSegment)
        {
            if (currentSegment.Type != CaseType.Intermittent)
                return;

            Certification cert = theCase.Certifications.FirstOrDefault();
            if (cert == null)
                return;

            row.IntermittentFrequency = cert.Frequency;
            row.IntermittentFrequencyUnit = cert.FrequencyType;
            row.IntermittentOccurrences = cert.Occurances;
            row.IntermittentDuration = cert.Duration;
            row.IntermittentDurationUnit = cert.DurationType;
        }

        private CaseRow CopyCaseNoteRecord(Case theCase, CaseNote note)
        {
            CaseRow row = CreateBaseRow(CaseRow.RecordType.Note, theCase);

            row.NoteDate = note.CreatedDate;
            row.CaseNoteCategory = note.Category;
            row.NoteText = note.Notes;
            row.NoteEnteredByEmployeeNumber = note.EnteredByEmployeeNumber;
            row.NoteEnteredByName = note.EnteredByName;

            return row;
        }

        #endregion

        public void ProcessRecord(long rowNumber, string[] recordText, char delimiter, string lineText, IEnumerable<CustomField> customFields = null, List<Employee> lstEmployee = null, List<EmployeeClass> employeeClassTypes = null)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ProcessRecord"))
            {
                // Simple common-sense validation
                if (string.IsNullOrWhiteSpace(lineText))
                {
                    ReportError(rowNumber, lineText, "Record text was not supplied");
                    return;
                }

                // Create an inline action callback here to be used by different scenarios in processing each record.
                var finishedHandler = new Action<CaseRow, CaseRow.ApplicationResult>((row, result) =>
                {
                    // Based on the result, we may or may not report an error
                    switch (result)
                    {
                        case EligibilityRow.ApplicationResult.Success:
                            // Do nothing, we'll just fall through out of the switch statement
                            //  and report this record as processed, since it was successful and all.

                            // Mark our progress by reporting this row processed against our eligibility upload record.
                            ReportProcessed(rowNumber, lineText);
                            break;
                        case EligibilityRow.ApplicationResult.Fail:
                            // Uh oh, need to report this item as an error; however it was still processed
                            // so we'll also, after reporting the error, fall through out of the switch statement
                            // and report this record as processed.
                            ReportError(rowNumber, lineText, row == null ? "Unknown Error" : row.Error);
                            break;
                        case EligibilityRow.ApplicationResult.Retry:
                        default:
                            string error = "Retry condition encountered.";
                            if (row != null)
                                row.AddError(error);
                            ReportError(rowNumber, lineText, row == null ? error : row.Error);
                            break;
                    }
                    
                });

                CaseRow myRow = null;
                CaseRow.ApplicationResult myResult = CaseRow.ApplicationResult.Fail;
                try
                {
                    // Get the row source from the item.
                    myRow = new CaseRow(recordText);
                    if (myRow.IsError)
                    {
                        return;
                    }

                    switch (myRow.RecType)
                    {
                        case RecordType.CaseConversionEligibility:
                            myResult = myRow.ApplyEmployee(CustomerId, EmployerId, User.DefaultUserId, lstEmployee, employeeClassTypes);
                            break;
                        default:
                            myResult = myRow.Apply(CustomerId, EmployerId, User.DefaultUserId, AttachmentFilePath, customFields, lstEmployee);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    if (myRow == null)
                        myRow = new CaseRow();
                    myRow.AddError(ex.ToString());
                    myResult = CaseRow.ApplicationResult.Fail;
                }
                finally
                {
                    finishedHandler(myRow, myResult);
                }
            }
        }//end: ProcessRecord
        #endregion Process


        
        public void ReportStart(string path)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportStart"))
            {
                watch.Start();
                WriteLine("{0:R} - Case Migration started for file '{1}'.", DateTime.Now, path);
            }
        }

        public void ReportEnd()
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportEnd"))
            {
                watch.Stop();
                WriteLine("{0:R} - Case Migration stopped.", DateTime.Now);
                WriteLine("{0:R} - Time Elapsed = '{1:G}'.", DateTime.Now, watch.Elapsed);
            }
        }

        public void ReportError(long rowNumber, string recordText, string error)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportError"))
            {
                
                WriteLine("{0},{1},{2},{3},{4}", rowNumber, "ERROR", DateTime.Now,  string.Format("{0}{1}{2}","\"",recordText,"\""), string.Format("{0}{1}{2}", "\"", error ?? "", "\"") );
            }
        } // ReportError

        public bool ReportFailed(string error)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportFailed"))
            {   
                WriteLine_Error("{0:R} - FAIL: {1}", DateTime.Now, error);
                return false;
            }
        } // ReportFailed

        public void ReportProcessed(long rowNumber, string recordText)
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportProcessed"))
            {
                WriteLine("{0},{1},{2},{3},{4}", rowNumber,"SUCCESS", DateTime.Now,  string.Format("{0}{1}{2}", "\"", recordText, "\""), "");
           }
        } // ReportProcessed

        /// <summary>
        /// Method to print header for Case Migration Service Process
        /// </summary>
        /// <param name="path"></param>
        public void ReportHeader()
        {
            using (InstrumentationContext context = new InstrumentationContext("CaseMigrationService.ReportStart"))
            {
                WriteLine("Row Number,Processed Status,Processed Time,Row Data,Error Message");
            }
        }

        /// <summary>
        /// The synchronization object for locking our file output
        /// </summary>
        private object sync = new object();
        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="formatArgs">The format arguments.</param>
        private void WriteLine(string format, params object[] formatArgs)
        {
            lock (sync)
            {
                (report ?? Console.Out).WriteLine(format, formatArgs);
            }
        }
        /// <summary>
        /// Writes the error line.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="formatArgs">The format arguments.</param>
        private void WriteLine_Error(string format, params object[] formatArgs)
        {
            lock (sync)
            {
                (report ?? Console.Error).WriteLine(format, formatArgs);
            }
        }
    }
}
