﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using ComponentSpace.SAML2.Data;
using MongoDB.Bson;
using System;
using System.Web;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Logic.SSO
{
    /// <summary>
    /// The single sign-on session store provides common functionality for storing single sign-on session state.
    /// </summary>
    [AuditClassNo]
    public class SsoSessionStore : AbstractSSOSessionStore
    {
        /// <summary>
        /// Gets the unique SSO session identifier.
        /// </summary>
        /// <value>
        /// The unique SSO session identifier.
        /// </value>
        public override string SessionID
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ApplicationException("There is no HTTP context.");
                return new AuthenticationService().Using(s => s.GetSessionId(new HttpContextWrapper(HttpContext.Current)));
            }
        }

        /// <summary>
        /// Loads the SSO session object.
        /// </summary>
        /// <param name="type">The SSO session object type.</param>
        /// <returns>
        /// The SSO session object or <c>null</c> if none.
        /// </returns>
        /// <exception cref="System.ApplicationException">The SSO session object couldn't be retrieved from the database SSO session store.</exception>
        public override object Load(Type type)
        {
            object result = null;
            try
            {
                var sessionObj = SsoSession.Repository.Collection.FindOneById(new ObjectId(SessionID));
                if (sessionObj != null)
                    sessionObj.SessionObjects.TryGetValue(type.FullName, out result);
            }
            catch (Exception exception)
            {
                Log.Error("The SSO session object couldn't be retrieved from the database SSO session store.", exception);
                throw;
            }
            return result;
        }

        /// <summary>
        /// Saves the SSO session object.
        /// </summary>
        /// <param name="ssoSession">The serializable SSO session object.</param>
        public override void Save(object ssoSession)
        {
            try
            {
                string name = ssoSession.GetType().FullName;
                var sessionObj = SsoSession.Repository.Collection.FindOneById(new ObjectId(SessionID)) ?? new SsoSession() { Id = SessionID };
                if (sessionObj.SessionObjects.ContainsKey(name))
                    sessionObj.SessionObjects[name] = ssoSession;
                else
                    sessionObj.SessionObjects.Add(name, ssoSession);
                sessionObj.Save();
            }
            catch (Exception exception)
            {
                Log.Error("The SSO session object couldn't be inserted into or updated in the database SSO session store.", exception);
                throw;
            }
        }
    }
}
