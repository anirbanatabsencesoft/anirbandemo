﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using ComponentSpace.SAML2.Metadata;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Logic.SSO
{
    /// <summary>
    /// Performs x509 certificate magic, 'cause that's what cryptography is, pure black magic.
    /// </summary>
    [AuditClassNo]
    public static class x509Magic
    {
        /// <summary>
        /// Generates a PFX certificate for this customer.
        /// </summary>
        /// <param name="customer">The customer to generate a certificate for.</param>
        /// <returns>A self-signed, PFX, x509v3 certificate with associated meta-data for use in an SSO profile.</returns>
        public static PfxCertificate GeneratePfx(this Customer customer)
        {
            return GeneratePfx(customer.GetCertificateSubject());
        }

        /// <summary>
        /// Generates a PFX certificate for this employer.
        /// </summary>
        /// <param name="employer">The employer to generate a certificate for.</param>
        /// <returns>A self-signed, PFX, x509v3 certificate with associated meta-data for use in an SSO profile.</returns>
        public static PfxCertificate GeneratePfx(this Employer employer)
        {
            return GeneratePfx(employer.GetCertificateSubject());
        }

        /// <summary>
        /// Gets the certificate subject from the target customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">customer</exception>
        private static string GetCertificateSubject(this Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            customer.Contact = customer.Contact ?? new Contact();
            customer.Contact.Address = customer.Contact.Address ?? new Address();

            StringBuilder subject = new StringBuilder();
            subject.AppendFormat("C={0}", customer.Contact.Address.Country ?? "US");
            if (!string.IsNullOrWhiteSpace(customer.Contact.Address.State))
                subject.AppendFormat(", ST={0}", customer.Contact.Address.State);
            if (!string.IsNullOrWhiteSpace(customer.Contact.Address.City))
                subject.AppendFormat(", L={0}", customer.Contact.Address.City);
            subject.AppendFormat(", O={0}, OU=Customer", customer.Name);
            subject.AppendFormat(", CN={0}", customer.Id);
            return subject.ToString();
        }

        /// <summary>
        /// Gets the certificate subject from the target employer.
        /// </summary>
        /// <param name="employer">The employer.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">employer</exception>
        private static string GetCertificateSubject(this Employer employer)
        {
            if (employer == null)
                throw new ArgumentNullException("employer");

            employer.Contact = employer.Contact ?? new Contact();
            employer.Contact.Address = employer.Contact.Address ?? new Address();

            StringBuilder subject = new StringBuilder();
            subject.AppendFormat("C={0}", employer.Contact.Address.Country ?? "US");
            if (!string.IsNullOrWhiteSpace(employer.Contact.Address.State))
                subject.AppendFormat(", ST={0}", employer.Contact.Address.State);
            if (!string.IsNullOrWhiteSpace(employer.Contact.Address.City))
                subject.AppendFormat(", L={0}", employer.Contact.Address.City);
            subject.AppendFormat(", O={0}, OU=Employer", employer.Name);
            subject.AppendFormat(", CN={0}", employer.Id);
            return subject.ToString();
        }

        /// <summary>
        /// Generates a PFX for the given subject name.
        /// </summary>
        /// <param name="subjectName">Name of the subject.</param>
        /// <returns></returns>
        private static PfxCertificate GeneratePfx(string subjectName)
        {
            // New up the certificate that we're going to return
            PfxCertificate pfx = new PfxCertificate()
            {
                Subject = subjectName,
                Alias = "AbsenceTracker-SAML2",
                Password = CryptoString.GetEncrypted(RandomString.GenerateKey(16)),
                IssuedDate = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddYears(100)
            };

            // Get our RSA Key Pair Generator
            var keyGen = new RsaKeyPairGenerator();

            // Certificate Strength, 2048 bits
            keyGen.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 2048));

            var kp = keyGen.GenerateKeyPair();

            var gen = new X509V3CertificateGenerator();

            // Set certificate generation properties 'n' stuff
            var certName = new X509Name(subjectName);
            var serialNo = BigInteger.ProbablePrime(120, new Random());
            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(certName);
            gen.SetIssuerDN(new X509Name("C=US, ST=CO, L=Lakewood, O=AbsenceSoft LLC, OU=AbsenceTracker, CN=absencetracker.com/emailAddress=contact@absencesoft.com"));
            gen.SetNotAfter(pfx.Expires);
            gen.SetNotBefore(pfx.IssuedDate);
            gen.SetSignatureAlgorithm("SHA256WithRSA");
            gen.SetPublicKey(kp.Public);

            // Add the certificate extensions and extended key usage (any)
            gen.AddExtension(X509Extensions.AuthorityKeyIdentifier.Id, false, new AuthorityKeyIdentifier(
                SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp.Public),
                new GeneralNames(new GeneralName(certName)), serialNo));
            gen.AddExtension(X509Extensions.ExtendedKeyUsage.Id, false, new ExtendedKeyUsage(KeyPurposeID.AnyExtendedKeyUsage));

            // Generate the certificate
            var newCert = gen.Generate(kp.Private);

            // Get our temporary key store
            var store = new Pkcs12Store();
            var entry = new X509CertificateEntry(newCert);

            // Set the certificate entry in our key store
            store.SetKeyEntry(pfx.Alias, new AsymmetricKeyEntry(kp.Private), new[] { entry });

            X509Certificate2 x2 = null;
            // Save our certificate to a memory stream
            using (MemoryStream certStream = new MemoryStream())
            {
                store.Save(certStream, pfx.Password.PlainText.ToCharArray(), new SecureRandom(new CryptoApiRandomGenerator()));
                certStream.Position = 0;
                // Write out the base 64 encoded certificate body secured using our password to our PFX
                pfx.Pfx = certStream.ToArray();
                x2 = new X509Certificate2(pfx.Pfx, pfx.Password.PlainText, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
            }

            pfx.Subject = x2.Subject;
            pfx.Thumbprint = x2.Thumbprint;
            pfx.PublicKey = x2.GetPublicKeyString();
            pfx.SerialNumber = x2.GetSerialNumberString();

            x2.Reset();

            return pfx;
        }

        /// <summary>
        /// Converts a PFX definitions to a x.5092 certificate (v3).
        /// </summary>
        /// <param name="pfx">The PFX that this method extends and converts.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">pfx</exception>
        /// <exception cref="System.ArgumentException">PFX is missing the certificate encoded content;pfx</exception>
        /// <exception cref="System.ApplicationException">
        /// PFX Certificate Subject does not match stored value
        /// or
        /// PFX Certificate Thumbprint does not match stored value
        /// </exception>
        public static X509Certificate2 ToCertificate(this PfxCertificate pfx)
        {
            if (pfx == null)
                throw new ArgumentNullException("pfx");
            if (pfx.Pfx == null)
                throw new ArgumentException("PFX is missing the certificate content", "pfx");

            string password = pfx.Password == null ? null : pfx.Password.PlainText;
            if (string.IsNullOrWhiteSpace(password) && !string.IsNullOrWhiteSpace(pfx.Password.Encrypted))
                password = pfx.Password.Encrypted.Decrypt();

            var x5092 = string.IsNullOrWhiteSpace(password)
                ? new System.Security.Cryptography.X509Certificates.X509Certificate2(pfx.Pfx, (string)null, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet)
                : new System.Security.Cryptography.X509Certificates.X509Certificate2(pfx.Pfx, password, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet);

            if (pfx.Subject != x5092.Subject)
                throw new ApplicationException("PFX Certificate Subject does not match stored value");
            
            if (pfx.Thumbprint != x5092.Thumbprint)
                throw new ApplicationException("PFX Certificate Thumbprint does not match stored value");

            return x5092;
        }

        /// <summary>
        /// Creates the key info.
        /// </summary>
        /// <param name="x509Certificate">The X509 certificate.</param>
        /// <returns></returns>
        internal static KeyInfo CreateKeyInfo(X509Certificate2 x509Certificate)
        {
            KeyInfoX509Data keyInfoX509Data = new KeyInfoX509Data();
            keyInfoX509Data.AddCertificate(x509Certificate);

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(keyInfoX509Data);

            return keyInfo;
        }

        /// <summary>
        /// Creates the key descriptor.
        /// </summary>
        /// <param name="x509Certificate">The X509 certificate.</param>
        /// <returns></returns>
        internal static KeyDescriptor CreateKeyDescriptor(X509Certificate2 x509Certificate)
        {
            KeyDescriptor keyDescriptor = new KeyDescriptor();
            KeyInfo keyInfo = CreateKeyInfo(x509Certificate);
            keyDescriptor.KeyInfo = keyInfo.GetXml();
            keyDescriptor.AddEncryptionMethod("http://www.w3.org/2001/04/xmlenc#aes256-cbc");
            return keyDescriptor;
        }
    }
}
