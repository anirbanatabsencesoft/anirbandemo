﻿using AbsenceSoft.Data.Security;
using ComponentSpace.SAML2.Data;
using System;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Logic.SSO
{
    [AuditClassNo]
    public class SsoIdCache : IIDCache
    {
        /// <summary>
        /// Adds the ID with an associated expiration time to the cache.
        /// </summary>
        /// <param name="id">The ID.</param>
        /// <param name="expirationDateTime">The expiration time or <c>null</c> if none.</param>
        /// <returns>
        ///   <c>true</c> if the ID doesn't already exist in the cache; otherwise <c>false</c>.
        /// </returns>
        public bool Add(string id, DateTime expirationDateTime)
        {
            return new SsoIdCacheEntry() { CacheId = id, Expires = expirationDateTime.ToUniversalTime() }.Save();
        }
    }
}
