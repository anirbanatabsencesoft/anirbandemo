﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using ComponentSpace.SAML2;
using ComponentSpace.SAML2.Configuration;
using ComponentSpace.SAML2.Metadata;
using ComponentSpace.SAML2.Utility;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Deployment.Internal.CodeSigning;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ComponentSpace.SAML2.Assertions;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Logic.SSO
{
    [AuditClassNo]
    public static class SsoConfiguration
    {
        public const string RoleAttributeName = "roleName";
        public const string EmployerReferenceCodeName = "employerReferenceCode";

        /// <summary>
        /// Configures the SSO and SAML configuration for the application.
        /// </summary>
        public static void ConfigureSso()
        {
            // Register the SHA-256 cryptographic algorithm.
            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

            SAMLConfiguration.IDCache = new SsoIdCache();
            SAMLConfiguration.SSOSessionStore = new SsoSessionStore();
            SsoProfile.Repository.Collection.FindAll().SetBatchSize(100).ForEach(p => UpdateSso(p));
        }

        /// <summary>
        /// Updates the SSO/SAML configuration for the given tenant/entity id. If not already set, will create it
        /// or if already set, will update it accordingly.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        public static void UpdateSso(string entityId)
        {
            var profiles = SsoProfile.Repository.Collection.Find(Query.Or(Query.EQ("EntityId", entityId), Query.EQ("IdpEntityId", entityId))).ToList();
            if (profiles == null || !profiles.Any())
                SAMLConfiguration.Configurations.Remove(entityId);
            else
                profiles.ForEach(p => UpdateSso(p));
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <returns></returns>
        public static SAMLConfiguration GetConfiguration(SsoProfile profile)
        {
            if (profile == null) return null;

            SAMLConfiguration config = new SAMLConfiguration();
            config.CertificateManager = new SsoCertificateManager();
            config.LocalServiceProviderConfiguration = new LocalServiceProviderConfiguration()
            {
                Name = profile.EntityId,
                AssertionConsumerServiceUrl = "~/SSO/saml2/AssertionConsumerService"
            };
            var idp = config.PartnerIdentityProviderConfigurations.Any() ? config.GetPartnerIdentityProvider(profile.IdpEntityId ?? profile.EntityId) : null;
            if (idp == null)
            {
                idp = new PartnerIdentityProviderConfiguration()
                {
                    Name = profile.IdpEntityId ?? profile.EntityId
                };
                config.AddPartnerIdentityProvider(idp);
            }
            idp.ClockSkew = profile.ClockSkew;
            idp.DataEncryptionMethod = profile.DataEncryptionMethod;
            idp.DigestMethod = profile.DigestMethod;
            idp.DisableAudienceRestrictionCheck = profile.DisableAudienceRestrictionCheck;
            idp.DisableInboundLogout = profile.DisableInboundLogout;
            idp.DisableInResponseToCheck = profile.DisableInResponseToCheck;
            idp.DisableOutboundLogout = profile.DisableOutboundLogout;
            idp.ForceAuthn = profile.ForceAuthn;
            idp.KeyEncryptionMethod = profile.KeyEncryptionMethod;
            idp.LogoutRequestLifeTime = profile.LogoutRequestLifeTime;
            idp.NameIDFormat = profile.NameIDFormat;
            idp.OverridePendingAuthnRequest = profile.OverridePendingAuthnRequest;
            idp.RequestedAuthnContext = profile.RequestedAuthnContext;
            idp.SignatureMethod = profile.SignatureMethod;
            idp.SignAuthnRequest = profile.SignAuthnRequest;
            idp.SignLogoutRequest = profile.SignLogoutRequest;
            idp.SignLogoutResponse = profile.SignLogoutResponse;
            idp.SingleLogoutServiceBinding = profile.SingleLogoutServiceBinding;
            idp.SingleLogoutServiceResponseUrl = profile.SingleLogoutServiceResponseUrl;
            idp.SingleLogoutServiceUrl = profile.SingleLogoutServiceUrl;
            idp.SingleSignOnServiceBinding = profile.SingleSignOnServiceBinding;
            idp.SingleSignOnServiceUrl = profile.SingleSignOnServiceUrl;
            idp.UseEmbeddedCertificate = profile.UseEmbeddedCertificate;
            idp.WantAssertionEncrypted = profile.WantAssertionEncrypted;
            idp.WantAssertionSigned = profile.WantAssertionSigned;
            idp.WantLogoutRequestSigned = profile.WantLogoutRequestSigned;
            idp.WantLogoutResponseSigned = profile.WantLogoutResponseSigned;
            idp.WantSAMLResponseSigned = profile.WantSAMLResponseSigned;

            return config;
        }

        /// <summary>
        /// Updates the SSO/SAML configuration for the given profile. If not already set, will create it
        /// or if already set, will update it accordingly.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <returns></returns>
        public static SAMLConfiguration UpdateSso(SsoProfile profile)
        {
            if (profile == null) return null;

            var config = SAMLConfiguration.Configurations.ContainsKey(profile.EntityId) ? SAMLConfiguration.Configurations[profile.EntityId] : null;
            if (config == null)
            {
                config = new SAMLConfiguration();
                SAMLConfiguration.Configurations.Add(profile.EntityId, config);
            }

            config.CertificateManager = new SsoCertificateManager();
            config.LocalServiceProviderConfiguration = new LocalServiceProviderConfiguration()
            {
                Name = profile.EntityId,
                AssertionConsumerServiceUrl = "~/SSO/saml2/AssertionConsumerService"
            };
            var idp = config.PartnerIdentityProviderConfigurations.Any() ? config.GetPartnerIdentityProvider(profile.IdpEntityId ?? profile.EntityId) : null;
            if (idp == null)
            {
                idp = new PartnerIdentityProviderConfiguration()
                {
                    Name = profile.IdpEntityId ?? profile.EntityId
                };
                config.AddPartnerIdentityProvider(idp);
            }
            idp.ClockSkew = profile.ClockSkew;
            idp.DataEncryptionMethod = profile.DataEncryptionMethod;
            idp.DigestMethod = profile.DigestMethod;
            idp.DisableAudienceRestrictionCheck = profile.DisableAudienceRestrictionCheck;
            idp.DisableInboundLogout = profile.DisableInboundLogout;
            idp.DisableInResponseToCheck = profile.DisableInResponseToCheck;
            idp.DisableOutboundLogout = profile.DisableOutboundLogout;
            idp.ForceAuthn = profile.ForceAuthn;
            idp.KeyEncryptionMethod = profile.KeyEncryptionMethod;
            idp.LogoutRequestLifeTime = profile.LogoutRequestLifeTime;
            idp.NameIDFormat = profile.NameIDFormat;
            idp.OverridePendingAuthnRequest = profile.OverridePendingAuthnRequest;
            idp.RequestedAuthnContext = profile.RequestedAuthnContext;
            idp.SignatureMethod = profile.SignatureMethod;
            idp.SignAuthnRequest = profile.SignAuthnRequest;
            idp.SignLogoutRequest = profile.SignLogoutRequest;
            idp.SignLogoutResponse = profile.SignLogoutResponse;
            idp.SingleLogoutServiceBinding = profile.SingleLogoutServiceBinding;
            idp.SingleLogoutServiceResponseUrl = profile.SingleLogoutServiceResponseUrl;
            idp.SingleLogoutServiceUrl = profile.SingleLogoutServiceUrl;
            idp.SingleSignOnServiceBinding = profile.SingleSignOnServiceBinding;
            idp.SingleSignOnServiceUrl = profile.SingleSignOnServiceUrl;
            idp.UseEmbeddedCertificate = profile.UseEmbeddedCertificate;
            idp.WantAssertionEncrypted = profile.WantAssertionEncrypted;
            idp.WantAssertionSigned = profile.WantAssertionSigned;
            idp.WantLogoutRequestSigned = profile.WantLogoutRequestSigned;
            idp.WantLogoutResponseSigned = profile.WantLogoutResponseSigned;
            idp.WantSAMLResponseSigned = profile.WantSAMLResponseSigned;

            // Ensure only ever 1 subscription here for this
            profile.Saved -= profile_Saved;
            profile.Saved += profile_Saved;

            return config;
        }

        /// <summary>
        /// Handles the proflie on Saved event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The entity event args for the SSO Profile.</param>
        static void profile_Saved(object sender, EntityEventArgs<SsoProfile> e)
        {
            if (e.Entity != null)
                UpdateSso(e.Entity);
        }

        /// <summary>
        /// Sets the saml configuration for the specified tenant/entity id.
        /// </summary>
        /// <param name="entityId">The entity id that identifies the tenant.</param>
        public static void SetSamlConfiguration(string entityId)
        {
            SAMLConfiguration.ConfigurationID = entityId;
        }

        /// <summary>
        /// Imports the SAML 2.0 metadata file, extracts the appropriate configuration
        /// items and sets them to the provided SSO Profile.
        /// </summary>
        /// <param name="metadata">The metadata XML to import.</param>
        /// <param name="profile">The profile to set the metadata settings into.</param>
        /// <returns>The passed in profile with the appropriate fields populated.</returns>
        /// <exception cref="System.ArgumentNullException">metadata
        /// or
        /// profile</exception>
        /// <exception cref="System.ArgumentException">There was no partner identity provider configuration provided in the supplied metadata;metadata</exception>
        public static SsoProfile ImportMetadata(string metadata, SsoProfile profile)
        {
            if (string.IsNullOrWhiteSpace(metadata))
                throw new ArgumentNullException("metadata");
            if (profile == null)
                throw new ArgumentNullException("profile");

            EntitiesDescriptor entitiesDescriptor = ParseMetadata(metadata);
            SAMLConfiguration samlConfiguration = new SAMLConfiguration();
            IList<X509Certificate2> x509Certificates = new List<X509Certificate2>();

            MetadataImporter.Import(entitiesDescriptor, samlConfiguration, x509Certificates);
            if (!samlConfiguration.PartnerIdentityProviderConfigurations.Any())
                throw new ArgumentException("There was no partner identity provider configuration provided in the supplied metadata", "metadata");

            profile.IdpMetadata = metadata;

            var idp = samlConfiguration.PartnerIdentityProviderConfigurations.First().Value;
            profile.IdpEntityId = idp.Name;
            profile.ClockSkew = idp.ClockSkew;
            profile.DataEncryptionMethod = idp.DataEncryptionMethod;
            profile.DigestMethod = idp.DigestMethod;
            profile.DisableAudienceRestrictionCheck = idp.DisableAudienceRestrictionCheck;
            profile.DisableInboundLogout = idp.DisableInboundLogout;
            profile.DisableInResponseToCheck = idp.DisableInResponseToCheck;
            profile.DisableOutboundLogout = idp.DisableOutboundLogout;
            profile.ForceAuthn = idp.ForceAuthn;
            profile.KeyEncryptionMethod = idp.KeyEncryptionMethod;
            profile.LogoutRequestLifeTime = idp.LogoutRequestLifeTime;
            profile.NameIDFormat = idp.NameIDFormat;
            profile.OverridePendingAuthnRequest = idp.OverridePendingAuthnRequest;
            profile.RequestedAuthnContext = idp.RequestedAuthnContext;
            profile.SignatureMethod = idp.SignatureMethod;
            profile.SignAuthnRequest = idp.SignAuthnRequest;
            profile.SignLogoutRequest = idp.SignLogoutRequest;
            profile.SignLogoutResponse = idp.SignLogoutResponse;
            profile.SingleLogoutServiceBinding = idp.SingleLogoutServiceBinding;
            profile.SingleLogoutServiceResponseUrl = idp.SingleLogoutServiceResponseUrl;
            profile.SingleLogoutServiceUrl = idp.SingleLogoutServiceUrl;
            profile.SingleSignOnServiceBinding = idp.SingleSignOnServiceBinding;
            profile.SingleSignOnServiceUrl = idp.SingleSignOnServiceUrl;
            profile.UseEmbeddedCertificate = idp.UseEmbeddedCertificate;
            profile.WantAssertionEncrypted = idp.WantAssertionEncrypted;
            profile.WantAssertionSigned = idp.WantAssertionSigned;
            profile.WantLogoutRequestSigned = idp.WantLogoutRequestSigned;
            profile.WantLogoutResponseSigned = idp.WantLogoutResponseSigned;
            profile.WantSAMLResponseSigned = idp.WantSAMLResponseSigned;

            // Get the signing key if any is provided, otherwise null it out.
            if (x509Certificates.Any())
                profile.PartnerCertificate = x509Certificates.First().GetRawCertData();
            else
                profile.PartnerCertificate = null;

            return profile;
        }

        /// <summary>
        /// Gets and exports the service provider metadata for the given SSO Profile.
        /// </summary>
        /// <param name="profile">The SSO profile to export our metadata for.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">profile</exception>
        public static string ExportMetadata(SsoProfile profile, bool ess)
        {
            if (profile == null) throw new ArgumentNullException("profile");
            var entityDescriptor = CreateSPEntityDescriptor(profile, ess);
            var metadataXml = entityDescriptor.ToXml().OwnerDocument;
            var x509Certificate = profile.LocalCertificate.ToCertificate();
            SAMLMetadataSignature.Generate(metadataXml.DocumentElement, x509Certificate.PrivateKey, x509Certificate);
            return metadataXml.OuterXml;
        }

        /// <summary>
        /// Creates the SPSSO descriptor.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <returns></returns>
        private static SPSSODescriptor CreateSPSSODescriptor(SsoProfile profile, bool ess)
        {
            string servicesBaseUrl = profile.Customer.GetBaseUrl(ess);
            if (profile.Employer != null)
                servicesBaseUrl = profile.Employer.GetBaseUrl();
            servicesBaseUrl = string.Concat(servicesBaseUrl, servicesBaseUrl.EndsWith("/") ? "" : "/", "{0}");

            SPSSODescriptor spSSODescriptor = new SPSSODescriptor();
            spSSODescriptor.ProtocolSupportEnumeration = SAML.NamespaceURIs.Protocol;
            spSSODescriptor.WantAssertionsSigned = true;
            spSSODescriptor.AuthnRequestsSigned = true;

            X509Certificate2 x509Certificate = profile.LocalCertificate.ToCertificate();
            spSSODescriptor.KeyDescriptors.Add(x509Magic.CreateKeyDescriptor(x509Certificate));

            spSSODescriptor.AssertionConsumerServices.Add(new IndexedEndpointType(1, true)
            {
                Binding = SAMLIdentifiers.BindingURIs.HTTPPost,
                Location = string.Format(servicesBaseUrl, "SSO/saml2/AssertionConsumerService")
            });
            spSSODescriptor.AssertionConsumerServices.Add(new IndexedEndpointType(2, false)
            {
                Binding = SAMLIdentifiers.BindingURIs.HTTPArtifact,
                Location = string.Format(servicesBaseUrl, "SSO/saml2/AssertionConsumerService.artifact")
            });
            spSSODescriptor.AssertionConsumerServices.Add(new IndexedEndpointType(3, false)
            {
                Binding = SAMLIdentifiers.BindingURIs.HTTPRedirect,
                Location = string.Format(servicesBaseUrl, "SSO/saml2/AssertionConsumerService.redirect")
            });

            spSSODescriptor.NameIDFormats.Add(SAMLIdentifiers.NameIdentifierFormats.Persistent);

            AttributeConsumingService attcs = new AttributeConsumingService() { Index = 1, IsDefault = true };
            attcs.ServiceNames.Add(new LocalizedNameType("AttributeContract", "en"));
            var att = new RequestedAttribute()
            {
                Name = RoleAttributeName,
                IsRequired = false,
                NameFormat = SAMLIdentifiers.AttributeNameFormats.Basic,
                FriendlyName = RoleAttributeName
            };
            var roles = Role.AsQueryable().Where(r => r.CustomerId == profile.CustomerId).ToList();
            if (profile.EmployerId != null || ess)
                roles.Where(r => r.Type == RoleType.SelfService).ForEach(r => att.Values.Add(new AttributeValue(r.Name)));
            else
                roles.Where(r => r.Type == RoleType.Portal).ForEach(r => att.Values.Add(new AttributeValue(r.Name)));
            attcs.RequestedAttributes.Add(att);
            if (ess) // Only include for ESS, not for the portal.
                attcs.RequestedAttributes.Add(new RequestedAttribute()
                {
                    Name = EmployerReferenceCodeName,
                    IsRequired = profile.Employer == null, // Required if this isn't for the "employer" ESS site, but instead the Customer level one.
                    NameFormat = SAMLIdentifiers.AttributeNameFormats.Basic,
                    FriendlyName = EmployerReferenceCodeName
                });
            spSSODescriptor.AttributeConsumingServices.Add(attcs);

            return spSSODescriptor;
        }

        /// <summary>
        /// Creates the SP entity descriptor.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <returns></returns>
        private static EntityDescriptor CreateSPEntityDescriptor(SsoProfile profile, bool ess)
        {
            EntityDescriptor entityDescriptor = new EntityDescriptor();
            entityDescriptor.EntityID = new EntityIDType(profile.EntityId);
            entityDescriptor.SPSSODescriptors.Add(CreateSPSSODescriptor(profile, ess));

            Organization organization = new Organization();
            organization.OrganizationNames.Add(new OrganizationName("AbsenceSoft", "en"));
            organization.OrganizationDisplayNames.Add(new OrganizationDisplayName("AbsenceSoft", "en"));
            organization.OrganizationURLs.Add(new OrganizationURL("www.absencesoft.com", "en"));
            entityDescriptor.Organization = organization;

            // TODO: Somehow make this configurable

            ContactPerson admin = new ContactPerson();
            admin.ContactTypeValue = "administrative";
            admin.GivenName = "Mohan";
            admin.Surname = "Ruthirakotti";
            admin.EmailAddresses.Add("rmohan@absencesoft.com");
            admin.TelephoneNumbers.Add("+18662115152");
            entityDescriptor.ContactPeople.Add(admin);

            ContactPerson contactPerson = new ContactPerson();
            contactPerson.ContactTypeValue = "technical";
            contactPerson.GivenName = "Chad";
            contactPerson.Surname = "Scharf";
            contactPerson.EmailAddresses.Add("chad@absencesoft.com");
            contactPerson.TelephoneNumbers.Add("+19044735431");
            entityDescriptor.ContactPeople.Add(contactPerson);

            return entityDescriptor;
        }

        /// <summary>
        /// Parses the metadata.
        /// </summary>
        /// <param name="metadata">The metadata.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Expecting entities descriptor or entity descriptor.</exception>
        private static EntitiesDescriptor ParseMetadata(string metadata)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.PreserveWhitespace = true;
            xmlDocument.LoadXml(metadata);

            EntitiesDescriptor entitiesDescriptor = null;

            if (EntitiesDescriptor.IsValid(xmlDocument.DocumentElement))
                entitiesDescriptor = new EntitiesDescriptor(xmlDocument.DocumentElement);
            else if (EntityDescriptor.IsValid(xmlDocument.DocumentElement))
            {
                entitiesDescriptor = new EntitiesDescriptor();
                entitiesDescriptor.EntityDescriptors.Add(new EntityDescriptor(xmlDocument.DocumentElement));
            }
            else
            {
                throw new ArgumentException("Expecting entities descriptor or entity descriptor.");
            }

            return entitiesDescriptor;
        }
    }
}
