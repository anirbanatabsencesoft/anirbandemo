﻿using AbsenceSoft.Common.Security;
using AbsenceSoft.Data.Security;
using ComponentSpace.SAML2.Certificates;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Audit;

namespace AbsenceSoft.Logic.SSO
{
    /// <summary>
    /// Our own special SSO certificate manager that reads all of the 
    /// certificates and profiles out of the database and caching them.
    /// </summary>
    [AuditClassNo]
    public class SsoCertificateManager : AbstractCertificateManager
    {
        private static ConcurrentDictionary<string, SsoProfile> _profiles = new ConcurrentDictionary<string, SsoProfile>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SsoCertificateManager" /> class.
        /// </summary>
        public SsoCertificateManager()
        {
            // Save the partner providers' certificates.
            foreach (var profile in SsoProfile.Repository.Collection.FindAll().SetBatchSize(100))
            {
                _profiles.TryAdd(profile.EntityId, profile);
                AddLocalServiceProviderCertificate(profile.EntityId, profile.LocalCertificate.ToCertificate());
                if (profile.PartnerCertificate != null)
                    AddPartnerIdentityProviderCertificate(profile.EntityId, new X509Certificate2(profile.PartnerCertificate, (string)null, X509KeyStorageFlags.MachineKeySet));
            }
        }

        /// <summary>
        /// Gets the local service provider X.509 certificate.
        /// </summary>
        /// <param name="partnerIdentityProviderName">The partner identity provider name or <c>null</c> if none.</param>
        /// <returns>
        /// The local service provider X.509 certificate.
        /// </returns>
        public override X509Certificate2 GetLocalServiceProviderCertificate(string partnerIdentityProviderName)
        {
            var cert = base.GetLocalServiceProviderCertificate(partnerIdentityProviderName);
            if (cert != null)
                return cert;
            var profile = _profiles.GetOrAdd(partnerIdentityProviderName, p => SsoProfile.AsQueryable().Where(s => s.EntityId == partnerIdentityProviderName).Skip(0).Take(1).FirstOrDefault());
            if (profile != null && profile.LocalCertificate != null)
            {
                cert = profile.LocalCertificate.ToCertificate();
                AddLocalServiceProviderCertificate(profile.EntityId, cert);
                return cert;
            }
            return null;
        }

        /// <summary>
        /// Gets the partner identity provider X.509 certificate.
        /// </summary>
        /// <param name="partnerIdentityProviderName">The partner identity provider name.</param>
        /// <returns>
        /// The partner identity provider X.509 certificate or <c>null</c> if none.
        /// </returns>
        public override X509Certificate2 GetPartnerIdentityProviderCertificate(string partnerIdentityProviderName)
        {
            var cert = base.GetPartnerIdentityProviderCertificate(partnerIdentityProviderName);
            if (cert != null)
                return cert;
            var profile = _profiles.GetOrAdd(partnerIdentityProviderName, p => SsoProfile.AsQueryable().Where(s => s.EntityId == partnerIdentityProviderName).Skip(0).Take(1).FirstOrDefault());
            if (profile != null && profile.PartnerCertificate != null)
            {
                cert = new X509Certificate2(profile.PartnerCertificate, (string)null, X509KeyStorageFlags.MachineKeySet);
                AddLocalServiceProviderCertificate(profile.EntityId, cert);
                return cert;
            }
            return null;
        }
    }
}