﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Logic.Expressions
{
    public class HelperMethods
    {

        /// <summary>
        /// Returns a boolean specifying if the character at the current pointer is a valid number
        /// i.e. it starts with a number or a minus sign immediately followed by a number
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ptr"></param>
        /// <returns></returns>
        public static bool IsANumber(string str, int ptr)
        {
            return
                (!IsNumeric(str, ptr - 1) & (str[ptr] == '-') & IsNumeric(str, ptr + 1)) ||
                IsNumeric(str, ptr);
        }

        /// <summary>
        /// Returns a boolean specifying if the character at the current point is of the range '0'..'9'
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="ptr">The PTR.</param>
        /// <returns></returns>
        public static bool IsNumeric(string str, int ptr)
        {
            if ((ptr >= 0) & (ptr < str.Length))
                return (str[ptr] >= '0' & str[ptr] <= '9');
            return true;
        }

        /// <summary>
        /// Determines whether [is hexadecimal start] [the specified string].
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="ptr">The PTR.</param>
        /// <returns></returns>
        public static bool IsHexStart(string str, int ptr)
        {
            if ((ptr >= 0) & (ptr + 2 < str.Length))
                return (str[ptr] == '-' & str[ptr + 1] == '0' & str[ptr + 2] == 'x');
            if ((ptr >= 0) & (ptr + 1 < str.Length))
                return (str[ptr] == '0' & str[ptr + 1] == 'x');
            return false;
        }

        /// <summary>
        /// Determines whether the specified string is hexadecimal.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="ptr">The PTR.</param>
        /// <returns></returns>
        public static bool IsHex(string str, int ptr)
        {
            if ((ptr >= 0) & (ptr < str.Length))
                return (str[ptr] >= '0' & str[ptr] <= '9') | (str[ptr] >= 'A' & str[ptr] <= 'F') | (str[ptr] >= 'a' & str[ptr] <= 'f');
            return true;
        }

        /// <summary>
        /// Determines whether the specified character is alpha.
        /// </summary>
        /// <param name="chr">The character.</param>
        /// <returns></returns>
        public static bool IsAlpha(char chr)
        {
            return (chr >= 'A' & chr <= 'Z') || (chr >= 'a' & chr <= 'z');
        }

        /// <summary>
        /// Tries to get the type from the type name, assigns the value, if any, to <paramref name="t"/>
        /// and then returns <c>true</c> if successful and the type was found, otherwise <c>false</c>.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="t">The type to assign the found type from the name.</param>
        /// <returns><c>true</c> if successful and the type was found, otherwise <c>false</c>.</returns>
        public static bool TryGetType(string typeName, out Type t)
        {
            try { t = Type.GetType(typeName); return t != null; }
            catch { t = null; return false; }
        }
    }
}
