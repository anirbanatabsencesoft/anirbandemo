﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Expressions
{
    public class BaseExpressionService : LogicService
    {
        public BaseExpressionService()
            : base()
        {

        }

        public BaseExpressionService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        public BaseExpressionService(string customerId, string employerId, User u)
            : base(customerId, employerId, u)
        {

        }



        internal Dictionary<string, string> getOperators(bool eq = true, bool neq = true, bool lt = false, bool lte = false, bool gt = false, bool gte = false, bool contains = false)
        {
            Dictionary<string, string> ops = new Dictionary<string, string>();
            if (eq) ops.Add("==", "Equals");
            if (neq) ops.Add("!=", "Does Not Equal");
            if (lt) ops.Add("<", "Less Than");
            if (lte) ops.Add("<=", "Less Than Or Equal To");
            if (gt) ops.Add(">", "Greater Than");
            if (gte) ops.Add(">=", "Greater Than Or Equal To");
            if (contains) ops.Add("::", "Contains");
            return ops;
        }//end: getOperators

        private List<RuleExpressionOption> getEnumOptions<TEnum>()
        {
            return Enum
                .GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .Select(m => new RuleExpressionOption()
                {
                    Value = m,
                    Text = m.ToString().SplitCamelCaseString()
                })
                .ToList();
        }//end: getEnumOptions

        /// <summary>
        /// Gets the Employee Class options.
        /// </summary>
        /// <returns></returns>
        private List<RuleExpressionOption> getEmployeeClassOptions()
        {
            using (var employeeClassService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var employeeClasses = employeeClassService.GetAllEmployeeClasses();
                return employeeClasses
                .Select(m => new RuleExpressionOption()
                {
                    Value = m.Code,
                    Text = m.Name
                })
                .ToList();
            }

        }

        private class ExpressionGroupCache
        {
            public List<RuleExpressionGroup> Expressions { get; set; }
            public DateTime Expires { get; set; }
        }

        private static Dictionary<string, ExpressionGroupCache> _expressionGroups = new Dictionary<string, ExpressionGroupCache>();
        private static object _sync = new object();
        /// <summary>
        /// Gets a collection of rule expressions, placed into logical groups for display to a user
        /// so they can select one to add to their own rule groups, expression trees, etc.
        /// </summary>
        /// <returns>A collection rule expressions.</returns>
        public virtual IEnumerable<RuleExpressionGroup> GetRuleExpressions()
        {
            if (!_expressionGroups.ContainsKey(EmployerId ?? "_"))
            {
                lock (_sync)
                {
                    if (_expressionGroups.ContainsKey(EmployerId ?? "_") && _expressionGroups[EmployerId ?? "_"].Expires <= DateTime.UtcNow)
                        _expressionGroups.Remove(EmployerId ?? "_");
                    if (!_expressionGroups.ContainsKey(EmployerId ?? "_"))
                    {
                        string getSystemTypeName(ControlType controlType)
                        {
                            switch (controlType)
                            {
                                case ControlType.Date:
                                    return typeof(DateTime).AssemblyQualifiedName;
                                case ControlType.Minutes:
                                    return typeof(int).AssemblyQualifiedName;
                                case ControlType.Numeric:
                                    return typeof(double).AssemblyQualifiedName;
                                case ControlType.CheckBox:
                                    return typeof(bool).AssemblyQualifiedName;
                                default:
                                    return typeof(string).AssemblyQualifiedName;
                            }
                        }

                        var myGroups = new List<RuleExpressionGroup>();
                        _expressionGroups.Add(EmployerId ?? "_", new ExpressionGroupCache()
                        {
                            Expressions = myGroups,
                            Expires = DateTime.UtcNow.AddHours(1)
                        });

                        #region employeeGroup
                        var employeeGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Employee Information",
                            Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's demographic, eligibility, relationship and other attributed data",
                            Expressions = new List<RuleExpression>()
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.EmployeeNumber",
                            Prompt = "Employee Number",
                            HelpText = "Matches an explicit employee number either to be inclusive or to exclude that employee number from consideration (i.e. everyone but the CEO gets this policy).",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Employee Number",
                            RuleDescription = "Employee number is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Info.OfficeLocation",
                            Prompt = "Office Location Name",
                            HelpText = "Matches an explicit office location name for the employee to be inclusive or exclude that office location from consideration (i.e. West Region Office).",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Office Location Name",
                            RuleDescription = "Office location name is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.WorkState",
                            Prompt = "Work State",
                            HelpText = "Matches on the employee's address state (office address, not state). Use the 2-character ISO code for US states and territories",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "State of Office",
                            RuleDescription = "Employee's state of office is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.WorkCounty",
                            Prompt = "Work County",
                            HelpText = "Matches on the employee's address work county",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "County of Office",
                            RuleDescription = "Employee's county of office is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.WorkCity",
                            Prompt = "Work City",
                            HelpText = "Matches on the employee's address work city",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "City of Office",
                            RuleDescription = "Employee's city of office is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.ResidenceCounty",
                            Prompt = "Residence County",
                            HelpText = "Matches on the employee's address residence county",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "County of Residence",
                            RuleDescription = "Employee's county of residence is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Info.Address.City",
                            Prompt = "City of Residence",
                            HelpText = "Matches on the employee's address city (residence address, not office)",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "City of Residence",
                            RuleDescription = "Employee's city of residence is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Info.Address.State",
                            Prompt = "State of Residence",
                            HelpText = "Matches on the employee's address state (residence address, not office). Use the 2-character ISO code for US states and territories",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "State of Residence",
                            RuleDescription = "Employee's state of residence is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Info.Address.PostalCode",
                            Prompt = "Postal Code of Residence",
                            HelpText = "Matches on the employee's address postal code (residence address, not office)",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Postal Code of Residence",
                            RuleDescription = "Employee's postal code of residence is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Info.Address.Country",
                            Prompt = "Country of Residence",
                            HelpText = "Matches on the employee's address country (residence address, not office). Use the 2-character ISO country code, e.g. US, CA, MX, etc.",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Country of Residence",
                            RuleDescription = "Employee's country of residence is or is not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.DoB",
                            Prompt = "Date of Birth",
                            HelpText = "Compares a provided value to the employee's date of birth, if provided",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee's Date of Birth",
                            RuleDescription = "Compares the employee's date of birth to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.MilitaryStatus",
                            Prompt = "Military Status",
                            HelpText = "Matches on the employee's military status",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            Options = getEnumOptions<MilitaryStatus>(),
                            RuleName = "Military Status",
                            RuleDescription = "Employee's military status is or is not",
                            RuleOperator = "==",
                            TypeName = typeof(MilitaryStatus).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Meets50In75MileRule",
                            Prompt = "50 Employees in 75 Mile Radius Rule",
                            HelpText = "Determines whether or not the employee meets or does not meet the 50 employees in 75 mile rule. If this is disabled in the employer configuration, the result of this rule always returns 'true'",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            IsFunction = true,
                            RuleName = "50 in 75 Mile Rule",
                            RuleDescription = "Employee meets the 50 employees in 75 mile rule",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "AgeInYears",
                            Prompt = "Employee Age in Years",
                            HelpText = "Gets the employee's age in years from the case start date for checking min, max or age equality",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            IsFunction = true,
                            RuleName = "Employee's Age in Years",
                            RuleDescription = "The employee's age in years meets the specified criteria",
                            RuleOperator = ">=",
                            Value = 0,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        employeeGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.IsFlightCrew",
                            Prompt = "Is Airline Flight Crew",
                            HelpText = "Determines whether or not the employee is an airline flight crew employee",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is Airline Flight Crew",
                            RuleDescription = "Ensures that an employee is an airline flight crew employee",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        //CustomField(name) --> lookup Employer custom fields...
                        if (!string.IsNullOrWhiteSpace(EmployerId))
                        {
                            var fields = CustomField.AsQueryable().Where(f => f.CustomerId == CustomerId && f.EmployerId == EmployerId).ToList()
                                .Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();
                            if (!string.IsNullOrWhiteSpace(CustomerId))
                                fields.AddRangeIfNotExists(
                                    CustomField.AsQueryable().Where(f => f.CustomerId == CustomerId && f.EmployerId == null).ToList()
                                    .Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList(),
                                    (c1, c2) => string.Equals(c1.Name, c2.Name, StringComparison.InvariantCultureIgnoreCase));
                            if (fields.Any())
                            {
                                employeeGroup.Expressions.AddRange(fields.Select(f =>
                                {
                                    var exp = new RuleExpression()
                                    {
                                        ControlType = ControlType.CheckBox,
                                        Operators = getOperators(true, true, true, true, true, true),
                                        Name = "EmployeeCustomField" + f.Name.Replace(" ", string.Empty),
                                        Prompt = f.Name,
                                        HelpText = f.HelpText ?? f.Description,
                                        RuleName = f.Name,
                                        RuleDescription = f.Description,
                                        RuleOperator = "==",
                                        IsFunction = true,
                                        IsCustom = true,
                                        Parameters = new List<RuleExpressionParameter>()
                                        {
                                            new RuleExpressionParameter()
                                            {
                                                ControlType = ControlType.SelectList,
                                                Name = "op",
                                                TypeName = typeof(string).AssemblyQualifiedName,
                                                Options = (f.DataType == CustomFieldType.Number || f.DataType == CustomFieldType.Date)
                                                    ? getOperators(true, true, true, true, true, true).Select(x => new RuleExpressionOption(){ Value = x.Key, Text = x.Value }).ToList()
                                                    : getOperators().Select(x => new RuleExpressionOption() { Value = x.Key, Text = x.Value }).ToList()
                                            },
                                            new RuleExpressionParameter()
                                            {
                                                ControlType = f.DataType == CustomFieldType.Flag 
                                                    ? ControlType.CheckBox : f.DataType == CustomFieldType.Number 
                                                    ? ControlType.Numeric : f.DataType == CustomFieldType.Date 
                                                    ? ControlType.Date : ControlType.TextBox,
                                                Name = "value",
                                                TypeName = getSystemTypeName(f.DataType == CustomFieldType.Flag
                                                    ? ControlType.CheckBox : f.DataType == CustomFieldType.Number
                                                    ? ControlType.Numeric : f.DataType == CustomFieldType.Date
                                                    ? ControlType.Date : ControlType.TextBox)
                                            },
                                            new RuleExpressionParameter()//Please do not change the order of this parameter since it is accessed by index 3
                                            {
                                                ControlType = ControlType.Hidden,
                                                Name = "fieldName",
                                                Value = f.Name,
                                                TypeName = typeof(String).AssemblyQualifiedName
                                            }
                                        },
                                        Value = true
                                    };
                                    exp.TypeName = getSystemTypeName(exp.ControlType);
                                    if (f.ValueType == CustomFieldValueType.SelectList)
                                    {
                                        exp.Parameters[1].ControlType = ControlType.SelectList;
                                        exp.Parameters[1].Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                                    }

                                    return exp;
                                }));
                            }
                        }//end: CustomField
                        #endregion

                        #region scheduleGroup
                        var scheduleGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Schedule Information",
                            Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's work schedule and work history information",
                            Expressions = new List<RuleExpression>()
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.ServiceDate",
                            Prompt = "Service Date",
                            HelpText = "Compares a provided value to the employee's service date, if provided",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee's Service Date",
                            RuleDescription = "Compares the employee's service date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.HireDate",
                            Prompt = "Hire Date",
                            HelpText = "Compares a provided value to the employee's hire date, if provided",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee's Hire Date",
                            RuleDescription = "Compares the employee's hire date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.RehireDate",
                            Prompt = "Rehire Date",
                            HelpText = "Compares a provided value to the employee's rehire date, if provided",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee's Rehire Date",
                            RuleDescription = "Compares the employee's rehire date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.TerminationDate",
                            Prompt = "Termination Date",
                            HelpText = "Compares a provided value to the employee's termination date, if provided",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee's Termination Date",
                            RuleDescription = "Compares the employee's termination date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "TotalHoursWorkedLast12Months",
                            Prompt = "Total Hours Worked in the Last 12 Months",
                            HelpText = "Used to compare the total number of hours this employee has worked in the last 12 months (measured from the start date of the case)",
                            ControlType = ControlType.Numeric,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Total Hours Worked in the Last 12 Months",
                            RuleDescription = "The total hours worked in the last 12 months conditions have been met",
                            RuleOperator = ">=",
                            Value = 0,
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "HoursWorkedPerWeek",
                            Prompt = "Hours Worked per Week",
                            HelpText = "Used to compare the average hours the employee works per week (measured as average over the course of the last year from the start date of the case)",
                            ControlType = ControlType.Numeric,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Hours Worked per Week",
                            RuleDescription = "Employee's average hours worked per week requirements are met",
                            RuleOperator = ">=",
                            Value = 0,
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "AverageHoursWorkedPerWeekOverPeriod",
                            Prompt = "Average hours worked per week",
                            HelpText = "Used to compare the average hours the employee works per week over a period (e.g. days, months, years, etc.)",
                            ControlType = ControlType.Numeric,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                 new RuleExpressionParameter()
                                {
                                    Name = "minAvergeHoursWorkedPerWeek",
                                    Prompt = "Hours Worked per Week",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Minimum Average Hours Worked Per Week .",
                                    Value = 0,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "minLengthOfService",
                                    Prompt = "Minimum Length of Service",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of service (i.e. for 3 months, enter in '3' here).",
                                    Value = 12,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of service (i.e. for 3 months, select 'Months').",
                                    Value = Unit.Months,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Average hours worked per week over a period",
                            RuleDescription = "Average hours worked per week over has been met",
                            RuleOperator = ">=",
                            Value = 25,
                            TypeName = typeof(Boolean).AssemblyQualifiedName

                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "MeetsNewYorkPflMinLengthOfServiceFT",
                            Prompt = "New York PFL Min Length Of Service-Full Time",
                            HelpText = "Use to compare the Minimum length of continous employement period for Full Time employees; provide the length of employment and applicable unit type for that duration (e.g. days, months, years, etc.)",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "minConsecutiveLengthOfEmployment",
                                    Prompt = "Minimum Consecutive Length of Employment",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of consecutive (i.e. for 26 weeks, enter in '26' here).",
                                    Value = 26,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of employment period (i.e. for 26 weeks, select 'weeks').",
                                    Value = Unit.Weeks,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Minimum length of consecutive employment period",
                            RuleDescription = "Minimum length of consecutive employment period has been met",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName

                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "MeetsNewYorkPflMinLengthOfServicePT",
                            Prompt = "New York PFL Min Length Of Service-Part Time",
                            HelpText = "Use to compare the Minimum length of non consecutive employment period for Part Time employees within configured continuous employment period.",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "minConsecutiveLengthOfEmployment",
                                    Prompt = "Minimum Consecutive Length of Employment",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of consecutive employment (i.e. for 56 weeks, enter in '56' here).",
                                    Value = 52,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of consecutive employment (i.e. for 56 weeks, select 'weeks').",
                                    Value = Unit.Weeks,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "minimumEmploymentPeriod",
                                    Prompt = "Minimum Employment Period",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum employment period in days(i.e. for 175 days, enter in '175' here).",
                                    Value = 175,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Minimum length of Employement Period",
                            RuleDescription = "Minimum length of employment period has been met",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName

                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "TotalMonthsWorked",
                            Prompt = "Total Months Worked",
                            HelpText = "Used to compare the total number of months that the employee has worked (measured from the start date of the case)",
                            ControlType = ControlType.Numeric,
                            IsFunction = true,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Total Months Worked",
                            RuleDescription = "Employee has worked the required number of months before the start of the case",
                            RuleOperator = ">=",
                            Value = 0,
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Has1YearOfService",
                            Prompt = "Has Minimum of 1 Year of Service",
                            HelpText = "Used to determine if the employee has at least one year of service from the service/hire date",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            RuleName = "Has minimum of 1 Year of Service",
                            RuleDescription = "The employee has at least 1 year (12 months) of service",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "HasMinLengthOfService",
                            Prompt = "Minimum Length of Service",
                            HelpText = "Minimum length of service has been met from their service/hire date; provide the length of service and applicable unit type for that duration (e.g. days, months, years, etc.)",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "minLengthOfService",
                                    Prompt = "Minimum Length of Service",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "Enter in the numeric portion of the minimum length of service (i.e. for 3 months, enter in '3' here).",
                                    Value = 12,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                },
                                new RuleExpressionParameter()
                                {
                                    Name = "unitType",
                                    Prompt = "Unit Type",
                                    ControlType = ControlType.SelectList,
                                    HelpText = "Enter in the unit portion of the minimum length of service (i.e. for 3 months, select 'Months').",
                                    Value = Unit.Months,
                                    Options = getEnumOptions<Unit>(),
                                    TypeName = typeof(Unit).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Minimum length of service",
                            RuleDescription = "Minimum length of service has been met",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        scheduleGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "HasLesserOfHalfFTHoursOrNHoursPerWeek",
                            Prompt = "Has Lesser of ½ FT or # Hours per Week",
                            HelpText = "Minimum hours worked per week is the lesser of n hrs/week or ½ of employers FT equivalency (i.e. used for Minnesota Parenting Leave)",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "nHours",
                                    Prompt = "Hours",
                                    ControlType = ControlType.Numeric,
                                    HelpText = "The number of hours to measure against, OR the employer's FT hours / 2, whichever is less.",
                                    Value = 20,
                                    TypeName = typeof(Double).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Minimum hours per week",
                            RuleDescription = "Minimum hours worked per week is the lesser of n hrs/week or ½ of employers FT equivalency",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        #endregion

                        #region jobGroup
                        var jobGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Job Information",
                            Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employee's job information",
                            Expressions = new List<RuleExpression>()
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.IsKeyEmployee",
                            Prompt = "Is Key Employee",
                            HelpText = "Determines whether or not the employee is a key employee",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is Key Employee",
                            RuleDescription = "Employee is or is not a key employee",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.IsExempt",
                            Prompt = "Is Exempt",
                            HelpText = "Determines whether or not the employee is a federally exempt employee",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is Exempt",
                            RuleDescription = "Employee is a federally exempt employee",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Status",
                            Prompt = "Employment Status",
                            HelpText = "Compares the employment status",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            RuleName = "Employment Status",
                            RuleDescription = "Employee is or is not the selected employment status",
                            RuleOperator = "==",
                            Options = getEnumOptions<EmploymentStatus>(),
                            Value = EmploymentStatus.Active,
                            TypeName = typeof(EmploymentStatus).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.PayType",
                            Prompt = "Pay Type",
                            HelpText = "Compares the employee's type of pay",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            RuleName = "Pay Type",
                            RuleDescription = "Employee does or does not have the selected pay type",
                            RuleOperator = "==",
                            Options = getEnumOptions<PayType>(),
                            Value = PayType.Salary,
                            TypeName = typeof(PayType).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Salary",
                            Prompt = "Employee Pay",
                            HelpText = "Compares the employee's pay rate/salary",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee Pay",
                            RuleDescription = "The employee's pay rate/salary requirements are met",
                            RuleOperator = ">=",
                            Value = 0,
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "EffectiveAnnualPayFromLeaveStartDate",
                            Prompt = "Effective Annual Pay",
                            HelpText = "Compares the employee's effective annual pay (measured from the start date of the case)",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Employee Pay",
                            RuleDescription = "The employee's effective annual pay from the leave start date requirements are met",
                            RuleOperator = ">=",
                            Value = 0,
                            IsFunction = true,
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.EmployeeClassCode",
                            Prompt = "Employee Class",
                            HelpText = "Compares the employee's type of work frequency",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            RuleName = "Work Type",
                            RuleDescription = "Employee does or does not have the selected work type",
                            RuleOperator = "==",
                            Options = getEmployeeClassOptions(),
                            Value = WorkType.FullTime,
                            TypeName = typeof(WorkType).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.JobTitle",
                            Prompt = "Job Title",
                            HelpText = "Compares the employee's job title via an exact match to a provided value",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Job Title",
                            RuleDescription = "The employee has or does not have the entered in job title",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Job.Classification",
                            Prompt = "Job Classification",
                            HelpText = "Compares the job classification for the employee's current position. Represents the Social Security Administration's defintions of the 5 job classifications and related restrictions when relating to social security claims, but are also used for worker's comp and other benefits and legal definitions around disability",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Job Classification",
                            RuleDescription = "Employee meets the job classification requirement",
                            RuleOperator = "==",
                            Options = getEnumOptions<JobClassification>(),
                            Value = JobClassification.Light,
                            TypeName = typeof(JobClassification).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Job.CensusJobCategoryCode",
                            Prompt = "Census Job Category Code",
                            HelpText = "Compares the US Equal Employment Opportunity Commission EEO-1 Job Classification Guide code",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Census Job Category Code",
                            RuleDescription = "Employee meets the requirement for the US Equal Employment Opportunity Commission EEO-1 Job Classification Guide code",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        jobGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employee.Job.SOCCode",
                            Prompt = "SOC Code",
                            HelpText = "Compares the Standard Occupational Classification Manual code for the employee's current job",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "SOC Code",
                            RuleDescription = "Employee meets the requirement for the Standard Occupational Classification Manual code",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        #endregion

                        #region caseGroup
                        var caseGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Case Information",
                            Description = "Used as filters, rules and expressions that evaluate properties and conditions of the case/absence, typically through intake",
                            Expressions = new List<RuleExpression>()
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "NumberOfChildrenForLeave",
                            Prompt = "Number of Children for Case",
                            HelpText = "Compares the number of children related to the case (i.e. for pregnancy/maternity or adoption of n children)",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Number of Children for Case",
                            RuleDescription = "The number of children related to the case requirements are met",
                            RuleOperator = ">=",
                            Value = 0,
                            IsFunction = true,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.StartDate",
                            Prompt = "Start Date",
                            HelpText = "Compares a provided value to the case's start date",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Case's Start Date",
                            RuleDescription = "Compares the case's start date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.EndDate",
                            Prompt = "End Date",
                            HelpText = "Compares a provided value to the case's end date",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Case's End Date",
                            RuleDescription = "Compares the case's end date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.RelatedCaseType",
                            Prompt = "Related Case Type",
                            HelpText = "When a case has a related case, matches the type of relationship that case shares (i.e. is it a recurring condition/relapse, or some other related reason)",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            RuleName = "Related Case Type",
                            RuleDescription = "The related case is of the proper type",
                            RuleOperator = "==",
                            Options = getEnumOptions<RelatedCaseType>(),
                            Value = RelatedCaseType.Recurring,
                            TypeName = typeof(RelatedCaseType).AssemblyQualifiedName
                        });

                        var contactTypes = new ContactTypeService().Using(s => s.GetContactTypes(CustomerId, EmployerId, ContactTypeDesignationType.Personal, ContactTypeDesignationType.Self));
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "CaseRelationshipType",
                            Prompt = "Related Person",
                            HelpText = "Select the type of case's related person allowed or not allowed for this rule",
                            ControlType = ControlType.MultiSelectList,
                            Operators = getOperators(true, true, false, false, false, false, true),
                            RuleName = "Related Person",
                            RuleDescription = "Relationship to employee is met",
                            RuleOperator = "::",
                            Options = contactTypes.Select(t => new RuleExpressionOption() { Value = t.Code, Text = t.Name }).ToList(),
                            Value = "SELF",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Contact.ContactTypeOther",
                            Prompt = "Related Person's Contact Type (when other)",
                            HelpText = "Matches a case's related contact type when the type selected is 'Other' and free-form text is provided. Matches against the free-form text itself, not the fact that it's 'Other'.",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Related Person's Contact Type (when other)",
                            RuleDescription = "The related contact type is 'x'",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "ContactAgeInYears",
                            Prompt = "Related Person's Age",
                            HelpText = "Compares the case's related person's age, if provided",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            IsFunction = true,
                            RuleName = "Related Person's Age",
                            RuleDescription = "The related contact's age is less than n",
                            RuleOperator = "<",
                            Value = 0,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "NumberOfMonthsFromDeliveryDate",
                            Prompt = "Number Of Months From Delivery Date",
                            HelpText = "Gets the Number Of Months From Delivery Date till End Of Case Date",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            IsFunction = true,
                            RuleName = "Delivery Date",
                            RuleDescription = "Pregnancy leave must end within n months of Delivery Date",
                            RuleOperator = "<=",
                            Value = 12,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "LengthOfServiceFromExpectedDeliveryDate",
                            Prompt = "Length Of Service From Expected Delivery Date",
                            HelpText = "Used to determine if the employee has mininum length of service from the expected delivery date",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            IsFunction = true,
                            RuleName = "Expected Delivery Date",
                            RuleDescription = "Employee has to be employed more than n months before their expected delivery date",
                            TypeName = typeof(Double).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "NumberOfMonthsFromAdoptionDate",
                            Prompt = "Number Of Months From Adoption Date",
                            HelpText = "Gets the Number Of Months From Adoption Date till End Of Case Date",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            IsFunction = true,
                            RuleName = "Adoption Date",
                            RuleDescription = "Adoption/Foster care leave must end within n months of Adoption Date",
                            RuleOperator = "<=",
                            Value = 12,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Contact.MilitaryStatus",
                            Prompt = "Related Person's Military Status",
                            HelpText = "Compares the person related to this case's military status to the value provided",
                            ControlType = ControlType.SelectList,
                            Operators = getOperators(),
                            RuleName = "Related Person's Military Status",
                            RuleDescription = "The related contact's military status matches the value",
                            RuleOperator = "==",
                            Options = getEnumOptions<MilitaryStatus>(),
                            Value = MilitaryStatus.Civilian,
                            TypeName = typeof(MilitaryStatus).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.IsAccommodation",
                            Prompt = "Is Accommodation",
                            HelpText = "Determines whether or not the case is an accommodation case",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is Accommodation",
                            RuleDescription = "This is an accommodation case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.IsInformationOnly",
                            Prompt = "Is Information Only",
                            HelpText = "Determines whether or not the case is information only",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is Information Only",
                            RuleDescription = "This is an information only case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "IsIntermittent",
                            Prompt = "Is Intermittent",
                            HelpText = "Determines whether or not the case or any portion thereof is intermittent",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            RuleName = "Is Intermittent",
                            RuleDescription = "This is an intermittent case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.IsSTD",
                            Prompt = "Is STD",
                            HelpText = "Determines whether or not the case is a short term disability case",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Is STD",
                            RuleDescription = "This is a short term disability case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Disability.PrimaryDiagnosis",
                            Prompt = "Primary Diagnosis Code",
                            HelpText = "Compares the primary diagnosis code with the value provided. No auto-complete is provided, please properly lookup the exact intended ICD9 or 10 code (they are not interchangeable)",
                            ControlType = ControlType.TextBox,
                            Operators = getOperators(),
                            RuleName = "Primary Diagnosis Code",
                            RuleDescription = "The primary diagnosis code for the employee's disability is equal or not equal to this value",
                            RuleOperator = "==",
                            TypeName = typeof(String).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Disability.MedicalComplications",
                            Prompt = "Medical Complications",
                            HelpText = "Determines whether or not the employee had any medical complications in relation to the case",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Medical Complications",
                            RuleDescription = "The employee had medical complications due to the illness or injury related to this case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Disability.Hospitalization",
                            Prompt = "Hospitalization",
                            HelpText = "Determines whether or not the employee had any hospitalizations in relation to the case",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            RuleName = "Hospitalization",
                            RuleDescription = "The employee was hospitalized due to the illness or injury related to this case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Case.Disability.ConditionStartDate",
                            Prompt = "Condition Start Date",
                            HelpText = "Compares a provided value to the case's disability condition's start date",
                            ControlType = ControlType.Date,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Condition Start Date",
                            RuleDescription = "Compares the case's disability condition's start date to this value",
                            RuleOperator = ">=",
                            TypeName = typeof(DateTime).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "IsWorkRelated",
                            Prompt = "Is Work Related",
                            HelpText = "Determines whether or not the case is work related",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            RuleName = "Is Case Work Related",
                            RuleDescription = "This is a work related case",
                            RuleOperator = "==",
                            Value = true,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "WillUseBonding",
                            Prompt = "Will use bonding?",
                            HelpText = "Is Bonding use on leave request",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            RuleName = "Will Use Bonding?",
                            RuleDescription = "This is Preg-Mat related",
                            RuleOperator = "==",
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        caseGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "IsMetadataValueTrue",
                            Prompt = "Is Metadata Value True",
                            HelpText = "[WARNING]: ***This is an advanced rule*** Please consult your integration/usage guide, or your AbsenceTracker representative if you feel you need to use this rule for your own policies; These metadata values are stored on the case itself, not the employee. These are NOT the same thing as custom fields and are dynamic, system generated properties.",
                            ControlType = ControlType.CheckBox,
                            IsFunction = true,
                            Operators = getOperators(),
                            Parameters = new List<RuleExpressionParameter>()
                            {
                                new RuleExpressionParameter()
                                {
                                    Name = "fieldName",
                                    Prompt = "Field Name",
                                    ControlType = ControlType.TextBox,
                                    HelpText = "The field name to lookup in the metadata that is expected to be a Boolean value, and should be true",
                                    Value = "",
                                    TypeName = typeof(String).AssemblyQualifiedName
                                }
                            },
                            RuleName = "Is Metadata Value True",
                            RuleDescription = "",
                            RuleOperator = "==",
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        if (!string.IsNullOrWhiteSpace(EmployerId))
                        {
                            var fields = CustomField.AsQueryable().Where(f => f.CustomerId == CustomerId && f.EmployerId == EmployerId).ToList().Where(f => (f.Target & EntityTarget.Case) == EntityTarget.Case).ToList();
                            if (!string.IsNullOrWhiteSpace(CustomerId))
                                fields.AddRangeIfNotExists(
                                    CustomField.AsQueryable().Where(f => f.CustomerId == CustomerId && f.EmployerId == null).ToList().Where(f => (f.Target & EntityTarget.Case) == EntityTarget.Case).ToList(),
                                    (c1, c2) => string.Equals(c1.Name, c2.Name, StringComparison.InvariantCultureIgnoreCase));
                            if (fields.Any())
                            {
                                caseGroup.Expressions.AddRange(fields.Select(f =>
                                {
                                    var exp = new RuleExpression()
                                    {
                                        ControlType = ControlType.CheckBox,
                                        Operators = getOperators(true, true, true, true, true, true),
                                        Name = "CaseCustomField" + f.Name.Replace(" ", string.Empty),
                                        Prompt = f.Name,
                                        HelpText = f.HelpText ?? f.Description,
                                        RuleName = f.Name,
                                        RuleDescription = f.Description,
                                        RuleOperator = "==",
                                        IsFunction = true,
                                        IsCustom = true,
                                        Parameters = new List<RuleExpressionParameter>()
                                        {
                                            new RuleExpressionParameter()
                                            {
                                                ControlType = ControlType.SelectList,
                                                Name = "op",
                                                TypeName = typeof(String).AssemblyQualifiedName,
                                                Options =  (f.DataType ==  CustomFieldType.Number || f.DataType ==  CustomFieldType.Date) 
                                                    ? getOperators(true, true, true, true, true, true).Select(x => new RuleExpressionOption(){ Value = x.Key, Text = x.Value }).ToList() 
                                                    : getOperators().Select(x => new RuleExpressionOption() { Value = x.Key, Text = x.Value }).ToList()
                                            },
                                            new RuleExpressionParameter()
                                            {
                                                ControlType = f.DataType == CustomFieldType.Flag 
                                                    ? ControlType.CheckBox : f.DataType == CustomFieldType.Number 
                                                    ? ControlType.Numeric : f.DataType == CustomFieldType.Date 
                                                    ? ControlType.Date : ControlType.TextBox,
                                                Name = "value",
                                                TypeName = getSystemTypeName(f.DataType == CustomFieldType.Flag
                                                    ? ControlType.CheckBox : f.DataType == CustomFieldType.Number
                                                    ? ControlType.Numeric : f.DataType == CustomFieldType.Date
                                                    ? ControlType.Date : ControlType.TextBox),
                                            },
                                            new RuleExpressionParameter()//Please do not change the order of this parameter since it is accessed by index 3
                                            {
                                                ControlType = ControlType.Hidden,
                                                Name = "fieldName",
                                                Value = f.Name,
                                                TypeName = typeof(string).AssemblyQualifiedName
                                            }
                                        },
                                        Value = true
                                    };
                                    exp.TypeName = getSystemTypeName(exp.ControlType);
                                    if (f.ValueType == CustomFieldValueType.SelectList)
                                    {
                                        exp.Parameters[1].ControlType = ControlType.SelectList;
                                        exp.Parameters[1].Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                                    }

                                    return exp;
                                }));
                            }
                        }
                        #endregion

                        #region employerGroup
                        var employerGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Employer Information",
                            Description = "Used as filters, rules and expressions that evaluate properties and conditions of the employer (such as number of employees, etc.)",
                            Expressions = new List<RuleExpression>()
                        });
                        employerGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "TotalEmployeesAtEmployer",
                            Prompt = "Total Employees at Employer",
                            HelpText = "Compares the total number of employees employed by this employer (in the AbsenceTracker database) against a provided value.",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Total Employees at Employer",
                            RuleDescription = "The total employees at this employer are compared to this value",
                            RuleOperator = ">=",
                            IsFunction = true,
                            Value = 0,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        employerGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "TotalEmployeesAtEmployerInTheSameWorkState",
                            Prompt = "Total Employees at Employer in Work State",
                            HelpText = "Compares the total number of employees employed by this employer (in the AbsenceTracker database) with the same work state against a provided value.",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(true, true, true, true, true, true),
                            RuleName = "Total Employees at Employer in Work State",
                            RuleDescription = "The total employees at this employer with the same work state are compared to this value",
                            RuleOperator = ">=",
                            IsFunction = true,
                            Value = 0,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        employerGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "Employer.IsPubliclyTradedCompany",
                            Prompt = "Employer is a Publicly Traded Company",
                            HelpText = "Determines whether or not the employer is a publicly traded company",
                            ControlType = ControlType.CheckBox,
                            Operators = getOperators(),
                            IsFunction = true,
                            RuleName = "Employer is a Publicly Traded Company",
                            RuleDescription = "Employer is or is not a publicly traded company",
                            RuleOperator = "==",
                            Value = false,
                            TypeName = typeof(Boolean).AssemblyQualifiedName
                        });
                        #endregion

                        #region logicGroup
                        var logicGroup = myGroups.AddFluid(new RuleExpressionGroup()
                        {
                            Title = "Logic",
                            Description = "Used as filters, rules and expressions that evaluate logical operations such as Boolean logic, arithmatic or other operations",
                            Expressions = new List<RuleExpression>()
                        });
                        logicGroup.Expressions.Add(new RuleExpression()
                        {
                            Name = "1",
                            Prompt = "Literal value of '1'",
                            HelpText = "Checks wither a number equals 1, such as 0 or 1. Used primarily for Always TRUE or Always FALSE conditions.",
                            ControlType = ControlType.Numeric,
                            Operators = getOperators(),
                            RuleName = "1 equals 0",
                            RuleDescription = "The Number 1 MUST equal the Number 0",
                            RuleOperator = "==",
                            Value = 0,
                            TypeName = typeof(Int32).AssemblyQualifiedName
                        });
                        #endregion logicGroup
                    }
                }
            }

            return _expressionGroups[EmployerId ?? "_"].Expressions;
        }//end: GetRuleExpressions

        public virtual RuleExpression GetExpression(Rule rule, IEnumerable<RuleExpressionGroup> expressionGroups)
        {
            List<string> validation = new List<string>();
            if (string.IsNullOrWhiteSpace(rule.Name))
                validation.Add("A name must be supplied for this rule");
            if (string.IsNullOrWhiteSpace(rule.Operator))
                validation.Add("A valid operator was not selected. Please select a valid operator");
            if (string.IsNullOrWhiteSpace(rule.LeftExpression))
                validation.Add("The rule is missing the left expression (core name) of the property or routine it maps to");
            if (validation.Any())
                throw new AbsenceSoftAggregateException(validation.ToArray());

            string name = rule.LeftExpression;
            string[] pars = null;
            if (!string.IsNullOrWhiteSpace(name) && name.Contains("("))
            {
                name = Regex.Replace(name, @"\(.*\)$", "");
                int idx = rule.LeftExpression.IndexOf('(');
                pars = rule.LeftExpression.Substring(idx + 1, rule.LeftExpression.Length - idx - 2).Split(',').Select(p => p.Trim()).ToArray();
            }

            RuleExpression expression = expressionGroups.SelectMany(g => g.Expressions).FirstOrDefault(r => (r.IsCustom && pars != null && pars.Length >= 3 && r.Name.Replace("'", string.Empty) == name + pars[2].Replace(" ", string.Empty).Replace("'", string.Empty).Replace("\\", string.Empty)) ||
            (r.IsCustom && pars != null && pars.Length == 1 && r.Name.Replace("'", string.Empty) == name + pars[0].Replace(" ", string.Empty).Replace("'", string.Empty).Replace("\\", string.Empty)) || r.Name == name);
            if (expression == null)
                return null;

            RuleExpression copy = expression.Clone<RuleExpression>();
            copy.RuleId = rule.Id;
            copy.RuleName = rule.Name;
            copy.RuleDescription = rule.Description;
            copy.RuleOperator = rule.Operator;
            copy.StringValue = rule.RightExpression;
            if (pars != null && copy.Parameters != null && pars.Length == copy.Parameters.Count)
                for (var i = 0; i < copy.Parameters.Count; i++)
                    copy.Parameters[i].StringValue = pars[i];

            return copy;

        }//end: GetExpression

        public Rule GetRule(RuleExpression expression)
        {
            List<string> validation = new List<string>();
            if (string.IsNullOrWhiteSpace(expression.RuleName))
                validation.Add("A name must be supplied for this rule");
            if (string.IsNullOrWhiteSpace(expression.RuleOperator))
                validation.Add("A valid operator was not selected. Please select a valid operator");
            if (!string.IsNullOrWhiteSpace(expression.RuleOperator) && !expression.Operators.ContainsKey(expression.RuleOperator))
                validation.Add("The operator provided is not valid for this rule or expression");
            if (validation.Any())
                throw new AbsenceSoftAggregateException(validation.ToArray());

            string left = expression.Name;
            //This is needed if there are more than one custom field specific rule at Case/Employee level. Since loa method is the same
            //there should be some way to distinguish between different custom field rule.
            if (expression.IsCustom)
            {
                if (left.Contains("CaseCustomField"))
                    left = "CaseCustomField";
                else if (left.Contains("EmployeeCustomField"))
                    left = "EmployeeCustomField";
                if (left.Contains("CaseCustomNumberField"))
                    left = "CaseCustomNumberField";
                else if (left.Contains("CaseCustomDateField"))
                    left = "CaseCustomDateField";
                else if (left.Contains("CaseCustomBoolField"))
                    left = "CaseCustomBoolField";
                else if (left.Contains("CustomNumberField"))
                    left = "CustomNumberField";
                else if (left.Contains("CustomDateField"))
                    left = "CustomDateField";
                else if (left.Contains("CustomBoolField"))
                    left = "CustomBoolField";
            }
            if (expression.IsFunction)
            {
                left += "(";
                if (expression.Parameters != null && expression.Parameters.Any())
                    left += string.Join(", ", expression.Parameters.Select(p => p.StringValue));
                left += ")";
            }

            return new Rule()
            {
                Id = expression.RuleId ?? Guid.NewGuid(),
                Name = expression.RuleName,
                Description = expression.RuleDescription,
                LeftExpression = left,
                Operator = expression.RuleOperator,
                RightExpression = expression.StringValue
            };
        }//end: GetRule

        internal RuleExpressionGroup GetGroup(List<RuleExpressionGroup> groups, string title, string description)
        {
            RuleExpressionGroup group = groups.FirstOrDefault(g => g.Title == title);
            if (group == null)
            {
                group = new RuleExpressionGroup()
                {
                    Title = title,
                    Description = description
                };
            }

            return group;
        }

        internal void AddGroupIfNotExists(List<RuleExpressionGroup> groups, RuleExpressionGroup group)
        {
            bool groupExists = groups.FirstOrDefault(g => g.Title == group.Title) != null;
            if (!groupExists)
            {
                groups.Add(group);
            }
        }

        internal void AddRuleIfNotExists(RuleExpressionGroup group, RuleExpression expression)
        {
            bool ruleExists = group.Expressions.FirstOrDefault(e => e.Name == expression.Name) != null;
            if (!ruleExists)
            {
                group.Expressions.Add(expression);
            }
        }
    }
}

