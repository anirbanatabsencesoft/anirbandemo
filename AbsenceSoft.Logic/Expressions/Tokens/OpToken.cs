namespace AbsenceSoft.Logic.Expressions.Tokens
{
    internal class OpToken : ValueToken
    {
        public OpToken()
        {
            IsOperator = true;
            ArgCount = 0;
        }
    }
}