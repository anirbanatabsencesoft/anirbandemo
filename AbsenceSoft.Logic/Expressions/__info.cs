﻿/*
 * AbsenceSoft.Logic.Expressions
 * C# Expression Evaluator, stolen fair and square :-)
 * 
 *  Author: Rupert Avery
 *  License: GNU Lesser General Public License (LGPL)
 *  Project Home: http://csharpeval.codeplex.com
 *  Documentation: http://csharpeval.codeplex.com/documentation
 *  
 */