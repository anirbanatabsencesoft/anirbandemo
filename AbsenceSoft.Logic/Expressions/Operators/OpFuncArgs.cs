﻿using System.Collections.Generic;
using System.Linq.Expressions;
using AbsenceSoft.Logic.Expressions.Tokens;

namespace AbsenceSoft.Logic.Expressions.Operators
{
    internal class OpFuncArgs
    {
        public Queue<ValueToken> TempQueue;
        public Stack<Expression> ExprStack;
        //public Stack<String> literalStack;
        public ValueToken T;
        public IOperator Op;
        public List<Expression> Args;
    }
}