﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Expressions.Tokens;

namespace AbsenceSoft.Logic.Expressions.Operators
{
    internal class OpFuncServiceProviders
    {
        public static Expression MethodOperatorFunc(
            OpFuncArgs args
            )
        {
            string nextToken = ((MemberToken)args.T).Name;
            Expression le = args.ExprStack.Pop();

            Expression result = ((MethodOperator)args.Op).Func(le, nextToken, args.Args);

            return result;
        }

        public static Expression TypeOperatorFunc(
            OpFuncArgs args
            )
        {
            Expression le = args.ExprStack.Pop();
            return ((TypeOperator)args.Op).Func(le, args.T.Type);
        }

        public static Expression UnaryOperatorFunc(
            OpFuncArgs args
            )
        {
            Expression le = args.ExprStack.Pop();
            return ((UnaryOperator)args.Op).Func(le);
        }

        public static Expression BinaryOperatorFunc(
            OpFuncArgs args
            )
        {
            Expression re = args.ExprStack.Pop();
            Expression le = args.ExprStack.Pop();
            // perform implicit conversion on known types
            TypeConversion.Convert(ref le, ref re);
            return ((BinaryOperator)args.Op).Func(le, re);
        }

        public static Expression MultiValuedOperatorFunc(
            OpFuncArgs args
        )
        {
            List<Expression> expressionList = new List<Expression>();
            List<MemberToken> memberTokensList = new List<MemberToken>();
            Expression le = Expression.Constant(false);

            while (args != null && args.ExprStack != null && args.ExprStack.Count > 0)
            {
                expressionList.Add(args.ExprStack.Peek());
                args.ExprStack.Pop();
            }

            while (args != null && args.TempQueue != null && args.TempQueue.Count > 0)
            {
                memberTokensList.Add(((MemberToken) args.TempQueue.Peek()));
                args.TempQueue.Dequeue();
            }
            
            LeaveOfAbsence loa = (LeaveOfAbsence)((ConstantExpression)expressionList.Last()).Value;
            PropertyInfo loaProperty = loa.GetType().GetProperty(memberTokensList.First().Name);
            le = Expression.Constant(loaProperty.GetValue(loa));
            
            expressionList.RemoveAt(expressionList.Count - 1);
            List<Expression> re = expressionList;
            
            return ((MultiValuedOperator)args.Op).Func(le, re);
        }
    }
}