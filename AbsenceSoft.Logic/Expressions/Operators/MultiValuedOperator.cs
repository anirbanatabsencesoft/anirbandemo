﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AbsenceSoft.Logic.Expressions.Operators
{
    /// <summary>
    /// The Multivalued operator class and its configuration
    /// </summary>
    internal class MultiValuedOperator : Operator<Func<Expression, Expression, Expression>>
    {
        public Func<Expression, List<Expression>, Expression> Func;

        public MultiValuedOperator(string value, int precedence, bool leftassoc,
            Func<Expression, List<Expression>, Expression> func)
            : base(value, precedence, leftassoc, null)
        {
            Arguments = 2;
            this.Func = func;
        }
    }
}