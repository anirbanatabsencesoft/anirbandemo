﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;

namespace AbsenceSoft.Logic.Expressions.Operators
{
    internal static class OperatorCustomExpressions
    {
        /// <summary>
        /// Returns an Expression that accesses a member on an Expression
        /// </summary>
        /// <param name="le">The expression that contains the member to be accessed</param>
        /// <param name="membername">The name of the member to access</param>
        /// <param name="args">Optional list of arguments to be passed if the member is a method</param>
        /// <returns></returns>
        public static Expression MemberAccess(Expression le, string membername, List<Expression> args)
        {
            List<Type> argTypes = new List<Type>();
            args.ForEach(x => argTypes.Add(x.Type));

            Expression instance = null;
            Type type = null;
            if (le.Type.Name == "RuntimeType")
            {
                type = ((Type)((ConstantExpression)le).Value);
            }
            else
            {
                type = le.Type;
                instance = le;
            }

            MethodInfo mi = type.GetMethod(membername, argTypes.ToArray());
            if (mi != null)
            {
                ParameterInfo[] pi = mi.GetParameters();
                for (int i = 0; i < pi.Length; i++)
                {
                    args[i] = TypeConversion.Convert(args[i], pi[i].ParameterType);
                }
                return Expression.Call(instance, mi, args);
            }
            else
            {
                Expression exp = null;

                PropertyInfo pi = type.GetProperty(membername);
                if (pi != null)
                {
                    exp = Expression.Property(instance, pi);
                }
                else
                {
                    FieldInfo fi = type.GetField(membername);
                    if (fi != null)
                    {
                        exp = Expression.Field(instance, fi);
                    }
                }

                if (exp != null)
                {
                    if (args.Count > 0)
                    {
                        return Expression.ArrayAccess(exp, args);
                    }
                    else
                    {
                        return exp;
                    }
                }
                else
                {
                    throw new Exception(string.Format("Member not found: {0}.{1}", le.Type.Name, membername));
                }
            }
        }

        /// <summary>
        /// Extends the Add Expression handler to handle string concatenation
        /// </summary>
        /// <param name="le"></param>
        /// <param name="re"></param>
        /// <returns></returns>
        public static Expression Add(Expression le, Expression re)
        {
            if (le.Type == typeof(string) && re.Type == typeof(string))
                return Expression.Add(le, re, typeof(string).GetMethod("Concat", new Type[] { typeof(string), typeof(string) }));
            else
                return WrapNullableExpression(le, re, Expression.Add);
        }

        /// <summary>
        /// Returns an Expression that access a 1-dimensional index on an Array expression 
        /// </summary>
        /// <param name="le"></param>
        /// <param name="re"></param>
        /// <returns></returns>
        public static Expression ArrayAccess(Expression le, Expression re)
        {
            if (le.Type == typeof(string))
            {
                MethodInfo mi = typeof(string).GetMethod("ToCharArray", new Type[] { });
                le = Expression.Call(le, mi);
            }

            return Expression.ArrayAccess(le, re);
        }

        /// <summary>
        /// Gets the nullable expression function using the wrapper method <see cref="WrapNullableExpression"/>
        /// to ensure if nullable expression targets are passed in they're handled appropriately.
        /// </summary>
        /// <param name="func">The function to convert to nullable.</param>
        /// <returns></returns>
        public static Func<Expression, Expression, Expression> GetNullableExpressionFunc(Func<Expression, Expression, Expression> func)
        {
            return (left, right) => WrapNullableExpression(left, right, func);
        }

        /// <summary>
        /// Wraps the potentially nullable expression(s) by upgrading the non-nullable type if the other is nullable so the binary
        /// operators/expressions work as expected.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="func">The operator binary expression to execute passing the left and right expressions after they've
        /// been transformed into the appropriate nullable types.</param>
        /// <returns>
        /// A <see cref="BinaryExpression"/> that has the <see cref="Expression.NodeType"/>
        /// property equal to the func passed in and the <see cref="BinaryExpression.Left"/> and <see cref="BinaryExpression.Right"/>
        /// properties set to the specified possibly nullable values.
        /// </returns>
        static Expression WrapNullableExpression(Expression left, Expression right, Func<Expression, Expression, Expression> func)
        {
            if (left == null)
                throw new ArgumentNullException("left");

            if (right == null)
                throw new ArgumentNullException("right");

            var isLeftNullable = IsNullableExpression(left);
            var isRightNullable = IsNullableExpression(right);

            if (isLeftNullable && !isRightNullable)
                right = FixExpressionType(right, left);
            else if (!isLeftNullable && isRightNullable)
                left = FixExpressionType(left, right);

            if (!AreSameType(left, right))
                throw new InvalidOperationException(
                    string.Format(
                    CultureInfo.InvariantCulture,
                    "Supplied expressions are of different types. First expression is [{0}], second expression is [{1}]",
                    left.Type.AssemblyQualifiedName ?? left.Type.FullName,
                    right.Type.AssemblyQualifiedName ?? right.Type.FullName));

            return func(left, right);
        }

        #region Nullable Helpers

        /// <summary>
        /// Determines whether expression is nullable.
        /// </summary>
        /// <param name="expression">The expression to check for nullability.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">expression</exception>
        static bool IsNullableExpression(Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            return expression.Type.IsGenericType && expression.Type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// Fixes the type of the expression.
        /// </summary>
        /// <param name="notNullableExpression">The not nullable expression.</param>
        /// <param name="nullableExpression">The nullable expression.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// notNullableExpression
        /// or
        /// nullableExpression
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// notNullableExpression
        /// or
        /// nullableExpression
        /// </exception>
        /// <exception cref="System.InvalidOperationException"></exception>
        static Expression FixExpressionType(Expression notNullableExpression, Expression nullableExpression)
        {
            if (notNullableExpression == null)
                throw new ArgumentNullException("notNullableExpression");

            if (nullableExpression == null)
                throw new ArgumentNullException("nullableExpression");

            if (IsNullableExpression(notNullableExpression))
                throw new ArgumentException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Not nullable expression is nullable: [{0}].",
                        notNullableExpression.Type.AssemblyQualifiedName ?? notNullableExpression.Type.FullName),
                    "notNullableExpression");

            if (!IsNullableExpression(nullableExpression))
                throw new ArgumentException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Nullable expression is not nullable: [{0}].",
                        nullableExpression.Type.AssemblyQualifiedName ?? nullableExpression.Type.FullName),
                    "nullableExpression");

            if (!AreSameType(nullableExpression, notNullableExpression))
                throw new InvalidOperationException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Supplied expressions are of different types. Not nullable expression is [{0}], nullable expression is [{1}].",
                        notNullableExpression.Type.AssemblyQualifiedName ?? notNullableExpression.Type.FullName,
                        nullableExpression.Type.AssemblyQualifiedName ?? nullableExpression.Type.FullName));

            return Expression.Convert(notNullableExpression, nullableExpression.Type);
        }

        /// <summary>
        /// Checks the 2 expressions to determine if the types are the same.
        /// </summary>
        /// <param name="nullableExpression">The nullable expression.</param>
        /// <param name="notNullableExpression">The not nullable expression.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// nullableExpression
        /// or
        /// notNullableExpression
        /// </exception>
        static bool AreSameType(Expression nullableExpression, Expression notNullableExpression)
        {
            if (nullableExpression == null)
                throw new ArgumentNullException("nullableExpression");

            if (notNullableExpression == null)
                throw new ArgumentNullException("notNullableExpression");
            
            var isLeftNullable = IsNullableExpression(nullableExpression);
            var isRightNullable = IsNullableExpression(notNullableExpression);

            if (isLeftNullable && !isRightNullable)
                return Nullable.GetUnderlyingType(nullableExpression.Type) == notNullableExpression.Type;
            else if (!isLeftNullable && isRightNullable)
                return Nullable.GetUnderlyingType(notNullableExpression.Type) == nullableExpression.Type;

            return nullableExpression.Type == notNullableExpression.Type;
        }

        #endregion Nullable Helpers

        /// <summary>
        /// Extends the Add Expression handler to handle string concatenation
        /// </summary>
        /// <param name="le"></param>
        /// <param name="re"></param>
        /// <returns></returns>
        public static Expression Contains(Expression left, List<Expression> right)
        {
            Expression expression = Expression.Constant(false);
            foreach (var rightExpression in right)
            {
                expression = Expression.Or(expression, Expression.Equal(left, rightExpression));
            }
            return expression;
        }
    }
}