﻿using System;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Expressions
{
    public class TypeRegistry : Dictionary<string, object>
    {
        public TypeRegistry()
        {
            //Add default aliases
            Add("bool", typeof(Boolean));
            Add("byte", typeof(Byte));
            Add("char", typeof(Char));
            Add("int", typeof(Int32));
            Add("decimal", typeof(Decimal));
            Add("double", typeof(Double));
            Add("float", typeof(Single));
            Add("object", typeof(Object));
            Add("string", typeof(String));
        }

        public void RegisterDefaultTypes()
        {
            Add("DateTime", typeof(DateTime));
            Add("Convert", typeof(Convert));
            Add("Math", typeof(Math));
            Add("bool?", typeof(bool?));
            Add("byte?", typeof(byte?));
            Add("char?", typeof(char?));
            Add("int?", typeof(int?));
            Add("decimal?", typeof(decimal?));
            Add("double?", typeof(double?));
            Add("float?", typeof(float?));
            Add("DateTime?", typeof(DateTime?));
        }
    }
}
