﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Establishment request - Api Specification
    /// </summary>

    public class SubmissionResponse
    {
        /// <summary>
        /// Gets or sets the identifier
        /// </summary>
        /// /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the Year Filing For
        /// </summary>
        /// /// <value>
        /// The Year Filing For.
        /// </value>
        [JsonProperty("year_filing_for")]
        public int YearFilingFor { get; set; }

        /// <summary>
        /// Gets or sets the Establishment
        /// </summary>
        /// <value>
        /// Establishment.
        /// </value>
        [JsonProperty("establishment")]
        public SubmissionEstablishment Establishment { get; set; }

        /// <summary>
        /// Gets or sets the Form300a
        /// </summary>
        /// <value>
        /// Form300a.
        /// </value>
        [JsonProperty("form300a")]
        public SubmissionForm300A Form300a { get; set; }

        /// <summary>
        /// Gets or sets the timeStamp.
        /// </summary>
        /// <value>
        /// The TimeStamp.
        /// </value>
        [JsonProperty("time_stamp")]
        public long TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the Created User Name.
        /// </summary>
        /// <value>
        /// The Created User Name.
        /// </value>
        [JsonProperty("created_username")]
        public string CreatedUserName { get; set; }

        /// <summary>
        /// Gets or sets the Link
        /// </summary>
        /// <value>
        /// The links.
        /// </value>
        [JsonProperty("links")]
        public SubmissionResponseLink Links { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        [JsonProperty("errors")]
        public List<string> Errors { get; set; }

        /// <summary>
        /// Gets or sets the warnings.
        /// </summary>
        /// <value>
        /// The warnings.
        /// </value>
        [JsonProperty("warnings")]
        public List<string> Warnings { get; set; }

    }
}