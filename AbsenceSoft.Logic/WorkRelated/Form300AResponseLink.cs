﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Links specification in Form300A response
    /// </summary>
    public class Form300AResponseLink
    {
        /// <summary>
        /// Gets or sets the self Link.
        /// </summary>
        /// <value>
        /// The self Link.
        /// </value>
        [JsonProperty("self")]
        public string Self { get; set; }

        /// <summary>
        /// Gets or sets the establishment link.
        /// </summary>
        /// <value>
        /// The establishment Link.
        /// </value>
        [JsonProperty("establishment")]
        public string Establishment { get; set; }
    }

}
