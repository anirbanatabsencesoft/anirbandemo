﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Data.Customers;
using System.Dynamic;
using System.Text.RegularExpressions;
using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Osha efiling Service
    /// </summary>
    /// <seealso cref="AbsenceSoft.Logic.LogicService" />
    public class OshaEFilingService : LogicService
    {
        readonly Uri baseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrlOshaApi"]);
        string token = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="OshaEFilingService"/> class.
        /// </summary>
        public OshaEFilingService()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OshaEFilingService"/> class.
        /// </summary>
        /// <param name="currentCustomer">The current customer.</param>
        /// <param name="currentEmployer">The current employer.</param>
        /// <param name="currentUser">The current user.</param>
        public OshaEFilingService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }
        /// <summary>
        /// Get Establishment Object type
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns an Establishment Object</returns>
        public Establishment GetEstablishment(Osha300AFormPreview model)
        {
            Establishment establishment = new Establishment();
            establishment.EstablishmentName = model.EstablishmentName;
            establishment.Company = new EstablishmentCompany();
            establishment.Company.CompanyName = model.EmployerName;
            establishment.Address = new EstablishmentAddress();
            establishment.Address.Street = model.StreetAddress;
            establishment.Address.City = model.City;
            establishment.Address.State = model.State;
            establishment.Address.Zip = model.PostalCode;
            establishment.Naics = new EstablishmentIndustry();
            establishment.Naics.NaicsCode = model.NaicsCode;
            establishment.Naics.Year = model.Year;
            establishment.Naics.IndustryDescription = model.IndustryDescription;
            establishment.Size = model.Size;
            if (model.EstablishmentType >= 1 && model.EstablishmentType < 4)
            {
                establishment.EstablishmentType = model.EstablishmentType;
            }
            return establishment;
        }
        /// <summary>
        /// Get Form300AForm  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Form300AForm object type</returns>
        public Form300ARequest Get300AForm(Osha300AFormPreview model)
        {
            Form300ARequest osha300AForm = new Form300ARequest();
            Form300AEstablishment establishment = new Form300AEstablishment();
            establishment.Id = model.EstablishmentId;
            establishment.EstablishmentName = model.EstablishmentName;
            osha300AForm.Establishment = establishment;
            osha300AForm.AnnualAverageEmployees = model.AverageEmployeeCount;
            osha300AForm.TotalHoursWorked = model.TotalHoursWorked;
            osha300AForm.TotalDeaths = model.TotalCasesDeaths;
            osha300AForm.TotalDafwCases = model.TotalCasesWithDaysAwayFromWork;
            osha300AForm.TotalDjtrCases = model.TotalCasesWithDaysTransferRestriction;
            osha300AForm.TotalOtherCases = model.TotalCasesOther;
            osha300AForm.TotalDafwDays = model.NumberOfDays;
            osha300AForm.TotalDjtrDays = model.NumberOfDaysJobRestriction;
            osha300AForm.TotalInjuries = model.TotalInjuries;
            osha300AForm.TotalSkinDisorders = model.TotalSkinDisorder;
            osha300AForm.TotalRespiratoryConditions = model.TotalRespiratory;
            osha300AForm.TotalPoisonings = model.TotalPoisoning;
            osha300AForm.TotalHearingLoss = model.TotalHearingLoss;
            osha300AForm.TotalotherIllnesses = model.TotalAllOtherIllnesses;
            return osha300AForm;
        }

        /// <summary>
        /// Form300A Submission request 
        /// </summary>
        /// <param name="establishmentId"></param>
        /// <param name="year"></param>
        /// <param name="reasonForChange"></param>
        /// <returns>The Request of Form300A Submission</returns>
        public SubmissionRequest Get300AFormSubmission(long establishmentId, int year, string reasonForChange)
        {
            SubmissionRequest submissionRequest = new SubmissionRequest();
            submissionRequest.EstablishmentId = establishmentId;
            submissionRequest.YearFilingFor = year;
            submissionRequest.ChangeReason = reasonForChange;
            return submissionRequest;
        }

        /// <summary>
        /// Save Establishment Information 
        /// </summary>
        /// <param name="model"></param>
        /// <returns> the Response Object of Establishment</returns>
        public async Task<EstablishmentResponse> SaveEstablishment(Osha300AFormPreview model)
        {

            using (var client = new HttpClient())
            {
                EstablishmentResponse establishment = null;
                client.DefaultRequestHeaders.Clear();
                token = CurrentEmployer.OshaApiToken;
                client.DefaultRequestHeaders.Add("Authorization", string.Concat("Bearer ", token));
                var content = new StringContent(JsonConvert.SerializeObject(GetEstablishment(model)), Encoding.UTF8, "application/json");
                // Post Establishment
                HttpResponseMessage responseMessage = await client.PostAsync(string.Concat(baseAddress, "establishments"), content);
                if (responseMessage != null && responseMessage.IsSuccessStatusCode)
                {
                    var response = await client.GetAsync(string.Concat(baseAddress, "establishments"));
                    var responseObj = await response.Content.ReadAsAsync<EstablishmentRootResponse>();
                    establishment = responseObj.Results.Find(x => x.EstablishmentName == model.EstablishmentName);
                    if (establishment.Id <= 0)
                    {
                        string[] errors = establishment.Errors.ToArray();
                        for (int i = 0; i < errors.Length; i++)
                        {
                            Match match = Regex.Match(errors[i], @"(The ID of the existing establishment is)\s+(\d+)");
                            Match matchId = Regex.Match(match.ToString(), @"(\d+)");
                            if (matchId.Length > 0)
                            {
                                establishment.Id = Convert.ToInt32(matchId.Value);
                            }
                        }
                    }
                    return establishment;
                }
            }
            return null;
        }

        /// <summary>
        /// Submit300A Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Json String of Form300A response </returns>
        public async Task<SubmissionResponse> Submit300A(Osha300AFormPreview model)
        {
            using (var client = new HttpClient())
            {
                Form300ARootResponse result300A = null;
                SubmissionResponse finalSubmission = new SubmissionResponse();
                EstablishmentResponse response = null;
                Form300AResponse form300A = null;
                client.DefaultRequestHeaders.Clear();

                if (CurrentEmployer == null)
                {
                    CurrentEmployer = Employer.GetById(model.EmployerId);
                }
                token = CurrentEmployer.OshaApiToken;
                
                if (string.IsNullOrEmpty(token))
                {
                    finalSubmission.Errors = new List<string>() { "E-Filing failed. No Token has been assigned to this employer." };
                    return finalSubmission;
                }

                if (model.AverageEmployeeCount == 0)
                {
                    finalSubmission.Errors = new List<string>() { "Annual average number of employees value can not be 0." };
                    return finalSubmission;
                }
                // Postal code should be a 5 digit or 9 digit number
                if (string.IsNullOrWhiteSpace(model.PostalCode))
                {
                    finalSubmission.Errors = new List<string>() { "Postal code must be a five or nine digit number" };
                    return finalSubmission;
                }
                else
                {
                    if (model.PostalCode.Length == 5 || model.PostalCode.Length == 9)
                    {
                        int i = 0;
                        if (!int.TryParse(model.PostalCode, out i))
                        {
                            finalSubmission.Errors = new List<string>() { "Postal code must be a five or nine digit number" };
                            return finalSubmission;
                        }
                    }
                    else
                    {
                        finalSubmission.Errors = new List<string>() { "Postal code must be a five or nine digit number" };
                        return finalSubmission;
                    }
                }

                if (model.TotalHoursWorked == 0)
                {
                    finalSubmission.Errors = new List<string>() { "Total hours worked by all employees last year value can not be 0." };
                    return finalSubmission;
                }
                //6 digit number
                if (model.NaicsCode < 100000 || model.NaicsCode > 999999)
                {
                    finalSubmission.Errors = new List<string>() { "The NAICS code is not valid." };
                    return finalSubmission;
                }

                client.DefaultRequestHeaders.Add("Authorization", string.Concat("Bearer ", token));

                Organization organization = Organization.GetByCode(model.EstablishmentCode, CurrentCustomer.Id, CurrentEmployer.Id);
                if (organization != null && (!organization.OshaEstablishmentId.HasValue || organization.OshaEstablishmentId == 0))
                {
                    response = await SaveEstablishment(model);
                    if (response != null && response.Id > 0)
                    {
                        organization.OshaEstablishmentId = response.Id;
                        organization.Save();
                    }
                }

                if (organization != null && organization.OshaEstablishmentId > 0)
                {
                    model.EstablishmentId = Convert.ToInt32(organization.OshaEstablishmentId);
                    var content300AFile = new StringContent(JsonConvert.SerializeObject(Get300AForm(model)), Encoding.UTF8, "application/json");
                    HttpResponseMessage responseMessage = await client.PostAsync(string.Concat(baseAddress, "forms/form300A"), content300AFile);

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        result300A = await responseMessage.Content.ReadAsAsync<Form300ARootResponse>();
                        if (result300A != null && result300A.Results != null)
                        {
                            form300A = result300A.Results.Find(result => result.EstablishmentId == model.EstablishmentId);
                            if (form300A == null)
                            {
                                form300A = result300A.Results[0];
                                if (form300A.Errors != null && form300A.Id == 0)
                                {
                                    finalSubmission.Errors = form300A.Errors;
                                    return finalSubmission;
                                }
                            }
                        }
                    }
                }

                if (organization != null && organization.OshaEstablishmentId > 0)
                {
                    var contentSubmission = new StringContent(JsonConvert.SerializeObject(Get300AFormSubmission(model.EstablishmentId, model.Year - 1, "No change")), Encoding.UTF8, "application/json");
                    HttpResponseMessage responseSubmission = await client.PostAsync(string.Concat(baseAddress, "submissions"), contentSubmission);

                    if (responseSubmission.IsSuccessStatusCode)
                    {
                        SubmissionRootResponse submissionResponse = await responseSubmission.Content.ReadAsAsync<SubmissionRootResponse>();
                        finalSubmission = submissionResponse.Results.Find(result => result.Establishment.Id == model.EstablishmentId);

                        if (finalSubmission == null && submissionResponse.Results != null)
                        {
                            finalSubmission = submissionResponse.Results[0];
                        }
                    }
                }
                return finalSubmission;
            }
        }

        /// <summary>
        /// Get Osha token  
        /// </summary>
        /// <param name="employerId"></param>
        /// <returns>Osha Token string</returns>
        public string GetToken(string employerId)
        {
            return CurrentEmployer.OshaApiToken;
        }

    }
}