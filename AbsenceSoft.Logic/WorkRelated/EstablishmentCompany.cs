﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{

    /// <summary>
    /// Company - Api Specification
    /// </summary>
    public class EstablishmentCompany
    {
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }
    }

}
