﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// NAICS Api Specification
    /// </summary>
    public class EstablishmentIndustry
    {
        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        [JsonProperty("naics_code")]
        public int NaicsCode { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        [JsonProperty("year")]
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets the industry description.
        /// </summary>
        /// <value>
        /// The industry description.
        /// </value>
        [JsonProperty("industry_description")]
        public string IndustryDescription { get; set; }
    }
}
