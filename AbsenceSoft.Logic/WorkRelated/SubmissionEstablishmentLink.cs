﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Links specification in Form300A response
    /// </summary>
    public class SubmissionEstablishmentLink
    {
        /// <summary>
        /// Gets or sets the self Link.
        /// </summary>
        /// <value>
        /// The self Link.
        /// </value>
        [JsonProperty("self")]
        public string Self { get; set; }

        /// <summary>
        /// Gets or sets the Form300a links.
        /// </summary>
        /// <value>
        /// The Form300a links
        /// </value>
        [JsonProperty("form300a_links")]
        public List<string> Form300aLinks { get; set; }

        /// <summary>
        /// Gets or sets the Submission links.
        /// </summary>
        /// <value>
        /// The Submission links
        /// </value>
        [JsonProperty("submission_links")]
        public List<string> SubmissionLinks { get; set; }
    }

}
