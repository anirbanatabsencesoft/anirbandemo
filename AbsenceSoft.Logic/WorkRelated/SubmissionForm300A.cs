﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Form300AResponseObject
    /// </summary>
    public class SubmissionForm300A
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the establishment identifier.
        /// </summary>
        /// <value>
        /// The establishment identifier.
        /// </value>
        [JsonProperty("establishment_id")]
        public int EstablishmentId { get; set; }

        /// <summary>
        /// Gets or sets the YearFilingfor.
        /// </summary>
        /// <value>
        /// YearFilingfor
        /// </value>
        [JsonProperty("year_filing_for")]
        public int YearFilingFor { get; set; }

        /// <summary>
        /// Gets or sets the annual average employees.
        /// </summary>
        /// <value>
        /// The annual average employees.
        /// </value>
        [JsonProperty("annual_average_employees")]
        public int AnnualAverageEmployees { get; set; }

        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        [JsonProperty("total_hours_worked")]
        public int TotalHoursWorked { get; set; }

        /// <summary>
        /// Gets or sets the no injuries.
        /// </summary>
        /// <value>
        /// The no injuries.
        /// </value>
        [JsonProperty("no_injuries_illnesses")]
        public int NoInjuriesIllnesses { get; set; }

        /// <summary>
        /// Gets or sets the Total number of deaths.
        /// </summary>
        /// <value>
        /// The Total number of deaths.
        /// </value>
        [JsonProperty("total_deaths")]
        public int TotalDeaths { get; set; }

        /// <summary>
        /// Gets or sets the Total number of cases with days away from work.
        /// </summary>
        /// <value>
        /// The Total number of cases with days away from work 
        /// </value>
        [JsonProperty("total_dafw_cases")]
        public int TotalDafwCases { get; set; }

        /// <summary>
        /// Gets or sets the Total number of cases with job transfer or restriction.
        /// </summary>
        /// <value>
        /// The Total number of cases with job transfer or restriction.
        /// </value>
        [JsonProperty("total_djtr_cases")]
        public int TotalDjtrCases { get; set; }

        /// <summary>
        /// Gets or sets the Total number of other recordable cases.
        /// </summary>
        /// <value>
        /// The Total number of other recordable cases.
        /// </value>
        [JsonProperty("total_other_cases")]
        public int TotalOtherCases { get; set; }

        /// <summary>
        /// Gets or sets the Total number of days away from work.
        /// </summary>
        /// <value>
        /// The Total number of days away from work.
        /// </value>
        [JsonProperty("total_dafw_days")]
        public int TotalDafwDays { get; set; }

        /// <summary>
        /// Gets or sets the Total number of days of job transfer or restriction.
        /// </summary>
        /// <value>
        /// The Total number of days of job transfer or restriction.
        /// </value>
        [JsonProperty("total_djtr_days")]
        public int TotalDjtrDays { get; set; }

        /// <summary>
        /// Gets or sets the Total number of Injuries.
        /// </summary>
        /// <value>
        /// The Total number of Injuries.
        /// </value>
        [JsonProperty("total_injuries")]
        public int TotalInjuries { get; set; }

        /// <summary>
        /// Gets or sets the total skin disorders.
        /// </summary>
        /// <value>
        /// The total skin disorders.
        /// </value>
        [JsonProperty("total_skin_disorders")]
        public int TotalSkinDisorders { get; set; }

        /// <summary>
        /// Gets or sets the total respiratory conditions.
        /// </summary>
        /// <value>
        /// The total respiratory conditions.
        /// </value>
        [JsonProperty("total_respiratory_conditions")]
        public int TotalRespiratoryConditions { get; set; }

        /// <summary>
        /// Gets or sets the total poisonings.
        /// </summary>
        /// <value>
        /// The total poisonings.
        /// </value>
        [JsonProperty("total_poisonings")]
        public int TotalPoisonings { get; set; }
        /// <summary>
        /// Gets or sets the total hearing loss.
        /// </summary>
        /// <value>
        /// The total hearing loss.
        /// </value>
        [JsonProperty("total_hearing_loss")]
        public int TotalHearingLoss { get; set; }

        /// <summary>
        /// Gets or sets the total other illnesses.
        /// </summary>
        /// <value>
        /// The total other illnesses.
        /// </value>
        [JsonProperty("total_other_illnesses")]
        public int TotalotherIllnesses { get; set; }

        /// <summary>
        /// Gets or sets the created timestamp.
        /// </summary>
        /// <value>
        /// The created timestamp.
        /// </value>
        [JsonProperty("created_timestamp")]
        public long CreatedTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the last edited timestamp.
        /// </summary>
        /// <value>
        /// The last edited timestamp.
        /// </value>
        [JsonProperty("last_edited_timestamp")]
        public long LastEditedTimestamp { get; set; }

        /// <summary>
        /// Gets or sets tcr.
        /// </summary>
        /// <value>
        /// The tcr.
        /// </value>
        [JsonProperty("tcr")]
        public int Tcr { get; set; }

        /// <summary>
        /// Gets or sets dart.
        /// </summary>
        /// <value>
        /// dart.
        /// </value>
        [JsonProperty("dart")]
        public int Dart { get; set; }

        /// <summary>
        /// Gets or sets Links.
        /// </summary>
        /// <value>
        /// Links.
        /// </value>
        [JsonProperty("links")]
        public Form300AResponseLink Links { get; set; }

        /// <summary>
        /// Gets or sets Links.
        /// </summary>
        /// <value>
        /// Links.
        /// </value>
        [JsonProperty("submission_status")]
        public int submission_status { get; set; }

    }

}
