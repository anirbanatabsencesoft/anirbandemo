﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Establishment request - Api Specification
    /// </summary>

    public class Establishment
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the establishment.
        /// </summary>
        /// <value>
        /// The name of the establishment.
        /// </value>
        [JsonProperty("establishment_name")]
        public string EstablishmentName { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        [JsonProperty("size")]
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        /// <summary>
        [JsonProperty("address")]
        public EstablishmentAddress Address { get; set; }

        /// <summary>
        /// Gets or sets the naics.
        /// </summary>
        /// <value>
        /// The naics.
        /// </value>
        [JsonProperty("naics")]
        public EstablishmentIndustry Naics { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>
        /// The company.
        /// </value>
        [JsonProperty("company")]
        public EstablishmentCompany Company { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the establishment.
        /// </summary>
        /// <value>
        /// The type of the establishment.
        /// </value>
        [JsonProperty("establishment_type")]
        public int? EstablishmentType { get; set; }

    }
}