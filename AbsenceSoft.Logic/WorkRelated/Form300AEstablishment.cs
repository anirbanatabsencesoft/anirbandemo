﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Establishment Form 300A - Api Specification
    /// </summary>

    public class Form300AEstablishment
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the establishment.
        /// </summary>
        /// <value>
        /// The name of the establishment.
        /// </value>
        [JsonProperty("establishment_name")]
        public string EstablishmentName { get; set; }

    }
}