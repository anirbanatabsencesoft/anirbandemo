﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Links specification in Form300A response
    /// </summary>
    public class EstablishmentResponseLink
    {
        /// <summary>
        /// Gets or sets the self Link.
        /// </summary>
        /// <value>
        /// The self Link.
        /// </value>
        [JsonProperty("self")]
        public string Self { get; set; }

    }

}
