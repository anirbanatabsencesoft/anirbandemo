﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Establishment Submission Response - Api Specification
    /// </summary>

    public class SubmissionEstablishment
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the establishment.
        /// </summary>
        /// <value>
        /// The name of the establishment.
        /// </value>
        [JsonProperty("establishment_name")]
        public string EstablishmentName { get; set; }

        ///// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        [JsonProperty("size")]
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        /// <summary>
        [JsonProperty("address")]
        public EstablishmentAddress Address { get; set; }

        /// <summary>
        /// Gets or sets the naics.
        /// </summary>
        /// <value>
        /// The naics.
        /// </value>
        [JsonProperty("naics")]
        public EstablishmentIndustry Naics { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>
        /// The company.
        /// </value>
        [JsonProperty("company")]
        public EstablishmentCompany Company { get; set; }

        /// <summary>
        /// Gets or sets the Years Submitted.
        /// </summary>
        /// <value>
        /// YearsSubmitted.
        /// </value>
        [JsonProperty("years_submitted")]
        public List<Int32> YearsSubmitted { get; set; }

        /// <summary>
        /// Gets or sets the created timestamp.
        /// </summary>
        /// <value>
        /// The created timestamp.
        /// </value>
        [JsonProperty("created_timestamp")]
        public long CreatedTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the last edited timestamp.
        /// </summary>
        /// <value>
        /// The last edited timestamp.
        /// </value>
        [JsonProperty("last_edited_timestamp")]
        public long LastEditedTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the submission status.
        /// </summary>
        /// <value>
        /// The submission status.
        /// </value>
        [JsonProperty("submission_status")]
        public int SubmissionStatus { get; set; }

        /// <summary>
        /// Gets or sets the establishment status.
        /// </summary>
        /// <value>
        /// The establishment status.
        /// </value>
        [JsonProperty("establishment_status")]
        public int EstablishmentStatus { get; set; }

        /// <summary>
        /// Gets or sets the Link
        /// </summary>
        /// <value>
        /// The links.
        /// </value>
        [JsonProperty("links")]
        public SubmissionEstablishmentLink Links { get; set; }
    }
}