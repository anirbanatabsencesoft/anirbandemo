﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Main Response object specification
    /// </summary>
    public class Form300ARootResponse
    {
        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        /// <value>
        /// The results.
        /// </value>
        [JsonProperty("results")]
        public List<Form300AResponse> Results { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Form300ARootResponse"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}
