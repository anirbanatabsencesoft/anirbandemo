﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Form300A Request
    /// </summary>
    public class Form300ARequest
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the establishment.
        /// </summary>
        /// <value>
        /// The establishment.
        /// </value>
        [JsonProperty("establishment")]
        public Form300AEstablishment Establishment { get; set; }

        /// <summary>
        /// Gets or sets the annual average employees.
        /// </summary>
        /// <value>
        /// The annual average employees.
        /// </value>
        [JsonProperty("annual_average_employees")]
        public int AnnualAverageEmployees { get; set; }

        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        [JsonProperty("total_hours_worked")]
        public decimal TotalHoursWorked { get; set; }

        /// <summary>
        /// Gets or sets the number of injuries/illnesses.
        /// </summary>
        /// <value>
        /// The no of injuries.
        /// </value>
        [JsonProperty("no_injuries_illnesses")]
        public int NoInjuriesIllnesses { get; set; }


        /// <summary>
        /// Gets or sets the total no of deaths.
        /// </summary>
        /// <value>
        /// The total no of deaths.
        /// </value>
        [JsonProperty("total_deaths")]
        public int TotalDeaths { get; set; }

        /// <summary>
        /// Gets or sets the total  cases.
        /// </summary>
        /// <value>
        /// The total dafw cases.
        /// </value>
        [JsonProperty("total_dafw_cases")]
        public int TotalDafwCases { get; set; }

        /// <summary>
        /// Gets or sets the Total number of cases with job transfer or restriction.
        /// </summary>
        /// <value>
        /// The Total number of cases with job transfer or restriction.
        /// </value>
        [JsonProperty("total_djtr_cases")]
        public int TotalDjtrCases { get; set; }

        /// <summary>
        /// Gets or sets the total other cases.
        /// </summary>
        /// <value>
        /// The total other cases.
        /// </value>
        [JsonProperty("total_other_cases")]
        public int TotalOtherCases { get; set; }

        /// <summary>
        /// Gets or sets Total number of days away from work.
        /// </summary>
        /// <value>
        /// The Total number of days away from work.
        /// </value>
        [JsonProperty("total_dafw_days")]
        public int TotalDafwDays { get; set; }

        /// <summary>
        /// Gets or sets Total number of days of job transfer or restriction.
        /// </summary>
        /// <value>
        /// The Total number of days of job transfer or restriction.
        /// </value>
        [JsonProperty("total_djtr_days")]
        public int TotalDjtrDays { get; set; }

        /// <summary>
        /// Gets or sets the total injuries.
        /// </summary>
        /// <value>
        /// The total injuries.
        /// </value>
        [JsonProperty("total_injuries")]
        public int TotalInjuries { get; set; }

        /// <summary>
        /// Gets or sets the total skin disorders.
        /// </summary>
        /// <value>
        /// The total skin disorders.
        /// </value>
        [JsonProperty("total_skin_disorders")]
        public int TotalSkinDisorders { get; set; }

        /// <summary>
        /// Gets or sets the total respiratory conditions.
        /// </summary>
        /// <value>
        /// The total respiratory conditions.
        /// </value>
        [JsonProperty("total_respiratory_conditions")]
        public int TotalRespiratoryConditions { get; set; }

        /// <summary>
        /// Gets or sets the total poisonings.
        /// </summary>
        /// <value>
        /// The total poisonings.
        /// </value>
        [JsonProperty("total_poisonings")]
        public int TotalPoisonings { get; set; }

        /// <summary>
        /// Gets or sets the total hearing loss.
        /// </summary>
        /// <value>
        /// The total hearing loss.
        /// </value>
        [JsonProperty("total_hearing_loss")]
        public int TotalHearingLoss { get; set; }

        /// <summary>
        /// Gets or sets the total other illnesses.
        /// </summary>
        /// <value>
        /// The total other illnesses.
        /// </value>
        [JsonProperty("total_other_illnesses")]
        public int TotalotherIllnesses { get; set; }
    }
}
