﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// OSHA 300A Form Preview 
    /// </summary>
    public class Osha300AFormPreview
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Osha300AFormPreview"/> class.
        /// </summary>
        public Osha300AFormPreview()
        {

        }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the employer.
        /// </summary>
        /// <value>
        /// The name of the employer.
        /// </value>
        public string EmployerName { get; set; }
        
        /// <summary>
        /// Gets or sets the total no of death cases.
        /// </summary>
        /// <value>
        /// The total no of death cases.
        /// </value>
        public int TotalCasesDeaths { get; set; }
        
        /// <summary>
        /// Gets or sets the total cases with days away from work.
        /// </summary>
        /// <value>
        /// The total cases with days away from work.
        /// </value>
        public int TotalCasesWithDaysAwayFromWork { get; set; }
        
        /// <summary>
        /// Gets or sets the total cases with days transfer restriction.
        /// </summary>
        /// <value>
        /// The total cases with days transfer restriction.
        /// </value>
        public int TotalCasesWithDaysTransferRestriction { get; set; }
        
        /// <summary>
        /// Gets or sets the total other cases.
        /// </summary>
        /// <value>
        /// The total other cases.
        /// </value>
        public int TotalCasesOther { get; set; }

        /// <summary>
        /// Gets or sets the number of days away from work.
        /// </summary>
        /// <value>
        /// The number of days away from work.
        /// </value>
        public int NumberOfDays { get; set; }
        
        /// <summary>
        /// Gets or sets the number of days job restriction.
        /// </summary>
        /// <value>
        /// The number of days job restriction.
        /// </value>
        public int NumberOfDaysJobRestriction { get; set; }
        
        /// <summary>
        /// Gets or sets the total no of injuries.
        /// </summary>
        /// <value>
        /// The total no of injuries.
        /// </value>
        public int TotalInjuries { get; set; }

        /// <summary>
        /// Gets or sets the total cases of skin disorder.
        /// </summary>
        /// <value>
        /// The total cases of skin disorder.
        /// </value>
        public int TotalSkinDisorder { get; set; }
        
        /// <summary>
        /// Gets or sets the total respiratory cases.
        /// </summary>
        /// <value>
        /// The total respiratory cases.
        /// </value>
        public int TotalRespiratory { get; set; }
        
        /// <summary>
        /// Gets or sets the total poisoning cases.
        /// </summary>
        /// <value>
        /// The total poisoning cases.
        /// </value>
        public int TotalPoisoning { get; set; }
        
        /// <summary>
        /// Gets or sets the total hearing loss cases.
        /// </summary>
        /// <value>
        /// The total hearing loss cases.
        /// </value>
        public int TotalHearingLoss { get; set; }
        
        /// <summary>
        /// Gets or sets the total all other illnesses.
        /// </summary>
        /// <value>
        /// The total all other illnesses.
        /// </value>
        public int TotalAllOtherIllnesses { get; set; }

        /// <summary>
        /// Gets or sets the establishment/Organization code.
        /// </summary>
        /// <value>
        /// The establishment/Organization code.
        /// </value>
        public string EstablishmentCode { get; set; }
        
        /// <summary>
        /// Gets or sets the establishment type code.
        /// </summary>
        /// <value>
        /// The establishment type code. i.e Office Location
        /// </value>
        public string EstablishmentTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the establishment identifier.
        /// </summary>
        /// <value>
        /// The establishment identifier.
        /// </value>
        public int EstablishmentId { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the establishment.
        /// </summary>
        /// <value>
        /// The name of the establishment.
        /// </value>
        public string EstablishmentName { get; set; }
        
        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>
        /// The street address.
        /// </value>
        public string StreetAddress { get; set; }
        
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }
        
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }
        
        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }
        
        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        public int NaicsCode { get; set; }
        
        /// <summary>
        /// Gets or sets the sic code.
        /// </summary>
        /// <value>
        /// The sic code.
        /// </value>
        public string SicCode { get; set; }
        
        /// <summary>
        /// Gets or sets the industry description.
        /// </summary>
        /// <value>
        /// The industry description.
        /// </value>
        public string IndustryDescription { get; set; }
        
        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public int Size { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the establishment.
        /// </summary>
        /// <value>
        /// The type of the establishment.
        /// </value>
        public int? EstablishmentType { get; set; }
        
        /// <summary>
        /// Gets or sets the average employee count.
        /// </summary>
        /// <value>
        /// The average employee count.
        /// </value>
        public int AverageEmployeeCount { get; set; }
        
        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        public decimal TotalHoursWorked { get; set; }
        
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public int Year { get; set; }
    }
}
