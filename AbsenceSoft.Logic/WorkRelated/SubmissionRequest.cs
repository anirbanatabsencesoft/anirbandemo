﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AbsenceSoft.Logic.WorkRelated
{
    /// <summary>
    /// Establishment request - Api Specification
    /// </summary>

    public class SubmissionRequest
    {
        /// <summary>
        /// Gets or sets the Id of the establishment
        /// </summary>
        /// /// <value>
        /// The Id of the establishment.
        /// </value>
        [JsonProperty("establishment_id")]
        public long EstablishmentId { get; set; }

        /// <summary>
        /// Gets or sets the Year Filing For
        /// </summary>
        /// /// <value>
        /// The Year Filing For.
        /// </value>
        [JsonProperty("year_filing_for")]
        public int YearFilingFor { get; set; }

        /// <summary>
        /// Gets or sets the Change Reason
        /// </summary>
        /// /// <value>
        /// The Change Reason
        /// </value>
        [JsonProperty("change_reason")]
        public string ChangeReason { get; set; }

    }
}