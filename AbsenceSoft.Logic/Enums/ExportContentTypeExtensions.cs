﻿using AbsenceSoft.Data.Enums;
using System;

namespace AbsenceSoft.Logic.Enums
{
    public static class ExportContentTypeExtensions
    {
        public static string GetExportContentType(CaseExportFormat format)
        {
            switch (format)
            {
                case CaseExportFormat.Pdf:
                    return "application/pdf";
                case CaseExportFormat.Zip:
                    return "application/zip";
                default:
                    return string.Empty;
            }
        }   
    }
}
