﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Security
{
    public class CreateAccountMessageData : BaseNonEntity
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string SiteUrl { get; set; }

    }
}
