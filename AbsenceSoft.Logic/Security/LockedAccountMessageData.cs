﻿using AbsenceSoft.Data;
using System;

namespace AbsenceSoft.Logic.Security
{
    [Serializable]
    class LockedAccountMessageData : BaseNonEntity
    {
        public LockedAccountMessageData() { }

        public string FirstName { get; set; }        

        public string CustomerSiteUrl { get; set; }

        public string SiteUrl { get; set; }

        public string ResetKey { get; set; }

        public string IpAddress { get; set; }
    }
}
