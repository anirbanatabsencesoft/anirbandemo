﻿using AbsenceSoft.Data;

namespace AbsenceSoft.Logic.Security
{
	public class EmployeeValidateEmailMesssageData: BaseNonEntity
	{
		public string Email { get; set; }

		public string Name { get; set; }

		public string ResetKey { get; set; }

		public string CustomerSiteUrl { get; set; }
	}
}
