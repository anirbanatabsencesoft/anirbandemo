﻿using AbsenceSoft.Data;
using System;

namespace AbsenceSoft.Logic.Security
{
    [Serializable]
    public class SelfAccountCreationMessageData : BaseNonEntity
    {
        public SelfAccountCreationMessageData() { }

        public string FirstName { get; set; }

        public string EmailBody { get; set; }

        public string CustomerSiteUrl { get; set; }

        public string SiteUrl { get; set; }

        public string VerificationKey { get; set; }

        public string CreateAccountLinkText { get; set; }
    }
}
