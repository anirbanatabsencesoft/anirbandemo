﻿using AbsenceSoft.Data;
using System;

namespace AbsenceSoft.Logic.Security
{
    [Serializable]
    public class ForgotPasswordMessageData : BaseNonEntity
    {
        public ForgotPasswordMessageData() { }

        public string FirstName { get; set; }

        public string CustomerSiteUrl { get; set; }

        public string SiteUrl { get; set; }

        public string ResetKey { get; set; }
    }
}
