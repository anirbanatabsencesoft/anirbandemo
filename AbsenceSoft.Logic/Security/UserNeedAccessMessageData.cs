﻿
namespace AbsenceSoft.Logic.Security
{
    public class UserNeedAccessMessageData
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        //public string BillingAddress1 { get; set; }
        //public string BillingAddress2 { get; set; }
        //public string BillingCountry { get; set; }
        //public string BillingCity { get; set; }
        //public string BillingState { get; set; }
        //public string BillingPostalCode { get; set; }

        public string PhoneNumber { get; set; }

        //public string Size { get; set; }

        public string SiteUrl { get; set; }

        public string ResetKey { get; set; }

        public string CustomerId { get; set; }
    }
}
