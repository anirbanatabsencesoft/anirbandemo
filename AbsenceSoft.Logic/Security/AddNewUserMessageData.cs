﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Security
{
    public class AddNewUserMessageData : BaseNonEntity
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string SiteUrl { get; set; }       

        public string UserName { get; set; }

        public string Password { get; set;}

        public string ResetKey { get; set; }

        public string CustomerSiteUrl { get; set; }

    }
}
