﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Common.Security;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Common;
using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using AbsenceSoft.Common.Properties;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using MongoDB.Driver;
using System.Web.Security;
using System.Threading;
using Newtonsoft.Json;
using MongoDB.Bson;
using System.Text;
using MongoDB.Driver.Builders;
using AT.Common.Core.NotificationEnums;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Notification;
using System.Security.Claims;
using AT.Common.Core;
using AbsenceSoft.Data.Communications;
using System.IO;
using System.Configuration;

namespace AbsenceSoft.Logic.Security
{
    /// <summary>
    /// The Authentication service provides methods for logging in and authenticating users, 
    /// logout, recording user activity, validating password complexity, resetting forgotten 
    /// passwords, etc. These are typical authentication and user specific actions relating 
    /// to authentication and security.
    /// </summary>
    public class AuthenticationService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationService"/> class.
        /// </summary>
        public AuthenticationService() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationService"/> class.
        /// </summary>
        /// <param name="currentEmployer">The current employer.</param>
        public AuthenticationService(Employer currentEmployer)
        {
            this.CurrentEmployer = currentEmployer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public AuthenticationService(User currentUser)
        {
            this.CurrentUser = currentUser;
        }

        /// <summary>
        /// Creates a new user account but does not save it yet. Use update user for that purpose.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public User CreateUser(string email, string password, string firstName, string lastName, params string[] roles)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email", "Email address is required to create a user");

            //Password validation removed per Mohan  8/8/14 RC            
            //if (string.IsNullOrWhiteSpace(password))
            //    throw new ArgumentNullException("password", "Password is required to create a user");
            //if (!ValidatePasswordComplexity(password))
            //    throw new ArgumentException("Password strength does not meet the minimum complexity", "password");

            string loweredEmail = email.ToLowerInvariant();

            if (User.Query.Find(User.Query.Where(u => u.Email == loweredEmail && !u.IsDeleted)).Any())
                throw new ArgumentException("An account for this email address already exists, please contact AbsenceSoft if you have any questions with this account.");

            return new User()
            {
                Email = loweredEmail,
                Password = CryptoString.GetPlainText(password),
                FirstName = firstName,
                LastName = lastName,
                LastActivityDate = DateTime.UtcNow,
                Roles = roles.ToList(),
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId
            };
        }

        public void UpdateUser(User user)
        {
            ValidateUser(user);
            user.Save();
        }

        public void CreateAccount(string verificationKey, string password)
        {
            using (new InstrumentationContext("AuthenticationService.ResetPassword"))
            {
                if (string.IsNullOrWhiteSpace(verificationKey))
                    throw new ArgumentNullException("resetKey");

                if (string.IsNullOrWhiteSpace(password))
                    throw new ArgumentNullException("newPassword");

                string loweredKey = verificationKey.ToLowerInvariant();

                SelfRegistrationRequest userRequest = SelfRegistrationRequest.AsQueryable().FirstOrDefault(c => c.VerificationKey == loweredKey);

                // If this doesn't match a request, or the request was deleted, or the request was made over an hour ago
                if (userRequest == null || userRequest.IsDeleted || userRequest.CreatedDate < DateTime.UtcNow.AddHours(-1))
                    throw new AbsensesoftAuthenticationError();

                //find the employee by email address
                Employee employee = Employee.AsQueryable().FirstOrDefault(e => e.Info.Email == userRequest.Email.ToLowerInvariant());
                if (employee == null)
                    throw new AbsensesoftAuthenticationError();

                var user = CreateUser(employee.Info.Email, password, employee.FirstName, employee.LastName);
                user.CustomerId = employee.CustomerId;

                user.UserType = UserType.SelfService;
                user.Employers.Add(new EmployerAccess { EmployerId = employee.EmployerId });

                AutoAssignEmployeeSelfServiceRole(user, employee.EmployerId);

                // User account is created - we'll soft delete the request now
                userRequest.Delete();

                // Save the user account
                user.Save();
            }
        }

        public List<Role> GetRoles(string customerId, bool includeSysAdmin = true)
        {
            List<Role> roles = new List<Role>();
            if (includeSysAdmin)
                roles.Add(Role.SystemAdministrator);         
            if (Customer.GetById(customerId).HasFeature(Feature.EmployeeSelfService))
            {
                roles.AddRange(Role.AsQueryable().Where(r => r.CustomerId == customerId).ToList());
            }
            else
            {
                roles.AddRange(Role.AsQueryable().Where(r => r.CustomerId == customerId && r.Type != RoleType.SelfService).ToList());
            }    
            return roles;
        }

        /// <summary>
        /// Get Employee Self Service Roles for specific customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<Role> GetEmployeeSelfServiceRoles(string customerId)
        {
            if (string.IsNullOrWhiteSpace(customerId))
                throw new ArgumentNullException("Customer account could not be located");

            List<Role> roles = new List<Role>();

            if (User.IsEmployeeSelfServicePortal)
                roles.AddRange(Role.AsQueryable().Where(r => r.CustomerId == customerId && r.Type == RoleType.SelfService).ToList());
            else if (Customer.GetById(customerId).HasFeature(Feature.ESSDataVisibilityInPortal))
                roles.AddRange(Role.AsQueryable().Where(r => r.CustomerId == customerId && r.Type == RoleType.Portal).ToList());

            return roles;
        }

        /// <summary>
        /// Get Employee Self Service Role for specific customer by Role name
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public Role GetRoleByName(string customerId, string roleName)
        {
            if (string.IsNullOrWhiteSpace(customerId))
                throw new ArgumentNullException("customerId is null");

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException("roleName is null");

            return Role.AsQueryable().Where(r => r.CustomerId == customerId).ToList().Where(r => r.Name.ToLowerInvariant() == roleName.ToLowerInvariant()).FirstOrDefault();
        }

        public bool CreateAccountMail(User user)
        {

            // Get our create account template
            string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_CreateAccount);

            body = Rendering.Template.RenderTemplate(body, new CreateAccountMessageData()
            {
                Email = user.Email,
                Name = user.FirstName + " " + user.LastName,
                SiteUrl = GetRootPath()
            });

            using (MailMessage msg = new MailMessage())
            {
                if (!string.IsNullOrWhiteSpace(Settings.Default.CreateAccountFrom))
                    msg.From = new MailAddress(Settings.Default.CreateAccountFrom);
                msg.To.Add(user.Email);
                msg.Bcc.Add(AbsenceSoft.Common.Properties.Settings.Default.CreateAccountMailBccList);

                msg.Subject = "Thank you registering with AbsenceTracker";
                msg.Body = body;
                msg.IsBodyHtml = true;

                return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
            }
        }

        public bool UserNeedAccessMail(User user)
        {
            // Generate reset key and set in user table
            var resetKey = GeneratePasswordResetKey(user);
            user.ResetKey = resetKey;
            user.ResetKeyDate = DateTime.Now;
            user.Save();

            // Get our create account template
            string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_UserNeedAccess);

            body = Rendering.Template.RenderTemplate(body, new UserNeedAccessMessageData()
            {
                Name = user.FirstName + " " + user.LastName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CompanyName = user.Customer.Name,
                Address1 = user.Customer.Contact.Address.Address1,
                Address2 = user.Customer.Contact.Address.Address2,
                City = user.Customer.Contact.Address.City,
                State = user.Customer.Contact.Address.State,
                PostalCode = user.Customer.Contact.Address.PostalCode,
                Country = user.Customer.Contact.Address.Country,
                PhoneNumber = user.Customer.Contact.WorkPhone,
                Email = user.Email,
                SiteUrl = GetRootPath(),
                ResetKey = resetKey,
                CustomerId = user.CustomerId
            });

            using (MailMessage msg = new MailMessage())
            {
                msg.To.Add("contact@absencesoft.com, sales@absencesoft.com");
                msg.Subject = "User Needs Access to AbsenceTracker";
                msg.Body = body;
                msg.IsBodyHtml = true;

                return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
            }
        }

        public bool AddNewUserMail(User user)
        {
            // Generate reset key and set in user table
            var resetKey = GeneratePasswordResetKey(user);
            user.ResetKey = resetKey;
            user.ResetKeyDate = DateTime.Now;
            user.Save();

            string body;
            AddNewUserMessageData addNewUserMessageData = new AddNewUserMessageData()
            {
                Email = user.Email,
                SiteUrl = GetRootPath(),
                CustomerSiteUrl = user.Customer != null ? user.Customer.GetBaseUrl() : GetRootPath(),
                Name = user.FirstName + " " + user.LastName,
                ResetKey = resetKey
            };

            string templateCode = user.Customer.NewUserEmailTemplateCode;
            if (!string.IsNullOrWhiteSpace(templateCode))
            {
                Template template = Template.GetByCode(templateCode, user.CustomerId);
                Stream docStream = template.Document.DownloadStream();
                body = AbsenceSoft.Rendering.Template.RenderTemplateToHTML(docStream, addNewUserMessageData);
            }
            else
            {
                // Get our create account template
                body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_AddNewUser);
                body = Rendering.Template.RenderTemplate(body, addNewUserMessageData);
            }

            using (MailMessage msg = new MailMessage())
            {
                if (!string.IsNullOrWhiteSpace(Settings.Default.CreateAccountFrom))
                    msg.From = new MailAddress(Settings.Default.CreateAccountFrom);
                msg.To.Add(user.Email);
                msg.Bcc.Add("sales@absencesoft.com");
                msg.Subject = "Welcome to AbsenceTracker";
                msg.Body = body;
                msg.IsBodyHtml = true;
                return msg.NotifyAsync(CurrentUser, Utilities.GetTokenFromClaims(), NotificationType.Email, "", null).GetAwaiter().GetResult();
            }
        }

        public void ValidateUser(User user)
        {
            if (string.IsNullOrWhiteSpace(user.Email))
                throw new ArgumentNullException("user.Email", "Email address is required to create a user");

            //Password validation removed per Mohan  8/8/14 RC
            //if (user.Password == null)
            //    throw new ArgumentNullException("user.Password", "Email address is required to create a user");

            //if (!string.IsNullOrWhiteSpace(user.Password.PlainText) && !ValidatePasswordComplexity(user.Password.PlainText))
            //    throw new ArgumentException("Password strength does not meet the minimum complexity", "user.Password");

            string loweredEmail = user.Email.ToLowerInvariant();
            if (User.AsQueryable().Where(u => u.Email == loweredEmail && u.Id != user.Id).Any())
                throw new ArgumentException("An account for this email address already exists, please use this existing account or the forgot password feature to log in", "email");

            if (user.Roles == null)
                user.Roles = new List<string>(0);

            if (user.Employers == null)
                user.Employers = new List<EmployerAccess>();

            if (user.Employers.GroupBy(e => e.EmployerId).Where(g => g.Count() > 1).Any())
                throw new AbsenceSoftException("The user contains duplicate employers. Please ensure there is exactly 0 or 1 Employer Access records per Employer this user has access to");

            if (user.Employers.Any(e => e.EffectiveDate.HasValue && e.ExpireDate.HasValue && e.EffectiveDate > e.ExpireDate))
                throw new AbsenceSoftException("The user contains an Employer Access record that expires before it is effective; all expiration dates must fall after any effective date if set");
        }

        public LoginResult Login(string email, string password, out User user, UserType userType = UserType.Portal)
        {
            using (new InstrumentationContext("AuthenticationService.Login"))
            {
                // 1. Get user by email address                
                // 2. Hash password							  
                // 3. Compare password hashes and validate
                // 4. If valid, return the user, otherwise null
                // 5. In the controller, create an authentication ticket for this user
                user = User.AsQueryable().Where(c => c.Email == email.ToLowerInvariant() && !c.IsDeleted).FirstOrDefault();
                if (!UserRecordIsValid(user, userType))
                    return LoginResult.Error;

                if (user.Password.Hashed.CompareHash(password) && !user.IsLocked)
                {
                    if (IsExpired(user))
                    {
                        user.MustChangePassword = true;
                        user.Save();
                        return LoginResult.Expired;
                    }
                    //For ESS login auto-asign ESS role (Supervisor/Human Resource)
                    if (User.IsEmployeeSelfServicePortal)
                    {
                        if (user.Must(UserFlag.CompleteESSAssociation) || user.Must(UserFlag.CompleteESSRegistration))
                            return LoginResult.Error;

                        SetCurrentEmployerByUser(user);

                        if (CurrentEmployer != null && !AutoAssignEmployeeSelfServiceRole(user, CurrentEmployer.Id))
                            return LoginResult.Error;
                    }
                    else if (!EmployeeSelfServiceRoleAssignedIfNecessary(user))
                        return LoginResult.Error;

                    // If the user's customer record has expired, prevent them from logging on.
                    if (!CustomerRecordIsValid(user))
                        return LoginResult.Error;
                    else
                    {
                        if (user.IsDisabled)
                            return LoginResult.Disabled;
                        else
                        {
                            return LoginResult.Ok;
                        }
                    }
                }
                else
                {
                    //User login faled
                    if (!user.IsLocked)
                        ProcessFailedLoginAttempt(user);

                    //Validate if user has been locked
                    if (!user.IsLocked)
                        return LoginResult.Error;
                    else
                        return LoginResult.Locked;
                }
            }
        }

        /// <summary>
        /// Validates the employee self service login with UserFlag and auto assign self service login.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>True if valid otherwise False</returns>
        public bool ValidEmployeeSelfServiceLogin(User user)
        {
            //For ESS login auto-asign ESS role (Supervisor/Human Resource)
            if (User.IsEmployeeSelfServicePortal)
            {
                if (user.Must(UserFlag.CompleteESSAssociation) || user.Must(UserFlag.CompleteESSRegistration))
                    return false;

                SetCurrentEmployerByUser(user);

                if (CurrentEmployer != null && !AutoAssignEmployeeSelfServiceRole(user, CurrentEmployer.Id))
                    return false;
            }
            else if (!EmployeeSelfServiceRoleAssignedIfNecessary(user))
                return false;

            return true;
        }

        private bool UserRecordIsValid(User user, UserType userType)
        {
            if (user == null)
                return false;

            if (user.IsDeleted)
                return false;

            if (userType == UserType.Admin && user.UserType != userType)
                return false;

            if (user.UserType != UserType.Admin && user.Customer == null)
                return false;

            return true;
        }

        /// <summary>
        /// Returns true if the AutoAssignEmployeeSelfServiceRole succeeded, or if it didn't need to be called
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool EmployeeSelfServiceRoleAssignedIfNecessary(User user)
        {
            if (user.Customer == null)
                return true;

            if (!user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal))
                return true;

            return AutoAssignEmployeeSelfServiceRole(user, Employer.CurrentId);
        }

        /// <summary>
        /// Returns true if the customer record is valid
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool CustomerRecordIsValid(User user)
        {
            if (user.UserType == UserType.Admin)
                return true;

            if (user.Customer.IsDeleted)
                return false;

            if (user.Customer.ExpirationDate < DateTime.Today)
                return false;

            return true;
        }

        /// <summary>
        /// <para>Sets the current employer based on the employeeId first, and then the employer Id if it's still not null.</para>
        /// <para>CurrentEmployer is not guaranteed to be populated after this method is called</para>
        /// </summary>
        /// <param name="user"></param>
        private void SetCurrentEmployerByUser(User user)
        {
            /// Try to set based on the employee Id
            if (!string.IsNullOrEmpty(user.EmployeeId))
            {
                Employee emp = Employee.GetById(user.EmployeeId);
                if (emp.Employer != null)
                    CurrentEmployer = emp.Employer;
            }

            if (CurrentEmployer == null && !string.IsNullOrEmpty(user.EmployerId))
            {
                CurrentEmployer = Employer.GetById(user.EmployerId);
            }
        }

        private bool IsExpired(User user)
        {
            MigratePasswordToHistory(user);
            var passwordPolicy = PasswordPolicy.Default();
            if (user.Customer != null)
                passwordPolicy = user.Customer.PasswordPolicy;

            var maxDays = passwordPolicy.ExpirationDays;
            if (maxDays <= 0)
            {
                return false;
            }

            var userPasswordDate = user.Passwords.Max(p => p.CreatedDate);
            if (userPasswordDate == null)
            {
                throw new InvalidOperationException("Unable to resolve password date for user with id: " + user.Id);
            }
            return DateTime.UtcNow.Subtract(userPasswordDate.Value).TotalDays > maxDays;
        }

        private void MigratePasswordToHistory(User user)
        {
            if (user.Passwords.Empty())
            {
                user.Passwords.Add(new UserPassword
                {
                    Password = user.Password,
                    CreatedDate = user.CreatedDate
                });
                user.Save();
            }
        }

        /// <summary>
        /// For ESS login validate and auto-assign Supervisor/Human Resource Role
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        private bool AutoAssignEmployeeSelfServiceRole(User user, string employerId)
        {
            using (new InstrumentationContext("AuthenticationService.AutoAssignEmployeeSelfServiceRole"))
            {
                Customer usersCustomer = user.Customer;
                if (user.IsDeleted || (!usersCustomer.HasFeature(Feature.EmployeeSelfService) && !usersCustomer.HasFeature(Feature.ESSDataVisibilityInPortal)))
                {
                    return false;
                }

                List<string> contactTypes = new List<string>();
                //Get all ESS Roles
                List<Role> essRoles = GetEmployeeSelfServiceRoles(user.CustomerId).Where(r => (r.ContactTypes != null && r.ContactTypes.Any())).ToList();

                if (essRoles == null || !essRoles.Any())
                {
                    Log.Info("SSO for User={0}, found no roles for potential auto-assignment", user.Email);
                }
                else
                {
                    Log.Info("SSO for User={0}, found the following roles for potential auto-assignment: {1}", user.Email, string.Join(", ",
                        essRoles.Where(r => r.ContactTypes != null && r.ContactTypes.Any()).Select(r => string.Concat(r.Name, ":", r.Id, " (", string.Join(",", r.ContactTypes), ")"))));
                }
                var myEmployer = GetSelfServiceEmployer(user, employerId);
                if (myEmployer == null)
                {
                    return false;
                }

                List<string> roles = new List<string>();
                List<Role> essDefaultRoles = GetEmployeeSelfServiceRoles(user.CustomerId).Where(r => r.DefaultRole || r.AlwaysAssign).ToList();

                if (!myEmployer.Roles.Any())
                {
                    foreach (Role rm in essDefaultRoles.Where(r => r.DefaultRole))
                    {
                        roles.AddIfNotExists(rm.Id);
                    }
                }

                foreach (Role rm in essDefaultRoles.Where(r => r.AlwaysAssign))
                {
                    roles.AddIfNotExists(rm.Id);
                }

                if (myEmployer != null)
                {
                    myEmployer.ContactOf = new List<string>(0);

                    // Only assign the employee role IF the user is an employee
                    if ((User.IsEmployeeSelfServicePortal || user.Customer.HasFeature(Feature.ESSDataVisibilityInPortal)) && !string.IsNullOrWhiteSpace(myEmployer.EmployeeId))
                    {
                        if (User.IsEmployeeSelfServicePortal)
                        {
                            foreach (string r in roles)
                            {
                                myEmployer.Roles.AddIfNotExists(r);
                            }
                        }

                        // Get the list of employees this user is a contact for
                        var contacts = EmployeeContact.AsQueryable()
                            .Where(c => c.CustomerId == user.CustomerId
                                && c.EmployerId == myEmployer.EmployerId
                                && c.RelatedEmployeeNumber == myEmployer.Employee.EmployeeNumber)
                            .ToList();

                        // Set the contacts of collection, this is just a flat collection of everyone this user can see
                        myEmployer.ContactOf = contacts.Select(c => c.EmployeeId).Distinct().ToList();

                        // Get the distinct list of contact types so we can provision any auto-assignment roles they should have
                        contactTypes = contacts.Select(c => c.ContactTypeCode).Distinct().ToList();
                    }

                    // Now add any employer contact types that may be there for this email address.
                    if (myEmployer.Employer != null ? myEmployer.Employer.HasFeature(Feature.EmployerContact) : user.Customer.HasFeature(Feature.EmployerContact))
                    {
                        List<IMongoQuery> ands = new List<IMongoQuery>();

                        ands.Add(EmployerContact.Query.EQ(ec => ec.EmployerId, employerId));
                        ands.Add(EmployerContact.Query.EQ(ec => ec.CustomerId, user.CustomerId));
                        ands.Add(EmployerContact.Query.Or(
                           EmployerContact.Query.EQ(ec => ec.Contact.EndDate, null),
                           EmployerContact.Query.GTE(ec => ec.Contact.EndDate, DateTime.UtcNow.Date)
                           ));
                        EmployerContact.Query.MatchesString(ec => ec.Contact.Email, user.Email, ands);
                        var query = EmployerContact.Query.Find(EmployerContact.Query.And(ands));
                        var employerContacts = query.ToList();
                        contactTypes.AddRangeIfNotExists(employerContacts.Select(t => t.ContactTypeCode).Distinct().ToList());
                    }
                }
                Log.Info("SSO for User={0}, found {1} contact(s) including type(s): {2}", user.Email, myEmployer.ContactOf.Count, string.Join(", ", contactTypes));

                if (essRoles != null)
                {
                    foreach (var role in essRoles)
                    {
                        if (role.ContactTypes == null || !role.ContactTypes.Any())
                        {
                            continue;
                        }

                        bool userHasMatchingContactTypes = role.ContactTypes.Intersect(contactTypes).Any();
                        if (role.RevokeAccessIfNoContactsOfTypes && !userHasMatchingContactTypes)
                        {
                            Log.Info("SSO for User={0}, Revoking Role {1}:{2} (no contact exists of specified type and revoke=true)", user.Email, role.Name, role.Id);
                            if (User.IsEmployeeSelfServicePortal)
                            {
                                myEmployer.Roles.Remove(role.Id);
                            }
                            else
                            {
                                user.Roles.Remove(role.Id);
                            }
                        }
                        else if (userHasMatchingContactTypes)
                        {
                            Log.Info("SSO for User={0}, Adding Role {1}:{2}, for Contact Types: {3}", user.Email, role.Name, role.Id, string.Join(", ", role.ContactTypes.Intersect(contactTypes)));
                            if (User.IsEmployeeSelfServicePortal)
                            {
                                myEmployer.Roles.AddIfNotExists(role.Id);
                            }
                            else
                            {
                                user.Roles.AddIfNotExists(role.Id);
                            }
                        }
                    }
                }

                user.Save();

                // If the user is not an employee based user, make sure they have this permission in ESS
                if (myEmployer == null || string.IsNullOrWhiteSpace(myEmployer.EmployeeId))
                {
                    var permissions = User.Permissions.GetPermissions(user, employerId);
                    return permissions.Contains(Permission.ViewAllEmployees.Id);
                }

                return true;
            }
        }

        private EmployerAccess GetSelfServiceEmployer(User user, string employerId = null)
        {
            employerId = employerId ?? EmployerId;

            // Do we have an EmployerAccess record already?
            EmployerAccess myEmployer = user.Employers.FirstOrDefault(e => e.IsActive
                && !string.IsNullOrWhiteSpace(e.EmployeeId)
                && (!User.IsEmployeeSelfServicePortal || e.EmployerId == (employerId ?? e.EmployerId)));

            // Nope, can we create one?
            if (myEmployer == null)
            {
                // Do we have an employee record matching the user logging in?
                var findEmp = Employee.AsQueryable().Where(e => e.CustomerId == user.CustomerId && e.Info.Email == user.Email);
                if (!string.IsNullOrWhiteSpace(employerId))
                    findEmp = findEmp.Where(e => e.EmployerId == employerId);
                Employee me = findEmp.FirstOrDefault();
                employerId = me == null ? employerId : me.EmployerId;
                // If me is null, that's OK, because 3rd parties, employer admins and employer contacts may log in w/o being employees themselves to ESS
                myEmployer = user.Employers.FirstOrDefault(e => e.IsActive && e.EmployerId == (employerId ?? e.EmployerId));
                if (myEmployer == null && !string.IsNullOrWhiteSpace(employerId))
                    myEmployer = user.Employers.AddFluid(new EmployerAccess()
                    {
                        EmployerId = employerId,
                        Employee = me,
                        AutoAssignCases = false,
                        Roles = new List<string>()
                    });
                else
                    myEmployer.Employee = me;
            }

            myEmployer.ContactOf = myEmployer.ContactOf ?? new List<string>(0);

            return myEmployer;
        }

        public void Logout(HttpContextBase context)
        {
            using (new InstrumentationContext("AuthenticationService.Logout"))
            {
                // update user's last activity date, etc. and save
                //  (the controller should nuke thier cookie/authentication ticket by expiring it)
                var user = User.Current;

                if (user != null)
                {
                    user.LastActivityDate = DateTime.Now;
                    user.Save();
                }

                DestroyAuthenticationTicket(context);
            }
        }

        public bool ValidatePasswordAgainstPolicy(PasswordPolicy policy, User targetUser, string newpassword, out ICollection<string> errors)
        {
            errors = new List<string>();
            using (new InstrumentationContext("AuthenticationService.ValidatePasswordComplexity"))
            {
                var hash = new CryptoString(newpassword).Hash().Hashed;
                var now = DateTime.UtcNow;

                //  min 8 characters and can be increased in Customer config
                var minlen = policy.Clamp().MinimumLength;
                if (newpassword.Length < minlen)
                {
                    errors.Add(string.Format("Password must be at least {0} characters long.", minlen));
                }
                if (policy.ReuseLimitDays > 0)
                {
                    var existing = targetUser.Passwords
                        .Where(p => p.Password.Hashed == hash &&
                        now.Subtract(p.CreatedDate.Value).TotalDays < policy.ReuseLimitDays
                    );
                    if (existing.Any())
                    {
                        errors.Add(string.Format("Password cannot be reused until {0} days have passed.", policy.ReuseLimitDays));
                    }
                }
                if (policy.ReuseLimitCount > 0)
                {
                    var existing = targetUser.Passwords
                        .OrderByDescending(o => o.CreatedDate)
                        .Take(policy.ReuseLimitCount)
                        .Where(p => p.Password.Hashed == hash);
                    if (existing.Any())
                    {
                        errors.Add(string.Format("Password cannot be reused if it is one of the previous {0} passwords.", policy.ReuseLimitCount));
                    }
                }
                //  at least 1 number OR special character OR space
                //  mixed case, at least one caps, one lower case
                if (!Regex.IsMatch(newpassword, "^.*(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[\\d\\W\\s]).*$"))
                {
                    errors.Add("Password must contain a minimum of one lower case character, one upper case character, and one digit or special character.");
                }
            }

            return errors.Count == 0;
        }

        /// <summary>
        /// Creates a link, that user can navigate/open to reset or change his/her password
        /// </summary>
        /// <param name="user">User, whose password need to change/reset </param>
        /// <returns>A link that will allow user to change password</returns>
        public string GetPasswordResetLink(User user)
        {
            using (new InstrumentationContext("AuthenticationService.ResetLockedPassword"))
            {                
                var resetKey = GeneratePasswordResetKey(user);
                user.ResetKey = resetKey;
                user.ResetKeyDate = DateTime.Now;
                user.Save();
                string resetLink = user.Customer.GetBaseUrl() + "/ResetPassword/" + resetKey;
                return resetLink;                                
            }
        }
        public Boolean ResetLockedPassword(User user)
        {
            using (new InstrumentationContext("AuthenticationService.ResetLockedPassword"))
            {
                // 1. Find user by email address
                // 2. Generate password reset key using GeneratePasswordResetKey
                // 3. Set the Must change password flag on user
                // 4. Save the user
                // 5. Send password reset email w/ link embedding the password reset key
                //      the template is an embedded resource in the resources file of common.
                string loweredEmail = user.Email.ToLowerInvariant();//email.ToLowerInvariant();
                var resetKey = GeneratePasswordResetKey(user);
                user.ResetKey = resetKey;
                user.ResetKeyDate = DateTime.Now;

                //set user Failed login attempts
                user.FailedLoginAttempts = Settings.Default.LockoutFailedLoginAttempts;
                user.Save();
                //user.MustChangePassword = true;
                //user.Save();

                // Get our forgot email password template
                string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_LockedOutEmail);

                body = Rendering.Template.RenderTemplate(body, new LockedAccountMessageData()
                {
                    FirstName = user.FirstName,
                    CustomerSiteUrl = user.Customer.GetBaseUrl(),
                    SiteUrl = GetRootPath(),
                    ResetKey = resetKey,
                    IpAddress = user.LockedIpAddress
                });

                using (MailMessage msg = new MailMessage())
                {
                    msg.To.Add(user.Email);
                    msg.Subject = "Your Account Has Been Locked";
                    msg.Body = body;
                    msg.IsBodyHtml = true;

                    return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
                }
            }
        }

        public bool ForgotPassword(string email)
        {
            using (new InstrumentationContext("AuthenticationService.ForgotPassword"))
            {
                // 1. Find user by email address
                // 2. Generate password reset key using GeneratePasswordResetKey
                // 3. Set the Must change password flag on user
                // 4. Save the user
                // 5. Send password reset email w/ link embedding the password reset key
                //      the template is an embedded resource in the resources file of common.
                string loweredEmail = email.ToLowerInvariant();
                var user = User.AsQueryable().Where(c => c.Email == loweredEmail && !c.IsDeleted).FirstOrDefault();

                if (user == null || user.IsDisabled || user.IsDeleted)
                    return false;

                var resetKey = GeneratePasswordResetKey(user);
                user.ResetKey = resetKey;
                user.ResetKeyDate = DateTime.Now;
                user.Save();

                // Get our forgot email password template
                string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_ForgotPassword);
                string url = null;
                if (user.Customer != null)
                    url = user.Customer.GetBaseUrl(User.IsEmployeeSelfServicePortal);
                else
                    url = GetRootPath();

                body = Rendering.Template.RenderTemplate(body, new ForgotPasswordMessageData()
                {
                    FirstName = user.FirstName,
                    CustomerSiteUrl = url,
                    SiteUrl = GetRootPath(),
                    ResetKey = resetKey
                });

                using (MailMessage msg = new MailMessage())
                {
                    msg.To.Add(user.Email);
                    msg.Subject = "Forgot Password";
                    msg.Body = body;
                    msg.IsBodyHtml = true;

                    return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
                }
            }
        }//ForgotPassword

        public void ResetPassword(string resetKey, string newPassword)
        {

            using (new InstrumentationContext("AuthenticationService.ResetPassword"))
            {
                if (string.IsNullOrWhiteSpace(resetKey))
                {
                    throw new ArgumentNullException("resetKey");
                }
                if (string.IsNullOrWhiteSpace(newPassword))
                {
                    throw new ArgumentNullException("newPassword");
                }

                string loweredKey = resetKey.ToLowerInvariant();
                User user = User.AsQueryable().Where(c => c.ResetKey == loweredKey && !c.IsDeleted).FirstOrDefault();
                
                int.TryParse(ConfigurationManager.AppSettings.Get("ResetKeyValidityHours"), out int configuredValidityHours);
                if (configuredValidityHours == 0)
                {
                    configuredValidityHours = 24;
                }
                // ResetKeyDate is in the valid date range
                if (!(user?.ResetKeyDate != null && user.ResetKeyDate <= DateTime.Now && user.ResetKeyDate > DateTime.Now.AddHours(-configuredValidityHours)))
                {
                    throw new AbsenceSoftResetPasswordFailureException();
                }

                // Find user by email address
                if (user == null || user.IsDeleted)
                    throw new AbsensesoftAuthenticationError();

                // Validate password complexity
                ICollection<string> errors;
                PasswordPolicy policy = PasswordPolicy.Default();
                if (user.Customer != null)
                    policy = user.Customer.PasswordPolicy;
                if (!ValidatePasswordAgainstPolicy(policy, user, newPassword, out errors))
                {
                    throw new AbsenceSoftException(errors);
                }

                // Set the new password
                user.Password = new CryptoString() { PlainText = newPassword };

                // Set the reset key back to null, it's no longer necessary, no need to keep taking up space
                user.ResetKey = null;
                user.ResetKeyDate = null;

                //Set LockedDate to null
                user.LockedDate = null;

                //Set LastFailedLoginAttempt to null
                user.LastFailedLoginAttempt = null;

                //Set FailedLoginAttempts to 0
                user.FailedLoginAttempts = 0;

                // Save the user account
                user.Save();
            }
        }

        private string GeneratePasswordResetKey(User user)
        {
            using (new InstrumentationContext("AuthenticationService.GeneratePasswordResetKey"))
            {
                // 1. Take combo of Email + "|" + HashedPassword + "|" + TimeStamp, rehash the concated string
                // 2. Return that value
                var newPassword = string.Format("{0}|{1}|{2}", user.Email, user.Password.Hashed, DateTime.Now);

                return newPassword.Sha256Hash().ToLowerInvariant();
            }
        }

        /// <summary>
        /// Locks users account to prevent login
        /// </summary>
        public void ProcessFailedLoginAttempt(User user)
        {
            if (GetTimeSinceLastLoginFailure(user) >= Settings.Default.LockoutFailedLoginTimeframe)
                user.FailedLoginAttempts = 1;
            else
                user.FailedLoginAttempts++;
            user.LastFailedLoginAttempt = DateTime.UtcNow;
            //user.Save();
            if (user.FailedLoginAttempts == Settings.Default.LockoutFailedLoginAttempts)
            {
                user.LockedDate = DateTime.UtcNow;
                user.LockedIpAddress = GetClientIPAddress();
                ResetLockedPassword(user);
            }

            user.Save();
        }

        /// <summary>
        /// Gets client IP address from current http context
        /// </summary>
        /// <returns>ip address</returns>
        private string GetClientIPAddress()
        {
            var request = HttpContext.Current == null ? null : HttpContext.Current.Request;
            if (request == null)
                return "127.0.0.1";

            return request.ClientIPAddress();
        }

        /// <summary>
        /// Gets number of minutes since last login attempt failure
        /// </summary>
        /// <returns>total minutes</returns>
        private TimeSpan GetTimeSinceLastLoginFailure(User user)
        {
            TimeSpan span = new TimeSpan();

            if (user.LastFailedLoginAttempt != null)
                span = DateTime.UtcNow - (DateTime)user.LastFailedLoginAttempt;

            return span;
        }

        /// <summary>
        /// Get list of all users having same customerid and employer id as logged in user
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<User> GetUsersList(string customerId, string employerId, bool skipDisabledUsers = true)
        {
            List<IMongoQuery> ands = new List<IMongoQuery>();
            ands.Add(User.Query.EQ(u => u.CustomerId, customerId));
            ands.Add(User.Query.NE(u => u.IsDeleted, true));

            if (skipDisabledUsers)
            {
                ands.Add(User.Query.EQ(e => e.DisabledDate, null));
            }
            if (!string.IsNullOrWhiteSpace(employerId))
                ands.Add(User.Query.ElemMatch(u => u.Employers, b => b.And(
                    b.EQ(e => e.EmployerId, employerId),
                    b.Or(b.EQ(e => e.EffectiveDate, null), b.LTE(e => e.EffectiveDate, DateTime.Today)),
                    b.Or(b.EQ(e => e.ExpireDate, null), b.GT(e => e.ExpireDate, DateTime.Today))
                )));
            IMongoFields fields = Fields.Exclude(
                "Password",
                "Passwords",
                "ResetKey",
                "DashboardId",
                "Employers.ContactOf",
                "Employers.HROf",
                "Employers.SupervisorOf");

            if (!User.Permissions.GetPermissions(CurrentUser).Contains(Permission.AllItems.Id))
            {
                List<string> myTeamsUserIds = new TeamService(CurrentUser).GetTeamMemberIdsForCurrentUser();
                ands.Add(User.Query.In(u => u.Id, myTeamsUserIds));
            }

            return User.Query.Find(User.Query.And(ands))
                .SetFields(fields)
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .ToList();
        }

        public User GetUserById(string id)
        {
            return User.GetById(id);
        }

        public bool IsValidResetKey(string customerId, string resetKey)
        {
            // 1. check if user exist with this reset key
            string loweredKey = resetKey.ToLowerInvariant();
            User user = User.AsQueryable().Where(c => c.ResetKey == loweredKey && c.CustomerId == customerId && !c.IsDeleted).FirstOrDefault();
            if (user == null)
                return false;

            // 2. Ensure the reset key date is still valid             
            int.TryParse(ConfigurationManager.AppSettings.Get("ResetKeyValidityHours"), out int configuredValidityHours);
            if (configuredValidityHours == 0)
            {
                configuredValidityHours = 24;
            }
            // ResetKeyDate is in the valid date range
            if (!(user?.ResetKeyDate != null && user.ResetKeyDate <= DateTime.Now && user.ResetKeyDate > DateTime.Now.AddHours(-configuredValidityHours)))
            {
                throw new AbsenceSoftResetPasswordFailureException();
            }

            return true;
        }

        public bool ApproveUser(string customerId, string resetKey)
        {
            // Get user with resetkey
            // Set resetkey & disabled date both to null, so that user can now login
            string loweredKey = resetKey.ToLowerInvariant();
            User user = User.AsQueryable().Where(c => c.ResetKey == loweredKey && c.CustomerId == customerId && !c.IsDeleted).FirstOrDefault();
            if (user != null)
            {
                //user.ResetKey = null;
                user.DisabledDate = null;
                UpdateUser(user);

                // Send welcome email to user
                WelcomeUserEmail(user);
                return true;
            }

            return false;
        }

        public bool WelcomeUserEmail(User user)
        {
            // Get our create account template
            string body = NotificationApiHelper.GetEmailTemplate(NotificationApiHelper.EmailTemplate_WelcomeEmail);

            body = Rendering.Template.RenderTemplate(body, new WelcomeEmailMessageData()
            {
                Name = user.FirstName + " " + user.LastName,
                Email = user.Email,
                SiteUrl = GetRootPath()
            });

            using (MailMessage msg = new MailMessage())
            {
                if (!string.IsNullOrWhiteSpace(Settings.Default.CreateAccountFrom))
                {
                    msg.From = new MailAddress(Settings.Default.CreateAccountFrom);
                }

                msg.To.Add(user.Email);
                msg.Bcc.Add("sales@absencesoft.com");
                msg.Subject = "Welcome to AbsenceTracker";
                msg.Body = body;
                msg.IsBodyHtml = true;

                msg.SendEmail(CurrentUser, Utilities.GetTokenFromClaims());
                return true;
            }
        }

        public bool DenyUser(string customerId, string resetKey)
        {
            // Get user with resetkey
            // Remove user account
            string loweredKey = resetKey.ToLowerInvariant();
            User user = User.AsQueryable().Where(c => c.ResetKey == loweredKey && c.CustomerId == customerId).FirstOrDefault();
            if (user != null)
            {
                // Delete Customer account
                AbsenceSoft.Data.Customers.Employer.AsQueryable().Where(e => e.CustomerId == user.CustomerId).ToList().ForEach(e => e.Delete());
                // Delete employer account
                AbsenceSoft.Data.Customers.Customer.AsQueryable().Where(e => e.Id == user.CustomerId).ToList().ForEach(e => e.Delete());
                // Delete user
                user.Delete();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Send Validation email to valide Employee email address as part of ESS registration
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool SendEmployeeRegistrationValidationEmail(User user)
        {
            // Get our create account template
            string body = NotificationApiHelper.GetEmailTemplate(
                user.Must(UserFlag.CompleteESSAssociation)
                ? NotificationApiHelper.EmailTemplate_EmployeeAssociationEmail
                : NotificationApiHelper.EmailTemplate_EmployeeValidationEmail);

            body = Rendering.Template.RenderTemplate(body, new EmployeeValidateEmailMesssageData()
            {
                Email = user.Email,
                Name = user.FirstName + " " + user.LastName,
                CustomerSiteUrl = user.Customer.GetBaseUrl(),
                ResetKey = user.ResetKey
            });

            using (MailMessage msg = new MailMessage())
            {
                msg.To.Add(user.Email);
                msg.Subject = "Employee Registration Validation";
                msg.Body = body;
                msg.IsBodyHtml = true;

                return msg.NotifyAsync(CurrentUser, Utilities.GetTokenFromClaims(), NotificationType.Email, "", null).GetAwaiter().GetResult();
            }
        }

        public bool IsEmployee(User user, Customer currentCustomer)
        {
            Role role = GetRoleByName(currentCustomer.Id, "Employee");

            return user.Employers.Any(m => m.Roles.Contains(role.Id));
        }

        private string GetRootPath()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Request != null)
            {
                Uri u = context.Request.Url;
                return string.Format("{0}://{1}", u.Scheme, u.Host);
            }

            // Default if we're not in an actual HttpContext (which may happen for unit tests, etc.)
            return "https://absencetracker.com";
        }

        /// <summary>
        /// Authenticates the sso.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="role">The role name of the user.</param>
        /// <param name="employerReferenceCode">The employer reference code.</param>
        /// <returns></returns>
        public User AuthenticateSso(HttpContextBase context, string userName, out string errorMessage, string role = null, string employerReferenceCode = null)
        {
            var cust = Customer.Current;
            if (cust == null)
            {
                errorMessage = "Unknown customer context for assertion";
                return null;
            }

            Log.Info("Beginning SSO Authentication for user: '{0}', role: '{1}', employerReferenceCode: '{2}'", userName, role, employerReferenceCode);

            string roleId = null;
            if (!string.IsNullOrWhiteSpace(role))
            {
                var myRole = Role.Query.Find(Role.Query.And(
                    Role.Query.EQ(r => r.CustomerId, cust.Id),
                    Role.Query.EQIgnoreCase(r => r.Name, role)))
                    .FirstOrDefault();
                if (myRole == null && ObjectId.TryParse(role, out ObjectId tryMe))
                {
                    roleId = role;
                }

                if (myRole != null)
                {
                    roleId = myRole.Id;
                }
            }

            Employer curEmployer = Employer.Current;
            string employerId = curEmployer == null ? null : curEmployer.Id;
            if (!string.IsNullOrWhiteSpace(employerReferenceCode))
            {
                curEmployer = Employer.Query.Find(
                    Employer.Query.And(
                        Employer.Query.EQ(e => e.CustomerId, cust.Id),
                        Employer.Query.EQIgnoreCase(e => e.ReferenceCode, employerReferenceCode)
                    )
                ).FirstOrDefault();
                if (curEmployer != null)
                {
                    employerId = curEmployer.Id;
                }
                else
                {
                    errorMessage = "Invalid or unknown employer";
                    return null;
                }
            }

            User user = null;
            List<Employee> matches = null;
            List<EmployerContact> matches2 = null;

            if (userName.Contains('@')) // it's an email address
            {
                string email = userName.ToLowerInvariant();
                user = User.AsQueryable().Where(u => u.Email == email && u.CustomerId == cust.Id && !u.IsDeleted).FirstOrDefault();
                if (user == null)
                {
                    List<IMongoQuery> ands = new List<IMongoQuery>()
                    {
                        Employee.Query.EQ(e => e.CustomerId, cust.Id),
                        Employee.Query.EQIgnoreCase(e => e.Info.Email, userName)
                    };
                    var query = Employee.Query.Find(Employee.Query.And(ands));
                    Log.Info("SSO Query to find user by email: '{0}'", query.Query.ToString());
                    matches = query.ToList();

                    if (cust.HasFeature(Feature.EmployerContact) && curEmployer != null)
                    {
                        // Ok, they have this feature, so we need to look to see if this person matches by Email address to
                        //  an employer contact instead.
                        ands = new List<IMongoQuery>()
                        {
                            EmployerContact.Query.EQ(e => e.CustomerId, cust.Id),
                            EmployerContact.Query.EQ(e => e.EmployerId, curEmployer.Id),
                            EmployerContact.Query.EQIgnoreCase(e => e.Contact.Email, userName)
                        };
                        var query2 = EmployerContact.Query.Find(EmployerContact.Query.And(ands));
                        Log.Info("SSO Query to find employer contact by email: '{0}'", query2.ToString());
                        matches2 = query2.ToList();
                    }

                    if ((matches == null || !matches.Any()) && (matches2 == null || !matches2.Any(m => m.Contact != null)))
                    {
                        errorMessage = UserNotFoundByEmail(userName, cust.Id);
                        return null;
                    }
                }
                else
                    Log.Info("SSO User Found, Email={0}, Id: {1}", userName, user.Id);
            }
            else // it's an employee number
            {
                List<IMongoQuery> ands = new List<IMongoQuery>()
                {
                    Employee.Query.EQ(e => e.CustomerId, cust.Id),
                    Employee.Query.EQIgnoreCase(e => e.EmployeeNumber, userName)
                };
                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));
                var query = Employee.Query.Find(Employee.Query.And(ands));
                Log.Info("SSO Query to find user by employee number: '{0}'", query.Query.ToString());
                matches = query.ToList();
                if (!matches.Any())
                {
                    errorMessage = UserNotFoundByNumber(userName, cust.Id, employerId);
                    return null;
                }
            }

            if (user == null && (matches != null || matches2 != null))
            {
                if (matches2 != null && matches2.Any())
                {
                    // It's an employer contact match...

                    EmployerContact con = null;

                    if (matches2.Count == 1)
                        con = matches2.First();
                    else if (!string.IsNullOrWhiteSpace(employerId))
                        con = matches2.FirstOrDefault(m => m.EmployerId == employerId);

                    if (con == null)
                    {
                        errorMessage = string.Format("No employer contact found with email address '{0}' for '{1}'. Please provide a valid employer contact email address or ensure this employer contact record exists in AbsenceTracker",
                            userName, curEmployer == null ? cust.Name : curEmployer.Name);
                        return null;
                    }
                    else
                        Log.Info("SSO for Subject={0}, Employer Contact Found '{1}', Name: '{2}'", userName, con.Contact.Email, con.Contact.FullName);

                    Log.Info("SSO for Subject={0}, User not found, Creating new User", userName);

                    // Create this user as a new user
                    user = new User()
                    {
                        Email = con.Contact.Email,
                        CustomerId = con.CustomerId,
                        Employers = new List<EmployerAccess>(1)
                        {
                            new EmployerAccess()
                            {
                                EmployerId = con.EmployerId,
                                AutoAssignCases = false,
                                Roles = string.IsNullOrWhiteSpace(roleId) || curEmployer == null ? new List<string>(0) : new List<string>(1) { roleId }
                            }
                        },
                        FirstName = con.Contact.FirstName,
                        LastName = con.Contact.LastName,
                        MustChangePassword = false,
                        Password = CryptoString.GetHash(Guid.NewGuid().ToString()),
                        UserFlags = UserFlag.None,
                        UserType = User.IsEmployeeSelfServicePortal ? UserType.SelfService : UserType.Portal,
                        Roles = string.IsNullOrWhiteSpace(roleId) || curEmployer != null ? new List<string>(0) : new List<string>(1) { roleId },
                        ContactInfo = con.Contact
                    };
                    user.Metadata.SetRawValue("SSO", true);
                }
                else
                {
                    // It's an employee match....

                    Employee emp = null;

                    if (matches.Count == 1)
                        emp = matches.First();
                    else if (!string.IsNullOrWhiteSpace(employerId))
                        emp = matches.FirstOrDefault(m => m.EmployerId == employerId);

                    if (emp == null)
                    {
                        errorMessage = string.Format("No employee found with employee number '{0}' for '{1}'. Please provide a valid employee number or email address or ensure this employee record exists in AbsenceTracker",
                            userName, curEmployer == null ? cust.Name : curEmployer.Name);
                        return null;
                    }
                    else
                        Log.Info("SSO for Subject={0}, Employee Found # '{1}', Name: '{2}'", userName, emp.EmployeeNumber, emp.FullName);

                    user = User.AsQueryable().Where(u => u.Email == emp.Info.Email.ToLowerInvariant() && !u.IsDeleted).FirstOrDefault();
                    if (user != null && user.CustomerId != emp.CustomerId)
                    {
                        errorMessage = string.Format("Employee '{0}' with email '{1}' cannot be created because the employee's email address is already in use by a different user account", userName, user.Email);
                        return null;
                    }
                    else if (user != null)
                        Log.Info("SSO for Subject={0}, User found for Employee # '{1}', by Email='{2}'", userName, emp.EmployeeNumber, user.Email);

                    if (user == null)
                    {
                        Log.Info("SSO for Subject={0}, User not found, Creating new User", userName);

                        // Create this user as a new user
                        user = new User()
                        {
                            Email = emp.Info.Email,
                            CustomerId = emp.CustomerId,
                            Employers = new List<EmployerAccess>(1)
                        {
                            new EmployerAccess()
                            {
                                EmployerId = emp.EmployerId,
                                EmployeeId = emp.Id,
                                AutoAssignCases = false,
                                Roles = string.IsNullOrWhiteSpace(roleId) || curEmployer == null ? new List<string>(0) : new List<string>(1) { roleId }
                            }
                        },
                            FirstName = emp.FirstName,
                            LastName = emp.LastName,
                            MustChangePassword = false,
                            Password = CryptoString.GetHash(Guid.NewGuid().ToString()),
                            UserFlags = UserFlag.None,
                            UserType = User.IsEmployeeSelfServicePortal ? UserType.SelfService : UserType.Portal,
                            Roles = string.IsNullOrWhiteSpace(roleId) || curEmployer != null ? new List<string>(0) : new List<string>(1) { roleId }
                        };
                        user.Metadata.SetRawValue("SSO", true);
                    }
                    else if (emp != null)
                    {
                        if (user.CustomerId != emp.CustomerId)
                        {
                            errorMessage = string.Format("Employee '{0}' with email '{1}' cannot be created because the employee's email address is already in use by a different user account", userName, user.Email);
                            return null;
                        }

                        Log.Info("SSO for Subject={0}, User exists, Employee found, adding Employer Access to User if not already exists", userName);
                        var employerAccess = user.Employers.FirstOrDefault(e => e.EmployerId == emp.EmployerId) ?? user.Employers.AddFluid(new EmployerAccess()
                        {
                            EmployerId = emp.EmployerId,
                            AutoAssignCases = false
                        });
                        employerAccess.EmployeeId = emp.Id;
                        employerAccess.Employee = emp;
                        employerAccess.Roles = employerAccess.Roles ?? new List<string>();
                        if (!string.IsNullOrWhiteSpace(roleId))
                            employerAccess.Roles.AddIfNotExists(roleId);
                        user.Metadata.SetRawValue("SSO", true);
                        user = user.Save();
                    }
                }
            }
            else if ((matches != null || matches2 != null) && curEmployer != null)
            {
                if (matches != null)
                {
                    Employee emp = matches.FirstOrDefault(m => m.EmployerId == curEmployer.Id);
                    if (emp != null)
                    {
                        if (user.CustomerId != emp.CustomerId)
                        {
                            errorMessage = string.Format("Employee '{0}' with email '{1}' cannot be created because the employee's email address is already in use by a different user account", userName, user.Email);
                            return null;
                        }

                        Log.Info("SSO for Subject={0}, User exists, Employee found, adding Employer Access to User if not already exists for ESS", userName);

                        var employerAccess = user.Employers.FirstOrDefault(e => e.EmployerId == emp.EmployerId) ?? user.Employers.AddFluid(new EmployerAccess()
                        {
                            EmployerId = curEmployer.Id,
                            Employer = curEmployer,
                            AutoAssignCases = false
                        });
                        employerAccess.EmployeeId = emp.Id;
                        employerAccess.Employee = emp;
                        employerAccess.Roles = employerAccess.Roles ?? new List<string>();
                        if (string.IsNullOrWhiteSpace(roleId))
                            employerAccess.Roles.AddIfNotExists(roleId);
                        user.Metadata.SetRawValue("SSO", true);
                        user = user.Save();
                    }
                }
                else if (matches2 != null)
                {
                    EmployerContact con = matches2.FirstOrDefault(m => m.EmployerId == curEmployer.Id);
                    if (con != null)
                    {
                        if (user.CustomerId != con.CustomerId)
                        {
                            errorMessage = string.Format("Employer Contact '{0}' with email '{1}' cannot be created because the employer contact's email address is already in use by a different user account", userName, user.Email);
                            return null;
                        }

                        Log.Info("SSO for Subject={0}, User exists, Employer Contact found, adding Employer Access to User if not already exists for ESS", userName);

                        var employerAccess = user.Employers.FirstOrDefault(e => e.EmployerId == con.EmployerId) ?? user.Employers.AddFluid(new EmployerAccess()
                        {
                            EmployerId = curEmployer.Id,
                            Employer = curEmployer,
                            AutoAssignCases = false
                        });
                        employerAccess.Roles = employerAccess.Roles ?? new List<string>();
                        if (string.IsNullOrWhiteSpace(roleId))
                            employerAccess.Roles.AddIfNotExists(roleId);
                        user.Metadata.SetRawValue("SSO", true);
                        user = user.Save();
                    }
                }
            }

            // Ensure user exists and is a member of this customer
            if (user == null)
            {
                errorMessage = string.Format("User '{0}' was not found or belongs to a different organization", userName);
                return null;
            }

            // If we're in ESS, then ensure this user has access to the current employer
            if (curEmployer != null && !user.HasEmployerAccess(curEmployer.Id))
            {
                errorMessage = "Current user does not have access to this employer";
                return null;
            }

            // Add the role if not already there
            if (!string.IsNullOrWhiteSpace(roleId))
            {
                if (curEmployer == null)
                {
                    if (!user.Roles.Contains(roleId))
                    {
                        user.Roles.Add(roleId);
                        user = user.Save();
                    }
                }
                else
                {
                    var empAccess = user.Employers.FirstOrDefault(a => a.EmployerId == curEmployer.Id);
                    if (empAccess != null && !empAccess.Roles.Contains(roleId))
                    {
                        empAccess.Roles.Add(roleId);
                        user = user.Save();
                    }
                }
            }

            // If the user is still a new user, then save it
            if (user.IsNew)
            {
                user.Save();
            }

            // If self service
            if (curEmployer != null || cust.HasFeature(Feature.ESSDataVisibilityInPortal))
            {
                Log.Info("SSO for Subject={0}, ESS or ESSDataVisibilityInPortal, Perform Contact Mapping and Auto Assign Roles", userName);
                try
                {
                    if (!AutoAssignEmployeeSelfServiceRole(user, curEmployer?.Id))
                    {
                        errorMessage = "Current user does not have access to this employer";
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error auto assigning employee self service roles for user, '{0}'", user.Email);
                    errorMessage = "Could not automatically determine or provision system access for user due to an internal system error";
                    return null;
                }
                Log.Info("SSO for Subject={0}, ESS or ESSDataVisibilityInPortal, Auto Assign Roles/Contacts Complete", user.Email);
            }

            Log.Info("SSO for Subject={0}, SUCCESS; User: {1}, Email: {2}, Roles: {3}", userName, user.Id, user.Email, string.Join(",", user.Roles));
            errorMessage = null;
            return user;
        }

        private string UserNotFoundByNumber(string employeeNumber, string customerId, string employerId = null)
        {
            return UserNotFound("employee number", employeeNumber, customerId, employerId);
        }

        private string UserNotFoundByEmail(string email, string customerId, string employerId = null)
        {
            return UserNotFound("email", email, customerId, employerId);
        }

        private string UserNotFound(string identifierType, string identifier, string customerId, string employerId)
        {
            StringBuilder warningMessage = new StringBuilder();
            warningMessage.AppendFormat("No employee found with {0} '{1}' for customerId '{2}'", identifierType, identifier, customerId, employerId);
            if (!string.IsNullOrWhiteSpace(employerId))
            {
                warningMessage.AppendFormat(" and employerId '{0}'", employerId);
            }
            warningMessage.Append(". Please provide a valid employee number or email address or ensure this employee record exists in AbsenceTracker");
            return warningMessage.ToString();
        }



        /// <summary>
        /// Sets or adds the session id and returns that Id back to the caller.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The current or new session id if one was not already set.</returns>
        /// <remarks>
        /// NOTE: This is only necessary because SSO requires some persistent state on a per-session
        /// basis in which since we don't use SessionState, we have no SessionId natively, and instead
        /// we want to use a session record in MongoDB with a set expiration anyway, so this enables
        /// us to generate an ObjectId string for session id and set it in a separate cookie. Hooray us!
        /// </remarks>
        public string EnsureSessionId(HttpContextBase context, HttpCookie formsCookie = null)
        {
            if (context == null) return null;

            formsCookie = formsCookie ?? context.Request.Cookies[FormsAuthentication.FormsCookieName];
            string fakeSessionId = null;
            HttpCookie atx = context.Request.Cookies["_atx"];
            if (atx != null)
            {
                fakeSessionId = atx.Value;
                atx.Expires = formsCookie == null ? atx.Expires : formsCookie.Expires;
            }
            else
            {
                fakeSessionId = ObjectId.GenerateNewId().ToString();
                atx = new HttpCookie("_atx", fakeSessionId)
                {
                    Domain = formsCookie == null ? FormsAuthentication.CookieDomain : formsCookie.Domain,
                    HttpOnly = true,
                    Path = formsCookie == null ? FormsAuthentication.FormsCookiePath : formsCookie.Path,
                    Secure = formsCookie == null ? FormsAuthentication.RequireSSL : formsCookie.Secure,
                    Shareable = formsCookie == null ? false : formsCookie.Shareable,
                    Expires = formsCookie == null
                        ? DateTime.UtcNow.Add(SecuritySettings.GetCurrentTimeoutPeriodAsTimeSpan(context))
                        : formsCookie.Expires
                };
            }
            context.Response.Cookies.Set(atx);
            return fakeSessionId;
        }

        /// <summary>
        /// Gets the session id.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public string GetSessionId(HttpContextBase context)
        {
            if (context == null) return null;

            HttpCookie atx = context.Request.Cookies["_atx"] ?? context.Response.Cookies["_atx"];
            if (atx != null && !string.IsNullOrWhiteSpace(atx.Value))
                return atx.Value;

            return null;
        }

        /// <summary>
        /// Creates and set the authentication ticket.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="user">The user.</param>
        public void CreateAuthenticationTicket(HttpContextBase context, User user)
        {
            if (user == null || context == null || user.IsDeleted)
                return;

            SecuritySettings.CreateAuthenticationTicket(context, user);
            EnsureSessionId(context);
        }

        /// <summary>
        /// Renews the authentication ticket.
        /// </summary>
        /// <param name="context">The context.</param>
        public void RenewAuthenticationTicket(HttpContextBase context)
        {
            var authenticationTicketCookie = SecuritySettings.RenewAuthenticationTicket(context);
            if (authenticationTicketCookie != null)
                EnsureSessionId(context, authenticationTicketCookie);
        }

        /// <summary>
        /// Destroys the authentication ticket and associated ATX cookie.
        /// </summary>
        /// <param name="context">The context.</param>
        public void DestroyAuthenticationTicket(HttpContextBase context)
        {
            // Sign out the user and expire their authentication ticket
            FormsAuthentication.SignOut();

            // If we have a session (and we should NOT) then clear it
            if (context.Session != null)
            {
                context.Session.Clear();
                context.Session.Abandon();
            }

            var atx = context.Request.Cookies["_atx"];
            if (atx != null)
            {
                atx.Expires = DateTime.UtcNow.AddDays(-1);
                context.Response.Cookies.Set(atx);
            }
        }

        public RegistrationStatus RegisterAccount(string email, bool resend, bool resendToAltEmail, string clientIpAddress)
        {
            using (new InstrumentationContext("AuthenticationService.RegisterAccount"))
            {
                if (string.IsNullOrWhiteSpace(email))
                    throw new ArgumentNullException("email", "Email address is required to register an account");

                if (!resend && resendToAltEmail)
                    resend = true;

                string loweredEmail = email.ToLowerInvariant();

                var user = User.AsQueryable().FirstOrDefault(c => c.Email == loweredEmail);
                if (user == null)
                    return SendRegistrationEmail(email, resend, resendToAltEmail, clientIpAddress);

                CurrentCustomer = user.Customer;
                return SendUserExistsEmail(user, clientIpAddress);
            }
        }

        public List<Employee> GetEmployeesByEmployerAndCompany(string email)
        {
            var employeeQueryable = Employee.AsQueryable().Where(e => e.Info.Email == email && e.IsDeleted != true);

            if (CurrentCustomer != null)
                employeeQueryable = employeeQueryable.Where(e => e.CustomerId == CustomerId);

            if (CurrentEmployer != null)
                employeeQueryable = employeeQueryable.Where(e => e.EmployerId == EmployerId);

            return employeeQueryable.ToList();
        }

        private RegistrationStatus SendRegistrationEmail(string email, bool resend, bool resendToAltEmail, string clientIpAddress)
        {
            email = email.ToLowerInvariant();
            bool registrationEnabled = true;

            SelfRegistrationRequest request = new SelfRegistrationRequest(email, clientIpAddress);
            if (resend)
            {
                SelfRegistrationRequest existingRequest = SelfRegistrationRequest.AsQueryable().OrderByDescending(ssr => ssr.CreatedDate).FirstOrDefault(ssr => ssr.Email == email);
                if (existingRequest.CreatedDate < DateTime.UtcNow.AddHours(-1))
                {
                    resend = false;
                }
                else
                {
                    request = existingRequest;
                }
            }

            // Check if more than one employee exists with same email address for same employee, same company
            var employees = GetEmployeesByEmployerAndCompany(email);
            Employee employee = employees.FirstOrDefault();
            if (employees.Count == 0)
            {
                SendNonMatchingEmployeeEmailToAdmin(email, clientIpAddress);
                request.Status = RegistrationStatus.EmployeeNotFound;
            }
            else if (employees.Count > 1)
            {
                SendEmployeeExistsEmailToAdmin(employee.Info.Email, clientIpAddress);
                request.Status = RegistrationStatus.EmployeeNotUnique;
            }
            else if (employees.Count == 1)
            {
                CurrentCustomer = employee.Customer;
                CurrentEmployer = employee.Employer;
                if (CurrentCustomer.SecuritySettings != null)
                    registrationEnabled = employee.Customer.SecuritySettings.EnableSelfServiceRegistration;
            }
            if (!resend && employees.Any())
            {
                if (!registrationEnabled)
                    request.Status = RegistrationStatus.SelfRegistrationDisabled;

                request.FirstName = employee.FirstName;
                request.AlternateEmail = employee.Info.AltEmail;
                request.EmployeeId = employee.Id;
                request.CustomerId = employee.CustomerId;
                request.EmployerId = employee.EmployerId;
                request.VerificationKey = Guid.NewGuid().ToString("N");
            }

            if (registrationEnabled && request.Status != RegistrationStatus.EmployeeNotFound && request.Status != RegistrationStatus.EmployeeNotUnique)
            {
                bool emailSuccessfullySent = SendSelfRegistrationEmailToUser(request, resendToAltEmail);
                if (resend && emailSuccessfullySent)
                {
                    request.Status = RegistrationStatus.EmailResent;
                }
                else if (resend && resendToAltEmail && emailSuccessfullySent)
                {
                    request.Status = RegistrationStatus.EmailResentToAlt;
                }
                else if (emailSuccessfullySent)
                {
                    request.Status = RegistrationStatus.EmailSent;
                }
                else
                {
                    request.Status = RegistrationStatus.ErrorSendingEmail;
                }
            }

            request.Save();
            return request.Status;
        }

        private RegistrationStatus SendUserExistsEmail(User user, string clientIpAddress)
        {
            SelfRegistrationRequest request = new SelfRegistrationRequest(user.Email, clientIpAddress);
            request.Status = RegistrationStatus.UserExists;
            if (user == null)
                throw new ArgumentNullException("user");

            if (user.IsDisabled)
            {
                request.Status = RegistrationStatus.DisabledUser;
            }
            else if (user.Customer == null || (user.Customer.SecuritySettings != null && !user.Customer.SecuritySettings.EnableSelfServiceRegistration))
            {
                request.Status = RegistrationStatus.SelfRegistrationDisabled;
            }

            if (request.Status == RegistrationStatus.UserExists)
            {
                SendUserExistsEmailToUser(user);
                SendUserExistsEmailToAdmin(user, clientIpAddress);
            }

            request.Save();
            return request.Status;
        }

        private bool TemplateAndSendSelfRegistrationEmail(string mailTemplate, string subject, string emailBody, SelfRegistrationRequest userRequest, string createAccountLinkText, bool sendToAltEmail)
        {
            // Get our self account creation email template
            string body = Rendering.Template.RenderTemplate(NotificationApiHelper.GetEmailTemplate(mailTemplate), new SelfAccountCreationMessageData()
            {
                FirstName = userRequest.FirstName,
                EmailBody = emailBody,
                CustomerSiteUrl = CurrentCustomer.GetBaseUrl(true),
                SiteUrl = CurrentCustomer.GetBaseUrl(true),
                VerificationKey = userRequest.VerificationKey,
                CreateAccountLinkText = createAccountLinkText
            });
            string email = userRequest.Email;
            if (sendToAltEmail)
                email = userRequest.AlternateEmail;

            // Failed to send because we don't have an alt email on file
            if (sendToAltEmail && string.IsNullOrWhiteSpace(userRequest.AlternateEmail))
                return false;

            return SendSelfRegistrationEmail(body, subject, email);
        }

        private bool TemplateAndSendSelfRegistrationEmail(string mailTemplate, string subject, string emailBody, User user)
        {
            // Get our self account creation email template
            string body = Rendering.Template.RenderTemplate(NotificationApiHelper.GetEmailTemplate(mailTemplate), new SelfAccountCreationMessageData()
            {
                FirstName = user.FirstName,
                EmailBody = emailBody,
                CustomerSiteUrl = CurrentCustomer.GetBaseUrl(true),
                SiteUrl = CurrentCustomer.GetBaseUrl(true),
            });

            return SendSelfRegistrationEmail(body, subject, user.Email);
        }

        private bool TemplateAndSendSelfRegistrationEmail(string mailTemplate, string subject, string emailBody, Customer customer)
        {
            if (customer == null || customer.Contact == null)
                return false;

            // Get our self account creation email template
            string body = Rendering.Template.RenderTemplate(NotificationApiHelper.GetEmailTemplate(mailTemplate), new SelfAccountCreationMessageData()
            {
                FirstName = customer.Contact.FirstName,
                EmailBody = emailBody,
                CustomerSiteUrl = customer.GetBaseUrl(true),
                SiteUrl = customer.GetBaseUrl(true),
            });

            return SendSelfRegistrationEmail(body, subject, customer.Contact.Email);
        }

        private bool SendSelfRegistrationEmail(string body, string subject, string email)
        {
            using (MailMessage msg = new MailMessage())
            {
                msg.To.Add(email);
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                return msg.NotifyAsync(null, "", NotificationType.Email, "", null).GetAwaiter().GetResult();
            }
        }

        private bool SendSelfRegistrationEmailToUser(SelfRegistrationRequest userRequest, bool sendToAltEmail)
        {
            string emailBody = "Please follow the link below to complete your account registration.";
            string createAccountLinkText = "Click here to complete registration";

            using (ContentService content = new ContentService(userRequest.CustomerId, userRequest.EmployerId, null))
            {
                string createAccountCustomContent = content.GetValueByKey("ESS.SelfRegister.Email.CreateAccount");
                if (!string.IsNullOrWhiteSpace(createAccountCustomContent))
                    emailBody = createAccountCustomContent;

                string createAccountLinkCustomContent = content.GetValueByKey("ESS.SelfRegister.Email.CreateAccount.LinkText");
                if (!string.IsNullOrWhiteSpace(createAccountLinkCustomContent))
                    createAccountLinkText = createAccountLinkCustomContent;
            }

            return TemplateAndSendSelfRegistrationEmail(NotificationApiHelper.EmailTemplate_SelfAccountCreationEmail, "Complete Registration", emailBody, userRequest, createAccountLinkText, sendToAltEmail);
        }

        private void SendUserExistsEmailToUser(User user)
        {
            string emailBody = "Your account already exists. Please try logging in.  If you are unable to login, you can reset your password by clicking 'Forgot Password' on the login page.";
            using (ContentService content = new ContentService(user.CustomerId, user.EmployerId, null))
            {
                string userExistsCustomContent = content.GetValueByKey("ESS.SelfRegister.Email.UserExistsToUser");
                if (!string.IsNullOrWhiteSpace(userExistsCustomContent))
                    emailBody = userExistsCustomContent;
            }

            TemplateAndSendSelfRegistrationEmail(NotificationApiHelper.EmailTemplate_UserExistsEmailToUser, "Your account already exists", emailBody, user);
        }

        private void SendEmployeeExistsEmailToAdmin(string email, string clientIpAddress)
        {
            string emailBodyToAdmin =
                string.Format(
                    "There is more than one employee with email address {0}.  The system does not know which employee is trying to register. The request came from IP address {1}",
                    email, clientIpAddress);

            using (ContentService content = new ContentService(CustomerId, EmployerId, null))
            {
                string customMessageToAdmin = content.GetValueByKey("ESS.SelfRegister.Email.EmployeeNotUnique");
                if (!string.IsNullOrEmpty(customMessageToAdmin))
                    emailBodyToAdmin = customMessageToAdmin;
            }

            TemplateAndSendSelfRegistrationEmail(NotificationApiHelper.EmailTemplate_EmployeeNotUniqueEmailToAdmin, "More than one employee exists with the same email", emailBodyToAdmin, CurrentCustomer);
        }

        private void SendUserExistsEmailToAdmin(User user, string clientIpAddress)
        {
            string emailBodyToAdmin = string.Format("An existing user, {0} {1}, tried to register from IP Address: {2})", user.FirstName, user.LastName, clientIpAddress);

            using (ContentService content = new ContentService(user.CustomerId, user.EmployerId, null))
            {
                string customMessageToAdmin = content.GetValueByKey("ESS.SelfRegister.Email.UserExistsToAdmin");
                if (!string.IsNullOrEmpty(customMessageToAdmin))
                    emailBodyToAdmin = customMessageToAdmin;
            }

            TemplateAndSendSelfRegistrationEmail(NotificationApiHelper.EmailTemplate_SelfRegistrationEmailToAdmin, "Existing User Tried To Register", emailBodyToAdmin, user.Customer);
        }

        private void SendNonMatchingEmployeeEmailToAdmin(string email, string clientIpAddress)
        {
            // No known admin to send email to
            if (CurrentCustomer == null)
                return;

            string emailBodyToAdmin = string.Format("A user with the email {0} did not match any employee, but a registration was attempted from IP address {1}", email, clientIpAddress);
            using (ContentService content = new ContentService(CustomerId, EmployerId, null))
            {
                string customMessageToAdmin = content.GetValueByKey("ESS.SelfRegister.Email.NoMatchEmployeeToAdmin");
                if (!string.IsNullOrWhiteSpace(customMessageToAdmin))
                {
                    emailBodyToAdmin = customMessageToAdmin;
                }
            }

            TemplateAndSendSelfRegistrationEmail(NotificationApiHelper.EmailTemplate_SelfRegistrationEmailToAdmin, "User with no matching email tried to register", emailBodyToAdmin, CurrentCustomer);
        }

        /// <summary>
        /// Checks whether a user exists in the database, deleted or not, since a soft deleted user will still prevent a user from being created
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool UserExists(string email)
        {
            email = email.ToLowerInvariant();
            return User.Repository.AsQueryable().Where(u => u.Email == email).Count() > 0;
        }        
    }
}
