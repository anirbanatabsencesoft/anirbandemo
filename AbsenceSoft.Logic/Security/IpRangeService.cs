﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Security
{
    public class IpRangeService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeService"/> class.
        /// </summary>
        public IpRangeService() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public IpRangeService(User currentUser) : base(currentUser) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public IpRangeService(Customer customer = null, Employer employer = null, User u = null) : base(customer, employer, u) { }

        /// <summary>
        /// Returns a list of all IpRanges for the current customer
        /// </summary>
        /// <returns></returns>
        public List<IpRange> GetAllIpRangesForCustomer()
        {
            using (new InstrumentationContext("IpRangeService.GetAllIpRangesForEmployer"))
            {
                return IpRange.AsQueryable().Where(ip => ip.CustomerId == CustomerId).OrderBy(d => d.Name).ToList();
            }
        }

        /// <summary>
        /// Returns an IpRange by the Id
        /// </summary>
        /// <param name="ipRangeId"></param>
        /// <returns></returns>
        public IpRange GetIpRangeById(string ipRangeId)
        {
            using (new InstrumentationContext("IpRangeService.GetIpRangeById"))
            {
                return IpRange.GetById(ipRangeId);
            }
        }

        /// <summary>
        /// Checks whether there are any existing IP Ranges
        /// </summary>
        /// <returns></returns>
        public bool CheckExistingIpRanges()
        {
            var results = IpRange.AsQueryable().Where(ip => ip.CustomerId == CustomerId && ip.IsEnabled == true);

            if (results == null || results.Count() == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Determines whether [is ip in ranges] [the specified ip address].
        /// </summary>
        /// <param name="ipAddress">The ip address.</param>
        /// <returns>
        ///   <c>true</c> if [is ip in ranges] [the specified ip address]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsIpInRanges(string ipAddress)
        {
            var ips = ipAddress.Split('.');

            if (ips.Length != 4)
                return false;

            short ip1 = short.Parse(ips[0]);
            short ip2 = short.Parse(ips[1]);
            short ip3 = short.Parse(ips[2]);
            short ip4 = short.Parse(ips[3]);

            Func<IpRange, bool> predicate = null;
            predicate = (d => d.CustomerId == CustomerId
                && (d.IsEnabled ?? false)
                && d.Ip1 == ip1
                && d.Ip2 == ip2
                && d.Ip3 == ip3
                && (d.Ip41 <= ip4 && d.Ip42 >= ip4 || d.Ip41 >= ip4 && d.Ip42 <= ip4)
                );

            var result = IpRange.AsQueryable().Where(predicate).FirstOrDefault();

            if (result == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Upserts the IpRange.
        /// </summary>
        /// <param name="ipRangeId"></param>
        /// <param name="name"></param>
        /// <param name="ip1"></param>
        /// <param name="ip2"></param>
        /// <param name="ip3"></param>
        /// <param name="ip41"></param>
        /// <param name="ip42"></param>
        /// <param name="isEnabled"></param>
        /// <returns></returns>
        public IpRange UpsertIpRange(string ipRangeId, string name, short? ip1, short? ip2, short? ip3, short? ip41, short? ip42, bool? isEnabled)
        {

            using (new InstrumentationContext("IpRangeService.UpsertIpRange"))
            {
                IpRange range = IpRange.GetById(ipRangeId);
                if (range == null)
                {
                    range = new IpRange()
                    {
                        CustomerId = CustomerId,
                    };
                }
                range.Name = name;
                range.Ip1 = ip1;
                range.Ip2 = ip2;
                range.Ip3 = ip3;
                range.Ip41 = ip41;
                range.Ip42 = ip42;
                range.IsEnabled = isEnabled;

                return range.Save();
            }
        }

        /// <summary>
        /// Removes the IP Range.
        /// </summary>
        /// <param name="ipRangeId">The necessity identifier.</param>
        public void RemoveIpRange(string ipRangeId)
        {
            IpRange ipRange = IpRange.GetById(ipRangeId);
            if (ipRange == null)
                return;
            ipRange.Delete();
        }

    }
}
