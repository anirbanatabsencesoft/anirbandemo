﻿using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic.Security
{
    public class SelfRegistration
    {
        public string Email { get; set; }
        public string EmployerId { get; set; }
        public string CheckEMailMessage { get; set; }
        public string ResendEMailMessage { get; set; }
        public string ResendToAltEMailMessage { get; set; }
        public string DisabledUserMessage { get; set; }
        public string UserExistsMessage { get; set; }
        public string EmployeeNotFoundMessage { get; set; }
        public string ErrorSendingEmailMessage;
        public RegistrationStatus RegistrationStatus { get; set; }
        public bool AltEmailExists { get; set; }
        public string VerificationKey { get; set; }
    }
}
