﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Security
{
    public class WelcomeEmailMessageData
    {
        public string Email { get; set; }

        public string Name { get; set; }

        // public string Size { get; set; }

        public string CustomerSiteUrl { get; set; }

        public string SiteUrl { get; set; }
    }
}
