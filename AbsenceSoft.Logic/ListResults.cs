﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AbsenceSoft.Logic
{
    [Serializable]
    public sealed class ListResults
    {
        private DateTime _init;
        private IEnumerable<ListResult> _results;
        private long _total;

        public ListResults()
        {
            _init = DateTime.UtcNow;
            Total = 0;
            Elapsed = 0;
            Results = new List<ListResult>();
        }
        public ListResults(ListCriteria criteria) : this() { Criteria = criteria; }

        /// <summary>
        /// Gets or sets the original list criteria that was used to generate the list result.
        /// </summary>
        public ListCriteria Criteria { get; set; }

        /// <summary>
        ///  Gets or sets the total number of results that match the criteria.
        /// </summary>
        public long Total
        {
            get { return _total; }
            set
            {
                _total = value;
                MarkTime();
            }
        }

        /// <summary>
        /// Gets or sets the total time elapsed in miliseconds that the list generation took.
        /// </summary>
        public double Elapsed { get; set; }

        /// <summary>
        /// Gets or sets the list of results themselves for display/usage
        /// </summary>
        public IEnumerable<ListResult> Results
        {
            get
            {
                if (_results == null)
                    _results = new List<ListResult>();
                return _results;
            }
            set
            {
                _results = value;
                MarkTime();
            }
        }

        /// <summary>
        /// Updates the elapsed time in milliseconds for completion.
        /// </summary>
        public void MarkTime()
        {
            this.Elapsed = this._init.Elapsed();
        }
    }

    /// <summary>
    /// JSON sexiness for C# :-).
    /// </summary>
    [Serializable]
    public sealed class ListResult
    {
        public ListResult()
        {
            ExtensionData = new Dictionary<string, object>();
        }
        public ListResult(object anonymousType) : this()
        {
            if (anonymousType != null)
                foreach (var prop in anonymousType.GetType().GetProperties())
                    ExtensionData.Add(prop.Name, prop.GetValue(anonymousType));
        }

        /// <summary>
        /// Gets or sets the extension data.
        /// </summary>
        [JsonExtensionData]
        public Dictionary<string, object> ExtensionData { get; set; }

        /// <summary>
        /// Gets a filter value based on the filter name of a certain type, or the defualt value
        /// of that type if the name is not found in the filter object graph.
        /// </summary>
        /// <typeparam name="T">The type of the return value expected by the property name.</typeparam>
        /// <param name="name">The name of the filter property to get the value of</param>
        /// <returns>A strongly typed value of type T for the given named property, or default(T) if name is not found.</returns>
        /// <exception cref="System.InvalidCastException">Thrown when the value specified by name is of a difference type than the type of T expected.</exception>
        public T Get<T>(string name)
        {
            if (!ExtensionData.ContainsKey(name))
                return default(T);

            var obj = ExtensionData[name];
            return (T)obj;
        }//Filter<T>

        /// <summary>
        /// Fluid interface for setting values inline.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ListResult Set(string name, object value)
        {
            this[name] = value;
            return this;
        }

        /// <summary>
        /// Gets or sets the extension data property as a shortcut by name (safe).
        /// </summary>
        /// <param name="name">The name of the property to get or set</param>
        /// <returns>The value of that extension data property, or null</returns>
        public object this[string name]
        {
            get { return ExtensionData.ContainsKey(name) ? ExtensionData[name] : null; }
            set
            {
                if (value == null)
                    ExtensionData.Remove(name);
                else
                    ExtensionData[name] = value;
            }
        }
    }
}
