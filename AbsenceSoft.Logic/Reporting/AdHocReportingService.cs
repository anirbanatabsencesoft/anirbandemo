﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using AbsenceSoft.Common.Postgres;
using AbsenceSoft.Common.Serializers;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Reporting
{
    public class AdHocReportingService : LogicService
    {
        private const string DataWarehouse = "DataWarehouse";

        /// <summary>
        /// Initializes a new instance of the <see cref="AdHocReportingService"/> class.
        /// </summary>
        public AdHocReportingService()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdHocReportingService"/> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public AdHocReportingService(User currentUser)
            : base(currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdHocReportingService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public AdHocReportingService(string customerId, string employerId, User currentUser = null)
            : base(customerId, employerId, currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdHocReportingService"/> class.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="u">The u.</param>
        public AdHocReportingService(Customer customer, Employer employer, User u)
            : base(customer, employer, u)
        {

        }

        /// <summary>
        /// Gets all saved report definitions for the current user.
        /// </summary>
        /// <returns>An enumeration of ReportDefinition.</returns>
        public List<ReportDefinition> GetSavedReportDefinitions()
        {
            using (new InstrumentationContext("AdHocReportingService.GetSavedReportDefinitions"))
            {
                return ReportDefinition.AsQueryable(CurrentUser).Where(r => r.IsDeleted != true).ToList();
            }
        }

        /// <summary>
        /// Saves the definition of a report.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to save.</param>
        /// <param name="name">The new name to save the report as.</param>
        /// <returns>The report definition.</returns>
        public ReportDefinition SaveReportDefinition(ReportDefinition reportDefinition, string name)
        {
            if (reportDefinition == null)
                throw new ArgumentNullException("reportDefinition");

            using (new InstrumentationContext("AdHocReportingService.SaveReportDefinition"))
            {
                reportDefinition.CustomerId = CurrentUser.CustomerId;
                reportDefinition.Name = name;
                reportDefinition.UserId = CurrentUser.Id;
                return reportDefinition.Save();
            }
        }

        /// <summary>
        /// Saves the report under a new name. This method clones the existing report and assigns a new object id.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to duplicate and change the name of.</param>
        /// <param name="name">The new name to save the report as.</param>
        /// <returns>The report definition.</returns>
        public ReportDefinition SaveAsReportDefinition(ReportDefinition reportDefinition, string name)
        {
            using (new InstrumentationContext("AdHocReportingService.SaveAsReportDefinition"))
            {
                var reportDefinitionClone = reportDefinition.Clone();
                reportDefinitionClone.Clean();
                reportDefinitionClone.Name = name;
                reportDefinitionClone.CustomerId = CurrentCustomer.Id;
                reportDefinitionClone.UserId = CurrentUser.Id;
                reportDefinition.CustomerId = CurrentCustomer.Id;
                reportDefinition.UserId = CurrentUser.Id;
                return reportDefinitionClone.Save();
            }
        }

        /// <summary>
        /// Returns a list of valid report type values.
        /// </summary>
        /// <returns>A list of ReportType.</returns>
        public List<ReportType> GetReportTypes()
        {
            return ReportType.AsQueryable().ToList();
        }

        /// <summary>
        /// Returns a report type by identifier.
        /// </summary>
        /// <param name="reportTypeId">The report type identifier to fetch.</param>
        /// <returns>A ReportType.</returns>
        public ReportType GetReportTypeById(string reportTypeId)
        {
            return ReportType.GetById(reportTypeId);
        }

        /// <summary>
        /// Gets a list of fields valid for a specific report type identifier.
        /// </summary>
        /// <param name="reportTypeId">The report type identifier to fetch.</param>
        /// <returns>A list of ReportField.</returns>
        public List<ReportField> GetFields(string reportTypeId)
        {
            var fields = ReportTypeField.AsQueryable()
                .Where(p => p.ReportTypeId == reportTypeId)
                .Select(p => p.FieldName)
                .ToList();
            return ReportField.AsQueryable(CurrentUser)
                .Where(f => fields.Contains(f.Name))
                .OrderBy(f => f.Id)
                .ToList();
        }

        /// <summary>
        /// Gets the field filter baseed on the field filter template and populates any options/values for it.
        /// </summary>
        /// <param name="reportTypeId">The report type identifier.</param>
        /// <param name="fieldName">The report field name.</param>
        /// <returns>A populated report filter for use by the UI.</returns>
        public ReportFilter GetFieldFilter(string reportTypeId, string fieldName)
        {
            // 1. Get the report filter
            var reportField = GetFields(reportTypeId).FirstOrDefault(p => p.Name == fieldName);
            if (reportField == null)
                return null;

            // 2. Get the filter from the field
            var reportFilter = reportField.Filter;
            // 3. Check if we need filter options
            var fetchOptions = false;
            switch (reportFilter.ControlType)
            {
                case ControlType.SelectList:
                case ControlType.RadioButtonList:
                case ControlType.CheckBoxList:
                    fetchOptions = true;
                    break;
            }
            // 4. Get some options for this field for this view
            if (fetchOptions)
            {
                // 5. Get the report type
                var reportType = ReportType.GetById(reportTypeId);
                // 6. Build distinct value query
                //    e.g. SELECT DISTINCT <field_name> FROM <report_type_key> WHERE cust_id = <customer_id> AND empl_id = <employer_id> ORDER BY <field_name>
                var args = BuildQueryArguments(reportField, reportType);
                // Note that \"%I\" = @0 does not get executed as a parameter until the second trip to the database.
                string query = BuildFormatQuery(reportType);

                // Run this query against PostgreSQL to get the list of values back
                var db = new CommandRunner(DataWarehouse);
                var formattedResult = db.ExecuteDynamic(query, args);
                string formattedQuery = formattedResult.First().query;
                var results = db.ExecuteDynamic(formattedQuery, CustomerId);
                var reportFilterOptions = new List<ReportFilterOption>();
                foreach (var result in results)
                {
                    reportFilterOptions.Add(
                        new ReportFilterOption
                        {
                            Text = result.value,
                            Value = result.value,
                            GroupText = string.Format("{0}_{1}", reportType.Key, reportField.Name)
                        });
                }
                reportFilter.Options = reportFilterOptions;
            }

            // 7. Return the filter
            return reportFilter;
        }

        private object[] BuildQueryArguments(ReportField field, ReportType type)
        {
            int arraySize = 3 + CurrentUser.Employers.Count;
            var args = new object[arraySize];
            args[0] = field.Name;
            args[1] = type.Key;
            args[2] = CustomerId;
            for (int i = 0; i < CurrentUser.Employers.Count; i++)
            {
                args[3 + i] = CurrentUser.Employers[i].EmployerId;
            }
            return args;
        }

        private string BuildFormatQuery(ReportType type)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("select format('");
            queryString.Append("select distinct \"%I\" as value from \"%I\" where cust_id = %L");
            if (type.IsEmployer)
            {
                queryString.Append(" and (eplr_id is null or eplr_id in (");
                foreach (var employer in CurrentUser.Employers)
                {
                    queryString.Append("%L");
                    if (CurrentUser.Employers.Last() != employer)
                        queryString.Append(", ");

                }
                queryString.Append("))");
            }
            queryString.Append(" order by \"%I\" asc', ");
            queryString.Append("variadic array[");
            queryString.Append("@0, @1, @2");
            if (type.IsEmployer)
            {
                queryString.Append(", ");
                for (int i = 0; i < CurrentUser.Employers.Count; i++)
                {
                    queryString.Append(string.Format("@{0}", i + 3));
                    if (i != CurrentUser.Employers.Count - 1)
                        queryString.Append(", ");
                }
            }
            queryString.AppendFormat(", @0");
            queryString.Append("]) as query;");

            return queryString.ToString();
        }

        // TODO: Figure out if we need this?
        //public string GetCriteriaBuilderForField()
        //{
        //    return string.Empty;
        //}

        /// <summary>
        /// Builds a query to be run against a context.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to build.</param>
        /// <param name="reportFields">The list of report fields to grab.</param>
        /// <param name="user">The user building the query.</param>
        /// <param name="inherentLimit">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <param name="inherentOffset">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <returns>An AdHocQueryBuilderResult from the report definition.</returns>
        internal AdHocQueryBuilderResult BuildQuery(
            ReportDefinition reportDefinition,
            List<ReportField> reportFields,
            User user,
            int? inherentLimit = null,
            int? inherentOffset = null)
        {
            return AdHocQueryBuilder.Build(reportDefinition, reportFields, user, inherentLimit, inherentOffset);
        }

        /// <summary>
        /// Exeuctes a query against a given context.
        /// </summary>
        /// <param name="query">The query to run against the context.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>A list of DynamicAwesome.</returns>
        internal List<DynamicAwesome> ExecuteQuery(string query, params object[] args)
        {
            return ExecuteQueryLazily(query, args).ToList();
        }

        /// <summary>
        /// Executes the query lazily.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        internal IEnumerable<DynamicAwesome> ExecuteQueryLazily(string query, params object[] args)
        {
            var db = new CommandRunner(DataWarehouse);
            return db.ExecuteDynamicAwesome(query, args);
        }

        /// <summary>
        /// Formats the dynamic result set.
        /// </summary>
        /// <param name="results">The initial results pulled back from a report to be formatted.</param>
        /// <returns>A list of DynamicAwesome.</returns>
        internal List<DynamicAwesome> FormatResults(List<DynamicAwesome> results)
        {
            var bsonNullConverter = new BsonNullConverter();
            foreach (var result in results)
                FormatResult(result, bsonNullConverter);
            return results;
        }

        /// <summary>
        /// Formats the dynamic result from the set.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="bsonNullConverter">The bson null converter.</param>
        /// <returns></returns>
        internal DynamicAwesome FormatResult(DynamicAwesome result, BsonNullConverter bsonNullConverter = null)
        {
            bsonNullConverter = bsonNullConverter ?? new BsonNullConverter();
            foreach (var key in result.Keys)
            {
                var value = result[key];
                if (bsonNullConverter.CanConvert(value.GetType()))
                    result[key] = string.Empty;
            }
            return result;
        }

        /// <summary>
        /// Returns a report definition by the specified id.
        /// </summary>
        /// <param name="reportId">The report identifier to fetch.</param>
        /// <returns>A ReportDefinition object.</returns>
        public ReportDefinition GetReportById(string reportId)
        {
            return ReportDefinition.GetById(reportId);
        }

        /// <summary>
        /// Builds and runs the report against the given context returning a dynamic result.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to run.</param>
        /// <param name="reportFields">Optional report fields if different from ReportType.Fields</param>
        /// <param name="user">Optional user parameter to do impersonation.</param>
        /// <param name="inherentLimit">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <param name="inherentOffset">Used to limit the rows of the report, if an offset is not speicified in the report definition. Skipped if null, the default.</param>
        /// <returns>A dynamic result.</returns>
        public AdHocReportResult RunReport(
            ReportDefinition reportDefinition,
            List<ReportField> reportFields = null,
            User user = null,
            int? inherentLimit = null,
            int? inherentOffset = null)
        {
            if (reportDefinition == null)
                throw new ArgumentNullException("reportDefinition");

            if (user == null)
                user = CurrentUser;

            using (new InstrumentationContext("AdHocReportingService.RunReport"))
            {

                if (reportFields == null)
                    reportFields = GetFields(reportDefinition.ReportType.Id);

                var requestDate = DateTime.UtcNow;
                var adHocReportResult = new AdHocReportResult
                {
                    Name = reportDefinition.Name,
                    AdHocReportType = GetReportCategory(reportDefinition),
                    IconImage = null,
                    RequestDate = requestDate,
                    RequestedBy = user,
                    Success = true,
                    Error = null,
                    Items = null,
                    RecordCount = null
                };

                try
                {
                    // Build Query
                    var queryResult = BuildQuery(reportDefinition, reportFields, user, inherentLimit, inherentOffset);

                    // Get sql injection safe query
                    var db = new CommandRunner(DataWarehouse);
                    var formattedResult = db.ExecuteDynamic(queryResult.Query, queryResult.Arguments.ToArray());
                    var query = Convert.ToString(formattedResult.First().query);

                    // Record query for report.
                    adHocReportResult.Query = query;

                    // Execute sql injection safe query
                    List<DynamicAwesome> results = ExecuteQuery(query);
                    var formattedResults = FormatResults(results);

                    if (formattedResults.Any())
                    {
                        var actualResult = formattedResults.Select(p => p).ToList();
                        foreach (var itemObject in formattedResults)
                        {
                            if (!ValidRow(itemObject))
                            {
                                actualResult.Remove(itemObject);
                            }
                        }
                        
                        formattedResults = actualResult;
                    }

                    adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                    adHocReportResult.Items = formattedResults.Select(p => new AdHocResultData(p)).ToList();
                    adHocReportResult.RecordCount = formattedResults.Count;
                    return adHocReportResult;
                }
                catch (Exception ex)
                {
                    adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                    adHocReportResult.Success = false;
                    adHocReportResult.Error = ex.Message;
                    return adHocReportResult;
                }
            }
        }

        /// <summary>
        /// Checks if the passed object has all the properties containing values
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static bool ValidRow(object row)
        {
            return row.GetType().GetProperties().All(p =>
            {
                var values = ((AbsenceSoft.DynamicAwesome)row).Values.Select(j => j);

                foreach (var value in values)
                {   
                    if (value != null)
                    {
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(value)))
                        {
                            return true;
                        }
                        switch (Type.GetTypeCode(p.GetType()))
                        {
                            case TypeCode.Int16:
                            case TypeCode.Int32:
                            case TypeCode.Int64:
                            case TypeCode.Double:
                            case TypeCode.DateTime:
                            case TypeCode.Boolean:
                            case TypeCode.Decimal:
                            case TypeCode.Object:
                            case TypeCode.SByte:
                            case TypeCode.Single:
                            case TypeCode.UInt16:
                            case TypeCode.UInt32:
                            case TypeCode.UInt64:
                                return true;
                        }
                    }
                }

                return false;
            });
        }

        /// <summary>
        /// Builds and runs the report against the given context returning a dynamic result.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to run.</param>
        /// <param name="reportFields">Optional report fields if different from ReportType.Fields</param>
        /// <param name="user">Optional user parameter to do impersonation.</param>
        /// <param name="adHocReportResult">The ad hoc report result.</param>
        /// <param name="output">The output.</param>
        /// <returns>
        /// A dynamic result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">reportDefinition</exception>
        protected AdHocReportResult RunReportLazily(
            ReportDefinition reportDefinition,
            List<ReportField> reportFields,
            User user,
            Action<DynamicAwesome> output)
        {
            if (reportDefinition == null)
                throw new ArgumentNullException("reportDefinition");

            if (user == null)
                user = CurrentUser;

            using (new InstrumentationContext("AdHocReportingService.RunReport"))
            {

                if (reportFields == null)
                    reportFields = GetFields(reportDefinition.ReportType.Id);

                var requestDate = DateTime.UtcNow;

                AdHocReportResult adHocReportResult = new AdHocReportResult()
                {
                    Name = reportDefinition.Name,
                    AdHocReportType = GetReportCategory(reportDefinition),
                    IconImage = null,
                    RequestDate = requestDate,
                    RequestedBy = user,
                    Success = true,
                    Error = null,
                    Items = null,
                    RecordCount = null
                };

                try
                {
                    // Build Query
                    var queryResult = BuildQuery(reportDefinition, reportFields, user);

                    // Get sql injection safe query
                    var db = new CommandRunner(DataWarehouse);
                    var formattedResult = db.ExecuteDynamic(queryResult.Query, queryResult.Arguments.ToArray());
                    var query = Convert.ToString(formattedResult.First().query);

                    // Record query for report.
                    adHocReportResult.Query = query;

                    // Execute sql injection safe query
                    long total = 0L;
                    BsonNullConverter converter = new BsonNullConverter();
                    IEnumerable<DynamicAwesome> results = ExecuteQueryLazily(query);
                    foreach (var result in results)
                    {
                        FormatResult(result, converter);
                        output(result);
                        total++;
                    }

                    adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                    adHocReportResult.RecordCount = total;
                    return adHocReportResult;
                }
                catch (Exception ex)
                {
                    adHocReportResult.CompletedIn = DateTime.UtcNow.ToUnixDate() - requestDate.ToUnixDate();
                    adHocReportResult.Success = false;
                    adHocReportResult.Error = ex.Message;
                    return adHocReportResult;
                }
            }
        }

        /// <summary>
        /// Gets the report category.
        /// </summary>
        /// <param name="reportDefinition">The report definition.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">reportDefinition</exception>
        private string GetReportCategory(ReportDefinition reportDefinition)
        {
            if (reportDefinition == null)
                throw new ArgumentNullException("reportDefinition");

            if (reportDefinition.ReportType == null || reportDefinition.ReportType.AdHocReportType == null)
                return string.Empty;

            return Enum.GetName(typeof(AdHocReportType), reportDefinition.ReportType.AdHocReportType);
        }

        /// <summary>
        /// Gets the property list.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>A list of strings.</returns>
        private static List<string> GetPropertyList(DynamicObject item)
        {
            return item.GetDynamicMemberNames().ToList();
        }

        /// <summary>
        /// Gets the property list.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns>A list of strings.</returns>
        private static List<string> GetPropertyList(IEnumerable<DynamicObject> items)
        {
            return items.SelectMany(GetPropertyList).Distinct().ToList();
        }

        /// <summary>
        /// This method escap some chars from string
        /// </summary>
        /// <param name="value">string to be escaped from some chars</param>
        /// <returns>returns string with escaped chars</returns>
        private static string Escape(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return string.Empty;

            if (value.Contains(Quote))
                value = value.Replace(Quote, EscapedQuote);

            if (value.IndexOfAny(CharactersThatMustBeQuoted) > -1)
                value = string.Concat(Quote, value, Quote);

            return value;
        }

        private const string Quote = "\"";
        private const string EscapedQuote = "\"\"";
        private static readonly char[] CharactersThatMustBeQuoted = { ',', '"', '\n' };

        /// <summary>
        /// Exports the report in a different format.
        /// </summary>
        /// <param name="reportDefinition">The definition of the report to export.</param>
        /// <param name="user">Optional user parameter to do impersonation.</param>
        /// <param name="output">The output action that is called any time there is output written.</param>
        public AdHocReportResult ExportReportAsCsv(
            ReportDefinition reportDefinition,
            User user,
            Action<string> output)
        {
            using (new InstrumentationContext("AdHocReportingService.ExportReportAsCsv"))
            {
                var reportFields = GetFields(reportDefinition.ReportType.Id);
                var friendlyNames = reportFields.ToDictionary(p => p.Name, p => p);

                var reportCategory = GetReportCategory(reportDefinition);
                var reportName = reportDefinition.Name ?? string.Empty;
                var reportTitle = string.Empty;

                if (reportCategory.Length > 0 && reportName.Length > 0)
                    reportTitle = string.Format("{0}: {1}", reportCategory, reportName);
                else if (reportCategory.Length > 0 && reportName.Length == 0)
                    reportTitle = reportCategory;
                else if (reportCategory.Length == 0 && reportName.Length > 0)
                    reportTitle = reportName;

                StringBuilder writer = new StringBuilder();

                if (reportTitle.Length > 0)
                {
                    writer.Append(reportTitle);
                    writer.AppendLine();
                }

                if (reportDefinition.Filters != null && reportDefinition.Filters.Any())
                {
                    foreach (var filter in reportDefinition.Filters)
                    {
                        if (filter.Value != null &&
                            (!(filter.Value is string) || !string.IsNullOrWhiteSpace(filter.ValueAs<string>())))
                        {
                            writer.Append(friendlyNames[filter.FieldName].Display);
                            writer.Append(",");
                            writer.Append(ReportOperator.GetName(filter.Operator));
                            writer.Append(",");
                            ReportFilterOption opt = null;
                            if (filter.Options != null)
                            {
                                opt = filter.Options.FirstOrDefault(o => filter.Value.Equals(o.Value))
                                    ?? filter.Options.FirstOrDefault(o => filter.Value.ToString() == o.Value.ToString());
                            }
                            writer.Append(opt == null ? filter.Value : opt.Text);
                            writer.AppendLine();
                        }
                    }

                    writer.AppendLine().AppendLine();
                }

                var list = new List<string>();
                return RunReportLazily(reportDefinition, reportFields, user, result =>
                {
                    if (list.Count <= 0)
                    {
                        list = GetPropertyList(result);
                        writer.AppendLine(string.Join(",", list));
                        output(writer.ToString());
                        writer.Clear();
                    }

                    foreach (var col in list)
                    {
                        writer.Append(Escape((result[col] ?? "").ToString()));
                        writer.Append(",");
                    }

                    writer.AppendLine();
                    output(writer.ToString());
                    writer.Clear();
                });
            }
        }

        /// <summary>
        /// Gets the most recently ran reports for a customer.
        /// </summary>
        /// <param name="customer">The customer to find recently run reports for.</param>
        /// <param name="limit">The maxium limit of reports to grab.</param>
        /// <returns>ReportDefinition objects.</returns>
        public IEnumerable<ReportDefinition> GetMostRecentlyRanReports(Customer customer, int limit = 10)
        {
            using (new InstrumentationContext("AdHocReportingService.GetMostRecentlyRanReports"))
            {
                return ReportDefinition.AsQueryable(CurrentUser)
                    .Where(r => r.IsDeleted != true)
                    .OrderByDescending(p => p.ModifiedDate)
                    .Take(limit);
            }
        }
    }
}
