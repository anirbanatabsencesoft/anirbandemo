﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Necessitys
{
    public class NecessityService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityService"/> class.
        /// </summary>
        public NecessityService() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public NecessityService(User currentUser) : base(currentUser) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityService" /> class.
        /// </summary>
        /// <param name="currentCustomer">The current customer</param>
        /// <param name="currentEmployer">The current employer</param>
        /// <param name="currentUser">The current user</param>
        public NecessityService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public NecessityService(string customerId, string employerId, User currentUser) : base(customerId, employerId, currentUser) { }

        /// <summary>
        /// Gets a necessity list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults NecessityList(ListCriteria criteria)
        {
            using (new InstrumentationContext("NecessityService.NecessityList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                Dictionary<string, string> emps = new Dictionary<string, string>();
                ListResults results = new ListResults(criteria);

                string name = criteria.Get<string>("Name");
                string code = criteria.Get<string>("Code");
                string description = criteria.Get<string>("Description");
                string type = criteria.Get<string>("Type");

                List<IMongoQuery> ands = Necessity.DistinctAnds(CurrentUser, CustomerId, EmployerId);
                Necessity.Query.MatchesString(d => d.Name, name, ands);
                Necessity.Query.MatchesString(d => d.Description, description, ands);
                Necessity.Query.MatchesString(d => d.Code, description, ands);
                if (!string.IsNullOrWhiteSpace(type))
                {
                    NecessityType necessityType = NecessityType.MedicalDevice;
                    if (NecessityType.TryParse(type, out necessityType))
                    {
                        ands.Add(Necessity.Query.EQ(d => d.Type, necessityType));
                    }
                }

                List<Necessity> necessity = Necessity.DistinctAggregation(ands);
                results.Total = necessity.Count;
                necessity = necessity
                    .SortByListCriteria(criteria)
                    .PageByListCriteria(criteria)
                    .ToList();
                
                results.Results = necessity.Select(d => new ListResult()
                    .Set("Id", d.Id)
                    .Set("Name", d.Name)
                    .Set("Code", d.Code)
                    .Set("Type", d.Type.ToString().SplitCamelCaseString())
                    .Set("Description", d.Description)
                    .Set("CustomerId", d.CustomerId)
                    .Set("EmployerId", d.EmployerId)
                    .Set("EmployerName", string.IsNullOrWhiteSpace(d.EmployerId) || !emps.ContainsKey(d.EmployerId) ? null : emps[d.EmployerId])
                    .Set("ModifiedDate", d.ModifiedDate)
                );

                return results;
            }
        }

        public Necessity GetNecessity(string necessityId)
        {
            return Necessity.GetById(necessityId);
        }

        /// <summary>
        /// Returns a list of all necessities for a specified customer and employer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <returns></returns>
        public List<Necessity> GetAllNecessitiesForEmployer(string employerId = null)
        {
            using (new InstrumentationContext("NecessityService.GetAllNecessitiesForEmployer"))
            {
                Func<Necessity, bool> predicate = null;
                if ((employerId ?? EmployerId) == null)
                    predicate = (d => d.CustomerId == CustomerId && d.EmployerId == null);
                else
                    predicate = (d => d.CustomerId == CustomerId && (d.EmployerId == employerId || d.EmployerId == null));
                return Necessity.AsQueryable().Where(predicate).OrderBy(d => d.Type).ThenBy(d => d.Name).ToList();
            }
        }

        /// <summary>
        /// Upserts the necessity.
        /// </summary>
        /// <param name="necessityId">The code.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public Necessity SaveNecessity(Necessity necessity)
        {
            using (new InstrumentationContext("NecessityService.UpsertNecessity"))
            {
                Necessity savedNecessity = Necessity.GetById(necessity.Id);
                if(savedNecessity != null
                    && (!savedNecessity.IsCustom || savedNecessity.EmployerId != EmployerId))
                {
                    necessity.Clean();
                }

                necessity.CustomerId = CustomerId;
                necessity.EmployerId = EmployerId;
                return necessity.Save();
            }
        }

        /// <summary>
        /// Removes the necessity.
        /// </summary>
        /// <param name="necessityId">The necessity identifier.</param>
        public void RemoveNecessity(string necessityId)
        {
            Necessity necessity = Necessity.GetById(necessityId);
            if (necessity == null)
                return;
            necessity.Delete();
        }

        /// <summary>
        /// Gets the current employee necessity.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public List<EmployeeNecessity> GetEmployeeNecessities(Employee employee, DateTime? asOf = null)
        {
            using (new InstrumentationContext("NecessityService.GetEmployeeNecessities1"))
            {
                if (employee == null)
                    return null;

                var necessities = employee.GetNecessities();
                return GetEmployeeNecessities(necessities, asOf);
            }
        }

        /// <summary>
        /// Gets the current employee necessity.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <returns></returns>
        public List<EmployeeNecessity> GetEmployeeNecessities(Employee employee)
        {
            using (new InstrumentationContext("NecessityService.GetEmployeeNecessities3"))
            {
                if (employee == null)
                    return null;

                return employee.GetNecessities();
            }
        }

        /// <summary>
        /// Gets the employee necessities.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public List<EmployeeNecessity> GetEmployeeNecessities(string employeeNumber, string employerId = null, DateTime? asOf = null)
        {
            using (new InstrumentationContext("NecessityService.GetEmployeeNecessities2"))
            {
                if (string.IsNullOrWhiteSpace(employerId ?? EmployerId))
                    return null;

                var necessities = EmployeeNecessity.AsQueryable().Where(r => r.CustomerId == CustomerId && r.EmployerId == (employerId ?? EmployerId) && r.EmployeeNumber == employeeNumber).ToList();
                return GetEmployeeNecessities(necessities, asOf);
            }
        }

        /// <summary>
        /// Gets the current employee necessity.
        /// </summary>
        /// <param name="necessities">The necessities.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public List<EmployeeNecessity> GetEmployeeNecessities(List<EmployeeNecessity> necessities, DateTime? asOf = null)
        {
            using (new InstrumentationContext("NecessityService.GetEmployeeNecessities3"))
            {
                if (necessities == null || !necessities.Any())
                    return necessities;

                return necessities
                    .Where(j => j.Dates == null || j.Dates.DateInRange(asOf ?? DateTime.UtcNow.ToMidnight()))
                    .OrderByDescending(j => j.ModifiedDate)
                    .ToList();
            }
        }

        /// <summary>
        /// Removes the employee necessity.
        /// </summary>
        /// <param name="employeeNecessityId">The employee necessity identifier.</param>
        public void RemoveEmployeeNecessity(string employeeNecessityId)
        {
            using (new InstrumentationContext("NecessityService.RemoveEmployeeNecessity"))
            {
                var necessity = EmployeeNecessity.GetById(employeeNecessityId);
                if (necessity != null)
                    necessity.Delete();
            }
        }

        /// <summary>
        /// Updates the employee necessity.
        /// </summary>
        /// <param name="necessity">The necessity.</param>
        public void UpdateEmployeeNecessity(EmployeeNecessity necessity)
        {
            using (new InstrumentationContext("NecessityService.UpdateEmployeeNecessity"))
            {
                if (necessity == null)
                    return;
                necessity.Save();
                necessity.WfOnEmployeeNecessityChanged(CurrentUser);
            }
        }

        /// <summary>
        /// Changes the necessity for an employee.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="necessityId">The necessity identifier.</param>
        /// <param name="dates">The dates.</param>
        /// <param name="caseId">The case identifier, if applicable.</param>
        /// <exception cref="System.ArgumentNullException">employeeId</exception>
        /// <exception cref="AbsenceSoftException">Employee with _id was not found</exception>
        public void ChangeEmployeeNecessity(string employeeId, string necessityId, string Id, DateRange dates, string caseId = null)
        {
            using (new InstrumentationContext("NecessityService.ChangeEmployeeNecessity"))
            {
                if (string.IsNullOrWhiteSpace(employeeId))
                    throw new ArgumentNullException("employeeId");
                if (string.IsNullOrWhiteSpace(necessityId))
                    throw new ArgumentNullException("necessityId");

                if (dates.IsNull)
                    dates.StartDate = DateTime.UtcNow.ToMidnight();

                var emp = Employee.GetById(employeeId);
                if (emp == null)
                    throw new AbsenceSoftException(string.Format("Employee with _id {0} was not found", employeeId));
                var necessity = Necessity.GetById(necessityId);
                if (necessity == null)
                    return;

                var myNecessities = emp.GetNecessities();

                var curr = myNecessities.FirstOrDefault(n => !string.IsNullOrEmpty(Id) && n.Id == Id || string.IsNullOrEmpty(Id) && (n.NecessityId == necessityId && n.CaseId == caseId) && (n.Dates == null || n.Dates.DateRangesOverLap(dates)));
                if (curr != null)
                {
                    if (!string.IsNullOrEmpty(Id))
                    {
                        curr.Dates = dates;
                    }
                    else if (curr.Dates != null && curr.Dates != dates)
                    {
                        curr.Dates.StartDate = (curr.Dates.StartDate > dates.StartDate ? dates.StartDate : curr.Dates.StartDate);
                        curr.Dates.EndDate = ((curr.Dates.EndDate ?? DateTime.MaxValue) > (dates.EndDate ?? DateTime.MaxValue) ? curr.Dates.EndDate : dates.EndDate);
                    }
                    curr.CaseId = curr.CaseId ?? caseId;
                    curr.Name = necessity.Name;
                    curr.NecessityId = necessityId;

                    curr = curr.Save();
                }
                else
                {

                    curr = new EmployeeNecessity()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        Employee = emp,
                        EmployeeNumber = emp.EmployeeNumber,
                        NecessityId = necessity.Id,
                        Necessity = necessity,
                        Name = necessity.Name,
                        Dates = dates,
                        Type = necessity.Type,
                        Description = necessity.Description,
                        CaseId = caseId
                    }.Save();
                }

                curr.WfOnEmployeeNecessityChanged(CurrentUser);
            }
        }
    }
}
