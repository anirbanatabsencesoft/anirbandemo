﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Jobs
{
    public class WorkRestrictionService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRestrictionService"/> class.
        /// </summary>
        public WorkRestrictionService() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRestrictionService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public WorkRestrictionService(User currentUser) : base(currentUser) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRestrictionService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public WorkRestrictionService(string customerId, string employerId, User currentUser) : base(customerId, employerId, currentUser) { }
    }
}
