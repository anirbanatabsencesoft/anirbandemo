﻿using AbsenceSoft.Data.Jobs;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.Jobs.Contracts
{
    public interface IJobService
    {
        void UpdateEmployeeJob(EmployeeJob job, bool EndCurrentJob = false);

        IList<Job> GetJobs(string customerId, string employerId, string jobCode = null, bool inactive = false);
    }
}
