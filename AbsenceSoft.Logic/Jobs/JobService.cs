﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Jobs.Contracts;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Jobs
{
    public class JobService : LogicService, IJobService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobService"/> class.
        /// </summary>
        public JobService() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public JobService(User currentUser) : base(currentUser) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobService" /> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="currentUser">The current user.</param>
        public JobService(string customerId, string employerId, User currentUser) : base(customerId, employerId, currentUser) { }

        public JobService(Customer currentCustomer, Employer currentEmployer, User currentUser) : base(currentCustomer, currentEmployer, currentUser) { }

        /// <summary>
        /// Gets a job list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults JobList(ListCriteria criteria)
        {
            using (new InstrumentationContext("JobService.JobList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (CurrentUser != null)
                    ands.Add(CurrentUser.BuildDataAccessFilters(Permission.ViewEmployee));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");

                //Check if user is limited to one employer
                if (string.IsNullOrWhiteSpace(employerId) && !string.IsNullOrWhiteSpace(EmployerId))
                    employerId = EmployerId;

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Job.Query.EQ(e => e.EmployerId, employerId));

                string codeFilter = criteria.Get<string>("Code");
                string nameFilter = criteria.Get<string>("Name");

                if (!string.IsNullOrWhiteSpace(codeFilter))
                {
                    string term = Regex.Escape(codeFilter);
                    var searchExp = new BsonRegularExpression(new Regex("^" + term, RegexOptions.IgnoreCase));
                    ands.Add(Job.Query.Matches(e => e.Code, searchExp));
                }
                if (!string.IsNullOrWhiteSpace(nameFilter))
                {
                    string term = Regex.Escape(nameFilter);
                    var searchExp = new BsonRegularExpression(new Regex("^" + term, RegexOptions.IgnoreCase));
                    ands.Add(Job.Query.Matches(e => e.Name, searchExp));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = Job.Query.Find(ands.Count > 0 ? Job.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                // Build sort by (try to use our index as much as possible)
                query = criteria.SetSort(query);

                result.Total = query.Count();
                Dictionary<string, string> emps = Employer.AsQueryable().Where(e => e.CustomerId == CustomerId).ToDictionary(e => e.Id, e => e.Name);

                result.Results = query.ToList().Select(j => new ListResult()
                    .Set("Id", j.Id)
                    .Set("Code", j.Code)
                    .Set("Name", j.Name)
                    .Set("Description", j.Description)
                    .Set("Location", j.Organization == null ? "None" : j.Organization.Name)
                    .Set("EmployerId", j.EmployerId)
                    .Set("EmployerName", string.IsNullOrWhiteSpace(j.EmployerId) || !emps.ContainsKey(j.EmployerId) ? null : emps[j.EmployerId]));

                return result;
            }
        }

        public Job GetJob(string jobId)
        {
            return Job.GetById(jobId);
        }

        /// <summary>
        /// Performs a type-ahead quick search/find for jobs for the current customer/employer.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults JobQuickFind(ListCriteria criteria)
        {
            using (new InstrumentationContext("JobService.JobQuickFind"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string text = criteria.Get<string>("text");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if (CurrentUser != null)
                    ands.Add(CurrentUser.BuildDataAccessFilters(Permission.ViewEmployee));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");

                //Check if user is limited to one employer
                if (string.IsNullOrWhiteSpace(employerId) && !string.IsNullOrWhiteSpace(EmployerId))
                    employerId = EmployerId;

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Job.Query.EQ(e => e.EmployerId, employerId));

                // If the lastname and firstname are equal, then it's a "full name" filter, duh, so do an OR filter
                if (!string.IsNullOrWhiteSpace(text))
                {
                    var safeText = Regex.Escape(text);
                    var reggie = new BsonRegularExpression(new Regex("^" + safeText, RegexOptions.IgnoreCase));
                    ands.Add(Employee.Query.Or(
                        Job.Query.Matches(e => e.Code, reggie),
                        Job.Query.Matches(e => e.Name, reggie)
                    ));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                ands.Add(Job.Query.IsNotDeleted());
                var query = Job.Repository.Collection.FindAs<BsonDocument>(ands.Count > 0 ? Job.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize)
                    .SetFields(Fields.Include("_id", "Code", "Name"));

                // Build sort by (try to use our index as much as possible)
                var sortBy = SortBy.Ascending("CustomerId", "EmployerId");

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                    sortBy = criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                        ? sortBy.Ascending(criteria.SortBy)
                        : sortBy.Descending(criteria.SortBy);
                else
                    sortBy = sortBy.Ascending("Code");
                query = query.SetSortOrder(sortBy);

                result.Total = query.Count();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("id", e.GetRawValue<string>("_id"))
                    .Set("text", string.Concat(e.GetRawValue<string>("Code"), ", ", e.GetRawValue<string>("Name")))
                );

                return result;
            }
        } // JobQuickFind

        /// <summary>
        /// Gets the jobs for location.
        /// </summary>
        /// <param name="officeLocationCode">The office location code.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<Job> GetJobsForLocation(string officeLocationCode)
        {
            return Job.AsQueryable(CurrentUser)
                .Where(j => j.EmployerId == EmployerId && (j.OrganizationCode == null || j.OrganizationCode == officeLocationCode))
                .ToList();
        }

        /// <summary>
        /// Gets the current employee job.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public EmployeeJob GetCurrentEmployeeJob(Employee employee, DateTime? asOf = null)
        {
            if (employee == null)
                return null;

            var jobs = employee.GetJobs();
            return GetCurrentEmployeeJob(jobs, asOf);
        }

        /// <summary>
        /// Gets the current employee job.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public EmployeeJob GetCurrentEmployeeJob(string employeeNumber, string employerId = null, DateTime? asOf = null)
        {
            if (string.IsNullOrWhiteSpace(employerId ?? EmployerId))
                return null;

            var jobs = EmployeeJob.AsQueryable().Where(r => r.CustomerId == CustomerId && r.EmployerId == (employerId ?? EmployerId) && r.EmployeeNumber == employeeNumber).ToList();
            return GetCurrentEmployeeJob(jobs, asOf);
        }

        /// <summary>
        /// Gets the current employee job.
        /// </summary>
        /// <param name="jobs">The jobs.</param>
        /// <param name="asOf">As of date.</param>
        /// <returns></returns>
        public EmployeeJob GetCurrentEmployeeJob(List<EmployeeJob> jobs, DateTime? asOf = null)
        {
            if (jobs == null || !jobs.Any())
                return null;

            return jobs
                .Where(j => j.Dates == null || j.Dates.DateInRange(asOf ?? DateTime.UtcNow.ToMidnight()))
                .OrderByDescending(j => j.ModifiedDate)
                .FirstOrDefault();
        }

        /// <summary>
        /// Marks the employee job as pending.
        /// </summary>
        /// <param name="employeeJobId">The employee job identifier.</param>
        public void PendEmployeeJob(string employeeJobId)
        {
            var job = EmployeeJob.GetById(employeeJobId);
            if (job != null && (job.Status == null || job.Status.Value != AdjudicationStatus.Pending))
            {
                job.Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(AdjudicationStatus.Pending, null, CurrentUser);
                job.Save();
                job.WfOnEmployeeJobStatusChanged(CurrentUser);
            }
        }

        /// <summary>
        /// Approves the employee job.
        /// </summary>
        /// <param name="employeeJobId">The employee job identifier.</param>
        public void ApproveEmployeeJob(string employeeJobId)
        {
            var job = EmployeeJob.GetById(employeeJobId);
            if (job != null && (job.Status == null || job.Status.Value != AdjudicationStatus.Approved))
            {
                job.Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(AdjudicationStatus.Approved, null, CurrentUser);
                job.Save();
                job.WfOnEmployeeJobStatusChanged(CurrentUser);
            }
        }

        /// <summary>
        /// Denies the employee job.
        /// </summary>
        /// <param name="employeeJobId">The employee job identifier.</param>
        /// <param name="reason">The reason.</param>
        /// <param name="comments">The comments.</param>
        public void DenyEmployeeJob(string employeeJobId, JobDenialReason reason, string comments = null)
        {
            var job = EmployeeJob.GetById(employeeJobId);
            if (job != null && (job.Status == null || job.Status.Value != AdjudicationStatus.Denied))
            {
                job.Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(AdjudicationStatus.Denied, reason, CurrentUser, comments);
                job.Save();
                job.WfOnEmployeeJobStatusChanged(CurrentUser);
            }
            else
            if (job != null && job.Status != null && job.Status.Value == AdjudicationStatus.Denied && job.Status.Comments != comments)
            {
                job.Status.Comments = comments;
                job.Save();
            }

        }

        /// <summary>
        /// Removes the job.
        /// </summary>
        /// <param name="employeeJobId">The employee job identifier.</param>
        public void RemoveEmployeeJob(string employeeJobId)
        {
            var job = EmployeeJob.GetById(employeeJobId);
            if (job != null)
                job.Delete();
        }

        /// <summary>
        /// Updates the employee job.
        /// </summary>
        /// <param name="job">The job.</param>
        public void UpdateEmployeeJob(EmployeeJob job, bool EndCurrentJob = false)
        {
            if (job == null)
                return;

            var curr = GetCurrentEmployeeJob(job.EmployeeNumber, job.EmployerId);
            if (EndCurrentJob)
            {
                var currJob = curr == null ? GetCurrentEmployeeJob(job.EmployeeNumber, job.EmployerId, job.Dates.StartDate) : curr;
                if (currJob != null)
                {
                    currJob.Dates.EndDate = job.Dates.StartDate.AddDays(-1);
                    currJob.Save();
                }

            }
            job.Save();


            //LevelEmployeeJobs(job);
            job.WfOnEmployeeJobChanged(CurrentUser);

            if (curr != null && curr.Status != null && curr.Status.Value != job.Status.Value)
                job.WfOnEmployeeJobStatusChanged(CurrentUser);
        }

        /// <summary>
        /// Changes the job for an employee.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <param name="jobCode">The job identifier.</param>
        /// <param name="dates">The dates.</param>
        /// <param name="jobTitle">The job title.</param>
        /// <param name="caseId">The case identifier, if applicable.</param>
        /// <exception cref="System.ArgumentNullException">employeeId</exception>
        /// <exception cref="AbsenceSoftException">Employee with _id was not found</exception>
        public void ChangeEmployeeJobs(string employeeId, string jobCode, DateRange dates, string jobTitle, string caseId = null)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
                throw new ArgumentNullException("employeeId");
            if (dates.IsNull)
                dates.StartDate = DateTime.UtcNow.ToMidnight();

            var emp = Employee.GetById(employeeId);
            if (emp == null)
                throw new AbsenceSoftException(string.Format("Employee with _id {0} was not found", employeeId));
            var job = string.IsNullOrWhiteSpace(jobCode) ? null : Job.GetByCode(jobCode, emp.CustomerId, emp.EmployerId);

            var myJobs = emp.GetJobs();
            var curr = job == null ? null : myJobs.FirstOrDefault(j => j.JobCode == job.Code && (j.Dates == null || j.Dates.DateRangesOverLap(dates)));
            if (curr != null)
            {
                if (curr.Dates == dates) // no change
                    return;
                curr.Dates = dates;
                curr = curr.Save();
            }
            else
            {
                if (job == null)
                    return;

                var offices = emp.GetOfficeLocations();
                var myOffice = offices
                    .Where(o => o.Dates == null || o.Dates.DateRangesOverLap(dates))
                    .Where(o => job.OrganizationCode == o.Id)
                    .FirstOrDefault();

                curr = new EmployeeJob()
                {
                    CustomerId = emp.CustomerId,
                    EmployerId = emp.EmployerId,
                    Employee = emp,
                    EmployeeNumber = emp.EmployeeNumber,
                    JobCode = job.Code,
                    Job = job,
                    JobName = job.Name,
                    Dates = dates,
                    JobTitle = jobTitle,
                    OfficeLocationCode = myOffice == null ? null : myOffice.Code,
                    Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(AdjudicationStatus.Pending, null, CurrentUser),
                    CaseId = caseId
                }.Save();
            }
            emp.JobActivity = job.Activity;
            emp.Save();
            LevelEmployeeJobs(curr, myJobs);
            curr.WfOnEmployeeJobChanged(CurrentUser);
        }

        /// <summary>
        /// Levels the jobs.
        /// </summary>
        /// <param name="updated">The updated.</param>
        /// <param name="allJobs">All jobs.</param>
        protected virtual void LevelEmployeeJobs(EmployeeJob updated, List<EmployeeJob> allJobs = null)
        {
            if (updated == null)
                return;
            if (allJobs == null)
                allJobs = EmployeeJob.AsQueryable().Where(e => e.CustomerId == updated.CustomerId && e.EmployerId == updated.EmployerId && e.EmployeeNumber == updated.EmployeeNumber).ToList();
            var otherJobs = allJobs.Where(j => j.Id != updated.Id && (j.Dates == null || j.Dates.DateRangesOverLap(updated.Dates))).ToList();
            foreach (var overlap in otherJobs)
            {
                var o = overlap.Dates ?? DateRange.Null;
                var u = updated.Dates ?? DateRange.Null;
                if (o == u)
                    overlap.Delete();
                else if (o > u)
                {
                    if (u.IsPerpetual)
                        overlap.Delete();
                    else
                    {
                        overlap.Dates = o = new DateRange(u.EndDate.Value.AddDays(1), o.EndDate);
                        overlap.Save();
                    }
                }
                else
                {
                    overlap.Dates = o = new DateRange(o.StartDate, u.StartDate.AddDays(-1));
                    overlap.Save();
                }
            }
        }

        #region Bulk API

        /// <summary>
        /// Upserts the job using the MongoDB Bulk API.
        /// </summary>
        /// <param name="bulk">The bulk.</param>
        /// <param name="jobCode">The job code.</param>
        /// <param name="jobName">Name of the job.</param>
        public virtual void UpsertJob(BulkWriteOperation<Job> bulk, string jobCode, string jobName)
        {
            if (string.IsNullOrWhiteSpace(jobCode))
                return;

            jobCode = jobCode.ToUpperInvariant();

            bulk.Find(Job.Query.And(
                Job.Query.EQ(j => j.CustomerId, CustomerId),
                Job.Query.EQ(j => j.EmployerId, EmployerId),
                Job.Query.EQ(j => j.Code, jobCode)
            ))
            .Upsert()
            .UpdateOne(Job.Updates
                .SetOnInsert(j => j.CustomerId, CustomerId)
                .SetOnInsert(j => j.EmployerId, EmployerId)
                .SetOnInsert(j => j.Code, jobCode)
                .SetOnInsert(j => j.Name, jobName ?? jobCode)
                .SetOnInsert(j => j.CreatedById, CurrentUser.Id)
                .SetOnInsert(j => j.CreatedDate, DateTime.UtcNow)
                .SetOnInsert(j => j.ModifiedById, CurrentUser.Id)
                .SetOnInsert(j => j.ModifiedDate, DateTime.UtcNow));
        }

        /// <summary>
        /// Saves a job, and calls any necessary hooks
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public Job SaveJob(Job job)
        {
            if (job == null)
                throw new ArgumentNullException("Job can not be null");

            Job savedJob = GetJob(job.Id);
            if (savedJob != null && (!savedJob.IsCustom || savedJob.EmployerId != job.EmployerId))
            {
                job.Clean();
            }

            job.CustomerId = CustomerId;
            job.EmployerId = EmployerId;

            return job.Save();
        }

        /// <summary>
        /// Deletes a job and calls any necessary hooks
        /// </summary>
        /// <param name="jobId"></param>
        public void DeleteJob(string jobId)
        {
            Job job = Job.GetById(jobId);
            if (job != null)
                job.Delete();
        }

        /// <summary>
        /// Upserts the employee job.
        /// </summary>
        /// <param name="bulk">The bulk.</param>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="jobCode">The job code.</param>
        /// <param name="jobName">Name of the job.</param>
        /// <param name="jobTitle">The job title.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="officeLocationCode">The office location code.</param>
        /// <param name="jobPosition">The job position.</param>
        /// <param name="jobStatus">The job status.</param>
        public virtual void UpsertEmployeeJob(BulkWriteOperation<EmployeeJob> bulk, string employeeNumber, string jobCode, string jobName, string jobTitle, DateTime startDate, DateTime? endDate, string officeLocationCode, int? jobPosition, AdjudicationStatus jobStatus, bool alreadyExists)
        {
            if (string.IsNullOrWhiteSpace(jobCode) || string.IsNullOrWhiteSpace(employeeNumber))
                return;

            jobCode = jobCode.ToUpperInvariant();
            string jobCodeExists = null;

            if (alreadyExists)
            {                
                if(EmployeeJob.AsQueryable().Any(u => u.EmployerId == EmployerId && u.CustomerId == CustomerId && u.EmployeeNumber == employeeNumber && u.JobCode == jobCode && u.JobName == jobName && u.JobTitle == jobTitle && u.OfficeLocationCode == officeLocationCode && u.Dates.StartDate == startDate))
                {
                    return; 
                }
            }
            else
            {
                jobCodeExists = jobCode;
            }


            bulk.Find(EmployeeJob.Query.And(
            EmployeeJob.Query.EQ(j => j.CustomerId, CustomerId),
            EmployeeJob.Query.EQ(j => j.EmployerId, EmployerId),
            EmployeeJob.Query.EQ(j => j.EmployeeNumber, employeeNumber),
            EmployeeJob.Query.EQ(j => j.JobCode, jobCodeExists != null ? jobCode : null ),
            EmployeeJob.Query.EQ(j => j.JobPosition, jobPosition)
        ))
        .Upsert()
        .UpdateOne(EmployeeJob.Updates
            .SetOnInsert
            (j => j.CustomerId, CustomerId)
            .SetOnInsert(j => j.EmployerId, EmployerId)
            .SetOnInsert(j => j.EmployeeNumber, employeeNumber)
            .SetOnInsert(j => j.JobCode, jobCode)
            .SetOnInsert(j => j.JobName, jobName ?? jobCode)
            .Set(j => j.JobTitle, jobTitle)
            .Set(j => j.OfficeLocationCode, officeLocationCode)
            .SetOnInsert(j => j.JobPosition, jobPosition)
            .SetOnInsert(j => j.CreatedById, CurrentUser.Id)
            .SetOnInsert(j => j.CreatedDate, DateTime.UtcNow)
            .SetOnInsert(j => j.Status.Value, jobStatus)
            .SetOnInsert(j => j.Status.UserId, CurrentUser.Id)
            .SetOnInsert(j => j.Status.Username, CurrentUser.DisplayName)
            .SetOnInsert(j => j.Status.Date, DateTime.UtcNow)
            .SetOnInsert(j => j.Status.Id, Guid.NewGuid())
            .Set(j => j.Dates.StartDate, startDate)
            .Set(j => j.Dates.EndDate, endDate)
            .Set(j => j.ModifiedById, CurrentUser.Id)
            .Set(j => j.ModifiedDate, DateTime.UtcNow)
            .Set(j => j.IsDeleted, false));

            // Let's not level data just yet, we'll leave the logic to the logic layer, not the DB
            //LevelEmployeeJobs(bulk, employeeNumber, jobCode, startDate, endDate);
        }

        /// <summary>
        /// Levels the employee jobs by using the Bulk API.
        /// </summary>
        /// <param name="bulk">The bulk write operation.</param>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="jobCode">The job code for the newly set job.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        protected virtual void LevelEmployeeJobs(BulkWriteOperation<EmployeeJob> bulk, string employeeNumber, string jobCode, DateTime startDate, DateTime? endDate)
        {
            Func<Func<IEnumerable<IMongoQuery>>, IMongoQuery> queryBuilder = new Func<Func<IEnumerable<IMongoQuery>>, IMongoQuery>(q =>
            {
                List<IMongoQuery> ands = new List<IMongoQuery>()
                {
                    EmployeeJob.Query.EQ(j => j.CustomerId, CustomerId),
                    EmployeeJob.Query.EQ(j => j.EmployerId, EmployerId),
                    EmployeeJob.Query.EQ(j => j.EmployeeNumber, employeeNumber),
                    EmployeeJob.Query.NE(j => j.JobCode, jobCode)
                };
                var queries = q();
                if (queries.Any())
                    ands.AddRange(queries);
                return EmployeeJob.Query.And(ands);
            });

            // Nukes out the old(er) jobs that would be replaced by the new one
            bulk.Find(queryBuilder(() =>
            {
                List<IMongoQuery> subQueries = new List<IMongoQuery>();
                subQueries.Add(EmployeeJob.Query.EQ(j => j.Dates.StartDate, startDate));
                if (endDate.HasValue)
                    subQueries.Add(EmployeeJob.Query.Or(
                        EmployeeJob.Query.GTE(j => j.Dates.EndDate, endDate),
                        EmployeeJob.Query.EQ(j => j.Dates.EndDate, null)
                    ));
                return subQueries;
            })).Update(EmployeeJob.Updates
                .Set(j => j.IsDeleted, true)
                .Set(j => j.ModifiedById, CurrentUser.Id)
                .CurrentDate(j => j.ModifiedDate));

            // Updates the start date of the old one to come after the new end date
            if (endDate.HasValue)
                bulk.Find(queryBuilder(() =>
                {
                    List<IMongoQuery> subQueries = new List<IMongoQuery>();
                    subQueries.Add(EmployeeJob.Query.GTE(j => j.Dates.StartDate, startDate));
                    subQueries.Add(EmployeeJob.Query.GT(j => j.Dates.EndDate, endDate));
                    return subQueries;
                }))
                .UpdateOne(EmployeeJob.Updates
                    .Set(j => j.Dates.StartDate, endDate.Value.AddDays(1))
                    .Set(j => j.ModifiedById, CurrentUser.Id)
                    .CurrentDate(j => j.ModifiedDate));

            bulk.Find(queryBuilder(() =>
            {
                List<IMongoQuery> subQueries = new List<IMongoQuery>();
                subQueries.Add(EmployeeJob.Query.LT(j => j.Dates.StartDate, startDate));
                return subQueries;
            }))
            .UpdateOne(EmployeeJob.Updates
                .Set(j => j.Dates.EndDate, startDate.AddDays(-1))
                .Set(j => j.ModifiedById, CurrentUser.Id)
                .CurrentDate(j => j.ModifiedDate));
        }

        #endregion Bulk API

        /// <summary>
        /// Get jobs
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="employerId"></param>
        /// <param name="jobCode"></param>
        /// <param name="inactive"></param>
        /// <returns></returns>
        public IList<Job> GetJobs(string customerId, string employerId, string jobCode = null, bool inactive = false)
        {
            var jobs = Job.AsQueryable()
                .Where(j => j.CustomerId == customerId
                    && j.EmployerId == employerId);

            if (!string.IsNullOrEmpty(jobCode))
            {
                jobs = jobs.Where(j => j.Code == jobCode);
            }

            if (inactive)
            {
                jobs = jobs.Where(j => j.IsDeleted);
            }

            return jobs.ToList();
        }
    }
}
