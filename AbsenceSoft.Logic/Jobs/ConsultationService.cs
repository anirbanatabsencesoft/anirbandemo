﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Consultation
{
    public class ConsultationService : LogicService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsultationService" /> class.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        public ConsultationService(User currentUser) : base(currentUser) { }

        // Consultation Types:

        /// <summary>
        /// Gets the consultation types.
        /// </summary>
        public List<ConsultationType> GetConsultationTypes()
        {
            return ConsultationType.AsQueryable(CurrentUser)
                .Where(c => (c.EmployerId == this.EmployerId || c.EmployerId == null) && c.CustomerId == this.CustomerId)
                .ToList();
        }

        /// <summary>
        /// Gets the consultation types.
        /// </summary>
        public ListResults GetConsultationTypesResults(ListCriteria criteria)
        {
            using (new InstrumentationContext("ConsultationService.GetConsultationTypes"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);

                string consult = criteria.Get<string>("Consult");
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());
                if (!string.IsNullOrEmpty(consult))
                    ands.Add(ConsultationType.Query.Matches(o => o.Consult, new BsonRegularExpression(consult, "i")));

                var query = ConsultationType.Query.Find(ConsultationType.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(o => new ListResult()
                    .Set("Id", o.Id)
                    .Set("EmployerId", o.EmployerId)
                    .Set("Title", o.Consult)
                );

                return results;
            }
        }

        // Employee Consultations:

        /// <summary>
        /// Gets the employee consultations.
        /// </summary>
        /// <param name="employeeNumber">The employee id.</param>
        public List<EmployeeConsultation> GetEmployeeConsultations(string employeeNumber)
        {
            return EmployeeConsultation.AsQueryable(CurrentUser)
                .Where(c => (c.EmployerId == this.EmployerId || c.EmployerId == null) && c.CustomerId == this.CustomerId && c.EmployeeNumber == employeeNumber)
                .ToList();
        }

        /// <summary>
        /// Gets the employee consultations combined employer consultations.
        /// </summary>
        /// <param name="employeeId">The employee id.</param>
        public List<EmployeeConsultation> GetCombinedEmployeeConsultations(string employeeId)
        {
            var consultationTypes = GetConsultationTypes();
            var consultList = GetEmployeeConsultations(employeeId);

            // combine company ones with employee's existing ones (exlude types that already exist for the employee)
            var combinedList = consultList.Concat(consultationTypes.Where(ct => consultList.All(c => c.ConsultationTypeId != ct.Id))
                .Select(ct => new EmployeeConsultation() { ConsultationTypeId = ct.Id, Consult = ct.Consult })).ToList();

            return combinedList;
        }

        /// <summary>
        /// Gets the employee consultations.
        /// </summary>
        public ListResults GetCombinedEmployeeConsultationsResults(string employeeId, ListCriteria criteria)
        {
            using (new InstrumentationContext("ConsultationService.GetCombinedEmployeeConsultationsResults"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);

                string title = criteria.Get<string>("Title");
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());
                if (!string.IsNullOrEmpty(title))
                    ands.Add(ConsultationType.Query.Matches(o => o.Consult, new BsonRegularExpression(title, "i")));

                var query = ConsultationType.Query.Find(ConsultationType.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                var employeeConsultations = GetEmployeeConsultations(employeeId);

                results.Results = query.ToList().Select(ct => new ListResult()
                    .Set("Id", employeeConsultations.Any(ec => ec.ConsultationTypeId == ct.Id) ? employeeConsultations.FirstOrDefault(ec => ec.ConsultationTypeId == ct.Id).Id : "")
                    .Set("ConsultationTypeId", ct.Id)
                    .Set("EmployerId", ct.EmployerId)
                    .Set("Title", ct.Consult)
                    .Set("EmployeeNumber", employeeId)
                    .Set("Comments", employeeConsultations.Any(ec => ec.ConsultationTypeId == ct.Id) ? employeeConsultations.FirstOrDefault(ec => ec.ConsultationTypeId == ct.Id).Comments : "")
                );

                return results;
            }           
        }

        /// <summary>
        /// Deletes a consultation
        /// </summary>
        /// <param name="consultationType"></param>
        public void DeleteConsultation(ConsultationType consultationType)
        {
            if (consultationType == null)
                throw new ArgumentNullException("consultationType");

            consultationType.Delete();
        }

        /// <summary>
        /// Saves a consultation
        /// </summary>
        /// <param name="consultationType"></param>
        public ConsultationType SaveConsultation(ConsultationType consultationType)
        {
            if (consultationType == null)
                throw new ArgumentNullException("consultationType");

            return consultationType.Save();
        }

        /// <summary>
        /// Deletes an employee consultations
        /// </summary>
        /// <param name="matchingSavedConsultation"></param>
        public void DeleteEmployeeConsultation(EmployeeConsultation consultation)
        {
            if (consultation == null)
                throw new ArgumentNullException("consultation");

            consultation.Delete();
        }

        public EmployeeConsultation SaveEmployeeConsultation(EmployeeConsultation consultation)
        {
            if (consultation == null)
                throw new ArgumentNullException("consultation");

            bool runWorkflow = consultation.IsNew;
            consultation.Save();

            if(runWorkflow)
                consultation.WfOnEmployeeConsultCreated(CurrentUser);

            return consultation;
        }
    }
}
