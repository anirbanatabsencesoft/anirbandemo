﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Notes.Contracts;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft;


namespace AbsenceSoft.Logic.Tasks
{
    public class NotesService : LogicService, ILogicService, INotesService
    {
        public NotesService(User u = null)
            : base(u)
        {

        }

        public NotesService(Customer currentCustomer, Employer currentEmployer = null, User currentUser = null)
            : base(currentCustomer, currentEmployer, currentUser)
        {

        }

        public ListResults EmployeeNoteList(ListCriteria criteria)
        {
            using (new InstrumentationContext("NotesService.EmployeeNoteList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string employeeId = criteria.Get<string>("Id");
                long? category = criteria.Get<long?>("Category");
                string notes = criteria.Get<string>("Notes");
                long? noteDate = criteria.Get<long?>("NoteDate");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                //if (AbsenceSoft.Data.Security.User.Current != null)
                //    ands.Add(EmployeeNote.Query.EQ(e => e.EmployerId, AbsenceSoft.Data.Security.User.Current.EmployerId));


                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                if (!string.IsNullOrWhiteSpace(employeeId))
                    ands.Add(EmployeeNote.Query.EQ(e => e.EmployeeId, employeeId));

                if (category.HasValue)
                {
                    NoteCategoryEnum noteCategory = ((NoteCategoryEnum)category);
                    ands.Add(EmployeeNote.Query.EQ(m => m.Category, noteCategory));
                }

                if (!string.IsNullOrWhiteSpace(notes))
                {
                    ands.Add(EmployeeNote.Query.Matches(e => e.Notes, new BsonRegularExpression(notes, "i")));    //("^" + employeeFilter, "i")
                }

                if (noteDate.HasValue)
                {
                    ands.Add(EmployeeNote.Query.GTE(e => e.CreatedDate, new BsonDateTime(noteDate.Value)));
                    // Window is 24 hours
                    ands.Add(EmployeeNote.Query.LT(e => e.CreatedDate, new BsonDateTime(noteDate.Value + 86400000)));
                }

                List<string> userPermissions;
                if (Employer.Current != null)
                    userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
                else
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();

                // if a user has not a permission to "ViewConfidentialNotes" then show user's own notes otherwise show all notes.
                if (!userPermissions.Any(m => m == Permission.ViewConfidentialNotes))
                    ands.Add(CaseNote.Query.Or(CaseNote.Query.EQ(e => e.Public, true), CaseNote.Query.EQ(e => e.CreatedById, User.Current.Id)));

                var query = EmployeeNote.Query.Find(ands.Count > 0 ? EmployeeNote.Query.And(ands) : null)
                            .SetFields(EmployeeNote.Query.IncludeFields(
                                    e => e.Category,
                                    e => e.Categories,
                                    e => e.CreatedDate,
                                    e => e.Notes,
                                    e => e.CreatedById,
                                    e => e.Id
                                ));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLower() == "createddate")
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("cdt")
                            : SortBy.Descending("cdt"));
                    }
                    else
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending(criteria.SortBy)
                            : SortBy.Descending(criteria.SortBy));
                    }
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                result.Total = query.Count();

                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("Id", e.Id)
                    .Set("CreatedDate", e.CreatedDate)
                    .Set("Category", e.CategoryString)
                    .Set("Notes", e.Notes)
                    .Set("CreatedBy", (e.CreatedBy ?? User.System).DisplayName)
                    .Set("ModifiedDate", e.ModifiedDate)
                    .Set("ModifiedBy", (e.ModifiedBy).DisplayName)
                );

                return result;
            }                                       // using (new InstrumentationContext("EmployeeService.EmployeeList"))
        }

        /// <summary>
        /// Checks whether note category code already exists
        /// We need to pass in the CustomerId instead of using what is in context because the UI needs to tell whether we're looking at a core one or a customer one
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool NoteCategoryCodeIsInUse(string id, string customerId, string code)
        {
            return NoteCategory.AsQueryable().Count(nc => nc.Code == code && nc.Id != id && nc.EmployerId == EmployerId && nc.CustomerId == customerId) > 0;
        }

        public bool NoteCategoryHasChildren(NoteCategory category)
        {
            return NoteCategory.AsQueryable().Count(nc => nc.ParentCode != null && nc.ParentCode == category.Code) > 0;
        }

        /// <summary>
        /// Toggles a note category by code and employer/customer depending on what is most specific
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public NoteCategory ToggleNoteCategoryByCode(string code)
        {
            using (new InstrumentationContext("NoteService.ToggleNoteCategoryByCode"))
            {
                NoteCategory category = NoteCategory.GetByCode(code, CustomerId, EmployerId, true);
                category.ToggleSuppression(CurrentCustomer, CurrentEmployer);
                return category;
            }
        }

        public ListResults GetCaseNoteList(ListCriteria criteria)
        {
            using (new InstrumentationContext("NotesService.EmployeeNoteList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("Id");
                string[] caseIds = criteria.Get<string[]>("Ids");
                string employerId = criteria.Get<string>("EmployerId");
                long? category = criteria.Get<long?>("Category");
                string notes = criteria.Get<string>("Notes");
                long? noteDate = criteria.Get<long?>("NoteDate");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                //if (AbsenceSoft.Data.Security.User.Current != null)
                //    ands.Add(EmployeeNote.Query.EQ(e => e.EmployerId, AbsenceSoft.Data.Security.User.Current.EmployerId));

                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                if (caseIds != null && caseIds.Any())
                    ands.Add(CaseNote.Query.In(e => e.CaseId, caseIds));
                else if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(CaseNote.Query.EQ(e => e.CaseId, caseId));

                if (!string.IsNullOrWhiteSpace(employerId))
                {
                    ands.Add(CaseNote.Query.EQ(e => e.EmployerId, EmployerId));
                }

                // Fetch user permission
                List<string> userPermissions;
                if (Employer.Current != null)
                    userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
                else
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();

                ///If you can view confidential notes, we need to filter, otherwise, you can only see public notes or notes you created
                if (!userPermissions.Any(m => m == Permission.ViewConfidentialNotes))
                    ands.Add(CaseNote.Query.Or(CaseNote.Query.EQ(e => e.Public, true), CaseNote.Query.EQ(e => e.CreatedById, CurrentUser != null ? CurrentUser.Id : User.Current.Id)));


                if (category.HasValue)
                {
                    NoteCategoryEnum noteCategory = ((NoteCategoryEnum)category);
                    ands.Add(CaseNote.Query.EQ(m => m.Category, noteCategory));
                }

                if (!string.IsNullOrWhiteSpace(notes))
                {
                    ands.Add(CaseNote.Query.Matches(e => e.Notes, new BsonRegularExpression(notes, "i")));    //("^" + employeeFilter, "i")
                }

                if (noteDate.HasValue)
                {
                    ands.Add(CaseNote.Query.GTE(e => e.CreatedDate, new BsonDateTime(noteDate.Value)));
                    // Window is 24 hours
                    ands.Add(CaseNote.Query.LT(e => e.CreatedDate, new BsonDateTime(noteDate.Value + 86400000)));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = CaseNote.Query.Find(ands.Count > 0 ? CaseNote.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLower() == "createddate")
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("cdt")
                            : SortBy.Descending("cdt"));
                    }
                    else
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending(criteria.SortBy)
                            : SortBy.Descending(criteria.SortBy));
                    }
                }


                string userEmployeeId = null;

                if (AbsenceSoft.Data.Security.User.IsEmployeeSelfServicePortal)
                {
                    //userEmployeeId = AbsenceSoft.Data.Security.User.Current.Employers.Where(e => e.EmployeeId != null && e.IsActive).FirstOrDefault().EmployeeId;
                    userEmployeeId = CurrentUser != null ? CurrentUser.Id : AbsenceSoft.Data.Security.User.Current.Id;
                }

                result.Total = query.Count();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("Id", e.Id)
                    .Set("CaseId", e.CaseId)
                    .Set("CreatedDate", e.CreatedDate)
                    .Set("Category", e.CategoryString)
                    .Set("NoteCategoryEnum", e.Category)
                    .Set("Notes", e.Notes)
                    .Set("CreatedBy", SetCreatedBy(e))
                    .Set("Contact", e.Metadata.GetRawValue<string>("Contact"))
                    .Set("Answer", e.Metadata.GetRawValue<bool?>("Answer"))
                    .Set("SAnswer", e.Metadata.GetRawValue<string>("SAnswer"))
                    .Set("UserOwnData", User.IsEmployeeSelfServicePortal ? (e.CreatedById == userEmployeeId ? true : false) : true)
                    .Set("ModifiedDate", e.ModifiedDate)
                    .Set("ModifiedBy", e.ModifiedBy != null ? (e.ModifiedBy).DisplayName : null)

               );
                return result;
            }                                       // using (new InstrumentationContext("EmployeeService.EmployeeList"))
        }        
        /// <summary>
        /// Set the CreatedBy for notes. This can be currently logged in user or user from the N File load process
        /// </summary>
        /// <param name="caseNote"></param>
        /// <returns></returns>
        public string SetCreatedBy(CaseNote caseNote)
        {
            if (!string.IsNullOrEmpty(caseNote.EnteredByName) || caseNote.CreatedBy == null)
                return caseNote.EnteredByName;

            return caseNote.CreatedBy.DisplayName;
        }
        /// <summary>
        /// Get CaseNote By CaseNoteId
        /// </summary>
        /// <param name="caseNoteId"></param>
        /// <returns></returns>
        public CaseNote GetCaseNoteById(string caseNoteId)
        {
            using (new InstrumentationContext("NoteService.GetCaseNoteById"))
            {
                if (String.IsNullOrEmpty(caseNoteId))
                {
                    throw new ArgumentNullException("caseNoteId");
                }

                var caseNote = CaseNote.GetById(caseNoteId);

                return caseNote;
            }

        }

        #region Note Category Administration
        public List<NoteCategory> GetAllNoteCategories(EntityTarget? target = null, bool isEss = false, bool includeSuppressed = false)
        {
            IEnumerable<NoteCategory> noteCategories = NoteCategory.DistinctFind(null, CustomerId, EmployerId);
            if (target != null)
                noteCategories = noteCategories.Where(nc => (nc.Target & target.Value) == target.Value);

            if(!includeSuppressed)
                noteCategories = noteCategories.Where(nc => !nc.IsSuppressed(CurrentCustomer, CurrentEmployer)).ToList();

            List<NoteCategory> rootNoteCategories = noteCategories.Where(nc => string.IsNullOrEmpty(nc.ParentCode) && !IsHidden(nc, isEss)).ToList();
            List<NoteCategory> allNoteCategories = noteCategories.ToList();
            foreach (var category in rootNoteCategories)
            {
                category.ChildCategories = GetChildNoteCategories(category, allNoteCategories, isEss);
            }

            return rootNoteCategories;
        }

        /// <summary>
        /// Determines if this category should be hidden.  Will always return false if ESS is false, since we only hide note categories in ESS
        /// </summary>
        /// <param name="category"></param>
        /// <param name="isEss"></param>
        /// <returns></returns>
        private bool IsHidden(NoteCategory category, bool? isEss)
        {
            // If we aren't in ESS, it's not hidden
            if (!isEss.HasValue || !isEss.Value)
                return false;

            // Probably running a unit test, safe to assume this isn't hidden
            if (CurrentUser == null)
                return false;

            // if the category doesn't have a defined HideInESS value, or the value is false, it's not hidden
            if (!category.HideInESS.HasValue || !category.HideInESS.Value)
                return false;

            // If we don't have any roles defined, it's always hidden in ESS
            if (category.RoleIds == null || !category.RoleIds.Any())
                return true;

            // If they are a system admin, it's not hidden
            if (CurrentUser.Roles.Contains(Role.SystemAdministrator.Id))
                return false;

            // If any of the current users roles are in the categories roles, then it isn't hidden
            if (category.RoleIds.Intersect(CurrentUser.Roles).Any())
                return false;

            //If we've gotten this far it's safe to say it isn't hidden.  Should rarely be reached
            return false;
        }

        private List<NoteCategory> GetChildNoteCategories(NoteCategory currentCategory, List<NoteCategory> possibleChildren, bool? isEss)
        {
            List<NoteCategory> childNoteCategories = possibleChildren.Where(pc => pc.ParentCode == currentCategory.Code && !IsHidden(pc, isEss)).ToList();
            foreach (var childCategory in childNoteCategories)
            {
                childCategory.ChildCategories = GetChildNoteCategories(childCategory, possibleChildren, isEss);
            }

            return childNoteCategories;
        }

        public NoteCategory GetNoteCategoryById(string noteCategoryId)
        {
            using (new InstrumentationContext("NoteService.GetNoteCategoryById"))
            {
                return NoteCategory.AsQueryable().FirstOrDefault(nc => nc.Id == noteCategoryId);
            }
        }

        public NoteCategory SaveNoteCategory(NoteCategory category)
        {
            using (new InstrumentationContext("NoteService.SaveNoteCategory"))
            {
                if (category == null)
                    return null;

                NoteCategory savedCategory = NoteCategory.GetById(category.Id);

                if (savedCategory != null && (!savedCategory.IsCustom || savedCategory.EmployerId != EmployerId))
                    category.Clean();

                category.CustomerId = CustomerId;
                category.EmployerId = EmployerId;

                return category.Save();
            }
        }

        public void DeleteNoteCategory(NoteCategory category)
        {
            using (new InstrumentationContext("NoteService.DeleteNoteCategory"))
            {
                category.Delete();
            }
        }

        #endregion

        #region Note Template Administration

        public ListResults GetNoteTemplates(ListCriteria criteria)
        {
            using (new InstrumentationContext("NoteService.GetNoteTemplates"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();

                ListResults results = new ListResults(criteria);

                string name = criteria.Get<string>("Name");
                string description = criteria.Get<string>("Description");
                List<IMongoQuery> ands = new List<IMongoQuery>();
                ands.Add(CurrentUser.BuildDataAccessFilters());

                if (!string.IsNullOrEmpty(name))
                {
                    ands.Add(NoteTemplate.Query.Matches(nt => nt.Name, new BsonRegularExpression(name, "i")));
                }

                if (!string.IsNullOrEmpty(description))
                {
                    ands.Add(NoteTemplate.Query.Matches(nt => nt.Description, new BsonRegularExpression(description, "i")));
                }

                var query = NoteTemplate.Query.Find(NoteTemplate.Query.And(ands));
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    var sortOrder =
                        criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ?
                            SortBy.Ascending(criteria.SortBy) :
                            SortBy.Descending(criteria.SortBy);

                    query = query.SetSortOrder(sortOrder);
                }

                int skip = Math.Max(0, ((criteria.PageNumber - 1) * criteria.PageSize));
                query = query.SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                results.Total = query.Count();

                results.Results = query.ToList().Select(nt => new ListResult()
                    .Set("Id", nt.Id)
                    .Set("Name", nt.Name)
                    .Set("Description", nt.Description)
                );

                return results;
            }
        }

        public NoteTemplate GetNoteTemplateById(string templateId)
        {
            using (new InstrumentationContext("NoteService.GetNoteTemplateById"))
            {
                return NoteTemplate.GetById(templateId);
            }
        }

        public NoteTemplate SaveNoteTemplate(NoteTemplate template)
        {
            using (new InstrumentationContext("NoteService.SaveNoteTemplate"))
            {
                return template.Save();
            }
        }

        public void DeleteNoteTemplate(string customerId, string noteTemplateId)
        {
            using (new InstrumentationContext("NoteService.DeleteNoteTemplate"))
            {
                NoteTemplate template = NoteTemplate.GetById(noteTemplateId);
                template.Delete();
            }
        }

        #endregion


        /// <summary>
        /// Gets all note templates associated with the specific note category or any of this note categories parents
        /// </summary>
        /// <param name="noteCategoryCode"></param>
        /// <returns></returns>
        public List<NoteTemplate> GetNoteTemplatesByCategory(string noteCategoryCode)
        {


            List<string> codes = GetNoteCategoryCodes(noteCategoryCode);

            List<NoteTemplate> templates = NoteTemplate.AsQueryable()
                .Where(nt => nt.CustomerId == CurrentCustomer.Id).ToList();

            templates = codes != null ? templates.Where(nt => nt.Categories.Any(c => codes.Contains(c))).ToList():null;
            return templates;

        }

        public List<SavedCategory> CreateSavedNoteCategories(string savedNoteCategory, EntityTarget target)
        {
            List<NoteCategory> categories = GetAllNoteCategories(target);
            NoteCategory selectedCategory = GetSelectedNoteCategory(categories, savedNoteCategory);
            if (selectedCategory == null)
                return new List<SavedCategory>(0);

            List<NoteCategory> parentCategories = new List<NoteCategory>() { selectedCategory };
            if (!string.IsNullOrEmpty(selectedCategory.ParentCode))
                GetParentNoteCategory(parentCategories, selectedCategory, categories);
            parentCategories.Reverse();
            List<SavedCategory> savedCategories = new List<SavedCategory>(parentCategories.Count);
            foreach (var category in parentCategories)
            {
                savedCategories.Add(new SavedCategory()
                {
                    CategoryCode = category.Code,
                    CategoryName = category.Name,
                    Order = parentCategories.IndexOf(category) + 1
                });
            }
            return savedCategories;
        }

        private List<string> GetNoteCategoryCodes(string noteCategoryCode)
        {
            List<NoteCategory> categories = GetAllNoteCategories();
            NoteCategory selectedCategory = GetSelectedNoteCategory(categories, noteCategoryCode);
            /// Now lets walk the tree backwards (ugh) to get all codes
            List<NoteCategory> selectedCategories = new List<NoteCategory>();
            selectedCategories.Add(selectedCategory);
            GetParentNoteCategory(selectedCategories, selectedCategory, categories);
            if (selectedCategory == null)
            {
                return null;
            }
            return selectedCategories.Select(c => c.Code).ToList();
        }

        private void GetParentNoteCategory(List<NoteCategory> parentCategories, NoteCategory childNoteCategory, List<NoteCategory> categories)
        {
            NoteCategory parentCategory = GetSelectedNoteCategory(categories, childNoteCategory?.ParentCode);
            if (parentCategory != null)
            {
                parentCategories.Add(parentCategory);
                GetParentNoteCategory(parentCategories, parentCategory, categories);
            }
        }

        private NoteCategory GetSelectedNoteCategory(List<NoteCategory> categories, string noteCategoryCode)
        {
            foreach (var category in categories)
            {
                if (category.Code == noteCategoryCode)
                    return category;


                if (category.ChildCategories != null && category.ChildCategories.Count > 0)
                {
                    NoteCategory noteCategory = GetSelectedNoteCategory(category.ChildCategories, noteCategoryCode);
                    if (noteCategory != null)
                        return noteCategory;
                }

            }

            return null;
        }

        public NoteTemplate RenderTemplate(string entityId, string templateId, EntityTarget target)
        {
            NoteTemplate template = NoteTemplate.GetById(templateId);
            object entity = null;
            switch (target)
            {
                case EntityTarget.Case:
                    entity = EntityTarget.Case;
                    break;
                case EntityTarget.Employee:
                    entity = EntityTarget.Employee;
                    break;
                default:
                    throw new NotImplementedException();
            }
            if (template == null)
                return null;

            template.RenderedTemplate = AbsenceSoft.Rendering.Template.RenderTemplate(template.Template, entity);
            return template;
        }


        /// <summary>
        /// Saves a note and calls workflow on it
        /// </summary>
        /// <param name="note"></param>
        public CaseNote SaveNote(CaseNote note)
        {
            note.Save();
            note.WfOnCaseNoteCreated(CurrentUser);

            return note;
        }

        /// <summary>
        /// Delete the case note
        /// </summary>
        /// <param name="noteid"></param>
        public void DeleteCaseNote(string noteid)
        {
            if (string.IsNullOrEmpty(noteid))
                throw new ArgumentNullException();

            CaseNote caseNote = CaseNote.GetById(noteid);
            if (caseNote != null)
                caseNote.Delete();
        }

        /// <summary>
        /// Saves a note and without workflow on it
        /// </summary>
        /// <param name="note"></param>
        public CaseNote SaveCaseNote(CaseNote note)
        {
            note.Save();           

            return note;
        }

        /// <summary>
        /// Get Note text by certId
        /// </summary>
        /// <param name="CertId"></param>
        /// <returns></returns>
        public string GetCaseNoteByCertId(Guid CertId)
        {
            return CaseNote.AsQueryable().ToList(n => n.CertId == CertId).LastOrDefault()?.Notes;
        }

        /// <summary>
        /// Gets list of case note with provided criteria by context
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public ListResults GetCaseNoteListByContext(ListCriteria criteria)
        {
            using (new InstrumentationContext("NotesService.EmployeeNoteList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseId = criteria.Get<string>("Id");
                string[] caseIds = criteria.Get<string[]>("Ids");
                long? category = criteria.Get<long?>("Category");
                string notes = criteria.Get<string>("Notes");
                long? noteDate = criteria.Get<long?>("NoteDate");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                //if (AbsenceSoft.Data.Security.User.Current != null)
                //    ands.Add(EmployeeNote.Query.EQ(e => e.EmployerId, AbsenceSoft.Data.Security.User.Current.EmployerId));

                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                if (caseIds != null && caseIds.Any())
                    ands.Add(CaseNote.Query.In(e => e.CaseId, caseIds));
                else if (!string.IsNullOrWhiteSpace(caseId))
                    ands.Add(CaseNote.Query.EQ(e => e.CaseId, caseId));

                // Fetch user permission
                List<string> userPermissions;
                if (Employer.Current != null)
                    userPermissions = User.Permissions.EmployerPermissions(Employer.Current.Id).ToList();
                else
                    userPermissions = User.Permissions.ProjectedPermissions.ToList();

                ///If you can view confidential notes, we need to filter, otherwise, you can only see public notes or notes you created
                if (!userPermissions.Any(m => m == Permission.ViewConfidentialNotes))
                    ands.Add(CaseNote.Query.Or(CaseNote.Query.EQ(e => e.Public, true), CaseNote.Query.EQ(e => e.CreatedById, (CurrentUser ?? User.Current).Id)));

                if (category.HasValue)
                {
                    NoteCategoryEnum noteCategory = ((NoteCategoryEnum)category);
                    ands.Add(CaseNote.Query.EQ(m => m.Category, noteCategory));
                }

                if (!string.IsNullOrWhiteSpace(notes))
                {
                    ands.Add(CaseNote.Query.Matches(e => e.Notes, new BsonRegularExpression(notes, "i")));
                }

                if (noteDate.HasValue)
                {
                    ands.Add(CaseNote.Query.GTE(e => e.CreatedDate, new BsonDateTime(noteDate.Value)));
                    // Window is 24 hours
                    ands.Add(CaseNote.Query.LT(e => e.CreatedDate, new BsonDateTime(noteDate.Value + 86400000)));
                }

                var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                if (skip < 0) skip = 0;
                var query = CaseNote.Query.Find(ands.Count > 0 ? CaseNote.Query.And(ands) : null)
                    .SetSkip(skip)
                    .SetLimit(criteria.PageSize);

                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    if (criteria.SortBy.ToLower() == "createddate")
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending("cdt")
                            : SortBy.Descending("cdt"));
                    }
                    else
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                            ? SortBy.Ascending(criteria.SortBy)
                            : SortBy.Descending(criteria.SortBy));
                    }
                }

                string userEmployeeId = null;

                if (AbsenceSoft.Data.Security.User.IsEmployeeSelfServicePortal)
                {                    
                    userEmployeeId = AbsenceSoft.Data.Security.User.Current.Id;
                }

                result.Total = query.Count();
                result.Results = query.ToList().Select(e => new ListResult()
                    .Set("Id", e.Id)
                    .Set("CaseId", e.CaseId)
                    .Set("CreatedDate", e.CreatedDate)
                    .Set("Category", e.CategoryString)
                    .Set("NoteCategoryEnum", e.Category)
                    .Set("Notes", e.Notes)
                    .Set("CreatedBy", SetCreatedBy(e))
                    .Set("Contact", e.Metadata.GetRawValue<string>("Contact"))
                    .Set("Answer", e.Metadata.GetRawValue<bool?>("Answer"))
                    .Set("SAnswer", e.Metadata.GetRawValue<string>("SAnswer"))
                    .Set("UserOwnData", User.IsEmployeeSelfServicePortal ? (e.CreatedById == userEmployeeId ? true : false) : true)
                    .Set("ModifiedDate", e.ModifiedDate)
                    .Set("ModifiedBy", (e.ModifiedBy).DisplayName)

                );
                return result;
            }                                       
        }
    }
}
