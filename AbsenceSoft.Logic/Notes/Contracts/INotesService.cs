﻿using AbsenceSoft.Data.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Notes.Contracts
{
    public interface INotesService
    {
        CaseNote SaveCaseNote(CaseNote note);

        string GetCaseNoteByCertId(Guid CertId);

        ListResults GetCaseNoteList(ListCriteria criteria);
        ListResults GetCaseNoteListByContext(ListCriteria criteria);
    }
}
