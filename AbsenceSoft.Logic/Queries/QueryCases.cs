﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Queries
{
    public class QueryCases
    {
        List<IMongoQuery> queries = new List<IMongoQuery>();

        private QueryCases()
        {

        }

        public static QueryCases That()
        {
            return new QueryCases();
        }

        public IEnumerable<Case> AsEnumerable()
        {
            return Case.Query.Find(Case.Query.And(queries));
        }

        public QueryCases AreActive()
        {
            var inactiveStatus = new[] { CaseStatus.Cancelled, CaseStatus.Closed };
            queries.Add(Case.Query.NotIn(c => c.Status, inactiveStatus));
            return this;
        }


        public QueryCases ApplyFilter(string filterJson)
        {
            queries.Add(Case.Query.ParseQuery(filterJson));
            return this;
        }
    }
}
