﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Logic
{
    public class DiagnosisService : LogicService
    {
        /// <summary>
        /// ICD9 text search on Name, Description, and Code
        /// </summary>
        /// <param name="code">The text to search for</param>
        /// <param name="limit">How many results to return default is 100</param>
        /// <returns></returns>
        public List<DiagnosisCode> Search(string text, int limit = 100)
        {
            string theQuery = text.ToLower();
            List<DiagnosisCode> rtVal = DiagnosisCode.AsQueryable()
                .Where(i => i.Name.ToLower().Contains(theQuery) 
                        || i.Description.ToLower().Contains(theQuery) 
                        || i.Code.ToLower().Contains(theQuery))
                .Take(limit).ToList();
            return rtVal ?? new List<DiagnosisCode>();
        }

        /// <summary>
        /// Get by ICD9 Code
        /// </summary>
        /// <param name="code">The icd9 code</param>
        // <returns></returns>
        public DiagnosisCode GetByCode(string code)
        {
            DiagnosisCode rtVal = DiagnosisCode.AsQueryable().Where(i => i.Code.Equals(code)).FirstOrDefault();

            return rtVal ?? new DiagnosisCode();
        }  
    }
}
