﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Tasks
{
    public partial class ToDoService
    {
        /// <summary>
        /// check for any pending segments on the leave and return appropriate validation messages
        /// </summary>
        /// <param name="theItem"></param>
        /// <param name="theUser"></param>
        /// <returns></returns>
        public List<string> ValidatePendingSegmentsForCaseClosed(string caseId)
        {
            List<string> messages = new List<string>();
            var myCase = Case.GetById(caseId);
            if (myCase == null)
                throw new ArgumentOutOfRangeException("caseId", caseId, "Case not found.");

            bool hasPendingTime = myCase.Segments.SelectMany(y => y.AppliedPolicies).SelectMany(z => z.Usage).Any(a => a.SummaryStatus == AdjudicationSummaryStatus.Pending);
            if (hasPendingTime)
                messages.Add("A determination for all pending time must be made before case can be closed.");

            return messages;
        }

        /// <summary>
        /// Creates a generic case review task for any valid reason needed outside of configured todo. This type of task doesn't
        /// have an associated todo, it's just a one-time deal with no side-effects, etc.
        /// </summary>
        /// <param name="caseId">The id of the case to create the task for.</param>
        /// <param name="title">The title of the new task.</param>
        /// <param name="priority">The priority that the new task will have, defaults to Normal.</param>
        /// <param name="dueDate">The due date for the task to be completed by, defaults to <c>null</c> which is replaced by [tomorrow]</param>
        /// <param name="optional">Whether or not the compltion of this task will be optional, defaults to <c>true</c>.</param>
        /// <param name="hideUntil">The hide until.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">caseId</exception>
        /// <exception cref="System.ArgumentException">Case could not be found or does not exist;caseId</exception>
        public ToDoItem CreateCaseReviewTodo(string caseId, string title, ToDoItemPriority priority = ToDoItemPriority.Normal, DateTime? dueDate = null, bool optional = true, DateTime? hideUntil = null)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("caseId");

            Case relatedCase = Case.GetById(caseId);

            if (relatedCase == null)
                throw new ArgumentException("Case could not be found or does not exist", "caseId");

            ToDoItem tdi = new ToDoItem()
            {
                ItemType = ToDoItemType.CaseReview,
                Case = relatedCase,
                CustomerId = relatedCase.CustomerId,
                EmployeeId = relatedCase.Employee.Id,
                EmployeeNumber = relatedCase.Employee.EmployeeNumber,
                EmployerId = relatedCase.EmployerId,
                AssignedToId = relatedCase.AssignedToId,
                Optional = optional,
                Title = title,
                Priority = priority,
                Status = ToDoItemStatus.Pending,
                DueDate = dueDate ?? DateTime.UtcNow.AddDays(1),
                EmployeeName = relatedCase.Employee.FullName,
                CreatedBy = User.Current,
                ModifiedBy = User.Current,
                HideUntil = hideUntil
            };

            CheckForDuplicateToDo(tdi);

            return tdi.Save();

        }//CreateCaseReviewTodo

        [Obsolete("Workflow - Need to un-hard-code the LTD SSDI Age 65 and WA-FAM PTO workflows, it's just too complex right now to solve that issue", false)]
        public void CaseCreated(Case theCase, User theUser)
        {
            var todoService = new ToDoService();

            if (theUser == null)
                theUser = User.System;

                //LTD (SSDI Age 65 workflow)
            // Need to leave this as hard-coded for the moment
#warning TODO: Workflow - Need to un-hard-code the LTD SSDI Age 65 workflow, it's just too complex right now to solve that issue
                if (theCase.Segments.SelectMany(s => s.AppliedPolicies).Any(p => p.Policy.PolicyType == PolicyType.LTD && p.Status == EligibilityStatus.Eligible))
                {
                    if (theCase.Employee != null && theCase.Employee.DoB.HasValue)
                    {
                        var age65Date = theCase.Employee.DoB.Value.AddYears(65);
                        if (age65Date.DateInRange(theCase.StartDate, theCase.EndDate))
                        {
                            DateTime due = age65Date.AddDays(-1) < theCase.StartDate ? theCase.StartDate : age65Date.AddDays(-1);
                            DateTime hideUntil = age65Date.AddDays(-14) < theCase.StartDate ? theCase.StartDate : age65Date.AddDays(-14);
                            string title = string.Format("Projected SSDI or Age 65 Date on {0:MM/dd/yyyy}", age65Date);
                            CreateCaseReviewTodo(theCase.Id, title, ToDoItemPriority.High, due, true, hideUntil);
                        }
                    }
                }

            // Need to leave this as hard-coded for the moment
#warning TODO: Workflow - Need to un-hard-code the WA-FAM PTO workflow, it's just too complex right now to solve that issue
                var waFam = theCase.Segments.SelectMany(s => s.AppliedPolicies).FirstOrDefault(p => p.Policy.Code == "WA-FAM" && p.Status == EligibilityStatus.Eligible);
                if (waFam != null)
                {
                    string hoursString = null;
                    var group = waFam.RuleGroups.FirstOrDefault(g => g.RuleGroup.RuleGroupType == PolicyRuleGroupType.Eligibility && g.RuleGroup.Metadata.GetRawValue<bool>("IsPtoRule") && g.Rules.Any());
                    if (group != null)
                        hoursString = group.Rules.First().OverrideValue;

                    CreateCaseReviewTodo(theCase.Id, string.Format("Validate Time Off w/ Pay Balance {0}{1} and set the appropriate approval period / exhaustion date for WA Family Care Act",
                        string.IsNullOrWhiteSpace(hoursString) ? "as of the case start date" : hoursString,
                        string.IsNullOrWhiteSpace(hoursString) ? "" : " hours"));
                }

        }//public ToDoItem CaseCreated(Case theNewCase)

        /// <summary>
        /// Generated LeaveExhausted TODO
        /// WARNING: DO NOT REMOVE THIS FOR NOW, it's used by the EL process' Bulk API and NOT the new Workflow Engine, so for now, it must stay in place.
        /// </summary>
        /// <remarks>
        /// Except for in EL or other bulk processing, @case should be
        /// the only paramater needed here.
        /// </remarks>
        /// <param name="@case"></param>
        /// <param name="modifiedById"></param>
        /// <param name="createdById"></param>
        /// <param name="modified"></param>
        /// <param name="created"></param>
        /// <returns></returns>
        [Obsolete("DO NOT REMOVE THIS FOR NOW, it's used by the EL process' Bulk API and NOT the new Workflow Engine, so for now, it must stay in place.", false)]
        public ToDoItem MakeLeaveExhaustedTodo(
            Case @case,
            string modifiedById = null,
            string createdById = null,
            DateTime? modified = null,
            DateTime? created = null
        )
        {
            var newExhaustDate = @case.GetMinLeaveExhaustionDate().Value;
            var result = new ToDoItem
            {
                AssignedToId = @case.AssignedToId,

                CreatedById = createdById != null ? createdById : null,
                ModifiedById = modifiedById != null ? modifiedById : null,

                CaseId = @case.Id,
                CaseNumber = @case.CaseNumber,
                CustomerId = @case.CustomerId,
                DueDate = newExhaustDate,
                HideUntil = newExhaustDate.AddDays(-7),
                EmployeeId = @case.Employee.Id,
                EmployeeNumber = @case.Employee.EmployeeNumber,
                EmployeeName = @case.Employee.FullName,
                EmployerId = @case.EmployerId,
                ItemType = ToDoItemType.Communication,
                Metadata = new BsonDocument().Add("CommunicationType", BsonValue.Create(CommunicationType.Mail)).Add("Template", new BsonString("ELGLEXH")),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Send Eligibility Notice Leave Exhausted",
                Weight = 1,
                Optional = false,
                AssignedToName = @case.AssignedToName,
                EmployerName = @case.EmployerName,
            };
            if (modified.HasValue) result.SetModifiedDate(modified.Value);
            if (created.HasValue) result.SetCreatedDate(created.Value);
            return result;
        }
    }
}
