﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.Tasks
{
    public partial class ToDoService
    {
        private ToDoItem CreateErgonomicAssessmentToDo(Case theCase, User theUser)
        {
            if (theUser == null) theUser = User.System;
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = theCase.Employee.FullName,
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.ScheduleErgonomicAssessment,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Schedule Accommodation Ergonomic Assessment",
                Weight = 1
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//createErgonomicAssessmentToDo        

        public ToDoItem ErgonomicAssessmentScheduled(ToDoItem theItem, User theUser, DateTime scheduledDate)
        {
            if (theUser == null) theUser = User.System;
            theItem.ResultText = "ERGONOMIC ASSESSMENT SCHEDULED";
            theItem.Status = ToDoItemStatus.Complete;
            theItem.ModifiedById = theUser.Id;
            theItem.Save();

            //create Complete Ergonomic Assessment TODO
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theItem.CaseId,
                CaseNumber = theItem.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theItem.CustomerId,
                DueDate = scheduledDate,
                EmployeeId = theItem.EmployeeId,
                EmployeeName = theItem.EmployeeName,
                EmployerId = theItem.EmployerId,
                ItemType = ToDoItemType.CompleteErgonomicAssessment,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Verify Accommodation Ergonomic Assessment Complete",
                Weight = 1
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//ErgonomicAssessmentScheduled

        public ToDoItem CompleteErgonomicAssessment(ToDoItem theItem, User theUser, DateTime? assessmentDate, string responseText)
        {
            if (theUser == null) theUser = User.System;

            switch (responseText)
            {
                case "Yes":
                    theItem.ResultText = "ERGONOMIC ASSESSMENT COMPLETED";
                    theItem.Status = ToDoItemStatus.Complete;
                    theItem.ModifiedById = theUser.Id;
                    theItem.Save();
                    return CreateCaseReviewTodo(theItem.Case.Id, "Perform Accommodation Determination");
                case "NotYet":
                    theItem.DueDate = assessmentDate.HasValue ? assessmentDate.Value : theItem.DueDate;
                    theItem.ModifiedById = theUser.Id;
                    theItem.Save();
                    break;
                case "No":
                    theItem.ResultText = "ERGONOMIC ASSESSMENT NOT COMPLETED";
                    theItem.Status = ToDoItemStatus.Complete;
                    theItem.ModifiedById = theUser.Id;
                    theItem.Save();
                    return CreateCaseReviewTodo(theItem.Case.Id, "Perform Accommodation Determination, No ergonomic assessment");
                default:
                    break;
            }

            return theItem;
        }//ErgonomicAssessmentScheduled

        private ToDoItem CreateEquipmentSoftwareOrderedToDo(Case theCase, User theUser)
        {
            if (theUser == null) theUser = User.System;
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = theCase.Employee.FullName,
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.EquipmentSoftwareOrdered,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Accommodation Equipment / Software Ordered",
                Weight = 1
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//createEquipmentSoftwareOrderedToDo

        public ToDoItem CompleteEquipmentSoftwareOrdered(ToDoItem theItem, User theUser, bool completed)
        {
            if (theUser == null) theUser = User.System;

            if (completed)
            {
                theItem.ResultText = "EQUIPMENT / SOFTWARE ORDER COMPLETED";
                theItem.Status = ToDoItemStatus.Complete;
                theItem.ModifiedById = theUser.Id;
                theItem.Save();

                //create Equipment Software Installed
                ToDoItem wfi = new ToDoItem()
                {
                    AssignedToId = theUser.Id,
                    CaseId = theItem.CaseId,
                    CaseNumber = theItem.CaseNumber,
                    CreatedById = theUser.Id,
                    CustomerId = theItem.CustomerId,
                    DueDate = theItem.DueDate,
                    EmployeeId = theItem.EmployeeId,
                    EmployeeName = theItem.EmployeeName,
                    EmployerId = theItem.EmployerId,
                    ItemType = ToDoItemType.EquipmentSoftwareInstalled,
                    Priority = ToDoItemPriority.Normal,
                    Status = ToDoItemStatus.Pending,
                    Title = "Accommodation Equipment / Software Installed",
                    Weight = 1
                };

                CheckForDuplicateToDo(wfi);
                wfi.Save();
                return wfi;
            }

            return theItem;
        }

        public ToDoItem CompleteEquipmentSoftwareInstalled(ToDoItem theItem, User theUser, bool completed)
        {
            if (theUser == null) theUser = User.System;

            if (completed)
            {
                theItem.ResultText = "EQUIPMENT / SOFTWARE INSTALLATION COMPLETED";
                theItem.Status = ToDoItemStatus.Complete;
                theItem.ModifiedById = theUser.Id;
                theItem.Save();
            }

            return theItem;
        }//CompleteEquipmentSoftwareInstalled

        private ToDoItem CreateHRISScheduleChangeToDo(Case theCase, User theUser)
        {
            if (theUser == null) theUser = User.System;
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = theCase.Employee.FullName,
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.HRISScheduleChange,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Accommodation HRIS Schedule Change",
                Weight = 1
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//createHRISScheduleChangeToDo

        public ToDoItem CompleteHRISScheduleChange(ToDoItem theItem, User theUser, bool completed)
        {
            if (theUser == null) theUser = User.System;

            if (completed)
            {
                theItem.ResultText = "HRIS SCHEDULE CHANGE COMPLETED";
                theItem.Status = ToDoItemStatus.Complete;
                theItem.ModifiedById = theUser.Id;
                theItem.Save();
            }

            return theItem;
        }//CompleteHRISScheduleChange

        private ToDoItem CreateOtherAccommoddationToDo(Case theCase, User theUser)
        {
            if (theUser == null) theUser = User.System;
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = theCase.Employee.FullName,
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.OtherAccommodation,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Other Accommodation Complete",
                Weight = 1
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//createOtherAccommoddationToDo

        public ToDoItem CompleteOtherAccommodation(ToDoItem theItem, User theUser, bool completed)
        {
            if (theUser == null) theUser = User.System;

            if (completed)
            {
                theItem.ResultText = "OTHER ACCOMMODATION COMPLETED";
                theItem.Status = ToDoItemStatus.Complete;
                theItem.ModifiedById = theUser.Id;
                theItem.Save();
            }

            return theItem;
        }//CompleteOtherAccommoddation

        public void PacketPaperWorkIncompleteAccommodation(ToDoItem theItem, User theUser)
        {
            Guid accommodationId = theItem.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
            theItem.ResultText = "ACCOMMODATION FORM MARKED INCOMPLETE";
            theItem.Status = ToDoItemStatus.Complete;
            theItem.ModifiedById = theUser.Id;
            theItem.Save();
            CreateCommunicationTodo(theItem.Case, theUser, CommunicationType.Mail, "INCACCOM", "Send Incomplete Accommodations Form Letter", null, null, false, accommodationId);
        }

        public void PaperworkApprovedAccommodation(ToDoItem theItem, User theUser)
        {
            theItem.ResultText = "ACCOMMODATION FORM MARKED COMPLETE";
            theItem.Status = ToDoItemStatus.Complete;
            theItem.ModifiedById = theUser.Id;
            theItem.Save();

            //Schedule Accommodation Determination
            CreateCaseReviewTodo(theItem.Case.Id, "Perform Accommodation Determination");

            //Schedule "End of Temporary Accommodation Letter" Todo if needed
            if (theItem == null || theItem.Case == null || theItem.Case.AccommodationRequest == null || theItem.Case.AccommodationRequest.Accommodations == null)
                return;

            CreateAccommodationEndOfTempAccommCommunicationTodoItem(theItem.Case, theUser);
        }

        public void CreateAccommodationAdjudicationLetterToDoItem(Case myCase, AdjudicationStatus status, Guid accommId)
        {
            // We need to create either the Approval OR Denial letter based on how they specified the status. For accommodatins
            // we need a seperate letter for each.

            // approval
            if (status == AdjudicationStatus.Approved)
            {
                var task = new ToDoItem()
                {
                    EmployeeId = myCase.Employee.Id,
                    EmployerId = myCase.EmployerId,
                    CustomerId = myCase.CustomerId,
                    AssignedToId = myCase.AssignedToId,
                    CaseId = myCase.Id,
                    CaseNumber = myCase.CaseNumber,
                    DueDate = DateTime.UtcNow.AddHours(1),
                    EmployeeName = myCase.Employee.FullName,
                    ItemType = ToDoItemType.Communication,
                    Optional = false,
                    Priority = ToDoItemPriority.Normal,
                    Status = ToDoItemStatus.Pending,
                    Title = "Send Designation Notice Accommodation Approved",
                    Weight = 0,
                    CreatedBy = myCase.CreatedBy
                };
                task.Metadata.SetRawValue("Template", "ACCOM-APPROVED");
                task.Metadata.SetRawValue("CommunicationType", (int)CommunicationType.Mail);
                task.Metadata.SetRawValue("ActiveAccommodationId", accommId);

                CheckForDuplicateToDo(task);
                task.Save();

                //Send Approval email to HR and SUP
                CreateCommunicationTodo(myCase, myCase.AssignedTo, CommunicationType.Email, "ACCOM-APPROVED-HR", "Send Accommodation Approval HR Communication", null, null, false, accommId);
                CreateCommunicationTodo(myCase, myCase.AssignedTo, CommunicationType.Email, "ACCOM-APPROVED-SUP", "Send Accommodation Approval Manager Communication", null, null, false, accommId);
            }

            // the denial only triggers if the case is fully denied
            var theAccom = myCase.AccommodationRequest.Accommodations.FirstOrDefault(a => a.Id == accommId);
            if (status == AdjudicationStatus.Denied && theAccom != null && theAccom.Determination == AdjudicationStatus.Denied)
            {
                var task = new ToDoItem()
                {
                    EmployeeId = myCase.Employee.Id,
                    EmployerId = myCase.EmployerId,
                    CustomerId = myCase.CustomerId,
                    AssignedToId = myCase.AssignedToId,
                    CaseId = myCase.Id,
                    CaseNumber = myCase.CaseNumber,
                    DueDate = DateTime.UtcNow.AddHours(1),
                    EmployeeName = myCase.Employee.FullName,
                    ItemType = ToDoItemType.Communication,
                    Optional = true,
                    Priority = ToDoItemPriority.Normal,
                    Status = ToDoItemStatus.Pending,
                    Title = "Send Designation Notice Accommodation Denied",
                    Weight = 0,
                    CreatedBy = myCase.CreatedBy
                };
                task.Metadata.SetRawValue("Template", "ACCOM-DENIED");
                task.Metadata.SetRawValue("CommunicationType", (int)CommunicationType.Mail);
                task.Metadata.SetRawValue("ActiveAccommodationId", accommId);
                CheckForDuplicateToDo(task);
                task.Save();
            }

            //create a mgr comm todo
            CreateCommunicationTodo(myCase, null, CommunicationType.Email, "MGRNOTICE", "Manager Communication", null, null, true, accommId);
        }

        public void AccommodationAdded(Case theCase, User theUser, Accommodation newAccomm)
        {
            if (theUser == null) theUser = User.System;

            //Parrot Types: No workflow for CONV accommodation
            if (newAccomm != null && (newAccomm.Type.Code == "CONV"))
            {
                return;
            }

            //For Parrot validate service data and if it is <1 year of service then schedule todo to call AON to create LOA case
            if (theCase != null && theCase.Customer.Metadata.GetRawValue<bool>("CreateLOACaseTodoFor1YearLessOfService"))
            {
                ScheduleLOACaseCreateTodo(theCase, theUser);
            }

            //Parrot Types: Skip Acknowledgment form and open "Case Review instead"
            if (newAccomm != null && (newAccomm.Type.Code == "LTA" || newAccomm.Type.Code == "TA" || newAccomm.Type.Code == "TWA"))
            {
                CreateCaseReviewTodo(theCase.Id, "Review Case", ToDoItemPriority.Normal, null, true);
                return;
            }


            List<ToDoItem> accommToDo = ToDoItem.AsQueryable().Where(t =>
                    t.CaseId == theCase.Id &&
                    t.ItemType == ToDoItemType.Communication &&
                    (t.Status == ToDoItemStatus.Pending || t.Status == ToDoItemStatus.Open || t.Status == ToDoItemStatus.Overdue)
                    ).ToList();

            bool hasAccommLetter = false;
            foreach (ToDoItem t in accommToDo)
            {
                var packet = t.Metadata.GetRawValue<string>("Template");
                if (packet.Equals("ACCOMACK"))
                    hasAccommLetter = true;
            }

            //all accommodations get the acknowledgement letter
            if (theCase.Segments.Any(s => s.Status == CaseStatus.Open) && theCase.AccommodationRequest.Status == CaseStatus.Open)
            {
                if (!hasAccommLetter)
                    CreateSendAccommodationCommunicationTodo(theCase, theUser, CommunicationType.Mail, "ACCOMACK", "Send Accommodation Acknowledgement Letter to Employee", newAccomm.Id);

                // Get the time contact preference
                string timePref = theCase.Metadata.GetRawValue<string>("TimePreference");
                // Get the state for mappping the time zone
                string state = theCase.Employee.Info.Address.State ?? theCase.Employee.WorkState ?? "";
                // Dynammically create our call ToDo item title based on the number of days, auto-detects DST/ST and formats it appropriately using the time preference
                //  and appends the correct TimeZone abbreviation.
                var callTitle = new Func<int, string>(days => string.Format("Day {0} Call, {1}{2}{3}", days, 
                    timePref, string.IsNullOrWhiteSpace(timePref) ? "" : ", ", state.ToTimeZone(DateTime.Now.AddDays(days)))); // leave local time here so we can determine DST/ST.

                // day 1
                if (newAccomm != null && newAccomm.Type.Code != "TWA")
                {
                    CreateCaseReviewTodo(theCase.Id, callTitle(1), ToDoItemPriority.Normal, DateTime.UtcNow.ToMidnight().AddDays(1));
                    CreateCommunicationTodo(theCase, theUser, CommunicationType.Email, "ACCOM-ECN-SEND-LETTER", "ECN to send letter");
                }

                // day 4
                // superceeded by the next if
                //CreateCommunicationTodo(theCase, theUser, CommunicationType.Email, "ACCOM-REMINDER-EMAIL", "Send Reminder Email", DateTime.UtcNow.ToMidnight().AddDays(4).Date, null, true, newAccomm.Id);
                if (newAccomm != null && newAccomm.Type.Code != "TWA" && newAccomm.Type.Code != "FFD" && newAccomm.Type.Code != "WPV")
                {
                    CreateCaseReviewTodo(theCase.Id, callTitle(4), ToDoItemPriority.Normal, DateTime.UtcNow.ToMidnight().AddDays(4));
                    CreateCommunicationTodo(theCase, theUser, CommunicationType.Email, "ACCOM-REMINDER-EMAIL", "Send Reminder Email", DateTime.UtcNow.ToMidnight().AddDays(4).Date, null, true, newAccomm.Id);
                }

                //Parrot:Send Form To Provider
                if (newAccomm != null && (newAccomm.Type.Code == "FFD" || newAccomm.Type.Code == "WPV"))
                    CreateSendAccommodationCommunicationTodo(theCase, theUser, CommunicationType.Mail, "ACCOMACKPROV", "Send Fitness for Duty Assessment Letter to Provider", newAccomm.Id);

                if (newAccomm != null && newAccomm.Type.Code == "R4C")
                {
                    CreateCommunicationTodo(theCase, theUser, CommunicationType.Email, "ACCOM-REMINDER-EMAIL", "Send Reminder Email", DateTime.UtcNow.ToMidnight().AddDays(8).Date, null, true, newAccomm.Id);
                    CreateCaseReviewTodo(theCase.Id, callTitle(7), ToDoItemPriority.Normal, DateTime.UtcNow.ToMidnight().AddDays(7));
                    CreateCaseReviewTodo(theCase.Id, callTitle(15), ToDoItemPriority.Normal, DateTime.UtcNow.ToMidnight().AddDays(15));
                }

                //if this is an ergonomic assessment
                if (String.Compare(newAccomm.Type.Code, "ErgonomicAssessment", true) == 0)
                    CreateErgonomicAssessmentToDo(theCase, theUser);
            }

        }//AccommodationAdded

        public void AccommodationApproved(ToDoItem theItem, User theUser)
        {
            if (theUser == null) theUser = User.System;

            if (theItem == null || theItem.Case == null || theItem.Case.AccommodationRequest == null || theItem.Case.AccommodationRequest.Accommodations == null)
                return;

            Guid accommId = theItem.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
            Accommodation accomm = theItem.Case.AccommodationRequest.Accommodations.Where(x => x.Id == accommId).FirstOrDefault();

            if (accomm == null)
                return;

            //create todos for specific accomm types           
            switch (accomm.Type.Code.ToUpper())
            {
                case "EQUIPMENTORSOFTWARE":
                    CreateEquipmentSoftwareOrderedToDo(theItem.Case, theUser);
                    break;
                case "ERGONOMICASSESSMENT":
                    break;
                case "SCHEDULECHANGE":
                    CreateHRISScheduleChangeToDo(theItem.Case, theUser);
                    break;
                case "OTHER":
                    CreateOtherAccommoddationToDo(theItem.Case, theUser);
                    break;
                default:
                    break;
            }

        }//AccommodationApproved

        private ToDoItem CreateSendAccommodationCommunicationTodo(Case theCase, User theUser, CommunicationType type, string packetKey, string title = "Send Communication", Guid? newAccommId = null)
        {
            if (theUser == null) theUser = User.System;
            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.Communication,
                Metadata = new BsonDocument().Add("CommunicationType", BsonValue.Create(type)).Add("Template", new BsonString(packetKey)),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = title,
                Weight = 1,
                Optional = true
            };

            if (newAccommId.HasValue)
                wfi.Metadata.SetRawValue("ActiveAccommodationId", newAccommId.Value);

            CheckForDuplicateToDo(wfi);
            wfi.Save();
            return wfi;
        }//CreateCommunicationTodo

        private ToDoItem CreateAccommodationEndOfTempAccommCommunicationTodoItem(Case theCase, User theUser)
        {
            ToDoItem wfi = null;

            if (theCase == null)
                return wfi;

            if (theUser == null) theUser = User.System;


            //Find temp accommodation with max end date
            Accommodation accomm = theCase.AccommodationRequest.Accommodations.Where(a => a.Duration == AccommodationDuration.Temporary).OrderByDescending(a => a.EndDate).FirstOrDefault();

            if (accomm == null || !accomm.EndDate.HasValue)
                return wfi;

            wfi = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = accomm.EndDate.Value.AddDays(-7),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.Communication,
                Metadata = new BsonDocument().Add("CommunicationType", BsonValue.Create(CommunicationType.Mail)).Add("Template", new BsonString("ACCOM-ENDTEMP")),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Send Notice of End of Temporary Accommodation to Employee",
                Weight = 1,
                Optional = false
            };

            CheckForDuplicateToDo(wfi);
            wfi.Save();

            //Schedule Task to Close Case
            ToDoItem cci = new ToDoItem()
            {
                AssignedToId = theUser.Id,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = theCase.EndDate.Value,
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.CloseCase,
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "End of Temporary Accommodation for Employee, Close Case",
                Weight = 1,
            };

            CheckForDuplicateToDo(cci);
            cci.Save();


            return wfi;
        }

        public ToDoItem SendFailureToProvideCertification(ToDoItem theItem, User theUser)
        {
            if (theUser == null) theUser = User.System;

            theItem.ResultText = "TASK COMPLETE";
            theItem.Status = ToDoItemStatus.Complete;
            theItem.ModifiedById = theUser.Id;
            theItem.Save();

            SetPaperworkStatus(theItem, PaperworkReviewStatus.NotReceived);

            return CreateCommunicationTodo(theItem.Case, theUser, CommunicationType.Mail, "FAILCERT", "Send Documentation Not Received Letter to Employee", DateTime.Now.AddDays(7), null, false);
        }

        public ToDoItem SendAccommodationCertificationNotReceived(ToDoItem theItem, User theUser)
        {
            if (theUser == null) theUser = User.System;

            theItem.ResultText = "TASK COMPLETE";
            theItem.Status = ToDoItemStatus.Complete;
            theItem.ModifiedById = theUser.Id;
            theItem.Save();

            Guid accomodationId = theItem.Metadata.GetRawValue<Guid>("ActiveAccommodationId");

            SetPaperworkStatus(theItem, PaperworkReviewStatus.NotReceived);

            return CreateCommunicationTodo(theItem.Case, theUser, CommunicationType.Mail, "ACCOM-NOTRECEIVED", "Send Accommodation Certification Not Received Letter to Employee", DateTime.Now.AddDays(7), null, false, accomodationId);
        }

        public ToDoItem NotifyWCManager(Case theCase, User theUser)
        {
            if (theCase == null) throw new ArgumentNullException("theCase");
            if (theCase.Employee == null) throw new ArgumentNullException("The employee is missing from the case", "theCase");
            if (theUser == null) theUser = User.System;

            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theCase.AssignedToId,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.NotifyManager,
                Metadata = new BsonDocument(),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = string.Format("New Case Created of Employee '{0} {1}'", theCase.Employee.FirstName, theCase.Employee.LastName),
                Weight = 1,
                Optional = true
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();

            return wfi;
        }

        public ToDoItem InitiateMyLeave(Case theCase, User theUser)
        {
            if (theCase == null) throw new ArgumentNullException("theCase");
            if (theCase.Employee == null) throw new ArgumentNullException("The employee is missing from the case", "theCase");
            if (theUser == null) theUser = User.System;

            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theCase.AssignedToId,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = DateTime.UtcNow.AddHours(1),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.InitiateNewLeaveCase,
                Metadata = new BsonDocument(),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = string.Format("Initiate New Leave Case"),
                Weight = 1,
                Optional = true
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();

            return wfi;
        }

        /// <summary>
        /// Parrot:Schedule Todo to call AON to open LOA case if employee has less than 1 year of service
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="theUser"></param>
        private void ScheduleLOACaseCreateTodo(Case theCase, User theUser)
        {
            if (theCase == null) throw new ArgumentNullException("theCase");
            if (theCase.Employee == null) throw new ArgumentNullException("The employee is missing from the case", "theCase");
            if (theUser == null) theUser = User.System;

            //Validate length of service and if it is less than 1 year schedule todo
            DateTime? serviceDate = theCase.Employee.ServiceDate ?? theCase.Employee.HireDate;
            if (!serviceDate.HasValue || serviceDate.Value.AddYears(1) <= DateTime.UtcNow)
            {
                return;
            }

            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = theCase.AssignedToId,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                DueDate = serviceDate.Value.AddYears(1),
                HideUntil = serviceDate.Value.AddYears(1).AddDays(-5),
                EmployeeId = theCase.Employee.Id,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                EmployerId = theCase.EmployerId,
                ItemType = ToDoItemType.Manual,
                Metadata = new BsonDocument(),
                Priority = ToDoItemPriority.Normal,
                Status = ToDoItemStatus.Pending,
                Title = "Call AON to open LOA case",
                Weight = 1,
                Optional = true
            };
            CheckForDuplicateToDo(wfi);
            wfi.Save();
        }
    }
}
