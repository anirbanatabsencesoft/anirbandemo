﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Tasks.Contracts;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Logic.Tasks
{
    /// <summary>
    /// This class contains hard coded todo methods
    /// </summary>
    public partial class ToDoService : LogicService, IToDoService
    {
        public ListResults ToDoItemList(User user, ListCriteria criteria)
        {
            using (new InstrumentationContext("ToDoService.TodoItemList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string employeeFilter = criteria.Get<string>("EmployeeName");
                string caseIdFilter = criteria.Get<string>("CaseId");
                string caseNumberFilter = criteria.Get<string>("CaseNumber");
                string textFilter = criteria.Get<string>("Title");
                long? dueFilter = criteria.Get<long?>("DueDate");
                long? statusFilter = criteria.Get<long?>("Status");
                string assignedToNameFilter = criteria.Get<string>("AssignedToName");
                string listTypeFilterText = criteria.Get<string>("ListType");

                VisibilityListType listTypeFilter;
                if (string.IsNullOrWhiteSpace(listTypeFilterText) || !Enum.TryParse<VisibilityListType>(listTypeFilterText, out listTypeFilter))
                    listTypeFilter = VisibilityListType.Individual;
                if (listTypeFilter == VisibilityListType.Team && !User.Permissions.ProjectedPermissions.Contains(Permission.ViewTeam.Id))
                    listTypeFilter = VisibilityListType.Individual;

                string nextToDoStatusFilter = criteria.Get<string>("NextToDoFilter");

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if ((user ?? User.Current) != null)
                    ands.Add((user ?? User.Current).BuildDataAccessFilters());

                // Hide ToDo items if they should be (one of these should be true, hence the OR)
                ands.Add(ToDoItem.Query.Or(
                    // Either the HideUntil property doesn't exist, which means we don't hide it btw
                    ToDoItem.Query.NotExists(t => t.HideUntil),
                    // Or the hide until date has passed
                    ToDoItem.Query.LTE(t => t.HideUntil, DateTime.UtcNow.ToMidnight()),
                    // Or the due date has passed
                    ToDoItem.Query.LTE(t => t.DueDate, DateTime.UtcNow.ToMidnight())));

                // Multi-Employer type filter
                string employerId = criteria.Get<string>("EmployerId");

                //Check if user is limited to one employer
                if (string.IsNullOrWhiteSpace(employerId) && Employer.Current != null)
                    employerId = Employer.Current.Id;

                if (!string.IsNullOrWhiteSpace(employerId))
                    ands.Add(Employee.Query.EQ(e => e.EmployerId, employerId));

                string lName = null, fName = null;
                if (!string.IsNullOrWhiteSpace(employeeFilter))
                {
                    // If the lastname and firstname are equal, then it's a "full name" filter, duh, so do an OR filter

                    if (employeeFilter.Contains(','))
                    {
                        string[] arrName = employeeFilter.Split(',');
                        if (arrName.Length > 0)
                        {
                            lName = arrName[0].Trim();
                            fName = arrName[1].Contains(' ') ? arrName[1].Trim().Split(' ')[0] : arrName[1].Trim();
                        }
                    }
                    else
                    {
                        string par = "^(.*)" + employeeFilter + "(.*)";

                        ands.Add(ToDoItem.Query.Matches(e => e.EmployeeName, new BsonRegularExpression(par, "i")));
                    }
                }

                // Otherwise, the filters are mutually exclusive, so if they are provided, make each one part of the AND group
                if (!string.IsNullOrWhiteSpace(lName) && !string.IsNullOrWhiteSpace(fName))
                {
                    string pattern = "^" + lName + "(.*?)" + fName + "(.*)";
                    ands.Add(ToDoItem.Query.Matches(e => e.EmployeeName, new MongoDB.Bson.BsonRegularExpression(pattern, "i")));

                }
                else
                {
                    string pattern = "^" + lName + "(.*)";
                    ands.Add(ToDoItem.Query.Matches(e => e.EmployeeName, new BsonRegularExpression(pattern, "i")));
                }

                if (!string.IsNullOrWhiteSpace(caseIdFilter))
                    ands.Add(ToDoItem.Query.EQ(e => e.CaseId, caseIdFilter));
                if (!string.IsNullOrWhiteSpace(caseNumberFilter))
                    ands.Add(ToDoItem.Query.Matches(e => e.CaseNumber, new BsonRegularExpression("^" + caseNumberFilter, "i")));
                if (!string.IsNullOrWhiteSpace(textFilter))
                    ands.Add(ToDoItem.Query.Matches(e => e.Title, new BsonRegularExpression(textFilter, "i")));
                if (statusFilter.HasValue && statusFilter != 0)
                {
                    if (statusFilter == 4)
                    {
                        ands.Add(ToDoItem.Query.In(e => e.Status, new[] { ToDoItemStatus.Pending, ToDoItemStatus.Overdue }));
                    }
                    else if (statusFilter == 2)//overdue
                    {
                        // Either it has an explicit status of overdue, OR, it is literally overdue with a different open/active status
                        ands.Add(ToDoItem.Query.Or(
                            ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Pending), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow.Date)),
                            ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Open), ToDoItem.Query.LT(e => e.DueDate, DateTime.UtcNow.Date)),
                            ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Overdue))
                        );
                    }
                    else if (statusFilter == -1)//Pending
                    {
                        // Either it has an explicit status of overdue, OR, it is literally overdue with a different open/active status
                        ands.Add(ToDoItem.Query.And(ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Pending), ToDoItem.Query.GT(e => e.DueDate, DateTime.UtcNow.Date)));
                    }
                    else
                    {
                        ToDoItemStatus enumStatusFilter = ((ToDoItemStatus)statusFilter);
                        ands.Add(ToDoItem.Query.EQ(e => e.Status, enumStatusFilter));
                    }
                }

                if (dueFilter.HasValue)
                {
                    // Due Date filter should be due on that date or PRIOR, need this for the TODO modal but also makes sense in filtering normal TODOs on the dashboard for now as well.
                    // ands.Add(ToDoItem.Query.GTE(e => e.DueDate, new BsonDateTime(dueFilter.Value)));
                    // Window is 24 hours
                    var dueDate = new BsonDateTime(dueFilter.Value);
                    ands.Add(ToDoItem.Query.And(ToDoItem.Query.GTE(e => e.DueDate, dueDate.Value.ToMidnight()), ToDoItem.Query.LTE(e => e.DueDate, dueDate.Value.EndOfDay()))); // not including
                }

                if (string.IsNullOrWhiteSpace(caseIdFilter))
                {
                    string userId = (User.Current ?? new User()).Id ?? "".PadLeft(24, '9');
                    // Check the user visibility for the user's team or just themselves based on permissions or filter selection and/or both
                    if (!string.IsNullOrWhiteSpace(assignedToNameFilter))
                        ands.Add(ToDoItem.Query.Matches(e => e.AssignedToName, new BsonRegularExpression("^" + assignedToNameFilter, "i")));
                    else if (listTypeFilter == VisibilityListType.Individual)
                        ands.Add(ToDoItem.Query.EQ(e => e.AssignedToId, userId));
                    else if (listTypeFilter == VisibilityListType.Team)
                    {
                        List<string> myTeamsUserIds = new TeamService(User.Current).GetTeamMemberIdsForCurrentUser();
                        ands.Add(ToDoItem.Query.In(e => e.AssignedToId, myTeamsUserIds));
                    }

                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(assignedToNameFilter))
                        ands.Add(ToDoItem.Query.Matches(e => e.AssignedToName, new BsonRegularExpression("^" + assignedToNameFilter, "i")));
                }

                // Filter to only allowed types for the user, so only pull ToDo item types the user has access to, duh.
                var allowedTypes = User.Permissions.ProjectedPermissions.GetsToDos();
                ands.Add(ToDoItem.Query.In(e => e.ItemType, allowedTypes));

                // Filter to only allowed types of communications for those communication ToDo item types the user has access to.
                var allowedComms = User.Permissions.ProjectedPermissions.GetsCommunications().Select(c => BsonValue.Create(c)).ToList();
                // We should allow NULL here as well, just in case it is null
                allowedComms.Insert(0, BsonNull.Value);
                ands.Add(ToDoItem.Query.Or(ToDoItem.Query.NE(e => e.ItemType, ToDoItemType.Communication), Query.In("Template", allowedComms)));

                // get next to do status filter
                if (!string.IsNullOrEmpty(nextToDoStatusFilter))
                {
                    ands.Add(ToDoItem.Query.Or
                            (
                                ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Pending),
                                ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Open),
                                ToDoItem.Query.EQ(e => e.Status, ToDoItemStatus.Overdue)
                            )
                        );
                }

                // apply filter
                // Set fields
                var query = ToDoItem.Query.Find(ands.Count > 0 ? ToDoItem.Query.And(ands) : null);

                // apply sorting
                if (!string.IsNullOrWhiteSpace(criteria.SortBy) &&
                    (criteria.SortBy.Trim().ToLower() != "status" || (!ContainsOpenToDos(statusFilter))))
                {
                    if (criteria.SortBy.Trim().ToLower() == "employee")
                    {
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                      ? SortBy.Ascending("Employee.LastName")
                      : SortBy.Descending("Employee.LastName"));
                    }
                    else
                    {
                        //Apply sorting on results since many Pending ToDo's in DB are actually overdue and are manipulated in following lines.
                        query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                           ? SortBy.Ascending(criteria.SortBy)
                           : SortBy.Descending(criteria.SortBy));
                    }
                }

                if (string.IsNullOrWhiteSpace(criteria.SortBy) ||
                    (!string.IsNullOrWhiteSpace(criteria.SortBy)
                    && (criteria.SortBy.Trim().ToLower() != "status" || (!ContainsOpenToDos(statusFilter)))))
                {
                    //Apply paging on results since many Pending ToDo's in DB are actually overdue and are manipulated in following lines.
                    var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                    if (skip < 0) skip = 0;
                    query = query.SetSkip(skip)
                        .SetLimit(criteria.PageSize);
                }

                result.Total = query.Count();

                //sorted todo list to resolve this item "EZW-15:Fix the Ability to add a RANKING for ToDos"
                var todos = query.OrderByDescending(o => o.Priority).OrderBy(o => o.DueDate).ToList();
                var modifiedByIds = todos.Select(e => e.ModifiedById).ToList();
                var modifiedByquery = User.Query.In(u => u.Id, modifiedByIds);
                var modifiedByUsers = User.Repository.Collection.Find(modifiedByquery);
                result.Results = todos.Select(c =>
                {

                    return new ListResult()
                        .Set("Id", c.Id)
                        .Set("EmployeeId", c.EmployeeId)
                        .Set("EmployeeName", c.EmployeeName)
                        .Set("EmployerId", c.EmployerId)
                        .Set("EmployerName", c.EmployerName)
                        .Set("Title", c.Title)
                        .Set("DueDate", c.DueDate.Date.ToUIString())
                        .Set("OriginalDueDate", c.OriginalDueDate.HasValue ? c.OriginalDueDate.Value.Date.ToUIString() : "")
                        .Set("Status", c.DueDate.Date < DateTime.UtcNow.Date && c.Status != ToDoItemStatus.Overdue && c.Status != ToDoItemStatus.Complete && c.Status != ToDoItemStatus.Cancelled ? ToDoItemStatus.Overdue.ToString() : c.DueDate.Date >= DateTime.UtcNow.Date && c.Status == ToDoItemStatus.Overdue ? ToDoItemStatus.Pending.ToString() : c.Status.ToString())
                        .Set("CaseId", c.CaseId)
                        .Set("CaseNumber", c.CaseNumber)
                        .Set("ItemType", c.ItemType.ToString())
                        .Set("Optional", c.Optional)
                        .Set("AssignedToId", c.AssignedToId)
                        .Set("AssignedToName", c.AssignedToName)
                        .Set("ModifiedByName", (modifiedByUsers.FirstOrDefault(m => m.Id == c.ModifiedById) ?? User.System).DisplayName)
                        .Set("CreatedDate", c.CreatedDate)
                        .Set("ModifiedDate", c.ModifiedDate)
                        .Set("CancelReason", c.CancelReason)
                        .Set("DueDateChangeReason", c.DueDateChangeReason)
                        .Set("StatusId", c.DueDate.Date < DateTime.UtcNow.Date && c.Status != ToDoItemStatus.Overdue && c.Status != ToDoItemStatus.Complete && c.Status != ToDoItemStatus.Cancelled ? ((int)ToDoItemStatus.Overdue).ToString() : c.DueDate.Date >= DateTime.UtcNow.Date && c.Status == ToDoItemStatus.Overdue ? ((int)ToDoItemStatus.Pending).ToString() : ((int)c.Status).ToString())
                        .Set("Description", c.Metadata.GetRawValue<string>("Description"))
                        .Set("ReasonCause", c.Metadata.GetRawValue<string>("ReasonCause"));
                }
                ).ToList();

                string[] caseIds = result.Results.Select(r => r.Get<string>("CaseId")).ToArray();
                var reasons = Case.AsQueryable().Where(c => caseIds.Contains(c.Id)).ToList().Select(c => new { Reason = c.Reason != null ? c.Reason.Name : string.Empty, CaseId = c.Id });
                result.Results.ForEach(r => r.Set("CaseReason", reasons.Where(t => t.CaseId == r.Get<string>("CaseId")).Select(t => t.Reason).FirstOrDefault() ?? ""));

                if (!string.IsNullOrWhiteSpace(criteria.SortBy) && criteria.SortBy.Trim().ToLower() == "status" && ContainsOpenToDos(statusFilter))
                {
                    //Apply sorting on results since many Pending ToDo's in DB are actually overdue and is manipulated in following lines.
                    var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                    if (skip < 0) skip = 0;
                    result.Results = (criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending ? result.Results.OrderBy(r => r["Status"]) : result.Results.OrderByDescending(r => r["Status"])).Skip(skip).Take(criteria.PageSize);
                }
                return result;
            }
        }//TodoItemList


        /// <summary>
        /// Changes the due date for a todo item, saves it and returns the saved result.
        /// </summary>
        /// <param name="toDoItemId">The ID of the todo item to modify the due date for.</param>
        /// <param name="dueDate">The new due date for that todo item.</param>
        /// <param name="reason">The textual, user provided reason for modifying the due date.</param>
        /// <returns>The modified todo item with the updated due date and reason.</returns>
        public ToDoItem ChangeDueDate(string toDoItemId, DateTime dueDate, string reason)
        {
            if (string.IsNullOrWhiteSpace(toDoItemId))
                throw new ArgumentNullException("toDoItemId");
            if (dueDate < DateTime.Today)
                throw new ArgumentException("Due date must be current or in the future", "dueDate");
            if (string.IsNullOrWhiteSpace(reason))
                throw new ArgumentNullException("reason");

            ToDoItem item = ToDoItem.GetById(toDoItemId);

            if (item == null)
                throw new ArgumentException("Item was not found", "toDoItemId");
            if (User.Current != null && !User.Current.HasEmployerAccess(item.EmployerId, Permission.ChangeToDoDueDate))
                throw new AbsenceSoftException("User does not have access to change the due date of this item");

            // We only want to update this if the Original Due Date hasn't already been updated
            if (!item.OriginalDueDate.HasValue)
            {
                item.OriginalDueDate = item.DueDate; 
            }
            

            item.DueDate = dueDate;
            item.DueDateChangeReason = reason;
            return item.Save();
        }//ChangeDueDate


        public ToDoItem ChangeStatus(string toDoItemId, ToDoItemStatus status, string cancelReason)
        {
            if (string.IsNullOrWhiteSpace(toDoItemId))
                throw new ArgumentNullException("toDoItemId");

            ToDoItem item = ToDoItem.GetById(toDoItemId);

            if (item == null)
                throw new ArgumentException("Item was not found", "toDoItemId");
            if (User.Current != null && !User.Current.HasEmployerAccess(item.EmployerId, Permission.CompleteToDo))
                throw new AbsenceSoftException("User does not have access to change the status of this item");

            item.Status = status;

            if (status == ToDoItemStatus.Cancelled)
            {
                item.CancelReason = cancelReason;
            }
          

            return item.Save();
        }

        public ToDoItem ReOpen(string toDoItemId)
        {
            if (string.IsNullOrWhiteSpace(toDoItemId))
                throw new ArgumentNullException("toDoItemId");

            ToDoItem item = ToDoItem.GetById(toDoItemId);

            if (item == null)
                throw new ArgumentException("Item was not found", "toDoItemId");
            if (User.Current != null && !User.Current.HasEmployerAccess(item.EmployerId, Permission.CreateToDo))
                throw new AbsenceSoftException("User does not have access to re-open this item");


            //create a new copy so that we maintane history
            item.Id = null;
            item.Status = ToDoItemStatus.Pending;
            item.DueDate = DateTime.UtcNow.AddDays(1);

            CheckForDuplicateToDo(item);
            item.Save();
            //Add activity history for re-opened ToDo items
            if (item.CaseId != null && item.WorkflowInstanceId != null)
            {
                var activityHistory = ActivityHistory.AsQueryable().Where(t =>
                     t.CaseId == item.CaseId
                     && t.WorkflowActivityId == item.Metadata.GetRawValue<Guid?>("WorkflowActivityId")
                     && t.WorkflowInstanceId == item.WorkflowInstanceId
                     && t.IsDeleted == false && t.Outcome != null
                     ).FirstOrDefault();
                activityHistory.Outcome = null;
                activityHistory.IsWaitingOnUserInput = true;
                activityHistory.Id = null;
                activityHistory.Metadata.SetRawValue("ToDoId", item.Id);
                activityHistory.Metadata.SetRawValue("ToDoItemId", item.Id);
                activityHistory.Save();
            }
            return item;
        }

        protected static void CheckForDuplicateToDo(ToDoItem newToDoItem)
        {
            /* need to close/cancel any future pending to-dos of the same kind */

            if (newToDoItem.Id != null)
                return;

            List<ToDoItem> duplicates = ToDoItem.AsQueryable().Where(t =>
                    t.Id != newToDoItem.Id &&
                    t.CaseId == newToDoItem.CaseId &&
                    t.ItemType == newToDoItem.ItemType &&
                    t.Title == newToDoItem.Title &&
                    (t.Status == ToDoItemStatus.Pending || t.Status == ToDoItemStatus.Open || t.Status == ToDoItemStatus.Overdue)
                    ).ToList();

            if (!duplicates.Any())
                return;

            //Allow duplicate comm todos
            List<ToDoItem> toRemove = new List<ToDoItem>(duplicates);
            foreach (ToDoItem t in duplicates.Where(x => x.ItemType == ToDoItemType.Communication))
            {
                if (IsNewCommTodo(t))
                    toRemove.Remove(t);
            }

            //cancel any duplicates
            toRemove.ForEach(t =>
            {
                t.Status = ToDoItemStatus.Cancelled;
                t.ModifiedById = newToDoItem.CreatedBy == null ? User.System.Id : newToDoItem.CreatedBy.Id;
                t.Save();
            });
        }

        private static bool IsNewCommTodo(ToDoItem newToDoItem)
        {
            //check for item type = communication and different template
            string templateOld = string.Empty;
            string templateNew = newToDoItem.Metadata.GetRawValue<string>("Template");
            Guid? accommIdOld = null;
            Guid? accommIdNew = newToDoItem.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
            accommIdOld = newToDoItem.Metadata.GetRawValue<Guid>("ActiveAccommodationId");
            templateOld = newToDoItem.Metadata.GetRawValue<string>("Template");

            //this is an accomm approval/denial letter for a different accomm - allow the duplicate
            if (accommIdNew.HasValue &&
                accommIdOld.HasValue &&
                (templateNew.Equals("ACCOM-APPROVED") || templateNew.Equals("ACCOM-DENIED")) &&
                (accommIdNew != accommIdOld))
            {
                return true;
            }

            //this is a communicatin todo with a different template allow duplicate
            else if (templateNew != templateOld)
                return true;

            return false;
        }

        public ToDoItem CompleteToDo(ToDoItem theItem, User theUser, bool completed, string resultText = null)
        {
            if (theUser == null) theUser = User.System;

            if (completed)
                theItem.WfOnToDoItemCompleted(theUser, resultText ?? BaseToDoActivity.CompleteOutcomeValue, true);

            return theItem;
        }//CompleteToDo

        /// <summary>
        /// Create Manual TodoItem
        /// </summary>
        /// <param name="theCase"></param>
        /// <param name="theUser"></param>
        /// <param name="type"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="dueDate"></param>
        /// <param name="priority"></param>
        /// <param name="assignToId"></param>
        /// <param name="assignToRole"></param>
        /// <returns></returns>
        public ToDoItem CreateManualTodoItem(Case theCase, User theUser, ToDoItemType type, string title, string description, DateTime dueDate, ToDoItemPriority priority, string assignToId = null)
        {
            if (theCase == null)
                throw new ArgumentNullException("theCase is NULL");
            if (String.IsNullOrWhiteSpace(title))
                throw new ArgumentNullException("title is NULL");
            if (String.IsNullOrWhiteSpace(description))
                throw new ArgumentNullException("description is NULL");

            if (theUser == null) theUser = User.System;
            if (!theUser.HasEmployerAccess(theCase.EmployerId, Permission.CreateToDo))
                throw new AbsenceSoftException("User does not have appropriate permissions to create a new ToDo item");

            ToDoItem wfi = new ToDoItem()
            {
                AssignedToId = assignToId ?? theCase.AssignedToId,
                CaseId = theCase.Id,
                CaseNumber = theCase.CaseNumber,
                CreatedById = theUser.Id,
                CustomerId = theCase.CustomerId,
                EmployerId = theCase.EmployerId,
                DueDate = dueDate,
                EmployeeId = theCase.Employee.Id,
                EmployeeNumber = theCase.Employee.EmployeeNumber,
                EmployeeName = string.Format("{0}, {1}", theCase.Employee.LastName, theCase.Employee.FirstName),
                ItemType = type,
                Metadata = new BsonDocument().Add("Description", new BsonString(description)),
                Priority = priority,
                Status = ToDoItemStatus.Pending,
                Title = title,
                Weight = 1,
                Optional = true,
            };

            wfi.Save();

            return wfi;
        }

        /// <summary>
        /// Gets a dictionary collection of all ToDoItemType the user has access to in an easy to use enum/string key value
        /// format.
        /// </summary>
        /// <param name="employerId">The employer to pull ToDo item types for.</param>
        /// <param name="theUser">The user that is requesting the list of ToDo item types.</param>
        /// <returns>A filtered dictionary of all ToDoItemTypes the user is allowed to created.</returns>
        public Dictionary<ToDoItemType, string> GetToDoTypes(string employerId, User theUser)
        {
            Dictionary<ToDoItemType, string> types = new Dictionary<ToDoItemType, string>();
            if (theUser == null || string.IsNullOrWhiteSpace(employerId))
                return types;

            //list of types with FeatureRestriction
            var typeList = ToDoItemTypeExtensions.GetToDoItemListWithFeatureRestriction();

            //Filter out list by features
            Employer employer = Employer.GetById(employerId);
            if (employer == null)
                return types;

            Customer customer = Customer.GetById(employer.CustomerId);
            if (customer == null)
                return types;

            var hasCustomAccommodations = customer.Metadata.GetRawValue<bool>("HasCustomAccommodations");

            if (!employer.HasFeature(Feature.ADA))
                typeList = typeList.Where(t => t.Value != Feature.ADA).ToDictionary(t => t.Key, t => t.Value);
            if (!employer.HasFeature(Feature.ShortTermDisability))
                typeList = typeList.Where(t => t.Value != Feature.ShortTermDisability).ToDictionary(t => t.Key, t => t.Value);
            if (!employer.HasFeature(Feature.EmployeeSelfService))
                typeList = typeList.Where(t => t.Value != Feature.EmployeeSelfService).ToDictionary(t => t.Key, t => t.Value);

            // If customer has his own accommodation types then no need to show global accommodation type's todos
            if (hasCustomAccommodations)
            {
                typeList.Remove(ToDoItemType.ScheduleErgonomicAssessment);
                typeList.Remove(ToDoItemType.CompleteErgonomicAssessment);
                typeList.Remove(ToDoItemType.EquipmentSoftwareInstalled);
                typeList.Remove(ToDoItemType.EquipmentSoftwareOrdered);
                typeList.Remove(ToDoItemType.HRISScheduleChange);
            }

            foreach (var t in typeList.Where(r => theUser.HasEmployerAccess(employerId, Permission.ToDos(r.Key))))
                types.Add(t.Key, t.Key.ToString().SplitCamelCaseString());

            types.OrderBy(m => m.Key);
            return types;
        }

        /// <summary>
        /// Gets a dictionary collection of all role groups of users a ToDo item for the given employer could be assigned to.
        /// </summary>
        /// <param name="theUser">The user requesting the list</param>
        /// <returns>A filtered list of role groups for user selection or ToDo item assignment.</returns>
        public Dictionary<string, string> GetToDoRoles(User theUser, ToDoItemType type)
        {
            using (new InstrumentationContext("ToDoService.GetToDoRoles"))
            {
                Dictionary<string, string> roles = new Dictionary<string, string>();
                roles.Add("000000000000000000000000", "All Roles");

                var perm = Permission.ToDos(type).FirstOrDefault();
                if (perm != null)
                {
                    Role.AsQueryable()
                        .Where(r => r.CustomerId == theUser.CustomerId && r.Permissions.Contains(perm.Id))
                        .ToList()
                        .ForEach(d => roles.Add(d.Id, d.Name));
                }
                return roles;
            }
        } // GetToDoRoles

        /// <summary>
        /// To the do user assignment suggestion list.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public ListResults ToDoUserAssignmentSuggestionList(User user, ListCriteria criteria)
        {
            using (new InstrumentationContext("ToDoService.ToDoUserAssignmentSuggestionList"))
            {
                if (criteria == null)
                    criteria = new ListCriteria();
                ListResults result = new ListResults(criteria);

                string caseIdFilter = criteria.Get<string>("CaseId");
                string todoItemTypeFilter = criteria.Get<string>("ToDoItemType");
                string roleFilter = criteria.Get<string>("Role");

                if (string.IsNullOrWhiteSpace(caseIdFilter))
                    return result;

                Case theCase = Case.AsQueryable().Where(c => c.Id == caseIdFilter).FirstOrDefault();
                if (theCase == null) return result;
                if (!user.HasEmployerAccess(theCase.EmployerId, Permission.CreateToDo))
                    return result;

                List<IMongoQuery> ands = new List<IMongoQuery>();
                // Always add the employer id to the filter
                if ((user ?? User.Current) != null)
                    ands.Add((user ?? User.Current).BuildDataAccessFilters());

                ands.Add(User.Query.EQ(t => t.CustomerId, user.CustomerId));
                ands.Add(User.Query.ElemMatch(e => e.Employers, y => y.EQ(r => r.EmployerId, theCase.EmployerId)));
                ands.Add(User.Query.NotExists(e => e.DisabledDate));
                if (!string.IsNullOrWhiteSpace(todoItemTypeFilter))
                {
                    ToDoItemType type = (ToDoItemType)Enum.Parse(typeof(ToDoItemType), todoItemTypeFilter);
                    Permission todoPerm = Permission.ToDos(type).FirstOrDefault();

                    List<string> roles = new List<string>();
                    if (!String.IsNullOrWhiteSpace(roleFilter))
                    {
                        if (roleFilter == "000000000000000000000000")
                            roles = Role.AsQueryable().Where(r => r.CustomerId == theCase.CustomerId && r.Permissions.Contains(todoPerm.Id)).Select(r => r.Id).ToList();

                        roles.Add(roleFilter);
                    }

                    ands.Add(User.Query.Or(
                        User.Query.In(u => u.Roles, roles.Select(r => r)),
                        User.Query.ElemMatch(e => e.Employers, e => e.And(Query.EQ("EmployerId", new ObjectId(theCase.EmployerId)),
                            Query.All("Roles", roles.Select(r => new BsonString(r)))))));
                }

                // apply filter
                // Set fields
                var query = User.Query.Find(ands.Count > 0 ? User.Query.And(ands) : null)
                    .SetFields(User.Query.IncludeFields(
                        w => w.FirstName,
                        w => w.LastName
                    ));

                // apply sorting
                if (!string.IsNullOrWhiteSpace(criteria.SortBy))
                {
                    query = query.SetSortOrder(criteria.SortDirection == AbsenceSoft.Data.Enums.SortDirection.Ascending
                       ? SortBy.Ascending(criteria.SortBy)
                       : SortBy.Descending(criteria.SortBy));
                }

                //var skip = ((criteria.PageNumber - 1) * criteria.PageSize);
                //if (skip < 0) skip = 0;
                //query = query.SetSkip(skip)
                //    .SetLimit(criteria.PageSize);

                result.Total = query.Count();
                result.Results = query.ToList().Select(c => new ListResult()
                    .Set("Id", c.Id)
                    .Set("FirstName", c.FirstName)
                    .Set("LastName", c.LastName)
                    .Set("DisplayName", c.DisplayName)
                ).ToList();

                return result;
            }
        }//ToDoUserAssignmentSuggestionList

        private bool ContainsOpenToDos(long? statusIdFilter)
        {
            if (statusIdFilter.HasValue == false || statusIdFilter == 0 || statusIdFilter == (int)ToDoItemStatus.Overdue || statusIdFilter == (int)ToDoItemStatus.Open || statusIdFilter == (int)ToDoItemStatus.Pending)
                return true;
            return false;
        }

        public void AssignToUserToDo(string caseId, User user)
        {
            var todos = ToDoItem.AsQueryable().Where(t => t.CaseId == caseId).ToList();
            foreach (var todo in todos)
            {
                if (todo.Status != ToDoItemStatus.Pending && todo.Status != ToDoItemStatus.Overdue)
                    continue;

                todo.AssignedToId = user.Id;
                todo.AssignedToName = user.DisplayName;
                todo.Save();
            }
        }
    }
}
