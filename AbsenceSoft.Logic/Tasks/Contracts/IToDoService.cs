﻿using System;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;

namespace AbsenceSoft.Logic.Tasks.Contracts
{
    public interface IToDoService
    {
        void AssignToUserToDo(string caseId, User user);
        ToDoItem CreateManualTodoItem(Case theCase, User theUser, ToDoItemType type, string title, string description, DateTime dueDate, ToDoItemPriority priority, string assignToId = null);

        ListResults ToDoItemList(User user, ListCriteria criteria);
    }
}
