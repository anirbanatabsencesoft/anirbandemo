﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class PolicyRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (rule.RuleValues != null && rule.RuleValues.Length > 0)
            {
                List<string>policyCodes = new List<string>();
                foreach(CaseSegment segment in empCase.Segments)
                {
                    foreach(AppliedPolicy appliedPolicy in segment.AppliedPolicies.Where(p => p.Status != EligibilityStatus.Ineligible))
                    {
                       policyCodes.Add(appliedPolicy.Policy.Code);
                    }
                }

                if (policyCodes.Intersect(rule.RuleValues).Any())
                {
                    ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
                }
                else if (rule.Behavior == null || rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                {
                    assigneeList?.Clear();
                }
            }
        }
    }
}
