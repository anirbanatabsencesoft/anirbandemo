﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class PolicyCrosswalkRule: BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (string.IsNullOrWhiteSpace(rule.Value))
                return;

            List<string> policyCrosswalkCodes = empCase.Segments.SelectMany(s => s.AppliedPolicies)
                .Where(ap => ap.PolicyCrosswalkCodes != null).SelectMany(ap => ap.PolicyCrosswalkCodes).Select(pcc => pcc.Code).ToList();

            if (policyCrosswalkCodes.Contains(rule.Value))
            {
                ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
            }
            else if (rule.Behavior == null || rule.Behavior == CaseAssignmentRuleBehavior.Filter)
            {
                assigneeList?.Clear();
            }
        }
    }
}
