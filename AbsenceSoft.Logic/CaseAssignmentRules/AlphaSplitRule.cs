﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class AlphaSplitRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (rule.Value != null && rule.Value2 != null)
            {                
                Employee emp = empCase.Employee;
                char[] lastName = emp.LastName.Trim().ToUpper().ToCharArray();
                char[] ruleValue = rule.Value.Trim().ToUpper().ToCharArray();
                char[] ruleValue2 = rule.Value2.Trim().ToUpper().ToCharArray();

                int maxLength = (ruleValue.Length > ruleValue2.Length) ? ruleValue.Length : ruleValue2.Length;
                long empRangeValue = 0;
                for (int i = 0; i < maxLength; i++)
                {
                    if (lastName.Length < (i + 1)) break;
                    empRangeValue = empRangeValue + lastName[i] * (long)Math.Pow(10, maxLength - i);
                }
                long ruleRangeValue = 0;
                for (int i = 0; i < maxLength; i++)
                {
                    if (ruleValue.Length < (i + 1)) break;
                    ruleRangeValue = ruleRangeValue + ruleValue[i] * (long)Math.Pow(10, maxLength - i);
                }
                long ruleRangeValue2 = 0;
                for (int i = 0; i < maxLength; i++)
                {
                    if (ruleValue2.Length < (i + 1)) break;
                    ruleRangeValue2 = ruleRangeValue2 + ruleValue2[i] * (long)Math.Pow(10, maxLength - i);
                }
                if (empRangeValue >= ruleRangeValue && empRangeValue <= ruleRangeValue2)
                {
                    ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
                }
                else if (rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                {
                    assigneeList?.Clear();
                }
            }
        }
    }
}
