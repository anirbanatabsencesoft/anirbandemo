﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class RotaryRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (assigneeList != null && assigneeList.Any())
            {
                // First, we need to see if any assigned team or person is being explicitly called out for assignment
                //  and if so, filter down to only that list for rotary, otherwise use the entire sample-set passed in
                //  through the assignment funnel.
                ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);

                // Get the list of users to filter by, but their ObjectId's in a BSON array format
                var userFilerList = string.Join(", ", assigneeList.Select(a => $"ObjectId(\"{a.Id}\")"));

                // Get the last assignment dates for each of those users
                var userLastAssignments = CaseAssignee.Repository.Collection.Aggregate(new AggregateArgs()
                {
                    OutputMode = AggregateOutputMode.Cursor,
                    Pipeline = new List<BsonDocument>(4)
                    {
                        // NOTE: We do want to actually include deleted items, because that's our assignment "history", including primary assignees
                        BsonDocument.Parse(string.Concat(@"{ 
                            ""$match"": {
                                ""CustomerId"": ObjectId(""", empCase.CustomerId, @"""),
                                ""UserId"": { ""$in"": [", userFilerList, @"] }
                            }
                        }")),
                        BsonDocument.Parse(@"{ ""$project"": { ""_id"": false, ""UserId"": 1, ""Date"": ""$cdt"" } }"),
                        BsonDocument.Parse(@"{ ""$group"": { ""_id"": ""$UserId"", ""LastAssigned"": { ""$max"": ""$Date"" } } }"),
                        BsonDocument.Parse(@"{ ""$sort"": { ""LastAssigned"": 1 } }")
                    }
                }).ToList();

                // Structure as a list of user date tuples. If the user last assignments list is empty, that's OK, things will just flow through
                var userTuples = userLastAssignments
                    .Select(t => new Tuple<string, DateTime>(t.GetRawValue<string>("_id"), t.GetRawValue<DateTime?>("LastAssigned") ?? DateTime.MinValue))
                    .OrderBy(t => t.Item2)
                    .ToList();

                var usersNeverAssigned = assigneeList.Select(a => a.Id).Except(userTuples.Select(t => t.Item1));
                if (usersNeverAssigned.Any())
                {
                    // Remove all other assignees but that one
                    assigneeList.RemoveAll(a => a.Id != usersNeverAssigned.First());
                    return;
                }

                // Get the very earliest assigned user without or longest since assigned a case
                // Remove all other assignees but that one
                assigneeList.RemoveAll(a => a.Id != userTuples.First().Item1);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="RotaryRule"/> should terminate further processing.
        /// </summary>
        /// <value>
        ///   <c>true</c> if terminate; otherwise, <c>false</c>.
        /// </value>
        public override bool Terminate { get { return true; } }
    }
}