﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class EmployerRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (assigneeList != null && assigneeList.Count > 0 && rule.RuleValues != null && rule.RuleValues.Length > 0)
            {
                // When the case is not for one of these employers, then bail and let the next rule pick up
                //  this rule wouldn't be applicable anyway.
                if (!rule.RuleValues.Contains(empCase.EmployerId))
                {
                    if (rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                    {
                        assigneeList?.Clear();
                    }
                    return;
                }

                List<User> appliedRuleAssigneeList = new List<User>();
                foreach (User user in assigneeList)
                {
                    rule.RuleValues.ForEach(value => {
                        // Only add the user to the list if we haven't already added them
                        if (!appliedRuleAssigneeList.Any(u => u.Id == user.Id) && user.HasEmployerAccess(value))
                            appliedRuleAssigneeList.Add(user);
                    });
                }

                if (appliedRuleAssigneeList != null && appliedRuleAssigneeList.Count > 0)
                    assigneeList = appliedRuleAssigneeList;

                // Ok we filtered all the stuff on our employer and access to that employer, NOW
                //  we need to apply the base implementation based on the team or individual selected.
                base.ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
            }
        }
    }
}
