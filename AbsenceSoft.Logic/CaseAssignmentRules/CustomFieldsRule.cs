﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class CustomFieldsRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            List<CustomField> customFieldsListAll = null;
            using (EmployerService empService = new EmployerService())
            {
                customFieldsListAll = empService.GetCustomFieldsForCustomer();
            }
            if (customFieldsListAll != null && customFieldsListAll.Any())
            {
                CustomField customFieldOfRule = customFieldsListAll.FirstOrDefault(c => c.Id == rule.CustomFieldId);
                if (customFieldOfRule != null)
                {
                    if (rule.CustomFieldType != null)
                    {
                        Enum.TryParse(rule.CustomFieldType, out EntityTarget entityTarget);
                        customFieldOfRule.Target = entityTarget;
                    }
                    if (customFieldOfRule.Target == EntityTarget.Case)
                    {
                        CustomField customFieldOfCase = empCase.CustomFields.FirstOrDefault(c => c.Code == customFieldOfRule.Code);
                        CheckAssignmentValues(customFieldOfCase, ref assigneeList, empCase, rule);
                    }
                    if (customFieldOfRule.Target == EntityTarget.Employee)
                    {
                        using (EmployeeService employeeService = new EmployeeService())
                        {
                            var employee = employeeService.GetEmployee(empCase.Employee.Id);
                            CustomField customFieldOfEmployee = employee.CustomFields.FirstOrDefault(c => c.Code == customFieldOfRule.Code);
                            CheckAssignmentValues(customFieldOfEmployee, ref assigneeList, empCase, rule);
                        }
                    }
                }
                else if (rule.Behavior == null || rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                {
                    assigneeList?.Clear();
                }
            }
            else if (rule.Behavior == null || rule.Behavior == CaseAssignmentRuleBehavior.Filter)
            {
                assigneeList?.Clear();
            }
        }

        /// <summary>
        /// Checks the assignment values.
        /// </summary>
        /// <param name="customField">The custom field.</param>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        private void CheckAssignmentValues(CustomField customField, ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (customField != null)
            {
                if (customField.ValueType == CustomFieldValueType.UserEntered && !string.IsNullOrWhiteSpace(rule.Value))
                {
                    if ((customField.DataType == CustomFieldType.Text && Convert.ToString(customField.SelectedValue) == rule.Value)
                        || (customField.DataType == CustomFieldType.Date && customField.SelectedValueText == rule.Value))
                    {
                        ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
                    }
                    else if (rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                    {
                        assigneeList?.Clear();
                    }
                }
                else if (customField.ValueType == CustomFieldValueType.SelectList 
                    && rule.RuleValues != null 
                    && rule.RuleValues.Length > 0 
                    && customField.SelectedValue != null
                    && rule.RuleValues.Contains(Convert.ToString(customField.SelectedValue)))
                {
                    List<User> appliedRuleAssigneeList = new List<User>();
                    foreach (User user in assigneeList)
                    {
                        rule.RuleValues.ForEach(value => {
                            // Only add the user to the list if we haven't already added them
                            if (!appliedRuleAssigneeList.Any(u => u.Id == user.Id) && user.HasEmployerAccess(value))
                                appliedRuleAssigneeList.Add(user);
                        });
                    }
                    if (appliedRuleAssigneeList != null && appliedRuleAssigneeList.Count > 0)
                    {
                        assigneeList = appliedRuleAssigneeList;
                    }

                    // Ok we filtered all the stuff on our employer and access to that employer, NOW
                    //  we need to apply the base implementation based on the team or individual selected.
                    base.ApplyAssigneeTeamFilter(ref assigneeList, empCase, rule);
                }
                else if (rule.Behavior == CaseAssignmentRuleBehavior.Filter)
                {
                    assigneeList?.Clear();
                }
            }            
        }
    }
}
