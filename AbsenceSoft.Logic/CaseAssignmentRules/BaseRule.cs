﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public abstract class BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public abstract void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule);

        /// <summary>
        /// Gets a value indicating whether this <see cref="BaseRule"/> is skipped.
        /// </summary>
        /// <value>
        ///   <c>true</c> if skip; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Skip { get { return false; } }

        /// <summary>
        /// Gets a value indicating whether this <see cref="BaseRule"/> is a terminator 
        /// (should terminate processing in the current rule set).
        /// </summary>
        /// <value>
        ///   <c>true</c> if terminate; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Terminate { get { return false; } }

        /// <summary>
        /// Filters the assignee list.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="assigneeFilterList">The assignee filter list.</param>
        public virtual void FilterAssigneeList(ref List<User> assigneeList, List<string> assigneeFilterList)
        {
            if (assigneeFilterList != null)
            {
                List<string> filteredList = assigneeList.Select(filter => filter.Id).Intersect(assigneeFilterList).ToList();
                if (filteredList != null && filteredList.Any())
                {
                    assigneeList = assigneeList.Where(assignee => filteredList.Contains(assignee.Id)).ToList();
                }
            }
        }

        /// <summary>
        /// Applies the assignee team filter.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public virtual void ApplyAssigneeTeamFilter(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            // If we're not going to specify an assignee (like in Rotary sometimes), then bail out and do nothing
            if (string.IsNullOrWhiteSpace(rule?.CaseAssignmentId))
            {
                return;
            }

            List<string> ruleAssigneeList = new List<string>();
            if (rule.CaseAssignmentType == CaseAssigmentType.TeamMember)
            {
                ruleAssigneeList.Add(rule.CaseAssignmentId);
            }
            else
            {
                ruleAssigneeList.AddRange(TeamMember.AsQueryable().Where(tm => tm.TeamId == rule.CaseAssignmentId).Select(tm => tm.UserId).ToList());
            }
            if (ruleAssigneeList.Any())
            {
                FilterAssigneeList(ref assigneeList, ruleAssigneeList);
            }
        }
    }
}
