﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using System.Collections.Generic;

namespace AbsenceSoft.Logic.CaseAssignmentRules
{
    public class UnassignedRule : BaseRule
    {
        /// <summary>
        /// Applies the case assignment rules.
        /// </summary>
        /// <param name="assigneeList">The assignee list.</param>
        /// <param name="empCase">The emp case.</param>
        /// <param name="rule">The rule.</param>
        public override void ApplyCaseAssignmentRules(ref List<User> assigneeList, Case empCase, CaseAssignmentRule rule)
        {
            if (assigneeList == null) assigneeList = new List<User>();
            assigneeList.Clear();
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="UnassignedRule"/> is terminate.
        /// </summary>
        /// <value>
        ///   <c>true</c> if terminate; otherwise, <c>false</c>.
        /// </value>
        public override bool Terminate { get { return true; } }
    }
}
