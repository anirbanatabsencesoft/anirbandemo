﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.RiskProfiles;

namespace AbsenceSoft.Logic.RiskProfiles.Contracts
{
    public interface IRiskProfileService
    {
        CalculatedRiskProfile CalculateAndSaveRiskProfile(string employeeId, string caseId = null);
        CalculatedRiskProfile GetLatestProfile(Employee employee, Case theCase = null);
    }
}
