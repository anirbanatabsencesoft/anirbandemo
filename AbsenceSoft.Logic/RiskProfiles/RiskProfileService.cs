﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Logic.Rules;
using AbsenceSoft.Logic.Cases;
using MongoDB.Driver;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.RiskProfiles.Contracts;

namespace AbsenceSoft.Logic.RiskProfiles
{
    public class RiskProfileService:BaseExpressionService, IRiskProfileService
    {
        public RiskProfileService()
        {

        }

        public RiskProfileService(Customer currentCustomer, Employer currentEmployer, User currentUser)
            :base(currentCustomer, currentEmployer, currentUser)
        {

        }

        /// <summary>
        /// Adds additional expressions specific to risk profiles
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<RuleExpressionGroup> GetRuleExpressions()
        {
            List<RuleExpressionGroup> ruleExpressions = base.GetRuleExpressions().ToList();
            /// Add in the new rules specifically for risk profiles
            var medicalConditionsGroup = GetGroup(ruleExpressions, "Medical Condition Risk", "For checking medical conditions that an employee may have");
            AddRuleIfNotExists(medicalConditionsGroup, new RuleExpression()
            {
                Name = "AverageAbsenceDurationComparedToODGGuidelines",
                Prompt = "Average Absence Duration",
                HelpText = "Compares the employees average absence duration to ODG guidelines. If a case is available, is for a specific case, otherwise is for all cases for the employee.",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Average Absence Duration",
                RuleDescription = "Absence Duration Compared To This Number",
                RuleOperator = "==",
                IsFunction = true,
                TypeName = typeof(int).AssemblyQualifiedName
            }
            );

            AddRuleIfNotExists(medicalConditionsGroup, new RuleExpression()
            {
                Name = "MedicalCondition",
                Prompt = "Medical Condition (ICD10)",
                HelpText = "For checking if an employee has a specific medical condition",
                ControlType = ControlType.SelectList,
                Operators = getOperators(),
                RuleName = "Medical Condition",
                RuleDescription = "Medical Condition An Employee May Have",
                RuleOperator = "==",
                Options = DiagnosisCode.AsQueryable().Where(dc => dc.CodeType == DiagnosisType.ICD10).Select(dc => new RuleExpressionOption()
                {
                    Text = dc.UIDescription,
                    Value = dc.Code
                }).ToList(),
                TypeName = typeof(String).AssemblyQualifiedName
            }
            );

            var employeeGroup = GetGroup(ruleExpressions, "Employee Information", "Used as filters, rules and expressions that evaluate properties and conditions of the employee's demographic, eligibility, relationship and other attributed data");
            AddRuleIfNotExists(employeeGroup, NumberOfCasesInTheLastYearRule("Consecutive"));
            AddRuleIfNotExists(employeeGroup, NumberOfCasesInTheLastYearRule("Intermittent"));
            AddRuleIfNotExists(employeeGroup, NumberOfCasesInTheLastYearRule("Reduced"));
            AddRuleIfNotExists(employeeGroup, NumberOfCasesInTheLastYearRule("Administrative"));
            AddRuleIfNotExists(employeeGroup, new RuleExpression()
            {
                Name = "NumberOfHoursUsedInTheLastYear",
                Prompt = "Number Of Hours Used In The Last Year",
                HelpText = "Checks the number of hours that the employee has used in the last year",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = "Number Of Hours In The Last Year",
                RuleDescription = "Number Of Hours Compared To This Number",
                RuleOperator = "==",
                IsFunction = true,
                TypeName = typeof(int).AssemblyQualifiedName
            }
            );
            //CustomField(name) --> lookup Employer custom fields...
            var employeeCustomfields = CustomField.AsQueryable().Where(f => f.CustomerId == CurrentUser.CustomerId && f.EmployerId == EmployerId).ToList().Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();
            if (employeeCustomfields.Any())
            {
                employeeCustomfields.ForEach(f =>
                {
                    var exp = new RuleExpression()
                    {
                        ControlType = f.DataType == CustomFieldType.Flag ? ControlType.CheckBox : f.DataType == CustomFieldType.Number ?
                                                ControlType.Numeric : f.DataType == CustomFieldType.Date ? ControlType.Date : ControlType.TextBox,
                        Operators = (f.DataType == CustomFieldType.Number || f.DataType == CustomFieldType.Date) ?
                                                getOperators(true, true, true, true, true, true) :
                                                getOperators(),
                        Name = f.DataType == CustomFieldType.Date ? "CustomDateField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Number) ? "CustomNumberField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Flag) ? "CustomBoolField" + f.Name.Replace(" ", string.Empty) : "CustomField" + f.Name.Replace(" ", string.Empty),
                        Prompt = f.Name,
                        HelpText = f.HelpText ?? f.Description,
                        RuleName = f.Name,
                        RuleDescription = f.Description,
                        RuleOperator = "==",
                        IsFunction = true,
                        IsCustom = true,
                        Parameters = new List<RuleExpressionParameter>()
                        {
                                        new RuleExpressionParameter()
                                        {
                                            ControlType = ControlType.Hidden,
                                            Name = "name",
                                            Value = f.Name,
                                            TypeName = typeof(String).AssemblyQualifiedName
                                        }
                        },
                        Value = f.SelectedValue
                    };
                    switch (exp.ControlType)
                    {
                        case ControlType.Date:
                            exp.TypeName = typeof(DateTime).AssemblyQualifiedName;
                            break;
                        case ControlType.Minutes:
                            exp.TypeName = typeof(Int32).AssemblyQualifiedName;
                            break;
                        case ControlType.Numeric:
                            exp.TypeName = typeof(Double).AssemblyQualifiedName;
                            break;
                        case ControlType.CheckBox:
                            exp.TypeName = typeof(Boolean).AssemblyQualifiedName;
                            break;
                        default:
                            exp.TypeName = typeof(String).AssemblyQualifiedName;
                            break;
                    }
                    if (f.ValueType == CustomFieldValueType.SelectList)
                    {
                        exp.ControlType = ControlType.SelectList;
                        exp.Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                    }
                    AddRuleIfNotExists(employeeGroup, exp);
                });
            }
            var caseGroup = GetGroup(ruleExpressions, "Case Information", "Used as filters, rules and expressions that evaluate properties and conditions of the case/absence, typically through intake");
            using (var caseService = new CaseService(CustomerId, EmployerId, CurrentUser))
            {
                AddRuleIfNotExists(caseGroup, new RuleExpression()
                {
                    Name = "Case.Reason.Code",
                    Prompt = "Absence Reason",
                    HelpText = "Checks the reason of the case",
                    ControlType = ControlType.SelectList,
                    Options = caseService.GetDistinctAbsenceReasons().Select(ar => new RuleExpressionOption()
                    {
                        Text = ar.Name,
                        Value = ar.Code
                    }).ToList(),
                    Operators = getOperators(),
                    RuleName = "Absence Reason",
                    RuleDescription = "The Case's Absence Reason",
                    RuleOperator = "==",
                    TypeName = typeof(string).AssemblyQualifiedName
                });
            }
            var caseCustomfields = CustomField.AsQueryable().Where(f => f.CustomerId == CurrentUser.CustomerId && f.EmployerId == EmployerId).ToList().Where(f => (f.Target & EntityTarget.Case) == EntityTarget.Case).ToList();
            if (caseCustomfields.Any())
            {
                caseCustomfields.ForEach(f =>
                {
                    var exp = new RuleExpression()
                    {
                        ControlType = f.DataType == CustomFieldType.Flag ? ControlType.CheckBox : f.DataType == CustomFieldType.Number ?
                                                ControlType.Numeric : f.DataType == CustomFieldType.Date ? ControlType.Date : ControlType.TextBox,
                        Operators = (f.DataType == CustomFieldType.Number || f.DataType == CustomFieldType.Date) ?
                                                getOperators(true, true, true, true, true, true) :
                                                getOperators(),
                        Name = f.DataType == CustomFieldType.Date ? "CaseCustomDateField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Number) ? "CaseCustomNumberField" + f.Name.Replace(" ", string.Empty) : (f.DataType == CustomFieldType.Flag) ? "CaseCustomBoolField" + f.Name.Replace(" ", string.Empty) : "CaseCustomField" + f.Name.Replace(" ", string.Empty),
                        Prompt = f.Name,
                        HelpText = f.HelpText ?? f.Description,
                        RuleName = f.Name,
                        RuleDescription = f.Description,
                        RuleOperator = "==",
                        IsFunction = true,
                        IsCustom = true,
                        Parameters = new List<RuleExpressionParameter>()
                        {
                                        new RuleExpressionParameter()
                                        {
                                            ControlType = ControlType.Hidden,
                                            Name = "name",
                                            Value = f.Name,
                                            TypeName = typeof(String).AssemblyQualifiedName
                                        }
                        },
                        Value = f.SelectedValue
                    };
                    switch (exp.ControlType)
                    {
                        case ControlType.Date:
                            exp.TypeName = typeof(DateTime).AssemblyQualifiedName;
                            break;
                        case ControlType.Minutes:
                            exp.TypeName = typeof(Int32).AssemblyQualifiedName;
                            break;
                        case ControlType.Numeric:
                            exp.TypeName = typeof(Double).AssemblyQualifiedName;
                            break;
                        case ControlType.CheckBox:
                            exp.TypeName = typeof(Boolean).AssemblyQualifiedName;
                            break;
                        default:
                            exp.TypeName = typeof(String).AssemblyQualifiedName;
                            break;
                    }
                    if (f.ValueType == CustomFieldValueType.SelectList)
                    {
                        exp.ControlType = ControlType.SelectList;
                        exp.Options = f.ListValues.Select(v => new RuleExpressionOption() { Value = v.Value, Text = v.Key }).ToList();
                    }
                    AddRuleIfNotExists(caseGroup, exp);
                });
            }
            AddGroupIfNotExists(ruleExpressions, medicalConditionsGroup);
            AddGroupIfNotExists(ruleExpressions, employeeGroup);
            AddGroupIfNotExists(ruleExpressions, caseGroup);

            return ruleExpressions;
        }

        public override RuleExpression GetExpression(Rule rule, IEnumerable<RuleExpressionGroup> expressionGroups)
        {
            if (!expressionGroups.Any())
                expressionGroups = GetRuleExpressions();

            return base.GetExpression(rule, expressionGroups);
        }

        private RuleExpression NumberOfCasesInTheLastYearRule(string ruleType)
        {
            return new RuleExpression()
            {
                Name = string.Format("NumberOf{0}CasesInTheLastYear", ruleType),
                Prompt = string.Format("Number Of {0} Cases In The Last Year", ruleType),
                HelpText = "Checks the number of cases that the employee has had in the last year",
                ControlType = ControlType.Numeric,
                Operators = getOperators(true, true, true, true, true, true),
                RuleName = string.Format("Number Of {0} Cases In The Last Year", ruleType),
                RuleDescription = string.Format("Number Of {0} Cases Compared To This Number", ruleType),
                RuleOperator = "==",
                IsFunction = true,
                TypeName = typeof(int).AssemblyQualifiedName
            };
        }

        public void DeleteRiskProfile(RiskProfile profile)
        {
            profile.Delete();
        }

        public RiskProfile SaveRiskProfile(RiskProfile profile)
        {
            RiskProfile savedProfile = RiskProfile.GetById(profile.Id);
            if(savedProfile != null &&
                (!savedProfile.IsCustom || savedProfile.EmployerId != EmployerId))
            {
                profile.Clean();
            }

            profile.CustomerId = CustomerId;
            profile.EmployerId = EmployerId;

            return profile.Save();
        }

        public RiskProfile GetRiskProfile(string id)
        {
            return RiskProfile.GetById(id);
        }

        public ListResults RiskProfileList(ListCriteria criteria)
        {
            if (criteria == null)
                criteria = new ListCriteria();

            ListResults result = new ListResults(criteria);
            List<IMongoQuery> ands = RiskProfile.DistinctAnds(CurrentUser, CustomerId, EmployerId);
            RiskProfile.Query.MatchesString(rp => rp.Name, criteria.Get<string>("Name"), ands);
            RiskProfile.Query.MatchesString(rp => rp.Code, criteria.Get<string>("Code"), ands);

            List<RiskProfile> riskProfiles = RiskProfile.DistinctAggregation(ands);
            result.Total = riskProfiles.Count();
            riskProfiles = riskProfiles
                .SortByListCriteria(criteria)
                .PageByListCriteria(criteria)
                .ToList();

            result.Results = riskProfiles.Select(rp => new ListResult()
                .Set("Id", rp.Id)
                .Set("CustomerId", rp.CustomerId)
                .Set("EmployerId", rp.EmployerId)
                .Set("Name", rp.Name)
                .Set("Order", rp.Order)
                .Set("Code", rp.Code)
                .Set("Color", rp.Color)
                .Set("Icon", rp.Icon)
                .Set("Description", rp.Code)
                .Set("ModifiedDate", rp.ModifiedDate)
            );

            return result;
        }

        public void UpdateRiskProfileOrder(string id, int order)
        {
            RiskProfile profile = RiskProfile.GetById(id);
            if (profile != null)
            {
                profile.Order = order;
                profile.Save();
            }
        }

        public int GetNextOrder()
        {
            var riskProfiles = RiskProfile.DistinctFind(null, CustomerId, EmployerId);
            if (riskProfiles.Count() == 0)
                return 1;

            return riskProfiles.Max(rp => rp.Order) + 1;
        }

        /// <summary>
        /// Shortcut method to calculate and save the risk profile of an employee and/or case based on the id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public CalculatedRiskProfile CalculateAndSaveRiskProfile(string employeeId, string caseId = null)
        {
            Employee employee = Employee.GetById(employeeId);
            Case theCase = Case.GetById(caseId);
            return CalculateAndSaveRiskProfile(employee, theCase);
        }

        /// <summary>
        /// Shortcut method to calculate and save a risk profile <para />
        /// Will return null no risk profile was generated
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="theCase"></param>
        /// <returns></returns>
        public CalculatedRiskProfile CalculateAndSaveRiskProfile(Employee employee, Case theCase = null)
        {
            RiskProfile profile = CalculateRiskProfile(employee, theCase);
            /// The risk profile didn't change, so let's just be lazy and return that instead of polluting the history
            CalculatedRiskProfile latestProfile = GetLatestProfile(employee, theCase);
            if (LatestCalculatedProfileMatchesCurrentCalculation(latestProfile, profile))
                return latestProfile;

            UpdateEmployeeRiskProfile(employee, profile);
            UpdateCaseRiskProfile(theCase, profile);

            CalculatedRiskProfile finalProfile = new CalculatedRiskProfile()
            {
                RiskProfile = profile,
                Employee = employee,
                Case = theCase,
                EmployerId = EmployerId ?? employee.EmployerId,
                CustomerId = CustomerId,
            };
            return SaveCalculatedRiskProfile(finalProfile);
        }

        public CalculatedRiskProfile GetLatestProfile(Employee employee, Case theCase = null)
        {
            string caseId = null;
            if (theCase != null)
                caseId = theCase.Id;

            return CalculatedRiskProfile.AsQueryable()
                .Where(crp => crp.EmployeeId == employee.Id && crp.CaseId == caseId)
                .OrderByDescending(crp => crp.CreatedDate).FirstOrDefault();
        }

        public bool LatestCalculatedProfileMatchesCurrentCalculation(CalculatedRiskProfile calculatedProfile, RiskProfile currentProfile)
        {
            if (calculatedProfile == null)
                return false;

            if (calculatedProfile.RiskProfile == null && currentProfile != null)
                return false;

            if (calculatedProfile.RiskProfile != null && currentProfile == null)
                return false;

            if (calculatedProfile.RiskProfile == null && currentProfile == null)
                return true;

            if (calculatedProfile.RiskProfile.Id != currentProfile.Id)
                return false;

            return true;
        }

        private void UpdateEmployeeRiskProfile(Employee employee, RiskProfile profile)
        {
            if (RiskProfileMatchesTarget(profile, EntityTarget.Employee))
            {
                employee.RiskProfile = profile;
                employee.Save();
            }
        }

        private void UpdateCaseRiskProfile(Case theCase, RiskProfile profile)
        {
            if (theCase != null && RiskProfileMatchesTarget(profile, EntityTarget.Case))
            {
                theCase.RiskProfile = profile;
                theCase.Save();
            }
        }

        private bool RiskProfileMatchesTarget(RiskProfile profile, EntityTarget target)
        {
            if (profile == null)
                return true;

            if((profile.Target & target) == target)
                return true;

            return false;
        }

        /// <summary>
        /// Saves a calculated risk profile, representing risk profile at a specific point in time
        /// </summary>
        /// <param name="calculatedRiskProfile"></param>
        /// <returns></returns>
        public CalculatedRiskProfile SaveCalculatedRiskProfile(CalculatedRiskProfile calculatedRiskProfile)
        {
            return calculatedRiskProfile.Save();
        }

        /// <summary>
        /// Calculates the highest risk profile for an employee and or case
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="theCase"></param>
        /// <returns></returns>
        public RiskProfile CalculateRiskProfile(Employee employee, Case theCase = null)
        {
            List<RiskProfile> profiles = RiskProfile.DistinctFind(null, CustomerId, EmployerId).ToList();
            if (theCase == null)
                profiles = profiles.Where(p => (p.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();
            else
                profiles = profiles.Where(p => (p.Target & EntityTarget.Case) == EntityTarget.Case).ToList();

            return profiles.Where(p => ContextMatchesRiskProfile(p, employee, theCase))
                .OrderByDescending(p => p.Order)
                .FirstOrDefault();
        }

        /// <summary>
        /// Determines whether the employee and case, if provided, matches the specific risk profile
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="employee"></param>
        /// <param name="theCase"></param>
        /// <returns></returns>
        private bool ContextMatchesRiskProfile(RiskProfile profile, Employee employee, Case theCase)
        {
            /// No rules means they match
            if (profile.RuleGroups == null)
                return true;

            Employer employer = null;
            if (employee != null)
                employer = employee.Employer;
            else if (theCase != null)
                employer = theCase.Employer;


            var leaveOfAbsence = new LeaveOfAbsence()
            {
                Employee = employee,
                Case = theCase,
                Employer = employer
            };

            using (var ruleService = new RuleService<LeaveOfAbsence>())
            {
                return profile.RuleGroups.All(rg => ruleService.EvaluateRuleGroup(rg, leaveOfAbsence));
            }
        }
    }
}
