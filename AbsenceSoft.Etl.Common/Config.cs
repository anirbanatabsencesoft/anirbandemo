﻿using System;
using System.Configuration;
using System.Linq;

namespace AbsenceSoft.Etl
{
    public class Config
    {
        private const string KEY_DW_CONNECTION_STRING = "DataWarehouseSettings";
        private const string KEY_SYNC_CONNECTION_STRING = "SynchronizerSettings";
        private const string KEY_LOOK_BACK_MILLISECONDS = "LookBackMilliSeconds";
        private const string KEY_SKIP_LOOKBACK_RETRY = "SkipLookBackRetry";
        private const string KEY_LOG_DEGUG_INFO = "LogDebugInfo";
        private const int MAX_SUPPORTED_DOP = 512;

        private static readonly int _maxDegreesOfParallelism = GetMaxDegreesOfParallelism();

        /// <summary>
        /// Initializes a new instance of the <see cref="Config"/> class.
        /// </summary>
        public Config()
        {
            try
            {
                DataWarehouseConnectionString =  ConfigurationManager.ConnectionStrings[KEY_DW_CONNECTION_STRING].ToString();
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorStacktrace = e.ToString();
                ErrorMessage = e.Message;
            }

            try
            {
                SynchronizerConnectionString = ConfigurationManager.ConnectionStrings[KEY_SYNC_CONNECTION_STRING].ToString();
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorStacktrace = e.ToString();
                ErrorMessage = e.Message;
            }

            //Set default to 3 secs
            try
            {
                LookBackTimeInMilliSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[KEY_LOOK_BACK_MILLISECONDS].ToString());
            }
            catch { LookBackTimeInMilliSeconds = 3; }


            //set look back retry. Default 3
            try
            {
                SkipLookBackRetry = Convert.ToInt32(ConfigurationManager.AppSettings[KEY_SKIP_LOOKBACK_RETRY].ToString());
            }
            catch { SkipLookBackRetry = 3; }

            //set the Log Debug Info
            var logInfo = ConfigurationManager.AppSettings[KEY_LOG_DEGUG_INFO].ToString();
            if(!string.IsNullOrWhiteSpace(logInfo) && logInfo.ToUpper() == "TRUE")
            {
                LogDebugInfo = true;
            }
            
        }

        /// <summary>
        /// Gets the error stacktrace.
        /// </summary>
        /// <value>
        /// The error stacktrace.
        /// </value>
        public string ErrorStacktrace { get; private set; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// HasError Property in class Column and is of type bool
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError { get; private set; }

        /// <summary>
        /// Gets or sets the data warehouse connection string.
        /// </summary>
        /// <value>
        /// The data warehouse connection string.
        /// </value>
        public string DataWarehouseConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the synchronizer connection string.
        /// </summary>
        /// <value>
        /// The synchronizer connection string.
        /// </value>
        public string SynchronizerConnectionString { get; set; }

        /// <summary>
        /// Gets the maximum degrees of parallelism.
        /// </summary>
        /// <value>
        /// The maximum degrees of parallelism.
        /// </value>
        public static int MaxDegreesOfParallelism { get { return _maxDegreesOfParallelism; } }

        /// <summary>
        /// Returns the Lookback time for mongo query to ensure nothing is missed.
        /// </summary>
        public int LookBackTimeInMilliSeconds { get; private set; } 

        /// <summary>
        /// After a certain number of same date retries, the system will stop look back
        /// </summary>
        public int SkipLookBackRetry { get; private set; }

        /// <summary>
        /// Log the sync debug info in database if true
        /// </summary>
        public bool LogDebugInfo { get; private set; } = false;

        /// <summary>
        /// Gets the maximum degrees of parallelism.
        /// </summary>
        /// <returns></returns>
        static int GetMaxDegreesOfParallelism()
        {
            return 1;
        }
    }
}
