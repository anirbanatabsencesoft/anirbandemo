﻿namespace AbsenceSoft.Etl
{
    public static class Const
    {
        public const long DEFAULT_HISTORY_COUNT = 0;
        public const long VER_SEQ_NEWEST_VALUE = 999999999999999999;
        public const long VER_SEQ_DELETED_VALUE = 0;

        public static class Dimensions
        {
            public const int NONE = 0;
            public const int RETRY_QUEUE = -1;
            public const int DIMENSION_EMPLOYEE = 1;
            public const int DIMENSION_EMPLOYER = 2;
            public const int DIMENSION_CUSTOMER = 3;
            public const int DIMENSION_CASES = 4;
            public const int DIMENSION_TODOITEM = 5;
            public const int DIMENSION_USERS = 6;
            public const int DIMENSION_ACCOMMODATION = 18;
            public const int DIMENSION_ORGANIZATION = 19;
            public const int DIMENSION_EMPLOYEE_ORGANIZATION = 20;
            public const int DIMENSION_DEMAND = 21;
            public const int DIMENSION_DEMAND_TYPE = 22;
            public const int DIMENSION_EMPLOYEE_RESTRICTION = 24;
            public const int DIMENSION_EMPLOYEE_CONTACT = 25;
            public const int DIMENSION_CASE_POLICY = 26;
            public const int DIMENSION_ORGANIZATION_ANNUAL_INFO = 27;
            public const int DIMENSION_CASE_AUTHORIZED_SUBMITTER = 28;
            public const int DIMENSION_CASE_EVENT_DATE = 29;
            public const int DIMENSION_CASE_PAY_PERIOD = 30;
            public const int DIMENSION_COMMUNICATION = 31;
            public const int DIMENSION_COMMUNICATION_PAPERWORK = 32;
            public const int DIMENSION_ATTACHMENT = 33;
            public const int DIMENSION_EMPLOYEE_NOTE = 34;
            public const int DIMENSION_EMPLOYEE_NECESSITY = 35;
            public const int DIMENSION_EMPLOYEE_JOB = 36;
            public const int DIMEMSION_EMPLOYER_SERVICE_OPTIONS = 37;
            public const int DIMENSION_CASE_NOTE = 38;
            public const int DIMENSION_WORKFLOW_INSTANCE = 39;
            public const int DIMENSION_VARIABLE_SCHEDULE_TIME = 40;
            public const int DIMEMSION_ROLE = 41;
            public const int DIMEMSION_EMPLOYER_CONTACT = 42;
            public const int DIMEMSION_TEAM = 43;
            public const int DIMEMSION_CUSTOMER_SERVICE_OPTION = 44;
            public const int DIMEMSION_TEAM_MEMBER = 45;
            public const int DIMEMSION_EVENT = 46;
            public const int DIMEMSION_EMPLOYER_CONTACT_TYPES = 47;
            public const int DIMEMSION_RELAPSE = 48;
            public const int DIMENSION_DENIAL_REASON = 49;
            public const int DIMENSION_CASE_SEGMENT = 50;
        }
    }
}
