﻿using AbsenceSoft;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl
{
    public static class Extensions
    {
        /// <summary>
        /// Gets a value back or DbNull.Value if the value passed in is null or has no viable value.
        /// </summary>
        /// <typeparam name="T">The type of value being passed in, automatically takes on the unboxed value type of the first parameter.</typeparam>
        /// <param name="value">The value to return if not null/empty.</param>
        /// <returns>Either <paramref name="value"/> or <c>DbNull.Value</c>.</returns>
        public static object ValueOrDbNull<T>(this T value)
        {
            if (value == null)
                return DBNull.Value;
            if (string.IsNullOrWhiteSpace(value.ToString()))
                return DBNull.Value;
            if (value.Equals(new DateTime()))
                return DBNull.Value;
            return value;
        }

        /// <summary>
        /// Gets a value back or DbNull.Value if the value passed in is null.
        /// </summary>
        /// <typeparam name="T">The type of nullable base struct being passed in, automatically takes on 
        /// the unboxed value type of the first parameter.</typeparam>
        /// <param name="value">The value to return if not null.</param>
        /// <returns>Either <paramref name="value"/>.Value or <c>DbNull.Value</c>.</returns>
        public static object ValueOrDbNull<T>(this T? value) where T : struct
        {
            if (!value.HasValue)
                return DBNull.Value;
            return value.Value;
        }

        /// <summary>
        /// Gets a string back or DbNull.Value if the string passed in is null or whitespace.
        /// </summary>
        /// <param name="value">The value to return if not null/empty.</param>
        /// <returns>Either <paramref name="value"/> or <c>DbNull.Value</c>.</returns>
        public static object ValueOrDbNull(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return DBNull.Value;
            return value;
        }

        /// <summary>
        /// Gets a value dictionary as a JSON object graph back or DbNull.Value if the value passed in is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static object JsonOrDbNull<T>(this Dictionary<string, T> value)
        {
            if (value == null || !value.Keys.Any())
                return DBNull.Value;

            return value.ToJSON();
        }
        
        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        public static string ExecuteString(this NpgsqlConnection conn, string sql)
        {
            string val = null;
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, conn))
            {
                object res = cmd.ExecuteScalar();
                if (res != null && !res.Equals(DBNull.Value))
                    val = Convert.ToString(res);
            }
            return val;
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar nullable value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        public static T? ExecuteScalar<T>(this NpgsqlConnection conn, string sql) where T : struct
        {
            T? val = null;
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, conn))
            {
                object res = cmd.ExecuteScalar();
                if (res != null && !res.Equals(DBNull.Value))
                    val = (T)Convert.ChangeType(res, typeof(T));
            }
            return val;
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        public static DateTime? ExecuteDate(this NpgsqlConnection conn, string sql)
        {
            return ExecuteScalar<DateTime>(conn, sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        public static int? ExecuteInt(this NpgsqlConnection conn, string sql)
        {
            return ExecuteScalar<int>(conn, sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        public static long? ExecuteLong(this NpgsqlConnection conn, string sql)
        {
            return ExecuteScalar<long>(conn, sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to see if any rows come back.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <returns><c>true</c>if any rows are returned; otherwise <c>false</c>.</returns>
        public static bool ExecuteExist(this NpgsqlConnection conn, string sql)
        {
            using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
            using (NpgsqlDataReader dr = command.ExecuteReader(CommandBehavior.SingleRow))
                if (dr.Read())
                    return true;

            return false;
        }

        /// <summary>
        /// Gets the value or null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static T? GetValueOrNull<T>(this NpgsqlDataReader reader, int index) where T : struct
        {
            try
            {
                var val = reader.GetValue(index);
                if (val == DBNull.Value)
                    return null;
                return (T)Convert.ChangeType(val, typeof(T));
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Gets the value or default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader">The reader.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(this NpgsqlDataReader reader, int index)
        {
            T retVal = default(T);
            try
            {
                var val = reader.GetValue(index);
                if (val == DBNull.Value)
                    return retVal;
                retVal = (T)Convert.ChangeType(val, typeof(T));
            }
            catch { }
            return retVal;
        }
        /// <summary>
        /// The method checks for DatimeTime minvalue,if true return null otherwise return the value.
        /// </summary>
        /// <param name="date"></param>
        /// <returns>Nullable DateTime</returns>
        public static DateTime? GetValueOrNullIfMinValue(this DateTime? date)
        {
            if (date.HasValue && date.Value == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return date;
            }
        }
        /// <summary>
        /// The method checks for DatimeTime minvalue,if true return null otherwise return the value.
        /// </summary>
        /// <param name="date"></param>
        /// <returns>Nullable DateTime</returns>
        public static DateTime? GetValueOrNullIfMinValue(this DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                return null;
            }
            else
            {
                return date;
            }
        }
    }
}
