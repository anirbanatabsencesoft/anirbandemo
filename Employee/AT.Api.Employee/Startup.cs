﻿using System.Web.Http;
using AT.Api.Core;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(AT.Api.Employee.Startup))]

namespace AT.Api.Employee
{
    /// <summary>
    /// The startup class for API
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Sets the configuration for the app
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();

            SwaggerConfig.Register(configuration);

            WebApiConfig.Register(configuration);
            app.UseWebApi(configuration);

            app.UseOAuthAuthorizationServer(AuthManager.BuildAuthOptions());

            configuration.EnsureInitialized();
        }
    }
}
