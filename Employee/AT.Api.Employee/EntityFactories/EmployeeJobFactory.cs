﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AT.Api.Employee.Models.EmployeeJob;
using System;

namespace AT.Api.Employee.EntityFactories
{
    internal class EmployeeJobFactory
    {
        internal static EmployeeJob FromEmployeeJobModel(CreateEmployeeJobModel model,
            EmployeeJob employeeJob,
            User user,
            DateTime serviceDate,
            string employeeNumber,
            Job job)
        {
            if (employeeJob != null && employeeJob.JobCode != model.JobCode)
            {
                if (employeeJob.Dates == null)
                {
                    employeeJob.Dates = new DateRange(serviceDate);
                }

                employeeJob.Dates.EndDate = DateTime.UtcNow.ToMidnight();
                employeeJob.Save();
                employeeJob = null;
            }
            
            employeeJob = employeeJob ?? new EmployeeJob(job);

            employeeJob.JobTitle = model.JobTitle;
            employeeJob.EmployeeNumber = employeeNumber;
            employeeJob.OfficeLocationCode = model.OfficeLocationCode;
            employeeJob.JobPosition = model.JobPosition;

            employeeJob.Dates = model.StartDate.HasValue
                ? new DateRange(model.StartDate.ToMidnight() ?? serviceDate.ToMidnight(), model.EndDate.ToMidnight())
                : DateRange.Null;

            if (model.Status != (int)employeeJob.Status.Value)
            {
                var status = (AdjudicationStatus)Enum.ToObject(typeof(AdjudicationStatus), model.Status);
                var jobDenialReason = !model.DenialReason.HasValue
                    ? null
                    : (JobDenialReason?)Enum.ToObject(typeof(JobDenialReason), model.DenialReason);

                employeeJob.Status.Value = status;
                employeeJob.Status.Reason = jobDenialReason;
                employeeJob.Status.Comments = model.DenialReasonOther;
                employeeJob.Status.User = user;
                employeeJob.Status.Date = DateTime.UtcNow.ToMidnight();
            }

            return employeeJob;
        }
    }
}