﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AT.Api.Employee.Models.EmployeeContact;
using System;

namespace AT.Api.Employee.EntityFactories
{
    internal class ContactFactory
    {
        internal static Contact FromContactModel(ContactModel contact)
        {
            var model = new Contact
            {
                CompanyName = contact.CompanyName,
                CellPhone = contact.CellPhone,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Title = contact.Title,
                MiddleName = contact.MiddleName,
                HomePhone = contact.HomePhone,
                WorkPhone = contact.WorkPhone,
                Email = contact.Email,
                DateOfBirth = contact.DateOfBirth.HasValue 
                                ? (DateTime?)contact.DateOfBirth.Value.ToMidnight() 
                                : null,
                Address = AddressFactory.FromAddressModel(contact.Address)
            };

            return model;
        }

        internal static EmployeeContact FromEmployeeContactModel(
            EmployeeContactModel employeeContactModel,
            string employerId,
            string customerId,
            string employeeId,
            EmployeeContact employeeContact = null)
        {
            employeeContact = employeeContact ?? new EmployeeContact();

            var militaryStatus = employeeContactModel.MilitaryStatus.HasValue
                ? (MilitaryStatus)Enum.ToObject(typeof(MilitaryStatus), employeeContactModel.MilitaryStatus)
                : MilitaryStatus.Civilian;

            employeeContact.EmployerId = employerId;
            employeeContact.CustomerId = customerId;
            employeeContact.EmployeeId = employeeId;
            employeeContact.ContactTypeCode = employeeContactModel.ContactTypeCode;
            employeeContact.RelatedEmployeeNumber = employeeContactModel.RelatedEmployeeNumber;
            employeeContact.MilitaryStatus = militaryStatus;
            employeeContact.Contact = FromContactModel(employeeContactModel.Contact);

            return employeeContact;
        }
    }
}