﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AT.Api.Shared.Model.Common;
using System.Linq;

namespace AT.Api.Employee.EntityFactories
{
    internal class EmployeeWorkScheduleFactory
    {
        internal static Schedule FromCreateWorkScheduleModel(
            CreateWorkScheduleModel model)
        {
            return new Schedule
            {
                ScheduleType = (ScheduleType)model.ScheduleType,
                StartDate = model.StartDate.ToMidnight(),
                EndDate = model.EndDate.ToMidnight(),
                Times = model.Times.Select(t => FromBaseTimeModel(t)).ToList()
            };
        }

        internal static Time FromBaseTimeModel(BaseTimeModel model)
        {
            return new Time
            {
                SampleDate = model.SampleDate.ToMidnight(),
                TotalMinutes = model.TotalMinutes,
                IsHoliday = model.IsHoliday
            };
        }
    }
}