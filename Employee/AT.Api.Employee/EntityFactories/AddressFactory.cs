﻿using AbsenceSoft.Data;
using AT.Api.Shared.Model.Common;

namespace AT.Api.Employee.EntityFactories
{
    internal class AddressFactory
    {
        internal static Address FromAddressModel(AddressModel address)
        {
            var model = new Address
            {
                Address1 = address.Address1,
                Address2 = address.Address2,
                City = address.City,
                Country = address.Country,
                PostalCode = address.PostalCode,
                State = address.State
            };

            return model;
        }
    }
}