﻿using AbsenceSoft.Data.Enums;
using AT.Api.Employee.Models.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using ATCustomers = AbsenceSoft.Data.Customers;
using ATData = AbsenceSoft.Data;

namespace AT.Api.Employee.EntityFactories
{
    internal class EmployeeFactory
    {
        internal static ATCustomers.Employee FromSaveEmployeeModel(
            string customerId,
            string employerId,
            SaveEmployeeModel baseEmployeeModel)
        { 
           
            var listOfMinutesWorkedPerWeek = new List<ATCustomers.MinutesWorkedPerWeek>();

            if (baseEmployeeModel.MinutesWorkedPerWeek != null && baseEmployeeModel.MinutesWorkedPerWeek.Count > 0)
            {
                listOfMinutesWorkedPerWeek = baseEmployeeModel.MinutesWorkedPerWeek
                    .Select(item => new ATCustomers.MinutesWorkedPerWeek
                    {
                        MinutesWorked = item.MinutesWorked,
                        AsOf = item.AsOf
                    })
                    .ToList();
            }

            return new ATCustomers.Employee
            {
                EmployerId = employerId,
                EmployeeNumber = baseEmployeeModel.Number,
                FirstName = baseEmployeeModel.FirstName,
                MiddleName = baseEmployeeModel.MiddleName,
                LastName = baseEmployeeModel.LastName,
                DoB = baseEmployeeModel.DateOfBirth,
                MilitaryStatus = baseEmployeeModel.MilitaryStatus,
                Gender = (Gender)baseEmployeeModel.Gender,
                Ssn = string.IsNullOrEmpty(baseEmployeeModel.Ssn) ? null : new ATData.CryptoString(baseEmployeeModel.Ssn),
                Info = new ATCustomers.EmployeeInfo
                {
                    Email = baseEmployeeModel.Info.Email,
                    AltEmail = baseEmployeeModel.Info.AltEmail,
                    Address = new ATData.Address
                    {
                        Address1 = baseEmployeeModel.Info.Address.Address1,
                        Address2 = baseEmployeeModel.Info.Address.Address2,
                        City = baseEmployeeModel.Info.Address.City,
                        State = baseEmployeeModel.Info.Address.State,
                        Country = baseEmployeeModel.Info.Address.Country,
                        PostalCode = baseEmployeeModel.Info.Address.PostalCode
                    },
                    AltAddress = new ATData.Address
                    {
                        Address1 = baseEmployeeModel.Info.AltAddress.Address1,
                        Address2 = baseEmployeeModel.Info.AltAddress.Address2,
                        City = baseEmployeeModel.Info.AltAddress.City,
                        State = baseEmployeeModel.Info.AltAddress.State,
                        Country = baseEmployeeModel.Info.AltAddress.Country,
                        PostalCode = baseEmployeeModel.Info.AltAddress.PostalCode
                    },
                    WorkPhone = baseEmployeeModel.Info.WorkPhone,
                    HomePhone = baseEmployeeModel.Info.HomePhone,
                    CellPhone = baseEmployeeModel.Info.CellPhone,
                    AltPhone = baseEmployeeModel.Info.AltPhone,
                    OfficeLocation = baseEmployeeModel.Info.OfficeLocation
                },
                WorkCountry = baseEmployeeModel.WorkCountry,
                WorkState = baseEmployeeModel.WorkState,
                WorkCounty = baseEmployeeModel.WorkCounty,
                WorkCity = baseEmployeeModel.WorkCity,
                CostCenterCode = baseEmployeeModel.CostCenterCode,
                Meets50In75MileRule = baseEmployeeModel.Meets50In75MileRule,
                HireDate = baseEmployeeModel.HireDate,
                RehireDate = baseEmployeeModel.RehireDate,
                ServiceDate = baseEmployeeModel.ServiceDate,
                TerminationDate=baseEmployeeModel.TerminationDate,
                MinutesWorkedPerWeek = listOfMinutesWorkedPerWeek,
                PayType = baseEmployeeModel.PayType,
                Salary = baseEmployeeModel.Salary,
                IsExempt = baseEmployeeModel.IsExempt,
                IsKeyEmployee = baseEmployeeModel.IsKeyEmployee,
                PayScheduleId = baseEmployeeModel.PayScheduleId,
                Status = (EmploymentStatus)baseEmployeeModel.Status,
                SpouseEmployeeNumber = baseEmployeeModel.SpouseEmployeeNumber,
                Department = baseEmployeeModel.Department,
                IsFlightCrew=baseEmployeeModel.IsFlightCrew,
                CustomerId = customerId
            };
        }
    }
}