﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AT.Api.Employee.Models.EmployeeOrganization;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.EntityFactories
{
    internal class EmployeeOrganizationFactory
    {
        internal static EmployeeOrganization FromEmployeeOrganizationModel(
            CreateEmployeeOrganizationModel model, 
            Customers.Employee employee,
            EmployeeOrganization employeeOrganization,
            Organization organization)
        {
            
            organization = organization ?? new Organization();
            string employeeOrganizationId = null;

            if (employeeOrganization != null)
            {
                organization = employeeOrganization.Organization;
                employeeOrganizationId = employeeOrganization.Id;
            } 

            if (employeeOrganization != null)
            {
                return employeeOrganization;
            }

            return new EmployeeOrganization(organization)
            {
                Id = employeeOrganizationId,
                Employee = employee,
                Customer = employee.Customer,
                Employer = employee.Employer
            };
        }

        internal static Organization FromOrganizationModel(
            CreateEmployeeOrganizationModel model,            
            Organization organization,
            string customerId,
            string employerId)
        {
            
            organization = organization ?? new Organization();

            var address = organization.Address ?? new Address();

            address.Address1 = model.Address1;
            address.Address2 = model.Address2;
            address.City = model.City;
            address.State = model.State;
            address.PostalCode = model.PostalCode;
            address.Country = model.CountryCode;

            organization.Code = model.Code;
            organization.Name = model.Name;
            organization.Description = model.Description;
            organization.TypeCode = model.TypeCode;
            organization.SicCode = model.SicCode;
            organization.NaicsCode = model.NaicsCode;
            organization.Address = address;

            organization.CustomerId = customerId;
            organization.EmployerId = employerId;

            organization.Path = string.IsNullOrWhiteSpace(model.Code)
                ? HierarchyPath.Root(model.Code)
                : HierarchyPath.Root(model.ParentOrgCode).AddDescendant(model.Code);

            return organization;
        }
    }
}