﻿using AT.Api.Core;
using AT.Api.Employee.Models.Employee;
using AT.Api.Employee.Requests;
using AT.Api.Employee.Requests.Employee;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to Employee
    /// </summary>
    [RoutePrefix("api/v1/employees")]
    public class EmployeeController : BaseApiController
    {
        /// <summary>
        /// Gets Employee data by employee number
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns>The Employee data</returns>
        [HttpGet]
        [Route("{employeeNumber}")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var parameters = new GetEmployeeParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            };

            var request = new GetEmployeeRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Save an employee
        /// </summary>
        /// <param name="employee">Data of employee to save</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Save(SaveEmployeeModel employee)
        {            
            var parameters = new SaveEmployeeParameters
            {
                Employee = employee,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId
            };

            var request = new SaveEmployeeRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Deletes an employee
        /// </summary>
        /// <param name="employeeNumber">Employee number for the employee to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{employeeNumber}")]
        public IHttpActionResult Delete(string employeeNumber)
        {
            var parameters = new DeleteEmployeeParameters
            {
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId
            };

            var request = new DeleteEmployeeRequest();
            return Ok(request.Handle(parameters));
        }
    }
}