﻿using AT.Api.Core;
using AT.Api.Employee.Models.EmployeeContact;
using AT.Api.Employee.Requests.EmployeeContact;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to contact related to an employee
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/contacts")]
    public class EmployeeContactController : BaseApiController
    {
        /// <summary>
        /// Gets a contact by employee number
        /// </summary>
        /// <param name="employeeNumber">Employee number for retrieving the related contact</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var request = new GetEmployeeContactsRequest();

            var result = request.Handle(new GetEmployeeContactsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Gets a contact by employee number and the contact type code
        /// </summary>
        /// <param name="employeeNumber">Employee number for retrieving the related contact</param>
        /// <param name="contactTypeCode">Contact type code to filter the contacts</param>
        /// <returns></returns>
        [HttpGet]
        [Route("type/{contactTypeCode}")]
        public IHttpActionResult Get(string employeeNumber, string contactTypeCode)
        {
            var request = new GetEmployeeContactsRequest();

            var result = request.Handle(new GetEmployeeContactsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                ContactTypeCode = contactTypeCode
            });

            return Ok(result);
        }

        /// <summary>
        /// Save a contact for an employee
        /// </summary>
        /// <param name="employeeNumber">Employee number for retrieving the related contact</param>
        /// <param name="model">Contact data to save</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Save(string employeeNumber, EmployeeContactModel model)
        {
            var request = new SaveEmployeeContactRequest();

            var result = request.Handle(new SaveEmployeeContactParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                EmployeeContact = model
            });

            return Ok(result);
        }
    }
}