﻿using AT.Api.Core;
using AT.Api.Employee.Requests.EmployeeWorkSchedule;
using AT.Api.Shared.Model.Common;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to Work Schedule for an employee
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/workschedules")]
    public class EmployeeWorkScheduleController : BaseApiController
    {
        /// <summary>
        /// Gets the employee Work Schedule
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns>Work Schedule details</returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var request = new ReadEmployeeWorkScheduleRequest();

            var result = request.Handle(new ReadEmployeeWorkScheduleParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
            });

            return Ok(result);
        }

        /// <summary>
        /// Creates the employee Work Schedule
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <param name="model">Work schedule details</param>
        /// <returns></returns>
        [HttpPost]
        [Route("current")]
        public IHttpActionResult Create(string employeeNumber, CreateWorkScheduleModel model)
        {
            var request = new CreateEmployeeWorkScheduleRequest();

            var result = request.Handle(new CreateEmployeeWorkScheduleParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                WorkSchedule = model
            });

            return Ok(result);
        }
    }
}