﻿using AT.Api.Core;
using AT.Api.Employee.Models.EmployeeJob;
using AT.Api.Employee.Requests.EmployeeJob;
using System;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to Employee Job
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/jobs")]
    public class EmployeeJobController : BaseApiController
    {
        /// <summary>
        /// Gets employee jobs
        /// </summary>
        /// <param name="employeeNumber">Employee number for the job</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Gets employee jobs filtered by job code
        /// </summary>
        /// <param name="employeeNumber">Employee number for the job</param>
        /// <param name="jobCode">Job code to filter employee jobs</param>
        /// <returns></returns>
        [HttpGet]
        [Route("code/{jobCode}")]
        public IHttpActionResult Get(string employeeNumber, string jobCode)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                JobCode = jobCode
            });

            return Ok(result);
        }

        /// <summary>
        /// Gets employee jobs filtered by job code and inactive
        /// </summary>
        /// <param name="employeeNumber">Employee number for the job</param>
        /// <param name="jobCode">Job code to filter employee jobs</param>
        /// <param name="inactive">status for inactive job</param>
        /// <returns></returns>
        [HttpGet]
        [Route("code/{jobCode}/inactive/{inactive}")]
        public IHttpActionResult Get(string employeeNumber, string jobCode, bool inactive)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                JobCode = jobCode,
                Inactive = inactive
            });

            return Ok(result);
        }

        /// <summary>
        /// Gets employee jobs filtered by job code and start date
        /// </summary>
        /// <param name="employeeNumber">Employee number for the job</param>
        /// <param name="jobCode">Code for the job</param>
        /// <param name="startDate">Start date to filter jobs</param>
        /// <returns></returns>
        [HttpGet]
        [Route("code/{jobCode}/start/{startDate:datetime}")]
        public IHttpActionResult Get(string employeeNumber, string jobCode, DateTime startDate)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                StartDate = startDate,
                JobCode = jobCode
            });

            return Ok(result);
        }


        /// <summary>
        /// Get Employee jobs by start date
        /// </summary>
        /// <param name="employeeNumber"></param>        
        /// <param name="startDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("start/{startDate:datetime}")]
        public IHttpActionResult Get(string employeeNumber, DateTime startDate)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                StartDate = startDate
            });

            return Ok(result);
        }


        /// <summary>
        /// Get Employee jobs by date range
        /// </summary>
        /// <param name="employeeNumber"></param>        
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("start/{startdate:datetime}/end/{enddate:datetime}")]
        public IHttpActionResult Get(string employeeNumber, DateTime? startDate, DateTime? endDate)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                StartDate = startDate,
                EndDate = endDate
            });

            return Ok(result);
        }

        /// <summary>
        /// Get Employee jobs by jobcode and date range
        /// </summary>
        /// <param name="employeeNumber"></param>        
        /// <param name="jobCode"></param>        
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("code/{jobcode}/start/{startdate:datetime}/end/{enddate:datetime}")]
        public IHttpActionResult Get(string employeeNumber, string jobCode, DateTime startDate, DateTime endDate)
        {
            var request = new GetEmployeeJobsRequest();

            var result = request.Handle(new GetEmployeeJobsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                JobCode = jobCode,
                StartDate = startDate,
                EndDate = endDate
            });

            return Ok(result);
        }

        /// <summary>
        /// Create Employee job
        /// </summary>
        /// <param name="employeeNumber"></param>        
        /// <param name="model"></param>        
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Create(string employeeNumber, CreateEmployeeJobModel model)
        {
            var request = new CreateEmployeeJobRequest();

            var result = request.Handle(new CreateEmployeeJobParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                UserId = AuthenticatedUser.Key,
                EmployeeJob = model ?? new CreateEmployeeJobModel()
            });

            return Ok(result);
        }
    }
}