﻿using AT.Api.Core;
using AT.Api.Employee.Requests.EmployeeCustomField;
using AT.Api.Shared.Model.CustomFields;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to employee custom field
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}")]
    public class EmployeeCustomFieldController : BaseApiController
    {
        /// <summary>
        /// Gets custom fields for an employee
        /// </summary>
        /// <param name="employeeNumber">Employee number to get the custom fields</param>
        /// <returns></returns>
        [HttpGet]
        [Route("customfields")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var request = new GetEmployeeCustomFieldsRequest(AuthenticatedUser.Key);

            var result = request.Handle(new GetEmployeeCustomFieldsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            });

            return Ok(result);
        }

        /// <summary>
        /// Saves custom fields for an employee
        /// </summary>
        /// <param name="employeeNumber">Employee number to save custom fields</param>
        /// <param name="model">Collection of custom fields data</param>
        /// <returns></returns>
        [HttpPost]
        [Route("customfields")]
        public IHttpActionResult Save(string employeeNumber, SaveCustomFieldModel[] models)
        {
            var request = new SaveEmployeeCustomFieldsRequest(AuthenticatedUser.Key);

            var result = request.Handle(new SaveEmployeeCustomFieldsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                CustomFields = models
            });

            return Ok(result);
        }

        /// <summary>
        /// Saves a single custom field for an employee
        /// </summary>
        /// <param name="employeeNumber">Employee number to save custom fields</param>
        /// <param name="model">Custom field data</param>
        /// <returns></returns>
        [HttpPost]
        [Route("customfield")]
        public IHttpActionResult Save(string employeeNumber, SaveCustomFieldModel model)
        {
            var request = new SaveEmployeeCustomFieldRequest(AuthenticatedUser.Key);

            var result = request.Handle(new SaveEmployeeCustomFieldParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                CustomField = model
            });

            return Ok(result);
        }

        /// <summary>
        /// Gets a custom field by code
        /// </summary>
        /// <param name="employeeNumber">Employee number associated to custom field</param>
        /// <param name="code">Custom field code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("customfields/{code}")]
        public IHttpActionResult Read(string employeeNumber, string code)
        {
            var request = new ReadEmployeeCustomFieldRequest();

            var result = request.Handle(new ReadEmployeeCustomFieldParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                Code = code
            });

            return Ok(result);
        }

        /// <summary>
        /// Delete a custom field on an employee by code
        /// </summary>
        /// <param name="employeeNumber">Employee number associated to custom field</param>
        /// <param name="code">Custom field code</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("customfields/{code}")]
        public IHttpActionResult Delete(string employeeNumber, string code)
        {
            var request = new DeleteEmployeeCustomFieldRequest();

            var result = request.Handle(new DeleteEmployeeCustomFieldParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                Code = code
            });

            return Ok(result);
        }
    }
}
