﻿using AT.Api.Core;
using AT.Api.Employee.Requests.EmployeeContact;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to contact
    /// </summary>
    [RoutePrefix("api/v1/contacts/{contactId}")]
    public class ContactController : BaseApiController
    {
        /// <summary>
        /// Gets a contact by contact id
        /// </summary>
        /// <param name="contactId">Id of contact to retrieve</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Read(string contactId)
        {
            var request = new ReadEmployeeContactRequest();

            var result = request.Handle(new ReadEmployeeContactParameters
            {
                ContactId = contactId
            });

            return Ok(result);
        }

        /// <summary>
        /// Deletes a contact by contact id
        /// </summary>
        /// <param name="contactId">Id of the contact to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("")]
        public IHttpActionResult Delete(string contactId)
        {
            var request = new DeleteEmployeeContactRequest();

            var result = request.Handle(new DeleteEmployeeContactParameters
            {
                ContactId = contactId
            });

            return Ok(result);
        }
    }
}
