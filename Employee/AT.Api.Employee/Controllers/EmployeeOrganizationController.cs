﻿using AT.Api.Core;
using AT.Api.Employee.Models.EmployeeOrganization;
using AT.Api.Employee.Requests.EmployeeOrganization;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to Employee Organization
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/organizations")]
    public class EmployeeOrganizationController : BaseApiController
    {
        /// <summary>
        /// Gets all Employee Organizations by employee number
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns>The Employee Organization</returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetAllByEmployee(string employeeNumber)
        {
            var parameters = new GetEmployeeOrganizationsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            };

            var request = new GetEmployeeOrganizationsRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Creates Employee Organization
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <param name="model">The employee and organization details</param>
        /// <returns>The unique identifier</returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Create(string employeeNumber, CreateEmployeeOrganizationModel model)
        {
            var parameters = new CreateEmployeeOrganizationParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                EmployeeOrganization = model
            };

            var request = new CreateEmployeeOrganizationRequest(AuthenticatedUser.Key);
            return Ok(request.Handle(parameters));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="typeCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("type/{typeCode}")]
        public IHttpActionResult GetOrganizationsByType(string employeeNumber, string typeCode)
        {
            var request = new GetEmployeeOrganizationsRequest(AuthenticatedUser.Key);

            var result =  request.Handle(new GetEmployeeOrganizationsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                TypeCode = typeCode
            });

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="typeCode"></param>
        /// <param name="inactive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("type/{typeCode}/inactive/{inactive}")]
        public IHttpActionResult GetOrganizationsByType(string employeeNumber, string typeCode, bool inactive)
        {
            var request = new GetEmployeeOrganizationsRequest(AuthenticatedUser.Key);

            var result = request.Handle(new GetEmployeeOrganizationsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                TypeCode = typeCode,
                Inactive =inactive 
            });

            return Ok(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("start/{startDate:datetime}/end/{endDate:datetime}")]
        public IHttpActionResult GetOrganizationDateRange(string employeeNumber, DateTime startDate, DateTime endDate)
        {
            var request = new GetEmployeeOrganizationsRequest(AuthenticatedUser.Key);

            var result =request.Handle(new GetEmployeeOrganizationsParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber,
                StartDate = startDate,
                EndDate = endDate
            });

            return Ok(result);
        }
    }
}