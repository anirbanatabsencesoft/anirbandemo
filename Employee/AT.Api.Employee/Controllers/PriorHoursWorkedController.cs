﻿using AT.Api.Core;
using AT.Api.Employee.Models.Common;
using AT.Api.Employee.Requests.PriorHoursWorked;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Handles operations related to Prior Hours Worked related to employee
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/priorhoursworked")]
    public class PriorHoursWorkedController : BaseApiController
    {
        /// <summary>
        /// Gets the Prior Hours Worked for an employee
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns></returns>
        [HttpGet]
        [Route("current")]
        public IHttpActionResult Get(string employeeNumber)
        {
            var parameters = new GetPriorHoursWorkedParameters
            {
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeNumber = employeeNumber
            };

            var request = new GetPriorHoursWorkedRequest();
            return Ok(request.Handle(parameters));
        }

        /// <summary>
        /// Saves the Prior Hours Worked for an employee
        /// </summary>
        /// <param name="priorHours">Prior Hours Worked data to be saved</param>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Save(PriorHoursModel priorHours, string employeeNumber)
        {
            var parameters = new SavePriorHoursWorkedParameters
            {
                PriorHours = priorHours,
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId
            };

            var request = new SavePriorHoursWorkedRequest();
            return Ok(request.Handle(parameters));
        }
    }
}
