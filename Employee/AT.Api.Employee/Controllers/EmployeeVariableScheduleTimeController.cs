﻿using AT.Api.Core;
using AT.Api.Employee.Models.EmployeeVariableScheduleTime;
using AT.Api.Employee.Requests.EmployeeVariableScheduleTime;
using System;
using System.Web.Http;

namespace AT.Api.Employee.Controllers
{
    /// <summary>
    /// Variable Schedule Times
    /// </summary>
    [RoutePrefix("api/v1/employees/{employeeNumber}/variablescheduletimes")]
    public class EmployeeVariableScheduleTimeController : BaseApiController
    {
        /// <summary>
        /// Create Variable Schedule Time
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <param name="model">Variable Schedule Time details</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateVariableScheduleTime(string employeeNumber, EmployeeVariableScheduleTimeModel model)
        {
            var request = new CreateVariableScheduleTimeRequest();

            var result = request.Handle(new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                EmployeeVariableScheduleTime = model
            });

            return Ok(result);
        }

        /// <summary>
        /// Get Variable Schedule Times by employee number
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetVariableScheduleTimes(string employeeNumber)
        {
            var request = new GetVariableScheduleTimesRequest();

            var result = request.Handle(new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId
            });

            return Ok(result);
        }


        /// <summary>
        /// Get Variable Schedule Times by employee number and start date
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <param name="startDate">Start date to filter the schedule times</param>
        /// <returns></returns>
        [HttpGet]
        [Route("start/{startDate:datetime}")]
        public IHttpActionResult GetVariableScheduleTimes(string employeeNumber, DateTime startDate)
        {
            var request = new GetVariableScheduleTimesRequest();

            var result = request.Handle(new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                StartDate = startDate
            });

            return Ok(result);
        }

        /// <summary>
        /// Get Variable Schedule Times by employee number and date range
        /// </summary>
        /// <param name="employeeNumber">The employee number</param>
        /// <param name="startDate">Start date to filter the schedule times</param>
        /// <param name="endDate">End date to filter the schedule times</param>
        /// <returns></returns>
        [HttpGet]
        [Route("start/{startDate:datetime}/end/{endDate:datetime}")]
        public IHttpActionResult GetVariableScheduleTimes(string employeeNumber, DateTime startDate, DateTime endDate)
        {
            var request = new GetVariableScheduleTimesRequest();

            var result = request.Handle(new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = employeeNumber,
                CustomerId = AuthenticatedUser.CustomerKey,
                EmployerId = EmployerId,
                StartDate = startDate,
                EndDate = endDate
            });

            return Ok(result);
        }

    }
}
