﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.Models.EmployeeOrganization;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeOrganization
{
    /// <summary>
    /// Request to handle retrieval of the employee organization
    /// </summary>
    public class GetEmployeeOrganizationsRequest : ApiRequest<GetEmployeeOrganizationsParameters, CollectionResultSet<GetEmployeeOrganizationModel>>
    {
        private readonly IOrganizationService _OrganizationService = null;
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IAdminUserService _AdminUserService = null;

        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="organizationService"></param>
        /// <param name="employeeService"></param>


        public GetEmployeeOrganizationsRequest(
            string userId,
            IAdminUserService adminUserService = null,
            IOrganizationService organizationService = null,
            IEmployeeService employeeService = null)
        {
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _OrganizationService = organizationService ?? new OrganizationService(user);
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override CollectionResultSet<GetEmployeeOrganizationModel> FulfillRequest(GetEmployeeOrganizationsParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var employeeOrganizations = _OrganizationService.GetEmployeeOrganizationsList(employee).AsEnumerable();

            if (!string.IsNullOrWhiteSpace(parameters.TypeCode))
            {
                employeeOrganizations = employeeOrganizations
                    .Where(e => e.TypeCode.ToLower() == parameters.TypeCode.ToLower());
            }
            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                var filterDateRange = new DateRange(parameters.StartDate.Value, parameters.EndDate.Value);

                employeeOrganizations = employeeOrganizations
                    .Where(eo => eo.Dates?.DateRangesOverLap(filterDateRange) ?? true);
            }

            if (parameters.Inactive)
            {
                var orgnizations = _OrganizationService
                    .GetOrganizations(parameters.EmployerId, parameters.CustomerId, null, true)
                    .Select(org => org.Code)
                    .ToArray();

                employeeOrganizations = employeeOrganizations
                    .Where(eo => orgnizations.Contains(eo.Code));
            }

            var organisationModels = new List<GetEmployeeOrganizationModel>();
            if (employeeOrganizations == null)
            {
                return new CollectionResultSet<GetEmployeeOrganizationModel>(
                    new List<GetEmployeeOrganizationModel>(),
                    "No organizations found for employee",
                    true,
                    HttpStatusCode.NoContent);
            }



            organisationModels.AddRange(employeeOrganizations.Select(org => org.ToGetOrganizationModel()));
            return new CollectionResultSet<GetEmployeeOrganizationModel>(organisationModels);
        }



        protected override bool IsValid(GetEmployeeOrganizationsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                if (parameters.StartDate.Value > parameters.EndDate.Value)
                {
                    message = "Start date should not be after the end date";
                    return false;
                }

            }

            return true;
        }

    }
}