﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.EntityFactories;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeOrganization
{
    /// <summary>
    /// Request to handle creation of employee organization
    /// </summary>
    public class CreateEmployeeOrganizationRequest : ApiRequest<CreateEmployeeOrganizationParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IOrganizationService _OrganizationService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="adminUserService"></param>
        /// <param name="employeeService"></param>
        /// <param name="organizationService"></param>
        public CreateEmployeeOrganizationRequest(
            string userId,
            IAdminUserService adminUserService = null,
            IEmployeeService employeeService = null,
            IOrganizationService organizationService = null)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "Unable to retrieve authorized user detail");
            }

            var userService = adminUserService ?? new AdminUserService();
            var user = userService.GetUserById(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _EmployeeService = employeeService ?? new EmployeeService();
            _OrganizationService = organizationService ?? new OrganizationService(user);
        }

        /// <summary>
        /// Fulfill to create employee orgnization
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(CreateEmployeeOrganizationParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(
                parameters.EmployeeNumber,
                parameters.CustomerId,
                parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var typeCode = _OrganizationService.GetOrganizationType(parameters.EmployerId, parameters.EmployeeOrganization.TypeCode);
            if (typeCode == null)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No orgnization type code found");
            }

            var parentOrgnization = _OrganizationService.GetOrganizations(parameters.EmployerId, parameters.CustomerId, parameters.EmployeeOrganization.ParentOrgCode, false).FirstOrDefault();
            if (parentOrgnization == null)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No parent orgnization found");
            }


            var orgnization = _OrganizationService.GetOrganizations(parameters.EmployerId, parameters.CustomerId, parameters.EmployeeOrganization.Code, false).FirstOrDefault();
            if (orgnization == null)
            {
                var orgnizationEntity = EmployeeOrganizationFactory.FromOrganizationModel(parameters.EmployeeOrganization, orgnization, parameters.CustomerId, parameters.EmployerId);
                orgnization = _OrganizationService.SaveOrgnization(orgnizationEntity);
            }

            var empOrgnization = _OrganizationService.GetEmployeeOrganizationByCode(employee, parameters.EmployeeOrganization.Code);

            var employeeOrganization = EmployeeOrganizationFactory.FromEmployeeOrganizationModel(parameters.EmployeeOrganization, employee, empOrgnization, orgnization);
            var savedEmployeeOrganization = _OrganizationService.UpdateOrganization(employeeOrganization);

            if (savedEmployeeOrganization == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Error saving employee organization");
            }

            return new ResultSet<string>(savedEmployeeOrganization.Code, "Employee organization saved");
        }

        protected override bool IsValid(CreateEmployeeOrganizationParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}