﻿using AT.Api.Employee.Models.EmployeeOrganization;

namespace AT.Api.Employee.Requests.EmployeeOrganization
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class CreateEmployeeOrganizationParameters
    {
        /// <summary>
        /// Id for the customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Id for the employer
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Details of the employee organization
        /// </summary>
        public CreateEmployeeOrganizationModel EmployeeOrganization { get; set; }
    }
}