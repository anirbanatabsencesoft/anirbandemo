﻿
using System;

namespace AT.Api.Employee.Requests.EmployeeOrganization
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class GetEmployeeOrganizationsParameters
    {
        /// <summary>
        /// Id for the customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Id for the employer
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Type code for Organisation
        /// </summary>
        public string TypeCode { get; set; }
        /// <summary>
        /// Start date
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// End date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Inactive
        /// </summary>
        public bool Inactive { get; set; }
    }
}

