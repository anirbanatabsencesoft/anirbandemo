﻿using AbsenceSoft;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.Common;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeVariableScheduleTime
{
    public class GetVariableScheduleTimesRequest : ApiRequest<GetVariableScheduleTimesParameters, CollectionResultSet<BaseTimeModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public GetVariableScheduleTimesRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override CollectionResultSet<BaseTimeModel> FulfillRequest(GetVariableScheduleTimesParameters parameters)
        {
            var employeeNumber = parameters.EmployeeNumber;
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(employeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            var appliedTimes = _EmployeeService.GetVariableScheduleTime(employee.Id).AsEnumerable();

            if (parameters.StartDate.HasValue)
            {
                appliedTimes = appliedTimes.
                    Where(d => d.Time.SampleDate >= parameters.StartDate.Value.ToMidnight());
            }

            if (parameters.EndDate.HasValue)
            {
                appliedTimes = appliedTimes.
                   Where(d => d.Time.SampleDate <= parameters.EndDate.Value.ToMidnight());
            }

            var result = appliedTimes.Select(t => t.Time).Select(d => new BaseTimeModel
            {
                SampleDate = d.SampleDate,
                TotalMinutes = d.TotalMinutes
            });

            return new CollectionResultSet<BaseTimeModel>(result);
        }

        /// <summary>
        /// Validate the parameters to get variable schedule for employee
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetVariableScheduleTimesParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            return true;
        }
    }
}