﻿using System;

namespace AT.Api.Employee.Requests.EmployeeVariableScheduleTime
{
    public class GetVariableScheduleTimesParameters
    {
        public string EmployeeNumber { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}