﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeVariableScheduleTime
{
    public class CreateVariableScheduleTimeRequest : ApiRequest<CreateVariableScheduleTimeParameters, ResultSetBase>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public CreateVariableScheduleTimeRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSetBase FulfillRequest(CreateVariableScheduleTimeParameters parameters)
        {
            var actualTimes = parameters.EmployeeVariableScheduleTime.ActualTimes;

            if (actualTimes == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Variable schedule times can not be null");
            }

            var employeeNumber = parameters.EmployeeNumber;
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(employeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            var appliedTimes = _EmployeeService.GetVariableScheduleTime(employee.Id);

            // Mark as delete existing times which are not present in actualTimes
            if (appliedTimes != null)
            {
                appliedTimes.ForEach(a =>
                {
                    if (!actualTimes.Select(at => at.SampleDate.ToMidnight()).Contains(a.Time.SampleDate))
                    {
                        a.Time.TotalMinutes = null;
                    }
                });
            }
            else
            {
                appliedTimes = new List<VariableScheduleTime>();
            }

            foreach (var time in actualTimes)
            {
                var matchingDate = appliedTimes.FirstOrDefault(t => t.Time.SampleDate == time.SampleDate.ToMidnight());
                int totalMinutes = time.TotalMinutes.GetValueOrDefault();

                if (matchingDate == null)
                {
                    appliedTimes.Add(new VariableScheduleTime()
                    {
                        Time = new Time()
                        {
                            SampleDate = time.SampleDate.ToMidnight(),
                            TotalMinutes = totalMinutes
                        }
                    });
                }
                else
                {
                    matchingDate.Time.TotalMinutes = totalMinutes;
                }
            }

            var updatedEmployee = _EmployeeService.Update(employee, null, appliedTimes);

            if (updatedEmployee == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Error creating variable schedule for employee");
            }

            return new ResultSetBase("Variable schedule time created successfully");
        }

        /// <summary>
        /// Validate the parameters to save variable schedule time request
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(CreateVariableScheduleTimeParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            return true;
        }
    }
}