﻿using AT.Api.Employee.Models.EmployeeVariableScheduleTime;

namespace AT.Api.Employee.Requests.EmployeeVariableScheduleTime
{
    public class CreateVariableScheduleTimeParameters
    {
        public string EmployeeNumber { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public EmployeeVariableScheduleTimeModel EmployeeVariableScheduleTime { get; set; }
    }
}