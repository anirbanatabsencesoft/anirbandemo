﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.Models.Employee;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Employee.Requests
{
    /// <summary>
    /// Request to handle retrieval of the employee
    /// </summary>
    public class GetEmployeeRequest : ApiRequest<GetEmployeeParameters, ResultSet<GetEmployeeModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public GetEmployeeRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<GetEmployeeModel> FulfillRequest(GetEmployeeParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var employeeModel = employee.ToGetEmployeeModel();

            return new ResultSet<GetEmployeeModel>(
                employeeModel,
                "Employee found for the given details",
                true,
                HttpStatusCode.OK);
        }

        protected override bool IsValid(GetEmployeeParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}