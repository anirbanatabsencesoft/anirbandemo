﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Employee.Requests.Employee
{
    /// <summary>
    /// Request to handle deletion of the employee
    /// </summary>
    public class DeleteEmployeeRequest : ApiRequest<DeleteEmployeeParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public DeleteEmployeeRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<string> FulfillRequest(DeleteEmployeeParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            _EmployeeService.DeleteEmployee(employee.Id);

            return new ResultSet<string>
            {
                Data = parameters.EmployeeNumber,
                Message = "Employee deleted successfully",
                StatusCode = HttpStatusCode.OK,
                Success = true
            };
        }

        protected override bool IsValid(DeleteEmployeeParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }
            return true;
        }
    }
}