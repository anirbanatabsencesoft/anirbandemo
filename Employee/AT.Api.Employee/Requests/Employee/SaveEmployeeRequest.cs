﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.EntityFactories;
using AT.Api.Employee.Models.Employee;
using AT.Common.Core;
using System.Net;
using ATCustomers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Requests.Employee
{
    /// <summary>
    /// Request to handle creation of the employee
    /// </summary>
    public class SaveEmployeeRequest : ApiRequest<SaveEmployeeParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public SaveEmployeeRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<string> FulfillRequest(SaveEmployeeParameters parameters)
        {
            ATCustomers.Employee employee = EmployeeFactory.FromSaveEmployeeModel(
                parameters.CustomerId,
                parameters.EmployerId,
                parameters.Employee);

            var savedEmployee = _EmployeeService.Update(employee);

            if (savedEmployee == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Error saving employee");
            }

            var savedEmployeeModel = savedEmployee.ToGetEmployeeModel();

            return new ResultSet<string>(
                    savedEmployee.EmployeeNumber,
                    "Employee saved successfully",
                    true,
                    HttpStatusCode.OK);
        }

        protected override bool IsValid(SaveEmployeeParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.Employee.Number))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}