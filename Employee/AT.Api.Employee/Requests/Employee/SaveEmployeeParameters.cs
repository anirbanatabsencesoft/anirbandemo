﻿using AT.Api.Employee.Models.Employee;

namespace AT.Api.Employee.Requests.Employee
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class SaveEmployeeParameters
    {
        /// <summary>
        /// Employee data
        /// </summary>
        public SaveEmployeeModel Employee { get; set; }

        /// <summary>
        /// Id of the Employer
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Id of the Customer
        /// </summary>
        public string CustomerId { get; set; }
    }
}