﻿namespace AT.Api.Employee.Requests.Employee
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class DeleteEmployeeParameters
    {
        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Id of the Customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Id of the Employer
        /// </summary>
        public string EmployerId { get; set; }
    }
}