﻿using System;

namespace AT.Api.Employee.Requests.EmployeeJob
{
    public class GetEmployeeJobsParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public string JobCode { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool Inactive { get; set; }
    }
}