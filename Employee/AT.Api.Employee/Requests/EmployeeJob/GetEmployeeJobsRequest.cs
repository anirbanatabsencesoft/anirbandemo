﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Jobs.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.Models.EmployeeJob;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Jobs = AbsenceSoft.Data.Jobs;

namespace AT.Api.Employee.Requests.EmployeeJob
{
    /// <summary>
    /// Request to get employee jobs
    /// </summary>
    public class GetEmployeeJobsRequest : ApiRequest<GetEmployeeJobsParameters, CollectionResultSet<EmployeeJobModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IJobService _JobService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employeeService"></param>
        /// <param name="jobService"></param>
        public GetEmployeeJobsRequest(
           IEmployeeService employeeService = null,
            IJobService jobService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _JobService = jobService ?? new JobService();
        }

        /// <summary>
        /// Fulfull to get employee job
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<EmployeeJobModel> FulfillRequest(GetEmployeeJobsParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var employeeJobs = employee.GetJobs();

            employeeJobs = FilterEmployeeJobs(employeeJobs, parameters);

            var jobModels = new List<EmployeeJobModel>();
            if (employeeJobs == null || employeeJobs.Count == 0)
            {
                return new CollectionResultSet<EmployeeJobModel>(
                    jobModels,
                    "No jobs found for employee",
                    true,
                    HttpStatusCode.NoContent);
            }

            jobModels.AddRange(employeeJobs.Select(job => job.ToJobModel()));

            return new CollectionResultSet<EmployeeJobModel>(jobModels);
        }

        private List<Jobs.EmployeeJob> FilterEmployeeJobs(List<Jobs.EmployeeJob> employeeJobs,
            GetEmployeeJobsParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.JobCode))
            {
                employeeJobs = employeeJobs
                    .Where(e => e.JobCode.Contains(parameters.JobCode))
                    .ToList();
            }

            if (parameters.StartDate.HasValue)
            {
                employeeJobs = employeeJobs
                    .Where(e => e.Dates.StartDate >= parameters.StartDate)
                    .ToList();
            }

            if (parameters.EndDate.HasValue)
            {
                employeeJobs = employeeJobs
                    .Where(e => e.Dates.EndDate == null || e.Dates.EndDate <= parameters.EndDate)
                    .ToList();
            }

            if (parameters.Inactive)
            {
                employeeJobs = employeeJobs
                    .Where(e => e.Job.IsDeleted)
                    .ToList();
            }

            return employeeJobs;
        }

        /// <summary>
        /// Validate the parameters to get the employee jobs 
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetEmployeeJobsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}