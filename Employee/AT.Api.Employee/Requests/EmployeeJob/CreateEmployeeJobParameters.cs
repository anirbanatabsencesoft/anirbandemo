﻿using AT.Api.Employee.Models.EmployeeJob;

namespace AT.Api.Employee.Requests.EmployeeJob
{
    public class CreateEmployeeJobParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public string UserId { get; set; }

        public CreateEmployeeJobModel EmployeeJob { get; set; }
    }
}