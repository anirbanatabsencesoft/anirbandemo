﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Jobs.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.EntityFactories;
using AT.Common.Core;
using System;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;
using Jobs = AbsenceSoft.Data.Jobs;

namespace AT.Api.Employee.Requests.EmployeeJob
{
    /// <summary>
    /// Request to create employee job
    /// </summary>
    public class CreateEmployeeJobRequest : ApiRequest<CreateEmployeeJobParameters, ResultSet<string>>
    {

        private readonly IEmployeeService _EmployeeService = null;

        private readonly IJobService _JobService = null;

        private readonly IAdminUserService _AdminUserService = null;

        /// <summary>
        /// Constructo
        /// </summary>
        /// <param name="employeeService"></param>
        /// <param name="jobService"></param>
        /// <param name="adminUserService"></param>
        public CreateEmployeeJobRequest(
           IEmployeeService employeeService = null,
           IJobService jobService = null,
           IAdminUserService adminUserService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _JobService = jobService ?? new JobService();
            _AdminUserService = adminUserService ?? new AdminUserService();
        }

        /// <summary>
        /// Fulfil to create employee job
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(CreateEmployeeJobParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var job = _JobService.GetJobs(parameters.CustomerId, parameters.EmployerId, parameters.EmployeeJob.JobCode,false).FirstOrDefault();
            if (job == null)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No job found for code");
            }

            var serviceDate = employee.ServiceDate ?? DateTime.UtcNow.ToMidnight();
            var employeeJob = employee.GetCurrentJob();

            if (employeeJob != null && !ChangedEmployeeJob(employeeJob, parameters))
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee Job Already Saved");
            }

            var user = _AdminUserService.GetUserById(parameters.UserId);
           
            employeeJob = EmployeeJobFactory.FromEmployeeJobModel(parameters.EmployeeJob,
                employeeJob, user, serviceDate, parameters.EmployeeNumber, job);

            _JobService.UpdateEmployeeJob(employeeJob, parameters.EmployeeJob.EndCurrentJob);

            UpdateEmployeeJobActivity(employee, parameters.EmployeeJob.JobTitle, parameters.EmployeeJob.JobActivity);

            return new ResultSet<string>(employeeJob.Id, "Job Saved Successfully");
        }

        private bool ChangedEmployeeJob(Jobs.EmployeeJob job, CreateEmployeeJobParameters parameters)
        {
            if (parameters.EmployeeJob.JobCode != job.JobCode ||
                parameters.EmployeeJob.JobPosition != job.JobPosition ||
                parameters.EmployeeJob.OfficeLocationCode != job.OfficeLocationCode ||
                job.Dates == null ||
                job.Dates.StartDate != parameters.EmployeeJob.StartDate ||
                job.Dates.EndDate != parameters.EmployeeJob.EndDate ||
                (int)job.Status.Value != parameters.EmployeeJob.Status)
            {
                return true;
            }

            return false;
        }

        private void UpdateEmployeeJobActivity(Customers.Employee employee, string jobTitle, int? jobActivity)
        {
            if (!string.IsNullOrEmpty(jobTitle))
            {
                employee.JobTitle = jobTitle;

                var activity = jobActivity.HasValue
                  ? null
                  : (JobClassification?)Enum.ToObject(typeof(JobClassification), jobActivity);

                employee.JobActivity = activity;

                employee = _EmployeeService.Update(employee, null, null);
            }
        }

        /// <summary>
        /// Validate the parameters to create the employee job 
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(CreateEmployeeJobParameters parameters, out string message)
        {
            message = string.Empty;
            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }
            if (string.IsNullOrWhiteSpace(parameters.EmployeeJob.JobCode))
            {
                message = "JobCode is required";
                return false;
            }

            return true;
        }
    }
}