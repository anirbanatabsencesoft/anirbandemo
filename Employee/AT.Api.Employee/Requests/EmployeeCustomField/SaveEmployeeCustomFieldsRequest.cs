﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.CustomFields;
using AT.Api.Shared.Utitilies;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    /// <summary>
    /// Request to save employee custom field
    /// </summary>
    public class SaveEmployeeCustomFieldsRequest : ApiRequest<SaveEmployeeCustomFieldsParameters, ResultSet<string[]>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IAdminUserService _AdminUserService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="employeeService"></param>
        /// <param name="employerService"></param>
        /// <param name="adminUserService"></param>
        public SaveEmployeeCustomFieldsRequest(
           string userId,
           IEmployeeService employeeService = null,
           IEmployerService employerService = null,
           IAdminUserService adminUserService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
            _EmployerService = employerService ?? new EmployerService(user);
        }

        /// <summary>
        /// Fulfill to save the employee custom field
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string[]> FulfillRequest(SaveEmployeeCustomFieldsParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            employee = ApplyCustomFieldToEmployee(employee, parameters.CustomFields);

            _EmployeeService.Update(employee, null, null);

            var customFieldIds = employee.CustomFields
                .Where(cf => parameters.CustomFields.Any(m => m.Code == cf.Code))
                .Select(cf => cf.Id)
                .ToArray();

            return new ResultSet<string[]>(customFieldIds, "Custom Fields saved successfully for employee");
        }


        private Customers.Employee ApplyCustomFieldToEmployee(Customers.Employee employee, SaveCustomFieldModel[] customFields)
        {
            var configurationFields = string.IsNullOrEmpty(employee.Id)
                ? _EmployerService.GetCustomFields(EntityTarget.Employee, true)
                : _EmployerService.GetCustomFields(EntityTarget.Employee, null);

            if (configurationFields == null ||
                configurationFields.Count == 0)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No configuration fields setting for employee");
            }

            if (customFields.Select(p => p.Code).Except(configurationFields.Select(p => p.Code)).Any())
            {
                throw new ApiException(HttpStatusCode.NoContent, "Invalid custom fields configuration code");
            }

            var isValid = CustomFieldValidator.Validate(configurationFields, customFields);
          
            foreach (var field in customFields)
            {
                var index = configurationFields.FindIndex(cf => cf.Code == field.Code);
                configurationFields[index].SelectedValue = field.Value;
            }

            if(employee.CustomFields != null)
            {
                configurationFields
                .Where(cf => employee.CustomFields.Any(f => f.Code == cf.Code && f.SelectedValue != null) &&
                             customFields.Any(f => f.Code != cf.Code))
                .ToList()
                .ForEach(cf =>
                {
                    cf.SelectedValue = employee.CustomFields.Where(f => f.Code == cf.Code).Select(f => f.SelectedValue).First();
                });

                }
            employee.CustomFields = configurationFields;

            return employee;
        }

        /// <summary>
        /// Validate the parameters to save the employee custom field
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(SaveEmployeeCustomFieldsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (parameters.CustomFields == null || parameters.CustomFields.Length == 0 || parameters.CustomFields.Any(c => string.IsNullOrWhiteSpace(c.Code)))
            {
                message = "Invalid custom fields";
                return false;
            }

            return true;
        }
    }
}