﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.CustomFields;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class DeleteEmployeeCustomFieldRequest : ApiRequest<DeleteEmployeeCustomFieldParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public DeleteEmployeeCustomFieldRequest(
           IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<string> FulfillRequest(DeleteEmployeeCustomFieldParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId); ;

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var customField = new CustomFieldModel();
            if (employee.CustomFields == null || employee.CustomFields.Count == 0)
            {
                return new ResultSet<string>(
                   string.Empty,
                   "No custom fields configuration for employee",
                   true,
                   HttpStatusCode.NoContent);
            }

            var employeeCustomField = employee.CustomFields.Where(cf => cf.Code == parameters.Code).FirstOrDefault();
            if (employeeCustomField == null)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No custom field configuration for requested code");
            }

            employee.CustomFields.Remove(employeeCustomField);

            _EmployeeService.Update(employee);

            return new ResultSet<string>(employeeCustomField.Id, "Custom field deleted successfully");
        }

        /// <summary>
        /// Validate the parameters to get the employee custom fields
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(DeleteEmployeeCustomFieldParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.Code))
            {
                message = "Custom Field Code is required";
                return false;
            }

            return true;
        }
    }
}