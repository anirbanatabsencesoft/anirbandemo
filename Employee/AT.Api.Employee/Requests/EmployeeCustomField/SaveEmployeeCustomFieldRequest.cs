﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Shared.Model.CustomFields;
using AT.Api.Shared.Utitilies;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class SaveEmployeeCustomFieldRequest : ApiRequest<SaveEmployeeCustomFieldParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IAdminUserService _AdminUserService = null;

        public SaveEmployeeCustomFieldRequest(
           string userId,
           IEmployeeService employeeService = null,
           IEmployerService employerService = null,
           IAdminUserService adminUserService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }

            _EmployerService = employerService ?? new EmployerService(user);
        }

        protected override ResultSet<string> FulfillRequest(SaveEmployeeCustomFieldParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            employee = ApplyCustomFieldToEmployee(employee, parameters.CustomField.Code, parameters.CustomField.Value);

            employee = _EmployeeService.Update(employee, null, null);

            var customFieldId = employee.CustomFields
                .Where(cf => cf.Code == parameters.CustomField.Code)
                .Select(cf => cf.Id)
                .FirstOrDefault();

            return new ResultSet<string>(customFieldId, "Custom Field saved successfully for employee");
        }

        /// <summary>
        /// Validate the configuration for custom field
        /// Assign the custom fields values to the employee
        /// </summary>
        /// <param name="employee"></param>        
        /// <param name="code"></param>        
        /// <param name="value"></param>        
        /// <returns></returns>
        private Customers.Employee ApplyCustomFieldToEmployee(Customers.Employee employee, string code, string value)
        {
            var configurationFields = string.IsNullOrEmpty(employee.Id)
                ? _EmployerService.GetCustomFields(EntityTarget.Employee, true)
                : _EmployerService.GetCustomFields(EntityTarget.Employee, null);

            if (configurationFields == null || configurationFields.Count == 0)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No configuration fields setting for employee");
            }

            if (!configurationFields.Any(cf => cf.Code == code))
            {
                throw new ApiException(HttpStatusCode.NoContent, "No custom field configuration for requested code");
            }

            var isValid = CustomFieldValidator.Validate(
                configurationFields,
                new SaveCustomFieldModel[]
                {
                    new SaveCustomFieldModel
                    {
                        Code = code,
                        Value = value
                    }
                });

            if (employee.CustomFields != null && employee.CustomFields.Count > 0 && employee.CustomFields.Any(cf => cf.Code == code))
            {
                var index = employee.CustomFields.FindIndex(c => c.Code == code);
                employee.CustomFields[index].SelectedValue = value;
            }
            else
            {
                foreach (var field in configurationFields)
                {
                    if (!string.IsNullOrWhiteSpace(code) && field.Code == code)
                    {
                        field.SelectedValue = value;
                    }
                }

                employee.CustomFields = configurationFields;
            }

            return employee;
        }

        /// <summary>
        /// Validate the parameters to save the employee custom fields
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(SaveEmployeeCustomFieldParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (parameters.CustomField == null || string.IsNullOrWhiteSpace(parameters.CustomField.Code))
            {
                message = "Invalid custom fields";
                return false;
            }

            return true;
        }
    }
}