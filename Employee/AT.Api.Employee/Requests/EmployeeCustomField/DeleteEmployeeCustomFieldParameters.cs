﻿namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class DeleteEmployeeCustomFieldParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public string Code { get; set; }
    }
}