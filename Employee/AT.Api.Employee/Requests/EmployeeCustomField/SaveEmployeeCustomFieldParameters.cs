﻿using AT.Api.Shared.Model.CustomFields;

namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class SaveEmployeeCustomFieldParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public SaveCustomFieldModel CustomField { get; set; }
    }
}