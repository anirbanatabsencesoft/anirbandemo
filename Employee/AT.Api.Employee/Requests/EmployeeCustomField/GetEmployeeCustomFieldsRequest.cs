﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Shared.Model.CustomFields;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class GetEmployeeCustomFieldsRequest : ApiRequest<GetEmployeeCustomFieldsParameters, CollectionResultSet<CustomFieldModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly IEmployerService _EmployerService = null;
        private readonly IAdminUserService _AdminUserService = null;

        public GetEmployeeCustomFieldsRequest(
           string userId,
           IEmployeeService employeeService = null,
           IEmployerService employerService = null,
           IAdminUserService adminUserService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
            _AdminUserService = adminUserService ?? new AdminUserService();

            var user = _AdminUserService.GetUserById(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Unable to find user");
            }
            _EmployerService = employerService ?? new EmployerService(user);
        }

        protected override CollectionResultSet<CustomFieldModel> FulfillRequest(GetEmployeeCustomFieldsParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId); ;

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var customFields = new List<CustomFieldModel>();
            if (employee.CustomFields == null || employee.CustomFields.Count == 0)
            {
                return new CollectionResultSet<CustomFieldModel>(
                   customFields,
                   "No custom fields configuration for employee",
                   true,
                   HttpStatusCode.NoContent);
            }

            var configurationFields = string.IsNullOrEmpty(employee.Id)
                ? _EmployerService.GetCustomFields(EntityTarget.Employee, true)
                : _EmployerService.GetCustomFields(EntityTarget.Employee, null);
            

            customFields = employee.CustomFields.Where(cf=> configurationFields.Any(f=>f.Code == cf.Code)).Select(c => c.ToCustomFieldModel()).ToList();
            return new CollectionResultSet<CustomFieldModel>(customFields);
        }

        /// <summary>
        /// Validate the parameters to get the employee custom fields
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetEmployeeCustomFieldsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }


            return true;
        }
    }
}