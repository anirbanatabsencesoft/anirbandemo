﻿namespace AT.Api.Employee.Requests.EmployeeCustomField
{
    public class GetEmployeeCustomFieldsParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }
    }
}