﻿namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class ReadEmployeeContactParameters
    {
        public string ContactId { get; set; }
    }
}