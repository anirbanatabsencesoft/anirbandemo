﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.Models.EmployeeContact;
using AT.Common.Core;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class GetEmployeeContactsRequest : ApiRequest<GetEmployeeContactsParameters, CollectionResultSet<EmployeeContactModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public GetEmployeeContactsRequest(
           IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        /// <summary>
        /// Get employee contacts
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override CollectionResultSet<EmployeeContactModel> FulfillRequest(GetEmployeeContactsParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId); ;

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var contactTypeCode = string.IsNullOrEmpty(parameters.ContactTypeCode) ? null : parameters.ContactTypeCode;
            var employeeContacts = _EmployeeService.GetContacts(employee.Id, contactTypeCode);

            var contactModels = new List<EmployeeContactModel>();
            if (employeeContacts == null || employeeContacts.Count == 0)
            {
                return new CollectionResultSet<EmployeeContactModel>(
                    contactModels,
                    "No contact found for employee",
                    true,
                    HttpStatusCode.NoContent);
            }

            contactModels.AddRange(employeeContacts.Select(contact => contact.ToContactModel()));

            return new CollectionResultSet<EmployeeContactModel>(contactModels);
        }

        /// <summary>
        /// Validate the parameters to get the employee contacts 
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(GetEmployeeContactsParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}