﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class DeleteEmployeeContactRequest : ApiRequest<DeleteEmployeeContactParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public DeleteEmployeeContactRequest(
           IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        /// <summary>
        /// Check employee contact is exist 
        /// Delete the employee contact
        /// </summary>
        /// <param name="parameters"></param>        
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(DeleteEmployeeContactParameters parameters)
        {
            var empContact = _EmployeeService.GetContact(parameters.ContactId);

            if (empContact == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "No employee contacts found");
            }

            if (empContact.Employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            _EmployeeService.DeleteContact(parameters.ContactId);

            return new ResultSet<string>(parameters.ContactId, "contact deleted successfully");
        }

        /// <summary>
        /// Validate the parameters to delete the contact 
        /// if parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(DeleteEmployeeContactParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.ContactId))
            {
                message = "Contact Id is required";
                return false;
            }
            return true;
        }

    }
}