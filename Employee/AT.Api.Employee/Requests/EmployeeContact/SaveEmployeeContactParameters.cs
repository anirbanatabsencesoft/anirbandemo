﻿using AT.Api.Employee.Models.EmployeeContact;

namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class SaveEmployeeContactParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public EmployeeContactModel EmployeeContact { get; set; }
    }
}