﻿namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class DeleteEmployeeContactParameters
    {        
        public string ContactId { get; set; }
    }
}