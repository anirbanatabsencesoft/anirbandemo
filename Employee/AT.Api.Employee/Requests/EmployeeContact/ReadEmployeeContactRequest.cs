﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Employee.Models.EmployeeContact;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class ReadEmployeeContactRequest : ApiRequest<ReadEmployeeContactParameters, ResultSet<EmployeeContactModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public ReadEmployeeContactRequest(
           IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        /// <summary>
        /// Read Employee Contact        
        /// </summary>
        /// <param name="parameters"></param>        
        /// <returns></returns>
        protected override ResultSet<EmployeeContactModel> FulfillRequest(ReadEmployeeContactParameters parameters)
        {
            var employeeContact = _EmployeeService.GetContact(parameters.ContactId);
            
            if (employeeContact == null || employeeContact.Employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "No contact found");
            }

            var contactModel = employeeContact.ToContactModel();

            return new ResultSet<EmployeeContactModel>(contactModel, "Contact  Details");
        }

        /// <summary>
        /// Validate the parameters to read employee contact 
        /// if parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(ReadEmployeeContactParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrEmpty(parameters.ContactId))
            {
                message = "Contact Id is required";
                return false;
            }
            return true;
        }
    }
}