﻿using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.EntityFactories;
using AT.Common.Core;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Requests.EmployeeContact
{
    /// <summary>
    /// Request to save employee contact
    /// </summary>
    public class SaveEmployeeContactRequest : ApiRequest<SaveEmployeeContactParameters, ResultSet<string>>
    {
        private readonly IEmployeeService _EmployeeService = null;
        private readonly ICaseService _CaseService = null;
        private readonly IContactTypeService _ContactTypeService = null;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employeeService"></param>
        /// <param name="caseService"></param>
        /// <param name="contactTypeService"></param>
        public SaveEmployeeContactRequest(            
            IEmployeeService employeeService = null,
            ICaseService caseService = null,
            IContactTypeService contactTypeService = null)
        {           
            _EmployeeService = employeeService ?? new EmployeeService();
            _CaseService = caseService ?? new CaseService();
            _ContactTypeService = contactTypeService ?? new ContactTypeService();
        }

        /// <summary>
        /// Save employee contact
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected override ResultSet<string> FulfillRequest(SaveEmployeeContactParameters parameters)
        {
            var employee = _EmployeeService
                .GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            ValidateContactTypeCode(parameters.EmployerId, parameters.CustomerId, parameters.EmployeeContact.ContactTypeCode);

            var empContact = new Customers.EmployeeContact();
            if (!string.IsNullOrEmpty(parameters.EmployeeContact.ContactId))
            {
                empContact = _EmployeeService.GetContact(parameters.EmployeeContact.ContactId);
            }

            empContact = ContactFactory.FromEmployeeContactModel(
                parameters.EmployeeContact,
                parameters.EmployerId,
                parameters.CustomerId,
                employee.Id,
                empContact);

            var employeeContact = _EmployeeService.SaveContact(empContact);

            _CaseService.UpdateContactOnAssociatedCases(employeeContact);

            return new ResultSet<string>(employeeContact.Id, "Contact saved successfully");
        }

        /// <summary>
        /// Validate contact type code in the system
        /// </summary>
        /// <param name="employerId"></param>
        /// <param name="customerId"></param>
        /// <param name="contactTypeCode"></param>
        private void ValidateContactTypeCode(string employerId, string customerId, string contactTypeCode)
        {
            var contactTypes = _ContactTypeService.GetContactTypes(customerId, employerId, null);
            if(!contactTypes.Any(ct=>ct.Code.ToLower() == contactTypeCode.ToLower()))
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid contact type code");
            }
        }

        /// <summary>
        /// Validate the parameters to save the employee contact
        /// If parameters are not valid then set error message
        /// </summary>
        /// <param name="parameters"></param>        
        /// <param name="message"></param>        
        /// <returns></returns>
        protected override bool IsValid(SaveEmployeeContactParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}