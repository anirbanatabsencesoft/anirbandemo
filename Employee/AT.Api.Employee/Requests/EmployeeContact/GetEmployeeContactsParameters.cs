﻿namespace AT.Api.Employee.Requests.EmployeeContact
{
    public class GetEmployeeContactsParameters
    {
        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        public string EmployeeNumber { get; set; }

        public string ContactTypeCode { get; set; }
    }
}