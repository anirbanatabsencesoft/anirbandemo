﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.DataExtensions;
using AT.Api.Shared.Model.Common;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeWorkSchedule
{
    /// <summary>
    /// Request to handle retrieval of employee work schedule
    /// </summary>
    public class ReadEmployeeWorkScheduleRequest : ApiRequest<ReadEmployeeWorkScheduleParameters, ResultSet<ReadWorkScheduleModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public ReadEmployeeWorkScheduleRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<ReadWorkScheduleModel> FulfillRequest(ReadEmployeeWorkScheduleParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            if (employee.WorkSchedules == null)
            {
                throw new ApiException(HttpStatusCode.NoContent, "No work schedules found for employee");
            }

            var schedule = employee.WorkSchedules.OrderByDescending(sched => sched.StartDate).FirstOrDefault();
            var scheduleModel = schedule.ToReadWorkScheduleModel();

            return new ResultSet<ReadWorkScheduleModel>(scheduleModel, "Work schedule found for employee");
        }

        protected override bool IsValid(ReadEmployeeWorkScheduleParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}