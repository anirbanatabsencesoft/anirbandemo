﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.EntityFactories;
using AT.Common.Core;
using System.Net;

namespace AT.Api.Employee.Requests.EmployeeWorkSchedule
{
    /// <summary>
    /// Request to handle creation of employee work schedule
    /// </summary>
    public class CreateEmployeeWorkScheduleRequest : ApiRequest<CreateEmployeeWorkScheduleParameters, ResultSetBase>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public CreateEmployeeWorkScheduleRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSetBase FulfillRequest(CreateEmployeeWorkScheduleParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var schedule = EmployeeWorkScheduleFactory.FromCreateWorkScheduleModel(parameters.WorkSchedule);

            _EmployeeService.SetWorkSchedule(employee, schedule, null);
            var updatedEmployee = _EmployeeService.Update(employee);

            if (updatedEmployee == null)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, "Error creating work schedule for employee");
            }

            return new ResultSetBase("Work schedule created");
        }

        protected override bool IsValid(CreateEmployeeWorkScheduleParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            return true;
        }
    }
}