﻿using AT.Api.Shared.Model.Common;

namespace AT.Api.Employee.Requests.EmployeeWorkSchedule
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class CreateEmployeeWorkScheduleParameters
    {
        /// <summary>
        /// Id for the customer
        /// </summary>  
        public string CustomerId { get; set; }

        /// <summary>
        /// Id for the employer
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Details of the employee work schedule
        /// </summary>
        public CreateWorkScheduleModel WorkSchedule { get; set; }
    }
}