﻿namespace AT.Api.Employee.Requests.EmployeeWorkSchedule
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class ReadEmployeeWorkScheduleParameters
    {
        /// <summary>
        /// Id for the customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Id for the employer
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }
    }
}