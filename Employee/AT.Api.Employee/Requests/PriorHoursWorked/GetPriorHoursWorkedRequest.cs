﻿using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.Models.Common;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.PriorHoursWorked
{
    /// <summary>
    /// Request to handle retrieval of the employee prior hours worked
    /// </summary>
    public class GetPriorHoursWorkedRequest : ApiRequest<GetPriorHoursWorkedParameters, ResultSet<PriorHoursModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public GetPriorHoursWorkedRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<PriorHoursModel> FulfillRequest(GetPriorHoursWorkedParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var priorHoursModel = new PriorHoursModel();

            if (employee.PriorHours == null)
            {
                return new ResultSet<PriorHoursModel>(
                    priorHoursModel,
                    "No prior hours worked found for employee",
                    true,
                    HttpStatusCode.NoContent);
            }

            var priorHours = employee.PriorHours.OrderBy(p => p.AsOf).LastOrDefault();

            priorHoursModel = new PriorHoursModel
            {
                HoursWorked = priorHours.HoursWorked,
                AsOf = priorHours.AsOf
            };

            return new ResultSet<PriorHoursModel>(
                priorHoursModel,
                "Prior hours worked found for employee",
                true,
                HttpStatusCode.OK);
        }

        protected override bool IsValid(GetPriorHoursWorkedParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            return true;
        }
    }
}