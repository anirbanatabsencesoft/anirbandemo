﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Core.Request;
using AT.Api.Employee.Models.Common;
using AT.Common.Core;
using System.Linq;
using System.Net;

namespace AT.Api.Employee.Requests.PriorHoursWorked
{
    /// <summary>
    /// Request to handle creation of the employee prior hours worked
    /// </summary>
    public class SavePriorHoursWorkedRequest : ApiRequest<SavePriorHoursWorkedParameters, ResultSet<PriorHoursModel>>
    {
        private readonly IEmployeeService _EmployeeService = null;

        public SavePriorHoursWorkedRequest(IEmployeeService employeeService = null)
        {
            _EmployeeService = employeeService ?? new EmployeeService();
        }

        protected override ResultSet<PriorHoursModel> FulfillRequest(SavePriorHoursWorkedParameters parameters)
        {
            var employee = _EmployeeService.GetEmployeeByEmployeeNumber(parameters.EmployeeNumber, parameters.CustomerId, parameters.EmployerId);

            if (employee == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Employee not found with the given details");
            }

            if (employee.IsDeleted)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "This employee is deleted");
            }

            var priorHours = new PriorHours
            {
                HoursWorked = parameters.PriorHours.HoursWorked,
                AsOf = parameters.PriorHours.AsOf
            };

            employee.PriorHours.Add(priorHours);

            var returnEmployee = _EmployeeService.Update(employee, null, null);

            if (returnEmployee == null)
            {
                return new ResultSet<PriorHoursModel>(
                    null,
                    "Employee not found for the given details",
                    false,
                    HttpStatusCode.ExpectationFailed);
            }

            var priorHoursModel = new PriorHoursModel
            {
                HoursWorked = returnEmployee.PriorHours.LastOrDefault().HoursWorked,
                AsOf = returnEmployee.PriorHours.LastOrDefault().AsOf
            };

            return new ResultSet<PriorHoursModel>(
                priorHoursModel,
                "Prior hours worked details updated for the given employee",
                true,
                HttpStatusCode.OK);
        }

        protected override bool IsValid(SavePriorHoursWorkedParameters parameters, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(parameters.EmployeeNumber))
            {
                message = "Employee number is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.EmployerId))
            {
                message = "Employer Id is required";
                return false;
            }

            if (string.IsNullOrWhiteSpace(parameters.CustomerId))
            {
                message = "Customer Id is required";
                return false;
            }

            if (parameters.PriorHours == null)
            {
                message = "Prior hours worked details are required";
                return false;
            }

            if (parameters.PriorHours.HoursWorked == 0)
            {
                message = "Prior hours worked details are required";
                return false;
            }

            return true;
        }
    }
}