﻿namespace AT.Api.Employee.Requests.PriorHoursWorked
{
    /// <summary>
    /// Parameters for the request
    /// </summary>
    public class GetPriorHoursWorkedParameters
    {
        /// <summary>
        /// Employee number
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Id of the Customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Id of the Employer
        /// </summary>
        public string EmployerId { get; set; }
    }
}