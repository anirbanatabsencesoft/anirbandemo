﻿using System;

namespace AT.Api.Employee.Models.EmployeeOrganization
{
    /// <summary>
    /// Employee Organization data
    /// </summary>
    public class GetEmployeeOrganizationModel : EmployeeOrganizationModel
    {
        /// <summary>
        /// Employee unique number
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Record created by
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Record updated by
        /// </summary>
        public string ModifiedBy { get; set; }
    }
}