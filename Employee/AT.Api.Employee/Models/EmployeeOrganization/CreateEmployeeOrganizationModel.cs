﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Employee.Models.EmployeeOrganization
{
    /// <summary>
    /// Data to create the employee organization
    /// </summary>
    public class CreateEmployeeOrganizationModel : EmployeeOrganizationModel
    {
        /// <summary>
        /// Code for the parent organization
        /// </summary>
        [Required]
        public string ParentOrgCode { get; set; }
    }
}