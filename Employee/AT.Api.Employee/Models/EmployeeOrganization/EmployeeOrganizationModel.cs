﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Employee.Models.EmployeeOrganization
{
    /// <summary>
    /// Base model for employee organization data
    /// </summary>
    public class EmployeeOrganizationModel
    {
        /// <summary>
        /// Organization code
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// Name of the employee organization
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Description for the employee organization
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The code for the type of employee organization
        /// </summary>
        [Required]
        public string TypeCode { get; set; }

        /// <summary>
        /// SIC code for employee organization
        /// </summary>
        [StringLength(4, ErrorMessage = "SIC code should be exactly 4 characters")]
        public string SicCode { get; set; }

        /// <summary>
        /// NAICS code for employee organization
        /// </summary>
        [StringLength(6, ErrorMessage = "NAICS code should be exactly 6 characters")]
        public string NaicsCode { get; set; }


        /// <summary>
        /// First address for employee organization
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Second address for employee organization
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// City name for employee organization
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// State name for employee organization
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Postal code for employee organization
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// The country code for the employee organization
        /// </summary>
        public string CountryCode { get; set; }
    }
}