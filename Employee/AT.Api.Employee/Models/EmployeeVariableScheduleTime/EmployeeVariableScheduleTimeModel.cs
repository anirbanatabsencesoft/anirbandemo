﻿using AT.Api.Shared.Model.Common;
using System.Collections.Generic;

namespace AT.Api.Employee.Models.EmployeeVariableScheduleTime
{
    public class EmployeeVariableScheduleTimeModel
    {
        public List<BaseTimeModel> ActualTimes { get; set; }
    }
}