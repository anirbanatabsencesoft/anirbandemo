﻿using System.ComponentModel.DataAnnotations;

namespace AT.Api.Employee.Models.EmployeeContact
{
    /// <summary>
    /// Employee contact model
    /// </summary>
    public class EmployeeContactModel
    {

        /// <summary>
        /// Contact Id
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// Contact Type Code
        /// </summary>
        [Required]
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Contact Position
        /// </summary>
        public int? ContactPosition { get; set; }

        /// <summary>
        /// Military Status
        /// </summary>
        public int? MilitaryStatus { get; set; }

        /// <summary>
        /// Related Employee Number
        /// </summary>
        public string RelatedEmployeeNumber { get; set; }

        /// <summary>
        /// Contact Model
        /// </summary>
        public ContactModel Contact { get; set; }

    }
}