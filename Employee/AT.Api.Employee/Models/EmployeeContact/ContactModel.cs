﻿using AT.Api.Shared.Model.Common;
using System;

namespace AT.Api.Employee.Models.EmployeeContact
{
    /// <summary>
    /// Contact Model
    /// </summary>
    public class ContactModel
    {
        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Date of birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Alt Email
        /// </summary>
        public string AltEmail { get; set; }

        /// <summary>
        /// Contact Person Name
        /// </summary>
        public string ContactPersonName { get; set; }

        /// <summary>
        /// Work phone number
        /// </summary>
        public string WorkPhone { get; set; }

        /// <summary>
        /// Cell phone
        /// </summary>
        public string CellPhone { get; set; }

        /// <summary>
        /// Home Phone
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Address Model
        /// </summary>
        public AddressModel Address { get; set; }

    }
}