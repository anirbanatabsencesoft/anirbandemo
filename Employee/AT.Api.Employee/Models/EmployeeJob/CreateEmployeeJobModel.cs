﻿using System;

namespace AT.Api.Employee.Models.EmployeeJob
{
    public class CreateEmployeeJobModel : EmployeeJobModel
    {
        public bool EndCurrentJob { get; set; }

    }
}