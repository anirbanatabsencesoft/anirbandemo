﻿using System;

namespace AT.Api.Employee.Models.EmployeeJob
{
    public class EmployeeJobModel
    {   

        public string JobCode { get; set; }

        public string JobName { get; set; }

        public string JobTitle { get; set; }

        public int? JobActivity { get; set; }

        public int Status { get; set; }

        public int? DenialReason { get; set; }

        public string DenialReasonOther { get; set; }        

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string OfficeLocationCode { get; set; }

        public int? JobPosition { get; set; }
    }
}