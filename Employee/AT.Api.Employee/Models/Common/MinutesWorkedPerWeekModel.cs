﻿using System;

namespace AT.Api.Employee.Models.Common
{
    /// <summary>
    /// Minutes worked per week data
    /// </summary>
    public class MinutesWorkedPerWeekModel
    {
        /// <summary>
        /// Gets or sets the Minutes worked
        /// </summary>
        public int MinutesWorked { get; set; }

        /// <summary>
        /// Gets or sets the As Of date for those  minutes worked
        /// </summary>
        public DateTime AsOf { get; set; }
    }
}