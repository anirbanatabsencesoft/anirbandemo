﻿using AT.Api.Shared.Model.Common;

namespace AT.Api.Employee.Models.Common
{
    /// <summary>
    /// Employee information
    /// </summary>
    public class EmployeeInfoModel
    {
        /// <summary>
        /// Gets or sets the employee's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the alternate email address for the employee
        /// </summary>
        public string AltEmail { get; set; }

        /// <summary>
        /// Gets or sets the employee's address
        /// </summary>
        public AddressModel Address { get; set; }

        /// <summary>
        /// Gets or sets the employee's address
        /// </summary>
        public AddressModel AltAddress { get; set; }

        /// <summary>
        /// Gets or sets the employee's work phone
        /// </summary>
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the employee's home phone
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Gets or sets the employee's cell phone
        /// </summary>
        public string CellPhone { get; set; }

        /// <summary>
        /// Gets or sets the employee's alternate phone
        /// </summary>
        public string AltPhone { get; set; }

        /// <summary>
        /// Gets or sets the name of, address of, tag for or other indicator for an employee's office location
        /// </summary>
        public string OfficeLocation { get; set; }
    }
}