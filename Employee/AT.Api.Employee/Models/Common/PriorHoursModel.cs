﻿using System;

namespace AT.Api.Employee.Models.Common
{
    /// <summary>
    /// Prior hours worked data
    /// </summary>
    public class PriorHoursModel
    {
        /// <summary>
        /// Gets or sets the employee's prior hours worked either from prior employment with the same company
        /// or in absence of a prior work schedule for the purposes of calculating eligibility for FMLA's 1,250 hours rule.
        /// </summary>
        public double HoursWorked { get; set; }

        /// <summary>
        /// Gets or sets the As Of date for those prior hours worked.
        /// </summary>
        public DateTime AsOf { get; set; }
    }
}