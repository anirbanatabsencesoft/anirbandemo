﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Employee.Models.Employee
{
    /// <summary>
    /// Employee data
    /// </summary>
    public class GetEmployeeModel : BaseEmployeeModel
    {
        /// <summary>
        /// Gets or sets the employer id
        /// </summary>
        [Required]
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer id
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Record created by
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Modification date
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Record updated by
        /// </summary>
        public string ModifiedBy { get; set; }        
    }
}