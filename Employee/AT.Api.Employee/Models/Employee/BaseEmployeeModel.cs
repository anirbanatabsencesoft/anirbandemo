﻿using AbsenceSoft.Data.Enums;
using AT.Api.Employee.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AT.Api.Employee.Models.Employee
{
    public class BaseEmployeeModel
    {
        /// <summary>
        /// Gets or sets the employee number used by the employer to uniquely identify this employee.
        /// </summary>
        [Required]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the employee's first name
        /// </summary>
        [Required]
        public string FirstName { get; set;}

        /// <summary>
        /// Gets or sets the employee's middle name or initial
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the employee's last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the employee's date of birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the contact's military status
        /// </summary>        
        public MilitaryStatus MilitaryStatus { get; set; }

        /// <summary>
        /// Gets or sets the employee's gender
        /// </summary>
        public Char Gender { get; set; }

        /// <summary>
        /// Gets or sets the employee's social security number
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// Gets or sets the employee's information
        /// </summary>
        public EmployeeInfoModel Info { get; set; }

        /// <summary>
        /// Gets or sets the employee's country of origin/employment.
        /// </summary>
        [Required]
        public string WorkCountry { get; set; }

        /// <summary>
        /// Gets or sets the employee's work state.
        /// </summary>
        [Required]
        public string WorkState { get; set; }

        /// <summary>
        /// Gets or sets the work county.
        /// </summary>
        public string WorkCounty { get; set; }

        /// <summary>
        /// Gets or sets the work city.
        /// </summary>
        public string WorkCity { get; set; }

        /// <summary>
        /// Gets or sets the cost center code for the employee
        /// </summary>
        public string CostCenterCode { get; set; }

        /// <summary>
        /// Gets or sets the indicator as to whether or not the employee's location meets the minimum 50 employees 
        /// in 75 mile radius rule as specified in the FMLA laws for the US
        /// </summary>
        public bool Meets50In75MileRule { get; set; }

        /// <summary>
        /// Gets or sets the employee's date of hire
        /// </summary>
        public DateTime? HireDate { get; set; }

        /// <summary>
        /// Gets or sets the employee's re-hire date, if applicable
        /// </summary>
        public DateTime? RehireDate { get; set; }
        /// <summary>
        /// Gets or sets the employee's TerminationDate date, if applicable
        /// </summary>        
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Datetime")]
        public DateTime? TerminationDate { get; set; }
        /// <summary>
        /// Gets or sets the employee's service date
        /// </summary>
        [Required]
        public DateTime? ServiceDate { get; set; }

        /// <summary>
        /// Gets or sets a collection of Average Weekly Minutes for the employee as of certain dates so this value
        /// can be collected in absence of a work schedule for calculating eligibility up to some date
        /// </summary>
        public List<MinutesWorkedPerWeekModel> MinutesWorkedPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the employee's pay type
        /// </summary>
        public PayType? PayType { get; set; }

        /// <summary>
        /// Gets or sets the employee's salary information
        /// </summary>
        public double? Salary { get; set; }

        /// <summary>
        /// Gets or set a value indicating whether or not this employee is exempt from minimum wage and overtime
        /// under regulations, 29 C.F.R. Part 541 of the Fair Labor Standards Act (FSLA)
        /// </summary>
        public bool IsExempt { get; set; }

        /// <summary>
        /// Gets or sets the code of employee class.
        /// </summary>
        public string EmployeeClassCode { get; set; }

        /// <summary>
        /// Gets or sets the description of employee class.
        /// </summary>
        public string EmployeeClassDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not this is a key employee for the FMLA Key Employee Exemption.
        /// Key Employee Exception Exempts salaried employees from reinstatement if among highest paid 10 percent and 
        /// if restoration would lead to grievous economic harm to employer.
        /// </summary>
        public bool IsKeyEmployee { get; set; }

        /// <summary>
        /// if not the default payment schedule, then it is this schedule
        /// </summary>
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the employee's status
        /// </summary>
        public char Status { get; set; }

        /// <summary>
        /// Gets or sets the employee's spouse's employee number if that employee's spouse is employed by the same employer.
        /// Otherwise this should be <c>null</c>. This is used for calculating max entitlement for FML provisions that
        /// change allowances for covered service member and other qualifying reasons where there is a max per each
        /// employee combined for a given reason.
        /// </summary>
        public string SpouseEmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        public string Department { get; set; }
        /// <summary>
        /// Gets or sets for FlightCrew
        /// </summary>
        public bool? IsFlightCrew { get; set; }
    }
}