﻿using AbsenceSoft.Data;
using AT.Api.Shared.Model.Common;

namespace AT.Api.Employee.DataExtensions
{
    internal static class AddressExtensions
    {
        internal static AddressModel ToAddressModel(this Address address)
        {
            var model = new AddressModel
            {
                Address1 = address.Address1 ?? string.Empty,
                Address2 = address.Address2 ?? string.Empty,
                City = address.City ?? string.Empty,
                Country = address.Country ?? string.Empty,
                PostalCode = address.PostalCode ?? string.Empty,
                State = address.State ?? string.Empty

            };

            return model;
        }
    }
}
