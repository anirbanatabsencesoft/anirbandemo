﻿using AbsenceSoft.Data.Customers;
using AT.Api.Employee.Models.EmployeeOrganization;

namespace AT.Api.Employee.DataExtensions
{
    internal static class EmployeeOrganizationExtensions
    {
        internal static GetEmployeeOrganizationModel ToGetOrganizationModel(this EmployeeOrganization employeeOrganization)
        {
            return new GetEmployeeOrganizationModel
            {
                Code = employeeOrganization.Code,
                Name = employeeOrganization.Name,
                Description = employeeOrganization.Organization.Description,
                TypeCode = employeeOrganization.Organization.TypeCode,
                EmployeeNumber = employeeOrganization.EmployeeNumber,
                SicCode = employeeOrganization.Organization.SicCode,
                NaicsCode = employeeOrganization.Organization.NaicsCode,
                Address1 = employeeOrganization.Organization.Address.Address1,
                Address2 = employeeOrganization.Organization.Address.Address2,
                CountryCode = employeeOrganization.Organization.Address.Country,
                State = employeeOrganization.Organization.Address.State,
                PostalCode = employeeOrganization.Organization.Address.PostalCode,
                City = employeeOrganization.Organization.Address.City,
                CreatedOn = employeeOrganization.CreatedDate.ToUniversalTime(),
                CreatedBy = employeeOrganization.CreatedBy.DisplayName,
                ModifiedOn = employeeOrganization.ModifiedDate.ToUniversalTime(),
                ModifiedBy = employeeOrganization.ModifiedBy.DisplayName
            };
        }
    }
}