﻿using AbsenceSoft.Data;
using AT.Api.Shared.Model.Common;
using System.Linq;

namespace AT.Api.Employee.DataExtensions
{
    internal static class ScheduleExtensions
    {
        internal static ReadWorkScheduleModel ToReadWorkScheduleModel(this Schedule schedule)
        {
            return new ReadWorkScheduleModel
            {
                Id = schedule.Id,
                StartDate = schedule.StartDate,
                EndDate = schedule.EndDate,
                ScheduleType = (int)schedule.ScheduleType,
                Times = schedule.Times.Select(time => time.ToBaseTimeModel()).ToList()
            };
        }

        internal static BaseTimeModel ToBaseTimeModel(this Time time)
        {
            return new BaseTimeModel
            {
                SampleDate = time.SampleDate,
                IsHoliday = time.IsHoliday,
                TotalMinutes = time.TotalMinutes
            };
        }
    }
}