﻿using AT.Api.Employee.Models.Common;
using AT.Api.Employee.Models.Employee;
using AT.Api.Shared.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using ATCustomers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.DataExtensions
{
    internal static class EmployeeExtensions
    {
        internal static GetEmployeeModel ToGetEmployeeModel(this ATCustomers.Employee employee)
        {
            List<MinutesWorkedPerWeekModel> lstMinutesWorkedPerWeek = new List<MinutesWorkedPerWeekModel>();
            string ssn = string.Empty;

            if (employee.MinutesWorkedPerWeek.Count > 0)
            {
                lstMinutesWorkedPerWeek = employee.MinutesWorkedPerWeek.Select(item => new MinutesWorkedPerWeekModel
                {
                    MinutesWorked = item.MinutesWorked,
                    AsOf = item.AsOf
                }).ToList();
            }

            if (employee.Ssn != null)
            {
                ssn = employee.Ssn.Hashed;
            }

            return new GetEmployeeModel
            {
                EmployerId = employee.EmployerId,
                Number = employee.EmployeeNumber,
                FirstName = employee.FirstName,
                MiddleName = employee.MiddleName,
                LastName = employee.LastName,
                DateOfBirth = employee.DoB.HasValue
                                ? employee.DoB
                                : null,
                MilitaryStatus = employee.MilitaryStatus,
                Gender = employee.Gender != null ? (Char)employee.Gender : '0',
                Ssn = ssn,

                Info = employee.Info != null ? new EmployeeInfoModel
                {
                    Email = employee.Info.Email,
                    AltEmail = employee.Info.AltEmail,

                    Address = new AddressModel
                    {
                        Address1 = employee.Info.Address.Address1,
                        Address2 = employee.Info.Address.Address2,
                        City = employee.Info.Address.City,
                        State = employee.Info.Address.State,
                        Country = employee.Info.Address.Country,
                        PostalCode = employee.Info.Address.PostalCode
                    },

                    AltAddress = new AddressModel
                    {
                        Address1 = employee.Info.AltAddress.Address1,
                        Address2 = employee.Info.AltAddress.Address2,
                        City = employee.Info.AltAddress.City,
                        State = employee.Info.AltAddress.State,
                        Country = employee.Info.AltAddress.Country,
                        PostalCode = employee.Info.AltAddress.PostalCode
                    },

                    WorkPhone = employee.Info.WorkPhone,
                    HomePhone = employee.Info.HomePhone,
                    CellPhone = employee.Info.CellPhone,
                    AltPhone = employee.Info.AltPhone,
                    OfficeLocation = employee.Info.OfficeLocation
                } : null,

                WorkCountry = employee.WorkCountry,
                WorkState = employee.WorkState,
                WorkCounty = employee.WorkCounty,
                WorkCity = employee.WorkCity,
                CostCenterCode = employee.CostCenterCode,
                Meets50In75MileRule = employee.Meets50In75MileRule,
                HireDate = employee.HireDate,
                RehireDate = employee.RehireDate,
                ServiceDate = employee.ServiceDate,
                TerminationDate = employee.TerminationDate,
                MinutesWorkedPerWeek = lstMinutesWorkedPerWeek,
                PayType = employee.PayType,
                Salary = employee.Salary,
                IsExempt = employee.IsExempt,
                IsKeyEmployee = employee.IsKeyEmployee,
                PayScheduleId = employee.PayScheduleId,
                Status = (Char)employee.Status,
                SpouseEmployeeNumber = employee.SpouseEmployeeNumber,
                Department = employee.Department,
                IsFlightCrew=employee.IsFlightCrew,
                CustomerId = employee.CustomerId,
                CreatedOn = employee.CreatedDate.ToUniversalTime(),
                CreatedBy = employee.CreatedBy?.DisplayName,
                ModifiedOn = employee.ModifiedDate.ToUniversalTime(),
                ModifiedBy = employee.ModifiedBy?.DisplayName
            };
        }

    }
}