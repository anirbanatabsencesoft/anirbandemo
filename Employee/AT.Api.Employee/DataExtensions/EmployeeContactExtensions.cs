﻿using AbsenceSoft.Data.Customers;
using AT.Api.Employee.Models.EmployeeContact;

namespace AT.Api.Employee.DataExtensions
{
    internal static class EmployeeContactExtensions
    {
        internal static EmployeeContactModel ToContactModel(this EmployeeContact employeeContact)
        {
            var model = new EmployeeContactModel
            {
                ContactId = employeeContact.Id,
                ContactPosition = employeeContact.ContactPosition,
                ContactTypeCode = employeeContact.ContactTypeCode ?? string.Empty,

                Contact = employeeContact.Contact.ToContactModel(),
                RelatedEmployeeNumber = employeeContact.RelatedEmployeeNumber,
                MilitaryStatus = ((int)employeeContact.MilitaryStatus)
            };

            return model;
        }

    }
}