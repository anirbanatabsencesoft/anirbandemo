﻿using AT.Api.Shared.Model.CustomFields;
using System.Collections.Generic;
using System.Linq;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.DataExtensions
{
    internal static class CustomFieldsExtensions
    {
        internal static CustomFieldModel ToCustomFieldModel(this Customers.CustomField customField)
        {
            var model = new CustomFieldModel()
            {
                Code = customField.Code,
                Value = customField.SelectedValue,
                Name = customField.Name,
                DataType = (int)customField.DataType,
                Description = customField.Description,
                ValueType = (int)customField.ValueType,
                ListValues = customField.ListValues != null
                     ? customField.ListValues.ToDictionary(x => x.Key, x => x.Value)
                     : new Dictionary<string, string>()
            };

            return model;
        }
    }
}