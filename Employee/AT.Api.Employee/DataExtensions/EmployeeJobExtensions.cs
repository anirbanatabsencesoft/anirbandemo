﻿using AbsenceSoft.Data.Jobs;
using AT.Api.Employee.Models.EmployeeJob;

namespace AT.Api.Employee.DataExtensions
{
    internal static class EmployeeJobExtensions
    {
        internal static EmployeeJobModel ToJobModel(this EmployeeJob job)
        {
            var model = new EmployeeJobModel
            {
                Status = (int)job.Status.Value,
                DenialReason = job.Status.Reason.HasValue ? ((int?)job.Status.Reason) : null,
                DenialReasonOther = job.Status.Reason != JobDenialReason.Other ? string.Empty : job.Status.Comments,
                EndDate = job.Dates.EndDate,
                StartDate = job.Dates.StartDate,
                JobCode = job.JobCode,
                JobName = job.JobName,
                JobPosition = job.JobPosition,
                JobTitle = job.JobTitle,
                OfficeLocationCode = job.OfficeLocationCode,
                JobActivity = job.Job.Activity.HasValue ? ((int?)job.Job.Activity) : null
            };

            return model;
        }
    }
}