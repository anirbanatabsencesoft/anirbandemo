﻿using AbsenceSoft.Data;
using AT.Api.Employee.Models.EmployeeContact;

namespace AT.Api.Employee.DataExtensions
{
    internal static class ContactExtensions
    {
        internal static ContactModel ToContactModel(this Contact contact)
        {
            var model = new ContactModel
            {
                Title = contact.Title,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                MiddleName = contact.MiddleName,
                CompanyName = contact.CompanyName,
                ContactPersonName = contact.ContactPersonName,
                DateOfBirth = contact.DateOfBirth,
                Email = contact.Email,
                AltEmail = contact.AltEmail,
                HomePhone = contact.HomePhone,
                CellPhone = contact.CellPhone,
                WorkPhone = contact.WorkPhone,
                Address = contact.Address.ToAddressModel()
            };

            return model;
        }
    }
}