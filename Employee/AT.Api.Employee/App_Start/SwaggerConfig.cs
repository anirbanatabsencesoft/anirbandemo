using System.Web.Http;
using AT.Api.Core;

namespace AT.Api.Employee
{
    /// <summary>
    /// Configuration for swagger
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// Registers swagger for the API
        /// </summary>
        /// <param name="configuration">The current HTTP configuration</param>
        public static void Register(HttpConfiguration configuration)
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.RegisterWithConfig(configuration, "Employee", thisAssembly);
        }
    }
}
