﻿using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.Common;
using AT.Api.Employee.Requests.PriorHoursWorked;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;


namespace AT.Api.Employee.Tests.Requests.PriorHoursWorked
{
    [TestClass]
    public class SavePriorHoursWorkedRequestTests
    {
        [TestMethod]
        public void SavePriorHoursWorked_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [TestMethod]
        public void SavePriorHoursWorked_NullPriorHours_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                PriorHours = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Prior hours worked details are required", response.Message);
        }

        [TestMethod]
        public void SavePriorHoursWorked_ZeroPriorHours_ThrowsException()
        {
            //// Arrange            
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                PriorHours = Model(hoursWorked: 0)
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Prior hours worked details are required", response.Message);
        }

        [TestMethod]
        public void SavePriorHoursWorked_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var priorHours1 = EntityHelper.PriorHours(hoursWorked: 10, asOf: datetime);
            var priorHours2 = EntityHelper.PriorHours(hoursWorked: 20, asOf: datetime);
            var employee = EntityHelper.Employee(priorHours: new List<Customers.PriorHours> { priorHours1, priorHours2 });            
            var employeeService = new Mock<IEmployeeService>();

            employee.UpdateAuditInfo(user, datetime);

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(

                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                PriorHours = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        [TestMethod]
        public void SavePriorHoursWorked_EmployeeNotFound_ReturnsEmptyPriorHours()
        {
            //// Arrange            
            var employeeService = EmployeeServiceStub(null);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                PriorHours = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_DeletedEmployeeFound_ReturnsEmptyPriorHours()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            employee.IsDeleted = true;
            var employeeService = EmployeeServiceStub(employee);
            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                PriorHours = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SavePriorHoursWorked_ValidData_ReturnsPriorHours()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var priorHours1 = EntityHelper.PriorHours(hoursWorked: 10, asOf: datetime);
            var priorHours2 = EntityHelper.PriorHours(hoursWorked: 20, asOf: datetime);
            var employee = EntityHelper.Employee(priorHours: new List<Customers.PriorHours> { priorHours1, priorHours2 });
            employee.UpdateAuditInfo(user, datetime);
            var employeeService = EmployeeServiceStub(employee);

            var request = new SavePriorHoursWorkedRequest(employeeService.Object);

            var parameter = new SavePriorHoursWorkedParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                PriorHours = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Prior hours worked details updated for the given employee", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, null))
                .Returns(employee);

            return employeeService;
        }

        private PriorHoursModel Model(
            double hoursWorked = 100,
            DateTime? asOf = null)
        {
            return new PriorHoursModel
            {
                HoursWorked = hoursWorked,
                AsOf = asOf ?? DateTime.UtcNow
            };
        }
    }
}

