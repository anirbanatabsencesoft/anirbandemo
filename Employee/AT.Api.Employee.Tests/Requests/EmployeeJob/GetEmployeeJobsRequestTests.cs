﻿using AbsenceSoft;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeJob;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;
using Jobs = AbsenceSoft.Data.Jobs;

namespace AT.Api.Employee.Tests.Requests.EmployeeJob
{
    [TestClass]
    public class GetEmployeeJobsRequestTests
    {
        [TestMethod]
        public void GetJobs_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = string.Empty,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = string.Empty
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeJobsRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetJobs_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange       
            Customers.Employee employee = null;
            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetJobs_DeletedEmployee_ThrowsException()
        {
            //// Arrange       
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameters = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetJobs_EmptyJobs_ReturnsEmptyJobs()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeJobs = new List<Jobs.EmployeeJob>();

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No jobs found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetJobs_NoJobs_ReturnsEmptyJobs()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No jobs found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }


        [TestMethod]
        public void GetJobs_ValidEmployee_ReturnsJobs()
        {
            //// Arrange
            var startDate1 = DateTime.UtcNow;
            var startDate2 = DateTime.UtcNow.AddMonths(-2);

            var endDate = startDate1.AddDays(-1);
            var employee = EntityHelper.Employee();

            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var job2 = EntityHelper.Job(id: "JobId02", code: "Code02", activity: JobClassification.Medium);

            var employeeJob1 = EntityHelper.EmployeeJob(employee: employee, job: job1, dateRange: new DateRange(startDate1));

            var employeeJob2 = EntityHelper.EmployeeJob("EmployeeJobId2", "Code02", "JobName02", 1234, "JobTitle02",
                status: AdjudicationStatus.Denied, reason: JobDenialReason.Other, comment: "DeniedReson", officeLocationCode: "OfficeLocationCode",
               dateRange: new DateRange(startDate2, endDate), employee: employee, job: job2);

            var employeeJobs = new List<Jobs.EmployeeJob>
            {
                 employeeJob1, employeeJob2
            };

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("Code01", result1.JobCode);
            Assert.AreEqual("JobName01", result1.JobName);
            Assert.AreEqual("JobTitle01", result1.JobTitle);
            Assert.AreEqual(2, result1.JobActivity);
            Assert.AreEqual(0, result1.Status);
            Assert.AreEqual(null, result1.DenialReason);
            Assert.AreEqual(string.Empty, result1.DenialReasonOther);
            Assert.IsTrue(result1.StartDate.Value.IsSameTimeAs(startDate1));
            Assert.AreEqual(null, result1.EndDate);
            Assert.AreEqual(null, result1.OfficeLocationCode);
            Assert.AreEqual(null, result1.JobPosition);

            var result2 = response.Data.ElementAt(1);

            Assert.AreEqual("Code02", result2.JobCode);
            Assert.AreEqual("JobName02", result2.JobName);
            Assert.AreEqual("JobTitle02", result2.JobTitle);
            Assert.AreEqual(3, result2.JobActivity);
            Assert.AreEqual(2, result2.Status);
            Assert.AreEqual(0, result2.DenialReason);
            Assert.AreEqual("DeniedReson", result2.DenialReasonOther);
            Assert.IsTrue(result2.StartDate.Value.IsSameTimeAs(startDate2));
            Assert.IsTrue(result2.EndDate.Value.IsSameTimeAs(endDate));
            Assert.AreEqual("OfficeLocationCode", result2.OfficeLocationCode);
            Assert.AreEqual(1234, result2.JobPosition);
        }

        [TestMethod]
        public void GetJobs_ByJobCode_ReturnsJobs()
        {
            //// Arrange
            var startDate1 = DateTime.UtcNow;

            var startDate2 = DateTime.UtcNow.AddMonths(-2);

            var endDate = startDate1.AddDays(-1);
            var employee = EntityHelper.Employee();

            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var job2 = EntityHelper.Job(id: "JobId02", code: "Code02", activity: JobClassification.Medium);

            var employeeJob1 = EntityHelper.EmployeeJob(employee: employee, job: job1, dateRange: new DateRange(startDate1));

            var employeeJob2 = EntityHelper.EmployeeJob("EmployeeJobId2", "Code02", "JobName02", 1234, "JobTitle02",
                status: AdjudicationStatus.Denied, reason: JobDenialReason.Other, comment: "DeniedReson", officeLocationCode: "OfficeLocationCode",
               dateRange: new DateRange(startDate2, endDate), employee: employee, job: job2);


            var employeeJobs = new List<Jobs.EmployeeJob>
            {
                 employeeJob1, employeeJob2
            };

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                JobCode = "Code02"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("Code02", result1.JobCode);
            Assert.AreEqual("JobName02", result1.JobName);
            Assert.AreEqual("JobTitle02", result1.JobTitle);
            Assert.AreEqual(3, result1.JobActivity);
            Assert.AreEqual(2, result1.Status);
            Assert.AreEqual(0, result1.DenialReason);
            Assert.AreEqual("DeniedReson", result1.DenialReasonOther);
            Assert.IsTrue(result1.StartDate.Value.IsSameTimeAs(startDate2));
            Assert.IsTrue(result1.EndDate.Value.IsSameTimeAs(endDate));
            Assert.AreEqual("OfficeLocationCode", result1.OfficeLocationCode);
            Assert.AreEqual(1234, result1.JobPosition);
        }

        [TestMethod]
        public void GetJobs_ByDateRange_ReturnsJobs()
        {
            //// Arrange

            var startDate1 = DateTime.UtcNow;

            var startDate2 = DateTime.UtcNow.AddMonths(-2);

            var endDate = startDate1.AddDays(-1);
            var employee = EntityHelper.Employee();

            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var job2 = EntityHelper.Job(id: "JobId02", code: "Code02", activity: JobClassification.Medium);

            var employeeJob1 = EntityHelper.EmployeeJob(employee: employee, job: job1, dateRange: new DateRange(startDate1));

            var employeeJob2 = EntityHelper.EmployeeJob("EmployeeJobId2", "Code02", "JobName02", 1234, "JobTitle02",
                status: AdjudicationStatus.Denied, reason: JobDenialReason.Other, comment: "DeniedReson", officeLocationCode: "OfficeLocationCode",
               dateRange: new DateRange(startDate2, endDate), employee: employee, job: job2);

            var employeeJobs = new List<Jobs.EmployeeJob>
            {
                 employeeJob1, employeeJob2
            };

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                StartDate = startDate1.AddDays(-1),
                EndDate = DateTime.UtcNow
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("Code01", result1.JobCode);
            Assert.AreEqual("JobName01", result1.JobName);
            Assert.AreEqual("JobTitle01", result1.JobTitle);
            Assert.AreEqual(2, result1.JobActivity);
            Assert.AreEqual(0, result1.Status);
            Assert.AreEqual(null, result1.DenialReason);
            Assert.AreEqual(string.Empty, result1.DenialReasonOther);
            Assert.IsTrue(result1.StartDate.Value.IsSameTimeAs(startDate1));
            Assert.AreEqual(null, result1.EndDate);
            Assert.AreEqual(null, result1.OfficeLocationCode);
            Assert.AreEqual(null, result1.JobPosition);
        }

        [TestMethod]
        public void GetJobs_ByInactiveJobs_ReturnsJobs()
        {
            //// Arrange

            var startDate1 = DateTime.UtcNow;

            var startDate2 = DateTime.UtcNow.AddMonths(-2);

            var endDate = startDate1.AddDays(-1);
            var employee = EntityHelper.Employee();

            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var job2 = EntityHelper.Job(id: "JobId02", code: "Code02", activity: JobClassification.Medium);

            var employeeJob1 = EntityHelper.EmployeeJob(employee: employee, job: job1, dateRange: new DateRange(startDate1));

            var employeeJob2 = EntityHelper.EmployeeJob("EmployeeJobId2", "Code02", "JobName02", 1234, "JobTitle02",
                status: AdjudicationStatus.Denied, reason: JobDenialReason.Other, comment: "DeniedReson", officeLocationCode: "OfficeLocationCode",
               dateRange: new DateRange(startDate2, endDate), employee: employee, job: job2);

            var employeeJobs = new List<Jobs.EmployeeJob>
            {
                 employeeJob1, employeeJob2
            };

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                StartDate = startDate1,
                EndDate = null
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("Code01", result1.JobCode);
            Assert.AreEqual("JobName01", result1.JobName);
            Assert.AreEqual("JobTitle01", result1.JobTitle);
            Assert.AreEqual(2, result1.JobActivity);
            Assert.AreEqual(0, result1.Status);
            Assert.AreEqual(null, result1.DenialReason);
            Assert.AreEqual(string.Empty, result1.DenialReasonOther);
            Assert.IsTrue(result1.StartDate.Value.IsSameTimeAs(startDate1));
            Assert.AreEqual(null, result1.EndDate);
            Assert.AreEqual(null, result1.OfficeLocationCode);
            Assert.AreEqual(null, result1.JobPosition);
        }

        [TestMethod]
        public void GetJobs_ByStartDate_ReturnsJobs()
        {
            //// Arrange

            var startDate1 = DateTime.UtcNow;

            var startDate2 = DateTime.UtcNow.AddMonths(-2);

            var endDate = startDate1.AddDays(-1);
            var employee = EntityHelper.Employee();

            var job1 = EntityHelper.Job(activity: JobClassification.Light, isDeleted:true);
            var job2 = EntityHelper.Job(id: "JobId02", code: "Code02", activity: JobClassification.Medium);

            var employeeJob1 = EntityHelper.EmployeeJob(employee: employee, job: job1, dateRange: new DateRange(startDate1));

            var employeeJob2 = EntityHelper.EmployeeJob("EmployeeJobId2", "Code02", "JobName02", 1234, "JobTitle02",
                status: AdjudicationStatus.Denied, reason: JobDenialReason.Other, comment: "DeniedReson", officeLocationCode: "OfficeLocationCode",
               dateRange: new DateRange(startDate2, endDate), employee: employee, job: job2);

            var employeeJobs = new List<Jobs.EmployeeJob>
            {
                 employeeJob1, employeeJob2
            };

            var employeeService = EmployeeServiceStub(employee, employeeJobs);

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Inactive = true
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("Code01", result1.JobCode);
            Assert.AreEqual("JobName01", result1.JobName);
            Assert.AreEqual("JobTitle01", result1.JobTitle);
            Assert.AreEqual(2, result1.JobActivity);
            Assert.AreEqual(0, result1.Status);
            Assert.AreEqual(null, result1.DenialReason);
            Assert.AreEqual(string.Empty, result1.DenialReasonOther);
            Assert.IsTrue(result1.StartDate.Value.IsSameTimeAs(startDate1));
            Assert.AreEqual(null, result1.EndDate);
            Assert.AreEqual(null, result1.OfficeLocationCode);
            Assert.AreEqual(null, result1.JobPosition);
        }

        [TestMethod]
        public void GetJobs_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var employee = EntityHelper.Employee(customerId: "000000000000000000000001", employerId: "000000000000000000000001");

            var jobs = new List<Jobs.EmployeeJob>();

            var employeeMock = new Mock<Customers.Employee>();
            employeeMock
               .Setup(x => x.GetJobs())
               .Returns(jobs)
               .Verifiable();

            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employeeMock.Object)
                .Verifiable();

            var request = new GetEmployeeJobsRequest(employeeService.Object);

            var parameter = new GetEmployeeJobsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };


            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee, List<Jobs.EmployeeJob> employeeJobs = null)
        {
            var employeeMock = new Mock<Customers.Employee>();
            employeeMock
                .Setup(x => x.GetJobs())
                .Returns(employeeJobs);

            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employeeMock.Object);

            return employeeService;
        }
    }
}
