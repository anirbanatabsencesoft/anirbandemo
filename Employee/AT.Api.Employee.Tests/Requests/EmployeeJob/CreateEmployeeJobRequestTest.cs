﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using AbsenceSoft.Logic.Jobs.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.EmployeeJob;
using AT.Api.Employee.Requests.EmployeeJob;
using AT.Common.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;
using Jobs = AbsenceSoft.Data.Jobs;

namespace AT.Api.Employee.Tests.Requests.EmployeeJob
{
    [TestClass]
    public class CreateEmployeeJobRequestTest
    {
        [TestMethod]
        public void CreateJob_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = string.Empty,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = string.Empty
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_NullJobCode_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = null
                }
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("JobCode is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_EmptyJobCode_ThrowsException()
        {
            //// Arrange
            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = " "
                }
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new CreateEmployeeJobRequest().Handle(parameters));

            //// Assert
            Assert.IsInstanceOfType(exception, typeof(ApiException));
            Assert.AreEqual("JobCode is required", exception.Message);
        }

        [TestMethod]
        public void CreateJob_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            Customers.Employee employee = null;
            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            var request = new CreateEmployeeJobRequest(employeeService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel { JobCode = "Code01" }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateJob_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            var request = new CreateEmployeeJobRequest(employeeService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel { JobCode = "Code01" }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateJob_JobNotFound_ThrowsException()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var job = EntityHelper.Job(activity: JobClassification.Light);
            var employeeJob = EntityHelper.EmployeeJob(employee: employee, job: job, dateRange: new DateRange(startDate));

            var employeeService = EmployeeServiceStub(employeeJob);
            var jobService = JobServiceStub(null);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = employeeJob.JobCode,
                    JobPosition = employeeJob.JobPosition,
                    OfficeLocationCode = employeeJob.OfficeLocationCode,
                    Status = (int)employeeJob.Status.Value,
                    StartDate = startDate,
                    EndDate = null
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("No job found for code", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);

        }

        [TestMethod]
        public void CreateJob_ExistingJob_ThrowsException()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var job = EntityHelper.Job(activity: JobClassification.Light);
            var employeeJob = EntityHelper.EmployeeJob(employee: employee, job: job, dateRange: new DateRange(startDate));

            var employeeService = EmployeeServiceStub(employeeJob);
            var jobService = JobServiceStub(job);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = employeeJob.JobCode,
                    JobPosition = employeeJob.JobPosition,
                    OfficeLocationCode = employeeJob.OfficeLocationCode,
                    Status = (int)employeeJob.Status.Value,
                    StartDate = startDate,
                    EndDate = null
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("Employee Job Already Saved", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

        }

        [TestMethod]
        public void CreateJob_ValidJob_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var job2 = EntityHelper.Job(code: "Code02");

            var dateRange = new DateRange(startDate);
            var employeeJob = EmployeeJobStub();
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.Dates).Returns(dateRange);

            var employeeService = EmployeeServiceStub(employeeJob.Object, new DateTime(2018, 05, 10));
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job2);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "JobCode02"
                }
            };

            ResultSet<string> response = null;

            //// Act
            response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateJobPosition_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob());
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.JobPosition).Returns(12);
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    JobPosition = 34,
                    Status = (int)AdjudicationStatus.Pending
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateOfficeLocationCode_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob(officeLocationCode: "OfficialCode01"));
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    JobPosition = 34,
                    OfficeLocationCode = "OfficialCode02",
                    Status = (int)AdjudicationStatus.Pending,
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_NullDatesUpdateStartDate_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob());
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = startDate
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateStartDate_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var dateRange = new DateRange(startDate);
            var employeeJob = EmployeeJobStub();
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);
            employeeJob.Setup(x => x.Dates).Returns(dateRange);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = startDate.AddDays(1)
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_NullDatesUpdateEndDate_ReturnsJob()
        {
            //// Arrange
            var date = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob());
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    EndDate = date
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateEndDate_ReturnsJob()
        {
            //// Arrange
            var date = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var dateRange = new DateRange(date.AddMonths(-1), date.AddDays(1));
            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob());
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);
            employeeJob.Setup(x => x.Dates).Returns(dateRange);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = date.AddMonths(-1),
                    EndDate = date
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateStatus_ReturnsJob()
        {
            //// Arrange
            var date = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeJob = EmployeeJobStub(EntityHelper.EmployeeJob());
            employeeJob.Setup(x => x.JobCode).Returns("Code01");
            employeeJob.Setup(x => x.OfficeLocationCode).Returns("OfficialCode01");
            employeeJob.Setup(x => x.Status.Value).Returns(AdjudicationStatus.Pending);

            var user = EntityHelper.UserStub();
            var job1 = EntityHelper.Job(activity: JobClassification.Light);
            var employeeService = EmployeeServiceStub(employeeJob.Object);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job1);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    Status = (int)AdjudicationStatus.Approved
                }
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_ExistingJobCode_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow.AddMonths(-2);

            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var job = EntityHelper.Job(activity: JobClassification.Light);

            var employeeJob = EntityHelper.EmployeeJob();

            var employeeService = EmployeeServiceStub(employeeJob);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    JobPosition = 34,
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = startDate,
                    EndDate = null
                }
            };

            ResultSet<string> response = null;

            //// Act

            response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_UpdateJobActivity_ReturnsJob()
        {
            //// Arrange
            var startDate = DateTime.UtcNow.AddMonths(-2);

            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var job = EntityHelper.Job(activity: JobClassification.Light);

            var employeeJob = EntityHelper.EmployeeJob();

            var employeeService = EmployeeServiceStub(employeeJob);
            var adminService = AdminServiceStub(user);
            var jobService = JobServiceStub(job);
            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    JobPosition = 34,
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = startDate,
                    EndDate = null,
                    JobTitle = "JobTitle01",
                    JobActivity = (int)JobClassification.Medium
                }
            };

            ResultSet<string> response = null;

            //// Act
            response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("EmployeeJobId01", response.Data);
            Assert.AreEqual("Job Saved Successfully", response.Message);
        }

        [TestMethod]
        public void CreateJob_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange

            var job = EntityHelper.Job();
            var startDate = DateTime.UtcNow;
            var employeeJob = EntityHelper.EmployeeJob();
            var user = EntityHelper.UserStub();

            var employeeMock = new Mock<Customers.Employee>();
            employeeMock
                .Setup(x => x.GetCurrentJob())
                .Returns(employeeJob);

            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employeeMock.Object)
                .Verifiable();

            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var jobService = new Mock<IJobService>();
            jobService
              .Setup(x => x.UpdateEmployeeJob(
                  It.Is<Jobs.EmployeeJob>(
                      empjob =>
                          empjob.JobCode == "Code01" &&
                          empjob.JobPosition == 34 &&
                          empjob.OfficeLocationCode == "OfficialCode01" &&
                          empjob.Status.Value == (int)AdjudicationStatus.Pending &&
                          empjob.Dates.StartDate == startDate.ToMidnight() &&
                          empjob.Dates.EndDate == null &&
                          empjob.JobTitle == "JobTitle01"),
                  It.Is<bool>(j => j == true)))
               .Verifiable();

            jobService
             .Setup(x => x.GetJobs(
                 It.Is<string>(cid => cid == "CustomerId01"),
                 It.Is<string>(eid => eid == "EmployerId01"),
                 It.Is<string>(code => code == "Code01"),
                 It.Is<bool>(i => i == false)))
             .Returns(new List<Jobs.Job> { job })
             .Verifiable();

            var request = new CreateEmployeeJobRequest(employeeService.Object, jobService.Object, adminService.Object);

            var parameters = new CreateEmployeeJobParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                UserId = "UserKey",
                EmployeeJob = new CreateEmployeeJobModel
                {
                    JobCode = "Code01",
                    JobPosition = 34,
                    OfficeLocationCode = "OfficialCode01",
                    Status = (int)AdjudicationStatus.Pending,
                    StartDate = startDate,
                    EndDate = null,
                    JobTitle = "JobTitle01",
                    JobActivity = (int)JobClassification.Medium,
                    EndCurrentJob = true
                }
            };

            ResultSet<string> response = null;

            //// Act
            response = request.Handle(parameters);

            //Assert
            employeeService.Verify();
            jobService.Verify();
            adminService.Verify();
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<Jobs.EmployeeJob> EmployeeJobStub(Jobs.EmployeeJob employeeJob = null)
        {
            var employeeJobMock = new Mock<Jobs.EmployeeJob>();
            employeeJobMock
                .Setup(x => x.Save())
                .Returns(employeeJob);

            return employeeJobMock;
        }


        private Mock<IJobService> JobServiceStub(Jobs.Job job)
        {
            var employeeJobService = new Mock<IJobService>();

            employeeJobService
                .Setup(x => x.UpdateEmployeeJob(It.IsAny<Jobs.EmployeeJob>(), It.IsAny<bool>()));

            employeeJobService
                .Setup(x => x.GetJobs(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new List<Jobs.Job> { job });

            return employeeJobService;
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Jobs.EmployeeJob employeeJob = null, DateTime? serviceDate = null)
        {
            var employeeMock = new Mock<Customers.Employee>();
            employeeMock
                .Setup(x => x.GetCurrentJob())
                .Returns(employeeJob);


            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employeeMock.Object);

            employeeService
               .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, null))
               .Returns(employeeMock.Object);

            if (serviceDate.HasValue)
            {
                employeeMock.Setup(e => e.ServiceDate).Returns(serviceDate);
            }

            return employeeService;
        }
    }
}
