﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeCustomField;
using AT.Api.Shared.Model.CustomFields;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeCustomField
{

    [TestClass]
    public class SaveEmployeeCustomFieldRequestTests
    {
        [TestMethod]
        public void SaveEmployeeCustomField_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new SaveEmployeeCustomFieldRequest(
                    "UserKey",
                    adminUserService: adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullCustomField_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = null,
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom fields", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_EmptyCustomFieldCode_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel() { Code = " " }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom fields", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullCustomFieldCode_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel() { Code = null }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Invalid custom fields", response.Message);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub();
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel { Code = "001" }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee(isDeleted:true);
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel { Code = "001" }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NullEmployeeCustomField_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "001"
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No configuration fields setting for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NoEmployeeCustomField_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee(customFields: new List<CustomField>());
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub();
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "001"
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
               request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No configuration fields setting for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NoMachingCode_ThrowsException()
        {
            //// Arrange
            var customField = EntityHelper.CustomField();
            var customFields = new List<CustomField>() { customField };
            var employee = EntityHelper.Employee(customFields: customFields);
            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "00A"
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No custom field configuration for requested code", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_UpdateExistingCustomFieldForEmployee_ReturnsCustomFieldId()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1, customField2 };

            var employee = EntityHelper.Employee(customFields: customFields);
            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "Code01",
                    Value = "ChangedValue01"
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("FieldId01", response.Data);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_AddCustomFieldForEmployee_ReturnsCustomFieldId()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1, customField2 };

            var employee = EntityHelper.Employee();
            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "Code01",
                    Value = "ChangedValue01"
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("FieldId01", response.Data);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_NotFoundEmployeeId_ReturnsCustomFieldId()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(employeeId: string.Empty);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new SaveEmployeeCustomFieldRequest(
                "UserKey",
                employeeService.Object,
                employerService.Object,
                adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "Code01",
                    Value = "ChangedValue01"
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("FieldId01", response.Data);
        }

        [TestMethod]
        public void SaveEmployeeCustomField_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var user = EntityHelper.UserStub();
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();
            var customField1 = EntityHelper.CustomField();
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);
            employee.SetCustomer(customer).SetEmployer(employer);


            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            employeeService
              .Setup(x => x.Update(It.Is<Customers.Employee>(
                        emp => emp.CustomerId == "CustomerId01" &&
                            emp.EmployeeNumber == "EmployeeNo01" &&
                            emp.EmployerId == "EmployerId01"),
                        It.Is<Schedule>(Schedule => Schedule == null),
                        It.Is<List<VariableScheduleTime>>(schTime => schTime == null)))
              .Returns(employee)
              .Verifiable();

            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var employerService = new Mock<IEmployerService>();
            employerService
             .Setup(x => x.GetCustomFields(
                 It.Is<EntityTarget>(er => er == EntityTarget.Employee),
                 It.Is<bool?>(er => er == null)))
             .Returns(customFields)
             .Verifiable();

            var request = new SaveEmployeeCustomFieldRequest(
               "UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new SaveEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                CustomField = new SaveCustomFieldModel
                {
                    Code = "Code01",
                    Value = "ChangedValue01"
                }
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
              .Setup(x => x.Update(It.IsAny<Customers.Employee>(), It.IsAny<Schedule>(), It.IsAny<List<VariableScheduleTime>>()))
              .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IEmployerService> EmployerServiceStub(List<CustomField> customFields = null)
        {
            var employerService = new Mock<IEmployerService>(MockBehavior.Loose);

            employerService
                .Setup(x => x.GetCustomFields(It.Is<EntityTarget>(en => en == EntityTarget.Employee), It.IsAny<bool?>()))
                .Returns(customFields);

            return employerService;
        }
    }
}
