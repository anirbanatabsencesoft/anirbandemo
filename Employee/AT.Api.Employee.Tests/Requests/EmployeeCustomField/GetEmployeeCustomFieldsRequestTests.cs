﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeCustomField;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeCustomField
{

    [TestClass]
    public class GetEmployeeCustomFieldsRequestTests
    {
        [TestMethod]
        public void SaveEmployeeCustomField_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new GetEmployeeCustomFieldsRequest(
                    "UserKey",
                    adminUserService: adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }
        [TestMethod]
        public void GetEmployeeCustomFields_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields:customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }      

        [TestMethod]
        public void GetEmployeeCustomFields_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: customFields);

            var employerService = EmployerServiceStub(customFields);            
            var adminService = AdminServiceStub(user);
            var employeeService = EmployeeServiceStub(null);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object); 

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee(isDeleted: true);                       

            var user = EntityHelper.UserStub();

            var employerService = EmployerServiceStub(new List<CustomField>());
            var adminService = AdminServiceStub(user);
            var employeeService = EmployeeServiceStub(employee);


            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object); ;

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_NullEmployeeCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange            
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields: null);

            var employerService = EmployerServiceStub(customFields);
            var adminService = AdminServiceStub(user);
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object); ;

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployeeCustomFields_NoEmployeeCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange
            var employee = EntityHelper.Employee(customFields: new List<CustomField>());
           
            var user = EntityHelper.UserStub();

            var customField1 = EntityHelper.CustomField(isCollectedAtIntake: true);
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<CustomField>() { customField1 };
            
            var employerService = EmployerServiceStub(customFields);
            var adminService = AdminServiceStub(user);
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object); ;

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }
      
        [TestMethod]
        public void GetEmployeeCustomFields_ValidEmployee_ReturnsCustomField()
        {
            //// Arrange
            var listItems = new List<ListItem>()
            {
                new ListItem { Key = "1",Value= "Number1"},
                new ListItem { Key = "2",Value= "Number2"}
            };

            var customField1 = EntityHelper.CustomField();

            var customField2 = EntityHelper.CustomField("Field02","Code02", "Name02", CustomFieldType.Number, CustomFieldValueType.SelectList, "1",listItems, "Description02");
            var customFields = new List<CustomField>() { customField1, customField2 };

            var employee = EntityHelper.Employee(customFields: customFields);
           
            var user = EntityHelper.UserStub();

        
            var employerService = EmployerServiceStub(customFields);
            var adminService = AdminServiceStub(user);
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2,response.Data.Count());

            var result1 = response.Data.ElementAt(0);
            Assert.AreEqual("Code01", result1.Code);
            Assert.AreEqual("Value01", result1.Value);
            Assert.AreEqual("Description01", result1.Description);
            Assert.AreEqual("Name01", result1.Name);
            Assert.AreEqual(0, result1.ListValues.Count);
            Assert.AreEqual(1, result1.ValueType);
            Assert.AreEqual(1, result1.DataType);

            var result2 = response.Data.ElementAt(1);
            Assert.AreEqual("Code02", result2.Code);
            Assert.AreEqual("1", result2.Value);
            Assert.AreEqual("Description02", result2.Description);
            Assert.AreEqual("Name02", result2.Name);
            Assert.AreEqual(2, result2.ListValues.Count);
            Assert.AreEqual(2, result2.ValueType);
            Assert.AreEqual(2, result2.DataType);

            var itemKeys = result2.ListValues.Keys.ToList();
            Assert.AreEqual("1", itemKeys[0]);
            Assert.AreEqual("2", itemKeys[1]);

            var itemValues = result2.ListValues.Values.ToList();
            Assert.AreEqual("Number1", itemValues[0]);
            Assert.AreEqual("Number2", itemValues[1]);
        }     

        [TestMethod]
        public void GetEmployeeCustomFields_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var customField1 = EntityHelper.CustomField();
            var customFields = new List<CustomField>() { customField1 };

            var employee = EntityHelper.Employee(customFields:customFields);
            var user = EntityHelper.UserStub();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var adminService = new Mock<IAdminUserService>();
            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var employerService = new Mock<IEmployerService>();
            employerService
             .Setup(x => x.GetCustomFields(
                 It.Is<EntityTarget>(er => er == EntityTarget.Employee),
                 It.Is<bool?>(er => er == null)))
             .Returns(customFields)
             .Verifiable();

            var request = new GetEmployeeCustomFieldsRequest("UserKey",
               employeeService.Object,
               employerService.Object,
               adminService.Object);

            var parameter = new GetEmployeeCustomFieldsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }



        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private Mock<IEmployerService> EmployerServiceStub(List<CustomField> customFields = null)
        {
            var employerService = new Mock<IEmployerService>(MockBehavior.Loose);

            employerService
                .Setup(x => x.GetCustomFields(It.Is<EntityTarget>(en => en == EntityTarget.Employee), It.IsAny<bool?>()))
                .Returns(customFields);

            return employerService;
        }

    }
}
