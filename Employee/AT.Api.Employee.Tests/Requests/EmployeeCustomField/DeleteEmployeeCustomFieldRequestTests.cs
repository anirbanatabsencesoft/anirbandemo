﻿using Customers = AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeCustomField;
using System.Net;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeCustomField
{

    [TestClass]
    public class DeleteEmployeeCustomFieldRequestTests
    {
        [TestMethod]
        public void DeleteEmployeeCustomField_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employee number is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Employer Id is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Customer Id is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NullCode_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Custom Field Code is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_EmptyCode_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Custom Field Code is required", response.Message);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            var employeeService = EmployeeServiceStub(null);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "001"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "001"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NullEmployeeCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange            
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "001"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_NoEmployeeCustomField_ReturnsEmptyCustomField()
        {
            //// Arrange
            var employee = EntityHelper.Employee(customFields: new List<CustomField>());
            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "001"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("No custom fields configuration for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }


        [TestMethod]
        public void DeleteEmployeeCustomField_NoMachingCode_ThrowsException()
        {
            //// Arrange
            var customField = EntityHelper.CustomField();
            var customFields = new List<CustomField>() { customField };
            var employee = EntityHelper.Employee(customFields: customFields);
            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "00A"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No custom field configuration for requested code", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployeeCustomField_ValidCustomCode_ReturnsCustomFieldId()
        {
            //// Arrange
            var customField1 = EntityHelper.CustomField();
            var customField2 = EntityHelper.CustomField("Code02", "Name02", dataType: CustomFieldType.Date, selectedValue: "01-01-2018");
            var customFields = new List<Customers.CustomField>() { customField1, customField2 };

            var employee = EntityHelper.Employee(customFields: customFields);
            var employeeService = EmployeeServiceStub(employee);

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "Code01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("FieldId01", response.Data);
        }


        [TestMethod]
        public void DeleteEmployeeCustomField_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var request = new DeleteEmployeeCustomFieldRequest(employeeService.Object);

            var parameter = new DeleteEmployeeCustomFieldParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Code = "Code02"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

    }
}
