﻿using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.Employee
{
    [TestClass]
    public class GetEmployeeRequestTests
    {
        [TestMethod]
        public void GetEmployee_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
                        
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_EmployeeNotFound_ReturnsEmptyEmployee()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(null);

            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [TestMethod]
        public void GetEmmployee_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var employee = EntityHelper.Employee();
            var employeeService = new Mock<IEmployeeService>();

            employee.UpdateAuditInfo(user, datetime);

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        [TestMethod]
        public void GetEmployee_ValidInput_ReturnsEmployee()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var employee = EntityHelper.Employee();

            employee.UpdateAuditInfo(user, datetime);
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new GetEmployeeRequest(employeeService.Object);

            var parameter = new GetEmployeeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("EmployeeNo01", response.Data.Number);
            Assert.AreEqual("FirstName01", response.Data.FirstName);
            Assert.AreEqual("LastName01", response.Data.LastName);
            Assert.AreEqual("WorkCountry01", response.Data.WorkCountry);
            Assert.AreEqual("WorkState01", response.Data.WorkState);
            Assert.AreEqual("Employee found for the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }
          
    }
}
