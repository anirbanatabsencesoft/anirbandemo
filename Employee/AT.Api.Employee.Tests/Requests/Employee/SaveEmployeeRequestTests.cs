﻿using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.Common;
using AT.Api.Employee.Models.Employee;
using AT.Api.Employee.Requests.Employee;
using AT.Api.Shared.Model.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.Employee
{
    [TestClass]
    public class SaveEmployeeRequestTests
    {
        [TestMethod]
        public void SaveEmployee_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployee_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployee_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployee_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmployee_ValidData_Saved()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var address1 = EntityHelper.Address();
            var info = EntityHelper.EmployeeInfo(address: address1);

            var employee = EntityHelper.Employee(employeeInfo: info);
            employee.UpdateAuditInfo(user, datetime);

            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("EmployeeNo01", response.Data);
            Assert.AreEqual("Employee saved successfully", response.Message);
            Assert.IsTrue(response.Success);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [TestMethod]
        public void SaveEmmployee_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();
            var employee = EntityHelper.Employee();
            var employeeService = new Mock<IEmployeeService>();

            employee.UpdateAuditInfo(user, datetime);

            employeeService
                .Setup(x => x.Update(
                    It.Is<Customers.Employee>(
                        emp =>
                            emp.EmployeeNumber == "EmployeeNo01" &&
                            emp.CustomerId == "CustomerId01" &&
                            emp.EmployerId == "EmployerId01" &&
                            emp.FirstName == "FirstName01" &&
                            emp.LastName == "LastName01" &&
                            emp.WorkCountry == "WorkCountry01" &&
                            emp.WorkState == "WorkState01" &&
                            emp.Info.Email == "email01@test.com" &&
                            emp.Info.AltEmail == "altEmail01@test.com" &&
                            emp.Info.Address.Address1 == "Address1Value01" &&
                            emp.Info.Address.Address2 == "Address2Value01" &&
                            emp.Info.Address.City == "City01" &&
                            emp.Info.Address.State == "State01" &&
                            emp.Info.Address.PostalCode == "PostalCode01" &&
                            emp.Info.Address.Country == "Country01" &&
                            emp.Info.AltAddress.Address1 == "Address1Value02" &&
                            emp.Info.AltAddress.Address2 == "Address2Value02" &&
                            emp.Info.AltAddress.City == "City02" &&
                            emp.Info.AltAddress.State == "State02" &&
                            emp.Info.AltAddress.PostalCode == "PostalCode02" &&
                            emp.Info.AltAddress.Country == "Country02" &&
                            emp.Info.WorkPhone == "555555" &&
                            emp.Info.HomePhone == "666666" &&
                            emp.Info.CellPhone == "777777" &&
                            emp.Info.AltPhone == "888888"
                            ),
                    null, null))
                .Returns(employee)
                .Verifiable();

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        [TestMethod]
        public void SaveEmployee_SaveError_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var user = EntityHelper.UserStub();

            var employeeService = EmployeeServiceStub(null);

            var request = new SaveEmployeeRequest(employeeService.Object);

            var parameter = new SaveEmployeeParameters
            {
                Employee = Model(),
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act            
            var requestException = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(requestException);
            Assert.AreEqual(HttpStatusCode.InternalServerError, requestException.StatusCode);
            Assert.AreEqual("Error saving employee", requestException.Message);
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>(MockBehavior.Loose);

            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, null))
                .Returns(employee);

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private SaveEmployeeModel Model()
        {
            var address = EntityHelper.Address();

            return new SaveEmployeeModel
            {
                Number = "EmployeeNo01",
                FirstName = "FirstName01",
                LastName = "LastName01",
                WorkCountry = "WorkCountry01",
                WorkState = "WorkState01",
                ServiceDate = null,
                Info = new EmployeeInfoModel
                {
                    Email = "email01@test.com",
                    AltEmail = "altEmail01@test.com",
                    Address = new AddressModel
                    {
                        Address1 = "Address1Value01",
                        Address2 = "Address2Value01",
                        City = "City01",
                        State = "State01",
                        PostalCode = "PostalCode01",
                        Country = "Country01"
                    },
                    AltAddress = new AddressModel
                    {
                        Address1 = "Address1Value02",
                        Address2 = "Address2Value02",
                        City = "City02",
                        State = "State02",
                        PostalCode = "PostalCode02",
                        Country = "Country02"
                    },
                    WorkPhone = "555555",
                    HomePhone = "666666",
                    CellPhone = "777777",
                    AltPhone = "888888"
                }
            };
        }
    }
}
