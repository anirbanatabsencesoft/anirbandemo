﻿using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.Employee;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.Employee
{
    [TestClass]
    public class DeleteEmployeeRequestTests
    {        
        [TestMethod]
        public void DeleteEmployee_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
               
        [TestMethod]
        public void DeleteEmployee_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployee_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployee_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployee_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployee_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            
            var employeeService = EmployeeServiceStub(employee);
            
            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmployee_EmployeeNotFound_ReturnsEmptyEmployee()
        {
            ////Arrange
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(null);

            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            ////Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            ////Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void DeleteEmmployee_CallServices_ServicesShouldBeCalled()
        {
            ////Arrange
            var employee = EntityHelper.Employee();
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var request = new DeleteEmployeeRequest(employeeService.Object);

            var parameter = new DeleteEmployeeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            ////Act
            var response = request.Handle(parameter);

            ////Assert
            employeeService.Verify();
        }
        
        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }
    }
}
