﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases.Contracts;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.EmployeeContact;
using AT.Api.Employee.Requests.EmployeeContact;
using AT.Api.Shared.Model.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeContact
{
    [TestClass]
    public class SaveEmployeeContactRequestTests
    {
        [TestMethod]
        public void SaveContact_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = string.Empty,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel(),
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = string.Empty
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new SaveEmployeeContactRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void SaveContact_InvalidContactTypeCode_ThrowsException()
        {
            //// Arrange
            var contactType = EntityHelper.ContactType();
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);
            var contactTypeService = ContactTypeServiceStub(new List<Customers.ContactType> { contactType });
            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel { ContactTypeCode = "code000" },
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "Cust01"
            };

            var request = new SaveEmployeeContactRequest(employeeService.Object, contactTypeService: contactTypeService.Object);
            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Invalid contact type code", exception.Message);
        }

        [TestMethod]
        public void SaveContact_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange       

            var employeeService = EmployeeServiceStub(null);

            var request = new SaveEmployeeContactRequest(employeeService.Object);

            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel { ContactTypeCode = "HR" },
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveContact_DeletedEmployee_ThrowsException()
        {
            //// Arrange       
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeService = EmployeeServiceStub(employee);

            var request = new SaveEmployeeContactRequest(employeeService.Object);

            var parameters = new SaveEmployeeContactParameters
            {
                EmployeeContact = new EmployeeContactModel { ContactTypeCode = "HR" },
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void SaveContact_ValidContact_ReturnsEmptyEmployeeContactId()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeContactModel = new EmployeeContactModel
            {
                ContactPosition = null,
                ContactTypeCode = "Code01",
                Contact = new ContactModel
                {
                    Title = "Title01",
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    MiddleName = string.Empty,
                    CompanyName = "Company01",
                    ContactPersonName = string.Empty,
                    DateOfBirth = new DateTime(1985, 01, 01),
                    Email = "Email01",
                    AltEmail = "AltEmail01",
                    HomePhone = "01",
                    CellPhone = "01",
                    WorkPhone = "02",
                    Address = new AddressModel
                    {
                        Address1 = "Address1Value01",
                        Address2 = "Address2Value01",
                        City = "City01",
                        Country = "State01",
                        PostalCode = "PostalCode01",
                        State = "Country01"
                    }
                },
                RelatedEmployeeNumber = string.Empty,
                MilitaryStatus = 0
            };

            var parameters = new SaveEmployeeContactParameters
            {
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeNumber = "EmployeeNo01",
                EmployeeContact = employeeContactModel
            };

            var contactType = EntityHelper.ContactType();
            var employeeContact = EntityHelper.EmployeeContact();

            var contactTypeService = ContactTypeServiceStub(new List<Customers.ContactType> { contactType });
            var employeeService = EmployeeServiceStub(employee, employeeContact);
            var caseService = CaseServieStub();

            var request = new SaveEmployeeContactRequest(employeeService.Object, caseService.Object, contactTypeService.Object);

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("Contact01", response.Data);
            Assert.AreEqual("Contact saved successfully", response.Message);
        }

        [TestMethod]
        public void SaveContact_UpdateExistingContact_ReturnsUpdatedContactId()
        {
            //// Arrange
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var employeeContact = EntityHelper.EmployeeContact();
            employeeContact = employeeContact.SetCustomer(customer).SetEmployer(employer);

            var employeeContactModel = new EmployeeContactModel
            {
                ContactId = "Contact01",
                ContactPosition = null,
                ContactTypeCode = "Code01",
                Contact = new ContactModel
                {
                    Title = "Title01",
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    MiddleName = string.Empty,
                    CompanyName = "Company01",
                    ContactPersonName = string.Empty,
                    DateOfBirth = new DateTime(1985, 01, 01),
                    Email = "Email01",
                    AltEmail = "AltEmail01",
                    HomePhone = "01",
                    CellPhone = "01",
                    WorkPhone = "02",
                    Address = new AddressModel
                    {
                        Address1 = "Address1Value01",
                        Address2 = "Address2Value01",
                        City = "City01",
                        Country = "State01",
                        PostalCode = "PostalCode01",
                        State = "Country01"
                    }
                },
                RelatedEmployeeNumber = string.Empty,
                MilitaryStatus = 0
            };

            var parameters = new SaveEmployeeContactParameters
            {
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeNumber = "EmployeeNo01",
                EmployeeContact = employeeContactModel
            };

            var contactType = EntityHelper.ContactType();
            var contactTypeService = ContactTypeServiceStub(new List<Customers.ContactType> { contactType });
            var employeeService = EmployeeServiceStub(employee, employeeContact);
            var caseService = CaseServieStub();

            var request = new SaveEmployeeContactRequest(employeeService.Object, caseService.Object, contactTypeService.Object);

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("Contact01", response.Data);
            Assert.AreEqual("Contact saved successfully", response.Message);
        }

        [TestMethod]
        public void SaveContact_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var employeeContactModel = new EmployeeContactModel
            {
                ContactPosition = null,
                ContactTypeCode = "Code01",
                Contact = new ContactModel
                {
                    Title = "Title01",
                    FirstName = "FirstName01",
                    LastName = "LastName01",
                    MiddleName = string.Empty,
                    CompanyName = "Company01",
                    ContactPersonName = string.Empty,
                    DateOfBirth = new DateTime(1985, 01, 01),
                    Email = "Email01",
                    AltEmail = "AltEmail01",
                    HomePhone = "01",
                    CellPhone = "01",
                    WorkPhone = "02",
                    Address = new AddressModel
                    {
                        Address1 = "Address1Value01",
                        Address2 = "Address2Value01",
                        City = "City01",
                        Country = "State01",
                        PostalCode = "PostalCode01",
                        State = "Country01"
                    }
                },
                RelatedEmployeeNumber = string.Empty,
                MilitaryStatus = 0
            };

            var parameters = new SaveEmployeeContactParameters
            {
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeNumber = "EmployeeNo01",
                EmployeeContact = employeeContactModel
            };

            var employeeContact = EntityHelper.EmployeeContact();
            var contactType = EntityHelper.ContactType();

            var employeeService = new Mock<IEmployeeService>();
            employeeService
                 .Setup(x => x.GetEmployeeByEmployeeNumber(
                     It.Is<string>(en => en == "EmployeeNo01"),
                     It.Is<string>(cid => cid == "CustomerId01"),
                     It.Is<string>(eid => eid == "EmployerId01")))
                 .Returns(employee)
                 .Verifiable();

            employeeService
               .Setup(x => x.SaveContact(It.IsAny<Customers.EmployeeContact>()))
               .Returns(employeeContact)
               .Verifiable();

            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.UpdateContactOnAssociatedCases(
                    It.Is<Customers.EmployeeContact>(ec => ec.ContactTypeCode == "HR"),false))
                .Verifiable();

            var ContactTypeService = new Mock<IContactTypeService>();
            ContactTypeService
               .Setup(x => x.GetContactTypes(
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01"),
                    It.Is<ContactTypeDesignationType[]>(ctd => ctd == null)))
                 .Returns(new List<Customers.ContactType> { contactType })
                .Verifiable();         

            var request = new SaveEmployeeContactRequest(employeeService.Object, caseService.Object,ContactTypeService.Object);

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            employeeService.Verify();
            caseService.Verify();
            ContactTypeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee,
            Customers.EmployeeContact employeeContact = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
               .Setup(x => x.SaveContact(It.IsAny<Customers.EmployeeContact>()))
               .Returns(employeeContact);

            employeeService
             .Setup(x => x.GetContact(It.IsAny<string>()))
             .Returns(employeeContact);

            return employeeService;
        }

        private Mock<ICaseService> CaseServieStub(Customers.EmployeeContact contact = null)
        {
            var caseService = new Mock<ICaseService>();
            caseService
                .Setup(x => x.UpdateContactOnAssociatedCases(It.IsAny<Customers.EmployeeContact>(),false));

            return caseService;
        }

        private Mock<IContactTypeService> ContactTypeServiceStub(List<Customers.ContactType> contactTypes = null)
        {
            var contactTypeService = new Mock<IContactTypeService>();
            contactTypeService
                .Setup(x => x.GetContactTypes(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ContactTypeDesignationType[]>()))
                .Returns(contactTypes);

            return contactTypeService;
        }

    }
}
