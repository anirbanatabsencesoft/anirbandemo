﻿using Customers = AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeContact;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Employee.Tests.Requests.EmployeeContact
{
    [TestClass]
    public class DeleteEmployeeContactRequestTests
    {
        [TestMethod]
        public void DeleteContact_EmptyContactId_ThrowsException()
        {
            //// Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = EmployeeServiceStub(employeeContact);

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = string.Empty
            };

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public void DeleteContact_NullContactId_ThrowsException()
        {
            //// Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = EmployeeServiceStub(employeeContact);

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = null
            };

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public void DeleteContact_NoMatchingContact_ReturnsEmptyContact()
        {
            //// Arrange       
           
            var employeeService = EmployeeServiceStub(null);

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = "contact001"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("No employee contacts found", exception.Message);
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        [TestMethod]
        public void DeleteContact_DeletedEmployeeContact_ReturnsEmptyContact()
        {
            //// Arrange       
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeContact = EntityHelper.EmployeeContact(employee: employee);
            var employeeService = EmployeeServiceStub(employeeContact);

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = "Contact01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("This employee is deleted", exception.Message);
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        [TestMethod]
        public void DeleteContact_ValidContactId_ReturnsContact()
        {
            //Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = "empContact01"
            };

            var employeeService = EmployeeServiceStub(employeeContact);

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            //Act

            var response = request.Handle(parameters);

            //Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual("empContact01", response.Data);
        }

        [TestMethod]
        public void DeleteContact_CallServices_ServicesShouldBeCalled()
        {
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetContact(It.Is<string>(en => en == "empContact01")))
                .Returns(employeeContact)
                .Verifiable();

            employeeService
               .Setup(x => x.DeleteContact(It.Is<string>(en => en == "empContact01")))
               .Verifiable();

            var parameters = new DeleteEmployeeContactParameters
            {
                ContactId = "empContact01"
            };

            var request = new DeleteEmployeeContactRequest(employeeService.Object);

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            employeeService.Verify();
        }
        private Mock<IEmployeeService> EmployeeServiceStub(Customers.EmployeeContact employeeContact)
        {

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetContact(It.IsAny<string>()))
                .Returns(employeeContact);

            employeeService
               .Setup(x => x.DeleteContact(It.IsAny<string>()));               

            return employeeService;
        }
    }
}
