﻿using Customers = AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeContact;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Employee.Tests.Requests.EmployeeContact
{
    [TestClass]
    public class ReadEmployeeContactRequestTests
    {
        [TestMethod]
        public void ReadContact_EmptyContactId_ThrowsException()
        {
            //// Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = EmployeeServiceStub(employeeContact);

            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = string.Empty
            };

            var request = new ReadEmployeeContactRequest(employeeService.Object);

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public void ReadContact_NullContactId_ThrowsException()
        {
            //// Arrange
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = EmployeeServiceStub(employeeContact);

            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = null
            };

            var request = new ReadEmployeeContactRequest(employeeService.Object);

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Contact Id is required", exception.Message);
        }

        [TestMethod]
        public void ReadContact_NoMatchingContact_ThrowsException()
        {
            //// Arrange       

            var employeeService = EmployeeServiceStub(null);

            var request = new ReadEmployeeContactRequest(employeeService.Object);

            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = "contact001"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("No contact found", exception.Message);
        }

        [TestMethod]
        public void ReadContact_DeletedEmployee_ThrowsException()
        {
            //// Arrange       
            var employee = EntityHelper.Employee(isDeleted: true);
            var employeeService = EmployeeServiceStub(null);

            var request = new ReadEmployeeContactRequest(employeeService.Object);

            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = "contact001"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => request.Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("No contact found", exception.Message);
        }

        [TestMethod]
        public void ReadContact_ValidContactId_ReturnsContact()
        {
            //// Arrange    
            
            var employeeContact = EntityHelper.EmployeeContact();
            var employeeService = EmployeeServiceStub(employeeContact);
           
            var request = new ReadEmployeeContactRequest(employeeService.Object);

            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = "Contact01"
            };

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);

            var result = response.Data;

            Assert.AreEqual("HR", result.ContactTypeCode);
            Assert.AreEqual(null, result.ContactPosition);
            Assert.AreEqual(0, result.MilitaryStatus);
            Assert.AreEqual(string.Empty, result.RelatedEmployeeNumber);
            Assert.AreEqual("Address1Value01", result.Contact.Address.Address1);
            Assert.AreEqual("Address2Value01", result.Contact.Address.Address2);
            Assert.AreEqual("City01", result.Contact.Address.City);
            Assert.AreEqual("Country01", result.Contact.Address.Country);
            Assert.AreEqual("PostalCode01", result.Contact.Address.PostalCode);
            Assert.AreEqual("State01", result.Contact.Address.State);
            Assert.AreEqual("AltEmail01", result.Contact.AltEmail);
            Assert.AreEqual("01", result.Contact.CellPhone);
            Assert.AreEqual("Company01", result.Contact.CompanyName);
            Assert.AreEqual("01-01-1985", result.Contact.DateOfBirth.Value.ToString("MM-dd-yyyy"));
            Assert.AreEqual("Email01", result.Contact.Email);
            Assert.AreEqual("FirstName01", result.Contact.FirstName);
            Assert.AreEqual("03", result.Contact.HomePhone);
            Assert.AreEqual("LastName01", result.Contact.LastName);
            Assert.AreEqual("Title01", result.Contact.Title);
            Assert.AreEqual("02", result.Contact.WorkPhone);
        }

        [TestMethod]
        public void ReadEmployeeContact_CallServices_ServicesShouldBeCalled()
        {
            var employeeContact = EntityHelper.EmployeeContact();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetContact(It.Is<string>(en => en == "Contact01")))
                .Returns(employeeContact)
                .Verifiable();
            
            var parameters = new ReadEmployeeContactParameters
            {
                ContactId = "Contact01"
            };

            var request = new ReadEmployeeContactRequest(employeeService.Object);

            //// Act
            var response = request.Handle(parameters);

            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.EmployeeContact employeeContact)
        {

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetContact(It.IsAny<string>()))
                .Returns(employeeContact);

        
            return employeeService;
        }
    }
}
