﻿using Customers = AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeContact;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AT.Api.Employee.Tests.Requests.EmployeeContact
{
    [TestClass]
    public class GetEmployeeContactsRequestTests
    {
        [TestMethod]
        public void GetContacts_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = string.Empty,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employee number is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = string.Empty,
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Employer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = null
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = string.Empty
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(
                () => new GetEmployeeContactsRequest().Handle(parameters));

            //// Assert
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
            Assert.AreEqual("Customer Id is required", exception.Message);
        }

        [TestMethod]
        public void GetContacts_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange     
            var employeeService = EmployeeServiceStub(null);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", exception.Message);
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        [TestMethod]
        public void GetContacts_DeletedEmployeeContact_ThrowsException()
        {
            //// Arrange       
            var employee = EntityHelper.Employee(isDeleted: true);            
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameters = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var exception = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameters));

            //// Assert
            Assert.AreEqual("This employee is deleted", exception.Message);
            Assert.IsNotNull(exception);
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        [TestMethod]
        public void GetContacts_ValidEmployee_ReturnsContacts()
        {
            //// Arrange
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var dob = new DateTime(1978, 01, 01);
            var address = EntityHelper.Address("AddressName02", "Address1Value02", "Address2Value02", "City02", "State02", "PostalCode02", "Country02");
            var contact = EntityHelper.Contact("Title02", "Company02", "012", "FirstName02", "LastName02", "021", "031", "AltEmail02", "Email02", dob, address);

            var employeeContact1 = EntityHelper.EmployeeContact(employee: employee);
            employeeContact1 = employeeContact1.SetCustomer(customer).SetEmployer(employer);

            var employeeContact2 = EntityHelper.EmployeeContact("Contact02", "EmployeeNo01", "Employee001", "RelatedEmp02", "SUPERVISOR", 1234, contact, employee);
            employeeContact2 = employeeContact2.SetCustomer(customer).SetEmployer(employer);

            var employeeContacts = new List<Customers.EmployeeContact>
            {
                employeeContact1, employeeContact2
            };

            var employeeService = EmployeeServiceStub(employee, employeeContacts);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameter = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("HR", result1.ContactTypeCode);            
            Assert.AreEqual(null, result1.ContactPosition);
            Assert.AreEqual(0, result1.MilitaryStatus);
            Assert.AreEqual(string.Empty, result1.RelatedEmployeeNumber);
            Assert.AreEqual("Address1Value01", result1.Contact.Address.Address1);
            Assert.AreEqual("Address2Value01", result1.Contact.Address.Address2);
            Assert.AreEqual("City01", result1.Contact.Address.City);
            Assert.AreEqual("Country01", result1.Contact.Address.Country);
            Assert.AreEqual("PostalCode01", result1.Contact.Address.PostalCode);
            Assert.AreEqual("State01", result1.Contact.Address.State);
            Assert.AreEqual("AltEmail01", result1.Contact.AltEmail);
            Assert.AreEqual("01", result1.Contact.CellPhone);
            Assert.AreEqual("Company01", result1.Contact.CompanyName);
            Assert.AreEqual("01-01-1985", result1.Contact.DateOfBirth.Value.ToString("MM-dd-yyyy"));
            Assert.AreEqual("Email01", result1.Contact.Email);
            Assert.AreEqual("FirstName01", result1.Contact.FirstName);
            Assert.AreEqual("03", result1.Contact.HomePhone);
            Assert.AreEqual("LastName01", result1.Contact.LastName);
            Assert.AreEqual("Title01", result1.Contact.Title);
            Assert.AreEqual("02", result1.Contact.WorkPhone);

            var result2 = response.Data.ElementAt(1);

            Assert.AreEqual("SUPERVISOR", result2.ContactTypeCode);            
            Assert.AreEqual(1234, result2.ContactPosition);
            Assert.AreEqual(0, result2.MilitaryStatus);
            Assert.AreEqual("RelatedEmp02", result2.RelatedEmployeeNumber);
            Assert.AreEqual("Address1Value02", result2.Contact.Address.Address1);
            Assert.AreEqual("Address2Value02", result2.Contact.Address.Address2);
            Assert.AreEqual("City02", result2.Contact.Address.City);
            Assert.AreEqual("Country02", result2.Contact.Address.Country);
            Assert.AreEqual("PostalCode02", result2.Contact.Address.PostalCode);
            Assert.AreEqual("State02", result2.Contact.Address.State);
            Assert.AreEqual("AltEmail02", result2.Contact.AltEmail);
            Assert.AreEqual("012", result2.Contact.CellPhone);
            Assert.AreEqual("Company02", result2.Contact.CompanyName);
            Assert.AreEqual("01-01-1978", result2.Contact.DateOfBirth.Value.ToString("MM-dd-yyyy"));
            Assert.AreEqual("Email02", result2.Contact.Email);
            Assert.AreEqual("FirstName02", result2.Contact.FirstName);
            Assert.AreEqual("031", result2.Contact.HomePhone);
            Assert.AreEqual("LastName02", result2.Contact.LastName);
            Assert.AreEqual("Title02", result2.Contact.Title);
            Assert.AreEqual("021", result2.Contact.WorkPhone);
        }

        [TestMethod]
        public void GetContacts_WithContactTypeCode_ReturnsContacts()
        {
            //// Arrange
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var dob = new DateTime(1978, 01, 01);
            var address = EntityHelper.Address("AddressName02", "Address1Value02", "Address2Value02", "City02", "State02", "PostalCode02", "Country02");
            var contact = EntityHelper.Contact("Title02", "Company02", "012", "FirstName02", "LastName02", "021", "031", "AltEmail02", "Email02", dob, address);

            var employeeContact1 = EntityHelper.EmployeeContact(employee: employee);
            employeeContact1 = employeeContact1.SetCustomer(customer).SetEmployer(employer);

            var employeeContact2 = EntityHelper.EmployeeContact("Contact02", "EmployeeNo01", "Employee001", "", "SUPERVISOR", 1234, contact, employee);
            employeeContact2 = employeeContact2.SetCustomer(customer).SetEmployer(employer);

            var employeeContacts = new List<Customers.EmployeeContact>
            {
                employeeContact2
            };

            var employeeService = EmployeeServiceStub(employee, employeeContacts);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameter = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                ContactTypeCode = "SUPERVISOR"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var result1 = response.Data.ElementAt(0);

            Assert.AreEqual("SUPERVISOR", result1.ContactTypeCode);            
            Assert.AreEqual(1234, result1.ContactPosition);
            Assert.AreEqual(0, result1.MilitaryStatus);
            Assert.AreEqual(string.Empty, result1.RelatedEmployeeNumber);
            Assert.AreEqual("Address1Value02", result1.Contact.Address.Address1);
            Assert.AreEqual("Address2Value02", result1.Contact.Address.Address2);
            Assert.AreEqual("City02", result1.Contact.Address.City);
            Assert.AreEqual("Country02", result1.Contact.Address.Country);
            Assert.AreEqual("PostalCode02", result1.Contact.Address.PostalCode);
            Assert.AreEqual("State02", result1.Contact.Address.State);
            Assert.AreEqual("AltEmail02", result1.Contact.AltEmail);
            Assert.AreEqual("012", result1.Contact.CellPhone);
            Assert.AreEqual("Company02", result1.Contact.CompanyName);
            Assert.AreEqual("01-01-1978", result1.Contact.DateOfBirth.Value.ToString("MM-dd-yyyy"));
            Assert.AreEqual("Email02", result1.Contact.Email);
            Assert.AreEqual("FirstName02", result1.Contact.FirstName);
            Assert.AreEqual("031", result1.Contact.HomePhone);
            Assert.AreEqual("LastName02", result1.Contact.LastName);
            Assert.AreEqual("Title02", result1.Contact.Title);
            Assert.AreEqual("021", result1.Contact.WorkPhone);
        }

        [TestMethod]
        public void GetContacts_EmptyContacts_ReturnsEmptyContacts()
        {
            //// Arrange
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var dob = new DateTime(1978, 01, 01);
            var address = EntityHelper.Address("AddressName02", "Address1Value02", "Address2Value02", "City02", "State02", "PostalCode02", "Country02");
            var contact = EntityHelper.Contact("Title02", "Company02", "012", "FirstName02", "LastName02", "021", "031", "AltEmail02", "Email02", dob, address);

            var employeeContact1 = EntityHelper.EmployeeContact(employee: employee);
            employeeContact1 = employeeContact1.SetCustomer(customer).SetEmployer(employer);

            var employeeContact2 = EntityHelper.EmployeeContact("Contact02", "EmployeeNo01", "Employee001", "", "SUPERVISOR", 1234, contact, employee);
            employeeContact2 = employeeContact2.SetCustomer(customer).SetEmployer(employer);

            var employeeContacts = new List<Customers.EmployeeContact>();

            var employeeService = EmployeeServiceStub(employee, employeeContacts);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameter = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No contact found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetContacts_NoContacts_ReturnsEmptyContacts()
        {
            //// Arrange
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var dob = new DateTime(1978, 01, 01);
            var address = EntityHelper.Address("AddressName02", "Address1Value02", "Address2Value02", "City02", "State02", "PostalCode02", "Country02");
            var contact = EntityHelper.Contact("Title02", "Company02", "012", "FirstName02", "LastName02", "021", "031", "AltEmail02", "Email02", dob, address);

            var employeeContact1 = EntityHelper.EmployeeContact(employee: employee);
            employeeContact1 = employeeContact1.SetCustomer(customer).SetEmployer(employer);

            var employeeContact2 = EntityHelper.EmployeeContact("Contact02", "EmployeeNo01", "Employee001", "", "SUPERVISOR", 1234, contact, employee);
            employeeContact2 = employeeContact2.SetCustomer(customer).SetEmployer(employer);
            

            var employeeService = EmployeeServiceStub(employee);

            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameter = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No contact found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetContacts_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var employee = EntityHelper.Employee();
            employee = employee.SetCustomer(customer).SetEmployer(employer);

            var employeeContact = EntityHelper.EmployeeContact();
            var contacts = new List<Customers.EmployeeContact>();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            employeeService
               .Setup(x => x.GetContacts(
                   It.Is<string>(ei => ei == "EmployeeId01"),
                   It.Is<string>(type => type == null)))
               .Returns(contacts)
               .Verifiable();


            var request = new GetEmployeeContactsRequest(employeeService.Object);

            var parameter = new GetEmployeeContactsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };


            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee, List<Customers.EmployeeContact> employeeContacts = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
                .Setup(x => x.GetContacts(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employeeContacts);


            return employeeService;
        }
    }
}
