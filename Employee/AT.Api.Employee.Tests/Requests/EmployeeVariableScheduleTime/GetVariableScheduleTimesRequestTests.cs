﻿using AbsenceSoft;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeVariableScheduleTime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeVariableScheduleTime
{
    [TestClass]
    public class GetVariableScheduleTimesRequestTests
    {
        [TestMethod]
        public void GetVariableSchedule_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);
            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_NullEmployerId_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_EmptyEmployerId_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_EmptyCustomerId_ThrowsException()
        {
            //// Arrange           
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange                       
            var employeeService = EmployeeServiceStub();

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetVariableSchedule_ValidEmployee_ReturnsVariableSchedules()
        {
            //// Arrange                       
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);
            var time2 = EntityHelper.Time(totalMinutes: 12, sampleDate: sampleDate2);
            var employee = EntityHelper.Employee();

            var variableSchedule1 = EntityHelper.VariableScheduleTime(employee, time1);
            variableSchedule1.SetEmployer(employer).SetCustomer(customer);

            var variableSchedule2 = EntityHelper.VariableScheduleTime(employee, time2);
            variableSchedule2.SetEmployer(employer).SetCustomer(customer);

            var employeeService = EmployeeServiceStub(employee, new List<Customers.VariableScheduleTime> { variableSchedule1, variableSchedule2 });

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(2, response.Data.Count());

            var result = response.Data.ToList();
            Assert.IsTrue(result[0].SampleDate.IsSameTimeAs(sampleDate1));
            Assert.AreEqual(10, result[0].TotalMinutes);

            Assert.IsTrue(result[1].SampleDate.IsSameTimeAs(sampleDate2));
            Assert.AreEqual(12, result[1].TotalMinutes);
        }


        [TestMethod]
        public void GetVariableSchedule_FilterByStartDate_ReturnsVariableSchedules()
        {
            //// Arrange                       
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);
            var time2 = EntityHelper.Time(totalMinutes: 12, sampleDate: sampleDate2);
            var employee = EntityHelper.Employee();

            var variableSchedule1 = EntityHelper.VariableScheduleTime(employee, time1);
            variableSchedule1.SetEmployer(employer).SetCustomer(customer);

            var variableSchedule2 = EntityHelper.VariableScheduleTime(employee, time2);
            variableSchedule2.SetEmployer(employer).SetCustomer(customer);

            var employeeService = EmployeeServiceStub(employee, new List<Customers.VariableScheduleTime> { variableSchedule1, variableSchedule2 });

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
                StartDate = new DateTime(2018, 10, 12)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(1, response.Data.Count());

            var result = response.Data.ToList();
            Assert.IsTrue(result[0].SampleDate.IsSameTimeAs(sampleDate2));
            Assert.AreEqual(12, result[0].TotalMinutes);
        }


        [TestMethod]
        public void GetVariableSchedule_FilterByEndDate_ReturnsVariableSchedules()
        {
            //// Arrange                       
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);
            var time2 = EntityHelper.Time(totalMinutes: 12, sampleDate: sampleDate2);
            var employee = EntityHelper.Employee();

            var variableSchedule1 = EntityHelper.VariableScheduleTime(employee, time1);
            variableSchedule1.SetEmployer(employer).SetCustomer(customer);

            var variableSchedule2 = EntityHelper.VariableScheduleTime(employee, time2);
            variableSchedule2.SetEmployer(employer).SetCustomer(customer);

            var employeeService = EmployeeServiceStub(employee, new List<Customers.VariableScheduleTime> { variableSchedule1, variableSchedule2 });

            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
                EndDate = new DateTime(2018, 10, 12)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(1, response.Data.Count());

            var result = response.Data.ToList();
            Assert.IsTrue(result[0].SampleDate.IsSameTimeAs(sampleDate1));
            Assert.AreEqual(10, result[0].TotalMinutes);
        }


        [TestMethod]
        public void GetVariableSchedule_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);
            var time2 = EntityHelper.Time(totalMinutes: 12, sampleDate: sampleDate2);
            var employee = EntityHelper.Employee();

            var variableSchedule1 = EntityHelper.VariableScheduleTime(employee, time1);
            variableSchedule1.SetEmployer(employer).SetCustomer(customer);

            var variableSchedule2 = EntityHelper.VariableScheduleTime(employee, time2);
            variableSchedule2.SetEmployer(employer).SetCustomer(customer);

            var variableSchedules = new List<Customers.VariableScheduleTime> { variableSchedule1, variableSchedule2 };

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            employeeService
                .Setup(x => x.GetVariableScheduleTime(
                    It.Is<string>(eid => eid == employee.Id)))
                .Returns(variableSchedules)
                .Verifiable();



            var request = new GetVariableScheduleTimesRequest(employeeService.Object);

            var parameter = new GetVariableScheduleTimesParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EndDate = new DateTime(2018, 10, 12)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }



        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee = null, List<Customers.VariableScheduleTime> variableScheduleTimes = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
                .Setup(x => x.GetVariableScheduleTime(It.IsAny<string>()))
                .Returns(variableScheduleTimes);

            return employeeService;
        }

    }
}
