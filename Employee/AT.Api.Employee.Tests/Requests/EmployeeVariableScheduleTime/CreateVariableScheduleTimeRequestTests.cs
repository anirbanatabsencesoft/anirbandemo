﻿using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.EmployeeVariableScheduleTime;
using AT.Api.Employee.Requests.EmployeeVariableScheduleTime;
using AT.Api.Shared.Model.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeVariableScheduleTime
{
    [TestClass]
    public class CreateVariableScheduleTimeRequestTests
    {
        [TestMethod]
        public void CreateVariableSchedule_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange            
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);
            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_NullEmployerId_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_EmptyEmployerId_ThrowsException()
        {
            //// Arrange

            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_EmptyCustomerId_ThrowsException()
        {
            //// Arrange           
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_NullActualTimes_ThrowsException()
        {
            //// Arrange           
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
                EmployeeVariableScheduleTime = new EmployeeVariableScheduleTimeModel
                {
                    ActualTimes = null
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Variable schedule times can not be null", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange                       
            var employeeService = EmployeeServiceStub();

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = "CustomerId01",
                EmployeeVariableScheduleTime = new EmployeeVariableScheduleTimeModel
                {
                    ActualTimes = new List<BaseTimeModel>()
                }
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateVariableSchedule_ErrorUpdatingEmployeeSchedule_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, It.IsAny<List<Customers.VariableScheduleTime>>()))
                .Returns<Customers.Employee>(null);

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeVariableScheduleTime = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Error creating variable schedule for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode); ;
        }

        [TestMethod]
        public void CreateVariableSchedule_ValidEmployee_CreatesVariableSchedule()
        {
            //// Arrange
            var employee = EntityHelper.Employee();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);

            var variableSchedule = EntityHelper.VariableScheduleTime(employee, time);

            var variableSchedules = new List<Customers.VariableScheduleTime> { variableSchedule };

            var employeeService = EmployeeServiceStub(employee, variableSchedules);

            var times = new List<BaseTimeModel>
            {
                new BaseTimeModel
                {
                    IsHoliday = true,
                    SampleDate = new DateTime(2018, 10, 13),
                    TotalMinutes = 21
                }
            };

            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeVariableScheduleTime = Model(times: times)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Variable schedule time created successfully", response.Message);
            Assert.IsTrue(response.Success);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var customer = EntityHelper.Customer();
            var employer = EntityHelper.Employer();

            var sampleDate1 = new DateTime(2018, 10, 11);
            var sampleDate2 = new DateTime(2018, 10, 13);

            var time = EntityHelper.Time(totalMinutes: 10, sampleDate: sampleDate1);
            var employee = EntityHelper.Employee();

            var variableSchedule = EntityHelper.VariableScheduleTime(employee, time);
            variableSchedule.SetEmployer(employer).SetCustomer(customer);

            var variableSchedules = new List<Customers.VariableScheduleTime> { variableSchedule };

            var times = new List<BaseTimeModel>
            {
                new BaseTimeModel
                {
                    IsHoliday = false,
                    SampleDate = sampleDate1,
                    TotalMinutes = 10
                },
                new BaseTimeModel
                {
                    IsHoliday = true,
                    SampleDate = sampleDate2,
                    TotalMinutes = 21
                }
            };

            var model = Model(times: times);

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            employeeService
                .Setup(x => x.GetVariableScheduleTime(
                    It.Is<string>(eid => eid == employee.Id)))
                .Returns(variableSchedules)
                .Verifiable();

            employeeService
                .Setup(x => x.Update(
                    It.Is<Customers.Employee>(emp => emp.Id == employee.Id),
                    null,
                    It.Is<List<Customers.VariableScheduleTime>>(
                        vst => vst.Count == 2 &&
                              vst[0].Time.SampleDate == sampleDate1 &&
                              vst[0].Time.TotalMinutes == 10 &&
                              vst[1].Time.SampleDate == sampleDate2 &&
                              vst[1].Time.TotalMinutes == 21)))
                .Returns(employee)
                .Verifiable();


            var request = new CreateVariableScheduleTimeRequest(employeeService.Object);

            var parameter = new CreateVariableScheduleTimeParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                EmployeeVariableScheduleTime = Model(times)
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }


        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee = null, List<Customers.VariableScheduleTime> variableScheduleTimes = null)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
                .Setup(x => x.GetVariableScheduleTime(It.IsAny<string>()))
                .Returns(variableScheduleTimes);


            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, It.IsAny<List<Customers.VariableScheduleTime>>()))
                .Returns(employee);

            return employeeService;
        }

        private EmployeeVariableScheduleTimeModel Model(
            List<BaseTimeModel> times = null)
        {
            return new EmployeeVariableScheduleTimeModel
            {
                ActualTimes = times ?? new List<BaseTimeModel>()
            };
        }

        private BaseTimeModel BaseTimeModel(
            DateTime? sampleDate = null,
            int totalMinutes = 0,
            bool isHoliday = false)
        {
            return new BaseTimeModel
            {
                IsHoliday = isHoliday,
                SampleDate = sampleDate ?? new DateTime(2000, 12, 10),
                TotalMinutes = totalMinutes
            };
        }
    }
}
