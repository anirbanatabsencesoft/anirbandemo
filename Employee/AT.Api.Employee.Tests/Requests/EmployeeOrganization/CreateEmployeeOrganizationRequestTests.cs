﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Models.EmployeeOrganization;
using AT.Api.Employee.Requests.EmployeeOrganization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeOrganization
{
    [TestClass]
    public class CreateEmployeeOrganizationRequestTests
    {
        [TestMethod]
        public void CreateOrganizations_UserIdEmpty_ThrowsException()
        {
            //// Act
            var requestException = Assert.ThrowsException<ApiException>(() =>
                new CreateEmployeeOrganizationRequest(
                    " ",
                    null));

            //// Assert
            Assert.IsNotNull(requestException);
            Assert.AreEqual(HttpStatusCode.Unauthorized, requestException.StatusCode);
            Assert.AreEqual("Unable to retrieve authorized user detail", requestException.Message);
        }

        [TestMethod]
        public void CreateOrganizations_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = new Mock<IAdminUserService>();


            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new CreateEmployeeOrganizationRequest(
                    "UserKey",
                    adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void CreateOrganizations_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();
            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        public void CreateOrganizations_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var employeeOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(employeeOrganization, employeeOrganization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_ValidData_Saved()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType = EntityHelper.OrganizationType();
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();

            var address1 = EntityHelper.Address();

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType,
                path: hierarchyPath,
                address: address1);

            organization1.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(employee: employee, organization: organization1);

            employeeOrganization1.UpdateAuditInfo(user, datetime);

            var organizationService = OrganizationServiceStub(employeeOrganization1, employeeOrganization1, organizationType, organization1);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("OrgCode01", response.Data);
            Assert.AreEqual("Employee organization saved", response.Message);
            Assert.IsTrue(response.Success);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }


        [TestMethod]        
        public void CreateOrganizations_NoOrganization_Saved()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType = EntityHelper.OrganizationType();
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();
            var address1 = EntityHelper.Address();

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType,
                path: hierarchyPath,
                address: address1);

            organization1.SetCustomer(customer1).SetEmployer(employer);
            employee.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(employee: employee, organization: organization1);

            employeeOrganization1.UpdateAuditInfo(user, datetime);
            var results = EntityHelper.OrgnizationTypeListResults(new string[] { organizationType.Code });

            var organizationService = OrganizationServiceStub(null, employeeOrganization1, organizationType, organization1);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("OrgCode01", response.Data);
            Assert.AreEqual("Employee organization saved", response.Message);
            Assert.IsTrue(response.Success);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        [TestMethod]
        public void CreateOrganizations_NoMatchingEmployee_ReturnsEmptyOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var organization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(organization, organization);
            var employeeService = EmployeeServiceStub(null);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_DeletedEmployee_ReturnsEmptyOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee(isDeleted: true);
            var user = EntityHelper.UserStub();
            var organization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(organization, organization);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_InvalidTypeCode_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var organization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(organization, organization, null, null);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No orgnization type code found", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void CreateOrganizations_NoParentOrgnizationFound_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            var address = EntityHelper.Address();
            var organizationType = EntityHelper.OrganizationType();
            var organization = EntityHelper.Organization(address: address, organizationType: organizationType);
            var empOrganization = EntityHelper.EmployeeOrganization();

            var organizationService = OrganizationServiceStub(empOrganization, empOrganization, organizationType, null);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("No parent orgnization found", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }
        [TestMethod]
        public void CreateOrganizations_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var address = EntityHelper.Address();
            var employee = EntityHelper.Employee();
            var organizationType = EntityHelper.OrganizationType();
            var organization = EntityHelper.Organization(address: address, organizationType: organizationType);
            var user = EntityHelper.UserStub();
            var employer = EntityHelper.Employer();
            var customer = EntityHelper.Customer();
            var employeeOrganization = EntityHelper.EmployeeOrganization(
                employee: employee,
                organization: organization);

            employee.SetCustomer(customer).SetEmployer(employer);

            var results = EntityHelper.OrgnizationTypeListResults(new string[] { organizationType.Code });

            var employeeService = new Mock<IEmployeeService>();
            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();


            var organizationService = new Mock<IOrganizationService>();
            organizationService
             .Setup(x => x.GetOrganizationType(
                   It.Is<string>(eid => eid == "EmployerId01"),
                   It.Is<string>(typeCode => typeCode == "OrgTypeCode01")))
              .Returns(organizationType)
              .Verifiable();

            organizationService
           .Setup(x => x.GetOrganizations(
                 It.Is<string>(eid => eid == "EmployerId01"),
                 It.Is<string>(cid => cid == "CustomerId01"),
                 It.Is<string>(code => code == "OrgCode01"),
                 It.Is<bool>(i => i == false)))
            .Returns(new List<Organization> { organization })
            .Verifiable();

            organizationService
           .Setup(x => x.GetOrganizations(
                 It.Is<string>(eid => eid == "EmployerId01"),
                 It.Is<string>(cid => cid == "CustomerId01"),
                 It.Is<string>(code => code == "ParentOrgCode01"),
                 It.Is<bool>(i => i == false)))
            .Returns(new List<Organization> { organization })
            .Verifiable();

            organizationService
                .Setup(x => x.GetEmployeeOrganizationByCode(
                    It.Is<Customers.Employee>(
                        emp =>
                            emp.CustomerId == "CustomerId01" &&
                            emp.EmployeeNumber == "EmployeeNo01" &&
                            emp.EmployerId == "EmployerId01"),
                    "OrgCode01"))
                .Returns(employeeOrganization)
                .Verifiable();

            organizationService
                .Setup(x => x.UpdateOrganization(
                    It.Is<Customers.EmployeeOrganization>(
                        emp =>
                            emp.Id == "000000000000000000000001" &&
                            emp.Organization.Code == "OrgCode01")))
                 .Returns(employeeOrganization)
                 .Verifiable();


            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
            organizationService.Verify();
            adminService.Verify();
        }

        [TestMethod]
        public void CreateOrganizations_SaveError_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType = EntityHelper.OrganizationType();
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();
            var address1 = EntityHelper.Address();

            employee.SetCustomer(customer1).SetEmployer(employer);

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType,
                path: hierarchyPath,
                address: address1);

            organization1.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(
                employee: employee,
                organization: organization1);

            employeeOrganization1.UpdateAuditInfo(user, datetime);

            var results = EntityHelper.OrgnizationTypeListResults(new string[] { organizationType.Code });
            var organizationService = OrganizationServiceStub(employeeOrganization1, null, organizationType, organization1);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new CreateEmployeeOrganizationRequest(
                "UserKey",
                adminService.Object,
                employeeService.Object,
                organizationService.Object);

            var parameter = new CreateEmployeeOrganizationParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                EmployeeOrganization = Model()
            };

            //// Act
            var requestException = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(requestException);
            Assert.AreEqual(HttpStatusCode.InternalServerError, requestException.StatusCode);
            Assert.AreEqual("Error saving employee organization", requestException.Message);
        }

        private Mock<IOrganizationService> OrganizationServiceStub(
            Customers.EmployeeOrganization employeeOrganization,
            Customers.EmployeeOrganization savedEmployeeOrganization,
            OrganizationType orgnizationType = null,
            Organization organization = null)
        {
            var organizationService = new Mock<IOrganizationService>(MockBehavior.Loose);

            organizationService
              .Setup(x => x.GetOrganizationType(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(orgnizationType);

            organizationService
              .Setup(x => x.GetOrganizations(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new List<Organization> { organization });

            organizationService
                .Setup(x => x.GetEmployeeOrganizationByCode(It.IsAny<Customers.Employee>(), It.IsAny<string>()))
                .Returns(employeeOrganization);

            organizationService
                .Setup(x => x.UpdateOrganization(It.IsAny<Customers.EmployeeOrganization>()))
                .Returns(savedEmployeeOrganization);

            organizationService
              .Setup(x => x.SaveOrgnization(It.IsAny<Customers.Organization>()))
              .Returns(organization);

            return organizationService;
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }

        private CreateEmployeeOrganizationModel Model()
        {
            return new CreateEmployeeOrganizationModel
            {
                ParentOrgCode = "ParentOrgCode01",
                Address1 = "Address1Value01",
                Address2 = "Address2Value01",
                City = "City01",
                Code = "OrgCode01",
                CountryCode = "CountryCode01",
                Description = "Description01",
                NaicsCode = "NAICS1",
                SicCode = "SIC1",
                Name = "Test Employee Org 01",
                PostalCode = "PostCode01",
                State = "State01",
                TypeCode = "OrgTypeCode01"
            };
        }
    }
}
