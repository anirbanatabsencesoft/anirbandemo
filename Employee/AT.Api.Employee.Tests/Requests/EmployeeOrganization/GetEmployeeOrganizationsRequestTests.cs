﻿using AbsenceSoft;
using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeOrganization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeOrganization
{
    [TestClass]
    public class GetEmployeeOrganizationsRequestTests
    {
        [TestMethod]
        public void GetOrganizations_UserNotFound_ThrowsException()
        {
            //// Arrange
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns((User)null);

            //// Act
            var request = Assert.ThrowsException<ApiException>(() =>
                new GetEmployeeOrganizationsRequest(
                    "UserKey",
                    adminService.Object));

            //// Assert
            Assert.IsNotNull(request);
            Assert.AreEqual(HttpStatusCode.InternalServerError, request.StatusCode);
            Assert.AreEqual("Unable to find user", request.Message);
        }

        [TestMethod]
        public void GetOrganizations_ValidEmployee_ReturnsOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType = EntityHelper.OrganizationType();
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();

            var address1 = EntityHelper.Address();

            var address2 = EntityHelper.Address(
                "AddressName02",
                "Address1Value02",
                "Address2Value02",
                "City02",
                "State02",
                "PostalCode02",
                "Country02");

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType,
                path: hierarchyPath,
                address: address1);

            var organization2 = EntityHelper.Organization(
                "OrgCode02",
                "OrgName02",
                "OrgDescription02",
                "SicCode02",
                "NaicsCode02",
                0,
                organizationType,
                hierarchyPath,
                address2);

            organization1.SetCustomer(customer1).SetEmployer(employer);
            organization2.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(employee: employee, organization: organization1);
            var employeeOrganization2 = EntityHelper.EmployeeOrganization("EmpNo02", employee: employee, organization: organization2);

            employeeOrganization1.UpdateAuditInfo(user, datetime);
            employeeOrganization2.UpdateAuditInfo(user, datetime);

            var employeeOrganizations = new List<Customers.EmployeeOrganization>
            {
                employeeOrganization1, employeeOrganization2
            };

            var organizationService = OrganizationServiceStub(employeeOrganizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(2, response.Data.Count());

            var orgResult1 = response.Data.ElementAt(0);
            var orgResult2 = response.Data.ElementAt(1);

            Assert.AreEqual("OrgCode01", orgResult1.Code);
            Assert.AreEqual("OrgName01", orgResult1.Name);
            Assert.AreEqual("OrgDescription01", orgResult1.Description);
            Assert.AreEqual("OrgTypeCode01", orgResult1.TypeCode);
            Assert.AreEqual("EmployeeNo01", orgResult1.EmployeeNumber);
            Assert.AreEqual("SicCode01", orgResult1.SicCode);
            Assert.AreEqual("NaicsCode01", orgResult1.NaicsCode);
            Assert.AreEqual("Address1Value01", orgResult1.Address1);
            Assert.AreEqual("Address2Value01", orgResult1.Address2);
            Assert.AreEqual("Country01", orgResult1.CountryCode);
            Assert.AreEqual("State01", orgResult1.State);
            Assert.AreEqual("PostalCode01", orgResult1.PostalCode);
            Assert.AreEqual("City01", orgResult1.City);
            Assert.IsTrue(orgResult1.CreatedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.CreatedBy);
            Assert.IsTrue(orgResult1.ModifiedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.ModifiedBy);

            Assert.AreEqual("OrgCode02", orgResult2.Code);
            Assert.AreEqual("OrgName02", orgResult2.Name);
            Assert.AreEqual("OrgDescription02", orgResult2.Description);
            Assert.AreEqual("OrgTypeCode01", orgResult2.TypeCode);
            Assert.AreEqual("EmployeeNo01", orgResult2.EmployeeNumber);
            Assert.AreEqual("SicCode02", orgResult2.SicCode);
            Assert.AreEqual("NaicsCode02", orgResult2.NaicsCode);
            Assert.AreEqual("Address1Value02", orgResult2.Address1);
            Assert.AreEqual("Address2Value02", orgResult2.Address2);
            Assert.AreEqual("Country02", orgResult2.CountryCode);
            Assert.AreEqual("State02", orgResult2.State);
            Assert.AreEqual("PostalCode02", orgResult2.PostalCode);
            Assert.AreEqual("City02", orgResult2.City);
            Assert.IsTrue(orgResult2.CreatedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult2.CreatedBy);
            Assert.IsTrue(orgResult2.ModifiedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult2.ModifiedBy);
        }

        [TestMethod]
        public void GetOrganizations_ByTypeCode_ReturnsOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType1 = EntityHelper.OrganizationType();
            var organizationType2 = EntityHelper.OrganizationType(code: "OrgTypeCode02");
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();

            var address1 = EntityHelper.Address();

            var address2 = EntityHelper.Address(
                "AddressName02",
                "Address1Value02",
                "Address2Value02",
                "City02",
                "State02",
                "PostalCode02",
                "Country02");

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType1,
                path: hierarchyPath,
                address: address1);

            var organization2 = EntityHelper.Organization(
                "OrgCode02",
                "OrgName02",
                "OrgDescription02",
                "SicCode02",
                "NaicsCode02",
                0,
                organizationType2,
                hierarchyPath,
                address2);

            organization1.SetCustomer(customer1).SetEmployer(employer);
            organization2.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(employee: employee, organization: organization1, organizationType: organizationType1);
            var employeeOrganization2 = EntityHelper.EmployeeOrganization("EmpNo02", employee: employee, organization: organization2, organizationType: organizationType2);

            employeeOrganization1.UpdateAuditInfo(user, datetime);
            employeeOrganization2.UpdateAuditInfo(user, datetime);

            var employeeOrganizations = new List<Customers.EmployeeOrganization>
            {
                employeeOrganization1, employeeOrganization2
            };

            var organizationService = OrganizationServiceStub(employeeOrganizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                TypeCode = "OrgTypeCode01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var orgResult1 = response.Data.ElementAt(0);

            Assert.AreEqual("OrgCode01", orgResult1.Code);
            Assert.AreEqual("OrgName01", orgResult1.Name);
            Assert.AreEqual("OrgDescription01", orgResult1.Description);
            Assert.AreEqual("OrgTypeCode01", orgResult1.TypeCode);
            Assert.AreEqual("EmployeeNo01", orgResult1.EmployeeNumber);
            Assert.AreEqual("SicCode01", orgResult1.SicCode);
            Assert.AreEqual("NaicsCode01", orgResult1.NaicsCode);
            Assert.AreEqual("Address1Value01", orgResult1.Address1);
            Assert.AreEqual("Address2Value01", orgResult1.Address2);
            Assert.AreEqual("Country01", orgResult1.CountryCode);
            Assert.AreEqual("State01", orgResult1.State);
            Assert.AreEqual("PostalCode01", orgResult1.PostalCode);
            Assert.AreEqual("City01", orgResult1.City);
            Assert.IsTrue(orgResult1.CreatedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.CreatedBy);
            Assert.IsTrue(orgResult1.ModifiedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.ModifiedBy);
        }

        [TestMethod]
        public void GetOrganizations_ByInactiveOrgnization_ReturnsOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var organizationType1 = EntityHelper.OrganizationType();
            var organizationType2 = EntityHelper.OrganizationType(code: "OrgTypeCode02");
            var hierarchyPath = HierarchyPath.Parse("/OrgCode01/OrgCode02/");
            var employer = EntityHelper.Employer();
            var customer1 = EntityHelper.Customer();
            var user = EntityHelper.UserStub();

            var address1 = EntityHelper.Address();

            var address2 = EntityHelper.Address(
                "AddressName02",
                "Address1Value02",
                "Address2Value02",
                "City02",
                "State02",
                "PostalCode02",
                "Country02");

            var organization1 = EntityHelper.Organization(
                organizationType: organizationType1,
                path: hierarchyPath,
                address: address1,
                isDeleted: true);

            var organization2 = EntityHelper.Organization(
                "OrgCode02",
                "OrgName02",
                "OrgDescription02",
                "SicCode02",
                "NaicsCode02",
                0,
                organizationType2,
                hierarchyPath,
                address2);

            organization1.SetCustomer(customer1).SetEmployer(employer);
            organization2.SetCustomer(customer1).SetEmployer(employer);

            var employeeOrganization1 = EntityHelper.EmployeeOrganization(employee: employee, organization: organization1, organizationType: organizationType1);
            var employeeOrganization2 = EntityHelper.EmployeeOrganization("EmpNo02", employee: employee, organization: organization2, organizationType: organizationType2);

            employeeOrganization1.UpdateAuditInfo(user, datetime);
            employeeOrganization2.UpdateAuditInfo(user, datetime);

            var employeeOrganizations = new List<Customers.EmployeeOrganization>
            {
                employeeOrganization1, employeeOrganization2
            };

            var organizationService = OrganizationServiceStub(employeeOrganizations, new List<Customers.Organization> { organization1 });
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                Inactive = true
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Data.Count());

            var orgResult1 = response.Data.ElementAt(0);

            Assert.AreEqual("OrgCode01", orgResult1.Code);
            Assert.AreEqual("OrgName01", orgResult1.Name);
            Assert.AreEqual("OrgDescription01", orgResult1.Description);
            Assert.AreEqual("OrgTypeCode01", orgResult1.TypeCode);
            Assert.AreEqual("EmployeeNo01", orgResult1.EmployeeNumber);
            Assert.AreEqual("SicCode01", orgResult1.SicCode);
            Assert.AreEqual("NaicsCode01", orgResult1.NaicsCode);
            Assert.AreEqual("Address1Value01", orgResult1.Address1);
            Assert.AreEqual("Address2Value01", orgResult1.Address2);
            Assert.AreEqual("Country01", orgResult1.CountryCode);
            Assert.AreEqual("State01", orgResult1.State);
            Assert.AreEqual("PostalCode01", orgResult1.PostalCode);
            Assert.AreEqual("City01", orgResult1.City);
            Assert.IsTrue(orgResult1.CreatedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.CreatedBy);
            Assert.IsTrue(orgResult1.ModifiedOn.IsSameTimeAs(datetime));
            Assert.AreEqual("User Name", orgResult1.ModifiedBy);
        }

        [TestMethod]
        public void GetOrganizations_NoOrganizations_ReturnsOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Data.Count());
            Assert.AreEqual("No organizations found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_NoMatchingEmployee_ReturnsEmptyOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(null);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_DeletedEmployee_ReturnsEmptyOrganizations()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee(isDeleted: true);
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var user = EntityHelper.UserStub();
            List<Customers.EmployeeOrganization> organizations = null;

            var organizationService = OrganizationServiceStub(organizations);
            var employeeService = EmployeeServiceStub(employee);
            var adminService = AdminServiceStub(user);

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetOrganizations_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var employee = EntityHelper.Employee();
            var savedOrganization1 = EntityHelper.EmployeeOrganization();
            var organizations = new List<Customers.EmployeeOrganization>();
            var user = EntityHelper.UserStub();

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            var organizationService = new Mock<IOrganizationService>();

            organizationService
                .Setup(x => x.GetEmployeeOrganizationsList(It.Is<Customers.Employee>(
                        emp =>
                            emp.CustomerId == "CustomerId01" &&
                            emp.EmployeeNumber == "EmployeeNo01" &&
                            emp.EmployerId == "EmployerId01")))
                .Returns(organizations)
                .Verifiable();

            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.Is<string>(val => val == "UserKey")))
                .Returns(user)
                .Verifiable();

            var request = new GetEmployeeOrganizationsRequest(
                "UserKey",
                adminService.Object,
                organizationService.Object,
                employeeService.Object);

            var parameter = new GetEmployeeOrganizationsParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
            organizationService.Verify();
            adminService.Verify();
        }

        private Mock<IOrganizationService> OrganizationServiceStub(List<Customers.EmployeeOrganization> employeeOrganizations,
            List<Customers.Organization> orgnizations = null)
        {
            var organizationService = new Mock<IOrganizationService>(MockBehavior.Loose);

            organizationService
                .Setup(x => x.GetEmployeeOrganizationsList(It.IsAny<Customers.Employee>()))
                .Returns(employeeOrganizations);

            organizationService
             .Setup(x => x.GetOrganizations(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
             .Returns(orgnizations);

            return organizationService;
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }

        private Mock<IAdminUserService> AdminServiceStub(User user)
        {
            var adminService = new Mock<IAdminUserService>();

            adminService
                .Setup(x => x.GetUserById(It.IsAny<string>()))
                .Returns(user);

            return adminService;
        }
    }
}
