﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeWorkSchedule;
using AT.Api.Shared.Model.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeWorkSchedule
{
    [TestClass]
    public class CreateEmployeeWorkScheduleRequestTests
    {
        [TestMethod]
        public void CreateWorkScheduleModel_ValidEmployee_CreatesWorkSchedule()
        {
            //// Arrange
            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: new DateTime(2018, 10, 11));
            var time2 = EntityHelper.Time(totalMinutes: 21, sampleDate: new DateTime(2018, 10, 13), isHoliday: true);

            var scheduleId1 = Guid.NewGuid();

            var schedule1Start = new DateTime(2018, 10, 11);
            var schedule1End = new DateTime(2018, 10, 18);

            var schedule1 = EntityHelper.Schedule(
                id: scheduleId1,
                startDate: schedule1Start,
                endDate: schedule1End,
                times: new List<Time> { time1, time2 });

            var employee = EntityHelper.Employee(workSchedules: new List<Schedule> { schedule1 });

            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                WorkSchedule = Model()
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(response.Success);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_ErrorUpdatingEmployeeSchedule_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(employee);

            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, null))
                .Returns<Customers.Employee>(null);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01",
                WorkSchedule = Model()
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Error creating work schedule for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode); ;
        }

        [TestMethod]
        public void CreateWorkScheduleModel_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(null);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee(isDeleted: true);

            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);
            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void CreateWorkScheduleModel_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange

            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: new DateTime(2018, 10, 11));
            var time2 = EntityHelper.Time(totalMinutes: 21, sampleDate: new DateTime(2018, 10, 13), isHoliday: true);

            var schedule1Start = new DateTime(2018, 10, 11);
            var schedule1End = new DateTime(2018, 10, 18);

            var schedule1 = EntityHelper.Schedule(
                startDate: schedule1Start,
                endDate: schedule1End,
                times: new List<Time> { time1, time2 });

            var employee = EntityHelper.Employee(workSchedules: new List<Schedule> { schedule1 });

            var times = new List<BaseTimeModel>
            {
                new BaseTimeModel
                {
                    IsHoliday = false,
                    SampleDate = new DateTime(2018, 10, 11),
                    TotalMinutes = 10
                },
                new BaseTimeModel
                {
                    IsHoliday = true,
                    SampleDate = new DateTime(2018, 10, 13),
                    TotalMinutes = 21
                }
            };

            var model = Model(startDate: schedule1Start, endDate: schedule1End, times: times);

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();

            employeeService
                .Setup(x => x.SetWorkSchedule(
                    It.IsAny<Customers.Employee>(),
                    It.Is<Schedule>(
                        sch =>
                            sch.ScheduleType == ScheduleType.Rotating &&
                            sch.StartDate.ToUIString() == schedule1Start.ToUIString() &&
                            sch.EndDate.ToUIString() == schedule1End.ToUIString() &&
                            sch.Times.Count == 2 &&
                            !sch.Times[0].IsHoliday &&
                            sch.Times[0].SampleDate.ToUIString() == new DateTime(2018, 10, 11).ToUIString() &&
                            sch.Times[0].TotalMinutes == 10 &&
                            sch.Times[1].IsHoliday &&
                            sch.Times[1].SampleDate.ToUIString() == new DateTime(2018, 10, 13).ToUIString() &&
                            sch.Times[1].TotalMinutes == 21),
                    null))
                .Verifiable();

            employeeService
                .Setup(x => x.Update(
                    It.Is<Customers.Employee>(emp => emp.Id == employee.Id),
                    null,
                    null))
                .Returns(employee)
                .Verifiable();


            var request = new CreateEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new CreateEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01",
                WorkSchedule = model
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            employeeService
                .Setup(x => x.SetWorkSchedule(
                    It.IsAny<Customers.Employee>(),
                    It.IsAny<Schedule>(),
                    It.IsAny<List<VariableScheduleTime>>()));

            employeeService
                .Setup(x => x.Update(It.IsAny<Customers.Employee>(), null, null))
                .Returns(employee);

            return employeeService;
        }

        private CreateWorkScheduleModel Model(
            ScheduleType scheduleType = ScheduleType.Rotating,
            DateTime? startDate = null,
            DateTime? endDate = null,
            List<BaseTimeModel> times = null)
        {
            return new CreateWorkScheduleModel
            {
                ScheduleType = (int)scheduleType,
                StartDate = startDate ?? new DateTime(2000, 12, 10),
                EndDate = endDate,
                Times = times ?? new List<BaseTimeModel>()
            };
        }

        private BaseTimeModel BaseTimeModel(
            DateTime? sampleDate = null,
            int totalMinutes = 0,
            bool isHoliday = false)
        {
            return new BaseTimeModel
            {
                IsHoliday = isHoliday,
                SampleDate = sampleDate ?? new DateTime(2000, 12, 10),
                TotalMinutes = totalMinutes
            };
        }
    }
}
