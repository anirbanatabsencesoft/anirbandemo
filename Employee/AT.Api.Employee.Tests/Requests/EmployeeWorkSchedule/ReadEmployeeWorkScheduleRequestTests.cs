﻿using AbsenceSoft;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers.Contracts;
using AT.Api.Core.Exceptions;
using AT.Api.Employee.Requests.EmployeeWorkSchedule;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests.Requests.EmployeeWorkSchedule
{
    [TestClass]
    public class ReadEmployeeWorkScheduleRequestTests
    {
        [TestMethod]
        public void ReadWorkScheduleModel_ValidEmployee_ReturnsWorkSchedule()
        {
            //// Arrange
            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: new DateTime(2018, 10, 11));
            var time2 = EntityHelper.Time(totalMinutes: 21, sampleDate: new DateTime(2018, 10, 13), isHoliday: true);
            var time3 = EntityHelper.Time(totalMinutes: 35);
            var time4 = EntityHelper.Time(totalMinutes: 92);

            var scheduleId1 = Guid.NewGuid();
            var scheduleId2 = Guid.NewGuid();

            var schedule1Start = new DateTime(2018, 10, 11);
            var schedule1End = new DateTime(2018, 10, 18);
            var schedule2Start = new DateTime(2018, 9, 11);
            var schedule2End = new DateTime(2018, 9, 18);

            var schedule1 = EntityHelper.Schedule(
                id: scheduleId1,
                startDate: schedule1Start,
                endDate: schedule1End,
                times: new List<Time> { time1, time2 });

            var schedule2 = EntityHelper.Schedule(
                id: scheduleId2,
                startDate: schedule2Start,
                endDate: schedule2End,
                times: new List<Time> { time3, time4 });

            var employee = EntityHelper.Employee(workSchedules: new List<Schedule> { schedule1, schedule2 });

            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(response.Success);

            Assert.AreEqual(scheduleId1, response.Data.Id);
            Assert.AreEqual((int)ScheduleType.Weekly, response.Data.ScheduleType);
            Assert.AreEqual(schedule1Start.ToUIString(), response.Data.StartDate.ToMidnight().ToUIString());
            Assert.AreEqual(schedule1End.ToUIString(), response.Data.EndDate.ToMidnight().ToUIString());
            Assert.IsFalse(response.Data.Times[0].IsHoliday);
            Assert.AreEqual(10, response.Data.Times[0].TotalMinutes);
            Assert.AreEqual(new DateTime(2018, 10, 11).ToUIString(), response.Data.Times[0].SampleDate.ToMidnight().ToUIString());
            Assert.IsTrue(response.Data.Times[1].IsHoliday);
            Assert.AreEqual(21, response.Data.Times[1].TotalMinutes);
            Assert.AreEqual(new DateTime(2018, 10, 13).ToUIString(), response.Data.Times[1].SampleDate.ToMidnight().ToUIString());
        }

        [TestMethod]
        public void ReadWorkScheduleModel_NoWorkSchedule_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() => request.Handle(parameter));

            //// Assert
            Assert.IsNotNull(response);
            Assert.AreEqual("No work schedules found for employee", response.Message);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_NoMatchingEmployee_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();

            var employeeService = EmployeeServiceStub(null);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee not found with the given details", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [TestMethod]
        public void ReadWorkScheduleModel_DeletedEmployee_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee(isDeleted: true);

            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("This employee is deleted", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_NullEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = null,
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_EmptyEmployeeNumber_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);
            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = " ",
                EmployerId = "EmployerId01",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employee number is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_NullEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = null,
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_EmptyEmployerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = " ",
                CustomerId = "CustomerId01"
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Employer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_NullCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = null
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_EmptyCustomerId_ThrowsException()
        {
            //// Arrange
            var datetime = DateTime.UtcNow;
            var employee = EntityHelper.Employee();
            var employeeService = EmployeeServiceStub(employee);

            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNumber01",
                EmployerId = "EmployedId01",
                CustomerId = " "
            };

            //// Act
            var response = Assert.ThrowsException<ApiException>(() =>
                request.Handle(parameter));

            //// Assert
            Assert.AreEqual("Customer Id is required", response.Message);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void ReadWorkScheduleModel_CallServices_ServicesShouldBeCalled()
        {
            //// Arrange
            var time1 = EntityHelper.Time(totalMinutes: 10, sampleDate: new DateTime(2018, 10, 11));
            var time2 = EntityHelper.Time(totalMinutes: 21, sampleDate: new DateTime(2018, 10, 13), isHoliday: true);
            var time3 = EntityHelper.Time(totalMinutes: 35);
            var time4 = EntityHelper.Time(totalMinutes: 92);

            var scheduleId1 = Guid.NewGuid();
            var scheduleId2 = Guid.NewGuid();

            var schedule1Start = new DateTime(2018, 10, 11);
            var schedule1End = new DateTime(2018, 10, 18);
            var schedule2Start = new DateTime(2018, 9, 11);
            var schedule2End = new DateTime(2018, 9, 18);

            var schedule1 = EntityHelper.Schedule(
                id: scheduleId1,
                startDate: schedule1Start,
                endDate: schedule1End,
                times: new List<Time> { time1, time2 });

            var schedule2 = EntityHelper.Schedule(
                id: scheduleId2,
                startDate: schedule2Start,
                endDate: schedule2End,
                times: new List<Time> { time3, time4 });

            var employee = EntityHelper.Employee(workSchedules: new List<Schedule> { schedule1, schedule2 });

            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(
                    It.Is<string>(en => en == "EmployeeNo01"),
                    It.Is<string>(cid => cid == "CustomerId01"),
                    It.Is<string>(eid => eid == "EmployerId01")))
                .Returns(employee)
                .Verifiable();


            var request = new ReadEmployeeWorkScheduleRequest(employeeService.Object);

            var parameter = new ReadEmployeeWorkScheduleParameters
            {
                EmployeeNumber = "EmployeeNo01",
                CustomerId = "CustomerId01",
                EmployerId = "EmployerId01"
            };

            //// Act
            var response = request.Handle(parameter);

            //// Assert
            employeeService.Verify();
        }

        private Mock<IEmployeeService> EmployeeServiceStub(Customers.Employee employee)
        {
            var employeeService = new Mock<IEmployeeService>();

            employeeService
                .Setup(x => x.GetEmployeeByEmployeeNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(employee);

            return employeeService;
        }
    }
}
