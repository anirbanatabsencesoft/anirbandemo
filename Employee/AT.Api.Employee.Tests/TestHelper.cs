﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic;
using Moq;
using System;
using System.Collections.Generic;
using Customers = AbsenceSoft.Data.Customers;

namespace AT.Api.Employee.Tests
{
    public static class EntityHelper
    {
        public static ListResults OrgnizationTypeListResults(string[] codes)
        {
            var results = new List<ListResult>();
            foreach (var code in codes)
            {
                results.Add(new ListResult().Set("Code", code));
            }

            return new ListResults
            {
                Results = results
            };
        }

        public static EmployeeOrganization EmployeeOrganization(
            string id = "000000000000000000000001",
            string employeeNumber = "EmpNo01",
            DateRange dates = null,
            Customers.Employee employee = null,
            Organization organization = null,
            OrganizationType organizationType = null,
            OrganizationAffiliation affiliation = OrganizationAffiliation.Member)
        {
            return new EmployeeOrganization(organization, affiliation)
            {
                Id = id,
                EmployeeNumber = employeeNumber,
                Dates = dates,
                Employee = employee,
                Organization = organization,
                OrganizationType = organizationType,
                Affiliation = affiliation,
                TypeCode = organizationType != null ? organizationType.Code : string.Empty
            };
        }

        public static Customers.Employee Employee(
            string employeeId = "EmployeeId01",
            string employeeNumber = "EmployeeNo01",
            string customerId = "CustomerId01",
            string employerId = "EmployerId01",
            string firstName = "FirstName01",
            string lastName = "LastName01",
            string workCountry = "WorkCountry01",
            string workState = "WorkState01",
            EmployeeInfo employeeInfo = null,
            List<CustomField> customFields = null,
            List<Schedule> workSchedules = null,
            List<PriorHours> priorHours = null,
            bool isDeleted = false,
            Gender gender=Gender.Male)
        {
            return new Customers.Employee
            {
                Id = employeeId,
                EmployeeNumber = employeeNumber,
                CustomerId = customerId,
                EmployerId = employerId,
                CustomFields = customFields,
                WorkSchedules = workSchedules,
                FirstName = firstName,
                LastName = lastName,
                WorkCountry = workCountry,
                WorkState = workState,
                Info = employeeInfo,
                PriorHours = priorHours,
                IsDeleted = isDeleted,
                Gender= gender
            };
        }

        public static PriorHours PriorHours(
            double hoursWorked = 00,
            DateTime? asOf = null)
        {
            var priorHours = new PriorHours
            {
                HoursWorked = hoursWorked,
                AsOf = asOf ?? DateTime.UtcNow
            };
            return priorHours;
        }

        public static CustomField CustomField(
            string id = "FieldId01",
            string code = "Code01",
            string name = "Name01",
            CustomFieldType dataType = CustomFieldType.Text,
            CustomFieldValueType valueType = CustomFieldValueType.UserEntered,
            string selectedValue = "Value01",
            List<ListItem> listItems = null,
            string description = "Description01",
            bool isCollectedAtIntake = false)
        {
            var customField = new CustomField
            {
                Id = id,
                Code = code,
                DataType = dataType,
                ValueType = valueType,
                SelectedValue = selectedValue,
                ListValues = listItems,
                Description = description,
                Name = name,
                Target = EntityTarget.Employee,
                IsCollectedAtIntake = isCollectedAtIntake
            };

            return customField;
        }

        public static Organization Organization(
            string code = "OrgCode01",
            string name = "OrgName01",
            string description = "OrgDescription01",
            string sicCode = "SicCode01",
            string naicsCode = "NaicsCode01",
            int? oshaEstablishmentId = 0,
            OrganizationType organizationType = null,
            HierarchyPath path = new HierarchyPath(),
            Address address = null,
            bool isDeleted = false)
        {
            return new Organization
            {
                Code = code,
                Name = name,
                Description = description,
                OrganizationType = organizationType,
                SicCode = sicCode,
                NaicsCode = naicsCode,
                OshaEstablishmentId = oshaEstablishmentId,
                Path = path,
                Address = address,
                IsDeleted  = isDeleted
            };
        }

        public static OrganizationType OrganizationType(
            string code = "OrgTypeCode01",
            string name = "OrgTypeName01",
            string description = "OrgTypeDescripton01")
        {
            return new OrganizationType
            {
                Code = code,
                Name = name,
                Description = description
            };
        }

        public static Address Address(
            string name = "AddressName01",
            string address1 = "Address1Value01",
            string address2 = "Address2Value01",
            string city = "City01",
            string state = "State01",
            string postalCode = "PostalCode01",
            string country = "Country01")
        {
            return new Address
            {
                Name = name,
                Address1 = address1,
                Address2 = address2,
                City = city,
                State = state,
                PostalCode = postalCode,
                Country = country
            };
        }


        public static Employer Employer(
            string id = "EmployerId01",
            string name = "EmployerName01")
        {
            return new Employer
            {
                Id = id,
                Name = name
            };
        }

        public static Customer Customer(
            string id = "CustomerId01",
            string name = "CustomerName01")
        {
            return new Customer
            {
                Id = id,
                Name = name
            };
        }

        public static ContactType ContactType(string code = "Code01", string name = "CodeName01")
        {
            return new ContactType()
            {
                Code = code,
                Name = name
            };
        }
        public static Contact Contact(
            string title = "Title01",
            string companyName = "Company01",
            string cellphone = "01",
            string firstname = "FirstName01",
            string lastname = "LastName01",
            string workPhone = "02",
            string homePhone = "03",
            string altEmail = "AltEmail01",
            string email = "Email01",
            DateTime? dateOfBirth = null,
            Address address = null)
        {
            var contact = new Contact()
            {
                CompanyName = companyName,
                CellPhone = cellphone,
                FirstName = firstname,
                Email = email,
                AltEmail = altEmail,
                Address = address ?? Address(),
                DateOfBirth = dateOfBirth ?? new DateTime(1985, 01, 01),
                LastName = lastname,
                HomePhone = homePhone,
                WorkPhone = workPhone,
                Title = title
            };

            return contact;
        }

        public static EmployeeContact EmployeeContact(
            string contactId = "Contact01",
            string employerNumber = "EmployeeNo01",
            string employeeId = "Employee001",
            string relatedEmpNumber = "",
            string contactTypeCode = "HR",
            int? contactPosition = null,
            Contact contact = null,
            Customers.Employee employee = null)
        {
            var employeeContact = new EmployeeContact
            {
                Id = contactId,
                EmployeeNumber = employerNumber,
                EmployeeId = employeeId,
                ContactTypeCode = contactTypeCode,
                ContactPosition = contactPosition,
                RelatedEmployeeNumber = relatedEmpNumber,
                MilitaryStatus = MilitaryStatus.Civilian,
                Contact = contact ?? Contact(),
                Employee = employee ?? Employee()
            };

            return employeeContact;
        }

        public static Job Job(
            string id = "JobId01",
            string code = "Code01",
            JobClassification? activity = null,
            bool isDeleted = false)
        {
            var job = new Job
            {
                Activity = activity,
                Code = code,
                Id = id,
                IsDeleted = isDeleted
            };

            return job;
        }

        public static EmployeeJob EmployeeJob(
            string jobId = "EmployeeJobId01",
            string jobCode = "Code01",
            string jobName = "JobName01",
            int? jobPosition = null,
            string jobTitle = "JobTitle01",
            string employeeNumber = "EmployeeNo01",
            AdjudicationStatus status = AdjudicationStatus.Pending,
            JobDenialReason? reason = null,
            string comment = null,
            string officeLocationCode = null,
            DateRange dateRange = null,
            Customers.Employee employee = null,
            Job job = null)
        {
            var employeeJob = new EmployeeJob
            {
                EmployeeNumber = employeeNumber,
                Employee = employee ?? Employee(),
                Dates = dateRange,
                Id = jobId,
                JobCode = jobCode,
                JobName = jobName,
                JobPosition = jobPosition,
                JobTitle = jobTitle,
                Job = job ?? Job(),
                Status = new UserStamp<AdjudicationStatus, JobDenialReason?>(status, reason, null, comment),
                OfficeLocationCode = officeLocationCode,
            };

            employeeJob.Status.Comments = comment;

            return employeeJob;
        }


        public static Schedule Schedule(
            Guid? id = null,
            ScheduleType scheduleType = ScheduleType.Weekly,
            DateTime? startDate = null,
            DateTime? endDate = null,
            List<Time> times = null)
        {
            return new Schedule
            {
                Id = id ?? Guid.NewGuid(),
                ScheduleType = scheduleType,
                StartDate = startDate ?? DateTime.UtcNow,
                EndDate = endDate,
                Times = times
            };
        }

        public static VariableScheduleTime VariableScheduleTime(
         Customers.Employee employee = null,         
          Time time = null)
        {
            return new VariableScheduleTime
            {
                Time = time,
                EmployeeId = employee.Id,
                Employee = employee
            };
        }

        public static Time Time(
            Guid? id = null,
            bool isHoliday = false,
            DateTime? sampleDate = null,
            int? totalMinutes = null)
        {
            return new Time
            {
                Id = id ?? Guid.NewGuid(),
                IsHoliday = isHoliday,
                SampleDate = sampleDate ?? DateTime.UtcNow,
                TotalMinutes = totalMinutes
            };
        }

        public static T UpdateAuditInfo<T>(
            this T entity,
            User updatedBy,
            DateTime updatetime) where T : BaseEntity<T>, new()
        {
            entity.CreatedBy = updatedBy;
            entity.ModifiedBy = updatedBy;
            entity.SetCreatedDate(updatetime);
            entity.SetModifiedDate(updatetime);

            return entity;
        }

        public static User UserStub(
            string userId = "UserId01",
            string firstName = "User",
            string lastName = "Name")
        {
            var user = new Mock<User>(MockBehavior.Loose);
            user.Setup(u => u.Id).Returns(userId);
            user.Object.FirstName = firstName;
            user.Object.LastName = lastName;

            return user.Object;
        }

        public static EmployeeInfo EmployeeInfo(
            string email = "email01@test.com",
            string altEmail = "altEmail01@test.com",            
            string workPhone = "111111",
            string homePhone = "222222",
            string cellPhone = "333333",
            string altPhone = "444444",
            Address address = null,
            Address altAddress = null
            )
        {
            return new EmployeeInfo
            {
                Email = email,
                AltEmail = altEmail,                
                WorkPhone = workPhone,
                HomePhone = homePhone,
                CellPhone = cellPhone,
                AltPhone = altPhone,
                Address = address,
                AltAddress = address
            };
        }

        public static T SetCustomer<T>(
            this T entity,
            Customer customer) where T : BaseCustomerEntity<T>, new()
        {
            entity.Customer = customer;
            return entity;
        }

        public static T SetEmployer<T>(
            this T entity,
            Employer employer) where T : BaseEmployerEntity<T>, new()
        {
            entity.Employer = employer;
            return entity;
        }
    }
}
