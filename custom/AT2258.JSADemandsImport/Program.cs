﻿using AbsenceSoft;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AT2258.JSADemandsImport
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            //Job Code,Office Locations,Demand Code,Demand Type,Value,Description,Other Office Locations...+
            // Initial state/default = Amazon
            string CustomerId = "546e5097a32aa00d60e3210a";
            string EmployerId = "546e52cda32aa00f78086269";
            string fileName = null;
            bool header = true;

            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                    fileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-c")
                    CustomerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-e")
                    EmployerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-h")
                    header = args.Length > (i + 1) ? args[i + 1] == "yes" : false;
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    AT2258.JSADemandsImport.exe [-f <file path>] [-c <customer id>] [-e <employer id>]");
                    Console.WriteLine();
                    Console.WriteLine("    -f <file path>: Specifies the file path to be converted");
                    Console.WriteLine("    -h <yes|no>: Specifies whether or not there is a header row in the file, default = yes");
                    Console.WriteLine("    -c <cusotmer id>: Specifies a customer Id OTHER than parrot's");
                    Console.WriteLine("    -e <employer id>: Specifies an employer Id OTHER than parrot's");
                    Console.WriteLine();
                    Environment.Exit(0);
                    return;
                }
            }
            if (!string.IsNullOrWhiteSpace(fileName) && !File.Exists(fileName))
            {
                log.ErrorFormat("-f \"{0}\" points to a file that does not exist or you do not have permission to view", fileName);
                Environment.Exit(9);
                return;
            }

            Stopwatch stopper = new Stopwatch();
            stopper.Start();

            List<Demand> demands = Demand.AsQueryable().Where(d => d.CustomerId == CustomerId && (d.EmployerId == null || d.EmployerId == EmployerId)).ToList();
            List<DemandType> types = DemandType.AsQueryable().Where(d => d.CustomerId == CustomerId && (d.EmployerId == null || d.EmployerId == EmployerId)).ToList();
            List<Job> jobs = Job.AsQueryable().Where(d => d.CustomerId == CustomerId && d.EmployerId == EmployerId).ToList();

            using (TextReader reader = new StreamReader(fileName))
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
            {
                long lineNumber = 0L;
                try
                {
                    parser.HasFieldsEnclosedInQuotes = true;
                    parser.SetDelimiters(",", "|", "\t");
                    parser.TrimWhiteSpace = true;
                    while (!parser.EndOfData)
                    {
                        try
                        {
                            lineNumber = parser.LineNumber;
                            if (header && lineNumber == 1L) // ignore header row
                            {
                                parser.ReadLine();
                                continue;
                            }
                            var row = parser.ReadFields();
                            DemandMap map = new DemandMap(row);
                            if (!map.IsComplete())
                                log.WarnFormat("Row # {0} is Incomplete/Not Valid: {1}", lineNumber, string.Join(",", row));
                            //
                            // TODO: Map the thing now.... Duh
                            foreach (var org in map.OfficeLocations)
                            {
                                Job job = jobs.FirstOrDefault(j => string.Equals(j.Code, map.JobCode, StringComparison.InvariantCultureIgnoreCase)
                                    && string.Equals(j.OrganizationCode, org, StringComparison.InvariantCultureIgnoreCase));
                                if (job == null)
                                {
                                    job = jobs.AddFluid(new Job()
                                    {
                                        CustomerId = CustomerId,
                                        EmployerId = EmployerId,
                                        Code = map.JobCode,
                                        Name = map.JobCode,
                                        Description = map.JobCode,
                                        OrganizationCode = org
                                    });
                                    log.InfoFormat("Line #: {2}: Will create a new job record, \"{0}\", for office location \"{1}\" as it did not already exist.", map.JobCode, org, lineNumber);
                                }
                                job.Description = job.Description ?? map.Description;

                                Demand dem = demands.FirstOrDefault(d => string.Equals(d.Code, map.DemandCode, StringComparison.InvariantCultureIgnoreCase));
                                if (dem == null)
                                {
                                    log.WarnFormat("Line # {1}, Org {2}: Demand code, \"{0}\", not found; this line will NOT be mapped.", map.DemandCode, lineNumber, org);
                                    continue;
                                }
                                JobRequirement requirement = job.Requirements.FirstOrDefault(r => r.DemandId == dem.Id) 
                                    ?? job.Requirements.AddFluid(new JobRequirement() { DemandId = dem.Id, Demand = dem, Values = new List<AppliedDemandValue<JobRequirementValue>>() });
                                requirement.Description = map.Description ?? requirement.Description;
                                DemandType dt = types.FirstOrDefault(t => t.Name.StartsWith(map.DemandType, StringComparison.InvariantCultureIgnoreCase));
                                if (dt == null)
                                {
                                    log.WarnFormat("Line #: {1}, Org {2}: Demand type, \"{0}\", not found; this line will NOT be mapped.", map.DemandType, lineNumber, org);
                                    continue;
                                }
                                if (!dem.Types.Contains(dt.Id))
                                {
                                    log.WarnFormat("Line #: {1}, Org {2}: Demand type \"{0}\" is not supported by demand \"{3}\"; this line will NOT be mapped.", map.DemandType, lineNumber, org, dem.Name);
                                    continue;
                                }
                                var val = requirement.Values.FirstOrDefault(v => v.DemandTypeId == dt.Id) 
                                    ?? requirement.Values.AddFluid(new JobRequirementValue() { DemandTypeId = dt.Id, DemandType = dt, Type = dt.Type });
                                switch (val.Type)
                                {
                                    case DemandValueType.Text:
                                        val.Value = map.Value;
                                        val.Applicable = true;
                                        break;
                                    case DemandValueType.Boolean:
                                        val.Value = map.Value == "True" 
                                            || map.Value == "true" 
                                            || map.Value == "t" 
                                            || map.Value == "T" 
                                            || map.Value == "y" 
                                            || map.Value == "Y" 
                                            || map.Value == "Yes" 
                                            || map.Value == "yes" 
                                            || map.Value == "1";
                                        val.Applicable = true;
                                        break;
                                    case DemandValueType.Value:
                                        int iv;
                                        if (int.TryParse(map.Value, out iv))
                                        {
                                            var item = dt.Items.FirstOrDefault(i => i.Value == iv);
                                            if (item == null)
                                            {
                                                log.WarnFormat("Line # {0}, Org {3}: Demand type of \"{1}\" does not have a valid value matching \"{2}\".", lineNumber, map.DemandType, map.Value, org);
                                                continue;
                                            }
                                            val.DemandItem = item;
                                            val.Applicable = true;
                                        }
                                        else
                                        {
                                            log.WarnFormat("Line # {0}, Org {3}: Demand type of \"{1}\" expects a valid number value; \"{2}\" is not a valid number.", lineNumber, map.DemandType, map.Value, org);
                                            continue;
                                        }
                                        break;
                                    case DemandValueType.Schedule:
                                        log.WarnFormat("Line # {0}, Org {1}: Can NOT support the demand type of 'Schedule', this line will NOT be mapped.", lineNumber, org);
                                        continue;
                                }

                                job.Save();
                                log.InfoFormat("Line # {0}, Org {3}: Job \"{1}\" successfully updated with _id: ObjectId(\"{2}\").", lineNumber, job.Code, job.Id, org);
                            }
                        }
                        catch (Microsoft.VisualBasic.FileIO.MalformedLineException lineEx)
                        {
                            log.Error(string.Format("Error on line # {0}, bad juju in row source: {1}", lineEx.LineNumber, lineEx.Source), lineEx);
                        }
                        catch (Exception ex)
                        {
                            log.Error(string.Format("Error processing line # {0}", lineNumber), ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Fatal("Fatal error processing file", ex);
                }
            }

            stopper.Stop();
            log.InfoFormat("All done, took {0}. Bye bye now.", stopper.Elapsed.ToReallyFriendlyTime());
        }
    }
}
