﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AbsenceSoft;

namespace AT2258.JSADemandsImport
{
    [Serializable]
    public class DemandMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DemandMap"/> class.
        /// </summary>
        public DemandMap()
        {
            OfficeLocations = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DemandMap"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        public DemandMap(string[] row) : this()
        {
            //Job Code,Office Locations,Demand Code,Demand Type,Value,Description,Other Office Locations...+
            if (row == null || row.Length == 0)
                return;
            JobCode = Normalize(row, 0);
            if (row.Length > 1 && !string.IsNullOrWhiteSpace(row[1]))
                OfficeLocations.Add(Normalize(row, 1));
            DemandCode = Normalize(row, 2);
            DemandType = Normalize(row, 3);
            Value = Normalize(row, 4);
            Description = Normalize(row, 5);
            for (var i = 6; i < row.Length; i++)
                if (!string.IsNullOrWhiteSpace(row[i]))
                    OfficeLocations.AddIfNotExists(Normalize(row, i));
        }

        /// <summary>
        /// Gets or sets the job code.
        /// </summary>
        /// <value>
        /// The job code.
        /// </value>
        public string JobCode { get; set; }

        /// <summary>
        /// Gets or sets the demand code.
        /// </summary>
        /// <value>
        /// The demand code.
        /// </value>
        public string DemandCode { get; set; }

        /// <summary>
        /// Gets or sets the type of the demand.
        /// </summary>
        /// <value>
        /// The type of the demand.
        /// </value>
        public string DemandType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the office locations.
        /// </summary>
        /// <value>
        /// The office locations.
        /// </value>
        public List<string> OfficeLocations { get; set; }

        /// <summary>
        /// Determines whether this instance is complete.
        /// </summary>
        /// <returns></returns>
        public bool IsComplete()
        {
            return !string.IsNullOrWhiteSpace(JobCode)
                && !string.IsNullOrWhiteSpace(DemandCode)
                && !string.IsNullOrWhiteSpace(DemandType)
                && !string.IsNullOrWhiteSpace(Value)
                && OfficeLocations.Any(o => !string.IsNullOrWhiteSpace(o));
        }

        /// <summary>
        /// Normalizes the specified index.
        /// </summary>
        /// <param name="parts">The parts.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected string Normalize(string[] parts, int index)
        {
            if (index >= parts.Length)
                return null;
            string data = parts[index];
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }
    }
}
