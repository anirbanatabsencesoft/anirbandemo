﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParrotELTranslator.Api.Models
{
    public class KickOffModel
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [Required]
        public string CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        [Required]
        public string EmployerId { get; set; }
        /// <summary>
        /// Gets or sets the file key.
        /// </summary>
        /// <value>
        /// The file key.
        /// </value>
        [Required]
        public string FileKey { get; set; }
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Concat("{\"CustomerId\":\"", CustomerId, "\",\"EmployerId\":\"", EmployerId, "\",\"FileKey\":\"", FileKey, "}");
        }
    }
}