﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using AbsenceSoft.Logic.Common;
using ParrotELTranslator.Api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ParrotELTranslator.Api
{
    public class ELController : ApiController
    {
        [Route("", Name = "Home", Order = 0)]
        public IHttpActionResult Get()
        {
            return ResponseMessage(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("All your base are belong to us!", Encoding.UTF8, "text/plain")
            });
        }

        // POST api/EL/
        public IHttpActionResult Post([FromBody]KickOffModel model)
        {
            if (!ModelState.IsValid)
                return ResponseMessage(new HttpResponseMessage(HttpStatusCode.BadRequest));

            string filePath = null;
            bool error = false;
            try
            {
                Log.Info("Recieved request to process {0}", model);
                filePath = new FileService().Using(fs => fs.DownloadFileToDiskAsText(model.FileKey, Settings.Default.S3BucketName_Processing));
                var startInfo = new ProcessStartInfo(HttpContext.Current.Server.MapPath("~/bin/ParrotELTranslator.exe"))
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    Arguments = string.Format(ConfigurationManager.AppSettings["ParrotELTranslator_CommandLineFormat"], model.CustomerId, model.EmployerId, filePath)
                };
                using (var process = new Process() { StartInfo = startInfo })
                {
                    StringBuilder output = new StringBuilder();
                    process.OutputDataReceived += new DataReceivedEventHandler((o, args) => output.AppendLine(args.Data));
                    process.ErrorDataReceived += new DataReceivedEventHandler((o, args) => output.AppendLine(args.Data));
                    if (process.Start())
                    {
                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();
                        process.WaitForExit();
                        HttpStatusCode status = HttpStatusCode.OK;
                        if (process.ExitCode != 0)
                        {
                            error = true;
                            status = HttpStatusCode.InternalServerError;
                        }
                        return ResponseMessage(new HttpResponseMessage(status) { Content = new StringContent(output.ToString(), Encoding.UTF8, "text/plain") });
                    }
                }
            }
            catch (Exception ex)
            {
                error = true;
                Log.Error(string.Format("Error processing file for message, {0}", model), ex);
                return InternalServerError(ex);
            }
            finally
            {
                if (!string.IsNullOrWhiteSpace(filePath) && File.Exists(filePath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Format("The file '{0}' could not be deleted", filePath), ex);
                    }
                }
            }

            return Ok();
        }
    }
}