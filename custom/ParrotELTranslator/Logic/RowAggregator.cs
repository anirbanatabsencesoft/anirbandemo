﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParrotELTranslator.Logic
{
    public static class RowAggregator
    {
        public static List<EligibilityRow> Aggregate(this IEnumerable<string> rows)
        {
            List<EligibilityRow> result = new List<EligibilityRow>();
            
            // Get our Parrot rows from the source passed in rows (parse 'em out).
            List<ParrotRow> parrots = rows.Select(r => new ParrotRow(r)).ToList();
            // Translate our Parrot rows to their respective eligibility rows (except client defined data, that comes later)
            List<EligibilityRow> eligRows = parrots
                .Where(p => p.RecType != ParrotRow.RecordType.ClientDefinedData)
                .SelectMany(p => p.ToEligibilityRows())
                .ToList();
            // Get our the HR/Supervisor contact rows from the converted eligibility rows, those shouldn't be merged
            List<EligibilityRow> contactRows = eligRows.Where(r => r.RecType == EligibilityRow.RecordType.Supervisor || r.RecType == EligibilityRow.RecordType.HR).ToList();
            // Get the final list of eligiblity rows to be merged
            eligRows = eligRows.Where(r => r.RecType != EligibilityRow.RecordType.HR && r.RecType != EligibilityRow.RecordType.Spouse && r.RecType != EligibilityRow.RecordType.Supervisor).ToList();

            // Build the contact rows, if any.
            var clientData = parrots.Where(p => p.RecType == ParrotRow.RecordType.ClientDefinedData).OrderBy(p => p.AssignedAttributeId).ToList();

            // These 2 persons are special (they directly correspond to the primary Manager/HR contact(s) on the primary Parrot eligibility file row)...
            List<EligibilityRow> special = new List<EligibilityRow>(2);
            ConsolidateSupervisorHrContactAndAddToList(special, EligibilityRow.RecordType.Supervisor,
                clientData.FirstOrDefault(c => "A0014".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
                clientData.FirstOrDefault(c => "A0015".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
                clientData.FirstOrDefault(c => "A0016".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)));
            //ConsolidateSupervisorHrContactAndAddToList(special, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0018".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0020".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0019".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 1);
            if (special.Any())
            {
                //var hr = contactRows.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.HR);
                var sup = contactRows.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.Supervisor);

                //var hr2 = special.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.HR);
                var sup2 = special.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.Supervisor);

                //if (hr == null && hr2 != null) contactRows.Add(hr2);
                //else if (hr != null && hr2 != null)
                //{
                //    hr.HRFirstName = hr2.HRFirstName ?? hr.HRFirstName;
                //    hr.HRLastName = hr2.HRLastName ?? hr.HRLastName;
                //    hr.HREmail = hr2.HREmail ?? hr.HREmail;
                //    hr.HRPhone = hr2.HRPhone ?? hr.HRPhone;
                //}

                if (sup == null && sup2 != null) contactRows.Add(sup2);
                else if (sup != null && sup2 != null)
                {
                    sup.ManagerFirstName = sup2.ManagerFirstName ?? sup.ManagerFirstName;
                    sup.ManagerLastName = sup2.ManagerLastName ?? sup.ManagerLastName;
                    sup.ManagerEmail = sup2.ManagerEmail ?? sup.ManagerEmail;
                    sup.ManagerPhone = sup2.ManagerPhone ?? sup.ManagerPhone;
                }
            }

            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0027".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0028".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0029".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 2);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0035".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0036".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0037".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 3);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0043".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0044".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0045".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 4);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0051".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0052".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0053".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 5);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0059".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0060".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0061".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 6);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0067".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0068".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0069".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 7);
            //ConsolidateSupervisorHrContactAndAddToList(contactRows, EligibilityRow.RecordType.HR,
            //    clientData.FirstOrDefault(c => "A0075".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0076".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)),
            //    clientData.FirstOrDefault(c => "A0077".Equals(c.AssignedAttributeId ?? "", StringComparison.CurrentCultureIgnoreCase)), 8);
            
            var hrDesignationRow = clientData.FirstOrDefault(c => "A0089".Equals(c.AssignedAttributeId ?? ""));
            if(hrDesignationRow != null){
                eligRows.Add(new EligibilityRow()
                {
                    RecType = EligibilityRow.RecordType.Eligibility,
                    HRDesignation = hrDesignationRow.AttributeValue.ToUpperInvariant() == "Y",
                    EmployeeNumber = parrots.First().EmployeeNumber
                });
            }

            
            // Output the merged eligibility rows
            result.AddRange(EligibilityRow.Merge(eligRows));
            // Output the final contact rows.
            result.AddRange(contactRows);

            return result;
        }

        private static void ConsolidateSupervisorHrContactAndAddToList(
            List<EligibilityRow> rows,
            EligibilityRow.RecordType recordType,
            ParrotRow name, 
            ParrotRow phone, 
            ParrotRow email, int? level = null)
        {
            if (rows == null) return;
            if (name == null && phone == null && email == null) return;
            if ((name == null || !name.RecType.IsValid) && (phone == null || !phone.RecType.IsValid) && (email == null || !email.RecType.IsValid)) return;
            if ((name == null || string.IsNullOrWhiteSpace(name.AssignedAttributeId)) && (phone == null || string.IsNullOrWhiteSpace(phone.AssignedAttributeId)) && (email == null || string.IsNullOrWhiteSpace(email.AssignedAttributeId))) return;
            if ((name == null || string.IsNullOrWhiteSpace(name.AttributeValue)) && (phone == null || string.IsNullOrWhiteSpace(phone.AttributeValue)) && (email == null || string.IsNullOrWhiteSpace(email.AttributeValue))) return;
            if (string.IsNullOrWhiteSpace((name ?? phone ?? email).EmployeeNumber)) return;

            EligibilityRow row = new EligibilityRow()
            {
                RecType = recordType,
                EmployeeNumber = (name ?? phone ?? email).EmployeeNumber,

                ManagerLastName = recordType == EligibilityRow.RecordType.Supervisor && name != null && !string.IsNullOrWhiteSpace(name.AttributeValue) ? SplitNames(name)[0] : null,
                ManagerFirstName = recordType == EligibilityRow.RecordType.Supervisor && name != null && !string.IsNullOrWhiteSpace(name.AttributeValue) ? SplitNames(name)[1] : null,
                ManagerEmail = recordType == EligibilityRow.RecordType.Supervisor && email != null && !string.IsNullOrWhiteSpace(email.AttributeValue) ? email.AttributeValue : null,
                ManagerPhone = recordType == EligibilityRow.RecordType.Supervisor && phone != null && !string.IsNullOrWhiteSpace(phone.AttributeValue) ? phone.AttributeValue : null,

                HRLastName = recordType == EligibilityRow.RecordType.HR && name != null && !string.IsNullOrWhiteSpace(name.AttributeValue) ? SplitNames(name)[0] : null,
                HRFirstName = recordType == EligibilityRow.RecordType.HR && name != null && !string.IsNullOrWhiteSpace(name.AttributeValue) ? SplitNames(name)[1] : null,
                HREmail = recordType == EligibilityRow.RecordType.HR && email != null && !string.IsNullOrWhiteSpace(email.AttributeValue) ? email.AttributeValue : null,
                HRPhone = recordType == EligibilityRow.RecordType.HR && phone != null && !string.IsNullOrWhiteSpace(phone.AttributeValue) ? phone.AttributeValue : null,
                HRLevel = level
            };

            rows.Add(row);
        }

        private static string[] SplitNames(ParrotRow name)
        {
            if (name == null) return new string[2] { "", "" };
            if (string.IsNullOrWhiteSpace(name.AttributeValue)) return new string[2] { "", "" };
            string[] names = name.AttributeValue.Split(',');
            bool flipper = false;
            if (names.Length == 1)
            {
                names = name.AttributeValue.Split(' ');
                flipper = names.Length > 1;
            }
            if (names.Length == 1)
                return new string[2] { names[0], "" };
            if (names.Length > 2)
                return flipper ? new string[2] { names[names.Length - 1], string.Join(" ", names.Take(names.Length - 1)) } : new string[2] { names[0], string.Join(",", names.Skip(1)) };
            return names;
        }
    }
}
