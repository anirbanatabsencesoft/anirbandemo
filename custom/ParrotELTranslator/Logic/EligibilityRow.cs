﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ParrotELTranslator.Logic
{
    public class EligibilityRow : BaseRow
    {
        #region Const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;

        private const int DateWorkedOrdinal = 2;
        private const int TimeWorkedOrdinal = 3;

        private const int SpouseSpouseEmployeeNumberOrdinal = 2;

        private const int ContactEmployeeNumberOrdinal = 2;
        private const int ContactEmailOrdinal = 3;
        private const int ContactFirstNameOrdinal = 4;
        private const int ContactLastNameOrdinal = 5;
        private const int ContactPhoneOrdinal = 6;

        private const int LastNameOrdinal = 2;
        private const int FirstNameOrdinal = 3;
        private const int MiddleNameOrdinal = 4;
        private const int JobTitleOrdinal = 5;
        private const int JobLocationOrdinal = 6;
        private const int WorkStateOrdinal = 7;
        private const int WorkCountryOrdinal = 8;
        private const int PhoneHomeOrdinal = 9;
        private const int PhoneWorkOrdinal = 10;
        private const int PhoneMobileOrdinal = 11;
        private const int PhoneAltOrdinal = 12;
        private const int EmailOrdinal = 13;
        private const int EmailAltOrdinal = 14;
        private const int AddressOrdinal = 15;
        private const int Address2Ordinal = 16;
        private const int CityOrdinal = 17;
        private const int StateOrdinal = 18;
        private const int PostalCodeOrdinal = 19;
        private const int CountryOrdinal = 20;
        private const int EmploymentTypeOrdinal = 21;
        private const int ManagerEmployeeNumberOrdinal = 22;
        private const int ManagerLastNameOrdinal = 23;
        private const int ManagerFirstNameOrdinal = 24;
        private const int ManagerPhoneOrdinal = 25;
        private const int ManagerEmailOrdinal = 26;
        private const int HREmployeeNumberOrdinal = 27;
        private const int HRLastNameOrdinal = 28;
        private const int HRFirstNameOrdinal = 29;
        private const int HRPhoneOrdinal = 30;
        private const int HREmailOrdinal = 31;
        private const int SpouseEmployeeNumberOrdinal = 32;
        private const int DateOfBirthOrdinal = 33;
        private const int GenderOrdinal = 34;
        private const int ExemptionStatusOrdinal = 35;
        private const int Meets50In75Ordinal = 36;
        private const int KeyEmployeeOrdinal = 37;
        private const int MilitaryStatusOrdinal = 38;
        private const int EmploymentStatusOrdinal = 39;
        private const int TerminationDateOrdinal = 40;
        private const int PayRateOrdinal = 41;
        private const int PayTypeOrdinal = 42;
        private const int HireDateOrdinal = 43;
        private const int RehireDateOrdinal = 44;
        private const int AdjustedServiceDateOrdinal = 45;
        private const int MinutesPerWeekOrdinal = 46;
        private const int HoursWorkedIn12MonthsOrdinal = 47;
        private const int WorkTimeSunOrdinal = 48;
        private const int WorkTimeMonOrdinal = 49;
        private const int WorkTimeTueOrdinal = 50;
        private const int WorkTimeWedOrdinal = 51;
        private const int WorkTimeThuOrdinal = 52;
        private const int WorkTimeFriOrdinal = 53;
        private const int WorkTimeSatOrdinal = 54;
        private const int VariableScheduleOrdinal = 55;
        private const int JobDescriptionOrdinal = 56;
        private const int JobClassificationOrdinal = 57;
        private const int USCensusJobCategoryCodeOrdinal = 58;
        private const int SOCCodeOrdinal = 59;
        private const int EmployeeClassOrdinal = 60;
        private const int HRDesignationOrdinal = 61;
        #endregion Const

        #region .ctor

        public EligibilityRow() : base()
        {
            Delimiter = '|';
        }
        public EligibilityRow(string rowSource) : base(rowSource) { }

        #endregion .ctor

        public enum RecordType
        {
            Eligibility = 'E',
            TimeWorked = 'M',
            Spouse = 'S',
            Basic = 'B',
            Supervisor = 'U',
            HR = 'H'
        }

        #region Fields

        public RecordType RecType { get; set; }
        public string EmployeeNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string JobLocation { get; set; }
        public string WorkState { get; set; }
        public string WorkCountry { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneAlt { get; set; }
        public string Email { get; set; }
        public string EmailAlt { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string EmploymentType { get; set; }
        public string ManagerEmployeeNumber { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public string HREmployeeNumber { get; set; }
        public string HRLastName { get; set; }
        public string HRFirstName { get; set; }
        public string HRPhone { get; set; }
        public string HREmail { get; set; }
        public bool? HRDesignation { get; set; }
        public int? HRLevel { get; set; }
        public string SpouseEmployeeNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public char? Gender { get; set; }
        public char? ExemptionStatus { get; set; }
        public bool? Meets50In75 { get; set; }
        public bool? KeyEmployee { get; set; }
        public byte? MilitaryStatus { get; set; }
        public char? EmploymentStatus { get; set; }
        public DateTime? TerminationDate { get; set; }
        public decimal? PayRate { get; set; }
        public byte? PayType { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? RehireDate { get; set; }
        public DateTime? AdjustedServiceDate { get; set; }
        public int? MinutesPerWeek { get; set; }
        public decimal? HoursWorkedIn12Months { get; set; }
        public int? WorkTimeSun { get; set; }
        public int? WorkTimeMon { get; set; }
        public int? WorkTimeTue { get; set; }
        public int? WorkTimeWed { get; set; }
        public int? WorkTimeThu { get; set; }
        public int? WorkTimeFri { get; set; }
        public int? WorkTimeSat { get; set; }
        public bool? VariableSchedule { get; set; }
        public string JobDescription { get; set; }
        public byte? JobClassification { get; set; }
        public string USCensusJobCategoryCode { get; set; }
        public string SOCCode { get; set; }

        // Time Worked Record Detail Only
        public DateTime? DateWorked { get; set; }
        public int? TimeWorked { get; set; }

        // EO Record Org STuff
        public string CostCenterCode { get; set; }
        public string CostCenterName { get; set; }

        // Added Scope
        public string EmployeeClassification { get; set; }

        #endregion Fields

        #region ToString
        
        public override string ToString()
        {
            int cols = 62;
            switch (RecType)
            {
                case RecordType.TimeWorked:
                    cols = 4;
                    break;
                case RecordType.Spouse:
                    cols = 3;
                    break;
                case RecordType.HR:
                case RecordType.Supervisor:
                    cols = 7;
                    break;
                case RecordType.Eligibility:
                case RecordType.Basic:
                default:
                    cols = 62;
                    break;
            }

            string[] fields = new string[cols];

            fields[RecordTypeOrdinal] = Emit((char)RecType);
            fields[EmployeeNumberOrdinal] = Emit(EmployeeNumber);

            if (RecType == RecordType.TimeWorked)
            {
                fields[DateWorkedOrdinal] = Emit(DateWorked);
                fields[TimeWorkedOrdinal] = Emit(TimeWorked);
            }
            else if (RecType == RecordType.Spouse)
            {
                fields[SpouseSpouseEmployeeNumberOrdinal] = Emit(SpouseEmployeeNumber);
            }
            else if (RecType == RecordType.Supervisor)
            {
                fields[ContactEmployeeNumberOrdinal] = Emit(ManagerEmployeeNumber);
                fields[ContactLastNameOrdinal] = Emit(ManagerLastName);
                fields[ContactFirstNameOrdinal] = Emit(ManagerFirstName);
                fields[ContactPhoneOrdinal] = Emit(ManagerPhone);
                fields[ContactEmailOrdinal] = Emit(ManagerEmail);
            }
            else if (RecType == RecordType.HR)
            {
                fields[ContactEmployeeNumberOrdinal] = Emit(HREmployeeNumber);
                fields[ContactLastNameOrdinal] = Emit(HRLastName);
                fields[ContactFirstNameOrdinal] = Emit(HRFirstName);
                fields[ContactPhoneOrdinal] = Emit(HRPhone);
                fields[ContactEmailOrdinal] = Emit(HREmail);
            }
            else
            {
                fields[LastNameOrdinal] = Emit(LastName);
                fields[FirstNameOrdinal] = Emit(FirstName);
                fields[MiddleNameOrdinal] = Emit(MiddleName);
                fields[JobTitleOrdinal] = Emit(JobTitle);
                fields[JobLocationOrdinal] = Emit(JobLocation);
                fields[WorkStateOrdinal] = Emit(WorkState);
                fields[WorkCountryOrdinal] = Emit(WorkCountry);
                fields[PhoneHomeOrdinal] = Emit(PhoneHome);
                fields[PhoneWorkOrdinal] = Emit(PhoneWork);
                fields[PhoneMobileOrdinal] = Emit(PhoneMobile);
                fields[PhoneAltOrdinal] = Emit(PhoneAlt);
                fields[EmailOrdinal] = Emit(Email);
                fields[EmailAltOrdinal] = Emit(EmailAlt);
                fields[AddressOrdinal] = Emit(Address);
                fields[Address2Ordinal] = Emit(Address2);
                fields[CityOrdinal] = Emit(City);
                fields[StateOrdinal] = Emit(State);
                fields[PostalCodeOrdinal] = Emit(PostalCode);
                fields[CountryOrdinal] = Emit(Country);
                fields[EmploymentTypeOrdinal] = Emit(EmploymentType);
                fields[ManagerEmployeeNumberOrdinal] = Emit(ManagerEmployeeNumber);
                fields[ManagerLastNameOrdinal] = Emit(ManagerLastName);
                fields[ManagerFirstNameOrdinal] = Emit(ManagerFirstName);
                fields[ManagerPhoneOrdinal] = Emit(ManagerPhone);
                fields[ManagerEmailOrdinal] = Emit(ManagerEmail);
                fields[HREmployeeNumberOrdinal] = Emit(HREmployeeNumber);
                fields[HRLastNameOrdinal] = Emit(HRLastName);
                fields[HRFirstNameOrdinal] = Emit(HRFirstName);
                fields[HRPhoneOrdinal] = Emit(HRPhone);
                fields[HREmailOrdinal] = Emit(HREmail);
                fields[HRDesignationOrdinal] = Emit(HRDesignation);
                fields[SpouseEmployeeNumberOrdinal] = Emit(SpouseEmployeeNumber);
                fields[DateOfBirthOrdinal] = Emit(DateOfBirth);
                fields[GenderOrdinal] = Emit(Gender);
                fields[ExemptionStatusOrdinal] = Emit(ExemptionStatus);
                fields[Meets50In75Ordinal] = Emit(Meets50In75);
                fields[KeyEmployeeOrdinal] = Emit(KeyEmployee);
                fields[MilitaryStatusOrdinal] = Emit(MilitaryStatus);
                fields[EmploymentStatusOrdinal] = Emit(EmploymentStatus);
                fields[TerminationDateOrdinal] = Emit(TerminationDate);
                fields[PayRateOrdinal] = Emit(PayRate);
                fields[PayTypeOrdinal] = Emit(PayType);
                fields[HireDateOrdinal] = Emit(HireDate);
                fields[RehireDateOrdinal] = Emit(RehireDate);
                fields[AdjustedServiceDateOrdinal] = Emit(AdjustedServiceDate);
                fields[MinutesPerWeekOrdinal] = Emit(MinutesPerWeek);
                fields[HoursWorkedIn12MonthsOrdinal] = Emit(HoursWorkedIn12Months);
                fields[WorkTimeSunOrdinal] = Emit(WorkTimeSun);
                fields[WorkTimeMonOrdinal] = Emit(WorkTimeMon);
                fields[WorkTimeTueOrdinal] = Emit(WorkTimeTue);
                fields[WorkTimeWedOrdinal] = Emit(WorkTimeWed);
                fields[WorkTimeThuOrdinal] = Emit(WorkTimeThu);
                fields[WorkTimeFriOrdinal] = Emit(WorkTimeFri);
                fields[WorkTimeSatOrdinal] = Emit(WorkTimeSat);
                fields[VariableScheduleOrdinal] = Emit(VariableSchedule);
                fields[JobDescriptionOrdinal] = Emit(JobDescription);
                fields[JobClassificationOrdinal] = Emit(JobClassification);
                fields[USCensusJobCategoryCodeOrdinal] = Emit(USCensusJobCategoryCode);
                fields[SOCCodeOrdinal] = Emit(SOCCode);
                fields[EmployeeClassOrdinal] = Emit(EmployeeClassification);
            }

            string record = string.Join(Delimiter.ToString(), fields);
            return record;
        } // ToString

        #endregion ToString

        #region Parsing

        public override bool Parse(string rowSource = null)
        {
            _parts = null;

            if (string.IsNullOrWhiteSpace(rowSource))
                return false;

            // Determine the delimiter (test for tabs, then for pipes, otherwise assume commas)
            if (rowSource.Contains('\t')) Delimiter = '\t';
            else if (rowSource.Contains('|')) Delimiter = '|';
            else Delimiter = ',';

            using (StringReader reader = new StringReader(rowSource))
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
            {
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters(Delimiter.ToString());
                if (!parser.EndOfData)
                    _parts = parser.ReadFields();
            }
            if (_parts == null || _parts.Length == 1)
                return false;

            //
            // Record Type
            if (Required(RecordTypeOrdinal, "Record Type") && In(RecordTypeOrdinal, "Record Type", "M", "E", "S", "U", "H", "C"))
                RecType = (RecordType)Convert.ToChar(_parts[RecordTypeOrdinal]);
            else
                return false;

            // =============
            // Shared Fields
            // =============
            //
            EmployeeNumber = Normalize(EmployeeNumberOrdinal);

            // =========================
            // Time Worked
            // =========================
            //
            if (RecType == RecordType.TimeWorked)
            {
                DateWorked = ParseDate(DateWorkedOrdinal, "Date Worked");
                TimeWorked = ParseInt(TimeWorkedOrdinal, "Time Worked");
            }
            // =========================
            // Eligibility
            // =========================
            //
            else if (RecType == RecordType.Eligibility)
            {
                LastName = Normalize(LastNameOrdinal);
                FirstName = Normalize(FirstNameOrdinal);
                MiddleName = Normalize(MiddleNameOrdinal);
                JobTitle = Normalize(JobTitleOrdinal);
                JobLocation = Normalize(JobLocationOrdinal);
                WorkState = Normalize(WorkStateOrdinal).ToUpperInvariant();
                WorkCountry = (Normalize(WorkCountryOrdinal) ?? "US").ToUpperInvariant();
                PhoneHome = Normalize(PhoneHomeOrdinal);
                PhoneWork = Normalize(PhoneWorkOrdinal);
                PhoneMobile = Normalize(PhoneMobileOrdinal);
                PhoneAlt = Normalize(PhoneAltOrdinal);
                Email = Normalize(EmailOrdinal);
                EmailAlt = Normalize(EmailAltOrdinal);
                Address = Normalize(AddressOrdinal);
                Address2 = Normalize(Address2Ordinal);
                City = Normalize(CityOrdinal);
                State = Normalize(StateOrdinal);
                PostalCode = Normalize(PostalCodeOrdinal);
                Country = Normalize(CountryOrdinal);
                EmploymentType = Normalize(EmploymentTypeOrdinal);
                ManagerEmployeeNumber = Normalize(ManagerEmployeeNumberOrdinal);
                ManagerLastName = Normalize(ManagerLastNameOrdinal);
                ManagerFirstName = Normalize(ManagerFirstNameOrdinal);
                ManagerPhone = Normalize(ManagerPhoneOrdinal);
                ManagerEmail = Normalize(ManagerEmailOrdinal);
                HREmployeeNumber = Normalize(HREmployeeNumberOrdinal);
                HRLastName = Normalize(HRLastNameOrdinal);
                HRFirstName = Normalize(HRFirstNameOrdinal);
                HRPhone = Normalize(HRPhoneOrdinal);
                HREmail = Normalize(HREmailOrdinal);
                HRDesignation = ParseBool(HRDesignationOrdinal, "HR Designation");
                SpouseEmployeeNumber = Normalize(SpouseEmployeeNumberOrdinal);
                DateOfBirth = ParseDate(DateOfBirthOrdinal, "Date of Birth");
                Gender = ParseChar(GenderOrdinal, "Date of Birth");
                ExemptionStatus = ParseChar(ExemptionStatusOrdinal, "Exemption Status");
                Meets50In75 = ParseBool(Meets50In75Ordinal, "Meets 50 in 75");
                KeyEmployee = ParseBool(KeyEmployeeOrdinal, "Key Employee");
                MilitaryStatus = ParseByte(MilitaryStatusOrdinal, "Military Status");
                EmploymentStatus = ParseChar(EmploymentStatusOrdinal, "Employment Status");
                TerminationDate = ParseDate(TerminationDateOrdinal, "Termination Date");
                PayRate = ParseDecimal(PayRateOrdinal, "Pay Rate");
                PayType = ParseByte(PayTypeOrdinal, "Pay Type");
                HireDate = ParseDate(HireDateOrdinal, "Hire Date");
                RehireDate = ParseDate(RehireDateOrdinal, "Rehire Date");
                AdjustedServiceDate = ParseDate(AdjustedServiceDateOrdinal, "Adjusted Service Date");
                MinutesPerWeek = ParseInt(MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                HoursWorkedIn12Months = ParseDecimal(HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                WorkTimeSun = ParseInt(WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                WorkTimeMon = ParseInt(WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                WorkTimeTue = ParseInt(WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                WorkTimeWed = ParseInt(WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                WorkTimeThu = ParseInt(WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                WorkTimeFri = ParseInt(WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                WorkTimeSat = ParseInt(WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                VariableSchedule = ParseBool(VariableScheduleOrdinal, "Variable Schedule Flag");
                JobDescription = Normalize(JobDescriptionOrdinal);
                JobClassification = ParseByte(JobClassificationOrdinal, "Job Classification");
                USCensusJobCategoryCode = Normalize(USCensusJobCategoryCodeOrdinal);
                SOCCode = Normalize(SOCCodeOrdinal);
                EmployeeClassification = Normalize(EmployeeClassOrdinal);
            }
            // =========================
            // Spouse
            // =========================
            //
            else if (RecType == RecordType.Spouse)
            {
                SpouseEmployeeNumber = Normalize(SpouseSpouseEmployeeNumberOrdinal);
            }
            // =========================
            // HR
            // =========================
            //
            else if (RecType == RecordType.HR)
            {
                HREmployeeNumber = Normalize(ContactEmployeeNumberOrdinal);
                HRLastName = Normalize(ContactLastNameOrdinal);
                HRFirstName = Normalize(ContactFirstNameOrdinal);
                HRPhone = Normalize(ContactPhoneOrdinal);
                HREmail = Normalize(ContactEmailOrdinal);
            }
            // =========================
            // Supervisor
            // =========================
            //
            else if (RecType == RecordType.Supervisor)
            {
                ManagerEmployeeNumber = Normalize(ContactEmployeeNumberOrdinal);
                ManagerLastName = Normalize(ContactLastNameOrdinal);
                ManagerFirstName = Normalize(ContactFirstNameOrdinal);
                ManagerPhone = Normalize(ContactPhoneOrdinal);
                ManagerEmail = Normalize(ContactEmailOrdinal);
            }

            return true;
        }
        
        #endregion Parsing

        #region Merge

        public static List<string> Merge(List<string> rowSources)
        {
            return Merge(rowSources.Select(r => new EligibilityRow(r)).ToList())
                .Select(r => r.ToString()).ToList();
        }

        public static List<EligibilityRow> Merge(List<EligibilityRow> rowSources)
        {
            List<EligibilityRow> rows = new List<EligibilityRow>();
            foreach (var source in rowSources)
            {
                EligibilityRow row = source;

                EligibilityRow target = rows.FirstOrDefault(t => t.RecType == row.RecType);
                if (target == null || row.RecType == RecordType.HR) // Parrot HACK: Include all HR contacts, only 1 supervisor though
                {
                    target = row;
                    rows.Add(target);
                    continue;
                }

                var coalesce = new Func<string, string, string>((a, b) => string.IsNullOrWhiteSpace(a) ? string.IsNullOrWhiteSpace(b) ? null : b : a);

                target.Address = coalesce(target.Address, row.Address);
                target.Address2 = coalesce(target.Address2, row.Address2);
                target.AdjustedServiceDate = target.AdjustedServiceDate ?? row.AdjustedServiceDate;
                target.City = coalesce(target.City, row.City);
                target.Country = coalesce(target.Country, row.Country);
                target.DateOfBirth = target.DateOfBirth ?? row.DateOfBirth;
                target.DateWorked = target.DateWorked ?? row.DateWorked;
                target.Email = coalesce(target.Email, row.Email);
                target.EmailAlt = coalesce(target.EmailAlt, row.EmailAlt);
                target.EmployeeNumber = coalesce(target.EmployeeNumber, row.EmployeeNumber);
                target.EmploymentStatus = target.EmploymentStatus ?? row.EmploymentStatus;
                target.EmploymentType = target.EmploymentType ?? row.EmploymentType;
                target.ExemptionStatus = target.ExemptionStatus ?? row.ExemptionStatus;
                target.FirstName = coalesce(target.FirstName, row.FirstName);
                target.Gender = target.Gender ?? row.Gender;
                target.HireDate = target.HireDate ?? row.HireDate;
                target.HoursWorkedIn12Months = target.HoursWorkedIn12Months ?? row.HoursWorkedIn12Months;
                target.HRDesignation = target.HRDesignation ?? row.HRDesignation;
                target.HREmail = coalesce(target.HREmail, row.HREmail);
                target.HREmployeeNumber = coalesce(target.HREmployeeNumber, row.HREmployeeNumber);
                target.HRFirstName = coalesce(target.HRFirstName, row.HRFirstName);
                target.HRLastName = coalesce(target.HRLastName, row.HRLastName);
                target.HRPhone = coalesce(target.HRPhone, row.HRPhone);
                target.JobClassification = target.JobClassification ?? row.JobClassification;
                target.JobDescription = coalesce(target.JobDescription, row.JobDescription);
                target.JobLocation = coalesce(target.JobLocation, row.JobLocation);
                target.JobTitle = coalesce(target.JobTitle, row.JobTitle);
                target.KeyEmployee = target.KeyEmployee ?? row.KeyEmployee;
                target.LastName = coalesce(target.LastName, row.LastName);
                target.ManagerEmail = coalesce(target.ManagerEmail, row.ManagerEmail);
                target.ManagerEmployeeNumber = coalesce(target.ManagerEmployeeNumber, row.ManagerEmployeeNumber);
                target.ManagerFirstName = coalesce(target.ManagerFirstName, row.ManagerFirstName);
                target.ManagerLastName = coalesce(target.ManagerLastName, row.ManagerLastName);
                target.ManagerPhone = coalesce(target.ManagerPhone, row.ManagerPhone);
                target.Meets50In75 = target.Meets50In75 ?? row.Meets50In75;
                target.MiddleName = coalesce(target.MiddleName, row.MiddleName);
                target.MilitaryStatus = target.MilitaryStatus ?? row.MilitaryStatus;
                target.MinutesPerWeek = target.MinutesPerWeek ?? row.MinutesPerWeek;
                target.PayRate = target.PayRate ?? row.PayRate;
                target.PayType = target.PayType ?? row.PayType;
                target.PhoneAlt = coalesce(target.PhoneAlt, row.PhoneAlt);
                target.PhoneHome = coalesce(target.PhoneHome, row.PhoneHome);
                target.PhoneMobile = coalesce(target.PhoneMobile, row.PhoneMobile);
                target.PhoneWork = coalesce(target.PhoneWork, row.PhoneWork);
                target.PostalCode = coalesce(target.PostalCode, row.PostalCode);
                target.RehireDate = target.RehireDate ?? row.RehireDate;
                target.SOCCode = coalesce(target.SOCCode, row.SOCCode);
                target.SpouseEmployeeNumber = coalesce(target.SpouseEmployeeNumber, row.SpouseEmployeeNumber);
                target.State = coalesce(target.State, row.State);
                target.TerminationDate = target.TerminationDate ?? row.TerminationDate;
                target.TimeWorked = target.TimeWorked ?? row.TimeWorked;
                target.USCensusJobCategoryCode = coalesce(target.USCensusJobCategoryCode, row.USCensusJobCategoryCode);
                target.VariableSchedule = target.VariableSchedule ?? row.VariableSchedule;
                target.WorkCountry = coalesce(target.WorkCountry, row.WorkCountry);
                target.WorkState = coalesce(target.WorkState, row.WorkState);
                target.WorkTimeFri = target.WorkTimeFri ?? row.WorkTimeFri;
                target.WorkTimeMon = target.WorkTimeMon ?? row.WorkTimeMon;
                target.WorkTimeSat = target.WorkTimeSat ?? row.WorkTimeSat;
                target.WorkTimeSun = target.WorkTimeSun ?? row.WorkTimeSun;
                target.WorkTimeThu = target.WorkTimeThu ?? row.WorkTimeThu;
                target.WorkTimeTue = target.WorkTimeTue ?? row.WorkTimeTue;
                target.WorkTimeWed = target.WorkTimeWed ?? row.WorkTimeWed;
                target.CostCenterCode = coalesce(target.CostCenterCode, row.CostCenterCode);
                target.CostCenterName = coalesce(target.CostCenterName, row.CostCenterName);
                target.EmployeeClassification = coalesce(target.EmployeeClassification, row.EmployeeClassification);
            }

            return rows;
        }

        #endregion
    }
}
