﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace ParrotELTranslator.Logic
{
    /// <summary>
    /// Just the most amazing class ever that pretty much does everything that anything in the entire
    /// world has ever done, EVER 'cause it's AMAZING!
    /// </summary>
    public static class AllThatJazz
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string META_SpouseEmployeeNumber = "SpouseEmployeeNumber";
        private const string META_RelatedEmployeeNumber = "RelatedEmployeeNumber";
        private const string META_ContactTypeCode = "ContactTypeCode";
        private const string META_ContactLevel = "Level";

        public class Glitter
        {
            public Glitter() { Success = true; Errors = new List<string>(); }
            public bool Success { get; set; }
            public List<string> Errors { get; set; }
        }

        private static AbsenceSoft.Logic.Customers.EmployeeService EmployeeService = new AbsenceSoft.Logic.Customers.EmployeeService();
        private static List<CustomField> customFields = null;

        //HRBP
        /// <summary>
        /// Sparkles the specified employee rows with JAZZ!
        /// </summary>
        /// <param name="employeeRows">The dull employee rows that should become dance shiny.</param>
        /// <param name="bulkEmployees">The bulk employees.</param>
        /// <param name="bulkContacts">The bulk contacts.</param>
        /// <param name="bulkTodos">The bulk todos.</param>
        /// <returns>
        /// The sparkle
        /// </returns>
        /// <exception cref="AbsenceSoftException">EmployeeService: Invalid Work Schedule</exception>
        public static Glitter Sparkle(this List<EligibilityRow> employeeRows, ref BulkWriteOperation<Employee> bulkEmployees, ref BulkWriteOperation<EmployeeContact> bulkContacts, ref BulkWriteOperation<ToDoItem> bulkTodos, ref BulkWriteOperation<Organization> bulkOrganization, ref BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, List<string> officeCodes)
        {
            Glitter shiny = new Glitter();

            var elRow = employeeRows.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.Eligibility);
            var spRow = employeeRows.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.Spouse);
            var supRow = employeeRows.FirstOrDefault(r => r.RecType == EligibilityRow.RecordType.Supervisor);
            //var hrRows = employeeRows.Where(r => r.RecType == EligibilityRow.RecordType.HR);

            string employeeNumber = (elRow ?? spRow ?? supRow ?? new EligibilityRow()).EmployeeNumber;
            if (string.IsNullOrWhiteSpace(employeeNumber))
            {
                shiny.Success = false;
                shiny.Errors.Add("No record with a Employee HRID was provided; Employee HRID is required");
                return shiny;
            }

            var emp = Program.CurrentEmployees.FirstOrDefault(e => e.EmployeeNumber == employeeNumber);
            if (emp == null && (elRow == null || string.IsNullOrWhiteSpace(elRow.LastName)))
            {
                shiny.Success = false;
                shiny.Errors.AddFormat("No employee found with HRID '{0}' (new employee?) however no EP data was provided for '{0}' in the same file or EP data was incomplete", employeeNumber);
                return shiny;
            }

            var hashMe = new Func<EligibilityRow, string>(s => s == null ? null : Convert.ToBase64String(System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(Regex.Replace(s.ToString().ToLowerInvariant(), @"[^a-zA-Z0-9]", "")))));
            var empHash = hashMe(elRow);
            var supHash = hashMe(supRow);
            if (supRow != null && emp != null && supHash == emp.Metadata.GetRawValue<string>("LastSupRow"))
                supRow = null;
            //hrRows = hrRows.Where(h => emp == null || hashMe(h) != emp.Metadata.GetRawValue<string>("LastHRRow" + h.HRLevel.ToString())).ToList();

            #region Eligibility Row

            bool isNew = emp == null;

            if (elRow != null && (emp == null || empHash != emp.Metadata.GetRawValue<string>("LastELRow")))
            {
                if (emp == null)
                {
                    emp = new Employee();
                    emp.SetCreatedDate(DateTime.UtcNow);
                    emp.Id = ObjectId.GenerateNewId().ToString();
                    emp.CustomerId = Program.CustomerId;
                    emp.EmployerId = Program.EmployerId;
                    emp.CreatedById = User.DefaultUserId;
                    emp.EmployeeNumber = employeeNumber;
                }
                emp.ModifiedById = User.DefaultUserId;
                emp.Metadata.SetRawValue("LastELRow", empHash);

                // Setup
                emp.Info = emp.Info ?? new EmployeeInfo();
                emp.Info.Address = emp.Info.Address ?? new Address();

                // Set Basic Properties (These are the easy ones)
                emp.LastName = elRow.LastName ?? emp.LastName;
                emp.FirstName = elRow.FirstName ?? emp.FirstName;
                emp.MiddleName = elRow.MiddleName ?? emp.MiddleName;
                emp.JobTitle = elRow.JobTitle ?? emp.JobTitle;
                emp.Info.OfficeLocation = elRow.JobLocation ?? emp.Info.OfficeLocation;
                if (!string.IsNullOrEmpty(elRow.JobLocation))
                {
                    UpdateOrganization(emp, elRow.JobLocation, ref bulkOrganization, ref bulkEmployeeOrganization, officeCodes);
                }
                emp.WorkState = elRow.WorkState ?? emp.WorkState;
                emp.WorkCountry = elRow.WorkCountry ?? emp.WorkCountry ?? "US";
                emp.Info.HomePhone = elRow.PhoneHome ?? emp.Info.HomePhone;
                emp.Info.WorkPhone = elRow.PhoneWork ?? emp.Info.WorkPhone;
                emp.Info.CellPhone = elRow.PhoneMobile ?? emp.Info.CellPhone;
                emp.Info.AltPhone = elRow.PhoneAlt ?? emp.Info.AltPhone;
                emp.Info.Email = elRow.Email ?? emp.Info.Email;
                emp.Info.AltEmail = elRow.EmailAlt ?? emp.Info.AltEmail;
                emp.Info.Address.Address1 = elRow.Address ?? emp.Info.Address.Address1;
                emp.Info.Address.Address2 = elRow.Address2 ?? emp.Info.Address.Address2;
                emp.Info.Address.City = elRow.City ?? emp.Info.Address.City;
                emp.Info.Address.State = elRow.State ?? emp.Info.Address.State;
                emp.Info.Address.PostalCode = elRow.PostalCode ?? emp.Info.Address.PostalCode;
                emp.Info.Address.Country = elRow.Country ?? emp.Info.Address.Country;
                emp.EmployeeClassCode = String.IsNullOrWhiteSpace(elRow.EmploymentType) ? emp.EmployeeClassCode : GetEmployementType(elRow.EmploymentType);
                emp.DoB = elRow.DateOfBirth ?? emp.DoB;
                emp.Gender = (Gender?)elRow.Gender ?? emp.Gender;
                emp.IsExempt = elRow.ExemptionStatus == 'E' ? true : emp.IsExempt;
                emp.Meets50In75MileRule = elRow.Meets50In75 ?? emp.Meets50In75MileRule;
                emp.IsKeyEmployee = elRow.KeyEmployee ?? emp.IsKeyEmployee;
                emp.MilitaryStatus = (MilitaryStatus)(elRow.MilitaryStatus ?? (byte)emp.MilitaryStatus);
                emp.TerminationDate = elRow.TerminationDate ?? emp.TerminationDate;
                emp.Salary = (double?)elRow.PayRate ?? emp.Salary;
                emp.PayType = (PayType?)elRow.PayType ?? emp.PayType;
                emp.HireDate = elRow.HireDate ?? emp.HireDate;
                emp.RehireDate = elRow.RehireDate ?? emp.RehireDate;
                emp.ServiceDate = elRow.AdjustedServiceDate ?? emp.ServiceDate;
                emp.Department = !string.IsNullOrEmpty(elRow.CostCenterCode) && Departments.ContainsKey(elRow.CostCenterCode) ? Departments[elRow.CostCenterCode] : null;
                emp.CostCenterCode = elRow.CostCenterCode;
                emp.Metadata.SetRawValue("CostCenterName", elRow.CostCenterName);

                // Custom fields...
                if (!string.IsNullOrWhiteSpace(elRow.EmployeeClassification))
                {
                    customFields = customFields ?? CustomField.AsQueryable().Where(f => f.CustomerId == emp.CustomerId && f.EmployerId == emp.EmployerId).ToList()
                        .Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee)
                        .ToList();

                    emp.CustomFields = emp.CustomFields ?? new List<CustomField>(1);

                    var empClass = customFields.FirstOrDefault(c => c.Name == "EmployeeClass");
                    if (empClass != null)
                    {
                        var myEmpClass = emp.CustomFields.FirstOrDefault(c => c.Name == empClass.Name);
                        if (myEmpClass == null)
                            myEmpClass = emp.CustomFields.AddFluid(empClass.Clone());

                        elRow.EmployeeClassification = elRow.EmployeeClassification.Trim();
                        if (!myEmpClass.ListValues.Any(v => v.Key == elRow.EmployeeClassification))
                            myEmpClass.ListValues.Add(new ListItem() { Key = elRow.EmployeeClassification, Value = elRow.EmployeeClassification });

                        myEmpClass.SelectedValue = elRow.EmployeeClassification;
                    }
                }

                if (elRow.HRDesignation.HasValue)
                {
                    customFields = customFields ?? customFields ?? CustomField.AsQueryable().Where(f => f.CustomerId == emp.CustomerId && f.EmployerId == emp.EmployerId).ToList()
                        .Where(f => (f.Target & EntityTarget.Employee) == EntityTarget.Employee)
                        .ToList();

                    emp.CustomFields = emp.CustomFields ?? new List<CustomField>(1);

                    var hrAssociate = customFields.FirstOrDefault(c => c.Name == "HRAssociate");
                    if (hrAssociate != null)
                    {
                        var myHRAssociate = emp.CustomFields.FirstOrDefault(c => c.Name == hrAssociate.Name);
                        if (myHRAssociate == null)
                            myHRAssociate = emp.CustomFields.AddFluid(hrAssociate.Clone());

                        myHRAssociate.SelectedValue = elRow.HRDesignation.Value;
                    }


                }

                // Create TODO items for any open cases if this employee is being marked as terminated.
                if (!isNew && emp.Status != EmploymentStatus.Terminated && elRow.EmploymentStatus == 'T')
                {
                    var cases = Program.CurrentOpenCases.Where(t => t.Employee.Id == emp.Id).ToList();
                    if (cases.Count > 0)
                    {
                        if (bulkTodos == null)
                            bulkTodos = ToDoItem.Repository.Collection.InitializeUnorderedBulkOperation();
                        foreach (var todo in cases.Select(c => new ToDoItem()
                        {
                            Title = "Employee Terminated; Review Open Case",
                            CreatedById = User.DefaultUserId,
                            ModifiedById = User.DefaultUserId,
                            CustomerId = emp.CustomerId,
                            EmployerId = emp.EmployerId,
                            AssignedToId = User.DefaultUserId,
                            CaseId = c.Id,
                            EmployeeId = c.Employee.Id,
                            EmployeeName = c.Employee.FullName,
                            CaseNumber = c.CaseNumber,
                            DueDate = DateTime.UtcNow,
                            ItemType = ToDoItemType.CaseReview,
                            Optional = false,
                            Priority = ToDoItemPriority.High,
                            Status = ToDoItemStatus.Pending,
                            Weight = 0
                        }))
                            bulkTodos.Insert(todo);
                    }
                }
                if (elRow.EmploymentStatus.HasValue)
                    emp.Status = (EmploymentStatus)elRow.EmploymentStatus.Value;

                if (elRow.HoursWorkedIn12Months.HasValue)
                {
                    emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
                    emp.PriorHours.Add(new PriorHours() { HoursWorked = (double)elRow.HoursWorkedIn12Months.Value, AsOf = DateTime.UtcNow.ToMidnight() });
                }
                else
                    emp.PriorHours = new List<PriorHours>(0);

                Schedule workSched = null;
                // If it's a variable schedule, we can't mess with that here, regardless of what they send in for other stuff, 'cause
                //  it has to get filled in via the other record type, hours worked.
                if (elRow.VariableSchedule != true)
                {
                    workSched = new Schedule();
                    workSched.StartDate = isNew ? (emp.HireDate ?? emp.ServiceDate ?? DateTime.UtcNow).ToMidnight() : DateTime.UtcNow.ToMidnight();

                    if (!elRow.WorkTimeSun.HasValue && !elRow.WorkTimeMon.HasValue && !elRow.WorkTimeTue.HasValue && !elRow.WorkTimeWed.HasValue && !elRow.WorkTimeThu.HasValue && !elRow.WorkTimeFri.HasValue && !elRow.WorkTimeSat.HasValue)
                    {
                        if (!elRow.MinutesPerWeek.HasValue)
                            workSched = null;
                        else
                        {
                            DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                            int dailyMinutes = Convert.ToInt32(Math.Floor((decimal)elRow.MinutesPerWeek.Value / 5m));
                            int leftOverMinute = elRow.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                            for (var d = 0; d < 7; d++)
                            {
                                if ((int)sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                    workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                                else
                                    workSched.Times.Add(new Time() { SampleDate = sample });
                                sample = sample.AddDays(1);
                            }
                            // Add our leftover minute to the first date, clugey, but it's whatever.
                            workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                        }
                    }
                    else
                    {
                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                        for (var d = 0; d < 7; d++)
                        {
                            int? workTime = null;
                            switch (sample.DayOfWeek)
                            {
                                case DayOfWeek.Sunday: workTime = elRow.WorkTimeSun; break;
                                case DayOfWeek.Monday: workTime = elRow.WorkTimeMon; break;
                                case DayOfWeek.Tuesday: workTime = elRow.WorkTimeTue; break;
                                case DayOfWeek.Wednesday: workTime = elRow.WorkTimeWed; break;
                                case DayOfWeek.Thursday: workTime = elRow.WorkTimeThu; break;
                                case DayOfWeek.Friday: workTime = elRow.WorkTimeFri; break;
                                case DayOfWeek.Saturday: workTime = elRow.WorkTimeSat; break;
                            }
                            workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                            sample = sample.AddDays(1);
                        }
                    }

                    if (!isNew && workSched != null)
                    {
                        Schedule dup = emp.WorkSchedules == null ? null : emp.WorkSchedules.FirstOrDefault(s => workSched.StartDate.DateRangesOverLap(workSched.EndDate, s.StartDate, s.EndDate) && s.ScheduleType == workSched.ScheduleType);
                        if (dup != null && dup.Times.Count == workSched.Times.Count && dup.Times.Sum(t => t.TotalMinutes ?? 0) == workSched.Times.Sum(t => t.TotalMinutes ?? 0))
                        {
                            // So far, these schedules appear to be the same (perhaps other than start date) and we don't want to duplicate a lot of stuff
                            bool isMatch = true;
                            foreach (var time in workSched.Times.OrderBy(t => t.SampleDate.DayOfWeek))
                                if (!dup.Times.Any(t => t.SampleDate.DayOfWeek == time.SampleDate.DayOfWeek && t.TotalMinutes == time.TotalMinutes))
                                {
                                    isMatch = false;
                                    break;
                                }
                            // See if the schedules still look the same
                            if (isMatch)
                                workSched = null; // we're not going to pass a new one in, it's the same; null this one out
                        }
                    }
                }

                using (AbsenceSoft.Logic.Customers.EmployeeService empSvc = new AbsenceSoft.Logic.Customers.EmployeeService())
                {
                    // Set the EE work schedule.
                    empSvc.SetWorkSchedule(emp, workSched, null);

                    // Set IsExempt to true if IsKeyEmployee is set to true. This does NOT apply to the converse situation
                    if (emp.IsKeyEmployee && !emp.IsExempt)
                        emp.IsExempt = true;
                }

                if (bulkEmployees == null)
                    bulkEmployees = Employee.Repository.Collection.InitializeUnorderedBulkOperation();

                emp.SetModifiedDate(DateTime.UtcNow);

                emp.Metadata.SetRawValue("LastSupRow", supHash);
                //foreach (var hr in hrRows)
                //    emp.Metadata.SetRawValue("LastHRRow" + hr.HRLevel.ToString(), hashMe(hr));

                if (isNew) bulkEmployees.Insert(emp);
                else bulkEmployees.Find(Employee.Query.EQ(e => e.Id, emp.Id)).ReplaceOne(emp);

                EmployeeService.ProcessUpdates(emp, Program.CurrentOpenCases.Where(t => t.Employee.Id == emp.Id).ToList(), User.System);
            }

            #endregion

            #region Supervisor

            if (supRow != null)
            {
                if (!string.IsNullOrWhiteSpace(supRow.ManagerEmployeeNumber) ||
                    !string.IsNullOrWhiteSpace(supRow.ManagerLastName) ||
                    !string.IsNullOrWhiteSpace(supRow.ManagerFirstName) ||
                    !string.IsNullOrWhiteSpace(supRow.ManagerPhone) ||
                    !string.IsNullOrWhiteSpace(supRow.ManagerEmail))
                {
                    if (bulkContacts == null)
                        bulkContacts = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();

                    if (string.IsNullOrWhiteSpace(supRow.ManagerEmployeeNumber) && !string.IsNullOrWhiteSpace(supRow.ManagerEmail))
                    {
                        var match = Program.CurrentEmployees.FirstOrDefault(e => e.Info != null && e.Info.Email != null && e.Info.Email.ToLowerInvariant() == supRow.ManagerEmail.ToLowerInvariant());
                        if (match != null)
                            supRow.ManagerEmployeeNumber = match.EmployeeNumber;
                    }

                    if (isNew)
                        bulkContacts.Insert(new EmployeeContact()
                        {
                            CustomerId = emp.CustomerId,
                            EmployerId = emp.EmployerId,
                            EmployeeId = emp.Id,
                            CreatedById = User.DefaultUserId,
                            ModifiedById = User.DefaultUserId,
                            ContactTypeCode = "SUPERVISOR",
                            MilitaryStatus = MilitaryStatus.Civilian,
                            RelatedEmployeeNumber = supRow.ManagerEmployeeNumber,
                            Contact = new Contact()
                            {
                                LastName = supRow.ManagerLastName,
                                FirstName = supRow.ManagerFirstName,
                                WorkPhone = supRow.ManagerPhone,
                                Email = supRow.ManagerEmail
                            }
                        }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                    else
                        bulkContacts.Find(
                            EmployeeContact.Query.And(
                                EmployeeContact.Query.EQ(c => c.EmployeeId, emp.Id),
                                EmployeeContact.Query.EQ(c => c.ContactTypeCode, "SUPERVISOR")
                            ))
                            .Upsert()
                            .UpdateOne(
                                Update.SetOnInsert("CustomerId", emp.CustomerId.ToBsonObjectId())
                                    .SetOnInsert("EmployerId", emp.EmployerId.ToBsonObjectId())
                                    .SetOnInsert("EmployeeId", emp.Id.ToBsonObjectId())
                                    .SetOnInsert("cby", User.DefaultUserId.ToBsonObjectId())
                                    .SetOnInsert("cdt", BsonNow())
                                    .SetOnInsert("ContactTypeCode", "SUPERVISOR".ToBsonValue())
                                    .SetOnInsert("MilitaryStatus", new BsonInt32((int)MilitaryStatus.Civilian))
                                    .SetOnInsert("Contact._id", Guid.NewGuid().ToString().ToBsonValue())
                                    .SetOnInsert("Contact.Address._id", Guid.NewGuid().ToString().ToBsonValue())
                                    .SetOnInsert("Contact.Address.Country", "US".ToBsonValue())
                                    .Set("mby", User.DefaultUserId.ToBsonObjectId())
                                    .Set("mdt", BsonNow())
                                    .Set("Contact.LastName", supRow.ManagerLastName.ToBsonValue())
                                    .Set("Contact.FirstName", supRow.ManagerFirstName.ToBsonValue())
                                    .Set("Contact.WorkPhone", supRow.ManagerPhone.ToBsonValue())
                                    .Set("Contact.Email", supRow.ManagerEmail.ToBsonValue())
                                    .Set(META_RelatedEmployeeNumber, supRow.ManagerEmployeeNumber.ToBsonValue())
                            );
                }
            }

            #endregion

            #region HRBP's

            //foreach (var hr in hrRows)
            //{
            //    if (!string.IsNullOrWhiteSpace(hr.HREmployeeNumber) ||
            //        !string.IsNullOrWhiteSpace(hr.HRLastName) ||
            //        !string.IsNullOrWhiteSpace(hr.HRFirstName) ||
            //        !string.IsNullOrWhiteSpace(hr.HRPhone) ||
            //        !string.IsNullOrWhiteSpace(hr.HREmail))
            //    {
            //        if (bulkContacts == null)
            //            bulkContacts = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();

            //        if (string.IsNullOrWhiteSpace(hr.HREmployeeNumber) && !string.IsNullOrWhiteSpace(hr.HREmail))
            //        {
            //            var match = Program.CurrentEmployees.FirstOrDefault(e => e.Info != null && e.Info.Email != null && e.Info.Email.ToLowerInvariant() == hr.HREmail.ToLowerInvariant());
            //            if (match != null)
            //                hr.HREmployeeNumber = match.EmployeeNumber;
            //        }

            //        if (isNew)
            //            bulkContacts.Insert(new EmployeeContact()
            //            {
            //                CustomerId = emp.CustomerId,
            //                EmployerId = emp.EmployerId,
            //                EmployeeId = emp.Id,
            //                CreatedById = User.DefaultUserId,
            //                ModifiedById = User.DefaultUserId,
            //                ContactTypeCode = "HR",
            //                ContactTypeName = "HR",
            //                MilitaryStatus = MilitaryStatus.Civilian,
            //                RelatedEmployeeNumber = hr.HREmployeeNumber,
            //                Contact = new Contact()
            //                {
            //                    LastName = hr.HRLastName,
            //                    FirstName = hr.HRFirstName,
            //                    WorkPhone = hr.HRPhone,
            //                    Email = hr.HREmail
            //                },
            //                Metadata = new BsonDocument().SetRawValue(META_ContactLevel, hr.HRLevel)
            //            }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
            //        else
            //        {
            //            // Upsert this guy
            //            bulkContacts.Find(
            //                EmployeeContact.Query.And(
            //                EmployeeContact.Query.EQ(c => c.EmployeeId, emp.Id),
            //                EmployeeContact.Query.EQ(c => c.ContactTypeCode, "HR"),
            //                Query.EQ(META_ContactLevel, hr.HRLevel)
            //            ))
            //            .Upsert()
            //            .UpdateOne(
            //                Update.SetOnInsert("CustomerId", emp.CustomerId.ToBsonObjectId())
            //                    .SetOnInsert("EmployerId", emp.EmployerId.ToBsonObjectId())
            //                    .SetOnInsert("EmployeeId", emp.Id.ToBsonObjectId())
            //                    .SetOnInsert("cby", User.DefaultUserId.ToBsonObjectId())
            //                    .SetOnInsert("cdt", BsonNow())
            //                    .SetOnInsert("ContactTypeCode", "HR".ToBsonValue())
            //                    .SetOnInsert("ContactTypeName", "HR".ToBsonValue())
            //                    .SetOnInsert("MilitaryStatus", new BsonInt32((int)MilitaryStatus.Civilian))
            //                    .SetOnInsert("Contact._id", Guid.NewGuid().ToString().ToBsonValue())
            //                    .SetOnInsert("Contact.Address._id", Guid.NewGuid().ToString().ToBsonValue())
            //                    .SetOnInsert("Contact.Address.Country", "US".ToBsonValue())
            //                    .Set("mby", User.DefaultUserId.ToBsonObjectId())
            //                    .Set("mdt", BsonNow())
            //                    .Set("Contact.LastName", hr.HRLastName.ToBsonValue())
            //                    .Set("Contact.FirstName", hr.HRFirstName.ToBsonValue())
            //                    .Set("Contact.WorkPhone", hr.HRPhone.ToBsonValue())
            //                    .Set("Contact.Email", hr.HREmail.ToBsonValue())
            //                    .Set(META_RelatedEmployeeNumber, hr.HREmployeeNumber.ToBsonValue())
            //                    .Set(META_ContactLevel, hr.HRLevel.ToBsonValue())
            //            );

            //            // But ensure they do not exist for any other HR level (duplicates are bad)
            //            bulkContacts.Find(
            //                EmployeeContact.Query.And(
            //                EmployeeContact.Query.EQ(c => c.EmployeeId, emp.Id),
            //                EmployeeContact.Query.EQ(c => c.ContactTypeCode, "HR"),
            //                Query.NE(META_ContactLevel, hr.HRLevel.ToBsonValue()),
            //                Query.EQ(META_RelatedEmployeeNumber, hr.HREmployeeNumber.ToBsonValue())
            //            )).Remove();
            //        }
            //    }
            //}

            #endregion

            shiny.Success = true;
            return shiny;
        }

        public static BsonValue ToBsonValue(this string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                return BsonNull.Value;

            return new BsonString(val);
        }

        public static BsonValue ToBsonValue(this int? val)
        {
            if (val.HasValue) return new BsonInt32(val.Value);
            return BsonNull.Value;
        }

        public static BsonValue ToBsonValue(this DateTime? val)
        {
            if (val.HasValue) return new BsonDateTime(val.Value);
            return BsonNull.Value;
        }

        public static BsonValue BsonNow() { return new BsonDateTime(DateTime.UtcNow); }

        public static BsonValue ToBsonObjectId(this string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return BsonNull.Value;
            ObjectId val;
            if (ObjectId.TryParse(id, out val))
                return new BsonObjectId(val);
            return BsonNull.Value;
        }

        private static string GetEmployementType(string employmentType)
        {
            if(employmentType == "1")
            {
                return WorkType.FullTime;
            }
            if (employmentType == "2")
            {
                return WorkType.PartTime;
            }
            if (employmentType == "3")
            {
                return WorkType.PerDiem;
            }
            return employmentType;
        }
        private static void UpdateOrganization(Employee emp, string officeLocation, ref BulkWriteOperation<Organization> bulkOrganization, ref BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization, List<string> officeCodes)
        {
            if (string.IsNullOrWhiteSpace(officeLocation) || officeCodes == null || emp == null)
                return;
            string code = officeLocation.ToUpperInvariant();
            if (!officeCodes.Contains(code))
            {
                bulkOrganization = bulkOrganization ?? Organization.Repository.Collection.InitializeUnorderedBulkOperation();
                bulkEmployeeOrganization = bulkEmployeeOrganization ?? EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();
                bulkOrganization.Find(Organization.Query.And(
                    Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
                    Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
                    Organization.Query.EQ(o => o.Code, code)
                )).Upsert().UpdateOne(
                    Organization.Updates
                        .SetOnInsert(o => o.CustomerId, emp.CustomerId)
                        .SetOnInsert(o => o.EmployerId, emp.EmployerId)
                        .SetOnInsert(o => o.Code, code)
                        .SetOnInsert(o => o.Path, HierarchyPath.Root(code))
                        .SetOnInsert(o => o.TypeCode, OrganizationType.OfficeLocationTypeCode)
                        .SetOnInsert(o => o.TypeName, "Office Location")
                        .SetOnInsert(o => o.Name, officeLocation)
                        .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                        .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(o => o.ModifiedById, User.DefaultUserId)
                        .SetOnInsert(o => o.ModifiedDate, DateTime.UtcNow)
                );

                officeCodes.Add(code);
                UpdateOfficeLocation(emp, officeLocation, bulkEmployeeOrganization);
            }
        }

        private static void UpdateOfficeLocation(Employee emp, string officeLocation, BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization)
        {
            string code = officeLocation.ToUpperInvariant();
            bulkEmployeeOrganization.Find(EmployeeOrganization.Query.And(
                EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                )).Upsert().UpdateOne(
                    EmployeeOrganization.Updates
                    .SetOnInsert(eo => eo.CustomerId, emp.CustomerId)
                    .SetOnInsert(eo => eo.EmployerId, emp.EmployerId)
                    .SetOnInsert(eo => eo.Code, code)
                    .SetOnInsert(eo => eo.TypeCode, OrganizationType.OfficeLocationTypeCode)
                    .SetOnInsert(eo => eo.Name, officeLocation)
                    .SetOnInsert(eo => eo.Path, HierarchyPath.Root(code))
                    .SetOnInsert(eo => eo.EmployeeNumber, emp.EmployeeNumber)
                    .SetOnInsert(eo => eo.CreatedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.CreatedDate, DateTime.UtcNow)
                    .SetOnInsert(eo => eo.ModifiedById, User.DefaultUserId)
                    .SetOnInsert(eo => eo.ModifiedDate, DateTime.UtcNow)
                );
        }

        #region Departments
        public static readonly Dictionary<string, string> Departments;

        static AllThatJazz()
        {
            Departments = new Dictionary<string, string>();
            Departments["1"] = "HQ Amazon.com";
            Departments["7"] = "Amazon.com Tech";
            Departments["1000"] = "WW Operations Execution";
            Departments["100053"] = "RAD & SIM";
            Departments["1000AUD"] = "Operations Management - AUD";
            Departments["1000ENG"] = "Operations Management - ENG";
            Departments["1000FIN"] = "Operations Management - FIN";
            Departments["1000HR"] = "Operations Mgmt - HR";
            Departments["1000OPS"] = "Operations Management - OPS";
            Departments["1000RAD"] = "WW Operations Execution";
            Departments["1000SOP"] = "Operations Management - SOP";
            Departments["1001"] = "Global Customer Fulfillment";
            Departments["1001HR"] = "Global Customer Fulfillment HR";
            Departments["1002"] = "Emerging Markets";
            Departments["1003"] = "Amazon Connections HR";
            Departments["1004"] = "Prime Now FC Fixed";
            Departments["1004HR"] = "Project Houdini Fixed HR";
            Departments["1005"] = "Prime Now FC Variable";
            Departments["1005HR"] = "Project Houdini Variable HR";
            Departments["1006"] = "WW Ops Execution Engineering";
            Departments["1008"] = "WW Prime Now Operations";
            Departments["1008HR"] = "WW Prime Now Operations HR";
            Departments["1009"] = "Prime Now Retail Fixed";
            Departments["1010"] = "EU Ops Management";
            Departments["1020"] = "Japan Operations";
            Departments["1021"] = "China Operations";
            Departments["1030"] = "HL Leadership";
            Departments["1040"] = "India Operations";
            Departments["1080"] = "Customer Packaging Experience";
            Departments["1100"] = "Seattle DC";
            Departments["1100"] = "Seattle FC";
            Departments["1120"] = "Energy & Environment";
            Departments["1121"] = "Hazardous Materials";
            Departments["1122"] = "Social Responsibility";
            Departments["1150"] = "AMZL & Transportation Admin";
            Departments["1150HR"] = "Transportation Admin HR";
            Departments["1150Z"] = "Transportation Admin-Zappos";
            Departments["1151"] = "Amazon Locker";
            Departments["1160"] = "Transportation Execution";
            Departments["1160HR"] = "Transportation Execution - HR";
            Departments["1169"] = "Prime Now - Last Mile";
            Departments["1170"] = "Outbound Transportation";
            Departments["1171"] = "Amzn Logistic-LastMileDelivery";
            Departments["1172"] = "Amzn Logistics-DeliveryStation";
            Departments["1173"] = "Amazon Logistics - Sortation";
            Departments["1173HR"] = "Amazon Logistics-Sortation HR";
            Departments["1174"] = "Amazon Logistics - Corporate";
            Departments["1174HR"] = "Amazon Logistics -Corporate HR";
            Departments["1176"] = "Amzn Logistics-Global Strategy";
            Departments["1176HR"] = "Global TBA Strategy HR";
            Departments["1177"] = "Last Mile Technology";
            Departments["1178"] = "Amzn Logistics-SortCenterAdmin";
            Departments["1178HR"] = "Amzn Logistics-SCAdmin - HR";
            Departments["1179"] = "Project Mosaic";
            Departments["1180"] = "International Transportation";
            Departments["1181"] = "Sort Center Temp Staffing";
            Departments["1181HR"] = "Sort Center Temp Staffing HR";
            Departments["1182"] = "Sort Center Variable";
            Departments["1182HR"] = "Sort Center Variable HR";
            Departments["1183"] = "Sort Center Regional Managemnt";
            Departments["1184"] = "Amazon Logistics-TOC Support";
            Departments["1187"] = "Kindle Direct Publishing - CS";
            Departments["1189"] = "Operations Engineering";
            Departments["1190"] = "North American Fulfillment";
            Departments["1190Z"] = "North American Fulfillment-ZAP";
            Departments["1191"] = "FC Systems Management";
            Departments["119110"] = "SW Integration";
            Departments["119111"] = "HW Integration";
            Departments["119115"] = "Deployment Engineering";
            Departments["119120"] = "Production";
            Departments["119125"] = "Tech Ops";
            Departments["119127"] = "Quality";
            Departments["119128"] = "Ops Prgm Mgt";
            Departments["119150"] = "Development HW";
            Departments["119158"] = "HW Architecture";
            Departments["119159"] = "XB-15";
            Departments["119171"] = "Solutions";
            Departments["119173"] = "PMO";
            Departments["119174"] = "Performance Engineering";
            Departments["119185"] = "Corporate";
            Departments["119190"] = "Occupancy BOS12";
            Departments["1191HR"] = "FC Systems Management-HR";
            Departments["1194"] = "WW Amazon Fresh and Pantry Ops";
            Departments["1194HR"] = "WW Amazon Fresh &Pantry Ops HR";
            Departments["1195"] = "Global Security";
            Departments["1196"] = "WW Amz Cust Excellence Systems";
            Departments["1197"] = "WW Product Compliance";
            Departments["1198"] = "Global Trade Services";
            Departments["1199"] = "WW Food Safety and Compliance";
            Departments["1200"] = "Fulfillment Center";
            Departments["1200010"] = "FC Receiving(010)";
            Departments["1200020"] = "FC Replenishment";
            Departments["1200030"] = "FC Picking";
            Departments["1200030Z"] = "FC Picking - Zappos";
            Departments["1200040"] = "FC Shipping";
            Departments["1200050"] = "DC Returns";
            Departments["1200050"] = "FC Fulfillment";
            Departments["1200050"] = "FC Returns";
            Departments["1200060"] = "DC QA/IC";
            Departments["1200060"] = "FC Production";
            Departments["1200070"] = "FC Support";
            Departments["1200070Z"] = "FC Support";
            Departments["1200080"] = "DC Media";
            Departments["1200080"] = "FC Non Sortables";
            Departments["1200090"] = "DC Gift Center";
            Departments["12000VF"] = "FC Receiving(0VF)";
            Departments["1200100"] = "DC VDF";
            Departments["1200110"] = "DC Order Assembly";
            Departments["1200120"] = "DC Bundler";
            Departments["1200130"] = "DC Problem Solver";
            Departments["1200140"] = "DC Away Team";
            Departments["1200150"] = "DC Collating";
            Departments["1200160"] = "DC Customer Service";
            Departments["1200170"] = "DC Facilities";
            Departments["1200180"] = "DC Flexi";
            Departments["1200190"] = "DC Training";
            Departments["120020"] = "Packaging - (BA)";
            Departments["1200200"] = "DC Gift Wrap";
            Departments["1200210"] = "DC Non Conveyable";
            Departments["1200220"] = "DC Materials";
            Departments["1200230"] = "DC Slam";
            Departments["120024"] = "Duplication - (BA)";
            Departments["1200240"] = "DC Random Stow";
            Departments["1200250"] = "DC Crisplant";
            Departments["1200260"] = "DC Sample Depot";
            Departments["1200270"] = "DC Replenishment";
            Departments["120028"] = "Telex - (BA)";
            Departments["1200280"] = "DC Reprofiling";
            Departments["120062"] = "Direct Market Shipping (BA)";
            Departments["120092"] = "Maintenance - (BA)";
            Departments["120094"] = "Returns - (BA)";
            Departments["120099"] = "Shipping - (BA)";
            Departments["1200ALL"] = "Allentown FC";
            Departments["1200AVP"] = "Hazleton FC";
            Departments["1200BA"] = "Brilliance Audio Operations";
            Departments["1200BFI"] = "BFI US FC's";
            Departments["1200BNA"] = "BNA US FC's";
            Departments["1200BOS"] = "BOS1-FC";
            Departments["1200BWI"] = "BWI1-FC";
            Departments["1200CAE"] = "CAE US FC's";
            Departments["1200CAM"] = "Campbellsville FC";
            Departments["1200CF"] = "CreateSpace";
            Departments["1200CHA"] = "CHA US FC's";
            Departments["1200DAL"] = "Dallas FC";
            Departments["1200DEL"] = "Delaware FC";
            Departments["1200DFW"] = "DFW US FC's";
            Departments["1200FAB"] = "Fulfillment Center-FAB";
            Departments["1200FIN"] = "Fulfillment Center-FIN";
            Departments["1200FLA"] = "Small Parts";
            Departments["1200GA"] = "Distribution Center - Georgia";
            Departments["1200GA"] = "Fulfillment Center - Georgia";
            Departments["1200GSP"] = "GSP US FC's";
            Departments["1200HR"] = "FC HR";
            Departments["1200IND"] = "Indiana FC";
            Departments["1200KAN"] = "Kansas FC";
            Departments["1200LAS"] = "Las Vegas FC";
            Departments["1200LEX"] = "Lexington FC";
            Departments["1200LOU"] = "Louisville";
            Departments["1200MDT"] = "Fulfillment Center";
            Departments["1200MDW"] = "MDW1-FC";
            Departments["1200MOD"] = "Fulfillment Center MOD";
            Departments["1200ND"] = "North Dakota FC";
            Departments["1200NEV"] = "Fernley FC";
            Departments["1200OAK"] = "OAK US FC's";
            Departments["1200ONT"] = "ONT US FC's";
            Departments["1200PHL"] = "PHL FC";
            Departments["1200PHX"] = "Phoenix FC";
            Departments["1200RIC"] = "RIC US FC's";
            Departments["1200SAT"] = "SAT US FC's";
            Departments["1200SC"] = "CreateSpace Operations";
            Departments["1200SEA"] = "Seattle FC";
            Departments["1200WI"] = "ShopBop";
            Departments["1200Z"] = "Fulfillment Center - Zappos";
            Departments["1200ZFIN"] = "Fulfillment Center-Zappos-FIN";
            Departments["1200ZHR"] = "FC HR - Zappos";
            Departments["1202070"] = "Delivery Operations(070)";
            Departments["1203"] = "AmznFresh Fulfillment Variable";
            Departments["1203010"] = "FC Delivery";
            Departments["1203HR"] = "AmznFresh Fulfillment Var HR";
            Departments["1204"] = "Amazon Fresh Fulfillment";
            Departments["1204HR"] = "Amazon Fresh Fulfillment HR";
            Departments["1205"] = "Tech Services Ingestion";
            Departments["1206"] = "Fullfillment Service";
            Departments["1207"] = "Internal Service Fulfillment";
            Departments["1210"] = "FC Support - Local ACES";
            Departments["1210Z"] = "FC Support - NA ACES - Zappos";
            Departments["1211"] = "Amazon Temp Staffing Variable";
            Departments["1211010"] = "Amzn Tmp Staffng Var Receiving";
            Departments["1211020"] = "Amzn TmpStaffing Replenishment";
            Departments["1211030"] = "Amzn Tmp Staffing Var Picking";
            Departments["1211040"] = "Amzn Tmp Staffing Var Shipping";
            Departments["1211070"] = "Amzn Tmp Staffing Var Support";
            Departments["1211HR"] = "Amazon Temp Staffing Variable";
            Departments["1212"] = "Amazon Temp Staffing";
            Departments["1212HR"] = "Amazon Temp Staffing - HR";
            Departments["1213"] = "Amazon Pantry Fulfillment-Var";
            Departments["1213HR"] = "AMZ Pantry Fulfillment-Var HR";
            Departments["1214"] = "Amazon Pantry Fulfillment-Fix";
            Departments["1218"] = "Quality & Loss Prevention";
            Departments["1219"] = "Environmental Health & Safety";
            Departments["1220"] = "Locker & Pickup & On My Way";
            Departments["1220070"] = "Fulfillment IT Support(070)";
            Departments["122080"] = "IS";
            Departments["1220HR"] = "Fulfillment IT Support HR";
            Departments["1221"] = "FC - HR Regional Management";
            Departments["1222"] = "FC-Finance Regional Management";
            Departments["1223"] = "FC-Regional Procurement/Other";
            Departments["1224"] = "Ops HR Management Recruiting";
            Departments["1225"] = "FC - HR Operations Support";
            Departments["1225Z"] = "FC-HR OperationsSupport-Zappos";
            Departments["1226"] = "FC - HR Staffing Engine";
            Departments["1226Z"] = "FC-HR Staffing Engine - Zappos";
            Departments["1227"] = "Local Trans & Supply Chain";
            Departments["1228"] = "FC Launch";
            Departments["1229"] = "Ops-Learning&Leadership Dvlpmt";
            Departments["1230"] = "FC IT";
            Departments["1230300"] = "USFC IT (300)";
            Departments["1230HR"] = "FC IT - HR";
            Departments["1230Z"] = "USFC IT - Zappos";
            Departments["1231"] = "FC IT - Variable";
            Departments["1234"] = "Amazon Campus Fulfillment–Var";
            Departments["1235"] = "Amazon Campus Fulfillment–Fix";
            Departments["1236"] = "FC HR";
            Departments["1237"] = "Sort Center HR";
            Departments["1250"] = "OPS HR - Regional Management";
            Departments["1251"] = "Ops HR - Temp Staffing";
            Departments["1252"] = "Ops HR - Staff Recruiting";
            Departments["1253"] = "Ops HR - Management Recruiting";
            Departments["1254"] = "Ops HR-Learning & Leadership";
            Departments["1255"] = "Amazon Global";
            Departments["1260"] = "FBA Business - Variable FC";
            Departments["1299"] = "Distribution Center - Var HC";
            Departments["129920"] = "Packaging Variable - (BA)";
            Departments["129924"] = "Duplication Variable - (BA)";
            Departments["129962"] = "Direct Market Shipping Variabl";
            Departments["129994"] = "Returns Variable - (BA)";
            Departments["129999"] = "Shipping Variable - (BA)";
            Departments["1299HR"] = "Distribution Center - VarHC-HR";
            Departments["1300"] = "Infrastructure";
            Departments["1306"] = "Payments Business Ops";
            Departments["1380"] = "FBA Business";
            Departments["1381"] = "FBA Business - Variable";
            Departments["1400"] = "Electronic Ordering";
            Departments["1500"] = "CS Operations Management";
            Departments["1500CE"] = "Customer Service CE";
            Departments["1500FIN"] = "CS Operations Management - FIN";
            Departments["1500HR"] = "CS Operation Mgmt - HR";
            Departments["1500INT"] = "Customer Service Intl";
            Departments["1501"] = "CS Operations";
            Departments["150130"] = "Studio Variable - (BA)";
            Departments["150140"] = "Graphic Art Variable - (BA)";
            Departments["150152"] = "Customer Service (BA)";
            Departments["150160"] = "Direct Market Sales (BA)";
            Departments["1501FAB"] = "CS Operations  - FAB";
            Departments["1501FIN"] = "CS Operations  - FIN";
            Departments["1501HR"] = "CS Operations-HR";
            Departments["1501INT"] = "CS Operations Interns";
            Departments["1501SC"] = "CS Operations  - SC";
            Departments["1501WI"] = "CS Operations  - WI";
            Departments["1502"] = "CS Operations - Alt Language 2";
            Departments["1503"] = "CS Operations Management - Alt";
            Departments["1504"] = "Alt Language 3";
            Departments["1505"] = "CS Management";
            Departments["1505FIN"] = "CS Management - FIN";
            Departments["1505HR"] = "CS Management - HR";
            Departments["1508"] = "Retail Fixed Ops";
            Departments["1508HR"] = "Retail Fixed Ops - HR";
            Departments["1509"] = "CS Operations - Alt Language 4";
            Departments["1509HR"] = "CS Operations- Alt Lang 4 - HR";
            Departments["1509VNAA"] = "1509VNAA CS Ops Management";
            Departments["1510"] = "TAM - Merchant and Seller Supp";
            Departments["1510HR"] = "TAM-Merchant and Seller SuppHR";
            Departments["1510NP"] = "TAM - Merchant &Seller Sup(NP)";
            Departments["1511"] = "CS CreateSpace";
            Departments["1511SC"] = "CS CreateSpace - SC";
            Departments["1512"] = "Seller Support Project";
            Departments["1515"] = "CS - Phone Sales Management";
            Departments["1516"] = "Product Quality Management";
            Departments["1518"] = "Risk Investigations Corporate";
            Departments["1520"] = "CS-WebServices Specialty Group";
            Departments["1521"] = "CS-Corp Acct Specialty Group";
            Departments["1522"] = "CS-Tax Specialty Group";
            Departments["1523"] = "Abuse Operations";
            Departments["1525"] = "CS - Phone Sales Operations";
            Departments["1526"] = "Customer Service - Phone Sales";
            Departments["1527"] = "Goodreads - Variable";
            Departments["1530"] = "CS HR";
            Departments["1535"] = "CS Tech Mgmt";
            Departments["1535HR"] = "CS Tech Mgmt HR";
            Departments["1570"] = "CS Operations-Kindle Variable";
            Departments["1578"] = "AWS Marketplace-Variable";
            Departments["1579"] = "AWS Customer Service";
            Departments["1580"] = "Digital Var Ops";
            Departments["1581"] = "CS Operations Mgmt WFH";
            Departments["1581FIN"] = "CS Operations Mgmt WFH";
            Departments["1581HR"] = "CS Operations Mgmt WFH HR";
            Departments["1582"] = "CS Ops - Variable - WFH";
            Departments["1582HR"] = "CS Ops - Variable - WFH - HR";
            Departments["1583"] = "CS - Kindle Support Operations";
            Departments["1585"] = "Fixed Digital Corp - CS";
            Departments["1590"] = "CS - Technical Support Ops";
            Departments["1593"] = "TAM - Operations";
            Departments["1595"] = "Falcon Technology";
            Departments["1596"] = "TAM - Special Project";
            Departments["1597"] = "TAM - Cross Functional";
            Departments["1597HR"] = "HR - TAM Cross Functional";
            Departments["1598"] = "Enterprise TAM - Variable";
            Departments["1599"] = "TAM - Administration";
            Departments["1599HR"] = "HR - TAM Administration";
            Departments["1600"] = "Out of Print Sourcing";
            Departments["1604"] = "SCOT - Fast Track";
            Departments["1620"] = "TRMS Compliance";
            Departments["1700"] = "Publisher Affairs";
            Departments["1710"] = "Regional Facilities";
            Departments["1716"] = "Global Credit";
            Departments["1717"] = "Gift Certificates - Corporate";
            Departments["1721"] = "Ops Real Estate";
            Departments["1777"] = "Seller Vetting Variable";
            Departments["1950"] = "Supply Chain Administration";
            Departments["2000"] = "After Market Services (AMS)";
            Departments["2000"] = "Control Buying";
            Departments["2001"] = "Co-op Management";
            Departments["2002"] = "Retail Training & Learning";
            Departments["2010"] = "Delivery Experience";
            Departments["2020"] = "Out of Print Sourcing";
            Departments["2100"] = "POD & MOD";
            Departments["2140"] = "External Fulfillment";
            Departments["2150"] = "Supply Chain Optimization Tech";
            Departments["2150HR"] = "Inventory Management HR";
            Departments["2157"] = "Supply Chain Execution";
            Departments["215726"] = "Supply Chain";
            Departments["2160"] = "Returns";
            Departments["2202"] = "TRMS Operations";
            Departments["2210"] = "Control Buying - Toys";
            Departments["2212"] = "Instock Mgmt - Health & Beauty";
            Departments["2212"] = "Instock Mgmt-Health & Personal";
            Departments["2213"] = "Instock Mgmt-Beauty & Grooming";
            Departments["2218"] = "Instock Management - Food";
            Departments["2219"] = "Instock Management - Baby";
            Departments["2220"] = "Entertainment CC";
            Departments["2222"] = "Control Buying - Shoes";
            Departments["2223"] = "CE Project Team";
            Departments["2228"] = "BISS/B2B Project Team";
            Departments["2230"] = "Control Buying - Apparel";
            Departments["2238"] = "Control Buying - Tools";
            Departments["2239"] = "Sports-OutdoorRecreationalTeam";
            Departments["2240"] = "Control Buying - CE";
            Departments["2242"] = "Wireless/Mobile Project Team";
            Departments["2244"] = "PC/Office Project Team";
            Departments["2246"] = "Heavy/ Bulky HE";
            Departments["2401"] = "Payment Operations";
            Departments["2500"] = "Books/Magazines";
            Departments["2500HR"] = "Books/Magazines HR";
            Departments["2501"] = "Trade Books";
            Departments["2502"] = "Textbooks";
            Departments["2503"] = "Amazon Student";
            Departments["2504"] = "Textbooks On Campus";
            Departments["2505"] = "Media Shopping XP";
            Departments["2506"] = "Merchant Risk Mgmt Programs";
            Departments["2507"] = "Claims Investigations";
            Departments["2508"] = "Merchant Review";
            Departments["2509"] = "Product Quality Investigations";
            Departments["2510"] = "Merchandise - Hardlines";
            Departments["2511"] = "EPS - Risk Investigations";
            Departments["2512"] = "AWS Variable Fraud";
            Departments["2518"] = "EPS - Risk & Sales";
            Departments["2519"] = "Chargeback Investigators";
            Departments["2520"] = "Music CC";
            Departments["2521"] = "Kindle Vendor Funded Services";
            Departments["2522"] = "Buying Ops";
            Departments["2523"] = "Control Buying - Home";
            Departments["2524"] = "Catalog services";
            Departments["2525"] = "Retail Operations & Initiative";
            Departments["2550"] = "Quality";
            Departments["2551"] = "Supply Chain Planning";
            Departments["2552"] = "Global Manufacturing &Ops Engg";
            Departments["2553"] = "Supply Chain and Compliance";
            Departments["2554"] = "Product Line Operations-PRQ";
            Departments["2555"] = "Global Operations Support";
            Departments["2556"] = "Advanced Manufacturing Engg";
            Departments["2558"] = "Operations";
            Departments["2559"] = "Product Line Operations";
            Departments["2560"] = "Accessories Operations";
            Departments["2698"] = "Home, Kitchen, Pet Tech Team";
            Departments["2700"] = "Operations Integration";
            Departments["3000"] = "Marketing";
            Departments["3100"] = "Business Development";
            Departments["3500"] = "Product Development";
            Departments["4000"] = "AWS Commercial Sales-Variable";
            Departments["4001"] = "AWS Professional Services";
            Departments["4002"] = "AWS Sales Training";
            Departments["4003"] = "AWS Solution Architecture";
            Departments["4004"] = "AWS Business Development";
            Departments["4005"] = "AWS Partner Program";
            Departments["4006"] = "AWS Public Sector Sales-Fixed";
            Departments["4008"] = "AWS International Product Mgmt";
            Departments["4009"] = "AWS Sales&BD Devlper Resources";
            Departments["4040"] = "AmazonLocal Sales and Productn";
            Departments["4110"] = "Biz Dev - MP";
            Departments["4120"] = "Marketing - MP";
            Departments["4130"] = "Merch Dev - MP";
            Departments["4200"] = "Business Development";
            Departments["4210"] = "Associates";
            Departments["4250"] = "Merchant Development";
            Departments["4300"] = "Mktg-Advertising and Planning";
            Departments["4300"] = "Search Traffic Marketing";
            Departments["4300AUD"] = "Mktg-Advertising and Planning";
            Departments["4301"] = "Marketing & Advertising-Fixed";
            Departments["4302"] = " Auto/Tools/HI/LG Project Team";
            Departments["4303"] = "Digital mass and brand market";
            Departments["4305"] = "Marketing Communication";
            Departments["4309"] = "Digital Video Marketing";
            Departments["4310"] = "Mktg - Admin";
            Departments["4315"] = "Mktg-Promotions";
            Departments["4320"] = "VAS Fixed";
            Departments["4321"] = "VAS Variable";
            Departments["4325"] = "Segment Marketing";
            Departments["4330"] = "Strategic Communication";
            Departments["4331"] = "Channels Sales";
            Departments["433150"] = "Sales Variable-(BA)";
            Departments["433160"] = "Direct Market Sales Var-(BA)";
            Departments["4331SC"] = "Channels Sales (CreateSpace)";
            Departments["4332"] = "Enterprise Sales";
            Departments["4333"] = "Small Business Solutions Sales";
            Departments["4334"] = "Business and Corporate Dev";
            Departments["4335"] = "Mktg-Corp Business Development";
            Departments["4336"] = "Advertising-Sales Management";
            Departments["4336HR"] = "Advertising-Sales Mgmt HR";
            Departments["4337"] = "Advertising Product - Web";
            Departments["4338"] = "Advertising Sales Var-Field";
            Departments["4339"] = "Advertising Ops-Field AM";
            Departments["4340"] = "Mktg - Market Intelligence";
            Departments["4340AUD"] = "Mktg - Market Intelligence-AUD";
            Departments["4350"] = "Mktg - Customer Experience";
            Departments["4355"] = "Mktg-Data Analysis";
            Departments["4375"] = "Amazon Registry Services";
            Departments["4380"] = "EPS - PM/Marketing";
            Departments["4400"] = "Sports/Toys Project Team";
            Departments["4450"] = "Kindle Marketing";
            Departments["4450HR"] = "Kindle Marketing HR";
            Departments["4451"] = "Dig Tech Mgmt - BD/Mktg";
            Departments["4455"] = "Data Mining";
            Departments["4500"] = "Marketing - Intl";
            Departments["4501"] = "Buyer Risk Investigations";
            Departments["4502"] = "Transaction Risk Management";
            Departments["4503"] = "Machine Learning-Seller Servic";
            Departments["4506"] = "Merchant Risk Investigations";
            Departments["4513"] = "Channel Sales Support";
            Departments["4514"] = "Localization";
            Departments["4516"] = "Global Sales Support";
            Departments["4600"] = "AWS - Marketing";
            Departments["4601"] = "AWS Marketing - Variable";
            Departments["4700"] = "GC Discount";
            Departments["4800"] = "Softlines Marketing";
            Departments["4800Z"] = "Softlines Marketing-Zappos";
            Departments["5000"] = "Editorial";
            Departments["5009"] = "AWS Sales&BD Devlper Resources";
            Departments["5050"] = "WW Retail Media Management";
            Departments["5055"] = "NA Retail Expansion";
            Departments["5060"] = "Books Management";
            Departments["5100"] = "POD & MOD";
            Departments["5100"] = "Product Development Mgt";
            Departments["5110"] = "Global Retail Development";
            Departments["5120"] = "International Technology";
            Departments["5120HR"] = "International Expansion-HR";
            Departments["5121"] = "Int Expansion - India/Cricket";
            Departments["5122"] = "Int Expansion - India/Junglee";
            Departments["5123"] = "Int Expansion - Brazil/Maicom";
            Departments["5124"] = "Javari Tech";
            Departments["5127"] = "Retail Wholesale";
            Departments["5140"] = "Applications&Services-Variable";
            Departments["5141"] = "Demand Planning";
            Departments["5142"] = "TAM/Special conversion";
            Departments["5143"] = "Kindle Reader Product Mgt";
            Departments["5144"] = "Kindle Enterprise/Eduction-Mgt";
            Departments["5145"] = "Digital Content Mgt";
            Departments["5145HR"] = "Digital Content Mgt";
            Departments["5146"] = "Alexa Support";
            Departments["5150"] = "Retail Systems G&A";
            Departments["5150HR"] = "Retail Systems G&A HR";
            Departments["5151"] = "SCOT - Vendor Ordering Systems";
            Departments["5152"] = "Retail Selection Systems";
            Departments["5153"] = "Vendor Management Tools";
            Departments["5154"] = "Buying Tools";
            Departments["5155"] = "Tech Program Mgt";
            Departments["5155HR"] = "HR-Digital Product Development";
            Departments["5155INT"] = "Tech Program Mgt Interns";
            Departments["5156"] = "D&A Prod Mgt/Merchandising";
            Departments["5157"] = "Kindle Wholesale - Mgt";
            Departments["5158"] = "Digital Video Business";
            Departments["5158HR"] = "Digital Video Product Dev-HR";
            Departments["5159"] = "Digital Video Product Ops";
            Departments["5160"] = "Amazon Publishing";
            Departments["5160HR"] = "Encore/CaC/ODP - HR";
            Departments["5161"] = "Content Creation";
            Departments["516130"] = "Studio - (BA)";
            Departments["516140"] = "Graphic Art - (BA)";
            Departments["516150"] = "Sales Fixed - (BA)";
            Departments["516160"] = "Direct Market Sales Fixed-(BA)";
            Departments["516170"] = "Management - (BA)";
            Departments["516175"] = "HR - (BA)";
            Departments["516176"] = "Editorial - (BA)";
            Departments["516178"] = "Office - (BA)";
            Departments["5162"] = "Music - Product Mgmt";
            Departments["5163"] = "Digital Audio Product Ops";
            Departments["5164"] = "Kindle Products - Mgt";
            Departments["5165"] = "Digital Management";
            Departments["5165HR"] = "Digital Management HR";
            Departments["5166"] = "Digital Platform - Mgt";
            Departments["5167"] = "Device Software & QA - Mgt";
            Departments["5168"] = "Kindle Project Mgt";
            Departments["5169"] = "Cross Platform Mgt";
            Departments["5170"] = "Content Demand - Mgt";
            Departments["5171"] = "Self Publishing";
            Departments["5172"] = "SGX Product and Program";
            Departments["5173"] = "Kindle Content Operations";
            Departments["5174"] = "Digital Tech Management";
            Departments["5174HR"] = "Digital Tech Management HR";
            Departments["5175"] = "Canada Team";
            Departments["5176"] = "Hardware - Mgt";
            Departments["5177"] = "Whispernet & Field Testing Mgt";
            Departments["5178"] = "Kindle Development Kit - Mgt";
            Departments["5179"] = "Amzn Studios Business&Creative";
            Departments["5180"] = "Disc on Demand";
            Departments["5181"] = "Digital Program Management";
            Departments["5182"] = "Vendor Self Service";
            Departments["5183"] = "Digital Product Management";
            Departments["5183HR"] = "Digital Product Management-HR";
            Departments["5184"] = "Content Quality Operations";
            Departments["5185"] = "SGX Operations";
            Departments["5186"] = "Publisher Tools to Reader -Ops";
            Departments["5187"] = "Kindle Direct Publishing - CS";
            Departments["5188"] = "Content Risk Management";
            Departments["5189"] = "Kindle Fraud Detection";
            Departments["5190"] = "Publisher Tools to Reader -Mgt";
            Departments["5191"] = "Content Platform - Mgt";
            Departments["5192"] = "Digital Book Store";
            Departments["5193"] = "Digital Project Mngmnt";
            Departments["5193HR"] = "Digital Project Mngmnt - HR";
            Departments["5196"] = "Content Programs Variable";
            Departments["5197"] = "Digital Product Mgmt-Solutions";
            Departments["5199"] = "Digital Support Management";
            Departments["5200"] = "Movies";
            Departments["5202"] = "Digital - Paid Text & STRIP";
            Departments["5203"] = "Content exp & Quality  - Mgt";
            Departments["5205"] = "Content Vendor Mgt";
            Departments["5206"] = "Content Sites";
            Departments["5207"] = "AmazonLocal";
            Departments["5207HR"] = "AmazonLocal HR";
            Departments["5208"] = "KDP Variable";
            Departments["5209"] = "WW Retail EGM Management";
            Departments["5210"] = "Consumer Shopping Experience";
            Departments["5211"] = "Customer Experience - Store";
            Departments["5212"] = "Imaging Fixed Operations";
            Departments["5212HR"] = "Imaging Fixed Operations HR";
            Departments["5212Z"] = "Imaging Fixed Ops - Zappos";
            Departments["5213"] = "Home Entertainment";
            Departments["5214"] = "Category Management - Kitchen";
            Departments["5215"] = "Jewelry Category";
            Departments["5216"] = "Sports Category";
            Departments["5218"] = "Imaging Fixed Studio Costs";
            Departments["5218Z"] = "Imaging Fixed Studio Costs-Zap";
            Departments["5219"] = "Imaging Variable Studio Costs";
            Departments["5219SDF"] = "Imaging Var Studio Costs-SDF";
            Departments["5219Z"] = "Imaging Var Studio Costs - Zap";
            Departments["5220"] = "Mobile App Operations";
            Departments["5221"] = "Imaging Variable VxD";
            Departments["5222"] = "Goodreads- Business (Non-Tech)";
            Departments["5222HR"] = "Goodreads-Bussin (Non-Tech) HR";
            Departments["5225"] = "Watches";
            Departments["5226"] = "comiXology-Business(Non-Tech)";
            Departments["5227"] = "Audible - Business (Non-Tech)";
            Departments["5227HR"] = "Audible-Business (Non-Tech) HR";
            Departments["5228"] = "Higher Ed - Business";
            Departments["5229"] = "Higher Ed - Tech";
            Departments["5231"] = "SWA - Business";
            Departments["5232"] = "SWA - Tech";
            Departments["5234"] = "Kindle KM - LOC";
            Departments["5235"] = "Top Level Domain";
            Departments["5250"] = "Gifts & Ecard";
            Departments["5256"] = "D&A Prod Mgt/Merchandising";
            Departments["5257"] = "Kindle Wholesale - Mgt";
            Departments["5300"] = "Product Management";
            Departments["5305"] = "Prod Dev - Retail Group";
            Departments["5307"] = "Global Vendor Management";
            Departments["5310"] = "Retail Project Management";
            Departments["5313"] = "Amazon Clicks Variable Expense";
            Departments["5315"] = "Media";
            Departments["5320"] = "Marketplace & 3rd Party";
            Departments["5325"] = "Corporate Accounts";
            Departments["5334"] = "Amazon Clicks";
            Departments["5335"] = "Account Management";
            Departments["5347"] = "Enterprise Operations";
            Departments["5350"] = "Browse Operations";
            Departments["5352"] = "Enterprise India Fixed";
            Departments["5353"] = "Webstore Businesses";
            Departments["5354"] = "Enterprise Integration";
            Departments["5355"] = "Enterprise Seller Success";
            Departments["5356"] = "Checkout";
            Departments["5357"] = "Falcon Business";
            Departments["5358"] = "Enterprise India Variable";
            Departments["5359"] = "Enterprise Sellers - Fixed";
            Departments["5360"] = "WebStore Applications";
            Departments["5362"] = "Cross Site Editorial";
            Departments["5363"] = "Retail Fin Intelligence Sys";
            Departments["5364"] = "Consumer Orders&Self-Serv Syst";
            Departments["5365"] = "CMT Systems";
            Departments["5366"] = "ECPS Prod Mgmt&Srvc Incubation";
            Departments["5367"] = "Webstore Technology";
            Departments["5368"] = "Hardlines Stores";
            Departments["5370"] = "Webstore Technical IN (Fixed)";
            Departments["5371"] = "ECPS Common Platform Services";
            Departments["5372"] = "CRM Merch Services";
            Departments["5375"] = "Webstore Technology";
            Departments["5380"] = "FBA Business";
            Departments["5381"] = "FBA Business - Variable";
            Departments["5382"] = "Amazon Global Selling-Business";
            Departments["5399"] = "Compliance Classification Syst";
            Departments["5400"] = "CMT Ops - Fixed";
            Departments["5401"] = "Retail Business Services Fixed";
            Departments["5402"] = "Selection Monitoring";
            Departments["5500"] = "Transaction Risk Management";
            Departments["5500HR"] = "Transaction Risk Management-HR";
            Departments["5501"] = "Buyer Risk Investigations";
            Departments["5502"] = "TRMS Operations";
            Departments["5503"] = "Machine Learning-Seller Servic";
            Departments["5504"] = "Amazon Lending Services";
            Departments["5505"] = "Merchant Risk Mgmt Programs";
            Departments["5506"] = "Merchant Risk Investigations";
            Departments["5507"] = "Claims Investigations";
            Departments["5508"] = "Merchant Review";
            Departments["5509"] = "Product Quality Investigations";
            Departments["5510"] = "EPS - Risk & Sales";
            Departments["5511"] = "EPS - Risk Investigations";
            Departments["5512"] = "AWS Variable Fraud";
            Departments["5513"] = "XBA";
            Departments["5514"] = "EPS - Admin";
            Departments["5515"] = "EPS - International";
            Departments["5516"] = "Product Quality Management";
            Departments["5517"] = "EPS - Variable";
            Departments["5518"] = "Risk Investigations Corporate";
            Departments["5519"] = "Chargeback Investigators";
            Departments["5520"] = "TRMS Compliance";
            Departments["5522"] = "Kindle Vendor Funded Services";
            Departments["5524"] = "Amazon Lending Management";
            Departments["5578"] = "AWS Marketplace-Variable";
            Departments["5600"] = "Enterprise Target - Variable";
            Departments["5604"] = "Fast Track";
            Departments["5605"] = "Software - Physical";
            Departments["5610"] = "Category Management - Toys";
            Departments["5611"] = "Category Management - Baby";
            Departments["5612"] = "Mobile Shopping";
            Departments["5614"] = "Gift Certificates - Variable";
            Departments["5616"] = "Category Mgmt-Beauty& Grooming";
            Departments["5619"] = "Consumables CX - Non-Tech";
            Departments["5620"] = "Category Mgmt-Health& Personal";
            Departments["5621"] = "Category Mgmt-Other Consumable";
            Departments["5621HR"] = "Category Mgmt-Other Consum HR";
            Departments["5622"] = "Prime Pantry";
            Departments["5625"] = "Platform Development";
            Departments["5626"] = "Category Mgmt - Amazon Fresh";
            Departments["5626ATS"] = "Amazon Fresh";
            Departments["5628"] = "Product Management - AmznFresh";
            Departments["5629"] = "International Dabba";
            Departments["5630"] = "Amazon Elements";
            Departments["5634"] = "Category Management - Food";
            Departments["5640"] = "Category Management - Auto";
            Departments["5646"] = "Category Management - BISS";
            Departments["5646HR"] = "HR - Category Management BISS";
            Departments["5647"] = "Amazon Business";
            Departments["5650"] = "Shoes Category";
            Departments["5650Z"] = "Shoes Category - Zappos";
            Departments["5651"] = "Bettis - Placeholder";
            Departments["5660"] = "MyHabit";
            Departments["5663"] = "Apparel Category";
            Departments["5663HR"] = "Apparel Category-HR";
            Departments["5664"] = "CategoryManagement-Lawn&Garden";
            Departments["5665"] = "Softlines Product Imaging-Vari";
            Departments["5668"] = "Category Management - Home";
            Departments["5668HR"] = "HR - Category Management Home";
            Departments["5670"] = "Category Management - Tools";
            Departments["5674"] = "Category Management - HI";
            Departments["5674"] = "Category Management - Tools";
            Departments["5674HR"] = "Category Management-Tools-HR";
            Departments["5675"] = "Softlines Product Imaging-Fixe";
            Departments["5676"] = "Category Management - Pets";
            Departments["5678"] = "Site Merchandising - CE";
            Departments["5682"] = "Category Management - CE";
            Departments["5682HR"] = "Category Management - CE - HR";
            Departments["5684"] = "HR/Recruiting";
            Departments["5688"] = "Category Management - Wireless";
            Departments["5690"] = "Appstore Prod Mgmt & Engg";
            Departments["5691"] = "Appstore Developer Marketing";
            Departments["5694"] = "Category Management - PC";
            Departments["5696"] = "Hardlines Shopping Experience";
            Departments["5696HR"] = "Category Management HR";
            Departments["5700"] = "Prod Dev Mgmt - MP";
            Departments["5701"] = "TAM - Technology (PCT)";
            Departments["5702"] = "Integration";
            Departments["5703"] = "eCPS Mercury Veil Project";
            Departments["5704"] = "eCPS Program and Ops Mgmt";
            Departments["5705"] = "Amazon Services Admin";
            Departments["5705HR"] = "Amazon Services Admin-HR";
            Departments["5706"] = "Channels Support";
            Departments["5710"] = "Payment Products and Services";
            Departments["5710HR"] = "Payment Products & Services HR";
            Departments["5711"] = "Amazon Local Commerce";
            Departments["5712"] = "Payment Instruments";
            Departments["5713"] = "Gift Certificates";
            Departments["5714"] = "Gift Certificates - Variable";
            Departments["5715"] = "WW Retail Applications";
            Departments["5716"] = "Global Credit";
            Departments["5717"] = "Gift Certificates - Corporate";
            Departments["5718"] = "Associates Technology";
            Departments["5730"] = "Periodicals";
            Departments["5740"] = "Category Mgmt - MP";
            Departments["5740"] = "Mass Vendor Recruitment (MVR)";
            Departments["5760"] = "Merchant Technologies-Business";
            Departments["5760HR"] = "Merchant Tech-Business HR";
            Departments["5761"] = "B2B Marketplace - Fixed";
            Departments["5762"] = "New Business-Mkt place-Fixed";
            Departments["5770"] = "Merchant Technologies-Variable";
            Departments["5771"] = "B2B Marketplace - Variable";
            Departments["5774"] = "AWS Commercial Sales - Fixed";
            Departments["5777"] = "Seller Vetting Variable";
            Departments["5790"] = "Kindle Field Sales Reps";
            Departments["5791"] = "Consumer Marketing Management";
            Departments["5791HR"] = "Traffic Management HR";
            Departments["5792"] = "Prime Management";
            Departments["5792HR"] = "Prime Management HR";
            Departments["5800"] = "Enterprise Sellers - Variable";
            Departments["5801"] = "Enterprise Sellers - Variable";
            Departments["5805"] = "Shopping Core";
            Departments["5805HR"] = "Amazon Mobile Business-HR";
            Departments["5810"] = "AWS Developer Resources";
            Departments["5811"] = "Anti-DDoS";
            Departments["5811"] = "UnSpun";
            Departments["5812"] = "AWS Support";
            Departments["5813"] = "Infrastructure Supply Chain";
            Departments["5814"] = "AWS Managed Cloud";
            Departments["5815"] = "AWS Documentation";
            Departments["5820"] = "Camera";
            Departments["5821"] = "Audio & Accessories";
            Departments["5822"] = "Musical Instruments";
            Departments["5823"] = "Global Sourcing";
            Departments["5824"] = "Mobile Electronics";
            Departments["5825"] = "Office Products";
            Departments["5826"] = "Physical Software";
            Departments["5827"] = "Digital Software";
            Departments["5828"] = "Physical Video Games";
            Departments["5829"] = "Digital Video Game";
            Departments["5830"] = "Appstore,Games & Cloud Drive";
            Departments["5830HR"] = "HR Electronics HQ";
            Departments["5831"] = "Appstore Consumer Marketing";
            Departments["5832"] = "Home EntertainmentSubscription";
            Departments["5833"] = "Furniture";
            Departments["5834"] = "Appstore Consumer Marketing";
            Departments["5849"] = "AWS Public Sector Sales-Var";
            Departments["5890"] = "Kindle Field Sales Reps";
            Departments["5900"] = "IMDb";
            Departments["5910"] = "IMDb - Variable";
            Departments["5940"] = "10K";
            Departments["5946"] = "Twitch - Fixed";
            Departments["594621"] = "Engineering Operations";
            Departments["594622"] = "Ads Operations";
            Departments["594631"] = "Product Engineering";
            Departments["594632"] = "Backend Engineering";
            Departments["594633"] = "Product";
            Departments["594641"] = "Sales";
            Departments["594642"] = "Custom Solutions";
            Departments["594643"] = "Client Services";
            Departments["594644"] = "Sales Marketing";
            Departments["594645"] = "Partnerships";
            Departments["594646"] = "Business Development";
            Departments["594647"] = "Marketing";
            Departments["594651"] = "Finance";
            Departments["594652"] = "People Ops";
            Departments["594653"] = "Recruiting";
            Departments["594654"] = "Legal";
            Departments["5946DX"] = "Twitch - Fixed";
            Departments["5947"] = "Twitch - Variable";
            Departments["5948"] = "AGS OC Control";
            Departments["5949"] = "AGS Kiln Studio";
            Departments["5950"] = "AGS Publishing Services";
            Departments["5950HR"] = "Social Games-HR";
            Departments["5953"] = "AGS OC Versus";
            Departments["5954"] = "AGS OC Campaign";
            Departments["6000"] = "European Fulfillment Network";
            Departments["6011"] = "EU & Direct Import";
            Departments["7000"] = "WW Operations Execution";
            Departments["7000AUD"] = "Operations Management - AUD";
            Departments["7000ENG"] = "Operations Management - ENG";
            Departments["7000FIN"] = "Operations Management - FIN";
            Departments["7000HR"] = "Operations Mgmt - HR";
            Departments["7000OPS"] = "Operations Management - OPS";
            Departments["7006"] = "WW Ops Execution Engineering";
            Departments["7030"] = "HL Leadership";
            Departments["7050"] = "Facilities";
            Departments["7080"] = "Customer Packaging Experience";
            Departments["7100"] = "Algorithms";
            Departments["7100"] = "Infrastructure Business Ops";
            Departments["7110"] = "Infrastructure - Business Dev";
            Departments["7111"] = "Technical Operations";
            Departments["7115"] = "Retail Tech team Project Team";
            Departments["7119"] = "Dig Tech - Hardware 2";
            Departments["7120"] = "Dig Prod Mgmt - BS";
            Departments["7121"] = "Digital User Experience";
            Departments["7122"] = "Apps & Services";
            Departments["7123"] = "Apub - Variable";
            Departments["7124"] = "Hardware-Product Development";
            Departments["7125"] = "Hardware - System Engineering";
            Departments["7126"] = "Computer Vision S/W Products";
            Departments["7127"] = "Kindle Reader Engineering";
            Departments["7128"] = "Home Products";
            Departments["7129"] = "Device Tech TPM & QA";
            Departments["7131"] = "Kindle Enterprise - Tech";
            Departments["7132"] = "Kindle Education - Tech";
            Departments["7133"] = "Hardware - EPM Tech.";
            Departments["7134"] = "Device TPM and Operations";
            Departments["7136"] = "Hardware-Advanced Product&Tech";
            Departments["7137"] = "Alexa - Software";
            Departments["7138"] = "Media Products";
            Departments["7139"] = "Goodreads - Tech";
            Departments["7140"] = "comiXology - Tech";
            Departments["7141"] = "Hardware - Platform";
            Departments["7142"] = "Audible - Tech";
            Departments["7143"] = "ZME - Tech";
            Departments["7144"] = "Kindle Education - TenMarks";
            Departments["7145"] = "IvonaTTS Tech";
            Departments["7146"] = "Evi Knowledge Platform";
            Departments["7147"] = "Alexa SDK & Access";
            Departments["7150"] = "AMZL & Transportation Admin";
            Departments["7150HR"] = "Transportation Admin HR";
            Departments["7177"] = "Last Mile Technology";
            Departments["7191"] = "FC Systems Management";
            Departments["719110"] = "SW Integration";
            Departments["719111"] = "HW Integration";
            Departments["719115"] = "Deployment Engineering";
            Departments["719120"] = "Production";
            Departments["719125"] = "Tech Ops";
            Departments["719127"] = "Quality";
            Departments["719128"] = "Ops Prgm Mgt";
            Departments["719150"] = "Development HW";
            Departments["719158"] = "HW Architecture";
            Departments["719159"] = "XB-15";
            Departments["719171"] = "Solutions";
            Departments["719172"] = "Scout Platform";
            Departments["719173"] = "PMO";
            Departments["719174"] = "Performance Engineering";
            Departments["719185"] = "Corporate";
            Departments["719190"] = "Occupancy BOS12";
            Departments["7191HR"] = "FC Systems Management-HR";
            Departments["7200"] = "Sports/Toys Project Team";
            Departments["7207"] = "Global Vendor Management";
            Departments["7212"] = "Instock Mgmt-Health & Personal";
            Departments["7218"] = "Instock Management - Food";
            Departments["7219"] = "Instock Management - Baby";
            Departments["7223"] = "Locker & Pickup & On My Way";
            Departments["722380"] = "IS";
            Departments["7230"] = "Control Buying - Apparel";
            Departments["7239"] = "Sports-OutdoorRecreationalTeam";
            Departments["7250"] = "Books/Magazines";
            Departments["7261"] = "B2B Marketplace - Fixed";
            Departments["7300"] = "Cloudfront";
            Departments["7301"] = "S3";
            Departments["7302"] = "Mturk";
            Departments["7303"] = "EC2";
            Departments["7303HR"] = "EC2 - HR";
            Departments["7304"] = "DBS Leadership";
            Departments["7305"] = "Payments Platform - India";
            Departments["7306"] = "Payments Business Ops";
            Departments["7307"] = "EPS - Mobile & IRIS";
            Departments["7308"] = "EPS - Engineering - Seller";
            Departments["7309"] = "EPS - Compliance";
            Departments["7310"] = "DBS EMR, DataPipeline";
            Departments["7312"] = "AWS Premium Support";
            Departments["7313"] = "Amazon Clicks Variable Expense";
            Departments["7314"] = "AWS Managed Cloud - Variable";
            Departments["7315"] = "EPS - TRUST";
            Departments["7317"] = "AWS Database Platform";
            Departments["7320"] = "Credit - Tech";
            Departments["7322"] = "EPS - Engineering - Buyer";
            Departments["7323"] = "Promotions Platform";
            Departments["7324"] = "Shipping Platform";
            Departments["7328"] = "EPS - TPM/CX";
            Departments["7329"] = "Retail Services Tech";
            Departments["7330"] = "Glacier";
            Departments["7331"] = "EC2 Networking";
            Departments["7332"] = "Work Suite";
            Departments["7333"] = "AWS Windows Business";
            Departments["7334"] = "Amazon Clicks";
            Departments["7335"] = "Advertising Product - Kindle";
            Departments["7336"] = "Advertising Platform";
            Departments["7337"] = "DBS Redshift, Aurora, Console";
            Departments["7338"] = "Elastic Beanstalk";
            Departments["7339"] = "Video Advertising";
            Departments["7340"] = "Mobile Advertising";
            Departments["7341"] = "Display Advertising Design";
            Departments["7341HR"] = "Display Advertising Design HR";
            Departments["7343"] = "Variable Sales - ISO";
            Departments["7344"] = "Variable Sales - Specialists";
            Departments["7345"] = "Operations - AAP";
            Departments["7346"] = "Advt Ops-Support & Leadership";
            Departments["7347"] = "Design - Fixed";
            Departments["7348"] = "Account Management - ISO";
            Departments["7349"] = "Trafficking";
            Departments["7350"] = "AWS Directory Service";
            Departments["7351"] = "AWS Cryptography";
            Departments["7352"] = "AWS IoT";
            Departments["7353"] = "AWS Forecasting";
            Departments["7354"] = "Edge Services Operations";
            Departments["7355"] = "AWS DNS";
            Departments["7356"] = "DBS ElastiCache";
            Departments["7357"] = "Annapurna G&A";
            Departments["7358"] = "Annapurna – Sales";
            Departments["7359"] = "AWS UX & Console";
            Departments["7360"] = "VAS Fixed";
            Departments["7361"] = "Project Poppin";
            Departments["7368"] = "Advertising Product - Web";
            Departments["7375"] = "Amazon Registry Services";
            Departments["7400"] = "Corporate Networking";
            Departments["7401"] = "Payment Operations";
            Departments["7402"] = "Payments Platform";
            Departments["7403"] = "Corporate Systems";
            Departments["7404"] = "Client Support Services";
            Departments["740474"] = "IT - (BA)";
            Departments["7405"] = "CAST";
            Departments["7406"] = "Amazon Local Commerce Tech";
            Departments["7407"] = "Corporate GC Tech";
            Departments["7408"] = "Audio Visual Services";
            Departments["7409"] = "Tech Infrastructure services";
            Departments["7410"] = "Network - Core";
            Departments["7410"] = "Tech Telecom & Networking";
            Departments["7411"] = "Hardware Engineering-Variable";
            Departments["7412"] = "Infrastructure Leadership";
            Departments["7413"] = "Infra. Perform Services - Var";
            Departments["7414"] = "Logistics - Variable";
            Departments["7419"] = "Amazon Registrar";
            Departments["7420"] = "Infra.Capacity Delivery & Auto";
            Departments["7425"] = "Device Ops Tech&Infrastructre";
            Departments["7430"] = "Learning Machines";
            Departments["7435"] = "Monitoring";
            Departments["7439"] = "Advertising Ops-Field AM";
            Departments["7440"] = "Legacy CDO Server Capital";
            Departments["7440"] = "Tech Server Related";
            Departments["7442"] = "Tech Risk Reduction-NA Retail";
            Departments["7442HR"] = "eCP Management - HR";
            Departments["7444"] = "AWS Accounts & Organizations";
            Departments["7450"] = "Caching";
            Departments["7450"] = "CloudFormation";
            Departments["7451"] = "Hardware Engineering";
            Departments["7452"] = "Existing Region Business Dev";
            Departments["7453"] = "Data Center Capital";
            Departments["7454"] = "Data Center Ops (DCO)-Variable";
            Departments["7455"] = "AWS Kinesis";
            Departments["7456"] = "Data Center GlobalServices-Var";
            Departments["7457"] = "AWS Technical Ops Servcs(TOS)";
            Departments["7458"] = "Admin Services and Tools";
            Departments["7459"] = "Network Operations - Variable";
            Departments["7460"] = "FinancialLedger&AccountingSys.";
            Departments["7461"] = "Knowledge Services";
            Departments["7462"] = "Silicon Optimization";
            Departments["7463"] = "Bill2Pay";
            Departments["7464"] = "New Business-Mktplace Tech-Fix";
            Departments["7470"] = "Corporate Applications Mgmt";
            Departments["7477"] = "Consumer GC Tech";
            Departments["7480"] = "DC Systems";
            Departments["7480300"] = "ERP Services - FC";
            Departments["7500"] = "Information Security";
            Departments["7500HR"] = "Information Security HR";
            Departments["7501"] = "Trade Books";
            Departments["7502"] = "Textbooks";
            Departments["7503"] = "Amazon Student";
            Departments["7504"] = "Amazon Lending Services";
            Departments["7510"] = "Platform Excellence Team";
            Departments["7514"] = "Localization";
            Departments["7520"] = "Music CC";
            Departments["7525"] = "Retail Operations & Initiative";
            Departments["7526"] = "Amazon Lending Management";
            Departments["7535"] = "CS Tech Mgmt";
            Departments["7600"] = "International";
            Departments["7625"] = "Platform Development";
            Departments["7700"] = "Gbl Security Eng - Contractors";
            Departments["7702"] = "CS Applications - Kindle";
            Departments["7703"] = "Construction, Engg & Optimiztn";
            Departments["7704"] = "Data Center Design Engineering";
            Departments["7705"] = "Infra. Perf Services - Fixed";
            Departments["7706"] = "AWS Payments & Bill Presentatn";
            Departments["7707"] = "Grand Challenge";
            Departments["7709"] = "Amazon Global Selling - Tech";
            Departments["7710"] = "Systems & Network Engineering";
            Departments["7711"] = "FBA Technical";
            Departments["7712"] = "Alexa Internet";
            Departments["7712HR"] = "Alexa Internet HR";
            Departments["7713"] = "Ordering";
            Departments["7714"] = "Automation and Messaging";
            Departments["7716"] = "AWS Mobile";
            Departments["7717"] = "Builder Tools";
            Departments["7718"] = "Associates Technology";
            Departments["7719"] = "Development";
            Departments["7719"] = "Personalization";
            Departments["7720"] = "Fulfillment Center Systems";
            Departments["772051"] = "SW QA";
            Departments["772052"] = "Systems";
            Departments["772054"] = "AFT";
            Departments["772055"] = "SW Engineering";
            Departments["772056"] = "Product IT";
            Departments["7720HR"] = "Fulfillment Center  HR";
            Departments["7721"] = "Quality,Deployment & Support S";
            Departments["7722"] = "Supply Chain Optimization Tech";
            Departments["7723"] = "Items and Offers Platform";
            Departments["7724"] = "Catalog Quality";
            Departments["7725"] = "Elastic Block Store";
            Departments["7726"] = "Customer Service Applications";
            Departments["7727"] = "Identity Services";
            Departments["7728"] = "eCPS Distributed Computing 2.0";
            Departments["7729"] = "Data Center Global Services";
            Departments["7730"] = "Distributed System Engineering";
            Departments["7731"] = "Global Financial Applications";
            Departments["7731HR"] = "Global Financial Apps - HR";
            Departments["7732"] = "Global Financial Systems";
            Departments["7733"] = "Enterprise Target - Fixed";
            Departments["7734"] = "AmazonLocal Tech";
            Departments["7735"] = "Point of Sale Services";
            Departments["7736"] = "AWS Direct Connect";
            Departments["7737"] = "Sable";
            Departments["7738"] = "SCOT - China Tech";
            Departments["7739"] = "Core Trans Technology";
            Departments["7740"] = "Website Assembly Technologies";
            Departments["7741"] = "eCP Management";
            Departments["7741HR"] = "ECPS Management - HR";
            Departments["7742"] = "Business Operations-Variable";
            Departments["7743"] = "Customer Protection Services";
            Departments["7744"] = "Amazon Mobile Engineering";
            Departments["7745"] = "EC2 Ops";
            Departments["7746"] = "Data Center Ops II (DCO)-Var";
            Departments["7747"] = "WAP & Builder Tools QA";
            Departments["7748"] = "Website Platform";
            Departments["7749"] = "Detail page";
            Departments["7750"] = "Merchant Technologies";
            Departments["7751"] = "Data Center Operations (DCO)";
            Departments["7752"] = "Search Experience";
            Departments["7752HR"] = "Search Experience - HR";
            Departments["7753"] = "Operational Excellence";
            Departments["7754"] = "Operational Excellence QA";
            Departments["7755"] = "Customer Behavior";
            Departments["7756"] = "Rev Log & Prod Compl SW Sys";
            Departments["7757"] = "SC Execution Tech";
            Departments["7757Z"] = "SC Execution Tech - Zappos";
            Departments["7758"] = "AWS Security";
            Departments["7760"] = "Data Center Engg Ops(DCEO)-Var";
            Departments["7761"] = "eCommerce Operational Excellen";
            Departments["7763"] = "Prime";
            Departments["7764"] = "AWS Silk";
            Departments["7765"] = "DBS NoSQL";
            Departments["7766"] = "Storage Services";
            Departments["7767"] = "Social Media Advertising";
            Departments["7768"] = "Browse";
            Departments["7769"] = "AWS Marketplace";
            Departments["7770"] = "DBS Space Needle";
            Departments["7771"] = "AWS - Administration";
            Departments["7771HR"] = "AWS - Administration HR";
            Departments["7772"] = "AIM";
            Departments["7773"] = "Direct Traffic Technology";
            Departments["7774"] = "AWS Commercial Sales - Fixed";
            Departments["7775"] = "Marketing Platform";
            Departments["7776"] = "Strategic Availability";
            Departments["7777"] = "Consumer Website";
            Departments["7778"] = "Tax Platform Services";
            Departments["7779"] = "Search Traffic Technology";
            Departments["7780"] = "S9";
            Departments["7781"] = "Direct Traffic";
            Departments["7782"] = "AWS Identity";
            Departments["7783"] = "AWS Fraud";
            Departments["7784"] = "AWS Metering & Bill Generation";
            Departments["7785"] = "AWS Business Support";
            Departments["7785HR"] = "AWS Business Support HR";
            Departments["7786"] = "Route 53";
            Departments["7787"] = "Transcoding";
            Departments["7788"] = "AWS New Initiatives";
            Departments["7789"] = "EC2 Load Balancing";
            Departments["7790"] = "DBS Relational";
            Departments["7791"] = "Search Technologies";
            Departments["7792"] = "A9 UI";
            Departments["7793"] = "Visual Search&MobileInnovation";
            Departments["7794"] = "Advertising Technology";
            Departments["7795"] = "A9 Services Group";
            Departments["7796"] = "Askville";
            Departments["7797"] = "Cloud Search";
            Departments["7798"] = "Associates Products & Platform";
            Departments["7799"] = "Warehouse Deals & Liquidations";
            Departments["7800"] = "Pricing Systems";
            Departments["7801"] = "Devices & Accessories - Tech";
            Departments["7802"] = "Hardware - PMO";
            Departments["7802HR"] = "HR-Digital Product Engineering";
            Departments["7803"] = "Digital Video Product Devlpmnt";
            Departments["7803HR"] = "Digital Video Product Tech HR";
            Departments["7804"] = "Music - Digital Tech";
            Departments["7804AUD"] = "Digital Audio Product Tech-AUD";
            Departments["7804HR"] = "Music - Digital Tech HR";
            Departments["7805"] = "Digital Apps";
            Departments["7806"] = "Low Level Platform";
            Departments["7807"] = "Online Digital Text Eng";
            Departments["7808"] = "Technology SW & VG";
            Departments["7809"] = "Shopbop - IT";
            Departments["7809"] = "Shopbop - Technology";
            Departments["7810"] = "Books Tech";
            Departments["7811"] = "Technology-Catalog Operations";
            Departments["7812"] = "Amazon Prime Air";
            Departments["7813"] = "Machine Learning Platform";
            Departments["7814"] = "Retail Promotion Services";
            Departments["7815"] = "Retail Platform Technology";
            Departments["7816"] = "Vendor Experience";
            Departments["7817"] = "Tech-Cust Buying Intelligence";
            Departments["7818"] = "Technology - China Development";
            Departments["7819"] = "Gateway/NAV";
            Departments["7819HR"] = "Technology-Retail Programs HR";
            Departments["7820"] = "Technology Subscriptions";
            Departments["7821"] = "Enterprise Data Warehouse";
            Departments["7822"] = "Product Aggregator and TCS";
            Departments["7823"] = "Data Warehouse Development";
            Departments["7824"] = "Consumer Website Services";
            Departments["7825"] = "Technology - DVD Rental";
            Departments["7826"] = "Technology-Amazon Fresh";
            Departments["7827"] = "Appstore Tech Team";
            Departments["7827HR"] = "Tech - Venezia HR";
            Departments["7828"] = "Applications and Services";
            Departments["7829"] = "Device Services and Security";
            Departments["7830"] = "Digital Commerce Platform";
            Departments["7831"] = "Publisher Tools to Reader-Tech";
            Departments["7832"] = "Content Ops and Programs -Tech";
            Departments["7833"] = "Cross Platform - Tech";
            Departments["7834"] = "Kindle Wholesale - Tech";
            Departments["7835"] = "Kindle Products - Tahoe";
            Departments["7837"] = "Content exp & Qlty Platfm-Tech";
            Departments["7838"] = "Social Shopping Tech";
            Departments["7838HR"] = "Social Shopping Tech - HR";
            Departments["7839"] = "Content Platform - Tech";
            Departments["7840"] = "Tech - AmazonWireless";
            Departments["7841"] = "Delivery Experience - Tech";
            Departments["7842"] = "Amazon Mom - Tech";
            Departments["7843"] = "Vendor Powered Coupons - Tech";
            Departments["7843HR"] = "Vendor Powered Coupons-TechHR";
            Departments["7844"] = "Subscribe & Save - Tech";
            Departments["7845"] = "Music Content & Community";
            Departments["7846"] = "Amazon Cloud Drive";
            Departments["7847"] = "Amazon Studios Tech";
            Departments["7849"] = "AWS Public Sector Sales-Var";
            Departments["7850"] = "Subsidiary Integration";
            Departments["7852"] = "Fast Retail URL";
            Departments["7853"] = "Vendor Negotiation&CustmrTrust";
            Departments["7854"] = "Appstore Selection & Dev Mktg";
            Departments["7855"] = "Appstore Developer Tech";
            Departments["7856"] = "Product & Service Discovery";
            Departments["7857"] = "Digital SW/VG Platform";
            Departments["7858"] = "Social Game Services";
            Departments["7859"] = "Game Insights";
            Departments["7860"] = "AGS OC Arcade";
            Departments["7861"] = "AppStream";
            Departments["7862"] = "Trade-In";
            Departments["7863"] = "DBS MVP, TOS Fixed";
            Departments["7864"] = "AWS Data Warehouse";
            Departments["7865"] = "Amazon Dev Center - Digital";
            Departments["7867"] = "AGS 2P Games";
            Departments["7868"] = "Game Services Lumberyard";
            Departments["7869"] = "Consumables - Technology";
            Departments["7869HR"] = "Consumables - Technology HR";
            Departments["7872"] = "Consumables CX - Tech";
            Departments["7878"] = "Softlines CX";
            Departments["7878HR"] = "Softlines CX HR";
            Departments["7880"] = "Kindle Support";
            Departments["7882"] = "AWS Security - Variable";
            Departments["7883"] = "AWS Legal Variable";
            Departments["7899"] = "Digital Products Software";
            Departments["7900"] = "Hardware - Industrial Design";
            Departments["7901"] = "Hardware - Accessory";
            Departments["7902"] = "Hardware - Product Integrity";
            Departments["7903"] = "Hardware - Concept Engineering";
            Departments["7904"] = "HW-Mechanical Engineering";
            Departments["7905"] = "Hardware - WhisperLab";
            Departments["7906"] = "Hardware-Sustaining&Compliance";
            Departments["7907"] = "HW-Certification&Compliance";
            Departments["7916"] = "Hardware - Advanced Products";
            Departments["8100"] = "Corporate";
            Departments["8100HR"] = "HR - Corporate (A9)";
            Departments["8101"] = "Finance Administration";
            Departments["8102"] = "JIHM 8102";
            Departments["8102HR"] = "Corporate Projects HR";
            Departments["8103"] = "JIHM Finance";
            Departments["8105"] = "Investor Relations";
            Departments["8106"] = "JIHM 8106";
            Departments["8107"] = "JIHM 8107";
            Departments["8108"] = "JIHM 8108";
            Departments["8109"] = "JIHM 8109";
            Departments["8110"] = "JIHM 8110";
            Departments["8111"] = "JIHM 8111";
            Departments["8112"] = "JIHM 8112";
            Departments["8113"] = "WW Corporate Affairs Admin";
            Departments["8115"] = "JIHM 8115";
            Departments["8116"] = "JIHM 8116";
            Departments["8117"] = "JIHM 8117";
            Departments["8150"] = "Legal";
            Departments["8150A2Z"] = "Legal-A2Z";
            Departments["8150HR"] = "Legal - HR";
            Departments["8160"] = "Corporate & Securities";
            Departments["8161"] = "IP Operations";
            Departments["8162"] = "International Consumer Legal";
            Departments["8163"] = "Intellect Property Lic-Legal";
            Departments["8164"] = "Digital Legal";
            Departments["8165"] = "AWS Legal";
            Departments["8166"] = "NA Consumer Legal";
            Departments["8167"] = "Public Policy";
            Departments["8168"] = "Compliance & Privacy Legal";
            Departments["8169"] = "Litigation Legal";
            Departments["8170"] = "Payments Legal";
            Departments["8171"] = "Digital Products Legal";
            Departments["8172"] = "JIHM Legal";
            Departments["8200"] = "Procurement";
            Departments["8300"] = "Accounting";
            Departments["8300"] = "Central Accounting";
            Departments["8300AUD"] = "Central Accounting - AUD";
            Departments["8300WI"] = "Central Accounting - WI";
            Departments["8301"] = "Tax";
            Departments["8302"] = "Operations Accounting";
            Departments["8303"] = "Accts Payable";
            Departments["8303HR"] = "Accts Payable HR";
            Departments["8304"] = "Internal Audit";
            Departments["8305"] = "Financial Planning & Analysis";
            Departments["8306"] = "BizDev - Finance";
            Departments["8307"] = "Payroll";
            Departments["8308"] = "Finance Operations Accounting";
            Departments["8309"] = "Finance Operations";
            Departments["8310"] = "Operations Finance";
            Departments["831083"] = "Finance";
            Departments["8311"] = "Finance Operations Tech";
            Departments["8312"] = "CS - Finance";
            Departments["8314"] = "WW Kindle Finance";
            Departments["8315"] = "Global Consumer  FPA";
            Departments["831572"] = "Finance - (BA)";
            Departments["8316"] = "Financial Tech & Innovation";
            Departments["8317"] = "Marketplace Sales SS TRMS Fin";
            Departments["8318"] = "AWS and Tech Finance";
            Departments["8319"] = "International Emerging Finance";
            Departments["8320"] = "eCommerce Platform Finance";
            Departments["8321"] = "US Softlines Finance";
            Departments["8322"] = "US Consumables Finance";
            Departments["8323"] = "US Media Canada Mexico Finance";
            Departments["8324"] = "Global Traffic Mobile RCX Fin";
            Departments["8325"] = "Benchmarking";
            Departments["8326"] = "Finance Operations Tax";
            Departments["8328"] = "Global Prime DEX GIP Finance";
            Departments["8330"] = "International Established Fin";
            Departments["8331"] = "Seller Srvcs- FBA EPSO WBA Fin";
            Departments["8332"] = "US Hardlines Finance";
            Departments["8333"] = "Global Retail Sys & Srvcs Fin";
            Departments["8334"] = "Amzn Payment Products Finance";
            Departments["8335"] = "FinOps Projects";
            Departments["8340"] = "Treasury";
            Departments["8341"] = "Risk Mgmt -Workers Comp Claims";
            Departments["8341HR"] = "Risk Mgmt -Workers Comp HR";
            Departments["8342"] = "Benefits";
            Departments["8345"] = "Corporate Development";
            Departments["8500"] = "Digital Products HR";
            Departments["8500HR"] = "Digital Tech Support - HR";
            Departments["8600"] = "Corporate HR";
            Departments["8600"] = "Global HR Solutions";
            Departments["8601"] = "HR Administration";
            Departments["8605"] = "Compensation";
            Departments["8606"] = "HR Ops & Analytics";
            Departments["8610"] = "Central Ops HR";
            Departments["8615"] = "HR Marketorial";
            Departments["8620"] = "HR Systems";
            Departments["8625"] = "International Consumer HR";
            Departments["8630"] = "HR Leadership Development";
            Departments["8640"] = "Talent Mgmt & Org Development";
            Departments["8645"] = "Global Talent Acquisition-SWAT";
            Departments["8650"] = "Global Talent Acquisition";
            Departments["8650A2Z"] = "Global Talent Acquisition-A2Z";
            Departments["8651"] = "Global Talent Acq - Univ Prgms";
            Departments["8652"] = "MBA Recruiting";
            Departments["8653"] = "Executive Recruiting";
            Departments["8654"] = "Diversity Compliance";
            Departments["8655"] = "Central HR";
            Departments["8655AUD"] = "HR Corporate-AUD";
            Departments["8656"] = "G&A Recruiting (Fin,HR, Legal)";
            Departments["8657"] = "Biz Dev & Advertising HR";
            Departments["8658"] = "BusinessDevelopment Recruiting";
            Departments["8659"] = "Consumer Recruiting";
            Departments["8660"] = "HR Operations";
            Departments["866081"] = "HR";
            Departments["8670"] = "HR Consumer";
            Departments["8671"] = "HR Services";
            Departments["8672"] = "Digital HR";
            Departments["8673"] = "Digital Recruiting";
            Departments["8680"] = "AWS HR";
            Departments["8681"] = "HR Seller Services";
            Departments["8682"] = "AWS Recruiting";
            Departments["8683"] = "Seller Services Recruiting";
            Departments["8700"] = "Facilities Administration";
            Departments["8700A2Z"] = "Facilities Administration A2Z";
            Departments["8700HR"] = "Facilities Administration HR";
            Departments["8705"] = "Office Management";
            Departments["8710"] = "Regional Facilities";
            Departments["8710Z"] = "Regional Facilities - Zappos";
            Departments["8715"] = "Security";
            Departments["8720"] = "Ops Real Estate";
            Departments["8725"] = "Facility Vendor Heads";
            Departments["8800"] = "G&A - International Retail";
            Departments["8800HR"] = "G&A - International Retail HR";
            Departments["8801"] = "G&A - North America Retail";
            Departments["8801HR"] = "G&A - North America Retail HR";
            Departments["8802"] = "Central Econ";
            Departments["8810"] = "Global Accounts Receivable";
            Departments["8811"] = "Finance ACES";
            Departments["8883"] = "AWS Legal Variable";
            Departments["CNV"] = "Conversion Dept:MISSING DATA";
        }

        #endregion
    }
}
