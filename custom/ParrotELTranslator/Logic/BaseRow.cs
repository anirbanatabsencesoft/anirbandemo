﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParrotELTranslator.Logic
{
    public abstract class BaseRow
    {
        protected string[] _parts;

        public BaseRow()
        {
            _parts = null;
        }
        public BaseRow(string rowSource) : this()
        {
            Parse(rowSource);
        }

        public char Delimiter { get; set; }

        #region Parsing

        public abstract bool Parse(string rowSource = null);

        protected string Normalize(int index)
        {
            if (index >= _parts.Length)
                return null;
            string data = _parts[index];
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }

        protected int? ParseInt(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            int n;
            if (int.TryParse(data, out n))
                return n;

            return null;
        }

        protected DateTime? ParseDate(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            DateTime dt;
            if (DateTime.TryParseExact(data, new string[] { "yyyy-MM-dd", "MM/dd/yyyy", "M/d/yyyy", "MM/dd/yy", "M/d/yy" }, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out dt))
                return dt.Kind == DateTimeKind.Local ? dt.ToUniversalTime() : dt;

            return null;
        }

        protected byte? ParseByte(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            byte b;
            if (byte.TryParse(data, out b))
                return b;

            return null;
        }

        protected char? ParseChar(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            char c;
            if (char.TryParse(data, out c))
                return c;

            return null;
        }

        protected bool? ParseBool(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (Regex.IsMatch(data, @"^[1YT]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return true;
            if (Regex.IsMatch(data, @"^[0NF]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return false;

            return null;
        }

        protected decimal? ParseDecimal(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            decimal d;
            if (decimal.TryParse(data, out d))
                return d;

            return null;
        }

        protected double? ParseDouble(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            double d;
            if (double.TryParse(data, out d))
                return d;

            return null;
        }

        protected virtual bool Required(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return false;
            return true;
        }

        protected virtual bool In(int index, string name, params string[] values)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return false;
            if (!values.Contains(data))
                return false;
            return true;
        }

        #endregion

        #region ToString

        protected string Emit(string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                string myVal = val;
                if (myVal.Contains('\"') || myVal.Contains(Delimiter))
                {
                    myVal = myVal.Replace("\"", "\"\"");
                    myVal = string.Concat("\"", myVal, "\"");
                }
                return myVal;
            }
            return string.Empty;
        }
        protected string Emit(DateTime? val)
        {
            if (val.HasValue)
                return string.Format("{0:MM/dd/yyyy}", val);
            return string.Empty;
        }
        protected string Emit(bool? val)
        {
            if (val.HasValue)
                return (val.Value ? "Y" : "N");
            return string.Empty;
        }
        protected string Emit(object val)
        {
            if (val == null)
                return string.Empty;
            if (val is bool)
                return Emit((bool)val);
            if (val is DateTime)
                return Emit((DateTime)val);
            if (val != null)
                return Emit(val.ToString());
            return string.Empty;
        }

        #endregion
    }
}
