﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParrotELTranslator.Logic
{
    public class ParrotRow : BaseRow
    {
        #region RecordType

        public struct RecordType
        {
            private string _val;
            private int _fieldCount;

            private RecordType(string type, int fieldCount) { _val = type; _fieldCount = fieldCount; }

            public bool IsValid { get { return !string.IsNullOrWhiteSpace(_val); } }

            public int FieldCount { get { return _fieldCount; } }

            public string Value { get { return _val; } }

            public static readonly RecordType None = new RecordType("", 0);
            public static readonly RecordType PersonalData = new RecordType("EP", 11);
            public static readonly RecordType MailAddress = new RecordType("EA", 14);
            public static readonly RecordType JobData = new RecordType("EJ", 22);
            public static readonly RecordType ScheduleData = new RecordType("ES", 20);
            public static readonly RecordType OrganizationData = new RecordType("EO", 31);
            public static readonly RecordType EmploymentData = new RecordType("EM", 8);
            public static readonly RecordType ClientDefinedData = new RecordType("CD", 5);

            public static RecordType Parse(string value)
            {
                if (string.IsNullOrWhiteSpace(value)) return RecordType.None;
                string test = value.ToUpperInvariant().Trim();
                switch (test)
                {
                    case "EP": return RecordType.PersonalData;
                    case "EA": return RecordType.MailAddress;
                    case "EJ": return RecordType.JobData;
                    case "ES": return RecordType.ScheduleData;
                    case "EO": return RecordType.OrganizationData;
                    case "EM": return RecordType.EmploymentData;
                    case "CD": return RecordType.ClientDefinedData;
                    default: return RecordType.None;
                }
            }
            public static bool operator ==(RecordType val1, RecordType val2) { return val1.Equals(val2); }
            public static bool operator ==(RecordType val1, string val2) { return val1.ToString().Equals(val2); }
            public static bool operator ==(string val1, RecordType val2) { return val1.Equals(val2.ToString()); }
            public static bool operator !=(RecordType val1, RecordType val2) { return !val1.Equals(val2); }
            public static bool operator !=(RecordType val1, string val2) { return !val1.ToString().Equals(val2); }
            public static bool operator !=(string val1, RecordType val2) { return !val1.Equals(val2.ToString()); }
            public override string ToString() { return _val; }
            public override int GetHashCode() { return _val.GetHashCode(); }
            public override bool Equals(object obj) { return base.Equals(obj); }
        }

        #endregion RecordType

        #region Const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;
        private const int EffectiveDateOrdinal = 2;

        //EP
        private const int EmployeeLastNameOrdinal = 3;
        private const int EmployeeFirstNameOrdinal = 4;
        private const int EmployeeMiddleInitialOrdinal = 5;
        private const int NationalIdOrdinal = 6;
        private const int DateOfBirthOrdinal = 7;
        private const int GenderOrdinal = 8;
        private const int MaritalStatusOrdinal = 9;
        private const int SpouseEmployeeIdOrdinal = 10;

        //EA
        private const int EmployeeMailingAddressOrdinal = 3;
        private const int EmployeeMailingAddress2Ordinal = 4;
        private const int EmployeeMailingAddress3Ordinal = 5;
        private const int EmployeeMailingCityOrdinal = 6;
        private const int EmployeeMailingStateOrdinal = 7;
        private const int EmployeeMailingPostalCodeOrdinal = 8;
        private const int EmployeeMailingCountryCodeOrdinal = 9;
        private const int EmployeePhoneOrdinal = 10;
        private const int EmployeeWorkPhoneOrdinal = 11;
        private const int EmployeeWorkPhoneExtOrdinal = 12;
        private const int EmployeeWorkEmailOrdinal = 13;

        //EJ
        private const int JobCodeOrdinal = 3;
        private const int JobTitleOrdinal = 4;
        private const int FullPartTimeOrdinal = 5;
        private const int RegTempOrdinal = 6;
        private const int EmployeeClassOrdinal = 7;
        private const int ExemptOrNonExemptOrdinal = 8;
        private const int CurrencyCodeOrdinal = 9;
        private const int HourlyOrSalaryOrdinal = 10;
        private const int WagePerOrdinal = 11;
        private const int WageRateOrdinal = 12;
        private const int ShiftDifferentialOrdinal = 13;
        private const int PayGroupOrdinal = 14;
        private const int PayLocationOrdinal = 15;
        private const int SupervisorEmployeeIdOrdinal = 16;
        private const int HRContactEmployeeIdOrdinal = 17;
        private const int WorkCityOrdinal = 18;
        private const int WorkStateOrdinal = 19;
        private const int WorkPostalCodeOrdinal = 20;
        private const int WorkCountyCodeOrdinal = 21;

        //ES
        private const int StandardHoursPerWeekOrdinal = 3;
        private const int StandardHoursPerDayOrdinal = 4;
        private const int StandardDaysPerWeekOrdinal = 5;
        private const int MonHoursOrdinal = 6;
        private const int TueHoursOrdinal = 7;
        private const int WedHoursOrdinal = 8;
        private const int ThuHoursOrdinal = 9;
        private const int FriHoursOrdinal = 10;
        private const int SatHoursOrdinal = 11;
        private const int SunHoursOrdinal = 12;
        private const int MonHours2Ordinal = 13;
        private const int TueHours2Ordinal = 14;
        private const int WedHours2Ordinal = 15;
        private const int ThuHours2Ordinal = 16;
        private const int FriHours2Ordinal = 17;
        private const int SatHours2Ordinal = 18;
        private const int SunHours2Ordinal = 19;

        //EO
        private const int PhysicalLocationCode1Ordinal = 3;
        private const int PhysicalLocationDesc1Ordinal = 4;
        private const int PhysicalLocationCode2Ordinal = 5;
        private const int PhysicalLocationDesc2Ordinal = 6;
        private const int PhysicalLocationCode3Ordinal = 7;
        private const int PhysicalLocationDesc3Ordinal = 8;
        private const int PhysicalLocationCode4Ordinal = 9;
        private const int PhysicalLocationDesc4Ordinal = 10;
        private const int PhysicalLocationCode5Ordinal = 11;
        private const int PhysicalLocationDesc5Ordinal = 12;
        private const int PhysicalLocationCode6Ordinal = 13;
        private const int PhysicalLocationDesc6Ordinal = 14;
        private const int PhysicalLocationCode7Ordinal = 15;
        private const int PhysicalLocationDesc7Ordinal = 16;
        private const int OrgCode1Ordinal = 17;
        private const int OrgName1Ordinal = 18;
        private const int OrgCode2Ordinal = 19;
        private const int OrgName2Ordinal = 20;
        private const int OrgCode3Ordinal = 21;
        private const int OrgName3Ordinal = 22;
        private const int OrgCode4Ordinal = 23;
        private const int OrgName4Ordinal = 24;
        private const int OrgCode5Ordinal = 25;
        private const int OrgName5Ordinal = 26;
        private const int OrgCode6Ordinal = 27;
        private const int OrgName6Ordinal = 28;
        private const int OrgCode7Ordinal = 29;
        private const int OrgName7Ordinal = 30;

        //EM
        private const int RehireDateOrdinal = 3;
        private const int HireDateOrdinal = 4;
        private const int TermDateOrdinal = 5;
        private const int AdjustedServiceDateOrdinal = 6;
        private const int EmployeeStatusOrdinal = 7;

        //CD
        private const int AssignedAttributeIdOrdinal = 3;
        private const int AttributeValueOrdinal = 4;

        #endregion Const

        #region .ctor

        public ParrotRow() : base()
        {
            Delimiter = '|';
        }
        public ParrotRow(string rowSource) : base(rowSource) { }

        #endregion .ctor

        #region Fields

        public RecordType RecType { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime? EffectiveDate { get; set; }

        //EP
        public string EmployeeLastName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeMiddleInitial { get; set; }
        public string NationalId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public char? Gender { get; set; } // F = Female, M = Male, U = Unkown
        public char? MaritalStatus { get; set; } // M = Married, K = Unknown, S = Separated, U = Widowed/Divorced/Single/Unmaried
        public string SpouseEmployeeId { get; set; }

        //EA
        public string EmployeeMailingAddress { get; set; }
        public string EmployeeMailingAddress2 { get; set; }
        public string EmployeeMailingAddress3 { get; set; }
        public string EmployeeMailingCity { get; set; }
        public string EmployeeMailingState { get; set; }
        public string EmployeeMailingPostalCode { get; set; }
        public string EmployeeMailingCountryCode { get; set; }
        public string EmployeePhone { get; set; }
        public string EmployeeWorkPhone { get; set; }
        public string EmployeeWorkPhoneExt { get; set; }
        public string EmployeeWorkEmail { get; set; }

        //EJ
        public string JobCode { get; set; }
        public string JobTitle { get; set; }
        public char? FullPartTime { get; set; } // F = Full Time, P = Part Time
        public char? RegTemp { get; set; } // R = Regular, T = Temporary
        public string EmployeeClass { get; set; }
        public char? ExemptOrNonExempt { get; set; } // E, N or null
        public string CurrencyCode { get; set; }
        public char? HourlyOrSalary { get; set; } // H or S
        public string WagePer { get; set; } // W = Weekly, BW = BiWeekly, SM = SemiMonthly, M = Monthly, D = Daily, A = Annual, H = Hourly
        public decimal? WageRate { get; set; }
        public decimal? ShiftDifferential { get; set; }
        public string PayGroup { get; set; }
        public string PayLocation { get; set; }
        public string SupervisorEmployeeId { get; set; }
        public string HRContactEmployeeId { get; set; }
        public string WorkCity { get; set; }
        public string WorkState { get; set; }
        public string WorkPostalCode { get; set; }
        public string WorkCountyCode { get; set; }

        //ES
        public decimal? StandardHoursPerWeek { get; set; }
        public decimal? StandardHoursPerDay { get; set; }
        public decimal? StandardDaysPerWeek { get; set; }
        public decimal? MonHours { get; set; }
        public decimal? TueHours { get; set; }
        public decimal? WedHours { get; set; }
        public decimal? ThuHours { get; set; }
        public decimal? FriHours { get; set; }
        public decimal? SatHours { get; set; }
        public decimal? SunHours { get; set; }
        public decimal? MonHours2 { get; set; }
        public decimal? TueHours2 { get; set; }
        public decimal? WedHours2 { get; set; }
        public decimal? ThuHours2 { get; set; }
        public decimal? FriHours2 { get; set; }
        public decimal? SatHours2 { get; set; }
        public decimal? SunHours2 { get; set; }

        //EO
        public string PhysicalLocationCode1 { get; set; }
        public string PhysicalLocationDesc1 { get; set; }
        public string PhysicalLocationCode2 { get; set; }
        public string PhysicalLocationDesc2 { get; set; }
        public string PhysicalLocationCode3 { get; set; }
        public string PhysicalLocationDesc3 { get; set; }
        public string PhysicalLocationCode4 { get; set; }
        public string PhysicalLocationDesc4 { get; set; }
        public string PhysicalLocationCode5 { get; set; }
        public string PhysicalLocationDesc5 { get; set; }
        public string PhysicalLocationCode6 { get; set; }
        public string PhysicalLocationDesc6 { get; set; }
        public string PhysicalLocationCode7 { get; set; }
        public string PhysicalLocationDesc7 { get; set; }
        public string OrgCode1 { get; set; }
        public string OrgName1 { get; set; }
        public string OrgCode2 { get; set; }
        public string OrgName2 { get; set; }
        public string OrgCode3 { get; set; }
        public string OrgName3 { get; set; }
        public string OrgCode4 { get; set; }
        public string OrgName4 { get; set; }
        public string OrgCode5 { get; set; }
        public string OrgName5 { get; set; }
        public string OrgCode6 { get; set; }
        public string OrgName6 { get; set; }
        public string OrgCode7 { get; set; }
        public string OrgName7 { get; set; }

        //EM
        public DateTime? RehireDate { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? TermDate { get; set; }
        public DateTime? AdjustedServiceDate { get; set; }
        public char? EmployeeStatus { get; set; } // A = Active, D = Deceased, T = Terminated, L = Leave, I = Inactive, P = Leave w/ Pay, S = Suspended, R = Retired

        //ER
        public DateTime? RoleEndDate { get; set; }
        public string RoleName { get; set; }
        public string RoleDomain { get; set; }
        public string RoleTargetId { get; set; }

        //CD
        public string AssignedAttributeId { get; set; }
        public string AttributeValue { get; set; }

        #endregion Fields

        #region Parsing

        public override bool Parse(string rowSource = null)
        {
            _parts = null;

            if (string.IsNullOrWhiteSpace(rowSource))
                return false;

            // Determine the delimiter (test for tabs, then for pipes, otherwise assume commas)
            if (rowSource.Contains('\t')) Delimiter = '\t';
            else if (rowSource.Contains('|')) Delimiter = '|';
            else Delimiter = ',';

            using (StringReader reader = new StringReader(rowSource))
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
            {
                parser.HasFieldsEnclosedInQuotes = Delimiter == ',';
                parser.SetDelimiters(Delimiter.ToString());
                if (!parser.EndOfData)
                    _parts = parser.ReadFields();
            }
            //_parts = rowSource.Split('|');
            if (_parts == null || _parts.Length == 1)
                return false;

            //
            // Record Type
            if (Required(RecordTypeOrdinal, "Record Type"))
                RecType = RecordType.Parse(_parts[RecordTypeOrdinal]);
            else
                return false;
            if (!RecType.IsValid)
                return false;

            // =============
            // Shared Fields
            // =============
            EmployeeNumber = Normalize(EmployeeNumberOrdinal);
            EffectiveDate = ParseDate(EffectiveDateOrdinal, "Effective Date");

            // =============
            // EP
            // =============
            //
            if (RecType == RecordType.PersonalData)
            {
                EmployeeLastName = Normalize(EmployeeLastNameOrdinal);
                EmployeeFirstName = Normalize(EmployeeFirstNameOrdinal);
                EmployeeMiddleInitial = Normalize(EmployeeMiddleInitialOrdinal);
                NationalId = Normalize(NationalIdOrdinal);
                DateOfBirth = ParseDate(DateOfBirthOrdinal, "Date of Birth");
                Gender = ParseChar(GenderOrdinal, "Gender");
                MaritalStatus = ParseChar(MaritalStatusOrdinal, "Marital Status");
                SpouseEmployeeId = Normalize(SpouseEmployeeIdOrdinal);
            }
            // =============
            // EA
            // =============
            //
            else if (RecType == RecordType.MailAddress)
            {
                EmployeeMailingAddress = Normalize(EmployeeMailingAddressOrdinal);
                EmployeeMailingAddress2 = Normalize(EmployeeMailingAddress2Ordinal);
                EmployeeMailingAddress3 = Normalize(EmployeeMailingAddress3Ordinal);
                EmployeeMailingCity = Normalize(EmployeeMailingCityOrdinal);
                EmployeeMailingState = Normalize(EmployeeMailingStateOrdinal);
                EmployeeMailingPostalCode = Normalize(EmployeeMailingPostalCodeOrdinal);
                EmployeeMailingCountryCode = Normalize(EmployeeMailingCountryCodeOrdinal);
                EmployeePhone = Normalize(EmployeePhoneOrdinal);
                EmployeeWorkPhone = Normalize(EmployeeWorkPhoneOrdinal);
                EmployeeWorkPhoneExt = Normalize(EmployeeWorkPhoneExtOrdinal);
                EmployeeWorkEmail = Normalize(EmployeeWorkEmailOrdinal);
            }
            // =============
            // EJ
            // =============
            //
            else if (RecType == RecordType.JobData)
            {
                JobCode = Normalize(JobCodeOrdinal);
                JobTitle = Normalize(JobTitleOrdinal);
                FullPartTime = ParseChar(FullPartTimeOrdinal, "Full/Part Time");
                RegTemp = ParseChar(RegTempOrdinal, "Regular/Temp");
                EmployeeClass = Normalize(EmployeeClassOrdinal);
                ExemptOrNonExempt = ParseChar(ExemptOrNonExemptOrdinal, "Exempt or Non-exempt");
                CurrencyCode = Normalize(CurrencyCodeOrdinal);
                HourlyOrSalary = ParseChar(HourlyOrSalaryOrdinal, "Hours or Salary");
                WagePer = Normalize(WagePerOrdinal);
                WageRate = ParseDecimal(WageRateOrdinal, "Wage Rate");
                ShiftDifferential = ParseDecimal(ShiftDifferentialOrdinal, "Shift Differential");
                PayGroup = Normalize(PayGroupOrdinal);
                PayLocation = Normalize(PayLocationOrdinal);
                SupervisorEmployeeId = Normalize(SupervisorEmployeeIdOrdinal);
                HRContactEmployeeId = Normalize(HRContactEmployeeIdOrdinal);
                WorkCity = Normalize(WorkCityOrdinal);
                WorkState = Normalize(WorkStateOrdinal);
                WorkPostalCode = Normalize(WorkPostalCodeOrdinal);
                WorkCountyCode = Normalize(WorkCountyCodeOrdinal);
            }
            // =============
            // ES
            // =============
            //
            else if (RecType == RecordType.ScheduleData)
            {
                StandardHoursPerWeek = ParseDecimal(StandardHoursPerWeekOrdinal, "Standard Hours per Week");
                StandardHoursPerDay = ParseDecimal(StandardHoursPerDayOrdinal, "Standard Hours per Day");
                StandardDaysPerWeek = ParseDecimal(StandardDaysPerWeekOrdinal, "Standard Days per Week");
                MonHours = ParseDecimal(MonHoursOrdinal, "Mon Hours");
                TueHours = ParseDecimal(TueHoursOrdinal, "Tue Hours");
                WedHours = ParseDecimal(WedHoursOrdinal, "Wed Hours");
                ThuHours = ParseDecimal(ThuHoursOrdinal, "Thu Hours");
                FriHours = ParseDecimal(FriHoursOrdinal, "Fri Hours");
                SatHours = ParseDecimal(SatHoursOrdinal, "Sat Hours");
                SunHours = ParseDecimal(SunHoursOrdinal, "Sun Hours");
                MonHours2 = ParseDecimal(MonHours2Ordinal, "Mon 2 Hours");
                TueHours2 = ParseDecimal(TueHours2Ordinal, "Tue 2 Hours");
                WedHours2 = ParseDecimal(WedHours2Ordinal, "Wed 2 Hours");
                ThuHours2 = ParseDecimal(ThuHours2Ordinal, "Thu 2 Hours");
                FriHours2 = ParseDecimal(FriHours2Ordinal, "Fri 2 Hours");
                SatHours2 = ParseDecimal(SatHours2Ordinal, "Sat 2 Hours");
                SunHours2 = ParseDecimal(SunHours2Ordinal, "Sun 2 Hours");
            }
            // =============
            // EO
            // =============
            //
            else if (RecType == RecordType.OrganizationData)
            {
                PhysicalLocationCode1 = Normalize(PhysicalLocationCode1Ordinal);
                PhysicalLocationDesc1 = Normalize(PhysicalLocationDesc1Ordinal);
                PhysicalLocationCode2 = Normalize(PhysicalLocationCode2Ordinal);
                PhysicalLocationDesc2 = Normalize(PhysicalLocationDesc2Ordinal);
                PhysicalLocationCode3 = Normalize(PhysicalLocationCode3Ordinal);
                PhysicalLocationDesc3 = Normalize(PhysicalLocationDesc3Ordinal);
                PhysicalLocationCode4 = Normalize(PhysicalLocationCode4Ordinal);
                PhysicalLocationDesc4 = Normalize(PhysicalLocationDesc4Ordinal);
                PhysicalLocationCode5 = Normalize(PhysicalLocationCode5Ordinal);
                PhysicalLocationDesc5 = Normalize(PhysicalLocationDesc5Ordinal);
                PhysicalLocationCode6 = Normalize(PhysicalLocationCode6Ordinal);
                PhysicalLocationDesc6 = Normalize(PhysicalLocationDesc6Ordinal);
                PhysicalLocationCode7 = Normalize(PhysicalLocationCode7Ordinal);
                PhysicalLocationDesc7 = Normalize(PhysicalLocationDesc7Ordinal);
                OrgCode1 = Normalize(OrgCode1Ordinal);
                OrgName1 = Normalize(OrgName1Ordinal);
                OrgCode2 = Normalize(OrgCode2Ordinal);
                OrgName2 = Normalize(OrgName2Ordinal);
                OrgCode3 = Normalize(OrgCode3Ordinal);
                OrgName3 = Normalize(OrgName3Ordinal);
                OrgCode4 = Normalize(OrgCode4Ordinal);
                OrgName4 = Normalize(OrgName4Ordinal);
                OrgCode5 = Normalize(OrgCode5Ordinal);
                OrgName5 = Normalize(OrgName5Ordinal);
                OrgCode6 = Normalize(OrgCode6Ordinal);
                OrgName6 = Normalize(OrgName6Ordinal);
                OrgCode7 = Normalize(OrgCode7Ordinal);
                OrgName7 = Normalize(OrgName7Ordinal);
            }
            // =============
            // EM
            // =============
            //
            else if (RecType == RecordType.EmploymentData)
            {
                RehireDate = ParseDate(RehireDateOrdinal, "Most Recent Date of Hire");
                HireDate = ParseDate(HireDateOrdinal, "Original Date of Hire");
                TermDate = ParseDate(TermDateOrdinal, "Employment Term Date");
                AdjustedServiceDate = ParseDate(AdjustedServiceDateOrdinal, "Adjusted Service Date");
                EmployeeStatus = ParseChar(EmployeeStatusOrdinal, "Employee Status");
            }
            // =============
            // CD
            // =============
            //
            else if (RecType == RecordType.ClientDefinedData)
            {
                AssignedAttributeId = Normalize(AssignedAttributeIdOrdinal);
                AttributeValue = Normalize(AttributeValueOrdinal);
            }

            return true;
        }

        #endregion Parsing

        public List<EligibilityRow> ToEligibilityRows()
        {
            if (!RecType.IsValid) return new List<EligibilityRow>(0);
            List<EligibilityRow> rows = new List<EligibilityRow>();

            EligibilityRow row = new EligibilityRow();
            row.EmployeeNumber = EmployeeNumber;
            rows.Add(row);

            if (RecType == RecordType.PersonalData)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                row.LastName = EmployeeLastName;
                row.FirstName = EmployeeFirstName;
                row.MiddleName = EmployeeMiddleInitial;
                row.DateOfBirth = DateOfBirth;
                row.Gender = Gender ?? 'U';
            }
            else if (RecType == RecordType.MailAddress)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                row.Address = EmployeeMailingAddress;
                row.Address2 = string.Concat(EmployeeMailingAddress2, " ", EmployeeMailingAddress3).Trim();
                row.City = EmployeeMailingCity;
                row.State = EmployeeMailingState;
                row.PostalCode = EmployeeMailingPostalCode;
                row.Country = EmployeeMailingCountryCode;
                if (!string.IsNullOrWhiteSpace(row.Country) && row.Country.Length > 2)
                    row.Country = row.Country.Substring(0, 2);
                row.PhoneHome = EmployeePhone;
                row.PhoneWork = string.Format("{0}{1}{2}", EmployeeWorkPhone, string.IsNullOrWhiteSpace(EmployeeWorkPhoneExt) ? "" : " x", EmployeeWorkPhoneExt);
                row.Email = EmployeeWorkEmail;
            }
            else if (RecType == RecordType.JobData)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                if (!string.IsNullOrWhiteSpace(SupervisorEmployeeId))
                {
                    rows.Add(new EligibilityRow()
                    {
                        RecType = EligibilityRow.RecordType.Supervisor,
                        EmployeeNumber = EmployeeNumber,
                        ManagerEmployeeNumber = SupervisorEmployeeId
                    });
                }
                if (!string.IsNullOrWhiteSpace(HRContactEmployeeId))
                {
                    rows.Add(new EligibilityRow()
                    {
                        RecType = EligibilityRow.RecordType.HR,
                        EmployeeNumber = EmployeeNumber,
                        HREmployeeNumber = HRContactEmployeeId
                    });
                }
                row.JobTitle = JobTitle;
                row.EmploymentType = FullPartTime == 'F' ? WorkType.FullTime : FullPartTime == 'P' ? WorkType.PartTime : "";
                row.ExemptionStatus = ExemptOrNonExempt;
                row.PayType = HourlyOrSalary == 'S' ? 1 : HourlyOrSalary == 'H' ? 2 : new byte?();
                row.PayRate = WageRate;
                row.WorkState = WorkState;
                row.WorkCountry = WorkCountyCode;
                if (!string.IsNullOrWhiteSpace(row.WorkCountry) && row.WorkCountry.Length > 2)
                    row.WorkCountry = row.WorkCountry.Substring(0, 2);
                row.EmployeeClassification = EmployeeClass;
            }
            else if (RecType == RecordType.ScheduleData)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                if (StandardHoursPerWeek.HasValue)
                    row.MinutesPerWeek = GetMinutes(StandardHoursPerWeek);
                else if (StandardHoursPerDay.HasValue && StandardDaysPerWeek.HasValue)
                    row.MinutesPerWeek = GetMinutes(StandardHoursPerDay, StandardDaysPerWeek);
                row.WorkTimeMon = GetMinutes(MonHours);
                row.WorkTimeTue = GetMinutes(TueHours);
                row.WorkTimeWed = GetMinutes(WedHours);
                row.WorkTimeThu = GetMinutes(ThuHours);
                row.WorkTimeFri = GetMinutes(FriHours);
                row.WorkTimeSat = GetMinutes(SatHours);
                row.WorkTimeSun = GetMinutes(SunHours);
            }
            else if (RecType == RecordType.OrganizationData)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                row.JobLocation = PhysicalLocationCode5 ?? PhysicalLocationCode4;
                row.CostCenterCode = OrgCode7;
                row.CostCenterName = OrgName7;
            }
            else if (RecType == RecordType.EmploymentData)
            {
                row.RecType = EligibilityRow.RecordType.Eligibility;
                row.RehireDate = RehireDate;
                row.HireDate = HireDate;
                if (HireDate >= RehireDate) row.RehireDate = null;
                row.TerminationDate = TermDate;
                row.AdjustedServiceDate = AdjustedServiceDate ?? RehireDate ?? HireDate;
                if (EmployeeStatus.HasValue)
                {
                    switch (EmployeeStatus.Value)
                    {
                        case 'A': // A- Active
                        case 'a':
                            row.EmploymentStatus = 'A'; // Active
                            break;
                        case 'L': // L- Leave
                        case 'l':
                        case 'I': // I- Inactive
                        case 'i':
                        case 'P': // P- Leave with Pay
                        case 'p':
                        case 'S': // S- Suspended
                        case 's':
                            row.EmploymentStatus = 'I'; // Inactive
                            break;
                        case 'D': // D- Deceased
                        case 'd':
                        case 'T': // T- Terminated
                        case 't':
                        case 'R': // R- Retired
                        case 'r':
                            row.EmploymentStatus = 'T'; // Terminated (retired, layoff, etc.)
                            break;
                    }
                }
            }

            return rows;
        }

        protected virtual int? GetMinutes(decimal? hours)
        {
            return GetMinutes(hours, 1M);
        }

        protected virtual int? GetMinutes(decimal? hours, decimal? factor)
        {
            if (hours == null)
                return null;

            return Convert.ToInt32(Math.Ceiling(hours.Value * (factor ?? 1M) * 60M));
        }
    }
}
