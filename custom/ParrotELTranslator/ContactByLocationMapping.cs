﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParrotELTranslator
{
    public class ContactByLocationMapping
    {
        public ContactByLocationMapping()
        {
            Locations = new List<string>();
        }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the locations.
        /// </summary>
        /// <value>
        /// The locations.
        /// </value>
        public List<string> Locations { get; set; }

        /// <summary>
        /// Gets or sets the contact type code.
        /// </summary>
        /// <value>
        /// The contact type code.
        /// </value>
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Employee #: '{0}', {1} for {2}", EmployeeNumber, ContactTypeCode, string.Join(", ", Locations));
        }
    }
}
