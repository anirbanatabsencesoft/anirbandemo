﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ParrotELTranslator.Logic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace ParrotELTranslator
{
    static class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static string CustomerId { get; set; } = "546e5097a32aa00d60e3210a";
        public static string EmployerId { get; set; } = "546e52cda32aa00f78086269";
        public static List<Employee> CurrentEmployees { get; set; }
        public static List<Case> CurrentOpenCases { get; set; }

        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            log.InfoFormat("ParrotELTranslator started with '{0}'", Environment.CommandLine);

            string fileName = null;
            int rowCount = 0;
            bool buildContacts = false;
            string otherContactsFileName = null;
            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                    fileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-c")
                    CustomerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-e")
                    EmployerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-contacts")
                    buildContacts = true;
                else if (a == "-o")
                    otherContactsFileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    ParrotELTranslator.exe [-f <file path>] [-c <customer id>] [-e <employer id>] [-contacts] [-o <file path>]");
                    Console.WriteLine();
                    Console.WriteLine("    -f <file path>: Specifies the file path to be converted");
                    Console.WriteLine("    -c <cusotmer id>: Specifies a customer Id OTHER than parrot's");
                    Console.WriteLine("    -e <employer id>: Specifies an employer Id OTHER than parrot's");
                    Console.WriteLine("    -contacts: Perform contact mapping, with or without import");
                    Console.WriteLine("    -o <file path>: specifies the post 'Contact' contact mapping file");
                    Console.WriteLine();
                    Environment.Exit(0);
                    return;
                }
            }
            if (!string.IsNullOrWhiteSpace(otherContactsFileName) && !File.Exists(otherContactsFileName))
            {
                log.ErrorFormat("-o {0} points to a file that does not exist or you do not have permission to view", otherContactsFileName);
                Environment.Exit(9);
                return;
            }

            int curDone = 0;
            decimal curPerc = 0M;
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                bool error = false;
                if (!File.Exists(fileName))
                {
                    log.ErrorFormat("The file -f '{0}' was not found", fileName);
                    error = true;
                }
                if (error)
                    Environment.Exit(9);

                rowCount = File.ReadLines(fileName).Count();
                log.InfoFormat("Wow! The file has {0} rows, nice!", rowCount);

                List<string> megaRows = new List<string>(rowCount);

                log.InfoFormat("Reading file '{0}'", fileName);
                using (StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (string.IsNullOrWhiteSpace(line) || line.Trim().Length < 2)
                            continue;
                        var t = line.Substring(0, 2);
                        switch (t)
                        {
                            case "EP":
                            case "EA":
                            case "EJ":
                            case "ES":
                            case "EO":
                            case "EM":
                            case "CD":
                                megaRows.Add(line);
                                break;
                        }
                    }
                }
                log.InfoFormat("Done reading file '{0}'", fileName);

                log.Info("Aggregating and saving records to database");

                CurrentEmployees = new List<Employee>(Employee.AsQueryable().Where(e => e.EmployerId == EmployerId).Count());
                CurrentEmployees.AddRange(Employee.Query.Find(Employee.Query.EQ(e => e.EmployerId, EmployerId))
                    .SetBatchSize(1000)
                    .SetMaxTime(TimeSpan.FromMinutes(30)));
                log.InfoFormat("{0} total employees already exist for this employer", CurrentEmployees.Count);

                CurrentOpenCases = new List<Case>(Case.AsQueryable().Where(e => e.EmployerId == EmployerId && e.Status == CaseStatus.Open).Count());

                CurrentOpenCases.AddRange(Case.Query.Find(Case.Query.And(Case.Query.EQ(c => c.EmployerId, EmployerId), Case.Query.EQ(c => c.Status, CaseStatus.Open)))
                    .SetBatchSize(1000)
                    .SetMaxTime(TimeSpan.FromMinutes(30)));

                log.Info("0.00% complete...");
                curDone = 0;
                curPerc = 0M;

                megaRows.GroupBy(l => l.Split('|')[1]).Batch(3000).ToList().ForEach(empGroup =>
                {
                    log.InfoFormat("Processing a batch of {0} employees", empGroup.Count());
                    BulkWriteOperation<Employee> bulkEmployees = null;
                    BulkWriteOperation<EmployeeContact> bulkContacts = null;
                    BulkWriteOperation<ToDoItem> bulkTodos = null;
                    BulkWriteOperation<Organization> bulkOrganization = null;
                    BulkWriteOperation<EmployeeOrganization> bulkEmployeeOrganization = null;
                    List<string> officeCodes = new List<string>();
                    foreach (var grp in empGroup)
                    {
                        try
                        {
                            RowAggregator.Aggregate(grp).Sparkle(ref bulkEmployees, ref bulkContacts, ref bulkTodos, ref bulkOrganization, ref bulkEmployeeOrganization, officeCodes);
                        }
                        catch (Exception ex)
                        {
                            log.Error(string.Format("Error processing Employee HRID '{0}'", grp.Key), ex);
                        }
                        curDone += grp.Count();

                        if ((curPerc + 0.01M) < ((decimal)curDone / (decimal)rowCount))
                        {
                            curPerc = ((decimal)curDone / (decimal)rowCount);
                            log.InfoFormat("{0:N2}% complete...", curPerc * 100M);
                        }
                    }
                    try
                    {
                        if (bulkEmployees != null)
                        {
                            bulkEmployees.Execute();
                        }
                        if (bulkContacts != null)
                        {
                            bulkContacts.Execute();
                        }
                        if (bulkTodos != null)
                        {
                            bulkTodos.Execute();
                        }
                        if (officeCodes.Count > 0)
                        {
                            if (bulkOrganization != null)
                            {
                                bulkOrganization.Execute();
                            }
                            if (bulkEmployeeOrganization != null)
                            {
                                bulkEmployeeOrganization.Execute();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error bulk upserting contacts/todos for employee batch", ex);
                    }

                    log.Info("That was awesome, now let's see how the server is doing...");
                    int tried = 0;
                    while (tried < 5 && Employee.Repository.Collection.Database.RunCommand("serverStatus").Response.GetRawValue<bool>("writeBacksQueued"))
                    {
                        log.Warn("Uh oh, write-backs are queued, let's wait another minute; up to 5 minutes.");
                        Thread.Sleep(TimeSpan.FromMinutes(1));
                        tried++;
                    }
                    log.Info("Server's good to go now, let's continue.");
                });

                megaRows.Clear();
                log.Info("Aggregated and saved all records to database");
            }

            if (CurrentEmployees != null || CurrentOpenCases != null)
            {
                if (CurrentEmployees != null)
                    CurrentEmployees.Clear();
                CurrentEmployees = null;
                if (CurrentOpenCases != null)
                    CurrentOpenCases.Clear();
                CurrentOpenCases = null;
            }

            if (buildContacts)
            {
                log.Info("Mapping HR Contacts missing Employee Number by Email");
                log.Info("0.00% complete...");
                curDone = 0;
                curPerc = 0M;
                var allEmployees = Employee.Repository.Collection.Database.GetCollection(Employee.Repository.CollectionName).Find(Employee.Query.And(
                    Employee.Query.EQ(e => e.CustomerId, CustomerId),
                    Employee.Query.EQ(e => e.EmployerId, EmployerId),
                    Employee.Query.NE(e => e.Info.Email, null)
                )).SetBatchSize(1000).SetFields(Fields.Include("_id", "EmployeeNumber", "Info.Email")).ToList()
                .Select(d => new
                {
                    EmployeeNumber = d.GetRawValue<string>("EmployeeNumber"),
                    Email = d["Info"].AsBsonDocument.GetRawValue<string>("Email").ToLowerInvariant()
                }).ToList();

                var contactsQuery = EmployeeContact.Repository.Collection.Database.GetCollection(EmployeeContact.Repository.CollectionName).Find(EmployeeContact.Query.And(
                    EmployeeContact.Query.EQ(e => e.CustomerId, CustomerId),
                    EmployeeContact.Query.EQ(e => e.EmployerId, EmployerId),
                    EmployeeContact.Query.EQ(e => e.ContactTypeCode, "HR"),
                    EmployeeContact.Query.EQ(e => e.RelatedEmployeeNumber, null),
                    EmployeeContact.Query.NE(e => e.Contact.Email, null)
                )).SetBatchSize(5000).SetFields(Fields.Include("_id", "Contact.Email"));
                var count = contactsQuery.Count();
                log.InfoFormat("Found {0:N0} contacts without employee numbers but with emails", count);

                if (count > 0)
                {
                    bool leftovers = false;
                    var bulkyMan = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                    foreach (var ctc in contactsQuery)
                    {
                        curDone += 1;

                        var id = ctc.GetRawValue<string>("_id");
                        var email = ctc["Contact"].AsBsonDocument.GetRawValue<string>("Email").ToLowerInvariant();

                        var match = allEmployees.FirstOrDefault(e => string.Equals(e.Email, email, StringComparison.InvariantCultureIgnoreCase));
                        // If no match, ok, that's cool, go on to the next one
                        if (match != null)
                        {
                            // Update our contact
                            bulkyMan.Find(EmployeeContact.Query.EQ(t => t.Id, id))
                                .UpdateOne(EmployeeContact.Updates.Set(t => t.RelatedEmployeeNumber, match.EmployeeNumber));
                            leftovers = true;
                        }

                        if (leftovers && curDone % 5000 == 0)
                        {
                            log.InfoFormat("Running update batch @ {0:N0}...", curDone);
                            bulkyMan.Execute();
                            log.Info("Update batch complete");
                            bulkyMan = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                            leftovers = false;
                        }

                        if ((curPerc + 0.01M) < (curDone / count))
                        {
                            curPerc = (curDone / count);
                            log.InfoFormat("{0:N2}% complete...", curPerc * 100M);
                        }
                    }
                    if (leftovers && bulkyMan != null)
                    {
                        log.InfoFormat("Running update batch @ {0:N0}...", curDone);
                        bulkyMan.Execute();
                        log.Info("Update batch complete");
                    }
                }

                allEmployees.Clear();
                log.Info("Finished Mapping HR Contacts by Email");
            }

            if (!string.IsNullOrWhiteSpace(otherContactsFileName))
            {
                log.Info("Mapping Employee Contacts by Location from File");
                List<ContactType> types = ContactType.AsQueryable().Where(r => r.CustomerId == CustomerId && r.EmployerId == EmployerId).ToList();

                Dictionary<string, ContactByLocationMapping> map = new Dictionary<string, ContactByLocationMapping>();
                foreach (var line in File.ReadLines(otherContactsFileName))
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }
                    char delim;
                    // Determine the delimiter (test for tabs, then for pipes, otherwise assume commas)
                    if (line.Contains('\t'))
                    {
                        delim = '\t';
                    }
                    else if (line.Contains('|'))
                    {
                        delim = '|';
                    }
                    else
                    {
                        delim = ',';
                    }

                    string[] parts = null;

                    using (StringReader reader = new StringReader(line))
                    using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
                    {
                        parser.HasFieldsEnclosedInQuotes = true;
                        parser.SetDelimiters(delim.ToString());
                        if (!parser.EndOfData)
                        {
                            parts = parser.ReadFields();
                        }
                    }
                    if (parts == null || parts.Length < 7)
                    {
                        continue;
                    }

                    ContactByLocationMapping me = null;
                    if (map.ContainsKey(parts[0]))
                    {
                        me = map[parts[0]];
                    }
                    else
                    {
                        me = new ContactByLocationMapping()
                        {
                            EmployeeNumber = parts[0],
                            ContactTypeCode = parts[6] ?? "WC"
                        };
                        map.Add(me.EmployeeNumber, me);
                    }
                    me.Locations.AddIfNotExists(parts[5]);
                }
                log.InfoFormat("Found {0} Contacts to map to {1} locations", map.Count, map.Sum(m => m.Value.Locations.Count));

                foreach (var dude in map.Values)
                {
                    var emp = Employee.AsQueryable().Where(e => e.CustomerId == CustomerId && e.EmployerId == EmployerId && e.EmployeeNumber == dude.EmployeeNumber).FirstOrDefault();
                    if (emp == null)
                    {
                        log.WarnFormat("Employee not found for {0}", dude);
                        continue;
                    }
                    var ct = types.FirstOrDefault(t => string.Equals(t.Code, dude.ContactTypeCode, StringComparison.InvariantCultureIgnoreCase));
                    if (ct == null)
                    {
                        log.WarnFormat("Employee contact type '{0}' was not found for {1}", dude.ContactTypeCode, dude);
                        continue;
                    }
                    log.InfoFormat("Pulling list of employees in ('{0}') for {1}: {2}", string.Join("', '", dude.Locations), emp.EmployeeNumber, emp.FullName);
                    var fromMongoEmployees = Employee.Repository.Collection.FindAs<BsonDocument>(Employee.Query.And(
                        Employee.Query.EQ(e => e.CustomerId, CustomerId),
                        Employee.Query.EQ(e => e.EmployerId, EmployerId),
#pragma warning disable CS0618 // Type or member is obsolete
                        Employee.Query.In(e => e.Info.OfficeLocation, dude.Locations)
#pragma warning restore CS0618 // Type or member is obsolete
                    )).SetFields(Fields.Include("_id").Include("EmployeeNumber"))
                    .ToList();
                    var dudesEmployees = fromMongoEmployees
                        .Select(d => new { Id = d.GetRawValue<string>("_id"), EmployeeNumber = d.GetRawValue<string>("EmployeeNumber") })
                        .ToList();
                    log.InfoFormat("Found {0} employees for {1} to be a '{2}' contact of", dudesEmployees.Count, emp.FullName, ct.Code);

                    var bulky = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkyNum = 0x0;
                    foreach (var empId in dudesEmployees)
                    {
                        bulky
                            .Find(EmployeeContact.Query.And(
                                EmployeeContact.Query.EQ(c => c.CustomerId, CustomerId),
                                EmployeeContact.Query.EQ(c => c.EmployerId, EmployerId),
                                EmployeeContact.Query.EQ(c => c.EmployeeId, empId.Id),
                                EmployeeContact.Query.EQ(c => c.ContactTypeCode, ct.Code),
                                EmployeeContact.Query.EQ(c => c.RelatedEmployeeNumber, emp.EmployeeNumber)
                            ))
                            .Upsert()
                            .UpdateOne(Update
                                .Set("Contact.FirstName", emp.FirstName == null ? BsonNull.Value : (BsonValue)new BsonString(emp.FirstName))
                                .Set("Contact.MiddleName", emp.MiddleName == null ? BsonNull.Value : (BsonValue)new BsonString(emp.MiddleName))
                                .Set("Contact.LastName", emp.LastName == null ? BsonNull.Value : (BsonValue)new BsonString(emp.LastName))
                                .Set("Contact.Email", emp.Info.Email == null ? BsonNull.Value : (BsonValue)new BsonString(emp.Info.Email))
                                .Set("Contact.WorkPhone", emp.Info.WorkPhone == null ? BsonNull.Value : (BsonValue)new BsonString(emp.Info.WorkPhone))
                                .Set("Contact.CellPhone", emp.Info.CellPhone == null ? BsonNull.Value : (BsonValue)new BsonString(emp.Info.CellPhone))
                                .Set("Contact.Address.Country", new BsonString(emp.Info.Address.Country ?? "US"))
                                .Set("mby", new BsonObjectId(new ObjectId(User.DefaultUserId)))
                                .Set("mdt", new BsonDateTime(DateTime.UtcNow))
                                .Set("Level", new BsonString(dude.ContactTypeCode))
                                // Purposefully tag this contact with a "contingent worker process" flag
                                .Set("Source", new BsonString("CWP"))
                                .Set("IsDeleted", false)
                                .SetOnInsert("CustomerId", new BsonObjectId(new ObjectId(CustomerId)))
                                .SetOnInsert("EmployerId", new BsonObjectId(new ObjectId(EmployerId)))
                                .SetOnInsert("EmployeeId", new BsonObjectId(new ObjectId(empId.Id)))
                                .SetOnInsert("EmployeeNumber", new BsonString(empId.EmployeeNumber))
                                .SetOnInsert("Contact._id", new BsonString(Guid.NewGuid().ToString("D")))
                                .SetOnInsert("Contact.Address._id", new BsonString(Guid.NewGuid().ToString("D")))
                                .SetOnInsert("RelatedEmployeeNumber", new BsonString(emp.EmployeeNumber))
                                .SetOnInsert("cby", new BsonObjectId(new ObjectId(User.DefaultUserId)))
                                .SetOnInsert("cdt", new BsonDateTime(DateTime.UtcNow))
                                .SetOnInsert("MilitaryStatus", new BsonInt32((int)MilitaryStatus.Civilian))
                                .SetOnInsert("ContactTypeCode", new BsonString(ct.Code))
                                .SetOnInsert("ContactTypeName", new BsonString(ct.Name ?? ct.Code))
                            );
                        bulkyNum++;
                    }
                    if (bulkyNum > 0)
                    {
                        log.InfoFormat("Starting bulk mapping operation for {0}", emp.FullName);
                        bulky.Execute();
                        log.InfoFormat("Finished bulk mapping operation for {0}", emp.FullName);
                    }

                    log.InfoFormat("Removing contact mappings that are no longer relevant for {0}", emp.FullName);
                    EmployeeContact.Repository.Collection.Update(EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(c => c.CustomerId, CustomerId),
                        EmployeeContact.Query.EQ(c => c.EmployerId, EmployerId),
                        EmployeeContact.Query.NotIn(c => c.EmployeeId, dudesEmployees.Select(d => d.Id)),
                        EmployeeContact.Query.EQ(c => c.ContactTypeCode, ct.Code),
                        EmployeeContact.Query.EQ(c => c.RelatedEmployeeNumber, emp.EmployeeNumber)
                    ), EmployeeContact.Updates
                        .Set(e => e.IsDeleted, true)
                        .Set(e => e.ModifiedById, User.DefaultUserId)
                        .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                    log.InfoFormat("Finished removing contact mappings that are no longer relevant for {0}", emp.FullName);
                }

                log.Info("Finished Mapping HR Contacts by Location from File");

                log.Info("Removing any old contingent worker mappings for users not on this file...");
                EmployeeContact.Repository.Collection.Update(EmployeeContact.Query.And(
                        EmployeeContact.Query.EQ(c => c.CustomerId, CustomerId),
                        EmployeeContact.Query.EQ(c => c.EmployerId, EmployerId),
                        Query.EQ("Source", "CWP"),
                        EmployeeContact.Query.NotIn(c => c.RelatedEmployeeNumber, map.Values.Select(v => v.EmployeeNumber))
                    ), EmployeeContact.Updates
                        .Set(e => e.IsDeleted, true)
                        .Set(e => e.ModifiedById, User.DefaultUserId)
                        .CurrentDate(e => e.ModifiedDate), UpdateFlags.Multi);
                log.Info("Done removing those old contingent worker mappings");
            }


            watch.Stop();
            log.InfoFormat("Finished in '{0}'", watch.Elapsed);
            Console.WriteLine();
            Console.WriteLine("Have a nice day! :)");

            // TODO: Email report, notice, log or whatever.
        }
    }
}
