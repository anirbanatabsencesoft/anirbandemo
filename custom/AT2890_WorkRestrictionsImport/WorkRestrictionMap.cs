﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AT2890.WorkRestrictionsImport
{
    public class WorkRestrictionMap
    {
        public WorkRestrictionMap() { }

        public WorkRestrictionMap(string[] row) : this()
        {
            //Case Number,Employee Number,Demand Code,Demand Type,Value,Start Date,End Date,,,,,+
            if (row == null || row.Length == 0)
                return;
            CaseNumber = Normalize(row, 0);
            EmployeeNumber = Normalize(row, 1);
            DemandCode = Normalize(row, 2);
            DemandType = Normalize(row, 3);
            Value = Normalize(row, 4);
            StartDate = ParseDate(row, 5);
            EndDate = ParseDate(row, 6);
        }

        /// <summary>
        /// Gets or sets the line number.
        /// </summary>
        /// <value>
        /// The line number.
        /// </value>
        public long LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the demand code.
        /// </summary>
        /// <value>
        /// The demand code.
        /// </value>
        public string DemandCode { get; set; }

        /// <summary>
        /// Gets or sets the type of the demand.
        /// </summary>
        /// <value>
        /// The type of the demand.
        /// </value>
        public string DemandType { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Determines whether this instance is complete.
        /// </summary>
        /// <returns></returns>
        public bool IsComplete()
        {
            return !string.IsNullOrWhiteSpace(CaseNumber)
                && !string.IsNullOrWhiteSpace(EmployeeNumber)
                && !string.IsNullOrWhiteSpace(DemandCode)
                && !string.IsNullOrWhiteSpace(DemandType)
                && (!string.IsNullOrWhiteSpace(Value) || DemandCode == "OTHER");
        }

        /// <summary>
        /// Normalizes the specified index.
        /// </summary>
        /// <param name="parts">The parts.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected string Normalize(string[] parts, int index)
        {
            if (index >= parts.Length)
                return null;
            string data = parts[index];
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }

        /// <summary>
        /// Parses the date.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual DateTime? ParseDate(string[] parts, int index)
        {
            string data = Normalize(parts, index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            DateTime dt;
            if (DateTime.TryParseExact(data, new string[] { "yyyy-MM-dd", "MM/dd/yyyy", "M/d/yyyy", "MM/dd/yy", "M/d/yy" }, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out dt))
                return dt.Kind == DateTimeKind.Local ? dt.ToUniversalTime() : dt;

            return null;
        }
    }
}
