﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Builders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AT2890.WorkRestrictionsImport
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            //Job Code,Office Locations,Demand Code,Demand Type,Value,Description,Other Office Locations...+
            // Initial state/default = Amazon
            string CustomerId = "546e5097a32aa00d60e3210a";
            string EmployerId = "546e52cda32aa00f78086269";
            string fileName = null;
            bool header = true;

            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                    fileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-c")
                    CustomerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-e")
                    EmployerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-h")
                    header = args.Length > (i + 1) ? args[i + 1] == "yes" : false;
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    AT2890.WorkRestrictionsImport.exe [-f <file path>] [-c <customer id>] [-e <employer id>]");
                    Console.WriteLine();
                    Console.WriteLine("    -f <file path>: Specifies the file path to be converted");
                    Console.WriteLine("    -h <yes|no>: Specifies whether or not there is a header row in the file, default = yes");
                    Console.WriteLine("    -c <cusotmer id>: Specifies a customer Id OTHER than parrot's");
                    Console.WriteLine("    -e <employer id>: Specifies an employer Id OTHER than parrot's");
                    Console.WriteLine();
                    Environment.Exit(0);
                    return;
                }
            }
            if (!string.IsNullOrWhiteSpace(fileName) && !File.Exists(fileName))
            {
                log.ErrorFormat("-f \"{0}\" points to a file that does not exist or you do not have permission to view", fileName);
                Environment.Exit(9);
                return;
            }

            Stopwatch stopper = new Stopwatch();
            stopper.Start();

            List<Demand> demands = Demand.AsQueryable().Where(d => d.CustomerId == CustomerId && (d.EmployerId == null || d.EmployerId == EmployerId)).ToList();
            List<DemandType> types = DemandType.AsQueryable().Where(d => d.CustomerId == CustomerId && (d.EmployerId == null || d.EmployerId == EmployerId)).ToList();
            List<string> oldRestrictionNames = Restriction.AsQueryable().Where(r => r.CustomerId == CustomerId && r.EmployerId == EmployerId).Select(r => r.Name).ToList();

            List<WorkRestrictionMap> mapping = new List<WorkRestrictionMap>();

            using (TextReader reader = new StreamReader(fileName))
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
            {
                long lineNumber = 0L;
                try
                {
                    parser.HasFieldsEnclosedInQuotes = true;
                    parser.SetDelimiters(",", "|", "\t");
                    parser.TrimWhiteSpace = true;
                    while (!parser.EndOfData)
                    {
                        try
                        {
                            lineNumber = parser.LineNumber;
                            if (header && lineNumber == 1L) // ignore header row
                            {
                                parser.ReadLine();
                                continue;
                            }
                            var row = parser.ReadFields();
                            WorkRestrictionMap map = new WorkRestrictionMap(row) { LineNumber = lineNumber };
                            if (!map.IsComplete())
                                log.WarnFormat("Row # {0} is Incomplete/Not Valid: {1}", lineNumber, string.Join(",", row));
                            else
                                mapping.Add(map);
                        }
                        catch (Microsoft.VisualBasic.FileIO.MalformedLineException lineEx)
                        {
                            log.Error(string.Format("Error on line # {0}, bad juju in row source: {1}", lineEx.LineNumber, lineEx.Source), lineEx);
                        }
                        catch (Exception ex)
                        {
                            log.Error(string.Format("Error processing line # {0}", lineNumber), ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Fatal("Fatal error processing file", ex);
                }
            }

            //
            // Map the things now.... Duh
            foreach (var caseMap in mapping.GroupBy(m => m.CaseNumber))
            {
                Case theCase = Case.AsQueryable().Where(c => c.CustomerId == CustomerId && c.EmployerId == EmployerId && c.CaseNumber == caseMap.Key).FirstOrDefault();
                if (theCase == null)
                {
                    log.WarnFormat("Case # {0} not found; these {1} line(s) will NOT be mapped; Line #'s: {2}.", caseMap.Key, caseMap.Count(), string.Join(", ", caseMap.Select(c => c.LineNumber)));
                    continue;
                }

                List<EmployeeRestriction> theRestrictions = EmployeeRestriction.AsQueryable().Where(r => 
                    r.CustomerId == theCase.CustomerId 
                    && r.EmployerId == theCase.EmployerId 
                    && r.EmployeeId == theCase.Employee.Id 
                    && r.CaseId == theCase.Id)
                    .ToList();

                List<OldRestriction> oldAndBusted = CaseNote.Repository.Collection.FindAs<BsonDocument>(
                    Query.And(
                        CaseNote.Query.EQ(c => c.CaseId, theCase.Id),
                        Query.Exists("WorkRestriction"),
                        Query.NotIn("WorkRestriction.Restriction", oldRestrictionNames.Select(r => new BsonString(r)))
                    )
                ).SetFields(Fields.Include("WorkRestriction"))
                    .ToList()
                    .Select(d => BsonSerializer.Deserialize<OldRestriction>(d["WorkRestriction"].AsBsonDocument))
                    .ToList();

                List<EmployeeRestriction> bbq = new List<EmployeeRestriction>();
                int otherCount = 0;
                
                foreach (var map in caseMap)
                {
                    if (map.DemandCode == "OTHER")
                    {
                        //var note = theNotes.Where(n => n.StartDate == map.StartDate.ToMidnight() && n.EndDate == map.EndDate.ToMidnight()).FirstOrDefault();
                        var note = oldAndBusted.Skip(otherCount).Take(1).FirstOrDefault();
                        if (note != null)
                        {
                            map.Value = map.Value ?? string.Format("{0}, {1}", note.Restriction, note.Limitation);
                            map.StartDate = note.StartDate ?? map.StartDate;
                            map.EndDate = note.EndDate ?? map.EndDate;
                            otherCount++;
                        }
                    }
                    if (string.IsNullOrWhiteSpace(map.Value))
                        continue;

                    Demand dem = demands.FirstOrDefault(d => string.Equals(d.Code, map.DemandCode, StringComparison.InvariantCultureIgnoreCase));
                    if (dem == null)
                    {
                        log.WarnFormat("Line # {1}, Case # {2}: Demand code, \"{0}\", not found; this line will NOT be mapped.", map.DemandCode, map.LineNumber, theCase.CaseNumber);
                        continue;
                    }

                    EmployeeRestriction restriction = theRestrictions.Where(r => r.Restriction.DemandId == dem.Id).FirstOrDefault();
                    bool isNew = restriction == null;
                    if (isNew)
                    {
                        restriction = theRestrictions.AddFluid(new EmployeeRestriction()
                        {
                            Id = ObjectId.GenerateNewId().ToString(),
                            EmployeeId = theCase.Employee.Id,
                            EmployeeNumber = theCase.Employee.EmployeeNumber,
                            CaseId = theCase.Id,
                            CaseNumber = theCase.CaseNumber,
                            CustomerId = theCase.CustomerId,
                            EmployerId = theCase.EmployerId,
                            Restriction = new WorkRestriction()
                            {
                                Id = new Guid(),
                                DemandId = dem.Id,
                                Demand = dem,
                                Dates = new DateRange(map.StartDate ?? theCase.StartDate, map.EndDate),
                                Description = dem.Description,
                                Values = new List<AppliedDemandValue<WorkRestrictionValue>>()
                            },
                            CreatedById = User.DefaultUserId
                        }.SetCreatedDate(DateTime.UtcNow));
                    }
                    restriction.ModifiedById = User.DefaultUserId;
                    restriction.SetModifiedDate(DateTime.UtcNow);

                    DemandType dt = types.FirstOrDefault(t => t.Name.StartsWith(map.DemandType, StringComparison.InvariantCultureIgnoreCase));
                    if (dt == null)
                    {
                        log.WarnFormat("Line #: {1}, Case # {2}: Demand type, \"{0}\", not found; this line will NOT be mapped.", map.DemandType, map.LineNumber, theCase.CaseNumber);
                        continue;
                    }
                    if (!dem.Types.Contains(dt.Id))
                    {
                        log.WarnFormat("Line #: {1}, Case # {2}: Demand type \"{0}\" is not supported by demand \"{3}\"; this line will NOT be mapped.", map.DemandType, map.LineNumber, theCase.CaseNumber, dem.Name);
                        continue;
                    }
                    restriction.Restriction.Values = restriction.Restriction.Values ?? new List<AppliedDemandValue<WorkRestrictionValue>>();
                    var val = restriction.Restriction.Values.FirstOrDefault(v => v.DemandTypeId == dt.Id)
                        ?? restriction.Restriction.Values.AddFluid(new WorkRestrictionValue() { DemandTypeId = dt.Id, DemandType = dt, Type = dt.Type });
                    switch (val.Type)
                    {
                        case DemandValueType.Text:
                            val.Value = map.Value;
                            val.Applicable = true;
                            break;
                        case DemandValueType.Boolean:
                            val.Value = map.Value == "True"
                                || map.Value == "true"
                                || map.Value == "t"
                                || map.Value == "T"
                                || map.Value == "y"
                                || map.Value == "Y"
                                || map.Value == "Yes"
                                || map.Value == "yes"
                                || map.Value == "1";
                            val.Applicable = true;
                            break;
                        case DemandValueType.Value:
                            int iv;
                            if (dt.Items == null || !dt.Items.Any())
                            {
                                log.WarnFormat("Line # {0}, Case # {3}: Demand type of \"{1}\" does not have a valid value matching \"{2}\".", map.LineNumber, map.DemandType, map.Value, theCase.CaseNumber);
                                continue;
                            }
                            if (int.TryParse(map.Value, out iv))
                            {
                                var item = dt.Items.FirstOrDefault(i => i.Value == iv);
                                if (item == null)
                                {
                                    log.WarnFormat("Line # {0}, Case # {3}: Demand type of \"{1}\" does not have a valid value matching \"{2}\".", map.LineNumber, map.DemandType, map.Value, theCase.CaseNumber);
                                    continue;
                                }
                                val.DemandItem = item;
                                val.Applicable = true;
                            }
                            else
                            {
                                log.WarnFormat("Line # {0}, Case # {3}: Demand type of \"{1}\" expects a valid number value; \"{2}\" is not a valid number.", map.LineNumber, map.DemandType, map.Value, theCase.CaseNumber);
                                continue;
                            }
                            break;
                        case DemandValueType.Schedule:
                            log.WarnFormat("Line # {0}, Case # {1}: Can NOT support the demand type of 'Schedule', this line will NOT be mapped.", map.LineNumber, theCase.CaseNumber);
                            continue;
                    }
                    bbq.RemoveAll(r => r.Id == restriction.Id);
                    bbq.Add(restriction);
                }

                if (bbq.Any())
                {
                    var chunkyDeepFriedBucketOfRestrictionWings = EmployeeRestriction.Repository.Collection.InitializeOrderedBulkOperation();
                    foreach (var buffalo in bbq)
                        chunkyDeepFriedBucketOfRestrictionWings.Find(EmployeeRestriction.Query.EQ(r => r.Id, buffalo.Id)).Upsert().ReplaceOne(buffalo);
                    chunkyDeepFriedBucketOfRestrictionWings.Execute();
                    log.InfoFormat("Case # {0}: {1} Work Restrictions created/replaced successfully.", theCase.CaseNumber, bbq.Count);
                    continue;
                }

                log.InfoFormat("Case # {0}: No Work Restrictions to modify or update at this time.", theCase.CaseNumber);
            }

            stopper.Stop();
            log.InfoFormat("All done, took {0}. Bye bye now.", stopper.Elapsed.ToReallyFriendlyTime());
        }
    }
}
