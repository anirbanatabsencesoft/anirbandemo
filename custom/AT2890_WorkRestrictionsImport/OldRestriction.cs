﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT2890.WorkRestrictionsImport
{
    [Serializable]
    public class OldRestriction : BaseNonEntity
    {
        public string Restriction { get; set; }

        public string Limitation { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}