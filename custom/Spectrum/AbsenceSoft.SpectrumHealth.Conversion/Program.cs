﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AbsenceSoft.SpectrumHealth.Conversion
{
    static class Program
    {
        /// <summary>
        /// Unhandled exception handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error(e.ExceptionObject as Exception);
            Environment.Exit(9);
        }

        /// <summary>
        /// Main entry method
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
            Common.Serializers.DateTimeSerializer.Register();

            // Ensure we allow overlapping cases
            Logic.Cases.CaseService.SetIgnoreOverlappingCases(true);
            
            string customerId = null, employerId = null, fileName = null, output = null;
            bool notes = false;
            int? parallel = null;

            #region Parse Arguments

            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                {
                    fileName = GetSwitchValue(args, i);
                    if (!File.Exists(fileName))
                    {
                        Console.Error.WriteLine(string.Format("The file '{0}' was not found.", fileName));
                        Environment.Exit(9052);
                        return;
                    }
                }
                else if (a == "-c")
                    customerId = GetSwitchValue(args, i);
                else if (a == "-e")
                    employerId = GetSwitchValue(args, i);
                else if (a == "-r")
                    output = GetSwitchValue(args, i);
                else if (a == "-n")
                    notes = true;
                else if (a == "-p" && int.TryParse(GetSwitchValue(args, i), out int p))
                    parallel = p;
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    AbsenceSoft.SpectrumHealth.Conversion.exe -c <customerId> -e <employerId> -f <file path> [-r <output path>]");
                    Console.WriteLine();
                    Console.WriteLine("    -c <customerId>: Specifies the customerId");
                    Console.WriteLine("    -e <employerId>: Specifies the employerId");
                    Console.WriteLine("    -f <file path>: Specifies the file path for the combined conversion file");
                    Console.WriteLine("    -r <output path>: The report output file name (defaults to the console)");
                    Console.WriteLine("    -n: Specifies that the file is a notes only file");
                    Console.WriteLine();
                    return;
                }
            }

            #endregion

            Logger.Start(output);
            Logger.Info("Process started: {0}", string.IsNullOrWhiteSpace(fileName) ? Environment.CommandLine : Path.GetFileName(fileName));

            // Validation routine
            bool badJuju = false;
            void curse(string e)
            {
                Console.Error.WriteLine(e);
                badJuju = true;
            }

            if (string.IsNullOrWhiteSpace(customerId))
                curse("Error 8002: -c with a valid customer id was not provided");
            if (string.IsNullOrWhiteSpace(employerId))
                curse("Error 8002: -e with a valid employer id was not provided");
            if (string.IsNullOrWhiteSpace(fileName))
                curse("Error 8002: -f with a valid file path/name was not provided");
            if (badJuju)
                Environment.Exit(8002);

            // Get the customer, employer and custom fields, policies and absence reasons
            Logger.Info("Reading customer");
            var customer = Customer.GetById(customerId);
            Logger.Info("Reading employer");
            var employer = Employer.GetById(employerId);
            Logger.Info("Reading custom fields");
            var customFields = notes ? null : CustomField.AsQueryable().Where(f => f.CustomerId == customerId).ToList();
            Logger.Info("Reading employee class types");
            var employeeClassTypes = EmployeeClass.AsQueryable().Where(f => f.CustomerId == customerId).ToList();
            Logger.Info("Reading policies");
            var policies = notes ? null : Policy.DistinctFind(Query.Null, customerId, employerId, true).ToList();
            Logger.Info("Reading absence reasons");
            var reasons = notes ? null : AbsenceReason.DistinctFind(Query.Null, customerId, employerId, true).ToList();
            Logger.Info("Reading list of employees (all of 'em)");
            var employees = notes ? null : Employee.Repository.Collection.Find(
                Employee.Query.And(Employee.Query.EQ(e => e.CustomerId, customerId), Employee.Query.EQ(e => e.EmployerId, employerId)))
                // see: http://stackoverflow.com/questions/14053803/mongodb-c-sharp-driver-cursor-not-found
                .SetFlags(QueryFlags.NoCursorTimeout).ToList();

            Logger.Info("Reading contact types");
            var contactTypes = notes ? null : ContactType.DistinctFind(Query.Null, customerId, employerId, true).ToDictionary(r => r.Code, r => r.Name);
            
            if (customer == null)
            {
                curse("Error 8003: -c with a valid customer id was not provided OR customer was not found");
            }

            if (employer == null)
            {
                curse("Error 8003: -e with a valid employer id was not provided OR employer was not found");
            }

            if (badJuju)
            {
                Environment.Exit(8003);
            }

            // Start doing some nifty multi-threaded I/O and processing, let's use all of these
            //  machine's cores and HT capabilities.
            bool doneReadingFile = false;
            ConcurrentQueue<FileRow> rowQueue = new ConcurrentQueue<FileRow>();
            void readFile()
            {
                Logger.Info("Reading file '{0}'", Path.GetFileName(fileName));
                File.OpenRead(fileName).Using(fs => ReadDelimitedFile(fs, rowQueue.Enqueue));
                Logger.Info("Done reading file '{0}'", Path.GetFileName(fileName));
                doneReadingFile = true;
            }

            bool doneParsingRows = false;
            ConcurrentQueue<CaseRow> processQueue = new ConcurrentQueue<CaseRow>();
            void parseRows()
            {
                Logger.Info("Parsing file rows for '{0}'", Path.GetFileName(fileName));
                while (!doneReadingFile)
                    while (rowQueue.TryDequeue(out FileRow row))
                        processQueue.Enqueue(new CaseRow(row.Parts, customFields, contactTypes, employeeClassTypes) { LineNumber = row.RowNumber, Delimiter = '\t' });
                Logger.Info("Done parsing file rows for '{0}'", Path.GetFileName(fileName));
                doneParsingRows = true;
            }

            if (!notes)
            {
                ConcurrentDictionary<string, EmployeeContainer> employeeMap = new ConcurrentDictionary<string, EmployeeContainer>();
                void aggregateEmployees()
                {
                    Logger.Info("Mapping employees and grouping parsed rows for '{0}'", Path.GetFileName(fileName));
                    while (!doneParsingRows)
                    {
                        while (processQueue.TryDequeue(out CaseRow row))
                        {
                            if (row.IsError)
                            {
                                foreach (var err in row.Errors)
                                    Logger.Error("Case #'{0}', Line {1}: {2}", row.CaseNumber, row.LineNumber, err);
                                continue;
                            }
                            employeeMap.AddOrUpdate(row.EmployeeNumber, new EmployeeContainer()
                            {
                                EmployeeNumber = row.EmployeeNumber,
                                Rows = new ConcurrentBag<CaseRow>(new[] { row }),
                                Errors = new ConcurrentBag<string>(),
                                Employee = employees.FirstOrDefault(e => e.EmployeeNumber == row.EmployeeNumber)
                            },
                            (key, container) =>
                            {
                                container.Rows.Add(row);
                                return container;
                            });
                        }
                    }
                    Logger.Info("Done mapping employees and grouping parsed rows for '{0}'", Path.GetFileName(fileName));
                }

                // Actually execute our processes in parallel
                Parallel.Invoke(readFile, parseRows, aggregateEmployees);
                rowQueue = null;
                processQueue = null;

                // Get to the meat of the thing and start processing each row

                Parallel.ForEach(employeeMap, new ParallelOptions()
                {
                    MaxDegreeOfParallelism = parallel ?? (Environment.GetCommandLineArgs().Contains("debug") ? 1 : -1)
                },
                kvp =>
                {
                // Now we "Do Everything" quite literally, per employee.
                using (CaseBuilder builder = new CaseBuilder
                    (
                        kvp.Value,
                        customer,
                        employer,
                        policies,
                        reasons,
                        customFields
                    ))
                    {
                        builder.DoEverything();
                    }
                });
            }
            else
            {
                bool startedImportingNotes = false;
                // NOTES file!
                void importNotes()
                {
                    var cases = !notes ? null : Case.AsQueryable()
                       .Where(c => c.CustomerId == customerId)
                       .Select(c => new { c.Id, c.CaseNumber })
                       .ToDictionary(a => a.CaseNumber, a => a.Id);

                    if (!cases.Any())
                    {
                        Logger.Error("No cases exist for this customer and therefore notes cannot be imported");
                        return;
                    }

                    Logger.Info("Importing note rows for '{0}'", Path.GetFileName(fileName));
                    var bulkNotes = CaseNote.Repository.Collection.InitializeUnorderedBulkOperation();
                    var bulkNoteCount = 0;
                    while (!doneParsingRows || !startedImportingNotes)
                    {
                        startedImportingNotes = true;
                        while (processQueue.TryDequeue(out CaseRow row))
                        {
                            if (row.IsError)
                            {
                                foreach (var err in row.Errors)
                                    Logger.Error("Case #'{0}', Line {1}: {2}", row.CaseNumber, row.LineNumber, err);
                                continue;
                            }
                            if (!cases.TryGetValue(row.CaseNumber, out string caseId))
                            {
                                Logger.Error("Case #'{0}', Line {1}: Case not found", row.CaseNumber, row.LineNumber);
                                continue;
                            }
                            bulkNotes.Insert(new CaseNote()
                            {
                                CreatedById = User.DefaultUserId,
                                ModifiedById = User.DefaultUserId,
                                CaseId = caseId,
#pragma warning disable CS0618 // Type or member is obsolete
                                // LEGACY USAGE
                                Category = (Data.Enums.NoteCategoryEnum?)row.CaseNoteCategory,
#pragma warning restore CS0618 // Type or member is obsolete
                                CustomerId = customerId,
                                EmployerId = employerId,
                                EnteredByEmployeeNumber = row.NoteEnteredByEmployeeNumber,
                                EnteredByName = row.NoteEnteredByName,
                                Notes = row.NoteText,
                                Public = false
                            }.SetCreatedDate(row.NoteDate ?? DateTime.UtcNow)
                            .SetModifiedDate(DateTime.UtcNow));
                            bulkNoteCount++;

                            if (bulkNoteCount >= 100)
                            {
                                Logger.Info("Running bulk note import batch of {0} notes", bulkNoteCount);
                                bulkNotes.Execute();
                                Logger.Info("Done running bulk note import batch of {0} notes", bulkNoteCount);
                                bulkNotes = CaseNote.Repository.Collection.InitializeUnorderedBulkOperation();
                                bulkNoteCount = 0;
                            }
                        }
                    }

                    if (bulkNoteCount > 0)
                    {
                        Logger.Info("Running bulk note import batch of {0} notes", bulkNoteCount);
                        bulkNotes.Execute();
                        Logger.Info("Done running bulk note import batch of {0} notes", bulkNoteCount);
                        bulkNoteCount = 0;
                        bulkNotes = null;
                    }
                    cases = null;
                    Logger.Info("Done importinng note rows for '{0}'", Path.GetFileName(fileName));
                }
                // Actually execute our processes in parallel
                Parallel.Invoke(readFile, parseRows, importNotes);
                rowQueue = null;
                processQueue = null;
            }

            // let's wait for this to finish up.
            Logger.Finish();
        }

        /// <summary>
        /// Reads the delimited file and calls the handleRow callback for each row's parsed set of values, if any.
        /// </summary>
        /// <param name="fileStream">The file stream to read from.</param>
        /// <param name="handleRow">An action that takes a row number/line #, the row parts, the delimiter and original line text which is invoked for
        /// each line in the file.</param>
        private static void ReadDelimitedFile(Stream fileStream, Action<FileRow> handleRow)
        {
            long lineNumber = 0;
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(fileStream))
            {
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters("\t", "|");
                while (!parser.EndOfData)
                {
                    lineNumber++;
                    try
                    {
                        var _parts = parser.ReadFields();
                        handleRow(new FileRow(lineNumber, _parts));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Line # {0} - {1}", lineNumber, ex.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Gets the command line argument switch value.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        static string GetSwitchValue(string[] args, int index)
        {
            return args.Length > index + 1 ? args[index + 1] : null;
        }
    }
}
