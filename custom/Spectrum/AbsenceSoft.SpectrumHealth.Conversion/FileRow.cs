﻿namespace AbsenceSoft.SpectrumHealth.Conversion
{
    /// <summary>
    /// Represents a file row while parsing a file.
    /// </summary>
    public class FileRow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileRow"/> class.
        /// </summary>
        /// <param name="rowNumber">The row number.</param>
        /// <param name="parts">The parts.</param>
        public FileRow(long rowNumber, string[] parts)
        {
            RowNumber = rowNumber;
            Parts = parts;
        }

        /// <summary>
        /// Gets or sets the parts.
        /// </summary>
        /// <value>
        /// The parts.
        /// </value>
        public string[] Parts { get; set; }

        /// <summary>
        /// Gets or sets the row number.
        /// </summary>
        /// <value>
        /// The row number.
        /// </value>
        public long RowNumber { get; set; }
    }
}
