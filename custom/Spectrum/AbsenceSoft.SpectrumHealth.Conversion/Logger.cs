﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.SpectrumHealth.Conversion
{
    internal static class Logger
    {
        /// <summary>
        /// The log entries queue
        /// </summary>
        private static ConcurrentQueue<string> LogEntries = new ConcurrentQueue<string>();

        /// <summary>
        /// The exceptions queue
        /// </summary>
        private static ConcurrentQueue<string> Exceptions = new ConcurrentQueue<string>();

        /// <summary>
        /// The is stopped flag
        /// </summary>
        private static volatile bool IsStopped = true;

        /// <summary>
        /// The logging thread
        /// </summary>
        private static Thread loggingThread = null;

        /// <summary>
        /// The how long does this take stopwatch
        /// </summary>
        private static Stopwatch howLongDoesThisTake = new Stopwatch();

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="arg">The argument.</param>
        public static void Error(string error, params object[] arg)
        {
            string formatted = string.Format("{0:MM-dd-yyyy HH:mm:ss} ERROR: {1}", DateTime.UtcNow, string.Format(error, arg));
            Console.Error.WriteLine(formatted);
            LogEntries.Enqueue(formatted);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="caseNumber">The case number.</param>
        public static void Error(Exception ex, string caseNumber = null, string message = null)
        {
            string msg = string.Format("{1}: {0}", ex.Message, message ?? "Failed with the following exception");
            string formatted = string.Format("{0:MM-dd-yyyy HH:mm:ss} ERROR: {1}", DateTime.UtcNow, 
                string.IsNullOrWhiteSpace(caseNumber) ? msg : string.Format("Case #'{0}': {2}: {1}", caseNumber, ex.Message, msg));
            Console.Error.WriteLine(formatted);
            LogEntries.Enqueue(formatted);
            Exceptions.Enqueue(string.Concat(formatted, Environment.NewLine, ex.ToString()));
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="warning">The warning.</param>
        /// <param name="arg">The argument.</param>
        public static void Warn(string warning, params object[] arg)
        {
            string formatted = string.Format("{0:MM-dd-yyyy HH:mm:ss} WARN: {1}", DateTime.UtcNow, string.Format(warning, arg));
            Console.WriteLine(formatted);
            LogEntries.Enqueue(formatted);
        }

        /// <summary>
        /// Logs the warning.
        /// </summary>
        /// <param name="caseNumber">The case number.</param>
        /// <param name="warning">The warning.</param>
        /// <param name="arg">The argument.</param>
        public static void Warn(string caseNumber, string warning, params object[] arg)
        {
            string formatted = string.Format("{0:MM-dd-yyyy HH:mm:ss} WARN: Case #'{1}': {2}", DateTime.UtcNow, caseNumber, string.Format(warning, arg));
            Console.WriteLine(formatted);
            LogEntries.Enqueue(formatted);
        }

        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="arg">The argument.</param>
        public static void Info(string info, params object[] arg)
        {
            string formatted = string.Format("{0:MM-dd-yyyy HH:mm:ss} INFO: {1}", DateTime.UtcNow, string.Format(info, arg));
            Console.WriteLine(formatted);
            LogEntries.Enqueue(formatted);
        }

        /// <summary>
        /// Starts the specified output path.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        public static void Start(string outputPath = null)
        {
            howLongDoesThisTake.Start();
            IsStopped = false;
            if (!string.IsNullOrWhiteSpace(outputPath))
            {
                loggingThread = new Thread(DoLogging) { IsBackground = true, Name = "LoggingThread" };
                loggingThread.Start(outputPath);
            }
        }

        /// <summary>
        /// Sets the output.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        public static void SetOutput(string outputPath)
        {
            // Now it doesn't matter, if the path is blank or null we'll just keep the queue empty
            loggingThread = new Thread(DoLogging) { IsBackground = true, Name = "LoggingThread" };
            loggingThread.Start(outputPath);
        }

        /// <summary>
        /// Finishes this instance.
        /// </summary>
        public static void Finish()
        {
            IsStopped = true;
            howLongDoesThisTake.Stop();
            Info("Process completed in {0}", howLongDoesThisTake.Elapsed);
            loggingThread?.Join(5000);
        }

        private static bool IsApplicationFinished()
        {
            return IsStopped;
        }

        /// <summary>
        /// Does the logging.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        private static void DoLogging(object outputPath)
        {
            var logFile = outputPath?.ToString();
            if (string.IsNullOrWhiteSpace(logFile) || IsApplicationFinished())
            {
                while (!IsApplicationFinished())
                {
                    //Exceptions
                    Parallel.Invoke(
                        () => { while (LogEntries.TryDequeue(out string result)) { /* Do Nothing */ } },
                        () => { while (Exceptions.TryDequeue(out string result)) { /* Do Nothing */ } }
                    );
                }
                return;
            }
            
            using (var writer = new StreamWriter(new FileStream(logFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)))
            using (var exWriter = new StreamWriter(new FileStream(string.Concat(logFile, ".exceptions.txt"), FileMode.Append, FileAccess.Write, FileShare.ReadWrite)))
            {
                while (!IsApplicationFinished())
                {
                    Parallel.Invoke(
                        () => { while (LogEntries.TryDequeue(out string result)) { writer.WriteLine(result); } },
                        () => { while (Exceptions.TryDequeue(out string result)) { exWriter.WriteLine(result); } }
                    );
                }

                if (!LogEntries.IsEmpty || !Exceptions.IsEmpty)
                {
                    Parallel.Invoke(
                        () => { while (LogEntries.TryDequeue(out string result)) { writer.WriteLine(result); } },
                        () => { while (Exceptions.TryDequeue(out string result)) { exWriter.WriteLine(result); } }
                    );
                }
            }
        }
    }
}
