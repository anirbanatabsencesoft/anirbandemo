﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AbsenceSoft.SpectrumHealth.Conversion
{
    public class CaseRow
    {
        #region Const

        private const int RecordTypeOrdinal = 0;
        private const int EmployeeNumberOrdinal = 1;
        private const int EmployeeLastNameOrdinal = 2;
        private const int EmployeeFirstNameOrdinal = 3;
        private const int E_EmployeeMiddleNameOrdinal = 4;

        private const int CaseNumberOrdinal = 4;

        // Eligibility Row Ordinals
        private const int LeaveIntervalOrdinal = 5;
        private const int ReasonForLeaveOrdinal = 6;
        private const int FamilyRelationshipOrdinal = 7;
        private const int IntermittentFrequencyOrdinal = 8;
        private const int IntermittentFrequencyUnitOrdinal = 9;
        private const int IntermittentOccurrencesOrdinal = 10;
        private const int IntermittentDurationOrdinal = 11;
        private const int IntermittentDurationUnitOrdinal = 12;
        private const int DateCaseOpenedOrdinal = 13;
        private const int ExpectedStartDateOrdinal = 14;
        private const int ExpectedRTWDateOrdinal = 15;
        private const int ConfirmedRTWDateOrdinal = 16;
        private const int CaseClosedDateOrdinal = 17;
        private const int CaseStatusOrdinal = 18;
        private const int DeliveryDateOrdinal = 19;
        private const int BondingStartDateOrdinal = 20;
        private const int BondingEndDateOrdinal = 21;
        private const int IllnessOrInjuryDateOrdinal = 22;
        private const int HospitalAdmissionDateOrdinal = 23;
        private const int HospitalReleaseDateOrdinal = 24;
        private const int RTWReleaseDateOrdinal = 25;
        private const int AutoPoliciesOrdinal = 26;
        private const int ReducedScheduleOrdinal = 27;
        private const int CaseCreateWorkflowOrdinal = 28;

        // Adjudication Detail
        private const int AdjudicationStartDateOrdinal = 5;
        private const int AdjudicationEndDateOrdinal = 6;
        private const int AdjudicationStatusOrdinal = 7;
        private const int MinutesUsedOrdinal = 8;

        // Intermittent Time Used
        private const int DateOfAbsenceOrdinal = 5;
        private const int MinutesApprovedOrdinal = 6;
        private const int MinutesDeniedOrdinal = 7;
        private const int MinutesPendingOrdinal = 8;

        // Adjudication + Intermittent Shared Ordinals
        private const int PolicyCodeOrdinal = 9;
        private const int WorkflowOrdinal = 10;

        // case notes
        private const int NOTE_NoteDate = 5;
        private const int NOTE_Category = 6;
        private const int NOTE_Text = 7;
        private const int NOTE_EnteredByName = 8;

        // work related
        private const int WC_Reportable = 5;
        private const int WC_HealthcarePrivate = 6;
        private const int WC_WhereOccurred = 7;
        private const int WC_CaseClassification = 8;
        private const int WC_TypeOfInjury = 9;
        private const int WC_DaysAwayfromWork = 10;
        private const int WC_DaysontheJobTransferorRestriction = 11;
        private const int WC_HealthcareProviderTypeCode = 12;
        private const int WC_HealthcareProviderFirstName = 13;
        private const int WC_HealthcareProviderLastName = 14;
        private const int WC_HealthcareProviderCompany = 15;
        private const int WC_HealthcareProviderAddress1 = 16;
        private const int WC_HealthcareProviderAddress2 = 17;
        private const int WC_HealthcareProviderCity = 18;
        private const int WC_HealthcareProviderState = 19;
        private const int WC_HealthcareProviderCountry = 20;
        private const int WC_HealthcareProviderPostalCode = 21;
        private const int WC_EmergencyRoom = 22;
        private const int WC_HospitalizedOvernight = 23;
        private const int WC_TimeEmployeeBeganWork = 24;
        private const int WC_TimeOfEvent = 25;
        private const int WC_ActivityBeforeIncident = 26;
        private const int WC_WhatHappened = 27;
        private const int WC_InjuryOrIllness = 28;
        private const int WC_WhatHarmedTheEmployee = 29;
        private const int WC_DateOfDeath = 30;
        private const int WC_Sharps = 31;
        private const int WC_SharpsInjuryLocation = 32;
        private const int WC_TypeOfSharp = 33;
        private const int WC_BrandOfSharp = 34;
        private const int WC_ModelOfSharp = 35;
        private const int WC_BodyFluidInvolved = 36;
        private const int WC_HaveEngineeredInjuryProtection = 37;
        private const int WC_WasProtectiveMechanismActivated = 38;
        private const int WC_WhenDidtheInjuryOccur = 39;
        private const int WC_JobClassification = 40;
        private const int WC_JobClassificationOther = 41;
        private const int WC_LocationandDepartment = 42;
        private const int WC_LocationandDepartmentOther = 43;
        private const int WC_SharpsProcedure = 44;
        private const int WC_SharpsProcedureOther = 45;
        private const int WC_ExposureDetail = 46;
        private const int WC_InjuryLocation = 47;
        private const int WC_PartOfBody = 48;
        

        // Custom_Fields
        private const int CF_CustomFieldCodeOrdinal = 2;
        private const int CF_CustomFieldValueOrdinal = 3;
        private const int CF_EmployerReferenceCodeOrdinal = 4;
        private const int CF_EntityTargetOrdinal = 5;
        private const int CF_CaseNumberOrdinal = 6;
        public CustomField[] CustomFields { get; set; }

        // Employee
        private const int E_JobTitleOrdinal = 5;
        private const int E_JobLocationOrdinal = 6;
        private const int E_WorkStateOrdinal = 7;
        private const int E_WorkCountryOrdinal = 8;
        private const int E_PhoneHomeOrdinal = 9;
        private const int E_PhoneWorkOrdinal = 10;
        private const int E_PhoneMobileOrdinal = 11;
        private const int E_PhoneAltOrdinal = 12;
        private const int E_EmailOrdinal = 13;
        private const int E_EmailAltOrdinal = 14;
        private const int E_AddressOrdinal = 15;
        private const int E_Address2Ordinal = 16;
        private const int E_CityOrdinal = 17;
        private const int E_StateOrdinal = 18;
        private const int E_PostalCodeOrdinal = 19;
        private const int E_CountryOrdinal = 20;
        private const int E_EmploymentTypeOrdinal = 21;
        private const int E_ManagerEmployeeNumberOrdinal = 22;
        private const int E_ManagerLastNameOrdinal = 23;
        private const int E_ManagerFirstNameOrdinal = 24;
        private const int E_ManagerPhoneOrdinal = 25;
        private const int E_ManagerEmailOrdinal = 26;
        private const int E_HREmployeeNumberOrdinal = 27;
        private const int E_HRLastNameOrdinal = 28;
        private const int E_HRFirstNameOrdinal = 29;
        private const int E_HRPhoneOrdinal = 30;
        private const int E_HREmailOrdinal = 31;
        private const int E_SpouseEmployeeNumberOrdinal = 32;
        private const int E_DateOfBirthOrdinal = 33;
        private const int E_GenderOrdinal = 34;
        private const int E_ExemptionStatusOrdinal = 35;
        private const int E_Meets50In75Ordinal = 36;
        private const int E_KeyEmployeeOrdinal = 37;
        private const int E_MilitaryStatusOrdinal = 38;
        private const int E_EmploymentStatusOrdinal = 39;
        private const int E_TerminationDateOrdinal = 40;
        private const int E_PayRateOrdinal = 41;
        private const int E_PayTypeOrdinal = 42;
        private const int E_HireDateOrdinal = 43;
        private const int E_RehireDateOrdinal = 44;
        private const int E_AdjustedServiceDateOrdinal = 45;
        private const int E_MinutesPerWeekOrdinal = 46;
        private const int E_HoursWorkedIn12MonthsOrdinal = 47;
        private const int E_WorkTimeSunOrdinal = 48;
        private const int E_WorkTimeMonOrdinal = 49;
        private const int E_WorkTimeTueOrdinal = 50;
        private const int E_WorkTimeWedOrdinal = 51;
        private const int E_WorkTimeThuOrdinal = 52;
        private const int E_WorkTimeFriOrdinal = 53;
        private const int E_WorkTimeSatOrdinal = 54;
        private const int E_VariableScheduleOrdinal = 55;
        private const int E_JobDescriptionOrdinal = 56;
        private const int E_JobClassificationOrdinal = 57;
        private const int E_USCensusJobCategoryCodeOrdinal = 58;
        private const int E_SOCCodeOrdinal = 59;
        private const int E_ScheduleEffectiveDateOrdinal = 60;
        private const int E_SSNOrdinal = 62;
        private const int E_PayScheduleNameOrdinal = 63;
        private const int E_StartDayOfWeekOrdinal = 64;
        private const int E_CaseNumberOrdinal = 65;
        private const int E_AverageMinutesWorkedPerWeekOrdinal = 66;

        // Work Schedule
        private const int K_MinutesPerWeekOrdinal = 2;
        private const int K_HoursWorkedIn12MonthsOrdinal = 3;
        private const int K_WorkTimeSunOrdinal = 4;
        private const int K_WorkTimeMonOrdinal = 5;
        private const int K_WorkTimeTueOrdinal = 6;
        private const int K_WorkTimeWedOrdinal = 7;
        private const int K_WorkTimeThuOrdinal = 8;
        private const int K_WorkTimeFriOrdinal = 9;
        private const int K_WorkTimeSatOrdinal = 10;
        private const int K_VariableScheduleOrdinal = 11;
        private const int K_ScheduleEffectiveDateOrdinal = 12;
        private const int K_CaseNumberOrdinal = 16;

        // RC File
        private const int Z_CaseNumberOrdinal = 2;
        private const int Z_StartDate = 3;
        private const int Z_EndDate = 4;
        private const int Z_LeaveInterval = 5;
        private const int Z_PolicyCode = 6;

        #endregion Const

        #region Private Members

        private string[] _parts;
        private List<string> _errors;
        private readonly List<string> _warnings;
        private const int MaxMinutesPerWeek = 10080;

        #endregion Private Members

        #region .ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CaseRow"/> class.
        /// </summary>
        public CaseRow()
        {
            _errors = new List<string>();
            _warnings = new List<string>();
            Delimiter = ',';
            _parts = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CaseRow" /> class.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        /// <param name="customFields">The custom fields.</param>
        /// <param name="contactTypes">The contact types.</param>
        public CaseRow(string[] rowSource, List<CustomField> customFields, Dictionary<string, string> contactTypes, List<EmployeeClass> employeeClassTypes) : this()
        {
            Parse(rowSource, customFields, contactTypes, employeeClassTypes);
        }

        #endregion .ctor

        public enum RecordType
        {
            Case = 'C',
            Adjudication = 'A',
            Intermittent = 'I',
            WorkRelated = 'W',
            CaseConversionEligibility = 'E',
            CustomField = 'F',
            Note = 'N',
            WorkSchedule = 'K',
            RCFile = 'Z'
        }

        #region Properties

        /// <summary>
        /// Gets or sets the delimiter.
        /// </summary>
        /// <value>
        /// The delimiter.
        /// </value>
        public char Delimiter { get; set; }
        /// <summary>
        /// Gets a value indicating whether this instance is error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is error; otherwise, <c>false</c>.
        /// </value>
        public bool IsError { get => _errors.Any(); }
        /// <summary>
        /// Gets a value indicating whether this instance is warning.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is warning; otherwise, <c>false</c>.
        /// </value>
        public bool IsWarning { get => _warnings.Any(); }
        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get => IsError ? string.Join(Environment.NewLine, _errors) : null; }
        /// <summary>
        /// Gets the warning.
        /// </summary>
        /// <value>
        /// The warning.
        /// </value>
        public string Warning { get => IsWarning ? string.Join(Environment.NewLine, _warnings) : null; }
        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public IReadOnlyCollection<string> Errors { get { return _errors.AsReadOnly(); } }
        /// <summary>
        /// Gets the warnings.
        /// </summary>
        /// <value>
        /// The warnings.
        /// </value>
        public IReadOnlyCollection<string> Warnings { get { return _warnings.AsReadOnly(); } }
        /// <summary>
        /// Gets or sets the line number.
        /// </summary>
        /// <value>
        /// The line number.
        /// </value>
        public long LineNumber { get; set; }

        public RecordType RecType { get; set; }
        public string EmployeeNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string CaseNumber { get; set; }

        // Eligibility Row Ordinals
        public CaseType? LeaveInterval { get; set; }
        public string ReasonForLeave { get; set; }
        public ContactType FamilyRelationship { get; set; }
        public int? IntermittentFrequency { get; set; }
        public Unit? IntermittentFrequencyUnit { get; set; }
        public int? IntermittentOccurrences { get; set; }
        public double? IntermittentDuration { get; set; }
        public Unit? IntermittentDurationUnit { get; set; }
        public DateTime? DateCaseOpened { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public DateTime? ExpectedRTWDate { get; set; }
        public DateTime? ConfirmedRTWDate { get; set; }
        public DateTime? CaseClosedDate { get; set; }
        public CaseStatus? CaseStatus { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public DateTime? IllnessOrInjuryDate { get; set; }
        public DateTime? HospitalAdmissionDate { get; set; }
        public DateTime? HospitalReleaseDate { get; set; }
        public DateTime? RTWReleaseDate { get; set; }
        public bool? AutoPolicies { get; set; }
        public int? ReducedSchedule { get; set; }

        // Adjudication Detail
        public DateTime? AdjudicationStartDate { get; set; }
        public DateTime? AdjudicationEndDate { get; set; }
        public AdjudicationStatus? AdjudicationStatus { get; set; }
        public int? MinutesUsed { get; set; }

        // Intermittent Time Used
        public DateTime? DateOfAbsence { get; set; }
        public int? MinutesApproved { get; set; }
        public int? MinutesDenied { get; set; }
        public int? MinutesPending { get; set; }

        // Shared
        public string PolicyCode { get; set; }
        public bool? Workflow { get; set; }


        // work related
        public WorkRelatedInfo WorkRelated { get; set; }

        // Custom Fields
        public string CustomFieldCode { get; set; }
        public string CustomFieldValue { get; set; }
        public string EmployerReferenceCode { get; set; }
        public EntityTarget EntityTargetValue { get; set; }
        public CustomField CustomField { get; set; }

        // Employee Demographic
        public string JobTitle { get; set; }
        public string JobLocation { get; set; }
        public string WorkState { get; set; }
        public string WorkCountry { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneAlt { get; set; }
        public string Email { get; set; }
        public string EmailAlt { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string EmploymentType { get; set; }
        public string ManagerEmployeeNumber { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerPhone { get; set; }
        public string ManagerEmail { get; set; }
        public string HREmployeeNumber { get; set; }
        public string HRLastName { get; set; }
        public string HRFirstName { get; set; }
        public string HRPhone { get; set; }
        public string HREmail { get; set; }
        public string SpouseEmployeeNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public char? Gender { get; set; }
        public char? ExemptionStatus { get; set; }
        public bool? Meets50In75 { get; set; }
        public bool? KeyEmployee { get; set; }
        public byte? MilitaryStatus { get; set; }
        public char? EmploymentStatus { get; set; }
        public DateTime? TerminationDate { get; set; }
        public decimal? PayRate { get; set; }
        public byte? PayType { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? RehireDate { get; set; }
        public DateTime? AdjustedServiceDate { get; set; }
        public int? MinutesPerWeek { get; set; }
        public decimal? HoursWorkedIn12Months { get; set; }
        public int? AverageMinutesWorkedPerWeek { get; set; }
        public int? WorkTimeSun { get; set; }
        public int? WorkTimeMon { get; set; }
        public int? WorkTimeTue { get; set; }
        public int? WorkTimeWed { get; set; }
        public int? WorkTimeThu { get; set; }
        public int? WorkTimeFri { get; set; }
        public int? WorkTimeSat { get; set; }
        public bool? VariableSchedule { get; set; }
        public bool? NoOverwriteFlag { get; set; }
        public string JobDescription { get; set; }
        public byte? JobClassification { get; set; }
        public string USCensusJobCategoryCode { get; set; }
        public string SOCCode { get; set; }
        public DateTime? ScheduleEffectiveDate { get; set; }
        public string SSN { get; set; }
        public string PayScheduleName { get; set; }
        public DayOfWeek? StartDayOfWeek { get; set; }


        // notes
        public DateTime? NoteDate { get; set; }
        public int? CaseNoteCategory { get; set; }
        public string NoteEnteredByName { get; set; }
        public string NoteEnteredByEmployeeNumber { get; set; }
        public string NoteText { get; set; }


        // Z Reocrd
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        #endregion Fields

        /// <summary>
        /// Gets a value indicating whether this instance has minutes warning.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has minutes warning; otherwise, <c>false</c>.
        /// </value>
        public bool HasMinutesWarning { get; private set; }

        #region Parsing

        /// <summary>
        /// Parses the specified row source.
        /// </summary>
        /// <param name="rowSource">The row source.</param>
        /// <param name="customFields">The custom fields.</param>
        /// <param name="contactTypes">The contact types.</param>
        /// <returns></returns>
        public bool Parse(string[] rowSource = null, List<CustomField> customFields = null, Dictionary<string, string> contactTypes = null, List<EmployeeClass> employeeClassTypes = null)
        {
            _parts = rowSource;
            _errors = new List<string>();

            if (rowSource == null || rowSource.Length == 0)
            {
                return AddError("No row source to parse");
            }

            //
            // Record Type
            if (Required(RecordTypeOrdinal, "Record Type"))
            {
                if (!new string[] { "C", "A", "I", "W", "F", "E", "N", "K", "Z" }.Contains(_parts[RecordTypeOrdinal]))
                {
                    return AddError("Unknown Record Type '{0}' passed. The first value in each record must either be 'C', 'A', 'I', 'F', 'E', 'N' 'W', 'K', or 'Z'", _parts[RecordTypeOrdinal]);
                }

                RecType = (RecordType)Convert.ToChar(_parts[RecordTypeOrdinal]);
            }
            else
            {
                return false;
            }

            // =============
            // Shared Fields
            // =============
            //
            // Employee Number
            if (Required(EmployeeNumberOrdinal, "Employee Number"))
            {
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);
            }

            if (RecType != RecordType.WorkSchedule)
            {
                LastName = Normalize(EmployeeLastNameOrdinal);
                FirstName = Normalize(EmployeeFirstNameOrdinal);
            }
            if (RecType == RecordType.CustomField)
            {
                if (Required(CF_CaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(CF_CaseNumberOrdinal);
                }
            }
            else if (RecType == RecordType.CaseConversionEligibility)
            {
                if (Required(E_CaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(E_CaseNumberOrdinal);
                }
            }
            else if (RecType == RecordType.WorkSchedule)
            {
                if (Required(K_CaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(K_CaseNumberOrdinal);
                }

                LastName = null;
                FirstName = null;
            }
            else if (RecType == RecordType.RCFile)
            {
                if (Required(Z_CaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(Z_CaseNumberOrdinal);
                }
            }
            else
            {
                if (Required(CaseNumberOrdinal, "Unique Case Number"))
                {
                    CaseNumber = Normalize(CaseNumberOrdinal);
                }
            }

            // =========================
            // Type Specific Record Fields
            // =========================
            if (RecType == RecordType.Case)
            {
                #region Case
                //
                // Leave Interval / Case Type
                if (Required(LeaveIntervalOrdinal, "Leave Interval"))
                {
                    int? leaveInterval = ParseInt(LeaveIntervalOrdinal, "Leave Interval");
                    if (leaveInterval.HasValue)
                    {
                        if (leaveInterval != 1 && leaveInterval != 2 && leaveInterval != 4 && leaveInterval != 8)
                            AddError("Leave interval of '{0}' is not valid, it must be 1, 2, 4 or 8.", leaveInterval);
                        else
                            LeaveInterval = (CaseType)leaveInterval.Value;
                    }
                }
                //
                // Reason for Leave
                if (Required(ReasonForLeaveOrdinal, "Reason for Leave"))
                    ReasonForLeave = Normalize(ReasonForLeaveOrdinal);
                //
                // Family Relationship
                int? famRel = ParseInt(FamilyRelationshipOrdinal, "Family Relationship");
                if (famRel.HasValue)
                {
                    if (famRel == 0 || (famRel >= 5 && famRel <= 32) || famRel == 37)
                        FamilyRelationship = famRel > 0 ? ContactType.GetById(String.Format("{0:000000000000000000000000}", famRel.Value)) : null; // Make SELF null
                    else
                        AddError("Family Relationship of '{0}' is not valid, it must be a valid value from the list of contact types, 0, 37, or between 5 and 32.", famRel);
                }
                //
                // Intermittent leaves
                if (LeaveInterval == CaseType.Intermittent)
                {
                    //
                    // Intermittent Frequency
                    IntermittentFrequency = ParseInt(IntermittentFrequencyOrdinal, "Intermittent Frequency");
                    //
                    // Intermittent Frequency Unit/Type
                    int? intFreqU = ParseInt(IntermittentFrequencyUnitOrdinal, "Intermittent Frequency Unit/Type");
                    if (intFreqU.HasValue && (intFreqU < 0 || intFreqU > 5))
                        AddError("Intermittent Frequency Unit/Type value of '{0}' is not valid, value must be an integer value 0 through 5.", intFreqU);
                    else if (intFreqU.HasValue)
                        IntermittentFrequencyUnit = (Unit)intFreqU.Value;
                    //
                    // Intermittent Occurrences
                    IntermittentOccurrences = ParseInt(IntermittentOccurrencesOrdinal, "Intermittent Occurrences");
                    //
                    // Intermittent Duration
                    IntermittentDuration = ParseDouble(IntermittentDurationOrdinal, "Intermittent Duration");
                    //
                    // Intermittent Duration Unit/Type
                    int? intDurU = ParseInt(IntermittentDurationUnitOrdinal, "Intermittent Frequency Unit/Type");
                    if (intDurU.HasValue && (intDurU < 0 || intDurU > 5))
                        AddError("Intermittent Duration Unit/Type value of '{0}' is not valid, value must be an integer value 0 through 5.", intDurU);
                    else if (intDurU.HasValue)
                        IntermittentDurationUnit = (Unit)intDurU.Value;
                    //
                }
                //
                // Date Case Opened
                if (Required(DateCaseOpenedOrdinal, "Date Case Opened"))
                    DateCaseOpened = ParseDate(DateCaseOpenedOrdinal, "Date Case Opened");
                //
                // Expected Start Date
                if (Required(ExpectedStartDateOrdinal, "Expected Start Date"))
                    ExpectedStartDate = ParseDate(ExpectedStartDateOrdinal, "Expected Start Date");
                //
                // Expected RTW Date
                //if (Required(ExpectedRTWDateOrdinal, "Expected RTW"))
                ExpectedRTWDate = ParseDate(ExpectedRTWDateOrdinal, "Expected RTW");
                //
                // Confirmed RTW Date
                ConfirmedRTWDate = ParseDate(ConfirmedRTWDateOrdinal, "Confirmed RTW Date");
                //
                // Date Case Closed
                CaseClosedDate = ParseDate(CaseClosedDateOrdinal, "Date Case Closed");
                //
                // Validate Non-Admin Case End Date
                if (LeaveInterval != CaseType.Administrative && !ExpectedRTWDate.HasValue && !ConfirmedRTWDate.HasValue && !CaseClosedDate.HasValue)
                    AddError("You must provide either an Expected RTW Date, Confirmed RTW Date or the Case Closed Date for Non-Administrative cases.");
                ExpectedRTWDate = ExpectedRTWDate ?? ConfirmedRTWDate ?? CaseClosedDate;
                //
                // Warn if gnarly case dates
                if (CaseClosedDate.HasValue && CaseClosedDate < DateCaseOpened)
                    AddWarning("The Case closed date of '{0:MM/dd/yyyy}' comes before the case opened date, '{1:MM/dd/yyyy}'.", CaseClosedDate, DateCaseOpened);
                if ((ConfirmedRTWDate ?? ExpectedRTWDate ?? CaseClosedDate) < ExpectedStartDate)
                    AddWarning("The Case end date of '{0:MM/dd/yyyy}' comes before the expected start date, '{1:MM/dd/yyyy}'", (ConfirmedRTWDate ?? ExpectedRTWDate ?? CaseClosedDate), ExpectedStartDate);
                //
                // Case Status
                int? cSt = ParseInt(CaseStatusOrdinal, "Case Status");
                if (cSt.HasValue && (cSt < 0 || cSt > 2))
                    AddError("Case Status of '{0}' is not valid. Must be 0 (Open), 1 (Closed) or 2 (Cancelled).", cSt);
                else
                    CaseStatus = (CaseStatus)(cSt ?? 0);
                //
                // Delivery Date
                DeliveryDate = ParseDate(DeliveryDateOrdinal, "Delivery Date");
                //
                // Bonding Start Date
                BondingStartDate = ParseDate(BondingStartDateOrdinal, "Bonding Start Date");
                //
                // Bonding End Date
                BondingEndDate = ParseDate(BondingEndDateOrdinal, "Bonding End Date");
                if (BondingStartDate.HasValue && BondingEndDate.HasValue && BondingStartDate > BondingEndDate)
                    AddError("The bonding start date of '{0:MM/dd/yyyy}' is after the end date of '{1:MM/dd/yyyy}'.", BondingStartDate, BondingEndDate);
                //
                // Illness or Injury Date
                IllnessOrInjuryDate = ParseDate(IllnessOrInjuryDateOrdinal, "Illness or Injury Date");
                //
                // Hospital Admission Date
                HospitalAdmissionDate = ParseDate(HospitalAdmissionDateOrdinal, "Hospital Admission Date");
                //
                // Hospital Release Date
                HospitalReleaseDate = ParseDate(HospitalReleaseDateOrdinal, "Hospital Release Date");
                if (HospitalAdmissionDate.HasValue && HospitalReleaseDate.HasValue && HospitalAdmissionDate > HospitalReleaseDate)
                    AddError("The hospital admission date of '{0:MM/dd/yyyy}' is after the release date of '{1:MM/dd/yyyy}'. If this is a relapse/re-admission, please use the original hospital admission date.", HospitalAdmissionDate, HospitalReleaseDate);
                //
                // RTW Release Date
                RTWReleaseDate = ParseDate(RTWReleaseDateOrdinal, "RTW Release Date");
                //
                // Auto Policies
                AutoPolicies = ParseBool(AutoPoliciesOrdinal, "Auto Policies");
                //
                // Reduced Schedule
                if (LeaveInterval == CaseType.Reduced && Required(ReducedScheduleOrdinal, "Reduced Schedule"))
                {
                    ReducedSchedule = ParseInt(ReducedScheduleOrdinal, "Reduced Schedule");
                }
                //
                // Case Create Workflow
                Workflow = ParseBool(CaseCreateWorkflowOrdinal, "Case Create Workflow");
                #endregion
            }
            else if (RecType == RecordType.Adjudication)
            {
                #region Adjudication
                //
                // Start Date
                if (Required(AdjudicationStartDateOrdinal, "Start Date"))
                    AdjudicationStartDate = ParseDate(AdjudicationStartDateOrdinal, "Start Date");
                //
                // End Date
                AdjudicationEndDate = ParseDate(AdjudicationEndDateOrdinal, "End Date");
                //
                // Status / Adjudication
                int? st = ParseInt(AdjudicationStatusOrdinal, "Status / Adjudication");
                if (st.HasValue && (st < 0 || st > 2))
                    AddError("Status / Adjudication of '{0}' is not valid. Must be 0 (Pending), 1 (Approved) or 2 (Denied).", st);
                else
                    AdjudicationStatus = (AdjudicationStatus)(st ?? 0);
                //
                // Minutes Used
                MinutesUsed = ParseInt(MinutesUsedOrdinal, "Minutes Used");
                if (MinutesUsed.HasValue && MinutesUsed < 0)
                    AddError("Minutes Used must be a positive integer. Value supplied was '{0}'.", MinutesUsed);
                if (AdjudicationStatus != Data.Enums.AdjudicationStatus.Approved && MinutesUsed.HasValue && MinutesUsed.Value != 0)
                {
                    AddWarning("You may not specify Minutes Used for a period when not approving that period.; setting this to null.");
                    MinutesUsed = null;
                }
                if (MinutesUsed.HasValue && AdjudicationEndDate.HasValue && AdjudicationStartDate.HasValue)
                {
                    var days = (AdjudicationEndDate.Value.ToMidnight() - AdjudicationStartDate.Value.ToMidnight()).TotalDays + 1;
                    var minutesPerDay = (double)MinutesUsed.Value / days;
                    if (minutesPerDay > 1440D)
                    {
                        AddWarning("{0:N0} Minutes Used is too large of a value over {1:N0} days; Equates to {2:N0} minutes per day, which is greater than the max per day usage of 1,440 minutes (24 hours).",
                            MinutesUsed, days, minutesPerDay);
                        HasMinutesWarning = true;
                    }
                }
                //
                // Policy Code
                PolicyCode = Normalize(PolicyCodeOrdinal);
                //
                // Workflow
                Workflow = ParseBool(WorkflowOrdinal, "Adjudication Workflow");
                #endregion
            }
            else if (RecType == RecordType.Intermittent)
            {
                #region Intermittent
                //
                // Date of Absence
                if (Required(DateOfAbsenceOrdinal, "Date of Absence"))
                    DateOfAbsence = ParseDate(DateOfAbsenceOrdinal, "Date of Absence");
                //
                // Minutes Approved
                MinutesApproved = ParseInt(MinutesApprovedOrdinal, "Minutes Approved");
                //
                // Minutes Denied
                MinutesDenied = ParseInt(MinutesDeniedOrdinal, "Minutes Denied");
                //
                // Minutes Pending
                MinutesPending = ParseInt(MinutesPendingOrdinal, "Minutes Pending");
                //
                // Minutes 
                var totalMinutes = (MinutesApproved ?? 0) + (MinutesDenied ?? 0) + (MinutesPending ?? 0);
                if (totalMinutes > 1440)
                {
                    AddWarning("{0:N0} is greater than the max per day usage of 1,440 minutes (24 hours).", totalMinutes);
                    HasMinutesWarning = true;
                }
                //
                // Policy Code
                PolicyCode = Normalize(PolicyCodeOrdinal);
                //
                // Workflow
                Workflow = ParseBool(WorkflowOrdinal, "Time Off Request Workflow");
                #endregion
            }
            else if (RecType == RecordType.WorkRelated)
            {
                #region Work Related

                WorkRelated = new WorkRelatedInfo()
                {
                    Reportable = ParseBool(WC_Reportable, "Reportable") ?? true,
                    HealthCarePrivate = ParseBool(WC_HealthcarePrivate, "Healthcare Private"),
                    WhereOccurred = Normalize(WC_WhereOccurred)
                };
                int? cl = ParseInt(WC_CaseClassification, "Case Classification");

                if (cl < 0 || cl > 3)
                {
                    AddError("'{0}' is not a valid case classification, it must be 0, 1, 2 or 3", Normalize(WC_CaseClassification));
                }
                else if (cl.HasValue)
                {
                    WorkRelated.Classification = (WorkRelatedCaseClassification)cl.Value;
                }

                int? c2 = ParseInt(WC_TypeOfInjury, "Type of Injury");
                if (c2 < 0 || c2 > 5)
                {
                    AddError("'{0}' is not a valid type of injury, it must be 0, 1, 2, 3, 4 or 5", Normalize(WC_TypeOfInjury));
                }
                else if (cl.HasValue)
                {
                    WorkRelated.TypeOfInjury = (WorkRelatedTypeOfInjury)c2.Value;
                }

                WorkRelated.DaysAwayFromWork = ParseInt(WC_DaysAwayfromWork, "Days Away from Work");
                WorkRelated.DaysOnJobTransferOrRestriction = ParseInt(WC_DaysontheJobTransferorRestriction, "Days on the Job Transfer or Restriction");
                string providerType = Normalize(WC_HealthcareProviderTypeCode);
                if (!string.IsNullOrWhiteSpace(providerType))
                {
                    WorkRelated.Provider = new EmployeeContact() { ContactTypeCode = providerType };
                    if (contactTypes != null && contactTypes.TryGetValue(providerType, out string name))
                    {
                        WorkRelated.Provider.ContactTypeName = name;
                    }

                    WorkRelated.Provider.Contact.FirstName = Normalize(WC_HealthcareProviderFirstName);
                    WorkRelated.Provider.Contact.LastName = Normalize(WC_HealthcareProviderLastName);
                    WorkRelated.Provider.Contact.CompanyName = Normalize(WC_HealthcareProviderCompany);
                    WorkRelated.Provider.Contact.Address.Address1 = Normalize(WC_HealthcareProviderAddress1);
                    WorkRelated.Provider.Contact.Address.Address2 = Normalize(WC_HealthcareProviderAddress2);
                    WorkRelated.Provider.Contact.Address.City = Normalize(WC_HealthcareProviderCity);
                    WorkRelated.Provider.Contact.Address.State = Normalize(WC_HealthcareProviderState);
                    WorkRelated.Provider.Contact.Address.Country = Normalize(WC_HealthcareProviderCountry);
                    WorkRelated.Provider.Contact.Address.PostalCode = Normalize(WC_HealthcareProviderPostalCode);
                    WorkRelated.Provider.EmployeeNumber = EmployeeNumber;
                }
                WorkRelated.EmergencyRoom = ParseBool(WC_EmergencyRoom, "Emergency Room");
                WorkRelated.HospitalizedOvernight = ParseBool(WC_HospitalizedOvernight, "Hospitalized Overnight");
                string tebw = Normalize(WC_TimeEmployeeBeganWork);
                if (!string.IsNullOrWhiteSpace(tebw))
                {
                    if (TimeOfDay.TryParse(tebw, out TimeOfDay tod))
                        WorkRelated.TimeEmployeeBeganWork = tod;
                    else
                        AddError("'{0}' is not a valid time of day. Please provide a valid value for 'Time Employee Began Work' using the 'HH:mm' or 'hh:mm tt' format", tebw);
                }
                string toe = Normalize(WC_TimeOfEvent);
                if (!string.IsNullOrWhiteSpace(toe))
                {
                    if (TimeOfDay.TryParse(toe, out TimeOfDay tod))
                        WorkRelated.TimeOfEvent = tod;
                    else
                        AddError("'{0}' is not a valid time of day. Please provide a valid value for 'Time Of Event' using the 'HH:mm' or 'hh:mm tt' format", toe);
                }
                WorkRelated.ActivityBeforeIncident = Normalize(WC_ActivityBeforeIncident);
                WorkRelated.WhatHappened = Normalize(WC_WhatHappened);
                WorkRelated.InjuryOrIllness = Normalize(WC_InjuryOrIllness);
                WorkRelated.InjuryLocation = Normalize(WC_InjuryLocation);
                WorkRelated.WhatHarmedTheEmployee = Normalize(WC_WhatHarmedTheEmployee);
                WorkRelated.DateOfDeath = ParseDate(WC_DateOfDeath, "Date of Death");
                WorkRelated.InjuredBodyPart = Normalize(WC_PartOfBody);
                WorkRelated.Sharps = ParseBool(WC_Sharps, "Sharps");
                if (WorkRelated.Sharps == true)
                {
                    WorkRelated.SharpsInfo = new WorkRelatedSharpsInfo();

                    cl = ParseInt(WC_SharpsInjuryLocation, "Sharps Injury Location");
                    if (cl < 0 || cl > 10)
                        AddError("'{0}' is not a valid sharps injury location, it must be an integer of 1 through 10", Normalize(WC_SharpsInjuryLocation));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.InjuryLocation = new List<WorkRelatedSharpsInjuryLocation>(1) { (WorkRelatedSharpsInjuryLocation)cl.Value };


                    WorkRelated.SharpsInfo.TypeOfSharp = Normalize(WC_TypeOfSharp);
                    WorkRelated.SharpsInfo.BrandOfSharp = Normalize(WC_BrandOfSharp);
                    WorkRelated.SharpsInfo.ModelOfSharp = Normalize(WC_ModelOfSharp);
                    WorkRelated.SharpsInfo.BodyFluidInvolved = Normalize(WC_BodyFluidInvolved);
                    WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection = ParseBool(WC_HaveEngineeredInjuryProtection, "Have Engineered Injury Protection");

                    if (WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection == true)
                    {
                        WorkRelated.SharpsInfo.WasProtectiveMechanismActivated = ParseBool(WC_WasProtectiveMechanismActivated, "Was Protective Mechanism Activated");
                        cl = ParseInt(WC_WhenDidtheInjuryOccur, "When Did the Injury Occur");
                        if (cl < 0 || cl > 3)
                            AddError("'{0}' is not a valid sharps injury timeframe, it must be an integer of 0, 1, 2 or 3", Normalize(WC_WhenDidtheInjuryOccur));
                        else if (cl.HasValue)
                            WorkRelated.SharpsInfo.WhenDidTheInjuryOccur = (WorkRelatedSharpsWhen)cl.Value;
                    }

                    cl = ParseInt(WC_JobClassification, "Job Classification");
                    if (cl < 0 || cl > 10)
                        AddError("'{0}' is not a valid sharps job classification, it must be an integer of 0 through 10", Normalize(WC_JobClassification));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.JobClassification = (WorkRelatedSharpsJobClassification)cl.Value;

                    if (WorkRelated.SharpsInfo.JobClassification == WorkRelatedSharpsJobClassification.Other)
                        WorkRelated.SharpsInfo.JobClassificationOther = Normalize(WC_JobClassificationOther);


                    cl = ParseInt(WC_LocationandDepartment, "Location and Department");
                    if (cl < 0 || cl > 8)
                        AddError("'{0}' is not a valid sharps job location/department, it must be an integer of 0 through 8", Normalize(WC_LocationandDepartment));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.LocationAndDepartment = (WorkRelatedSharpsLocation)cl.Value;

                    if (WorkRelated.SharpsInfo.LocationAndDepartment == WorkRelatedSharpsLocation.Other)
                        WorkRelated.SharpsInfo.JobClassificationOther = Normalize(WC_LocationandDepartmentOther);

                    cl = ParseInt(WC_SharpsProcedure, "Sharps Procedure");
                    if (cl < 0 || cl > 8)
                        AddError("'{0}' is not a valid sharps procedure, it must be an integer of 0 through 8", Normalize(WC_SharpsProcedure));
                    else if (cl.HasValue)
                        WorkRelated.SharpsInfo.Procedure = (WorkRelatedSharpsProcedure)cl.Value;

                    if (WorkRelated.SharpsInfo.Procedure == WorkRelatedSharpsProcedure.Other)
                    {
                        WorkRelated.SharpsInfo.ProcedureOther = Normalize(WC_SharpsProcedureOther);
                    }

                    WorkRelated.SharpsInfo.ExposureDetail = Normalize(WC_ExposureDetail);
                }

                #endregion
            }
            else if (RecType == RecordType.CustomField)
            {
                #region Case Custom Fields
                
                CustomFieldCode = Normalize(CF_CustomFieldCodeOrdinal);
                CustomFieldValue = Normalize(CF_CustomFieldValueOrdinal);
                EmployerReferenceCode = Normalize(CF_EmployerReferenceCodeOrdinal);

                int nc = ParseInt(CF_EntityTargetOrdinal, "Entity Target") ?? 1;
                if (Enum.TryParse(nc.ToString(), out EntityTarget ncCon))
                    EntityTargetValue = ncCon;
                else
                    EntityTargetValue = EntityTarget.Employee;

                if (string.IsNullOrWhiteSpace(CustomFieldCode))
                    AddError("Custom Fields Code can't be blank");
                if (string.IsNullOrWhiteSpace(CustomFieldValue))
                    AddError("Custom Fields Value can't be blank");

                CustomField = customFields.Where(f => f.Target.HasFlag(EntityTargetValue))
                          .FirstOrDefault(cf => string.Equals(cf.Code, CustomFieldCode, StringComparison.InvariantCultureIgnoreCase))?.Clone();

                if (CustomField != null)
                {
                    UpdateCustomFieldValue(ParseCustomFieldValue(CustomField), CustomField);
                }
                else
                {
                    AddError("This row contains a custom field which is not configured");
                }

                #endregion
            }
            else if (RecType == RecordType.CaseConversionEligibility)
            {
                #region CaseConversionEligibility
                //
                // Employer Reference Code
                EmployeeNumber = Normalize(EmployeeNumberOrdinal);

                //
                // Employee Last Name
                if (Required(EmployeeLastNameOrdinal, "Employee Last Name"))
                    LastName = Normalize(EmployeeLastNameOrdinal);
                //
                // Employee First Name
                if (Required(EmployeeFirstNameOrdinal, "Employee First Name"))
                    FirstName = Normalize(EmployeeFirstNameOrdinal);
                //
                // Employee Middle Name
                MiddleName = Normalize(E_EmployeeMiddleNameOrdinal);
                //
                // Job Title
                JobTitle = Normalize(E_JobTitleOrdinal);
                //
                // Job Location
                JobLocation = Normalize(E_JobLocationOrdinal);
                //
                // Work State
                // Work Country
                if (Required(E_WorkStateOrdinal, "Work State"))
                {
                    WorkState = Normalize(E_WorkStateOrdinal).ToUpperInvariant();
                    WorkCountry = (Normalize(E_WorkCountryOrdinal) ?? "US").ToUpperInvariant();
                    if (WorkCountry == "US")
                        ValidUSState(WorkState, "Work State");
                }
                //
                // Employee Phone Home
                PhoneHome = Normalize(E_PhoneHomeOrdinal);
                //
                // Employee Phone Work
                PhoneWork = Normalize(E_PhoneWorkOrdinal);
                //
                // Employee Phone Mobile
                PhoneMobile = Normalize(E_PhoneMobileOrdinal);
                //
                // Employee Phone Alt
                PhoneAlt = Normalize(E_PhoneAltOrdinal);
                //
                // Employee Email
                Email = Normalize(E_EmailOrdinal);
                ValidEmail(Email, "Employee Email");
                //
                // Employee Alt Email
                EmailAlt = Normalize(E_EmailAltOrdinal);
                ValidEmail(EmailAlt, "Employee Alt Email");
                //
                // Mailing Address
                Address = Normalize(E_AddressOrdinal);
                //
                // Mailing Address 2
                Address2 = Normalize(E_Address2Ordinal);
                //
                // Mailing City
                City = Normalize(E_CityOrdinal);
                //
                // Mailing State
                State = Normalize(E_StateOrdinal);
                //
                // Mailing Postal Code
                PostalCode = Normalize(E_PostalCodeOrdinal);
                //
                // Mailing Country
                Country = Normalize(E_CountryOrdinal);
                if (!string.IsNullOrWhiteSpace(State) && (string.IsNullOrWhiteSpace(Country) || Country == "US"))
                    ValidUSState(State, "Mailing State");
                if (string.IsNullOrWhiteSpace(Country) || Country == "US")
                    if (!string.IsNullOrWhiteSpace(PostalCode) && PostalCode.Length == 4)
                        PostalCode = string.Concat("0", PostalCode);
                //
                // PT/FT Status
                EmploymentType = GetEmployementType(Normalize(E_EmploymentTypeOrdinal));
                if (!String.IsNullOrWhiteSpace(EmploymentType) && !ValidateEmployementType(EmploymentType, employeeClassTypes))
                {
                    AddError("'{0}' is not a valid value for EmploymentType'.", EmploymentType);
                }
                //
                // Manager Employee Number
                ManagerEmployeeNumber = Normalize(E_ManagerEmployeeNumberOrdinal);
                //
                // Manager Last Name
                ManagerLastName = Normalize(E_ManagerLastNameOrdinal);
                //
                // Manager First Name
                ManagerFirstName = Normalize(E_ManagerFirstNameOrdinal);
                //
                // Manager Phone
                ManagerPhone = Normalize(E_ManagerPhoneOrdinal);
                //
                // Manager Email
                ManagerEmail = Normalize(E_ManagerEmailOrdinal);
                ValidEmail(ManagerEmail, "Manager Email");
                //
                // HR Contact Employee Number
                HREmployeeNumber = Normalize(E_HREmployeeNumberOrdinal);
                //
                // HR Contact Last Name
                HRLastName = Normalize(E_HRLastNameOrdinal);
                //
                // HR Contact First Name
                HRFirstName = Normalize(E_HRFirstNameOrdinal);
                //
                // HR Contact Phone
                HRPhone = Normalize(E_HRPhoneOrdinal);
                //
                // HR Contact Email
                HREmail = Normalize(E_HREmailOrdinal);
                ValidEmail(HREmail, "HR Contact Email");
                //
                // Spouse Employee Number
                SpouseEmployeeNumber = Normalize(E_SpouseEmployeeNumberOrdinal);
                //
                // Date of Birth
                DateOfBirth = ParseDate(E_DateOfBirthOrdinal, "Date of Birth");
                //
                // Gender
                Gender = ParseChar(E_GenderOrdinal, "Date of Birth");
                if (Gender.HasValue && Gender != 'F' && Gender != 'M' && Gender != 'U')
                    AddError("'{0}' is not a recognized employee gender assignment code, please provide the value 'F' (Female), 'M' (Male) or 'U' (Unknown/Not Collected)", Gender);
                else if (!Gender.HasValue)
                    AddWarning("Gender was not supplied for the employee... it's not required but we just thought you should know that");
                //
                // Exemption Status
                ExemptionStatus = ParseChar(E_ExemptionStatusOrdinal, "Exemption Status");
                if (ExemptionStatus.HasValue && ExemptionStatus != 'E' && ExemptionStatus != 'N')
                    AddError("'{0}' is not a recognized employee exemption status indicator, please provide the value 'E' (Exempt) or 'N' (Non-Exempt)", ExemptionStatus);
                else if (!ExemptionStatus.HasValue)
                {
                    ExemptionStatus = 'N'; // default
                    AddWarning("Exemption status was not provided so we defaulted it to Not Exempt, hope that was correct");
                }
                //
                // Meets 50 in 75
                Meets50In75 = ParseBool(E_Meets50In75Ordinal, "Meets 50 in 75");
                //
                // Key Employee
                KeyEmployee = ParseBool(E_KeyEmployeeOrdinal, "Key Employee");
                //
                // Military Status
                MilitaryStatus = ParseByte(E_MilitaryStatusOrdinal, "Military Status");
                if (MilitaryStatus.HasValue && (MilitaryStatus < 0 || MilitaryStatus > 2))
                    AddError("'{0}' is not a valid value for 'Military Status'. Valid values are bytes 0 (Civilian), 1 (Active Duty) or 2 (Veteran)", MilitaryStatus);
                //
                // Employment Status
                EmploymentStatus = ParseChar(E_EmploymentStatusOrdinal, "Employment Status");
                if (EmploymentStatus.HasValue && !EmploymentStatusValue.IsValidValue(EmploymentStatus.Value))
                {
                    AddError("'{0}' is not a valid value for 'Employment Status'. Valid values: {1}",
                        EmploymentStatus,
                        EmploymentStatusValue.GetCodeLookupAsString()
                    );
                }
                else if (!EmploymentStatus.HasValue)
                {
                    EmploymentStatus = 'A';
                    AddWarning("Employment status was not provided so we defaulted it to Active ('A'), hope that was correct");
                }
                //
                // Termination Date
                TerminationDate = ParseDate(E_TerminationDateOrdinal, "Termination Date");
                //
                // Pay Rate
                PayRate = ParseDecimal(E_PayRateOrdinal, "Pay Rate");
                //
                // Pay Type
                PayType = ParseByte(E_PayTypeOrdinal, "Pay Type");
                if (PayType.HasValue && PayType != 1 && PayType != 2)
                    AddError("'{0}' is not a valid value for 'Pay Type'. Valid values are bytes 1 (Annual Salary) or 2 (Hourly)", PayType);

                //
                // Hire Date
                HireDate = ParseDate(E_HireDateOrdinal, "Hire Date");
                if (!HireDate.HasValue)
                    AddWarning("No hire date was provided, not cool guys but we'll allow it.");

                //
                // Rehire Date
                RehireDate = ParseDate(E_RehireDateOrdinal, "Rehire Date");

                //
                // Adjusted Service Date
                AdjustedServiceDate = ParseDate(E_AdjustedServiceDateOrdinal, "Adjusted Service Date");
                if (!AdjustedServiceDate.HasValue)
                    AddWarning("No adjusted service date was provided, thing will be okay though.");

                //
                // Scheduled Minutes per Week

                MinutesPerWeek = ParseInt(E_MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);

                // Hours Worked in 12 Months

                HoursWorkedIn12Months = ParseDecimal(E_HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);

                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(E_WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(E_WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(E_WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(E_WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(E_WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(E_WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(E_WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(E_VariableScheduleOrdinal, "Variable Schedule Flag");
                //
                // Job Description
                JobDescription = Normalize(E_JobDescriptionOrdinal);
                //
                // Job Classification
                JobClassification = ParseByte(E_JobClassificationOrdinal, "Job Classification");
                if (JobClassification.HasValue && (JobClassification < 1 || JobClassification > 5))
                    AddError("The value '{0}' is not a valid 'Job Classification'. Valid values are bytes 1 (Sedentary Work), 2 (Light Work), 3 (Medium Work), 4 (Heavy Work) or 5 (Very Heavy Work)", JobClassification);
                //
                // US Census Job Category Code
                USCensusJobCategoryCode = Normalize(E_USCensusJobCategoryCodeOrdinal);
                //
                // SOC Code
                SOCCode = Normalize(E_SOCCodeOrdinal);
                //
                // Schedule Effective Date
                ScheduleEffectiveDate = ParseDate(E_ScheduleEffectiveDateOrdinal, "Schedule Effective Date");
                //
                // SSN
                SSN = Normalize(E_SSNOrdinal);
                //
                // Pay Schedule Name
                PayScheduleName = Normalize(E_PayScheduleNameOrdinal);


                //
                // Start Day of Week
                byte? dow = ParseByte(E_StartDayOfWeekOrdinal, "Start Day of Week");
                if (dow.HasValue)
                {
                    if (dow < 0 || dow > 6)
                        AddError("The value'{0}' is not a valid 'Day of Week'. Valid values are bytes 0 (Sunday) through 6 (Saturday)", dow);
                    else
                        StartDayOfWeek = (DayOfWeek)dow.Value;
                }

                CaseNumber = Normalize(E_CaseNumberOrdinal);

                // Scheduled Hours Worked

                AverageMinutesWorkedPerWeek = ParseInt(E_AverageMinutesWorkedPerWeekOrdinal, "Average minutes worked per week");
                if (AverageMinutesWorkedPerWeek.HasValue && (AverageMinutesWorkedPerWeek > 10080 || AverageMinutesWorkedPerWeek < 0))
                    AddError("The 'Average minutes worked per week' of '{0}' is invalid. Value may not be less than zero and must be less than 10,080 minutes", AverageMinutesWorkedPerWeek);
                #endregion
            }
            else if (RecType == RecordType.Note)
            {
                #region Note

                NoteDate = ParseDate(NOTE_NoteDate, "Note Date");
                if (NoteDate.HasValue)
                {
                    NoteDate = DateTime.Parse($"{NoteDate.Value.Year}-{NoteDate.Value.Month}-{NoteDate.Value.Day} 12:00:00-0400");
                }
                CaseNoteCategory = ParseInt(NOTE_Category, "Note Category") ?? 0;
                NoteEnteredByName = Normalize(NOTE_EnteredByName);
                NoteText = Normalize(NOTE_Text);
                if (string.IsNullOrWhiteSpace(NoteText))
                    AddWarning("Note is blank");

                #endregion
            }
            else if (RecType == RecordType.WorkSchedule)
            {
                #region Work Schedule
                                
                //
                // Scheduled Minutes per Week
                MinutesPerWeek = ParseInt(K_MinutesPerWeekOrdinal, "Scheduled Minutes per Week");
                ValidMinutesPerWeek(MinutesPerWeek);
                //
                // Hours Worked in 12 Months
                HoursWorkedIn12Months = ParseDecimal(K_HoursWorkedIn12MonthsOrdinal, "Hours Worked in 12 Months");
                if (HoursWorkedIn12Months.HasValue && (HoursWorkedIn12Months > 8778 || HoursWorkedIn12Months < 0))
                    AddError("The 'Hours Worked in 12 Months' of '{0}' is invalid. Value may not be less than zero and must be less than 8,778 hours", HoursWorkedIn12Months);
                //
                // Scheduled Work Time Sunday
                WorkTimeSun = ParseInt(K_WorkTimeSunOrdinal, "Scheduled Work Time Sunday");
                if (WorkTimeSun.HasValue && WorkTimeSun < 0 || WorkTimeSun > 1440)
                    AddError("The 'Scheduled Work Time Sunday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSun);
                //
                // Scheduled Work Time Monday
                WorkTimeMon = ParseInt(K_WorkTimeMonOrdinal, "Scheduled Work Time Monday");
                if (WorkTimeMon.HasValue && WorkTimeMon < 0 || WorkTimeMon > 1440)
                    AddError("The 'Scheduled Work Time Monday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeMon);
                //
                // Scheduled Work Time Tuesday
                WorkTimeTue = ParseInt(K_WorkTimeTueOrdinal, "Scheduled Work Time Tuesday");
                if (WorkTimeTue.HasValue && WorkTimeTue < 0 || WorkTimeTue > 1440)
                    AddError("The 'Scheduled Work Time Tuesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeTue);
                //
                // Scheduled Work Time Wednesday
                WorkTimeWed = ParseInt(K_WorkTimeWedOrdinal, "Scheduled Work Time Wednesday");
                if (WorkTimeWed.HasValue && WorkTimeWed < 0 || WorkTimeWed > 1440)
                    AddError("The 'Scheduled Work Time Wednesday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeWed);
                //
                // Scheduled Work Time Thursday
                WorkTimeThu = ParseInt(K_WorkTimeThuOrdinal, "Scheduled Work Time Thursday");
                if (WorkTimeThu.HasValue && WorkTimeThu < 0 || WorkTimeThu > 1440)
                    AddError("The 'Scheduled Work Time Thursday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeThu);
                //
                // Scheduled Work Time Friday
                WorkTimeFri = ParseInt(K_WorkTimeFriOrdinal, "Scheduled Work Time Friday");
                if (WorkTimeFri.HasValue && WorkTimeFri < 0 || WorkTimeFri > 1440)
                    AddError("The 'Scheduled Work Time Friday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeFri);
                //
                // Scheduled Work Time Saturday
                WorkTimeSat = ParseInt(K_WorkTimeSatOrdinal, "Scheduled Work Time Saturday");
                if (WorkTimeSat.HasValue && WorkTimeSat < 0 || WorkTimeSat > 1440)
                    AddError("The 'Scheduled Work Time Saturday' value of '{0}' is invalid. It may not be less than zero or more than 1,440 minutes", WorkTimeSat);
                //
                // Variable Schedule Flag
                VariableSchedule = ParseBool(K_VariableScheduleOrdinal, "Variable Schedule Flag");
                //
                // Schedule Effective Date
                ScheduleEffectiveDate = ParseDate(K_ScheduleEffectiveDateOrdinal, "Schedule Effective Date");

                CaseNumber = Normalize(K_CaseNumberOrdinal);

                #endregion
            }
            else if (RecType == RecordType.RCFile)
            {
                #region RC File
                //
                // Date
                if (Required(Z_StartDate, "Start Date"))
                    StartDate = ParseDate(Z_StartDate, "Start Date");
                //
                // End Date
                if (Required(Z_EndDate, "End Date"))
                    EndDate = ParseDate(Z_EndDate, "End Date");
                //
                // Case Type
                if (Required(Z_LeaveInterval, "Case Type/Leave Interval"))
                {
                    string interval = Normalize(Z_LeaveInterval);
                    if (!string.IsNullOrEmpty(interval))
                    {
                        switch (interval)
                        {
                            case "C":
                                LeaveInterval = CaseType.Consecutive;
                                break;
                            case "R":
                                LeaveInterval = CaseType.Reduced;
                                break;
                            case "I":
                                LeaveInterval = CaseType.Intermittent;
                                break;
                            case "A":
                                LeaveInterval = CaseType.Administrative;
                                break;
                            default:
                                AddError("'Case Type/Leave Interval' is a required field and was not provided, was missing, blank or did not contain any valid information.");
                                break;
                        }
                    }
                }
                //
                // Policy Code
                PolicyCode = Normalize(Z_PolicyCode);
                #endregion
            }
            return IsError;
        }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public virtual bool AddError(string error, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(error))
                return false;
            if (args.Length > 0)
                _errors.Add(string.Format(error, args));
            else
                _errors.Add(error);
            return false;
        }

        /// <summary>
        /// Adds the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public virtual bool AddWarning(string warn, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(warn))
                return false;
            if (args.Length > 0)
                _warnings.Add(string.Format(warn, args));
            else
                _warnings.Add(warn);
            return false;
        }

        /// <summary>
        /// Normalizes the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected virtual string Normalize(int index)
        {
            if (index >= _parts.Length)
                return null;
            string data = _parts[index];
            if (string.IsNullOrWhiteSpace(data))
                return null;
            data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
            data = data.Replace("\"\"", "\"");
            data = data.Replace("\\n", Environment.NewLine);
            if (string.IsNullOrWhiteSpace(data))
                return null;
            return data;
        }

        /// <summary>
        /// Parses the int.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual int? ParseInt(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (int.TryParse(data, out int n))
                return n;

            AddError("'{0}' is not a valid integer. Please provide a valid integer value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the date.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual DateTime? ParseDate(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (DateTime.TryParseExact(data, new string[] { "yyyy-MM-dd", "MM/dd/yyyy", "M/d/yyyy", "MM/dd/yy", "M/d/yy" }, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out DateTime dt))
                return dt.Kind == DateTimeKind.Local ? dt.ToUniversalTime() : dt;

            AddError("'{0}' is not a valid date or in an incorrect format. Please provide a date for '{1}' in the format of 'YYYY-MM-DD' or 'MM/DD/YYYY'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the byte.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual byte? ParseByte(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (byte.TryParse(data, out byte b))
                return b;

            AddError("'{0}' is not a valid byte. Please provide a valid byte value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the character.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual char? ParseChar(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (char.TryParse(data, out char c))
                return c;

            AddError("'{0}' is not a valid char value (1 character alpha). Please provide a valid char value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the bool.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool? ParseBool(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (Regex.IsMatch(data, @"^[1YT]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return true;
            if (Regex.IsMatch(data, @"^[0NF]$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
                return false;

            AddError("'{0}' is not a valid Boolean value. Please provide a valid 1 character Boolean value for '{1}' of either True ('Y', '1', or 'T') or False ('N', '0', or 'F').", data, name);
            return null;
        }

        /// <summary>
        /// Parses the decimal.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual decimal? ParseDecimal(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (decimal.TryParse(data, out decimal d))
                return d;

            AddError("'{0}' is not a valid decimal. Please provide a valid decimal value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Parses the double.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual double? ParseDouble(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return null;

            if (double.TryParse(data, out double d))
                return d;

            AddError("'{0}' is not a valid floating point number. Please provide a valid floating point value for '{1}'.", data, name);
            return null;
        }

        /// <summary>
        /// Requireds the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool Required(int index, string name)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return AddError("'{0}' is a required field and was not provided, was missing, blank or did not contain any valid information.", name);
            return true;
        }

        /// <summary>
        /// Ins the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        protected virtual bool In(int index, string name, params string[] values)
        {
            string data = Normalize(index);
            if (string.IsNullOrWhiteSpace(data))
                return false;
            if (!values.Contains(data))
                return AddError("'{0}' is not a valid value for {1}. The value for {1} must be one of '{2}'.", data, name, string.Join("', '", values));
            return true;
        }

        /// <summary>
        /// Valids the state of the us.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool ValidUSState(string state, string name)
        {
            if (string.IsNullOrWhiteSpace(state))
                return false;
            if (state.Length != 2 || !Regex.IsMatch(state, @"^(A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$"))
                return AddError("'{0}' is not a valid or recognized 2 character US Postal Service State abbreviation for a 'US' state, region or territory. Please provide a valid state for '{1}'.", state, name);
            return true;
        }

        /// <summary>
        /// Valids the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected virtual bool ValidEmail(string email, string name)
        {
            if (!string.IsNullOrWhiteSpace(email) && (!email.Contains('@') || !email.Contains('.')))
                return AddError("'{0}' does not appear to be a valid email address. If providing '{1}', please provide a valid email address.", email, name);
            return true;
        }

        /// <summary>
        /// Valids the minutes per week.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected virtual bool ValidMinutesPerWeek(int? value)
        {
            if (value.HasValue)
            {
                return ValidRange("Scheduled Minutes per Week", value, 0, MaxMinutesPerWeek, "minutes");
            }
            return true;
        }

        /// <summary>
        /// Valids the range.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns></returns>
        protected virtual bool ValidRange(string name, int? value, int start, int end, string unitType = null)
        {
            if (!value.IsInRange(start, end))
            {
                return AddError("'Invalid value '{0}' for '{1}'. Valid value range is {2} through {3} {4}",
                    value,
                    name,
                    start,
                    end,
                    unitType
                );
            }
            return true;
        }

        private bool ValidateEmployementType(string employeMentType, List<EmployeeClass> employeeClassTypes)
        {
            if (employeMentType == "1" || employeMentType == "2" || employeMentType == "3")
            {
                return true;
            }
            if (employeeClassTypes.Any(e => e.Code == employeMentType))
            {
                return true;
            }
            return false;
        }

        private string GetEmployementType(string employeMentType)
        {
            if (employeMentType == "1")
            {
                return WorkType.FullTime;
            }
            if (employeMentType == "2")
            {
                return WorkType.PartTime;
            }
            if (employeMentType == "3")
            {
                return WorkType.PerDiem;
            }

            return employeMentType;

        }
        #endregion Parsing

        #region ToString

        public override string ToString()
        {
            int cols;
            switch (RecType)
            {
                case RecordType.Adjudication:
                case RecordType.Intermittent:
                    cols = 11;
                    break;
                default:
                    cols = 29;
                    break;
                case RecordType.CaseConversionEligibility:
                    cols = 65;
                    break;
                case RecordType.WorkRelated:
                    cols = 47;
                    break;
                case RecordType.WorkSchedule:
                    cols = 17;
                    break;
                case RecordType.RCFile:
                    cols = 7;
                    break;
            }

            string[] fields = new string[cols];

            fields[RecordTypeOrdinal] = Emit((char)RecType);
            fields[EmployeeNumberOrdinal] = Emit(EmployeeNumber);
            if (RecType != RecordType.WorkSchedule && RecType != RecordType.RCFile)
            {
                fields[EmployeeLastNameOrdinal] = Emit(LastName);
                fields[EmployeeFirstNameOrdinal] = Emit(FirstName);
            }
            fields[CaseNumberOrdinal] = Emit(CaseNumber);

            switch (RecType)
            {
                case RecordType.Adjudication:
                    fields[AdjudicationStartDateOrdinal] = Emit(AdjudicationStartDate);
                    fields[AdjudicationEndDateOrdinal] = Emit(AdjudicationEndDate);
                    fields[AdjudicationStatusOrdinal] = Emit((int?)AdjudicationStatus);
                    fields[MinutesUsedOrdinal] = Emit(MinutesUsed);
                    fields[PolicyCodeOrdinal] = Emit(PolicyCode);
                    fields[WorkflowOrdinal] = Emit(Workflow);
                    break;
                case RecordType.Intermittent:
                    fields[DateOfAbsenceOrdinal] = Emit(DateOfAbsence);
                    fields[MinutesApprovedOrdinal] = Emit(MinutesApproved);
                    fields[MinutesDeniedOrdinal] = Emit(MinutesDenied);
                    fields[MinutesPendingOrdinal] = Emit(MinutesPending);
                    fields[PolicyCodeOrdinal] = Emit(PolicyCode);
                    fields[WorkflowOrdinal] = Emit(Workflow);
                    break;
                case RecordType.WorkRelated:
                    fields[WC_Reportable] = Emit(WorkRelated.Reportable);
                    fields[WC_HealthcarePrivate] = Emit(WorkRelated.HealthCarePrivate);
                    fields[WC_WhereOccurred] = Emit(WorkRelated.WhereOccurred);
                    fields[WC_CaseClassification] = Emit((int?)WorkRelated.Classification);
                    fields[WC_TypeOfInjury] = Emit((int?)WorkRelated.TypeOfInjury);
                    fields[WC_DaysAwayfromWork] = Emit(WorkRelated.DaysAwayFromWork);
                    fields[WC_DaysontheJobTransferorRestriction] = Emit(WorkRelated.DaysOnJobTransferOrRestriction);
                    if (WorkRelated.Provider != null)
                    {
                        fields[WC_HealthcareProviderTypeCode] = Emit(WorkRelated.Provider.ContactTypeCode);
                        if (WorkRelated.Provider.Contact != null)
                        {
                            fields[WC_HealthcareProviderFirstName] = Emit(WorkRelated.Provider.Contact.FirstName);
                            fields[WC_HealthcareProviderLastName] = Emit(WorkRelated.Provider.Contact.LastName);
                            fields[WC_HealthcareProviderCompany] = Emit(WorkRelated.Provider.Contact.CompanyName);
                            if (WorkRelated.Provider.Contact.Address != null)
                            {
                                fields[WC_HealthcareProviderAddress1] = Emit(WorkRelated.Provider.Contact.Address.Address1);
                                fields[WC_HealthcareProviderAddress2] = Emit(WorkRelated.Provider.Contact.Address.Address2);
                                fields[WC_HealthcareProviderCity] = Emit(WorkRelated.Provider.Contact.Address.City);
                                fields[WC_HealthcareProviderState] = Emit(WorkRelated.Provider.Contact.Address.State);
                                fields[WC_HealthcareProviderCountry] = Emit(WorkRelated.Provider.Contact.Address.Country);
                                fields[WC_HealthcareProviderPostalCode] = Emit(WorkRelated.Provider.Contact.Address.PostalCode);
                            }
                        }
                    }
                    fields[WC_EmergencyRoom] = Emit(WorkRelated.EmergencyRoom);
                    fields[WC_HospitalizedOvernight] = Emit(WorkRelated.HospitalizedOvernight);
                    fields[WC_TimeEmployeeBeganWork] = Emit(WorkRelated.TimeEmployeeBeganWork);
                    fields[WC_TimeOfEvent] = Emit(WorkRelated.TimeOfEvent);
                    fields[WC_ActivityBeforeIncident] = Emit(WorkRelated.ActivityBeforeIncident);
                    fields[WC_WhatHappened] = Emit(WorkRelated.WhatHappened);
                    fields[WC_InjuryOrIllness] = Emit(WorkRelated.InjuryOrIllness);
                    fields[WC_WhatHarmedTheEmployee] = Emit(WorkRelated.WhatHarmedTheEmployee);
                    fields[WC_DateOfDeath] = Emit(WorkRelated.DateOfDeath);
                    fields[WC_Sharps] = Emit(WorkRelated.Sharps);
                    if (WorkRelated.SharpsInfo != null)
                    {
                        if (WorkRelated.SharpsInfo.InjuryLocation != null && WorkRelated.SharpsInfo.InjuryLocation.Any())
                            fields[WC_SharpsInjuryLocation] = Emit((int?)WorkRelated.SharpsInfo.InjuryLocation.First());
                        fields[WC_TypeOfSharp] = Emit(WorkRelated.SharpsInfo.TypeOfSharp);
                        fields[WC_BrandOfSharp] = Emit(WorkRelated.SharpsInfo.BrandOfSharp);
                        fields[WC_ModelOfSharp] = Emit(WorkRelated.SharpsInfo.ModelOfSharp);
                        fields[WC_BodyFluidInvolved] = Emit(WorkRelated.SharpsInfo.BodyFluidInvolved);
                        fields[WC_HaveEngineeredInjuryProtection] = Emit(WorkRelated.SharpsInfo.HaveEngineeredInjuryProtection);
                        fields[WC_WasProtectiveMechanismActivated] = Emit(WorkRelated.SharpsInfo.WasProtectiveMechanismActivated);
                        fields[WC_WhenDidtheInjuryOccur] = Emit((int?)WorkRelated.SharpsInfo.WhenDidTheInjuryOccur);
                        fields[WC_JobClassification] = Emit(WorkRelated.SharpsInfo.JobClassification);
                        fields[WC_JobClassificationOther] = Emit(WorkRelated.SharpsInfo.JobClassificationOther);
                        fields[WC_LocationandDepartment] = Emit(WorkRelated.SharpsInfo.JobClassification);
                        fields[WC_LocationandDepartmentOther] = Emit(WorkRelated.SharpsInfo.JobClassificationOther);
                        fields[WC_SharpsProcedure] = Emit(WorkRelated.SharpsInfo.Procedure);
                        fields[WC_SharpsProcedureOther] = Emit(WorkRelated.SharpsInfo.ProcedureOther);
                        fields[WC_ExposureDetail] = Emit(WorkRelated.SharpsInfo.ExposureDetail);
                    }
                    break;
                case RecordType.WorkSchedule:
                    fields[K_MinutesPerWeekOrdinal] = Emit(MinutesPerWeek);
                    fields[K_HoursWorkedIn12MonthsOrdinal] = Emit(HoursWorkedIn12Months);
                    fields[K_WorkTimeSunOrdinal] = Emit(WorkTimeSun);
                    fields[K_WorkTimeMonOrdinal] = Emit(WorkTimeMon);
                    fields[K_WorkTimeTueOrdinal] = Emit(WorkTimeTue);
                    fields[K_WorkTimeWedOrdinal] = Emit(WorkTimeWed);
                    fields[K_WorkTimeThuOrdinal] = Emit(WorkTimeThu);
                    fields[K_WorkTimeFriOrdinal] = Emit(WorkTimeFri);
                    fields[K_WorkTimeSatOrdinal] = Emit(WorkTimeSat);
                    fields[K_VariableScheduleOrdinal] = Emit(VariableSchedule);
                    fields[K_ScheduleEffectiveDateOrdinal] = Emit(ScheduleEffectiveDate);
                    fields[K_CaseNumberOrdinal] = Emit(ReasonForLeave);
                    break;
                case RecordType.RCFile:
                    fields[Z_CaseNumberOrdinal] = Emit(CaseNumber);
                    fields[Z_StartDate] = Emit(StartDate);
                    fields[Z_EndDate] = Emit(EndDate);
                    fields[Z_LeaveInterval] = Emit(LeaveInterval);
                    fields[Z_PolicyCode] = Emit(PolicyCode);
                    break;
                default:
                    fields[LeaveIntervalOrdinal] = Emit((int?)LeaveInterval);
                    fields[ReasonForLeaveOrdinal] = Emit(ReasonForLeave);
                    fields[FamilyRelationshipOrdinal] = Emit(FamilyRelationship != null ? Convert.ToInt16(FamilyRelationship.Id) : (int?)null);
                    fields[IntermittentFrequencyOrdinal] = Emit(IntermittentFrequency);
                    fields[IntermittentFrequencyUnitOrdinal] = Emit((int?)IntermittentFrequencyUnit);
                    fields[IntermittentOccurrencesOrdinal] = Emit(IntermittentOccurrences);
                    fields[IntermittentDurationOrdinal] = Emit(IntermittentDuration);
                    fields[IntermittentDurationUnitOrdinal] = Emit((int?)IntermittentDurationUnit);
                    fields[DateCaseOpenedOrdinal] = Emit(DateCaseOpened);
                    fields[ExpectedStartDateOrdinal] = Emit(ExpectedStartDate);
                    fields[ExpectedRTWDateOrdinal] = Emit(ExpectedRTWDate);
                    fields[ConfirmedRTWDateOrdinal] = Emit(ConfirmedRTWDate);
                    fields[CaseClosedDateOrdinal] = Emit(CaseClosedDate);
                    fields[CaseStatusOrdinal] = Emit((int?)CaseStatus);
                    fields[DeliveryDateOrdinal] = Emit(DeliveryDate);
                    fields[BondingStartDateOrdinal] = Emit(BondingStartDate);
                    fields[BondingEndDateOrdinal] = Emit(BondingEndDate);
                    fields[IllnessOrInjuryDateOrdinal] = Emit(IllnessOrInjuryDate);
                    fields[HospitalAdmissionDateOrdinal] = Emit(HospitalAdmissionDate);
                    fields[HospitalReleaseDateOrdinal] = Emit(HospitalReleaseDate);
                    fields[RTWReleaseDateOrdinal] = Emit(RTWReleaseDate);
                    fields[AutoPoliciesOrdinal] = Emit(AutoPolicies);
                    fields[ReducedScheduleOrdinal] = Emit(ReducedSchedule);
                    fields[CaseCreateWorkflowOrdinal] = Emit(Workflow);
                    break;
            }

            string record = string.Join(Delimiter.ToString(), fields);
            return record;
        }

        /// <summary>
        /// Emits the specified value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        protected virtual string Emit(string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                string myVal = val;
                if (myVal.Contains('\"') || myVal.Contains(Delimiter))
                {
                    myVal = myVal.Replace("\"", "\"\"");
                    myVal = string.Concat("\"", myVal, "\"");
                }
                return myVal;
            }
            return string.Empty;
        }
        /// <summary>
        /// Emits the specified value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        protected virtual string Emit(DateTime? val)
        {
            if (val.HasValue)
                return string.Format("{0:MM/dd/yyyy}", val);
            return string.Empty;
        }
        /// <summary>
        /// Emits the specified value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        protected virtual string Emit(bool? val)
        {
            if (val.HasValue)
            {
                return (val.Value ? "Y" : "N");
            }

            return string.Empty;
        }
        /// <summary>
        /// Emits the specified value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        protected virtual string Emit(object val)
        {
            switch (val)
            {
                case null:
                    return string.Empty;
                case bool _:
                    return Emit((bool)val);
                case DateTime _:
                    return Emit((DateTime)val);
            }

            return Emit(val.ToString());
        }

        #endregion

        #region Case Custom Fields
        /// <summary>
        /// Updates the custom field value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="customField">The custom field.</param>
        private void UpdateCustomFieldValue(object val, CustomField customField)
        {
            if (val == null)
                return;

            switch (customField.ValueType)
            {
                case CustomFieldValueType.SelectList:
                    string stringVal = val.ToString();
                    var item = GetMatchingCustomFieldValue(customField, stringVal, out bool wasAdded);
                    if (item == null)
                    {
                        AddError(FormatCustomFieldError(customField, stringVal));
                    }
                    else
                    {
                        customField.SelectedValue = stringVal;
                        if (wasAdded)
                        {
                            AddWarning(FormatCustomFieldError(customField, stringVal));
                        }
                    }
                    break;
                case CustomFieldValueType.UserEntered:
                default:
                    customField.SelectedValue = val;
                    break;
            }
        }

        /// <summary>
        /// Gets the matching custom field value.
        /// </summary>
        /// <param name="customField">The custom field.</param>
        /// <param name="stringVal">The string value.</param>
        /// <returns></returns>
        private ListItem GetMatchingCustomFieldValue(CustomField customField, string stringVal, out bool wasAdded)
        {
            wasAdded = false;
            ListItem matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Key) && l.Key.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));

            if (matchingItem != null)
                return matchingItem;

            matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Value) && l.Value.Equals(stringVal, StringComparison.InvariantCultureIgnoreCase)));

            if (matchingItem != null)
                return matchingItem;

            matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Key) && l.Key.StartsWith(stringVal, StringComparison.InvariantCultureIgnoreCase)));

            if (matchingItem != null)
                return matchingItem;

            matchingItem = customField.ListValues
                .FirstOrDefault(l => (!string.IsNullOrWhiteSpace(l.Value) && l.Value.StartsWith(stringVal, StringComparison.InvariantCultureIgnoreCase)));
            
            if (matchingItem == null)
            {
                matchingItem = customField.ListValues.AddFluid(new ListItem() { Key = stringVal, Value = stringVal });
                wasAdded = true;
            }
            return matchingItem;
        }

        /// <summary>
        /// Formats the custom field error.
        /// </summary>
        /// <param name="customField">The custom field.</param>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        private string FormatCustomFieldError(CustomField customField, string val)
        {
            StringBuilder fieldError = new StringBuilder();
            fieldError.AppendFormat("'{0}' is not a valid value for '{1}'. ", val, customField.Label);
            if (customField.ListValues.Count > 10)
            {
                fieldError.Append("Valid values are defined under Custom Fields.");
            }
            else
            {
                fieldError.AppendFormat("Valid values are {0}", string.Join(", ", customField.ListValues
                    .Where(l => l.Value != val && l.Key != val)
                    .Select(l => FormatListValueForError(l))));
            }

            return fieldError.ToString();
        }

        /// <summary>
        /// Formats the list value for error.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private string FormatListValueForError(ListItem item)
        {
            return string.Format("{0} ({1})", item.Key, item.Value);
        }

        /// <summary>
        /// Parses the custom field value.
        /// </summary>
        /// <param name="customField">The custom field.</param>
        /// <returns></returns>
        private object ParseCustomFieldValue(CustomField customField)
        {
            object val;
            switch (customField.DataType)
            {
                case CustomFieldType.Number:
                    val = ParseDecimal(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                case CustomFieldType.Flag:
                    val = ParseBool(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                case CustomFieldType.Date:
                    val = ParseDate(CF_CustomFieldValueOrdinal, CustomFieldValue);
                    break;
                default:
                    val = Normalize(CF_CustomFieldValueOrdinal);
                    break;
            }

            return val;
        }
        #endregion
    }
}
