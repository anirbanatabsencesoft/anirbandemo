﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Logic.Rules;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AbsenceSoft.SpectrumHealth.Conversion
{
#pragma warning disable S3881 // "IDisposable" should be implemented correctly
    public class CaseBuilder : IDisposable
#pragma warning restore S3881 // You know what, it IS implemented correctly so ^%@#! you LINTER!
    {
        /// <summary>
        /// The policy code for FMLA.
        /// </summary>
        const string POLICY_CODE_FMLA = "0076";

        #region Privates
        /// <summary>
        /// The employee container
        /// </summary>
        private EmployeeContainer Container = null;

        /// <summary>
        /// The customer.
        /// </summary>
        private Customer Customer;

        /// <summary>
        /// The employer.
        /// </summary>
        private Employer Employer;

        /// <summary>
        /// The policies.
        /// </summary>
        private List<Policy> Policies;

        /// <summary>
        /// The reasons.
        /// </summary>
        private List<AbsenceReason> Reasons;

        /// <summary>
        /// The custom fields.
        /// </summary>
        private List<CustomField> CustomFields;

        /// <summary>
        /// To dos
        /// </summary>
        private List<ToDoItem> ToDos;
        #endregion

        #region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="CaseBuilder" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="customer">The customer.</param>
        /// <param name="employer">The employer.</param>
        /// <param name="policies">The policies.</param>
        /// <param name="reasons">The reasons.</param>
        /// <param name="fields">The fields.</param>
        public CaseBuilder(
            EmployeeContainer container,
            Customer customer,
            Employer employer,
            List<Policy> policies,
            List<AbsenceReason> reasons,
            List<CustomField> fields
        )
        {
            Container = container;
            Customer = customer;
            Employer = employer;
            Policies = policies;
            Reasons = reasons;
            CustomFields = fields;
            ToDos = new List<ToDoItem>();
        }
        #endregion

        /// <summary>
        /// Does the everything.
        /// </summary>
        /// <param name="container">The container.</param>
        public void DoEverything()
        {
            if (Container == null || Container.Rows == null || !Container.Rows.Any())
                return;
            
            // Get all case rows that are not in error
            foreach (var caseGroup in Container.Rows.GroupBy(r => r.CaseNumber).OrderBy(r => Date.Min(r.Min(x => x.StartDate), r.Min(x => x.AdjudicationStartDate), r.Min(x => x.DateOfAbsence))))
            {
                // Check to see that all case rows are clean and purdy first
                if (caseGroup.Any(r => r.IsError))
                {
                    Logger.Info("Skipping Case #'{0}' because there are rows with errors related to this case and it may become unstable", caseGroup.Key);
                    continue;
                }
                // Log all the warnings, it'll be fun
                foreach (var warnRow in caseGroup.Where(r => r.IsWarning))
                {
                    foreach (var warn in warnRow.Warnings)
                    {
                        Logger.Warn(caseGroup.Key, warn);
                    }
                }

                // Get the actual case row
                var caseRow = caseGroup.FirstOrDefault(r => r.RecType == CaseRow.RecordType.Case);

                // Create the case instance, not saved until the very end
                var @case = caseRow != null ? BuildCase(caseRow) : null;
                // If something wrong happened we won't get a value back, return early if that's the case
                if (caseRow != null && @case == null)
                    continue;

                // Determine if this case already exists
                var existingCase = Case.Repository.Collection.FindOne(Case.Query.And(
                    Case.Query.EQ(c => c.CustomerId, Customer.Id),
                    Case.Query.EQ(c => c.EmployerId, Employer.Id),
                    Case.Query.EQ(c => c.CaseNumber, caseGroup.Key)
                ));
                if (existingCase != null)
                {
                    // We're going to just replace the case, hooray!
                    if (@case == null)
                    {
                        @case = existingCase;
                        var empRow = caseGroup.FirstOrDefault(r => r.RecType == CaseRow.RecordType.CaseConversionEligibility);
                        if (empRow != null)
                        {
                            @case.Employee = BuildEmployee(caseGroup.Key);
                        }
                    }
                    else
                    {
                        @case.Id = existingCase.Id;
                    }
                    @case.IsDeleted = false;
                }
                if (@case == null)
                {
                    Logger.Error("Case #'{0}': Case was not found and no 'C' record exists to create it in the file", caseGroup.Key);
                    continue;
                }
                try
                {
                    // Set the values of the case and employee custom fields as provided
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.CustomField)
                        && !SetCustomFields(@case, caseGroup))
                    {
                        continue;
                    }
                    // Set the WC/OSHA work related information into the case when applicable
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.WorkRelated)
                        && !SetWorkRelatedInfo(@case, caseGroup))
                    {
                        continue;
                    }
                    // Build the leave schedule and change from consecutive to reduced if appropriate
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.Case || r.RecType == CaseRow.RecordType.Adjudication || r.RecType == CaseRow.RecordType.Intermittent || r.RecType == CaseRow.RecordType.RCFile)
                        && !BuildSegments(@case))
                    {
                        continue;
                    }
                    // Build the applied policies for the case with our nifty version of add manual policy
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.Adjudication || r.RecType == CaseRow.RecordType.Intermittent)
                        && !BuildPolicies(@case))
                    {
                        continue;
                    }
                    // Build all of the intermittent time off requests with our other nifty version of that method
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.Intermittent || (r.RecType == CaseRow.RecordType.Adjudication && @case.Segments.Any(s => s.Type == CaseType.Intermittent && r.AdjudicationStartDate.Value.DateInRange(s.StartDate, s.EndDate))))
                        && !BuildIntermittentTime(@case, caseGroup))
                    {
                        continue;
                    }
                    // Mark the entire case as approved and calculates the case building all of the approved usage
                    if (caseGroup.Any(r => r.RecType == CaseRow.RecordType.Case || r.RecType == CaseRow.RecordType.Adjudication || r.RecType == CaseRow.RecordType.Intermittent)
                        && !BuildUsage(@case))
                    {
                        continue;
                    }

                    // Determine if we need to update the case itself.
                    var updateCase = caseGroup.Any(r =>
                           r.RecType == CaseRow.RecordType.Case
                        || r.RecType == CaseRow.RecordType.Adjudication
                        || r.RecType == CaseRow.RecordType.Intermittent
                        || r.RecType == CaseRow.RecordType.CustomField
                        || r.RecType == CaseRow.RecordType.CaseConversionEligibility
                        || r.RecType == CaseRow.RecordType.WorkRelated
                        || r.RecType == CaseRow.RecordType.RCFile);

                    // Finally! Let's save this mutha %^$#@'er
                    if (updateCase)
                    {
                        using (var svc = new CaseService(false)
                        {
                            CurrentCustomer = Customer,
                            CurrentEmployer = Employer,
                            CurrentUser = User.System
                        })
                        {
                            using (var empSvc = new EmployeeService())
                            {
                                empSvc.AreSchedulesValid(@case.Employee.WorkSchedules);
                            }

                            // Run calcs
                            svc.GetProjectedUsage(@case);

                            // Map policy crosswalk codes
                            svc.CalculatePolicyCrosswalkCodes(@case);

                            // Now save the case and update everything
                            svc.UpdateCase(@case, @case.IsNew ? CaseEventType.CaseCreated : default(CaseEventType?));
                        }
                    }

                    // Set any notes
                    SaveNotes(@case, caseGroup);

                    // Save any todos that we may have queued up for this thing
                    ToDos.Where(t => t.CaseNumber == @case.CaseNumber).ForEach(t =>
                    {
                        t.CaseId = @case.Id;
                        t.AssignedToId = @case.AssignedToId;
                        t.AssignedToName = @case.AssignedToName;
                        t.Save();
                    });
                }
                catch (Exception ex)
                {
                    caseRow?.AddError(ex.ToString());
                    Logger.Error(ex, caseRow?.CaseNumber ?? @case?.CaseNumber ?? caseGroup.Key);
                }
            }
        }

        #region Implementation

        /// <summary>
        /// Saves the notes.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="rows">The rows.</param>
        private void SaveNotes(Case @case, IGrouping<string, CaseRow> rows)
        {
            var caseNoteBulk = CaseNote.Repository.Collection.InitializeOrderedBulkOperation();

            // Remove any case notes so we can replace them all
            caseNoteBulk.Find(Query.And(CaseNote.Query.EQ(n => n.CustomerId, @case.CustomerId),
                CaseNote.Query.EQ(n => n.EmployerId, @case.EmployerId),
                CaseNote.Query.EQ(n => n.CaseId, @case.Id))).Remove();

            // Build discovery note for FMLA eligibility criteria
            var phw = @case.Employee.PriorHours.FirstOrDefault(p => p.Metadata.GetRawValue<string>("CaseNumber") == @case.CaseNumber);
            var discoveryNote = string.Format("Case Created: {0}{1}{2}{3}{4}",
                (@case.GetCaseEventDate(CaseEventType.CaseCreated) ?? @case.CreatedDate).ToUIString(),
                phw == null ? "" : "\nPrior Hours Worked: ",
                phw?.HoursWorked,
                phw == null ? "" : " as of ",
                phw?.AsOf.ToUIString());
            caseNoteBulk.Insert(new CaseNote()
            {
                CustomerId = @case.CustomerId,
                EmployerId = @case.EmployerId,
                CaseId = @case.Id,
#pragma warning disable CS0618 // Type or member is obsolete
                Category = NoteCategoryEnum.CaseSummary,
#pragma warning restore CS0618 // Type or member is obsolete
                EnteredByName = User.System.DisplayName,
                Public = false,
                Notes = discoveryNote,
                CreatedById = User.DefaultUserId,
                ModifiedById = User.DefaultUserId
            }.SetCreatedDate(@case.GetCaseEventDate(CaseEventType.CaseCreated) ?? @case.CreatedDate));

            // Create all other case notes
            foreach (var noteRow in rows.Where(r => r.RecType == CaseRow.RecordType.Note))
            {
                caseNoteBulk.Insert(new CaseNote()
                {
                    CustomerId = @case.CustomerId,
                    EmployerId = @case.EmployerId,
                    CaseId = @case.Id,
#pragma warning disable CS0618 // Type or member is obsolete
                    Category = (NoteCategoryEnum)(noteRow.CaseNoteCategory ?? 0),
#pragma warning restore CS0618 // Type or member is obsolete
                    EnteredByName = noteRow.NoteEnteredByName,
                    EnteredByEmployeeNumber = noteRow.NoteEnteredByEmployeeNumber,
                    Public = false,
                    Notes = noteRow.NoteText,
                    CreatedById = User.DefaultUserId,
                    ModifiedById = User.DefaultUserId
                }.SetCreatedDate(noteRow.NoteDate ?? DateTime.UtcNow));
            }

            caseNoteBulk.Execute();
        }

        /// <summary>
        /// Adds to do to be saved when the case is saved.
        /// </summary>
        /// <param name="case">The @case.</param>
        /// <param name="title">The title.</param>
        /// <param name="type">The type.</param>
        private void AddToDo(Case @case, string title, ToDoItemType type = ToDoItemType.CaseReview)
        {
            ToDos.Add(new ToDoItem()
            {
                CaseNumber = @case.CaseNumber,
                Title = string.Concat("Conversion: ", title),
                DueDate = DateTime.UtcNow.AddDays(1),
                CustomerId = @case.CustomerId,
                EmployerId = @case.EmployerId,
                EmployeeId = @case.Employee.Id,
                EmployeeNumber = @case.Employee.EmployeeNumber,
                EmployeeName = @case.Employee.FullName,
                EmployerName = Employer.Name,
                ItemType = type,
                Status = ToDoItemStatus.Pending,
                Priority = ToDoItemPriority.High,
                Optional = true
            });
        }

        /// <summary>
        /// Sets the custom fields. Does not save anything.
        /// </summary>
        /// <param name="case">The case.</param>
        private bool SetCustomFields(Case @case, IGrouping<string, CaseRow> caseGroup)
        {
            bool allGood = true;
            var fieldRows = caseGroup.Where(r => r.RecType == CaseRow.RecordType.CustomField).Where(r => !r.IsError).ToList();
            foreach (var row in fieldRows)
            {
                var cf = row.CustomField;
                if (cf == null)
                {
                    row.AddError("Custom field with code '{0}' could not be found or is not configured", row.CustomFieldCode);
                    allGood = false;
                    continue;
                }
                if (row.EntityTargetValue.HasFlag(EntityTarget.Employee))
                {
                    int objIndex = @case.Employee.CustomFields.FindIndex(f => f.Code == cf.Code);
                    if (objIndex != -1)
                    {
                        @case.Employee.CustomFields[objIndex] = cf;
                    }
                    else
                    {
                        @case.Employee.CustomFields.Add(cf);
                    }
                }
                if (row.EntityTargetValue.HasFlag(EntityTarget.Case))
                {
                    int objIndex = @case.CustomFields.FindIndex(f => f.Code == cf.Code);
                    if (objIndex != -1)
                    {
                        @case.CustomFields[objIndex] = cf;
                    }
                    else
                    {
                        @case.CustomFields.Add(cf);
                    }
                }
            }
            return allGood;
        }

        /// <summary>
        /// Builds the policies. Does not save anything.
        /// </summary>
        /// <param name="case">The @case.</param>
        private bool BuildPolicies(Case @case)
        {
            if (@case.Segments.All(s => s.Type == CaseType.Administrative))
                return true;
            var distinctPolicyCodes = Container.Rows
                .Where(r => r.CaseNumber == @case.CaseNumber)
                .Where(r => r.RecType == CaseRow.RecordType.Adjudication || r.RecType == CaseRow.RecordType.Intermittent || r.RecType == CaseRow.RecordType.RCFile)
                .Where(r => !string.IsNullOrWhiteSpace(r.PolicyCode))
                .Where(r => !r.IsError)
                .Select(r => r.PolicyCode)
                .Distinct()
                .ToList();
            foreach (var policyCode in distinctPolicyCodes)
                if (!AddManualPolicy(@case, policyCode))
                    return false;
            if (!distinctPolicyCodes.Any())
            {
                // FMLA
                var fmla = Policies.FirstOrDefault(p => p.Code == POLICY_CODE_FMLA && p.AbsenceReasons.Any(r => r.ReasonId == @case.Reason?.Id));
                if (fmla != null)
                {
                    if (AddManualPolicy(@case, fmla.Code, true))
                        AddToDo(@case, "Automatically added FMLA to case");
                }
                else
                {
                    AddToDo(@case, "No policies and no prior usage recorded");
                }
            }

            // Now calculate any policy crosswalk codes for these policies.
            CalculatePolicyCrosswalkCodes(@case);

            return true;
        }

        /// <summary>
        /// Adds a manual policy to the each case segment. Does NOT persist changes, returns
        /// ready for calcs on the newly added policy, if any.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="policyCode">The policy code.</param>
        /// <param name="force">if set to <c>true</c> then this method force-feeds the policy down each segment's throat
        /// whether it likes it or not.</param>
        /// <returns></returns>
        private bool AddManualPolicy(Case @case, string policyCode, bool force = false)
        {
            // If the policy already exists, just bail, no need to re-add it, and not really exception material here.
            if (@case.Segments.SelectMany(s => s.AppliedPolicies.Select(p => p.Policy.Code)).Any(p => p == policyCode))
                return true;

            Policy manualPolicy = Policies?.FirstOrDefault(p => p.Code == policyCode);
            if (manualPolicy == null)
            {
                Logger.Error("Case #'{0}': the policy code of '{1}' does not exist or is not available", @case.CaseNumber, policyCode);
                return false;
            }

            // add the policy to the applied policy group to get ready for eligibility
            string absenceReasonCode = @case.Reason.Code;
            foreach (CaseSegment segment in @case.Segments.Where(s => s.Type != CaseType.Administrative))
            {
                var dateQuery = Container.Rows
                    .Where(r => r.CaseNumber == @case.CaseNumber)
                    .Where(r => r.RecType == CaseRow.RecordType.RCFile && r.PolicyCode == manualPolicy.Code && r.StartDate.HasValue)
                    .Where(r => r.StartDate.Value.DateRangesOverLap(r.EndDate, segment.StartDate, segment.EndDate))
                    .SelectMany(r => r.StartDate.Value.AllDatesInRange(r.EndDate ?? r.StartDate.Value))
                    .Union(Container.Rows
                    .Where(r => r.CaseNumber == @case.CaseNumber)
                    .Where(r => r.RecType == CaseRow.RecordType.Adjudication && r.PolicyCode == manualPolicy.Code && r.AdjudicationStartDate.HasValue)
                    .Where(r => r.AdjudicationStartDate.Value.DateInRange(segment.StartDate, segment.EndDate))
                    .Select(r => r.AdjudicationStartDate.Value))
                    .Union(Container.Rows
                    .Where(r => r.CaseNumber == @case.CaseNumber)
                    .Where(r => r.RecType == CaseRow.RecordType.Intermittent && (r.PolicyCode == manualPolicy.Code || string.IsNullOrWhiteSpace(r.PolicyCode)) && r.DateOfAbsence.HasValue)
                    .Where(r => r.DateOfAbsence.Value.DateInRange(segment.StartDate, segment.EndDate))
                    .Select(r => r.DateOfAbsence.Value)).ToList();

                if (!force && !dateQuery.Any())
                {
                    // Look buddy, we're not "force-feeding" policies here and there's no "A", "I" or "Z" record  for this thing during
                    //  these dates, so move along and keep to yourself, nobody wants your policy sass on this segment
                    continue;
                }

                if (!dateQuery.Any())
                {
                    dateQuery.Add(segment.StartDate);
                    dateQuery.Add(segment.EndDate.Value);
                }

                PolicyAbsenceReason reason = manualPolicy.AbsenceReasons?.FirstOrDefault(a => a.Reason?.Code == absenceReasonCode);
                if (reason == null)
                {
                    reason = manualPolicy.AbsenceReasons?.FirstOrDefault(a => a.CaseTypes.HasFlag(segment.Type));
                    if (reason == null)
                        reason = manualPolicy.AbsenceReasons?.FirstOrDefault();
                    if (reason == null)
                    {
                        Logger.Warn("Case #'{0}': Policy '{2}' has absolutely no absence reasons configured and I can't infer anything for the reason '{1}",
                            @case.CaseNumber, absenceReasonCode, policyCode);
                        AddToDo(@case, string.Format("Unable to add policy '{0}' to case", policyCode));
                        return true;
                    }
                    Logger.Warn(@case.CaseNumber, "The absence reason '{0}' does not have a mapping for the policy '{1}'. Auto-adding it by copying from the reason '{2}' instead.", absenceReasonCode, policyCode, reason.Reason.Code);
                    reason = reason.Clone();
                    reason.Id = Guid.NewGuid();
                    reason.Reason = @case.Reason;
                    reason.ReasonId = @case.Reason.Id;
                }

                AppliedPolicy aPolicy = new AppliedPolicy()
                {
                    // Stupid reverse reverse date reversal logic for reversing... because complexity = yay!
                    //  In other words, we need to only set the policy start date to cover exactly what it's supposed to, BUT NOT before the
                    //  the actual segment's start date, so we take both "start" dates and then the last one we can find to ensure we box
                    //  the policy dates correctly against the segment dates
                    StartDate = new[] { dateQuery.Min(), segment.StartDate }.Max(),
                    // Same crap here with the end date, just opposite; trust me, it makes sense in my head... it's scary in my head
                    EndDate = new[] { dateQuery.Max(), segment.EndDate }.Min(),
                    Policy = manualPolicy,
                    PolicyReason = reason,
                    Status = EligibilityStatus.Eligible,
                    ManuallyAdded = true,
                    ManuallyAddedNote = "Case Conversion"
                };
                if (aPolicy.PolicyReason != null)
                    aPolicy.PolicyReason.PeriodType = aPolicy.PolicyReason.PeriodType ?? @case.Employer.FMLPeriodType;
                segment.AppliedPolicies.Add(aPolicy);

                // Manually run eligibility rules for this policy and store them, unflip the manually added flag
                if (aPolicy.Policy.Code == POLICY_CODE_FMLA)
                {
                    aPolicy.ManuallyAdded = false;
                    aPolicy.ManuallyAddedNote = null;
                    aPolicy.RuleGroups = GetAppliedRuleGroups(aPolicy.Policy.RuleGroups.Union(aPolicy.PolicyReason.RuleGroups).ToList());

                    LeaveOfAbsence loa = new LeaveOfAbsence()
                    {
                        Case = @case,
                        Employee = @case.Employee,
                        ActiveSegment = segment,
                        Customer = Customer,
                        Employer = Employer,
                        WorkSchedule = @case.Employee.WorkSchedules
                    };

                    EvaluateRuleGroups(loa, aPolicy.RuleGroups, PolicyRuleGroupType.Eligibility);
                    // Just ensure this stays eligibile, even if they're not, this is conversion, not us doing it.
                    aPolicy.Status = EligibilityStatus.Eligible;
                }
            }
            return true;
        }

        private void CalculatePolicyCrosswalkCodes(Case @case)
        {
            LeaveOfAbsence loa = new LeaveOfAbsence()
            {
                Employee = @case.Employee,
                Case = @case,
                Employer = @case.Employer,
                Customer = @case.Customer
            };

            foreach (var segment in @case.Segments)
            {
                foreach (var policy in segment.AppliedPolicies)
                {
                    policy.PolicyCrosswalkCodes = null;
                    var thePolicy = policy.Policy;
                    List<PolicyCrosswalkCode> codesToApply = new List<PolicyCrosswalkCode>();

                    EvaluateAndSetPolicyCrosswalkCodes(loa, thePolicy.RuleGroups.Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk).ToList(), codesToApply);

                    var policyAbsenceReason = thePolicy.AbsenceReasons.FirstOrDefault(ar => ar.ReasonId == @case.Reason.Id);
                    if (policyAbsenceReason != null)
                    {
                        EvaluateAndSetPolicyCrosswalkCodes(loa, policyAbsenceReason.RuleGroups.Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk).ToList(), codesToApply);
                    }

                    policy.PolicyCrosswalkCodes = codesToApply;
                }
            }
        }

        private List<PolicyCrosswalkType> GetTypesThatMayApplyToCase(Case myCase)
        {
            var policyCrosswalkTypeCodes = myCase.Segments
                .SelectMany(s => s.AppliedPolicies)
                .Select(ap => ap.Policy)
                .SelectMany(p => p.RuleGroups)
                .Where(rg => rg.RuleGroupType == PolicyRuleGroupType.Crosswalk)
                .Select(rg => rg.CrosswalkTypeCode).ToList();

            var query = PolicyCrosswalkType.Query.In(pct => pct.Code, policyCrosswalkTypeCodes);

            return PolicyCrosswalkType.DistinctFind(query, myCase.CustomerId, myCase.EmployerId).ToList();
        }

        private void EvaluateAndSetPolicyCrosswalkCodes(LeaveOfAbsence loa, List<PolicyRuleGroup> ruleGroupsToEvaluate, List<PolicyCrosswalkCode> codesToApply)
        {
            using (var ruleService = new RuleService<LeaveOfAbsence>())
            {
                var policyCrosswalkTypes = GetTypesThatMayApplyToCase(loa.Case);

                var passedRuleGroups = ruleGroupsToEvaluate.Where(rg => ruleService.EvaluateRuleGroup(rg, loa));
                if (passedRuleGroups.Any())
                {
                    foreach (var ruleGroup in passedRuleGroups)
                    {
                        var matchingType = policyCrosswalkTypes.FirstOrDefault(pct => pct.Code == ruleGroup.CrosswalkTypeCode);

                        if (matchingType != null)
                        {
                            var existingCode = codesToApply.FirstOrDefault(r => r.TypeCode == matchingType.Code);
                            if (existingCode != null)
                            {
                                existingCode.Code = ruleGroup.CrosswalkCode;
                            }
                            else
                            {
                                codesToApply.Add(new PolicyCrosswalkCode(matchingType.Code, matchingType.Name, ruleGroup.CrosswalkCode));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the intermittent time. Does not save anything.
        /// </summary>
        /// <param name="case">The @case.</param>
        private bool BuildIntermittentTime(Case @case, IGrouping<string, CaseRow> caseGroup)
        {
            if (!@case.Segments.Any(s => s.Type == CaseType.Intermittent))
                return true;

            var intRows = caseGroup
                .Where(r => r.RecType == CaseRow.RecordType.Intermittent)
                .Where(r => r.CaseNumber == @case.CaseNumber)
                .Where(r => !r.IsError)
                .Where(r => !r.HasMinutesWarning) // ignore these, they suck
                .Where(r => r.DateOfAbsence.HasValue)
                .GroupBy(r => r.DateOfAbsence.Value.ToMidnight())
                .Select(g => new IntermittentTimeRequest()
                {
                    RequestDate = g.Key,
                    Notes = "Conversion - I",
                    EmployeeEntered = false,
                    Detail = g.Select(r => new IntermittentTimeRequestDetail()
                    {
                        Approved = r.MinutesApproved ?? 0,
                        Denied = r.MinutesDenied ?? 0,
                        Pending = r.MinutesPending ?? 0,
                        PolicyCode = r.PolicyCode,
                        DenialReasonCode = (r.MinutesDenied ?? 0) > 0 ? AdjudicationDenialReason.Other : null,
                        DenialReasonName = (r.MinutesDenied ?? 0) > 0 ? AdjudicationDenialReason.OtherDescription : null,
                        DeniedReasonOther = (r.MinutesDenied ?? 0) > 0 ? "Conversion" : null,
                        TotalMinutes = (r.MinutesApproved ?? 0) + (r.MinutesDenied ?? 0) + (r.MinutesPending ?? 0)
                    }).ToList(),
                    TotalMinutes = g.Any() ? g.Max(d => d.MinutesApproved + d.MinutesPending + d.MinutesDenied) ?? 0 : 0
                })
                .Union(caseGroup
                .Where(r => r.RecType == CaseRow.RecordType.Adjudication)
                .Where(r => r.CaseNumber == @case.CaseNumber)
                .Where(r => !r.IsError)
                .Where(r => !r.HasMinutesWarning) // ignore these, they suck
                .Where(r => r.AdjudicationStartDate.HasValue)
                // Ensure it's within an intermittent segment
                .Where(r => @case.Segments.Any(s => s.Type == CaseType.Intermittent && r.AdjudicationStartDate.Value.DateInRange(s.StartDate, s.EndDate)))
                .GroupBy(r => r.AdjudicationStartDate.Value.ToMidnight())
                .Select(g => new IntermittentTimeRequest()
                {
                    RequestDate = g.Key,
                    Notes = "Conversion - A",
                    EmployeeEntered = false,
                    Detail = g.Select(r => new IntermittentTimeRequestDetail()
                    {
                        Approved = r.MinutesUsed ?? 0,
                        Denied = 0,
                        Pending = 0,
                        PolicyCode = r.PolicyCode,
                        DenialReasonCode = null,
                        DenialReasonName = null,
                        DeniedReasonOther = null,
                        TotalMinutes = r.MinutesUsed ?? 0
                    }).ToList(),
                    TotalMinutes = g.Any() ? g.Max(d => d.MinutesApproved + d.MinutesPending + d.MinutesDenied) ?? 0 : 0
                }))
                .OrderBy(r => r.Notes)
                .Distinct(r => r.RequestDate)
                .OrderBy(r => r.RequestDate)
                .ToList();

            if (!intRows.Any())
            {
                if (@case.Status == CaseStatus.Closed)
                    AddToDo(@case, "Intermittent closed case has no recorded time off");
                return true;
            }

            var policies = @case.Segments
                .Where(s => s.Type == CaseType.Intermittent)
                .SelectMany(s => s.AppliedPolicies)
                .Select(p => p.Policy.Code)
                .Distinct();

            foreach (var req in intRows)
            {
                var aRecs = caseGroup.Where(r => r.RecType == CaseRow.RecordType.Adjudication && req.RequestDate.DateInRange(r.AdjudicationStartDate.Value, r.AdjudicationEndDate)).ToList();
                // Ensures we don't add a TOR for a policy that doesn't exist on that specific date from the "A" record, which would be bad 'n' stuff
                //  Use the Intersect extension to only get where policy codes match for the A records on the file
                foreach (var code in policies)
                {
                    var adj = aRecs.FirstOrDefault(r => r.PolicyCode == code);
                    var hasIRecord = req.Detail.Any(r => r.PolicyCode == code);
                    var detail = req.Detail.FirstOrDefault(r => r.PolicyCode == code || string.IsNullOrWhiteSpace(r.PolicyCode)) ?? req.Detail.AddFluid(new IntermittentTimeRequestDetail());
                    detail.PolicyCode = code;
                    detail.TotalMinutes = new int[] { req.Detail.Max(d => d.TotalMinutes), aRecs.Any() ? aRecs.Max(d => d.MinutesUsed) ?? 0 : 0 }.Max();
                    detail.Approved = adj?.MinutesUsed ?? 0;
                    // Don't think Spectrum will ever send Pending or Denied time, so zero here.
                    detail.Pending = 0;
                    detail.Denied = 0;
                    if (!hasIRecord)
                    {
                        detail.Denied = detail.TotalMinutes - detail.Approved;
                        if (detail.Denied < 0)
                        {
                            // Well, this is odd, how did this happen, who knows, but correct it to zero
                            detail.Denied = 0;
                        }
                        // We also have to handle the scenario where Prognos is sending a Reduced schedule leave
                        //  that we're converting to Intermittent (no I record) on the same date for a different
                        //  policy, and in that conversion we need to take the max time for that day and explicitly deny
                        //  the time not being used by the intermittent time
                        // Only set the denied reason if it's greater than zero
                        if (detail.Denied > 0)
                        {
                            detail.DenialReasonCode = AdjudicationDenialReason.IntermittentNonAllowed;
                            detail.DenialReasonName = AdjudicationDenialReason.IntermittentNonAllowedDescription;
                        }
                    }

                    // If there is trailing time marked as approved from the "A" record, then the "I" record is
                    //  a hack from Prognos that actually is a split determination for that single day where the
                    //  employee exhausted time mid-day and has to be recorded as intermittent.
                    if (hasIRecord && detail.TotalMinutes > detail.Approved && detail.Denied == 0)
                    {
                        detail.Denied = detail.TotalMinutes - detail.Approved;
                        detail.DenialReasonCode = AdjudicationDenialReason.Exhausted;
                        detail.DenialReasonName = AdjudicationDenialReason.ExhaustedDescription;
                        detail.DeniedReasonOther = null;
                    }
                }
            }

            return CreateOrModifyTimeOffRequest(@case, intRows);
        }

        /// <summary>
        /// Set the user override info on a time off request
        /// </summary>
        /// <param name="case">The case.</param>
        /// <param name="timeRequested">The time requested.</param>
        /// <exception cref="AbsenceSoftException">
        /// There are no active intermittent case periods for this case
        /// or
        /// or
        /// </exception>
        public bool CreateOrModifyTimeOffRequest(Case @case, List<IntermittentTimeRequest> timeRequested)
        {
            // See if there's anything to do
            if (timeRequested?.Any() != true)
                return true;

            // Get all intermittent case segments (even cancelled ones)
            var intSegments = @case.Segments.Where(s => s.Type == CaseType.Intermittent && s.IsEligibleOrPending);
            // If there are no intermittent segments, that's bad, shouldn't allow an intermittent request for time off at that point, no sir.
            if (!intSegments.Any() && timeRequested.Any(t => t.Detail.Any(d => d.Approved > 0)))
            {
                Logger.Error("Case #'{0}': There are no active intermittent case periods for this case but intermittent time requested", @case.CaseNumber);
                return false;
            }

            // verify all of the info before setting anything
            bool allgood = true;
            foreach (IntermittentTimeRequest requestDate in timeRequested)
            {
                // Stage the total minutes for our intermittent request date based on the max entered time for all policies.
                requestDate.TotalMinutes = requestDate.Detail.Any() ? requestDate.Detail.Max(d => d.Approved + d.Pending + d.Denied) : 0;

                // Get the applicable segment, since they can't overlap, not active ones anyway, then we need to find the one that
                //  is appropriate for our request date.
                CaseSegment seg = intSegments.FirstOrDefault(s => requestDate.RequestDate.DateInRange(s.StartDate, s.EndDate));

                // Uh oh, there isn't one that covers our requested date, bail out and raise some red flags.
                if (seg == null)
                {
                    allgood = false;
                    Logger.Error("Case #'{0}': The requested date ({1:MM-dd-yyyy}) does not fall within any intermittent period for this case", @case.CaseNumber, requestDate.RequestDate);
                    continue;
                }

                // see if this date is already used, and if so remove it then add in the new request
                seg.UserRequests.RemoveAll(a => a.RequestDate == requestDate.RequestDate && (!a.IntermittentType.HasValue || a.IntermittentType == requestDate.IntermittentType));

                // Only add this back in if the user actually has some time in there.
                if (requestDate.TotalMinutes > 0)
                    seg.UserRequests.Add(requestDate);
            }
            return allgood;
        }

        /// <summary>
        /// Builds the consecutive and intermittent usage. Does not save anything.
        /// </summary>
        /// <param name="case">The @case.</param>
        private bool BuildUsage(Case @case)
        {
            if (@case.Segments.All(s => s.Type == CaseType.Administrative))
            {
                return true;
            }

            // Get the scheduled times to build the usage from
            var empTimes = MaterializeSchedule(@case.StartDate, @case.EndDate.Value, BuildSchedule(@case.CaseNumber));
            AppliedPolicyUsageHelper helper = new AppliedPolicyUsageHelper(empTimes, @case.Employee);

            var isDenied = new Func<DateTime, bool>(d =>
            {
                return Container.Rows.Any(r => r.CaseNumber == @case.CaseNumber
                    && r.RecType == CaseRow.RecordType.Adjudication
                    && r.AdjudicationStartDate.HasValue
                    && (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Denied
                    && d.DateInRange(r.AdjudicationStartDate.Value, r.AdjudicationEndDate));
            });

            foreach (var segment in @case.Segments.Where(s => s.Type != CaseType.Administrative).OrderBy(s => s.StartDate))
            {
                segment.Absences = Container.Rows.Where(r => r.CaseNumber == @case.CaseNumber
                    && r.RecType == CaseRow.RecordType.Adjudication
                    && r.AdjudicationStartDate.HasValue
                    && r.MinutesUsed.HasValue
                    && (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Approved
                    && r.AdjudicationStartDate.Value.DateInRange(segment.StartDate, segment.EndDate))
                    .GroupBy(g => g.AdjudicationStartDate.Value)
                    .Select(r => new Time()
                    {
                        SampleDate = r.Key,
                        TotalMinutes = r.Max(x => x.MinutesUsed)
                    }).ToList();

                foreach (var ap in segment.AppliedPolicies.Where(p => p != null && p.PolicyReason != null))
                {
                    AdjudicationStatus lastStatus = AdjudicationStatus.Pending;
                    if (segment.Type == CaseType.Intermittent)
                    {
                        ap.Usage = ap.StartDate.AllDatesInRange(ap.EndDate.Value).OrderBy(d => d).Select(iDate =>
                        {
                            var totalMinutesScheduled = empTimes.FirstOrDefault(t => t.SampleDate == iDate)?.TotalMinutes ?? 0;
                            var tor = segment.UserRequests.FirstOrDefault(r => r.RequestDate == iDate);
                            if (tor == null)
                            {
                                return new AppliedPolicyUsage()
                                {
                                    DateUsed = iDate,
                                    MinutesInDay = totalMinutesScheduled,
                                    MinutesUsed = 0,
                                    UserEntered = true,
                                    PolicyReasonId = ap.PolicyReason.ReasonId,
                                    Determination = isDenied(iDate) ? AdjudicationStatus.Denied : AdjudicationStatus.Approved,
                                    // Ooooh, whether or not to mark this as locked... I think it needs to be if the case is closed
                                    IsLocked = @case.Status == CaseStatus.Closed,
                                    IntermittentDetermination = segment.Type == CaseType.Intermittent ? (isDenied(iDate) ? IntermittentStatus.Denied : IntermittentStatus.Allowed) : default(IntermittentStatus?),
                                    AvailableToUse = helper.GetUsage(iDate)
                                };
                            }

                            // How much did they say the employee was taking this day in history?
                            var passedMinutesRows = Container.Rows.Where(r =>
                                r.CaseNumber == @case.CaseNumber
                                && r.RecType == CaseRow.RecordType.Adjudication
                                && r.MinutesUsed.HasValue
                                && r.AdjudicationStartDate.HasValue
                                && tor.RequestDate.DateInRange(r.AdjudicationStartDate.Value, r.AdjudicationEndDate)
                                && r.PolicyCode == ap.Policy.Code);
                            var passedMinutes = !passedMinutesRows.Any(r => (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Approved)
                                ? null
                                : passedMinutesRows.Where(r => (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Approved).Max(x => x?.MinutesUsed);

                            // How much intermittent time did they approve for this day in history?
                            var intermittentMinutes = tor
                                ?.Detail
                                ?.FirstOrDefault(d => d.PolicyCode == ap.Policy.Code)
                                ?.Approved;

                            // Build the usage record based on these values
                            var det = passedMinutesRows.FirstOrDefault(r => r.AdjudicationStatus.HasValue)?.AdjudicationStatus ?? AdjudicationStatus.Approved;
                            var usage = new AppliedPolicyUsage()
                            {
                                DateUsed = tor.RequestDate,
                                MinutesInDay = totalMinutesScheduled,
                                MinutesUsed = intermittentMinutes ?? passedMinutes ?? 0,
                                UserEntered = true,
                                PolicyReasonId = ap.PolicyReason.ReasonId,
                                Determination = det,
                                // Ooooh, whether or not to mark this as locked... I think it needs to be if the case is closed
                                IsLocked = @case.Status == CaseStatus.Closed,
                                IntermittentDetermination = segment.Type == CaseType.Intermittent
                                    ? (det == AdjudicationStatus.Approved ? IntermittentStatus.Allowed : (IntermittentStatus)(int)det)
                                    : default(IntermittentStatus?),
                                AvailableToUse = helper.GetUsage(tor.RequestDate)
                            };

                            // Track our last status.
                            lastStatus = usage.Determination;

                            // Return the usage we want to select/map here.
                            return usage;
                        })
                        .ToList();

                        // No need to continue on this segment, it's intermittent.
                        continue;
                    }

                    var policyZRows = Container.Rows
                        .Where(r => r.RecType == CaseRow.RecordType.RCFile
                            && r.CaseNumber == @case.CaseNumber
                            && r.LeaveInterval != CaseType.Intermittent
                            && r.LeaveInterval != CaseType.Administrative
                            && r.PolicyCode == ap.Policy.Code
                            && r.StartDate.Value.DateRangesOverLap(r.EndDate, segment.StartDate, segment.EndDate))
                        .ToList();

                    ap.Usage = policyZRows.SelectMany(zRow =>
                    {
                        List<AppliedPolicyUsage> usages = new List<AppliedPolicyUsage>();
                        foreach (var iDate in zRow.StartDate.Value.AllDatesInRange(zRow.EndDate ?? zRow.StartDate.Value)
                            .Where(z => z.DateInRange(ap.StartDate, ap.EndDate))
                            .OrderBy(d => d))
                        {
                            var totalMinutesScheduled = empTimes.FirstOrDefault(t => t.SampleDate == iDate)?.TotalMinutes ?? 0;

                            // How much did they say the employee was taking this day in history?
                            var passedMinutesRows = Container.Rows.Where(r =>
                                r.CaseNumber == @case.CaseNumber
                                && r.RecType == CaseRow.RecordType.Adjudication
                                && r.MinutesUsed.HasValue
                                && r.AdjudicationStartDate.HasValue
                                && iDate.DateInRange(r.AdjudicationStartDate.Value, r.AdjudicationEndDate)
                                && r.PolicyCode == ap.Policy.Code);
                            var passedMinutes = !passedMinutesRows.Any(r => (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Approved) 
                                ? null 
                                : passedMinutesRows.Where(r => (r.AdjudicationStatus ?? AdjudicationStatus.Approved) == AdjudicationStatus.Approved).Max(x => x?.MinutesUsed);

                            // How much intermittent time did they approve for this day in history?
                            var intermittentMinutes = segment.UserRequests
                                .FirstOrDefault(r => r.RequestDate == iDate)
                                ?.Detail
                                ?.FirstOrDefault(d => d.PolicyCode == ap.Policy.Code)
                                ?.Approved;

                            // Build the usage record based on these values
                            var det = passedMinutesRows.FirstOrDefault(r => r.AdjudicationStatus.HasValue)?.AdjudicationStatus ?? AdjudicationStatus.Approved;
                            var usage = new AppliedPolicyUsage()
                            {
                                DateUsed = iDate,
                                MinutesInDay = totalMinutesScheduled,
                                MinutesUsed = intermittentMinutes ?? passedMinutes ?? 0,
                                UserEntered = true,
                                PolicyReasonId = ap.PolicyReason.ReasonId,
                                Determination = det,
                                // Ooooh, whether or not to mark this as locked... I think it needs to be if the case is closed
                                IsLocked = @case.Status == CaseStatus.Closed,
                                IntermittentDetermination = segment.Type == CaseType.Intermittent 
                                    ? (det == AdjudicationStatus.Approved ? IntermittentStatus.Allowed : (IntermittentStatus)(int)det) 
                                    : default(IntermittentStatus?),
                                AvailableToUse = helper.GetUsage(iDate)
                            };

                            // Track our last status.
                            lastStatus = usage.Determination;

                            // Return the usage we want to select/map here.
                            usages.Add(usage);
                        }
                        return usages;
                    }).ToList();
                }
            }

            try
            {
                using (var svc = new CaseService())
                {
                    svc.CalcCaseSummary(@case);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, @case.CaseNumber, "Error building prior usage records threw exception");
            }
            return true;
        }

        /// <summary>
        /// Sets the work related information. Does not save anything.
        /// </summary>
        /// <param name="case">The case.</param>
        private bool SetWorkRelatedInfo(Case @case, IGrouping<string, CaseRow> caseGroup)
        {
            var workRow = caseGroup.FirstOrDefault(r => r.RecType == CaseRow.RecordType.WorkRelated && !r.IsError);
            if (workRow == null || workRow.WorkRelated == null)
                return true;
            if (workRow.WorkRelated != null)
                @case.Metadata.SetRawValue("IsWorkRelated", true);

            // Determine if we need to set the illness or injury date
            if (workRow.WorkRelated.IllnessOrInjuryDate.HasValue)
            {
                @case.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, workRow.WorkRelated.IllnessOrInjuryDate.Value);
            }
            else if (@case.GetCaseEventDate(CaseEventType.IllnessOrInjuryDate).HasValue)
            {
                workRow.WorkRelated.IllnessOrInjuryDate = @case.GetCaseEventDate(CaseEventType.IllnessOrInjuryDate);
            }

            // Determine if we need to save the provider contact
            if (!string.IsNullOrWhiteSpace((workRow.WorkRelated.Provider?.ContactTypeCode)))
            {
                workRow.WorkRelated.Provider.CustomerId = @case.CustomerId;
                workRow.WorkRelated.Provider.EmployerId = @case.EmployerId;
                workRow.WorkRelated.Provider.EmployeeId = @case.Employee.Id;
                workRow.WorkRelated.Provider.EmployeeNumber = @case.Employee.EmployeeNumber;
                if (string.IsNullOrWhiteSpace(workRow.WorkRelated.Provider.ContactTypeName))
                {
                    ContactType type = ContactType.GetByCode(workRow.WorkRelated.Provider.ContactTypeCode, @case.CustomerId, @case.EmployerId, true);
                    if (type != null)
                        workRow.WorkRelated.Provider.ContactTypeName = type.Name;
                }
                workRow.WorkRelated.Provider.Save();
            }

            // Update the injury location Organization if it doesn't already exist. Adds a little overhead, but that's okay at this point.
            if (!string.IsNullOrWhiteSpace(workRow.WorkRelated.InjuryLocation))
            {
                UpdateOrganization(workRow.WorkRelated.InjuryLocation);
                // Ensure it's set by the "Code" properly
                workRow.WorkRelated.InjuryLocation = workRow.WorkRelated.InjuryLocation.ToUpperInvariant();
            }

            // assign the Work related info
            @case.WorkRelated = workRow.WorkRelated;
            return true;
        }

        private void UpdateOrganization(string officeLocation)
        {
            string code = officeLocation.ToUpperInvariant();
            var update = Organization.Updates
                .SetOnInsert(o => o.CustomerId, Customer.Id)
                .SetOnInsert(o => o.EmployerId, Employer.Id)
                .SetOnInsert(o => o.Code, code)
                .SetOnInsert(o => o.Path, HierarchyPath.Root(code))
                .SetOnInsert(o => o.TypeCode, OrganizationType.OfficeLocationTypeCode)
                .SetOnInsert(o => o.TypeName, "Office Location")
                .SetOnInsert(o => o.Name, officeLocation)
                .SetOnInsert(o => o.Address.Id, Guid.NewGuid())
                .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                .SetOnInsert(o => o.ModifiedById, User.DefaultUserId)
                .SetOnInsert(o => o.ModifiedDate, DateTime.UtcNow);

            Organization.Update(Organization.Query.And(
                Organization.Query.EQ(o => o.CustomerId, Customer.Id),
                Organization.Query.EQ(o => o.EmployerId, Employer.Id),
                Organization.Query.EQ(o => o.Code, code)
            ), update, UpdateFlags.Multi | UpdateFlags.Upsert);
        }

        /// <summary>
        /// Calculates the case end date.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="case">The @case.</param>
        /// <returns></returns>
        private DateTime? CalculateCaseEndDate(CaseRow row, Case @case)
        {
            var rtwDate = row.ConfirmedRTWDate ?? row.CaseClosedDate ?? row.ExpectedRTWDate;

            // Do we need to self-correct the case end date? Probably.
            bool manuallyFigureOutEndDate = false;
            if (!rtwDate.HasValue && row.LeaveInterval != CaseType.Administrative)
            {
                // Need to warn them about this!
                Logger.Warn(row.CaseNumber, "The case has a leave interval of '{0}' but no end date specified.", row.LeaveInterval.ToString().SplitCamelCaseString());
                manuallyFigureOutEndDate = true;
            }
            if (row.ExpectedStartDate.Value > rtwDate)
            {
                // Need to warn them about this!
                Logger.Warn(row.CaseNumber, "The RTW/closed date of '{0:MM/dd/yyyy}' comes before the start date of '{1:MM/dd/yyyy}'.", rtwDate, @case.StartDate);
                manuallyFigureOutEndDate = true;
            }
            List<DateTime?> possibleEndDates = new List<DateTime?>
            {
                Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Intermittent).Max(r => r.DateOfAbsence),
                Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Adjudication).Max(r => r.AdjudicationStartDate),
                Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Adjudication).Max(r => r.AdjudicationEndDate),
                Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.RCFile).Max(r => r.EndDate),
                Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.RCFile).Max(r => r.StartDate)
            };
            if (!manuallyFigureOutEndDate && rtwDate < possibleEndDates.Max())
            {
                Logger.Warn(row.CaseNumber, "The RTW/closed date of '{0:MM/dd/yyyy}' comes before the last date of time lost on '{1:MM/dd/yyyy}'.", rtwDate, possibleEndDates.Max());
                manuallyFigureOutEndDate = true;
            }
            if (manuallyFigureOutEndDate)
            {
                possibleEndDates.Add(row.ConfirmedRTWDate);
                possibleEndDates.Add(row.ExpectedRTWDate);
                possibleEndDates.Add(row.ExpectedStartDate);
                var newEndDate = possibleEndDates.Max();
                AddToDo(@case, string.Format("The case end date overridden from '{0:MM/dd/yyyy}' to '{1:MM/dd/yyyy}'", rtwDate, newEndDate));
            }

            return possibleEndDates.Max() ?? rtwDate ?? @case.StartDate;
        }

        /// <summary>
        /// Builds the case. Does not save anything.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        private Case BuildCase(CaseRow row)
        {
            try
            {
                Employee emp = BuildEmployee(row.CaseNumber);
                if (Container.Employee == null)
                    Container.Employee = emp;

                if (emp == null)
                    return null;

                Case @case = new Case()
                {
                    Customer = Customer,
                    CustomerId = Customer.Id,
                    Employer = Employer,
                    EmployerId = Employer.Id,
                    Employee = emp,
                    CaseNumber = row.CaseNumber,
                    EmployerCaseNumber = row.CaseNumber,
                    Description = "Migrated to AbsenceTracker",
                    StartDate = row.ExpectedStartDate.Value,
                    Status = row.CaseStatus ?? CaseStatus.Open,
                    CustomFields = CustomFields.Where(f => f.Target.HasFlag(EntityTarget.Case)).Select(f => f.Clone()).ToList()
                };

                @case.EndDate = CalculateCaseEndDate(row, @case);

                // HACK: Set this value in the metadata... it's probably going to change based on usage/time lost A/I records
                //  but we need this back out of the timkeeping feed later on, so stuff it in metadata.
                @case.Metadata.SetRawValue("SHOriginalStartDate", row.ExpectedStartDate);

                // Do we need to self-correct the case start date? Really!?!.
                List<DateTime?> possibleStartDates = new List<DateTime?>
                {
                    Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Intermittent).Min(r => r.DateOfAbsence),
                    Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Adjudication).Min(r => r.AdjudicationStartDate),
                    Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.Adjudication).Min(r => r.AdjudicationEndDate),
                    Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.RCFile).Min(r => r.StartDate),
                    Container.Rows.Where(r => r.CaseNumber == row.CaseNumber).Where(r => r.RecType == CaseRow.RecordType.RCFile).Min(r => r.EndDate)
                };
                if (@case.StartDate > possibleStartDates.Min())
                {
                    Logger.Warn(row.CaseNumber, "The case start date of '{0:MM/dd/yyyy}' comes after the earliest date of time lost on '{1:MM/dd/yyyy}'.", @case.StartDate, possibleStartDates.Min());
                    possibleStartDates.Add(row.ExpectedStartDate);
                    var newStartDate = possibleStartDates.Min() ?? @case.StartDate;
                    AddToDo(@case, string.Format("The case end date overridden from '{0:MM/dd/yyyy}' to '{1:MM/dd/yyyy}'", @case.StartDate, newStartDate));
                    @case.StartDate = newStartDate;
                }

                var minuteError = Container.Rows
                    .FirstOrDefault(r => r.CaseNumber == @case.CaseNumber && r.IsWarning && r.HasMinutesWarning);
                if (minuteError != null)
                {
                    // Create a minutes 
                    AddToDo(@case, string.Format("{0:N0} minutes used on {1:MM/dd/yyyy} is over 1,440 (24 hours)",
                        minuteError.AdjudicationStartDate,
                        minuteError.MinutesUsed
                    ));
                }

                @case.SetCreatedDate(row.DateCaseOpened ?? DateTime.UtcNow);

                // Date Opened
                @case.SetCaseEvent(CaseEventType.CaseCreated, row.DateCaseOpened ?? DateTime.UtcNow.ToMidnight());

                // Absence Reason
                AbsenceReason reason = Reasons.FirstOrDefault(r => r.Code == row.ReasonForLeave);
                if (reason == null)
                {
                    Container.Errors.Add(string.Format("The Reason '{0}' for case # '{1}' was not found, is invalid or is not yet configured for the employer.", row.ReasonForLeave, row.CaseNumber));
                    Logger.Error("The Reason '{0}' for case # '{1}' was not found, is invalid or is not yet configured for the employer.", row.ReasonForLeave, row.CaseNumber);
                    return null;
                }
                @case.Reason = reason;

                // if this is an accommodation case then set the flag                        
                @case.IsAccommodation = reason.Flags.HasValue && reason.Flags.Value.HasFlag(AbsenceReasonFlag.ShowAccommodation);

                // Family Relationship / Case Contact
                if (row.FamilyRelationship != null)
                {
                    @case.Contact = new EmployeeContact()
                    {
                        Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                        Employee = emp,
                        EmployeeId = emp.Id,
                        EmployeeNumber = emp.EmployeeNumber,
                        EmployerId = emp.EmployerId,
                        CustomerId = emp.CustomerId,
                        CreatedById = User.DefaultUserId,
                        ModifiedById = User.DefaultUserId,
                        // Military status cheat hack
                        MilitaryStatus = reason.Category == "Military" ? MilitaryStatus.ActiveDuty : MilitaryStatus.Civilian,
                        ContactTypeCode = row.FamilyRelationship.Code,
                        ContactTypeName = row.FamilyRelationship.Name,
                        Contact = new Contact()
                        {
                            FirstName = string.Concat(emp.FirstName, emp.FirstName.EndsWith("s") ? "'" : "'s"),
                            LastName = row.FamilyRelationship.Name
                        }
                    }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow);
                }

                // Intermittent Certification/Schedule
                if (row.LeaveInterval == CaseType.Intermittent && (row.IntermittentFrequency.HasValue || row.IntermittentOccurrences.HasValue || row.IntermittentDuration.HasValue))
                {
                    Certification cert = @case.Certifications.AddFluid(new Certification());
                    cert.StartDate = @case.StartDate;
                    cert.EndDate = @case.EndDate ?? DateTime.MaxValue;
                    cert.Frequency = row.IntermittentFrequency ?? 0;
                    cert.FrequencyType = row.IntermittentFrequencyUnit ?? Unit.Days;
                    cert.FrequencyUnitType = DayUnitType.Workday;
                    cert.Occurances = row.IntermittentOccurrences ?? 0;
                    cert.Duration = row.IntermittentDuration ?? 0;
                    cert.DurationType = row.IntermittentDurationUnit ?? Unit.Hours;
                }

                // Date Closed
                if (row.CaseClosedDate.HasValue || @case.Status == CaseStatus.Closed || @case.Status == CaseStatus.Cancelled || row.ConfirmedRTWDate.HasValue)
                {
                    if (@case.Status == CaseStatus.Cancelled)
                    {
                        @case.ClosureReason = CaseClosureReason.LeaveCancelled;
                        @case.CancelReason = CaseCancelReason.Other;
                        @case.SetCaseEvent(CaseEventType.CaseCancelled, row.CaseClosedDate ?? DateTime.UtcNow.ToMidnight());
                    }
                    else
                        @case.ClosureReason = row.ConfirmedRTWDate.HasValue ? CaseClosureReason.ReturnToWork : CaseClosureReason.Other;
                    @case.SetCaseEvent(CaseEventType.CaseClosed, row.CaseClosedDate ?? row.ConfirmedRTWDate ?? DateTime.UtcNow.ToMidnight());
                }

                // Event Dates
                if (row.ConfirmedRTWDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.ReturnToWork, row.ConfirmedRTWDate.Value);
                if (row.DeliveryDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.DeliveryDate, row.DeliveryDate.Value);
                if (row.BondingStartDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.BondingStartDate, row.BondingStartDate.Value);
                if (row.BondingEndDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.BondingEndDate, row.BondingEndDate.Value);
                if (row.IllnessOrInjuryDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.IllnessOrInjuryDate, row.IllnessOrInjuryDate.Value);
                if (row.HospitalAdmissionDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.HospitalAdmissionDate, row.HospitalAdmissionDate.Value);
                if (row.HospitalReleaseDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.HospitalReleaseDate, row.HospitalReleaseDate.Value);
                if (row.RTWReleaseDate.HasValue)
                    @case.SetCaseEvent(CaseEventType.ReleaseReceived, row.RTWReleaseDate.Value);

                // Case Migration Stuff
                @case.Metadata.SetRawValue("CaseMigrationDate", DateTime.UtcNow);
                var myFileName = Environment.GetCommandLineArgs().SkipWhile(s => s != "-f").Skip(1).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(myFileName))
                    @case.Metadata.SetRawValue("CaseMigrationFile", Path.GetFileName(myFileName));

                return @case;
            }
            catch (Exception ex)
            {
                row.AddError(ex.Message);
                Logger.Error(ex, row.CaseNumber, "Failed to create initial case with exception");
                return null;
            }
        }

        /// <summary>
        /// Builds the employee from any "E" rows unless the employee is already there.
        /// </summary>
        private Employee BuildEmployee(string caseNumber)
        {
            // Get the employee for this case
            var empRow = Container.Rows.FirstOrDefault(r => r.RecType == CaseRow.RecordType.CaseConversionEligibility && r.CaseNumber == caseNumber);
            // Get any employee rows from the list as we may need them for other "stuff" later on.
            var allEmployeeRows = Container.Rows.Where(r => r.RecType == CaseRow.RecordType.CaseConversionEligibility).ToList();
            // No dice? okay then get any freaking one, doesn't matter we just need an employee
            if (empRow == null)
                empRow = allEmployeeRows.FirstOrDefault();
            // Seriously, WTF, okay, just return the container's employee (existing) and if it's null let the caller deal with it
            if (empRow == null)
                return Container.Employee;

            // Build OR merge the employee
            Employee emp = Container.Employee ?? new Employee()
            {
                CustomerId = Customer.Id,
                Customer = Customer,
                EmployerId = Employer.Id,
                Employer = Employer,
                EmployerName = Employer.Name,
                EmployeeNumber = Container.EmployeeNumber
            };
            emp.IsDeleted = false;
            emp.Info = emp.Info ?? new EmployeeInfo();
            emp.Info.Address = emp.Info.Address ?? new Address();
            emp.ModifiedById = User.DefaultUserId;
            emp.SetModifiedDate(DateTime.UtcNow);
            emp.LastName = empRow.LastName;
            emp.FirstName = empRow.FirstName;
            emp.MiddleName = empRow.MiddleName;
            emp.JobTitle = empRow.JobTitle;
            emp.WorkState = empRow.WorkState;
            emp.WorkCountry = empRow.WorkCountry ?? "US";
            emp.Info.HomePhone = empRow.PhoneHome;
            emp.Info.WorkPhone = empRow.PhoneWork;
            emp.Info.CellPhone = empRow.PhoneMobile;
            emp.Info.AltPhone = empRow.PhoneAlt;
            emp.Info.Email = empRow.Email;
            emp.Info.AltEmail = empRow.EmailAlt;
            emp.Info.Address.Address1 = empRow.Address;
            emp.Info.Address.Address2 = empRow.Address2;
            emp.Info.Address.City = empRow.City;
            emp.Info.Address.State = empRow.State;
            emp.Info.Address.PostalCode = empRow.PostalCode;
            emp.Info.Address.Country = empRow.Country;
            emp.EmployeeClassCode = empRow.EmploymentType;
            emp.DoB = empRow.DateOfBirth;
            emp.Gender = (Gender?)empRow.Gender;
            emp.IsExempt = empRow.ExemptionStatus == 'E';
            emp.Meets50In75MileRule = empRow.Meets50In75 ?? false;
            emp.IsKeyEmployee = empRow.KeyEmployee ?? false;
            emp.MilitaryStatus = (MilitaryStatus)(empRow.MilitaryStatus ?? 0);
            emp.TerminationDate = empRow.TerminationDate;
            emp.Salary = (double?)empRow.PayRate;
            emp.PayType = (PayType?)empRow.PayType;
            emp.HireDate = empRow.HireDate;
            emp.RehireDate = empRow.RehireDate;
            emp.ServiceDate = empRow.AdjustedServiceDate;
            if (!string.IsNullOrWhiteSpace(empRow.SSN))
            {
                emp.Ssn = new CryptoString(empRow.SSN).Hash().Encrypt();
            }
            if (empRow.JobClassification.HasValue)
            {
                emp.JobActivity = (JobClassification)empRow.JobClassification.Value;
            }
            // Ignore pay schedules, Spectrum doesn't use them
            emp.StartDayOfWeek = empRow.StartDayOfWeek;
            emp.Status = (EmploymentStatus)(empRow.EmploymentStatus ?? EmploymentStatusValue.Active);

            // Build Prior Hours Worked across all rows and existing ones
            emp.PriorHours = emp.PriorHours ?? new List<PriorHours>();
            foreach (var row in allEmployeeRows.Where(r => r.HoursWorkedIn12Months.HasValue))
            {
                emp.PriorHours.RemoveAll(p => p.AsOf == (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() && p.Metadata.GetRawValue<string>("CaseNumber") == row.CaseNumber);
                var ph = emp.PriorHours.AddFluid(new PriorHours() { HoursWorked = (double)row.HoursWorkedIn12Months.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                ph.Metadata.SetRawValue("CaseNumber", row.CaseNumber);
            }

            // Build Minutes Worked per Week across all rows and existing ones
            emp.MinutesWorkedPerWeek = emp.MinutesWorkedPerWeek ?? new List<MinutesWorkedPerWeek>();
            foreach (var row in allEmployeeRows.Where(r => r.AverageMinutesWorkedPerWeek.HasValue))
            {
                emp.MinutesWorkedPerWeek.RemoveAll(p => p.AsOf == (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() && p.Metadata.GetRawValue<string>("CaseNumber") == row.CaseNumber);
                var mw = emp.MinutesWorkedPerWeek.AddFluid(new MinutesWorkedPerWeek() { MinutesWorked = row.AverageMinutesWorkedPerWeek.Value, AsOf = (row.ScheduleEffectiveDate ?? DateTime.UtcNow).ToMidnight() });
                mw.Metadata.SetRawValue("CaseNumber", row.CaseNumber);
            }

            emp.WorkSchedules = BuildSchedule() ?? new List<Schedule>(0);
            LevelWorkSchedules(emp);
            SetCustomFields(emp, caseNumber);

            if (!string.IsNullOrWhiteSpace(empRow.JobLocation))
            {
                string code = empRow.JobLocation.ToUpperInvariant();
                emp.Info.OfficeLocation = code;
                Organization.Repository.Collection.Update(Organization.Query.And(
                    Organization.Query.EQ(o => o.CustomerId, emp.CustomerId),
                    Organization.Query.EQ(o => o.EmployerId, emp.EmployerId),
                    Organization.Query.EQ(o => o.Code, code)),
                    Organization.Updates
                        .SetOnInsert(o => o.CustomerId, emp.CustomerId)
                        .SetOnInsert(o => o.EmployerId, emp.EmployerId)
                        .SetOnInsert(o => o.Code, code)
                        .SetOnInsert(o => o.Name, empRow.JobLocation)
                        .SetOnInsert(o => o.Path, HierarchyPath.Root(code))
                        .SetOnInsert(o => o.TypeCode, OrganizationType.OfficeLocationTypeCode)
                        .SetOnInsert(o => o.TypeName, "Office Location")
                        .SetOnInsert(o => o.Address.Id, Guid.NewGuid())
                        .SetOnInsert(o => o.CreatedById, User.DefaultUserId)
                        .SetOnInsert(o => o.ModifiedById, User.DefaultUserId)
                        .SetOnInsert(o => o.CreatedDate, DateTime.UtcNow)
                        .CurrentDate(o => o.ModifiedDate)
                        .Set(o => o.IsDeleted, false),
                        UpdateFlags.Upsert);

                var empOrg = EmployeeOrganization.Repository.Collection.FindOne(EmployeeOrganization.Query.And(
                    EmployeeOrganization.Query.EQ(eo => eo.CustomerId, emp.CustomerId),
                    EmployeeOrganization.Query.EQ(eo => eo.EmployerId, emp.EmployerId),
                    EmployeeOrganization.Query.EQ(eo => eo.EmployeeNumber, emp.EmployeeNumber),
                    EmployeeOrganization.Query.EQ(eo => eo.Code, code)
                ));
                if (empOrg == null)
                {
                    new EmployeeOrganization()
                    {
                        CustomerId = emp.CustomerId,
                        EmployerId = emp.EmployerId,
                        EmployeeNumber = emp.EmployeeNumber,
                        Code = code,
                        TypeCode = OrganizationType.OfficeLocationTypeCode,
                        Name = empRow.JobLocation,
                        Path = HierarchyPath.Root(code)
                    }.Save();
                }
                else if (empOrg.IsDeleted)
                {
                    empOrg.IsDeleted = false;
                    empOrg.Save();
                }
            }

            // Always persist the employee changes to the database
            bool isNew = emp.IsNew;
            emp.IsDeleted = false;
            try
            {
                // Update/save the employee
                // Set IsExempt to true if IsKeyEmployee is set to true. This does NOT apply to the converse situation
                if (emp.IsKeyEmployee && !emp.IsExempt)
                    emp.IsExempt = true;

                // we've made it this far we can save (the spouse method may save contacts)
                emp.Save();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, caseNumber, string.Format("Creating employee '{0}' failed with the following error(s)", emp.EmployeeNumber));
                // Set the employee to null so we don't proceed any further.
                emp = null;
            }

            // Create any employee contacts if necessary
            empRow = Container.Rows.FirstOrDefault(r => r.RecType == CaseRow.RecordType.CaseConversionEligibility && r.CaseNumber == caseNumber);
            if (empRow != null)
            {
                var bulkContact = EmployeeContact.Repository.Collection.InitializeUnorderedBulkOperation();
                var bulkCount = 0x0;

                if (!string.IsNullOrWhiteSpace(empRow.ManagerEmployeeNumber) ||
                    !string.IsNullOrWhiteSpace(empRow.ManagerLastName) ||
                    !string.IsNullOrWhiteSpace(empRow.ManagerFirstName) ||
                    !string.IsNullOrWhiteSpace(empRow.ManagerPhone) ||
                    !string.IsNullOrWhiteSpace(empRow.ManagerEmail))
                {
                    #region Supervisor
                    try
                    {
                        // Only try to match up the Supervisor/Manager IF no name is passed in
                        if (!string.IsNullOrWhiteSpace(empRow.ManagerEmployeeNumber) && string.IsNullOrWhiteSpace(empRow.ManagerLastName) && string.IsNullOrWhiteSpace(empRow.ManagerFirstName))
                        {
                            empRow.ManagerLastName = empRow.ManagerLastName ?? empRow.ManagerEmployeeNumber;
                            empRow.ManagerFirstName = empRow.ManagerFirstName ?? "Supervisor";
                        }

                        if (isNew)
                        {
                            bulkContact.Insert(new EmployeeContact()
                            {
                                CustomerId = emp.CustomerId,
                                EmployerId = emp.EmployerId,
                                EmployeeId = emp.Id,
                                EmployeeNumber = emp.EmployeeNumber,
                                MilitaryStatus = MilitaryStatus.Civilian,
                                CreatedById = User.DefaultUserId,
                                ModifiedById = User.DefaultUserId,
                                ContactTypeCode = "SUPERVISOR",
                                ContactTypeName = "Supervisor",
                                Contact = new Contact()
                                {
                                    LastName = empRow.ManagerLastName,
                                    FirstName = empRow.ManagerFirstName,
                                    WorkPhone = empRow.ManagerPhone,
                                    Email = empRow.ManagerEmail?.ToLowerInvariant()
                                },
                                RelatedEmployeeNumber = empRow.ManagerEmployeeNumber
                            }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                            bulkCount++;
                        }
                        else
                        {
                            bulkContact.Find(EmployeeContact.Query.And(
                                EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                EmployeeContact.Query.EQ(e => e.ContactTypeCode, "SUPERVISOR")
                            ))
                            .Upsert()
                            .UpdateOne(EmployeeContact.Updates
                                .SetOnInsert(c => c.ModifiedById, User.DefaultUserId)
                                .SetOnInsert(c => c.ModifiedDate, DateTime.UtcNow)
                                .SetOnInsert(c => c.RelatedEmployeeNumber, empRow.ManagerEmployeeNumber)
                                .SetOnInsert(c => c.Contact.LastName, empRow.ManagerLastName)
                                .SetOnInsert(c => c.Contact.FirstName, empRow.ManagerFirstName)
                                .SetOnInsert(c => c.Contact.WorkPhone, empRow.ManagerPhone)
                                .SetOnInsert(c => c.Contact.Email, empRow.ManagerEmail?.ToLowerInvariant())
                                .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                .SetOnInsert(c => c.EmployeeId, emp.Id)
                                .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                .SetOnInsert(c => c.CreatedById, User.DefaultUserId)
                                .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                .SetOnInsert(c => c.ContactTypeCode, "SUPERVISOR")
                                .SetOnInsert(c => c.ContactTypeName, "Supervisor")
                                .SetOnInsert(c => c.MilitaryStatus, MilitaryStatus.Civilian)
                                .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                .SetOnInsert(c => c.Contact.Address.Country, "US")
                            );
                            bulkCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error processing Manager/Supervisor row", ex);
                    }

                    #endregion Supervisor
                }

                if (!string.IsNullOrWhiteSpace(empRow.HREmployeeNumber) ||
                        !string.IsNullOrWhiteSpace(empRow.HRLastName) ||
                        !string.IsNullOrWhiteSpace(empRow.HRFirstName) ||
                        !string.IsNullOrWhiteSpace(empRow.HRPhone) ||
                        !string.IsNullOrWhiteSpace(empRow.HREmail))
                {
                    #region HR
                    try
                    {
                        // Only try to match up the HR peep IF no name is passed in
                        if (!string.IsNullOrWhiteSpace(empRow.HREmployeeNumber) && string.IsNullOrWhiteSpace(empRow.HRLastName) && string.IsNullOrWhiteSpace(empRow.HRLastName))
                        {
                            empRow.HRLastName = empRow.HRLastName ?? empRow.HREmployeeNumber;
                            empRow.HRFirstName = empRow.HRFirstName ?? "HR";
                        }

                        if (isNew)
                        {
                            bulkContact.Insert(new EmployeeContact()
                            {
                                CustomerId = emp.CustomerId,
                                EmployerId = emp.EmployerId,
                                EmployeeId = emp.Id,
                                EmployeeNumber = emp.EmployeeNumber,
                                MilitaryStatus = Data.Enums.MilitaryStatus.Civilian,
                                CreatedById = User.DefaultUserId,
                                ModifiedById = User.DefaultUserId,
                                ContactTypeCode = "HR",
                                ContactTypeName = "HR",
                                Contact = new Contact()
                                {
                                    LastName = empRow.HRLastName,
                                    FirstName = empRow.HRFirstName,
                                    WorkPhone = empRow.HRPhone,
                                    Email = empRow.HREmail?.ToLowerInvariant()
                                },
                                RelatedEmployeeNumber = empRow.HREmployeeNumber
                            }.SetCreatedDate(DateTime.UtcNow).SetModifiedDate(DateTime.UtcNow));
                            bulkCount++;
                        }
                        else
                        {
                            bulkContact.Find(EmployeeContact.Query.And(
                                EmployeeContact.Query.EQ(e => e.CustomerId, emp.CustomerId),
                                EmployeeContact.Query.EQ(e => e.EmployerId, emp.EmployerId),
                                EmployeeContact.Query.EQ(e => e.EmployeeId, emp.Id),
                                EmployeeContact.Query.EQ(e => e.ContactTypeCode, "HR")
                            ))
                            .Upsert()
                            .UpdateOne(EmployeeContact.Updates
                                .SetOnInsert(c => c.ModifiedById, User.DefaultUserId)
                                .SetOnInsert(c => c.ModifiedDate, DateTime.UtcNow)
                                .SetOnInsert(c => c.RelatedEmployeeNumber, empRow.HREmployeeNumber)
                                .SetOnInsert(c => c.Contact.LastName, empRow.HRLastName)
                                .SetOnInsert(c => c.Contact.FirstName, empRow.HRFirstName)
                                .SetOnInsert(c => c.Contact.WorkPhone, empRow.HRPhone)
                                .SetOnInsert(c => c.Contact.Email, empRow.HREmail?.ToLowerInvariant())
                                .SetOnInsert(c => c.CustomerId, emp.CustomerId)
                                .SetOnInsert(c => c.EmployerId, emp.EmployerId)
                                .SetOnInsert(c => c.EmployeeId, emp.Id)
                                .SetOnInsert(c => c.EmployeeNumber, emp.EmployeeNumber)
                                .SetOnInsert(c => c.CreatedById, User.DefaultUserId)
                                .SetOnInsert(c => c.CreatedDate, DateTime.UtcNow)
                                .SetOnInsert(c => c.ContactTypeCode, "HR")
                                .SetOnInsert(c => c.ContactTypeName, "HR")
                                .SetOnInsert(c => c.MilitaryStatus, AbsenceSoft.Data.Enums.MilitaryStatus.Civilian)
                                .SetOnInsert(c => c.Contact.Id, Guid.NewGuid())
                                .SetOnInsert(c => c.Contact.Address.Id, Guid.NewGuid())
                                .SetOnInsert(c => c.Contact.Address.Country, "US")
                            );
                            bulkCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error processing HR row", ex);
                    }
                    #endregion HR
                }

                if (bulkCount > 0)
                {
                    try
                    {
                        bulkContact.Execute();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, caseNumber, string.Format("Creating employee '{0}' administrative contacts", emp.EmployeeNumber));
                    }
                }
            }

            return emp;
        }

        private void LevelWorkSchedules(Employee emp)
        {
            Schedule last = null;
            foreach (var sched in emp.WorkSchedules.OrderBy(s => s.StartDate))
            {
                if (last != null && last.EndDate?.ToMidnight() != sched.StartDate.AddDays(-1).ToMidnight())
                {
                    last.EndDate = sched.StartDate.AddDays(-1).ToMidnight();
                }
                if (sched.StartDate <= sched.EndDate)
                    last = sched;
            }
            emp.WorkSchedules.RemoveAll(s => s.StartDate > s.EndDate);
            emp.WorkSchedules = emp.WorkSchedules.GroupBy(x => x.StartDate).Select(g => g.First()).ToList();
        }

        /// <summary>
        /// Sets the custom fields. Does not save anything.
        /// </summary>
        /// <param name="case">The case.</param>
        private void SetCustomFields(Employee emp, string caseNumber)
        {
            var fieldRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.CustomField)
                .Where(r => r.EntityTargetValue.HasFlag(EntityTarget.Employee))
                .Where(r => !r.IsError)
                .Where(r => r.CaseNumber == caseNumber)
                .ToList();
            foreach (var row in fieldRows)
            {
                var cf = row.CustomField;
                if (cf == null)
                {
                    row.AddError("Custom field with code '{0}' could not be found or is not configured", row.CustomFieldCode);
                    continue;
                }
                int objIndex = emp.CustomFields.FindIndex(f => f.Code == cf.Code);
                if (objIndex != -1)
                {
                    emp.CustomFields[objIndex] = cf;
                }
                else
                {
                    emp.CustomFields.Add(cf);
                }
            }
        }

        /// <summary>
        /// Inserts the segment into case segments and properly trims up and cleans up
        /// the other segments around this new one.
        /// </summary>
        /// <param name="case">The @case.</param>
        /// <param name="segment">The segment.</param>
        private void InsertOrUpdateCaseSegment(Case @case, CaseSegment segment)
        {
            // check to see if they are replacing the entire segment with a different type. If this happens then there are
            // differnt rules for the update
            CaseSegment csChangeOnly = @case.Segments.FirstOrDefault(s => s.StartDate == segment.StartDate && s.EndDate == segment.EndDate);
            if (csChangeOnly != null && csChangeOnly.Type != segment.Type)
            {
                csChangeOnly.Type = segment.Type;
                csChangeOnly.LeaveSchedule = segment.LeaveSchedule;
                csChangeOnly.UserRequests?.Clear();
                csChangeOnly.Absences?.Clear();
                csChangeOnly.AppliedPolicies?.ForEach(p => p?.Usage?.Clear());
                return;
            }

            // make sure the segments are sorted by start date
            @case.Segments = @case.Segments.OrderBy(o => o.StartDate).ToList();

            // find the segment that this applies too, that will be the first one where these dates
            // overlap and the case type matches
            CaseSegment theChangedOne = @case.Segments.FirstOrDefault(s => s.Type == segment.Type
                // Notice here we're going to subtract a day from the segment's start date
                //  This targets where we're not only overlapping, but "continuing" a contiguous segment
                //  of time in our scehdule calculations. This is especially important as for reduced
                //  schedule leaves we'll never have overlapping leave schedules and segments are built
                //  schedule by schedule so we'll want to "continue" an existing segment in this instance
                //  and simply add the new leave schedule.
                && s.StartDate.DateRangesOverLap(s.EndDate, segment.StartDate.AddDays(-1), segment.EndDate));

            // yeah! we found one, change the start and end dates on it to match
            if (theChangedOne != null)
            {
                theChangedOne.StartDate = new[] { segment.StartDate, theChangedOne.StartDate }.Min();
                theChangedOne.EndDate = new[] { segment.EndDate, theChangedOne.EndDate }.Max();
                theChangedOne.LeaveSchedule = theChangedOne.LeaveSchedule ?? new List<Schedule>(1);
                theChangedOne.LeaveSchedule.AddRange(segment.LeaveSchedule);
            }

            // if we didn't find an overlapping segment, then we need to do something else with it
            if (theChangedOne == null)
            {
                // first see if the start and end dates match some other case type, if so
                // change the type
                bool changedType = false;
                foreach (CaseSegment cs in @case.Segments)
                {
                    if (cs.StartDate == segment.StartDate && cs.EndDate == segment.EndDate)
                    {
                        cs.Type = segment.Type;
                        cs.LeaveSchedule = cs.LeaveSchedule ?? new List<Schedule>(1);
                        cs.LeaveSchedule.AddRange(segment.LeaveSchedule);
                        changedType = true;
                        theChangedOne = cs;
                        break;
                    }
                }

                // didn't find one there, then add a new one
                if (!changedType)
                {
                    // clone the first one in the list and the override the dates
                    // and type
                    theChangedOne = @case.Segments.FirstOrDefault()?.Clone() ?? new CaseSegment();
                    theChangedOne.Clean();

                    theChangedOne.StartDate = segment.StartDate;
                    theChangedOne.EndDate = segment.EndDate;
                    theChangedOne.Type = segment.Type;
                    theChangedOne.Status = @case.Status;
                    // Need to remove any applied policy usage and absences, 'cause this needs to get re-calced.
                    theChangedOne.AppliedPolicies?.ForEach(p =>
                    {
                        p.Usage?.Clear();
                        p.StartDate = segment.StartDate;
                        p.EndDate = segment.EndDate;
                    });
                    theChangedOne.Absences?.Clear();
                    theChangedOne.UserRequests?.Clear();
                    theChangedOne.LeaveSchedule = segment.LeaveSchedule;

                    @case.Segments.Add(theChangedOne);
                }
            }

            // if we went through all that and didn't find anything to change, 
            // then something weird is going on so just exit the routine
            if (theChangedOne == null)
                return;

            // now that we have changed dates and possibly inserted segments
            // sort it all again
            @case.Segments = @case.Segments.OrderBy(o => o.StartDate).ToList();

            // Remove any segments that are completely engulfed by the new segment
            @case.Segments.RemoveAll(s => s != theChangedOne && s.StartDate >= theChangedOne.StartDate && s.EndDate <= theChangedOne.EndDate);

            // Now we need to trim up any segments that this new segment overlaps 'n' stuff
            List<CaseSegment> toAdd = new List<CaseSegment>();
            List<CaseSegment> toRemove = new List<CaseSegment>();
            foreach (CaseSegment curSegment in @case.Segments.Where(s => s != theChangedOne).OrderBy(s => s.StartDate))
            {
                // Uh spaghetti-O's!!! Looks like this new segment is splitting the old one, awe schucks, we have to
                //  split them out now into 3 segments instead of 2, gee whiz BatMan, what a conumdrum!
                if (curSegment.StartDate < theChangedOne.StartDate && curSegment.EndDate > theChangedOne.EndDate)
                {
                    // Get our calculated start and end dates for our current segment split (1 = before new, 2 = after new)
                    DateTime s1 = curSegment.StartDate;
                    DateTime e1 = theChangedOne.StartDate.AddDays(-1);
                    DateTime s2 = theChangedOne.EndDate.Value.AddDays(1);
                    DateTime e2 = curSegment.EndDate.Value;
                    // Now clone our current segment (the split)
                    CaseSegment clone = curSegment.Clone();
                    clone.Id = Guid.NewGuid();
                    // Assign the clone/copy's start and end date accordingly to occur after the newly added segment that
                    //  broke it into 2.
                    clone.StartDate = s2;
                    clone.EndDate = e2;
                    // Add the clone to our toAdd collection that will be placed into the segment collection of the case later.
                    toAdd.Add(clone);
                    // Now assign the current seegment's new start and end dates so they occur before our newly added segment
                    //  that broke it into 2.
                    curSegment.StartDate = s1;
                    curSegment.EndDate = e1;
                }

                // We need to trim the beginning of our current segment because the newly added segment starts before
                //  this segment does and ends some point after this segment's start date, therefore this segment should
                //  simply start later (called a segment push).
                if (curSegment.StartDate >= theChangedOne.StartDate && curSegment.EndDate >= theChangedOne.EndDate && curSegment.StartDate <= theChangedOne.EndDate)
                {
                    curSegment.StartDate = theChangedOne.EndDate.Value.AddDays(1);
                }

                // We need to trim the end of our current segment because the newly added segment starts after
                //  this segment does and ends some point after this segment's end date, therefore this segment should
                //  simply end earlier (called a segment pull).
                if (curSegment.StartDate <= theChangedOne.StartDate && curSegment.EndDate <= theChangedOne.EndDate && curSegment.EndDate >= theChangedOne.StartDate)
                {
                    curSegment.EndDate = theChangedOne.StartDate.AddDays(-1);
                }
            }

            // Remove any bad ones
            if (toRemove.Any())
            {
                @case.Segments.RemoveAll(s => toRemove.Contains(s));
            }
            // Add any we need to here
            if (toAdd.Any())
            {
                @case.Segments.AddRange(toAdd);
            }

            // now that we have changed dates and possibly inserted segments
            // sort it all again
            @case.Segments = @case.Segments.OrderBy(o => o.StartDate).ToList();

            foreach (var testSegment in @case.Segments)
            {
                // Unlike "change case" in the real world, here we already know our case dates
                //  are perfect however our segments are being sent to this method "dirty" so
                //  we need to take these schedule boxes times and trim them up or down to the
                //  case dates specifically.
                if (testSegment.StartDate < @case.StartDate)
                {
                    testSegment.StartDate = @case.StartDate;
                }
                if (testSegment.EndDate > @case.EndDate)
                {
                    testSegment.EndDate = @case.EndDate;
                }
            }
        }

        /// <summary>
        /// Builds out any consuective and/or reduced schedule segments baesd on the
        /// leave interval passed into the "A" record.
        /// </summary>
        /// <param name="case"></param>
        private bool BuildSegments(Case @case)
        {
            // Get the employee's work schedule(s)
            var empSchedules = BuildSchedule(@case.CaseNumber);

            // Remember that awesome thing where employee schedules are built in reverse from all
            //  cases the employee has? Well, all Spectrum can send is Consecutive or Intermittent
            //  so we need to figure out if the employee is actually working a reduced schedule
            //  leave and we do that by comparing the total inferred schedule AND the time lost
            //  for the case, which will give us the leave schedule.
            // Now we get the actual leave schedules for this case.
            var caseSchedules = BuildLeaveSchedules(@case);

            var zRowLinq = Container.Rows.Where(r => r.CaseNumber == @case.CaseNumber && r.RecType == CaseRow.RecordType.RCFile && r.StartDate.HasValue && !r.IsError);

            // Ensure we have some records here
            if (!zRowLinq.Any())
            {
                // If we don't, it means it's an administrative case
                zRowLinq = new List<CaseRow>(1)
                {
                    new CaseRow()
                    {
                        RecType = CaseRow.RecordType.RCFile,
                        CaseNumber = @case.CaseNumber,
                        EmployeeNumber = @case.Employee.EmployeeNumber,
                        StartDate = @case.StartDate,
                        EndDate = @case.EndDate,
                        LeaveInterval = CaseType.Administrative
                    }
                }.AsEnumerable();
            }

            // Get the min and max date boundaries for our full date range to create segments for
            var minDate = Date.Min(
                zRowLinq.Min(z => z.StartDate.Value),
                zRowLinq.Min(z => z.EndDate ?? z.StartDate.Value)
            );
            var maxDate = Date.Max(
                zRowLinq.Max(z => z.StartDate.Value),
                zRowLinq.Max(z => z.EndDate ?? z.StartDate.Value)
            );

            // Get the RC File "Z" rows
            var rcRows = minDate.AllDatesInRange(maxDate)
                .Select(d =>
                {
                    // Get the Z rows for this date so we can see what our actual "winning" case type should be
                    var forThisDay = zRowLinq.Where(a => d.DateInRange(a.StartDate.Value, a.EndDate)).ToList();

                    // Get any "I" rows for this date so we can see if Intermittent prevails no matter what
                    var intRows = Container.Rows
                        .Where(r => r.CaseNumber == @case.CaseNumber && r.RecType == CaseRow.RecordType.Intermittent && r.DateOfAbsence == d)
                        .ToList();

                    // If any intermittent rows (I rows) then Intermittent is the case type
                    var caseType = intRows.Any() ? CaseType.Intermittent :
                        // Otherwise, if any Z rows exist for Intermittent that cover this date then it's intermittent
                        forThisDay.Any(g => g.LeaveInterval == CaseType.Intermittent) ? CaseType.Intermittent :
                        // Otherwise, if any Z rows exist for Reduced schedule leave that cover this date then it's reduced
                        forThisDay.Any(g => g.LeaveInterval == CaseType.Reduced) ? CaseType.Reduced :
                        // Otherwise, if any Z rows exist for Consecutive leave that cover this date then it's consecutive
                        forThisDay.Any(g => g.LeaveInterval == CaseType.Consecutive) ? CaseType.Consecutive :
                        // Otherwise, if any Z rows exist for Administrative leave (i.e. ADA/WC reporting only) cover
                        //  this date then it's Administrative
                        forThisDay.Any(g => g.LeaveInterval == CaseType.Administrative) ? CaseType.Administrative :
                        // Otherwise it's None (no coverage, or it's bascially a gap, perhaps a period before relapse)
                        CaseType.None;

                    // Return an anonymous structure that tracks the date and case type for use later on.
                    return new
                    {
                        Date = d,
                        LeaveInterval = caseType
                    };
                })
                .OrderBy(d => d.Date)
                .ToList();

            CaseSegment seg = null;
            void addSegment()
            {
                if (seg == null)
                {
                    return;
                }

                // If it's a reduced schedule leave, we need to build the case schedule
                //  and assign the leave schedule to the segment.
                if (seg.Type == CaseType.Reduced)
                {
                    SetReducedLeaveSchedule(seg, empSchedules, caseSchedules);
                }
                // Actually update the case by inserting this segment.
                InsertOrUpdateCaseSegment(@case, seg);
            }
            foreach (var row in rcRows)
            {
                // If the leave interval for this day is None, then that means it should just 
                //  be skipped when building segments.
                if (row.LeaveInterval == CaseType.None)
                {
                    // Add the working segment
                    addSegment();
                    // Set the segment to null and move to the next date
                    seg = null;
                    continue;
                }

                // Create the segment if necessary.
                seg = seg ?? new CaseSegment()
                {
                    Type = row.LeaveInterval,
                    Status = @case.Status,
                    StartDate = row.Date,
                    EndDate = row.Date
                };

                // If we're changing the leave interval/type, then we need to complete our
                //  working segment and then create a new one of the new type..
                if (row.LeaveInterval != seg.Type)
                {
                    // Add the segment
                    addSegment();

                    // Now create a new working segment of the new type for this row.
                    seg = new CaseSegment()
                    {
                        Type = row.LeaveInterval,
                        Status = @case.Status,
                        StartDate = row.Date,
                        EndDate = row.Date
                    };
                }

                // Set the end date of the current working segment so we can track
                //  each subsequent day of the segment (leave type).
                seg.EndDate = row.Date;
            }

            // Finish up by adding the current worknig segment, which would not be
            //  added prior to this line of code here.
            addSegment();

            // Good to go!
            return true;
        }

        private void SetReducedLeaveSchedule(CaseSegment segment, List<Schedule> empSchedules, List<Schedule> caseSchedules)
        {
            // Get employee schedule times based on the segment dates.
            var empTimes = MaterializeSchedule(segment.StartDate, segment.EndDate.Value, empSchedules);
            // Get the case schedule materialized to compare against the employee schedule to figure out
            //  the reduced schedule.
            var caseTimes = MaterializeSchedule(segment.StartDate, segment.EndDate.Value, caseSchedules);

            // Check the sum of times between the schedules and see if the case is lower
            if (empTimes.Sum(t => t.TotalMinutes) > caseTimes.Sum(t => t.TotalMinutes))
            {
                // New up the leave schedules
                segment.LeaveSchedule = new List<Schedule>();

                // Uh oh, Spaghetti-O's, looks like we have some reduced schedule stuff going on here.
                // Now, believe it or not our schedules from BuildLeaveSchedules are week by week, 1 week each
                //  that's important to know, so pay attention here. Because of this we can flip 1 by 1
                //  and determine if it's different than the employee's schedule for those same dates
                //  and if not then we need to build a reduced schedule segment and add it to the list
                //  of segments or pop it into a contiguous reduced segment.
                foreach (var schedule in caseSchedules.Where(s => s.StartDate.DateRangesOverLap(s.EndDate, segment.StartDate, segment.EndDate)).OrderBy(s => s.StartDate))
                {
                    // Build our reduced schedule (the schedule that represents the time scheduled)
                    //  which should contain the difference of time lost to the employee's actual work schedule
                    var reducedSchedule = new Schedule()
                    {
                        ScheduleType = ScheduleType.Weekly,
                        StartDate = schedule.StartDate,
                        EndDate = schedule.EndDate,
                        Times = new List<Time>(7)
                    };

                    // For each sample time in the schedule, get the difference for the reduced schedule
                    foreach (var time in schedule.Times.OrderBy(t => t.SampleDate))
                    {
                        // Find the normal employee schedule
                        var fullTime = empTimes.FirstOrDefault(t => t.SampleDate == time.SampleDate);
                        // Add the time to the reduced schedule
                        reducedSchedule.Times.Add(new Time()
                        {
                            SampleDate = time.SampleDate,
                            // Calculate the difference, if we didn't find normal schedule times then assume they missed
                            //  the entire amount (would have been scheduled that entire amount for that day)
                            //  Otherwise assume the difference between time lost and their normal schedule was the time scheduled
                            //  during the reduced period.
                            TotalMinutes = fullTime == null ? time.TotalMinutes : fullTime.TotalMinutes - time.TotalMinutes,
                            IsHoliday = fullTime?.IsHoliday ?? false
                        });
                    }

                    // Assign the leave schedule to the reduced segment
                    segment.LeaveSchedule.Add(reducedSchedule);
                }
            }
        }

        /// <summary>
        /// Builds the leave schedules.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        private List<Schedule> BuildLeaveSchedules(Case @case)
        {
            // Build the case/leave schedules based on the usage passed in so we can compare it and
            //  create any inferred reduced schedule leave segments.
            var caseSchedules = new List<Schedule>();

            // Get all determination and intermittent rows to reverse-engineer the schedule from
            var usageRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.Adjudication)
                .Where(r => !r.IsError)
                .Where(r => !r.HasMinutesWarning) // ignore these, they suck
                .Where(r => r.CaseNumber == @case.CaseNumber)
                .ToList();

            // Get the case dates to become our schedule dates
            var scheduleStart = @case.StartDate.GetFirstDayOfWeek();
            var scheduleEnd = @case.EndDate.Value.GetLastDayOfWeek();
            var testDate = scheduleStart;
            Schedule currentSchedule = null;

            // Work at building out the schedules until we've filled in all of the complete weeks
            //  representing the entire span of this case.
            while (testDate <= scheduleEnd)
            {
                // Determine if we need to set the current schedule
                //  and add it to the list for building
                if (testDate.DayOfWeek == DayOfWeek.Sunday || currentSchedule == null || currentSchedule.Times.Count == 7)
                {
                    currentSchedule = caseSchedules.AddFluid(new Schedule()
                    {
                        StartDate = testDate.GetFirstDayOfWeek(),
                        EndDate = testDate.GetLastDayOfWeek(),
                        ScheduleType = ScheduleType.Weekly,
                        Times = new List<Time>(7)
                    });
                    currentSchedule.Metadata.SetRawValue("SystemEntered", true);
                }

                // Here we assume 1 day of usage per "A" record
                var minutesUsed = usageRows
                    .Where(u => u.CaseNumber == @case.CaseNumber)
                    .Where(u => u.AdjudicationStartDate.HasValue)
                    .Where(u => testDate.DateInRange(u.AdjudicationStartDate.Value, u.AdjudicationEndDate))
                    .Select(u => u.MinutesUsed)
                    // Add any intermittent time usage for this day
                    .Union(usageRows
                    .Where(u => u.CaseNumber == @case.CaseNumber)
                    .Where(u => u.DateOfAbsence.HasValue)
                    .Where(u => u.DateOfAbsence.Value == testDate.Date)
                    .Where(u => u.MinutesApproved.HasValue || u.MinutesDenied.HasValue || u.MinutesPending.HasValue)
                    .Select(u => u.MinutesApproved + u.MinutesDenied + u.MinutesPending))
                    .Max();

                // Add the time to the schedule
                currentSchedule.Times.Add(new Time()
                {
                    SampleDate = testDate,
                    TotalMinutes = minutesUsed ?? 0
                });

                // Go to the next date
                testDate = testDate.AddDays(1);
            }

            //caseSchedules.RemoveAll(s => s.Times.Sum(t => t.TotalMinutes) == 0);

            return caseSchedules;
        }

        /// <summary>
        /// Builds the schedule from reverse engineering the "A" and "I" rows passed in with constraints around
        /// the expected min and max case dates.
        /// </summary>
        /// <param name="caseNumber">The case number.</param>
        /// <returns>
        /// A list of schedules to add/merge to the employee that gets created.
        /// </returns>
        private List<Schedule> BuildSchedule(string caseNumber = null)
        {
            var schedules = new List<Schedule>();

            // See if we have a schedule row
            var scheduleRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.WorkSchedule)
                .Where(r => !r.IsError)
                .Where(r => string.IsNullOrWhiteSpace(caseNumber) || r.CaseNumber == caseNumber)
                .ToList();

            if (scheduleRows.Any())
            {
                foreach (var row in scheduleRows)
                {
                    // Pre-set the schedule effective date here (you'll see why in a few lines)
                    row.ScheduleEffectiveDate = row.ScheduleEffectiveDate
                        ?? Date.Min(
                            Container.Rows.FirstOrDefault(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.Case)?.DateCaseOpened,
                            Container.Rows.FirstOrDefault(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.Case)?.ExpectedStartDate,
                            Container.Rows.Where(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.RCFile).Min(r => r.StartDate),
                            Container.Rows.Where(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.Adjudication).Min(r => r.AdjudicationStartDate),
                            Container.Rows.Where(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.Intermittent).Min(r => r.DateOfAbsence)
                        )
                        ?? Container.Employee?.HireDate
                        ?? Container.Employee?.ServiceDate
                        ?? Container.Employee?.RehireDate
                        ?? DateTime.UtcNow;
                }

                // Yep, that's why. Now we need to order these based on their effective date so we can build and destroy accordingly.
                foreach (var row in scheduleRows.OrderBy(r => r.ScheduleEffectiveDate))
                {
                    DateTime effectiveDate = row.ScheduleEffectiveDate
                        ?? Container.Rows.FirstOrDefault(r => r.CaseNumber == row.CaseNumber && r.RecType == CaseRow.RecordType.Case)?.ExpectedStartDate
                        ?? DateTime.UtcNow;

                    var workSched = new Schedule() { ScheduleType = row.VariableSchedule == true ? ScheduleType.Variable : ScheduleType.Weekly };
                    workSched.StartDate = effectiveDate.ToMidnight();

                    if (!row.WorkTimeSun.HasValue &&
                        !row.WorkTimeMon.HasValue &&
                        !row.WorkTimeTue.HasValue &&
                        !row.WorkTimeWed.HasValue &&
                        !row.WorkTimeThu.HasValue &&
                        !row.WorkTimeFri.HasValue &&
                        !row.WorkTimeSat.HasValue)
                    {
                        if (!row.MinutesPerWeek.HasValue)
                        {
                            workSched = null;
                        }
                        else
                        {
                            DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                            int dailyMinutes = Convert.ToInt32(Math.Floor(row.MinutesPerWeek.Value / 5m));
                            int leftOverMinute = row.MinutesPerWeek.Value % 5 == 0 ? 0 : 1;
                            for (var d = 0; d < 7; d++)
                            {
                                if (sample.DayOfWeek > 0 && (int)sample.DayOfWeek < 6)
                                {
                                    workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = dailyMinutes });
                                }
                                else
                                {
                                    workSched.Times.Add(new Time() { SampleDate = sample });
                                }
                                sample = sample.AddDays(1);
                            }
                            // Add our leftover minute to the first date, clugey, but it's whatever.
                            workSched.Times.First(t => t.TotalMinutes.HasValue).TotalMinutes += leftOverMinute;
                        }
                    }
                    else
                    {
                        DateTime sample = workSched.StartDate.GetFirstDayOfWeek();
                        for (var d = 0; d < 7; d++)
                        {
                            int? workTime = null;
                            switch (sample.DayOfWeek)
                            {
                                case DayOfWeek.Sunday: workTime = row.WorkTimeSun; break;
                                case DayOfWeek.Monday: workTime = row.WorkTimeMon; break;
                                case DayOfWeek.Tuesday: workTime = row.WorkTimeTue; break;
                                case DayOfWeek.Wednesday: workTime = row.WorkTimeWed; break;
                                case DayOfWeek.Thursday: workTime = row.WorkTimeThu; break;
                                case DayOfWeek.Friday: workTime = row.WorkTimeFri; break;
                                case DayOfWeek.Saturday: workTime = row.WorkTimeSat; break;
                            }
                            workSched.Times.Add(new Time() { SampleDate = sample, TotalMinutes = workTime });
                            sample = sample.AddDays(1);
                        }
                    }

                    var overlap = schedules.OrderByDescending(s => s.StartDate).FirstOrDefault();
                    if (overlap != null)
                    {
                        overlap.EndDate = workSched.StartDate.AddDays(-1);
                    }
                    if (workSched.Times.Sum(t => t.TotalMinutes) > 0)
                    {
                        schedules.Add(workSched);
                    }
                }
            }

            var inferredSchedules = new List<Schedule>();

            // Get all determination and intermittent rows to reverse-engineer the schedule from
            var usageRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.Adjudication || r.RecType == CaseRow.RecordType.Intermittent)
                .Where(r => !r.IsError)
                .Where(r => !r.HasMinutesWarning) // ignore these, they suck
                .Where(r => string.IsNullOrWhiteSpace(caseNumber) || r.CaseNumber == caseNumber)
                .ToList();

            // Get an IEnumerable we can use to iterate over and over our case rows
            var caseRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.Case)
                .Where(r => !r.IsError)
                .Where(r => string.IsNullOrWhiteSpace(caseNumber) || r.CaseNumber == caseNumber);

            // Get an IEnumerable we can use to iterativeover and over our Z rows
            var zRows = Container.Rows
                .Where(r => r.RecType == CaseRow.RecordType.RCFile)
                .Where(r => !r.IsError)
                .Where(r => string.IsNullOrWhiteSpace(caseNumber) || r.CaseNumber == caseNumber)
                .Where(r => r.StartDate.HasValue);

            // Calculate the earliest possible date we're going to need to cover for this case
            var minCaseStart = Date.Min(
                caseRows.Min(c => c.ExpectedStartDate),
                usageRows.Min(u => u.AdjudicationStartDate),
                usageRows.Min(u => u.DateOfAbsence),
                zRows.Any() ? zRows.Min(u => u.StartDate) : null,
                zRows.Any() ? zRows.Min(u => u.EndDate) : null
            );
            // Calculate the latest possible date we're going to need to cover for this case
            var maxCaseEnd = Date.Max(
                caseRows.Max(c => c.ExpectedRTWDate),
                caseRows.Max(c => c.ConfirmedRTWDate),
                caseRows.Max(c => c.CaseClosedDate),
                usageRows.Max(u => u.AdjudicationEndDate),
                usageRows.Max(u => u.DateOfAbsence),
                zRows.Any() ? zRows.Min(u => u.StartDate) : null,
                zRows.Any() ? zRows.Min(u => u.EndDate) : null
            );
            // We really do need this information, sheesh people
            if (!minCaseStart.HasValue)
            {
                Container.Errors.Add(string.Format("Can not find a start date to infer a work schedule for employee '{0}'", Container.EmployeeNumber));
                return new List<Schedule>(0);
            }
            // We also really need this information, lost time requires an end date, period
            if (!maxCaseEnd.HasValue)
            {
                Container.Errors.Add(string.Format("Can not find an end date to infer a work schedule for employee '{0}' starting '{1:MM-dd-yyyy}'", Container.EmployeeNumber, minCaseStart));
                return new List<Schedule>(0);
            }

            var scheduleStart = minCaseStart.Value.GetFirstDayOfWeek();
            var scheduleEnd = maxCaseEnd.Value.GetLastDayOfWeek();
            var testDate = scheduleStart;
            Schedule currentSchedule = null;

            // Work at building out the schedules until we've filled in all of the complete weeks
            //  representing the entire span of all cases for this employee.
            while (testDate <= scheduleEnd)
            {
                // Determine if we need to set the current schedule
                //  and add it to the list for building
                if (testDate.DayOfWeek == DayOfWeek.Sunday || currentSchedule == null || currentSchedule.Times.Count == 7)
                {
                    currentSchedule = inferredSchedules.AddFluid(new Schedule()
                    {
                        StartDate = testDate.GetFirstDayOfWeek(),
                        EndDate = testDate.GetLastDayOfWeek(),
                        ScheduleType = ScheduleType.Weekly,
                        Times = new List<Time>(7)
                    });
                    currentSchedule.Metadata.SetRawValue("SystemEntered", true);
                }

                // Here we assume 1 day of usage per "A" record
                var minutesUsed = usageRows
                    .Where(u => u.AdjudicationStartDate.HasValue)
                    .Where(u => testDate.DateInRange(u.AdjudicationStartDate.Value, u.AdjudicationEndDate))
                    .Select(u => u.MinutesUsed)
                    // Add any intermittent time usage for this day
                    .Union(usageRows
                    .Where(u => u.DateOfAbsence.HasValue)
                    .Where(u => u.DateOfAbsence.Value == testDate.Date)
                    .Where(u => u.MinutesApproved.HasValue || u.MinutesDenied.HasValue || u.MinutesPending.HasValue)
                    .Select(u => u.MinutesApproved + u.MinutesDenied + u.MinutesPending))
                    .Max();

                // Add the time to the schedule
                currentSchedule.Times.Add(new Time()
                {
                    SampleDate = testDate,
                    TotalMinutes = minutesUsed ?? 0
                });

                // Go to the next date
                testDate = testDate.AddDays(1);
            }

            // Remove all Zero time scheduled schedules, they're worthless
            inferredSchedules.RemoveAll(s => s.Times.Sum(t => t.TotalMinutes) == 0);

            // Now we need to combine the aggregate inferred schedules AND any schedule rows passed
            //  because it turns out Prognos is stupid and doesn't know a schedule from a hole in you
            //  know  what.
            if (!schedules.Any())
            {
                // No schedule rows to merge so just return our inferred schedule(s).
                return inferredSchedules;
            }

            // Okay, magic time.
            // Loop through all  of the inferred schedules (there will always be an equal number OR MORE of these than      
            //  the schedule rows passed in, that's just how it works.
            foreach (var sched in inferredSchedules.OrderBy(s => s.StartDate))
            {
                // Get the schedule that this inferred schedule sits on top of over/overlaps
                var testSchedule = schedules.FirstOrDefault(s => s.StartDate.DateRangesOverLap(s.EndDate, sched.StartDate, sched.EndDate));

                // If there is no overlap then we're missing some schedule info for these dates, which are obviously important.
                //  In this case just wholesale add the leave/absence schedule from the inferred list to the work schedules
                //  for the employee.
                if (testSchedule == null)
                {
                    schedules.Add(sched.Clone());
                    // We're done, just add one and code on.
                    continue;
                }

                if (sched.Times.Sum(t => t.TotalMinutes ?? 0) > testSchedule.Times.Sum(t => t.TotalMinutes ?? 0))
                {
                    // Our leave schedule is MORE than our weekly schedule, WHAT THE!?!! Yeah, that's right, sheesh people, this sucks
                    //  So basically we need to "inject" this new leave schedule into the normal schedule and split the normal schedule
                    //  around the leave schedule that's all jacked up.

                    // Pre-clone this schedule
                    var splitSchedule = testSchedule.Clone();

                    // Set the old schedule's end date to the day before our newly adjusted schedule's start date.
                    testSchedule.EndDate = sched.StartDate.AddDays(-1);

                    // Now we add a clone of the adjusted schedule
                    schedules.Add(sched.Clone());

                    // If we need the split schedule "after" this one, then add it
                    if (splitSchedule.EndDate > sched.EndDate)
                    {
                        // Set the adjusted start date of our original schedule that we split
                        splitSchedule.StartDate = sched.EndDate.Value.AddDays(1);
                        // Add the copy of our original schedule as the "split end" back to the list
                        schedules.Add(splitSchedule);
                    }
                }
            }

            return FixSchedulesWithMissingCoverageForUsage(schedules, caseNumber);
        }

        /// <summary>
        /// Fixes the schedules from usage and returns a corrected list of schedules for
        /// the employee.
        /// </summary>
        /// <param name="schedules">The schedules.</param>
        /// <param name="caseNumber">The case number.</param>
        /// <returns></returns>
        private List<Schedule> FixSchedulesWithMissingCoverageForUsage(List<Schedule> schedules, string caseNumber = null)
        {
            // Get the usage rows from Adjudications, these will tell us what days the employee
            //  missed that aren't covered by a work schedule.
            var aRows = Container.Rows.Where(r => r.RecType == CaseRow.RecordType.Adjudication
                && !r.IsError
                && !r.HasMinutesWarning
                && r.CaseNumber == (caseNumber ?? r.CaseNumber)
                && r.AdjudicationStartDate.HasValue
                && r.MinutesUsed.HasValue
                && r.MinutesUsed > 0);

            // If there's nothing to do, then duh, don't do anything, just return all happy like.
            if (!aRows.Any())
            {
                return schedules;
            }

            // This will hold our new schedules that cover a week were we "missed" some time
            List<Schedule> newSchedules = new List<Schedule>();

            // Rotate week by week of all days in all usage to check by
            //  do this by grouping all days in all "A" rows passed in by week
            //  and then testing the schedule against those by scope of the week.
            foreach (var week in aRows.GroupBy(k => new
            {
                StartDate = k.AdjudicationStartDate.Value.GetFirstDayOfWeek(),
                EndDate = k.AdjudicationStartDate.Value.GetLastDayOfWeek()
            }).OrderBy(g => g.Key.StartDate))
            {
                // Stage out a corrected schedule for this week, if we don't need it, we'll throw
                //  it to the GC to deal with once we leave the loop context
                Schedule correctedWeekSchedule = new Schedule()
                {
                    ScheduleType = ScheduleType.Weekly,
                    StartDate = week.Key.StartDate,
                    EndDate = week.Key.EndDate,
                    Times = new List<Time>(7)
                };
                List<DateTime?> missingDates = new List<DateTime?>();
                List<DateTime?> goodDates = new List<DateTime?>();
                List<DateTime?> extraDates = new List<DateTime?>();

                // Loop through each date of this week and determine if we're covered, otherwise
                //  of if there are no "A" records for that date, then move on
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the time used/lost for this day of this week
                    var usage = week.Where(a => a.AdjudicationStartDate == date).Max(a => a.MinutesUsed) ?? 0;
                    // Get the work schedule that covers this date
                    var sched = schedules.OrderBy(s => s.StartDate).FirstOrDefault(s => date.DateInRange(s.StartDate, s.EndDate));
                    if (sched == null)
                    {
                        // If there is no schedule that covers it, we need to create a date that covers it
                        missingDates.Add(date);
                        continue;
                    }
                    // Get the time that matches the same "day of the week" of the current date we're testing
                    var time = sched.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek);
                    if (time.TotalMinutes < usage)
                    {
                        // If the total minutes scheduled is less than the recorded time used, then we need to correct
                        //  that as well
                        missingDates.Add(date);
                        continue;
                    }
                    // If we're not using time but time is scheduled, add this to the extra dates
                    if (usage == 0 && time.TotalMinutes > 0)
                    {
                        extraDates.Add(date);
                        continue;
                    }
                    // If we're using time and have time scheduled and they "match(ish)", 
                    //  AWESOME, add this to our good dates collection.
                    if (usage <= time.TotalMinutes)
                    {
                        goodDates.Add(date);
                    }
                }

                // Loop through each date of this week and determine if we're going to pull
                //  from the good date, missing date OR extra date.
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the work schedule that covers this date
                    var sched = schedules.OrderBy(s => s.StartDate).FirstOrDefault(s => date.DateInRange(s.StartDate, s.EndDate));
                    // Get the time that matches the same "day of the week" of the current date we're testing
                    var time = sched?.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek);

                    // This was good, then keep it
                    if (goodDates.Contains(date))
                    {
                        // Add this to the corrected schedule
                        correctedWeekSchedule.Times.Add(new Time()
                        {
                            SampleDate = date,
                            TotalMinutes = time.TotalMinutes
                        });
                        // continue to the next day
                        continue;
                    }

                    // This was missing, add it to the weekly schedule date
                    if (missingDates.Contains(date))
                    {
                        // Get the time used/lost for this day of this week
                        var usage = week.Where(a => a.AdjudicationStartDate == date).Max(a => a.MinutesUsed) ?? 0;
                        if (usage > 0)
                        {
                            // So usage was more than zero, however if our time scheduled was zero, we need to find
                            //  an "extra" day where time was sceduled but not used. This essentially "shifts" the
                            //  usage for that date to this day and leaves that day at zero
                            if ((time?.TotalMinutes ?? 0) == 0)
                            {
                                // Get the first substitute date with enough time to cover this one
                                var substituteDate = extraDates
                                    .FirstOrDefault(e => sched?.Times.Any(a => a.SampleDate.DayOfWeek == e.Value.DayOfWeek && a.TotalMinutes >= usage) == true);
                                // Check if we have one, if so we're good, if not we're adding an 
                                //  entirely new scheduled day of week (increase in schedule)
                                if (substituteDate.HasValue)
                                {
                                    usage = sched.Times.FirstOrDefault(a => a.SampleDate.DayOfWeek == substituteDate.Value.DayOfWeek && a.TotalMinutes >= usage)?.TotalMinutes ?? 0;
                                    // Remove this from our extra dates list so we don't re-use it
                                    extraDates.Remove(substituteDate.Value);
                                }
                            }
                        }
                        // Add this to the corrected schedule
                        correctedWeekSchedule.Times.Add(new Time()
                        {
                            SampleDate = date,
                            TotalMinutes = usage
                        });
                        // continue to the next day
                        continue;
                    }
                }

                // One last loop through these to fill in any extra dates after accouting for filling
                //  in/shifting for missing dates and good dates
                foreach (var date in week.Key.StartDate.AllDatesInRange(week.Key.EndDate))
                {
                    // Get the day of the week of scheduled time for the new schedule
                    var time = correctedWeekSchedule.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == date.DayOfWeek);
                    if (time != null)
                    {
                        // We have one aleady, move on to the next day
                        continue;
                    }
                    var sampleDate = extraDates.FirstOrDefault();
                    if (sampleDate.HasValue)
                    {
                        // Remove this sample date so we don't use it again
                        extraDates.Remove(sampleDate);
                        // Get the work schedule that covers this date
                        var sched = schedules.OrderBy(s => s.StartDate).FirstOrDefault(s => sampleDate.Value.DateInRange(s.StartDate, s.EndDate));
                        // Get the time that matches the same "day of the week" of the current date we're testing
                        time = sched.Times.FirstOrDefault(t => t.SampleDate.DayOfWeek == sampleDate.Value.DayOfWeek);
                        // Determine if we have one
                        if (time != null)
                        {
                            // Add this to the corrected schedule
                            correctedWeekSchedule.Times.Add(new Time()
                            {
                                SampleDate = date,
                                TotalMinutes = time.TotalMinutes
                            });
                            // Go do the next day
                            continue;
                        }
                    }

                    // We're here, so add a zero day to the schedule.
                    // Add this to the corrected schedule
                    correctedWeekSchedule.Times.Add(new Time()
                    {
                        SampleDate = date,
                        TotalMinutes = 0
                    });
                }

                // If there were any missing dates, this means we need to adjust the schedule
                //  for the given week.
                if (missingDates.Any())
                {
                    correctedWeekSchedule.Times = correctedWeekSchedule.Times.OrderBy(t => t.SampleDate).ToList();
                    newSchedules.Add(correctedWeekSchedule);
                }
            }

            // Merge the lists of schedules together so we're all kosher
            return MergeSchedules(schedules, newSchedules);
        }

        /// <summary>
        /// Merges the collections of work schedules (copied primarily from Employee Service but geared for this
        /// type of awesomeness.
        /// </summary>
        /// <param name="currentScheules">The current scheules.</param>
        /// <param name="newSchedules">The new schedules.</param>
        /// <returns></returns>
        private List<Schedule> MergeSchedules(List<Schedule> currentScheules, List<Schedule> newSchedules)
        {
            if (currentScheules == null)
                return newSchedules;

            foreach (var newWorkSchedule in newSchedules)
            {
                if (newWorkSchedule != null && !currentScheules.Any(w => w.Id == newWorkSchedule.Id))
                    currentScheules.Add(newWorkSchedule);
                else if (newWorkSchedule != null)
                {
                    var oldSched = currentScheules.FirstOrDefault(w => w.Id == newWorkSchedule.Id);
                    if (oldSched != null && (
                        oldSched.StartDate != newWorkSchedule.StartDate ||
                        oldSched.EndDate != newWorkSchedule.EndDate ||
                        oldSched.Times.Count != newWorkSchedule.Times.Count ||
                        oldSched.ScheduleType != newWorkSchedule.ScheduleType ||
                        oldSched.Times.Except(newWorkSchedule.Times).Any()))
                    {
                        newWorkSchedule.Id = Guid.NewGuid();
                        currentScheules.Add(newWorkSchedule);
                    }
                }

                // Level work schedules
                if (newWorkSchedule != null)
                {
                    var overlap = currentScheules.Where(w => w.Id != newWorkSchedule.Id &&
                        (w.StartDate.DateRangesOverLap(w.EndDate, newWorkSchedule.StartDate, newWorkSchedule.EndDate) ||
                        newWorkSchedule.StartDate.DateRangesOverLap(newWorkSchedule.EndDate, w.StartDate, w.EndDate))
                        ).ToList();
                    foreach (var fix in overlap)
                    {
                        if (fix.StartDate < newWorkSchedule.StartDate)
                        {
                            if (newWorkSchedule.EndDate.HasValue && (!fix.EndDate.HasValue || fix.EndDate > newWorkSchedule.EndDate))
                            {
                                // We're applying a schedule change in the middle somewhere for a fixed period, so
                                //  we need to copy the old schedule to make the EE's schedule perpetual.
                                currentScheules.Add(new Schedule()
                                {
                                    StartDate = newWorkSchedule.EndDate.Value.AddDays(1),
                                    EndDate = fix.EndDate,
                                    Times = fix.Times.Select(t => new Time()
                                    {
                                        SampleDate = t.SampleDate,
                                        TotalMinutes = t.TotalMinutes
                                    }).ToList()
                                });
                            }
                            fix.EndDate = newWorkSchedule.StartDate.AddDays(-1);
                        }
                        else if (newWorkSchedule.EndDate.HasValue && !fix.EndDate.HasValue)
                        {
                            fix.StartDate = newWorkSchedule.EndDate.Value.AddDays(1);
                            fix.Times.ForEach(t => t.SampleDate = fix.StartDate.AddDays((t.SampleDate - fix.StartDate).TotalDays));
                        }
                        else
                        {
                            // this is a replacement. Remove it from the work schedules collection
                            currentScheules.Remove(fix);
                        }
                    }
                }
            }

            return currentScheules;
        }

        /// <summary>
        /// Use an initialized LOA (this) ojbect and generate a schedule that is between 
        /// the passed start and end dates, inclusive of both dates
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>List of Time objects</returns>
        private List<Time> MaterializeSchedule(DateTime startDate, DateTime endDate, List<Schedule> schedules = null)
        {
            var properSchedules = schedules ?? Container.Employee.WorkSchedules ?? new List<Schedule>(0);

            // Clone our schedules so we can calc the calc without modifying the underlying entity
            properSchedules = properSchedules.Clone();

            // If we only have a single schedule, determine whether or not we need to extend the start/end date of that
            //  schedule to properly accommodate the start and end date requested for materialization.
            if (properSchedules.Count == 1)
            {
                var s = properSchedules[0];
                if (s.StartDate > startDate)
                {
                    var absoluteMin = new DateTime[2] { Container.Employee.HireDate ?? Container.Employee.ServiceDate ?? startDate, Container.Employee.ServiceDate ?? startDate }.Min();
                    s.StartDate = absoluteMin;
                }
                if (s.EndDate.HasValue && s.EndDate.Value < endDate)
                {
                    var leastOfMax = new DateTime[2] { Container.Employee.Status == EmploymentStatus.Terminated ? Container.Employee.TerminationDate ?? endDate : endDate, endDate }.Min();
                    s.EndDate = leastOfMax;
                }
            }

            var times = new List<Time>();

            // now start copying
            foreach (Schedule s in properSchedules)
            {
                // if it's not manual then make sure it is in the range of dates we are calculating
                // no sense adding it to the list if it's outside the range
                if (!s.StartDate.DateRangesOverLap(s.EndDate, startDate, endDate))
                    continue;

                // everything else is some sort of repeating pattern
                // find a start date
                DateTime start = s.StartDate;
                if (startDate > start)
                    start = startDate;

                // find the end date, which is either the schedules end date or if that is null the end date of the case
                DateTime end = s.EndDate ?? endDate;

                // find where in the pattern the start calc date is releative to the pattern
                DateTime psd = s.Times.Min(wt => wt.SampleDate);
                int pos = PatternPosition(psd, start, s.Times.Count);

                // now roll it all forward including the end date
                while (start <= end)
                {
                    // either include everything or include holidays
                    int totalMinutes = s.Times[pos].TotalMinutes ?? 0;
                    // copy them all? wrap in an if() to remove 0s
                    times.Add(new Time()
                    {
                        SampleDate = start,
                        TotalMinutes = totalMinutes,
                        IsHoliday = false
                    });

                    // are we at the end of the pattern? then back around to the beginning
                    pos++;
                    if (pos == s.Times.Count)
                        pos = 0;

                    // next
                    start = start.AddDays(1);
                }
            }

            var timeList = times.Where(t => t.SampleDate.DateInRange(startDate, endDate)).OrderBy(t => t.SampleDate).ToList();
            if (timeList.Any())
                return timeList;

            return startDate.AllDatesInRange(endDate).Select(d => new Time() { SampleDate = d, TotalMinutes = 0 }).ToList();
        }

        /// <summary>
        /// pass this method the pattern's start date, the date you are looking at and how
        /// many days are in the pattern and it will return the element position of
        /// the target date
        /// </summary>
        /// <param name="patternStartDate">The pattern start date.</param>
        /// <param name="targetDate">The target date.</param>
        /// <param name="patternLength">Length of the pattern.</param>
        /// <returns></returns>
        private int PatternPosition(DateTime patternStartDate, DateTime targetDate, int patternLength)
        {
            int days = (patternStartDate - targetDate).Days;
            int pos = Math.Abs(days) % patternLength;
            if (days > 0)
            {
                pos = patternLength - pos;
                if (pos == patternLength)
                    pos = 0;
            }

            return pos;
        }

        #endregion

        #region Eligibility
        
        /// <summary>
        /// Gets a list of policy rule groups for a given policy and leave of absence (case used to get the proper Reason).
        /// </summary>
        /// <param name="policy">The policy to pull the rule groups from that are applicable to the specified case reason.</param>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <returns>A list of policy rule groups that are applicable to the policy</returns>
        private List<PolicyRuleGroup> GetRuleGroups(Policy policy, LeaveOfAbsence loa)
        {
            using (new InstrumentationContext("EligibilityService.GetRuleGroups"))
            {
                List<PolicyRuleGroup> ruleGroups = new List<PolicyRuleGroup>();
                ruleGroups.AddRange(policy.RuleGroups);
                var absenceReason = policy.AbsenceReasons.FirstOrDefault(a => a.ReasonCode == loa.Case.Reason.Code);
                if (absenceReason != null)
                {
                    ruleGroups.AddRange(absenceReason.RuleGroups);
                }
                return ruleGroups;
            }
        }

        /// <summary>
        /// Translates a list of policy rule groups to their applied rule group counterparts which are
        /// ready to be attached to an applied policy for rule evaluation.
        /// </summary>
        /// <param name="groups">The policy rule groups to convert to applied rule groups.</param>
        /// <returns>A list of applied rule groups based on the original list or policy rule groups</returns>
        private List<AppliedRuleGroup> GetAppliedRuleGroups(List<PolicyRuleGroup> groups)
        {
            using (new InstrumentationContext("EligibilityService.GetAppliedRuleGroups"))
            {
                return groups.Select(g => new AppliedRuleGroup()
                {
                    RuleGroup = g,
                    Rules = g.Rules.Select(r => new AppliedRule() { Rule = r }).ToList()
                }).ToList();
            }
        }

        /// <summary>
        /// Evaluates a list of rule groups together. The policy group type dictates the types of rule groups that
        /// should be evaluated in this operation (e.g. selection, eligibility, time, etc.).
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="groups">The list of applied rule groups to evaluate which may or may not match the specified rule group type</param>
        /// <param name="type">The policy rule group type </param>
        private void EvaluateRuleGroups(LeaveOfAbsence loa, List<AppliedRuleGroup> groups, PolicyRuleGroupType type)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRuleGroups"))
            {
                foreach (var group in groups.Where(g => g.RuleGroup.RuleGroupType == type))
                    EvaluateRuleGroup(loa, group);
            }
        }

        /// <summary>
        /// Evaluates each rule within the specified rule group, sets the eval date.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="group">The applied rule group containing the rules to evaluate</param>
        private void EvaluateRuleGroup(LeaveOfAbsence loa, AppliedRuleGroup group)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRuleGroup"))
            {
                foreach (var rule in group.Rules)
                    EvaluateRule(loa, rule);

                group.EvalDate = DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Evaluates an applied rule annd determines what the actual value is and whether or not the rule
        /// passes.
        /// </summary>
        /// <param name="loa">The leave of absence to be used as the primary source for evaluation</param>
        /// <param name="rule">The applied rule to evaluate expressions from</param>
        private void EvaluateRule(LeaveOfAbsence loa, AppliedRule rule)
        {
            using (new InstrumentationContext("EligibilityService.EvaluateRule"))
            {
                // If the rule has been overridden, we still calculate because we need to show the user the original evaluation 

                AppliedRuleEvalResult ruleEvalResult = AppliedRuleEvalResult.Unknown;
                try
                {
                    rule.ActualValue = LeaveOfAbsenceEvaluator.Eval(rule.Rule.LeftExpression, loa);
                    if (rule.ActualValue != null)
                        rule.ActualValueString = rule.ActualValue.ToString();

                    FormatRightExpressionForMultipleValues(rule);

                    bool result = LeaveOfAbsenceEvaluator.Eval<bool>(rule.Rule.ToString(), loa);
                    ruleEvalResult = result ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (NullReferenceException)
                {
                    // If the result was NULL and it threw an exception, that's OK, let's check
                    //  though to see if we were actually expecting null.
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                    if (rule.Rule.RightExpression == "null")
                        ruleEvalResult = rule.Rule.Operator == "==" ? AppliedRuleEvalResult.Pass : AppliedRuleEvalResult.Fail;
                }
                catch (Exception ex)
                {
                    rule.Messages.AddFormat("An error occurred evaluating this rule, {0}", ex.Message);
                    ruleEvalResult = AppliedRuleEvalResult.Unknown;
                }

                if (rule.Overridden)
                {
                    rule.OverrideFromResult = ruleEvalResult;
                }
                else
                {
                    rule.Result = ruleEvalResult;
                }
            }
        }

        /// <summary>
        /// The function to format the RightExpression for multivalued operator
        /// </summary>
        /// <param name="rule"></param>
        private static void FormatRightExpressionForMultipleValues(AppliedRule rule)
        {
            // In case of multiple value operator, the right expression contains values in the form 'SELF,PARENT,SPOUSE'
            // The values needs to be converted to 'SELF','PARENT','SPOUSE'
            // If it is already formatted, then don't format again
            if (rule != null && rule.Rule != null && rule.Rule.ToString().Contains("::") &&
                !Regex.IsMatch(rule.Rule.RightExpression, "'([A-Z])\\w+'"))
            {
                string concatValues = string.Empty;

                if (!string.IsNullOrEmpty(rule.Rule.RightExpression))
                {
                    string[] singleValue = rule.Rule.RightExpression.Split(',');

                    if (singleValue.Length > 0)
                    {
                        singleValue.ForEach(b => { concatValues = concatValues + "'" + b + "',"; });
                    }
                }

                if (concatValues.Length > 0)
                {
                    concatValues = concatValues.Replace("''", "'");
                    concatValues = concatValues.Substring(0, concatValues.Length - 1);
                    rule.Rule.RightExpression = "(" + concatValues + ")";
                }
            }
        }

        #endregion

        #region IDisposable Support
        /// <summary>
        /// The disposed value to detect redundant calls
        /// </summary>
        private bool disposedValue = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; 
        /// <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // set large fields to null.
                Container = null;
                Customer = null;
                Employer = null;
                Policies = null;
                Reasons = null;
                CustomFields = null;
                ToDos = null;
                disposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
