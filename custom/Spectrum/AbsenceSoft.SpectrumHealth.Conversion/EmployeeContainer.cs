﻿using AbsenceSoft.Data.Customers;
using System.Collections.Concurrent;

namespace AbsenceSoft.SpectrumHealth.Conversion
{
    public class EmployeeContainer
    {
        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the employee.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the rows.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public ConcurrentBag<CaseRow> Rows { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>
        /// The errors.
        /// </value>
        public ConcurrentBag<string> Errors { get; set; }
    }
}
