﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class InjuryDescription : BaseEntity<InjuryDescription>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.WorkRelated.WhatHarmedTheEmployee
        /// </summary>
        [Display(Name = "CAUSE", Order = 3)]
        public string Cause { get => TryGetValue("Cause", out string val) ? val : null; set => this["Cause"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "PREPAREDBY", Order = 4)]
        public string PreparedBy { get => TryGetValue("PreparedBy", out string val) ? val : null; set => this["PreparedBy"] = value; }

        /// <summary>
        /// Case.CreatedBy.JobTitle
        /// </summary>
        [Display(Name = "POSITION", Order = 5)]
        public string Position { get => TryGetValue("Position", out string val) ? val : null; set => this["Position"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "PREPAREDATE", Order = 6)]
        public string PrepareDate { get => TryGetValue("PrepareDate", out string val) ? val : null; set => this["PrepareDate"] = value; }

        /// <summary>
        /// Case.WorkRElated.ActivityBeforeIncident
        /// </summary>
        [Display(Name = "ACTIVITY", Order = 7)]
        public string Activity { get => TryGetValue("Activity", out string val) ? val : null; set => this["Activity"] = value; }

        /// <summary>
        /// Case.CustomFields[WPROCESS]
        /// </summary>
        [Display(Name = "WPROCESS", Order = 8)]
        public string WProcess { get => TryGetValue("WProcess", out string val) ? val : null; set => this["WProcess"] = value; }

        /// <summary>
        /// Case.CustomFields[SAFETYEQUIP]
        /// </summary>
        [Display(Name = "SAFETYEQUIP", Order = 9)]
        public string SafetyEquipment { get => TryGetValue("SafetyEquipment", out string val) ? val : null; set => this["SafetyEquipment"] = value; }

        /// <summary>
        /// Case.CustomFields[SAFETYUSED]
        /// </summary>
        [Display(Name = "SAFETYUSED", Order = 10)]
        public string SafetyUsed { get => TryGetValue("SafetyUsed", out string val) ? val : null; set => this["SafetyUsed"] = value; }

        /// <summary>
        /// Case.WorkRelated.WhereOccurred
        /// </summary>
        [Display(Name = "INCIDENTLOCATION", Order = 11)]
        public string IncidentLocation { get => TryGetValue("IncidentLocation", out string val) ? val : null; set => this["IncidentLocation"] = value; }

        /// <summary>
        /// Case.WorkRelated.TypeOfInjury.ToString().SplitCamelCaseString()
        /// </summary>
        [Display(Name = "CAUSETYPE", Order = 12)]
        public string CauseType { get => TryGetValue("CauseType", out string val) ? val : null; set => this["CauseType"] = value; }

        /// <summary>
        /// Case.WorkRelated.WhatHappened
        /// </summary>
        [Display(Name = "INJOCCURED", Order = 13)]
        public string InjuryOccurred { get => TryGetValue("InjuryOccurred", out string val) ? val : null; set => this["InjuryOccurred"] = value; }

        /// <summary>
        /// Case.WorkRelated.WhatHappened
        /// </summary>
        [Display(Name = "UNSAFEACT", Order = 14)]
        public string UnsafeAct { get => TryGetValue("UnsafeAct", out string val) ? val : null; set => this["UnsafeAct"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 15)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

        /// <summary>
        /// Case.WorkRelated.WhatHarmedTheEmployee
        /// </summary>
        [Display(Name = "TYPE", Order = 16)]
        public string Type { get => TryGetValue("Type", out string val) ? val : null; set => this["Type"] = value; }

        /// <summary>
        /// Case.WorkRelated ? "Y" : "N"
        /// </summary>
        [Display(Name = "WORKRELATED", Order = 17)]
        public string IsWorkRelated { get => TryGetValue("IsWorkRelated", out string val) ? val : null; set => this["IsWorkRelated"] = value; }

    }
}
