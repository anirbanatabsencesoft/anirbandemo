﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class Contact : BaseEntity<Contact>
    {
        /// <summary>
        /// Case.AssignedToId.HashCode()?
        /// </summary>
        [Display(Name = "CONTACT_ID", Order = 1)]
        public string ContactId { get => TryGetValue("ContactId", out string val) ? val : null; set => this["ContactId"] = value; }

        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 2)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =3)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.AssignedTo.LastName
        /// </summary>
        [Display(Name = "LNAME", Order = 4)]
        public string LastName { get => TryGetValue("LastName", out string val) ? val : null; set => this["LastName"] = value; }

        /// <summary>
        /// Case.AssignedTo.DisplayName
        /// </summary>
        [Display(Name = "USERID", Order = 5)]
        public string UserId { get => TryGetValue("UserId", out string val) ? val : null; set => this["UserId"] = value; }

        /// <summary>
        /// Case.AssignedTo.JobTitle
        /// </summary>
        [Display(Name = "TITLE", Order = 6)]
        public string Title { get => TryGetValue("Title", out string val) ? val : null; set => this["Title"] = value; }

        /// <summary>
        /// Case.AssignedTo.IsDisabled
        /// </summary>
        [Display(Name = "ACTIVE", Order = 7)]
        public string IsActive { get => TryGetValue("IsActive", out string val) ? val : null; set => this["IsActive"] = value; }

        /// <summary>
        /// Case.AssignedTo.FirstName
        /// </summary>
        [Display(Name = "FNAME", Order = 8)]
        public string FirstName { get => TryGetValue("FirstName", out string val) ? val : null; set => this["FirstName"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 9)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

    }
}
