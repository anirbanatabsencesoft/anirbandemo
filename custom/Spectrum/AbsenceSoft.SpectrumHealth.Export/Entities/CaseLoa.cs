﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class CaseLoa : BaseEntity<CaseLoa>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Set to: Case.Reason.Code
        /// </summary>
        [Display(Name = "LEAVEREASON", Order = 3)]
        public string LeaveReason { get => TryGetValue("LeaveReason", out string val) ? val : null; set => this["LeaveReason"] = value; }

        /// <summary>
        /// Set to: Case.Reason.Code
        /// </summary>
        [Display(Name = "LEAVEFOR", Order = 4)]
        public string LeaveFor { get => TryGetValue("LeaveFor", out string val) ? val : null; set => this["LeaveFor"] = value; }

        /// <summary>
        /// Set to: Case.Metadata.Hospitalized
        /// </summary>
        [Display(Name = "HOSPITALIZED", Order = 5)]
        public string Hospitalized { get => TryGetValue("Hospitalized", out string val) ? val : null; set => this["Hospitalized"] = value; }

        /// <summary>
        /// Set to: Case.GetEventDate
        /// </summary>
        [Display(Name = "ADMITDATE", Order = 6)]
        public string AdmitDate { get => TryGetValue("AdmitDate", out string val) ? val : null; set => this["AdmitDate"] = value; }

        /// <summary>
        /// Set to: Case.GetEventDate
        /// </summary>
        [Display(Name = "DISCHARGEDATE", Order = 7)]
        public string DischargeDate { get => TryGetValue("DischargeDate", out string val) ? val : null; set => this["DischargeDate"] = value; }

        /// <summary>
        /// Set to: Case.Employee.WorkSchedule.ToString()
        /// </summary>
        [Display(Name = "DAYSWORKED", Order = 8)]
        public string DaysWorked { get => TryGetValue("DaysWorked", out string val) ? val : null; set => this["DaysWorked"] = value; }

        /// <summary>
        /// Case.Employee.WorkSchedule > Calc Hours Scheduled on Average per Day
        /// </summary>
        [Display(Name = "HOURSWORKEDDAY", Order = 9)]
        public string HoursWorkedDay { get => TryGetValue("HoursWorkedDay", out string val) ? val : null; set => this["HoursWorkedDay"] = value; }

        /// <summary>
        /// Case.Employee.WorkSchedule > Calc Hours Scheduled on Average per Week. 
        /// There may already be methods for this in LeaveOfAbsence.cs; under Logic.Cases
        /// </summary>
        [Display(Name = "HOURSWORKEDWEEK", Order = 10)]
        public string HoursWorkedWeek { get => TryGetValue("HoursWorkedWeek", out string val) ? val : null; set => this["HoursWorkedWeek"] = value; }

        /// <summary>
        /// Case.Employee.WorkSchedule > Calc Hours Scheduled on Average per Year
        /// </summary>
        [Display(Name = "HOURSACTUALYEAR", Order = 11)]
        public string HoursActualYear { get => TryGetValue("HoursActualYear", out string val) ? val : null; set => this["HoursActualYear"] = value; }

        /// <summary>
        /// Employee.CustomFields
        /// Employee Custom Field : "CURRENTPTO"
        /// </summary>
        [Display(Name = "PTOBALANCE", Order = 12)]
        public string PTOBalance { get => TryGetValue("PTOBalance", out string val) ? val : null; set => this["PTOBalance"] = value; }

        /// <summary>
        /// Case.Status
        /// </summary>
        [Display(Name = "STATUS", Order = 13)]
        public string Status { get => TryGetValue("Status", out string val) ? val : null; set => this["Status"] = value; }

        /// <summary>
        /// Case.Status check, Case.EventDates for ActualRTW
        /// </summary>
        [Display(Name = "OUTCOME", Order = 14)]
        public string Outcome { get => TryGetValue("Outcome", out string val) ? val : null; set => this["Outcome"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATEDBY", Order = 15)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 16)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 17)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 18)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

        /// <summary>
        /// Employee.CustomFields
        /// Employee Custom Field : "FTE"
        /// </summary>
        [Display(Name = "HOURSWORKEDFTEVALUE", Order = 19)]
        public string HoursWorkedFTEValue { get => TryGetValue("HoursWorkedFTEValue", out string val) ? val : null; set => this["HoursWorkedFTEValue"] = value; }
    }
}
