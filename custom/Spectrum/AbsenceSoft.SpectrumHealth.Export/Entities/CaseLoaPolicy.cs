﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class CaseLoaPolicy : BaseEntity<CaseLoaPolicy>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyCrosswalkCodes(type of PAYCODE)
        /// </summary>
        [Display(Name = "NAME", Order = 3)]
        public string Name { get => TryGetValue("Name", out string val) ? val : null; set => this["Name"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.Policy.Name
        /// </summary>
        [Display(Name = "CLASS", Order = 4)]
        public string PolicyClass { get => TryGetValue("PolicyClass", out string val) ? val : null; set => this["PolicyClass"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.Paid, 
        /// AppliedPolicies.Policy.PolicyType == PolicyType.FMLA or STateFML (then job protected)
        /// </summary>
        [Display(Name = "TYPE", Order = 5)]
        public string PolicyType { get => TryGetValue("PolicyType", out string val) ? val : null; set => this["PolicyType"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.WorkState ?? Case.Segments.AppliedPolicies.Policy.WorkState
        /// </summary>
        [Display(Name = "JURISDICTION", Order = 6)]
        public string Jurisdiction { get => TryGetValue("Jurisdiction", out string val) ? val : null; set => this["Jurisdiction"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.PeriodType
        /// </summary>
        [Display(Name = "LEAVEYEARCALC", Order = 7)]
        public string LeaveYearCacl { get => TryGetValue("LeaveYearCacl", out string val) ? val : null; set => this["LeaveYearCacl"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.EntitlementType
        /// </summary>
        [Display(Name = "UNIT", Order = 8)]
        public string Unit { get => TryGetValue("Unit", out string val) ? val : null; set => this["Unit"] = value; }
        
        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.Entitlement (converted)
        /// </summary>
        [Display(Name = "MAXBENEFIT", Order = 9)]
        public string MaxBenefit { get => TryGetValue("MaxBenefit", out string val) ? val : null; set => this["MaxBenefit"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.EliminationPeriod when Excliusive in Days
        /// </summary>
        [Display(Name = "WAITINGPERIOD", Order = 10)]
        public string WaitingPeriod { get => TryGetValue("WaitingPeriod", out string val) ? val : null; set => this["WaitingPeriod"] = value; }

        /// <summary>
        /// Case.Employee.StartDayOfWeek
        /// </summary>
        [Display(Name = "WORKWEEKBEGIN", Order = 11)]
        public string WorkWeekBegin { get => TryGetValue("WorkWeekBegin", out string val) ? val : null; set => this["WorkWeekBegin"] = value; }

        /// <summary>
        /// Hard coded, always send "clock number"
        /// </summary>
        [Display(Name = "UNIQUEBY", Order = 12)]
        public string UniqueBy { get => TryGetValue("UniqueBy", out string val) ? val : null; set => this["UniqueBy"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATEDBY", Order = 13)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 14)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 15)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 16)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.PeriodType == PerOccurrance, 
        /// then "per incident" otherwise "per request"
        /// </summary>
        [Display(Name = "WAITINGPERIODAPPLIES", Order = 17)]
        public string WaitingPeriodApplies { get => TryGetValue("WaitingPeriodApplies", out string val) ? val : null; set => this["WaitingPeriodApplies"] = value; }

        /// <summary>
        /// AT doesn't currently support waiving waiting periods; this would always be N
        /// </summary>
        [Display(Name = "WAITINGPERIODWAIVED", Order = 18)]
        public string WaitingPeriodWaived { get => TryGetValue("WaitingPeriodWaived", out string val) ? val : null; set => this["WaitingPeriodWaived"] = value; }
    }
}
