﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class VrptLoaCertificateUsage : BaseEntity<VrptLoaCertificateUsage>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Employee.FullName
        /// </summary>
        [Display(Name = "EMPLOYEE", Order = 3)]
        public string Employee { get => TryGetValue("Employee", out string val) ? val : null; set => this["Employee"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[2PMCOMPANYNAME]
        /// </summary>
        [Display(Name = "COMPANY", Order = 4)]
        public string Company { get => TryGetValue("Company", out string val) ? val : null; set => this["Company"] = value; }

        /// <summary>
        /// Case.WorkRelated.IllnessOrInjuryDate
        /// </summary>
        [Display(Name = "DATEOFINJURY", Order = 5)]
        public string DateOfInjury { get => TryGetValue("DateOfInjury", out string val) ? val : null; set => this["DateOfInjury"] = value; }

        /// <summary>
        /// Case.Status
        /// </summary>
        [Display(Name = "STATUS", Order = 6)]
        public string Status { get => TryGetValue("Status", out string val) ? val : null; set => this["Status"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[PROCESSLEVEL]
        /// </summary>
        [Display(Name = "PROCESSLEVEL", Order = 7)]
        public string ProcessLevel { get => TryGetValue("ProcessLevel", out string val) ? val : null; set => this["ProcessLevel"] = value; }

        /// <summary>
        /// Case.Employee.JobTitle
        /// </summary>
        [Display(Name = "OCCUPATION", Order = 8)]
        public string Occupation { get => TryGetValue("Occupation", out string val) ? val : null; set => this["Occupation"] = value; }

        /// <summary>
        /// Employee Number
        /// </summary>
        [Display(Name = "CLOCKNUMBER", Order = 9)]
        public string ClockNumber { get => TryGetValue("ClockNumber", out string val) ? val : null; set => this["ClockNumber"] = value; }

        /// <summary>
        /// Case.StartDate - days to last weekday
        /// </summary>
        [Display(Name = "LDW", Order = 10)]
        public string LDW { get => TryGetValue("LDW", out string val) ? val : null; set => this["LDW"] = value; }

        /// <summary>
        /// Case.Segments[Type in Reduced, Intermittent].StartDate
        /// </summary>
        [Display(Name = "DRW_RESTR", Order = 11)]
        public string DRW_RESTR { get => TryGetValue("DRW_RESTR", out string val) ? val : null; set => this["DRW_RESTR"] = value; }

        /// <summary>
        /// Case.CaseEvents[ActualRTW] ?? Case.CaseEvents[EstimatedRTW]
        /// </summary>
        [Display(Name = "DRW_FULL", Order = 12)]
        public string DRW_FULL { get => TryGetValue("DRW_FULL", out string val) ? val : null; set => this["DRW_FULL"] = value; }

        /// <summary>
        /// If ActualRTW null then E, otherwise A
        /// </summary>
        [Display(Name = "DRW_ACT_EST", Order = 13)]
        public string DRW_ACT_EST { get => TryGetValue("DRW_ACT_EST", out string val) ? val : null; set => this["DRW_ACT_EST"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyCrosswalkCodes[PAYCODE] ?? Case.Segments.AppliedPolicies.Policy.Code
        /// </summary>
        [Display(Name = "CLASS", Order = 14)]
        public string Class { get => TryGetValue("Class", out string val) ? val : null; set => this["Class"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyReason.EntitlementType
        /// AT Case Usage Native Unit, H = Work Weeks, H = Working Hours, D = Calendar Weeks, 
        /// D = Calendar Days, D = Calendar Months, D = Calendar Years, H = Working Months, 
        /// H = Work Days, NULL = Reasonable Period (unlimited)
        /// </summary>
        [Display(Name = "UNIT", Order = 15)]
        public string Unit { get => TryGetValue("Unit", out string val) ? val : null; set => this["Unit"] = value; }

        /// <summary>
        /// Case.Status
        /// </summary>
        [Display(Name = "LOASTATUS", Order = 16)]
        public string LOAStatus { get => TryGetValue("LOAStatus", out string val) ? val : null; set => this["LOAStatus"] = value; }

        /// <summary>
        /// Employee.CustomFields[CURRENTPTO]
        /// </summary>
        [Display(Name = "PTOBALANCE", Order = 17)]
        public string PTOBalance { get => TryGetValue("PTOBalance", out string val) ? val : null; set => this["PTOBalance"] = value; }

        /// <summary>
        /// Case.Segments.CaseType
        /// </summary>
        [Display(Name = "LEAVETYPE", Order = 18)]
        public string LeaveType { get => TryGetValue("LeaveType", out string val) ? val : null; set => this["LeaveType"] = value; }

        /// <summary>
        /// Case.Segments.StartDate
        /// </summary>
        [Display(Name = "BENEFITFROM", Order = 19)]
        public string BenefitFromDate { get => TryGetValue("BenefitFromDate", out string val) ? val : null; set => this["BenefitFromDate"] = value; }

        /// <summary>
        /// Case.Segments.EndDate
        /// </summary>
        [Display(Name = "BENEFITTHRU", Order = 20)]
        public string BenefitToDate { get => TryGetValue("BenefitToDate", out string val) ? val : null; set => this["BenefitToDate"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CCREATEBY", Order = 21)]
        public string CaseCreatedBy { get => TryGetValue("CaseCreatedBy", out string val) ? val : null; set => this["CaseCreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "CCDATECREATED", Order = 22)]
        public string CaseCreatedDate { get => TryGetValue("CaseCreatedDate", out string val) ? val : null; set => this["CaseCreatedDate"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName 
        /// </summary>
        [Display(Name = "CCHANGEDBY", Order = 23)]
        public string CaseChangedBy { get => TryGetValue("CaseChangedBy", out string val) ? val : null; set => this["CaseChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "CDATECHANGED", Order = 24)]
        public string CaseChageDate { get => TryGetValue("CaseChageDate", out string val) ? val : null; set => this["CaseChageDate"] = value; }

        /// <summary>
        /// Case.Reason.Code
        /// </summary>
        [Display(Name = "REASON", Order = 25)]
        public string Reason { get => TryGetValue("Reason", out string val) ? val : null; set => this["Reason"] = value; }

        /// <summary>
        /// Case.Certifications.StartDate (first)
        /// </summary>
        [Display(Name = "CERTIFIEDDATE", Order = 26)]
        public string CertifiedDate { get => TryGetValue("CertifiedDate", out string val) ? val : null; set => this["CertifiedDate"] = value; }

        /// <summary>
        /// Case.WorkRelated.Provider.Contact.CompanmyName || FullName
        /// </summary>
        [Display(Name = "CERTIFIEDBY", Order = 27)]
        public string CertifiedBy { get => TryGetValue("CertifiedBy", out string val) ? val : null; set => this["CertifiedBy"] = value; }

        /// <summary>
        /// Logic.Cases.CaseService.GetCasePrimaryProviderContact(Case) > Contact.CompanyName ?? Contact.FullName
        /// </summary>
        [Display(Name = "CERTIFIEDBY_ID", Order = 28)]
        public string CertifiedById { get => TryGetValue("CertifiedById", out string val) ? val : null; set => this["CertifiedById"] = value; }

        /// <summary>
        /// Case.AssignedTo.DisplayName
        /// </summary>
        [Display(Name = "LEADCASEMANAGER", Order = 29)]
        public string LeadCaseManager { get => TryGetValue("LeadCaseManager", out string val) ? val : null; set => this["LeadCaseManager"] = value; }

        /// <summary>
        /// Case.Employee.Info.OfficeLocation
        /// </summary>
        [Display(Name = "LOCATIONNAME", Order = 30)]
        public string LocationName { get => TryGetValue("LocationName", out string val) ? val : null; set => this["LocationName"] = value; }
    }
}
