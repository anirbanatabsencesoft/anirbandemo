﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class WorkInfo : BaseEntity<WorkInfo>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENO", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name = "EMPLOYEENUMBER", Order = 2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[PROCESSLEVEL]
        /// </summary>
        [Display(Name = "PROCESSLEVEL", Order = 3)]
        public string ProcessLevel { get => TryGetValue("ProcessLevel", out string val) ? val : null; set => this["ProcessLevel"] = value; }

        /// <summary>
        /// Case.Employee.CustomField[COSTCENTER]
        /// </summary>
        [Display(Name = "COSTCENTER", Order = 4)]
        public string CostCenter { get => TryGetValue("CostCenter", out string val) ? val : null; set => this["CostCenter"] = value; }

        /// <summary>
        /// Case.Employee.JobTitle
        /// </summary>
        [Display(Name = "OCCUPATION", Order = 5)]
        public string Occupation { get => TryGetValue("Occupation", out string val) ? val : null; set => this["Occupation"] = value; }

        /// <summary>
        /// Case.Employee.HireDate
        /// </summary>
        [Display(Name = "DATEHIRED", Order = 6)]
        public string DateHired { get => TryGetValue("DateHired", out string val) ? val : null; set => this["DateHired"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[2PMFIRSTNAME] + " " + Case.Employee.CustomFields[2PMLASTNAME]
        /// </summary>
        [Display(Name = "CONTACT", Order = 7)]
        public string Contact { get => TryGetValue("Contact", out string val) ? val : null; set => this["Contact"] = value; }

        /// <summary>
        /// Case.Employee.Info.WorkPhone
        /// </summary>
        [Display(Name = "WORKPHONENUMBER", Order = 8)]
        public string WorkPhoneNumber { get => TryGetValue("WorkPhoneNumber", out string val) ? val : null; set => this["WorkPhoneNumber"] = value; }

        /// <summary>
        /// NULL
        /// </summary>
        [Display(Name = "EXT", Order = 9)]
        public string Ext { get => TryGetValue("Ext", out string val) ? val : null; set => this["Ext"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[EMPLOYEESTATUS]
        /// </summary>
        [Display(Name = "WSTATUS", Order = 10)]
        public string WStatus { get => TryGetValue("WStatus", out string val) ? val : null; set => this["WStatus"] = value; }

        /// <summary>
        /// Case.Employee.Salary
        /// </summary>
        [Display(Name = "PAYRATE", Order = 11)]
        public string PayRate { get => TryGetValue("PayRate", out string val) ? val : null; set => this["PayRate"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[UNION]
        /// </summary>
        [Display(Name = "UNIONAFFILIATE", Order = 12)]
        public string UnionAffiliate { get => TryGetValue("UnionAffiliate", out string val) ? val : null; set => this["UnionAffiliate"] = value; }

        /// <summary>
        /// Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name = "CLOCKNUMBER", Order = 13)]
        public string ClockNumber { get => TryGetValue("ClockNumber", out string val) ? val : null; set => this["ClockNumber"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 14)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

        /// <summary>
        /// NULL
        /// </summary>
        [Display(Name = "SOC_ID", Order = 15)]
        public string SOCId { get => TryGetValue("SOCId", out string val) ? val : null; set => this["SOCId"] = value; }

        /// <summary>
        /// Case.Employee.Salary
        /// </summary>
        [Display(Name = "AVGWAGE", Order = 16)]
        public string AverageWage { get => TryGetValue("AverageWage", out string val) ? val : null; set => this["AverageWage"] = value; }

        /// <summary>
        /// Employee.Status (from Employee collection)
        /// </summary>
        [Display(Name = "EMPLOYMENTSTATUS", Order = 17)]
        public string EmploymentStatus { get => TryGetValue("EmploymentStatus", out string val) ? val : null; set => this["EmploymentStatus"] = value; }

        /// <summary>
        /// Employee.ModifiedDate (from Employee collection)
        /// </summary>
        [Display(Name = "EMPLOYMENTSTATUSDATE", Order = 18)]
        public string EmploymentStatusDate { get => TryGetValue("EmploymentStatusDate", out string val) ? val : null; set => this["EmploymentStatusDate"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[INTEGRATIONDATE]
        /// </summary>
        [Display(Name = "INTEGRATIONDATE", Order = 19)]
        public string IntegrationDate { get => TryGetValue("IntegrationDate", out string val) ? val : null; set => this["IntegrationDate"] = value; }

    }
}
