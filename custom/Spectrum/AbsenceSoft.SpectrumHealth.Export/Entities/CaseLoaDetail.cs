﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class CaseLoaDetail : BaseEntity<CaseLoaDetail>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyCrosswalkCodes(type of PAYCODE)
        /// </summary>
        [Display(Name = "POLICYCODE", Order = 3)]
        public string PolicyCode { get => TryGetValue("PolicyCode", out string val) ? val : null; set => this["PolicyCode"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.Usage
        /// </summary>
        [Display(Name = "POLICY_DATE", Order = 4)]
        public string PolicyDate { get => TryGetValue("PolicyDate", out string val) ? val : null; set => this["PolicyDate"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.Usage.MinutesUsed / 60
        /// </summary>
        [Display(Name = "VALUE", Order = 5)]
        public string Value { get => TryGetValue("Value", out string val) ? val : null; set => this["Value"] = value; }
                
        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATEDBY", Order = 6)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 7)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 8)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 9)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

    }
}
