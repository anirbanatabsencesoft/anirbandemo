﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class CaseLoaRequest : BaseEntity<CaseLoaRequest>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.PolicyCrosswalkCodes(type of PAYCODE)
        /// </summary>
        [Display(Name = "NAME", Order = 3)]
        public string Name { get => TryGetValue("Name", out string val) ? val : null; set => this["Name"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.StartDate
        /// </summary>
        [Display(Name = "FROMDATE", Order = 4)]
        public string FromDate { get => TryGetValue("FromDate", out string val) ? val : null; set => this["FromDate"] = value; }

        /// <summary>
        /// Case.Segments.AppliedPolicies.EndDate
        /// </summary>
        [Display(Name = "THRUDATE", Order = 5)]
        public string ToDate { get => TryGetValue("ToDate", out string val) ? val : null; set => this["ToDate"] = value; }

        /// <summary>
        /// Case.Segments.CaseType
        /// </summary>
        [Display(Name = "LEAVETYPE", Order = 6)]
        public string LeaveType { get => TryGetValue("LeaveType", out string val) ? val : null; set => this["LeaveType"] = value; }

        /// <summary>
        /// Case.Segments.LeaveSchedule
        /// </summary>
        [Display(Name = "REDUCEDSCHEDULE", Order = 7)]
        public string ReducedSchedule { get => TryGetValue("ReducedSchedule", out string val) ? val : null; set => this["ReducedSchedule"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATEDBY", Order = 8)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 9)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 10)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 11)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }
        
    }
}
