﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class Rtj : BaseEntity<Rtj>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.CaseEvents[ActualRTW] ?? Case.CaseEvents[EstimatedRTW]
        /// </summary>
        [Display(Name = "DATERETURNEDTOWORK", Order = 3)]
        public string DateOfRTW { get => TryGetValue("DateOfRTW", out string val) ? val : null; set => this["DateOfRTW"] = value; }

        /// <summary>
        /// Case.CaseEvents[ActualRTW]
        /// </summary>
        [Display(Name = "FULLDUTYRETURNEDTOWORK", Order = 4)]
        public string DateToFullDutyRTW { get => TryGetValue("DateToFullDutyRTW", out string val) ? val : null; set => this["DateToFullDutyRTW"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 5)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

    }
}
