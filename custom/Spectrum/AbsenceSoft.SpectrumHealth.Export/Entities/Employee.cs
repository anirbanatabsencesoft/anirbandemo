﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class Employee : BaseEntity<Employee>
    {
        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name = "EMPLOYEENUMBER", Order = 1)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Employee.CustomField[2PMCOMPANYNAME]
        /// </summary>
        [Display(Name = "COMPANY", Order = 2)]
        public string Company { get => TryGetValue("Company", out string val) ? val : null; set => this["Company"] = value; }

        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENO", Order = 3)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Case.Employee.LastName
        /// </summary>
        [Display(Name = "LNAME", Order = 4)]
        public string LastName { get => TryGetValue("LastName", out string val) ? val : null; set => this["LastName"] = value; }

        /// <summary>
        /// NULL
        /// </summary>
        [Display(Name = "SUFFIX", Order = 5)]
        public string Suffix { get => TryGetValue("Suffix", out string val) ? val : null; set => this["Suffix"] = value; }

        /// <summary>
        /// Case.Empoyee.FirstName
        /// </summary>
        [Display(Name = "FNAME", Order = 6)]
        public string FirstName { get => TryGetValue("FirstName", out string val) ? val : null; set => this["FirstName"] = value; }

        /// <summary>
        /// Case.Employee.MiddleName
        /// </summary>
        [Display(Name = "MIDDLE", Order = 7)]
        public string MiddleName { get => TryGetValue("MiddleName", out string val) ? val : null; set => this["MiddleName"] = value; }

        /// <summary>
        /// Case.WorkRelated.IllnessOrInjuryDate
        /// </summary>
        [Display(Name = "DATEOFINJURY", Order = 8)]
        public string DateOfInjury { get => TryGetValue("DateOfInjury", out string val) ? val : null; set => this["DateOfInjury"] = value; }

        /// <summary>
        /// Case.Employee.Ssn.Encrypted.Decrypt()
        /// </summary>
        [Display(Name = "SSN", Order = 9)]
        public string SSN { get => TryGetValue("SSN", out string val) ? val : null; set => this["SSN"] = value; }

        /// <summary>
        /// Case.CustomFields[TOTALCOST]
        /// </summary>
        [Display(Name = "TCOST", Order = 10)]
        public string TotalCost { get => TryGetValue("TotalCost", out string val) ? val : null; set => this["TotalCost"] = value; }

        /// <summary>
        /// Case.CustomFields[NATUREOTHER]
        /// </summary>
        [Display(Name = "NATURE_OTHER", Order = 11)]
        public string NatureOther { get => TryGetValue("NatureOther", out string val) ? val : null; set => this["NatureOther"] = value; }

        /// <summary>
        /// Case.CustomFields[DESCRIPTION]
        /// </summary>
        [Display(Name = "BODYPART", Order = 12)]
        public string BodyPart { get => TryGetValue("BodyPart", out string val) ? val : null; set => this["BodyPart"] = value; }

        /// <summary>
        /// Case.CustomFields[DEGREE]
        /// </summary>
        [Display(Name = "DEGREE", Order = 13)]
        public string Degree { get => TryGetValue("Degree", out string val) ? val : null; set => this["Degree"] = value; }

        /// <summary>
        /// Case.Status
        /// Case Status: Open, Closed, Requested (=Pending), Inquiry (=Pending)
        /// </summary>
        [Display(Name = "STATUS", Order = 14)]
        public string Status { get => TryGetValue("Status", out string val) ? val : null; set => this["Status"] = value; }

        /// <summary>
        /// Case.Description
        /// </summary>
        [Display(Name = "MEMO", Order = 15)]
        public string Memo { get => TryGetValue("Memo", out string val) ? val : null; set => this["Memo"] = value; }

        /// <summary>
        /// Case.CustomFields[BODYPARTS]
        /// </summary>
        [Display(Name = "WC_BODYPART", Order = 16)]
        public string WcBodyPart { get => TryGetValue("WcBodyPart", out string val) ? val : null; set => this["WcBodyPart"] = value; }

        /// <summary>
        /// Case.WorkRelated.DaysAwayFromWork
        /// </summary>
        [Display(Name = "CONLOSTWORK", Order = 17)]
        public string DaysAwayFromWork { get => TryGetValue("DaysAwayFromWork", out string val) ? val : null; set => this["DaysAwayFromWork"] = value; }

        /// <summary>
        /// Case.WorkRelated.DaysOnJobTransferOrRestriction
        /// </summary>
        [Display(Name = "CONRESTRICTEDDUTY", Order = 18)]
        public string RestrictedDuty { get => TryGetValue("RestrictedDuty", out string val) ? val : null; set => this["RestrictedDuty"] = value; }

        /// <summary>
        /// Case.CaseEvents[ActualRTWDate]
        /// </summary>
        [Display(Name = "DATERETURNEDTOWORK", Order = 19)]
        public string RTWDate { get => TryGetValue("RTWDate", out string val) ? val : null; set => this["RTWDate"] = value; }

        /// <summary>
        /// Case.CaseEvents[EstimatedRTWDate]
        /// </summary>
        [Display(Name = "ESTIMATEDRETURNTOWORK", Order = 20)]
        public string EstimatedRTWDate { get => TryGetValue("EstimatedRTWDate", out string val) ? val : null; set => this["EstimatedRTWDate"] = value; }
        
        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATOR", Order = 21)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 22)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 23)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 24)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }

        /// <summary>
        /// Case.Metadata.GetRawValue<bool>("IsWorkRelated") ? "O" : "N"
        /// </summary>
        [Display(Name = "CASE_TYPE", Order = 25)]
        public string CaseType { get => TryGetValue("CaseType", out string val) ? val : null; set => this["CaseType"] = value; }

        /// <summary>
        /// Case.CurrentOfficeLocation
        /// </summary>
        [Display(Name = "OFFICELOCATION", Order = 26)]
        public string OfficeLocation { get => TryGetValue("OfficeLocation", out string val) ? val : null; set => this["OfficeLocation"] = value; }

        /// <summary>
        /// Case.CustomFields[DEGREETYPE]
        /// </summary>
        [Display(Name = "LEAVETYPE", Order = 27)]
        public string LeaveType { get => TryGetValue("LeaveType", out string val) ? val : null; set => this["LeaveType"] = value; }

        /// <summary>
        /// Case.CustomFields[NATUREILLNESS]
        /// </summary>
        [Display(Name = "NATURE_ILLNESS", Order = 28)]
        public string NatureOfIllness { get => TryGetValue("NatureOfIllness", out string val) ? val : null; set => this["NatureOfIllness"] = value; }

        /// <summary>
        /// Case.CustomFields[WCNATURE]
        /// </summary>
        [Display(Name = "WC_NATURE_ILLNESS", Order = 29)]
        public string WcNatureOfIllness { get => TryGetValue("WcNatureOfIllness", out string val) ? val : null; set => this["WcNatureOfIllness"] = value; }

        /// <summary>
        /// Case.CustomFields[NATURE]
        /// </summary>
        [Display(Name = "WC_NATURE_INJURY", Order = 30)]
        public string WcNatureOfInjury { get => TryGetValue("WcNatureOfInjury", out string val) ? val : null; set => this["WcNatureOfInjury"] = value; }

        /// <summary>
        /// Case.CustomFields[INJBODYSIDE]
        /// </summary>
        [Display(Name = "BODYSIDE", Order = 31)]
        public string BodySide { get => TryGetValue("BodySide", out string val) ? val : null; set => this["BodySide"] = value; }

        /// <summary>
        /// Case.Employee.CustomFields[CLASSIFICATION]
        /// </summary>
        [Display(Name = "CLASSIFICATION", Order = 32)]
        public string Classification { get => TryGetValue("Classification", out string val) ? val : null; set => this["Classification"] = value; }
    }
}
