﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class Diagnosis : BaseEntity<Diagnosis>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.Disability.ConditionStartDate
        /// </summary>
        [Display(Name = "DIAGNOSISDATE", Order = 3)]
        public string DiagnosisDate { get => TryGetValue("DiagnosisDate", out string val) ? val : null; set => this["DiagnosisDate"] = value; }

        /// <summary>
        /// We will always send "primary"
        /// </summary>
        [Display(Name = "DIAGNOSISTYPE", Order = 4)]
        public string DiagnosisType { get => TryGetValue("DiagnosisType", out string val) ? val : null; set => this["DiagnosisType"] = value; }

        /// <summary>
        /// Case.Disability.PrimaryDiagnosis.Code
        /// </summary>
        [Display(Name = "ICD_CODE", Order = 5)]
        public string ICD { get => TryGetValue("ICD", out string val) ? val : null; set => this["ICD"] = value; }

        /// <summary>
        /// Case.Disability.PrimaryDiagnosis.Name
        /// </summary>
        [Display(Name = "DIAGNOSISDESCR", Order = 6)]
        public string DiagnosisDescription { get => TryGetValue("DiagnosisDescription", out string val) ? val : null; set => this["DiagnosisDescription"] = value; }

        /// <summary>
        /// Case.Disability.GeneralHealthCondition
        /// </summary>
        [Display(Name = "COMMENTS", Order = 7)]
        public string Comments { get => TryGetValue("Comments", out string val) ? val : null; set => this["Comments"] = value; }

        /// <summary>
        /// Case.CreatedBy.DisplayName
        /// </summary>
        [Display(Name = "CREATOR", Order = 9)]
        public string CreatedBy { get => TryGetValue("CreatedBy", out string val) ? val : null; set => this["CreatedBy"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATECREATED", Order = 10)]
        public string DateCreated { get => TryGetValue("DateCreated", out string val) ? val : null; set => this["DateCreated"] = value; }

        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 11)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 8)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }
        
    }
}
