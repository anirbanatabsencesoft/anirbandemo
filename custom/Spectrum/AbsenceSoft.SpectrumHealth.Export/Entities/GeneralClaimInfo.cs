﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class GeneralClaimInfo : BaseEntity<GeneralClaimInfo>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENO", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name = "EMPLOYEENUMBER", Order = 2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATEREPORTED", Order = 3)]
        public string DateReported { get => TryGetValue("DateReported", out string val) ? val : null; set => this["DateReported"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "TIMEREPORTED", Order = 4)]
        public string TimeReported { get => TryGetValue("TimeReported", out string val) ? val : null; set => this["TimeReported"] = value; }

        /// <summary>
        /// Case.WorkRelated.DateOfDeath
        /// </summary>
        [Display(Name = "DATEOFDEATH", Order = 5)]
        public string DateOfDeath { get => TryGetValue("DateOfDeath", out string val) ? val : null; set => this["DateOfDeath"] = value; }

        /// <summary>
        /// Case.WorkRelated.Reportable
        /// </summary>
        [Display(Name = "OSHARECORDABLE", Order = 6)]
        public string IsOSHAReportable { get => TryGetValue("IsOSHAReportable", out string val) ? val : null; set => this["IsOSHAReportable"] = value; }

        /// <summary>
        /// Case.WorkRelated.DaysAwayFromWork
        /// </summary>
        [Display(Name = "OSHALOSTWORK", Order = 7)]
        public string DaysAwayFromWork { get => TryGetValue("DaysAwayFromWork", out string val) ? val : null; set => this["DaysAwayFromWork"] = value; }

        /// <summary>
        /// Case.WorkRelated.DaysOnJobTransferOrRestriction
        /// </summary>
        [Display(Name = "OSHARESTRICTEDDUTY", Order = 8)]
        public string OSHARestrictedDuty { get => TryGetValue("OSHARestrictedDuty", out string val) ? val : null; set => this["OSHARestrictedDuty"] = value; }

        /// <summary>
        /// Case.CreatedDate
        /// </summary>
        [Display(Name = "DATERECORDED", Order = 9)]
        public string DateRecorded { get => TryGetValue("DateRecorded", out string val) ? val : null; set => this["DateRecorded"] = value; }

        /// <summary>
        /// Case.WorkRelated.TimeEmployeeBeganWork.ToString()
        /// </summary>
        [Display(Name = "TIMEBEGANWORK", Order = 10)]
        public string TimeBeganAtWork { get => TryGetValue("TimeBeganAtWork", out string val) ? val : null; set => this["TimeBeganAtWork"] = value; }

        /// <summary>
        /// Case.StartDate.AddDays(-(n days from last weekday))
        /// </summary>
        [Display(Name = "LASTDAYWORKED", Order = 11)]
        public string LastDayWorked { get => TryGetValue("LastDayWorked", out string val) ? val : null; set => this["LastDayWorked"] = value; }

        /// <summary>
        /// Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 12)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }
        

    }
}
