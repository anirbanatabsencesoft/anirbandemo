﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class AssignedCaseManager : BaseEntity<AssignedCaseManager>
    {
        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =1)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 2)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.AssignedTo.EmployeeId > Employee > Employee.EmployeeNumber
        /// </summary>
        [Display(Name = "CMIDNO", Order = 3)]
        public string CMIDNo { get => TryGetValue("CMIDNo", out string val) ? val : null; set => this["CMIDNo"] = value; }

        /// <summary>
        /// Set to: Case.CreatedDate
        /// </summary>
        [Display(Name = "DATEASSIGNED", Order = 4)]
        public string DateAssigned { get => TryGetValue("DateAssigned", out string val) ? val : null; set => this["DateAssigned"] = value; }

        /// <summary>
        /// Always set the value as Y
        /// </summary>
        [Display(Name = "LEADMANAGER", Order = 5), DefaultValue("Y")]
        public string LeadManager { get => TryGetValue("LeadManager", out string val) ? val : null; set => this["LeadManager"] = value; }

        /// <summary>
        /// Set to: Case.ModifiedDate
        /// </summary>
        [Display(Name = "DATECHANGED", Order = 6)]
        public string DateChanged { get => TryGetValue("DateChanged", out string val) ? val : null; set => this["DateChanged"] = value; }
    }
}
