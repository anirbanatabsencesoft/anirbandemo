﻿using AbsenceSoft.SpectrumHealth.Export.Common;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Entities
{
    public class CaseStatus : BaseEntity<CaseStatus>
    {
        /// <summary>
        /// Set to: Case.CaseNumber
        /// </summary>
        [Display(Name = "CASENUMBER", Order = 1)]
        public string CaseNumber { get => TryGetValue("CaseNumber", out string val) ? val : null; set => this["CaseNumber"] = value; }

        /// <summary>
        /// Set to: Case.Employee.EmployeeNumber
        /// </summary>
        [Display(Name ="EMPLOYEENUMBER", Order =2)]
        public string EmployeeNumber { get => TryGetValue("EmployeeNumber", out string val) ? val : null; set => this["EmployeeNumber"] = value; }

        /// <summary>
        /// DateTime.Date within Case.StartDate and Case.EndDate….. 
        /// </summary>
        [Display(Name = "STATUSDATE", Order = 3)]
        public string StatusDate { get => TryGetValue("StatusDate", out string val) ? val : null; set => this["StatusDate"] = value; }

        /// <summary>
        /// Case.Status == Open, then "O", else "C"
        /// </summary>
        [Display(Name = "CASEACTION", Order = 4)]
        public string CaseAction { get => TryGetValue("CaseAction", out string val) ? val : null; set => this["CaseAction"] = value; }

        /// <summary>
        /// "NEW CASE" if Open, otherwise "RETURN TO WORK" OR "TERMINATED" 
        /// if Closed, where  Actual RTW is null for Terminated, if not null then RTW. 
        /// Actual RTW date is in Case.EventDAtes
        /// </summary>
        [Display(Name = "REASON", Order = 5)]
        public string Reason { get => TryGetValue("Reason", out string val) ? val : null; set => this["Reason"] = value; }
               
        /// <summary>
        /// Case.ModifiedBy.DisplayName
        /// </summary>
        [Display(Name = "CHANGEDBY", Order = 7)]
        public string ChangedBy { get => TryGetValue("ChangedBy", out string val) ? val : null; set => this["ChangedBy"] = value; }
        
    }
}
