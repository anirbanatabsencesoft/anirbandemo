﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    public abstract class BaseEntity<T> : IDictionary<string, string> where T : BaseEntity<T>, new()
    {
        /// <summary>
        /// The properties
        /// </summary>
        private Dictionary<string, string> _properties = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        [Display(Name ="ACTION", Order =0)]
        public string Action { get => _properties.TryGetValue("Action", out string val) ? val : null; set => _properties["Action"] = value; }
        
        /// <summary>
        /// Set the action for each entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void SetAction<TEntity>(Data.BaseEntity<TEntity> entity) where TEntity : Data.BaseEntity<TEntity>, new()
        {
            if (ExecutionData.Instance.LastSynced == DateTime.MinValue && entity.IsDeleted == false)
                Action = "C";
            else
            {
                if (entity.CreatedDate > ExecutionData.Instance.LastSynced)
                    Action = "C";
                else if (entity.IsDeleted == true)
                    Action = "D";
                else
                    Action = "U";
            }
        }

        #region IDictionary<string, string>
        /// <summary>
        /// Gets or sets the <see cref="string"/> with the specified key.
        /// </summary>
        /// <value>
        /// The <see cref="string"/>.
        /// </value>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string this[string key] { get => ((IDictionary<string, string>)_properties)[key]; set => ((IDictionary<string, string>)_properties)[key] = value; }

        /// <summary>
        /// Gets the keys.
        /// </summary>
        /// <value>
        /// The keys.
        /// </value>
        public ICollection<string> Keys => ((IDictionary<string, string>)_properties).Keys;

        /// <summary>
        /// Gets the values.
        /// </summary>
        /// <value>
        /// The values.
        /// </value>
        public ICollection<string> Values => ((IDictionary<string, string>)_properties).Values;

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count => ((IDictionary<string, string>)_properties).Count;

        /// <summary>
        /// Gets a value indicating whether this instance is read only.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly => ((IDictionary<string, string>)_properties).IsReadOnly;

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add(string key, string value)
        {
            ((IDictionary<string, string>)_properties).Add(key, value);
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Add(KeyValuePair<string, string> item)
        {
            ((IDictionary<string, string>)_properties).Add(item);
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            ((IDictionary<string, string>)_properties).Clear();
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(KeyValuePair<string, string> item)
        {
            return ((IDictionary<string, string>)_properties).Contains(item);
        }

        /// <summary>
        /// Determines whether the specified key contains key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if the specified key contains key; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKey(string key)
        {
            return ((IDictionary<string, string>)_properties).ContainsKey(key);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
        {
            ((IDictionary<string, string>)_properties).CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return ((IDictionary<string, string>)_properties).GetEnumerator();
        }

        /// <summary>
        /// Removes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return ((IDictionary<string, string>)_properties).Remove(key);
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public bool Remove(KeyValuePair<string, string> item)
        {
            return ((IDictionary<string, string>)_properties).Remove(item);
        }

        /// <summary>
        /// Tries the get value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool TryGetValue(string key, out string value)
        {
            return ((IDictionary<string, string>)_properties).TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<string, string>)_properties).GetEnumerator();
        }
        #endregion
    }
}
