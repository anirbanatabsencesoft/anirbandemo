﻿using AbsenceSoft.Data.Enums;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    internal class UsageSummary
    {
        /// <summary>
        /// Gets or sets the date used.
        /// </summary>
        /// <value>
        /// The date used.
        /// </value>
        public DateTime DateUsed { get; set; }

        /// <summary>
        /// Gets or sets the interval.
        /// </summary>
        /// <value>
        /// The interval.
        /// </value>
        public CaseType Interval { get; set; }

        /// <summary>
        /// Gets or sets the type of entitlement for the policy.
        /// </summary>
        /// <value>
        /// The entitlement type.
        /// </value>
        public EntitlementType? EntitlementType { get; set; }

        /// <summary>
        /// Gets or sets the policy code.
        /// </summary>
        /// <value>
        /// The policy code.
        /// </value>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets the minutes used.
        /// </summary>
        /// <value>
        /// The minutes used.
        /// </value>
        public double MinutesUsed { get; set; }

        /// <summary>
        /// Gets or sets the minutes in day.
        /// </summary>
        /// <value>
        /// The minutes in day.
        /// </value>
        public double MinutesInDay { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public AdjudicationStatus Determination { get; set; }

        /// <summary>
        /// Gets or sets the denial reason.
        /// </summary>
        /// <value>
        /// The denial reason.
        /// </value>
        public string DenialReason { get; set; }
    }
}
