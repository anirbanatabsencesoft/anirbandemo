﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    public abstract class BaseExecutionHandler<T> : IExecutionHandler, IDisposable where T : BaseEntity<T>, new()
    {
        private Task _task = null;
        private ExecutionData _executaionData = null;
        private Settings _settings = null;
        private string _exportFilePath = null;
        private List<FileColumn> _columns = null;
        Stream _file = null;
        TextWriter _writer = null;

        /// <summary>
        /// Gets the name of this handler or the handler that inherits this one.
        /// </summary>
        public virtual string Name { get { return _task?.ExportFileName ?? GetType().Name; } }

        public void Init(Task ExecutionTask, ExecutionData Data, Settings AppSettings)
        {
            //set the variables first
            _task = ExecutionTask;
            _executaionData = Data;
            _settings = AppSettings;
            _exportFilePath = Path.Combine(_settings.TempDirectory, _task.ExportFileName);
            _columns = Utilities.GetColumns<T>();
            _file = new FileStream(_exportFilePath, FileMode.Create, FileAccess.Write, FileShare.Read);
            _writer = TextWriter.Synchronized(new StreamWriter(_file));
            Utilities.WriteHeader<T>(_writer, _columns);
        }

        /// <summary>
        /// Processes the specified execution task.
        /// </summary>
        /// <param name="case">The case.</param>
        public virtual void Process(Case @case)
        {
            GetData(@case, data => data.WriteItem(_writer, _columns));
        }

        /// <summary>
        /// Processes all.
        /// </summary>
        /// <param name="cases">The cases.</param>
        public virtual void ProcessAll(List<Case> cases)
        {
            cases.AsParallel().ForAll(c => GetData(c, data => data.WriteItem(_writer, _columns)));
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <param name="emit">The emit.</param>
        protected abstract void GetData(Case c, Action<T> emit);
                
        /// <summary>
        /// Copy to local or network drive
        /// </summary>
        /// <param name="filePath"></param>
        private void CopyToDrive(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Copy(filePath, _settings.OutputPath + Path.DirectorySeparatorChar + Path.GetFileName(filePath), true);
            }
            else
                Log.Error("File not found");
        }
        
        /// <summary>
        /// Export the data to CSV
        /// </summary>
        private void ExportData()
        {
            try
            {
                CopyToDrive(_exportFilePath);
            }
            catch (Exception ex)
            {
                Log.Error("Error while copying to DRIVE", ex);
            }
        }
        
        /// <summary>
        /// Fixes up the usage and returns a prettier picture more accurate to
        /// how Spectrum uses our data on their file feeds.
        /// </summary>
        /// <param name="case">The case.</param>
        /// <returns></returns>
        internal List<UsageSummary> FixUpUsage(Case @case, CaseSegment segment = null)
        {
            List<UsageSummary> summaries = new List<UsageSummary>();

            if (@case.Segments != null)
            {
                foreach (var s in @case.Segments.Where(r => r.Id == (segment?.Id ?? r.Id)))
                {
                    if (s.AppliedPolicies != null)
                    {
                        foreach (var p in s.AppliedPolicies)
                        {
                            if (p.Usage != null)
                            {
                                foreach (var u in p.Usage)
                                {
                                    // Add the final usage summary first, then suppliment/adjust accordingly for today
                                    summaries.AddFluid(new UsageSummary()
                                    {
                                        DateUsed = u.DateUsed,
                                        PolicyCode = p.PolicyCrosswalkCodes?.FirstOrDefault(x => x.Code == "PAYCODE")?.Code ?? p.Policy?.Code,
                                        MinutesUsed = u.MinutesUsed,
                                        MinutesInDay = u.MinutesInDay,
                                        Determination = u.Determination,
                                        DenialReason = u.DenialReasonCode,
                                        Interval = s.Type,
                                        EntitlementType = p.PolicyReason?.EntitlementType
                                    });
                                }
                            }
                        }
                    }
                }
            }

            return summaries;
        }

        /// <summary>
        /// Gets the custom field value.
        /// </summary>
        /// <param name="fields">The fields.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        protected string GetCustomFieldValue(List<CustomField> fields, string code)
        {
            return fields.Where(p => p.Code == code && !string.IsNullOrWhiteSpace(p.SelectedValueText))
                .Select(p => p.SelectedValueText)
                .FirstOrDefault();
        }

        #region IDisposable Support
        /// <summary>
        /// The disposed value
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _writer?.Flush();
                    _writer?.Dispose();
                    _file?.Dispose();
                    ExportData();
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                _columns = null;
                _writer = null;
                _file = null;
                disposedValue = true;
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
