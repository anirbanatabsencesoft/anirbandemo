﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    public static class Utilities
    {
        /// <summary>
        /// Returns XML string for an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToXmlString<T>(this T input)
        {
            using (var writer = new StringWriter())
            {
                new XmlSerializer(typeof(T)).Serialize(writer, input);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Returns object from XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlInput"></param>
        /// <returns></returns>
        public static T ToObject<T>(this string xmlInput)
        {
            using (var reader = new StringReader(xmlInput))
            {
                return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
            }
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<FileColumn> GetColumns<T>()
        {
            //Get the properties for type T for the headers
            return typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.GetCustomAttribute<DisplayAttribute>() != null)
                .Select(p => new FileColumn()
                {
                    Key = p.Name,
                    Order = p.GetCustomAttribute<DisplayAttribute>().Order,
                    Header = p.GetCustomAttribute<DisplayAttribute>().Name ?? p.Name
                })
                .OrderBy(p => p.Order)
                .ToList();
        }

        /// <summary>
        /// Writes the header.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer">The writer.</param>
        public static void WriteHeader<T>(TextWriter writer, List<FileColumn> columns) where T : BaseEntity<T>, new()
        {
            writer.WriteLine(string.Join(",", columns.Select(c => string.Concat("\"", c.Header, "\""))));
        }

        /// <summary>
        /// Writes the item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="columns">The columns.</param>
        public static void WriteItem<T>(this T item, TextWriter writer, List<FileColumn> columns) where T : BaseEntity<T>, new()
        {
            StringBuilder row = new StringBuilder();
            foreach (var col in columns)
            {
                if (item.TryGetValue(col.Key, out string value))
                {
                    // If it's null or just empty whitespace then no need to do all of this jazz
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        // If contains a double quote, escape the double quote with double quotes
                        if (value.Contains("\""))
                            value = value.Replace("\"", "\"\"");

                        // Build the list of stuff that has to be encapsulated with field double quotes
                        var quoteables = new[] 
                        {
                            Environment.NewLine,
                            "\r",
                            "\n",
                            "\"",
                            ","
                        };

                        // If contains a comma, wrap it in double quotes
                        if (quoteables.Any(q => value.Contains(q)))
                            value = string.Concat("\"", value, "\"");
                    }

                    row.Append(value);
                }
                row.Append(",");
            }
            writer.WriteLine(row.ToString());
            row = null;
        }
        
        /// <summary>
        /// Extension method to split camel case
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetText(this System.Enum en)
        {
            if (en == null)
                return "";
            var input = en.ToString();
            return System.Text.RegularExpressions.Regex.Replace(input, "([A-Z])", " $1", System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.Singleline).Trim();
        }

        /// <summary>
        /// Extension method for code reduction
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateTimeString(this DateTime? dt)
        {
            if (dt == null)
                return string.Empty;
            else
                return dt.ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// To the date time string.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public static string ToDateTimeString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// Extension method for code reduction
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateString(this DateTime? dt)
        {
            if (dt == null)
                return string.Empty;
            else
                return dt.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// To the date string.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public static string ToDateString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd");
        }
    }
}
