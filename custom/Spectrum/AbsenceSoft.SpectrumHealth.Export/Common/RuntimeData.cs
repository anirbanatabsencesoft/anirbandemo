﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Workflows;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    internal static class RuntimeData
    {
        /// <summary>
        /// Gets or sets the employees.
        /// </summary>
        /// <value>
        /// The employees.
        /// </value>
        public static List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        public static List<User> Users { get; set; }

        /// <summary>
        /// Gets or sets the reopen events.
        /// </summary>
        /// <value>
        /// The reopen events.
        /// </value>
        public static List<Event> ReopenEvents { get; set; }

        /// <summary>
        /// Gets or sets the providers.
        /// </summary>
        /// <value>
        /// The providers.
        /// </value>
        public static List<EmployeeContact> Providers { get; set; }

        /// <summary>
        /// Gets or sets the orgs.
        /// </summary>
        /// <value>
        /// The orgs.
        /// </value>
        public static List<Organization> Orgs { get; set; }

        /// <summary>
        /// Gets or sets the employee orgs.
        /// </summary>
        /// <value>
        /// The employee orgs.
        /// </value>
        public static List<EmployeeOrganization> EmployeeOrgs { get; set; }

        /// <summary>
        /// Gets or sets the employee jobs.
        /// </summary>
        /// <value>
        /// The employee jobs.
        /// </value>
        public static List<EmployeeJob> EmployeeJobs { get; set; }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        public static void Load()
        {
            Parallel.Invoke(
                LoadEmployees,
                LoadUsers,
                LoadReopenEvents,
                LoadProviders,
                LoadOrganizations,
                LoadEmployeeOrganizations,
                LoadEmployeeJobs
            );
        }

        /// <summary>
        /// Loads the employees.
        /// </summary>
        private static void LoadEmployees()
        {
            Employees = Employee.Repository.Collection.Find(Query.And(
                Event.Query.EQ(e => e.CustomerId, ExecutionData.Instance.CustomerId),
                Event.Query.EQ(e => e.EmployerId, ExecutionData.Instance.EmployerId),
                Event.Query.NE(e => e.IsDeleted, true)
            )).ToList();
        }

        /// <summary>
        /// Loads the users.
        /// </summary>
        private static void LoadUsers()
        {
            Users = User.Repository.Collection.Find(
                User.Query.EQ(u => u.CustomerId, ExecutionData.Instance.CustomerId)
            ).ToList();
        }

        /// <summary>
        /// Loads the reopen events.
        /// </summary>
        private static void LoadReopenEvents()
        {
            ReopenEvents = Event.Repository.Collection.Find(Query.And(
                Event.Query.EQ(e => e.CustomerId, ExecutionData.Instance.CustomerId),
                Event.Query.EQ(e => e.EmployerId, ExecutionData.Instance.EmployerId),
                Event.Query.NE(e => e.IsDeleted, true),
                Event.Query.EQ(e => e.EventType, EventType.CaseReopened)
            )).ToList();
        }

        /// <summary>
        /// Loads the providers.
        /// </summary>
        private static void LoadProviders()
        {
            List<string> contactTypeCodes = ContactType.Repository.Collection.Find(Query.And(
                ContactType.Query.EQ(e => e.CustomerId, ExecutionData.Instance.CustomerId),
                ContactType.Query.EQ(e => e.EmployerId, ExecutionData.Instance.EmployerId),
                ContactType.Query.NE(e => e.IsDeleted, true),
                ContactType.Query.EQ(c => c.ContactCategory, ContactTypeDesignationType.Medical)))
                .Select(c => c.Code)
                .ToList();
            Providers = EmployeeContact.Repository.Collection.Find(Query.And(
                EmployeeContact.Query.EQ(c => c.CustomerId, ExecutionData.Instance.CustomerId),
                EmployeeContact.Query.EQ(c => c.EmployerId, ExecutionData.Instance.EmployerId),
                EmployeeContact.Query.NE(c => c.IsDeleted, true),
                EmployeeContact.Query.In(c => c.ContactTypeCode, contactTypeCodes),
                EmployeeContact.Query.EQ(c => c.Contact.IsPrimary, true)
            )).ToList();
        }

        /// <summary>
        /// Loads the organizations.
        /// </summary>
        private static void LoadOrganizations()
        {
            Orgs = Organization.Repository.Collection.Find( Query.And(
                Organization.Query.EQ(o => o.CustomerId, ExecutionData.Instance.CustomerId),
                Organization.Query.EQ(o => o.EmployerId, ExecutionData.Instance.EmployerId),
                Organization.Query.NE(o => o.IsDeleted, true),
                Organization.Query.NE(o => o.TypeCode, OrganizationType.OfficeLocationTypeCode)
            )).ToList();
        }

        /// <summary>
        /// Loads the employee organizations.
        /// </summary>
        private static void LoadEmployeeOrganizations()
        {
            EmployeeOrgs = EmployeeOrganization.Repository.Collection.Find(Query.And(
                EmployeeOrganization.Query.EQ(e => e.CustomerId, ExecutionData.Instance.CustomerId),
                EmployeeOrganization.Query.EQ(e => e.EmployerId, ExecutionData.Instance.EmployerId),
                EmployeeOrganization.Query.NE(e => e.IsDeleted, true),
                EmployeeOrganization.Query.In(e => e.TypeCode, new[]
                {
                    "HEAD PROCESS LEVEL",
                    "DEPARTMENT"
                }),
                Query.Or(
                    EmployeeOrganization.Query.NotExists(e => e.Dates),
                    EmployeeOrganization.Query.EQ(e => e.Dates, null),
                    EmployeeOrganization.Query.LTE(e => e.Dates.StartDate, DateTime.Today.ToMidnight())
                ),
                Query.Or(
                    EmployeeOrganization.Query.NotExists(e => e.Dates),
                    EmployeeOrganization.Query.EQ(e => e.Dates, null),
                    EmployeeOrganization.Query.EQ(e => e.Dates.EndDate, null),
                    EmployeeOrganization.Query.GTE(e => e.Dates.EndDate, DateTime.Today.ToMidnight())
                )
            )).ToList();
        }

        /// <summary>
        /// Loads the employee jobs.
        /// </summary>
        public static void LoadEmployeeJobs()
        {
            EmployeeJobs = EmployeeJob.Repository.Collection.Find(Query.And(
                EmployeeJob.Query.EQ(j => j.CustomerId, ExecutionData.Instance.CustomerId),
                EmployeeJob.Query.EQ(j => j.EmployerId, ExecutionData.Instance.EmployerId),
                EmployeeJob.Query.NE(j => j.IsDeleted, true)
            )).ToList();
        }

        /// <summary>
        /// Gets the employee.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public static Employee GetEmployee(string employeeId)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
                return null;
            return Employees?.FirstOrDefault(e => e.Id == employeeId)
                ?? Employee.GetById(employeeId);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static User GetUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return null;
            return Users?.FirstOrDefault(u => u.Id == userId)
                ?? User.GetById(userId);
        }

        /// <summary>
        /// Gets the last reopen date.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public static Event GetLastReopenDate(string caseId)
        {
            if (string.IsNullOrWhiteSpace(caseId))
                return null;
            return ReopenEvents?
                .OrderByDescending(e => e.CreatedDate)
                .FirstOrDefault(e => e.CaseId == caseId);
        }

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public static EmployeeContact GetProvider(string employeeId)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
                return null;
            return Providers?.FirstOrDefault(p => p.EmployeeId == employeeId);
        }

        /// <summary>
        /// Gets the employee organizations.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <returns></returns>
        public static List<EmployeeOrganization> GetEmployeeOrganizations(string employeeNumber)
        {
            if (string.IsNullOrWhiteSpace(employeeNumber))
                return new List<EmployeeOrganization>(0);
            return EmployeeOrgs?
                .Where(e => string.Equals(e.EmployeeNumber, employeeNumber, StringComparison.InvariantCultureIgnoreCase))
                .ToList();
        }

        /// <summary>
        /// Gets the organization.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static Organization GetOrganization(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return null;
            return Orgs?.FirstOrDefault(o => o.Path == path);
        }

        /// <summary>
        /// Gets the employee job.
        /// </summary>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static EmployeeJob GetEmployeeJob(string employeeNumber, DateTime startDate, DateTime? endDate)
        {
            var jobs = EmployeeJobs?.Where(j => j.EmployeeNumber == employeeNumber).ToList();

            return jobs.FirstOrDefault(j => j.Dates.DateRangesOverLap(startDate, endDate)) ??
                jobs.FirstOrDefault(j => j.Dates.DateInRange(DateTime.UtcNow)) ??
                jobs.OrderBy(j => j.Dates.StartDate).FirstOrDefault();
        }
    }
}
