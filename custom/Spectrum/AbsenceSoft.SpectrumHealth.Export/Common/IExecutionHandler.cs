﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    /// <summary>
    /// Common interface for execution handlers to strongly type execution
    /// vs. using a dynamic runtime binder (exception bubbling)
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IExecutionHandler : IDisposable
    {
        /// <summary>
        /// Gets the name of this handler or the handler that inherits this one.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Initializes the specified execution task.
        /// </summary>
        /// <param name="ExecutionTask">The execution task.</param>
        /// <param name="Data">The data.</param>
        /// <param name="AppSettings">The application settings.</param>
        void Init(Task ExecutionTask, ExecutionData Data, Settings AppSettings);

        /// <summary>
        /// Processes the specified execution handler task for this case.
        /// </summary>
        /// <param name="case">The case.</param>
        void Process(Case @case);

        /// <summary>
        /// Processes all of the passed in cases using this wrapped execution task.
        /// </summary>
        /// <param name="cases">The cases.</param>
        void ProcessAll(List<Case> cases);
    }
}
