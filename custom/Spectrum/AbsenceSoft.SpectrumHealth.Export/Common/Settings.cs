﻿using System.IO;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    public class Settings
    {
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string TempDirectory { get; set; }
        public string DataDirectory { get; set; }
        public string OutputPath { get; set; }

        /// <summary>
        /// Constructor to set the data from config file
        /// </summary>
        public Settings()
        {
            CustomerId = System.Configuration.ConfigurationManager.AppSettings["CustomerId"];
            EmployerId = System.Configuration.ConfigurationManager.AppSettings["EmployerId"];
            OutputPath = System.Configuration.ConfigurationManager.AppSettings["OutputPath"];

            //set local directories
            TempDirectory = System.AppDomain.CurrentDomain.BaseDirectory + "\\tmp";
            DataDirectory = System.AppDomain.CurrentDomain.BaseDirectory + "\\data";

            //create local directories if not exists
            if (!Directory.Exists(TempDirectory))
                Directory.CreateDirectory(TempDirectory);
            if (!Directory.Exists(DataDirectory))
                Directory.CreateDirectory(DataDirectory);
            if (!Directory.Exists(OutputPath))
                Directory.CreateDirectory(OutputPath);
        }
    }
}
