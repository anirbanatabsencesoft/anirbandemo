﻿using System;
using System.IO;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    [Serializable]
    public class ExecutionData
    {
        public DateTime LastSynced { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }

        private ExecutionData() { }
        public static ExecutionData Instance
        {
            get
            {
                var settings = new Settings();
                ExecutionData instance = null;
                try
                {
                    instance = File.ReadAllText(settings.DataDirectory + "\\executiondata.xml").ToObject<ExecutionData>();
                }
                catch { }

                //if null create default
                if (instance == null || instance.CustomerId != settings.CustomerId || instance.CustomerId != settings.EmployerId)
                    instance = new ExecutionData()
                    {
                        CustomerId = settings.CustomerId,
                        EmployerId = settings.EmployerId,
                        LastSynced = DateTime.MinValue
                    };

                return instance;
            }
        } 
    }
}
