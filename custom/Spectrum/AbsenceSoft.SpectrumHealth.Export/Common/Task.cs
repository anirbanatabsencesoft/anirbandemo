﻿using System;
using System.Xml.Serialization;

namespace AbsenceSoft.SpectrumHealth.Export.Common
{
    [Serializable]
    public class Task
    {
        public string ClassName { get; set; }
        public string ExportPrefix { get; set; }

        [XmlIgnore]
        public string ExportFileName { get { return string.Concat(ExportPrefix, ".csv"); } }
    }
}
