﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using MongoDB.Driver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AbsenceSoft.SpectrumHealth.Export
{
    static class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            //Handle all exceptions
            AppDomain.CurrentDomain.UnhandledException += HandleExceptions;
            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();

            Console.WriteLine("Loading settings and task list...");
            //Get the settings
            var settings = new Settings();

            //Get the tasks
            var tasks = File.ReadAllText(settings.DataDirectory + "\\tasks.xml").ToObject<List<Common.Task>>();
            tasks.ForEach(t => Console.WriteLine("Found export task '{0}' to be output to '{1}'", t.ClassName, t.ExportFileName));

            //Get the last sync execution data
            ExecutionData lastSynced = ExecutionData.Instance;
            Console.WriteLine("Done loading settings and task list");

            //cleanup old data
            try
            {
                Console.WriteLine("Clearing temp data directory...");
                Directory.GetFiles(settings.TempDirectory).AsParallel().ForAll(f =>
                {
                    Console.WriteLine("Deleting temporary file: '{0}'...", f);
                    File.Delete(f);
                    Console.WriteLine("Successfully deleted temporary file: '{0}'", f);
                });
                Console.WriteLine("Done clearing temp data directory");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error deleting temp directory: {ex}");
            }

            // Load our runtime data
            Console.WriteLine("Loading runtime data, employee population, users, etc...");
            RuntimeData.Load();
            Console.WriteLine("Done loading runtime data");

            //get the current time for next operation
            var dt = DateTime.UtcNow;

            //Start Log
            Console.WriteLine("Building handlers...");
            List<IExecutionHandler> handlers = new List<IExecutionHandler>();
            tasks.ForEach(t =>
            {
                Console.WriteLine("Creating object instance of '{0}' for file '{1}'...", t.ClassName, t.ExportFileName);
                //process
                IExecutionHandler handler = null;
                try
                {
                    handler = (IExecutionHandler)Activator.CreateInstance(Type.GetType(t.ClassName));
                    Console.WriteLine("Successfully created object instance of '{0}' for file '{1}'", t.ClassName, t.ExportFileName);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error creating instance of handler for file '{0}': {1}", t.ExportFileName, ex.ToString());
                    Log.Error(ex);
                }

                if (handler != null)
                {
                    Console.WriteLine("Initializing handler for file '{0}'...", t.ExportFileName);
                    handler.Init(t, lastSynced, settings);
                    Console.WriteLine("Successfully initialized handler for file '{0}'", t.ExportFileName);
                    handlers.Add(handler);
                    Console.WriteLine("Added handler for file '{0}' to processing list", t.ExportFileName);
                }
            });
            Console.WriteLine("Done building handlers");

            ConcurrentQueue<Case> queue = new ConcurrentQueue<Case>();
            
            // Get the case list
            Console.WriteLine("Processing cases...");
            bool done = false;
            Parallel.Invoke(
                () =>
                {
                    Console.WriteLine("Reading cases for processing...");
                    Case.Repository.Collection.Find(GetCaseQuery(lastSynced.CustomerId, lastSynced.EmployerId)).SetFlags(QueryFlags.NoCursorTimeout).ForEach(c =>
                    {
                        c.CreatedBy = RuntimeData.GetUser(c.CreatedById);
                        c.ModifiedBy = RuntimeData.GetUser(c.ModifiedById);
                        queue.Enqueue(c);
                        Console.WriteLine("Queued Case # {0} for export", c.CaseNumber);
                    });
                    Console.WriteLine("Done reading cases for processing");
                    done = true;
                },
                () =>
                {
                    while (!done || !queue.IsEmpty)
                    {
                        while (queue.TryDequeue(out Case result))
                        {
                            if (result == null)
                            {
                                continue;
                            }
                            Console.WriteLine("Processing case # {0}...", result.CaseNumber);
                            handlers.Where(h => h != null).AsParallel().ForAll(h =>
                            {
                                Console.WriteLine("Running handler '{0}' for case # {1}...", h.Name, result.CaseNumber);
                                h.Process(result);
                                Console.WriteLine("Exported case # {1} to '{0}'", h.Name, result.CaseNumber);
                            });
                            Console.WriteLine("Done processing case # {0}", result.CaseNumber);
                        }
                    }
                }
            );
            while (!queue.IsEmpty)
            {
                while (queue.TryDequeue(out Case result))
                {
                    if (result == null)
                    {
                        continue;
                    }
                    Console.WriteLine("Processing case # {0}...", result.CaseNumber);
                    handlers.Where(h => h != null).AsParallel().ForAll(h =>
                    {
                        Console.WriteLine("Running handler '{0}' for case # {1}...", h.Name, result.CaseNumber);
                        h.Process(result);
                        Console.WriteLine("Exported case # {1} to '{0}'", h.Name, result.CaseNumber);
                    });
                    Console.WriteLine("Done processing case # {0}", result.CaseNumber);
                }
            }
            Console.WriteLine("Done processing cases");

            Console.WriteLine("Disposing, clearing and cleaning up handlers (GC)...");
            handlers.Where(h => h != null).AsParallel().ForAll(h => h.Dispose());
            handlers.Clear();
            handlers = null;
            Console.WriteLine("Successfully disposed of and cleared handlers");

            //set the time for execution record
            lastSynced.LastSynced = dt;

            //save the data
            Console.WriteLine("Writing execution data...");
            File.WriteAllText(settings.DataDirectory + "\\executiondata.xml", lastSynced.ToXmlString());
            Console.WriteLine("Successfully wrote execution data to '{0}\\executiondata.xml'", settings.DataDirectory);

            //completion
            Console.WriteLine("All items successful");
            timer.Stop();
            Console.WriteLine("Finished in {0}", timer.Elapsed);
        }

        /// <summary>
        /// Handle all exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void HandleExceptions(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error(e.ExceptionObject);
            Console.Error.WriteLine(e.ExceptionObject?.ToString() ?? "ERROR!!!");
            // If this is an aggregate exception, unpack it's detail list and log each
            //  individual excpetion as well.
            if (e.ExceptionObject is AggregateException aggEx)
            {
                foreach (var ex in aggEx.InnerExceptions)
                {
                    Log.Error(ex);
                    Console.Error.WriteLine(ex?.ToString() ?? "ERROR!!!");
                }
            }
        }

        /// <summary>
        /// Returns the case query
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        static IMongoQuery GetCaseQuery(string customerId, string employerId)
        {
            return Case.Query.And(
                Case.Query.EQ(p => p.CustomerId, customerId),
                Case.Query.EQ(p => p.EmployerId, employerId),
                // Filter out deleted records per Spectrum request.
                Case.Query.NE(p => p.IsDeleted, true)
            );
        }
    }
}
