﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class GeneralClaimInfoHandler : BaseExecutionHandler<GeneralClaimInfo>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<GeneralClaimInfo> emit)
        {
            GeneralClaimInfo m = new GeneralClaimInfo();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            m.DateReported = c.CreatedDate.ToDateString();
            m.TimeReported = c.WorkRelated?.TimeOfEvent?.ToString();

            var wr = c.WorkRelated;
            if (wr != null)
            {
                m.DateOfDeath = wr.DateOfDeath.ToDateString();
                m.IsOSHAReportable = wr.Reportable ? "Y" : "N";
                m.DaysAwayFromWork = Convert.ToString(wr.DaysAwayFromWork);
                m.OSHARestrictedDuty = Convert.ToString(wr.DaysOnJobTransferOrRestriction);
                m.TimeBeganAtWork = wr.TimeEmployeeBeganWork.ToString();
            }

            m.DateRecorded = c.CreatedDate.ToDateString();
            m.LastDayWorked = c.StartDate.AddDays(-1).ToDateString();
            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
