﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseStatusHandler : BaseExecutionHandler<Entities.CaseStatus>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<Entities.CaseStatus> emit)
        {
            Entities.CaseStatus m = new Entities.CaseStatus();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            m.StatusDate = c.ModifiedDate.ToDateString();
            m.CaseAction = c.Status == CaseStatus.Open ? "O" : "C";
            if (c.Status == CaseStatus.Open)
            {
                var eventDate = RuntimeData.GetLastReopenDate(c.Id);
                if (eventDate == null)
                {
                    m.Reason = "NEW CASE";
                    m.StatusDate = (c.GetCaseEventDate(CaseEventType.CaseCreated)?.Date ?? c.CreatedDate).ToDateString();
                    m.ChangedBy = c.CreatedBy?.DisplayName;
                }
                else
                {
                    m.Reason = "REOPEN";
                    m.StatusDate = eventDate.CreatedDate.ToDateString();
                    m.ChangedBy = eventDate.CreatedBy?.DisplayName;
                }
            }
            else if (c.Status == CaseStatus.Closed)
            {
                if (c.GetCaseEventDate(CaseEventType.ReturnToWork) == null)
                {
                    m.Reason = "TERMINATED";
                }
                else
                {
                    m.Reason = "RETURN TO WORK";
                }
                m.StatusDate = (c.GetCaseEventDate(CaseEventType.CaseClosed)?.Date ?? c.ModifiedDate).ToDateString();
            }
            else if (c.Status == CaseStatus.Cancelled)
            {
                m.Reason = "CANCELLED";
                m.StatusDate = (c.GetCaseEventDate(CaseEventType.CaseCancelled)?.Date ?? c.ModifiedDate).ToDateString();
            }
            else if (c.Status == CaseStatus.Requested || c.Status == CaseStatus.Inquiry)
            {
                m.Reason = "INQUIRY";
                m.CaseAction = "I";
                m.StatusDate = (c.GetCaseEventDate(CaseEventType.CaseCreated)?.Date ?? c.CreatedDate).ToDateString();
                m.ChangedBy = c.CreatedBy?.DisplayName;
            }

            //audit cascading null assignment of presedence
            m.ChangedBy = m.ChangedBy ?? c.ModifiedBy?.DisplayName ?? User.System.DisplayName;

            emit(m);
        }
    }
}
