﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class InjuryDescriptionHandler : BaseExecutionHandler<InjuryDescription>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<InjuryDescription> emit)
        {
            InjuryDescription m = new InjuryDescription();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            var wr = c.WorkRelated;
            if (wr != null)
            {
                m.IncidentLocation = wr.InjuryLocation;
                m.IsWorkRelated = "Y";
            }
            else
            {
                m.IsWorkRelated = "N";
            }

            m.PreparedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
            m.Position = c.CreatedBy?.JobTitle;
            m.PrepareDate = c.CreatedDate.ToDateString();


            //Get custom fields
            var cf = c.CustomFields;
            if (cf != null)
            {
                m.Activity = GetCustomFieldValue(cf, "ACTIVITY");
                m.Type = GetCustomFieldValue(cf, "PARTTYPE");
                m.WProcess = GetCustomFieldValue(cf, "WPROCESS");
                m.SafetyEquipment = GetCustomFieldValue(cf, "SAFETYEQUIP");
                m.SafetyUsed = GetCustomFieldValue(cf, "SAFETYUSED");
                m.Cause = GetCustomFieldValue(cf, "CAUSE") ?? m.Cause;
                m.CauseType = GetCustomFieldValue(cf, "CAUSETYPE") ?? m.CauseType;
                m.UnsafeAct = GetCustomFieldValue(cf, "UNSAFEACT") ?? m.UnsafeAct;
                m.InjuryOccurred = GetCustomFieldValue(cf, "INJOCCURED") ?? m.InjuryOccurred;
            }

            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
