﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class DescriptionOfInjuryHandler : BaseExecutionHandler<DescriptionOfInjury>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<DescriptionOfInjury> emit)
        {
            DescriptionOfInjury m = new DescriptionOfInjury();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;
            m.Description = c.WorkRelated?.WhatHappened;
            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
