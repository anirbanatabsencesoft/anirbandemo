﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseLoaRequestHandler : BaseExecutionHandler<CaseLoaRequest>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<CaseLoaRequest> emit)
        {
            if (c.Segments != null)
            {
                foreach (var s in c.Segments)
                {
                    var usageGroups = FixUpUsage(c, s).GroupBy(u => u.PolicyCode);
                    foreach (var p in usageGroups)
                    {
                        CaseLoaRequest m = new CaseLoaRequest();
                        //set action
                        m.SetAction(c);

                        //set basic properties
                        m.EmployeeNumber = c.Employee.EmployeeNumber;
                        m.CaseNumber = c.CaseNumber;

                        //Policy info
                        m.Name = p.Key;

                        m.FromDate = p.Min(r => r.DateUsed).ToDateString();
                        // Per Katie at Spectrum, 
                        m.ToDate = p.Max(r => r.DateUsed).ToDateString();
                        m.LeaveType = s.Type.GetText();
                        m.ReducedSchedule = (s.LeaveSchedule != null && s.LeaveSchedule.Count > 0) ? s.LeaveSchedule.FirstOrDefault().ToString() : "";

                        //audit
                        m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                        m.DateCreated = c.CreatedDate.ToDateTimeString();
                        m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                        m.DateChanged = c.ModifiedDate.ToDateTimeString();

                        emit(m);
                    }
                }
            }
        }
    }
}
