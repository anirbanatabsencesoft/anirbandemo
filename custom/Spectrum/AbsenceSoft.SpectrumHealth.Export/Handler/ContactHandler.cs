﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class ContactHandler : BaseExecutionHandler<Contact>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<Contact> emit)
        {
            var assignedUser = User.GetById(c.AssignedToId);
            if (assignedUser != null)
            {
                Contact m = new Contact();
                //set action
                m.SetAction(c);

                //set basic properties
                m.ContactId = assignedUser.Id;
                m.EmployeeNumber = c.Employee.EmployeeNumber;
                m.CaseNumber = c.CaseNumber;

                m.LastName = assignedUser.LastName;
                m.UserId = assignedUser.DisplayName;
                m.Title = assignedUser.JobTitle;
                m.IsActive = assignedUser.IsDisabled ? "N" : "Y";
                m.FirstName = assignedUser.FirstName;
                m.DateChanged = c.ModifiedDate.ToDateTimeString();

                emit(m);
            }
        }
    }
}
