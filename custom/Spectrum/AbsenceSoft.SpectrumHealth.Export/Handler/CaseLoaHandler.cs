﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseLoaHandler : BaseExecutionHandler<CaseLoa>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<CaseLoa> emit)
        {
            CaseLoa m = new CaseLoa();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;
            m.LeaveReason = c.Reason?.Code ?? "";
            m.LeaveFor = c.Contact?.ContactTypeCode ?? "";
            m.Outcome = c.Status != Data.Enums.CaseStatus.Closed ? null :
                c.ClosureReason == Data.Enums.CaseClosureReason.Terminated ? "Terminated" : "RTW";

            //hospitalization related
            m.Hospitalized = Convert.ToString(c.Metadata.GetValue("Hospitalized", ""));
            var ad = c.GetCaseEventDate(Data.Enums.CaseEventType.HospitalAdmissionDate);
            var dd = c.GetCaseEventDate(Data.Enums.CaseEventType.HospitalReleaseDate);
            m.AdmitDate = ad != null ? ad.ToDateString() : string.Empty;
            m.DischargeDate = dd != null ? dd.ToDateString() : string.Empty;

            var emp = RuntimeData.GetEmployee(c.Employee.Id) ?? c.Employee;
            var s = emp.WorkSchedules;
            if (s != null)
            {
                try
                {
                    m.DaysWorked = ""; // not used right now, but need to leave it in the file.
                    var sum = s.Sum(p => p.Times.Sum(q => q.TotalMinutes));
                    var div = s.Sum(p => p.Times.Count(q => q.TotalMinutes.HasValue && q.TotalMinutes.Value > 0));
                    m.HoursWorkedDay = div == 0 ? "0" : (sum / (60 * div)).ToString();

                    // Make sure they're matching 1:1 from UI to this for Spectrum
                    var taggedHours = emp.PriorHours?.FirstOrDefault(p => p.Metadata.GetRawValue<string>("CaseNumber") == c.CaseNumber)?.HoursWorked
                        ?? c.Employee.PriorHours?.FirstOrDefault(p => p.Metadata.GetRawValue<string>("CaseNumber") == c.CaseNumber)?.HoursWorked;
                    var lastHours = emp.PriorHours?.Where(p => p.AsOf <= c.StartDate).LastOrDefault() ?? emp.PriorHours?.LastOrDefault();
                    var calcHours = emp.WorkSchedules == null || !emp.WorkSchedules.Any() ? null : new double?(LeaveOfAbsence.TotalHoursWorkedLast12Months(emp));

                    m.HoursActualYear = taggedHours?.ToString() ?? (lastHours == null ? calcHours?.ToString() ?? "" : lastHours.HoursWorked.ToString());

                    m.HoursWorkedWeek = LeaveOfAbsence.HoursWorkedPerWeek(c).ToString();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Unable to evaluate employee hours for EEID: {0}, Case #: {1}.", emp.EmployeeNumber, c.CaseNumber);
                }
            }

            //Get custom fields
            var cf = c.Employee.CustomFields;
            if (cf != null)
            {
                m.PTOBalance = GetCustomFieldValue(cf, "CURRENTPTO");
                m.HoursWorkedFTEValue = GetCustomFieldValue(cf, "TOTALFTE");
            }

            //Status
            m.Status = c.Status.ToString().ToLower();

            //set outcome
            if (c.Status == Data.Enums.CaseStatus.Closed)
            {
                if (c.Segments.All(r => r.Type == Data.Enums.CaseType.Administrative))
                {
                    m.Outcome = "Administrative";
                }
                else if (c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork) != null)
                {
                    m.Outcome = "RTW same job";
                }
                else
                {
                    m.Outcome = "Terminated";
                }
            }

            //audit
            m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
            m.DateCreated = c.CreatedDate.ToDateTimeString();
            m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
