﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class AssignedCaseManagerHandler : BaseExecutionHandler<AssignedCaseManager>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        /// <param name="c">The c.</param>
        /// <param name="emit">The emit.</param>
        protected override void GetData(Case c, Action<AssignedCaseManager> emit)
        {
            AssignedCaseManager m = new AssignedCaseManager();
            //set action
            m.SetAction(c);
            //set properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;
            var assignedTo = RuntimeData.GetUser(c.AssignedToId);
            if (assignedTo != null && !string.IsNullOrWhiteSpace(assignedTo.EmployeeId))
            {
                var at = RuntimeData.GetEmployee(assignedTo.EmployeeId);
                if (at != null)
                {
                    m.CMIDNo = at.EmployeeNumber;
                    m.LeadManager = "Y";
                }
            }
            m.DateAssigned = c.ModifiedDate.ToDateTimeString();
            m.DateChanged = c.ModifiedDate.ToDateTimeString();
            emit(m);
        }
    }
}
