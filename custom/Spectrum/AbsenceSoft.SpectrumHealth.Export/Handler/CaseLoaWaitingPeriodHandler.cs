﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseLoaWaitingPeriodHandler : BaseExecutionHandler<CaseLoaWaitingPeriod>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<CaseLoaWaitingPeriod> emit)
        {
            var usage = FixUpUsage(c);
            var deniedList = usage.Where(u => u.PolicyCode == "0075")
                .GroupBy(r => new { r.PolicyCode, DateUsed = r.DateUsed.ToDateString() })
                .ToList();

            foreach (var d in deniedList)
            {
                CaseLoaWaitingPeriod m = new CaseLoaWaitingPeriod();
                //set action
                m.SetAction(c);

                //set basic properties
                m.EmployeeNumber = c.Employee.EmployeeNumber;
                m.CaseNumber = c.CaseNumber;

                //Policy info
                m.Name = d.Key.PolicyCode;
                m.DateUsed = d.Key.DateUsed;

                //audit
                m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                m.DateCreated = c.CreatedDate.ToDateTimeString();
                m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                m.DateChanged = c.ModifiedDate.ToDateTimeString();

                emit(m);
            }
        }
    }
}
