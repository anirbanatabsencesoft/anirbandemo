﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class WorkInfoHandler : BaseExecutionHandler<WorkInfo>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<WorkInfo> emit)
        {
            WorkInfo m = new WorkInfo();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            // From that, get the current job for the case, or today, or just latest
            var currentJob = RuntimeData.GetEmployeeJob(m.EmployeeNumber, c.StartDate, c.EndDate);

            if (currentJob != null)
                m.Occupation = string.Concat(currentJob.JobCode, " ", currentJob.JobName);
            else
                m.Occupation = c.Employee.JobTitle;
            m.DateHired = c.Employee.HireDate.ToDateString();
            m.WorkPhoneNumber = c.Employee?.Info?.WorkPhone ?? "";
            m.Ext = "";
            m.PayRate = Convert.ToString(c.Employee.Salary);


            //Get custom fields
            var cf = c.Employee.CustomFields;
            if (cf != null)
            {
                m.ProcessLevel = GetCustomFieldValue(cf, "PROCESSLEVEL");
                m.CostCenter = GetCustomFieldValue(cf, "COSTCENTER");

                var cFname = GetCustomFieldValue(cf, "2PMFIRSTNAME");
                var cLname = GetCustomFieldValue(cf, "2PMLASTNAME");
                m.Contact = (cFname ?? "") + " " + (cLname ?? "");

                m.WStatus = GetCustomFieldValue(cf, "EMPLOYEESTATUS");
                m.UnionAffiliate = GetCustomFieldValue(cf, "UNION");

                m.EmploymentStatus = GetCustomFieldValue(cf, "EMPLOYEESTATUS");
                m.IntegrationDate = GetCustomFieldValue(cf, "INTEGRATIONDATE");
            }

            if (string.IsNullOrWhiteSpace(m.ProcessLevel) || string.IsNullOrWhiteSpace(m.CostCenter))
            {
                var orgs = RuntimeData.GetEmployeeOrganizations(c.Employee.EmployeeNumber);

                if (string.IsNullOrWhiteSpace(m.CostCenter))
                    m.CostCenter = orgs.FirstOrDefault(o => o.TypeCode == "DEPARTMENT")?.Code;

                if (string.IsNullOrWhiteSpace(m.ProcessLevel))
                    m.ProcessLevel = orgs.FirstOrDefault(o => o.TypeCode == "HEAD PROCESS LEVEL")?.Code;

                if (string.IsNullOrWhiteSpace(m.ProcessLevel))
                {
                    var dept = orgs.FirstOrDefault(o => o.TypeCode == "DEPARTMENT");
                    if (dept != null)
                    {
                        var hpl = RuntimeData.GetOrganization(dept.Path.GetParentPath());
                        m.ProcessLevel = hpl?.Code;
                    }
                }
            }

            m.ClockNumber = c.Employee.EmployeeNumber;

            m.DateChanged = c.ModifiedDate.ToDateTimeString();
            m.SOCId = "";
            m.AverageWage = Convert.ToString(c.Employee.Salary);

            var emp = Data.Customers.Employee.GetById(c.Employee.Id);

            //added the validation as in our db case exists with employee but not in employee collection
            if (emp != null)
            {
                m.EmploymentStatus = m.EmploymentStatus ?? (emp.Status == Data.Enums.EmploymentStatus.Active ? "Active" : "Inactive");
                m.EmploymentStatusDate = emp.ModifiedDate.ToDateString();
            }
            else
            {
                m.EmploymentStatus = "Inactive";
                m.EmploymentStatusDate = c.Employee.ModifiedDate.ToDateString();
            }

            emit(m);
        }
    }
}
