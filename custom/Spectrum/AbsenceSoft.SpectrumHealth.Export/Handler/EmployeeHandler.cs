﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class EmployeeHandler : BaseExecutionHandler<Entities.Employee>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<Entities.Employee> emit)
        {
            Entities.Employee m = new Entities.Employee();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            //Employee details
            m.LastName = c.Employee.LastName;
            m.FirstName = c.Employee.FirstName;
            m.MiddleName = c.Employee.MiddleName;

            m.DateOfInjury = (
                c.WorkRelated?.IllnessOrInjuryDate 
                ?? c.GetCaseEventDate(Data.Enums.CaseEventType.IllnessOrInjuryDate) 
                ?? c.StartDate).ToDateString();

            // Defaulting this for now to blank, no longer needed
            m.SSN = string.Empty;

            //Get custom fields
            var cf = c.Employee.CustomFields;
            if (cf != null)
            {
                m.Company = GetCustomFieldValue(cf, "C_NAME");
                m.Classification = GetCustomFieldValue(cf, "CLASSIFICATION");
            }
            cf = c.CustomFields;
            if (cf != null)
            {
                if (string.IsNullOrWhiteSpace(m.Classification))
                    m.Classification = GetCustomFieldValue(cf, "CLASSIFICATION");
                m.TotalCost = GetCustomFieldValue(cf, "PS");
                m.NatureOther = GetCustomFieldValue(cf, "NATREOTHER");
                m.BodyPart = GetCustomFieldValue(cf, "DESCRIPTION");
                m.WcBodyPart = GetCustomFieldValue(cf, "BODYPARTS");
                m.Degree = GetCustomFieldValue(cf, "DEGREE");
                m.Memo = GetCustomFieldValue(cf, "MEMO");
                if (m.Memo?.ToLowerInvariant()?.Contains("absencetracker") == true)
                    m.Memo = "";

                m.LeaveType = GetCustomFieldValue(cf, "DEGREETYPE");
                m.NatureOfIllness = GetCustomFieldValue(cf, "NATUREILLNESS");
                m.WcNatureOfIllness = GetCustomFieldValue(cf, "WCNATURE");
                m.WcNatureOfInjury = GetCustomFieldValue(cf, "NATURE");
                m.BodySide = GetCustomFieldValue(cf, "INJBODYSIDE");
            }

            m.Status = c.Status.GetText();
            
            m.DaysAwayFromWork = c.WorkRelated?.DaysAwayFromWork.HasValue == true ? c.WorkRelated.DaysAwayFromWork.ToString() : "0";
            m.RestrictedDuty = c.WorkRelated?.DaysOnJobTransferOrRestriction.HasValue == true ? c.WorkRelated.DaysOnJobTransferOrRestriction.ToString() : "0";

            m.RTWDate = c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork).ToDateString();
            m.EstimatedRTWDate = c.GetCaseEventDate(Data.Enums.CaseEventType.EstimatedReturnToWork).ToDateString();
            if (c.Status == Data.Enums.CaseStatus.Closed && string.IsNullOrWhiteSpace(m.RTWDate) && (c.ClosureReason == Data.Enums.CaseClosureReason.Other || c.ClosureReason == Data.Enums.CaseClosureReason.ReturnToWork))
                m.RTWDate = m.EstimatedRTWDate;

            m.CaseType = c.Metadata.GetRawValue<bool>("IsWorkRelated") ? "O" : "N";
            m.OfficeLocation = c.CurrentOfficeLocation;

            //audit
            m.CreatedBy = User.GetById(c.CreatedById)?.DisplayName ?? User.System.DisplayName;
            m.DateCreated = c.CreatedDate.ToDateTimeString();
            m.ChangedBy = User.GetById(c.ModifiedById)?.DisplayName ?? User.System.DisplayName;
            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
