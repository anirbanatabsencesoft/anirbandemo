﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class VrptLoaCertificateUsageHandler : BaseExecutionHandler<VrptLoaCertificateUsage>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<VrptLoaCertificateUsage> emit)
        {
            foreach (var intervalGroup in FixUpUsage(c).GroupBy(g => g.Interval))
            {
                foreach (var policyGroup in intervalGroup.GroupBy(g => g.PolicyCode))
                {
                    VrptLoaCertificateUsage m = new VrptLoaCertificateUsage();
                    //set action
                    m.SetAction(c);

                    //set basic properties
                    m.EmployeeNumber = c.Employee.EmployeeNumber;
                    m.CaseNumber = c.CaseNumber;

                    m.Employee = c.Employee.FullName;
                    m.Occupation = c.Employee.JobTitle;


                    var wr = c.WorkRelated;
                    if (wr != null)
                    {
                        m.DateOfInjury = wr.IllnessOrInjuryDate.ToDateString();
                        m.CertifiedBy = (wr.Provider != null && wr.Provider.Contact != null) ? wr.Provider.Contact.FullName + ", " + wr.Provider.Contact.CompanyName : "";
                    }

                    m.Status = c.Status.GetText();

                    //Get custom fields
                    var cf = c.Employee.CustomFields;
                    if (cf != null)
                    {
                        m.ProcessLevel = GetCustomFieldValue(cf, "PROCESSLEVEL"); 
                        m.PTOBalance = GetCustomFieldValue(cf, "CURRENTPTO"); 
                        m.Company = GetCustomFieldValue(cf, "2PMCOMPANYNAME"); 
                    }

                    m.ClockNumber = c.Employee.EmployeeNumber;

                    for (var i = 1; i < 7; i++)
                    {
                        var dt = c.StartDate.AddDays(-i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                            continue;
                        else
                        {
                            m.LDW = dt.ToString();
                            break;
                        }
                    }

                    if (intervalGroup.Key == Data.Enums.CaseType.Intermittent || intervalGroup.Key == Data.Enums.CaseType.Reduced)
                        m.DRW_RESTR = intervalGroup.Min(g => g.DateUsed).ToDateString();

                    m.DRW_FULL = (c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork) ?? c.GetCaseEventDate(Data.Enums.CaseEventType.EstimatedReturnToWork)).ToDateString();
                    m.DRW_ACT_EST = c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork) == null ? "E" : "A";
                    m.Class = policyGroup.Key;
                    m.Unit = "H";
                    m.LOAStatus = c.Status.GetText();
                    m.LeaveType = intervalGroup.Key.GetText();
                    m.BenefitFromDate = policyGroup.Min(p => p.DateUsed).ToDateString();
                    m.BenefitToDate = policyGroup.Max(p => p.DateUsed).ToDateString();
                    
                    m.CaseCreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                    m.CaseCreatedDate = c.CreatedDate.ToDateTimeString();
                    m.CaseChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                    m.CaseChageDate = c.ModifiedDate.ToDateTimeString();

                    m.Reason = c.Reason.Code;
                    m.CertifiedDate = (c.Certifications != null && c.Certifications.Count > 0) ? c.Certifications[0].StartDate.ToDateString() : "";

                    var contact = RuntimeData.GetProvider(c.Employee.Id);
                    if (contact != null && contact.Contact != null)
                        m.CertifiedById = contact.Contact.CompanyName ?? contact.Contact.FullName;

                    m.LeadCaseManager = c.AssignedToName;
                    m.LocationName = c.Employee.Info.OfficeLocation;
                    emit(m);
                }
            }
        }
    }
}
