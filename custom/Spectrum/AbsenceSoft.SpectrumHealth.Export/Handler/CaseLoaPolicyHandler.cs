﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseLoaPolicyHandler : BaseExecutionHandler<CaseLoaPolicy>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<CaseLoaPolicy> emit)
        {
            if (c.Segments != null)
            {
                foreach (var s in c.Segments.SelectMany(s => s.AppliedPolicies ?? new System.Collections.Generic.List<AppliedPolicy>(0)).GroupBy(a => a?.Policy?.Code))
                {
                    if (s.Key != null)
                    {
                        var p = s.FirstOrDefault();

                        CaseLoaPolicy m = new CaseLoaPolicy();
                        //set action
                        m.SetAction(c);

                        //set basic properties
                        m.EmployeeNumber = c.Employee.EmployeeNumber;
                        m.CaseNumber = c.CaseNumber;

                        //Policy info
                        m.Name = (p.PolicyCrosswalkCodes != null && p.PolicyCrosswalkCodes.FirstOrDefault(x => x.Code == "PAYCODE") != null) ? p.PolicyCrosswalkCodes.FirstOrDefault(x => x.Code == "PAYCODE").Code : p.Policy.Code;
                        m.PolicyClass = p.Policy.Name;

                        if (p.PolicyReason.Paid)
                        {
                            m.PolicyType = "paid";
                        }
                        else if (p.Policy.PolicyType == PolicyType.FMLA || p.Policy.PolicyType == PolicyType.StateFML)
                        {
                            m.PolicyType = "job protection";
                        }
                        else
                        {
                            m.PolicyType = "";
                        }

                        m.Jurisdiction = p.Policy.WorkState;
                        m.LeaveYearCacl = p.PolicyReason.PeriodType.GetText();
                        m.Unit = p.PolicyReason.EntitlementType.ToUnit().ToString().Substring(0, 1);
                        m.MaxBenefit = Convert.ToString(p.PolicyReason.Entitlement);

                        //Need info
                        if (p.PolicyReason.PolicyEliminationCalcType == PolicyEliminationType.Exclusive && p.PolicyReason.Elimination != null)
                        {
                            switch (p.PolicyReason.EntitlementType)
                            {
                                case EntitlementType.WorkDays:
                                case EntitlementType.CalendarDays:
                                    m.WaitingPeriod = Convert.ToString(p.PolicyReason.Elimination);
                                    break;
                                case EntitlementType.CalendarMonths:
                                case EntitlementType.WorkingMonths:
                                    m.WaitingPeriod = Convert.ToString(p.PolicyReason.Elimination * 30);
                                    break;
                                case EntitlementType.CalendarWeeks:
                                case EntitlementType.WorkWeeks:
                                    m.WaitingPeriod = Convert.ToString(p.PolicyReason.Elimination * 7);
                                    break;
                                case EntitlementType.CalendarYears:
                                    m.WaitingPeriod = Convert.ToString(p.PolicyReason.Elimination * 365);
                                    break;
                                case EntitlementType.WorkingHours:
                                    m.WaitingPeriod = Convert.ToString(p.PolicyReason.Elimination / 24);
                                    break;
                            }
                        }

                        m.WorkWeekBegin = c.Employee.StartDayOfWeek.ToString();
                        m.UniqueBy = "clock number";

                        //audit
                        m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                        m.DateCreated = c.CreatedDate.ToDateTimeString();
                        m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                        m.DateChanged = c.ModifiedDate.ToDateTimeString();

                        if (p.PolicyReason.PeriodType == null)
                        {
                            m.WaitingPeriodApplies = "";
                        }
                        else
                        {
                            m.WaitingPeriodApplies = p.PolicyReason.PeriodType == PeriodType.PerOccurrence ? "per incident" : "per request";
                        }
                        m.WaitingPeriodWaived = "N";

                        emit(m);
                    }
                }
            }
        }
    }
}
