﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class RtjHandler : BaseExecutionHandler<Rtj>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<Rtj> emit)
        {
            Rtj m = new Rtj();
            //set action
            m.SetAction(c);

            //set basic properties
            m.EmployeeNumber = c.Employee.EmployeeNumber;
            m.CaseNumber = c.CaseNumber;

            m.DateOfRTW = (c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork) ?? c.GetCaseEventDate(Data.Enums.CaseEventType.EstimatedReturnToWork)).ToDateString();
            var restrictedPeriods = c.Segments.OrderBy(s => s.StartDate).Skip(1).Where(s => s.Type != Data.Enums.CaseType.Consecutive);
            if (restrictedPeriods.Any())
            {
                var firstRestrictedDate = restrictedPeriods.Min(s => s.StartDate);
                m.DateOfRTW = firstRestrictedDate.ToDateString();
            }
            m.DateToFullDutyRTW = c.GetCaseEventDate(Data.Enums.CaseEventType.ReturnToWork).ToDateString();

            m.DateChanged = c.ModifiedDate.ToDateTimeString();

            emit(m);
        }
    }
}
