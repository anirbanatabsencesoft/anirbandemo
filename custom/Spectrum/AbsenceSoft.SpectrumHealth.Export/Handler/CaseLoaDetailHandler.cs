﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;
using System.Linq;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class CaseLoaDetailHandler : BaseExecutionHandler<CaseLoaDetail>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<CaseLoaDetail> emit)
        {
            var usage = FixUpUsage(c);
            foreach (var u in usage
                // Only approved time
                .Where(r => r.Determination == AdjudicationStatus.Approved)
                // Only days where time was actually used
                .Where(r => r.MinutesUsed > 0 
                    || r.EntitlementType == EntitlementType.CalendarDays 
                    || r.EntitlementType == EntitlementType.CalendarWeeks 
                    || r.EntitlementType == EntitlementType.CalendarMonths 
                    || r.EntitlementType == EntitlementType.CalendarYears)
                // AT has some bugz, it may duplicate the same policy code for the same day, same stats, need to group and aggregate
                //  the total time used below just in case.
                .GroupBy(r => new { r.PolicyCode, PolicyDate = r.DateUsed.ToDateString(), r.EntitlementType }))
            {
                CaseLoaDetail m = new CaseLoaDetail();
                //set action
                m.SetAction(c);

                //set basic properties
                m.EmployeeNumber = c.Employee.EmployeeNumber;
                m.CaseNumber = c.CaseNumber;

                //Policy info
                m.PolicyCode = u.Key.PolicyCode;
                m.PolicyDate = u.Key.PolicyDate;

                // Here we need native units if the type is not minutes and say, days.
                var unit = u.Key.EntitlementType.Value;
                switch (unit)
                {
                    case EntitlementType.CalendarWeeks:
                        m.Value = Convert.ToString(Math.Round(u.Any(r => r.Determination == AdjudicationStatus.Approved) ? 1 : 0 / 7D, 15));
                        break;
                    case EntitlementType.CalendarDays:
                        m.Value = u.Any(r => r.Determination == AdjudicationStatus.Approved) ? "1" : "0";
                        break;
                    case EntitlementType.CalendarMonths:
                        m.Value = Convert.ToString(Math.Round(u.Any(r => r.Determination == AdjudicationStatus.Approved) ? 1 : 0 / 30D, 15));
                        break;
                    case EntitlementType.WorkDays:
                        m.Value = Convert.ToString(Math.Round(u.Max(r => r.MinutesUsed) / u.Max(r => r.MinutesInDay), 15));
                        break;
                    default:
                        m.Value = Convert.ToString(Math.Round(u.Max(r => r.MinutesUsed) / 60, 15));
                        break;
                }

                //audit
                m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                m.DateCreated = c.CreatedDate.ToDateTimeString();
                m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                m.DateChanged = c.ModifiedDate.ToDateTimeString();

                emit(m);
            }
        }
    }
}
