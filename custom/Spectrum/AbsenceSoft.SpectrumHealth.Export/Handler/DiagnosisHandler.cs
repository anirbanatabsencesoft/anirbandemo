﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.SpectrumHealth.Export.Common;
using AbsenceSoft.SpectrumHealth.Export.Entities;
using System;

namespace AbsenceSoft.SpectrumHealth.Export.Handler
{
    public class DiagnosisHandler : BaseExecutionHandler<Diagnosis>
    {
        /// <summary>
        /// Retrieves the assigned case manager's data from Mongo
        /// </summary>
        protected override void GetData(Case c, Action<Diagnosis> emit)
        {
            //check if diagnosis/disability is not null
            var d = c.Disability;
            if (d != null && d.PrimaryDiagnosis != null)
            {
                Diagnosis m = new Diagnosis();
                //set action
                m.SetAction(c);

                //set basic properties
                m.EmployeeNumber = c.Employee?.EmployeeNumber;
                m.CaseNumber = c.CaseNumber;

                m.DiagnosisDate = d.ConditionStartDate.ToDateString();
                m.DiagnosisType = "primary";
                m.ICD = d.PrimaryDiagnosis.Code;
                m.DiagnosisDescription = d.PrimaryDiagnosis.Name;
                m.Comments = d.GeneralHealthCondition;

                //audit
                m.CreatedBy = c.CreatedBy?.DisplayName ?? User.System.DisplayName;
                m.DateCreated = c.CreatedDate.ToDateTimeString();
                m.ChangedBy = c.ModifiedBy?.DisplayName ?? User.System.DisplayName;
                m.DateChanged = c.ModifiedDate.ToDateTimeString();

                emit(m);
            }
        }
    }
}
