﻿using AbsenceSoft;
using AbsenceSoft.Common;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using MongoDB.Driver;
//using ParrotConversionUtility.Logic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParrotConversionUtility
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static string CustomerId = "546e5097a32aa00d60e3210a";
        public static string EmployerId = "546e52cda32aa00f78086269";
        public static List<Employee> CurrentEmployees = null;
        public static List<Case> CurrentCases = null;

        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            log.InfoFormat("ParrotConversionUtility started with '{0}'", Environment.CommandLine);

            bool attachments = false;
            string fileName = null;
            string attachmentsFileName = null;
            int rowCount = 0;
            for (var i = 0; i < args.Length; i++)
            {
                string a = args[i];
                if (a == "-f")
                    fileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-c")
                    CustomerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-e")
                    EmployerId = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-z")
                    attachmentsFileName = args.Length > (i + 1) ? args[i + 1] : null;
                else if (a == "-a")
                    attachments = true;
                else if (a.Contains("?") || a.Contains("help"))
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Command Syntax:");
                    Console.WriteLine("    ParrotConversionUtility.exe -a [-f <file path>] [-z <file path>] [-c <customerId>] [-e <employerId>]");
                    Console.WriteLine();
                    Console.WriteLine("    -a: perform attachments import (-f and -z are required)");
                    Console.WriteLine("    -f <file path>: Specifies the file path to be converted");
                    Console.WriteLine("    -z <file path>: Specifies the attachments zip file path");
                    Console.WriteLine("    -c <customerId>: Specifies the customerId (optional)");
                    Console.WriteLine("    -e <employerId>: Specifies the employerId (optional)");
                    Console.WriteLine();
                    Environment.Exit(0);
                    return;
                }
            }

            bool error = false;
            if (string.IsNullOrWhiteSpace(fileName))
            {
                log.Error("-f was not supplied or no file name was provided for the -f option");
                error = true;
            }
            else if (!File.Exists(fileName))
            {
                log.ErrorFormat("The file -f '{0}' was not found", fileName);
                error = true;
            }
            else if (attachments && string.IsNullOrWhiteSpace(attachmentsFileName))
            {
                log.Error("The -z attachments zip file path is required for -a");
                error = true;
            }
            else if (attachments && !File.Exists(attachmentsFileName))
            {
                log.ErrorFormat("The file -z '{0}' was not found", attachmentsFileName);
                error = true;
            }
            if (error)
                Environment.Exit(9);

            rowCount = File.ReadLines(fileName).Count();
            log.InfoFormat("Wow! The file has {0} rows, nice!", rowCount);

            List<string> megaRows = new List<string>(rowCount);

            log.InfoFormat("Reading file '{0}'", fileName);
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line))
                        megaRows.Add(line);
                }
            }
            log.InfoFormat("Done reading file '{0}'", fileName);

            log.Info("Aggregating and saving records to database");

            CurrentEmployees = new List<Employee>(Employee.AsQueryable().Where(e => e.EmployerId == EmployerId).Count());
            CurrentEmployees.AddRange(Employee.Query.Find(Employee.Query.EQ(e => e.EmployerId, EmployerId))
                .SetBatchSize(1000)
                .SetMaxTime(TimeSpan.FromMinutes(30)));
            log.InfoFormat("{0} total employees exist for this employer", CurrentEmployees.Count);

            var query = Case.Query.Find(Case.Query.And(Case.Query.EQ(c => c.CustomerId, CustomerId), Case.Query.EQ(c => c.EmployerId, EmployerId)))
                .SetBatchSize(1000)
                .SetMaxTime(TimeSpan.FromMinutes(30));

            CurrentCases = new List<Case>(query.Count<Case>());
            CurrentCases.AddRange(query);

            log.Info("0.00% complete...");
            int curDone = 0;
            decimal curPerc = 0M;

            megaRows.Batch(2000).AsParallel().ForAll(batch =>
            {
                log.InfoFormat("Processing a batch of {0} attachments", batch.Count());
                BulkWriteOperation<DocumentType> bulkDocuments = null;
                BulkWriteOperation<Attachment> bulkAttachments = null;
                foreach (var row in batch)
                {
                    try
                    {
                        // TODO: Stuff
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error processing attachment batch", ex);
                    }
                    curDone += batch.Count();

                    if ((curPerc + 0.01M) < ((decimal)curDone / (decimal)rowCount))
                    {
                        curPerc = ((decimal)curDone / (decimal)rowCount);
                        //Console.SetCursorPosition(0, Console.CursorTop);
                        log.InfoFormat("{0:N2}% complete...", curPerc * 100M);
                    }
                }
                try
                {
                    if (bulkDocuments != null)
                    {
                        bulkDocuments.Execute();
                        bulkDocuments = null;
                    }
                    if (bulkAttachments != null)
                    {
                        bulkAttachments.Execute();
                        bulkAttachments = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error bulk upserting attachments for file/attachment batch", ex);
                }

                log.Info("That was awesome, now let's see how the server is doing...");
                int tried = 0;
                while (tried < 5 && Attachment.Repository.Collection.Database.RunCommand("serverStatus").Response.GetRawValue<bool>("writeBacksQueued"))
                {
                    log.Warn("Uh oh, write-backs are queued, let's wait another minute; up to 5 minutes.");
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                    tried++;
                }
                log.Info("Server's good to go now, let's continue.");
            });

            megaRows.Clear();
            megaRows = null;
            GC.Collect();
            log.Info("Aggregated and saved all records to database");

            watch.Stop();
            log.InfoFormat("Finished in '{0}'", watch.Elapsed);
            Console.WriteLine();
            Console.WriteLine("Have a nice day! :)");

            // TODO: Email report, notice, log or whatever.
        }
    }
}
