﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParrotConversionUtility.Logic
{
    public class AttachmentRow : BaseRow
    {
        #region Const

        private const int CaseNumberOrdinal = 0;
        private const int DocumentTypeOrdinal = 1;
        private const int EmployeeNumberOrdinal = 2;
        private const int EmployeeLastNameOrdinal = 3;
        private const int EmployeeFirstNameOrdinal = 4;
        private const int EmployeeMiddleInitialOrdinal = 5;
        private const int DateOrdinal = 6;
        private const int DescriptionOrdinal = 7;
        private const int FilePathOrdinal = 8;

        #endregion Const

        #region .ctor

        public AttachmentRow() : base()
        {
            Delimiter = '|';
        }
        public AttachmentRow(string rowSource) : base(rowSource) { }

        #endregion .ctor

        #region Fields

        public string CaseNumber { get; set; }
        public string DocumentType { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeMiddleInitial { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }

        #endregion Fields

        #region Parsing

        public override bool Parse(string rowSource = null)
        {
            _parts = null;

            if (string.IsNullOrWhiteSpace(rowSource))
                return false;

            // Determine the delimiter (test for tabs, then for pipes, otherwise assume commas)
            if (rowSource.Contains('\t')) Delimiter = '\t';
            else if (rowSource.Contains('|')) Delimiter = '|';
            else Delimiter = ',';

            using (StringReader reader = new StringReader(rowSource))
            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(reader))
            {
                parser.HasFieldsEnclosedInQuotes = Delimiter == ',';
                parser.SetDelimiters(Delimiter.ToString());
                if (!parser.EndOfData)
                    _parts = parser.ReadFields();
            }
            //_parts = rowSource.Split('|');
            if (_parts == null || _parts.Length == 1)
                return false;

            //
            // Record Type
            if (Required(CaseNumberOrdinal, "Case Number"))
                CaseNumber = Normalize(CaseNumberOrdinal);
            else
                return false;

            DocumentType = Normalize(DocumentTypeOrdinal);
            EmployeeNumber = Normalize(EmployeeNumberOrdinal);
            EmployeeLastName = Normalize(EmployeeLastNameOrdinal);
            EmployeeFirstName = Normalize(EmployeeFirstNameOrdinal);
            EmployeeMiddleInitial = Normalize(EmployeeMiddleInitialOrdinal);
            Date = ParseDate(DateOrdinal, "Attachment Date") ?? DateTime.UtcNow;
            Description = Normalize(DescriptionOrdinal);
            if (Required(FilePathOrdinal, "File Path"))
                FilePath = Normalize(FilePathOrdinal);
            else
                return false;

            return true;
        }

        #endregion Parsing
    }
}
