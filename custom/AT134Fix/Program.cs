﻿using AbsenceSoft;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AT134Fix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reading Customers, Employers and Case ID's w/ End Dates");
            List<Customer> customers = Customer.AsQueryable().ToList();
            List<Employer> employers = Employer.AsQueryable().ToList();
            List<string> caseIds = Case.Repository.Collection
                .FindAs<BsonDocument>(Query.NE("EndDate", BsonNull.Value))
                .SetFields(Fields.Include("_id"))
                .SetBatchSize(100)
                .ToList()
                .Select(r => r.GetRawValue<string>("_id"))
                .ToList();

            Console.WriteLine("Found {0:N0} Cases to fix", caseIds.Count);
            foreach (var id in caseIds)
            {
                Console.WriteLine("Correcting Case '{0}'", id);
                try
                {
                    var myCase = Case.GetById(id);
                    var emp = Employee.GetById(myCase.Employee.Id);
                    LeaveOfAbsence loa = new LeaveOfAbsence()
                    {
                        Customer = customers.First(c => c.Id == myCase.CustomerId),
                        Employer = employers.First(e => e.Id == myCase.EmployerId),
                        Employee = emp,
                        Case = myCase,
                        WorkSchedule = emp.WorkSchedules
                    };

                    var times = loa.MaterializeSchedule(myCase.EndDate.Value.AddDays(1), myCase.EndDate.Value.AddDays(60), false);
                    DateTime rtwDate = myCase.EndDate.Value;
                    foreach (var t in times.OrderBy(d => d.SampleDate))
                        if (t.TotalMinutes.HasValue && t.TotalMinutes.Value > 0)
                        {
                            rtwDate = t.SampleDate;
                            break;
                        }
                    myCase.SetCaseEvent(CaseEventType.EstimatedReturnToWork, rtwDate);
                    myCase.Save();
                    Console.WriteLine("Successfully Corrected Case '{0}'", id);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Failed to Correct Case '{0}'", id);
                    Console.Error.WriteLine(ex.ToString());
                }
            }
            Console.WriteLine("All Done! Hit Enter to Exit");
            Console.ReadLine();
        }
    }
}
