﻿using Npgsql;
using System;
using System.Text;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class JobHistory : BaseBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobHistory"/> class.
        /// </summary>
        public JobHistory() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobHistory"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public JobHistory(string connectionString) : this()
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobHistory"/> class.
        /// </summary>
        /// <param name="job">The job.</param>
        public JobHistory(Job job) : this(job == null ? null : job.ConnectionString)
        {
            if (job == null)
                return;

            JobId = job.JobId;
            Name = job.Name;
            Command = job.Command;
        }

        /// <summary>
        /// Gets or sets the job history identifier.
        /// </summary>
        /// <value>
        /// The job history identifier.
        /// </value>
        public long? JobHistoryId { get; set; }

        /// <summary>
        /// Gets or sets the job identifier.
        /// </summary>
        /// <value>
        /// The job identifier.
        /// </value>
        public long JobId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public string Command { get; set; }

        /// <summary>
        /// Gets or sets the run date.
        /// </summary>
        /// <value>
        /// The run date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is error; otherwise, <c>false</c>.
        /// </value>
        public bool IsError { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (JobHistoryId.HasValue)
                Execute(Update);
            else
                Execute(Insert);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            me.AppendFormat("Job History Id: {0}", JobHistoryId).AppendLine(", ");
            me.AppendFormat("Job Id: {0}", JobId).AppendLine(", ");
            me.AppendFormat("Name: {0}", Name).AppendLine(", ");
            me.AppendFormat("Start Date: {0}", StartDate).AppendLine(", ");
            if (EndDate.HasValue)
                me.AppendFormat("End Date: {0}", EndDate).AppendLine(", ");
            me.AppendFormat("Command: {0}", Command).AppendLine(", ");
            me.AppendFormat("Status: {0}", Status);
            if (IsError)
                me.AppendLine(", ").AppendFormat("Error: {0}", Error);
            return me.ToString();
        }

        /// <summary>
        /// Inserts this instance with the specified PGSQL connection.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        protected void Insert(NpgsqlConnection conn)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("insert into operational.job_history");
            sbSQL.Append(" (job_id,name,command,start_date,end_date,status,is_error,error)");
            sbSQL.Append(" values ");
            sbSQL.Append(" (@job_id,@name,@command,@start_date,@end_date,@status,@is_error,@error)");
            sbSQL.Append(" RETURNING job_history_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@job_id", JobId));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@command", Command.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_error", IsError));
                cmd.Parameters.Add(new NpgsqlParameter("@error", Error.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                JobHistoryId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Updates this instance with the specified PGSQL connection.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        protected void Update(NpgsqlConnection conn)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("UPDATE operational.job_history SET ");
            sbSQL.Append(" end_date=@end_date,status=@status,is_error=@is_error,error=@error");
            sbSQL.Append(" WHERE job_history_id = @job_history_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@job_history_id", JobHistoryId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_error", IsError));
                cmd.Parameters.Add(new NpgsqlParameter("@error", Error.ValueOrDbNull()));
                cmd.ExecuteNonQuery();
            }
        }
    }
}