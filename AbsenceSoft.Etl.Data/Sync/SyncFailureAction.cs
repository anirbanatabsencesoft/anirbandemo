﻿namespace AbsenceSoft.Etl.Data.Sync
{
    /// <summary>
    /// Represents the different actions that can be taken on a syncronization failure.
    /// </summary>
    public enum SyncFailureAction : short
    {
        /// <summary>
        /// The nothing action, means pretty much do nothing, you guessed it.
        /// </summary>
        Nothing = 0,
        /// <summary>
        /// The retry action means the record will be retried again by the 
        /// sync manager process.
        /// </summary>
        Retry = 1,
        /// <summary>
        /// The success action means you still don't do anything with it, but it
        /// did resync successfully.
        /// </summary>
        Success = 2,
        /// <summary>
        /// The expired action means the record was attempted to be resolved,
        /// however the target system has already synced a newer version and this
        /// one is no longer relevant.
        /// </summary>
        Expired = -1
    }
}
