﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class SyncFailure : BaseBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncFailure"/> class.
        /// </summary>
        public SyncFailure() : base()
        {
            Attempts = 1;
            IsResolved = false;
            DoAction = SyncFailureAction.Nothing;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncFailure"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SyncFailure(string connectionString) : this()
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets or sets the synchronize failure identifier.
        /// </summary>
        /// <value>
        /// The synchronize failure identifier.
        /// </value>
        public long? SyncFailureId { get; set; }

        /// <summary>
        /// Gets or sets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public long DimensionId { get; set; }

        /// <summary>
        /// Gets or sets the external reference identifier.
        /// </summary>
        /// <value>
        /// The external reference identifier.
        /// </value>
        public string ExternalReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the external modified date.
        /// </summary>
        /// <value>
        /// The external modified date.
        /// </value>
        public long ExternalModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the last modified date.
        /// </summary>
        /// <value>
        /// The last modified date.
        /// </value>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the entity json.
        /// </summary>
        /// <value>
        /// The entity json.
        /// </value>
        public string EntityJson { get; set; }

        /// <summary>
        /// Gets or sets the do action.
        /// </summary>
        /// <value>
        /// The do action.
        /// </value>
        public SyncFailureAction DoAction { get; set; }

        /// <summary>
        /// Gets or sets the attempts.
        /// </summary>
        /// <value>
        /// The attempts.
        /// </value>
        public int Attempts { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is resolved.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is resolved; otherwise, <c>false</c>.
        /// </value>
        public bool IsResolved { get; set; }

        /// <summary>
        /// Gets or sets the resolved date.
        /// </summary>
        /// <value>
        /// The resolved date.
        /// </value>
        public DateTime? ResolvedDate { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            if (SyncFailureId.HasValue)
                Execute(Update);
            else
                Execute(Insert);
        }

        /// <summary>
        /// Gets all unresolved failures.
        /// </summary>
        /// <param name="coolOffPeriod">The cool off period.</param>
        /// <returns></returns>
        public List<SyncFailure> GetAllUnresolvedFailures(TimeSpan coolOffPeriod)
        {
            return Execute(conn => ReadUnresolved(conn, coolOffPeriod));
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            me.AppendFormat("Sync Failure Id: {0}", SyncFailureId).AppendLine(", ");
            me.AppendFormat("Dimension Id: {0}", DimensionId).AppendLine(", ");
            me.AppendFormat("External Reference Id: {0}", ExternalReferenceId).AppendLine(", ");
            me.AppendFormat("Last Modified Date: {0}", ModifiedDate).AppendLine(", ");
            me.AppendFormat("Resolved: {0}", IsResolved);
            if (ResolvedDate.HasValue)
                me.AppendLine(", ").AppendFormat("Resolved Date: {0}", ResolvedDate);
            if (!string.IsNullOrWhiteSpace(ErrorMessage))
                me.AppendLine(", ").AppendFormat("Error: {0}", ErrorMessage);
            return me.ToString();
        }

        /// <summary>
        /// Inserts this instance with the specified PGSQL connection.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        protected void Insert(NpgsqlConnection conn)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("insert into operational.sync_failure");
            sbSQL.Append(" (dimension_id,ext_ref_id,ext_modified_date,errormsg,errortrace,entity,do_action)");
            sbSQL.Append(" values ");
            sbSQL.Append(" (@dimension_id,@ext_ref_id,@ext_modified_date,@errormsg,@errortrace,@entity,@do_action)");
            sbSQL.Append(" RETURNING sync_failure_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dimension_id", DimensionId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@errormsg", ErrorMessage.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@errortrace", StackTrace.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@entity", NpgsqlDbType.Json) { Value = EntityJson.ValueOrDbNull() });
                cmd.Parameters.Add(new NpgsqlParameter("@do_action", (short)DoAction));

                object res = cmd.ExecuteScalar();
                SyncFailureId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Updates this instance with the specified PGSQL connection.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        protected void Update(NpgsqlConnection conn)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("UPDATE operational.sync_failure SET ");
            sbSQL.Append(" modified_date=utc(), do_action=@do_action, attempts=@attempts, resolved=@resolved,");
            sbSQL.Append(" resolved_date=(case when @resolved then utc() else resolved_date end)");
            sbSQL.Append(" WHERE sync_failure_id = @sync_failure_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@sync_failure_id", SyncFailureId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@do_action", (short)DoAction));
                cmd.Parameters.Add(new NpgsqlParameter("@attempts", Attempts));
                cmd.Parameters.Add(new NpgsqlParameter("@resolved", IsResolved));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Reads the unresolved sync failures.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <returns></returns>
        protected List<SyncFailure> ReadUnresolved(NpgsqlConnection conn, TimeSpan coolOffPeriod)
        {
            List<SyncFailure> failures = new List<SyncFailure>();

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT sync_failure_id")
                .Append(",dimension_id")
                .Append(",created_date")
                .Append(",modified_date")
                .Append(",ext_ref_id")
                .Append(",ext_modified_date")
                .Append(",errormsg")
                .Append(",errortrace")
                .Append(",entity")
                .Append(",do_action")
                .Append(",attempts")
                .Append(",resolved")
                .Append(",resolved_date").Append(" ");
            sql.Append("FROM operational.sync_failure ");
            sql.Append("WHERE NOT resolved")
                .Append(" AND do_action = 1")
                .Append(" AND modified_date <= @not_after");

            if (DimensionId > 0)
                sql.Append(" AND dimension_id = @dimension_id");

            using (NpgsqlCommand command = new NpgsqlCommand(sql.ToString(), conn))
            {
                if (DimensionId > 0)
                    command.Parameters.Add(new NpgsqlParameter("@dimension_id", DimensionId));
                command.Parameters.Add(new NpgsqlParameter("@not_after", DateTime.UtcNow.Subtract(coolOffPeriod)));
                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        SyncFailure failure = new Sync.SyncFailure();
                        failure.SyncFailureId = dr.GetValueOrDefault<long>(i++);
                        failure.DimensionId = dr.GetValueOrDefault<long>(i++);
                        failure.CreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        failure.ModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                        failure.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        failure.ExternalModifiedDate = dr.GetValueOrDefault<long>(i++);
                        failure.ErrorMessage = dr.GetValueOrDefault<string>(i++);
                        failure.StackTrace = dr.GetValueOrDefault<string>(i++);
                        failure.EntityJson = dr.GetValueOrDefault<string>(i++);
                        failure.DoAction = (SyncFailureAction)dr.GetValueOrDefault<short>(i++);
                        failure.Attempts = dr.GetValueOrDefault<int>(i++);
                        failure.IsResolved = dr.GetValueOrDefault<bool>(i++);
                        failure.ResolvedDate = dr.GetValueOrDefault<DateTime>(i++);
                        failures.Add(failure);
                    }
                }
            }

            return failures;
        }
    }
}
