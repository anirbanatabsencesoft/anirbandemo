﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class Job : BaseBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        public Job() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public Job(string connectionString) : this()
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets or sets the job identifier.
        /// </summary>
        /// <value>
        /// The job identifier.
        /// </value>
        public long JobId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public string Frequency { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public string Command { get; set; }

        /// <summary>
        /// Gets or sets the first run date.
        /// </summary>
        /// <value>
        /// The first run date.
        /// </value>
        public DateTime? FirstRunDate { get; set; }

        /// <summary>
        /// Gets or sets the last run date.
        /// </summary>
        /// <value>
        /// The last run date.
        /// </value>
        public DateTime? LastRunDate { get; set; }

        /// <summary>
        /// Gets or sets the next run date.
        /// </summary>
        /// <value>
        /// The next run date.
        /// </value>
        public DateTime? NextRunDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the connection string.
        /// </summary>
        /// <value>
        /// The name of the connection string.
        /// </value>
        public string ConnectionStringName { get; set; }

        /// <summary>
        /// Gets the next job run.
        /// </summary>
        /// <returns></returns>
        public List<Job> GetNextJobRun()
        {
            return Execute(GetJobs);
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns></returns>
        public JobHistory Execute()
        {
            JobHistory history = new JobHistory(this);
            try
            {
                history.Status = "Running";
                history.StartDate = DateTime.UtcNow;
                history.Save();
                if (string.IsNullOrWhiteSpace(Command))
                {
                    history.IsError = true;
                    history.Error = "The command text is empty.";
                    history.Status = "Failed";
                    return FinishExecution(history);
                }

                string connectionString = ConnectionString;
                if (!string.IsNullOrWhiteSpace(ConnectionStringName))
                {
                    var config = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                    if (config != null)
                        connectionString = config.ConnectionString;
                    else
                    {
                        history.IsError = true;
                        history.Error = string.Format("The connection string with key '{0}' is not configured for this environment.", ConnectionStringName);
                        history.Status = "Failed";
                        return FinishExecution(history);
                    }
                }

                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                using (NpgsqlCommand cmd = new NpgsqlCommand(Command, conn))
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }

                history.Status = "Complete";
            }
            catch (Exception ex)
            {
                history.IsError = true;
                history.Error = ex.ToString();
                history.Status = "Failed";
            }

            return FinishExecution(history);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder me = new StringBuilder();
            me.AppendFormat("Job Id: {0}", JobId).AppendLine(", ");
            me.AppendFormat("Created Date: {0}", CreatedDate).AppendLine(", ");
            me.AppendFormat("Name: {0}", Name).AppendLine(", ");
            me.AppendFormat("Frequency: {0}", Frequency).AppendLine(", ");
            me.AppendFormat("Command: {0}", Command).AppendLine(", ");
            me.AppendFormat("First Run: {0}", FirstRunDate).AppendLine(", ");
            me.AppendFormat("Last Run: {0}", LastRunDate).AppendLine(", ");
            me.AppendFormat("Next Run: {0}", NextRunDate).AppendLine(", ");
            me.AppendFormat("Connection: {0}", ConnectionStringName);
            return me.ToString();
        }

        /// <summary>
        /// Finishes the execution.
        /// </summary>
        /// <param name="history">The history.</param>
        /// <returns></returns>
        protected JobHistory FinishExecution(JobHistory history)
        {
            history.EndDate = DateTime.UtcNow;
            history.Save();

            LastRunDate = history.EndDate;
            FirstRunDate = FirstRunDate ?? LastRunDate;
            NextRunDate = (history.StartDate ?? DateTime.UtcNow).AddMinutes((Frequency ?? "1h").ParseFriendlyTime());

            Execute(Update);

            return history;
        }

        /// <summary>
        /// Updates the current instance with the specified PGSQL connection.
        /// </summary>
        /// <param name="conn">The connection.</param>
        protected void Update(NpgsqlConnection conn)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE operational.job SET ");
            sql.Append(" first_run_date = @first_run_date, last_run_date = @last_run_date, next_run_date = @next_run_date");
            sql.Append(" WHERE job_id = @job_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@first_run_date", FirstRunDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_run_date", LastRunDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@next_run_date", NextRunDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_id", JobId.ValueOrDbNull()));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Gets the jobs.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <returns></returns>
        protected List<Job> GetJobs(NpgsqlConnection conn)
        {
            List<Job> jobs = new List<Job>();
            try
            {
                StringBuilder sql = new StringBuilder()
                    .Append("SELECT job_id, created_date, name, frequency, command, first_run_date, last_run_date, next_run_date, connection_string_name").Append(" ")
                    .Append("FROM operational.job").Append(" ")
                    .Append("WHERE (next_run_date IS NULL OR next_run_date <= @utc) AND is_deleted = false AND job_id NOT IN (")
                    .Append("SELECT job_id FROM operational.job_history WHERE end_date IS NULL)").Append(" ")
                    .Append("ORDER BY next_run_date ASC");

                using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@utc", DateTime.UtcNow));
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                            jobs.Add(new Job(ConnectionString)
                            {
                                JobId = reader.GetValueOrDefault<long>(0),
                                CreatedDate = reader.GetValueOrDefault<DateTime>(1),
                                Name = reader.GetValueOrDefault<string>(2),
                                Frequency = reader.GetValueOrDefault<string>(3),
                                Command = reader.GetValueOrDefault<string>(4),
                                FirstRunDate = reader.GetValueOrNull<DateTime>(5),
                                LastRunDate = reader.GetValueOrNull<DateTime>(6),
                                NextRunDate = reader.GetValueOrNull<DateTime>(7),
                                ConnectionStringName = reader.GetValueOrDefault<string>(8)
                            });
                }
            }
            catch (Exception ex)
            {
                HasError = true;
                ErrorMessage = "Error getting jobs";
                StackTrace = ex.ToString();
            }
            return jobs;
        }
    }
}
