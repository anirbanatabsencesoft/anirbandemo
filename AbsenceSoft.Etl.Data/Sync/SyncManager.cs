﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Text;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class SyncManager : SyncBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncManager"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public SyncManager(string connString)
        {
            ConnectionString = connString;
            MaxFailureAttempts = 1;
            DefaultFailureAction = SyncFailureAction.Nothing;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is dirty.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is dirty; otherwise, <c>false</c>.
        /// </value>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets the lock date.
        /// </summary>
        /// <value>
        /// The lock date.
        /// </value>
        public DateTime LockDate { get; set; }

        /// <summary>
        /// Gets or sets the suspend date.
        /// </summary>
        /// <value>
        /// The suspend date.
        /// </value>
        public DateTime SuspendDate { get; set; }

        /// <summary>
        /// Gets or sets the suspend task client identifier.
        /// </summary>
        /// <value>
        /// The suspend task client identifier.
        /// </value>
        public long SuspendTaskClientId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is suspended.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is suspended; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuspended { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is locked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is locked; otherwise, <c>false</c>.
        /// </value>
        public bool IsLocked { get; set; }

        /// <summary>
        /// Gets or sets the suspended master index identifier.
        /// </summary>
        /// <value>
        /// The suspended master index identifier.
        /// </value>
        public long SuspendedMasterIndexId { get; set; }

        /// <summary>
        /// Gets or sets the lock task client identifier.
        /// </summary>
        /// <value>
        /// The lock task client identifier.
        /// </value>
        public long LockTaskClientId { get; set; }

        /// <summary>
        /// Gets or sets the size of the batch.
        /// </summary>
        /// <value>
        /// The size of the batch.
        /// </value>
        public int BatchSize { get; set; }

        public int BatchLookAheadMinutes { get; set; }
        public int BatchLookBackMinutes { get; set; }

        /// <summary>
        /// Gets or sets the synchronize manager identifier.
        /// </summary>
        /// <value>
        /// The synchronize manager identifier.
        /// </value>
        public long SyncManagerId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the last synchronize date update.
        /// </summary>
        /// <value>
        /// The last synchronize date update.
        /// </value>
        public DateTime? LastSyncDateUpdate { get; set; }

        /// <summary>
        /// Gets or sets the new batch maximum modified date.
        /// </summary>
        /// <value>
        /// The new batch maximum modified date.
        /// </value>
        public DateTime? NewBatchMaxModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public long DimensionId { get; set; }

        /// <summary>
        /// Gets or sets the source count.
        /// </summary>
        /// <value>
        /// The source count.
        /// </value>
        public long SourceCount { get; set; }

        /// <summary>
        /// Gets or sets the Initial Load.
        /// </summary>
        /// <value>
        /// The source count.
        /// </value>
        public bool InitialLoad { get; set; }

        /// <summary>
        /// Gets or sets the maximum failure attempts.
        /// </summary>
        /// <value>
        /// The maximum failure attempts.
        /// </value>
        public int MaxFailureAttempts { get; set; }

        /// <summary>
        /// Gets or sets the default failure action.
        /// </summary>
        /// <value>
        /// The default failure action.
        /// </value>
        public SyncFailureAction DefaultFailureAction { get; set; }

        /// <summary>
        /// This property will be used while retrieving
        /// If it exceeds a configurable number, then the synchronizer will not add any buffer to selection
        /// </summary>
        public int SkipBufferCount { get; set; }

        /// <summary>
        /// Gets the last synchronize date and identifier update.
        /// </summary>
        /// <param name="modifiedDate">The modified date.</param>
        /// <returns></returns>
        public void SetNewBatchMaxModifiedDate(DateTime? modifiedDate)
        {
            if (!modifiedDate.HasValue)
                return;

            if (!NewBatchMaxModifiedDate.HasValue)
                NewBatchMaxModifiedDate = modifiedDate;
            else
                NewBatchMaxModifiedDate = modifiedDate > NewBatchMaxModifiedDate ? modifiedDate : NewBatchMaxModifiedDate;
        }

        /// <summary>
        /// Loads the synchronize manager.
        /// </summary>
        public void LoadSyncManager()
        {
            Execute(sqlLoad);
        }

        /// <summary>
        /// Updates the synchronize update.
        /// </summary>
        public void UpdateSyncUpdate()
        {
            LastSyncDateUpdate = NewBatchMaxModifiedDate ?? LastSyncDateUpdate;
            Execute(sqlUpdateSyncUpdate);
        }

        /// <summary>
        /// Overloaded method for sync update
        /// </summary>
        public void UpdateSyncUpdate(Npgsql.NpgsqlConnection pgsqlConnection, DateTime? lastModifiedDate)
        {
            LastSyncDateUpdate = lastModifiedDate ?? LastSyncDateUpdate;
            sqlUpdateSyncUpdate(pgsqlConnection);
        }

        /// <summary>
        /// Sets the synchronize manager lock.
        /// </summary>
        public void SetSyncManagerLock()
        {
            Execute(sqlSetLock);
        }

        /// <summary>
        /// Releases the synchronize manager lock.
        /// </summary>
        public void ReleaseSyncManagerLock()
        {
            Execute(sqlReleaseLock);
        }

        /// <summary>
        /// Sets the synchronize manager dirty.
        /// </summary>
        public void SetSyncManagerDirty()
        {
            Execute(sqlSetDirty);
        }

        /// <summary>
        /// Releases the synchronize manager dirty.
        /// </summary>
        public void ReleaseSyncManagerDirty()
        {
            Execute(sqlReleaseDirty);
        }

        /// <summary>
        /// Updates the source count.
        /// </summary>
        public void UpdateSourceCount()
        {
            Execute(sqlUpdateSourceCount);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return new StringBuilder()
                .Append("{syncmanagerid=").Append(SyncManagerId).Append("}")
                .Append("{dimenstionid=").Append(DimensionId).Append("}")
                .Append("{batchsize=").Append(BatchSize).Append("}")
                .Append("{lastsyncdateupdate=").Append(LastSyncDateUpdate).Append("}")
                .Append("{initialload=").Append(InitialLoad).Append("}")
                .Append("{sourcecount=").Append(SourceCount).Append("}")
                .Append("{maxfailureattempts=").Append(MaxFailureAttempts).Append("}")
                .Append("{defaultfailureaction=").Append(DefaultFailureAction).Append("}")
                .Append("{samedateexecution=").Append(SkipBufferCount).Append("}")
                .ToString();
        }

        protected void sqlLoad(NpgsqlConnection conn)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT sync_manager_id")
                .Append(",created_date")
                .Append(",modified_date")
                .Append(",last_sync_date_update")
                .Append(",dimension_id")
                .Append(",batch_size")
                .Append(",is_locked")
                .Append(",is_suspended")
                .Append(",lock_task_client_id")
                .Append(",suspended_master_index_id")
                .Append(",lock_date")
                .Append(",suspend_task_client_id")
                .Append(",suspend_date")
                .Append(",is_dirty")
                .Append(",source_count")
                .Append(",initial_load")
                .Append(",max_failure_attempts")
                .Append(",default_failure_action")
                .Append(",skip_buffer_count")
                .Append(",batch_look_back_minutes")
                .Append(",batch_look_ahead_minutes")

                ;

            sql.Append(" FROM operational.sync_manager");

            if (SyncManagerId != 0)
                sql.Append(" where sync_manager_id = " + SyncManagerId);
            else if (DimensionId != 0)
                sql.Append(" where dimension_id = " + DimensionId);

            sql.Append(" limit 1 ");

            using (NpgsqlCommand command = new NpgsqlCommand(sql.ToString(), conn))
            using (NpgsqlDataReader dr = command.ExecuteReader())
                if (dr.Read())
                {
                    var i = 0;
                    SyncManagerId = dr.GetValueOrDefault<long>(i++);
                    CreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                    ModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                    LastSyncDateUpdate = dr.GetValueOrNull<DateTime>(i++);
                    DimensionId = dr.GetValueOrDefault<long>(i++);
                    BatchSize = dr.GetValueOrDefault<int>(i++);                
                    IsLocked = dr.GetValueOrDefault<bool>(i++);
                    IsSuspended = dr.GetValueOrDefault<bool>(i++);
                    LockTaskClientId = dr.GetValueOrDefault<long>(i++);
                    SuspendedMasterIndexId = dr.GetValueOrDefault<long>(i++);
                    LockDate = dr.GetValueOrDefault<DateTime>(i++);
                    SuspendTaskClientId = dr.GetValueOrDefault<long>(i++);
                    SuspendDate = dr.GetValueOrDefault<DateTime>(i++);
                    IsDirty = dr.GetValueOrDefault<bool>(i++);
                    SourceCount = dr.GetValueOrDefault<long>(i++);
                    InitialLoad = dr.GetValueOrDefault<bool>(i++);
                    MaxFailureAttempts = dr.GetValueOrDefault<int>(i++);
                    DefaultFailureAction = (SyncFailureAction)dr.GetValueOrDefault<short>(i++);
                    SkipBufferCount = dr.GetValueOrDefault<int>(i++);
                    BatchLookBackMinutes = dr.GetValueOrDefault<int>(i++);
                    BatchLookAheadMinutes = dr.GetValueOrDefault<int>(i++);

                }
        }

        protected void sqlSetLock(NpgsqlConnection pgsqlConnection)
        {
            string sp = "operational.getsynclockifnotexist";
            using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(sp, pgsqlConnection))
            using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
            {
                pgsqlcommand.CommandType = CommandType.StoredProcedure;
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("syncmanagerid", NpgsqlDbType.Bigint) { Value = SyncManagerId });
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("locktaskclientid", NpgsqlDbType.Bigint) { Value = LockTaskClientId });
                pgsqlcommand.ExecuteNonQuery();
                tran.Commit();
            }
        }

        protected void sqlReleaseLock(NpgsqlConnection pgsqlConnection)
        {
            string sp = "operational.releasesynclockifexist";
            using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(sp, pgsqlConnection))
            using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
            {
                pgsqlcommand.CommandType = CommandType.StoredProcedure;
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("syncmanagerid", NpgsqlDbType.Bigint) { Value = SyncManagerId });
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("locktaskclientid", NpgsqlDbType.Bigint) { Value = LockTaskClientId });
                pgsqlcommand.ExecuteNonQuery();
                tran.Commit();
            }
        }

        protected void sqlUpdateSyncUpdate(NpgsqlConnection pgsqlConnection)
        {
            //get the last sync time and retry count
            DateTime? lastSyncUpdate = null;
            using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT last_sync_date_update, skip_buffer_count FROM operational.sync_manager WHERE sync_manager_id=@syncmanagerid", pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@syncmanagerid", SyncManagerId));
                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        lastSyncUpdate = dr.GetValueOrNull<DateTime>(0);
                        SkipBufferCount = dr.GetValueOrDefault<int>(1);
                    }
                }
            }

            //check if the last update time is same or not. If same increment the number of last sync
            //matching with string as Postgres only stores millisecond up-to 3 decimal
            if (lastSyncUpdate.HasValue && lastSyncUpdate.Value.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff") == LastSyncDateUpdate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"))
                SkipBufferCount++;
            else //reset
                SkipBufferCount = 0;


            using (NpgsqlCommand cmd = new NpgsqlCommand("update operational.sync_manager set last_sync_date_update=@last_sync_date_update, modified_date=utc(), skip_buffer_count=@skip_buffer_count where sync_manager_id=@syncmanagerid", pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@syncmanagerid", SyncManagerId));
                cmd.Parameters.Add(new NpgsqlParameter("@last_sync_date_update", LastSyncDateUpdate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@skip_buffer_count", SkipBufferCount.ValueOrDbNull()));
                var i = cmd.ExecuteNonQuery();
            }
        }

        protected void sqlSetDirty(NpgsqlConnection pgsqlConnection)
        {
            string sp = "operational.getsyncisdirtyifnotexist";
            using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(sp, pgsqlConnection))
            using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
            {
                pgsqlcommand.CommandType = CommandType.StoredProcedure;
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("syncmanagerid", NpgsqlDbType.Bigint) { Value = SyncManagerId });
                pgsqlcommand.ExecuteNonQuery();
                tran.Commit();
            }
        }

        protected void sqlReleaseDirty(NpgsqlConnection pgsqlConnection)
        {
            string sp = "operational.releasesyncisdirtyifexist";
            using (NpgsqlCommand pgsqlcommand = new NpgsqlCommand(sp, pgsqlConnection))
            using (NpgsqlTransaction tran = pgsqlConnection.BeginTransaction())
            {
                pgsqlcommand.CommandType = CommandType.StoredProcedure;
                pgsqlcommand.Parameters.Add(new NpgsqlParameter("syncmanagerid", NpgsqlDbType.Bigint) { Value = SyncManagerId });
                pgsqlcommand.ExecuteNonQuery();
                tran.Commit();
            }
        }

        protected void sqlUpdateSourceCount(NpgsqlConnection pgsqlConnection)
        {
            if (SourceCount > 0)
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand("update operational.sync_manager set source_count=@source_count, modified_date=utc(), initial_load=(case when initial_load and @source_count = 0 then FALSE else initial_load end) where sync_manager_id=@syncmanagerid", pgsqlConnection))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@syncmanagerid", SyncManagerId));
                    cmd.Parameters.Add(new NpgsqlParameter("@source_count", SourceCount));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
