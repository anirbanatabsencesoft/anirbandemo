﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class TaskClient : SyncBase
    {
        /// <summary>
        /// Gets or sets the API settings.
        /// </summary>
        /// <value>
        /// The API settings.
        /// </value>
        public string ApiSettings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is disabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Gets or sets the check interval in ms.
        /// </summary>
        /// <value>
        /// The check interval in ms.
        /// </value>
        public int CheckIntervalInMs { get; set; }

        /// <summary>
        /// Gets or sets the heartbeat interval in ms.
        /// </summary>
        /// <value>
        /// The heartbeat interval in ms.
        /// </value>
        public int HeartbeatIntervalInMs { get; set; }

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>
        /// The name of the process.
        /// </value>
        public string ProcessName { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        /// <value>
        /// The notes.
        /// </value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnString { get; set; }

        /// <summary>
        /// Gets or sets the process identifier.
        /// </summary>
        /// <value>
        /// The process identifier.
        /// </value>
        public string ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the last heartbeat date.
        /// </summary>
        /// <value>
        /// The last heartbeat date.
        /// </value>
        public DateTime LastHeartbeatDate { get; set; }

        /// <summary>
        /// Gets or sets the last process date.
        /// </summary>
        /// <value>
        /// The last process date.
        /// </value>
        public DateTime LastProcessDate { get; set; }

        /// <summary>
        /// Gets or sets the initialize processing date.
        /// </summary>
        /// <value>
        /// The initialize processing date.
        /// </value>
        public DateTime InitProcessingDate { get; set; }

        /// <summary>
        /// Gets or sets the initialize register date.
        /// </summary>
        /// <value>
        /// The initialize register date.
        /// </value>
        public DateTime InitRegisterDate { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the synchronize manager identifier.
        /// </summary>
        /// <value>
        /// The synchronize manager identifier.
        /// </value>
        public long SyncManagerId { get; set; }

        /// <summary>
        /// Gets or sets the task client identifier.
        /// </summary>
        /// <value>
        /// The task client identifier.
        /// </value>
        public long TaskClientId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the failure retry cool off period.
        /// </summary>
        /// <value>
        /// The failure retry cool off period.
        /// </value>
        public TimeSpan FailureRetryCoolOffPeriod { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskClient"/> class.
        /// </summary>
        public TaskClient() : base() { }

        /// <summary>
        /// Saves the specified connection.
        /// </summary>
        /// <param name="conn">The connection.</param>
        public void Save(NpgsqlConnection conn)
        {
            try
            {
                sqlInsert(conn);
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }
        }

        /// <summary>
        /// Loads the specified connection.
        /// </summary>
        /// <param name="conn">The connection.</param>
        public void Load(NpgsqlConnection conn)
        {
            try
            {
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }
        }

        /// <summary>
        /// Loads the name of the by process.
        /// </summary>
        /// <param name="conn">The connection.</param>
        public void LoadByProcessName(NpgsqlConnection conn)
        {
            try
            {
                sqlLoadByProcessName(conn);
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }
        }

        /// <summary>
        /// Updates the task client initialize.
        /// </summary>
        /// <param name="conn">The connection.</param>
        public void UpdateTaskClientInit(NpgsqlConnection conn)
        {
            try
            {
                sqlUpdateTaskClientInit(conn);
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }
        }

        protected void sqlUpdateTaskClientInit(NpgsqlConnection pgsqlConnection)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand("update operational.task_client set init_register_date=@init_register_date, connstring=@connstring, process_name=@process_name, heartbeat_interval_in_ms=@heartbeat_interval_in_ms, check_interval_in_ms=@check_interval_in_ms, sync_manager_id=@sync_manager_id where task_client_id = " + TaskClientId, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@init_register_date", InitRegisterDate));
                cmd.Parameters.Add(new NpgsqlParameter("@connstring", ConnString));
                cmd.Parameters.Add(new NpgsqlParameter("@process_name", ProcessName));
                cmd.Parameters.Add(new NpgsqlParameter("@heartbeat_interval_in_ms", HeartbeatIntervalInMs));
                cmd.Parameters.Add(new NpgsqlParameter("@check_interval_in_ms", CheckIntervalInMs));
                cmd.Parameters.Add(new NpgsqlParameter("@sync_manager_id", SyncManagerId));
                cmd.ExecuteNonQuery();
            }
        }

        protected void sqlLoadByProcessName(NpgsqlConnection conn)
        {
            string sql = "SELECT task_client_id FROM operational.task_client where process_name = '" + ProcessName + "' limit 1";

            using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
            using (NpgsqlDataReader dr = command.ExecuteReader())
                if (dr.Read())
                {
                    TaskClientId = (long)dr[0];
                }
        }

        protected void sqlLoad(NpgsqlConnection conn)
        {
            string sql = "SELECT task_client_id, sync_manager_id, process_id, is_disabled, api_settings FROM operational.task_client where task_client_id = '" + TaskClientId + "' limit 1";

            using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
            using (NpgsqlDataReader dr = command.ExecuteReader())
                while (dr.Read())
                {
                    TaskClientId = (Int64)dr[0];
                    SyncManagerId = (Int64)dr[1];
                    ProcessId = (string)dr[2];
                    IsDisabled =  dr[3] == DBNull.Value ? false : (bool)dr[3];
                    ApiSettings = dr[4] == DBNull.Value ? null : (string)dr[4];
                }
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("insert into operational.task_client");
            sbSQL.Append(" (init_register_date, process_id, connstring, process_name, heartbeat_interval_in_ms, check_interval_in_ms, sync_manager_id)");
            sbSQL.Append(" values");
            sbSQL.Append("(@init_register_date, @process_id, @connstring, @process_name,@heartbeat_interval_in_ms,@check_interval_in_ms, @sync_manager_id)");
            sbSQL.Append(" RETURNING task_client_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@init_register_date", InitRegisterDate));
                cmd.Parameters.Add(new NpgsqlParameter("@process_id", ProcessId));
                cmd.Parameters.Add(new NpgsqlParameter("@connstring", ConnString));
                cmd.Parameters.Add(new NpgsqlParameter("@process_name", ProcessName));
                cmd.Parameters.Add(new NpgsqlParameter("@heartbeat_interval_in_ms", HeartbeatIntervalInMs));
                cmd.Parameters.Add(new NpgsqlParameter("@check_interval_in_ms", CheckIntervalInMs));
                cmd.Parameters.Add(new NpgsqlParameter("@sync_manager_id", SyncManagerId));
                
                object res = cmd.ExecuteScalar();
                TaskClientId = Convert.ToInt64(res);
            }
        }
    }
}
