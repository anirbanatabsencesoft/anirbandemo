﻿using System;
using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class SyncLogInfo : SyncBase
    {
        /// <summary>
        /// Gets or sets the synchronize log information identifier.
        /// </summary>
        /// <value>
        /// The synchronize log information identifier.
        /// </value>
        public long SyncLogInfoId { get; set; }

        /// <summary>
        /// Gets or sets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public long DimensionId { get; set; }

        /// <summary>
        /// Gets or sets the MSG source.
        /// </summary>
        /// <value>
        /// The MSG source.
        /// </value>
        public string MsgSource { get; set; }

        /// <summary>
        /// Gets or sets the MSG action.
        /// </summary>
        /// <value>
        /// The MSG action.
        /// </value>
        public string MsgAction { get; set; }

        /// <summary>
        /// Gets or sets the MSG text.
        /// </summary>
        /// <value>
        /// The MSG text.
        /// </value>
        public string MsgText { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncLogInfo"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public SyncLogInfo(string connString) : base()
        {
            ConnectionString = connString;
        }

        /// <summary>
        /// Saves the synchronize log information.
        /// </summary>
        public void SaveSyncLogInfo()
        {
            Execute(sqlInsert);
        }
        
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            Config config = new Config();
            if (config.LogDebugInfo)
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("insert into operational.sync_log_info");
                sbSQL.Append(" (dimension_id, msgsource, msgaction, msgtxt)");
                sbSQL.Append(" values");
                sbSQL.Append("(@dimension_id, @msgsource, @msgaction, @msgtxt)");
                sbSQL.Append(" RETURNING sync_log_info_id");
                using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), pgsqlConnection))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@dimension_id", DimensionId));
                    cmd.Parameters.Add(new NpgsqlParameter("@msgsource", MsgSource.ValueOrDbNull()));
                    cmd.Parameters.Add(new NpgsqlParameter("@msgaction", MsgAction.ValueOrDbNull()));
                    cmd.Parameters.Add(new NpgsqlParameter("@msgtxt", MsgText.ValueOrDbNull()));

                    object res = cmd.ExecuteScalar();
                    SyncLogInfoId = Convert.ToInt64(res);
                }
            }
        }
    }
}
