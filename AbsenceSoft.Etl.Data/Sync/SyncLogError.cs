﻿using System;
using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Sync
{
    public class SyncLogError : SyncBase
    {
        /// <summary>
        /// Gets or sets the synchronize log error identifier.
        /// </summary>
        /// <value>
        /// The synchronize log error identifier.
        /// </value>
        public long SyncLogErrorId { get; set; }

        /// <summary>
        /// Gets or sets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public long DimensionId { get; set; }

        /// <summary>
        /// Gets or sets the MSG source.
        /// </summary>
        /// <value>
        /// The MSG source.
        /// </value>
        public string MsgSource { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncLogError"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public SyncLogError(string connString) : base()
        {
            ConnectionString = connString;
        }

        /// <summary>
        /// Saves the synchronize log error.
        /// </summary>
        public void SaveSyncLogError()
        {
            Execute(sqlInsert);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            if (string.IsNullOrWhiteSpace(ErrorMessage) && string.IsNullOrWhiteSpace(StackTrace))
                return;

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("insert into operational.sync_log_error");
            sbSQL.Append(" (dimension_id, msgsource, errormsg, errortrace)");
            sbSQL.Append(" values");
            sbSQL.Append("(@dimension_id, @msgsource, @errormsg, @errortrace)");
            sbSQL.Append(" RETURNING sync_log_error_id");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dimension_id", DimensionId));
                cmd.Parameters.Add(new NpgsqlParameter("@msgsource", MsgSource));
                cmd.Parameters.Add(new NpgsqlParameter("@errormsg", string.Empty));
                cmd.Parameters.Add(new NpgsqlParameter("@errortrace", "{" + (ErrorMessage ?? "") + "}" + "{" + (StackTrace ?? "") + "}"));

                object res = cmd.ExecuteScalar();
                SyncLogErrorId = Convert.ToInt64(res);
            }
        }
    }
}
