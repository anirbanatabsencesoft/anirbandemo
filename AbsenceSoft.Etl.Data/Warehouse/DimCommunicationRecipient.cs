﻿using AbsenceSoft.Etl.Data.Warehouse;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCommunicationRecipient : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationRecipient"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCommunicationRecipient(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationRecipient"/> class.
        /// </summary>
        public DimCommunicationRecipient() : base() { }
        public override int DimensionId { get { return Const.Dimensions.NONE; } }

        public long DimCommunicationId { get; set; }

        public long DimCommunicationRecipientId { get; set; }
        public int RecipientType { get; set; }
        public string AddressId { get; set; }
        public string AddressAddress1 { get; set; }

        public string AddressCity { get; set; }

        public string AddressCountry { get; set; }

        public string AddressPostalCode { get; set; }

        public string AddressState { get; set; }

        public string AltEmail { get; set; }

        public string Email { get; set; }

        public string Fax { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string HomeEmail { get; set; }
        public string CompanyName { get; set; }
        public string ContactPersonName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public bool? IsPrimary { get; set; }
        

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(SqlDelete);
        }
        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            SqlDelete(pgsqlConnection);
        }

        /// <summary>
        /// Inserts this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sql = @"insert into dim_communication_recipient
                        (dim_communication_id  , recipient_type , ext_ref_id , address_id , address_address1 , address_city
                        ,address_country , address_postal_code , address_state,  alt_email, email, fax , title , first_name	, middle_name , last_name
                        , home_email, company_name , contact_person_name , date_of_birth , home_phone , cell_phone , is_primary)
                        values
                        (@dim_communication_id  , @recipient_type , @ext_ref_id , @address_id , @address_address1 , @address_city
                        , @address_country , @address_postal_code , @address_state, @alt_email, @email, @fax , @title , @first_name	, @middle_name , @last_name
                        , @home_email, @company_name , @contact_person_name , @date_of_birth , @home_phone , @cell_phone , @is_primary)";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_communication_id", DimCommunicationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));        
                cmd.Parameters.Add(new NpgsqlParameter("@recipient_type", RecipientType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address_id", AddressId.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@address_address1", AddressAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address_city", AddressCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address_country", AddressCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address_postal_code", AddressPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address_state", AddressState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_email", AltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@email", Email.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@fax", Fax.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@title", Title.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_name",FirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@middle_name", MiddleName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_name", LastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@home_email", HomeEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@company_name", CompanyName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_person_name", ContactPersonName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@date_of_birth", DateOfBirth.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@home_phone", HomePhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cell_phone", CellPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_primary", IsPrimary.ValueOrDbNull()));

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_communication_recipient where dim_communication_id = @dim_communication_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_communication_id", DimCommunicationId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
