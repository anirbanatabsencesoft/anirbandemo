﻿using AbsenceSoft.Etl.Data.Warehouse;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCommunicationPaperworkField : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationPaperworkField"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCommunicationPaperworkField(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationPaperworkField"/> class.
        /// </summary>
        public DimCommunicationPaperworkField() : base() { }
        public override int DimensionId { get { return Const.Dimensions.NONE; } }
        public long  DimCommunicationPaperworkId { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public bool Required { get; set; }
        public int? FieldOrder { get; set; }

        public string RecordHash { get; set; }
        public string Value { get; set; }
        
        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(SqlDelete);
        }
        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            SqlDelete(pgsqlConnection);
        }

        /// <summary>
        /// Inserts this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sql = @"insert into dim_communication_paperwork_field
                        (dim_communication_paperwork_id, ext_ref_id, name, status, type, required, field_order, value, created_date, is_deleted)
                        values
                        (@dim_communication_paperwork_id ,@ext_ref_id ,@name ,@status ,@type ,@required, @field_order, @value , @created_date, @is_deleted)";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_communication_paperwork_id", DimCommunicationPaperworkId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull())); 
                cmd.Parameters.Add(new NpgsqlParameter("@name" ,Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status" , Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type" , Type.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@required" , Required.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@field_order", FieldOrder.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@value", Value.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted.ValueOrDbNull()));
                
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_communication_paperwork_field where dim_communication_paperwork_id = @dim_communication_paperwork_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_communication_paperwork_id", DimCommunicationPaperworkId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
