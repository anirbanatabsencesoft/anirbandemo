﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCasePayPeriod :  DimBase
    {
        public DimCasePayPeriod(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        public DimCasePayPeriod() : base() { }

        public string RecordHash { get; set; }
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_PAY_PERIOD; } }

        public long DimCasePayPeriodId { get; set; }

        public string CaseId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public DateTime? LockedDate { get; set; }
        public string LockedBy { get; set; }

        public decimal? MaxWeeklyPayAmount { get; set; }

        public DateTime? PayrollDate { get; set; }

        public DateTime? PayrollDateOverride { get; set; }

        public DateTime? EndDateOverride { get; set; }

        public string PayrollDateOverrideBy { get; set; }

        public string EndDateOverrideBy { get; set; }

        public decimal Offset { get; set; }

        public decimal Total { get; set; }

        public decimal SubTotal { get; set; }

        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimCasePayPeriod> LoadHistory(string caseId)
        {
            return Execute(conn => SqlLoadHistory(conn, caseId));
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_case_pay_period
                    (case_id ,start_date , end_date , status , locked_date , locked_by, max_weekly_pay_amount , payroll_date_override, payroll_date , end_date_override, 
                    offset_amount, total, sub_total, dim_ver_id, ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired , ver_root_id , 
                    ver_prev_id , ver_history_count , ext_created_by , ext_modified_by, record_hash, is_deleted)
                    values
                    (@case_id ,@start_date , @end_date , @status , @locked_date , @locked_by, @max_weekly_pay_amount , @payroll_date_override, @payroll_date , @end_date_override, 
                    @offset, @total, @sub_total, @dim_ver_id, @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired , @ver_root_id , 
                    @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by, @record_hash, @is_deleted)
                    RETURNING dim_case_pay_period_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@locked_date", LockedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@locked_by", LockedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@max_weekly_pay_amount", MaxWeeklyPayAmount.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@payroll_date_override", PayrollDateOverride.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@payroll_date", PayrollDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date_override", EndDateOverride.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@offset", Offset.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@total", Total.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@sub_total", SubTotal.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@record_hash", RecordHash));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));               
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCasePayPeriodId = Convert.ToInt64(res);
            }
        }

        protected List<DimCasePayPeriod> SqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimCasePayPeriod> lstCasePayPeriods = new List<DimCasePayPeriod>();

            string sql = @" SELECT ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired
                        , ver_root_id, ver_prev_id, Ver_history_count, case_id,start_date , end_date , status , locked_date , locked_by
                        , max_weekly_pay_amount , payroll_date_override, payroll_date , end_date_override , payroll_date_override_by, end_date_override_by
                        , offset_amount, total, sub_total, is_deleted , dim_case_pay_period_id, record_hash, ext_created_by, ext_modified_by
                         FROM dim_case_pay_period
                         WHERE case_id = @case_id and ver_is_current = true
                         ORDER BY ver_seq";

            using (NpgsqlCommand command = new NpgsqlCommand(sql, pgsqlConnection))
            {
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));
                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        DimCasePayPeriod dimCasePayPeriod = new DimCasePayPeriod(ConnectionString)
                        {
                            ExternalReferenceId = dr.GetValueOrDefault<string>(i++),
                            ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++),
                            ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++),
                            DimVerId = dr.GetValueOrDefault<long>(i++),
                            VersionIsCurrent = dr.GetValueOrDefault<bool>(i++),
                            VersionSequence = dr.GetValueOrDefault<long>(i++),
                            VersionIsExpired = dr.GetValueOrDefault<bool>(i++),
                            VersionRootId = dr.GetValueOrDefault<long>(i++),
                            VersionPreviousId = dr.GetValueOrDefault<long>(i++),
                            VersionHistoryCount = dr.GetValueOrDefault<long>(i++),
                            CaseId = dr.GetValueOrDefault<string>(i++),
                            StartDate = dr.GetValueOrDefault<DateTime>(i++),
                            EndDate = dr.GetValueOrDefault<DateTime>(i++),
                            Status = dr.GetValueOrDefault<int>(i++),
                            LockedDate = dr.GetValueOrDefault<DateTime>(i++),
                            LockedBy = dr.GetValueOrDefault<string>(i++),
                            Offset= dr.GetValueOrDefault<decimal>(i++),
                            Total= dr.GetValueOrDefault<decimal>(i++),
                            SubTotal= dr.GetValueOrDefault<decimal>(i++),
                            MaxWeeklyPayAmount = dr.GetValueOrDefault<decimal>(i++),
                            PayrollDateOverride = dr.GetValueOrDefault<DateTime>(i++),
                            PayrollDate = dr.GetValueOrDefault<DateTime>(i++),
                            EndDateOverride = dr.GetValueOrDefault<DateTime>(i++),
                            PayrollDateOverrideBy = dr.GetValueOrDefault<string>(i++),
                            EndDateOverrideBy = dr.GetValueOrDefault<string>(i++),
                            IsDeleted = dr.GetValueOrDefault<bool>(i++),
                            DimCasePayPeriodId = dr.GetValueOrDefault<long>(i++),
                            RecordHash = dr.GetValueOrDefault<string>(i++),
                            CreatedBy = dr.GetValueOrDefault<string>(i++),
                            ModifiedBy = dr.GetValueOrDefault<string>(i++),
                        };
                        lstCasePayPeriods.Add(dimCasePayPeriod);
                    }
                }
            }

            return lstCasePayPeriods;
        }
    }
}
