﻿using System;
using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimOrganization : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganization"/> class.
        /// </summary>
        public DimOrganization() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganization"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimOrganization(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ORGANIZATION; } }

        /// <summary>
        /// Gets or sets the dim organization identifier.
        /// </summary>
        /// <value>
        /// The dim organization identifier.
        /// </value>
        public long DimOrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the type code.
        /// </summary>
        /// <value>
        /// The type code.
        /// </value>
        public string TypeCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the org created by email.
        /// </summary>
        /// <value>
        /// The org created by email.
        /// </value>
        public string OrgCreatedByEmail { get; set; }

        /// <summary>
        /// Gets or sets the first name of the org created by.
        /// </summary>
        /// <value>
        /// The first name of the org created by.
        /// </value>
        public string OrgCreatedByFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the org created by.
        /// </summary>
        /// <value>
        /// The last name of the org created by.
        /// </value>
        public string OrgCreatedByLastName { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the sic code.
        /// </summary>
        /// <value>
        /// The sic code.
        /// </value>
        public string SicCode { get; set; }

        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        public string NaicsCode { get; set; }

        /// <summary>
        /// Gets or sets the org address1.
        /// </summary>
        /// <value>
        /// The org address1.
        /// </value>
        public string OrgAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the org address2.
        /// </summary>
        /// <value>
        /// The org address2.
        /// </value>
        public string OrgAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the org city.
        /// </summary>
        /// <value>
        /// The org city.
        /// </value>
        public string OrgCity { get; set; }

        /// <summary>
        /// Gets or sets the org postal code.
        /// </summary>
        /// <value>
        /// The org postal code.
        /// </value>
        public string OrgPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the state of the org.
        /// </summary>
        /// <value>
        /// The state of the org.
        /// </value>
        public string OrgState { get; set; }

        /// <summary>
        /// Gets or sets the org country.
        /// </summary>
        /// <value>
        /// The org country.
        /// </value>
        public string OrgCountry { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_organization")
                .Append("(ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id")
                .Append(", ver_history_count, employer_id, customer_id, code, name, description, path, type_code, type_name, org_created_by_email, org_created_by_first_name")
                .Append(", org_created_by_last_name, is_deleted")
                .Append(", ext_created_by, ext_modified_by,siccode,naicscode,address1,address2,city,postalcode,state,country")
                .Append(") values ")
                .Append("(@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id,")
                .Append(" @ver_history_count, @employer_id, @customer_id, @code, @name, @description, @path, @type_code, @type_name, @org_created_by_email, @org_created_by_first_name,")
                .Append(" @org_created_by_last_name, @is_deleted")
                .Append(", @ext_created_by, @ext_modified_by,@siccode,@naicscode,@address1,@address2,@city,@postalcode,@state,@country")
                .Append(") RETURNING dim_organization_id ");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@code", Code.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@path", Path.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type_code", TypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type_name", TypeName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@org_created_by_email", OrgCreatedByEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@org_created_by_first_name", OrgCreatedByFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@org_created_by_last_name", OrgCreatedByLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@siccode", SicCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@naicscode", NaicsCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address1", OrgAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address2", OrgAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@city", OrgCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@postalcode", OrgPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@state", OrgState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@country", OrgCountry.ValueOrDbNull()));
          


                object res = cmd.ExecuteScalar();
                DimOrganizationId = Convert.ToInt64(res);
            }
        }
    }
}
