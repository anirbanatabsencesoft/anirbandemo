﻿using AbsenceSoft.Etl.Data.Warehouse;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCommunication : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunication"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCommunication(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationPaperwork"/> class.
        /// </summary>
        public DimCommunication() : base() { }
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_COMMUNICATION; } }

        public long DimCommunicationId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        public string CaseId { get; set; }
        public string AttachmentId { get; set; }
        public int CommunicationType { get; set; }
        public string CreatedByEmail { get; set; }
        public string CreatedByName { get; set; }
        public int? EmailRepliesType { get; set; }       
        public string Body { get; set; }
        public bool IsDraft { get; set; }
        public string Name { get; set; }
        public bool Public { get; set; }
        public DateTime? FirstSendDate { get; set; }
        public DateTime? LastSendDate { get; set; }
        public string Subject { get; set; }
        public string Template { get; set; }
        public string ToDoItemId { get; set; }

        

        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_communication
                    (employee_id ,employer_id ,customer_id ,case_id ,attachment_id ,body ,communication_type ,created_by_email ,created_by_name ,email_replies_type 
                    ,is_draft ,name ,public ,first_send_date ,last_send_date ,subject ,template ,to_do_item_id 
                    , dim_ver_id, ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired , ver_root_id
                    ,ver_prev_id , ver_history_count , ext_created_by , ext_modified_by, is_deleted)
                    values
                    (@employee_id , @employer_id , @customer_id , @case_id , @attachment_id , @body , @communication_type , @created_by_email , @created_by_name , @email_replies_type 
                    , @is_draft , @name , @public , @first_send_date , @last_send_date , @subject , @template , @to_do_item_id 
                    , @dim_ver_id, @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired , @ver_root_id
                    , @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by, @is_deleted)
                    RETURNING dim_communication_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id",EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id",EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id",CustomerId.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@attachment_id",AttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@body",Body.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_type",CommunicationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_by_email",CreatedByEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_by_name",CreatedByName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@email_replies_type",EmailRepliesType.ValueOrDbNull()));             
                cmd.Parameters.Add(new NpgsqlParameter("@is_draft",IsDraft.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name",Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@public",Public.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_send_date",FirstSendDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_send_date",LastSendDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@subject",Subject.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@template",Template.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@to_do_item_id",ToDoItemId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));               
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCommunicationId = Convert.ToInt64(res);
            }
        }

      
    }
}
