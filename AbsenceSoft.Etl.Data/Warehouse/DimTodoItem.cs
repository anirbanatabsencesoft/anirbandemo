﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimTodoItem : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimTodoItem"/> class.
        /// </summary>
        public DimTodoItem() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTodoItem"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimTodoItem(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_TODOITEM; } }

        /// <summary>
        /// Gets or sets the dim todo item identifier.
        /// </summary>
        /// <value>
        /// The dim todo item identifier.
        /// </value>
        public long DimTodoItemId { get; set; }

        /// <summary>
        /// Gets or sets the accomodation identifier.
        /// </summary>
        /// <value>
        /// The accomodation identifier.
        /// </value>
        public string AccomodationId { get; set; }

        /// <summary>
        /// Gets or sets the active accomodation identifier.
        /// </summary>
        /// <value>
        /// The active accomodation identifier.
        /// </value>
        public string ActiveAccomodationId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the type of the item.
        /// </summary>
        /// <value>
        /// The type of the item.
        /// </value>
        public int ItemType { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the assigned to identifier.
        /// </summary>
        /// <value>
        /// The assigned to identifier.
        /// </value>
        public string AssignedToId { get; set; }

        /// <summary>
        /// Gets or sets the name of the assigned to.
        /// </summary>
        /// <value>
        /// The name of the assigned to.
        /// </value>
        public string AssignedToName { get; set; }

        /// <summary>
        /// Gets or sets the due date.
        /// </summary>
        /// <value>
        /// The due date.
        /// </value>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the assigned to email.
        /// </summary>
        /// <value>
        /// The assigned to email.
        /// </value>
        public string AssignedToEmail { get; set; }

        /// <summary>
        /// Gets or sets the first name of the assigned to.
        /// </summary>
        /// <value>
        /// The first name of the assigned to.
        /// </value>
        public string AssignedToFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the assigned to.
        /// </summary>
        /// <value>
        /// The last name of the assigned to.
        /// </value>
        public string AssignedToLastName { get; set; }

        /// <summary>
        /// Gets or sets the due date change reason.
        /// </summary>
        /// <value>
        /// The due date change reason.
        /// </value>
        public string DueDateChangeReason { get; set; }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public decimal Weight { get; set; }

        /// <summary>
        /// Gets or sets the result text.
        /// </summary>
        /// <value>
        /// The result text.
        /// </value>
        public string ResultText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DimTodoItem"/> is optional.
        /// </summary>
        /// <value>
        ///   <c>true</c> if optional; otherwise, <c>false</c>.
        /// </value>
        public bool Optional { get; set; }

        /// <summary>
        /// Gets or sets the hide until.
        /// </summary>
        /// <value>
        /// The hide until.
        /// </value>
        public DateTime? HideUntil { get; set; }

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        /// <value>
        /// The template.
        /// </value>
        public string Template { get; set; }

        /// <summary>
        /// Gets or sets the name of the work flow.
        /// </summary>
        /// <value>
        /// The name of the work flow.
        /// </value>
        public string WorkFlowName { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_todoitem";
            sqlCmd += "(ext_ref_id, accommodation_id, active_accommodation_id, case_number, case_id, item_type, status";
            sqlCmd += ", assigned_to_id, assigned_to_name, employee_id, customer_id, employer_id, due_date";
            sqlCmd += ", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date";
            sqlCmd += ", priority, title, assigned_to_email, assigned_to_first_name, assigned_to_last_name, due_date_change_reason, weight, result_text";
            sqlCmd += ", optional, hide_until, template, work_flow_name, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @accommodation_id, @active_accommodation_id, @case_number, @case_id, @item_type, @status, @assigned_to_id, @assigned_to_name, ";
            sqlCmd += " @employee_id, @customer_id, @employer_id, @due_date,";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date,";
            sqlCmd += " @priority, @title, @assigned_to_email, @assigned_to_first_name, @assigned_to_last_name, @due_date_change_reason, @weight, @result_text,";
            sqlCmd += " @optional, @hide_until, @template, @work_flow_name, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_todoitem_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_id", AccomodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@active_accommodation_id", ActiveAccomodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_number", CaseNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@item_type", ItemType));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_id", AssignedToId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_name", AssignedToName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@due_date", DueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@priority", Priority));
                cmd.Parameters.Add(new NpgsqlParameter("@title", Title.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_email", AssignedToEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_first_name", AssignedToFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_last_name", AssignedToLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@due_date_change_reason", DueDateChangeReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@weight", Weight));
                cmd.Parameters.Add(new NpgsqlParameter("@result_text", ResultText.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@optional", Optional));
                cmd.Parameters.Add(new NpgsqlParameter("@hide_until", HideUntil.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@template", Template.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_flow_name", WorkFlowName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimTodoItemId = Convert.ToInt64(res);
            }
        }
    }
}
