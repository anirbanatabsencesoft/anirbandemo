﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimRole : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContact"/> class.
        /// </summary>
        public DimRole() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContact"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimRole(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_ROLE; } }

        /// <summary>
        /// Gets or sets the dim role identifier.
        /// </summary>
        /// <value>
        /// The dim role Id
        /// </value>
        public long DimRoleId { get; set; }

        /// <summary>
        /// Gets or sets the dim customer identifier.
        /// </summary>
        /// <value>
        /// The dim customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the permissions
        /// </summary>
        public string[] Permissions { get; set; }

        /// <summary>
        /// Gets or sets the name of the role
        /// </summary>
        /// <value>
        /// The role name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the role
        /// </summary>
        public int? RoleType { get; set; }

        /// <summary>
        /// Is Deleted Flag
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_role")
                .Append("(name, customer_id, permissions, role_type, is_deleted, dim_ver_id, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count")
                .Append(") values (")
                .Append("@name, @customer_id, @permissions, @role_type, @is_deleted, @dim_ver_id, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count")
                .Append(") RETURNING dim_role_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@permissions", Permissions));
                cmd.Parameters.Add(new NpgsqlParameter("@role_type", RoleType));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));

                object res = cmd.ExecuteScalar();
                DimRoleId = Convert.ToInt64(res);
            }
        }
    }
}
