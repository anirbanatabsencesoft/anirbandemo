﻿using System;
using Npgsql;
using System.Collections.Generic;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployee : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployee"/> class.
        /// </summary>
        public DimEmployee() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployee"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployee(string connectionString) : base(connectionString) { }

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE; } }

        /// <summary>
        /// Gets or sets a value indicating whether [meets50 in75 mile rule].
        /// </summary>
        /// <value>
        /// <c>true</c> if [meets50 in75 mile rule]; otherwise, <c>false</c>.
        /// </value>
        public bool Meets50In75MileRule { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is key employee.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is key employee; otherwise, <c>false</c>.
        /// </value>
        public bool IsKeyEmployee { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is exempt.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is exempt; otherwise, <c>false</c>.
        /// </value>
        public bool IsExempt { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the information work phone.
        /// </summary>
        /// <value>
        /// The information work phone.
        /// </value>
        public string InfoWorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the information home phone.
        /// </summary>
        /// <value>
        /// The information home phone.
        /// </value>
        public string InfoHomePhone { get; set; }

        /// <summary>
        /// Gets or sets the information cell phone.
        /// </summary>
        /// <value>
        /// The information cell phone.
        /// </value>
        public string InfoCellPhone { get; set; }

        /// <summary>
        /// Gets or sets the information alt phone.
        /// </summary>
        /// <value>
        /// The information alt phone.
        /// </value>
        public string InfoAltPhone { get; set; }

        /// <summary>
        /// Gets or sets the information email.
        /// </summary>
        /// <value>
        /// The information email.
        /// </value>
        public string InfoEmail { get; set; }

        /// <summary>
        /// Gets or sets the information altemail.
        /// </summary>
        /// <value>
        /// The information altemail.
        /// </value>
        public string InfoAltemail { get; set; }

        /// <summary>
        /// Gets or sets the name of the address.
        /// </summary>
        /// <value>
        /// The name of the address.
        /// </value>
        public string AddressName { get; set; }

        /// <summary>
        /// Gets or sets the address address1.
        /// </summary>
        /// <value>
        /// The address address1.
        /// </value>
        public string AddressAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the address address2.
        /// </summary>
        /// <value>
        /// The address address2.
        /// </value>
        public string AddressAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the address city.
        /// </summary>
        /// <value>
        /// The address city.
        /// </value>
        public string AddressCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the address.
        /// </summary>
        /// <value>
        /// The state of the address.
        /// </value>
        public string AddressState { get; set; }

        /// <summary>
        /// Gets or sets the address potalcode.
        /// </summary>
        /// <value>
        /// The address potalcode.
        /// </value>
        public string AddressPotalcode { get; set; }

        /// <summary>
        /// Gets or sets the address country.
        /// </summary>
        /// <value>
        /// The address country.
        /// </value>
        public string AddressCountry { get; set; }

        /// <summary>
        /// Gets or sets the alt address1.
        /// </summary>
        /// <value>
        /// The alt address1.
        /// </value>
        public string AltAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the alt address2.
        /// </summary>
        /// <value>
        /// The alt address2.
        /// </value>
        public string AltAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the alt city.
        /// </summary>
        /// <value>
        /// The alt city.
        /// </value>
        public string AltCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the alt.
        /// </summary>
        /// <value>
        /// The state of the alt.
        /// </value>
        public string AltState { get; set; }

        /// <summary>
        /// Gets or sets the alt potalcode.
        /// </summary>
        /// <value>
        /// The alt potalcode.
        /// </value>
        public string AltPotalcode { get; set; }

        /// <summary>
        /// Gets or sets the alt country.
        /// </summary>
        /// <value>
        /// The alt country.
        /// </value>
        public string AltCountry { get; set; }

        /// <summary>
        /// Gets or sets the name of the middle.
        /// </summary>
        /// <value>
        /// The name of the middle.
        /// </value>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the Employee Class Name.
        /// </summary>
        /// <value>
        /// The name of the Employee Class Name.
        /// </value>
        public string EmployeeClassName { get; set; }

        /// <summary>
        /// Gets or sets the cost center code.
        /// </summary>
        /// <value>
        /// The cost center code.
        /// </value>
        public string CostCenterCode { get; set; }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the job start date.
        /// </summary>
        /// <value>
        /// The job start date.
        /// </value>
        public DateTime? JobStartDate { get; set; }

        /// <summary>
        /// Gets or sets the job end date.
        /// </summary>
        /// <value>
        /// The job end date.
        /// </value>
        public DateTime? JobEndDate { get; set; }

        /// <summary>
        /// Gets or sets the job code.
        /// </summary>
        /// <value>
        /// The job code.
        /// </value>
        public string JobCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the job office location code.
        /// </summary>
        /// <value>
        /// The job office location code.
        /// </value>
        public string JobOfficeLocationCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the job office location.
        /// </summary>
        /// <value>
        /// The name of the job office location.
        /// </value>
        public string JobOfficeLocationName { get; set; }

        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        public string Department { get; set; }

        /// <summary>
        /// Gets or sets the work country.
        /// </summary>
        /// <value>
        /// The work country.
        /// </value>
        public string WorkCountry { get; set; }

        /// <summary>
        /// Gets or sets the spouse employee number.
        /// </summary>
        /// <value>
        /// The spouse employee number.
        /// </value>
        public string SpouseEmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the military status.
        /// </summary>
        /// <value>
        /// The name of the military status.
        /// </value>
        public string MilitaryStatusName { get; set; }

        /// <summary>
        /// Gets or sets the name of the employer.
        /// </summary>
        /// <value>
        /// The name of the employer.
        /// </value>
        public string EmployerName { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Employee Class Code.
        /// </summary>
        /// <value>
        /// The Employee Class Code.
        /// </value>
        public string EmployeeClassCode { get; set; }

        /// <summary>
        /// Gets or sets the pay schedule identifier.
        /// </summary>
        /// <value>
        /// The pay schedule identifier.
        /// </value>
        public string PayScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the military status.
        /// </summary>
        /// <value>
        /// The military status.
        /// </value>
        public int? MilitaryStatus { get; set; }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        /// <value>
        /// The dob.
        /// </value>
        public DateTime? Dob { get; set; }

        /// <summary>
        /// Gets or sets the type of the pay.
        /// </summary>
        /// <value>
        /// The type of the pay.
        /// </value>
        public int? PayType { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public int? Gender { get; set; }

        /// <summary>
        /// Gets or sets the service date.
        /// </summary>
        /// <value>
        /// The service date.
        /// </value>
        public DateTime? ServiceDate { get; set; }

        /// <summary>
        /// Gets or sets the hire date.
        /// </summary>
        /// <value>
        /// The hire date.
        /// </value>
        public DateTime? HireDate { get; set; }

        /// <summary>
        /// Gets or sets the re hire date.
        /// </summary>
        /// <value>
        /// The re hire date.
        /// </value>
        public DateTime? ReHireDate { get; set; }

        /// <summary>
        /// Gets or sets the termination date.
        /// </summary>
        /// <value>
        /// The termination date.
        /// </value>
        public DateTime? TerminationDate { get; set; }

        /// <summary>
        /// Gets or sets the state of the work.
        /// </summary>
        /// <value>
        /// The state of the work.
        /// </value>
        public string WorkState { get; set; }

        /// <summary>
        /// Gets or sets the name of the pay schedule.
        /// </summary>
        /// <value>
        /// The name of the pay schedule.
        /// </value>
        public string PayScheduleName { get; set; }

        /// <summary>
        /// Gets or sets the salary.
        /// </summary>
        /// <value>
        /// The salary.
        /// </value>
        public double? Salary { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employee created by email.
        /// </summary>
        /// <value>
        /// The employee created by email.
        /// </value>
        public string EmployeeCreatedByEmail { get; set; }

        /// <summary>
        /// Gets or sets the first name of the employee created by.
        /// </summary>
        /// <value>
        /// The first name of the employee created by.
        /// </value>
        public string EmployeeCreatedByFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the employee created by.
        /// </summary>
        /// <value>
        /// The last name of the employee created by.
        /// </value>
        public string EmployeeCreatedByLastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the alt email.
        /// </summary>
        /// <value>
        /// The alt email.
        /// </value>
        public string AltEmail { get; set; }

        /// <summary>
        /// Gets or sets the work phone.
        /// </summary>
        /// <value>
        /// The work phone.
        /// </value>
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the home phone.
        /// </summary>
        /// <value>
        /// The home phone.
        /// </value>
        public string HomePhone { get; set; }

        /// <summary>
        /// Gets or sets the cell phone.
        /// </summary>
        /// <value>
        /// The cell phone.
        /// </value>
        public string CellPhone { get; set; }

        /// <summary>
        /// Gets or sets the alt phone.
        /// </summary>
        /// <value>
        /// The alt phone.
        /// </value>
        public string AltPhone { get; set; }

        /// <summary>
        /// Gets or sets the office location.
        /// </summary>
        /// <value>
        /// The office location.
        /// </value>
        public string OfficeLocation { get; set; }

        /// <summary>
        /// Gets or sets the empl pay schedule.
        /// </summary>
        /// <value>
        /// The empl pay schedule.
        /// </value>
        public string EmplPaySchedule { get; set; }

        /// <summary>
        /// Gets or sets the dim employee identifier.
        /// </summary>
        /// <value>
        /// The dim employee identifier.
        /// </value>
        public long DimEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the employee.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the custom fields.
        /// </summary>
        /// <value>
        /// The custom fields.
        /// </value>
        public Dictionary<string, object> CustomFields { get; set; }

        /// <summary>
        /// Gets or sets the hours worked.
        /// </summary>
        /// <value>
        /// The hours worked.
        /// </value>
        public double? HoursWorked { get; set; }

        /// <summary>
        /// Gets or sets the hours worked as of date.
        /// </summary>
        /// <value>
        /// The hours worked as of date.
        /// </value>
        public DateTime? HoursWorkedAsOfDate { get; set; }

        /// <summary>
        /// Gets or sets the average minute worked per week.
        /// </summary>
        /// <value>
        /// The hours worked as of date.
        /// </value>
        public double? AverageMinutesWorkedPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the average minute worked as of date.
        /// </summary>
        /// <value>
        /// The hours worked as of date.
        /// </value>
        public DateTime? AverageMinutesWorkedPerWeekAsOfDate { get; set; }

        /// <summary>
        /// Set the SSN for the employee
        /// Map to: ssn
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Set the employee risk profile code if exists.
        /// Map to: risk_profile_code
        /// </summary>
        public string RiskProfileCode { get; set; }

        /// <summary>
        /// Set the risk profile name for the employee here
        /// Map to: risk_profile_name
        /// </summary>
        public string RiskProfileName { get; set; }

        /// <summary>
        /// Employee risk profile description of the employee
        /// Map to: risk_profile_description
        /// </summary>
        public string RiskProfileDescription { get; set; }

        /// <summary>
        /// The risk profile order for the employee. The higher the number, heigher risk involved.
        /// Map to: risk_profile_order
        /// </summary>
        public int? RiskProfileOrder { get; set; }

        /// <summary>
        /// Set the job classification here. Lookup to dim_lookup_empl_job_classification
        /// Map to: job_classification
        /// </summary>
        public long? JobClassification { get; set; }

        /// <summary>
        /// Set the start day of week for the employee. From ENUM get the text.
        /// Map to: start_day_of_week
        /// </summary>
        public string StartDayOfWeek { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_employee ");
            sqlCmd.Append("(ext_ref_id, employee_number, first_name, middle_name, last_name, status, salary, pay_type, gender");
            sqlCmd.Append(", dob, employee_class_code, employee_class_name, cost_center_code, job_title, department, service_date, hire_date, rehire_date, termination_date ");
            sqlCmd.Append(", work_state, work_country, pay_schedule_id, pay_schedule_name, meets50in75mile_rule, key_employee, exempt, military_status");
            sqlCmd.Append(", spouse_employee_number, employer_name, employer_id, customer_id, empl_employee_created_by_email, empl_employee_created_by_first_name ");
            sqlCmd.Append(", empl_employee_created_by_last_name, email, alt_email, work_phone, home_phone, cell_phone, alt_phone, office_location, empl_pay_schedule");
            sqlCmd.Append(", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date");
            sqlCmd.Append(", address1, address2, city, state, postal_code, country, alt_address1, alt_address2, alt_city, alt_state, alt_postal_code, alt_country");
            sqlCmd.Append(", job_start_date, job_end_date, job_code, job_name, job_office_location_code, job_office_location_name, custom_fields, is_deleted, hours_worked, hours_worked_as_of");
            sqlCmd.Append(" ,average_minutes_worked_per_week,average_minutes_worked_per_week_as_of,ssn ,job_classification ,risk_profile_code ,risk_profile_name ,risk_profile_description ,risk_profile_order ,start_day_of_week");
            sqlCmd.Append(", ext_created_by, ext_modified_by");
            sqlCmd.Append(") values (");
            sqlCmd.Append(" @ext_ref_id, @employee_number, @first_name, @middle_name, @last_name, @status, @salary, @pay_type, @gender,");
            sqlCmd.Append(" @dob, @employee_class_code, @employee_class_name, @cost_center_code, @job_title, @department, @service_date, @hire_date, @rehire_date, @termination_date,");
            sqlCmd.Append(" @work_state, @work_country, @pay_schedule_id, @pay_schedule_name, @meets50in75mile_rule, @key_employee, @exempt, @military_status,");
            sqlCmd.Append(" @spouse_employee_number, @employer_name, @employer_id, @customer_id, @empl_employee_created_by_email, @empl_employee_created_by_first_name, ");
            sqlCmd.Append(" @empl_employee_created_by_last_name, @email, @alt_email, @work_phone, @home_phone, @cell_phone, @alt_phone, @office_location, @empl_pay_schedule,");
            sqlCmd.Append(" @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date,");
            sqlCmd.Append(" @address1, @address2, @city, @state, @postal_code, @country, @alt_address1, @alt_address2, @alt_city, @alt_state, @alt_postal_code, @alt_country,");
            sqlCmd.Append(" @job_start_date, @job_end_date, @job_code, @job_name, @job_office_location_code, @job_office_location_name, @custom_fields, @is_deleted, @hours_worked, @hours_worked_as_of");
            sqlCmd.Append(" ,@average_minutes_worked_per_week,@average_minutes_worked_per_week_as_of,@ssn ,@job_classification ,@risk_profile_code ,@risk_profile_name ,@risk_profile_description ,@risk_profile_order ,@start_day_of_week");
            sqlCmd.Append(", @ext_created_by, @ext_modified_by");
            sqlCmd.Append(") RETURNING dim_employee_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number", EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_name", FirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@middle_name", MiddleName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_name", LastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status));
                cmd.Parameters.Add(new NpgsqlParameter("@salary", Salary.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_type", PayType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@gender", Gender.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dob", Dob.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_class_code", EmployeeClassCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_class_name", EmployeeClassName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cost_center_code", CostCenterCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_title", JobTitle.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@department", Department.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@service_date", ServiceDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hire_date", HireDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@rehire_date", ReHireDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@termination_date", TerminationDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_state", WorkState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_country", WorkCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_schedule_id", PayScheduleId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_schedule_name", PayScheduleName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@meets50in75mile_rule", Meets50In75MileRule.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@key_employee", IsKeyEmployee.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@exempt", IsExempt.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@military_status", MilitaryStatus.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@spouse_employee_number", SpouseEmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_name", EmployerName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@empl_employee_created_by_email", EmployeeCreatedByEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@empl_employee_created_by_first_name", EmployeeCreatedByFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@empl_employee_created_by_last_name", EmployeeCreatedByLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@email", Email.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_email", AltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_phone", WorkPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@home_phone", HomePhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cell_phone", CellPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_phone", AltPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@office_location", OfficeLocation.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@empl_pay_schedule", EmplPaySchedule.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@address1", AddressAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address2", AddressAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@city", AddressCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@state", AddressState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@postal_code", AddressPotalcode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@country", AddressCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_address1", AltAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_address2", AltAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_city", AltCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_state", AltState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_postal_code", AltPotalcode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_country", AltCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_start_date", JobStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_end_date", JobEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_code", JobCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_name", JobName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_office_location_code", JobOfficeLocationCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_office_location_name", JobOfficeLocationName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@custom_fields", NpgsqlTypes.NpgsqlDbType.Jsonb) { Value = CustomFields.JsonOrDbNull() });
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@hours_worked", HoursWorked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hours_worked_as_of", HoursWorkedAsOfDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@average_minutes_worked_per_week", AverageMinutesWorkedPerWeek.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@average_minutes_worked_per_week_as_of", AverageMinutesWorkedPerWeekAsOfDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ssn", SSN.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_classification", JobClassification.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_code", RiskProfileCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_name", RiskProfileName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_description", RiskProfileDescription.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_order", RiskProfileOrder.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_day_of_week", StartDayOfWeek.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployeeId = Convert.ToInt64(res);
            }
        }
    }
}
