﻿using AbsenceSoft.Etl.Data.Warehouse;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCommunicationPaperwork :DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCommunicationPaperwork(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCommunicationPaperwork"/> class.
        /// </summary>
        public DimCommunicationPaperwork() : base() { }
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_COMMUNICATION_PAPERWORK; } }
        public long DimCommunicationPaperworkId { get; set; }
        public string CommunicationId { get; set; }        
        public string EmployerId { get; set; }
        public string EmployeeId { get; set; }
        public string CustomerId { get; set; }
        public string AttachmentId	{ get; set; }
        public bool Checked	 { get; set; }
		public DateTime? DueDate{ get; set; }
        public string ReturnAttachmentId	{ get; set; }
		public int Status	{ get; set; }
        public DateTime? StatusDate	 { get; set; }
        public string PaperworkId	{ get; set; }
        public string PaperworkCreatedBy	{ get; set; }
        public DateTime? PaperworkCreatedDate	{ get; set; }
        public string PaperworkCode	{ get; set; }
        public string PaperworkCustomerId	{ get; set; }
        public string PaperworkDescription	{ get; set; }
        public int PaperworkDocType { get; set; }
        public string PaperworkEmployerId	{ get; set; }
        public string PaperworkEnclosure	{ get; set; }		
        public string PaperworkFileId	{ get; set; }
        public string PaperworkFileName	{ get; set; }
        public bool PaperworkIsDeleted { get; set; }
        public string PaperworkModifiedBy { get; set; }
        public DateTime? PaperworkModifiedDate	{ get; set; }
        public string PaperworkName	{ get; set; }
        public bool PaperworkRequiresReview	{ get; set; }
        public TimeSpan? PaperworkReturnDateAdjustment	{ get; set; }
        public int? PaperworkReturnDateAdjustmentDays	{ get; set; }
        public DateTime? CreatedDate { get; set; }
        public string RecordHash { get; set; }

        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimCommunicationPaperwork> LoadHistory(string caseId)
        {
            return Execute(conn => SqlLoadHistory(conn, caseId));
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_communication_paperwork
                    (communication_id,ext_ref_id ,attachment_id ,checked,due_date, return_attachment_id ,status	,status_date,paperwork_id	,paperwork_cby	
                    ,paperwork_cdt	,paperwork_code	,paperwork_customer_id	,paperwork_description	,paperwork_doc_type ,paperwork_employer_id	,paperwork_enclosure_text	
                    ,paperwork_file_id	,paperwork_file_name ,paperwork_is_deleted,paperwork_mby	,paperwork_mdt	,paperwork_name	,paperwork_requires_review	
                    ,paperwork_return_date_adjustment	,paperwork_return_date_adjustment_days, ext_created_date , ext_modified_date , dim_ver_id, ver_is_current , ver_seq , ver_is_expired 
                    , ver_root_id , ver_prev_id , ver_history_count , ext_created_by , ext_modified_by, is_deleted)
                    values
                    (@communication_id, @ext_ref_id ,@attachment_id ,@checked,@due_date,@return_attachment_id ,@status	,@status_date,@paperwork_id	,@paperwork_cby	
                    ,@paperwork_cdt	,@paperwork_code	,@paperwork_customer_id	,@paperwork_description	,@paperwork_doc_type ,@paperwork_employer_id	,@paperwork_enclosure_text	
                    ,@paperwork_file_id	,@paperwork_file_name ,@paperwork_is_deleted , @paperwork_mby	,@paperwork_mdt	,@paperwork_name	,@paperwork_requires_review	
                    ,@paperwork_return_date_adjustment	,@paperwork_return_date_adjustment_days	,@ext_created_date ,@ext_modified_date ,@dim_ver_id,@ver_is_current ,@ver_seq ,@ver_is_expired 
                    ,@ver_root_id ,@ver_prev_id ,@ver_history_count, @ext_created_by, @ext_modified_by, @is_deleted)
                    RETURNING dim_communication_paperwork_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_id", CommunicationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@attachment_id", AttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@checked",Checked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@due_date",DueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@return_attachment_id",ReturnAttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status",Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status_date",StatusDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_id",PaperworkId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_cby",PaperworkCreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_cdt",PaperworkCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_code",PaperworkCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_customer_id",PaperworkCustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_description",PaperworkDescription.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_doc_type",PaperworkDocType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_employer_id",PaperworkEmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_enclosure_text",PaperworkEnclosure.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_file_id",PaperworkFileId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_file_name",PaperworkFileName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_is_deleted",PaperworkIsDeleted.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_mby",PaperworkModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_mdt",PaperworkModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_name",PaperworkName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_requires_review",PaperworkRequiresReview.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_return_date_adjustment",PaperworkReturnDateAdjustment.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_return_date_adjustment_days",PaperworkReturnDateAdjustmentDays.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCommunicationPaperworkId = Convert.ToInt64(res);
            }
        }

        protected List<DimCommunicationPaperwork> SqlLoadHistory(NpgsqlConnection pgsqlConnection, string communicationId)
        {
            List<DimCommunicationPaperwork> lstCommPaperworks = new List<DimCommunicationPaperwork>();

            string sql = @" SELECT ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired
                        , ver_root_id, ver_prev_id, Ver_history_count, communication_id,  attachment_id , checked, due_date, return_attachment_id 
                        , status, status_date, paperwork_id, paperwork_cby, paperwork_cdt, paperwork_code, paperwork_customer_id, paperwork_description
                        , paperwork_doc_type, paperwork_employer_id, paperwork_enclosure_text, paperwork_file_id, paperwork_file_name , paperwork_is_deleted
                        , paperwork_mby, paperwork_mdt, paperwork_name, paperwork_requires_review, paperwork_return_date_adjustment, paperwork_return_date_adjustment_days
                        , is_deleted, dim_communication_paperwork_id, record_hash, ext_created_by, ext_modified_by
                         FROM dim_communication_paperwork
                         WHERE communication_id = @communication_id
                         ORDER BY ver_seq";

            using (NpgsqlCommand command = new NpgsqlCommand(sql, pgsqlConnection))
            {
                command.Parameters.Add(new NpgsqlParameter("@communication_id", communicationId));
                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        DimCommunicationPaperwork dimCommPaperwork = new DimCommunicationPaperwork(ConnectionString)
                        {
                            ExternalReferenceId = dr.GetValueOrDefault<string>(i++),
                            ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++),
                            ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++),
                            DimVerId = dr.GetValueOrDefault<long>(i++),
                            VersionIsCurrent = dr.GetValueOrDefault<bool>(i++),
                            VersionSequence = dr.GetValueOrDefault<long>(i++),
                            VersionIsExpired = dr.GetValueOrDefault<bool>(i++),
                            VersionRootId = dr.GetValueOrDefault<long>(i++),
                            VersionPreviousId = dr.GetValueOrDefault<long>(i++),
                            VersionHistoryCount = dr.GetValueOrDefault<long>(i++),
                            CommunicationId = dr.GetValueOrDefault<string>(i++),                           
                            AttachmentId = dr.GetValueOrDefault<string>(i++),
                            Checked = dr.GetValueOrDefault<bool>(i++),
                            DueDate = dr.GetValueOrDefault<DateTime>(i++),
                            ReturnAttachmentId = dr.GetValueOrDefault<string>(i++),
                            Status = dr.GetValueOrDefault<int>(i++),
                            StatusDate = dr.GetValueOrDefault<DateTime>(i++),
                            PaperworkId = dr.GetValueOrDefault<string>(i++),
                            PaperworkCreatedBy = dr.GetValueOrDefault<string>(i++),
                            PaperworkCreatedDate = dr.GetValueOrDefault<DateTime>(i++),
                            PaperworkCode = dr.GetValueOrDefault<string>(i++),
                            PaperworkCustomerId = dr.GetValueOrDefault<string>(i++),
                            PaperworkDescription = dr.GetValueOrDefault<string>(i++),
                            PaperworkDocType = dr.GetValueOrDefault<int>(i++),
                            PaperworkEmployerId = dr.GetValueOrDefault<string>(i++),
                            PaperworkEnclosure = dr.GetValueOrDefault<string>(i++),
                            PaperworkFileId = dr.GetValueOrDefault<string>(i++),
                            PaperworkFileName = dr.GetValueOrDefault<string>(i++),
                            PaperworkIsDeleted = dr.GetValueOrDefault<bool>(i++),
                            PaperworkModifiedBy = dr.GetValueOrDefault<string>(i++),
                            PaperworkModifiedDate = dr.GetValueOrDefault<DateTime>(i++),
                            PaperworkName = dr.GetValueOrDefault<string>(i++),
                            PaperworkRequiresReview = dr.GetValueOrDefault<bool>(i++),
                            PaperworkReturnDateAdjustment = dr.GetValueOrDefault<TimeSpan>(i++),
                            PaperworkReturnDateAdjustmentDays = dr.GetValueOrDefault<int>(i++),
                            IsDeleted = dr.GetValueOrDefault<bool>(i++),
                            DimCommunicationPaperworkId = dr.GetValueOrDefault<long>(i++),
                            RecordHash = dr.GetValueOrDefault<string>(i++),
                            CreatedBy = dr.GetValueOrDefault<string>(i++),
                            ModifiedBy = dr.GetValueOrDefault<string>(i),
                        };
                        lstCommPaperworks.Add(dimCommPaperwork);
                    }
                }
            }

            return lstCommPaperworks;
        }
    }
    }
