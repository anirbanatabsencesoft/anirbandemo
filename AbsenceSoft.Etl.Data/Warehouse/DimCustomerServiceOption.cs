﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCustomerServiceOption : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomerServiceOption"/> class.
        /// </summary>
        public DimCustomerServiceOption() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomerServiceOption"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCustomerServiceOption(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_CUSTOMER_SERVICE_OPTION; } }

        /// <summary>
        /// Gets or sets the dim employeer service option identifier.
        /// </summary>
        /// <value>
        /// The dim employeer service option.
        /// </value>
        public long DimCustomerServiceOptionId { get; set; }

        /// <summary>
        /// Gets or sets the dim customer identifier.
        /// </summary>
        /// <value>
        /// The dim customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Key.
        /// </summary>
        /// <value>
        /// The Key attribute.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value attribute.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_customer_service_option")
                .Append("(customer_id, key, value, \"order\", is_deleted, dim_ver_id, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count")
                .Append(") values (")
                .Append("@customer_id, @key, @value, @order, @is_deleted, @dim_ver_id, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count")
                .Append(") RETURNING dim_customer_service_option_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@key", Key.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@value", Value.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@order", Order));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));

                object res = cmd.ExecuteScalar();
                DimCustomerServiceOptionId = Convert.ToInt64(res);
            }
        }
    }
}
