﻿using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Table: dim_case_policy 
    /// </summary>
    public class DimCasePolicy : DimBase
    {

        #region Constructor chain
        /// <summary>
        /// Constructor with the connection string
        /// </summary>
        /// <param name="connectionString">Connection string pf Postgres</param>
        public DimCasePolicy(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Default constructor for DimCasePolicy
        /// </summary>
        public DimCasePolicy() : base() { }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_POLICY; } }

        /// <summary>
        /// Maped to dim_case_policy_id
        /// </summary>
        public long DimCasePolicyId { get; set; }

        /// <summary>
        /// Map to: ext_created_by 
        /// Derive from Case .
        /// Case Modified By If not new.
        /// </summary>
        [MaxLength(24)]
        public string ExternalCreatedBy { get; set; }

        /// <summary>
        /// Map to: ext_modified_by
        /// Derive from Case
        /// </summary>
        [MaxLength(24)]
        public string ExternalModifiedBy { get; set; }

        /// <summary>
        /// Map to case_id
        /// </summary>
        [MaxLength(24), Required]
        public string CaseId { get; set; }

        /// <summary>
        /// Map to: employee_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Map to: customer_id
        /// </summary>
        [MaxLength(24), Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// Map to: employer_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployerId { get; set; }

        /// <summary>
        /// Map to: policy_code
        /// </summary>
        [MaxLength(50), Required]
        public string PolicyCode { get; set; }

        /// <summary>
        /// Map to: policy_name
        /// </summary>
        [MaxLength(255), Required]
        public string PolicyName { get; set; }

        /// <summary>
        /// Map to: start_date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Map to: end_date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Map to: case_type
        /// Lookup: dim_lookup_case_types
        /// </summary>
        public int? CaseType { get; set; }

        /// <summary>
        /// Map to: eligibility
        /// Lookup:  dim_lookup. Type: eligibility_status
        /// </summary>
        public int? Eligibility { get; set; }

        /// <summary>
        /// Map to: determination
        /// Lookup: dim_lookup_case_determination
        /// </summary>
        public int? Determination { get; set; }

        /// <summary>
        /// Map to: denial_reason
        /// Lookup: dim_lookup_case_denial_reason
        /// </summary>
        public int? DenialReason { get; set; }

        /// <summary>
        /// Map to: denial_reason_other
        /// </summary>
        public string OtherDenialReason { get; set; }

        /// <summary>
        /// Map to: denial_reason_code
        /// </summary>
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Map to: denial_reason_description
        /// </summary>
        public string DenialReasonDescription { get; set; }

        /// <summary>
        /// Map to: payout_percentage
        /// </summary>
        public decimal? PayoutPercentage { get; set; }

        /// <summary>
        /// Map to: crosswalk_codes
        /// </summary>
        public Dictionary<string, string> CrosswalkCodes {get; set;}


        private DimCasePolicyUsage _dimCasePolicyUsage;

        /// <summary>
        /// Map to: actual_duration
        /// </summary>
        public int? ActualDuration { get; set; }

        /// <summary>
        /// Map to: exceeded_duration
        /// </summary>
        public int? ExceededDuration { get; set; }

        /// <summary>
        /// Returns the Case Policy Usage
        /// </summary>
        public DimCasePolicyUsage CasePolicyUsage
        {
            get
            {
                if (_dimCasePolicyUsage == null)
                    _dimCasePolicyUsage = new DimCasePolicyUsage();

                return _dimCasePolicyUsage;
            }
        }

        /// <summary>
        /// Gets or sets the record hash. This value is used to compare versions of the policy
        /// and is a direct replacement for the absence of a valid modified date on the policy itself.
        /// </summary>
        /// <value>
        /// The record hash. Basically an MD5 hash checksum of the record JSON.
        /// </value>
        public string RecordHash { get; set; }

        /// <summary>
        /// Map to: SegmentId
        /// </summary>
        [Required]
        public string SegmentId { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(sqlDelete);
        }

        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            sqlDelete(pgsqlConnection);
        }


        /// <summary>
        /// Returns the SQL
        /// </summary>
        /// <returns></returns>
        private string GetSQLString()
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("INSERT INTO dim_case_policy (");

            //system fields
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");

            //dimension fields
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            sqlCmd.Append(",policy_code");
            sqlCmd.Append(",policy_name");
            sqlCmd.Append(",start_date");
            sqlCmd.Append(",end_date");
            sqlCmd.Append(",case_type");
            sqlCmd.Append(",eligibility");
            sqlCmd.Append(",determination");
            sqlCmd.Append(",denial_reason_other");
            sqlCmd.Append(",denial_reason_code");
            sqlCmd.Append(",denial_reason_description");
            sqlCmd.Append(",payout_percentage");
            sqlCmd.Append(",record_hash");
            sqlCmd.Append(",crosswalk_codes");

            sqlCmd.Append(", actual_duration, exceeded_duration,segment_id");

            sqlCmd.Append(") VALUES (");

            //system parameters
            sqlCmd.Append("@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count");

            //dimension parameters
            sqlCmd.Append(",@ext_created_by");
            sqlCmd.Append(",@ext_modified_by");
            sqlCmd.Append(",@case_id");
            sqlCmd.Append(",@employee_id");
            sqlCmd.Append(",@customer_id");
            sqlCmd.Append(",@employer_id");
            sqlCmd.Append(",@policy_code");
            sqlCmd.Append(",@policy_name");
            sqlCmd.Append(",@start_date");
            sqlCmd.Append(",@end_date");
            sqlCmd.Append(",@case_type");
            sqlCmd.Append(",@eligibility");
            sqlCmd.Append(",@determination");
            sqlCmd.Append(",@denial_reason_other");
            sqlCmd.Append(",@denial_reason_code");
            sqlCmd.Append(",@denial_reason_description");
            sqlCmd.Append(",@payout_percentage");
            sqlCmd.Append(",@record_hash");
            sqlCmd.Append(",@crosswalk_codes");

            sqlCmd.Append(", @actual_duration, @exceeded_duration,@segmentid");

            sqlCmd.Append(") RETURNING dim_case_policy_id");

            return sqlCmd.ToString();
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(GetSQLString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", ExternalCreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ExternalModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_code", PolicyCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_name", PolicyName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_type", CaseType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@eligibility", Eligibility.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination", Determination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_other", OtherDenialReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_code", DenialReasonCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_description", DenialReasonDescription.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@payout_percentage", PayoutPercentage.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@record_hash", RecordHash.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@crosswalk_codes", NpgsqlDbType.Jsonb) { Value = CrosswalkCodes.JsonOrDbNull() });

                cmd.Parameters.Add(new NpgsqlParameter("@actual_duration", ActualDuration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@exceeded_duration", ExceededDuration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@segmentid", SegmentId.ValueOrDbNull()));
                object res = cmd.ExecuteScalar();
                DimCasePolicyId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimCasePolicy> LoadHistory(string caseId)
        {
            return Execute(conn => sqlLoadHistory(conn, caseId));
        }


        /// <summary>
        /// Get the data from Postgres against a case id
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        protected List<DimCasePolicy> sqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimCasePolicy> lstCasePolicies = new List<DimCasePolicy>();

            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("SELECT ");
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            sqlCmd.Append(",policy_code");
            sqlCmd.Append(",policy_name");
            sqlCmd.Append(",start_date");
            sqlCmd.Append(",end_date");
            sqlCmd.Append(",case_type");
            sqlCmd.Append(",eligibility");
            sqlCmd.Append(",determination");
            sqlCmd.Append(",denial_reason_other");
            sqlCmd.Append(",denial_reason_code");
            sqlCmd.Append(",denial_reason_description");
            sqlCmd.Append(",payout_percentage");
            sqlCmd.Append(",record_hash");
            sqlCmd.Append(",crosswalk_codes");
            sqlCmd.Append(",actual_duration");
            sqlCmd.Append(",exceeded_duration");
            sqlCmd.Append(" FROM dim_case_policy WHERE case_id = @case_id and ver_is_current = true");
            sqlCmd.Append(" ORDER BY ver_seq");

            using (NpgsqlCommand command = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                //add parameters to search
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));

                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        //instantiate
                        DimCasePolicy dimCP = new DimCasePolicy(ConnectionString);

                        dimCP.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.DimVerId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsCurrent = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionSequence = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsExpired = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionRootId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionPreviousId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionHistoryCount = dr.GetValueOrDefault<long>(i++);
                        
                        dimCP.ExternalCreatedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalModifiedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.CaseId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployeeId = dr.GetValueOrDefault<string>(i++);
                        dimCP.CustomerId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployerId = dr.GetValueOrDefault<string>(i++);
                        dimCP.PolicyCode = dr.GetValueOrDefault<string>(i++);
                        dimCP.PolicyName = dr.GetValueOrDefault<string>(i++);
                        dimCP.StartDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.EndDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.CaseType = dr.GetValueOrDefault<int>(i++);
                        dimCP.Eligibility = dr.GetValueOrDefault<int>(i++);
                        dimCP.Determination = dr.GetValueOrDefault<int>(i++);
                        dimCP.OtherDenialReason = dr.GetValueOrDefault<string>(i++);
                        dimCP.DenialReasonCode = dr.GetValueOrDefault<string>(i++);
                        dimCP.DenialReasonDescription = dr.GetValueOrDefault<string>(i++);
                        dimCP.PayoutPercentage = dr.GetValueOrDefault<decimal>(i++);
                        dimCP.RecordHash = dr.GetValueOrDefault<string>(i++);
                        
                        string crosswalkCodes = dr.GetValueOrDefault<string>(i++);

                        if (!string.IsNullOrEmpty(crosswalkCodes))
                        {
                            dimCP.CrosswalkCodes = crosswalkCodes.DeserializeJson<Dictionary<string, string>>();
                        }
                        //add to list
                        lstCasePolicies.Add(dimCP);
                    }
                }
            }

            return lstCasePolicies;
        }


        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void sqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_case_policy where case_id = @dim_case_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_id", CaseId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}

