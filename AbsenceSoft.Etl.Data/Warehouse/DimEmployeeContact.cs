﻿using Npgsql;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployeeContact : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeContact"/> class.
        /// </summary>
        public DimEmployeeContact() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeContact"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeContact(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_CONTACT; } }
        
        /// <summary>
        /// Gets or sets the dim employee contact identifier.
        /// </summary>
        /// <value>
        /// The dim employee contact identifier.
        /// </value>
        public long DimEmployeeContactId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }
        
        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of this contact is to the employee.
        /// </summary>
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the contact type.
        /// </summary>
        /// <value>
        /// The name of the contact type.
        /// </value>
        public string ContactTypeName { get; set; }
        
        /// <summary>
        /// Gets or sets the contact's company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Title address.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the contact's First Name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Middle Name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Last Name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the contact's Date OF Birth.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the contact's Email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the contact's Alternate Email Address.
        /// </summary>
        public string AltEmail { get; set; }

        /// <summary>
        /// Gets or sets the contact's Work Phone.
        /// </summary>
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the contact's Home Phone.
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Gets or sets the contact's Cell Phone.
        /// </summary>
        public string CellPhone { get; set; }

        /// <summary>
        /// Gets or sets the contact's fax phone number.
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>
        /// The street.
        /// </value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the 2nd address line such as a unit number, apartment or other information
        /// for international addresses.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the address' city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address' state or province.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the address' mailing postal code, in the US this is the ZIP Code.
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the country's 2-digit ISO code.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the is primary.
        /// </summary>
        /// <value>
        /// The is primary.
        /// </value>
        public bool? IsPrimary { get; set; }

        /// <summary>
        /// Gets or sets the if a HCP contact person (not to be confused with the HCP provider's name).
        /// </summary>
        public string ContactPersonName { get; set; }
        
        /// <summary>
        /// Gets or sets the years of age.
        /// </summary>
        /// <value>
        /// The years of age.
        /// </value>
        public double? YearsOfAge { get; set; }

        /// <summary>
        /// Gets or sets the military status.
        /// </summary>
        /// <value>
        /// The military status.
        /// </value>
        public int? MilitaryStatus { get; set; }

        /// <summary>
        /// Gets or sets the contact position.
        /// </summary>
        /// <value>
        /// The contact position.
        /// </value>
        public int? ContactPosition { get; set; }

                        
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection conn)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_employee_contact (";
            sqlCmd += "ext_ref_id, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date";
            sqlCmd += ", employee_id, employer_id, customer_id, ext_created_by, ext_modified_by, contact_type_code, contact_type_name, company_name";
            sqlCmd += ", title, first_name, middle_name, last_name, dob, email, alt_email, work_phone, home_phone, cell_phone, fax";
            sqlCmd += ", street, address1, address2, city, state, postal_code, country";
            sqlCmd += ", is_primary, contact_person_name, years_of_age, military_status, contact_position, employee_number";
            sqlCmd += ", is_deleted";
            sqlCmd += ") values (";
            sqlCmd += "@ext_ref_id, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date";
            sqlCmd += ", @employee_id, @employer_id, @customer_id, @ext_created_by, @ext_modified_by, @contact_type_code, @contact_type_name, @company_name";
            sqlCmd += ", @title, @first_name, @middle_name, @last_name, @dob, @email, @alt_email, @work_phone, @home_phone, @cell_phone, @fax";
            sqlCmd += ", @street, @address1, @address2, @city, @state, @postal_code, @country";
            sqlCmd += ", @is_primary, @contact_person_name, @years_of_age, @military_status, @contact_position, @employee_number";
            sqlCmd += ", @is_deleted) RETURNING dim_employee_contact_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_code", ContactTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_name", ContactTypeName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@company_name", CompanyName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@title", Title.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_name", FirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@middle_name", MiddleName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_name", LastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dob", DateOfBirth.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@email", Email.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_email", AltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_phone", WorkPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@home_phone", HomePhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cell_phone", CellPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@fax", Fax.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@street", Street.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address1", Address1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@address2", Address2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@city", City.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@state", State.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@postal_code", PostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@country", Country.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_primary", IsPrimary.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_person_name", ContactPersonName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@years_of_age", YearsOfAge.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@military_status", MilitaryStatus.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_position", ContactPosition.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number", EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                object res = cmd.ExecuteScalar();
                DimEmployeeContactId = Convert.ToInt64(res);
            }
        }
    }
}
