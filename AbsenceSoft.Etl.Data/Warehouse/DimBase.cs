﻿using Npgsql;
using System;
using System.Data;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public abstract class DimBase : BaseBase
    {
        private string _tableName;

        /// <summary>
        /// Initializes a new instance of the <see cref="DimBase"/> class.
        /// </summary>
        public DimBase() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimBase"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimBase(string connectionString) : this()
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public abstract int DimensionId { get; }

        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <value>
        /// The name of the table.
        /// </value>
        public string TableName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_tableName))
                    _tableName = GetDimensionTableName();
                return _tableName;
            }
        }

        /// <summary>
        /// Gets or sets the dim version identifier.
        /// </summary>
        /// <value>
        /// The dim ver identifier.
        /// </value>
        public long DimVerId { get; set; }

        /// <summary>
        /// Gets or sets the external created date.
        /// </summary>
        /// <value>
        /// The external created date.
        /// </value>
        public DateTime ExternalCreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the external modified date.
        /// </summary>
        /// <value>
        /// The external modified date.
        /// </value>
        public DateTime ExternalModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the external reference identifier.
        /// </summary>
        /// <value>
        /// The external reference identifier.
        /// </value>
        public string ExternalReferenceId { get; set; }

        /// <summary>
        /// Gets or sets the version sequence.
        /// </summary>
        /// <value>
        /// The version sequence.
        /// </value>
        public long VersionSequence { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [version is expired].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [version is expired]; otherwise, <c>false</c>.
        /// </value>
        public bool VersionIsExpired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [version is current].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [version is current]; otherwise, <c>false</c>.
        /// </value>
        public bool VersionIsCurrent { get; set; }

        /// <summary>
        /// Gets or sets the version root identifier.
        /// </summary>
        /// <value>
        /// The version root identifier.
        /// </value>
        public long VersionRootId { get; set; }

        /// <summary>
        /// Gets or sets the version previous identifier.
        /// </summary>
        /// <value>
        /// The version previous identifier.
        /// </value>
        public long VersionPreviousId { get; set; }

        /// <summary>
        /// Gets or sets the version history count.
        /// </summary>
        /// <value>
        /// The version history count.
        /// </value>
        public long VersionHistoryCount { get; set; }

        /// <summary>
        /// The object created by. This has to be the ID field.
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// The object modified by. This has to be the ID field.
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets a new dim_ver record for this dim base as a new version.
        /// </summary>
        /// <param name="dimensionId">The dimension identifier.</param>
        /// <param name="dimTableName">Name of the dim table.</param>
        /// <returns></returns>
        public DimVer NewVersion()
        {
            DimVer ver = Execute(CheckForVersion);

            if (ver != null && !HasError && ver.VersionHistoryCount > 0)
                Execute(conn => UpdateOldDimensionVersions(conn, ver));

            if (ver != null && !HasError && !ver.HasError)
            {
                DimVerId = ver.DimVerId;
                VersionSequence = ver.VersionSequence;
                VersionRootId = ver.VersionRootId;
                VersionPreviousId = ver.VersionPreviousId;
                VersionHistoryCount = ver.VersionHistoryCount;
            }

            return ver;
        }

        /// <summary>
        /// Overloaded method
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public DimVer NewVersion(NpgsqlConnection conn)
        {
            DimVer ver = CheckForVersion(conn);

            if (ver != null && !HasError && ver.VersionHistoryCount > 0)
                UpdateOldDimensionVersions(conn, ver);

            if (ver != null && !HasError && !ver.HasError)
            {
                DimVerId = ver.DimVerId;
                VersionSequence = ver.VersionSequence;
                VersionRootId = ver.VersionRootId;
                VersionPreviousId = ver.VersionPreviousId;
                VersionHistoryCount = ver.VersionHistoryCount;
            }

            return ver;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public virtual void Save()
        {
            throw new NotImplementedException();
        }

        public virtual void Save(Npgsql.NpgsqlConnection pgsqlConnection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the name of the dimension table.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <returns></returns>
        private string GetDimensionTableName()
        {
            using (NpgsqlConnection syncConn = new NpgsqlConnection(new Config().SynchronizerConnectionString))
            using (NpgsqlCommand cmd = new NpgsqlCommand("select table_name from operational.dimension where dimension_id = @dimension_id limit 1", syncConn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dimension_id", DimensionId));
                syncConn.Open();
                return cmd.ExecuteScalar() as string;
            }
        }
                

        /// <summary>
        /// Updates the old dimension versions.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <param name="ver">The ver.</param>
        private void UpdateOldDimensionVersions(NpgsqlConnection conn, DimVer ver)
        {
            StringBuilder sql = new StringBuilder()
                .AppendFormat("UPDATE {0} SET ", TableName)
                .Append("ver_is_current = false, ")
                .Append("ver_seq = @ver_seq, ")
                .Append("ver_history_count = @ver_history_count ")
                .Append("WHERE ver_is_current AND ext_ref_id = @ext_ref_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", ver.VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", ver.VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ver.ExternalReferenceId));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Check whether the object has old version or not
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        private DimVer CheckForVersion(NpgsqlConnection conn)
        {
            StringBuilder sql = new StringBuilder()
                .Append("SELECT ")
                .AppendFormat("(SELECT COUNT(1) as count FROM {0} WHERE ext_ref_id = @ext_ref_id)", TableName)
                .Append(" ,ext_ref_id, ext_created_date, ext_modified_date, ver_is_current")
                .AppendFormat(" FROM {0} ", TableName)
                .Append(" WHERE ver_is_current AND ext_ref_id = @ext_ref_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));

                using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow | CommandBehavior.SingleResult))
                {
                    if (reader.Read())
                    {
                        return new DimVer(ConnectionString)
                        {
                            DimVerId = Const.VER_SEQ_NEWEST_VALUE,
                            VersionHistoryCount = reader.GetValueOrDefault<long>(0),
                            ExternalReferenceId = reader.GetValueOrDefault<string>(1),
                            ExternalCreatedDate = reader.GetValueOrDefault<DateTime>(2),
                            ExternalModifiedDate = reader.GetValueOrDefault<DateTime>(3),
                            VersionIsCurrent = reader.GetValueOrDefault<bool>(4)
                            //VersionRootId = reader.GetValueOrDefault<long>(8),
                            //VersionPreviousId = reader.GetValueOrDefault<long>(9),
                        }.SetDimensionId(reader.GetValueOrDefault<int>(1));
                    }
                }
            }

            return new DimVer(ConnectionString)
            {
                DimVerId = Const.VER_SEQ_NEWEST_VALUE,
                VersionHistoryCount = Const.DEFAULT_HISTORY_COUNT,
                ExternalReferenceId = ExternalReferenceId,
                ExternalCreatedDate = ExternalCreatedDate,
                ExternalModifiedDate = ExternalModifiedDate,
                VersionIsCurrent = true,
            };
        }
    }
}











