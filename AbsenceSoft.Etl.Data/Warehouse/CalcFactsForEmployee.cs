﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class CalcFactsForEmployee
    {
        private DimEmployee dimEmployee;
        private DimEmployerAccess dimEmployerAccess;
        private DimEmployer dimEmployer;
        private DimCases dimCases;
        private DimTodoItem dimTodos;
       
        public CalcFactsForEmployee()
        {
            dimEmployee = new DimEmployee();
            dimEmployerAccess = new DimEmployerAccess();
            dimEmployer = new DimEmployer();
            dimCases = new DimCases();
            dimTodos = new DimTodoItem();

        }

        #region EmployeesSpecific
        public Int64 EmplAgeInYears(DateTime BirthDate)
        {
            Int64 _nEmplAgeInYears = 0;
            _nEmplAgeInYears = DateTime.Today.Year - BirthDate.Year;
            if (DateTime.Now.Month < BirthDate.Month || (DateTime.Now.Month == BirthDate.Month && DateTime.Now.Day < BirthDate.Day))
                _nEmplAgeInYears--;
            return _nEmplAgeInYears;
        }

        public Int64 EmplDaysOfService(DateTime ServiceDate)
        {
            Int64 _nEmplDaysOfService = 0;
            _nEmplDaysOfService = (DateTime.Now.Date - ServiceDate.Date).Days;
            return _nEmplDaysOfService;
        }

        public Int64 EmplWeeksOfService(DateTime ServiceDate)
        {
            Int64 _nEmplWeeksOfService = 0;
            _nEmplWeeksOfService = (DateTime.Now.Date - ServiceDate.Date).Days / 7;
            return _nEmplWeeksOfService;
        }

        public Int64 EmplMonthsOfService(DateTime ServiceDate)
        {
            Int64 _nEmplMonthsOfService = 0;
            if (ServiceDate > DateTime.Today)
            {
                return  -1;
            }
            _nEmplMonthsOfService = ((DateTime.Now.Year * 12) + DateTime.Now.Month) - ((ServiceDate.Year * 12) + ServiceDate.Month);
            if (DateTime.Now.Day >= ServiceDate.Day)
            {
                _nEmplMonthsOfService++;
            }
            return _nEmplMonthsOfService;
        }

        public Int64 EmplYearsOfService(DateTime ServiceDate)
        {
            Int64 _nEmplYearsOfService = 0;
            _nEmplYearsOfService = (DateTime.Now.Year - ServiceDate.Year - 1) +
                    (((DateTime.Now.Month > ServiceDate.Month) ||
                        ((DateTime.Now.Month == ServiceDate.Month) && (DateTime.Now.Day >= ServiceDate.Day))) ? 1 : 0);
            return _nEmplYearsOfService;
        }

        public Int64 EmplCountCases(string EmployeeID)
        {
            Int64 _nEmplCountCases = 0;
            _nEmplCountCases = dimCases.EmplCountCases(EmployeeID);
            return _nEmplCountCases;
        }

        public Int64 EmplCountOpenCases(string EmployeeID)
        {
            Int64 _nEmplCountOpenCases = 0;
            _nEmplCountOpenCases = dimCases.EmplCountOpenCases(EmployeeID);
            return _nEmplCountOpenCases;
        }

        public Int64 EmplCountClosedCases(string EmployeeID)
        {
            Int64 _nEmplCountClosedCases = 0;
            _nEmplCountClosedCases = dimCases.EmplCountClosedCases(EmployeeID);
            return _nEmplCountClosedCases;
        }

        public Int64 EmplCountTodos(string EmployeeID)
        {
            Int64 _nEmplCountTodos = 0;
            _nEmplCountTodos = dimTodos.EmplCountTodos(EmployeeID);
            return _nEmplCountTodos;
        }

        public Int64 EmplCountOpenTodos(string EmployeeID)
        {
            Int64 _nEmplCountOpenTodos = 0;
            _nEmplCountOpenTodos = dimTodos.EmplCountOpenTodos(EmployeeID);
            return _nEmplCountOpenTodos;
        }

        public Int64 EmplCountClosedTodos(string EmployeeID)
        {
            Int64 _nEmplCountClosedTodos = 0;
            _nEmplCountClosedTodos = dimTodos.EmplCountClosedTodos(EmployeeID);
            return _nEmplCountClosedTodos;
        }

        public Int64 EmplCountOverdueTodos(string EmployeeID)
        {
            Int64 _nEmplCountOverdueTodos = 0;
            _nEmplCountOverdueTodos = dimTodos.EmplCountOverdueTodos(EmployeeID);
            return _nEmplCountOverdueTodos;
        }
        #endregion


        #region EmployerSpecific

        public Int64 EplrCountEmployees(string EmployerID)// Need to change to string and cascade to table level
        {
            Int64 _nEplrCountEmployees = 0;
            _nEplrCountEmployees = dimEmployee.EplrCountEmployees(EmployerID);
            return _nEplrCountEmployees;
        }

        public Int64 EplrCountCases(string EmployerID)
        {
            Int64 _nEplrCountCases = 0;
            _nEplrCountCases = dimCases.EplrCountCases(EmployerID);
            return _nEplrCountCases;
        }

        public Int64 EplrCountOpenCases(string EmployerID)
        {
            Int64 _nEplrCountOpenCases = 0;
            _nEplrCountOpenCases = dimCases.EplrCountOpenCases(EmployerID);
            return _nEplrCountOpenCases;
        }

        public Int64 EplrCountClosedCases(string EmployerID)
        {
            Int64 _nEplrCountClosedCases = 0;
            _nEplrCountClosedCases = dimCases.EplrCountClosedCases(EmployerID);
            return _nEplrCountClosedCases;
        }


        public Int64 EplrCountTodos(string EmployerID)
        {
            Int64 _nEplrCountTodos = 0;
            _nEplrCountTodos = dimTodos.EplrCountTodos(EmployerID);
            return _nEplrCountTodos;
        }

        public Int64 EplrCountOpenTodos(string EmployerID)
        {
            Int64 _nEplrCountOpenTodos = 0;
            _nEplrCountOpenTodos = dimTodos.EplrCountOpenTodos(EmployerID);
            return _nEplrCountOpenTodos;
        }

        public Int64 EplrCountClosedTodos(string EmployerID)
        {
            Int64 _nEplrCountClosedTodos = 0;
            _nEplrCountClosedTodos = dimTodos.EplrCountClosedTodos(EmployerID);
            return _nEplrCountClosedTodos;
        }

        public Int64 EplrCountOverdueTodos(string EmployerID)
        {
            Int64 _nEplrCountOverdueTodos = 0;
            _nEplrCountOverdueTodos = dimTodos.EplrCountOverdueTodos(EmployerID);
            return _nEplrCountOverdueTodos;
        }

        public Int64 EplrCountUsers(string EmployerID)// Need to change to string and cascade to table level
        {
            Int64 _nEplrCountUsers = 0;
            _nEplrCountUsers = dimEmployerAccess.EplrCountUsers(EmployerID);
            return _nEplrCountUsers;
        }

        #endregion


        #region CustomerSpecific

        public Int64 CustCountEmployers(string CustomerID)
        {
            Int64 _nCustCountEmployers = 0;
            _nCustCountEmployers = dimEmployer.CustCountEmployers(CustomerID);
            return _nCustCountEmployers;
        }

        public Int64 CustCountEmployees(string CustomerID)
        {
            Int64 _nCustCountEmployees = 0;
            _nCustCountEmployees = dimEmployee.CustCountEmployees(CustomerID);
            return _nCustCountEmployees;
        }

        public Int64 CustCountCases(string CustomerID)
        {
            Int64 _nCustCountCases = 0;
            _nCustCountCases = dimCases.CustCountCases(CustomerID);
            return _nCustCountCases;
        }

        public Int64 CustCountOpenCases(string CustomerID)
        {
            Int64 _nCustCountOpenCases = 0;
            _nCustCountOpenCases = dimCases.CustCountOpenCases(CustomerID);
            return _nCustCountOpenCases;
        }

        public Int64 CustCountClosedCases(string CustomerID)
        {
            Int64 _nCustCountClosedCases = 0;
            _nCustCountClosedCases = dimCases.CustCountClosedCases(CustomerID);
            return _nCustCountClosedCases;
        }


        public Int64 CustCountTodos(string CustomerID)
        {
            Int64 _nCustCountTodos = 0;
            //todo : Code here
            return _nCustCountTodos;
        }

        public Int64 CustCountOpenTodos(string CustomerID)
        {
            Int64 _nCustCountOpenTodos = 0;
            //todo : Code here
            return _nCustCountOpenTodos;
        }

        public Int64 CustCountClosedTodos(string CustomerID)
        {
            Int64 _nCustCountClosedTodos = 0;
            //todo : Code here
            return _nCustCountClosedTodos;
        }

        public Int64 CustCountOverdueTodos(string CustomerID)
        {
            Int64 _nCustCountOverdueTodos = 0;
            //todo : Code here
            return _nCustCountOverdueTodos;
        }

        public Int64 CustCountUsers(string CustomerID)
        {
            Int64 _nCustCountUsers = 0;
            //todo : Code here
            return _nCustCountUsers;
        }

        #endregion

    }
}
