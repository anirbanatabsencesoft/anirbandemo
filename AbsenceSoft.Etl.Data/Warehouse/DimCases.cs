﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCases : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCases"/> class.
        /// </summary>
        /// <param name="connectionString">The string connection string.</param>
        public DimCases(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCases"/> class.
        /// </summary>
        public DimCases() : base() { }

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASES; } }

        /// <summary>
        /// Gets or sets the dim case identifier.
        /// </summary>
        /// <value>
        /// The dim case identifier.
        /// </value>
        public long DimCaseId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>
        /// The case number.
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the case reporter identifier.
        /// </summary>
        /// <value>
        /// The case reporter identifier.
        /// </value>
        public string CaseReporterId { get; set; }

        /// <summary>
        /// Gets or sets the case reporter type code.
        /// </summary>
        /// <value>
        /// The case reporter type code.
        /// </value>
        public string CaseReporterTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the first name of the case reporter.
        /// </summary>
        /// <value>
        /// The name of the case reporter.
        /// </value>
        public string CaseReporterFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the case reporter.
        /// </summary>
        /// <value>
        /// The case reporter type code.
        /// </value>
        public string CaseReporterLastName { get; set; }

        /// <summary>
        /// Gets or sets the name of the case reporter.
        /// </summary>
        /// <value>
        /// The name of the case reporter.
        /// </value>
        public string CaseReporterName { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the spouse case.
        /// </summary>
        /// <value>
        /// The spouse case.
        /// </value>
        public string SpouseCase { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public int? Type { get; set; }

        /// <summary>
        /// Gets or sets the case reason.
        /// </summary>
        /// <value>
        /// The case reason.
        /// </value>
        public string CaseReason { get; set; }

        /// <summary>
        /// Gets or sets the cancel reason.
        /// </summary>
        /// <value>
        /// The cancel reason.
        /// </value>
        public int? CancelReason { get; set; }

        /// <summary>
        /// Gets or sets the name of the assigned to.
        /// </summary>
        /// <value>
        /// The name of the assigned to.
        /// </value>
        public string AssignedToName { get; set; }

        /// <summary>
        /// Gets or sets the assigned to identifier.
        /// </summary>
        /// <value>
        /// The assigned to identifier.
        /// </value>
        public string AssignedToId { get; set; }

        /// <summary>
        /// Gets or sets the case closure reason.
        /// </summary>
        /// <value>
        /// The case closure reason.
        /// </value>
        public int? CaseClosureReason { get; set; }

        /// <summary>
        /// Gets or sets the case closure reason other.
        /// </summary>
        /// <value>
        /// The case closure reason other.
        /// </value>
        public string CaseClosureReasonOther { get; set; }

        /// <summary>
        /// Gets or sets the case closure other reason details.
        /// </summary>
        /// <value>
        /// The case closure other reason details.
        /// </value>
        public string CaseClosureOtherReasonDetails { get; set; }

        /// <summary>
        /// Gets or sets the primary path text.
        /// </summary>
        /// <value>
        /// The primary path text.
        /// </value>
        public string PrimaryPathText { get; set; }

        /// <summary>
        /// Gets or sets the primary path minimum days.
        /// </summary>
        /// <value>
        /// The primary path minimum days.
        /// </value>
        public int? PrimaryPathMinDays { get; set; }

        /// <summary>
        /// Gets or sets the primary path maximum days.
        /// </summary>
        /// <value>
        /// The primary path maximum days.
        /// </value>
        public int? PrimaryPathMaxDays { get; set; }

        /// <summary>
        /// Gets or sets the hospitalization.
        /// </summary>
        /// <value>
        /// The hospitalization.
        /// </value>
        public bool? Hospitalization { get; set; }

        /// <summary>
        /// Gets or sets the Medical Complications.
        /// </summary>
        /// <value>
        /// The Medical Complications.
        /// </value>
        public bool? MedicalComplications { get; set; }

        /// <summary>
        /// Gets or sets the General Health Condition.
        /// </summary>
        /// <value>
        /// The General Health Condition.
        /// </value>
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Gets or sets the condition start date.
        /// </summary>
        /// <value>
        /// The condition start date.
        /// </value>
        public DateTime? ConditionStartDate { get; set; }

        /// <summary>
        /// Gets or sets the related case number.
        /// </summary>
        /// <value>
        /// The related case number.
        /// </value>
        public string RelatedCaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the related case.
        /// </summary>
        /// <value>
        /// The type of the related case.
        /// </value>
        public int? RelatedCaseType { get; set; }

        /// <summary>
        /// Gets or sets the job office location code.
        /// </summary>
        /// <value>
        /// The job office location code.
        /// </value>
        public string JobOfficeLocationCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the job office location.
        /// </summary>
        /// <value>
        /// The name of the job office location.
        /// </value>
        public string JobOfficeLocationName { get; set; }

        /// <summary>
        /// Gets or sets the occurances.
        /// </summary>
        /// <value>
        /// The occurances.
        /// </value>
        public int? Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the type of the frequency.
        /// </summary>
        /// <value>
        /// The type of the frequency.
        /// </value>
        public int? FrequencyType { get; set; }

        /// <summary>
        /// Gets or sets the type of the frequency unit.
        /// </summary>
        /// <value>
        /// The type of the frequency unit.
        /// </value>
        public int? FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public double? Duration { get; set; }

        /// <summary>
        /// Gets or sets the type of the duration.
        /// </summary>
        /// <value>
        /// The type of the duration.
        /// </value>
        public int? DurationType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is accommodation.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is accommodation; otherwise, <c>false</c>.
        /// </value>
        public bool IsAccommodation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is work related.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is work related; otherwise, <c>false</c>.
        /// </value>
        public bool? IsWorkRelated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [case is denied].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [case is denied]; otherwise, <c>false</c>.
        /// </value>
        public bool CaseIsDenied { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [case is approved].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [case is approved]; otherwise, <c>false</c>.
        /// </value>
        public bool CaseIsApproved { get; set; }

        /// <summary>
        /// Gets or sets the case is eligible.
        /// </summary>
        /// <value>
        /// The case is eligible.
        /// </value>
        public int? CaseIsEligible { get; set; }

        /// <summary>
        /// Gets or sets the case determination.
        /// </summary>
        /// <value>
        /// The case determination.
        /// </value>
        public int? CaseDetermination { get; set; }

        /// <summary>
        /// Gets or sets the case fmla is approved.
        /// </summary>
        /// <value>
        /// The case fmla is approved.
        /// </value>
        public bool? CaseFmlaIsApproved { get; set; }

        /// <summary>
        /// Gets or sets the case fmla is denied.
        /// </summary>
        /// <value>
        /// The case fmla is denied.
        /// </value>
        public bool? CaseFmlaIsDenied { get; set; }

        /// <summary>
        /// Gets or sets the case fmla projected usage.
        /// </summary>
        /// <value>
        /// The case fmla projected usage.
        /// </value>
        public string CaseFmlaProjectedUsage { get; set; }

        /// <summary>
        /// Gets or sets the case fmla exhaust date.
        /// </summary>
        /// <value>
        /// The case fmla exhaust date.
        /// </value>
        public DateTime? CaseFmlaExhaustDate { get; set; }

        /// <summary>
        /// Gets or sets the case maximum approved thru date.
        /// </summary>
        /// <value>
        /// The case maximum approved thru date.
        /// </value>
        public DateTime? CaseMaxApprovedThruDate { get; set; }

        /// <summary>
        /// Gets or sets the case accom status.
        /// </summary>
        /// <value>
        /// The case accom status.
        /// </value>
        public int? CaseAccomStatus { get; set; }

        /// <summary>
        /// Gets or sets the case accom start date.
        /// </summary>
        /// <value>
        /// The case accom start date.
        /// </value>
        public DateTime? CaseAccomStartDate { get; set; }

        /// <summary>
        /// Gets or sets the case accom end date.
        /// </summary>
        /// <value>
        /// The case accom end date.
        /// </value>
        public DateTime? CaseAccomEndDate { get; set; }

        /// <summary>
        /// Gets or sets the case accom minimum approved date.
        /// </summary>
        /// <value>
        /// The case accom minimum approved date.
        /// </value>
        public DateTime? CaseAccomMinApprovedDate { get; set; }

        /// <summary>
        /// Gets or sets the case accom maximum approved date.
        /// </summary>
        /// <value>
        /// The case accom maximum approved date.
        /// </value>
        public DateTime? CaseAccomMaxApprovedDate { get; set; }

        /// <summary>
        /// Gets or sets the case accom determination.
        /// </summary>
        /// <value>
        /// The case accom determination.
        /// </value>
        public int? CaseAccomDetermination { get; set; }

        /// <summary>
        /// Gets or sets the case accom cancel reason.
        /// </summary>
        /// <value>
        /// The case accom cancel reason.
        /// </value>
        public int? CaseAccomCancelReason { get; set; }

        /// <summary>
        /// Gets or sets the case accom cancel reason other.
        /// </summary>
        /// <value>
        /// The case accom cancel reason other.
        /// </value>
        public string CaseAccomCancelReasonOther { get; set; }

        /// <summary>
        /// Gets or sets the duration of the case accom.
        /// </summary>
        /// <value>
        /// The duration of the case accom.
        /// </value>
        public string CaseAccomDuration { get; set; }

        /// <summary>
        /// Gets or sets the type of the case accom duration.
        /// </summary>
        /// <value>
        /// The type of the case accom duration.
        /// </value>
        public string CaseAccomDurationType { get; set; }

        /// <summary>
        /// Gets or sets the case primary diagnosis code.
        /// </summary>
        /// <value>
        /// The case primary diagnosis code.
        /// </value>
        public string CasePrimaryDiagnosisCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the case primary diagnosis.
        /// </summary>
        /// <value>
        /// The name of the case primary diagnosis.
        /// </value>
        public string CasePrimaryDiagnosisName { get; set; }


        /// <summary>
        /// Gets or sets the case secondary diagnosis code.
        /// </summary>
        /// <value>
        /// The case secondary diagnosis code.
        /// </value>
        public string CaseSecondaryDiagnosisCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the case secondary diagnosis.
        /// </summary>
        /// <value>
        /// The name of the case secondary diagnosis.
        /// </value>
        public string CaseSecondaryDiagnosisName { get; set; }

        /// <summary>
        /// Gets or sets the employer case number.
        /// </summary>
        /// <value>
        /// The employer case number.
        /// </value>
        public string EmployerCaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the case minimum approved thru date.
        /// </summary>
        /// <value>
        /// The case minimum approved thru date.
        /// </value>
        public DateTime? CaseMinApprovedThruDate { get; set; }

        /// <summary>
        /// Gets or sets the case created by email.
        /// </summary>
        /// <value>
        /// The case created by email.
        /// </value>
        public string CaseCreatedByEmail { get; set; }

        /// <summary>
        /// Gets or sets the first name of the case created by.
        /// </summary>
        /// <value>
        /// The first name of the case created by.
        /// </value>
        public string CaseCreatedByFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the case created by.
        /// </summary>
        /// <value>
        /// The last name of the case created by.
        /// </value>
        public string CaseCreatedByLastName { get; set; }

        /// <summary>
        /// Gets or sets the custom fields.
        /// </summary>
        /// <value>
        /// The custom fields.
        /// </value>
        public Dictionary<string, object> CustomFields { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is intermittent.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is intermittent; otherwise, <c>false</c>.
        /// </value>
        public bool? IsIntermittent { get; set; }

        /// <summary>
        /// Gets or sets the name of the relationship type.
        /// </summary>
        /// <value>
        /// The name of the relationship type.
        /// </value>
        public string RelationshipTypeName { get; set; }

        /// <summary>
        /// Gets or sets the relationship type code.
        /// </summary>
        /// <value>
        /// The relationship type code.
        /// </value>
        public string RelationshipTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the return to work.
        /// </summary>
        /// <value>
        /// The return to work.
        /// </value>
        public DateTime? ReturnToWork { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the bonding start date.
        /// </summary>
        /// <value>
        /// The bonding start date.
        /// </value>
        public DateTime? BondingStartDate { get; set; }

        /// <summary>
        /// Gets or sets the bonding end date.
        /// </summary>
        /// <value>
        /// The bonding end date.
        /// </value>
        public DateTime? BondingEndDate { get; set; }

        /// <summary>
        /// Gets or sets the illness or injury date.
        /// </summary>
        /// <value>
        /// The illness or injury date.
        /// </value>
        public DateTime? IllnessOrInjuryDate { get; set; }

        /// <summary>
        /// Gets or sets the hospital admission date.
        /// </summary>
        /// <value>
        /// The hospital admission date.
        /// </value>
        public DateTime? HospitalAdmissionDate { get; set; }

        /// <summary>
        /// Gets or sets the hospital release date.
        /// </summary>
        /// <value>
        /// The hospital release date.
        /// </value>
        public DateTime? HospitalReleaseDate { get; set; }

        /// <summary>
        /// Gets or sets the release received.
        /// </summary>
        /// <value>
        /// The release received.
        /// </value>
        public DateTime? ReleaseReceived { get; set; }

        /// <summary>
        /// Gets or sets the paperwork received.
        /// </summary>
        /// <value>
        /// The paperwork received.
        /// </value>
        public DateTime? PaperworkReceived { get; set; }

        /// <summary>
        /// Gets or sets the accommodation added.
        /// </summary>
        /// <value>
        /// The accommodation added.
        /// </value>
        public DateTime? AccommodationAdded { get; set; }

        /// <summary>
        /// Gets or sets the case closed.
        /// </summary>
        /// <value>
        /// The case closed.
        /// </value>
        public DateTime? CaseClosed { get; set; }

        /// <summary>
        /// Gets or sets the case cancelled.
        /// </summary>
        /// <value>
        /// The case cancelled.
        /// </value>
        public DateTime? CaseCancelled { get; set; }

        /// <summary>
        /// Gets or sets the adoption date.
        /// </summary>
        /// <value>
        /// The adoption date.
        /// </value>
        public DateTime? AdoptionDate { get; set; }

        /// <summary>
        /// Gets or sets the estimated return to work.
        /// </summary>
        /// <value>
        /// The estimated return to work.
        /// </value>
        public DateTime? EstimatedReturnToWork { get; set; }

        /// <summary>
        /// Gets or sets the employer holiday schedule changed.
        /// </summary>
        /// <value>
        /// The employer holiday schedule changed.
        /// </value>
        public DateTime? EmployerHolidayScheduleChanged { get; set; }

        /// <summary>
        /// Gets or sets the case certifation start date.
        /// </summary>
        /// <value>
        /// The adoption date.
        /// </value>
        public DateTime? CaseCertStartDate { get; set; }

        /// <summary>
        /// Gets or sets the case certifation end date.
        /// </summary>
        /// <value>
        /// The adoption date.
        /// </value>
        public DateTime? CaseCertEndDate { get; set; }

        /// <summary>
        /// IntermittentType
        /// </summary>
        public int? IntermittentType { get; set; }

        #region Alternate Contact Info

        public string AltEmail { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltAddress1 { get; set; }
        public string AltAddress2 { get; set; }
        public string AltCity { get; set; }
        public string AltState { get; set; }
        public string AltPostalCode { get; set; }
        public string AltCountry { get; set; }

        #endregion

        #region Work Related
        /// <summary>
        /// Map to: wr_date_of_injury
        /// </summary>
        public DateTime? WorkRelatedDateOfInjury { get; set; }

        /// <summary>
        /// Map to: wr_is_pvt_healthcare
        /// </summary>
        public bool? WorkRelatedIsHealthCarePrivate { get; set; }

        /// <summary>
        /// Map to: wr_is_reportable 
        /// </summary>
        public bool? WorkRelatedIsReportable { get; set; }

        /// <summary>
        /// Map to: wr_place_of_occurance
        /// </summary>
        public string WorkRelatedPlaceOfOccurance { get; set; }

        /// <summary>
        /// Map to: wr_work_classification
        /// Lookup: dim_lookup_work_classification
        /// </summary>
        public long? WorkRelatedWorkClassification { get; set; }

        /// <summary>
        /// Map to: wr_type_of_injury 
        /// Lookup: dim_lookup_type_of_injury
        /// </summary>
        public long? WorkRelatedTypeOfInjury { get; set; }

        /// <summary>
        /// Map to: wr_days_away_from_work
        /// </summary>
        public int? WorkRelatedDaysAwayFromWork { get; set; }

        /// <summary>
        /// Map to: wr_restricted_days
        /// </summary>
        public int? WorkRelatedRestrictedDays { get; set; }

        /// <summary>
        /// Map to: wr_provider_first_name
        /// </summary>
        public string WorkRelatedProviderFirstName { get; set; }

        /// <summary>
        /// Map to: wr_provider_last_name 
        /// </summary>
        public string WorkRelatedProviderLastName { get; set; }

        /// <summary>
        /// Map to: wr_provider_company_name 
        /// </summary>
        public string WorkRelatedProviderCompanyName { get; set; }

        /// <summary>
        /// Map to: wr_provider_address1
        /// </summary>
        public string WorkRelatedProviderAddress1 { get; set; }

        /// <summary>   
        /// Map to: wr_provider_address2
        /// </summary>
        public string WorkRelatedProviderAddress2 { get; set; }

        /// <summary>
        /// Map to: wr_provider_city 
        /// </summary>
        public string WorkRelatedProviderCity { get; set; }

        /// <summary>
        /// Map to: wr_provider_state 
        /// </summary>
        public string WorkRelatedProviderState { get; set; }

        /// <summary>
        /// Map to: wr_provider_country 
        /// </summary>
        public string WorkRelatedProviderCountry { get; set; }

        /// <summary>
        /// Map to: wr_provider_postalcode 
        /// </summary>
        public string WorkRelatedProviderPostalCode { get; set; }



        /// <summary>
        /// Map to: wr_is_treated_in_emer_room  
        /// </summary>
        public bool? WorkRelatedIsTreatedInEmergencyRoom { get; set; }

        /// <summary>
        /// Map to: wr_is_hospitalized  
        /// </summary>
        public bool? WorkRelatedIsHospitalized { get; set; }

        /// <summary>
        /// Map to: wr_time_started_working  
        /// </summary>
        public string WorkRelatedEmployeeStartedWorking { get; set; }

        /// <summary>
        /// Map to: wr_time_of_injury  
        /// </summary>
        public string WorkRelatedTimeOfInjury { get; set; }

        /// <summary>
        /// Map to: wr_activity_before_injury  
        /// </summary>
        public string WorkRelatedActivityBeforeInjury { get; set; }

        /// <summary>
        /// Map to: wr_what_happened  
        /// </summary>
        public string WorkRelatedWhatHappened { get; set; }

        /// <summary>
        /// Map to: wr_injury_details  
        /// </summary>
        public string WorkRelatedInjuryDetails { get; set; }

        /// <summary>
        /// Map to: wr_what_harmed_empl  
        /// </summary>
        public string WorkRelatedWhatHarmedEmployee { get; set; }

        /// <summary>
        /// Map to: wr_date_of_death  
        /// </summary>
        public DateTime? WorkRelatedDateOfDeath { get; set; }

        /// <summary>
        /// Map to: wr_is_sharp
        /// </summary>
        public bool? WorkRelatedIsSharp { get; set; }

        /// <summary>
        /// Map to: wr_sharp_body_part  
        /// </summary>
        public string WorkRelatedSharpBodyPart { get; set; }

        /// <summary>
        /// Map to: wr_type_of_sharp  
        /// </summary>
        public string WorkRelatedTypeOfSharp { get; set; }

        /// <summary>
        /// Map to: wr_brand_of_sharp  
        /// </summary>
        public string WorkRelatedBrandOfSharp { get; set; }

        /// <summary>
        /// Map to: wr_model_of_sharp  
        /// </summary>
        public string WorkRelatedModelOfSharp { get; set; }

        /// <summary>
        /// Map to: wr_body_fluid  
        /// </summary>
        public string WorkRelatedBodyFluid { get; set; }

        /// <summary>
        /// Map to: wr_is_engn_injury_protection
        /// </summary>
        public bool? WorkRelatedIsEngineeredInjuryProtection { get; set; }

        /// <summary>
        /// Map to: wr_is_protective_mech_activated  
        /// </summary>
        public bool? WorkRelatedIsProtectiveMechanismActivated { get; set; }

        /// <summary>
        /// Map to: wr_when_sharp_incident_happened  
        /// Lookup: dim_lookup_sharp_incdnt_when
        /// </summary>
        public long? WorkRelatedWhenSharpIncidentHappened { get; set; }

        /// <summary>
        /// Map to: wr_sharp_job_classification  
        /// Lookup: dim_lookup_sharp_job_class
        /// </summary>
        public long? WorkRelatedSharpJobClassification { get; set; }

        /// <summary>
        /// Map to: wr_sharp_other_job_classification  
        /// </summary>
        public string WorkRelatedSharpJobClassificationOther { get; set; }

        /// <summary>
        /// Map to: wr_sharp_location_department  
        /// Lookup: dim_lookup_sharp_location_dept
        /// </summary>
        public long? WorkRelatedSharpLocationOrDepartment { get; set; }

        /// <summary>
        /// Map to: wr_sharp_other_location_dept  
        /// </summary>
        public string WorkRelatedSharpLocationOrDepartmentOther { get; set; }

        /// <summary>
        /// Map to: wr_sharp_procedure  
        /// Lookup: dim_lookup_sharp_procedure
        /// </summary>
        public long? WorkRelatedSharpProcedure { get; set; }

        /// <summary>
        /// Map to: wr_sharp_other_procedure  
        /// </summary>
        public string WorkRelatedSharpProcedureOther { get; set; }

        /// <summary>
        /// Map to: wr_sharp_exposure_detail  
        /// </summary>
        public string WorkRelatedSharpExposureDetail { get; set; }

        #endregion


        #region Comorbidity Guideline Data

        /// <summary>
        /// Map to: cg_risk_asmt_score 
        /// </summary>
        public decimal? CGRiskAssessmentScore { get; set; }

        /// <summary>
        /// Map to: cg_risk_asmt_status 
        /// </summary>
        public string CGRiskAssessmentStatus { get; set; }

        /// <summary>
        /// Map to: cg_mid_range_all_absence 
        /// </summary>
        public decimal? CGMidRangeAllAbsence { get; set; }

        /// <summary>
        /// Map to: cg_at_risk_all_absence 
        /// </summary>
        public decimal? CGAtRiskAllAbsence { get; set; }

        /// <summary>
        /// Map to: cg_best_practice_days 
        /// </summary>
        public decimal? CGBestPracticeDays { get; set; }

        /// <summary>
        /// Map to: cg_actual_days_lost 
        /// </summary>
        public decimal? CGActualDaysLost { get; set; }

        #endregion



        #region Identified Missing Attributes

        /// <summary>
        /// Map to: closure_reason
        /// Lookup to dim_lookup, type: case_closure_reason
        /// </summary>
        public long? ClosureReason { get; set; }

        /// <summary>
        /// Map to: closure_reason_detail
        /// </summary>
        public string ClosureReasonDetails { get; set; }

        /// <summary>
        /// Map to: description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Map to: narrative
        /// </summary>
        public string Narrative { get; set; }

        /// <summary>
        /// Map to: is_rpt_auth_submitter
        /// </summary>
        public bool? IsReportedByAuthorizedSubmitter { get; set; }

        /// <summary>
        /// Map to: send_e_communication
        /// </summary>
        public bool? SendECommunication { get; set; }

        /// <summary>
        /// Map to: e_communication_request_date
        /// </summary>
        public DateTime? ECommunicationRequestDate { get; set; }

        /// <summary>
        /// Map to: risk_profile_code
        /// </summary>
        public string RiskProfileCode { get; set; }

        /// <summary>
        /// Map to: risk_profile_name
        /// </summary>
        public string RiskProfileName { get; set; }

        /// <summary>
        /// Map to: current_office_location
        /// </summary>
        public string CurrentOfficeLocation { get; set; }

        /// <summary>
        /// Map to: total_paid
        /// </summary>
        public decimal? TotalPaid { get; set; }

        /// <summary>
        /// Map to: total_offset
        /// </summary>
        public decimal? TotalOffset { get; set; }

        /// <summary>
        /// Map to: apply_offsets_by_default
        /// </summary>
        public bool? ApplyOffsetsByDefault { get; set; }

        /// <summary>
        /// Map to: waive_waiting_period
        /// </summary>
        public bool? WaiveWaitingPeriod { get; set; }

        /// <summary>
        /// Map to: pay_schedule_id
        /// </summary>
        public string PayScheduleId { get; set; }
        #endregion

        /// <summary>
        /// Map to: actual_duration
        /// </summary>
        public int? ActualDuration { get; set; }

        /// <summary>
        /// Map to: exceeded_duration
        /// </summary>
        public int? ExceededDuration { get; set; }

        /// <summary>
        /// map to:has_relapse
        /// </summary>
        public bool HasRelapse { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_cases");
            sqlCmd.Append("(ext_ref_id, employee_id, case_reporter_id, case_number, customer_id, employer_id, status, start_date, end_date");
            sqlCmd.Append(", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date");
            sqlCmd.Append(",  spouse_case, type, case_reason, cancel_reason, assigned_to_name, case_closure_reason, case_closure_reason_other, case_closure_other_reason_details , primary_path_text, primary_path_min_days, primary_path_max_days");
            sqlCmd.Append(", hospitalization, medical_complications, general_health_condition, condition_start_date, related_case_number, related_case_type, job_office_location_code");
            sqlCmd.Append(", job_office_location_name, occurances, frequency, frequency_type, frequency_unit_type, duration, duration_type, intermittent_type, is_accommodation");
            sqlCmd.Append(", is_work_related, case_is_denied, case_is_approved, case_is_eligible, case_determination, case_fmla_is_approved, case_fmla_is_denied");
            sqlCmd.Append(", case_fmla_projected_usage, case_fmla_exhaust_date, case_max_approved_thru_date, case_accom_status, case_accom_start_date");
            sqlCmd.Append(", case_accom_end_date, case_accom_min_approved_date, case_accom_max_approved_date, case_accom_determination, case_accom_cancel_reason");
            sqlCmd.Append(", case_accom_cancel_reason_other, case_accom_duration, case_accom_duration_type, ext_created_by, case_primary_diagnosis_code, case_primary_daignosis_name");
            sqlCmd.Append(", case_secondary_diagnosis_code, case_secondary_daignosis_name, employer_case_number, case_min_approved_thru_date, case_created_by_email");
            sqlCmd.Append(", case_created_by_first_name, case_created_by_last_name, custom_fields, assigned_to_id, case_reporter_type_code, case_reporter_type_name, case_reporter_first_name, case_reporter_last_name");
            sqlCmd.Append(", relationship_type_code, relationship_type_name, is_intermittent, is_deleted, return_to_work, delivery_date, bonding_start_date");
            sqlCmd.Append(", bonding_end_date, illness_or_injury_date, hospital_admission_date, hospital_release_date, release_received, paperwork_received");
            sqlCmd.Append(", accommodation_added, case_closed, case_cancelled, adoption_date, estimated_return_to_work, employer_holiday_schedule_changed, case_cert_start_date, case_cert_end_date");

            //work related data
            sqlCmd.Append(" ,wr_date_of_injury ,wr_is_pvt_healthcare ,wr_is_reportable, wr_place_of_occurance, wr_work_classification ,wr_type_of_injury ,wr_days_away_from_work ,wr_restricted_days");
            sqlCmd.Append(" ,wr_provider_first_name ,wr_provider_last_name ,wr_provider_company_name ,wr_provider_address1, wr_provider_address2 ,wr_provider_city ,wr_provider_state ,wr_provider_country ,wr_provider_postalcode");
            sqlCmd.Append(" ,wr_is_treated_in_emer_room ,wr_is_hospitalized ,wr_time_started_working ,wr_time_of_injury ,wr_activity_before_injury ,wr_what_happened ,wr_injury_details ,wr_what_harmed_empl ,wr_date_of_death ,wr_is_sharp");
            sqlCmd.Append(" ,wr_sharp_body_part  ,wr_type_of_sharp  ,wr_brand_of_sharp ,wr_model_of_sharp ,wr_body_fluid ,wr_is_engn_injury_protection ,wr_is_protective_mech_activated ,wr_when_sharp_incident_happened ,wr_sharp_job_classification ");
            sqlCmd.Append(" ,wr_sharp_other_job_classification ,wr_sharp_location_department ,wr_sharp_other_location_dept ,wr_sharp_procedure ,wr_sharp_other_procedure ,wr_sharp_exposure_detail ");

            //alternate contact info
            sqlCmd.Append(" ,alt_phone_number ,alt_email ,alt_address1 ,alt_address2 ,alt_city ,alt_state ,alt_postal_code ,alt_country");

            //Comorbidity guidelines data
            sqlCmd.Append("	,cg_risk_asmt_score ,cg_risk_asmt_status ,cg_mid_range_all_absence ,cg_at_risk_all_absence ,cg_best_practice_days ,cg_actual_days_lost");

            //missing attributes
            sqlCmd.Append(" ,closure_reason ,closure_reason_detail, description ,narrative ,is_rpt_auth_submitter ,send_e_communication ,e_communication_request_date");
            sqlCmd.Append(" ,risk_profile_code ,risk_profile_name ,current_office_location ,total_paid ,total_offset ,apply_offsets_by_default ,waive_waiting_period ,pay_schedule_id");
            sqlCmd.Append(", ext_modified_by");
            // New attributes
            sqlCmd.Append(", actual_duration, exceeded_duration");
            sqlCmd.Append(", has_relapse");

            sqlCmd.Append(") values ");
            sqlCmd.Append("(@ext_ref_id, @employee_id, @case_reporter_id, @case_number, @customer_id, @employer_id, @status, @start_date, @end_date,");
            sqlCmd.Append(" @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date,");
            sqlCmd.Append(" @spouse_case, @type, @case_reason, @cancel_reason, @assigned_to_name, @case_closure_reason, @case_closure_reason_other, @case_closure_other_reason_details, @primary_path_text, @primary_path_min_days, @primary_path_max_days,");
            sqlCmd.Append(" @hospitalization, @medical_complications, @general_health_condition, @condition_start_date, @related_case_number, @related_case_type, @job_office_location_code,");
            sqlCmd.Append(" @job_office_location_name, @occurances, @frequency, @frequency_type, @frequency_unit_type, @duration, @duration_type, @intermittent_type, @is_accommodation,");
            sqlCmd.Append(" @is_work_related, @case_is_denied, @case_is_approved, @case_is_eligible, @case_determination, @case_fmla_is_approved, @case_fmla_is_denied,");
            sqlCmd.Append(" @case_fmla_projected_usage, @case_fmla_exhaust_date, @case_max_approved_thru_date, @case_accom_status, @case_accom_start_date,");
            sqlCmd.Append(" @case_accom_end_date, @case_accom_min_approved_date, @case_accom_max_approved_date, @case_accom_determination, @case_accom_cancel_reason,");
            sqlCmd.Append(" @case_accom_cancel_reason_other, @case_accom_duration, @case_accom_duration_type, @ext_created_by, @case_primary_diagnosis_code, @case_primary_daignosis_name,");
            sqlCmd.Append(" @case_secondary_diagnosis_code, @case_secondary_daignosis_name, @employer_case_number, @case_min_approved_thru_date, @case_created_by_email,");
            sqlCmd.Append(" @case_created_by_first_name, @case_created_by_last_name, @custom_fields, @assigned_to_id, @case_reporter_type_code, @case_reporter_type_name, @case_reporter_first_name, @case_reporter_last_name,");
            sqlCmd.Append(" @relationship_type_code, @relationship_type_name, @is_intermittent, @is_deleted, @return_to_work, @delivery_date, @bonding_start_date,");
            sqlCmd.Append(" @bonding_end_date, @illness_or_injury_date, @hospital_admission_date, @hospital_release_date, @release_received, @paperwork_received,");
            sqlCmd.Append(" @accommodation_added, @case_closed, @case_cancelled, @adoption_date, @estimated_return_to_work, @employer_holiday_schedule_changed, @case_cert_start_date, @case_cert_end_date");

            //work related data
            sqlCmd.Append(" ,@wr_date_of_injury ,@wr_is_pvt_healthcare ,@wr_is_reportable, @wr_place_of_occurance, @wr_work_classification ,@wr_type_of_injury ,@wr_days_away_from_work ,@wr_restricted_days");
            sqlCmd.Append(" ,@wr_provider_first_name ,@wr_provider_last_name ,@wr_provider_company_name ,@wr_provider_address1, @wr_provider_address2 ,@wr_provider_city ,@wr_provider_state ,@wr_provider_country ,@wr_provider_postalcode");
            sqlCmd.Append(" ,@wr_is_treated_in_emer_room ,@wr_is_hospitalized ,@wr_time_started_working ,@wr_time_of_injury ,@wr_activity_before_injury ,@wr_what_happened ,@wr_injury_details ,@wr_what_harmed_empl ,@wr_date_of_death ,@wr_is_sharp");
            sqlCmd.Append(" ,@wr_sharp_body_part  ,@wr_type_of_sharp  ,@wr_brand_of_sharp ,@wr_model_of_sharp ,@wr_body_fluid ,@wr_is_engn_injury_protection ,@wr_is_protective_mech_activated ,@wr_when_sharp_incident_happened ,@wr_sharp_job_classification ");
            sqlCmd.Append(" ,@wr_sharp_other_job_classification ,@wr_sharp_location_department ,@wr_sharp_other_location_dept ,@wr_sharp_procedure ,@wr_sharp_other_procedure ,@wr_sharp_exposure_detail ");

            //Alternate Contact Info
            sqlCmd.Append(" ,@alt_phone_number ,@alt_email ,@alt_address1 ,@alt_address2 ,@alt_city ,@alt_state ,@alt_postal_code ,@alt_country");

            //Comorbidity guidelines data
            sqlCmd.Append("	,@cg_risk_asmt_score ,@cg_risk_asmt_status ,@cg_mid_range_all_absence ,@cg_at_risk_all_absence ,@cg_best_practice_days ,@cg_actual_days_lost");

            //missing attributes
            sqlCmd.Append(" ,@closure_reason ,@closure_reason_detail, @description ,@narrative ,@is_rpt_auth_submitter ,@send_e_communication ,@e_communication_request_date");
            sqlCmd.Append(" ,@risk_profile_code ,@risk_profile_name ,@current_office_location ,@total_paid ,@total_offset ,@apply_offsets_by_default ,@waive_waiting_period ,@pay_schedule_id");

            sqlCmd.Append(", @ext_modified_by");

            sqlCmd.Append(", @actual_duration, @exceeded_duration");

            sqlCmd.Append(", @has_relapse");

            sqlCmd.Append(") RETURNING dim_case_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reporter_id", CaseReporterId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_number", CaseNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@spouse_case", SpouseCase.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type", Type.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reason", CaseReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cancel_reason", CancelReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_name", AssignedToName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_closure_reason", CaseClosureReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_closure_reason_other", CaseClosureReasonOther.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_closure_other_reason_details", CaseClosureOtherReasonDetails.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@primary_path_text", PrimaryPathText.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@primary_path_min_days", PrimaryPathMinDays.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@primary_path_max_days", PrimaryPathMaxDays.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hospitalization", Hospitalization.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@medical_complications", MedicalComplications.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@general_health_condition", GeneralHealthCondition.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@condition_start_date", ConditionStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@related_case_number", RelatedCaseNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@related_case_type", RelatedCaseType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_office_location_code", JobOfficeLocationCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_office_location_name", JobOfficeLocationName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@occurances", Occurances.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@frequency", Frequency.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@frequency_type", FrequencyType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@frequency_unit_type", FrequencyUnitType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@duration", Duration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@duration_type", DurationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@intermittent_type", IntermittentType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_accommodation", IsAccommodation));
                cmd.Parameters.Add(new NpgsqlParameter("@is_work_related", IsWorkRelated.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_is_denied", CaseIsDenied));
                cmd.Parameters.Add(new NpgsqlParameter("@case_is_approved", CaseIsApproved));
                cmd.Parameters.Add(new NpgsqlParameter("@case_is_eligible", CaseIsEligible.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_determination", CaseDetermination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_fmla_is_approved", CaseFmlaIsApproved.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_fmla_is_denied", CaseFmlaIsDenied.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_fmla_projected_usage", CaseFmlaProjectedUsage.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_fmla_exhaust_date", CaseFmlaExhaustDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_max_approved_thru_date", CaseMaxApprovedThruDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_status", CaseAccomStatus.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_start_date", CaseAccomStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_end_date", CaseAccomEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_min_approved_date", CaseAccomMinApprovedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_max_approved_date", CaseAccomMaxApprovedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_determination", CaseAccomDetermination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_cancel_reason", CaseAccomCancelReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_cancel_reason_other", CaseAccomCancelReasonOther.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_duration", CaseAccomDuration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_accom_duration_type", CaseAccomDurationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_primary_diagnosis_code", CasePrimaryDiagnosisCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_primary_daignosis_name", CasePrimaryDiagnosisName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_secondary_diagnosis_code", CaseSecondaryDiagnosisCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_secondary_daignosis_name", CaseSecondaryDiagnosisName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_case_number", EmployerCaseNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_min_approved_thru_date", CaseMinApprovedThruDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_created_by_email", CaseCreatedByEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_created_by_first_name", CaseCreatedByFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_created_by_last_name", CaseCreatedByLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@custom_fields", NpgsqlTypes.NpgsqlDbType.Jsonb) { Value = CustomFields.JsonOrDbNull() });
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_to_id", AssignedToId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reporter_type_code", CaseReporterTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reporter_type_name", CaseReporterName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reporter_first_name", CaseReporterFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_reporter_last_name", CaseReporterLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@relationship_type_code", RelationshipTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@relationship_type_name", RelationshipTypeName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_intermittent", IsIntermittent.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@return_to_work", ReturnToWork.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@delivery_date", DeliveryDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@bonding_start_date", BondingStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@bonding_end_date", BondingEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@illness_or_injury_date", IllnessOrInjuryDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hospital_admission_date", HospitalAdmissionDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hospital_release_date", HospitalReleaseDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@release_received", ReleaseReceived.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_received", PaperworkReceived.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_added", AccommodationAdded.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_closed", CaseClosed.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_cancelled", CaseCancelled.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@adoption_date", AdoptionDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@estimated_return_to_work", EstimatedReturnToWork.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_holiday_schedule_changed", EmployerHolidayScheduleChanged.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_cert_start_date", CaseCertStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_cert_end_date", CaseCertEndDate.ValueOrDbNull()));


                //Work related parameters
                cmd.Parameters.Add(new NpgsqlParameter("@wr_date_of_injury", WorkRelatedDateOfInjury.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_pvt_healthcare", WorkRelatedIsHealthCarePrivate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_reportable", WorkRelatedIsReportable.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_place_of_occurance", WorkRelatedPlaceOfOccurance.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_work_classification", WorkRelatedWorkClassification.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_type_of_injury", WorkRelatedTypeOfInjury.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_days_away_from_work", WorkRelatedDaysAwayFromWork.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_restricted_days", WorkRelatedRestrictedDays.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_first_name", WorkRelatedProviderFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_last_name", WorkRelatedProviderLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_company_name", WorkRelatedProviderCompanyName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_address1", WorkRelatedProviderAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_address2", WorkRelatedProviderAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_city", WorkRelatedProviderCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_state", WorkRelatedProviderState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_country", WorkRelatedProviderCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_provider_postalcode", WorkRelatedProviderPostalCode.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_treated_in_emer_room", WorkRelatedIsTreatedInEmergencyRoom.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_hospitalized", WorkRelatedIsHospitalized.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_time_started_working", WorkRelatedEmployeeStartedWorking.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_time_of_injury", WorkRelatedTimeOfInjury.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_activity_before_injury", WorkRelatedActivityBeforeInjury.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_what_happened", WorkRelatedWhatHappened.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_injury_details", WorkRelatedInjuryDetails.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_what_harmed_empl", WorkRelatedWhatHarmedEmployee.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_date_of_death", WorkRelatedDateOfDeath.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_sharp", WorkRelatedIsSharp.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_body_part", WorkRelatedSharpBodyPart.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_type_of_sharp", WorkRelatedTypeOfSharp.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_brand_of_sharp", WorkRelatedBrandOfSharp.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_model_of_sharp", WorkRelatedModelOfSharp.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_body_fluid", WorkRelatedBodyFluid.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_engn_injury_protection", WorkRelatedIsEngineeredInjuryProtection.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_is_protective_mech_activated", WorkRelatedIsProtectiveMechanismActivated.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_when_sharp_incident_happened", WorkRelatedWhenSharpIncidentHappened.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_job_classification", WorkRelatedSharpJobClassification.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_other_job_classification", WorkRelatedSharpJobClassificationOther.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_location_department", WorkRelatedSharpLocationOrDepartment.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_other_location_dept", WorkRelatedSharpLocationOrDepartmentOther.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_procedure", WorkRelatedSharpProcedure.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_other_procedure", WorkRelatedSharpProcedureOther.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wr_sharp_exposure_detail", WorkRelatedSharpExposureDetail.ValueOrDbNull()));

                //Alternate Contact Info
                cmd.Parameters.Add(new NpgsqlParameter("@alt_phone_number", AltPhoneNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_email", AltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_address1", AltAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_address2", AltAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_city", AltCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_state", AltState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_postal_code", AltPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@alt_country", AltCountry.ValueOrDbNull()));


                cmd.Parameters.Add(new NpgsqlParameter("@cg_risk_asmt_score", CGRiskAssessmentScore.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cg_risk_asmt_status", CGRiskAssessmentStatus.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cg_mid_range_all_absence", CGMidRangeAllAbsence.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cg_at_risk_all_absence", CGAtRiskAllAbsence.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cg_best_practice_days", CGBestPracticeDays.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cg_actual_days_lost", CGActualDaysLost.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@closure_reason", ClosureReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@closure_reason_detail", ClosureReasonDetails.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@narrative", Narrative.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_rpt_auth_submitter", IsReportedByAuthorizedSubmitter.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@send_e_communication", SendECommunication.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@e_communication_request_date", ECommunicationRequestDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_code", RiskProfileCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@risk_profile_name", RiskProfileName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@current_office_location", CurrentOfficeLocation.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@total_paid", TotalPaid.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@total_offset", TotalOffset.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@apply_offsets_by_default", ApplyOffsetsByDefault.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@waive_waiting_period", WaiveWaitingPeriod.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_schedule_id", PayScheduleId.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@actual_duration", ActualDuration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@exceeded_duration", ExceededDuration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@has_relapse", HasRelapse.ValueOrDbNull()));


                object res = cmd.ExecuteScalar();
                DimCaseId = Convert.ToInt64(res);
            }
        }
    }
}

