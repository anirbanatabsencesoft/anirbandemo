﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimTeam : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeam"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimTeam(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeam"/> class.
        /// </summary>
        public DimTeam() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_TEAM; } }

        /// <summary>
        /// Gets or sets the dim team identifier.
        /// </summary>
        /// <value>
        /// The dim team identifier.
        /// </value>
        public long DimTeamId { get; set; }

        /// <summary>
        /// Gets or sets the customer Id.
        /// </summary>
        /// <value>
        /// The customer Id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the team.
        /// </summary>
        /// <value>
        /// The name of the team.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the team description.
        /// </summary>
        /// <value>
        /// The team description.
        /// </value>
        public string Description { get; set; }
        
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd += "insert into dim_team";
            sqlCmd += "(ext_ref_id, customer_id, name, description";
            sqlCmd += ", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @customer_id, @name, @description,";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_team_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimTeamId = Convert.ToInt64(res);
            }
        }
    }
}
