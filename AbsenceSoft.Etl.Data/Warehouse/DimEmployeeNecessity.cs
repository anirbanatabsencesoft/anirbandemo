﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Dim Employee Necessity
    /// </summary>
    public class DimEmployeeNecessity : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeNecessity"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeNecessity(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeNecessity"/> class.
        /// </summary>
        public DimEmployeeNecessity() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_NECESSITY; } }

        /// <summary>
        /// Gets or sets the dim employee necessity id.
        /// </summary>
        /// <value>
        /// The dim employee necessity identifier.
        /// </value>
        public long DimEmployeeNecessityId { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        /// <value>
        /// The Employee Id.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets Id of the Employer.
        /// </summary>
        /// <value>
        /// The Employer Id.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        /// <value>
        /// The customer Id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the necessity.
        /// </summary>
        /// <value>
        /// The Necessity Name
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the necessity type
        /// </summary>
        /// <value>
        /// The necessity type
        /// </value>
        public int? Type { get; set; }

        /// <summary>
        /// Gets or sets description of the necessity
        /// </summary>
        /// <value>
        /// The description of the necessity
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Case Id.
        /// </summary>
        /// <value>
        /// The Case Id
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the Case Number
        /// </summary>
        /// <value>
        /// The Case Number
        /// </value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets Start Date
        /// </summary>
        /// <value>
        /// The Start Date
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the End Date
        /// </summary>
        /// <value>
        /// The End Date
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        /// <summary>
        /// Save the instance
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Sql Insert the dimension
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_employee_necessity ");
            sqlCmd.Append("(employee_id, employer_id, customer_id, name, type, description, start_date, end_date, case_id, case_number, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, is_deleted");
            sqlCmd.Append(") values (");
            sqlCmd.Append(" @employee_id, @employer_id, @customer_id, @name, @type, @description, @start_date, @end_date, @case_id, @case_number, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @is_deleted");
            sqlCmd.Append(") RETURNING dim_employee_necessity_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type", Type.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_number", CaseNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));                

                object res = cmd.ExecuteScalar();
                DimEmployeeNecessityId = Convert.ToInt64(res);
            }
        }
    }
}
