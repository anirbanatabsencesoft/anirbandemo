﻿namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimVer : DimBase
    {
        private int _dimensionId;

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return _dimensionId; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimVer" /> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimVer(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Sets the dimension identifier.
        /// </summary>
        /// <param name="dimensionId">The dimension identifier.</param>
        /// <returns></returns>
        public DimVer SetDimensionId(int dimensionId)
        {
            _dimensionId = dimensionId;
            return this;
        }
    }
}
