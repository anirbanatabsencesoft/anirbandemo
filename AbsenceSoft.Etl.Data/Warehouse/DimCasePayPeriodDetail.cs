﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCasePayPeriodDetail: DimBase
    {

        public DimCasePayPeriodDetail(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCasePayPeriodDetail"/> class.
        /// </summary>
        public DimCasePayPeriodDetail() : base() { }

        public override int DimensionId { get { return Const.Dimensions.NONE; } }

        public long DimCasePayPeriodDetailId { get; set; }

        public long DimCasePayPeriodId { get; set; }

        public string CaseId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsOffset { get; set; }

        public decimal? MaxPaymentAmount { get; set; }

        public decimal PayAmountOverrideValue { get; set; }

        public decimal PayAmountSystemValue { get; set; }

        public decimal PaymentTierPercentage { get; set; }

        public string PayPercentageOverrideBy { get; set; }

        public DateTime? PayPercentageOverrideDate { get; set; }

        public decimal PayPercentageOverrideValue { get; set; }

        public decimal PayPercentageSystemValue { get; set; }

        public string PolicyCode { get; set; }

        public string PolicyName { get; set; }

        public decimal SalaryTotal { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(SqlDelete);
        }
        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            SqlDelete(pgsqlConnection);
        }

        /// <summary>
        /// Inserts this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sql = @"insert into dim_case_pay_period_detail
                        (dim_case_pay_period_id  , ext_ref_id  , case_id  , start_date  , end_date  , is_Offset , 
                        max_payment_amount, pay_amount_override_value  , pay_amount_system_value  , payment_tier_percentage  , 
                        pay_percentage_override_by  , pay_percentage_override_date  , pay_percentage_override_value, pay_percentage_system_value, policy_code , 
                        policy_name  , salary_total, created_date)
                        values
                        (@dim_case_pay_period_id  , @ext_ref_id  , @case_id  , @start_date  , @end_date  , @is_Offset , 
                        @max_payment_amount, @pay_amount_override_value  , @pay_amount_system_value  , @payment_tier_percentage  , 
                        @pay_percentage_override_by  , @pay_percentage_override_date  , @pay_percentage_override_value, @pay_percentage_system_value, @policy_code ,
                        @policy_name  , @salary_total, @created_date)";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_pay_period_id", DimCasePayPeriodId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_Offset", IsOffset.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@max_payment_amount", MaxPaymentAmount.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_amount_override_value", PayAmountOverrideValue.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_amount_system_value", PayAmountSystemValue.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@payment_tier_percentage", PaymentTierPercentage.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@payment_tier_percentage", PaymentTierPercentage.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_percentage_override_by", PayPercentageOverrideBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_percentage_override_date", PayPercentageOverrideDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_percentage_override_value", PayPercentageOverrideValue.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@pay_percentage_system_value", PayPercentageSystemValue.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_code", PolicyCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_name", PolicyName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@salary_total", SalaryTotal.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_date", ExternalCreatedDate.ValueOrDbNull()));
                
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void SqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_case_pay_period_detail where dim_case_pay_period_id = @dim_case_pay_period_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_pay_period_id", DimCasePayPeriodId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
