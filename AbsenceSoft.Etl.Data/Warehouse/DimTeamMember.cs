﻿using System;
using System.Text;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimTeamMember : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeamMember"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimTeamMember(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimTeamMember"/> class.
        /// </summary>
        public DimTeamMember() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_TEAM_MEMBER; } }

        /// <summary>
        /// Gets or sets the dim team member identifier.
        /// </summary>
        /// <value>
        /// The dim team member identifier.
        /// </value>
        public long DimTeamMemberId { get; set; }

        /// <summary>
        /// Gets or sets the customer Id.
        /// </summary>
        /// <value>
        /// The customer Id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the user Id.
        /// </summary>
        /// <value>
        /// The user Id.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the team Id.
        /// </summary>
        /// <value>
        /// The team Id.
        /// </value>
        public string TeamId { get; set; }

        /// <summary>
        /// Gets or sets the IsTeamLead.
        /// </summary>
        /// <value>
        /// The IsTeamLead.
        /// </value>
        public bool IsTeamLead { get; set; }

        /// <summary>
        /// Gets or sets the IsDeleted.
        /// </summary>
        /// <value>
        /// The IsDeleted.
        /// </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the Assigned By Id.
        /// </summary>
        /// <value>
        /// The Assgined By Id.
        /// </value>
        public string AssignedById { get; set; }

        /// <summary>
        /// Gets or sets the Assigned On.
        /// </summary>
        /// <value>
        /// The Assgined On.
        /// </value>
        public DateTime AssignedOn { get; set; }

        /// <summary>
        /// Gets or sets the Modified By Id.
        /// </summary>
        /// <value>
        /// The Modified By Id.
        /// </value>
        public string ModifiedById { get; set; }

        /// <summary>
        /// Gets or sets the Modified On.
        /// </summary>
        /// <value>
        /// The Modified On.
        /// </value>
        public DateTime ModifiedOn { get; set; }
        
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("insert into dim_team_member");
            sqlCmd.Append("(ext_ref_id, ext_created_date, ext_modified_date");
            sqlCmd.Append(", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");
            sqlCmd.Append(", ext_created_by, ext_modified_by, customer_id, user_id, team_id, is_team_lead, assigned_by_id, assigned_on, modified_by_id, modified_on, is_deleted");
            sqlCmd.Append(") values ");
            sqlCmd.Append("(@ext_ref_id, @ext_created_date, @ext_modified_date");
            sqlCmd.Append(", @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count");
            sqlCmd.Append(", @ext_created_by, @ext_modified_by, @customer_id, @user_id, @team_id, @is_team_lead, @assigned_by_id, @assigned_on, @modified_by_id, @modified_on, @is_deleted");
            sqlCmd.Append(") RETURNING dim_team_member_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@user_id", UserId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@team_id", TeamId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_team_lead", IsTeamLead.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_by_id", AssignedById.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@assigned_on", AssignedOn.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@modified_by_id", ModifiedById.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@modified_on", ModifiedOn.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimTeamMemberId = Convert.ToInt64(res);
            }
        }
    }
}
