﻿using System;
using System.Text;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEvent : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEvent"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEvent(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEvent"/> class.
        /// </summary>
        public DimEvent() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EVENT; } }

        /// <summary>
        /// Gets or sets the dim event identifier.
        /// </summary>
        /// <value>
        /// The dim event identifier.
        /// </value>
        public long DimEventId { get; set; }

        /// <summary>
        /// Gets or sets the customer Id.
        /// </summary>
        /// <value>
        /// The customer Id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer Id.
        /// </summary>
        /// <value>
        /// The employer Id.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the employee Id.
        /// </summary>
        /// <value>
        /// The employee Id.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the todo item Id.
        /// </summary>
        /// <value>
        /// The todo item Id.
        /// </value>
        public string ToDoItemId { get; set; }

        /// <summary>
        /// Gets or sets the accomodation Id.
        /// </summary>
        /// <value>
        /// The accomodation Id.
        /// </value>
        public string AccomodationId { get; set; }

        /// <summary>
        /// Gets or sets the active accomodation Id.
        /// </summary>
        /// <value>
        /// The active accomodation id.
        /// </value>
        public string ActiveAccomodationId { get; set; }

        /// <summary>
        /// Gets or sets the accomodation type code.
        /// </summary>
        /// <value>
        /// The accomodation type code.
        /// </value>
        public string AccomodationTypeCode { get; set; }

        /// Gets or sets the accomodation end date.
        /// </summary>
        /// <value>
        /// The accomodation end date.
        /// </value>
        public DateTime? AccomodationEndDate { get; set; }

        /// <summary>
        /// Gets or sets the determination.
        /// </summary>
        /// <value>
        /// The determination.
        /// </value>
        public int? Determination { get; set; }

        /// <summary>
        /// Gets or sets Denial Explanation.
        /// </summary>
        /// <value>
        /// The Denial explanation.
        /// </value>
        public string DenialExplanation { get; set; }

        /// <summary>
        /// Gets or sets the workflow activity id.
        /// </summary>
        /// <value>
        /// The workflow activity id.
        /// </value>
        public string WorkFlowActivityId { get; set; }

        /// <summary>
        /// Gets or sets CaseId.
        /// </summary>
        /// <value>
        /// The CaseId.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the Accomodation Type Id.
        /// </summary>
        /// <value>
        /// The accomodation type id.
        /// </value>
        public string AccomodationTypeId { get; set; }

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        /// <value>
        /// The template.
        /// </value>
        public string Template { get; set; }

        /// <summary>
        /// Gets or sets the Communication type.
        /// </summary>
        /// <value>
        /// The communication type.
        /// </value>
        public int? CommunicationType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Event type.
        /// </summary>
        /// <value>
        /// The event type.
        /// </value>
        public string EventType { get; set; }

        /// <summary>
        /// Gets or sets the Is deleted.
        /// </summary>
        /// <value>
        /// The is deleted.
        /// </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the applied policy id
        /// </summary>
        public string AppliedPolicyId { get; set; }

        /// <summary>
        /// Gets or sets the attachment id
        /// </summary>
        public string AttachmentId { get; set; }
        
        /// <summary>
        /// Gets or sets the close case
        /// </summary>
        public bool? CloseCase { get; set; }

        /// <summary>
        /// Gets or sets the communication id
        /// </summary>
        public string CommunicationId { get; set; }

        /// <summary>
        /// Gets or sets the condition start date
        /// </summary>
        public DateTime? ConditionStartDate { get; set; }

        /// <summary>
        /// Gets or sets contact employee due date
        /// </summary>
        public DateTime? ContactEmployeeDueDate { get; set; }

        /// <summary>
        /// Gets or sets the contact hcp due date
        /// </summary>
        public DateTime? ContactHcpDueDate { get; set; }

        /// <summary>
        /// Gets or sets the Denial Reason
        /// </summary>
        public int? DenialReason { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the due date
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Gets or sets the Ergonomic Assessment Date
        /// </summary>
        public DateTime? ErgonomicAssessmentDate { get; set; }
        
        /// <summary>
        /// Gets or sets the Extend grace period
        /// </summary>
        public bool? ExtendGracePeriod { get; set; }

        /// <summary>
        /// Gets or sets the First Exhaustion date
        /// </summary>
        public DateTime? FirstExhaustionDate { get; set; }

        /// <summary>
        /// Gets or sets the General Health Condition
        /// </summary>
        public string GeneralHealthCondition { get; set; }

        /// <summary>
        /// Gets or sets the Has Work Restriction
        /// </summary>
        public bool? HasWorkRestrictions { get; set; }

        /// <summary>
        /// Gets or sets the Illness or injury date
        /// </summary>
        public DateTime? IllnessOrInjuryDate { get; set; }

        /// <summary>
        /// Gets or sets the New Due Date
        /// </summary>
        public DateTime? NewDueDate { get; set; }

        /// <summary>
        /// Gets or sets the Paperwork attachment Id
        /// </summary>
        public string PaperworkAttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the paperwork due date
        /// </summary>
        public DateTime? PaperworkDueDate { get; set; }

        /// <summary>
        /// Gets or sets the paperwork id
        /// </summary>
        public string PaperworkId { get; set; }

        /// <summary>
        /// Gets or sets the paperwork name
        /// </summary>
        public string PaperworkName { get; set; }

        /// <summary>
        /// Gets or sets paperwork received
        /// </summary>
        public bool? PaperworkReceived { get; set; }

        /// <summary>
        /// Gets or sets policy code
        /// </summary>
        public string PolicyCode { get; set; }

        /// <summary>
        /// Gets or sets policy id
        /// </summary>
        public string PolicyId { get; set; }

        /// <summary>
        /// Gets or sets the reason
        /// </summary>
        public int? Reason { get; set; }

        /// <summary>
        /// Gets or sets the requires review
        /// </summary>
        public bool? RequiresReview { get; set; }

        /// <summary>
        /// Gets or sets the return attachment id
        /// </summary>
        public string ReturnAttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the Return to work date
        /// </summary>
        public DateTime? ReturnToWorkDate { get; set; }

        /// <summary>
        /// Gets or sets the RTW
        /// </summary>
        public bool? RTW { get; set; }

        /// <summary>
        /// Gets or sets the Source workflow activity activity id
        /// </summary>
        public string SourceWorkflowActivityActivityId { get; set; }

        /// <summary>
        /// SourceWorkflowActivityId
        /// </summary>
        public string SourceWorkflowActivityId { get; set; }

        /// <summary>
        /// SourceWorkflowInstanceId
        /// </summary>
        public string SourceWorkflowInstanceId { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("insert into dim_event");
            sqlCmd.Append("(ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id");
            sqlCmd.Append(", ver_prev_id, ver_history_count, ext_created_by, ext_modified_by, customer_id, employer_id, employee_id, todo_item_id, accomodation_id");
            sqlCmd.Append(", active_accomodation_id, accomodation_type_code, accomodation_end_date, determination, denial_explanation, case_id");
            sqlCmd.Append(", accomodation_type_id, template, communication_type, name, event_type, is_deleted, applied_policy_id, attachment_id, close_case, communication_id");
            sqlCmd.Append(", condition_start_date, contact_employee_due_date, contact_hcp_due_date, denial_reason, description, due_date, ergonomic_assessment_date");
            sqlCmd.Append(", extended_grace_period, first_exhaustion_date, general_health_condition, has_work_restriction, illness_injury_date, new_due_date");
            sqlCmd.Append(", paperwork_attachment_id, paperwork_due_date, paperwork_id, paperwork_name, paperwork_received, policy_code, policy_id, reason, requires_review");
            sqlCmd.Append(", return_attachment_id, return_to_work_date, rtw, source_workflow_activity_activityid, source_workflow_activityid, source_workflow_instanceid, workflow_activity_id");
            sqlCmd.Append(") values ");
            sqlCmd.Append("(@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id");
            sqlCmd.Append(", @ver_prev_id, @ver_history_count, @ext_created_by, @ext_modified_by, @customer_id, @employer_id, @employee_id, @todo_item_id, @accomodation_id");
            sqlCmd.Append(", @active_accomodation_id, @accomodation_type_code, @accomodation_end_date, @determination, @denial_explanation, @case_id");
            sqlCmd.Append(", @accomodation_type_id, @template, @communication_type, @name, @event_type, @is_deleted, @applied_policy_id, @attachment_id, @close_case, @communication_id");
            sqlCmd.Append(", @condition_start_date, @contact_employee_due_date, @contact_hcp_due_date, @denial_reason, @description, @due_date, @ergonomic_assessment_date");
            sqlCmd.Append(", @extended_grace_period, @first_exhaustion_date, @general_health_condition, @has_work_restriction, @illness_injury_date, @new_due_date");
            sqlCmd.Append(", @paperwork_attachment_id, @paperwork_due_date, @paperwork_id, @paperwork_name, @paperwork_received, @policy_code, @policy_id, @reason, @requires_review");
            sqlCmd.Append(", @return_attachment_id, @return_to_work_date, @rtw, @source_workflow_activity_activityid, @source_workflow_activityid, @source_workflow_instanceid, @workflow_activity_id");
            sqlCmd.Append(") RETURNING dim_event_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@todo_item_id", ToDoItemId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomodation_id", AccomodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@active_accomodation_id", ActiveAccomodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomodation_type_code", AccomodationTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomodation_end_date", AccomodationEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination", Determination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_explanation", DenialExplanation.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomodation_type_id", AccomodationTypeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@template", Template.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_type", CommunicationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@event_type", EventType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@applied_policy_id", AppliedPolicyId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@attachment_id", AttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@close_case", CloseCase.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_id", CommunicationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@condition_start_date", ConditionStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_employee_due_date", ContactEmployeeDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_hcp_due_date", ContactHcpDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason", DenialReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@due_date", DueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ergonomic_assessment_date", ErgonomicAssessmentDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@extended_grace_period", ExtendGracePeriod.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_exhaustion_date", FirstExhaustionDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@general_health_condition", GeneralHealthCondition.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@has_work_restriction", HasWorkRestrictions.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@illness_injury_date", IllnessOrInjuryDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@new_due_date", NewDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_attachment_id", PaperworkAttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_due_date", PaperworkDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_id", PaperworkId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_name", PaperworkName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_received", PaperworkReceived.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_code", PolicyCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_id", PolicyId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@reason", Reason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@requires_review", RequiresReview.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@return_attachment_id", ReturnAttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@return_to_work_date", ReturnToWorkDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@rtw", RTW.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_activity_activityid", SourceWorkflowActivityActivityId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_activityid", SourceWorkflowActivityId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_instanceid", SourceWorkflowInstanceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@workflow_activity_id", WorkFlowActivityId.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEventId = Convert.ToInt64(res);
            }
        }
    }
}
