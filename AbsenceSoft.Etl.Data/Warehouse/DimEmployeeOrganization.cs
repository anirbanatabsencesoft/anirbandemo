﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployeeOrganization : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeOrganization"/> class.
        /// </summary>
        public DimEmployeeOrganization() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeOrganization" /> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeOrganization(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_ORGANIZATION; } }

        /// <summary>
        /// Gets or sets the dim employee organization identifier.
        /// </summary>
        /// <value>
        /// The dim employee organization identifier.
        /// </value>
        public long DimEmployeeOrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the type code.
        /// </summary>
        /// <value>
        /// The type code.
        /// </value>
        public string TypeCode { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_employee_organization";
            sqlCmd += "(ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id";
            sqlCmd += ", ver_history_count, employer_id, customer_id, employee_number, code, name, description, path, type_code, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id,";
            sqlCmd += " @ver_history_count, @employer_id, @customer_id, @employee_number, @code, @name, @description, @path, @type_code, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_employee_organization_id ";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number", EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@code", Code.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@path", Path.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type_code", TypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployeeOrganizationId = Convert.ToInt64(res);
            }
        }
    }
}
