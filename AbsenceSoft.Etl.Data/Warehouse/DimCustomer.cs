﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCustomer : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomer"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCustomer(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCustomer"/> class.
        /// </summary>
        public DimCustomer() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CUSTOMER; } }

        /// <summary>
        /// Gets or sets the dim customer identifier.
        /// </summary>
        /// <value>
        /// The dim customer identifier.
        /// </value>
        public long DimCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the customer created date.
        /// </summary>
        /// <value>
        /// The customer created date.
        /// </value>
        public DateTime? CustomerCreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the customer URL.
        /// </summary>
        /// <value>
        /// The customer URL.
        /// </value>
        public string CustomerUrl { get; set; }

        /// <summary>
        /// Gets or sets the first name of the customer contact.
        /// </summary>
        /// <value>
        /// The first name of the customer contact.
        /// </value>
        public string CustomerContactFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the customer contact.
        /// </summary>
        /// <value>
        /// The last name of the customer contact.
        /// </value>
        public string CustomerContactLastName { get; set; }

        /// <summary>
        /// Gets or sets the customer contact email.
        /// </summary>
        /// <value>
        /// The customer contact email.
        /// </value>
        public string CustomerContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the customer contact address1.
        /// </summary>
        /// <value>
        /// The customer contact address1.
        /// </value>
        public string CustomerContactAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the customer contact address2.
        /// </summary>
        /// <value>
        /// The customer contact address2.
        /// </value>
        public string CustomerContactAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the customer contact city.
        /// </summary>
        /// <value>
        /// The customer contact city.
        /// </value>
        public string CustomerContactCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the customer contact.
        /// </summary>
        /// <value>
        /// The state of the customer contact.
        /// </value>
        public string CustomerContactState { get; set; }

        /// <summary>
        /// Gets or sets the customer contact country.
        /// </summary>
        /// <value>
        /// The customer contact country.
        /// </value>
        public string CustomerContactCountry { get; set; }

        /// <summary>
        /// Gets or sets the customer contact postal code.
        /// </summary>
        /// <value>
        /// The customer contact postal code.
        /// </value>
        public string CustomerContactPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the first name of the customer billing.
        /// </summary>
        /// <value>
        /// The first name of the customer billing.
        /// </value>
        public string CustomerBillingFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the customer billing.
        /// </summary>
        /// <value>
        /// The last name of the customer billing.
        /// </value>
        public string CustomerBillingLastName { get; set; }

        /// <summary>
        /// Gets or sets the customer billing email.
        /// </summary>
        /// <value>
        /// The customer billing email.
        /// </value>
        public string CustomerBillingEmail { get; set; }

        /// <summary>
        /// Gets or sets the customer billing address1.
        /// </summary>
        /// <value>
        /// The customer billing address1.
        /// </value>
        public string CustomerBillingAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the customer billing address2.
        /// </summary>
        /// <value>
        /// The customer billing address2.
        /// </value>
        public string CustomerBillingAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the customer billing city.
        /// </summary>
        /// <value>
        /// The customer billing city.
        /// </value>
        public string CustomerBillingCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the customer billing.
        /// </summary>
        /// <value>
        /// The state of the customer billing.
        /// </value>
        public string CustomerBillingState { get; set; }

        /// <summary>
        /// Gets or sets the customer billing country.
        /// </summary>
        /// <value>
        /// The customer billing country.
        /// </value>
        public string CustomerBillingCountry { get; set; }

        /// <summary>
        /// Gets or sets the customer billing postal code.
        /// </summary>
        /// <value>
        /// The customer billing postal code.
        /// </value>
        public string CustomerBillingPostalCode { get; set; }
        
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd += "insert into dim_customer";
            sqlCmd += "(ext_ref_id, cust_customer_name, cust_customer_url, cust_contact_first_name, cust_contact_last_name";
            sqlCmd += ", cust_contact_email, cust_contact_address1, cust_contact_address2, cust_contact_city ";
            sqlCmd += ", cust_contact_state, cust_contact_postal_code, cust_contact_country, cust_billing_first_name ";
            sqlCmd += ", cust_billing_last_name, cust_billing_email, cust_billing_address1, cust_billing_address2";
            sqlCmd += ", cust_billing_city, cust_billing_state, cust_billing_postal_code, cust_billing_country";
            sqlCmd += ", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @cust_customer_name, @cust_customer_url, @cust_contact_first_name, @cust_contact_last_name,";
            sqlCmd += " @cust_contact_email, @cust_contact_address1, @cust_contact_address2, @cust_contact_city, ";
            sqlCmd += " @cust_contact_state, @cust_contact_postal_code, @cust_contact_country, @cust_billing_first_name,";
            sqlCmd += " @cust_billing_last_name, @cust_billing_email, @cust_billing_address1, @cust_billing_address2,";
            sqlCmd += " @cust_billing_city, @cust_billing_state, @cust_billing_postal_code, @cust_billing_country,";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_customer_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_customer_name", CustomerName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_customer_url", CustomerUrl.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_first_name", CustomerContactFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_last_name", CustomerContactLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_email", CustomerContactEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_address1", CustomerContactAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_address2", CustomerContactAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_city", CustomerContactCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_state", CustomerContactState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_postal_code", CustomerContactPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_contact_country", CustomerContactCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_first_name", CustomerBillingFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_last_name", CustomerBillingLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_email", CustomerBillingEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_address1", CustomerBillingAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_address2", CustomerBillingAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_city", CustomerBillingCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_state", CustomerBillingState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_postal_code", CustomerBillingPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cust_billing_country", CustomerBillingCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCustomerId = Convert.ToInt64(res);
            }
        }
    }
}
