﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimDemandType : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimDemandType"/> class.
        /// </summary>
        public DimDemandType() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDemandType"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimDemandType(string connectionString) : base(connectionString) { }
        
        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_DEMAND_TYPE; } }

        /// <summary>
        /// Gets or sets the dim demand type identifier.
        /// </summary>
        /// <value>
        /// The dim demand type identifier.
        /// </value>
        public long DimDemandTypeId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the type data.
        /// </summary>
        /// <value>
        /// The type data.
        /// </value>
        public string TypeData { get; set; }

        /// <summary>
        /// Gets or sets the order data.
        /// </summary>
        /// <value>
        /// The order data.
        /// </value>
        public int? OrderData { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the requirement label.
        /// </summary>
        /// <value>
        /// The requirement label.
        /// </value>
        public string RequirementLabel { get; set; }

        /// <summary>
        /// Gets or sets the restriction label.
        /// </summary>
        /// <value>
        /// The restriction label.
        /// </value>
        public string RestrictionLabel { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd += "insert into dim_demand_type";
            sqlCmd += "(ext_ref_id, customer_id, description, type_data, order_data, requirement_label, restriction_label, code,";
            sqlCmd += " dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @customer_id, @description, @type_data, @order_data, @requirement_label, @restriction_label, @code,";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_demand_type_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@type_data", TypeData.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@order_data", OrderData.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@requirement_label", RequirementLabel.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@restriction_label", RestrictionLabel.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@code", Code.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimDemandTypeId = Convert.ToInt64(res);
            }
        }
    }
}
