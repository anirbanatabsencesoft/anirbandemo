﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployerServiceOption : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerServiceOption"/> class.
        /// </summary>
        public DimEmployerServiceOption() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerServiceOption"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployerServiceOption(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_SERVICE_OPTIONS; } }

        /// <summary>
        /// Gets or sets the dim employeer service option identifier.
        /// </summary>
        /// <value>
        /// The dim employeer service option.
        /// </value>
        public long DimEmployerServiceOptionId { get; set; }

        /// <summary>
        /// Gets or sets the dim customer identifier.
        /// </summary>
        /// <value>
        /// The dim customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Employer Id
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the is_deleted flag.
        /// </summary>
        /// <value>
        /// The is_deleted flag
        /// </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the Key.
        /// </summary>
        /// <value>
        /// The Key attribute.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value attribute.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the customer service option id.
        /// </summary>
        /// <value>
        /// The customer service option id.
        /// </value>
        public string CustomerServiceOptionId { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_employer_service_option")
                .Append("(customer_id, employer_id, is_deleted, key, cusomerserviceoptionid, dim_ver_id, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count")
                .Append(") values (")
                .Append("@customer_id, @employer_id, @is_deleted, @key, @cusomerserviceoptionid, @dim_ver_id, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count")
                .Append(") RETURNING dim_employer_service_option_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@key", Key));
                cmd.Parameters.Add(new NpgsqlParameter("@cusomerserviceoptionid", CustomerServiceOptionId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));

                object res = cmd.ExecuteScalar();
                DimEmployerServiceOptionId = Convert.ToInt64(res);
            }
        }
    }
}
