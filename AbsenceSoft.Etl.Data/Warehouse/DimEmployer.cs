﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployer : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployer"/> class.
        /// </summary>
        public DimEmployer() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployer"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployer(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYER; } }

        /// <summary>
        /// Gets or sets the dim employer identifier.
        /// </summary>
        /// <value>
        /// The dim employer identifier.
        /// </value>
        public long DimEmployerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the reference code.
        /// </summary>
        /// <value>
        /// The reference code.
        /// </value>
        public string ReferenceCode { get; set; }

        /// <summary>
        /// Gets or sets the employer URL.
        /// </summary>
        /// <value>
        /// The employer URL.
        /// </value>
        public string EmployerUrl { get; set; }

        /// <summary>
        /// Gets or sets the reset month.
        /// </summary>
        /// <value>
        /// The reset month.
        /// </value>
        public int? ResetMonth { get; set; }

        /// <summary>
        /// Gets or sets the reset day of month.
        /// </summary>
        /// <value>
        /// The reset day of month.
        /// </value>
        public int? ResetDayOfMonth { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the type of the FML period.
        /// </summary>
        /// <value>
        /// The type of the FML period.
        /// </value>
        public long? FMLPeriodType { get; set; }

        /// <summary>
        /// Gets or sets the allow intermittent for birth adoption or foster care.
        /// </summary>
        /// <value>
        /// The allow intermittent for birth adoption or foster care.
        /// </value>
        public bool? AllowIntermittentForBirthAdoptionOrFosterCare { get; set; }

        /// <summary>
        /// Gets or sets the ignore schedule for prior hours worked.
        /// </summary>
        /// <value>
        /// The ignore schedule for prior hours worked.
        /// </value>
        public bool? IgnoreScheduleForPriorHoursWorked { get; set; }

        /// <summary>
        /// Gets or sets the ignore average minutes worked per week.
        /// </summary>
        /// <value>
        /// The ignore average minutes worked per week.
        /// </value>
        public bool? IgnoreAverageMinutesWorkedPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the enable50in75mile rule.
        /// </summary>
        /// <value>
        /// The enable50in75mile rule.
        /// </value>
        public bool? Enable50in75mileRule { get; set; }

        /// <summary>
        /// Gets or sets the enable spouse at same employer rule.
        /// </summary>
        /// <value>
        /// The enable spouse at same employer rule.
        /// </value>
        public bool? EnableSpouseAtSameEmployerRule { get; set; }

        /// <summary>
        /// Gets or sets the ft weekly work hours.
        /// </summary>
        /// <value>
        /// The ft weekly work hours.
        /// </value>
        public double? FTWeeklyWorkHours { get; set; }

        /// <summary>
        /// Gets or sets the publicly traded company.
        /// </summary>
        /// <value>
        /// The publicly traded company.
        /// </value>
        public bool? PubliclyTradedCompany { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the first name of the contact.
        /// </summary>
        /// <value>
        /// The first name of the contact.
        /// </value>
        public string ContactFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the contact.
        /// </summary>
        /// <value>
        /// The last name of the contact.
        /// </value>
        public string ContactLastName { get; set; }

        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        /// <value>
        /// The contact email.
        /// </value>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the contact address1.
        /// </summary>
        /// <value>
        /// The contact address1.
        /// </value>
        public string ContactAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the contact address2.
        /// </summary>
        /// <value>
        /// The contact address2.
        /// </value>
        public string ContactAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the contact city.
        /// </summary>
        /// <value>
        /// The contact city.
        /// </value>
        public string ContactCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the contact.
        /// </summary>
        /// <value>
        /// The state of the contact.
        /// </value>
        public string ContactState { get; set; }

        /// <summary>
        /// Gets or sets the contact country.
        /// </summary>
        /// <value>
        /// The contact country.
        /// </value>
        public string ContactCountry { get; set; }

        /// <summary>
        /// Gets or sets the contact postal code.
        /// </summary>
        /// <value>
        /// The contact postal code.
        /// </value>
        public string ContactPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the type of the service.
        /// </summary>
        /// <value>
        /// The type of the service.
        /// </value>
        public string ServiceType { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);        
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd += "insert into dim_employer";
            sqlCmd += "(ext_ref_id, name, reference_code, url, reset_month, reset_day_of_month";
            sqlCmd += ", fml_period_type, allow_intermittent_for_birth_adoption_or_foster_care, ignore_schedule_for_prior_hours_worked,ignore_average_minutes_worked_per_week ";
            sqlCmd += ", enable50in75mile_rule, enable_spouse_at_same_employer_rule, ft_weekly_work_hours, publicly_traded_company ";
            sqlCmd += ", customer_id, contact_first_name, contact_last_name, contact_email";
            sqlCmd += ", contact_address1, contact_address2, contact_city, contact_state, contact_postal_code, contact_country, is_deleted";
            sqlCmd += ", start_date, end_date";
            sqlCmd += ", dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date, eplr_service_type";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @name,@reference_code, @url, @reset_month, @reset_day_of_month, @fml_period_type, @allow_intermittent_for_birth_adoption_or_foster_care,";
            sqlCmd += " @ignore_schedule_for_prior_hours_worked,@ignore_average_minutes_worked_per_week, @enable50in75mile_rule, @enable_spouse_at_same_employer_rule, @ft_weekly_work_hours, @publicly_traded_company,";
            sqlCmd += " @customer_id, @contact_first_name, @contact_last_name, @contact_email, @contact_address1, @contact_address2, @contact_city, ";
            sqlCmd += " @contact_state, @contact_postal_code, @contact_country, @is_deleted,";
            sqlCmd += " @start_date, @end_date,";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date, @eplr_service_type";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_employer_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@reference_code", ReferenceCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@url", EmployerUrl.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@reset_month", ResetMonth.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@reset_day_of_month", ResetDayOfMonth.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@fml_period_type", FMLPeriodType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@allow_intermittent_for_birth_adoption_or_foster_care", AllowIntermittentForBirthAdoptionOrFosterCare.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ignore_schedule_for_prior_hours_worked", IgnoreScheduleForPriorHoursWorked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ignore_average_minutes_worked_per_week", IgnoreAverageMinutesWorkedPerWeek.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@enable50in75mile_rule", Enable50in75mileRule.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@enable_spouse_at_same_employer_rule", EnableSpouseAtSameEmployerRule.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ft_weekly_work_hours", FTWeeklyWorkHours.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@publicly_traded_company", PubliclyTradedCompany.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_first_name", ContactFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_last_name", ContactLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_email", ContactEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_address1", ContactAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_address2", ContactAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_city", ContactCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_state", ContactState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_postal_code",  ContactPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_country", ContactCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@eplr_service_type", ServiceType.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployerId = Convert.ToInt64(res);
            }
        }
    }
}
