﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimWorkflowInstance : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimWorkflowInstance"/> class.
        /// </summary>
        /// <param name="connectionstring">The connection string.</param>
        public DimWorkflowInstance(string connectionstring) : base(connectionstring) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimWorkflowInstance"/> class.
        /// </summary>
        public DimWorkflowInstance() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_WORKFLOW_INSTANCE; } }

        public long DimWorkflowInstanceId { get; set; }
        public DateTime? AccommodationEndDate { get; set; }
        public string AccommodationId { get; set; }
        public string AccommodationTypeCode { get; set; }
        public string AccommodationTypeId { get; set; }
        public string ActiveAccommodationId { get; set; }
        public string AppliedPolicyId { get; set; }
        public string AttachmentId { get; set; }
        public string CaseId { get; set; }
        public bool? CloseCase { get; set; }
        public string CommunicationId { get; set; }
        public int? CommunicationType { get; set; }
        public DateTime? ConditionStartDate { get; set; }
        public DateTime? ContactEmployeeDueDate { get; set; }
        public DateTime? ContactHcpDueDate { get; set; }
        public string CustomerId { get; set; }
        public string DenialExplanation { get; set; }
        public int? DenialReason { get; set; }
        public string Description { get; set; }
        public int? Determination { get; set; }
        public DateTime? DueDate { get; set; }
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public DateTime? ErgonomicAssessmentDate { get; set; }
        public string EventId { get; set; }
        public int? EventType { get; set; }
        public bool? ExtendGracePeriod { get; set; }
        public DateTime? FirstExhaustionDate { get; set; }
        public string GeneralHealthCondition { get; set; }
        public bool? HasWorkRestrictions { get; set; }
        public DateTime? IllnessOrInjuryDate { get; set; }        
        public DateTime? NewDueDate { get; set; }
        public string PaperworkAttachmentId { get; set; }
        public DateTime? PaperworkDueDate { get; set; }
        public string PaperworkId { get; set; }
        public string PaperworkName { get; set; }
        public bool? PaperworkReceived { get; set; }
        public string PolicyCode { get; set; }
        public string PolicyId { get; set; }
        public int? Reason { get; set; }
        public bool? RequiresReview { get; set; }
        public string ReturnAttachmentId { get; set; }
        public DateTime? ReturnToWorkDate { get; set; }
        public bool? RTW { get; set; }
        public string SourceWorkflowActivityActivityId { get; set; }
        public string SourceWorkflowActivityId { get; set; }
        public string SourceWorkflowInstanceId { get; set; }
        public int? Status { get; set; }
        public string Template { get; set; }
        public string WorkflowActivityId { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowName { get; set; }

        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }
       
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_workflow_instance
                    (employee_id, employer_id, accommodation_end_date	 , accommodation_id	, accommodation_type_code , accommodation_type_id , active_accommodation_id, applied_policy_id, attachment_id , case_id	 
                    , close_case , communication_id	 , communication_type , condition_start_date , contact_employee_due_date , contact_hcp_due_date 
                    , customer_id , denial_explanation	 , denial_reason , description	 , determination , due_date	 , ergonomic_assessment_date 
                    , event_id	 , event_type , extend_grace_period	 , first_exhaustion_date , general_health_condition	 , has_work_restrictions 
                    , illness_or_injury_date ,new_due_date	 , paperwork_attachment_id	 , paperwork_due_date , paperwork_id, paperwork_name	 
                    , paperwork_received , policy_code	 , policy_id , reason , requires_review	 , return_attachment_id	 , return_to_work_date	 , rtw 
                    , source_workflow_activity_activity_id	 , source_workflow_activity_id, source_workflow_instance_id	 , status , template , workflow_activity_id
                    , workflow_id , workflow_name , ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired 
                    , ver_root_id , ver_prev_id , ver_history_count , ext_created_by , ext_modified_by)
                    values
                    (@employee_id, @employer_id,@accommodation_end_date , @accommodation_id, @accommodation_type_code , @accommodation_type_id , @active_accommodation_id, @applied_policy_id, @attachment_id	 , @case_id	 
                    , @close_case , @communication_id , @communication_type , @condition_start_date , @contact_employee_due_date , @contact_hcp_due_date 
                    , @customer_id , @denial_explanation , @denial_reason , @description , @determination , @due_date	 , @ergonomic_assessment_date 
                    , @event_id	 , @event_type , @extend_grace_period	 , @first_exhaustion_date , @general_health_condition	 , @has_work_restrictions 
                    , @illness_or_injury_date ,@new_due_date , @paperwork_attachment_id	 , @paperwork_due_date , @paperwork_id, @paperwork_name	 
                    , @paperwork_received , @policy_code , @policy_id , @reason , @requires_review	 , @return_attachment_id , @return_to_work_date	 , @rtw 
                    , @source_workflow_activity_activity_id	 , @source_workflow_activity_id, @source_workflow_instance_id , @status , @template , @workflow_activity_id
                    , @workflow_id , @workflow_name , @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired 
                    , @ver_root_id , @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by)
                    RETURNING dim_workflow_instance_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_end_date" , AccommodationEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_id" , AccommodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_type_code" , AccommodationTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_type_id" , AccommodationTypeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@active_accommodation_id" , ActiveAccommodationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@applied_policy_id" , AppliedPolicyId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@attachment_id" , AttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id" , CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@close_case" , CloseCase.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_id" , CommunicationId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@communication_type" , CommunicationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@condition_start_date" , ConditionStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_employee_due_date" , ContactEmployeeDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_hcp_due_date" , ContactHcpDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id" , CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_explanation" , DenialExplanation.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason" , DenialReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description" , Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination" , Determination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@due_date" , DueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ergonomic_assessment_date" , ErgonomicAssessmentDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@event_id" , EventId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@event_type" , EventType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@extend_grace_period" , ExtendGracePeriod.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_exhaustion_date" , FirstExhaustionDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@general_health_condition" , GeneralHealthCondition.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@has_work_restrictions" , HasWorkRestrictions.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@illness_or_injury_date" , IllnessOrInjuryDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@new_due_date" , NewDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_attachment_id" , PaperworkAttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_due_date" , PaperworkDueDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_id" , PaperworkId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_name" , PaperworkName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@paperwork_received" , PaperworkReceived.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_code" , PolicyCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_id" , PolicyId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@reason" , Reason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@requires_review" , RequiresReview.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@return_attachment_id" , ReturnAttachmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@return_to_work_date" , ReturnToWorkDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@rtw" , RTW.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_activity_activity_id" , SourceWorkflowActivityActivityId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_activity_id" , SourceWorkflowActivityId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@source_workflow_instance_id" , SourceWorkflowInstanceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status" , Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@template" , Template.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@workflow_activity_id" , WorkflowActivityId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@workflow_id" , WorkflowId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@workflow_name" , WorkflowName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimWorkflowInstanceId = Convert.ToInt64(res);
            }
        }

    }
}
