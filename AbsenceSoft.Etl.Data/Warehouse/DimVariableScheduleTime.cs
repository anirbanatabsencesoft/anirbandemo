﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimVariableScheduleTime : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimVariableScheduleTime"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimVariableScheduleTime(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimVariableScheduleTime"/> class.
        /// </summary>
        public DimVariableScheduleTime() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_VARIABLE_SCHEDULE_TIME; } }
        public long DimVariableScheduleTimeId { get; set; }
        public string EmployeeId  { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        public DateTime? TimeSampleDate { get; set; }
        public int? TimeTotalMinutes { get; set; }
        public string EmployeeNumber { get; set; }

        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }
       
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;
            
            sqlCmd = @"insert into dim_variable_schedule_time
                    (ext_ref_id, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, 
                     ver_prev_id, ver_history_count, ext_created_by, ext_modified_by, employee_id, employer_id, customer_id, 
                     time_sample_date, time_total_minutes, employee_number, is_deleted, created_date
                    )
                    values
                    (
                    @ext_ref_id, @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired, @ver_root_id,
                    @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by, @employee_id , @employer_id , @customer_id ,
                    @time_sample_date, @time_total_minutes, @employee_number, @is_deleted, @created_date
                    )";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@time_sample_date", TimeSampleDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@time_total_minutes",TimeTotalMinutes.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number", EmployeeNumber.ValueOrDbNull()));              
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_date", ExternalCreatedDate.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimVariableScheduleTimeId = Convert.ToInt64(res);
            }
        }

    }
}
