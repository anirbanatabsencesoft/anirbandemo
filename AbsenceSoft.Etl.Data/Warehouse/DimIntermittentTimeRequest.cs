﻿using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;
using System.Text;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Table: dim_case_segment_request 
    /// </summary>
    public class DimIntermittentTimeRequest : DimBase
    {

        #region Constructor chain
        /// <summary>
        /// Constructor with the connection string
        /// </summary>
        /// <param name="connectionString">Connection string pf Postgres</param>
        public DimIntermittentTimeRequest(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Default constructor for DimIntermittentTimeRequest
        /// </summary>
        public DimIntermittentTimeRequest() : base() { }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_SEGMENT; } }

        /// <summary>
        /// Maped to dim_case_segment_request_id
        /// </summary>
        public long DimIntermittentTimeRequestId { get; set; }

        public long DimCaseSegmentId { get; set; }

        /// <summary>
        /// Map to: ext_created_by 
        /// Derive from Case .
        /// Case Modified By If not new.
        /// </summary>
        [MaxLength(24)]
        public string ExternalCreatedBy { get; set; }

        /// <summary>
        /// Map to: ext_modified_by
        /// Derive from Case
        /// </summary>
        [MaxLength(24)]
        public string ExternalModifiedBy { get; set; }

        /// <summary>
        /// Map to case_id
        /// </summary>
        [MaxLength(24), Required]
        public string CaseId { get; set; }

        /// <summary>
        /// Map to: employee_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Map to: customer_id
        /// </summary>
        [MaxLength(24), Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// Map to: employer_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployerId { get; set; }

        /// <summary>
        /// Start Time of Day
        /// </summary>
        public string StartTimeOfLeave { get; set; }

        /// <summary>
        /// End Time of Day
        /// </summary>
        public string EndTimeOfLeave { get; set; }

        /// <summary>
        /// Total minutes
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// Free form notes field; user supplied
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// The date to change
        /// </summary>
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the record hash. This value is used to compare versions of the policy
        /// and is a direct replacement for the absence of a valid modified date on the policy itself.
        /// </summary>
        /// <value>
        /// The record hash. Basically an MD5 hash checksum of the record JSON.
        /// </value>
        public string RecordHash { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Returns the SQL
        /// </summary>
        /// <returns></returns>
        private string GetSQLString()
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("INSERT INTO dim_case_segment_request (");

            //system fields
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");

            //dimension fields
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");

            sqlCmd.Append(",dim_case_segment_id");
            sqlCmd.Append(",start_time_of_leave");
            sqlCmd.Append(",end_time_of_leave");
            sqlCmd.Append(",total_minutes");
            sqlCmd.Append(",notes");
            sqlCmd.Append(",request_date");

            sqlCmd.Append(",record_hash");

            sqlCmd.Append(") VALUES (");

            //system parameters
            sqlCmd.Append("@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count");

            //dimension parameters
            sqlCmd.Append(",@ext_created_by");
            sqlCmd.Append(",@ext_modified_by");
            sqlCmd.Append(",@case_id");
            sqlCmd.Append(",@employee_id");
            sqlCmd.Append(",@customer_id");
            sqlCmd.Append(",@employer_id");
            
            sqlCmd.Append(",@dim_case_segment_id");
            sqlCmd.Append(",@start_time_of_leave");
            sqlCmd.Append(",@end_time_of_leave");
            sqlCmd.Append(",@total_minutes");
            sqlCmd.Append(",@notes");
            sqlCmd.Append(",@request_date");

            sqlCmd.Append(",@record_hash");

            sqlCmd.Append(") RETURNING dim_case_segment_request_id");

            return sqlCmd.ToString();
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(GetSQLString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", ExternalCreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ExternalModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_segment_id", DimCaseSegmentId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_time_of_leave", StartTimeOfLeave.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_time_of_leave", EndTimeOfLeave.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@total_minutes", TotalMinutes.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@notes", Notes.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@request_date", RequestDate.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@record_hash", RecordHash.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimIntermittentTimeRequestId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimIntermittentTimeRequest> LoadHistory(string caseId)
        {
            return Execute(conn => sqlLoadHistory(conn, caseId));
        }


        /// <summary>
        /// Get the data from Postgres against a case id
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        protected List<DimIntermittentTimeRequest> sqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimIntermittentTimeRequest> lstCasePolicies = new List<DimIntermittentTimeRequest>();

            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("SELECT ");
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            
            sqlCmd.Append(",dim_case_segment_id");
            sqlCmd.Append(",start_time_of_leave");
            sqlCmd.Append(",end_time_of_leave");
            sqlCmd.Append(",TotalMinutes");
            sqlCmd.Append(",Notes");
            sqlCmd.Append(",RequestDate");

            sqlCmd.Append(",record_hash");
            
            sqlCmd.Append(" FROM dim_case_segment_request WHERE case_id = @case_id and ver_is_current = true");
            sqlCmd.Append(" ORDER BY ver_seq");

            using (NpgsqlCommand command = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                //add parameters to search
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));

                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        //instantiate
                        DimIntermittentTimeRequest dimCP = new DimIntermittentTimeRequest(ConnectionString);

                        dimCP.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.DimVerId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsCurrent = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionSequence = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsExpired = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionRootId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionPreviousId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionHistoryCount = dr.GetValueOrDefault<long>(i++);
                        
                        dimCP.ExternalCreatedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalModifiedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.CaseId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployeeId = dr.GetValueOrDefault<string>(i++);
                        dimCP.CustomerId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployerId = dr.GetValueOrDefault<string>(i++);

                        dimCP.DimCaseSegmentId = dr.GetValueOrDefault<long>(i++);
                        dimCP.StartTimeOfLeave = dr.GetValueOrDefault<string>(i++);
                        dimCP.EndTimeOfLeave = dr.GetValueOrDefault<string>(i++);
                        dimCP.TotalMinutes = dr.GetValueOrDefault<int>(i++);
                        dimCP.Notes = dr.GetValueOrDefault<string>(i++);
                        dimCP.RequestDate = dr.GetValueOrDefault<DateTime>(i++);

                        dimCP.RecordHash = dr.GetValueOrDefault<string>(i++);
                        
                        //add to list
                        lstCasePolicies.Add(dimCP);
                    }
                }
            }

            return lstCasePolicies;
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(sqlDelete);
        }

        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            sqlDelete(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void sqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_case_segment_request where dim_case_segment_id = @dim_case_segment_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_segment_id", DimCaseSegmentId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}

