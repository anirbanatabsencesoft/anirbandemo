﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Dim Employee Job
    /// </summary>
    public class DimEmployeeJob : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeJob"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeJob(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeJob"/> class.
        /// </summary>
        public DimEmployeeJob() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_JOB; } }

        /// <summary>
        /// Gets or sets the dim employee job id.
        /// </summary>
        /// <value>
        /// The dim employee job identifier.
        /// </value>
        public long DimEmployeeJobId { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        /// <value>
        /// The employee id
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the id of the employer.
        /// </summary>
        /// <value>
        /// The id of the employer.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        /// <value>
        /// The job name of the employee.
        /// </value>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets the job code.
        /// </summary>
        /// <value>
        /// The job code.
        /// </value>
        public string JobCode { get; set; }

        /// <summary>
        /// Gets or sets the status of the job.
        /// </summary>
        /// <value>
        /// The job status.
        /// </value>
        public int? StatusValue { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>
        /// The reason
        /// </value>
        public int? StatusReason { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        public string StatusComments { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The Username.
        /// </value>
        public string StatusUserName { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The Username.
        /// </value>
        public string StatusUserId { get; set; }

        /// <summary>
        /// Gets or sets the StatusDate.
        /// </summary>
        /// <value>
        /// The StatusDate.
        /// </value>
        public DateTime? StatusDate { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate
        /// </summary>
        /// <value>
        /// The EndDate
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        /// <summary>
        /// Save the dimension
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Sql Insert function
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_employee_job ");
            sqlCmd.Append("(employee_id, employer_id, customer_id, job_name, job_code, status_value, status_reason, status_comments, status_username, status_userid, status_date, start_date, end_date, dim_ver_id, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, is_deleted");
            sqlCmd.Append(") values (");
            sqlCmd.Append(" @employee_id, @employer_id, @customer_id, @job_name, @job_code, @status, @reason, @comments, @username, @userid, @status_date, @start_date, @end_date, @dim_ver_id, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @is_deleted");
            sqlCmd.Append(") RETURNING dim_employee_job_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_name", JobName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@job_code", JobCode.ValueOrDbNull()));

                if(StatusValue.HasValue)
                    cmd.Parameters.Add(new NpgsqlParameter("@status", Convert.ToInt32(StatusValue.Value)));
                else
                    cmd.Parameters.Add(new NpgsqlParameter("@status", DBNull.Value));

                if (StatusReason.HasValue)
                    cmd.Parameters.Add(new NpgsqlParameter("@reason", Convert.ToInt32(StatusReason.Value)));
                else
                    cmd.Parameters.Add(new NpgsqlParameter("@reason", DBNull.Value));

                cmd.Parameters.Add(new NpgsqlParameter("@comments", StatusComments.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@username", StatusUserName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@userid", StatusUserId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status_date", StatusDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                
                object res = cmd.ExecuteScalar();
                DimEmployeeJobId = Convert.ToInt64(res);
            }
        }
    }
}
