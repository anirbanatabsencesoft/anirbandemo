﻿using System;
using Npgsql;
using NpgsqlTypes;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimUsers : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimUsers"/> class.
        /// </summary>
        public DimUsers() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimUsers"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimUsers(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_USERS; } }

        /// <summary>
        /// Gets or sets the dim user identifier.
        /// </summary>
        /// <value>
        /// The dim user identifier.
        /// </value>
        public long DimUserId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the failed login attempts.
        /// </summary>
        /// <value>
        /// The failed login attempts.
        /// </value>
        public long FailedLoginAttempts { get; set; }

        /// <summary>
        /// Gets or sets the last failed login attempt.
        /// </summary>
        /// <value>
        /// The last failed login attempt.
        /// </value>
        public DateTime? LastFailedLoginAttempt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DimUsers"/> is locked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if locked; otherwise, <c>false</c>.
        /// </value>
        public bool Locked { get; set; }

        /// <summary>
        /// Gets or sets the locked date.
        /// </summary>
        /// <value>
        /// The locked date.
        /// </value>
        public DateTime? LockedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DimUsers"/> is disabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disabled; otherwise, <c>false</c>.
        /// </value>
        public bool Disabled { get; set; }

        /// <summary>
        /// Gets or sets the disabled date.
        /// </summary>
        /// <value>
        /// The disabled date.
        /// </value>
        public DateTime? DisabledDate { get; set; }

        /// <summary>
        /// Gets or sets the last activity date.
        /// </summary>
        /// <value>
        /// The last activity date.
        /// </value>
        public DateTime? LastActivityDate { get; set; }

        /// <summary>
        /// Gets or sets the user flags.
        /// </summary>
        /// <value>
        /// The user flags.
        /// </value>
        public int UserFlags { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is self service user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is self service user; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelfServiceUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is portal user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is portal user; otherwise, <c>false</c>.
        /// </value>
        public bool IsPortalUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [must change password].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [must change password]; otherwise, <c>false</c>.
        /// </value>
        public bool MustChangePassword { get; set; }

        /// <summary>
        /// Gets or sets the user status.
        /// </summary>
        /// <value>
        /// The user status.
        /// </value>
        public int UserStatus { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        public int UserType { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the name of the role.
        /// </summary>
        /// <value>
        /// The name of the role.
        /// </value>
        public string RoleName { get; set; }

        /// <summary>
        /// Gets or sets the employee reference identifier.
        /// </summary>
        /// <value>
        /// The employee reference identifier.
        /// </value>
        public string EmployeeRefId { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the employer ids.
        /// </summary>
        /// <value>
        /// The employer ids.
        /// </value>
        public string[] EmployerIds { get; set; }

        /// <summary>
        /// Gets or sets the role ids.
        /// </summary>
        /// <value>
        /// The role ids.
        /// </value>
        public string[] RoleIds { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd += "insert into dim_users";
            sqlCmd += "(ext_ref_id, first_name, last_name, email, is_deleted,";
            sqlCmd += " dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date,";
            sqlCmd += " failed_login_attempts, last_failed_login_attempt, locked, locked_date, disabled, disabled_date, ";
            sqlCmd += " last_activity_date, user_flags, is_self_service_user,customer_id, ";
            sqlCmd += " is_portal_user, user_status, user_type, role_name, role_ids, user_employee_id, user_employee_number, must_change_password, display_name, employer_ids";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += "(@ext_ref_id, @first_name, @last_name, @email, @is_deleted, ";
            sqlCmd += " @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date, ";
            sqlCmd += " @failed_login_attempts, @last_failed_login_attempt, @locked, @locked_date, @disabled, @disabled_date, ";
            sqlCmd += " @last_activity_date, @user_flags, @is_self_service_user, @customer_id, ";
            sqlCmd += " @is_portal_user, @user_status, @user_type, @role_name, @role_ids, @user_employee_id, @user_employee_number, @must_change_password, @display_name, @employer_ids";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_user_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_name", FirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_name", LastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@email", Email.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@failed_login_attempts", FailedLoginAttempts));
                cmd.Parameters.Add(new NpgsqlParameter("@last_failed_login_attempt", LastFailedLoginAttempt.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@locked", Locked));
                cmd.Parameters.Add(new NpgsqlParameter("@locked_date", LockedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@disabled", Disabled));
                cmd.Parameters.Add(new NpgsqlParameter("@disabled_date", DisabledDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_activity_date", LastActivityDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@user_flags", UserFlags));
                cmd.Parameters.Add(new NpgsqlParameter("@is_self_service_user", IsSelfServiceUser));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_portal_user", IsPortalUser));
                cmd.Parameters.Add(new NpgsqlParameter("@user_status", UserStatus));
                cmd.Parameters.Add(new NpgsqlParameter("@user_type", UserType));
                cmd.Parameters.Add(new NpgsqlParameter("@role_name", RoleName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@role_ids", NpgsqlDbType.Array | NpgsqlDbType.Varchar) { Value = RoleIds.ValueOrDbNull() });
                cmd.Parameters.Add(new NpgsqlParameter("@user_employee_id", EmployeeRefId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@user_employee_number", EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@must_change_password", MustChangePassword));
                cmd.Parameters.Add(new NpgsqlParameter("@display_name", DisplayName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_ids", NpgsqlDbType.Array | NpgsqlDbType.Varchar) { Value = EmployerIds.ValueOrDbNull() });

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimUserId = Convert.ToInt64(res);
            }
        }
    }
}
