﻿using Npgsql;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimDenialReason : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimDenialReason"/> class.
        /// </summary>
        public DimDenialReason() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDenialReason"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimDenialReason(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_DENIAL_REASON; } }

        /// <summary>
        /// Gets or sets the dim employee contact type identifier.
        /// </summary>
        /// <value>
        /// The dim denial reason identifier.
        /// </value>
        public long DimDenialReasonId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the code
        /// </summary>
        public DateTime? DisabledDate { get; set; }
        
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection conn)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_denial_reason (";
            sqlCmd += "ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count ";
            sqlCmd += ", ext_created_by, ext_modified_by, is_deleted, customer_id,  description, code, disabled_date";
            sqlCmd += ") values (";
            sqlCmd += "@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count";
            sqlCmd += ", @ext_created_by, @ext_modified_by, @is_deleted, @customer_id,  @description, @code, @disabled_date";
            sqlCmd += " ) RETURNING dim_denial_reason_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@code", Code.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@disabled_date", DisabledDate.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimDenialReasonId = Convert.ToInt64(res);
            }
        }
    }
}
