﻿using System;
using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimOrganizationAnnualInfo : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganizationAnnualInfo"/> class.
        /// </summary>
        public DimOrganizationAnnualInfo() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimOrganizationAnnualInfo"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimOrganizationAnnualInfo(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ORGANIZATION_ANNUAL_INFO; } }

        /// <summary>
        /// Gets or sets the dim organization annual information identifier.
        /// </summary>
        /// <value>
        /// The dim organization annual information identifier.
        /// </value>
        public long DimOrganizationAnnualInfoId { get; set; }

     
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the org code.
        /// </summary>
        /// <value>
        /// The org code.
        /// </value>
        public string OrganizationCode { get; set; }

        /// <summary>
        /// Gets or sets the org year.
        /// </summary>
        /// <value>
        /// The org year.
        /// </value>
        public int OrganizationYear { get; set; }

        /// <summary>
        /// Gets or sets the average employee count.
        /// </summary>
        /// <value>
        /// The average employee count.
        /// </value>
        public int? AverageEmployeeCount { get; set; }

        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        public decimal? TotalHoursWorked { get; set; }
      
        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_organization_annual_info")
                .Append("(ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id")
                .Append(", ver_history_count, employer_id, customer_id, org_code, org_year, average_employee_count, total_hours_worked,")
                .Append("is_deleted")
                .Append(", ext_created_by, ext_modified_by")
                .Append(") values ")
                .Append("(@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id,")
                .Append(" @ver_history_count, @employer_id, @customer_id, @orgcode, @orgyear, @average_employee_count, @total_hours_worked,")
                .Append("@is_deleted")
                .Append(", @ext_created_by, @ext_modified_by")
                .Append(") RETURNING dim_organization_annual_info_id ");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@orgcode", OrganizationCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@orgyear", OrganizationYear.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@average_employee_count", AverageEmployeeCount.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@total_hours_worked", TotalHoursWorked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimOrganizationAnnualInfoId = Convert.ToInt64(res);
            }
        }
    }
}
