﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Dim Employee Note
    /// </summary>
    public class DimEmployeeNote : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeNote"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeNote(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeNote"/> class.
        /// </summary>
        public DimEmployeeNote() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_NOTE; } }

        /// <summary>
        /// Gets or sets the dim employee note id.
        /// </summary>
        /// <value>
        /// The dim employee note identifier.
        /// </value>
        public long DimEmployeeNoteId { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        /// <value>
        /// The employee Id.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employer id.
        /// </summary>
        /// <value>
        /// The id of the employer.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer Id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Note
        /// </summary>
        /// <value>
        /// The Note text
        /// </value>
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets whether the note is public or not.
        /// </summary>
        /// <value>
        /// true/false.
        /// </value>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Gets or sets the Category Code.
        /// </summary>
        /// <value>
        /// The Category Code.
        /// </value>
        public string CategoryCode { get; set; }

        /// <summary>
        /// Gets or sets the Category Name.
        /// </summary>
        /// <value>
        /// The Category Name.
        /// </value>
        public string CategoryName { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        /// <summary>
        /// Saves the instance
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Sql Insert function
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_employee_note ");
            sqlCmd.Append("(ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_by, ext_modified_by, employee_id, employer_id, customer_id, note, is_public, category_code, category_name");
            sqlCmd.Append(") values (");
            sqlCmd.Append(" @ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_by, @ext_modified_by, @employee_id, @employer_id, @customer_id, @note, @is_public, @category_code, @category_name");
            sqlCmd.Append(") RETURNING dim_employee_note_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@note", Note.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_public", IsPublic));
                cmd.Parameters.Add(new NpgsqlParameter("@category_code", CategoryCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@category_name", CategoryName.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployeeNoteId = Convert.ToInt64(res);
            }
        }
    }
}
