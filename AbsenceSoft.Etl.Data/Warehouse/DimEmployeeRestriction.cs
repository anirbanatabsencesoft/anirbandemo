﻿using System;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployeeRestriction : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeRestriction"/> class.
        /// </summary>
        public DimEmployeeRestriction() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeRestriction"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeRestriction(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier. dim_employee_restriction
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_EMPLOYEE_RESTRICTION; } }

        /// <summary>
        /// Gets or sets the name of the restriction.
        /// </summary>
        /// <value>
        /// The name of the restriction.
        /// </value>
        public string RestrictionName { get; set; }

        /// <summary>
        /// Gets or sets the restriction code.
        /// </summary>
        /// <value>
        /// The restriction code.
        /// </value>
        public string RestrictionCode { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the dim employee restriction identifier.
        /// </summary>
        /// <value>
        /// The dim employee restriction identifier.
        /// </value>
        public long DimEmployeeRestrictionId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_employee_restriction ";
            sqlCmd += "(ext_ref_id, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count, ext_created_date, ext_modified_date";
            sqlCmd += ", start_date, end_date, case_id, wkrs_restriction_name, wkrs_restriction_code, is_deleted";
            sqlCmd += ", ext_created_by, ext_modified_by";
            sqlCmd += ") values ";
            sqlCmd += " (@ext_ref_id, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_date, @ext_modified_date";
            sqlCmd += ", @start_date, @end_date, @case_id, @wkrs_restriction_name, @wkrs_restriction_code, @is_deleted";
            sqlCmd += ", @ext_created_by, @ext_modified_by";
            sqlCmd += ") RETURNING dim_employee_restriction_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wkrs_restriction_name", RestrictionName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wkrs_restriction_code", RestrictionCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployeeRestrictionId = Convert.ToInt64(res);
            }
        }
    }
}
