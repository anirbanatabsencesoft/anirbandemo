﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCaseEventDate : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCaseEventDate(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        public DimCaseEventDate() : base() { }

        public string RecordHash { get; set; }
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_EVENT_DATE; } }

        public long DimCaseEventDateId { get; set; }
        public string CaseId { get; set; }

        public int EventType { get; set; }

        public DateTime? EventDate { get; set; }
        
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimCaseEventDate> LoadHistory(string caseId)
        {
            return Execute(conn => sqlLoadHistory(conn, caseId));
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_case_event_date
                    (case_id , event_type , event_date, dim_ver_id, ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired , ver_root_id , 
                    ver_prev_id , ver_history_count , ext_created_by , ext_modified_by)
                    values
                    (@case_id , @event_type, @event_date, @dim_ver_id, @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired , @ver_root_id ,
                    @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by)
                    RETURNING dim_case_event_date_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@event_date", EventDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@event_type", EventType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCaseEventDateId = Convert.ToInt64(res);
            }
        }

        protected List<DimCaseEventDate> sqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimCaseEventDate> lstCaseEventDates = new List<DimCaseEventDate>();

            string sql = @" SELECT ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired
                        , ver_root_id, ver_prev_id, Ver_history_count, case_id,event_date, event_type, is_deleted
                        , dim_case_event_date_id, record_hash, ext_created_by, ext_modified_by
                         FROM dim_case_event_date
                         WHERE case_id = @case_id and ver_is_current = true
                         ORDER BY ver_seq";

            using (NpgsqlCommand command = new NpgsqlCommand(sql, pgsqlConnection))
            {
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));
                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        DimCaseEventDate dimCaseEventDate = new DimCaseEventDate(ConnectionString);
                        dimCaseEventDate.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        dimCaseEventDate.ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCaseEventDate.ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++);   
                        dimCaseEventDate.DimVerId= dr.GetValueOrDefault<long>(i++);
                        dimCaseEventDate.VersionIsCurrent = dr.GetValueOrDefault<bool>(i++);
                        dimCaseEventDate.VersionSequence = dr.GetValueOrDefault<long>(i++);
                        dimCaseEventDate.VersionIsExpired = dr.GetValueOrDefault<bool>(i++);
                        dimCaseEventDate.VersionRootId = dr.GetValueOrDefault<long>(i++);
                        dimCaseEventDate.VersionPreviousId = dr.GetValueOrDefault<long>(i++);
                        dimCaseEventDate.VersionHistoryCount = dr.GetValueOrDefault<long>(i++);                      
                        dimCaseEventDate.CaseId = dr.GetValueOrDefault<string>(i++);
                        dimCaseEventDate.EventDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCaseEventDate.EventType = dr.GetValueOrDefault<int>(i++);                       
                        dimCaseEventDate.IsDeleted = dr.GetValueOrDefault<bool>(i++);
                        dimCaseEventDate.DimCaseEventDateId = dr.GetValueOrDefault<long>(i++);                     
                        dimCaseEventDate.RecordHash = dr.GetValueOrDefault<string>(i++);                     
                        dimCaseEventDate.CreatedBy = dr.GetValueOrDefault<string>(i++);
                        dimCaseEventDate.ModifiedBy = dr.GetValueOrDefault<string>(i++);
                        lstCaseEventDates.Add(dimCaseEventDate);
                    }
                }
            }

            return lstCaseEventDates;
        }

    }
}
