﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Table: dim_case_policy_usage 
    /// </summary>
    public class DimCasePolicyUsage : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the DimCasePolicyUsage class.
        /// </summary>
        /// <param name="connectionString">The string connection string.</param>
        public DimCasePolicyUsage(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCases"/> class.
        /// </summary>
        public DimCasePolicyUsage() : base() { }

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_POLICY; } }

        /// <summary>
        /// Map to: dim_case_policy_usage_id
        /// Auto incremented valuein database
        /// </summary>
        public long DimCasePolicyUsageId { get; set; }


        [MaxLength(24)]
        public string ExternalCreatedBy { get; set; }

        /// <summary>
        /// Map to: ext_modified_by
        /// Derive from Case
        /// </summary>
        [MaxLength(24)]
        public string ExternalModifiedBy { get; set; }

        /// <summary>
        /// Map to case_id
        /// </summary>
        [MaxLength(24), Required]
        public string CaseId { get; set; }

        /// <summary>
        /// Map to: employee_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Map to: customer_id
        /// </summary>
        [MaxLength(24), Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// Map to: employer_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployerId { get; set; }

        /// <summary>
        /// Map to: policy_code
        /// Provides compound key against dim_case_policy
        /// </summary>
        [MaxLength(50), Required]
        public string PolicyCode { get; set; }

        /// <summary>
        /// Map to: date_used
        /// </summary>
        public DateTime? DateUsed { get; set; }

        /// <summary>
        /// Map to: minutes_used
        /// </summary>
        public int? MinutesUsed { get; set; }

        /// <summary>
        /// Map to: minutes_in_day
        /// </summary>
        public int? MinutesInDay { get; set; }

        /// <summary>
        /// Map to: hours_used
        /// </summary>
        public decimal HoursUsed { get; set; }

        /// <summary>
        /// Map to: hours_in_day
        /// </summary>
        public decimal HoursInDay { get; set; }

        /// <summary>
        /// Map to: user_entered
        /// </summary>
        public bool? UserEntered { get; set; }

        /// <summary>
        /// Map to: determination
        /// Lookup: dim_lookup_case_determination
        /// </summary>
        public int? Determination { get; set; }

        /// <summary>
        /// Map to: denial_reason_other
        /// </summary>
        public string OtherDenialReason { get; set; }

        /// <summary>
        /// Map to: denial_reason_code
        /// </summary>
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Map to: denial_reason_description
        /// </summary>
        public string DenialReasonDescription { get; set; }

        /// <summary>
        /// Map to: determined_by
        /// </summary>
        [MaxLength(24)]
        public string DeterminedBy { get; set; }

        /// <summary>
        /// Map to: intermittent_determination
        /// Lookup against dim_lookup. Type: case_intermittent_status
        /// </summary>
        public int? IntermittentDetermination { get; set; }

        /// <summary>
        /// Map to: is_locked
        /// </summary>
        public bool? IsLocked { get; set; }

        /// <summary>
        /// Map to: uses_time
        /// </summary>
        public bool? UsesTime { get; set; }

        /// <summary>
        /// Map to: percent_of_day
        /// </summary>
        public decimal? PercentOfDay { get; set; }

        /// <summary>
        /// Map to: status
        /// Lookup against dim_lookup_case_determination
        /// Maps against Summary Status
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// Map to: is_intermittent_restriction
        /// </summary>
        public bool?  IsIntermittentRestriction { get; set; }

        /// <summary>
        /// Usage for Case Policy Id
        /// Map to: dim_case_policy_id
        /// </summary>
        public long? DimCasePolicyId { get; set; }

     
        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(sqlDelete);
        }

        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            sqlDelete(pgsqlConnection);
        }


        //Get the SQL String for insert
        private string GetSQLString()
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("INSERT INTO dim_case_policy_usage (");

            //system fields
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");

            //dimension fields
            sqlCmd.Append(",dim_case_policy_id");
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            sqlCmd.Append(",policy_code");
            sqlCmd.Append(",date_used");
            sqlCmd.Append(",minutes_used");
            sqlCmd.Append(",minutes_in_day");
            sqlCmd.Append(",hours_used");
            sqlCmd.Append(",hours_in_day");
            sqlCmd.Append(",user_entered");
            sqlCmd.Append(",determination");
            sqlCmd.Append(",denial_reason_other");
            sqlCmd.Append(",denial_reason_code");
            sqlCmd.Append(",denial_reason_description");
            sqlCmd.Append(",determined_by");
            sqlCmd.Append(",intermittent_determination");
            sqlCmd.Append(",is_locked");
            sqlCmd.Append(",uses_time");
            sqlCmd.Append(",percent_of_day");
            sqlCmd.Append(",status");
            sqlCmd.Append(",is_intermittent_restriction");

            sqlCmd.Append(") VALUES (");


            //system parameters
            sqlCmd.Append("@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count");

            //dimension parameters
            sqlCmd.Append(",@dim_case_policy_id");
            sqlCmd.Append(",@ext_created_by");
            sqlCmd.Append(",@ext_modified_by");
            sqlCmd.Append(",@case_id");
            sqlCmd.Append(",@employee_id");
            sqlCmd.Append(",@customer_id");
            sqlCmd.Append(",@employer_id");
            sqlCmd.Append(",@policy_code");
            sqlCmd.Append(",@date_used");
            sqlCmd.Append(",@minutes_used");
            sqlCmd.Append(",@minutes_in_day");
            sqlCmd.Append(",@hours_used");
            sqlCmd.Append(",@hours_in_day");
            sqlCmd.Append(",@user_entered");
            sqlCmd.Append(",@determination");
            sqlCmd.Append(",@denial_reason_other");
            sqlCmd.Append(",@denial_reason_code");
            sqlCmd.Append(",@denial_reason_description");
            sqlCmd.Append(",@determined_by");
            sqlCmd.Append(",@intermittent_determination");
            sqlCmd.Append(",@is_locked");
            sqlCmd.Append(",@uses_time");
            sqlCmd.Append(",@percent_of_day");
            sqlCmd.Append(",@status");
            sqlCmd.Append(",@is_intermittent_restriction");

            sqlCmd.Append(")");

            return sqlCmd.ToString();
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(GetSQLString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));


                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_policy_id", this.DimCasePolicyId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", this.ExternalCreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", this.ExternalModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@policy_code", PolicyCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@date_used", DateUsed.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@minutes_used", MinutesUsed.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@minutes_in_day", MinutesInDay.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@hours_used", HoursUsed));
                cmd.Parameters.Add(new NpgsqlParameter("@hours_in_day", HoursInDay.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@user_entered", UserEntered.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination", Determination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_other", OtherDenialReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_code", DenialReasonCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_description", DenialReasonDescription.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determined_by", DeterminedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@intermittent_determination", IntermittentDetermination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_locked", IsLocked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@uses_time", UsesTime.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@percent_of_day", PercentOfDay.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_intermittent_restriction", IsIntermittentRestriction.ValueOrDbNull()));
                

                object res = cmd.ExecuteScalar();
                DimCasePolicyUsageId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void sqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_case_policy_usage where case_id = @dim_case_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_case_id", CaseId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}

