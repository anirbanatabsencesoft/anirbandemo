﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCaseNote : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimAttachment"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCaseNote(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimAttachment"/> class.
        /// </summary>
        public DimCaseNote() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_NOTE; } }
        public long DimCaseNoteId  { get; set;}
        public string CaseId { get; set; }
        public string EmployeeId { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string AccommId  { get; set;}
        public string AccommQuestionId { get; set; }
        public bool? Answer  { get; set;} 
        public string CertId { get; set; }
        public string Contact { get; set; }
        public string CopiedFrom  { get; set;} 
        public string CopiedFromEmployee  { get; set;} 
        public string EmployeeNumber { get; set; }
        public string EnteredByName { get; set; }
        public string Notes { get; set; }
        public string ProcessPath { get; set; }
        public bool Public { get; set; }
        public DateTime? RequestDate { get; set;} 
        public string RequestId { get; set; }
        public string SAnswer { get; set; }
        public string WorkRestrictionId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public int? Category { get; set; }
        public string RecordHash { get; set; }

        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }
       
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_case_note
                    ( case_id , employee_id , customer_id , employer_id	, accomm_id	, accomm_question_id	, answer , cert_id	
                    , contact	, copied_from	, copied_from_employee	, employee_number	, entered_by_name	, notes	, process_path	, public
                    , request_date	, request_id , s_answer	, work_restriction_id, category_code ,category_name, category, is_deleted , ext_ref_id , ext_created_date 
                    , ext_modified_date , ver_is_current , ver_seq , ver_is_expired , ver_root_id , ver_prev_id , ver_history_count , ext_created_by , ext_modified_by)
                    values
                    ( @case_id , @employee_id , @customer_id , @employer_id	, @accomm_id	, @accomm_question_id	, @answer , @cert_id	
                    , @contact	, @copied_from	, @copied_from_employee	, @employee_number	, @entered_by_name	, @notes	, @process_path	, @public
                    , @request_date	, @request_id , @s_answer	, @work_restriction_id, @category_code ,@category_name, @category, @is_deleted , @ext_ref_id , @ext_created_date 
                    , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired , @ver_root_id , @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by)
                    RETURNING dim_case_note_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomm_id", AccommId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accomm_question_id",AccommQuestionId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@answer",Answer.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cert_id",CertId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact",Contact.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@copied_from", CopiedFrom.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@copied_from_employee",CopiedFromEmployee.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number",EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@entered_by_name",EnteredByName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@notes",Notes.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@process_path",ProcessPath.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@request_date",RequestDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@request_id",RequestId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@s_answer",SAnswer.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_restriction_id",WorkRestrictionId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@category_code",CategoryCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@category_name", CategoryName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@category", Category.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@public", Public.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCaseNoteId = Convert.ToInt64(res);
            }
        }

    }
}
