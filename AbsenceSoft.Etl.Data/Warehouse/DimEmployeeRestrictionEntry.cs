﻿using Npgsql;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployeeRestrictionEntry : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeRestrictionEntry"/> class.
        /// </summary>
        public DimEmployeeRestrictionEntry() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployeeRestrictionEntry"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployeeRestrictionEntry(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.NONE; } }

        /// <summary>
        /// Gets or sets the dim employee restriction entry identifier.
        /// </summary>
        /// <value>
        /// The dim employee restriction entry identifier.
        /// </value>
        public long DimEmployeeRestrictionEntryId { get; set; }

        /// <summary>
        /// Gets or sets the dim employee restriction identifier.
        /// </summary>
        /// <value>
        /// The dim employee restriction identifier.
        /// </value>
        public long DimEmployeeRestrictionId { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the ext reference demand type identifier.
        /// </summary>
        /// <value>
        /// The ext reference demand type identifier.
        /// </value>
        public string ExtRefDemandTypeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the work restriction.
        /// </summary>
        /// <value>
        /// The type of the work restriction.
        /// </value>
        public string WorkRestrictionType { get; set; }

        /// <summary>
        /// Gets or sets the work restriction.
        /// </summary>
        /// <value>
        /// The work restriction.
        /// </value>
        public string WorkRestriction { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(sqlDelete);
        }
        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            sqlDelete(pgsqlConnection);
        }

        protected void sqlDelete(NpgsqlConnection pgsqlConnection)
        {
            if (DimEmployeeRestrictionId <= 0)
                return;

            string sqlCmd = "delete from dim_employee_restriction_entry where dim_employee_restriction_id = @dim_employee_restriction_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_employee_restriction_id", DimEmployeeRestrictionId));
                cmd.ExecuteNonQuery();
            }
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_employee_restriction_entry")
                .Append("(dim_employee_restriction_id, case_id, ext_ref_demand_type_id, wkrs_restriction_type, wkrs_restriction, is_deleted, ver_is_current")
                .Append(") values ")
                .Append("(@dim_employee_restriction_id,@case_id,@ext_ref_demand_type_id,@wkrs_restriction_type, @wkrs_restriction, @is_deleted, @ver_is_current)");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_employee_restriction_id", DimEmployeeRestrictionId));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_demand_type_id", ExtRefDemandTypeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wkrs_restriction_type", WorkRestrictionType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@wkrs_restriction", WorkRestriction.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", true));
                
                cmd.ExecuteNonQuery();
            }
        }
    }
}
