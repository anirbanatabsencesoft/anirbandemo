﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Relpase Dimension 
    /// </summary>
    public class DimRelapse : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimRelapse"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimRelapse(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimRelapse"/> class.
        /// </summary>
        public DimRelapse() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_RELAPSE; } }

        /// <summary>
        /// Gets or sets the dim relpase identifier.
        /// </summary>
        public long DimRelpaseId { get; set; }

        /// <summary>
        /// Employee id for relapse
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Employer id for relapse
        /// </summary>
        public string EmployerId { get; set; }

        /// <summary>
        /// Customer id for relapse
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Case id for relapse
        /// </summary>
        public string CaseId { get; set; }   
        
        // <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Pre-Relapse RTW Date
        /// </summary>
        public DateTime? PriorReturnToWorkDate { get; set; }

        /// <summary>
        /// Pre-Relapse RTW Days 
        /// </summary>
        public int? PriorReturnToWorkDays { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        /// <summary>
        /// Save the data
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// SQL insert command
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"INSERT INTO public.dim_relapse(
	                    ext_ref_id, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, is_deleted,
                        ver_root_id, ver_prev_id, ver_history_count, ext_created_by, ext_modified_by,
                        employee_id, customer_id, employer_id, case_id,
                        start_date, end_date, prior_rtw_date, prior_rtw_days)
	                    VALUES (@ext_ref_id, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @is_deleted, 
                        @ver_root_id, @ver_prev_id, @ver_history_count, @ext_created_by, @ext_modified_by, 
                        @employee_id, @customer_id, @employer_id, @case_id, 
                        @start_date, @end_date, @prior_return_to_work_date,@prior_return_to_work_days)
                        RETURNING dim_relapse_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));                
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@prior_return_to_work_date", PriorReturnToWorkDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@prior_return_to_work_days", PriorReturnToWorkDays.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimRelpaseId = Convert.ToInt64(res);
            }
        }
    }
}
