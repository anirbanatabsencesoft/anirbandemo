﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployerContact : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContact"/> class.
        /// </summary>
        public DimEmployerContact() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContact"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployerContact(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_CONTACT; } }

        /// <summary>
        /// Gets or sets the dim employeer contact identifier.
        /// </summary>
        /// <value>
        /// The dim employeer contact.
        /// </value>
        public long DimEmployerContactId { get; set; }

        /// <summary>
        /// Gets or sets the dim customer identifier.
        /// </summary>
        /// <value>
        /// The dim customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Employer Id
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the contact firstname.
        /// </summary>
        /// <value>
        /// The contact firstname
        /// </value>
        public string ContactFirstName { get; set; }

        /// <summary>
        /// Gets or sets the contact lastname.
        /// </summary>
        /// <value>
        /// The contact lastname.
        /// </value>
        public string ContactLastName { get; set; }

        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        /// <value>
        /// The contact email.
        /// </value>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the contact alternate email.
        /// </summary>
        /// <value>
        /// The contact alternate email.
        /// </value>
        public string ContactAltEmail { get; set; }

        /// <summary>
        /// Gets or sets the contact work phone.
        /// </summary>
        /// <value>
        /// The contact work phone.
        /// </value>
        public string ContactWorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the contact fax.
        /// </summary>
        /// <value>
        /// The contact fax.
        /// </value>
        public string ContactFax { get; set; }

        /// <summary>
        /// Gets or sets the contact address.
        /// </summary>
        /// <value>
        /// The contact address.
        /// </value>
        public string ContactAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the contact city.
        /// </summary>
        /// <value>
        /// The contact city.
        /// </value>
        public string ContactCity { get; set; }

        /// <summary>
        /// Gets or sets the contact state.
        /// </summary>
        /// <value>
        /// The contact state.
        /// </value>
        public string ContactState { get; set; }

        /// <summary>
        /// Gets or sets the contact postal code.
        /// </summary>
        /// <value>
        /// The contact postal code.
        /// </value>
        public string ContactPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the contact country.
        /// </summary>
        /// <value>
        /// The contact country.
        /// </value>
        public string ContactCountry { get; set; }

        /// <summary>
        /// Gets or sets the contact type code.
        /// </summary>
        /// <value>
        /// The contact type code.
        /// </value>
        public string ContactTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the contact type name.
        /// </summary>
        /// <value>
        /// The contact type name.
        /// </value>
        public string ContactTypeName { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sql = new StringBuilder()
                .Append("insert into dim_employer_contact")
                .Append("(customer_id, employer_id, is_deleted, contact_firstname, contact_lastname, contact_email, contact_alt_email, contact_work_phone, contact_fax, contact_address1, contact_city, contact_state, contact_postal_code, contact_country, contact_type_code, contact_type_name, dim_ver_id, ext_ref_id, ext_created_by, ext_modified_by, ext_created_date, ext_modified_date, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count")
                .Append(") values (")
                .Append("@customer_id, @employer_id, @is_deleted, @contact_firstname, @contact_lastname, @contact_email, @contact_alt_email, @contact_work_phone, @contact_fax, @contact_address1, @contact_city, @contact_state, @contact_postal_code, @contact_country, @contact_type_code, @contact_type_name, @dim_ver_id, @ext_ref_id, @ext_created_by, @ext_modified_by, @ext_created_date, @ext_modified_date, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count")
                .Append(") RETURNING dim_employer_contact_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_firstname", ContactFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_lastname", ContactLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_email", ContactEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_alt_email", ContactAltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_work_phone", ContactWorkPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_fax", ContactFax.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_address1", ContactAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_city", ContactCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_state", ContactState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_postal_code", ContactPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_country", ContactCountry.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_code", ContactTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_name", ContactTypeName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));

                object res = cmd.ExecuteScalar();
                DimEmployerContactId = Convert.ToInt64(res);
            }
        }
    }
}
