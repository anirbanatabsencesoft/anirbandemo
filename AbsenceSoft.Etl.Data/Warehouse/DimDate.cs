﻿using System;
using Npgsql;
using System.Text;
using AbsenceSoft.Etl.Common;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimDate : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimDate"/> class.
        /// </summary>
        public DimDate() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimDate"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimDate(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.NONE; } }

        /// <summary>
        /// Gets the dim date identifier.
        /// </summary>
        /// <value>
        /// The dim date identifier.
        /// </value>
        public long DimDateId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is leap year.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is leap year; otherwise, <c>false</c>.
        /// </value>
        public bool IsLeapYear { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is first day of week.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is first day of week; otherwise, <c>false</c>.
        /// </value>
        public bool IsFirstDayOfWeek { get; private set; }

        /// <summary>
        /// Gets the name of the day of week.
        /// </summary>
        /// <value>
        /// The name of the day of week.
        /// </value>
        public string DayOfWeekName { get; private set; }

        /// <summary>
        /// Gets the quarter.
        /// </summary>
        /// <value>
        /// The quarter.
        /// </value>
        public int Quarter { get; private set; }

        /// <summary>
        /// Gets or sets the iso8601 date.
        /// </summary>
        /// <value>
        /// The iso8601 date.
        /// </value>
        public DateTime Iso8601Date { get; set; }

        /// <summary>
        /// Gets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// Gets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public int Year { get; private set; }

        /// <summary>
        /// Gets the month.
        /// </summary>
        /// <value>
        /// The month.
        /// </value>
        public int Month { get; private set; }

        /// <summary>
        /// Gets the day.
        /// </summary>
        /// <value>
        /// The day.
        /// </value>
        public int Day { get; private set; }

        /// <summary>
        /// Gets the minute.
        /// </summary>
        /// <value>
        /// The minute.
        /// </value>
        public int Minute { get; private set; }

        /// <summary>
        /// Gets the second.
        /// </summary>
        /// <value>
        /// The second.
        /// </value>
        public int Second { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is weekday.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is weekday; otherwise, <c>false</c>.
        /// </value>
        public bool IsWeekday { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is weekend.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is weekend; otherwise, <c>false</c>.
        /// </value>
        public bool IsWeekend { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is american holiday.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is american holiday; otherwise, <c>false</c>.
        /// </value>
        public bool IsUSHoliday { get; private set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("insert into dim_date");
            sbSQL.Append(" (iso8601date, year, month, day, minute, second, is_weekend, is_weekday, is_american_holiday, quarter, day_of_week_name, is_leap_year, first_day_of_week, last_day_of_week, first_day_of_month, last_day_of_month, first_day_of_year, last_day_of_year, days_in_year, iso8601_week_of_year, closest_weekday)");
            sbSQL.Append(" values" );
            sbSQL.Append("(@iso8601date, @year, @month, @day, @minute, @second, @is_weekend, @is_weekday, @is_american_holiday, @quarter, @day_of_week_name, @is_leap_year, @first_day_of_week, @last_day_of_week, @first_day_of_month, @last_day_of_month, @first_day_of_year, @last_day_of_year, @days_in_year, @iso8601_week_of_year, @closest_weekday)");
            sbSQL.Append(" RETURNING dim_date_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sbSQL.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@iso8601date", Iso8601Date));
                cmd.Parameters.Add(new NpgsqlParameter("@year", Iso8601Date.Year));
                cmd.Parameters.Add(new NpgsqlParameter("@month", Iso8601Date.Month));
                cmd.Parameters.Add(new NpgsqlParameter("@day", Iso8601Date.Day));
                cmd.Parameters.Add(new NpgsqlParameter("@minute", Iso8601Date.Minute));
                cmd.Parameters.Add(new NpgsqlParameter("@second", Iso8601Date.Second));
                cmd.Parameters.Add(new NpgsqlParameter("@is_weekend", Iso8601Date.IsWeekend()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_weekday", Iso8601Date.IsWeekday()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_american_holiday", UtilsDate.isAmericanHoliday(Iso8601Date)));
                cmd.Parameters.Add(new NpgsqlParameter("@quarter", Iso8601Date.GetFiscalQuarter()));
                cmd.Parameters.Add(new NpgsqlParameter("@day_of_week_name", Iso8601Date.DayOfWeek.ToString()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_leap_year", Iso8601Date.IsLeapYear()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_day_of_week", Iso8601Date.GetFirstDayOfWeek()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_day_of_week", Iso8601Date.GetLastDayOfWeek()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_day_of_month", Iso8601Date.GetFirstDayOfMonth()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_day_of_month", Iso8601Date.GetLastDayOfMonth()));
                cmd.Parameters.Add(new NpgsqlParameter("@first_day_of_year", Iso8601Date.GetFirstDayOfYear()));
                cmd.Parameters.Add(new NpgsqlParameter("@last_day_of_year", Iso8601Date.GetLastDayOfYear()));
                cmd.Parameters.Add(new NpgsqlParameter("@days_in_year", Iso8601Date.DaysInYear()));
                cmd.Parameters.Add(new NpgsqlParameter("@iso8601_week_of_year", Iso8601Date.GetIso8601WeekOfYear()));
                cmd.Parameters.Add(new NpgsqlParameter("@closest_weekday", Iso8601Date.ClosestWeekday()));

                object res = cmd.ExecuteScalar();
                DimDateId = Convert.ToInt64(res);
            }
        }
    }
}
