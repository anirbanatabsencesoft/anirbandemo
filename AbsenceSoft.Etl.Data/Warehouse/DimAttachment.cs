﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimAttachment : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimAttachment"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimAttachment(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimAttachment"/> class.
        /// </summary>
        public DimAttachment() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ATTACHMENT; } }

        public long DimAttachmentId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        public string CaseId { get; set; }
        public long ContentLength { get; set; }
        public string ContentType { get; set; }
        public int AttachmentType { get; set; }
        public string CreatedByName { get; set; }
        public string Description { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string RecordHash { get; set; }
        public DateTime TimeSampleDate { get; set; }
        public int TimeTotalMinutes { get; set; }
        public string EmployeeNumber { get; set; }
        public int DimVariableScheduleTimeId { get; set; }
        public bool? Checked { get; set; }
        public bool Public { get; set; }
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_attachment
                    (employee_id , employer_id , customer_id , case_id  , content_length , content_type , attachment_type , created_by_name , description 
                    , file_id , file_Name , public ,checked, is_deleted , ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired 
                    , ver_root_id , ver_prev_id , ver_history_count , ext_created_by , ext_modified_by, time_sample_date, time_total_minutes, employee_number, dim_variable_schedule_time_id)
                    values
                    (@employee_id , @employer_id , @customer_id , @case_id  , @content_length , @content_type , @attachment_type , @created_by_name , @description 
                    , @file_id , @file_Name , @public ,@checked, @is_deleted , @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired 
                    , @ver_root_id , @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by, @time_sample_date, @time_total_minutes, @employee_number, @dim_variable_schedule_time_id )
                    RETURNING dim_attachment_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@content_length", ContentLength.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@content_type", ContentType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@attachment_type", AttachmentType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@created_by_name", CreatedByName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@description", Description.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@file_id", FileId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@file_Name", FileName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@checked", Checked.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@public", Public.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@time_sample_date", TimeSampleDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@time_total_minutes", TimeTotalMinutes.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_number", EmployeeNumber.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_variable_schedule_time_id", DimVariableScheduleTimeId.ValueOrDbNull()));
                
                object res = cmd.ExecuteScalar();
                DimAttachmentId = Convert.ToInt64(res);
            }
        }

    }
}
