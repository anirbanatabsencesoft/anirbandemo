﻿using Npgsql;
using System;
using System.Text;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimAccommodationUsage : DimBase
    {
        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.NONE; } }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the determination.
        /// </summary>
        /// <value>
        /// The determination.
        /// </value>
        public int Determination { get; set; }

        /// <summary>
        /// Gets or sets the dim accommodation identifier.
        /// </summary>
        /// <value>
        /// The dim accommodation identifier.
        /// </value>
        public long DimAccommodationId { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            Execute(sqlDelete);
        }
        public void Delete(NpgsqlConnection pgsqlConnection)
        {
            sqlDelete(pgsqlConnection);
        }

        /// <summary>
        /// Inserts this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_accommodation_usage");
            sqlCmd.Append("(dim_accommodation_id, ext_ref_id, start_date, end_date, determination, is_deleted");
            sqlCmd.Append(") values ");
            sqlCmd.Append("(@dim_accommodation_id, @ext_ref_id, @start_date, @end_date, @determination, @is_deleted)");
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_accommodation_id", DimAccommodationId));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination", Determination.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Deletes this record using the specified pgSQL connection.
        /// </summary>
        /// <param name="pgsqlConnection">The PGSQL connection.</param>
        protected void sqlDelete(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = "delete from dim_accommodation_usage where dim_accommodation_id = @dim_accommodation_id";
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@dim_accommodation_id", DimAccommodationId));
                cmd.ExecuteNonQuery();
            }
        }
    };
}
