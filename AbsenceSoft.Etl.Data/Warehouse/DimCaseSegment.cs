﻿using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;
using System.Text;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    /// <summary>
    /// Table: dim_case_segment 
    /// </summary>
    public class DimCaseSegment : DimBase
    {

        #region Constructor chain
        /// <summary>
        /// Constructor with the connection string
        /// </summary>
        /// <param name="connectionString">Connection string pf Postgres</param>
        public DimCaseSegment(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Default constructor for DimCaseSegment
        /// </summary>
        public DimCaseSegment() : base() { }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_SEGMENT; } }

        /// <summary>
        /// Maped to dim_case_segment_id
        /// </summary>
        public long DimCaseSegmentId { get; set; }

        /// <summary>
        /// Map to: ext_created_by 
        /// Derive from Case .
        /// Case Modified By If not new.
        /// </summary>
        [MaxLength(24)]
        public string ExternalCreatedBy { get; set; }

        /// <summary>
        /// Map to: ext_modified_by
        /// Derive from Case
        /// </summary>
        [MaxLength(24)]
        public string ExternalModifiedBy { get; set; }

        /// <summary>
        /// Map to case_id
        /// </summary>
        [MaxLength(24), Required]
        public string CaseId { get; set; }

        /// <summary>
        /// Map to: employee_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Map to: customer_id
        /// </summary>
        [MaxLength(24), Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// Map to: employer_id
        /// </summary>
        [MaxLength(24), Required]
        public string EmployerId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CaseType { get; set; }
        public int? CaseStatus { get; set; }

        /// <summary>
        /// Gets or sets the record hash. This value is used to compare versions of the policy
        /// and is a direct replacement for the absence of a valid modified date on the policy itself.
        /// </summary>
        /// <value>
        /// The record hash. Basically an MD5 hash checksum of the record JSON.
        /// </value>
        public string RecordHash { get; set; }

        private DimIntermittentTimeRequest _dimIntermittentTimeRequest;

        /// <summary>
        /// Returns the Intermittent TimeRequest
        /// </summary>
        public DimIntermittentTimeRequest CaseIntermittentTimeRequest
        {
            get
            {
                if (_dimIntermittentTimeRequest == null)
                    _dimIntermittentTimeRequest = new DimIntermittentTimeRequest();

                return _dimIntermittentTimeRequest;
            }
        }
        #endregion Public Properties

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Returns the SQL
        /// </summary>
        /// <returns></returns>
        private string GetSQLString()
        {
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.Append("INSERT INTO dim_case_segment (");

            //system fields
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");

            //dimension fields
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            sqlCmd.Append(",start_date");
            sqlCmd.Append(",end_date");
            sqlCmd.Append(",case_type");
            sqlCmd.Append(",case_status");
            sqlCmd.Append(",record_hash");

            sqlCmd.Append(") VALUES (");

            //system parameters
            sqlCmd.Append("@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count");

            //dimension parameters
            sqlCmd.Append(",@ext_created_by");
            sqlCmd.Append(",@ext_modified_by");
            sqlCmd.Append(",@case_id");
            sqlCmd.Append(",@employee_id");
            sqlCmd.Append(",@customer_id");
            sqlCmd.Append(",@employer_id");
            sqlCmd.Append(",@start_date");
            sqlCmd.Append(",@end_date");
            sqlCmd.Append(",@case_type");
            sqlCmd.Append(",@case_status");
            sqlCmd.Append(",@record_hash");

            sqlCmd.Append(") RETURNING dim_case_segment_id");

            return sqlCmd.ToString();
        }

        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(GetSQLString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", ExternalCreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ExternalModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", StartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", EndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_type", CaseType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_status", CaseStatus.ValueOrDbNull()));
                
                cmd.Parameters.Add(new NpgsqlParameter("@record_hash", RecordHash.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCaseSegmentId = Convert.ToInt64(res);
            }
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimCaseSegment> LoadHistory(string caseId)
        {
            return Execute(conn => sqlLoadHistory(conn, caseId));
        }


        /// <summary>
        /// Get the data from Postgres against a case id
        /// </summary>
        /// <param name="pgsqlConnection"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        protected List<DimCaseSegment> sqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimCaseSegment> lstCasePolicies = new List<DimCaseSegment>();

            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("SELECT ");
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count");
            sqlCmd.Append(",ext_created_by");
            sqlCmd.Append(",ext_modified_by");
            sqlCmd.Append(",case_id");
            sqlCmd.Append(",employee_id");
            sqlCmd.Append(",customer_id");
            sqlCmd.Append(",employer_id");
            
            sqlCmd.Append(",start_date");
            sqlCmd.Append(",end_date");
            sqlCmd.Append(",case_type");
            sqlCmd.Append(",case_status");
            
            sqlCmd.Append(",record_hash");
            
            sqlCmd.Append(" FROM dim_case_segment WHERE case_id = @case_id and ver_is_current = true");
            sqlCmd.Append(" ORDER BY ver_seq");

            using (NpgsqlCommand command = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                //add parameters to search
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));

                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        //instantiate
                        DimCaseSegment dimCP = new DimCaseSegment(ConnectionString);

                        dimCP.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.DimVerId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsCurrent = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionSequence = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionIsExpired = dr.GetValueOrDefault<bool>(i++);
                        dimCP.VersionRootId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionPreviousId = dr.GetValueOrDefault<long>(i++);
                        dimCP.VersionHistoryCount = dr.GetValueOrDefault<long>(i++);
                        
                        dimCP.ExternalCreatedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.ExternalModifiedBy = dr.GetValueOrDefault<string>(i++);
                        dimCP.CaseId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployeeId = dr.GetValueOrDefault<string>(i++);
                        dimCP.CustomerId = dr.GetValueOrDefault<string>(i++);
                        dimCP.EmployerId = dr.GetValueOrDefault<string>(i++);
                        
                        dimCP.StartDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.EndDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimCP.CaseType = dr.GetValueOrDefault<int>(i++);
                        dimCP.CaseStatus = dr.GetValueOrDefault<int>(i++);
                        
                        dimCP.RecordHash = dr.GetValueOrDefault<string>(i++);
                        
                        //add to list
                        lstCasePolicies.Add(dimCP);
                    }
                }
            }

            return lstCasePolicies;
        }
    }
}

