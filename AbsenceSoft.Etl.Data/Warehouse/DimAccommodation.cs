﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Text;
using NpgsqlTypes;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimAccommodation : DimBase
    {
        private DimAccommodationUsage dimAccommodationUsage;

        /// <summary>
        /// Initializes a new instance of the <see cref="DimAccommodation"/> class.
        /// </summary>
        public DimAccommodation() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimAccommodation"/> class.
        /// </summary>
        /// <param name="connectionString">The string connection string.</param>
        public DimAccommodation(string connectionString) : base(connectionString) { }
        
        /// <summary>
        /// Gets the accommodation usage.
        /// </summary>
        /// <value>
        /// The accommodation usage.
        /// </value>
        public DimAccommodationUsage AccommodationUsage
        {
            get
            {
                if (dimAccommodationUsage ==  null)
                {
                    dimAccommodationUsage = new DimAccommodationUsage();
                    return dimAccommodationUsage;
                }
                else
                {
                    return dimAccommodationUsage;
                }
            }
        }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_ACCOMMODATION; } }

        /// <summary>
        /// Gets or sets the dim accommodation identifier.
        /// </summary>
        /// <value>
        /// The dim accommodation identifier.
        /// </value>
        public long DimAccommodationId { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the resolution.
        /// </summary>
        /// <value>
        /// The resolution.
        /// </value>
        public string Resolution { get; set; }

        /// <summary>
        /// Gets or sets the resolved.
        /// </summary>
        /// <value>
        /// The resolved.
        /// </value>
        public bool Resolved { get; set; }

        /// <summary>
        /// Gets or sets the resolved date.
        /// </summary>
        /// <value>
        /// The resolved date.
        /// </value>
        public DateTime? ResolvedDate { get; set; }

        /// <summary>
        /// Gets or sets the granted.
        /// </summary>
        /// <value>
        /// The granted.
        /// </value>
        public bool Granted { get; set; }

        /// <summary>
        /// Gets or sets the granted date.
        /// </summary>
        /// <value>
        /// The granted date.
        /// </value>
        public DateTime? GrantedDate { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        /// <value>
        /// The cost.
        /// </value>
        public decimal? Cost { get; set; }

        /// <summary>
        /// Gets or sets the type of the accommodation.
        /// </summary>
        /// <value>
        /// The type of the accommodation.
        /// </value>
        public string AccommodationType { get; set; }

        /// <summary>
        /// Gets or sets the accommodation type code.
        /// </summary>
        /// <value>
        /// The accommodation type code.
        /// </value>
        public string AccommodationTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the cancel reason.
        /// </summary>
        /// <value>
        /// The cancel reason.
        /// </value>
        public int? CancelReason { get; set; }

        /// <summary>
        /// Gets or sets the cancel reason other desc.
        /// </summary>
        /// <value>
        /// The cancel reason other desc.
        /// </value>
        public string CancelReasonOtherDesc { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public int? Duration { get; set; }

        /// <summary>
        /// Gets or sets the is work related.
        /// </summary>
        /// <value>
        /// The is work related.
        /// </value>
        public bool? IsWorkRelated { get; set; }

        /// <summary>
        /// Gets or sets the determination.
        /// </summary>
        /// <value>
        /// The determination.
        /// </value>
        public int Determination { get; set; }

        /// <summary>
        /// Gets or sets the accom start date.
        /// </summary>
        /// <value>
        /// The accom start date.
        /// </value>
        public DateTime? AccomStartDate { get; set; }

        /// <summary>
        /// Gets or sets the accom end date.
        /// </summary>
        /// <value>
        /// The accom end date.
        /// </value>
        public DateTime? AccomEndDate { get; set; }

        /// <summary>
        /// Gets or sets the minimum approved date.
        /// </summary>
        /// <value>
        /// The minimum approved date.
        /// </value>
        public DateTime? MinApprovedDate { get; set; }

        /// <summary>
        /// Gets or sets the maximum approved date.
        /// </summary>
        /// <value>
        /// The maximum approved date.
        /// </value>
        public DateTime? MaxApprovedDate { get; set; }

        /// <summary>
        /// Gets or sets the record hash. This value is used to compare versions of the accommodation
        /// and is a direct replacement for the absence of a valid modified date on the accommodation itself.
        /// </summary>
        /// <value>
        /// The record hash. Basically an MD5 hash checksum of the record JSON.
        /// </value>
        public string RecordHash { get; set; }

        /// <summary>
        /// Gets or sets the denial reason code.
        /// </summary>
        /// <value>
        /// The denial reason code.
        /// </value>
        public string DenialReasonCode { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }

        /// <summary>
        /// Loads the history.
        /// </summary>
        /// <param name="caseId">The case identifier.</param>
        /// <returns></returns>
        public List<DimAccommodation> LoadHistory(string caseId)
        {
            return Execute(conn => sqlLoadHistory(conn, caseId));
        }
        
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("insert into dim_accommodation (");
            sqlCmd.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired");
            sqlCmd.Append(", ver_root_id, ver_prev_id, Ver_history_count, employee_id, employer_id, customer_id, case_id");
            sqlCmd.Append(", accommodation_type, accommodation_type_code, duration, resolution, resolved, resolved_date, granted, granted_date, cost");
            sqlCmd.Append(", start_date, end_date, determination, status, cancel_reason, cancel_reason_other_desc, denial_reason_code, work_related, is_deleted, record_hash, min_approved_date, max_approved_date");
            sqlCmd.Append(", ext_created_by, ext_modified_by");
            sqlCmd.Append(") values (");
            sqlCmd.Append("@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @Ver_seq, @ver_is_expired");
            sqlCmd.Append(", @ver_root_id, @ver_prev_id, @Ver_history_count, @employee_id, @employer_id, @customer_id, @case_id");
            sqlCmd.Append(", @accommodation_type, @accommodation_type_code, @duration, @resolution, @resolved, @resolved_date, @granted, @granted_date, @cost");
            sqlCmd.Append(", @start_date, @end_date, @determination, @status, @cancel_reason, @cancel_reason_other_desc, @denial_reason_code, @work_related, @is_deleted, @record_hash, @min_approved_date, @max_approved_date");
            sqlCmd.Append(", @ext_created_by, @ext_modified_by");
            sqlCmd.Append(") RETURNING dim_accommodation_id");

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd.ToString(), pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employer_id", EmployerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@case_id", CaseId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_type", AccommodationType.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@accommodation_type_code", AccommodationTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@duration", Duration.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@resolution", Resolution.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@resolved", Resolved));
                cmd.Parameters.Add(new NpgsqlParameter("@resolved_date", ResolvedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@granted", Granted));
                cmd.Parameters.Add(new NpgsqlParameter("@granted_date", GrantedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cost", Cost.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@start_date", AccomStartDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@end_date", AccomEndDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@determination", Determination));
                cmd.Parameters.Add(new NpgsqlParameter("@status", Status));
                cmd.Parameters.Add(new NpgsqlParameter("@cancel_reason", CancelReason.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@cancel_reason_other_desc", CancelReasonOtherDesc.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@denial_reason_code", DenialReasonCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@work_related", IsWorkRelated.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@record_hash", RecordHash.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@min_approved_date", MinApprovedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@max_approved_date", MaxApprovedDate.ValueOrDbNull()));

                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimAccommodationId = Convert.ToInt64(res);
            }
        }
        
        protected List<DimAccommodation> sqlLoadHistory(NpgsqlConnection pgsqlConnection, string caseId)
        {
            List<DimAccommodation> lstAccommodation = new List<DimAccommodation>();

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired");
            sql.Append(", ver_root_id, ver_prev_id, Ver_history_count, employee_id, employer_id, customer_id, case_id");
            sql.Append(", accommodation_type, accommodation_type_code, duration, resolution, resolved, resolved_date, granted, granted_date, cost");
            sql.Append(", start_date, end_date, determination, status, cancel_reason, work_related, is_deleted, cancel_reason_other_desc, denial_reason_code, dim_accommodation_id, record_hash, min_approved_date, max_approved_date");
            sql.Append(", ext_created_by, ext_modified_by");
            sql.Append(" FROM dim_accommodation");
            sql.Append(" WHERE case_id = @case_id and ver_is_current = true ");
            sql.Append(" ORDER BY ver_seq");

            using (NpgsqlCommand command = new NpgsqlCommand(sql.ToString(), pgsqlConnection))
            {
                command.Parameters.Add(new NpgsqlParameter("@case_id", caseId));
                using (NpgsqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var i = 0;
                        DimAccommodation dimAccom = new DimAccommodation(ConnectionString);
                        dimAccom.ExternalReferenceId = dr.GetValueOrDefault<string>(i++);
                        dimAccom.ExternalCreatedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimAccom.ExternalModifiedDate = dr.GetValueOrDefault<DateTime>(i++);
                        dimAccom.DimVerId = dr.GetValueOrDefault<long>(i++);
                        dimAccom.VersionIsCurrent = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.VersionSequence = dr.GetValueOrDefault<long>(i++);
                        dimAccom.VersionIsExpired = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.VersionRootId = dr.GetValueOrDefault<long>(i++);
                        dimAccom.VersionPreviousId = dr.GetValueOrDefault<long>(i++);
                        dimAccom.VersionHistoryCount = dr.GetValueOrDefault<long>(i++);
                        dimAccom.EmployeeId = dr.GetValueOrDefault<string>(i++);
                        dimAccom.EmployerId = dr.GetValueOrDefault<string>(i++);
                        dimAccom.CustomerId = dr.GetValueOrDefault<string>(i++);
                        dimAccom.CaseId = dr.GetValueOrDefault<string>(i++);
                        dimAccom.AccommodationType = dr.GetValueOrDefault<string>(i++);
                        dimAccom.AccommodationTypeCode = dr.GetValueOrDefault<string>(i++);
                        dimAccom.Duration = dr.GetValueOrNull<int>(i++);
                        dimAccom.Resolution = dr.GetValueOrDefault<string>(i++);
                        dimAccom.Resolved = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.ResolvedDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.Granted = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.GrantedDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.Cost = dr.GetValueOrNull<decimal>(i++);
                        dimAccom.AccomStartDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.AccomEndDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.Determination = dr.GetValueOrDefault<int>(i++);
                        dimAccom.Status = dr.GetValueOrDefault<int>(i++);
                        dimAccom.CancelReason = dr.GetValueOrNull<int>(i++);
                        dimAccom.IsWorkRelated = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.IsDeleted = dr.GetValueOrDefault<bool>(i++);
                        dimAccom.CancelReasonOtherDesc = dr.GetValueOrDefault<string>(i++);
                        dimAccom.DenialReasonCode = dr.GetValueOrDefault<string>(i++);
                        dimAccom.DimAccommodationId = dr.GetValueOrDefault<long>(i++);
                        dimAccom.RecordHash = dr.GetValueOrDefault<string>(i++);
                        dimAccom.MinApprovedDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.MaxApprovedDate = dr.GetValueOrNull<DateTime>(i++);
                        dimAccom.CreatedBy = dr.GetValueOrDefault<string>(i++);
                        dimAccom.ModifiedBy = dr.GetValueOrDefault<string>(i++);
                        lstAccommodation.Add(dimAccom);
                    }
                }
            }

            return lstAccommodation;
        }
    }
}

