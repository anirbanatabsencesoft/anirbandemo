﻿using Npgsql;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimEmployerContactType : DimBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContactType"/> class.
        /// </summary>
        public DimEmployerContactType() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimEmployerContactType"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimEmployerContactType(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMEMSION_EMPLOYER_CONTACT_TYPES; } }

        /// <summary>
        /// Gets or sets the dim employee contact type identifier.
        /// </summary>
        /// <value>
        /// The dim employee contact type identifier.
        /// </value>
        public long DimEmployerContactTypeId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Saves this instance.
        /// </summary>
        public override void Save()
        {
            Execute(SqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            SqlInsert(pgsqlConnection);
        }

        protected void SqlInsert(NpgsqlConnection conn)
        {
            string sqlCmd = string.Empty;
            sqlCmd += "insert into dim_employer_contact_types (";
            sqlCmd += "ext_ref_id, ext_created_date, ext_modified_date, dim_ver_id, ver_is_current, ver_seq, ver_is_expired, ver_root_id, ver_prev_id, ver_history_count ";
            sqlCmd += ", ext_created_by, ext_modified_by, is_deleted, customer_id,  name, code";
            sqlCmd += ") values (";
            sqlCmd += "@ext_ref_id, @ext_created_date, @ext_modified_date, @dim_ver_id, @ver_is_current, @ver_seq, @ver_is_expired, @ver_root_id, @ver_prev_id, @ver_history_count";
            sqlCmd += ", @ext_created_by, @ext_modified_by, @is_deleted, @customer_id,  @name, @code";
            sqlCmd += " ) RETURNING dim_employer_contact_types_id";

            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, conn))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@dim_ver_id", DimVerId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@name", Name.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@code", Code.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimEmployerContactTypeId = Convert.ToInt64(res);
            }
        }
    }
}
