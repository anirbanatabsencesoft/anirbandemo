﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AbsenceSoft.Etl.Data.Warehouse
{
    public class DimCaseAuthorizedSubmitter : DimBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DimCaseAuthorizedSubmitter(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimCaseAuthorizedSubmitter"/> class.
        /// </summary>
        public DimCaseAuthorizedSubmitter() : base() { }

        /// <summary>
        /// Gets the dimension identifier.
        /// </summary>
        /// <value>
        /// The dimension identifier.
        /// </value>
        public override int DimensionId { get { return Const.Dimensions.DIMENSION_CASE_AUTHORIZED_SUBMITTER; } }
        public long DimCaseAuthorizedSubmitterId { get; set; }
        public string AuthorizeSubmitterId { get; set; }
        public string EmployeeId { get; set; }
        public string CustomerId { get; set; }
        public string ContactTypeCode { get; set; }        
        public string ContactTypeName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactAltEmail { get; set; }
        public string ContactWorkPhone { get; set; }
        public string ContactHomePhone { get; set; }
        public string ContactCellPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactStreet { get; set; }
        public string ContactAddress1 { get; set; }
        public string ContactAddress2 { get; set; }
        public string ContactCity { get; set; }
        public string ContactState { get; set; }
        public string ContactPostalCode { get; set; }
        public string ContactCountry { get; set; }
        public string RecordHash { get; set; }
        public override void Save()
        {
            Execute(sqlInsert);
        }

        public override void Save(NpgsqlConnection pgsqlConnection)
        {
            sqlInsert(pgsqlConnection);
        }
       
        protected void sqlInsert(NpgsqlConnection pgsqlConnection)
        {
            string sqlCmd = string.Empty;

            sqlCmd = @"insert into dim_case_authorized_submitter
                    (authorized_submitter_id , employee_id , customer_id , contact_type_code , contact_type_name ,contact_first_name , contact_last_name ,contact_email,
                    contact_alt_email , contact_work_phone , contact_home_phone , contact_cell_phone , contact_fax , contact_street , contact_address1 , contact_address2 , 
                    contact_city , contact_state , contact_postal_code , contact_country ,	is_deleted ,
                    ext_ref_id , ext_created_date , ext_modified_date , ver_is_current , ver_seq , ver_is_expired , ver_root_id , 
                    ver_prev_id , ver_history_count , ext_created_by , ext_modified_by)
                    values
                    (@authorized_submitter_id , @employee_id , @customer_id , @contact_type_code , @contact_type_name ,@contact_first_name , @contact_last_name , @contact_email,
                    @contact_alt_email , @contact_work_phone , @contact_home_phone , @contact_cell_phone , @contact_fax , @contact_street , @contact_address1 , @contact_address2,
                    @contact_city , @contact_state , @contact_postal_code , @contact_country ,@is_deleted ,
                    @ext_ref_id , @ext_created_date , @ext_modified_date , @ver_is_current , @ver_seq , @ver_is_expired , @ver_root_id ,
                    @ver_prev_id , @ver_history_count , @ext_created_by , @ext_modified_by)
                    RETURNING dim_case_authorized_submitter_id";
            
            using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCmd, pgsqlConnection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("@ext_ref_id", ExternalReferenceId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@authorized_submitter_id", AuthorizeSubmitterId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@employee_id", EmployeeId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@customer_id", CustomerId.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_code", ContactTypeCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_type_name", ContactTypeName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_first_name", ContactFirstName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_last_name", ContactLastName.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_email", ContactEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_alt_email", ContactAltEmail.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_work_phone", ContactWorkPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_home_phone", ContactHomePhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_cell_phone", ContactCellPhone.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_fax", ContactFax.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_street ", ContactStreet.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_address1", ContactAddress1.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_address2", ContactAddress2.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_city", ContactCity.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_state", ContactState.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_postal_code", ContactPostalCode.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@contact_country", ContactCountry.ValueOrDbNull()));                
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_current", VersionIsCurrent));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_seq", VersionSequence));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_is_expired", VersionIsExpired));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_root_id", VersionRootId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_prev_id", VersionPreviousId));
                cmd.Parameters.Add(new NpgsqlParameter("@ver_history_count", VersionHistoryCount));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_date", ExternalCreatedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_date", ExternalModifiedDate.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@is_deleted", IsDeleted));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_created_by", CreatedBy.ValueOrDbNull()));
                cmd.Parameters.Add(new NpgsqlParameter("@ext_modified_by", ModifiedBy.ValueOrDbNull()));

                object res = cmd.ExecuteScalar();
                DimCaseAuthorizedSubmitterId = Convert.ToInt64(res);
            }
        }
    }
}
