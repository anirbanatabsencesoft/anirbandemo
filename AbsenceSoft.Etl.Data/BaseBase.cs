﻿using Npgsql;
using System;

namespace AbsenceSoft.Etl.Data
{
    public abstract class BaseBase
    {
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the stack trace.
        /// </summary>
        /// <value>
        /// The stack trace.
        /// </value>
        public string StackTrace { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the delete flag.
        /// </summary>
        /// <value>
        /// The delete flag.
        /// </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }

        /// <summary>
        /// Free the instance variables of this object.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        /// <summary>
        /// Throws the exception.
        /// </summary>
        /// <exception cref="System.Exception">{ + ErrorMessage + }{ + StackTrace + }</exception>
        public void ThrowException()
        {
            throw new Exception("{" + ErrorMessage + "}{" + StackTrace + "}");
        }
        
        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        protected string sqlExecuteString(NpgsqlConnection conn, string sql)
        {
            string val = null;
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, conn))
            {
                object res = cmd.ExecuteScalar();
                if (res != null && !res.Equals(DBNull.Value))
                    val = Convert.ToString(res);
            }
            return val;
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar nullable value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        protected T? sqlExecuteScalar<T>(NpgsqlConnection conn, string sql) where T : struct
        {
            T? val = null;
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, conn))
            {
                object res = cmd.ExecuteScalar();
                if (res != null && !res.Equals(DBNull.Value))
                    val = (T)Convert.ChangeType(res, typeof(T));
            }
            return val;
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        protected DateTime? sqlExecuteDate(NpgsqlConnection conn, string sql)
        {
            return conn.ExecuteDate(sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        protected int? sqlExecuteInt(NpgsqlConnection conn, string sql)
        {
            return conn.ExecuteInt(sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to fetch a scalar value from the database.
        /// </summary>
        /// <param name="conn">The PGSQL connection.</param>
        /// <param name="sql">The SQL to execute scalar.</param>
        /// <returns>The scalar value OR <c>null</c>.</returns>
        protected long? sqlExecuteLong(NpgsqlConnection conn, string sql)
        {
            return conn.ExecuteLong(sql);
        }

        /// <summary>
        /// Executes the SQL against the pgSQL connection provided to see if any rows come back.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <returns><c>true</c>if any rows are returned; otherwise <c>false</c>.</returns>
        protected bool sqlExecuteExist(NpgsqlConnection conn, string sql)
        {
            return conn.ExecuteExist(sql);
        }


        /// <summary>
        /// Executes the specified action.
        /// </summary>
        /// <param name="action">The action.</param>
        protected void Execute(Action<NpgsqlConnection> action)
        {
            if (action == null)
                return;

            try
            {
                using (var pgsqlConn = new NpgsqlConnection(ConnectionString))
                {

                        pgsqlConn.Open();
                        action(pgsqlConn);

                }
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }
        }

        /// <summary>
        /// Executes the specified function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="function">The function.</param>
        /// <returns></returns>
        protected T Execute<T>(Func<NpgsqlConnection, T> function)
        {
            T val = default(T);
            if (function == null)
                return val;

            try
            {
                using (var pgsqlConn = new NpgsqlConnection(ConnectionString))
                {
                    pgsqlConn.Open();
                    val = function(pgsqlConn);
                }
            }
            catch (Exception e)
            {
                HasError = true;
                ErrorMessage = e.Message;
                StackTrace = e.ToString();
            }

            return val;
        }
    }
}
