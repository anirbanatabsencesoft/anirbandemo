﻿using AbsenceSoft.Data;
using System;
using System.Configuration;
using System.Linq;

namespace AbsenceSoft.Etl.Remote.Tasker
{
    public static class LocalConfig
    {
        private const string KEY_SYNC_CONNECTION_STRING = "SynchronizerSettings";
        private const string KEY_DATA_CONNECTION_STRING = "DataWarehouseSettings";

        /// <summary>
        /// Gets the synchronization code.
        /// </summary>
        /// <value>
        /// The synchronization code.
        /// </value>
        public static string SynchronizationCode { get { return Properties.Settings.Default.SynchronizationCode; } }

        /// <summary>
        /// Gets the data warehouse synchronize connection string.
        /// </summary>
        /// <value>
        /// The data warehouse synchronize connection string.
        /// </value>
        public static string DataWarehouseSyncConnString { get; private set; }

        /// <summary>
        /// Gets the data warehouse data connection string.
        /// </summary>
        /// <value>
        /// The data warehouse data connection string.
        /// </value>
        public static string DataWarehouseDataConnString { get; private set; }

        /// <summary>
        /// Gets the check interval in ms.
        /// </summary>
        /// <value>
        /// The check interval in ms.
        /// </value>
        public static int CheckIntervalInMs { get { return Properties.Settings.Default.CheckIntervalInMs; } }

        /// <summary>
        /// Gets the heartbeat interval in ms.
        /// </summary>
        /// <value>
        /// The heartbeat interval in ms.
        /// </value>
        public static int HeartbeatIntervalInMs { get { return Properties.Settings.Default.HeartbeatIntervalInMs; } }

        /// <summary>
        /// Gets the failure retry cool off period.
        /// </summary>
        /// <value>
        /// The failure retry cool off period.
        /// </value>
        public static TimeSpan FailureRetryCoolOffPeriod {  get { return Properties.Settings.Default.FailureRetryCoolOffPeriod; } }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public static string ServiceName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is local debug.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is local debug; otherwise, <c>false</c>.
        /// </value>
        public static bool IsLocalDebug { get; private set; }

        /// <summary>
        /// Initializes the <see cref="LocalConfig"/> class.
        /// </summary>
        static LocalConfig()
        {
            // Allow  to throw exception if connection string is missing.
            DataWarehouseSyncConnString = ConfigurationManager.ConnectionStrings[KEY_SYNC_CONNECTION_STRING].ToString();
            DataWarehouseDataConnString = ConfigurationManager.ConnectionStrings[KEY_DATA_CONNECTION_STRING].ToString();
            ServiceName = CryptoString.GetHash(Environment.CommandLine).Hashed;

            var args = Environment.GetCommandLineArgs();
            if (args.Length > 0)
            {
                IsLocalDebug = args.Contains("DEBUG-LOCAL");
                for (var i = 0; i < args.Length; i++)
                {
                    string a = args[i];
                    if (a == "-ServiceName")
                    {
                        var sn = args.Length > i + 1 ? args[i + 1] : null;
                        ServiceName = string.IsNullOrWhiteSpace(sn) ? ServiceName : sn;
                    }
                }
            }
        }
    }
}
