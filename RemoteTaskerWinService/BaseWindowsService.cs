﻿using System.ServiceProcess;
using System.Threading;

namespace AbsenceSoft.Etl.Remote.Tasker
{
    public abstract class BaseWindowsService : ServiceBase
    {
        private volatile bool _stop = false;
        private static Thread _thread = null;

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) 
        /// or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            _stop = false;
            _thread = new Thread(new ThreadStart(StartWork));
            _thread.Start();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). 
        /// Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            _stop = true;
            try
            {
                _thread.Abort();
                _thread.Join();
            }
            catch { }
        }

        /// <summary>
        /// Starts the work.
        /// </summary>
        public abstract void StartWork();

        /// <summary>
        /// Determines whether this instance can work.
        /// </summary>
        /// <returns></returns>
        protected bool CanWork()
        {
            return !_stop;
        }
    }
}
