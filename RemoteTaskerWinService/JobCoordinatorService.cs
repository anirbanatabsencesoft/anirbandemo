﻿using AbsenceSoft.Etl.Data;
using AbsenceSoft.Etl.Data.Sync;
using System;
using System.Linq;
using System.Timers;

namespace AbsenceSoft.Etl.Remote.Tasker
{
    partial class JobCoordinatorService : BaseWindowsService
    {
        private Timer checkForWorkTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobCoordinatorService"/> class.
        /// </summary>
        public JobCoordinatorService()
        {
            InitializeComponent();
            checkForWorkTimer = new Timer(3000L) { AutoReset = true, Site = Site };
            checkForWorkTimer.AutoReset = true;
            checkForWorkTimer.Elapsed += CheckForWorkTimer_Elapsed;
        }

        private bool blocking = false;
        /// <summary>
        /// Sets the blocking bit on this instance and returns the same instance with the bit set to <c>true</c>.
        /// </summary>
        /// <returns>The current instance as blocking.</returns>
        public JobCoordinatorService AsBlocking()
        {
            blocking = true;
            return this;
        }

        /// <summary>
        /// Handles the Elapsed event of the CheckForWorkTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void CheckForWorkTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckForWork();
        }

        /// <summary>
        /// Starts the work.
        /// </summary>
        public override void StartWork()
        {
            if (blocking)
                while (CanWork())
                {
                    CheckForWork();
                    System.Threading.Thread.Sleep(LocalConfig.CheckIntervalInMs);
                }
            else if (CanWork())
                checkForWorkTimer.Enabled = true;
        }

        /// <summary>
        /// Checks for work.
        /// </summary>
        private void CheckForWork()
        {
            checkForWorkTimer.Enabled = false;
            if (!CanWork())
                return;

            LogInfo("CheckForWork", "Loading jobs");
            var jobLoader = new Job(LocalConfig.DataWarehouseSyncConnString);
            var jobs = jobLoader.GetNextJobRun();
            if (jobLoader.HasError)
            {
                LogError(jobLoader);
                Environment.Exit(9);
            }
            LogInfo("CheckForWork", "Found {0} jobs to process", jobs.Count);

            jobs.AsParallel()
                .WithDegreeOfParallelism(LocalConfig.IsLocalDebug ? 1 : Math.Min(Environment.ProcessorCount, 512))
                .ForAll(job =>
                {
                    if (!CanWork())
                        return;
                    LogInfo("FOREACH-JOB", "Processing job: {0}", job);
                    var history = job.Execute();
                    LogInfo("FOREACH-JOB", "Completed job with: {0}", history);
                });

            LogInfo("CheckForWork", "Completed {0} jobs", jobs.Count);
            if (!blocking)
                StartWork();
        }


        /// <summary>
        /// Logs the information.
        /// </summary>
        /// <param name="action">The MSG action.</param>
        /// <param name="message">The MSG text.</param>
        /// <param name="formatArgs">The format arguments.</param>
        protected void LogInfo(string action, string message, params object[] formatArgs)
        {
            try
            {
                new SyncLogInfo(LocalConfig.DataWarehouseSyncConnString)
                {
                    MsgSource = "JOB_COORDINATOR",
                    MsgAction = action,
                    MsgText = formatArgs.Length > 0 ? string.Format(message, formatArgs) : message,
                    DimensionId = -1
                }.SaveSyncLogInfo();
            }
            catch { }
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="instance">The instance.</param>
        protected virtual void LogError(BaseBase instance)
        {
            if (instance.HasError)
                LogError(instance.ErrorMessage, instance.StackTrace);
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="stackTrace">The stack trace.</param>
        protected virtual void LogError(string message, string stackTrace)
        {
            new SyncLogError(LocalConfig.DataWarehouseSyncConnString)
            {
                DimensionId = -1,
                MsgSource = "JOB_COORDINATOR",
                ErrorMessage = message,
                StackTrace = stackTrace
            }.SaveSyncLogError();
        }
    }
}
