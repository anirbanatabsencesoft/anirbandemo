﻿using System;
using System.Threading;
using AbsenceSoft.Etl.Lib;
using AbsenceSoft.Etl.Data.Sync;

namespace AbsenceSoft.Etl.Remote.Tasker
{
    public partial class DataSyncronizationService : BaseWindowsService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataSyncronizationService"/> class.
        /// </summary>
        public DataSyncronizationService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Starts the work.
        /// </summary>
        public override void StartWork()
        {
            while (CanWork())
            {
                RemoteTasker remoteTasker = new RemoteTasker();
                long syncManagerId = remoteTasker.GetSyncManagerId(LocalConfig.SynchronizationCode) ?? 0;

                TaskClient taskClient = new TaskClient()
                {
                    InitRegisterDate = DateTime.UtcNow,
                    ProcessId = LocalConfig.SynchronizationCode,
                    ConnString = LocalConfig.DataWarehouseSyncConnString,
                    HeartbeatIntervalInMs = LocalConfig.HeartbeatIntervalInMs,
                    CheckIntervalInMs = LocalConfig.CheckIntervalInMs,
                    ProcessName = LocalConfig.ServiceName,
                    SyncManagerId = syncManagerId,
                    FailureRetryCoolOffPeriod = LocalConfig.FailureRetryCoolOffPeriod
                };
                remoteTasker.Register(taskClient);

                if (!remoteTasker.HasError)
                {
                    remoteTasker.LoadTaskClient(taskClient);
                    if ((taskClient != null) && (taskClient.SyncManagerId != 0) && (!taskClient.IsDisabled))
                    {
                        // make sync request to remote tasker for this client
                        remoteTasker.RequestSyncTask(taskClient);
                    }
                    else
                    {
                        // task client not assigned sync dimension
                        SyncLogInfo logInfo = new SyncLogInfo(LocalConfig.DataWarehouseSyncConnString)
                        {
                            DimensionId = remoteTasker.DimensionId,
                            MsgSource = LocalConfig.SynchronizationCode,
                            MsgAction = "StartWork",
                            MsgText = "Task client not assigned sync dimension, syncManagerId=" + syncManagerId.ToString()
                        };
                        logInfo.SaveSyncLogInfo();
                        if (logInfo.HasError)
                            logInfo.ThrowException();
                    }
                }
                else
                {
                    // this service has trouble registering.  log
                    SyncLogError logError = new SyncLogError(LocalConfig.DataWarehouseSyncConnString)
                    {
                        DimensionId = remoteTasker.DimensionId,
                        MsgSource = "{processname=" + taskClient.ProcessName + ",processid=" + taskClient.ProcessId + "}" + "{" + "startWork}",
                        ErrorMessage = "Remote Tasker Error"
                    };
                    logError.SaveSyncLogError();
                    if (logError.HasError)
                        logError.ThrowException();
                }

                

                if (LocalConfig.CheckIntervalInMs > 0)
                    Thread.Sleep(1000);
            }
        }
    }
}
