﻿using AbsenceSoft.Etl.Data.Sync;
using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace AbsenceSoft.Etl.Remote.Tasker
{
    static class Program
    {
        const string JOB_COORDINATOR = "JOB_COORDINATOR";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // Error Handling
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            // Check it out, just right-click on your project in Solution Explorer
            //  then go to Properties page
            //  go to the Debug tab
            //  in the Command line arguments box, paste DEBUG-LOCAL
            //  save, it will not modify the project but save debug settings locally on your box
            //  now debug and enjoy!
            if (args != null && args.Length > 0 && args.Contains("DEBUG-LOCAL"))
            {
                if (LocalConfig.SynchronizationCode == JOB_COORDINATOR)
                    new JobCoordinatorService().AsBlocking().StartWork();
                else
                    new DataSyncronizationService().StartWork();
                return;
            }

            var services = new ServiceBase[1];
            if (LocalConfig.SynchronizationCode == JOB_COORDINATOR)
                services[0] = new JobCoordinatorService();
            else
                services[0] = new DataSyncronizationService();

            ServiceBase.Run(services);
        }

        /// <summary>
        /// Handles the ThreadException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ThreadExceptionEventArgs"/> instance containing the event data.</param>
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        /// <summary>
        /// Handles the UnhandledException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException((Exception)e.ExceptionObject);
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="e">The e.</param>
        static void HandleException(Exception e)
        {
            if (e is ThreadAbortException)
                return;

            //Handle the Exception here
            new SyncLogError(LocalConfig.DataWarehouseSyncConnString)
            {
                DimensionId = -1,
                MsgSource = LocalConfig.SynchronizationCode,
                ErrorMessage = "Unhandled Exception",
                StackTrace = e.ToString()
            }.SaveSyncLogError();
        }
    }
}
