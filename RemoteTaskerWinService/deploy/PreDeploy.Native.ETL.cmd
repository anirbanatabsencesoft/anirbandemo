set env=%1
set sn=%2

if not exist "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish" mkdir "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish"

echo Checking for Service '%sn%'
sc query RemoteTaskerWinService_%env%_%sn% | find "RemoteTaskerWinService_%env%_%sn%" >nul
if %errorlevel%==0 goto ServiceFound
if %errorlevel%==1 goto ServiceMissing

:ServiceMissing
echo Service '%sn%' not found, skipping step
goto end

:ServiceFound
echo Service '%sn%' found, stopping service
net stop RemoteTaskerWinService_%env%_%sn%
echo Service '%sn%' stopped
goto end

:end
echo All Done
exit /b 0