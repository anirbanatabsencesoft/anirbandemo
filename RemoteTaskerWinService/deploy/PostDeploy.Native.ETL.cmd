﻿set env=%1
set sn=%2
set SynchronizationCode=%3

if not exist "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%" mkdir "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%"
if not exist "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish" mkdir "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish"
if not exist "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\%sn%" mkdir "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\%sn%"

echo Copying Binaries for Service '%sn%'
copy /Y "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish\*.*" "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\%sn%\" 

echo Altering Sync Code in config file to '%SynchronizationCode%'
"C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\Publish\deploy\fart" "C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\%sn%\RemoteTaskerWinService.exe.config" "$SYNCHRONIZATIONCODE$" "%SynchronizationCode%"

echo Checking for Service '%sn%'
sc query RemoteTaskerWinService_%env%_%sn%  |find "RemoteTaskerWinService_%env%_%sn%" >nul
if %errorlevel%==0 goto ServiceFound
if %errorlevel%==1 goto ServiceMissing

:ServiceMissing
echo Service '%sn%' not found, installing service
sc create RemoteTaskerWinService_%env%_%sn% binPath= "\"C:\AbsenceTrackerServices\RemoteTaskerWinService_%env%\%sn%\RemoteTaskerWinService.exe\" -ServiceName \"%sn%\"" start= auto DisplayName= "AbsenceTracker - ETL %sn% (%env%)"
if %errorlevel%==0 goto ServiceFound
goto end

:ServiceFound
echo Service '%sn%' found, starting service
net start RemoteTaskerWinService_%env%_%sn%
echo Service '%sn%' started
goto end

:end
echo All Done
exit /b 0