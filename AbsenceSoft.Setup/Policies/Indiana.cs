﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Indiana : PolicyBase
    {
        public override void BuildPolicies()
        {
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 1
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1500hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = IL
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  10 work days per occurence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Family see Matrix
            //      Min Length of Military Duty = 89 Days Calendar
            //      Active Military Duty = 
            //      Search & Rescue = 
            //      National Guard = x
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("IN-MIL", "Indiana Family Military Leave", PolicyType.StateFML, "IN");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    WorkState = "IN",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,  
                    PeriodType = PeriodType.CalendarYear,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "PARENT" , "STEPPARENT" , "FOSTERPARENT", "GRANDPARENT" , "CHILD"
                        , "STEPCHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD" , "GRANDCHILD" , "SIBLING" ),

                        EligibilityRuleGroup("Indiana Family Military Leave", MinLengthOfServiceRule(12, Unit.Months), WorkedHoursInLast12MonthsRule(1500), MinLengthOfServiceRule(89, Unit.Days))
                       

                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50));
            

            return SavePolicy(policy);
        }

    }
}
