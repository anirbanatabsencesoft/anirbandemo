﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class FMLAFC : PolicyBase
    {
        public override void BuildPolicies()
        {
            BuildPolicy();
        }

        public static Policy BuildPolicy()
        {
            Policy policy = StagePolicy("FMLA-FC", "Family Medical Leave Act for Airline Flight Crew", PolicyType.FMLA);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.FamilyHealthCondition,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "STEPPARENT", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "SAMESEXSPOUSE","INLOCOPARENTIS"),
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Gender = Gender.Female,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        BondingPeriodRuleGroup(12, "Delivery / Expected Delivery Date", "NumberOfMonthsFromDeliveryDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                            EventType = CaseEventType.DeliveryDate,
                            DateType = EventDateType.EndPolicyBasedOnPeriod,
                            EventPeriod = 12
                        }
                    }
                },
                // Bonding
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD"),
                        BondingPeriodRuleGroup(12, "Delivery / Expected Delivery Date", "NumberOfMonthsFromDeliveryDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                            EventType = CaseEventType.DeliveryDate,
                            DateType = EventDateType.EndPolicyBasedOnPeriod,
                            EventPeriod = 12
                        }
                    }
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18),
                        BondingPeriodRuleGroup(12, "Adoption / Foster Care Start Date", "NumberOfMonthsFromAdoptionDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                            EventType = CaseEventType.AdoptionDate,
                            DateType = EventDateType.EndPolicyBasedOnPeriod,
                            EventPeriod = 12
                        }
                    }
                },
                // Covered Service Member
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 156,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    Reason = Reasons.CoveredServiceMember,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup("Service Member", "SPOUSE", "PARENT", "CHILD","NEXTOFKIN"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty, MilitaryStatus.Veteran)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays
                },
                // Qualifying Exigency (max 15 work days per occurrance, max 12 weeks total)
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    PerUseCap = 15,
                    PerUseCapType = EntitlementType.FixedWorkDays,
                    Reason = Reasons.QualifyingExigency,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays
                },
                // Ceremony / Send Off 
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.FixedWorkDays,
                    Entitlement = 72,
                    Period = 12,
                    FixedWorkDaysPerWeek = 6,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.Ceremonial,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.FixedWorkDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                 FMLEligibilityRuleGroup(504, false, true, "FMLA-FC"),
            };
            policy.AirlineFlightCrew = true;
            return SavePolicy(policy);
        }
    }
}
