﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Massachusetts : PolicyBase
    {
        public override void BuildPolicies()
        {
            Maternity();
            SmallNecessities();
            VictimOfDomesticViolence();
        }

        public static Policy Maternity() // Maternity Leave
        {
            // Selection:
            // - Work State = "MA"
            // - Min 6 Employees at Employer

            // Eligibility:
            // - Min legnth of service is the longer of 3 months or employers probationary period
            // - Min 40 hrs/week scheduled working hours

            // Reasons:
            // - Pregnancy / Maternity, Adoption/Foster

            // Entitlement:
            // - 8 weeks per child for birth or adoption.  If twins then 16 weeks, triplets 24 weeks etc.

            Policy policy = StagePolicy("MA-MATERN", "Massachusetts Maternity Leave", PolicyType.StateFML, "MA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Gender = Gender.Female
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(24) // Less than or equal to 23 (basically the same as < 24)
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Massachusetts Maternity Leave", MinLengthOfServiceRule(3, Unit.Months), MinHoursWorkedPerWeekRule(40)),
            };

            // Birth Multipliers, lol, yeah, WTF, but we have to do this 'cause people in MA like to have babies :-)
            for (int i = 2; i <= 12; i++)
                policy.RuleGroups.Add(ChildMultiplierEntitlementTimeRuleGroup(i, i * 8));

            return SavePolicy(policy);
        }

        private static PolicyRuleGroup ChildMultiplierEntitlementTimeRuleGroup(int children, double entitlement)
        {
            return new PolicyRuleGroup()
            {
                Name = "Number of Child Events Multiplier Rules",
                Description = "Increases entitlement based on number of children either born or adopted for this leave event",
                Entitlement = entitlement,
                RuleGroupType = PolicyRuleGroupType.Time,
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        Name = string.Format("Employee gave birth to or adopted {0} children", children),
                        Description = string.Format("Employee gave birth to or adopted {0} children and is therefore entitled to {0} work weeks' leave", children, entitlement),
                        LeftExpression = "NumberOfChildrenForLeave()",
                        Operator = "==",
                        RightExpression = children.ToString()
                    }
                }
            };
        }

        public static Policy SmallNecessities() // Small Necessities Leave
        {
            // Selection:
            // - Work State = "MA"
            // - Min Employees >= 50

            // Eligibility:
            // - 1250 hours in 12 months
            // - 1 year of service

            // Reasons:
            // - School/Parental, Family Health Condition

            // Entitlement:
            // - 24 Hours in a 12 month period May take leave to:   
            //  - participate in school activities directly related to the educational advancement of a son or daughter, such as parent-teacher conferences or interviewing for a new school;
            //  - accompany a son or daughter or an elderly relative to routine medical or dental appointments, such as check-ups or vaccinations; and
            //  - accompany an elderly relative to appointments for professional services related to the elder's care, such as interviewing at nursing or group homes.

            Policy policy = StagePolicy("MA-NECESSITIES", "Massachusetts Small Necessities Leave", PolicyType.StateFML, "MA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 24,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "INLOCOPARENTIS")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 24,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ELDERLYRELATIVE", "BLOODRELATIVE", "RELATIVEBYMARRIAGE")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1250, false, true, "Massachusetts Small Necessities Leave"),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50)
            };
            return SavePolicy(policy);
        }

        public static Policy VictimOfDomesticViolence() // Domestic Violence Leave Law
        {
            // Selection:
            // - Work State = "MA"

            // Eligibility:
            // - Day 1 Eligible
            // - Min 50 Employees at Employer

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 15 Work Days per year

            Policy policy = StagePolicy("MA-DOM", "Massachusetts Domestic Violence Leave Law", PolicyType.StateFML, "MA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
