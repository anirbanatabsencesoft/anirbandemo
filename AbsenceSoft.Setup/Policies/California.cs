﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class California : PolicyBase
    {
        public override void BuildPolicies()
        {
            FamilyRightsAct();
            PDL();
            DomesticViolence();
            OrganOrBoneMarrowDonation();
            FSPA();
            MIL();
            CaliforniaDisabilityInsurance();
            PFL();
            CAP();
            TMLA();
            TMLAS();
            VFLED();
            VFTL();
            KinCare();
        }
        public static Policy VFTL() // California Volunteer Firefighter Training Leave
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = 50

            // Eligibility:
            // - 

            // Reasons:
            // - 

            // Entitlement:
            // - 14 calendar days per calendar year of unpaid Leave.

            const string leaveName = "California Volunteer Firefighter Training Leave";

            Policy policy = StagePolicy("CA-VFTL", leaveName, PolicyType.Other, "CA");
            policy.EffectiveDate = new DateTime(2015, 1, 1);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 14,
                    Period= 1,
                    PeriodType= PeriodType.CalendarYear,
                    Reason = Reasons.CivicService,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesSelectionRuleGroup(50),
                ManualOnlyRuleGroup(PolicyRuleGroupType.Eligibility)
            };

            return SavePolicy(policy);

        }
        public static Policy VFLED() // California Volunteer Firefighter Leave for Emergency Duty
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = No limits

            // Eligibility:
            // - Min. Length of Service = No limits

            // Reasons:
            // - 

            // Entitlement:
            // - No limits, Eligible employees are permitted to take a reasonable amount of time off

            Policy policy = StagePolicy("CA-VFLED", "California Volunteer Firefighter Leave for Emergency Duty", PolicyType.Other, "CA");
            policy.EffectiveDate = new DateTime(2015, 1, 1);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.CivicService,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                ManualOnlyRuleGroup(PolicyRuleGroupType.Eligibility)
            };

            return SavePolicy(policy);

        }
        public static Policy TMLA() // Temporary Military Leave of Absence
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = No limits

            // Eligibility:
            // - Min. Length of Service = No limits

            // Reasons:
            // - 

            // Entitlement:
            // - 17 calendar days per year of unpaid Leave.

            Policy policy = StagePolicy("CA-TMLA", "California Temporary Military Leave of Absence", PolicyType.Other, "CA");
            policy.EffectiveDate = new DateTime(1957,5,21);            
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 17,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.ReserveTraining,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EmployeeRelationshipRuleGroup(null, "SELF")
            };

            return SavePolicy(policy);

        }
        public static Policy TMLAS() // California Temporary Military Leave of Absence for Spouses
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = 25

            // Eligibility:
            // - Min. Length of Service =  20 or more hours per week

            // Reasons:
            // - 

            // Entitlement:
            // - 17 calendar days per year of unpaid Leave.
            const string leaveName = "California Temporary Military Leave of Absence for Spouses";

            Policy policy = StagePolicy("CA-TMLAS", leaveName, PolicyType.Other, "CA");
            policy.EffectiveDate = new DateTime(2007, 10, 9);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 10,
                    Reason = Reasons.ActiveDuty,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 10,
                    Reason = Reasons.QualifyingExigency,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesSelectionRuleGroup(25),
                EligibilityRuleGroup(leaveName, MinHoursWorkedPerWeekRule(20)),
                EmployeeRelationshipRuleGroup(null, "SPOUSE"),
                ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
            };

            return SavePolicy(policy);

        }

        public static Policy CAP() // Civil Air Patrol Leave
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = 15

            // Eligibility:
            // - Min. Length of Service = 90 Days,  except those employees who are required to respond to the same or similar emergency situations as 
            //                                      the Civil Air Patrol by way of their designation as first responders or disaster service workers for 
            //                                      a local, state, or federal agency
            // - 

            // Reasons:
            // - 

            // Entitlement:
            // - 10 calendar days per year of unpaid Civil Air Patrol Leave. However, such leave shall not exceed 3 days for a single emergency operational mission 
            //   unless an extension of time is granted by the applicable governmental entity and approved by the employer.

            Policy policy = StagePolicy("CA-CAP", "California Civil Air Patrol Leave", PolicyType.Other, "CA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 10,
                    PerUseCap = 3,
                    PerUseCapType = EntitlementType.CalendarDays,
                    Period = 3,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.ActiveDuty,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                EligibilityRuleGroup(policy.Name, MinLengthOfServiceRule(90, Unit.Days)),
                ManualOnlyRuleGroup()
            };
            return SavePolicy(policy);
        }
        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 25
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 20hrs/wk
            //      Min Len Service = 1 day
            //      Work State = CA
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame = 10 workdays per Active Military Leave
            //      Runs Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = x
            //      Eligible Family Members = Spuse Domestic Partner
            //      Min Length of Military Duty = 
            //      Active Military Duty = x
            //      Search & Rescue = 
            //      National Guard = x
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("CA-MIL", "California Family Military Leave", PolicyType.StateFML, "CA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    WorkState = "CA",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    WorkState = "CA",
                    Reason = Reasons.QualifyingExigency,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    WorkState = "CA",
                    Reason = Reasons.Ceremonial,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25));
            policy.RuleGroups.Add(EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER"));
            policy.RuleGroups.Add(ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty));
            policy.RuleGroups.Add(EligibilityRuleGroup("California Family Military Leave", MinHoursWorkedPerWeekRule(20)));

            return SavePolicy(policy);
        }

        public static Policy FamilyRightsAct() // California Family Rights Act (CFRA)
        {
            Policy policy = StagePolicy("CA-CFRA", "California Family Rights Act", PolicyType.StateFML, "CA");
            policy.ConsecutiveTo.Add("CA-CPDL");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD", "DOMESTICPARTNER", "STEPPARENT", "FOSTERPARENT", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "SAMESEXSPOUSE", "INLOCOPARENTIS")
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Gender = Gender.Female
                },
                // Bonding
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.Bonding,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD")
                    },
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesSelectionRuleGroup(50),
                FMLEligibilityRuleGroup(1250, false, true, "California Family Rights Act", true)
            };
            return SavePolicy(policy);
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "CA"
            // - Min 5 Employees
            // - Females Only

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - 4 work months in a 12 month period

            Policy policy = StagePolicy("CA-CPDL", "California Pregnancy Disability Leave", PolicyType.StateFML, "CA", Gender.Female);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingMonths,
                    Entitlement = 4,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Gender = Gender.Female,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                             EventType = CaseEventType.BondingStartDate,
                             DateType = EventDateType.EndPolicyBasedOnEventDate
                        }
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(5)
            };
            return SavePolicy(policy);
        }

        public static Policy DomesticViolence() // Domestic Violence Leave
        {
            // Selection:
            // - Work State = "CA"
            // - Min Employees = 25

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 12 weeks in a 12 month period

            Policy policy = StagePolicy("CA-DOM", "California Domestic Violence Leave", PolicyType.StateFML, "CA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25)
            };
            return SavePolicy(policy);
        }

        public static Policy OrganOrBoneMarrowDonation() // Organ and Bone Marrow Donation
        {
            // Selection:
            // - Work State = "CA"
            // - Minimum Employees = 15

            // Eligibility:
            // - Min. Length of Service = 90 Days
            // - 

            // Reasons:
            // - Bone Marrow Donation
            // - Organ Donation

            // Entitlement:
            // - 30 days for Organ Donation 5 Days for Bone Marrow donation
            // - Not concurrent to FML

            Policy policy = StagePolicy("CA-DONOR", "California Organ and Bone Marrow Donation", PolicyType.StateFML, "CA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 30,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.OrganDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 5,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                EmployeeRelationshipRuleGroup(null, "SELF"),
                EligibilityRuleGroup(policy.Name,
                    MinLengthOfServiceRule(90, Unit.Days)
                )
            };
            return SavePolicy(policy);
        }

        public static Policy FSPA() // Family-School Partnership Leave (FSPA)
        {
            // Selection:
            // - Work State = "CA"
            // - Min Employees = 25

            // Eligibility:
            // - N/A

            // Reasons:
            // - Parental/School

            // Entitlement:
            // - Equivalent of ee's scheduled hours per week may be taken in a rolling 12 month time-frame.
            //      However the ee may only take the equivalent of 1 workday per month (default calc is scheduled hours divided by 5)
            // - Non-Concurrent with EVERYTHING

            Policy policy = StagePolicy("CA-FSPA", "California Family-School Partnership Leave (FSPA)", PolicyType.StateFML, "CA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.ConsecutiveTo.Add("STD");
            policy.ConsecutiveTo.Add("LTD");
            policy.ConsecutiveTo.Add("WC");
            policy.ConsecutiveTo.Add("CA-CFRA");
            policy.ConsecutiveTo.Add("CA-CPDL");
            policy.ConsecutiveTo.Add("CA-DOM");
            policy.ConsecutiveTo.Add("CA-DONOR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD","GRANDCHILD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.Educational,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD","GRANDCHILD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25)
            };
            return SavePolicy(policy);
        }

        public static Policy CaliforniaDisabilityInsurance()
        {
            Policy policy = StagePolicy("CA-STD", "California Disability Insurance", PolicyType.StateDisability, "CA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>() 
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 52,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = 0m, IsWaitingPeriod = true },
                        new PaymentInfo() { Duration = 52, Order = 2, PaymentPercentage = .55M }
                    },
                    UseWholeDollarAmountsOnly = true,
                    MaxAllowedAmount = 1104M,
                    EliminationType = EntitlementType.CalendarDays,
                    Elimination = 7d,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,                    
                    Period = 52,
                    PeriodType = PeriodType.PerOccurrence,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        IsNotWorkRelated()
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 52,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = 0m, IsWaitingPeriod = true },
                        new PaymentInfo() { Duration = 52, Order = 2, PaymentPercentage = .55M }
                    },
                    UseWholeDollarAmountsOnly = true,
                    MaxAllowedAmount = 1104M,
                    EliminationType = EntitlementType.CalendarDays,
                    Elimination = 7d,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    Period = 52,
                    PeriodType = PeriodType.PerOccurrence,                    
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        IsNotWorkRelated()
                    },
                    Gender = Gender.Female
                }
            };

            return SavePolicy(policy);
        }

        public static Policy PFL() // Paid Family Leave
        {
            // Selection:
            // - Work State = "CA"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Family Health Condition
            // - Pregnancy/Maternity (After Bonding)
            // - Parental/Bonding
            // - Adoption/Foster Care (After Bonding)

            // Entitlement:
            // - 6 work weeks in a 12 month period (+ 1 week waiting period, unpaid)

            // Pay:
            // - 1 week waiting period (7-day)
            // - Paid for 6 weeks

            Policy policy = StagePolicy("CA-PFL", "California Paid Family Leave", PolicyType.StateDisability, "CA");
            policy.EffectiveDate = new DateTime(2004, 1, 1).ToMidnight();
            policy.Description = "In 2002, legislation (Senate Bill 1661) extended disability compensation to individuals who take time off work to care for a seriously ill child, spouse, parent, domestic partner, or to bond with a new child or a child in connection with adoption or foster care placement. In 2013, legislation (Senate Bill 770) expanded eligibility to also include the following family members: parent-in-law, grandparent, grandchild, and sibling.";
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD", "DOMESTICPARTNER", "FOSTERCHILD", "ADOPTEDCHILD", "PARENTINLAW", "GRANDPARENT", "GRANDCHILD", "SIBLING")
                    },
                    Paid = true,
                    AllowOffset = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo() { Order = 0, IsWaitingPeriod = true, Duration = 1, PaymentPercentage = 0m },
                        new PaymentInfo() { Order = 1, Duration = 6, PaymentPercentage = 1m, MaxPaymentPercentage = 1m }
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent() { DateType = EventDateType.StartPolicyBasedOnEventDate, EventType = CaseEventType.BondingStartDate }
                    },
                    Paid = true,
                    AllowOffset = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo() { Order = 0, IsWaitingPeriod = true, Duration = 1, PaymentPercentage = 0m },
                        new PaymentInfo() { Order = 1, Duration = 6, PaymentPercentage = 1m, MaxPaymentPercentage = 1m }
                    },
                    
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent() { DateType = EventDateType.StartPolicyBasedOnEventDate, EventType = CaseEventType.BondingStartDate }
                    },
                    Paid = true,
                    AllowOffset = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo() { Order = 0, IsWaitingPeriod = true, Duration = 1, PaymentPercentage = 0m },
                        new PaymentInfo() { Order = 1, Duration = 6, PaymentPercentage = 1m, MaxPaymentPercentage = 1m }
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent() { DateType = EventDateType.StartPolicyBasedOnEventDate, EventType = CaseEventType.BondingStartDate }
                    },
                    Paid = true,
                    AllowOffset = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo() { Order = 0, IsWaitingPeriod = true, Duration = 1, PaymentPercentage = 0m },
                        new PaymentInfo() { Order = 1, Duration = 6, PaymentPercentage = 1m, MaxPaymentPercentage = 1m }
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }

        public static Policy KinCare() // California Kin Care
        {
            // Selection:
            // - Work State = "CA"
            // - Valid family relationship (kin)

            // Reasons:
            // - EHC
            // - FHC
            // - DOM

            // Entitlement:
            // - any reasonable period

            Policy policy = StagePolicy("CA-KC", "California Kin Care", PolicyType.StateFML, "CA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.EmployeeHealthCondition,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    CaseTypes = CaseType.Consecutive
                },
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.FamilyHealthCondition,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    CaseTypes = CaseType.Consecutive
                },
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.DomesticViolence,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    CaseTypes = CaseType.Consecutive
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "ADOPTEDCHILD", "FOSTERCHILD", "STEPCHILD", "LEGALWARD", "INLOCOPARENTIS", "PARENT", "PARENTINLAW", "SPOUSE",
                    "DOMESTICPARTNER", "GRANDCHILD", "GRANDPARENT", "SIBLING")
            };
            return SavePolicy(policy);
        }
    }
}
