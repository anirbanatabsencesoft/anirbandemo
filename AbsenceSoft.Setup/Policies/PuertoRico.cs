﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class PuertoRico : PolicyBase
    {
        public override void BuildPolicies()
        {
        
            PRWMPA();
                
        }


        public static Policy PRWMPA() // Pregnancy Disability Leave
        {

            PolicyRuleGroup adoptedChildRuleGroup = EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD");
            adoptedChildRuleGroup.RuleGroupType = PolicyRuleGroupType.Eligibility;


            Policy policy = StagePolicy("PRWMPA", "Puerto Rico Working Mothers' Protection Act", PolicyType.StateDisability, "PR", Gender.Female);
            policy.Description = "Puerto Rico Working Mothers' Protection Act";
            policy.Gender = Gender.Female;
          
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    PeriodType = PeriodType.PerCase,
                    Entitlement =8,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive ,
                    WorkState = "PR",
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Paid = false,
                    Gender = Gender.Female
                },

            new PolicyAbsenceReason()
            {
                EffectiveDate = policy.EffectiveDate,
                EntitlementType = EntitlementType.CalendarWeeks,
                PeriodType = PeriodType.PerCase,
                Entitlement =8,
                Reason = Reasons.AdoptionFosterCare,
                CaseTypes = CaseType.Consecutive ,
                WorkState = "PR",
                ShowType = PolicyShowType.OnlyTimeUsed,
                Paid = false,
                Gender = Gender.Female,
               RuleGroups = new List<PolicyRuleGroup>()
                    {
                      adoptedChildRuleGroup,
                      AdoptedMaxAgeRule(5)
                    },
            }
            };

            return SavePolicy(policy);
        }

    }
}
