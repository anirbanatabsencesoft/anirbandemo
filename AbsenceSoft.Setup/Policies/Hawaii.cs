﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Hawaii : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            PDL();
            VictimsProtection();
            OrganOrBoneMarrowDonation();
        }

        public static Policy FML() // Family Leave
        {
            // Selection:
            // - Work State = HI
            // - Min 100 Employees

            // Eligibility:
            // - 6 Months of Service

            // Reasons:
            // - PREGMAT, ADOPT, FHC

            // Entitlement:
            // - 4 weeks in a calendar year

            Policy policy = StagePolicy("HI-FML", "Hawaii Family Leave", PolicyType.StateFML, "HI");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "CIVILUNIONPARTNER", "SAMESEXSPOUSE", "PARENT", "PARENTINLAW", "STEPPARENT", "FOSTERPARENT", "GRANDPARENT", "GRANDPARENTINLAW", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "GRANDCHILD", "RECIPROCALBENEFICIARY",
                            "SIBLING", "GUARDIAN")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(100),
                EligibilityRuleGroup("Hawaii Family Leave", MinLengthOfServiceRule(6, Unit.Months))
            };
            return SavePolicy(policy);
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "HI"
            // - Females Only

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - A Reasonable Amount of Time, lol

            Policy policy = StagePolicy("HI-PDL", "Hawaii Pregnancy Disability Leave", PolicyType.StateFML, "HI");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            return SavePolicy(policy);
        }

        public static Policy VictimsProtection() // Victims Protection
        {
            // Selection:
            // - Work State = "HI"
            // - Min Employees = 5

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - Two Rules 
            //  1) Employers with 50 or more employees 30 Workdays per Calendar year 
            //  2) Employers with 5-49 employees 5 workdays per Calendar year

            Policy policy = StagePolicy("HI-DOM", "Hawaii Victims Protection", PolicyType.StateFML, "HI");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 5,
                    Period = 12,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer in Hawaii",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                            }
                        }
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(5),
                EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "CHILDLIVINGWITHEMPLOYEE")
            };
            return SavePolicy(policy);
        }

        public static Policy OrganOrBoneMarrowDonation() // Organ and Bone Marrow Donation
        {
            // Selection:
            // - Work State = "HI"
            // - Minimum Employees = 50

            // Eligibility:
            // - Day 1 Eligible
            // - 

            // Reasons:
            // - Bone Marrow Donation
            // - Organ Donation

            // Entitlement:
            // - 30 calendar days for Organ Donation 7 Days for Bone Marrow donation
            // - Not concurrent to FML

            Policy policy = StagePolicy("HI-DONOR", "Hawaii Organ and Bone Marrow Donation Leave", PolicyType.StateFML, "HI");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 30,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.OrganDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 7,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                EmployeeRelationshipRuleGroup(null, "SELF")
            };
            return SavePolicy(policy);
        }
    }
}
