﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Nebraska : PolicyBase
    {
        public override void BuildPolicies()
        {
            //Adoption();
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 15
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = NE
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  1) 15-50 ee's 15 workdays per occurrence
            //                              2) > 50 ee's 30 workdays per occurrence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = spouse & child
            //      Min Length of Military Duty = 179 Calendar days
            //      Active Military Duty = yes
            //      Search & Rescue = yes
            //      National Guard = yes
            //      Reserve Training = yes
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("NE-MIL", "Nebraska Family Military Leave", PolicyType.StateFML, "NE");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    WorkState = "NE",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,  "SPOUSE" , "CHILD","FOSTERCHILD" ,"ADOPTEDCHILD","LEGALWARD" ),   
                        EligibilityRuleGroup("Nebraska Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(179, Unit.Days)),

                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer in Nebraska",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                               
                            }
                        },
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },

                new PolicyAbsenceReason()
                {
                     WorkState = "NE",
                    Reason = Reasons.ReserveTraining,
                    EntitlementType = EntitlementType.CalendarMonths,
                    Entitlement = 15, 
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "CHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD" ),
                        EligibilityRuleGroup("Nebraska Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(179, Unit.Days)),
                                                                        
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer in Nebraska",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                            }
                        }
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }

            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15));
            
            return SavePolicy(policy);
        }

        public static Policy Adoption() // Adoption Leave
        {
#warning TODO: State FML: Nebraska: Adoption Leave, Requires company policies to be implemented
            // Selection:
            // - Work State = "NE"

            // Eligibility:
            // - Same as company policy

            // Reasons:
            // - Adoption/Foster Care

            // Entitlement:
            // - Same as company policy

            throw new NotImplementedException();

            //Policy policy = StagePolicy("NE-ADOPT", "Nebraska Adoption Leave", PolicyType.StateFML, "NE");
            //policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            //{
            //    new PolicyAbsenceReason()
            //    {
            //        EffectiveDate = policy.EffectiveDate,
            //        EntitlementType = EntitlementType.CalendarWeeks,
            //        Entitlement = 6,
            //        Period = 1,
            //        PeriodType = PeriodType.PerCase,
            //        Reason = Reasons.AdoptionFosterCare,
            //        CaseTypes = CaseType.Consecutive,
            //        RuleGroups = new List<PolicyRuleGroup>()
            //        {
            //            EmployeeRelationshipRuleGroup(,Self, ,AdoptedChild)
            //        }
            //    }
            //};
            //return SavePolicy(policy);
        }
    }
}
