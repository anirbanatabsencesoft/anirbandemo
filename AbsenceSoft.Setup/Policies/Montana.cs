﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Montana : PolicyBase
    {
        public override void BuildPolicies()
        {
            PDL();
        }

        public static Policy PDL() // Maternity/Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "MT"
            // - Females Only

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - Reasonable period of time

            Policy policy = StagePolicy("MT-PDL", "Montana Maternity/Pregnancy Disability Leave", PolicyType.StateFML, "MT");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            return SavePolicy(policy);
        }
    }
}
