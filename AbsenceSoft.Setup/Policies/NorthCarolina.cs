﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class NorthCarolina : PolicyBase
    {
        public override void BuildPolicies()
        {
            ParentInvolvement();
        }

        public static Policy ParentInvolvement() // Leave for Parent Involvement in Schools
        {
            // Selection:
            // - Work State = "NC"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Parental/School

            // Entitlement:
            // - 4 hours per year

            Policy policy = StagePolicy("NC-PARENT", "North Carolina Leave for Parent Involvement in Schools", PolicyType.StateFML, "NC");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 4,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
