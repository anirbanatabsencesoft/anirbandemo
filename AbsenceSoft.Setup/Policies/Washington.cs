﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using MongoDB.Bson;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Washington : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            PDL();
            DomesticViolence();
            MIL();
            FamilyCareAct();
            Adoption();
        }

       public static Policy MIL() 
       {
            // Indicative Data Elements
            //      #EE = 20
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 20hrs/wk
            //      Min Len Service = 1 day
            //      Work State = WA
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame = 15 workdays per coccurance
            //      Runs Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = x
            //      Eligible Family Members = Spuse Domestic Partner
            //      Min Length of Military Duty = 
            //      Active Military Duty = x
            //      Search & Rescue = x
            //      National Guard = x
            //      Reserve Training = x
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("WA-MIL", "Washington Family Military Leave", PolicyType.StateFML, "WA");
            
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {

                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "DOMESTICPARTNER"),                   
                        EligibilityRuleGroup("Oregon Family Military Leave", MinHoursWorkedPerWeekRule(20),  MinLengthOfServiceRule(1, Unit.Days) )
                        
                    },
                    WorkState = "WA",
                    Reason = Reasons.CoveredServiceMember,
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            
            policy.RuleGroups.Add(ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty));

            return SavePolicy(policy);
        }

        public static Policy FML() // Family Leave
        {
            // Selection:
            // - Work State = "WA"
            // - Min 50 employees at employer

            // Eligibility:
            // - Min 1 year of service @ 1250 hours

            // Reasons:
            // - Employee Serious Health Condition
            // - Family Member Serious Health Condition
            // - Parental Bonding

            // Entitlement:
            // - Same as Federal FMLA, 12 weeks in a 12 month period

            Policy policy = StagePolicy("WA-FML", "Washington Family Leave", PolicyType.StateFML, "WA");
            // Git #2007 - WA FML should be Consecutive to WA PDL. Meaning PDL is first, then WA-FML.
            policy.ConsecutiveTo.Add("WA-PDL");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER", "PARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "DOMESTICPARTNERSCHILD", "INLOCOPARENTIS", "ADOPTIVEPARENT")
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                // Bonding
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.Bonding,
                    Paid = false,
                    CombinedForSpouses = false,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD"),
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1250, false, true, "Washington Family Leave"),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50)
            };
            return SavePolicy(policy);
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "WA"
            // - Females Only
            // - employers with 8 or more employees

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - A Reasonable Amount of Time

            Policy policy = StagePolicy("WA-PDL", "Washington Pregnancy Disability Leave", PolicyType.StateFML, "WA");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(8)
                    }
                }
            };
            return SavePolicy(policy);
        }

        public static Policy DomesticViolence() // Domestic Violence Leave
        {
            // Selection:
            // - Work State = "WA"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - A reasonable amount of leave

            Policy policy = StagePolicy("WA-DOM", "Washington Domestic Violence Leave", PolicyType.StateFML, "WA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "DOMESTICPARTNER", "GRANDPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "COMMITTEDRELATIONSHIP", "DOMESTICPARTNERSCHILD",
                            "PARENT", "ADOPTIVEPARENT", "INLOCOPARENTIS", "PARENTINLAW")
                    }
                }
            };
            return SavePolicy(policy);
        }

       

        public static Policy FamilyCareAct() // Family Care Act
        {
            // Selection:
            // - Work State = "WA"

            // Eligibility:
            // - All employees with a PTO balance are eligible

            // Reasons:
            // - Family Health Condition
            // - Adoption/Foster Care

            // Entitlement:
            // - Reasonable Period, up to the user to specify based on EE's available
            //      PTO bank (not tracked by us)

            Policy policy = StagePolicy("WA-FAM", "Washington Family Care Act", PolicyType.StateFML, "WA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EmployeeRelationshipRuleGroup(null, "SPOUSE", "SAMESEXSPOUSE", "DOMESTICPARTNER", "PARENT", "PARENTINLAW", "GRANDPARENT", "CHILD", "ADOPTEDCHILD", "FOSTERCHILD", "LEGALWARD", "STEPCHILD", "DOMESTICPARTNERSCHILD", "SAMESEXSPOUSECHILD"),
                new PolicyRuleGroup()
                {
                    Name = "Employee Must Have a PTO Balance",
                    Description = "The Employee must have a time off w/ pay balance greater than 0 (zero) hours in order to be eligible for this leave (AbsenceTracker does not track Time Off w/ Pay balances, if the Employee has one, please override the outcome of this rule below and enter in the actual balance as of the Case Start Date):",
                    SuccessType = RuleGroupSuccessType.And,
                    RuleGroupType = PolicyRuleGroupType.Eligibility,
                    Rules = new List<Rule>()
                    {
                        new Rule()
                        {
                            Name = "Time Off w/ Pay Balance",
                            Description = "The Employee's Time Off w/ Pay Balance must be greater than 0",
                            LeftExpression = "0",
                            Operator = ">",
                            RightExpression = "0",
                        }
                    },
                    Metadata = BsonDocument.Parse("{'IsPtoRule':true}")
                }
            };
            return SavePolicy(policy);
        }

        public static Policy Adoption() // Adoption Leave
        {
            // Selection:
            // - Same as company policy

            // Eligibility:
            // - Same as company policy

            // Reasons:
            // - Adoption / Foster Care

            // Entitlement:
            // - Same as company policy (so reasonable period, up to the employer)

            Policy policy = StagePolicy("WA-ADOPT", "Washington Adoption Leave", PolicyType.StateFML, "WA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                         EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD"),
                        AdoptedMaxAgeRule(5)
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
