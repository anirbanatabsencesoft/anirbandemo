﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Maine : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            VictimOfViolence();
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 15
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = IL
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  15 work days per occurence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Family see Matrix
            //      Min Length of Military Duty = 180 Days Calendar
            //      Active Military Duty = 
            //      Search & Rescue = 
            //      National Guard = x
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("ME-MIL", "Maine Family Military Leave", PolicyType.StateFML, "ME");

            // policy.ConsecutiveTo.Add("ME-MIL"); // removed as per 4/3/14 email from Chad

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    WorkState = "ME",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "DOMESTICPARTNER" , "CHILD"),
                        EligibilityRuleGroup("Maine Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(180, Unit.Days)),
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15));
            policy.RuleGroups.Add(ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty));

            return SavePolicy(policy);
        }


        public static Policy FML() // Family Medical Leave
        {
            // Selection:
            // - Work State = HI
            // - Min 15 Employees

            // Eligibility:
            // - 12 Months of Service

            // Reasons:
            // - PREGMAT, ADOPT, FHC

            // Entitlement:
            // - 10 weeks within a 2 year (24 month) period

            Policy policy = StagePolicy("ME-FML", "Maine Family Medical Leave", PolicyType.StateFML, "ME");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 10,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD", "ADOPTEDCHILD", "DOMESTICPARTNER", "SIBLING")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 10,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(17)
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                EligibilityRuleGroup("Maine Family Medical Leave", MinOneYearServiceRule())
            };
            return SavePolicy(policy);
        }

        public static Policy VictimOfViolence() // Employee Leave for Victims of Violence
        {
            // Selection:
            // - Work State = "ME"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - Reasonable amount of time

            Policy policy = StagePolicy("ME-DOM", "Maine Employee Leave for Victims of Violence", PolicyType.StateFML, "ME");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "PARENT", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
