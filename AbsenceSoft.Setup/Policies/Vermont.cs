﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Vermont : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            FMLExtension();
        }

        public static Policy FML() // Parental and Family Leave
        {
            // Selection:
            // - Work State = VT
            // - Min Employees: 10 for Parental and 15 for Family

            // Eligibility:
            // - Min 30/hrs per week
            // - 1 Year of Service

            // Reasons:
            // - EHC, PREGMAT, ADOPT, FHC

            // Entitlement:
            // - 12 weeks of consecutive and intermittent leave in a 12 month period 
            //  an aditional 48 hours may be added for intermittent use only. (see extension leave below)
            //  In addition the 48 hours may be used for school related requests for child

            Policy policy = StagePolicy("VT-FML", "Vermont Parental and Family Leave", PolicyType.StateFML, "VT");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "CIVILUNIONPARTNER", "SAMESEXSPOUSE", "PARENT", "PARENTINLAW", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(10)
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(10),
                        EmployeeRelationshipRuleGroup(null, "SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(10),
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Vermont Parental and Family Leave", MinOneYearServiceRule(), MinHoursWorkedPerWeekRule(30))
            };
            return SavePolicy(policy);
        }

        public static Policy FMLExtension()
        {
            // Extends FML as an aditional 48 hours may be added for intermittent use only

            Policy policy = StagePolicy("VT-FMLEXT", "Vermont Parental and Family Leave (Intermittent Extension)", PolicyType.StateFML, "VT");
            policy.ConsecutiveTo.Add("VT-FML");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "CIVILUNIONPARTNER", "SAMESEXSPOUSE", "PARENT", "PARENTINLAW", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(10)
                
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(10),
                        EmployeeRelationshipRuleGroup(null, "SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 48,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent,
                    CombinedForSpouses = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Vermont Parental and Family Leave (Intermittent Extension)", MinOneYearServiceRule(), MinHoursWorkedPerWeekRule(30))
            };
            return SavePolicy(policy);
        }
    }
}
