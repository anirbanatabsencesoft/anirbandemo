﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Illinois : PolicyBase
    {
        public override void BuildPolicies()
        {
            VESSA();
            SchoolVisitationRights();
            BloodDonation();
            MIL();
            CBLL();
            KinCare();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 15
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = IL
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  1) 15-50 ee's 15 workdays per occurrence
            //                              2) > 50 ee's 30 workdays per occurrence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Family see Matrix
            //      Min Length of Military Duty = 30 Days
            //      Active Military Duty = 
            //      Search & Rescue = 
            //      National Guard = 
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("IL-MIL", "Illinois Family Military Leave", PolicyType.StateFML, "IL");
            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateSelectionRuleGroup(15));
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    WorkState = "IL",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15, 
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "CIVILUNIONPARTNER" , "PARENT" , "GRANDPARENT" , "CHILD" 
                            , "ADOPTEDCHILD" , "FOSTERCHILD" , "LEGALWARD" , "CIVILUNIONPARTNERSCHILD" , "CHILD" ),

                        EligibilityRuleGroup("Illinois Family Military Leave", MinLengthOfServiceRule(12, Unit.Months), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(30, Unit.Days)),
                        
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer who work in Illinois",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(51)
                            }
                        }                        
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            return SavePolicy(policy);
        }

        public static Policy VESSA() // VESSA
        {
            // Selection:
            // - Work State = "IL"
            // - Min Employees = 14

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - Two Rules 
            //  1) employers with15-49 ee's within state of IL entitled to 8 weeks of leave in 12 month period
            //  2) employers with 50 or more ee's within the state of IL are entitled to 12 weeks of leave within a 12 month period

            Policy policy = StagePolicy("IL-VESSA", "Illinois VESSA", PolicyType.StateFML, "IL");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 8,
                    Period = 12,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer who work in Illinois",
                            Entitlement = 12,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                            }
                        }
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(14),
                EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "RESIDENCEINHOUSE", "CHILDLIVINGWITHEMPLOYEE",
                    "CIVILUNIONPARTNER", "BLOODRELATIVE", "RELATIVEBYMARRIAGE", "COPARENT")
            };
            return SavePolicy(policy);
        }

        public static Policy SchoolVisitationRights() // School Visitation Rights Leave
        {
            // Selection:
            // - Work State = "IL"
            // - Min EE's >= 50

            // Eligibility:
            // - Min 20hrs/week
            // - 6 months min length of service

            // Reasons:
            // - Parental / School

            // Entitlement:
            // - 8 hours in a school year, but no more than 4 hours in any one day. A question should be asked during intitial leave 
            //  request intake stating “Does the employee have compensatory time off remaining?” If yes then the employee should NOT qualify for this leave.

            Policy policy = StagePolicy("IL-PARENT", "Illinois School Visitation Rights Leave", PolicyType.StateFML, "IL");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 8,
                    Period = 1,
                    PeriodType = PeriodType.SchoolYear,
                    PerUseCap = 4,
                    PerUseCapType = EntitlementType.WorkingHours,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Illinois School Visitation Rights Leave", MinHoursWorkedPerWeekRule(20), MinLengthOfServiceRule(6, Unit.Months)),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50)
            };
            return SavePolicy(policy);
        }

        public static Policy BloodDonation() // Blood Donation Leave
        {
            // Selection:
            // - Work State = "IL"
            // - Min EE's >= 51

            // Eligibility:
            // - Min 40 hrs/week
            // - 6 months minimum legnth of service

            // Reasons:
            // - Blood Donation

            // Entitlement:
            // - Limited to 1 hour every 56 days.
            // - Non-concurrent with FMLA

            Policy policy = StagePolicy("IL-DONOR", "Illinois Blood Donation Leave", PolicyType.StateFML, "IL");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 1,
                    Period = (56d / 365.25d), // Yes, entitlement is around 0.1533196440793977 calendar years, or 56 days (56 / 365.25)
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.BloodDonor,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("Illinois Blood Donation Leave", MinLengthOfServiceRule(6, Unit.Months), MinHoursWorkedPerWeekRule(40))
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(51)
            };
            return SavePolicy(policy);
        }

        public static Policy CBLL() // Child Bereavement Leave Law
        {
            // Selection:
            // - Work State = "IL"
            // - Min EE's >= 50

            // Eligibility:
            // - Min 20hrs/week
            // - 6 months min length of service

            // Reasons:
            // - Parental / School

            // Entitlement:
            // - 8 hours in 12 month period, but no more than 4 hours in any one day. A question should be asked during intitial leave 
            //  request intake stating “Does the employee have compensatory time off remaining?” If yes then the employee should NOT qualify for this leave.

            Policy policy = StagePolicy("IL-CBLL", "Illinois Child Bereavement Leave Law", PolicyType.StateFML, "IL");
            //policy.ConsecutiveTo.Add("FMLA");
            policy.EffectiveDate = new DateTime(2016, 7, 29).ToMidnight();
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 2, // 2 weeks (10 work days) of unpaid bereavement leave per the death of the employee’s child.
                    Period = 12,
                    Paid = false,
                    Reason = Reasons.Bereavement,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "INLOCOPARENTIS")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup(
                    "Illinois Child Bereavement Leave Law", 
                    MinNumberOfEmployeesRule(50), 
                    MinLengthOfServiceRule(12, Unit.Months), // Employees who have worked for the employer for 12 months
                    MinLengthOfServiceRule(1250, Unit.Hours) // Employees who have worked at least 1,250 hours during the 12-month period 
                )
            };
            return SavePolicy(policy);
        }

        public static Policy KinCare() // Illinois Kin Care
        {
            // Selection:
            // - Work State = "IL"
            // - Valid family relationship (kin)

            // Reasons:
            // - FHC

            // Entitlement:
            // - any reasonable period

            Policy policy = StagePolicy("IL-KC", "Illinois Kin Care", PolicyType.StateFML, "IL");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER", "CHILD", "STEPCHILD", "PARENT", "STEPPARENT", "PARENTINLAW", "GRANDCHILD", "GRANDPARENT")
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
