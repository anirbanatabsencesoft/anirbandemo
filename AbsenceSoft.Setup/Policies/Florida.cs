﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Florida : PolicyBase
    {
        public override void BuildPolicies()
        {
            VictimsProtection();
        }

        public static Policy VictimsProtection() // Victims Protection
        {
            // Selection:
            // - Work State = "FL"
            // - Min Employees = 50

            // Eligibility:
            // - Min 3 months of service

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 3 Days in a 12 month period only after all accrued vacation/sick/personal time has been used

            Policy policy = StagePolicy("FL-DOM", "Florida Victims Protection", PolicyType.StateFML, "FL");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 3,
                    Period = 12,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                EligibilityRuleGroup("Florida Victims Protection", MinLengthOfServiceRule(3, Unit.Months)),
                EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "RESIDENCEINHOUSE", "CHILDLIVINGWITHEMPLOYEE",
                    "FORMERSPOUSE", "BLOODRELATIVE", "RELATIVEBYMARRIAGE", "COPARENT", "OTHERFAMILYMEMBER")
            };
            return SavePolicy(policy);
        }
    }
}
