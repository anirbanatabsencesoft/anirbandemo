﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class DC : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            Parental();
        }

        public static Policy FML() // Family and Medical Leave
        {
            // Selection:
            // - Work State = DC
            // - Min 20 Employees

            // Eligibility:
            // - 1000 Rule
            // - 12 Months of Service

            // Reasons:
            // - EHC, PREGMAT, ADOPT, FHC

            // Entitlement:
            // - Same as FMLA (Concurrent)

            Policy policy = StagePolicy("DC-FML", "District of Columbia Family and Medical Leave", PolicyType.StateFML, "DC");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    Reason = Reasons.FamilyHealthCondition,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "COMMITTEDRELATIONSHIP", "NEXTOFKIN", "CHILDLIVINGWITHEMPLOYEE")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                } ,
                 new PolicyAbsenceReason()
                 {
                     EffectiveDate = policy.EffectiveDate,
                     EntitlementType = EntitlementType.WorkWeeks,
                     Entitlement = 16,
                     Period = 24,
                     Reason = Reasons.Bonding,
                     CombinedForSpouses = true,
                     CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                     RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "FOSTERCHILD", "ADOPTEDCHILD")
                    }
                 }
                 };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                FMLEligibilityRuleGroup(1000, false, true, "District of Columbia Family and Medical Leave")
            };
            return SavePolicy(policy);
        }

        public static Policy Parental() // Parental Leave
        {
            // Selection:
            // - work State = DC
            // - for Child (not Self)

            // Eligibility:
            // - Day 1 eligible

            // Reasons:
            // - Parental

            // Entitlement:
            // - 16 hours in the last 12 months?

            Policy policy = StagePolicy("DC-PARENTAL", "District of Columbia Parental Leave", PolicyType.StateFML, "DC");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 24,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD")
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
