﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class STD : PolicyBase
    {
        public override void BuildPolicies()
        {
            BuildPolicy();
            BuildDemoPoliy80();
            BuildDemoPoliy60();
            BuildTestPoliy80();
        }

        public static Policy BuildPolicy()
        {
            Console.WriteLine("Creating policy 'STD'");
            Policy std = StagePolicy("STD", "Short Term Disability", PolicyType.STD);

            std.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Gender = Gender.Female
                }
            };
            std.RuleGroups = new List<PolicyRuleGroup>() { ManualOnlyRuleGroup() };
            std.CreatedById = "000000000000000000000000";
            std.ModifiedById = "000000000000000000000000";
            std.Save();
            Console.WriteLine("Saved policy 'STD' with Id '{0}'", std.Id);
            return std;
        }

        public static Policy BuildDemoPoliy80()
        {
            /* For Test Data employees to be used in Demo PROD environment please configure the following STD policy.
             * leave for employee's own serious health condition and not as a work related illness or injury
             *  - A minimum of 8 calendar days for the leave to be eligible for this benefit
             *  - 6 months of employment
             *  - no minimum hours worked per week requirement
             *  - Entitlement all runs consecutive
             *  - with a one week waiting period unpaid and not concurrent to the 80% STD benefit below
             *  - 11 weeks at 80%
             *  - plus 14 weeks at 60%
             */

            Console.WriteLine("Creating policy 'STD' for DEMO");
            Policy std = StagePolicy("STD", "Short Term Disability", PolicyType.STD, customerId: "000000000000000000000001", employerId: "000000000000000000000001");
            std.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 25,
                    Period = 1,
                    PeriodType = PeriodType.PerOccurrence,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = .0m},
                        new PaymentInfo() { Duration = 24, Order = 2, PaymentPercentage = .6m}
                    },
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,                    
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(6, Unit.Months))
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 25,
                    Period = 1,
                    PeriodType = PeriodType.PerOccurrence,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = .0m},
                        new PaymentInfo() { Duration = 24, Order = 2, PaymentPercentage = .6m}
                    },
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,                    
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(6, Unit.Months))
                    },
                    Gender = Gender.Female
                }
            };
            std.Save();
            Console.WriteLine("Saved policy 'STD' with Id '{0}' for DEMO", std.Id);
            return std;
        }

        public static Policy BuildTestPoliy80()
        {
            /* For Test Data employees to be used in Demo PROD environment please configure the following STD policy.
             * leave for employee's own serious health condition and not as a work related illness or injury
             *  - A minimum of 8 calendar days for the leave to be eligible for this benefit
             *  - 6 months of employment
             *  - no minimum hours worked per week requirement
             *  - Entitlement all runs consecutive
             *  - with a one week waiting period unpaid and not concurrent to the 80% STD benefit below
             *  - 11 weeks at 80%
             *  - plus 14 weeks at 60%
             */

            Console.WriteLine("Creating policy 'STD' for TEST");
            Policy std = StagePolicy("STD", "Short Term Disability", PolicyType.STD, customerId: "000000000000000000000002", employerId: "000000000000000000000002");
            std.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 25,
                    Period = 1,
                    PeriodType = PeriodType.PerOccurrence,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = .0m},
                        new PaymentInfo() { Duration = 24, Order = 2, PaymentPercentage = .6m}
                    },
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,                    
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(6, Unit.Months))
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 25,
                    Period = 1,
                    PeriodType = PeriodType.PerOccurrence,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = .0m},
                        new PaymentInfo() { Duration = 24, Order = 2, PaymentPercentage = .6m}
                    },
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,                    
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(6, Unit.Months))
                    },
                    Gender = Gender.Female
                }
            };
            std.Save();
            Console.WriteLine("Saved policy 'STD' with Id '{0}' for TEST", std.Id);
            return std;
        }

        public static Policy BuildDemoPoliy60()
        {
            var std = Policy.AsQueryable().Where(p => p.Code == "STD-EXT" && p.EmployerId == "000000000000000000000001").FirstOrDefault();
            if (std != null)
                std.Delete();
            return std;
            /*
            Console.WriteLine("Creating policy 'STD-EXT' for DEMO");
            Policy std = StagePolicy("STD-EXT", "Short Term Disability (Extended)", PolicyType.STD, customerId: "000000000000000000000001", employerId: "000000000000000000000001");
            std.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = std.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 14,
                    Period = 1,
                    PeriodType = PeriodType.PerOccurrence,
                    Paid = true,
                    PaymentPercentage = 60,                    
                    Reason = AbsenceReason.GetByCode("EHC"),
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup("SELF"),
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(6, Unit.Months))
                    }
                }
            };
            std.ConsecutiveTo = new List<string>() { "STD" };
            std.Save();
            Console.WriteLine("Saved policy 'STD-EXT' with Id '{0}' for DEMO", std.Id);
            return std;*/
        }
    }
}
