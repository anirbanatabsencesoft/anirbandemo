﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class ADA : PolicyBase
    {
        public override void BuildPolicies()
        {
            BuildPolicy();
            //RemovePolicy();
        }

        public static Policy BuildPolicy()
        {
            Console.WriteLine("Creating policy 'ADA'");
            Policy ada = Policy.AsQueryable().Where(p => p.Code == "ADA").FirstOrDefault() ?? new Policy() { Code = "ADA" };
            ada.Name = "Accommodation";
            //ada.Description = "";
            ada.PolicyType = PolicyType.Other;
            ada.EffectiveDate = new DateTime(1990, 1, 1);
            ada.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Accommodations
                new PolicyAbsenceReason()
                {
                    EffectiveDate = ada.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,//not used with ReasonablePeriod type
                    Period = 12,
                    Reason = Reasons.Accommodation,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    ShowType = PolicyShowType.OnlyTimeUsed
                }                
            };
            ada.RuleGroups = new List<PolicyRuleGroup>();
            
            ada.CreatedById = "000000000000000000000000";
            ada.ModifiedById = "000000000000000000000000";
            ada.Save();
            Console.WriteLine("Saved policy 'ADA' with Id '{0}'", ada.Id);
            return ada;
        }

        public static void RemovePolicy()
        {
            Console.WriteLine("Removing policy 'ADA'");
            if(Policy.AsQueryable().Any(p => p.Code == "ADA"))
            {
                Policy.Delete(Policy.AsQueryable().Where(p => p.Code == "ADA").FirstOrDefault().Id);
                Console.WriteLine("ADA policy removed");
            }
            else
                Console.WriteLine("No ADA policy found");
        }
    }
}
