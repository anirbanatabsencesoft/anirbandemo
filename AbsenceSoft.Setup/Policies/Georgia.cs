﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Georgia : PolicyBase
    {
        public override void BuildPolicies()
        {
            KinCare();
        }

        public static Policy KinCare() // Georgia Kin Care
        {
            // Selection:
            // - Work State = "GA"
            // - Valid family relationship (kin)
            // - Min 25+ employees at employer

            // Eligibility:
            // - Works 3+ hours per week

            // Reasons:
            // - FHC

            // Entitlement:
            // - any reasonable period

            Policy policy = StagePolicy("GA-KC", "Georgia Kin Care", PolicyType.StateFML, "GA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "CHILD", "DEPENDENT", "PARENT", "GRANDCHILD", "GRANDPARENT", "GAURDIAN")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesSelectionRuleGroup(25),
                new PolicyRuleGroup()
                {
                    Name = "Employee must work at least 3 hours per week",
                    Description = "Employee must work at least 3 hours per week",
                    RuleGroupType = PolicyRuleGroupType.Eligibility,
                    SuccessType = RuleGroupSuccessType.And,
                    Rules = new List<Data.Rules.Rule>()
                    {
                        MinHoursWorkedPerWeekRule(3)
                    }
                }                
            };
            return SavePolicy(policy);
        }
    }
}
