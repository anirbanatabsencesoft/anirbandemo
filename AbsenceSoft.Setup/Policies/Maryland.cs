﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Maryland : PolicyBase
    {
        public override void BuildPolicies()
        {
            Adoption();
            //Flexible();
            MIL();
            Parental();
            MPDL();
                
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 50
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = MD
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  1 work day per occurence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Family see Matrix
            //      Min Length of Military Duty = 180 Days Calendar
            //      Active Military Duty = 
            //      Search & Rescue = 
            //      National Guard = x
            //      Reserve Training = 
            //      Duty Outside US Required = yes
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("MD-MIL", "Maryland Family Military Leave", PolicyType.StateFML, "MD");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 1,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                         EmployeeRelationshipRuleGroup( null, "SPOUSE" , "SAMESEXSPOUSE" , "PARENT" , "STEPPARENT" , "CHILD" 
                        , "STEPCHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD" , "SIBLING" ),  
                        EligibilityRuleGroup("Maryland Family Military Leave", MinLengthOfServiceRule(12, Unit.Months), WorkedHoursInLast12MonthsRule(1250)),
                    },
                    WorkState = "MD",
                    Reason = Reasons.QualifyingExigency,
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50));
            
            return SavePolicy(policy);
        }

        public static Policy Adoption() // Adoption Leave
        {
            // Selection:
            // - Work State = "MD"

            // Eligibility:
            // - Same as company policy

            // Reasons:
            // - Adoption/Foster Care

            // Entitlement:
            // - The leave entitlement will be the exact same as the employers company policy leave 
            //  for birth of a child. If no company policy exists this leave should be suppressed

            Policy policy = StagePolicy("MD-ADOPT", "Maryland Adoption Leave", PolicyType.StateFML, "MD");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                }
            };
            return SavePolicy(policy);
        }

        public static Policy Flexible() // Flexible Leave
        {
#warning TODO: State FML: Maryland: Flexible Leave, Requires company policies to be implemented
            // Selection:
            // - Min Employees >= 15
            // - Work State = "MD"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - FHC

            // Entitlement:
            // - The leave entitlement will be the exact same as the employers company
            //  policy leave on Medical for Self. If no company policy exists this leave should be suppressed

            throw new NotImplementedException();

            //Policy policy = StagePolicy("MD-FLEX", "Maryland Flexible Leave", PolicyType.StateFML, "MD");
            //policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            //{
            //    new PolicyAbsenceReason()
            //    {
            //        EffectiveDate = policy.EffectiveDate,
            //        EntitlementType = EntitlementType.WorkWeeks,
            //        Entitlement = 12,
            //        Period = 12,
            //        Reason = Reasons.FamilyHealthCondition,
            //        CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
            //        RuleGroups = new List<PolicyRuleGroup>()
            //        {
            //            EmployeeRelationshipRuleGroup(ContactType.Spouse, ContactType.Parent, ContactType.Child)
            //        }
            //    }
            //};
            //policy.RuleGroups = new List<PolicyRuleGroup>()
            //{
            //    MinNumberOfEmployeesInTheSameWorkStateSelectionRuleGroup(15)
            //};
            //return SavePolicy(policy);
        }

        public static Policy Parental() // Parental Leave
        {
            // Selection:
            // - Work State = "MD"
            // - Min 15 employees at employer

            // Eligibility:
            // - Min 1 year of service @ 1250 hours
            // - 75mile Radius Rule

            // Reasons:
            // - Preggers + Adoption

            // Entitlement:
            // - 6 weeks of leave following birth or adoption of a child

            Policy policy = StagePolicy("MD-PARENT", "Maryland Parental Leave Act", PolicyType.StateFML, "MD");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    PolicyEvents = new List<PolicyEvent>(1)
                    {
                        // 6 weeks of leave following birth of a child
                        new PolicyEvent()
                        {
                            DateType = EventDateType.StartPolicyBasedOnEventDate,
                            EventType = CaseEventType.DeliveryDate
                        }
                    }
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    PolicyEvents = new List<PolicyEvent>(1)
                    {
                        // 6 weeks of leave following adoption of a child
                        new PolicyEvent()
                        {
                            DateType = EventDateType.StartPolicyBasedOnEventDate,
                            EventType = CaseEventType.AdoptionDate
                        }
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1250, false, true, "Maryland Parental Leave Act"),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15),
                EmployeeRelationshipRuleGroup(null, "CHILD", "ADOPTEDCHILD")
            };
            return SavePolicy(policy);
        }

        public static Policy MPDL() //Maryland Pregnancy Disability Leave
        {
            // Eligibility:
            // - Work State = "MD"
            // - Min 15 employees at employer


            // Reasons:
            // - Pregnancy /Maternity

            // Entitlement:
            // - Reasonable Period

            Policy policy = StagePolicy("MD-PDL", "Maryland Pregnancy Disability Leave", PolicyType.StateFML, "MD", Gender.Unknown);
            policy.Description = "Disabilities caused or contributed to by pregnancy or childbirth are temporary disabilities for all job-related purposes and must be treated as temporary disabilities under any health or temporary disability insurance or sick leave plan available in connection with employment.";
            policy.WorkCountry = "US";
            policy.Gender = null;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced| CaseType.Administrative,
                    WorkState = "MD",
                    WorkCountry ="US",
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    Paid = false,
                    Gender = Gender.Female,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                             EventType = CaseEventType.BondingStartDate,
                             DateType = EventDateType.EndPolicyBasedOnEventDate
                        }
                    },
                }

            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15)
            };
            return SavePolicy(policy);
        }

    }
}
