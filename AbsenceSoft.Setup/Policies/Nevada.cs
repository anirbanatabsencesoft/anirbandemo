﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Nevada : PolicyBase
    {
        public override void BuildPolicies()
        {
            SchoolActivities();
            //Remove NV-PDL Policy if it Exists
            var nvPdlPolicy = Policy.GetByCode("NV-PDL");
            if (nvPdlPolicy != null)
            {
                nvPdlPolicy.Delete();
            }

        }

        public static Policy SchoolActivities() // School Activities Leave
        {
            // Selection:
            // - Work State = "NV"
            // - Min 50 employees at employer

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Parental/School

            // Entitlement:
            // - 4 hours per child per school year

            Policy policy = StagePolicy("NV-PARENT", "Nevada School Activities Leave", PolicyType.StateFML, "NV");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.SchoolYearPerContact,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50)
            };
            return SavePolicy(policy);
        }

    
    }
}
