﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class Arkansas : PolicyBase
    {
        public override void BuildPolicies()
        {
            Adoption();
            OrganOrBoneMarrowDonation();
        }

        public static Policy Adoption() // Adoption Leave
        {
            // Selection:
            // - Work State = "AR"
            // - Case Relationship Type = Adopted Child

            // Eligibility:
            // - Base FMLA Eligibility

            // Reasons:
            // - Adoption/Foster Case

            // Entitlement:
            // - Same as FMLA

            Policy policy = StagePolicy("AR-ADOPT", "Arkansas Adoption Leave", PolicyType.StateFML, "AR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,"ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1250, false, true)
            };
            return SavePolicy(policy);
        }

        public static Policy OrganOrBoneMarrowDonation() // Organ or Bone Marrow Donation
        {
            // Selection:
            // - Work State = "AR"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Organ Donor

            // Entitlement:
            // - 90 days per occurrence no limitation by time period

            Policy policy = StagePolicy("AR-DONOR", "Arkansas Organ or Bone Marrow Donation", PolicyType.StateFML, "AR");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 90,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.OrganDonor,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 90,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
