﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Minnesota : PolicyBase
    {
        public override void BuildPolicies()
        {
            Parenting();
            BloodMarrowDonation();
            MIL();
            SafetyLeave();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 1
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1 day
            //      Min Len Service = 1 day
            //      Work State = MN
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            // 
            //      Amount in Time Frame =  1) Ceremony Leave for 1 workday in a calendar year includes send off or return from conflict or Nataional Emergency
            //                              2) 10 workdays for injured or killed service member
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Family see Matrix
            //      Min Length of Military Duty = 89 Days Calendar
            //      Active Military Duty = yes
            //      Search & Rescue = 
            //      National Guard = x
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = only applied to 2nd entitlement rules

            Policy policy = StagePolicy("MN-MIL", "Minnesota Family Military Leave", PolicyType.StateFML, "MN");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "PARENT" , "GRANDPARENT" , "CHILD" 
                            , "ADOPTEDCHILD" , "FOSTERCHILD" , "LEGALWARD" , "SIBLING")
                    },
                    WorkState = "MN",
                    Reason = Reasons.CoveredServiceMember,
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 1,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE" , "PARENT" , "GRANDPARENT" , "CHILD", "ADOPTEDCHILD", "FOSTERCHILD" , "LEGALWARD", "SIBLING",
                            "GRANDCHILD", "GUARDIAN", "FIANCE")
                    },
                    WorkState = "MN",
                    Reason = Reasons.Ceremonial,
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(1));
            policy.RuleGroups.Add(ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty));

            return SavePolicy(policy);
        }

        public static Policy Parenting() // Parenting Leave
        {
            // Selection:
            // - For Parenting and Sick Leave 21 or more ee's at worksite. For school leave no min requirement
            // - 

            // Eligibility:
            // - Lesser of 20 hrs/week or ½ of employers FT equivalency. So if 35 hrs is defined as FT then change to 17.5 Hrs/week
            // - Min 1 year of service

            // Reasons:
            // - Parenting, School Leave

            // Entitlement:
            // - Parenting Leave: 6 weeks for birth or adoption per occurrence not limited by period of time.
            // - School Visitation Leave: 16 hours within 12 month period.

            Policy policy = StagePolicy("MN-FML", "Minnesota Parenting Leave", PolicyType.StateFML, "MN");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 160,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "PARENT", "PARENTINLAW","STEPPARENT", "GRANDPARENT","GRANDCHILD", "CHILD","STEPCHILD","STEPGRANDCHILD", "ADOPTEDGRANDCHILD","FOSTERGRANDCHILD","FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "SIBLING")
                       
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                  new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.Bonding,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,  "CHILD","FOSTERCHILD", "ADOPTEDCHILD")

                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 16,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Minnesota Parenting Leave", MinOneYearServiceRule(), new Rule()
                {
                    Name = "Minimum hours per week",
                    Description = "Minimum hours worked per week is the lesser of 20 hrs/week or ½ of employers FT equivalency",
                    LeftExpression = "HasLesserOfHalfFTHoursOrNHoursPerWeek(20)",
                    Operator = "==",
                    RightExpression = "true"
                })
            };
            return SavePolicy(policy);
        }

        public static Policy BloodMarrowDonation() // Blood Marrow Donation Leave
        {
            // Selection:
            // - Work State = "MN"
            // - Min EE's >= 20

            // Eligibility:
            // - Min 20 hrs/week
            // - Day 1 Eligible

            // Reasons:
            // - Blood Donation

            // Entitlement:
            // - 40 hours per occurrence no limitation on time period
            // - Non-concurrent with FMLA

            Policy policy = StagePolicy("MN-DONOR", "Minnesota Blood Marrow Donation Leave", PolicyType.StateFML, "MN");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 40,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("Minnesota Blood Marrow Donation Leave", MinHoursWorkedPerWeekRule(20))
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(20)
            };
            return SavePolicy(policy);
        }

        public static Policy SafetyLeave() // Safety Leave
        {
            // Selection:
            // - Work State = "MN"

            // Eligibility:
            // - 20 hrs/week scheduled hours minimum
            // - Min 1 year of service
            // - Min 21 Employees at Employer

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - Reasonable period

            Policy policy = StagePolicy("MN-DOM", "Minnesota Safety Leave", PolicyType.StateFML, "MN");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>(1)
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "PARENT", "PARENTINLAW", "STEPPARENT", "GRANDPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "GRANDCHILD", "SIBLING",
                            "STEPGRANDCHILD", "ADOPTEDGRANDCHILD", "FOSTERGRANDCHILD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>(2)
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(21),
                new PolicyRuleGroup()
                {
                    RuleGroupType = PolicyRuleGroupType.Eligibility,
                    Name = "Minnesota Safety Leave Eligibility",
                    Description = "All of the following statements about the Employee must be true:",
                    SuccessType = RuleGroupSuccessType.And,
                    Rules = new List<Rule>()
                    {
                        MinHoursWorkedPerWeekRule(20),
                        MinOneYearServiceRule()
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
