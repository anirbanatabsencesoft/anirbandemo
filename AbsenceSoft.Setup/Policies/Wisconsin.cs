﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Wisconsin : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            Donated();
        }

        public static Policy FML() // Family and Medical Leave
        {
            // Selection:
            // - Work State = "WI"
            // - Min 50 employees

            // Eligibility:
            // - Min 1 year of service
            // - Min 1,000 hours in last 12 months

            // Entitlement:
            // - During a 12-month period:
            //  - 6 weeks for birth or adoption;
            //  - 2 weeks for serious health condition of parent, child or spouse; and
            //  - 2 weeks for employee's own serious health condition.
            //  - employee may not take more than 8 weeks in a year for any combination of leave

            Policy policy = StagePolicy("WI-FML", "Wisconsin Family and Medical Leave", PolicyType.StateFML, "WI");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 2,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 2,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.FamilyHealthCondition,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER", "PARENT", "PARENTINLAW", "STEPPARENT", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "DOMESTICPARTNERSCHILD",
                            "ADOPTIVEPARENT", "PARENTOFADOMESTICPARTNER")
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                  // Bonding
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 2,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    CombinedForSpouses = false,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD","CHILD")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                EligibilityRuleGroup("Wisconsin Family and Medical Leave", MinLengthOfServiceRule(52, Unit.Weeks), WorkedHoursInLast12MonthsRule(1000))
            };
            return SavePolicy(policy);
        }

        public static Policy Donated()
        {
            Policy policy = StagePolicy("WI-DONATE", "Wisconsin Bone Marrow or Organ Donation", PolicyType.StateFML, "WI");
            policy.EffectiveDate = new DateTime(2016, 7, 1);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                /// Marrow Donor
                new PolicyAbsenceReason(){
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                },
                // Organ donor
                new PolicyAbsenceReason(){
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 6,
                    Period = 12,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.OrganDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                },
            };

            policy.RuleGroups = new List<PolicyRuleGroup>(){
                EligibilityRuleGroup(
                    "Wisconsin Bone Marrow or Organ Donation",
                    MinLengthOfServiceRule(12, Unit.Months),
                    WorkedHoursInLast12MonthsRule(1000)
                )
            };


            return SavePolicy(policy);
        }
    }
}
