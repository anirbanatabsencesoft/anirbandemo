﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class NewYork : PolicyBase
    {
        public override void BuildPolicies()
        {
            BloodAndBoneMarrowDonation();
            MIL();
            NewYorkTemporaryDisability();
            PFLBL();
            NewYorkPaidFamilyLeave();
        }

        public static Policy NewYorkPaidFamilyLeave()
        {
            // Selection:
            // - Work State = NY
            // 

            // Eligibility:
            // - FT - 26 consecutive work weeks
            // - PT - 52 consecutive work weeks and 175 days worked in those 52 weeks

            // Reasons:
            // - FHC, BONDING, EXIGENCY

            // Runs AFTER NYDLB (NY-STD), Consecutive To

            Policy policy = StagePolicy("NYPFL", "New York Paid Family Leave", PolicyType.StateFML, "NY");
            policy.Description = "New York Paid Family Leave, effective 1/1/18 with incremental changes in 2019, 2020, 2021.";
            policy.EffectiveDate = new DateTime(2018, 1, 1).ToMidnight();
            policy.PolicyBehaviorType = PolicyBehavior.Ignored;
            policy.ConsecutiveTo = new List<string>(1)
            {
                "NY-STD" // This is really called NYDBL (New York Temporary Disability) but it's coded wrong, oh well.
            };
            policy.RuleGroups = new List<PolicyRuleGroup>(2)
            {

            new PolicyRuleGroup()
                {
                    Name = "FT or PT Consecutive Service & Days Worked",
                    Description = "Full time (more than 20/hrs a week) employees eligible after 26 consecutive weeks employment and Part time (20 hours or less/week) employees eligible after 52 consecutive weeks of employment with 175 days worked in that period",
                    SuccessType = RuleGroupSuccessType.Or,
                    RuleGroupType = PolicyRuleGroupType.Eligibility,
                    Rules = new List<Data.Rules.Rule>(2)
                    {
                        new Data.Rules.Rule()
                        {
                            Name = "FT Consecutive Weeks' Employment",
                            Description = "Employee is full time and has 26 consecutive weeks' employment (from case start date). Full time is defined as working over 20 hours per week by the state of NY for this policy.",
                            LeftExpression = "MeetsNewYorkPflMinLengthOfServiceFT(26, Unit.Weeks)",
                            Operator = "==",
                            RightExpression = "true"
                        },
                        new Data.Rules.Rule()
                        {
                            Name = "PT Consecutive Weeks' Employment and Days Worked",
                            Description = "Employee is part time, has 52 consecutive weeks' employment (from case start date) and has worked at least 175 days in that 52 consecutive week period. Part time is defined as working 20 hours or less per week by the state of NY for this policy.",
                            LeftExpression = "MeetsNewYorkPflMinLengthOfServicePT(52, Unit.Weeks, 175)",
                            Operator = "==",
                            RightExpression = "true"
                        }
                    }
            }
            };

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.FamilyHealthCondition,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8D,       
                    Period = 12D,
                    Paid = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo()
                        {
                            Duration = 8, // the full 8 weeks
                            Order = 1,
                            PaymentPercentage = 0.5M /* 50% */,
                            MinPayAmount = 100M
                        }
                    },
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "PARENT", "GRANDPARENT", "CHILD", "GRANDCHILD", "SPOUSE", "DOMESTICPARTNER","STEPCHILD","STEPPARENT","PARENTINLAW"),
                        IncreaseEntitlementBasedonCaseStartDate(10,new DateTime(2019, 1, 1).ToMidnight())
                    },
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.FromDateOfAbsence,
                    IntermittentRestrictionsMinimum = 1,
                    IntermittentRestrictionsMinimumTimeUnit = EntitlementType.WorkDays,
                    CombinedForSpouses = false,
                    ShowType = PolicyShowType.Always
                },
                // Bonding
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.Bonding,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8D, 
                    Period = 12D,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo()
                        {
                            Duration = 8, // the full 8 weeks
                            Order = 1,
                            PaymentPercentage = 0.5M /* 50% */,
                            MinPayAmount = 100M
                        }
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup("Child", "CHILD", "FOSTERCHILD", "ADOPTEDCHILD"),
                        IncreaseEntitlementBasedonCaseStartDate(10,new DateTime(2019, 1, 1).ToMidnight())
                    },
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.FromDateOfAbsence,
                    IntermittentRestrictionsMinimum = 1,
                    IntermittentRestrictionsMinimumTimeUnit = EntitlementType.WorkDays,
                    CombinedForSpouses = false,
                    ShowType = PolicyShowType.Always
                },
                // Qualifying Exigency (military)
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.QualifyingExigency,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8D, 
                    Period = 12D,
                    Paid = true,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo()
                        {
                            Duration = 8, // the full 8 weeks
                            Order = 1,
                            PaymentPercentage = 0.5M /* 50% */,
                            MinPayAmount = 100M
                        }
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty),
                        IncreaseEntitlementBasedonCaseStartDate(10,new DateTime(2019, 1, 1).ToMidnight())
                    },
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.FromDateOfAbsence,
                    IntermittentRestrictionsMinimum = 1,
                    IntermittentRestrictionsMinimumTimeUnit = EntitlementType.WorkDays,
                    CombinedForSpouses = false,
                    ShowType = PolicyShowType.Always
                },
                // PREGMAT
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.PregnancyMaternity,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8D, 
                    Period = 12,
                    Paid = true,
                    AllowOffset = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    PaymentTiers = new List<PaymentInfo>()
                    {
                        new PaymentInfo()
                        {
                            Duration = 8,
                            Order = 1,
                            PaymentPercentage = 0.5M,
                            MinPayAmount = 100M
                        }
                    },
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        WillUseBonding(),
                        IncreaseEntitlementBasedonCaseStartDate(10,new DateTime(2019, 1, 1).ToMidnight())
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingStartDate,
                                 DateType = EventDateType.StartPolicyBasedOnEventDate
                            },
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingEndDate,
                                 DateType = EventDateType.EndPolicyBasedOnEventDate
                            }
                    },
                    ShowType = PolicyShowType.Always
                },
              
            };            
            return SavePolicy(policy);
        }

        public static void PFLBL()
        {
            // Per Jenn, this needs to die.
            // Obsolete
            Policy policy = Policy.GetByCode("PFLBL", null, null, false);
            if (policy != null)
            {
                policy.Delete();
            }
            policy = Policy.GetByCode("NY-PFLBL", null, null, false);
            if (policy != null)
            {
                policy.Delete();
            }
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 20
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 20hrs/wk
            //      Min Len Service = 1st day eligible
            //      Work State = NY
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame = 10 workdays per Active Military Leave
            //      Runs Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = x
            //      Eligible Family Members = Spouse
            //      Min Length of Military Duty = 
            //      Active Military Duty = x
            //      Search & Rescue = x
            //      National Guard = x
            //      Reserve Training = x
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("NY-MIL", "New York Family Military Leave", PolicyType.StateFML, "NY");

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    WorkState = "NY",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1, 
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,

                    RuleGroups = new List<PolicyRuleGroup>()
                    {                  
                        EmployeeRelationshipRuleGroup(null,  "SPOUSE" ),
                        EligibilityRuleGroup("New York Family Military Leave", MinHoursWorkedPerWeekRule(20), MinLengthOfServiceRule(1, Unit.Days)), 
                    },
                    
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                
                new PolicyAbsenceReason()
                {
                    WorkState = "NY",
                    Reason = Reasons.ReserveTraining,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10, 
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF" , "SPOUSE" ),
                        EligibilityRuleGroup("New York Family Military Leave", MinHoursWorkedPerWeekRule(20), MinLengthOfServiceRule(1, Unit.Days)), 
                    }, 
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(20));
           

            return SavePolicy(policy);
        }

        public static Policy BloodAndBoneMarrowDonation() // Blood and Bone Marrow Donation Leave
        {
            // Selection:
            // - Work State = "NY"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Blood OR Bone Marrow Donation

            // Entitlement:
            // - 24 work hours for Bone Marrow Donation and 3 hours for blood donation in 12 month period

            Policy policy = StagePolicy("NY-DONOR", "New York Blood and Bone Marrow Donation Leave", PolicyType.StateFML, "NY");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 3,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.BloodDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 24,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(20),
                EligibilityRuleGroup("New York Blood and Bone Marrow Donation Leave", MinHoursWorkedPerWeekRule(20))
            };
            return SavePolicy(policy);
        }

        public static Policy NewYorkTemporaryDisability()
        {
            Policy policy = StagePolicy("NY-STD", "New York Temporary Disability", PolicyType.StateDisability, "NY");
            policy.PolicyBehaviorType = PolicyBehavior.Ignored; 
             //Changed Related CaseWindow
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    PaymentTiers = new List<PaymentInfo>() { new PaymentInfo() { Duration = 26, Order = 1, PaymentPercentage = 1.0m}},
                    EliminationType = EntitlementType.CalendarDays,
                    Elimination = 7d,
                    Period = 52,                    
                    PeriodType = PeriodType.RollingBack,
                    PolicyBehaviorType = PolicyBehavior.Ignored,
                    PeriodUnit = Unit.Weeks,
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup(policy.Name, MinLengthOfServiceRule(4, Unit.Weeks)),
                        IsNotWorkRelated()
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    PaymentTiers = new List<PaymentInfo>() { new PaymentInfo() { Duration = 26, Order = 1, PaymentPercentage = 1.0m}},
                    EliminationType = EntitlementType.CalendarDays,
                    Elimination = 7d,
                    Period = 52,
                    PolicyBehaviorType =  PolicyBehavior.Ignored,
                    PeriodType = PeriodType.RollingBack,
                    PolicyEliminationCalcType = PolicyEliminationType.Exclusive,                    
                    PeriodUnit = Unit.Weeks,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup(policy.Name, MinLengthOfServiceRule(4, Unit.Weeks)),
                        IsNotWorkRelated()
                    },
                    Gender = Gender.Female,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingStartDate,
                                 DateType = EventDateType.EndPolicyBasedOnEventDate
                            } 
                    }
                }

            };
            return SavePolicy(policy);
        }
    }
}
