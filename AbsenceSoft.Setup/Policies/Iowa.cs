﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Iowa : PolicyBase
    {
        public override void BuildPolicies()
        {
            PDL();
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "IA"
            // - Females Only
            // - Min Employees >= 4

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - 8 weeks per maternity leave (childbirth) NO 12 month or calendar restriction

            Policy policy = StagePolicy("IA-PDL", "Iowa Pregnancy Disability Leave", PolicyType.StateFML, "IA");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(4)
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
