﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Kentucky : PolicyBase
    {
        public override void BuildPolicies()
        {
            Adoption();
        }

        public static Policy Adoption() // Adoption Leave
        {
            // Selection:
            // - Work State = "KY"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Adoption/Foster Care

            // Entitlement:
            // - Only if child is under age of 7 the employee is entitled to 6 weeks per adoption 
            //  no restrictions on the number of adoptions per 12 month or calendar year 

            Policy policy = StagePolicy("KY-ADOPT", "Kentucky Adoption Leave", PolicyType.StateFML, "KY");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 6,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup("SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(7)
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
