﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class WorkersComp : PolicyBase
    {
        public override void BuildPolicies()
        {
            BuildPolicy();
        }

        public static Policy BuildPolicy()
        {
            Policy policy = StagePolicy("WC", "Workers Comp", PolicyType.WorkersComp);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        new PolicyRuleGroup()
                        {
                            Name = "Work Related",
                            Description = "Leave of absence must be work related",
                            SuccessType = RuleGroupSuccessType.And,
                            RuleGroupType = PolicyRuleGroupType.Selection,
                            Rules = GetIsNotWorkRelatedRules("Is Work Related", "Leave of absence is work related", "IsMetadataValueTrue('IsWorkRelated')", "==","true") 
                            
                        }
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
