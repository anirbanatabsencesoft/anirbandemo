﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class NewMexico : PolicyBase
    {
        public override void BuildPolicies()
        {
            VictimsOfDomesticAbuse();
        }

        public static Policy VictimsOfDomesticAbuse() // Victims of Domestic Abuse Leave
        {
            // Selection:
            // - Work State = "NM"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 14 Work Days in a Calendar Year with no more than 8 hours taken in a workday

            Policy policy = StagePolicy("NM-DOM", "New Mexico Victims of Domestic Abuse Leave", PolicyType.StateFML, "NM");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 14,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
