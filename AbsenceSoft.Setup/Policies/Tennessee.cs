﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Tennessee : PolicyBase
    {
        public override void BuildPolicies()
        {
            Parental();
        }

        public static Policy Parental() // Parental Leave
        {
            // Selection:
            // - Work State = "TN"
            // - Min 100 Employees at Employer

            // Eligibility:
            // - 35 hrs/week (the actual definition is full-time this may need to be adjusted by client)
            // - 12 months min length of service

            // Reasons:
            // - Pregnancy / Maternity, Adoption/Foster

            // Entitlement:
            // - 4 months per child not to be doubled if twins, tripled if triplets, etc.

            Policy policy = StagePolicy("TN-FML", "Tennessee Parental Leave", PolicyType.StateFML, "TN");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingMonths,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Gender = Gender.Female
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingMonths,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingMonths,
                    Entitlement = 4,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    CombinedForSpouses = false,
                    Paid = false
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Tennessee Parental Leave", MinOneYearServiceRule(), 
                    new Rule()
                    {
                        Name = "Employee is full time",
                        Description = "Employee is a full time employee",
                        LeftExpression = "Employee.EmployeeClassCode",
                        Operator = "==",
                        RightExpression = "'" + WorkType.FullTime + "'" 
                    }),
            };

            return SavePolicy(policy);
        }
    }
}
