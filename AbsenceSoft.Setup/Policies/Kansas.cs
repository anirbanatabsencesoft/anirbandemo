﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Kansas : PolicyBase
    {
        public override void BuildPolicies()
        {
            PDL();
            VictimOfDomesticViolence();
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
#warning TODO: State FML: Kansas: Kansas Pregnancy Disability Leave, Needs company policies in place
            // Selection:
            // - Work State = "KS"
            // - Min 4 Employees
            // - Females Only

            // Eligibility:
            // - Same as company policy

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - The leave entitlement will be the exact same as the employers company policy leave for disability 
            //  or medical for self. If no company policy exists this leave should be suppressed

            Policy policy = StagePolicy("KS-PDL", "Kansas Pregnancy Disability Leave", PolicyType.StateFML, "KS", Gender.Female);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement =1,
                    Period =1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateSelectionRuleGroup(4)
                    }
                }
            };
            return SavePolicy(policy);
        }

        public static Policy VictimOfDomesticViolence() // Victims of Domestic Violence or Sexual Assault
        {
            // Selection:
            // - Work State = "KS"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 8 Days in a Calendar year only after all accrued vacation/sick/personal time has been used

            Policy policy = StagePolicy("KS-DOM", "Kansas Victims of Domestic Violence or Sexual Assault", PolicyType.StateFML, "KS");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
