﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Louisiana : PolicyBase
    {
        public override void BuildPolicies()
        {
            PDL();
            BoneMarrowDonation();
            SchoolAndDayCare();
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "LA"
            // - Females Only
            // - Min Employees >= 25

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - Two Rules: 
            //  1) 4 months for pregnancy and or medical complications from childbirth 
            //  2) 6 Weeks for normal delivery

            Policy policy = StagePolicy("LA-PDL", "Louisiana Pregnancy Disability Leave", PolicyType.StateFML, "LA");
            policy.Gender = Gender.Female;
            policy.RuleGroups.Add(MinNumberOfEmployeesSelectionRuleGroup(25));
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 8,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        new PolicyRuleGroup()
                        {
                            Name = "Pregnancy had complications",
                            Description = "Pregnancy had complications entitlement increase to 4 months",
                            Entitlement = (52 / 12) * 4, // 4 Months
                            RuleGroupType = PolicyRuleGroupType.Time,
                            Rules = new List<Rule>(){ new Rule(){Name = "Medical Complications is true",
		                                                                     Description = "Pregnancy had complications",
		                                                                     LeftExpression = "Case.Disability.MedicalComplications",
		                                                                     Operator = "==",
		                                                                     RightExpression = "True",}},
                            SuccessType = RuleGroupSuccessType.And
                        }
                    }
                }
            };
            return SavePolicy(policy);
        }

        public static Policy BoneMarrowDonation() // Bone Marrow Donation Leave
        {
            // Selection:
            // - Work State = "LA"
            // - Min EE's >= 20

            // Eligibility:
            // - Min 20 hrs/week
            // - Day 1 Eligible

            // Reasons:
            // - Blood Donation

            // Entitlement:
            // - Reasonable Period
            // - Non-concurrent with FMLA

            Policy policy = StagePolicy("LA-DONOR", "Louisiana Bone Marrow Donation Leave", PolicyType.StateFML, "LA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("Louisiana Bone Marrow Donation Leave", MinHoursWorkedPerWeekRule(20))
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(20)
            };
            return SavePolicy(policy);
        }

        public static Policy SchoolAndDayCare() // School and Day Care Conference and Activities Leave
        {
            // Selection:
            // - Work State = "LA"

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Parental / School

            // Entitlement:
            // - 16 Hours in 12 month Period

            Policy policy = StagePolicy("LA-PARENT", "Louisiana School and Day Care Conference and Activities Leave", PolicyType.StateFML, "LA");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
