﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Colorado : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            // Adoption is not yet implemented and won't be until we have company policies in place :-(
            //Adoption();
            ParentalInvolvement();
            VictimOfDomesticAbuse();
        }

        public static Policy FML() // Colorado Family Leave
        {
            // Selection:
            // - Work State = CO
            // - Min 50 Employees

            // Eligibility:
            // - 1250 Rule
            // - 12 Months of Service

            // Reasons:
            // - EHC, PREGMAT, ADOPT, FHC

            // Entitlement:
            // - Same as FMLA (Concurrent)

            Policy policy = StagePolicy("CO-FML", "Colorado Family Care Act", PolicyType.StateFML, "CO");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
           
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "DOMESTICPARTNER", "CIVILUNIONPARTNER")
                    }
                }
              
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                FMLEligibilityRuleGroup(1250, false, true, "Colorado Family Leave")
            };
            return SavePolicy(policy);
        }

        public static Policy Adoption() // Colorado Adoption Leave Law
        {
#warning TODO: State FML: Colorado: Colorado Adoption Leave Law, Needs company policies in place
            // Selection:
            // - Employer has company Birth of Child policy, otherwise supress

            // Eligibility:
            // - Same rules coded from the employer's company policy leave on birth.
            //      If no policy exists then this leave type should be suppressed

            // Reasons:
            // - Adoption/Foster Care

            // Entitlement:
            // - The leave entitlement will be the exact same as the employers company policy 
            //      leave for birth of a child. If no company policy exists this leave should be suppressed

            throw new NotImplementedException();
        }

        public static Policy ParentalInvolvement() // Parental Involvement in K-12 Education
        {
            // Obsolete/No Longer in Effect
            Policy policy = Policy.AsQueryable().Where(p => p.Code == "CO-SCHOOL" && p.EmployerId == null && p.CustomerId == null).FirstOrDefault();
            if (policy != null)
            {
                policy?.Delete();
            }
            return policy;
        }

        public static Policy VictimOfDomesticAbuse() // Victims of Domestic Abuse, Sexual Assault or Stalking
        {
            // Selection:
            // - Work State = "CO"
            // - Min Employees = 50

            // Eligibility:
            // - Min 1 year of service

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 3 Days in a 12 month period only after all accrued vacation/sick/personal time has been used

            Policy policy = StagePolicy("CO-DOM", "Colorado Victims of Domestic Abuse, Sexual Assault or Stalking", PolicyType.StateFML, "CO");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 3,
                    Period = 12,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                FMLEligibilityRuleGroup(null, false, true, "Policy")
            };
            return SavePolicy(policy);
        }
    }
}
