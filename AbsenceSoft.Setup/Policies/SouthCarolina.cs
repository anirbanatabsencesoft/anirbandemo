﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class SouthCarolina : PolicyBase
    {
        public override void BuildPolicies()
        {
            BoneMarrowDonation();
        }

        public static Policy BoneMarrowDonation() // Bone Marrow Donation Leave
        {
            // Selection:
            // - Work State = "SC"
            // - Min EE's >= 20

            // Eligibility:
            // - Min 20 hrs/week
            // - Day 1 Eligible

            // Reasons:
            // - Blood Donation

            // Entitlement:
            // - 40 hours per occurrence no limitation on time period
            // - Non-concurrent with FMLA

            Policy policy = StagePolicy("SC-DONOR", "South Carolina Bone Marrow Donation Leave", PolicyType.StateFML, "SC");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 40,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.MarrowDonor,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup("South Carolina Bone Marrow Donation Leave", MinHoursWorkedPerWeekRule(20))
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(20)
            };
            return SavePolicy(policy);
        }
    }
}
