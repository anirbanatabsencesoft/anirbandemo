﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class Company : PolicyBase
    {
        public override void BuildPolicies()
        {
            CompanyMedical();
            Personal();
            Educational();
            JuryDuty();
            PaidMilitary();
            Union();
            Bereavement();
            Administrative();
        }

        public static Policy CompanyMedical()
        {
            Policy policy = StagePolicy("COMP-MED", "Company Medical Leave", PolicyType.Company);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 26,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        ManualOnlyRuleGroup(),
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Company Medical Leave", MinHoursWorkedPerWeekRule(20), MinLengthOfServiceRule(6, Unit.Months))
                    },
                    Elimination = 1 //= 1 calendar week?
                }
            };
            return SavePolicy(policy);
        }

        public static Policy Personal()
        {
            Policy policy = StagePolicy("PER", "Personal Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 30,
                    Period = 12,
                    Reason = Reasons.Personal,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Personal Leave", MinLengthOfServiceRule(90, Unit.Days))
                    },
                }
            };

            return SavePolicy(policy);
        }

        public static Policy Educational()
        {
            Policy policy = StagePolicy("EDU", "Educational Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarYears,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.Educational,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Educational Leave", MinOneYearServiceRule(), MinHoursWorkedPerWeekRule(40))
                    },
                }
            };

            return SavePolicy(policy);
        }

        public static Policy JuryDuty()
        {
            Policy policy = StagePolicy("JURY", "Jury Duty Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    Reason = Reasons.JuryDuty,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    },
                }
            };

            return SavePolicy(policy);
        }

        public static Policy PaidMilitary()
        {
            Policy policy = StagePolicy("PD-MIL", "Paid Military Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarYears,
                    Entitlement = 1,
                    Period = 12,
                    Reason = Reasons.PaidMilitary,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Paid Military Leave", MinLengthOfServiceRule(6, Unit.Months))
                    },
                }
            };

            return SavePolicy(policy);
        }

        public static Policy Union()
        {
            Policy policy = StagePolicy("UN", "Union Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarMonths,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.Union,
                    CaseTypes = CaseType.Consecutive,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    },
                }
            };

            return SavePolicy(policy);
        }

        public static Policy Bereavement()
        {
            //Entitlement based on relationship to employee
            //Some family members qualify for 30 days (spouse, parent, child, adopted child, step child, foster child and step parent )
            //All other = 3 days

            string[] types = new string[7];
            types[0] = "SPOUSE";
            types[1] = "PARENT";
            types[2] = "CHILD";
            types[3] = "ADOPTEDCHILD";
            types[4] = "STEPCHILD";
            types[5] = "FOSTERCHILD";
            types[6] = "STEPPARENT";

            Policy policy = StagePolicy("BRVMT", "Bereavement Leave", PolicyType.Company);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.CalendarDays,
                    Entitlement = 3,
                    Period = 12,
                    Reason = Reasons.Bereavement,
                    CaseTypes = CaseType.Consecutive,
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Bereavement Leave", MinLengthOfServiceRule(90, Unit.Days)),
                new PolicyRuleGroup()
                {
                    Name = "Relationship Type Entitlement Adjustment",
                    Description = "Adjust entitlement if bereavement is for an immediate family member",
                    Entitlement = 30,
                    RuleGroupType = PolicyRuleGroupType.Time,
                    SuccessType = RuleGroupSuccessType.Or,
                    Rules = types.Select(t => new Rule()
                    {
                        Name = t.SplitCamelCaseString(),
                        Description = "Relationship to Employee is " + t.SplitCamelCaseString(),
                        LeftExpression = "CaseRelationshipType",
                        Operator = "==",
                        RightExpression = "'" + t.ToString() + "'"
                    }).ToList(),
                }
            };

            return SavePolicy(policy);
        }

        public static Policy Administrative()
        {
            Policy policy = StagePolicy("ADMIN", "Administrative Leave", PolicyType.Company);

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    Reason = Reasons.Administrative,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    },
                }
            };

            return SavePolicy(policy);
        }

    }
}
