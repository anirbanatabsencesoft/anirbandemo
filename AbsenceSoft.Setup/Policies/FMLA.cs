﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public class FMLA : PolicyBase
    {
        public override void BuildPolicies()
        {
            BuildPolicy();
        }

        public static Policy BuildPolicy()
        {
            Policy policy = StagePolicy("FMLA", "Family Medical Leave Act", PolicyType.FMLA);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                      PolicyEliminationCalcType = PolicyEliminationType.Inclusive
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "STEPPARENT", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "SAMESEXSPOUSE","INLOCOPARENTIS"),
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Gender = Gender.Female,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {                      
                        BondingPeriodRuleGroup(12, "Delivery / Expected Delivery Date", "NumberOfMonthsFromDeliveryDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    PolicyEvents = new List<PolicyEvent>(){new PolicyEvent(){EventType=CaseEventType.DeliveryDate, DateType=EventDateType.EndPolicyBasedOnPeriod, EventPeriod=12}}
                },
                // Bonding
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD"),
                        BondingPeriodRuleGroup(12, "Delivery / Expected Delivery Date", "NumberOfMonthsFromDeliveryDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    PolicyEvents = new List<PolicyEvent>(){new PolicyEvent(){EventType=CaseEventType.DeliveryDate, DateType=EventDateType.EndPolicyBasedOnPeriod, EventPeriod=12}}
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18),
                        BondingPeriodRuleGroup(12, "Adoption / Foster Care Start Date", "NumberOfMonthsFromAdoptionDate")
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    PolicyEvents = new List<PolicyEvent>(){new PolicyEvent(){EventType=CaseEventType.AdoptionDate, DateType=EventDateType.EndPolicyBasedOnPeriod, EventPeriod=12}}
                },
                // Covered Service Member
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    Period = 12,
                    Reason = Reasons.CoveredServiceMember,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup("Service Member", "SPOUSE", "PARENT", "CHILD","NEXTOFKIN"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty, MilitaryStatus.Veteran)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive
                },
                // Qualifying Exigency (max 15 work days per occurrance, max 12 weeks total)
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    PerUseCap = 15,
                    PerUseCapType = EntitlementType.WorkDays,
                    Reason = Reasons.QualifyingExigency,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,"SPOUSE", "PARENT", "CHILD"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive
                },
                // Ceremony / Send Off 
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.Ceremonial,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,"SPOUSE", "PARENT", "CHILD"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
                    },
                    Elimination = 3,
                    EliminationType = EntitlementType.CalendarDays,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                 FMLEligibilityRuleGroup(1250, true, true),
                //MinNumberOfEmployeesSelectionRuleGroup(50)
            };
            policy.AirlineFlightCrew = false;
            return SavePolicy(policy);
        }
    }
}
