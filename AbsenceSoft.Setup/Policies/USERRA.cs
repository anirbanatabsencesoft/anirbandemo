﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class USERRA : PolicyBase
    {
        public override void BuildPolicies()
        {
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 1
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1 day
            //      Min Len Service = 1 day
            //      Work State = ALL
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  5 years
            //      Concurrent Exception = N/A
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Self
            //      Min Length of Military Duty = 
            //      Active Military Duty = 
            //      Search & Rescue = 
            //      National Guard = 
            //      Reserve Training = 
            //      Duty Outside US Required = 
            //      Only for injured or killed service member =

            Policy policy = StagePolicy("USERRA", "Uniformed Services Employment and Reemployment Rights Act (USERRA)", PolicyType.FMLA);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.ActiveDuty,
                    EntitlementType = EntitlementType.CalendarYears,
                    Entitlement = 5,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive,
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                new PolicyAbsenceReason()
                {
                    Reason = Reasons.ReserveTraining,
                    EntitlementType = EntitlementType.CalendarMonths,
                    Entitlement = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            return SavePolicy(policy);
        }
 
    }
}