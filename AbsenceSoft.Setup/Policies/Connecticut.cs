﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class Connecticut : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            PDL();
            VictimsOfFamilyViolence();
        }

        public static Policy FML() // Family and Medical Leave
        {
            // Selection:
            // - Work State = "CT"
            // - (for private companies supposed to be min 75 employees)

            // Eligibility:
            // - 

            // Reasons:
            // - EHC, PREGMAT, ADOPT, FHC, ORGAN, MARROW

            // Entitlement:
            // - 16 Weeks in a 24 Month Period for Privately Held Companies
            // - 24 Weeks in a 24 Month Period for Publicly Held Companies

            Policy policy = StagePolicy("CT-FML", "Connecticut Family and Medical Leave", PolicyType.StateFML, "CT");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Employee Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    }
                },
                // Family Health Condition
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.FamilyHealthCondition,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "PARENTINLAW", "FOSTERPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "STEPPARENT", "INLOCOPARENTIS", "ADOPTIVEPARENT")
                    }
                },
                // Pregnancy/Maternity
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.PregnancyMaternity,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Gender = Gender.Female
                },
                // Adoption/Foster Care
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    },
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.MarrowDonor,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.OrganDonor,
                    CombinedForSpouses = true,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 16,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    Reason = Reasons.QualifyingExigency,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "CHILD", "PARENT", "NEXTOFKIN"),
                        ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty)
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1000, false, true, "Connecticut Family and Medical Leave"),
                new PolicyRuleGroup()
                {
                    Name = "Minimum Employees",
                    Description = "Privately Held Companies must have at least 75 employees in Connecticut to qualify",
                    RuleGroupType = PolicyRuleGroupType.Eligibility,
                    SuccessType = RuleGroupSuccessType.Or,
                    Rules = new List<Rule>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateRule(75)
                    }
                }
            };
            return SavePolicy(policy);
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "CT"
            // - Female
            // - Min Employees = 3

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - A Reasonable Amount of Time, sheesh

            Policy policy = StagePolicy("CT-PDL", "Connecticut Pregnancy Disability Leave", PolicyType.StateFML, "CT");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(3)
            };
            return SavePolicy(policy);
        }

        public static Policy VictimsOfFamilyViolence() // Victims of Family Violence Leave
        {
            // Selection:
            // - Work State = "CT"
            // - Min Employees = 3

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 12 Work Days  in 1 Year Period

            Policy policy = StagePolicy("CT-DOM", "Connecticut Victims of Family Violence Leave", PolicyType.StateFML, "CT");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    ShowType = PolicyShowType.OnlyTimeUsed,
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 12,
                    Period = 1,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF")
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(3)
            };
            return SavePolicy(policy);
        }
    }
}
