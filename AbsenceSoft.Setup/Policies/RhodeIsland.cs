﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Policies
{
    public class RhodeIsland : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            SchoolInvolvement();
            TemporaryCaregiver();
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 15
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = RI
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  1) 15-50 ee's 15 workdays per occurrence
            //                              2) > 50 ee's 30 workdays per occurrence
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = spouse & child
            //      Min Length of Military Duty = 30 Calendar Days
            //      Active Military Duty = yes 
            //      Search & Rescue = yes
            //      National Guard = yes
            //      Reserve Training = yes
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("RI-MIL", "Rhode Island Family Military Leave", PolicyType.StateFML, "RI");
           
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                // Family Military Leave
                new PolicyAbsenceReason()
                {
                    WorkState = "RI",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 15,
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,  "SPOUSE" , "CHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD"),   
                        EligibilityRuleGroup("Rhode Island Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250),  MinLengthOfServiceRule(30, Unit.Days)),
                        
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer in Rhode Island",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                               
                            }
                        },
                        
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                },

                new PolicyAbsenceReason()
                {
                    WorkState = "RI",
                    Reason = Reasons.ReserveTraining,
                    EntitlementType = EntitlementType.CalendarMonths,
                    Entitlement = 15, 
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        
                        EmployeeRelationshipRuleGroup(null,  "SPOUSE" , "CHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD" ),
                        EligibilityRuleGroup("Rhode Island Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250),  MinLengthOfServiceRule(30, Unit.Days)),
                                                
                        new PolicyRuleGroup()
                        {
                            Name = "Min Employee Entitlement Adjustment",
                            Description = "Adjust entitlement based on minimum number of employees at employer in Rhode Island",
                            Entitlement = 30,
                            RuleGroupType = PolicyRuleGroupType.Time,
                            SuccessType = RuleGroupSuccessType.And,
                            Rules = new List<Rule>()
                            {
                                MinNumberOfEmployeesInTheSameWorkStateRule(50)
                            }
                        }
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }

            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(15));
            

            return SavePolicy(policy);
        }

        public static Policy FML() // Parental and Family Medical Leave
        {
            // Selection:
            // - Work State = RI
            // - Min 50 Employees

            // Eligibility:
            // - Min 30 hrs/week
            // - Min 12 months of service

            // Reasons:
            // - Bonding, Adoption, Foster Care; FHC

            // Entitlement:
            // - 13 weeks in a 2 Calendar year pressence

            Policy policy = StagePolicy("RI-FML", "Rhode Island Parental and Family Medical Leave", PolicyType.StateFML, "RI");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 13,
                    Period = 2,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER", "CIVILUNIONPARTNER", "PARENT", "PARENTINLAW", "CHILD", "STEPCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 13,
                    Period = 2,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.Bonding,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 13,
                    Period = 2,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        WillUseBonding()
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                            DateType = EventDateType.StartPolicyBasedOnEventDate,
                            EventType = CaseEventType.BondingStartDate
                        },
                        new PolicyEvent()
                        {
                            DateType = EventDateType.EndPolicyBasedOnEventDate,
                            EventType = CaseEventType.BondingEndDate
                        }
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 13,
                    Period = 2,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(16)
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                },
                  new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 13,
                    Period = 2,
                    PeriodType = PeriodType.CalendarYear,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                EligibilityRuleGroup("Rhode Island Parental and Family Medical Leave", MinHoursWorkedPerWeekRule(30), MinOneYearServiceRule())
            };
            return SavePolicy(policy);
        }

        public static Policy SchoolInvolvement() // School Involvement Leave
        {
            // Selection:
            // - Work State = "RI"
            // - Min 50 Employees

            // Eligibility:
            // - Min 12 months of service

            // Reasons:
            // - Parental/School

            // Entitlement:
            // - 10 hours per 12 month period

            Policy policy = StagePolicy("RI-PARENT", "Rhode Island School Involvement Leave", PolicyType.StateFML, "RI");
            policy.ConsecutiveTo.Add("FMLA");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 10,
                    Period = 12,
                    Reason = Reasons.ParentalSchool,
                    CaseTypes = CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50),
                EligibilityRuleGroup("Rhode Island School Involvement Leave", MinOneYearServiceRule())
            };
            return SavePolicy(policy);
        }

        public static Policy TemporaryCaregiver() // Temporary Caregiver Insurance Leave & Paid Family Leave
        {
            // Selection:
            // - Work State = "RI" OR Residence State = "RI"

            // Eligibility:
            // - EE earning $9,300 in a base period
            // - Contributes to the RI disability plan through payroll deductions

            // Reasons:
            // - Birth, Adoption, Foster Care; FHC

            // Entitlement:
            // - 4 weeks in a 12 month period

            // Elimination Period:
            // - 7 Calendar Days

            Policy policy = StagePolicy("RI-CAREGIVER", "Rhode Island Temporary Caregiver Insurance Leave & Paid Family Leave", PolicyType.StateFML, "RI");
            policy.ResidenceState = "RI";
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "DOMESTICPARTNER", "PARENT", "PARENTINLAW", "GRANDPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive,
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 4,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive,
                    Elimination = 7,
                    EliminationType = EntitlementType.CalendarDays,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18)
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                EligibilityRuleGroup("Rhode Island Temporary Caregiver Insurance Leave & Paid Family Leave",
                    IsMetadataValueTrueRule("ContributesToRIDisability", "Contributes to the RI disability plan through payroll deductions"),
                    new Rule()
                    {
                        Name = "Employee earns at least $9,300 in a base period",
                        Description = "Employee earns at least $9,300 in a base period",
                        LeftExpression = "EffectiveAnnualPayFromLeaveStartDate()",
                        Operator = ">=",
                        RightExpression = "9300"
                    })
            };
            return SavePolicy(policy);
        }
    }
}
