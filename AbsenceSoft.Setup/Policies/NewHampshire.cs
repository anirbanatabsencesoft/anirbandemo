﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class NewHampshire : PolicyBase
    {
        public override void BuildPolicies()
        {
            PDL();
        }

        public static Policy PDL() // Pregnancy Disability Leave
        {
            // Selection:
            // - Work State = "NH"
            // - Females Only

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - Reasonable period of time

            Policy policy = StagePolicy("NH-PDL", "New Hampshire Pregnancy Disability Leave", PolicyType.StateFML, "NH");
            policy.Gender = Gender.Female;
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.PregnancyMaternity,
                    Gender = Gender.Female,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(6)
                    }
                }
            };
            return SavePolicy(policy);
        }
    }
}
