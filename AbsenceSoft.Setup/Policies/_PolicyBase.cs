﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Policies
{
    public abstract class PolicyBase
    {
        public static class Reasons
        {
            public static AbsenceReason EmployeeHealthCondition { get { return AbsenceReason.GetByCode("EHC"); } }
            public static AbsenceReason FamilyHealthCondition { get { return AbsenceReason.GetByCode("FHC"); } }
            public static AbsenceReason PregnancyMaternity { get { return AbsenceReason.GetByCode("PREGMAT"); } }
            public static AbsenceReason Bonding { get { return AbsenceReason.GetByCode("BONDING"); } }
            public static AbsenceReason AdoptionFosterCare { get { return AbsenceReason.GetByCode("ADOPT"); } }
            public static AbsenceReason CoveredServiceMember { get { return AbsenceReason.GetByCode("MILITARY"); } }
            public static AbsenceReason QualifyingExigency { get { return AbsenceReason.GetByCode("EXIGENCY"); } }
            public static AbsenceReason ParentalSchool { get { return AbsenceReason.GetByCode("PARENTAL"); } }
            public static AbsenceReason DomesticViolence { get { return AbsenceReason.GetByCode("DOM"); } }
            public static AbsenceReason OrganDonor { get { return AbsenceReason.GetByCode("ORGAN"); } }
            public static AbsenceReason MarrowDonor { get { return AbsenceReason.GetByCode("BONE"); } }
            public static AbsenceReason BloodDonor { get { return AbsenceReason.GetByCode("BLOOD"); } }
            public static AbsenceReason ReserveTraining { get { return AbsenceReason.GetByCode("RESERVETRAIN"); } }
            public static AbsenceReason CriminalProceeding { get { return AbsenceReason.GetByCode("CRIME"); } }
            public static AbsenceReason Ceremonial { get { return AbsenceReason.GetByCode("CEREMONY"); } }
            public static AbsenceReason Personal { get { return AbsenceReason.GetByCode("PERSONAL"); } }
            public static AbsenceReason Educational { get { return AbsenceReason.GetByCode("EDUCATIONAL"); } }
            public static AbsenceReason JuryDuty { get { return AbsenceReason.GetByCode("JURY"); } }
            public static AbsenceReason PaidMilitary { get { return AbsenceReason.GetByCode("MILITARY"); } }
            public static AbsenceReason Union { get { return AbsenceReason.GetByCode("UNION"); } }
            public static AbsenceReason Bereavement { get { return AbsenceReason.GetByCode("BEREAVEMENT"); } }
            public static AbsenceReason Administrative { get { return AbsenceReason.GetByCode("ADMIN"); } }
            public static AbsenceReason Accommodation { get { return AbsenceReason.GetByCode("ACCOMM"); } }
            public static AbsenceReason ActiveDuty { get { return AbsenceReason.GetByCode("ACTIVEDUTY"); } }
            public static AbsenceReason CivicService { get { return AbsenceReason.GetByCode("CIVICSERVICE"); } }
            public static AbsenceReason ParentalLeave { get { return AbsenceReason.GetByCode("PARENTALLEAVE"); } }
            public static AbsenceReason LifeThreateningMedicalCondition { get { return AbsenceReason.GetByCode("LFTHRMEDCOND"); } }
        }

        public abstract void BuildPolicies();

        public static Policy StagePolicy(string code, string name, PolicyType type, string workState = null, Gender? gender = null, string customerId = null, string employerId = null)
        {
            Console.WriteLine("Creating policy '{0}'", code);
            Policy policy = Policy.AsQueryable().Where(p => p.Code == code && p.EmployerId == employerId && p.CustomerId == customerId).FirstOrDefault() ?? new Policy() { Code = code };
            policy.Name = name;
            policy.PolicyType = type;
            policy.EffectiveDate = new DateTime(1970, 1, 1).ToMidnight(); // Fake effective date
            policy.WorkState = workState;
            policy.Gender = gender;
            policy.CustomerId = customerId;
            policy.EmployerId = employerId;
            policy.PolicyBehaviorType = PolicyBehavior.ReducesWorkWeek;
            policy.RuleGroups.Clear();
            policy.AbsenceReasons.Clear();
            policy.ResidenceState = null;
            policy.ConsecutiveTo.Clear();
            policy.CreatedById = policy.CreatedById ?? User.DefaultUserId;
            policy.ModifiedById = policy.ModifiedById ?? User.DefaultUserId;
            return policy;
        }
        public static PolicyRuleGroup WillUseBonding()
        {
            return new PolicyRuleGroup()
            {
                Name = "Will use bonding",
                Description = "Leave of absence is bonding related",
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Selection,
                Rules = GetIsNotWorkRelatedRules("Will use bonding", "Leave of absence is bonding related", "WillUseBonding", "==", "true")
            };
        }
        public static Policy SavePolicy(Policy policy)
        {
            policy.CreatedById = policy.CreatedById ?? "000000000000000000000000";
            policy.ModifiedById = "000000000000000000000000";
            policy.Save();
            Console.WriteLine("Saved policy '{0}' with Id '{1}'", policy.Code, policy.Id);
            return policy;
        }

        public static PolicyRuleGroup ManualOnlyRuleGroup(PolicyRuleGroupType type = PolicyRuleGroupType.Selection)
        {
            return new PolicyRuleGroup()
            {
                Name = "NEVER Selected",
                Description = "This policy is currently never auto-selected:",
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = type,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        Name = "1 equals 0",
                        Description = "The Number 1 MUST equal the Number 0",
                        LeftExpression = "1",
                        Operator = "==",
                        RightExpression = "0",
                    }
                }
            };
        }



        public static PolicyRuleGroup EmployeeRelationshipRuleGroup(string who, params string[] types)
        {
            string typeString = string.Join(",", types);
            string who2 = who ?? "";
            if (!string.IsNullOrWhiteSpace(who))
            {
                who2 = who2 + "'";
                if (!who.EndsWith("s"))
                    who2 += "s";
                who2 += " ";
            }
            return new PolicyRuleGroup()
            {
                Name = who2 + "Relationship to Employee",
                Description = string.Format("One of the following statements about \"{0}Relationship to Employee\" must be true:", who2),
                SuccessType = RuleGroupSuccessType.Or,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        Name = "Relationship To Employee Rule",
                        Description = "The relationship to employee should be one of the below",
                        LeftExpression = "CaseRelationshipType",
                        Operator = "::",
                        RightExpression = "'" + typeString + "'"
                    }
                }
            };
        }

        public static PolicyRuleGroup EmployeeRelationshipExceptForSpecificStatesGroups(string who, string state, params string[] types)
        {
            string who2 = who ?? "";
            if (!string.IsNullOrWhiteSpace(who))
            {
                who2 = who2 + "'";
                if (!who.EndsWith("s"))
                    who2 += "s";
                who2 += " ";
            }
            var prg = new PolicyRuleGroup()
            {
                Name = who2 + string.Format("Relationship to Employee Outside of '{0}'", state),
                Description = string.Format("One of the following statements about \"{0}Relationship to Employee\" OR Work State must be false:", who2),
                SuccessType = RuleGroupSuccessType.Nor,
                RuleGroupType = PolicyRuleGroupType.Selection,
                Rules = types.Select(t => new Rule()
                {
                    Name = t.SplitCamelCaseString(),
                    Description = "is for the employee's " + t.SplitCamelCaseString(),
                    LeftExpression = "CaseRelationshipType",
                    Operator = "==",
                    RightExpression = "'" + t.ToString() + "'"
                }).ToList()
            };
            prg.Rules.Insert(0, new Rule()
            {
                Name = "Work State is " + state,
                Description = "Where the employee's work state is " + state,
                LeftExpression = "Employee.WorkState",
                Operator = "==",
                RightExpression = "'" + state + "'"
            });
            return prg;
        }

        public static PolicyRuleGroup EmployeeRelationshipNotRuleGroup(string who, params string[] types)
        {
            string who2 = who ?? "";
            if (!string.IsNullOrWhiteSpace(who))
            {
                who2 = who2 + "'";
                if (!who.EndsWith("s"))
                    who2 += "s";
                who2 += " ";
            }
            return new PolicyRuleGroup()
            {
                Name = who2 + "Relationship to Employee",
                Description = string.Format("One of the following statements about \"{0}Relationship to Employee\" must be true:", who2),
                SuccessType = RuleGroupSuccessType.Not,
                RuleGroupType = PolicyRuleGroupType.Selection,
                Rules = types.Select(t => new Rule()
                {
                    Name = "Not " + t.SplitCamelCaseString(),
                    Description = "is not for the employee's " + t.SplitCamelCaseString(),
                    LeftExpression = "CaseRelationshipType",
                    Operator = "==",
                    RightExpression = "'" + t.ToString() + "'"
                }).ToList(),
            };
        }

        public static PolicyRuleGroup IncreaseEntitlementBasedonCaseStartDate(double entitlementValue,DateTime caseStartDateGreaterThan)
        {
            return new PolicyRuleGroup()
            {
                Name = string.Format("Increase Entitlement to {0} weeks",entitlementValue.ToString()),
                Description = string.Format("Increases entitlement If a Case Created on or after {0}", caseStartDateGreaterThan.ToString()),
                Entitlement = entitlementValue,
                RuleGroupType = PolicyRuleGroupType.Time,
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    new Rule()
                    {
                        Name = "Case's Start Date",
                        Description = string.Format("Employee Case Start Date should be greater or eaqual {0} ","01/01/2019" ),
                        LeftExpression = "Case.StartDate",
                        Operator = ">=",
                        RightExpression = $"#{caseStartDateGreaterThan:yyyy-MM-dd}#"
                    }
                }
            };
        }

        public static PolicyRuleGroup ContactMilitaryStatusRuleGroup(params MilitaryStatus[] status)
        {
            return new PolicyRuleGroup()
            {
                Name = "Service Member's Military Status",
                Description = "One of the following statements about \"Service Member's Military Status\" must be true:",
                SuccessType = RuleGroupSuccessType.Or,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = status.Select(t => new Rule()
                {
                    Name = t.ToString().SplitCamelCaseString(),
                    Description = "Service member's military status is " + t.ToString().SplitCamelCaseString(),
                    LeftExpression = "Case.Contact.MilitaryStatus",
                    Operator = "==",
                    RightExpression = "MilitaryStatus." + t.ToString()
                }).ToList()
            };
        }

        public static PolicyRuleGroup EmployeeOrContactMilitaryStatusRuleGroup(params MilitaryStatus[] status)
        {
            return new PolicyRuleGroup()
            {
                Name = "Service Member's Military Status",
                Description = "One of the following statements about \"Service Member's Military Status\" must be true:",
                SuccessType = RuleGroupSuccessType.Or,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = status.Select(t => new Rule()
                {
                    Name = t.ToString().SplitCamelCaseString(),
                    Description = "Service member's military status is " + t.ToString().SplitCamelCaseString(),
                    LeftExpression = "Case.Contact.MilitaryStatus",
                    Operator = "==",
                    RightExpression = "MilitaryStatus." + t.ToString()
                }).ToList().AddRangeFluid(status.Select(t => new Rule()
                {
                    Name = t.ToString().SplitCamelCaseString(),
                    Description = "Employee's military status is " + t.ToString().SplitCamelCaseString(),
                    LeftExpression = "Employee.MilitaryStatus",
                    Operator = "==",
                    RightExpression = "MilitaryStatus." + t.ToString()
                }))
            };
        }

        public static PolicyRuleGroup EmployeeMilitaryStatusRuleGroup(params MilitaryStatus[] status)
        {
            return new PolicyRuleGroup()
            {
                Name = "Employee's Military Status",
                Description = "One of the following statements about \"Employee's Military Status\" must be true:",
                SuccessType = RuleGroupSuccessType.Or,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = status.Select(t => new Rule()
                {
                    Name = t.ToString().SplitCamelCaseString(),
                    Description = "Employee's military status is " + t.ToString().SplitCamelCaseString(),
                    LeftExpression = "Employee.MilitaryStatus",
                    Operator = "==",
                    RightExpression = "MilitaryStatus." + t.ToString()
                }).ToList(),
            };
        }

        public static PolicyRuleGroup FMLEligibilityRuleGroup(int? hoursInLast12Months, bool use50in75Rule, bool use1YearServiceRule, string policyName = "FMLA", bool useKeyEmployeeRule = false)
        {
            var fmlaEligibility = new PolicyRuleGroup()
            {
                Name = ((policyName??"") + " Eligibility").Trim(),
                Description = "All of the following statements about the Employee must be true:",
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = new List<Rule>()
            };
            if (hoursInLast12Months.HasValue)
                fmlaEligibility.Rules.Add(WorkedHoursInLast12MonthsRule(hoursInLast12Months.Value));
            if (use50in75Rule)
                fmlaEligibility.Rules.Add(EmployeeMeets50In75Rule());
            if (use1YearServiceRule)
                fmlaEligibility.Rules.Add(MinOneYearServiceRule());
            if (useKeyEmployeeRule)
                fmlaEligibility.Rules.Add(KeyEmployeeRule());

            return fmlaEligibility;
        }

        public static PolicyRuleGroup EligibilityRuleGroup(string policyName, params Rule[] rules)
        {
            return new PolicyRuleGroup()
            {
                Name = ((policyName ?? "") + " Eligibility").Trim(),
                Description = "All of the following statements about the Employee must be true:",
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = rules.ToList()
            };
        }

        public static PolicyRuleGroup MinNumberOfEmployeesSelectionRuleGroup(int employees)
        {
            return new PolicyRuleGroup()
            {
                Name = "Minimum Employees at Employer",
                Description = string.Format("The Employer has at least {0:n0} employees", employees),
                RuleGroupType = PolicyRuleGroupType.Selection,
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    MinNumberOfEmployeesRule(employees)
                }
            };
        }

        public static Rule MinNumberOfEmployeesRule(int employees)
        {
            return new Rule()
            {
                Name = string.Format("Employer has at least {0:n0} Employees", employees),
                Description = string.Format("employer has at least {0:n0} employees", employees),
                LeftExpression = "TotalEmployeesAtEmployer()",
                Operator = ">=",
                RightExpression = employees.ToString()
            };
        }
        
        public static PolicyRuleGroup MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(int employees)
        {
            return new PolicyRuleGroup()
            {
                Name = "Minimum Employees at Employer with the same Work State",
                Description = string.Format("The Employer has at least {0:n0} employees with the same work state", employees),
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    MinNumberOfEmployeesInTheSameWorkStateRule(employees)
                }
            };
        }

        public static PolicyRuleGroup MinNumberOfEmployeesInTheSameWorkStateSelectionRuleGroup(int employees)
        {
            return new PolicyRuleGroup()
            {
                Name = "Minimum Employees at Employer with the same Work State",
                Description = string.Format("The Employer has at least {0:n0} employees with the same work state", employees),
                RuleGroupType = PolicyRuleGroupType.Selection,
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {
                    MinNumberOfEmployeesInTheSameWorkStateRule(employees)
                }
            };
        }

        public static Rule MinNumberOfEmployeesInTheSameWorkStateRule(int employees)
        {
            return new Rule()
            {
                Name = string.Format("Employer has at least {0:n0} Employees with the same work state", employees),
                Description = string.Format("employer has at least {0:n0} employees with the same work state", employees),
                LeftExpression = "TotalEmployeesAtEmployerInTheSameWorkState()",
                Operator = ">=",
                RightExpression = employees.ToString()
            };
        }

        public static Rule KeyEmployeeRule()
        {
            return new Rule()
            {
                Name = "Key Employees Ineligible",
                Description = "Employee may not be a key employee",
                LeftExpression = "Employee.IsKeyEmployee",
                Operator = "==",
                RightExpression = "false"
            };
        }

        public static Rule WorkedHoursInLast12MonthsRule(int hours)
        {
            return new Rule()
            {
                Name = string.Format("{0:n0} Hours Worked", hours),
                Description = string.Format("The Employee has worked at least {0:n0} hours in the last 12 months", hours),
                LeftExpression = "TotalHoursWorkedLast12Months()",
                Operator = ">=",
                RightExpression = hours.ToString()
            };
        }

        public static Rule EmployeeMeets50In75Rule()
        {
            return new Rule()
            {
                Name = "50 Employees in 75 Mile Radius Rule",
                Description = "The Employee works in an office with at least 50 employees that all work within a 75 mile radius",
                LeftExpression = "Meets50In75MileRule()",
                Operator = "==",
                RightExpression = "true"
            };
        }

        public static Rule MinOneYearServiceRule()
        {
            return new Rule()
            {
                Name = "Worked 12 Months",
                Description = "The Employee has worked at least 12 months",
                LeftExpression = "Has1YearOfService()",
                Operator = "==",
                RightExpression = "true"
            };
        }

        public static Rule MinLengthOfServiceRule(int minLengthOfService, Unit unitsType)
        {
            return new Rule()
            {
                Name = string.Format("Worked {0} {1}", minLengthOfService, unitsType),
                Description = string.Format("The Employee has worked at least {0} {1}", minLengthOfService, unitsType),
                LeftExpression = string.Format("HasMinLengthOfService({0}, Unit.{1})", minLengthOfService, unitsType.ToString()),
                Operator = "==",
                RightExpression = "true"
            };
        }

        public static Rule MinHoursWorkedPerWeekRule(int hoursPerWeek)
        {
            return new Rule()
            {
                Name = string.Format("Works minimum of {0} hours per week", hoursPerWeek),
                Description = string.Format("The Employee works at least {0} per week", hoursPerWeek),
                LeftExpression = "HoursWorkedPerWeek()",
                Operator = ">=",
                RightExpression = hoursPerWeek.ToString()
            };
        }

        public static Rule AverageHoursWorkedPerWeekOverPeriodRule(int hoursPerWeek, int minLengthOfService, Unit unitsType)
        {
            return new Rule()
            {
                Name = string.Format("Minimum of {0} average hours per week", hoursPerWeek),
                Description = string.Format("The Employee works at least {0} hours in average per week", hoursPerWeek),
                LeftExpression = string.Format("AverageHoursWorkedPerWeekOverPeriod({0},{1}, Unit.{2})", hoursPerWeek, minLengthOfService, unitsType),
                Operator = ">=",
                RightExpression = hoursPerWeek.ToString()
            };
        }

        public static Rule IsMetadataValueTrueRule(string fieldName, string description)
        {
            return new Rule()
            {
                Name = string.Format("{0} is true", fieldName),
                Description = description ?? string.Format("{0} is true", fieldName),
                LeftExpression = string.Format("IsMetadataValueTrue('{0}')", fieldName),
                Operator = "==",
                RightExpression = "true"
            };
        }

         public static List<Rule> GetIsNotWorkRelatedRules(string name,string description,string leftExpression,string op,string rightExpression)
        {
           return new List<Rule>()
                {
                    new Rule()
                    {
                        Name = name,
                        Description =description ,
                        LeftExpression =leftExpression,
                        Operator =op,
                        RightExpression = rightExpression
                    }
                };
         }
        public static PolicyRuleGroup IsNotWorkRelated()
        {
            return new PolicyRuleGroup
            {
                Name = "Not Work Related",
                Description = "Leave of absence must not be work related",
                SuccessType = RuleGroupSuccessType.Not,
                RuleGroupType = PolicyRuleGroupType.Selection,
                Rules = GetIsNotWorkRelatedRules("Is Work Related", "Leave of absence is work related", "IsMetadataValueTrue(\'IsWorkRelated\')", "==", "true")
            };
        }

        public static Rule ContactMaxAgeRule(int? maxAgeInYears, string description = null)
        {
            return new Rule()
            {
                Name = string.Format("Contact is less than {0} years old", maxAgeInYears),
                Description = description ?? string.Format("Contact is less than {0} years old", maxAgeInYears),
                LeftExpression = "ContactAgeInYears()",
                Operator = "<",
                RightExpression = maxAgeInYears.ToString()
            };
        }

        public static PolicyRuleGroup ContactMaxAgeRuleGroup(int? maxAgeInYears, string description = null)
        {
            return new PolicyRuleGroup()
            {
                Name = string.Format("Contact is less than {0} years old", maxAgeInYears),
                Description = description ?? string.Format("Contact is less than {0} years old", maxAgeInYears),
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = new List<Rule>(1) { ContactMaxAgeRule(maxAgeInYears, description) }
            };
        }

        public static PolicyRuleGroup BondingPeriodRuleGroup(int months, string eventName, string rule ,string description = null)
        {
            return new PolicyRuleGroup()
            {
                Name = string.Format("Leave would end {0} months from the {1}", months, eventName),
                Description = description ?? string.Format("Leave must be taken within {0} months from the {1}", months, eventName),
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = new List<Rule>(1) { BondingPeriodRule(months, eventName,rule, description) }
            };
        }
        public static Rule BondingPeriodRule(int months, string eventName, string rule, string description = null)
        {
            return new Rule()
            {
                Name = string.Format("Leave would end {0} months from the {1}", months, eventName),
                Description = description ?? string.Format("Leave must be taken within {0} months from the {1}", months, eventName),
                LeftExpression = rule + "()",
                Operator = "<",
                RightExpression = months.ToString()
            };
        }

        public static PolicyRuleGroup AdoptedMaxAgeRule(int? maxAgeInYears, string description = null)
        {
            var adoptedMaxAgeRule = new PolicyRuleGroup()
            {
                Name = $"Adopted Child <= {maxAgeInYears ?? 5} Years Old",
                Description = $"Adopted Child <= {maxAgeInYears ?? 5} Years Old",
                SuccessType = RuleGroupSuccessType.And,
                RuleGroupType = PolicyRuleGroupType.Eligibility,
                Rules = new List<Rule>()
            };
            adoptedMaxAgeRule.Rules.Add(new Rule()
            {
                Name = string.Format("Adopted Child <= {0} Years Old", maxAgeInYears),
                Description = description ?? string.Format("Adopted Child <= {0} Years Old", maxAgeInYears),
                LeftExpression = "ContactAgeInYears",
                Operator = "<=",
                RightExpression = maxAgeInYears.ToString()
            });

            return adoptedMaxAgeRule;



        }


    }
}
