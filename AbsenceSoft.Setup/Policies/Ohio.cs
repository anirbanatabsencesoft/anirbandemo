﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System.Collections.Generic;

namespace AbsenceSoft.Setup.Policies
{
    public class Ohio : PolicyBase
    {
        public override void BuildPolicies()
        {
            MIL();
        }

        public static Policy MIL() // Military
        {
            // Indicative Data Elements
            //      #EE = 50
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 1250hrs/12 mnts
            //      Min Len Service = 12 months
            //      Work State = OH
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame =  The lesser of 10 workdays or 80 hours in a calendar year
            //      Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = 
            //      Eligible Family Members = Spouse & Child
            //      Min Length of Military Duty = 30 Calendar Days or if Service Member Injured
            //      Active Military Duty = yes
            //      Search & Rescue = yes
            //      National Guard = yes
            //      Reserve Training = yes
            //      Duty Outside US Required = yes
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("OH-MIL", "Ohio Family Military Leave", PolicyType.StateFML, "OH");

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    WorkState = "OH",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 10,
                    PeriodType = PeriodType.PerCase,
                    Period = 1, 
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,

                    RuleGroups = new List<PolicyRuleGroup>()
                    {                  
                        EmployeeRelationshipRuleGroup(null, "SELF" , "SPOUSE" , "CHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD" ),
                        EligibilityRuleGroup("Ohio Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(30, Unit.Days))                    
                    },
                    
                    ShowType = PolicyShowType.OnlyTimeUsed
                },
                
                new PolicyAbsenceReason()
                {
                    WorkState = "OH",
                    Reason = Reasons.ReserveTraining,
                    EntitlementType = EntitlementType.WorkingHours,
                    Entitlement = 80, 
                    PeriodType = PeriodType.PerCase,
                    Period = 1,
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF" , "SPOUSE" , "CHILD" , "FOSTERCHILD" , "ADOPTEDCHILD" , "LEGALWARD" ),
                        EligibilityRuleGroup("Ohio Family Military Leave", MinOneYearServiceRule(), WorkedHoursInLast12MonthsRule(1250), MinLengthOfServiceRule(30, Unit.Days))
                    },

                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(50));
 
            return SavePolicy(policy);
        }

     
    }
}

