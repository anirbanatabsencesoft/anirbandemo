﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class NewJersey : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            VictimsOfDomesticViolence();
            NewJerseyTemporaryDisability();
            NewJerseyFamilyTemporaryDisability();
        }

        public static Policy FML() // Family Leave
        {
            // Selection:
            // - Work State = "NJ"
            // - Min Employees >= 50

            // Eligibility:
            // - 1,000/12 Months
            // - 12 months of service

            // Reasons:
            // - Family Health Condition

            // Entitlement:
            // - 12 Weeks in 24 Month Period

            // Elimination Period:
            // - 1 work week default is 5 workdays.
            //      Non reduced schedule requests of less than 1 work week should be denied

            Policy policy = StagePolicy("NJ-FML", "New Jersey Family Leave", PolicyType.StateFML, "NJ");
	        policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    PeriodUnit = Unit.Months,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Reduced,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                         EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "PARENTINLAW", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD",
                        "LEGALWARD","GRANDPARENT","GRANDCHILD","BLOODRELATIVE","DESIGNATEDPERSON"),
                    }
                },
                
               new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    PeriodUnit = Unit.Months,
                    IntermittentRestrictionsMinimum =1,
                    IntermittentRestrictionsMinimumTimeUnit =EntitlementType.WorkWeeks,
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.BeginningOfPeriod,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive |  CaseType.Intermittent |CaseType.Reduced,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    ShowType = PolicyShowType.Always,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "ADOPTEDCHILD","FOSTERCHILD"),
                    }
                },
               new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    PeriodUnit = Unit.Months,
                    IntermittentRestrictionsMinimum =1,
                    IntermittentRestrictionsMinimumTimeUnit =EntitlementType.WorkWeeks,
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.BeginningOfPeriod,
                    Reason = Reasons.AdoptionFosterCare,
                    CaseTypes = CaseType.Consecutive |  CaseType.Intermittent |CaseType.Reduced,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    ShowType = PolicyShowType.Always,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "ADOPTEDCHILD")
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 24,
                    PeriodType = PeriodType.RollingBack,
                    PeriodUnit = Unit.Months,
                    IntermittentRestrictionsMinimum =1,
                    IntermittentRestrictionsMinimumTimeUnit =EntitlementType.WorkWeeks,
                    IntermittentRestrictionsCalcType = IntermittentRestrictions.BeginningOfPeriod,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive |  CaseType.Intermittent |CaseType.Reduced,
                    Elimination = 1,
                    Gender =Gender.Female,
                    EliminationType = EntitlementType.WorkWeeks,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    ShowType = PolicyShowType.Always,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                             EventType = CaseEventType.BondingStartDate,
                             DateType = EventDateType.StartPolicyBasedOnEventDate
                        }
                    },
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1000, false, true, "New Jersey Family Leave"),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(30)
            };
            return SavePolicy(policy);
        }

        public static Policy VictimsOfDomesticViolence() // Victims of Domestic Violence Leave
        {
            // Selection:
            // - Work State = "NJ"
            // - Min Employees >= 25

            // Eligibility:
            // - 1,000/12 Months
            // - 12 months of service

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 20 Work Days  in 1 Year Period

            Policy policy = StagePolicy("NJ-DOM", "New Jersey Victims of Domestic Violence Leave", PolicyType.StateFML, "NJ");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 20,
                    Period = 12,
                    PeriodType = PeriodType.RollingForward,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "DOMESTICPARTNER", "CIVILUNIONPARTNER", "PARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "DOMESTICPARTNERSCHILD", "CIVILUNIONPARTNERSCHILD")
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                FMLEligibilityRuleGroup(1000, false, true, "New Jersey Victims of Domestic Violence Leave"),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25)
            };
            return SavePolicy(policy);
        }

        public static Policy NewJerseyTemporaryDisability()
        {
            Policy policy = StagePolicy("NJ-STD", "New Jersey Temporary Disability", PolicyType.StateDisability, "NJ");

            policy.AbsenceReasons = new List<PolicyAbsenceReason>() 
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = 0m, IsWaitingPeriod = true },
                        new PaymentInfo() { Duration = 26, Order = 2, PaymentPercentage = .6667M }
                    },
                    MaxAllowedAmount = 604M,
                    EliminationType = EntitlementType.WorkDays,
                    Elimination = 7d,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    Period = 26,                    
                    PeriodType = PeriodType.PerOccurrence,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup(policy.Name, MinLengthOfServiceRule(20, Unit.Weeks)),
                        IsNotWorkRelated()
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 26,
                    PaymentTiers = new List<PaymentInfo>() { 
                        new PaymentInfo() { Duration = 1, Order = 1, PaymentPercentage = 0m, IsWaitingPeriod = true },
                        new PaymentInfo() { Duration = 26, Order = 2, PaymentPercentage = .6667M }
                    },
                    MaxAllowedAmount = 604M,
                    EliminationType = EntitlementType.WorkDays,
                    Elimination = 7d,
                    PolicyEliminationCalcType = PolicyEliminationType.Inclusive,
                    Period = 26,
                    PeriodType = PeriodType.PerOccurrence,                    
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive,
                    Paid = true,
                    AllowOffset = true,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EligibilityRuleGroup(policy.Name, MinLengthOfServiceRule(20, Unit.Weeks)),
                        IsNotWorkRelated()
                    },
                    Gender = Gender.Female,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent() { DateType = EventDateType.EndPolicyBasedOnEventDate,
                            EventType = CaseEventType.BondingStartDate }
                    }
                }

            };
           
            return SavePolicy(policy);
        }

        public static Policy NewJerseyFamilyTemporaryDisability()
        {
            Policy policy = StagePolicy("NJ-FTD", "New Jersey Family Temporary Disability", PolicyType.StateDisability, "NJ");            

            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 6,                    
                    Period = 12,                    
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Paid = true,
                    AllowOffset = false,                    
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD", "DOMESTICPARTNERSCHILD", "CIVILUNIONPARTNERSCHILD", "ADOPTEDCHILD","FOSTERCHILD"),
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 6,
                    Period = 12,                    
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive| CaseType.Intermittent,
                    Paid = true,
                    AllowOffset = false,                    
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                       EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "PARENTINLAW", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD",
                        "LEGALWARD","GRANDPARENT","GRANDCHILD","BLOODRELATIVE","DESIGNATEDPERSON"),
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 6,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive| CaseType.Intermittent,
                    Paid = true,
                    AllowOffset = false,
                    Gender = Gender.Female,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        WillUseBonding()
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingStartDate,
                                 DateType = EventDateType.StartPolicyBasedOnEventDate
                            },
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingEndDate,
                                 DateType = EventDateType.EndPolicyBasedOnEventDate
                            }
                    }
                },
                 new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    ShowType = PolicyShowType.Always,
                    EntitlementType = EntitlementType.CalendarWeeks,
                    Entitlement = 6,
                    PeriodType =PeriodType.RollingForward,
                    PeriodUnit = Unit.Months,
                    Gender = Gender.Female,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Paid = true,
                    AllowOffset = false,
                    PolicyEvents = new List<PolicyEvent>()
                    {
                            new PolicyEvent()
                            {
                                 EventType = CaseEventType.BondingStartDate,
                                 DateType = EventDateType.StartPolicyBasedOnEventDate
                            }
                    }
                },
            };

            return SavePolicy(policy);
        }
    }
}
