﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Policies
{
    public class Oregon : PolicyBase
    {
        public override void BuildPolicies()
        {
            FML();
            FML_SC();
            FML_PREGMAT();
            CriminalProceeding();
            Victims();
            MIL();
        }

        public static Policy MIL() 
        {
            // Indicative Data Elements
            //      #EE = 25
            //      Radious = 
            //      Gender =
            //      Min Hours Worked/Time Frame = 20hrs/wk
            //      Min Len Service = 1st day eligible
            //      Work State = OR
            //      Key EE Designation = N
            // Entitlement
            //      Shared by spouse at Employer = N
            //      Amount in Time Frame = 14 workdays per occurrence taken consecutive or intermittently
            //      Runs Concurrent with military FMLA
            // UI Provided Data Elements (Reason for Leave )
            //      Only for Active duty during time of Military Conflict = x
            //      Eligible Family Members = Spouse & Domestic Partner
            //      Min Length of Military Duty = 
            //      Active Military Duty = x
            //      Search & Rescue = x
            //      National Guard = x
            //      Reserve Training = x
            //      Duty Outside US Required = 
            //      Only for injured or killed service member = 

            Policy policy = StagePolicy("OR-MIL", "Oregon Family Military Leave", PolicyType.StateFML, "OR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    WorkState = "OR",
                    Reason = Reasons.CoveredServiceMember,
                    EntitlementType = EntitlementType.WorkDays,
                    Entitlement = 14,
                    PeriodType = PeriodType.PerCase,
                    Period = 1, 
                    EffectiveDate = policy.EffectiveDate,
                    CaseTypes = CaseType.Intermittent | CaseType.Consecutive,

                    RuleGroups = new List<PolicyRuleGroup>()
                    {                  
                        EmployeeRelationshipRuleGroup(null, "SELF" , "SPOUSE" , "SAMESEXSPOUSE", "DOMESTICPARTNER" ),
                        EligibilityRuleGroup("Oregon Family Military Leave", AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days), MinLengthOfServiceRule(180, Unit.Days))
                   },
                    
                    ShowType = PolicyShowType.OnlyTimeUsed
                },                
                
            };

            policy.RuleGroups.Add(MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25));
            policy.RuleGroups.Add(ContactMilitaryStatusRuleGroup(MilitaryStatus.ActiveDuty));

            return SavePolicy(policy);
        }

        public static Policy FML() // Family Leave
        {
            // Selection:
            // - Work State = OR
            // - Min 25 Employees

            // Eligibility:
            // - Min 25 hrs/week
            // - 180 calendar days min length of service

            // Reasons:
            // - EHC, PREGMAT, ADOPT, FHC, BEREAVEMENT

            // Entitlement:
            // - 12 weeks per 12 months for all leaves other than pregnancy leave or a 
            //  second/multiple parental leaves within 12 months. Pregnancy and multiple 
            //  parental leave is 24 weeks in 12 month period.  
            //
            // - 2 weeks per 12 months for Bereavement, counts alongside total entitlement/usage
            //  for all other leave types on this policy as well.

            Policy policy = StagePolicy("OR-FML", "Oregon Family Leave", PolicyType.StateFML, "OR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.EmployeeHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF"),
                        EligibilityRuleGroup("Oregon Family Leave",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days))
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        // "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD",  are handled under "Sick Child"
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "STEPPARENT", "FOSTERPARENT", "GRANDPARENT", "GRANDCHILD",
                            "PARENTINLAW", "INLOCOPARENTIS", "CIVILUNIONPARTNER", "DOMESTICPARTNER", "ADOPTEDGRANDCHILD", "ADOPTEDGRANDPARENT", "ADOPTIVEPARENT"),
                        EligibilityRuleGroup("Oregon Family Leave",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days))

                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    Gender = Gender.Female
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Period = 12,
                    Reason = Reasons.AdoptionFosterCare,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "FOSTERCHILD", "ADOPTEDCHILD"),
                        ContactMaxAgeRuleGroup(18),
                        EligibilityRuleGroup("Oregon Family Leave",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days))
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 2,
                    Period = 12,
                    Reason = Reasons.Bereavement,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SPOUSE", "PARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD","STEPPARENT","FOSTERPARENT","PARENTINLAW","GRANDPARENT","INLOCOPARENTIS"),
                        EligibilityRuleGroup("Oregon Family Leave",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days))
                    }
                },
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Reason = Reasons.Bonding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null,  "CHILD",  "FOSTERCHILD", "ADOPTEDCHILD"),
                         ContactMaxAgeRuleGroup(18)
                    },
                    PolicyEvents = new List<PolicyEvent>()
                    {
                        new PolicyEvent()
                        {
                             EventType = CaseEventType.BondingStartDate,
                             DateType = EventDateType.StartPolicyBasedOnEventDate
                        }
                    }
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                IsNotWorkRelated(),
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25),
                 EligibilityRuleGroup("Oregon Family Leave",   MinLengthOfServiceRule(180, Unit.Days))
             };
            return SavePolicy(policy);
        }

        public static Policy FML_SC() // Family Leave Sick Child
        {
            // Selection:
            // - Work State = OR
            // - Min 25 Employees

            // Eligibility:
            // - Min 25 hrs/week
            // - 180 calendar days min length of service

            // Reasons:
            // - EHC Sick Child

            // Entitlement:
            // - 12 weeks per 12 months for all leaves

            Policy policy = StagePolicy("OR-FMLSC", "Oregon Family Leave Act - Sick Child", PolicyType.StateFML, "OR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 24,
                    Period = 12,
                    Reason = Reasons.FamilyHealthCondition,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "CHILD","STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD","LEGALWARD")
                    },
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25),
                EligibilityRuleGroup("Oregon Family Leave Act - Sick Child",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days), MinLengthOfServiceRule(180, Unit.Days)),
                ManualOnlyRuleGroup()
            };
            return SavePolicy(policy);
        }

        public static Policy FML_PREGMAT() // Family Leave-Pregnancy/Maternity
        {
            // Selection:
            // - Work State = OR
            // - Min 25 Employees
            // - 180 calendar days min length of service

            // Reasons:
            // - Pregnancy/Maternity

            // Entitlement:
            // - 12 weeks per 12 months for all leaves

            Policy policy = StagePolicy("OFLA-PREGMAT", "Oregon Family Leave-Pregnancy/Maternity", PolicyType.StateFML, "OR",Gender.Female);
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.WorkWeeks,
                    Entitlement = 12,
                    Gender = Gender.Female,
                    Period = 12,
                    Reason = Reasons.PregnancyMaternity,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced
                }
            };
            policy.RuleGroups = new List<PolicyRuleGroup>()
            {
                MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(25),
                EligibilityRuleGroup("Oregon Family Leave-Pregnancy/Maternity", MinLengthOfServiceRule(180, Unit.Days)),
            };
            return SavePolicy(policy);
        }

        public static Policy CriminalProceeding() // Criminal Proceeding Leave
        {
            // Selection:
            // - Work State = "OR"
            // - Min Employees >= 6

            // Eligibility:
            // - Min 25 hrs/week
            // - 180 calendar days min length of service

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 14 Work Days in a Calendar Year with no more than 8 hours taken in a workday

            Policy policy = StagePolicy("OR-CRIME", "Oregon Criminal Proceeding Leave", PolicyType.StateFML, "OR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.CriminalProceeding,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    Elimination = 1,
                    EliminationType = EntitlementType.WorkWeeks,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "SPOUSE", "DOMESTICPARTNER", "PARENT", "GRANDPARENT", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "DOMESTICPARTNERSCHILD", "SIBLING"),
                        EligibilityRuleGroup("Oregon Criminal Proceeding Leave",  AverageHoursWorkedPerWeekOverPeriodRule(25,180,Unit.Days), MinLengthOfServiceRule(180, Unit.Days)),
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(6)
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }

        public static Policy Victims() // Leave for Victims of Domestic Violence, Sexual Assault, Stalking, or Harassment
        {
            // Selection:
            // - Work State = "OR"
            // - Min Employees >= 6

            // Eligibility:
            // - Day 1 Eligible

            // Reasons:
            // - Domestic Violence

            // Entitlement:
            // - 14 Work Days in a Calendar Year with no more than 8 hours taken in a workday

            Policy policy = StagePolicy("OR-DOM", "Oregon Leave for Victims of Domestic Violence, Sexual Assault, Stalking, or Harassment", PolicyType.StateFML, "OR");
            policy.AbsenceReasons = new List<PolicyAbsenceReason>()
            {
                new PolicyAbsenceReason()
                {
                    EffectiveDate = policy.EffectiveDate,
                    EntitlementType = EntitlementType.ReasonablePeriod,
                    Entitlement = 1,
                    Period = 1,
                    PeriodType = PeriodType.PerCase,
                    Reason = Reasons.DomesticViolence,
                    CaseTypes = CaseType.Consecutive | CaseType.Intermittent,
                    RuleGroups = new List<PolicyRuleGroup>()
                    {
                        EmployeeRelationshipRuleGroup(null, "SELF", "CHILD", "STEPCHILD", "FOSTERCHILD", "ADOPTEDCHILD", "LEGALWARD", "CHILDLIVINGWITHEMPLOYEE"),
                        MinNumberOfEmployeesInTheSameWorkStateEligibilityRuleGroup(6),
                        EligibilityRuleGroup("Oregon Leave for Victims of Domestic Violence, Sexual Assault, Stalking, or Harassment", MinHoursWorkedPerWeekRule(25), MinLengthOfServiceRule(180, Unit.Days))
                    },
                    ShowType = PolicyShowType.OnlyTimeUsed
                }
            };
            return SavePolicy(policy);
        }
    }
}
