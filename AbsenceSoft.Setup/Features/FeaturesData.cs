﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;

namespace AbsenceSoft.Setup.Features
{
    public static class FeaturesData
    {
        /// <summary>
        /// Sets up the default feature data.
        /// </summary>
        public static void SetupData()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            MongoUrl url = MongoUrl.Create(connectionString);
            MongoClientSettings settings = MongoClientSettings.FromUrl(url);
            settings.GuidRepresentation = GuidRepresentation.Standard;
            string databaseName = url.DatabaseName;
            MongoClient client = new MongoClient(settings);
            MongoServer server = client.GetServer();
            var db = server.GetDatabase(databaseName);
            if (!db.CollectionExists(Customer.Repository.Collection.Name))
            {
                return;
            }

            //Set feature's default value for each customer if it is missing (when you add new feature please set default value here for existing customer)
            foreach (var customer in Customer.AsQueryable().ToList())
            {
                if (customer.Features == null)
                {
                    customer.Features = new List<AppFeature>();
                }

                SetDefaultEnabledFeatures(customer);
                SetDefaultDisabledFeatures(customer);

                bool hasSsoProfile = SsoProfile.AsQueryable().FirstOrDefault(sp => sp.CustomerId == customer.Id) != null;
                bool multiEmployerAccess = Employer.AsQueryable().Count(e => e.CustomerId == customer.Id) > 1;

                SetFeatureDefault(customer, Feature.MultiEmployerAccess, multiEmployerAccess);
                SetFeatureDefault(customer, Feature.EmployerServiceOptions, multiEmployerAccess);
                SetFeatureDefault(customer, Feature.SingleSignOn, hasSsoProfile);

                customer.Save();
            }
        }

        /// <summary>
        /// Sets the default enabled features.
        /// </summary>
        /// <param name="customer">The customer.</param>
        private static void SetDefaultEnabledFeatures(Customer customer)
        {
            SetFeatureDefault(customer, Feature.LOA, true);
            SetFeatureDefault(customer, Feature.ADA, true);
            SetFeatureDefault(customer, Feature.WorkRelatedReporting, true);
            SetFeatureDefault(customer, Feature.ESSWorkRelatedReporting, true);
            SetFeatureDefault(customer, Feature.WorkRestriction, true);
            SetFeatureDefault(customer, Feature.BusinessIntelligenceReport, true);
            SetFeatureDefault(customer, Feature.ContactPreferences, true);
            SetFeatureDefault(customer, Feature.AdminPaySchedules, true);
            SetFeatureDefault(customer, Feature.AdminPhysicalDemands, true);
            SetFeatureDefault(customer, Feature.AdminCustomFields, true);
            SetFeatureDefault(customer, Feature.AdminNecessities, true);
            SetFeatureDefault(customer, Feature.AdminNoteCategories, true);
            SetFeatureDefault(customer, Feature.AdminConsultations, true);
            SetFeatureDefault(customer, Feature.AdminAssignTeams, true);
            SetFeatureDefault(customer, Feature.AdminOrganizations, true);
            SetFeatureDefault(customer, Feature.AdminDataUpload, true);
            SetFeatureDefault(customer, Feature.AdminEmployers, true);
            SetFeatureDefault(customer, Feature.AdminSecuritySettings, true);
           
        }

        /// <summary>
        /// Sets the default disabled features.
        /// </summary>
        /// <param name="customer">The customer.</param>
        private static void SetDefaultDisabledFeatures(Customer customer)
        {
            SetFeatureDefault(customer, Feature.ShortTermDisability, false);
            SetFeatureDefault(customer, Feature.PolicyConfiguration, false);
            SetFeatureDefault(customer, Feature.CommunicationConfiguration, false);
            SetFeatureDefault(customer, Feature.EmployeeSelfService, false);
            SetFeatureDefault(customer, Feature.GuidelinesData, false);
            SetFeatureDefault(customer, Feature.WorkflowConfiguration, false);
            SetFeatureDefault(customer, Feature.InquiryCases, false);
            SetFeatureDefault(customer, Feature.EmployeeConsults, false);
            SetFeatureDefault(customer, Feature.ShortTermDisablityPay, false);
            SetFeatureDefault(customer, Feature.ESSDataVisibilityInPortal, false);
            SetFeatureDefault(customer, Feature.CaseReporter, false);
            SetFeatureDefault(customer, Feature.WorkRelatedReporting, false);
            SetFeatureDefault(customer, Feature.ESSWorkRelatedReporting, false);
            SetFeatureDefault(customer, Feature.IPRestrictions, false);
            SetFeatureDefault(customer, Feature.JSA, false);
            SetFeatureDefault(customer, Feature.JobConfiguration, false);
            SetFeatureDefault(customer, Feature.OshaReporting, false);
            SetFeatureDefault(customer, Feature.AdHocReporting, false);
            SetFeatureDefault(customer, Feature.AccommodationTypeCategories, false);
            SetFeatureDefault(customer, Feature.AdjustEmployeeStartDayOfWeek, false);
            SetFeatureDefault(customer, Feature.PolicyCrosswalk, false);
            SetFeatureDefault(customer, Feature.CaseBulkDownload, false);
            SetFeatureDefault(customer, Feature.DashboardChart, false);
            SetFeatureDefault(customer, Feature.ReportingTwoBeta, false);
            SetFeatureDefault(customer, Feature.CaseClosureCategories, false);
            SetFeatureDefault(customer, Feature.ESSWorkSchedule, false);
            SetFeatureDefault(customer, Feature.FlightCrew, false);
            SetFeatureDefault(customer, Feature.AdminDenialReason, false);
        }

        /// <summary>
        /// Sets the feature default.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="feature">The feature.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        private static void SetFeatureDefault(Customer customer, Feature feature, bool enabled)
        {
            if (customer.Features.Exists(f => f.Feature == feature))
            {
                return;
            }

            customer.Features.Add(new AppFeature() { Feature = feature, Enabled = enabled });
        }
    }
}
