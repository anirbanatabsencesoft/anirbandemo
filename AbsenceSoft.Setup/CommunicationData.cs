﻿using AbsenceSoft.Common.Properties;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Common;
using AbsenceSoft.Setup.Communications;
using AbsenceSoft.Setup.PaperworkFiles;
using AbsenceSoft.Setup.Properties;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup
{
    public static class CommunicationData
    {
        public static void BuildTemplates()
        {
            Console.WriteLine("Building CORE Paperwork");
            var Paperwork_EmployeeHealthCondition = CreatePaperwork("Certification for FMLA Employee Health Condition",
                "CERTIFICATIONFORFMLAEMPLOYEEHEALTHCONDITION",
                "Certification for FMLA Employee Health Condition.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Certification_for_FMLA_Employee_Health_Condition,
                "Certification for FMLA Employee Health Condition",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Employee Health Condition",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has FMLA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Not CA or OR",
                        SuccessType = RuleGroupSuccessType.Not,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "OR",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "CA",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            var Paperwork_NonFMLAForFamilyMember = CreatePaperwork("Certification for Non FMLA Family",
                "CERTIFICATIONFORNONFMLAFAMILY",
                "Certification for Non FMLA Family.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Certification_of_Healthcare_Provider_for_Non_FMLA_Family,
                "Certification for Non FMLA Family",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Family Health Condition",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Does not have FMLA",
                        SuccessType = RuleGroupSuccessType.Not,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Care for a Family Member",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            var Paperwork_EmployeeHealthCondition_CA = CreatePaperwork("Certification of Health Care Provider (CFRA/FMLA)",
                "CERTIFICATIONOFHEALTHCAREPROVIDERCFRAFMLA",
                "Certification of Health Care Provider.docx",
                DocumentType.MSWordDocument,
                Resources.Certification_of_Health_Care_Provider,
                "Certification of Health Care Provider (CFRA/FMLA)",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Certification of Health Care Provider (CFRA/FMLA)",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "CA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has FMLA or CFRA",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "CA-CFRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy OR Family Health Condition",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            var Paperwork_EmployeeHealthCareProvider_OR = CreatePaperwork("Certification of Health Care Provider (OFLA/FMLA)",
                "CERTOFHEALTHCAREPROVIDEDOFLAFMLA",
                "Certification of Health Care Provider.docx",
                DocumentType.MSWordDocument,
                Resources.OFLA_SHC_Cert,
                "Certification of Health Care Provider (OFLA/FMLA)",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Certification of Health Care Provided (OFLA/FMLA)",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "OR",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "OR",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has FMLA or OFLA",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "OFLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy OR Family Health Condition",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            var Paperwork_EmployeeHealthCondition_OR = CreatePaperwork("Certification for Serious Health Condition",
                "CERTIFICATIONFORSERIOUSHEALTHCONDITION",
                "Certification for Serious Health Condition.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.OFLA_SHC_Cert,
                "Certification for Serious Health Condition",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Certification for Serious Health Condition",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "OR",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "OR",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has FMLA or OR-FML",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "OR-FML",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Family Health Condition (NON-FMLA)
            var Paperwork_NONFMLAEmployeeHealthCondition = CreatePaperwork(
                "Certification for NON-FMLA Employee Health Condition",
                "CERTIFICATIONFORNONFMLAEMPLOYEEHEALTHCONDITION",
                "Certification for NONFMLA Employee Health Condition.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Certification_for_NON_FMLA_Employee_Health_Condition,
                "Certification for NON-FMLA Employee Health Condition",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Employee Health Condition",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has NOT FMLA",
                        SuccessType = RuleGroupSuccessType.Not,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "CA-CFRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "OR-FML",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Serious Illness Veteran
            var Paperwork_CertForSeriousOrIllnessVeteran = CreatePaperwork("Certification for Serious Injury or illness of a Veteran",
                "CERTIFICATIONFORSERIOUSINJURYORILLNESSOFAVETERAN",
                "Certification for Serious Injury or illness of a Veteran.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Certification_for_Serious_Injury_Or_Illness_Of_a_Veteran,
                "Certification for Serious Injury or illness of a Veteran",
                TimeSpan.FromDays(15),
                true,
                true,
                "Medical Certification - Certification for Serious Injury or illness of a Veteran",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Military AND Contact is Veteran",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "MILITARY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = MilitaryStatus.Veteran.ToString(),
                                CriteriaType = PaperworkCriteriaType.MilitaryStatus
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Serious Illness Current Service Member
            var Paperwork_CertForSeriousOrIllnessCurrentSM = CreatePaperwork("Certification for Serious Injury or illness of a Current Servicemember",
                "CERTIFICATIONFORSERIOUSINJURYORILLNESSOFACURRENTSERVICEMEMBER",
                "Certification for Serious Injury or illness of a Current Servicemember.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Certification_for_Serious_Injury_or_Illness_of_a_Current_Servicemember,
                "Certification for Serious Injury or illness of a of a Current Servicemember",
                TimeSpan.FromDays(15),
                true, true,
                "Medical Certification - Certification for Serious Injury or illness of a Current Servicemember",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Military AND Contact is Active Duty",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "MILITARY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = MilitaryStatus.ActiveDuty.ToString(),
                                CriteriaType = PaperworkCriteriaType.MilitaryStatus
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Qualifying Exigency
            var Paperwork_QualifyingExigencyCert = CreatePaperwork("Qualifying Exigency Certification",
                "QUALIFYINGEXIGENCYCERTIFICATION",
                "Qualifying Exigency Certification.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Qualifying_Exigency_Certification,
                "Qualifying Exigency Certification",
                TimeSpan.FromDays(15),
                true, true,
                "Qualifying Exigency Certification",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Qualifying Exigency",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EXIGENCY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Family Member Serious Health Condition
            var Paperwork_FamilyMemberSHC = CreatePaperwork("Family Member Serious Health Condition",
                "FAMILYMEMBERSERIOUSHEALTHCONDITION",
                "Family Member Serious Health Condition.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Family_Member_Serious_Health_Condition,
                "Family Member Serious Health Condition",
                TimeSpan.FromDays(15),
                true, true,
                "Family Member Serious Health Condition",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Family Health Condition",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Not CA",
                        SuccessType = RuleGroupSuccessType.Not,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //Fitness Duty Cert
            var Paperwork_FitnessforDutyCert = CreatePaperwork("Fitness Duty Certification",
                "FITNESSDUTYCERTIFICATION",
                "Fitness Duty Certification.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Fitness_for_Duty_Certification,
                "Fitness Duty Certification",
                TimeSpan.FromDays(15),
                true, true,
                "Fitness Duty Certification",
                new List<PaperworkCriteriaGroup>());

            var Paperwork_AuthorizationToReleaseMedicalInformation = CreatePaperwork("AUTHORIZATION TO RELEASE MEDICAL INFORMATION",
                "AUTHORIZATIONTORELEASEMEDICALINFORMATION",
                "AUTHORIZATION TO RELEASE MEDICAL INFORMATION.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.AUTHORIZATION_TO_RELEASE_MEDICAL_INFORMATION,
                "AUTHORIZATION TO RELEASE MEDICAL INFORMATION",
                TimeSpan.FromDays(15),
                false, true,
                "AUTHORIZATION TO RELEASE MEDICAL INFORMATION",
                new List<PaperworkCriteriaGroup>());

            //Accommodation Request Form
            var Paperwork_AccommodationRequestForm = CreatePaperwork("Accommodation Request Form",
                "ACCOMMODATIONREQUESTFORM",
                "AccommodationRequestForm.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Accommodation_Request_Form,
                "Accommodation Request Form",
                TimeSpan.FromDays(10),
                true, true,
                "Accommodation Request Form",
                new List<PaperworkCriteriaGroup>());

            // DFEH-100-20 is for CA Pregnancy leave
            var Paperwork_Pregnancy_CA_DFEH10020 = CreatePaperwork("DFEH-100-20 (04/16)",
                "DFEH100200416",
                "DFEH-100-20 (04-16).pdf",
                DocumentType.Pdf,
                MSWordPaperwork.DFEH_100_20__04_16_,
                "DFEH-100-20 (04/16) - YOUR RIGHTS AND OBLIGATIONS AS A PREGNANT EMPLOYEE",
                null,
                false, true,
                "DFEH-100-20 (04/16)",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "CA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has CFRA",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA-CFRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Pregnancy",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            // DFEH-100-210 is for CA Employee Health Condition
            var Paperwork_EHC_CA_DFEH100210 = CreatePaperwork("DFEH-100-21 (07/15)",
                "DFEH100210715",
                "DFEH-100-210-rev072015.pdf",
                DocumentType.Pdf,
                MSWordPaperwork.DFEH_100_210_rev072015,
                "DFEH-100-21 (07/15) - FAMILY CARE AND MEDICAL LEAVE (CFRA LEAVE) AND PREGNANCY DISABILITY LEAVE",
                null,
                false, true,
                "DFEH-100-21 (07/15)",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "CA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA",
                                CriteriaType = PaperworkCriteriaType.WorkState
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has CFRA",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "CA-CFRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Employee Health Condition OR Pregnancy",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "PREGMAT",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            //YOUR RIGHTS UNDER USERRA, userra_private.pdf
            var Paperwork_USERRA = CreatePaperwork("Your Rights Under USERRA",
                "YOURRIGHTSUNDERUSERRA",
                "userra_private.pdf",
                DocumentType.Pdf,
                MSWordPaperwork.userra_private,
                "Your Rights Under USERRA",
                null,
                false, true,
                "Your Rights Under USERRA",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has USERRA",
                        SuccessType = RuleGroupSuccessType.Or,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "USERRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    }
                }
                #endregion criteria
                );

            var Paperwork_USERRA_InfoPosting = CreatePaperwork("USERRA Information Posting",
                "USERRAINFOPOSTING",
                "USERRA Information Posting.pdf",
                DocumentType.Pdf,
                MSWordPaperwork.USERRA_Information_Posting,
                "USERRA Information Posting",
                null,
                false, true,
                "USERRA Information Posting",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has USERRA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "USERRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Is ACTIVEDUTY Leave",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "ACTIVEDUTY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            // DOL_FMLA_Certification_VeteranInjuryIllness
            var Paperwork_DOL_FMLA_Certification_VeteranInjuryIllness = CreatePaperwork("FMLA Care of Service Member Certification-Veteran",
                "DOLVETERANCERT",
                "DOL_FMLA_Certification_VeteranInjuryIllness.pdf",
                DocumentType.Pdf,
                MSWordPaperwork.DOL_FMLA_Certification_VeteranInjuryIllness,
                "Department of Labor Certification for Care of Service Member-Veteran",
                TimeSpan.FromDays(15),
                true, true,
                "",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Policy=FMLA, Absence Reason=MILITARY, Military Status=VETERAN",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                             new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy

                            },
                              new PaperworkCriteria()
                            {
                                Criteria = "MILITARY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason

                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "Veteran",
                                CriteriaType = PaperworkCriteriaType.MilitaryStatus
                               
                            }
                        }
                    }
                },
                #endregion criteria
                  true,
            #region PdfFillFields
                     new Dictionary<string, List<string>>()
               {
                       { "Name and address of employer this is the employer of the employee requesting leave to care for a veteran",
                          new List<string>() { "Employer.Name","Employer.Address" }
                        },
                       { "Name of employee requesting leave to care for a veteran",
                          new List<string>() { "Employee.FirstName", "Employee.LastName" }
                      },
                       { "Name of veteran for whom employee is requesting leave",
                          new List<string>() { "RelatedPerson.FirstName" , "RelatedPerson.LastName" }
                      }
               }
                
                  #endregion PdfFillFields
                );

            var Paperwork_DOL_FMLA_Certification_EmployeeHealth = CreatePaperwork("FMLA Employee Health Certification",
                "DOLEHCCERT",
                "DOL_FMLA_Certification_EmployeeHealth.pdf",
                DocumentType.Pdf,
                MSWordPaperwork.DOL_FMLA_Certification_EmployeeHealth,
                "Department of Labor employee health certification for FMLA cases",
                null,
                false, true,
                "",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Policy=FMLA, Absence Reason=Employee Health",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                              new PaperworkCriteria()
                            {
                                Criteria = "EHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                },
            #endregion criteria
                  true,
              #region PdfFillFields
                  new Dictionary<string, List<string>>()
                  {
                       { "txtEmployerName",
                          new List<string>() { "Employer.Name", "CurrentUser.WorkPhone" }
                        },
                       { "txtEmployeeTitle",
                          new List<string>() { "Employee.JobTitle" }
                        },
                       { "txtYourName",
                          new List<string>() { "Employee.FirstName", "Employee.LastName" }
                      }
                  }
                  #endregion criteria
                );

            var Paperwork_DOL_FMLA_Certification_FamilyHealth = CreatePaperwork("FMLA Family Health Condition Certification",
              "DOLFHCCERT",
              "DOL_FMLA_Certification_FamilyHealth.pdf",
              DocumentType.Pdf,
              MSWordPaperwork.DOL_FMLA_Certification_FamilyHealth,
              "Department of Labor family health condition certification for FMLA cases",
              null,
              false, true,
              "",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Policy=FMLA, Absence Reason=Family Health",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy

                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        },
                       
                    }
                },
            #endregion criteria
                  true,
            #region PdfFillFields
                new Dictionary<string, List<string>>()
                {
                       { "txtNameContact",
                          new List<string>() { "Employer.Name" }
                        },
                       { "txtNameContact2",
                          new List<string>() { "CurrentUser.WorkPhone" }
                        },
                       { "txtYourName",
                          new List<string>() { "Employee.FirstName", "Employee.LastName" }
                      },
                       { "txtNameOfFamilyMember",
                          new List<string>() { "RelatedPerson.FirstName", "RelatedPerson.LastName" }
                      },
                       { "txtDateOfBirth",
                          new List<string>() { "RelatedPerson.DateOfBirth"}
                      },
                }
                #endregion PdfFillFields
              );

            var Paperwork_DOL_FMLA_Certification_QualifyingExigency = CreatePaperwork("FMLA Qualifying Exigency Certification",
              "DOLEXIGENCYCERT",
              "DOL_FMLA_Certification_QualifyingExigency.pdf",
              DocumentType.Pdf,
              MSWordPaperwork.DOL_FMLA_Certification_QualifyingExigency,
              "Department of Labor Qualifying Exigency Certification for FMLA cases",
              TimeSpan.FromDays(15),
              true, true,
              "",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Policy=FMLA, Absence Reason=Exigency",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                             new PaperworkCriteria()
                            {
                                Criteria = "EXIGENCY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                },
            #endregion criteria
                  true,
            #region PdfFillFields
                new Dictionary<string, List<string>>()
                {
                       { "Employer name",
                          new List<string>() { "Employer.Name" }
                        },
                       { "Contact Information",
                          new List<string>() { "CurrentUser.Name" , "CurrentUser.WorkPhone" }
                        },
                       { "Your Name",
                          new List<string>() { "Employee.FirstName", "Employee.LastName" }
                      },
                       { "First",
                          new List<string>() { "RelatedPerson.FirstName"  }
                      },
                       { "Last",
                          new List<string>() { "RelatedPerson.LastName" }
                      },
                        { "Relationship of military member to you",
                          new List<string>() { "RelatedPerson.Type" }
                      },
                }
                #endregion PdfFillFields
              );

            var Paperwork_DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness = CreatePaperwork("FMLA Care of Service Member Certification (Current Service Member)",
             "DOLMILITARYCURRENTCERT",
             "DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness.pdf",
             DocumentType.Pdf,
             MSWordPaperwork.DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness,
             "Department of Labor FMLA certification for care of current service members",
              TimeSpan.FromDays(15),
             true, true,
             "",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Policy=FMLA, Absence Reason=Military, MilitaryStatus=",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FLMA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "MILITARY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "ACTIVE DUTY",
                                CriteriaType = PaperworkCriteriaType.MilitaryStatus
                            }
                        }
                    }
                },
            #endregion criteria
            true,
            #region PdfFillFields
              new Dictionary<string, List<string>>()
                  {
                       { "servicemember",
                          new List<string>() { "Employer.Name", "Employer.Address" }
                        },
                       { "Name of Employee Requesting Leave to Care for the Current Servicemember",
                          new List<string>() { "Employee.FirstName", "Employee.LastName" }
                        },
                       { "Name of the Current Servicemember for whom employee is requesting leave to care",
                          new List<string>() { "RelatedPerson.FirstName","RelatedPerson.LastName" }
                      }
                  }
              #endregion PdfFillFields
             );

            var Paperwork_USERRA_ActiveDutyReturn = CreatePaperwork("USERRA Employee Active Duty Return Notification letter to Employer",
                "USERRAACTIVEDUTYRETURN",
                "Employee Active Duty Return Notification letter to Employer.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.Employee_Active_Duty_Return_Notification_letter_to_Employer,
                "Employee Active Duty Return Notification letter to Employer",
                null,
                false, true,
                "Employee Active Duty Return Notification letter to Employer",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Has USERRA",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "USERRA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            }
                        }
                    },
                    new PaperworkCriteriaGroup()
                    {
                        Name = "Is ACTIVEDUTY Leave",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "ACTIVEDUTY",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            }
                        }
                    }
                }
                #endregion criteria
                );

            // Child Certification if Over 18
            var Paperwork_FHC_AdultChildCert = CreatePaperwork("FMLA Adult Child Certification of Disability",
                "FMLAADULTCHILDCERTIFICATIONOFDISABILITY",
                "cert-disability-adult-child.docx",
                DocumentType.MSWordDocument,
                MSWordPaperwork.cert_disability_adult_child,
                "Family Medical Leave Act Adult Child Certification of Disability",
                TimeSpan.FromDays(15),
                true, true,
                "FMLA Adult Child Certification of Disability",
            #region criteria
                new List<PaperworkCriteriaGroup>()
                {
                    new PaperworkCriteriaGroup()
                    {
                        Name = "FMLA, Employee Health Condition for Child over 18",
                        SuccessType = RuleGroupSuccessType.And,
                        Criteria = new List<PaperworkCriteria>()
                        {
                            new PaperworkCriteria()
                            {
                                Criteria = "FMLA",
                                CriteriaType = PaperworkCriteriaType.Policy
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "FHC",
                                CriteriaType = PaperworkCriteriaType.AbsenceReason
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "CHILD",
                                CriteriaType = PaperworkCriteriaType.ContactType
                            },
                            new PaperworkCriteria()
                            {
                                Criteria = "ContactAgeInYears() >= 18",
                                CriteriaType = PaperworkCriteriaType.LeaveOfAbsenceExpression
                            }
                        }
                    }
                }
                #endregion criteria
                );


            Console.WriteLine("Completed creating CORE Paperwork");
            Console.WriteLine("Building CORE Communication Templates");

            // USERRA
            CreateTemplate("USERRANOTICE", "Eligibility USERRA Letter to Employee", "Eligibility USERRA Letter to Employee for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Eligibility_USERRA_Letter_to_Employee,
                true,
                Paperwork_USERRA_InfoPosting.Code,
                Paperwork_USERRA_ActiveDutyReturn.Code);

            // Eligibility_and_Rights_and_Responsibilities_Notice
            CreateTemplate("ELGNOTICE", "Eligibility Rights and Responsibilities Notice", "Eligibility Rights and Responsibilities for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Eligibility_and_Rights_and_Responsibilities_Notice,
                true,
                Paperwork_EmployeeHealthCondition.Code,
                Paperwork_NONFMLAEmployeeHealthCondition.Code,
                Paperwork_FamilyMemberSHC.Code,
                Paperwork_QualifyingExigencyCert.Code,
                Paperwork_CertForSeriousOrIllnessVeteran.Code,
                Paperwork_CertForSeriousOrIllnessCurrentSM.Code,
                Paperwork_EmployeeHealthCondition_CA.Code,
                Paperwork_EmployeeHealthCareProvider_OR.Code,
                Paperwork_NonFMLAForFamilyMember.Code,
                Paperwork_Pregnancy_CA_DFEH10020.Code,
                Paperwork_EHC_CA_DFEH100210.Code,
                Paperwork_USERRA.Code,
                Paperwork_FHC_AdultChildCert.Code,
                Paperwork_AuthorizationToReleaseMedicalInformation.Code);

            // Designation_Notice_Leave_Aproved
            CreateTemplate("CASECLOSE", "Case Closure Notice", "Case Closure Notice for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.CaseClosure);

            // Designation_Notice_Leave_Aproved
            CreateTemplate("APPROVED", "Designation Notice Leave Approved", "Designation Notice Leave Approved for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.Designation_Notice_Leave_Approved, true);

            // Designation_Notice_Leave_Not_Aproved
            CreateTemplate("NTAPPROVED", "Designation Notice Leave Not Approved", "Designation Notice Leave Not Approved for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.Designation_Notice_Leave_Not_Approved, true);

            // Eligibility_Notice_Leave_Exhausted
            CreateTemplate("ELGLEXH", "Eligibility Notice Leave Exhausted", "Eligibility Notice Leave Exhausted for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.Eligibility_Notice_Leave_Exhausted, true);

            // Eligibility_Notice_Not_Eligible
            CreateTemplate("ELGNOTELG", "Eligibility Notice Not Eligible", "Eligibility Not Eligible for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.Eligibility_Notice_Not_Eligible, true);

            // FMLA Leave Expiring Shortly
            CreateTemplate("FMLAEXP", "FMLA Leave Expiring Shortly", "FMLA Leave Expiring for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.FMLA_Leave_Expiring_Shortly, true);

            //TODO: Finish the HR/MGR communication templates and set the body here.
            //CreateTemplate("HRNOTICE", "HR Communication", "Leave of Absence Requested, Case # {{Case.CaseNumber}}", "<p>Case # {{Case.CaseNumber}} created for employee {{Employee.FullName}}.</p><p>TODO: Create this template</p>");
            CreateTemplate("HRNOTICE", "HR Communication", "Leave of Absence Requested, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.EmailToHR);

            // Incomplete_or_Insufficient_Certification
            CreateTemplate(
                "INCOMPCERT",
                "Incomplete or Insufficient Certification",
                "Incomplete or Insufficient Certification for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Incomplete_or_Insufficient_Certification,
                false,
                Paperwork_EmployeeHealthCondition.Code,
                Paperwork_FamilyMemberSHC.Code,
                Paperwork_QualifyingExigencyCert.Code,
                Paperwork_CertForSeriousOrIllnessVeteran.Code,
                Paperwork_CertForSeriousOrIllnessCurrentSM.Code,
                Paperwork_EmployeeHealthCondition_CA.Code,
                Paperwork_EmployeeHealthCondition_OR.Code);

            // leave denied
            CreateTemplate("DENIED", "Leave Denied Eligibility Notice", "Leave Denied ELGNOTICE for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.Designation_Notice_Leave_Not_Approved, true, Paperwork_EmployeeHealthCondition.Code);

            //Manager Notice
            CreateTemplate("MGRNOTICE", "Manager Communication", "Leave of Absence Requested, Case # {{Case.CaseNumber}}", null, false, DocumentType.MSWordDocument, MSWordTemplates.EmailToMGR);

            // Notice_of_Failure_To_Provide_Certification
            CreateTemplate(
                "FAILCERT",
                "Documentation Not Received", "Provide Certification Failure for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Notice_of_Failure_To_Provide_Certification,
                false,
                Paperwork_EmployeeHealthCondition.Code,
                Paperwork_FamilyMemberSHC.Code,
                Paperwork_QualifyingExigencyCert.Code,
                Paperwork_CertForSeriousOrIllnessVeteran.Code,
                Paperwork_CertForSeriousOrIllnessCurrentSM.Code,
                Paperwork_EmployeeHealthCondition_CA.Code,
                Paperwork_EmployeeHealthCondition_OR.Code);

            // Documenation Not Received Denial
            CreateTemplate(
                "DENIED-DOCUMENTATION",
                "Documentation Not Received Denial",
                "Leave Denied for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Documents_not_received_denial_letter,
                true);


            // Need the following templates based off of the work flow diagram

            //TODO: get these configured
            // recert paperwork

            // Need to clarify with Seth whether the following commented lines are just todos or letters.
            CreateTemplate(
                "RECERT",
                "Request for Recertification", " Request for Recertification ELGNOTICE for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Request_For_Recertification,
                true,
                Paperwork_EmployeeHealthCondition.Code,
                Paperwork_FamilyMemberSHC.Code,
                Paperwork_QualifyingExigencyCert.Code,
                Paperwork_CertForSeriousOrIllnessVeteran.Code,
                Paperwork_CertForSeriousOrIllnessCurrentSM.Code,
                Paperwork_EmployeeHealthCondition_CA.Code,
                Paperwork_EmployeeHealthCondition_OR.Code);

            // rtw work release TODO: Add Return to work Release Template

            // rtw notifications
            CreateTemplate(
                "RTWNOTIFY",
                "Return To Work Notify", "Return to work notify for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Return_to_Work_Notify);

            // Request_for_Fitness_for_Duty_Certification
            CreateTemplate(
                "FITDCERT",
                "Request For Fitness Duty Certification", "Request for Fitness Duty Certification {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Request_for_Fitness_for_Duty_Certification,
                false,
                Paperwork_FitnessforDutyCert.Code);

            // Request_For_Second_Medical_Opinion
            CreateTemplate(
                "2MEDOP",
                "Request For Second Medical Opinion", "Request for Second Medical Opinion for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Request_For_Second_Medical_Opinion,
                true,
                Paperwork_AuthorizationToReleaseMedicalInformation.Code);

            // Request_For_Third_Medical_Opinion
            CreateTemplate(
                "3MEDOP",
                "Request For Third Medical Opinion", "Request for Third Medical Opinion for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Request_For_Third_Medical_Opinion,
                true);

            // Accommodations Acknowledgement Letter
            CreateTemplate(
                "ACCOMACK",
                "Accommodation Acknowledgement Letter",
                "Accommodation Acknowledgement Letter for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_Acknowledgement,
                false,
                Paperwork_AccommodationRequestForm.Code);

            // Accommodation Incomplete
            CreateTemplate(
                "INCACCOM",
                "Accommodation Documentation Incomplete",
                "Accommodation Documentation Incomplete for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_Incomplete,
                false,
               Paperwork_AccommodationRequestForm.Code);

            // Accommodation NotReceived
            CreateTemplate(
                "ACCOM-NOTRECEIVED",
                "Accommodation Documentation Not Received",
                "Accommodation Documentation Not Received for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                true,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_NotReceived,
                false,
                Paperwork_AccommodationRequestForm.Code);

            // Accommodation Approval
            CreateTemplate(
                "ACCOM-APPROVED",
                "Accommodation Approval",
                "Accommodation Approval for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_Approval);

            // Accommodation Approval To HR
            CreateTemplate(
                "ACCOM-APPROVED-HR",
                "Accommodation Approval HR Communication",
                "Accommodation Approval for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_ApprovalToHR);

            // Accommodation Approval To SUP
            CreateTemplate(
                "ACCOM-APPROVED-SUP",
                "Accommodation Approval Manager Communication",
                "Accommodation Approval for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_ApprovalToSUP);


            // Accommodation Denial
            CreateTemplate(
                "ACCOM-DENIED",
                "Accommodation Denial",
                "Accommodation Denial for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_Denial);

            // Accommodation Case Closure
            CreateTemplate(
                "ACCOM-CASECLOSE",
                "Accommodation Case Closure",
                "Accommodation Closure for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_Case_Closure);

            // Accommodation Notice of End of Temp Accommodation
            CreateTemplate(
                "ACCOM-ENDTEMP",
                "Notice of End of Temporary Accommodation",
                "Notice of End of Temporary Accommodation for {{Employee.FirstName}} {{Employee.LastName}}, Case # {{Case.CaseNumber}}",
                null,
                false,
                DocumentType.MSWordDocument,
                MSWordTemplates.Accommodation_EndOfTemporary);

            Console.WriteLine("Finished creating CORE Communication Templates");
        }

        private static Template CreateTemplate(string code, string name, string subject, string body, bool attachLatestIncompletePaperwork, DocumentType docType, byte[] document, bool isLOASpecific = false, params string[] paperworkCodes)
        {
            try
            {
                Console.WriteLine("Creating template, {0}", code);
                Template tmp = Template.AsQueryable().FirstOrDefault(t => t.Code == code && t.EmployerId == null && t.CustomerId == null) ?? new Template();

                // Updates are done through admin tool now, don't override any changes UI has made
                if (tmp.IsNew)
                {

                    string fileExtension = null;
                    switch (docType)
                    {
                        case DocumentType.Template:
                        case DocumentType.Html:
                            fileExtension = ".htm";
                            break;
                        case DocumentType.MSWordDocument:
                            fileExtension = ".docx";
                            break;
                        case DocumentType.Pdf:
                            fileExtension = ".pdf";
                            break;
                        case DocumentType.None:
                        default:
                            break;
                    }

                    string documentName = string.Format("{0}{1}", name, fileExtension);

                    tmp.Body = body;
                    tmp.Code = code;
                    tmp.Name = name;
                    tmp.Subject = subject;
                    tmp.AttachLatestIncompletePaperwork = attachLatestIncompletePaperwork;
                    tmp.PaperworkCodes = paperworkCodes.ToList();
                    tmp.CreatedById = "000000000000000000000000";
                    tmp.ModifiedById = "000000000000000000000000";
                    tmp.IsLOASpecific = isLOASpecific;
                    tmp.DocType = docType;
                    tmp.DocumentName = documentName;
                    tmp.DocumentId = CreateFile(documentName, docType, document, null, null, tmp.Document);
                }

                return tmp.Save();
            }
            finally
            {
                Console.WriteLine("Finished creating template, {0}", code);
            }
        }
        internal static Template CreateEmployerTemplate(string customerId, string employerId, string code, string name, string subject, string body, bool attachLatestIncompletePaperwork, bool isLOASpecific = false, params string[] paperworkCodes)
        {
            try
            {
                Console.WriteLine("Creating employer template, {0}, for {1}", code, employerId);
                Template tmp = Template.AsQueryable().FirstOrDefault(t => t.Code == code && t.EmployerId == employerId && t.CustomerId == customerId) ?? new Template();
                tmp.CustomerId = customerId;
                tmp.EmployerId = employerId;
                tmp.Body = body;
                tmp.Code = code;
                tmp.Name = name;
                tmp.Subject = subject;
                tmp.AttachLatestIncompletePaperwork = attachLatestIncompletePaperwork;
                tmp.PaperworkCodes = paperworkCodes.ToList();
                tmp.CreatedById = "000000000000000000000000";
                tmp.ModifiedById = "000000000000000000000000";
                tmp.IsLOASpecific = isLOASpecific;
                tmp.DocType = DocumentType.Html;
                return tmp.Save();
            }
            finally
            {
                Console.WriteLine("Finished creating template, {0}, for {1}", code, employerId);
            }
        }
        private static Paperwork CreatePaperwork(string name, string code, string fileName, DocumentType type, byte[] file, string description, TimeSpan? dueBack, bool requiresReview, bool isForApprovalDecision, string enclosureText, List<PaperworkCriteriaGroup> criteria, bool fillPdf = false, Dictionary<string, List<string>> pdfFields = null)
        {
            try
            {
                Console.WriteLine("Creating paperwork, {0}", name);
                Paperwork p = Paperwork.AsQueryable().FirstOrDefault(o => o.Code == code && o.CustomerId == null && o.EmployerId == null) ?? new Paperwork();
                // Updates Done Through Admin Tool, now, don't overrwrite them
                if (p.IsNew)
                {
                    p.Name = name;
                    p.Code = code;
                    p.Description = description;
                    p.DocType = type;
                    p.EnclosureText = enclosureText;
                    p.ReturnDateAdjustment = dueBack;
                    p.FileName = fileName;
                    p.RequiresReview = requiresReview;
                    p.IsForApprovalDecision = isForApprovalDecision;
                    p.Criteria = criteria ?? new List<PaperworkCriteriaGroup>();
                    p.ModifiedById = "000000000000000000000000";
                    p.FileId = CreateFile(fileName, type, file, null, null, p.Document);
                    p.FillPdf = fillPdf;
                    if (pdfFields != null && pdfFields.Any())
                    {
                        p.PdfFields = pdfFields;
                    }
                }

                return p.Save();
            }
            finally
            {
                Console.WriteLine("Finished creating paperwork, {0}", name);
            }
        }
        internal static Paperwork CreateEmployerPaperwork(string customerId, string employerId, string name, string fileName, DocumentType type, byte[] file, string description, TimeSpan? dueBack, bool requiresReview,bool isForApprovalDecision, string enclosureText, List<PaperworkField> fields, List<PaperworkCriteriaGroup> criteria)
        {
            try
            {
                Console.WriteLine("Creating template, {0} for {1}", name, employerId);
                Paperwork p = Paperwork.AsQueryable().FirstOrDefault(o => o.Name == name && o.EmployerId == employerId && o.CustomerId == customerId) ?? new Paperwork();
                p.Name = name;
                p.Description = description;
                p.DocType = type;
                p.EnclosureText = enclosureText;
                p.ReturnDateAdjustment = dueBack;
                p.FileName = fileName;
                p.RequiresReview = requiresReview;
                p.IsForApprovalDecision = isForApprovalDecision;
                p.Fields = fields ?? new List<PaperworkField>();
                p.Criteria = criteria ?? new List<PaperworkCriteriaGroup>();
                p.ModifiedById = "000000000000000000000000";
                p.FileId = CreateFile(fileName, type, file, customerId, employerId, p.Document);
                p.EmployerId = employerId;
                p.CustomerId = customerId;
                return p.Save();
            }
            finally
            {
                Console.WriteLine("Finished creating employer paperwork, {0} for {1}", name, employerId);
            }
        }

        private static string CreateFile(string fileName, DocumentType type, byte[] file, string customerId = null, string employerId = null, Document doc = null)
        {
            try
            {
                Console.WriteLine("Creating document, {0}", fileName);
                string contentType = "";
                switch (type)
                {
                    case DocumentType.Html:
                    case DocumentType.Template:
                        contentType = "text/html";
                        break;
                    case DocumentType.MSWordDocument:
                        contentType = fileName.EndsWith("x") ? "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "application/msword";
                        break;
                    case DocumentType.Pdf:
                        contentType = "application/pdf";
                        break;
                    case DocumentType.None:
                    default:
                        contentType = "text/plain";
                        break;
                }
                var myDoc = doc ?? new Document();
                myDoc.CustomerId = customerId;
                myDoc.EmployerId = employerId;
                myDoc.ContentLength = file.LongLength;
                myDoc.FileName = fileName;
                myDoc.File = file;
                myDoc.ContentType = contentType;
                myDoc.Metadata.SetRawValue("DocumentType", (int)type);
                string oldKey = myDoc.Locator;
                myDoc = myDoc.Upload();
                if (!string.IsNullOrWhiteSpace(oldKey))
                    using (var fs = new FileService())
                        fs.DeleteS3Object(oldKey, Common.Properties.Settings.Default.S3BucketName_Files ?? "files.absencetracker.com");
                return myDoc.Id;
            }
            finally
            {
                Console.WriteLine("Finished creating document, {0}", fileName);
            }
        }
    }
}
