﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Setup.Properties;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup
{
    public static class DiagnosisData
    {
        public static bool DoSetup = false;

        public static void SetupData()
        {
            if (DoSetup)
            {
                SetUpICD9();
                SetUpICD10();
            }
        }


        private static void SetUpICD10()
        {
            Console.WriteLine("Setting up ICD-10 Codes");
            int i = 0;
            BulkWriteOperation<DiagnosisCode> bulk = null;
            using (StringReader reader = new StringReader(Resources.ICD10))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    if (i == 0 || i >= 200)
                    {
                        i = 0;
                        if (bulk != null)
                            try
                            {
                                bulk.Execute();
                            }
                            catch (MongoBulkWriteException bulkEx)
                            {
                                Console.Error.WriteLine("Error writing ICD10 Code(s): {0}", bulkEx.Message);
                                if (bulkEx.WriteConcernError != null)
                                    Console.Error.WriteLine("\t{0} - {1}", bulkEx.WriteConcernError.Code, bulkEx.WriteConcernError.Message);
                                if (bulkEx.WriteErrors != null && bulkEx.WriteErrors.Count > 0)
                                    bulkEx.WriteErrors.ForEach(e => Console.Error.WriteLine("\t{0} {1} - {2}", e.Index, e.Code, e.Message));
                                Console.Error.Flush();
                                throw;
                            }
                        bulk = null;
                        bulk = DiagnosisCode.Repository.Collection.InitializeUnorderedBulkOperation();
                    }
                    string[] parts = line.Split('|');
                    string code = parts[0];
                    string desc = parts.Length > 1 ? parts[1] : code;
                    string name = (parts.Length > 2) ? parts[2] : desc;
                    string uiDesc = code == name ? code : string.Concat(code, " - ", name);
                    bulk.Find(DiagnosisCode.Query.And(DiagnosisCode.Query.EQ(d => d.Code, parts[0]), DiagnosisCode.Query.EQ(d => d.CodeType, DiagnosisType.ICD10)))
                        .Upsert()
                        .UpdateOne(DiagnosisCode.Updates
                        .SetOnInsert(d => d.Code, code)
                        .SetOnInsert(d => d.CodeType, DiagnosisType.ICD10)
                        .SetOnInsert(d => d.CreatedById, User.DefaultUserId)
                        .SetOnInsert(d => d.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(d => d.ModifiedById, User.DefaultUserId)
                        .SetOnInsert(d => d.ModifiedDate, DateTime.UtcNow)
                        .Set(d => d.Name, name)
                        .Set(d => d.Description, desc)
                        .Set(d => d.UIDescription, uiDesc));
                    i++;
                    line = reader.ReadLine();
                }
                bulk.Execute();
            }
            Console.WriteLine("Done setting up ICD-10 Codes");
        }// SetUpICD10


        private static void SetUpICD9()
        {
            Console.WriteLine("Setting up ICD-9 Codes");
            int i = 0;
            BulkWriteOperation<DiagnosisCode> bulk = null;
            using (StringReader reader = new StringReader(Resources.ICD9))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    if (i == 0 || i >= 250)
                    {
                        i = 0;
                        if (bulk != null)
                            bulk.Execute();
                        bulk = null;
                        bulk = DiagnosisCode.Repository.Collection.InitializeUnorderedBulkOperation();
                    }
                    string[] parts = line.Split('|');
                    string code = parts[0];
                    string desc = parts.Length > 1 ? parts[1] : code;
                    string name = (parts.Length > 2) ? parts[2] : desc;
                    string uiDesc = code == name ? code : string.Concat(code, " - ", name);
                    bulk.Find(DiagnosisCode.Query.And(DiagnosisCode.Query.EQ(d => d.Code, parts[0]), DiagnosisCode.Query.EQ(d => d.CodeType, DiagnosisType.ICD9)))
                        .Upsert()
                        .UpdateOne(DiagnosisCode.Updates
                        .SetOnInsert(d => d.Code, code)
                        .SetOnInsert(d => d.CodeType, DiagnosisType.ICD9)
                        .SetOnInsert(d => d.CreatedById, User.DefaultUserId)
                        .SetOnInsert(d => d.CreatedDate, DateTime.UtcNow)
                        .SetOnInsert(d => d.ModifiedById, User.DefaultUserId)
                        .SetOnInsert(d => d.ModifiedDate, DateTime.UtcNow)
                        .Set(d => d.Name, name)
                        .Set(d => d.Description, desc)
                        .Set(d => d.UIDescription, uiDesc));
                    i++;
                    line = reader.ReadLine();
                }
                bulk.Execute();
            }
            Console.WriteLine("Done setting up ICD-9 Codes");
        }// SetUpICD9
    }
}
