﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.ContactTypes
{
    public static class ContactTypesData
    {
        public static void SetupData()
        {
            ContactType ct;

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000000").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000000";
            ct.Code = "SELF";
            ct.Name = "Self";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Self;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000001").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000001";
            ct.Code = "SUPERVISOR";
            ct.Name = "Supervisor";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Administrative;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000002").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000002";
            ct.Code = "HR";
            ct.Name = "HR";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Administrative;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000003").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000003";
            ct.Code = "OTHER";
            ct.Name = "Other";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Administrative;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000004").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000004";
            ct.Code = "PROVIDER";
            ct.Name = "Provider";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Medical;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            //FMLA/Federally Defined Relationship Types
            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000005").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000005";
            ct.Code = "SPOUSE";
            ct.Name = "Spouse";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000006").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000006";
            ct.Code = "PARENT";
            ct.Name = "Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000007").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000007";
            ct.Code = "CHILD";
            ct.Name = "Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000008").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000008";
            ct.Code = "NEXTOFKIN";
            ct.Name = "Next Of Kin";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000013").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000013";
            ct.Code = "STEPPARENT";
            ct.Name = "Step Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000014").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000014";
            ct.Code = "FOSTERPARENT";
            ct.Name = "Foster Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000017").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000017";
            ct.Code = "STEPCHILD";
            ct.Name = "Step Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000018").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000018";
            ct.Code = "FOSTERCHILD";
            ct.Name = "Foster Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000019").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000019";
            ct.Code = "ADOPTEDCHILD";
            ct.Name = "Adopted Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000020").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000020";
            ct.Code = "LEGALWARD";
            ct.Name = "Legal Ward";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            //State Specific Relationship Types
            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000009").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000009";
            ct.Code = "DOMESTICPARTNER";
            ct.Name = "Domestic Partner";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CA", "CO", "NJ", "OR", "RI", "WA", "WI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000010").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000010";
            ct.Code = "CIVILUNIONPARTNER";
            ct.Name = "Civil Union Partner";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CO", "HI", "NJ", "RI", "VT" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000011").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000011";
            ct.Code = "SAMESEXSPOUSE";
            ct.Name = "Same Sex Spouse";
            ct.DisplayOnCommunicationsAs = "Spouse";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CO", "HI", "VT" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000012").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000012";
            ct.Code = "PARENTINLAW";
            ct.Name = "Parent In Law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CT", "HI", "MA", "NJ", "RI", "VT", "WA", "WI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000015").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000015";
            ct.Code = "GRANDPARENT";
            ct.Name = "Grand Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "HI", "MA", "MN", "OR", "RI", "WA" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000016").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000016";
            ct.Code = "GRANDPARENTINLAW";
            ct.Name = "Grand Parent In Law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "HI", "MA" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000021").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000021";
            ct.Code = "RESIDENCEINHOUSE";
            ct.Name = "Residence In House";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "FL", "IL" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000022").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000022";
            ct.Code = "COMMITTEDRELATIONSHIP";
            ct.Name = "Committed Relationship";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "DC", "WA" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000023").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000023";
            ct.Code = "CHILDSSIGNIFICANTOTHER";
            ct.Name = "Childs Significant Other";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "IL" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000024").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000024";
            ct.Code = "DOMESTICPARTNERSCHILD";
            ct.Name = "Domestic Partners Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CO", "DC", "NJ", "OR", "WA", "WI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000025").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000025";
            ct.Code = "CIVILUNIONPARTNERSCHILD";
            ct.Name = "Civil Union Partners Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "CO", "NJ", "OR", "WA", "WI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000026").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000026";
            ct.Code = "CHILDLIVINGWITHEMPLOYEE";
            ct.Name = "Child Living With Employee";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "DC", "FL", "HI", "IL", "OR" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000027").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000027";
            ct.Code = "GRANDCHILD";
            ct.Name = "Grand Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "DC", "HI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000028").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000028";
            ct.Code = "NIECE";
            ct.Name = "Niece";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "DC" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000029").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000029";
            ct.Code = "NEPHEW";
            ct.Name = "Nephew";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "DC" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000030").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000030";
            ct.Code = "RECIPROCALBENEFICIARY";
            ct.Name = "Reciprocal Beneficiary";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "HI" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000031").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000031";
            ct.Code = "ELDERLYRELATIVE";
            ct.Name = "Elderly Relative";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "MA" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000032").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000032";
            ct.Code = "SIBLING";
            ct.Name = "Sibling";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "MA", "MN", "OR" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000033").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000033";
            ct.Code = "SAMESEXSPOUSECHILD";
            ct.Name = "Same Sex Spouses Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.WorkStateRestrictions = new List<string>() { "WA" };
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            // medical contact types (plus "PROVIDER" above)
            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000034").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000034";
            ct.Code = "URGENTCARE";
            ct.Name = "Urgent Care";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Medical;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000035").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000035";
            ct.Code = "EMERGENCYROOM";
            ct.Name = "Emergency Room";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Medical;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000036").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000036";
            ct.Code = "OTHER";
            ct.Name = "Other";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Medical;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);
            // END: medical contact types

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000037").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000037";
            ct.Code = "INLOCOPARENTIS";
            ct.Name = "In Loco Parentis";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);


            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000038").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000038";
            ct.Code = "AUNT";
            ct.Name = "Aunt";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000039").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000039";
            ct.Code = "UNCLE";
            ct.Name = "Uncle";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000040").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000040";
            ct.Code = "FIRSTCOUSIN";
            ct.Name = "First Cousin";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000041").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000041";
            ct.Code = "SONINLAW";
            ct.Name = "Son-in-law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000042").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000042";
            ct.Code = "DAUGHTERINLAW";
            ct.Name = "Daughter-in-law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000043").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000043";
            ct.Code = "BROTHERINLAW";
            ct.Name = "Brother-in-law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000044").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000044";
            ct.Code = "SISTERINLAW";
            ct.Name = "Sister-in-law";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000045").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000045";
            ct.Code = "SPOUSEGRANDPARENT";
            ct.Name = "Spouse Grandparent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000046").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000046";
            ct.Code = "ADOPTEDGRANDPARENT";
            ct.Name = "Adopted Grandparent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000047").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000047";
            ct.Code = "ADOPTEDPARENT";
            ct.Name = "Adopted Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000048").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000048";
            ct.Code = "BENEFICIARY";
            ct.Name = "Beneficiary";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000049").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000049";
            ct.Code = "BLOODRELATIVE";
            ct.Name = "Blood Relative";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000050").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000050";
            ct.Code = "COPARENT";
            ct.Name = "Co-Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000051").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000051";
            ct.Code = "CUSTODIANOFVICTIM";
            ct.Name = "Custodian Of Victim";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000052").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000052";
            ct.Code = "DEPENDENT";
            ct.Name = "Dependent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000053").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000053";
            ct.Code = "DESIGNATEDPERSON";
            ct.Name = "Designated Person";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000054").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000054";
            ct.Code = "FORMERSPOUSE";
            ct.Name = "Former Spouse";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000055").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000055";
            ct.Code = "GUARDIAN";
            ct.Name = "Guardian";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000056").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000056";
            ct.Code = "INCOMPETENT";
            ct.Name = "Incompetent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000057").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000057";
            ct.Code = "INDIVIDUALFORWHOANEMPLOYEEISAREPRESENTATIVE";
            ct.Name = "Individual For Who An Employee Is A Representative";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000058").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000058";
            ct.Code = "LEGALGUARDIAN";
            ct.Name = "Legal Guardian";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000059").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000059";
            ct.Code = "LEGALWARDMINORCHILD";
            ct.Name = "Legal Ward (Minor Child)";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000060").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000060";
            ct.Code = "MINORCHILD";
            ct.Name = "Minor Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000061").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000061";
            ct.Code = "OTHERFAMILYMEMBER";
            ct.Name = "Other Family Member";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000062").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000062";
            ct.Code = "OTHERRELATIVEOFAVICTIM";
            ct.Name = "Other Relative Of A Victim";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000063").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000063";
            ct.Code = "PARENTOFADOMESTICPARTNER";
            ct.Name = "Parent Of A Domestic Partner";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000064").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000064";
            ct.Code = "PHYSICALLYORPSYCHOLOGICALLYINCAPACITATED";
            ct.Name = "Physically Or Psychologically Incapacitated";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000065").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000065";
            ct.Code = "RELATIVEBYMARRIAGE";
            ct.Name = "Relative By Marriage";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000066").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000066";
            ct.Code = "REPRESENTATIVEOFVICTIM";
            ct.Name = "Representative Of Victim";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000067").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000067";
            ct.Code = "SIGNIFICANTOTHER";
            ct.Name = "Significant Other";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000068").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000068";
            ct.Code = "ADOPTIVEPARENT";
            ct.Name = "Adoptive Parent";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000069").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000069";
            ct.Code = "STEPGRANDCHILD";
            ct.Name = "Step Grand Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000070").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000070";
            ct.Code = "ADOPTEDGRANDCHILD";
            ct.Name = "Adopted Grand Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000071").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000071";
            ct.Code = "FOSTERGRANDCHILD";
            ct.Name = "Foster Grand Child";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);

            ct = ContactType.AsQueryable().Where(c => c.Id == "000000000000000000000074").FirstOrDefault() ?? new ContactType();
            ct.Id = "000000000000000000000074";
            ct.Code = "FIANCE";
            ct.Name = "Fiancé";
            ct.ContactCategory = Data.Enums.ContactTypeDesignationType.Personal;
            ct.Save();
            Console.WriteLine("Saved Contact type '{0}' with Id '{1}'", ct.Code, ct.Id);
        }
    }
}
