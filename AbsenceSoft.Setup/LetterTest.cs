﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup
{
    public class LetterTest
    {
        public static void PopulateLetter(string sourceFileName, string caseId)
        {
            if (string.IsNullOrWhiteSpace(sourceFileName))
                throw new ArgumentNullException("sourceFileName", "You must specifiy a file name");

            if (!File.Exists(sourceFileName))
                throw new FileNotFoundException("sourceFileName does not exist or could not be found");

            string template = File.ReadAllText(sourceFileName);
            if (string.IsNullOrWhiteSpace(template))
                throw new ArgumentException("File found but did not contain any text");

            if (string.IsNullOrWhiteSpace(caseId))
                throw new ArgumentNullException("caseId", "You must specifiy a valid Case ID (not case number)");

            Case myCase = Case.GetById(caseId);
            if (myCase == null)
                throw new ArgumentException("Case was not found, ensure the case exists and the ID is a valid 22-digit ObjectID from MongoDB");

            Communication comm = new Communication()
            {
                CaseId = myCase.Id,
                EmployeeId = myCase.Employee.Id,
                EmployerId = myCase.EmployerId,
                CommunicationType = CommunicationType.Mail,
                Recipients = new List<Data.Contact>()
                {
                    new Data.Contact()
                    {
                        FirstName = "Hello",
                        LastName = "World",
                        Email = "hello.world@absencesoft.com",
                        Address = new Data.Address()
                        {
                            Address1 = "3 Rodeo Drive",
                            Address2 = "STE 310",
                            City = "Beverly Hills",
                            Country = "US",
                            Name = "Work",
                            PostalCode = "90210",
                            State = "CA"
                        }
                    }
                }
            };

            TemplateRenderModel model = new TemplateRenderModel();
            model.Case = myCase;
            if (model.Case.Contact != null)
            {
                var ct = ContactType.GetByCode(model.Case.Contact.ContactTypeCode, model.Case.CustomerId, model.Case.EmployerId);
                if (ct != null && !string.IsNullOrWhiteSpace(ct.DisplayOnCommunicationsAs))
                    model.Case.Contact.ContactTypeName = ct.DisplayOnCommunicationsAs;
            }
            model.Employee = model.Case.Employee;
            model.Employer = model.Case.Employer;
            model.Customer = model.Case.Employer.Customer;
            model.Communication = comm;
            model.ActiveSegment = model.Case.Segments.OrderByDescending(s => s.CreatedDate).First();

            template = AbsenceSoft.Rendering.Template.RenderTemplate(template, model);

            byte[] pdf = Rendering.Pdf.ConvertHtmlToPdf(template);

            string newFile = string.Format("{0}-{1}.pdf", sourceFileName, DateTime.UtcNow.Ticks);

            File.WriteAllBytes(newFile, pdf);

            Console.WriteLine("Created file: {0}", newFile);
        }
    }
}
