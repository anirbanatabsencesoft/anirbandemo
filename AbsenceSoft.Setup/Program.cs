﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Setup.Accommodation;
using AbsenceSoft.Setup.ContactTypes;
using AbsenceSoft.Setup.Features;
using AbsenceSoft.Setup.Reporting;
using System;
using System.Configuration;
using System.Linq;

namespace AbsenceSoft.Setup
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
                
                //*******************************************************************************
                // Register any custom serializers
                //*******************************************************************************
                AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();
                
                Console.WriteLine("Starting Schema Update");
                SchemaUpdate.SetupData();
                Console.WriteLine("Finished Schema Update");

				if (args.Any(a => a == "-c") && args.Any(a => a == "-f"))
				{
					try
					{
						string caseId = args[args.ToList().IndexOf("-c") + 1];
						string sourceFile = args[args.ToList().IndexOf("-f") + 1];
						LetterTest.PopulateLetter(sourceFile, caseId);
					}
					catch (Exception ex)
					{
						Console.Error.WriteLine(ex.ToString());
						Environment.Exit(9);
						return;
					}
					Environment.Exit(0);
					return;
				}

				//Diagnosis Codes
				if (args.Any(a => a == "-diagnosis"))
				{
					DiagnosisData.DoSetup = true;
				}

				//Accommodation Type
				AccommodationTypeData.SetupData();

                //Accommodation Questions
                AccommodationQuestionsData.SetupData();

				//Contact Types
				ContactTypesData.SetupData();

                // Report Data
                ReportData.SetupData();

				DateTime startDate = DateTime.UtcNow;
				DateTime? endDate = null;

				Console.WriteLine("Starting Data Setup (Core Data)");
                // Do not load core communication templates and paperwork
				CoreData.SetupData(buildCommunicationsAndPaperwork: false);
				Console.WriteLine("Finished Data Setup (Core Data)");

#if !RELEASE
				Console.WriteLine("Starting Data Setup (Demo Data)");
				DemoData.SetupData();
				Console.WriteLine("Finished Data Setup (Demo Data)");
#endif
				Console.WriteLine("Starting Data Setup (Test Data)");
				TestData.SetupData();
				Console.WriteLine("Finished Data Setup (Test Data)");

				Console.WriteLine("Starting Customer Features Validation");
				FeaturesData.SetupData();
				Console.WriteLine("Finished Customer Features Validation");

                try
                {
                    Console.WriteLine("Dropping the ASPNETDB Database");
                    new MongoDB.Driver.MongoClient(ConfigurationManager.ConnectionStrings["MongoServerSettings_ASPNETDB"].ConnectionString).DropDatabase(
                        MongoDB.Driver.MongoUrl.Create(ConfigurationManager.ConnectionStrings["MongoServerSettings_ASPNETDB"].ConnectionString).DatabaseName);
                    Console.WriteLine("Dropped the ASPNETDB Database");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                }
                
                try
                {
                    Console.WriteLine("Dropping the Logging Database");
                    new MongoDB.Driver.MongoClient(ConfigurationManager.ConnectionStrings["MongoServerSettings_Logging"].ConnectionString).DropDatabase(
                        MongoDB.Driver.MongoUrl.Create(ConfigurationManager.ConnectionStrings["MongoServerSettings_Logging"].ConnectionString).DatabaseName);
                    Console.WriteLine("Dropped the Logging Database");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                }

				endDate = DateTime.UtcNow;
				Console.WriteLine("Finished in {0}", (endDate - startDate));
				Environment.Exit(0);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.ToString());
				//Console.ReadLine();
				Environment.Exit(9);
			}
		}
	}
}
