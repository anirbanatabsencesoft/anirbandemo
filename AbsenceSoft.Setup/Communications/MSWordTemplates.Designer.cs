﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AbsenceSoft.Setup.Communications {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class MSWordTemplates {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MSWordTemplates() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AbsenceSoft.Setup.Communications.MSWordTemplates", typeof(MSWordTemplates).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Acknowledgement {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Acknowledgement", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Approval {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Approval", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_ApprovalToHR {
            get {
                object obj = ResourceManager.GetObject("Accommodation_ApprovalToHR", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_ApprovalToSUP {
            get {
                object obj = ResourceManager.GetObject("Accommodation_ApprovalToSUP", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Case_Closure {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Case_Closure", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Denial {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Denial", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_EndOfTemporary {
            get {
                object obj = ResourceManager.GetObject("Accommodation_EndOfTemporary", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Incomplete {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Incomplete", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_NotReceived {
            get {
                object obj = ResourceManager.GetObject("Accommodation_NotReceived", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] CaseClosure {
            get {
                object obj = ResourceManager.GetObject("CaseClosure", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Designation_Notice_Leave_Approved {
            get {
                object obj = ResourceManager.GetObject("Designation_Notice_Leave_Approved", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Designation_Notice_Leave_Not_Approved {
            get {
                object obj = ResourceManager.GetObject("Designation_Notice_Leave_Not_Approved", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Documents_not_received_denial_letter {
            get {
                object obj = ResourceManager.GetObject("Documents_not_received_denial_letter", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Eligibility_and_Rights_and_Responsibilities_Notice {
            get {
                object obj = ResourceManager.GetObject("Eligibility_and_Rights_and_Responsibilities_Notice", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Eligibility_Notice_Leave_Exhausted {
            get {
                object obj = ResourceManager.GetObject("Eligibility_Notice_Leave_Exhausted", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Eligibility_Notice_Not_Eligible {
            get {
                object obj = ResourceManager.GetObject("Eligibility_Notice_Not_Eligible", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Eligibility_USERRA_Letter_to_Employee {
            get {
                object obj = ResourceManager.GetObject("Eligibility_USERRA_Letter_to_Employee", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] EmailToHR {
            get {
                object obj = ResourceManager.GetObject("EmailToHR", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] EmailToMGR {
            get {
                object obj = ResourceManager.GetObject("EmailToMGR", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] FMLA_Leave_Expiring_Shortly {
            get {
                object obj = ResourceManager.GetObject("FMLA_Leave_Expiring_Shortly", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Incomplete_or_Insufficient_Certification {
            get {
                object obj = ResourceManager.GetObject("Incomplete_or_Insufficient_Certification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Notice_of_Failure_To_Provide_Certification {
            get {
                object obj = ResourceManager.GetObject("Notice_of_Failure_To_Provide_Certification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Request_for_Fitness_for_Duty_Certification {
            get {
                object obj = ResourceManager.GetObject("Request_for_Fitness_for_Duty_Certification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Request_For_Recertification {
            get {
                object obj = ResourceManager.GetObject("Request_For_Recertification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Request_For_Second_Medical_Opinion {
            get {
                object obj = ResourceManager.GetObject("Request_For_Second_Medical_Opinion", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Request_For_Third_Medical_Opinion {
            get {
                object obj = ResourceManager.GetObject("Request_For_Third_Medical_Opinion", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Return_to_Work_Notify {
            get {
                object obj = ResourceManager.GetObject("Return_to_Work_Notify", resourceCulture);
                return ((byte[])(obj));
            }
        }
    }
}
