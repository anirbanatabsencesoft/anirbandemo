﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Processing;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Workflows;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Linq;
using MongoDB.Bson;
using System.Configuration;

namespace AbsenceSoft.Setup
{
    public static class SchemaUpdate
    {
        public static void SetupData()
        {
            //Drop old indexes before 1-25-18 optimization
            if (Employee.Repository.Collection.IndexExists(IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeNumber")))
                Employee.Repository.Collection.DropIndex(
                    IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeNumber"));

            if (Employee.Repository.Collection.IndexExistsByName("Employee_List"))
                Employee.Repository.Collection.DropIndexByName("Employee_List");

            if (Employee.Repository.Collection.IndexExistsByName("Employee_FindByCustomerEmail"))
                Employee.Repository.Collection.DropIndexByName("Employee_FindByCustomerEmail");

            Employee.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "LastName"),
               IndexOptions.SetBackground(true));
            Employee.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "EmployeeNumber"),
               IndexOptions.SetBackground(true));
            Employee.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "HireDate"),
               IndexOptions.SetBackground(true));
            Employee.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "Info.Email"),
               IndexOptions.SetBackground(true));
            // Greatly improves EL file processing for any file that includes SSN
            Employee.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("Ssn.h"),
               IndexOptions.SetBackground(true));

            Communication.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "CaseId", "IsDraft"),
               IndexOptions.SetBackground(true));

            User.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted", "DisabledDate"),
               IndexOptions.SetBackground(true));

            #region ETL Indexes

            BuildEtlIndex<AbsenceReason>();
            BuildEtlIndex<AccommodationQuestion>();
            BuildEtlIndex<AccommodationType>();
            BuildEtlIndex<ActivityHistory>();
            BuildEtlIndex<Attachment>();
            BuildEtlIndex<CalculatedRiskProfile>();
            BuildEtlIndex<Case>();
            BuildEtlIndex<CaseAssignee>();
            BuildEtlIndex<CaseAssigneeType>();
            BuildEtlIndex<CaseAssignmentRuleSet>();
            BuildEtlIndex<CaseNote>();
            BuildEtlIndex<Communication>();
            BuildEtlIndex<ConsultationType>();
            BuildEtlIndex<ContactType>();
            BuildEtlIndex<Content>();
            BuildEtlIndex<CustomField>();
            BuildEtlIndex<Customer>();
            BuildEtlIndex<CustomerServiceOption>();
            BuildEtlIndex<Demand>();
            BuildEtlIndex<DemandType>();
            BuildEtlIndex<DiagnosisCode>();
            BuildEtlIndex<DiagnosisGuidelines>();
            BuildEtlIndex<Document>();
            BuildEtlIndex<Employee>();
            BuildEtlIndex<EmployeeConsultation>();
            BuildEtlIndex<EmployeeContact>();
            BuildEtlIndex<EmployeeJob>();
            BuildEtlIndex<EmployeeNecessity>();
            BuildEtlIndex<EmployeeNote>();
            BuildEtlIndex<EmployeeOrganization>();
            BuildEtlIndex<EmployeeRestriction>();
            BuildEtlIndex<Employer>();
            BuildEtlIndex<EmployerContact>();
            BuildEtlIndex<EmployerContactType>();
            BuildEtlIndex<EmployerServiceOption>();
            BuildEtlIndex<Event>();
            BuildEtlIndex<IpRange>();
            BuildEtlIndex<Job>();
            BuildEtlIndex<Necessity>();
            BuildEtlIndex<NoteCategory>();
            BuildEtlIndex<NoteTemplate>();
            BuildEtlIndex<NotificationConfiguration>();
            BuildEtlIndex<Organization>();
            BuildEtlIndex<OrganizationAnnualInfo>();
            BuildEtlIndex<OrganizationType>();
            BuildEtlIndex<Paperwork>();
            BuildEtlIndex<PaySchedule>();
            BuildEtlIndex<Policy>();
            BuildEtlIndex<ReportDefinition>();
            BuildEtlIndex<ReportField>();
            BuildEtlIndex<ReportType>();
            BuildEtlIndex<ReportTypeField>();
            BuildEtlIndex<RiskProfile>();
            BuildEtlIndex<Role>();
            BuildEtlIndex<Team>();
            BuildEtlIndex<TeamMember>();
            BuildEtlIndex<Template>();
            BuildEtlIndex<ToDoItem>();
            BuildEtlIndex<User>();
            BuildEtlIndex<VariableScheduleTime>();
            BuildEtlIndex<Workflow>();
            BuildEtlIndex<WorkflowInstance>();

            #endregion ETL Indexes
                        
            // This is a bad index
            if (Case.Repository.Collection.IndexExists(IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeId", "StartDate", "EndDate")))
                Case.Repository.Collection.DropIndex(IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeId", "StartDate", "EndDate"));
            // This is a gooder one
            Case.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId", "EmployerId", "Employee._id", "StartDate", "EndDate"),
                IndexOptions.SetBackground(true));
            Case.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId").Ascending("EmployerId").Ascending("cdt"),
                IndexOptions.SetBackground(true));
            //1-25-optimization
            Case.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId", "EmployerId", "CaseNumber", "IsDeleted"),
                IndexOptions.SetBackground(true));
            Case.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId", "EmployerId", "Employee._id", "IsDeleted"),
                IndexOptions.SetBackground(true));
            //Optimize Sorting on LastName and EmployeeNo for large dataset
            Case.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("Employee.LastName", "Employee.EmployeeNumber"),
                IndexOptions.SetBackground(true));


            EligibilityUploadResult.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("EligibilityUploadId", "IsError", "RowNumber"));

            EligibilityUploadResult.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("EligibilityUploadId", "RowNumber"));

            if (DiagnosisCode.Repository.Collection.IndexExists(IndexKeys.Ascending("Code")))
                DiagnosisCode.Repository.Collection.DropIndex(IndexKeys.Ascending("Code"));
            DiagnosisCode.Repository.Collection.CreateIndex
                (IndexKeys.Ascending("Code", "CodeType"), IndexOptions.SetUnique(true).SetSparse(true));

            if (EmployeeContact.Repository.Collection.IndexExists(IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeId", "RelatedEmployeeNumber")))
                EmployeeContact.Repository.Collection.DropIndex(IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeId", "RelatedEmployeeNumber"));
            EmployeeContact.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId", "EmployerId", "EmployeeId", "ContactTypeCode"),
                IndexOptions.SetSparse(true));
            EmployeeContact.Repository.Collection.CreateIndex(IndexKeys.Ascending("CustomerId", "EmployerId", "RelatedEmployeeNumber", "ContactTypeCode"), 
                IndexOptions.SetBackground(true));

            VariableScheduleTime.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("EmployeeId").Ascending("Time.SampleDate"));

            if (!SsoIdCacheEntry.Repository.Collection.IndexExists(IndexKeys<SsoIdCacheEntry>.Ascending(s => s.Expires)))
                SsoIdCacheEntry.Repository.Collection.CreateIndex(IndexKeys<SsoIdCacheEntry>.Ascending(s => s.Expires), IndexOptions<SsoIdCacheEntry>.SetTimeToLive(TimeSpan.Zero));
            
            if (!SsoSession.Repository.Collection.IndexExists(IndexKeys<SsoSession>.Ascending(s => s.Expires)))
                SsoSession.Repository.Collection.CreateIndex(IndexKeys<SsoSession>.Ascending(s => s.Expires), IndexOptions<SsoSession>.SetTimeToLive(TimeSpan.Zero));
            
            if (!SsoProfile.Repository.Collection.IndexExists(IndexKeys<SsoProfile>.Ascending(s => s.EntityId)))
                SsoProfile.Repository.Collection.CreateIndex(IndexKeys<SsoProfile>.Ascending(s => s.EntityId), IndexOptions<SsoProfile>.SetBackground(true).SetUnique(true));
            if (!ToDoItem.Repository.Collection.IndexExists(IndexKeys<ToDoItem>.Ascending(t => t.WorkflowInstanceId)))
                ToDoItem.Repository.Collection.CreateIndex(IndexKeys<ToDoItem>.Ascending(t => t.WorkflowInstanceId), IndexOptions<ToDoItem>.SetBackground(true));


            PaySchedule.Repository.Collection.Update(Query.Null, Update.Unset("Default"), UpdateFlags.Multi);
            Policy.Repository.Collection.Update(Query.Null, Update.Unset("SupplementalTo"), UpdateFlags.Multi);

            ToDoItem.Repository.Collection.CreateIndex(IndexKeys<ToDoItem>.Ascending(t => t.CustomerId, t => t.EmployerId, t => t.CaseId, t => t.Status));
            ToDoItem.Repository.Collection.CreateIndex(
               IndexKeys.Ascending("CustomerId", "EmployerId", "IsDeleted"),
               IndexOptions.SetBackground(true));

            if (!User.Repository.Collection.IndexExists(IndexKeys<User>.Ascending(s => s.Email)))
                User.Repository.Collection.CreateIndex(IndexKeys<User>.Ascending(s => s.Email), IndexOptions<User>.SetUnique(true));
            // Index for finding organizations by code for an employer
            if (!Organization.Repository.Collection.IndexExists(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Code)))
                Organization.Repository.Collection.CreateIndex(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Code));

            // Index for finding organizations by full path for an employer for re-parenting and orphaning tree searches and traversals
            if (!Organization.Repository.Collection.IndexExists(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Path)))
                Organization.Repository.Collection.CreateIndex(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Path));
            if (!Organization.Repository.Collection.IndexExists(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.TypeCode)))
                Organization.Repository.Collection.CreateIndex(IndexKeys<Organization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.TypeCode));
            if (!OrganizationType.Repository.Collection.IndexExists(IndexKeys<OrganizationType>.Ascending(o => o.CustomerId, o => o.EmployerId)))
                OrganizationType.Repository.Collection.CreateIndex(IndexKeys<OrganizationType>.Ascending(o => o.CustomerId, o => o.EmployerId));


            if (!Content.Repository.Collection.IndexExists(IndexKeys<Content>.Ascending(s => s.CustomerId)))
                Content.Repository.Collection.CreateIndex(IndexKeys<Content>.Ascending(s => s.CustomerId), IndexOptions<Content>.SetSparse(true).SetBackground(true));
            // Index for UI and EL  to match up and upsert employee organizations
            if (!EmployeeOrganization.Repository.Collection.IndexExists(IndexKeys<EmployeeOrganization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.EmployeeNumber, o => o.TypeCode, o => o.Code)))
                EmployeeOrganization.Repository.Collection.CreateIndex(IndexKeys<EmployeeOrganization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.EmployeeNumber, o => o.TypeCode, o => o.Code));
            // Index for re-parenting and orphaning tree searches and traversals
            if (!EmployeeOrganization.Repository.Collection.IndexExists(IndexKeys<EmployeeOrganization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Path)))
                EmployeeOrganization.Repository.Collection.CreateIndex(IndexKeys<EmployeeOrganization>.Ascending(o => o.CustomerId, o => o.EmployerId, o => o.Path));

            string connectionString = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            MongoUrl url = MongoUrl.Create(connectionString);
            MongoClientSettings settings = MongoClientSettings.FromUrl(url);
            settings.GuidRepresentation = GuidRepresentation.Standard;
            string databaseName = url.DatabaseName;
            MongoClient client = new MongoClient(settings);
            MongoServer server = client.GetServer();
            var db = server.GetDatabase(databaseName);
            if (db.CollectionExists("DataUpdatePreference"))
                db.DropCollection("DataUpdatePreference");

            // CustomerServiceOption Unique Index
            if (!CustomerServiceOption.Repository.Collection.IndexExists(
                IndexKeys<CustomerServiceOption>.Ascending(p => p.CustomerId, p => p.Key, p => p.Value)))
                CustomerServiceOption.Repository.Collection.CreateIndex(
                    IndexKeys<CustomerServiceOption>.Ascending(p => p.CustomerId, p => p.Key, p => p.Value),
                    IndexOptions<CustomerServiceOption>.SetBackground(true).SetSparse(false).SetUnique(true));

            // EmployerServiceOption Unique Index (Correct Unique Index)
            if (!EmployerServiceOption.Repository.Collection.IndexExists(
                IndexKeys<EmployerServiceOption>.Ascending(p => p.CustomerId, p => p.EmployerId, p => p.Key)))
                EmployerServiceOption.Repository.Collection.CreateIndex(
                    IndexKeys<EmployerServiceOption>.Ascending(p => p.CustomerId, p => p.EmployerId, p => p.Key),
                    IndexOptions<EmployerServiceOption>.SetBackground(true).SetSparse(false).SetUnique(true));

            if (!CaseNote.Repository.Collection.IndexExistsByName("Case_Notes"))
                CaseNote.Repository.Collection.CreateIndex(
                    IndexKeys.Ascending("CaseId"),
                    IndexOptions.SetName("Case_Notes"));

            if (!Attachment.Repository.Collection.IndexExistsByName("Case_Attachments"))
                Attachment.Repository.Collection.CreateIndex(
                    IndexKeys.Ascending("CaseId"),
                    IndexOptions.SetName("Case_Attachments"));

            Attachment.Repository.Collection.CreateIndex(
                IndexKeys.Ascending("CustomerId", "EmployerId"),
                IndexOptions.SetBackground(true));

            if (!Event.Repository.Collection.IndexExistsByName("Event_EventsByTypeAndDate"))
                Event.Repository.Collection.CreateIndex(
                    IndexKeys<Event>
                        .Ascending(e => e.CustomerId)
                        .Ascending(e => e.EmployerId)
                        .Ascending(e => e.IsDeleted)
                        .Ascending(e => e.EventType),
                    IndexOptions<Event>
                        .SetBackground(true)
                        .SetName("Event_EventsByTypeAndDate")
                );
        }

        /// <summary>
        /// Builds the index for the ETL process if used by ETL. Each relevant collection should have one.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        static void BuildEtlIndex<TEntity>() where TEntity : BaseEntity<TEntity>, new()
        {
            // Remove the old and busted version of these indexes, these used mdt + _id and that's just
            //  not the case anymore with our new ETL sync logic. Instead we should use just mdt, simple and easy
            string oldAndBustedIndexName = "Etl_Sync_Sort";
            if (BaseEntity<TEntity>.Repository.Collection.IndexExistsByName(oldAndBustedIndexName))
                BaseEntity<TEntity>.Repository.Collection.DropIndexByName(oldAndBustedIndexName);

            //Index required for ad-hoc ETL
            if (!BaseEntity<TEntity>.Repository.Collection.IndexExistsByName(DataConstants.EtlSyncSortByIndexName))
            {
                // Now, determine if there's already an index by key w/o our awesome pretty special name, and if so, drop that sucker
                if (BaseEntity<TEntity>.Repository.Collection.IndexExists(IndexKeys<TEntity>.Ascending(e => e.ModifiedDate)))
                    BaseEntity<TEntity>.Repository.Collection.DropIndex(IndexKeys<TEntity>.Ascending(e => e.ModifiedDate));

                // Now build our super awesome beautifully named index that's awesome
                BaseEntity<TEntity>.Repository.Collection.CreateIndex(IndexKeys<TEntity>.Ascending(e => e.ModifiedDate), 
                    IndexOptions.SetBackground(true).SetName(DataConstants.EtlSyncSortByIndexName));
            }
        }        
    }
}
