﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AbsenceSoft.Setup.PaperworkFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class MSWordPaperwork {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MSWordPaperwork() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AbsenceSoft.Setup.PaperworkFiles.MSWordPaperwork", typeof(MSWordPaperwork).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Accommodation_Request_Form {
            get {
                object obj = ResourceManager.GetObject("Accommodation_Request_Form", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] AUTHORIZATION_TO_RELEASE_MEDICAL_INFORMATION {
            get {
                object obj = ResourceManager.GetObject("AUTHORIZATION_TO_RELEASE_MEDICAL_INFORMATION", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] cert_disability_adult_child {
            get {
                object obj = ResourceManager.GetObject("cert_disability_adult_child", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Certification_for_FMLA_Employee_Health_Condition {
            get {
                object obj = ResourceManager.GetObject("Certification_for_FMLA_Employee_Health_Condition", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Certification_for_NON_FMLA_Employee_Health_Condition {
            get {
                object obj = ResourceManager.GetObject("Certification_for_NON_FMLA_Employee_Health_Condition", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Certification_for_Serious_Injury_or_Illness_of_a_Current_Servicemember {
            get {
                object obj = ResourceManager.GetObject("Certification_for_Serious_Injury_or_Illness_of_a_Current_Servicemember", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Certification_for_Serious_Injury_Or_Illness_Of_a_Veteran {
            get {
                object obj = ResourceManager.GetObject("Certification_for_Serious_Injury_Or_Illness_Of_a_Veteran", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Certification_of_Healthcare_Provider_for_Non_FMLA_Family {
            get {
                object obj = ResourceManager.GetObject("Certification_of_Healthcare_Provider_for_Non_FMLA_Family", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] CertMilitaryCaregiverLeave {
            get {
                object obj = ResourceManager.GetObject("CertMilitaryCaregiverLeave", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DFEH_100_20__04_16_ {
            get {
                object obj = ResourceManager.GetObject("DFEH_100_20__04_16_", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DFEH_100_210_rev072015 {
            get {
                object obj = ResourceManager.GetObject("DFEH_100_210_rev072015", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness {
            get {
                object obj = ResourceManager.GetObject("DOL_FMLA_Certification_CurrentServiceMemberInjuryIllness", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DOL_FMLA_Certification_EmployeeHealth {
            get {
                object obj = ResourceManager.GetObject("DOL_FMLA_Certification_EmployeeHealth", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DOL_FMLA_Certification_FamilyHealth {
            get {
                object obj = ResourceManager.GetObject("DOL_FMLA_Certification_FamilyHealth", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DOL_FMLA_Certification_QualifyingExigency {
            get {
                object obj = ResourceManager.GetObject("DOL_FMLA_Certification_QualifyingExigency", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] DOL_FMLA_Certification_VeteranInjuryIllness {
            get {
                object obj = ResourceManager.GetObject("DOL_FMLA_Certification_VeteranInjuryIllness", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Employee_Active_Duty_Return_Notification_letter_to_Employer {
            get {
                object obj = ResourceManager.GetObject("Employee_Active_Duty_Return_Notification_letter_to_Employer", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Family_Member_Serious_Health_Condition {
            get {
                object obj = ResourceManager.GetObject("Family_Member_Serious_Health_Condition", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Fitness_for_Duty_Certification {
            get {
                object obj = ResourceManager.GetObject("Fitness_for_Duty_Certification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] OFLA_SHC_Cert {
            get {
                object obj = ResourceManager.GetObject("OFLA_SHC_Cert", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] Qualifying_Exigency_Certification {
            get {
                object obj = ResourceManager.GetObject("Qualifying_Exigency_Certification", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] USERRA_Information_Posting {
            get {
                object obj = ResourceManager.GetObject("USERRA_Information_Posting", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] userra_private {
            get {
                object obj = ResourceManager.GetObject("userra_private", resourceCulture);
                return ((byte[])(obj));
            }
        }
    }
}
