﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Logic.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Setup
{
    public static class TestData3
    {
        public static void SetupData()
        {
            Contact demoContact = new Contact()
            {
                Email = "markbacom@absencesoft.com",
                WorkPhone = "+1.303.332.6852",
                FirstName = "QA",
                LastName = "Administrator",
                Address = new Address()
                {
                    Address1 = "10200 W 44th AVE",
                    Address2 = "SUITE 140",
                    City = "Wheat Ridge",
                    State = "CO",
                    PostalCode = "80033"
                },
            };

            Console.WriteLine("Creating Customer 3/Employer 3 'AbsenceSoft Employer 3'");
            Customer testCustomer = Customer.GetById("000000000000000000000003") ?? new Customer() { Id = "000000000000000000000003" };
            testCustomer.Name = "AbsenceSoft Employer Test 3";
            testCustomer.Url = "Test3";
            testCustomer.Contact = demoContact;
            testCustomer.CreatedById = "000000000000000000000000";
            testCustomer.ModifiedById = "000000000000000000000000";
            testCustomer.Save();
            Console.WriteLine("Saved customer 'AbsenceSoft Demo' with Id '{0}'", testCustomer.Id);

            new AbsenceSoft.Logic.Customers.CustomerService().Using(s => s.EnsureDefaultRolesForCustomer(testCustomer.Id));

            Console.WriteLine("Creating employer 'AbsenceSoft Empoyer 3'");
            Employer testEmployer = Employer.GetById("000000000000000000000003") ?? new Employer() { Id = "000000000000000000000003" };
            testEmployer.Name = "AbsenceSoft Employer Test 3";
            testEmployer.ReferenceCode = "Test 3";
            testEmployer.Contact = demoContact;
            testEmployer.Customer = testCustomer;
            testEmployer.CustomerId = testCustomer.Id;
            testEmployer.Ein = new CryptoString("11-111-1111");
            testEmployer.ResetMonth = Month.January;
            testEmployer.ResetDayOfMonth = 1;
            testEmployer.FMLPeriodType = PeriodType.RollingBack;
            testEmployer.FTWeeklyWorkHours = 35;
            testEmployer.Holidays.Clear();
            testEmployer.Holidays.AddRange(new List<Holiday>()
            {
                new Holiday() { Name = "New Year’s Day",                Month = Month.January,      DayOfMonth = 1, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                new Holiday() { Name = "Martin Luther King’s Birthday", Month = Month.January,      WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "George Washington's Birthday",  Month = Month.February,     WeekOfMonth = 3,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "Memorial Day",                  Month = Month.May,          WeekOfMonth = -1,   DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "Independence Day",              Month = Month.July,         DayOfMonth = 4, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                new Holiday() { Name = "Labor Day",                     Month = Month.September,    WeekOfMonth = 1,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "Columbus Day",                  Month = Month.October,      WeekOfMonth = 2,    DayOfWeek = DayOfWeek.Monday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "Veterans Day",                  Month = Month.November,     DayOfMonth = 11, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true },
                new Holiday() { Name = "Thanksgiving Day",              Month = Month.November,     WeekOfMonth = 4,    DayOfWeek = DayOfWeek.Thursday, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc) },
                new Holiday() { Name = "Christmas Day",                 Month = Month.December,     DayOfMonth = 25, StartDate = new DateTime(2010,1,1, 0, 0, 0, DateTimeKind.Utc), OrClosestWeekday = true }
            });
			testEmployer.Features = new List<AppFeature>()
			{
				new AppFeature(){Feature = Feature.ADA, Enabled= true}, 
				new AppFeature(){Feature = Feature.ShortTermDisability, Enabled = true},
                new AppFeature() { Feature = Feature.LOA, Enabled = true }
			};
            testEmployer.CreatedById = "000000000000000000000000";
            testEmployer.ModifiedById = "000000000000000000000000";
            testEmployer.Save();
            Console.WriteLine("Saved employer 3 'AbsenceSoft Test' with Id '{0}'", testEmployer.Id);

            Console.WriteLine("Creating user 'QA 3(qa3@absencesoft.com)'");
            User qaUser = User.GetById("000000000000000000000003") ?? new User() { Id = "000000000000000000000003" };
            qaUser.Email = "qa3@absencesoft.com";
            qaUser.FirstName = "QA3";
            qaUser.LastName = "User";
            qaUser.CreatedById = "000000000000000000000000";
            qaUser.ModifiedById = "000000000000000000000000";
            qaUser.Password = new CryptoString() { PlainText = "!Complex001" };
            qaUser.Roles = new List<string>() { Role.SystemAdministrator.Id };
            qaUser.Customer = testCustomer;
            SetEmployer(qaUser, testEmployer.Id);
            qaUser.Save();
            Console.WriteLine("Saved user 'QA (qa@absencesoft.com)' with Id '{0}'", "000000000000000000000000");

            //(DON'T REUSE IDs)
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000001", EmployeeNumber = "300000001", LastName = "TestStateMin", FirstName = "IL15Min01", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000002", EmployeeNumber = "300000002", LastName = "TestStateMin", FirstName = "IL15Min02", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000003", EmployeeNumber = "300000003", LastName = "TestStateMin", FirstName = "IL15Min03", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000004", EmployeeNumber = "300000004", LastName = "TestStateMin", FirstName = "IL15Min04", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000005", EmployeeNumber = "300000005", LastName = "TestStateMin", FirstName = "IL15Min05", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000006", EmployeeNumber = "300000006", LastName = "TestStateMin", FirstName = "IL15Min06", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000007", EmployeeNumber = "300000007", LastName = "TestStateMin", FirstName = "IL15Min07", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000008", EmployeeNumber = "300000008", LastName = "TestStateMin", FirstName = "IL15Min08", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000009", EmployeeNumber = "300000009", LastName = "TestStateMin", FirstName = "IL15Min09", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000010", EmployeeNumber = "300000010", LastName = "TestStateMin", FirstName = "IL15Min10", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000011", EmployeeNumber = "300000011", LastName = "TestStateMin", FirstName = "IL15Min11", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000012", EmployeeNumber = "300000012", LastName = "TestStateMin", FirstName = "IL15Min12", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'M', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });
            CoreData.UpsertEmployee(new Employee() { EmployerId = testEmployer.Id, CustomerId = testCustomer.Id, Id = "000000000000000300000013", EmployeeNumber = "300000013", LastName = "TestStateMin", FirstName = "IL15Min13", WorkState = "IL", ServiceDate = DateTime.Parse("2009-02-02"), HireDate = DateTime.Parse("2009-02-02"), RehireDate = null, PriorHours = new List<PriorHours>(1) { new PriorHours() { AsOf = DateTime.UtcNow.ToMidnight(), HoursWorked = 2080 } }, Status = (EmploymentStatus)'A', IsExempt = false, Gender = (Gender)'F', IsKeyEmployee = false, Meets50In75MileRule = true, SpouseEmployeeNumber = null, WorkSchedules = new List<Schedule>() { new Schedule() { ScheduleType = ScheduleType.Weekly, StartDate = DateTime.Parse("2009-02-02"), Times = GetTimes(DateTime.Parse("2009-02-02"), 40) } }, CreatedById = "000000000000000000000000", ModifiedById = "000000000000000000000000" });

            //Test Cases (DON'T REUSE IDs)
            //var myCase = new CaseService().Using(s => s.UpdateCase(new EligibilityService().Using(e => e.RunEligibility(new CaseService().Using(c => c.CreateCase("000000000000000000000001", DateTime.Today.AddDays(20), DateTime.Today.AddDays(40), CaseType.Consecutive, AbsenceReason.GetByCode("EHC").Id, reducedSchedule: null, description: "Chad's example 1 leave of absence")))).Case));
        }

        static List<Data.Time> GetTimes(DateTime startDate, int hoursPerWeek)
        {
            DateTime scheduleStart = startDate.GetFirstDayOfWeek();
            int remainderMinutes = (hoursPerWeek * 60) % 5;
            int minutesPerDay = (int)Math.Floor((double)((hoursPerWeek * 60) / 5));

            List<Data.Time> times = new List<Data.Time>()
            {
                new Data.Time() { SampleDate = scheduleStart.AddDays(0), TotalMinutes = 0 },
                new Data.Time() { SampleDate = scheduleStart.AddDays(1), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(2), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(3), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(4), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(5), TotalMinutes = minutesPerDay + remainderMinutes },
                new Data.Time() { SampleDate = scheduleStart.AddDays(6), TotalMinutes = 0 },
            };

            return times;
        }

        static void SetEmployer(User user, string employerId)
        {
            if (user.Employers == null)
                user.Employers = new List<EmployerAccess>();
            if (!user.Employers.Any(e => e.EmployerId == employerId))
                user.Employers.Add(new EmployerAccess()
                {
                    EmployerId = employerId,
                    AutoAssignCases = true
                });
            if (user.Employers.Count(e => e.EmployerId == employerId) > 1)
            {
                var first = user.Employers.First(e => e.EmployerId == employerId);
                user.Employers.RemoveAll(e => e.EmployerId == employerId && e.Id != first.Id);
            }
        }
    }
}
