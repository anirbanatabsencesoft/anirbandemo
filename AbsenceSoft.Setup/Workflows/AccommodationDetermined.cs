﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        /// <summary>
        /// 1. If approved
        ///     a. Create a Communication ToDo, ACCOM-APPROVED, due in 1 hour, title: “Send Designation Notice Accommodation Approved”
        ///     b. Create an Email Communication ToDo, ACCOM-APPROVED-HR, title: “Send Accommodation Approval HR Communication”
        ///     c. Create an Email Communication ToDo, ACCOM-APPROVED-SUP, due in 1 hour, title: “Send Accommodation Approval Manager Communication”
        ///     d. If accommodation type = “EQUIPMENTORSOFTWARE”, 
        ///         i. then create Equipment Software Ordered ToDo Item, due in 1 hour, title: “Accommodation Equipment / Software Ordered”
        ///     e. If accommodation type = “SCHEDULECHANGE”, 
        ///         i. then create HRIS Schedule Change ToDo item, due in 1 hour, title: “Accommodation HRIS Schedule Change”
        ///     f. If accommodation type = “OTHER”, 
        ///         i. then create Other Accommodation ToDo item, due in 1 hour, title: “Other Accommodation Complete”
        /// 2. ELSE, if denied
        ///     a. Create a Mail Communication ToDo item, ACCOM-DENIED, due in 1 hour, title: “Send Designation Notice Accommodation Denied”
        /// 3. Create an Email Communication ToDo item, MGRNOTICE, title: “Manager Communication”
        /// </summary>
        [WorkflowDataMethod]
        private static void AccommodationDetermined()
        {
            Workflow wf = StageWorkflow("AccommodationDetermined", "Accommodation Determined", "Workflow that runs when an accommodation is approved or denied",
                EventType.AccommodationAdjudicated, new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Not Pending",
                        Description = "Checks that the requested accommodation is not pending",
                        LeftExpression = "IsDetermination(AdjudicationStatus.Pending)",
                        Operator = "==",
                        RightExpression = "false"
                    }
                });
            //
            // Is Approved?
            var approved = SetActivity(wf, BuildActivity(new Guid("010398FE-1B1D-4607-BBA6-5158F741ECF9"), "Is Approved?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Approved",
                Description = "Determines whether the accommodation is approved",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Approved",
                        Description = "Checks that the requested accommodation is approved",
                        LeftExpression = "IsDetermination(AdjudicationStatus.Approved)",
                        Operator = "==",
                        RightExpression = "true"
                    }
                }
            }));
            //
            // Send Denied Letter
            var deniedLetter = SetActivity(wf, BuildActivity(new Guid("FEFE252D-A092-43DB-B418-1902F438E0C1"), "Send Denied Letter", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Designation Notice Accommodation Denied")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-DENIED")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //
            // Send Manager Communication
            var managerCommunication = SetActivity(wf, BuildActivity(new Guid("32239AE4-FEA9-4F4B-A66D-A76C73CB8297"), "Send Manager Communication", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Manager Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "MGRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //
            // Send Accommodation Approved
            var accommApproved = SetActivity(wf, BuildActivity(new Guid("A534093C-E3E7-4C0D-BA18-520CAD85D250"), "Send Accommodation Approved", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Designation Notice Accommodation Approved")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-APPROVED")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //
            // Send Accommodation Approved HR
            var accommApprovedHR = SetActivity(wf, BuildActivity(new Guid("2D448447-DB24-4A15-8D77-8E4EAADD8BAA"), "Send Accommodation Approved HR", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Accommodation Approval HR Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-APPROVED-HR")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "HR" })
                .SetRawValue("Unique", true)));
            //
            // Send Accommodation Approved Supervisor
            var accommApprovedSup = SetActivity(wf, BuildActivity(new Guid("A2B3BEB2-7C45-4C99-8378-7E624432F46F"), "Send Accommodation Approved Manager", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Accommodation Approval Manager Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-APPROVED-SUP")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "SUPERVISOR" })
                .SetRawValue("Unique", true)));
            //
            // Is Equipment or Software
            var isEquipmentOrSoftware = SetActivity(wf, BuildActivity(new Guid("639B9122-4040-48D4-9B69-A46FE935BE61"), "Is Equipment or Software", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Equipment or Software",
                Description = "Determines whether this Accommodation requested is the type of Equipment or Software",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Equipment or Software",
                        Description = "Ensures this Accommodation is a Equipment or Software type",
                        LeftExpression = "EventTarget.Type.Code",
                        Operator = "==",
                        RightExpression = "'EOS'"
                    }
                }
            }));
            // 
            // Equipment / Software Ordered
            var equipmentOrSoftware = SetActivity(wf, BuildActivity(new Guid("639B9122-4040-48D4-9B69-A46FE935BE61"), "Equipment or Software Ordered", new EquipmentSoftwareOrderedActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Accommodation Equipment / Software Ordered")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Is Schedule Change
            var isScheduleChange = SetActivity(wf, BuildActivity(new Guid("E376E2AB-2506-430C-985F-1AEA2F9C01A9"), "Is Schedule Change", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Schedule Change",
                Description = "Determines whether this Accommodation requested is the type of Schedule Change",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Schedule Change",
                        Description = "Ensures this Accommodation is a Schedule Change type",
                        LeftExpression = "EventTarget.Type.Code",
                        Operator = "==",
                        RightExpression = "'SC'"
                    }
                }
            }));
            // 
            // HRIS Schedule Change
            var scheduleChange = SetActivity(wf, BuildActivity(new Guid("2D373247-ED0C-4B3B-9732-2745E0CE18B4"), "HRIS Schedule Change", new HRISScheduleChangeActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Accommodation HRIS Schedule Change")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Is Other
            var isOther = SetActivity(wf, BuildActivity(new Guid("9EE6D1A2-2CA4-4456-8322-D62994091810"), "Is Other Accommodation", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Other",
                Description = "Determines whether this Accommodation requested is the type of Other",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Other",
                        Description = "Ensures this Accommodation is a Other type",
                        LeftExpression = "EventTarget.Type.Code",
                        Operator = "==",
                        RightExpression = "'OTR'"
                    }
                }
            }));
            // 
            // Other Accommodation Complete
            var other = SetActivity(wf, BuildActivity(new Guid("65E0B715-8C7D-4904-A17E-517884EC072E"), "Other Accommodation Complete", new OtherAccommodationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Other Accommodation Complete")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            //
            // End of Temporary Accommodation
            var tempAccomm = SetActivity(wf, BuildActivity(new Guid("24068A93-4091-4E20-8A57-1C5F83564B9C"), "End of Temporary Accommodation", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", -5)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDateMeta", CaseEventType.CaseEndDate)
                .SetRawValue("TargetDueDate", CaseEventType.CaseEndDate)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Notice of End of Temporary Accommodation to Employee")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-ENDTEMP")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            //
            // Close Temporary Case
            var closeTempCase = SetActivity(wf, BuildActivity(new Guid("D3E3BB88-9506-439D-9A4F-7532D6516631"), "Close Temporary Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("TargetDueDateMeta", "AccommodationEndDate")
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "End of Temporary Accommodation for Employee, Close Case")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)
                .SetRawValue("CloseAutomatically", false)));
            
            //
            // Transitions
            // If Denied
            SetTransition(wf, BuildTransition(new Guid("9A1CA270-3F91-4F3A-9A59-EA6E7A4F00B5"), approved, managerCommunication, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("C421999C-2542-44D6-8E68-4E967448506F"), approved, deniedLetter, ConditionActivity.NoOutcomeValue));
            // Approved
            SetTransition(wf, BuildTransition(new Guid("97735EA7-8372-4B94-ADCE-EFA0B052C071"), approved, managerCommunication, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("1225E82A-1DEA-4518-9924-85ADDB81BBE1"), approved, accommApproved, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("52B820C8-910C-451D-9443-53585CB8A140"), approved, accommApprovedHR, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("C5476B93-88BC-4020-B581-5DC70ED3CCA2"), approved, accommApprovedSup, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("943063D7-BABC-4D42-8928-569D40936368"), approved, tempAccomm, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A582FEE1-C53D-47AA-B36F-E36C232057D6"), approved, closeTempCase, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("F8E94B15-081A-420A-844E-CA9EC2D66109"), approved, isEquipmentOrSoftware, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("F3F45C51-A90E-4C5A-9228-616E38EDE0FA"), isEquipmentOrSoftware, equipmentOrSoftware, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("58AD1358-9D1B-4743-BD9F-9522D1B2453A"), approved, isScheduleChange, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("D9FF75D0-4A56-4C33-BE34-927245024D7E"), isScheduleChange, scheduleChange, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4A4D0EFB-E55F-4EB0-999A-CE21042CDF6D"), approved, isOther, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4AE53834-CF06-49CE-AC46-19B24D69A4D1"), isOther, other, ConditionActivity.YesOutcomeValue));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            //
            // Save
            wf.Save();
        }
    }
}
