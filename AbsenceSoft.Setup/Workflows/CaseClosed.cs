﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseClosed()
        {
            //
            // CaseClosed Workflow
            Workflow wf = StageWorkflow("CaseClosed", "Case Closed", "Workflow that runs when a case is closed.", EventType.CaseClosed, new List<Rule>(1)
            {
                new Rule()
                {
                    Name = "Case Is Closed",
                    Description = "Determines if the case is actually closed",
                    LeftExpression = "Case.Status",
                    Operator = "==",
                    RightExpression = "CaseStatus.Closed"
                }
            });
            //
            // Cancel Active ToDos
            var cancelToDos = SetActivity(wf, BuildActivity(new Guid("C309A3E7-5995-47DD-B055-78D8D2EE0CCB"), "Cancel Open ToDos", new ClearToDosActivity().Id, m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ResultText", "CASE CLOSED")));
            //
            // Send Case Closure Notice
            var closureNotice = SetActivity(wf, BuildActivity(new Guid("D260D5B3-790F-4CE7-8B34-590A25030FD1"), "Send Case Closure", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Case Closure Notice")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "CASECLOSE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            // Case Close Todo
            var closeCaseToDo = SetActivity(wf, BuildActivity(new Guid("D419B4F8-5995-47DD-B055-78D8D2EE0CCB"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("CloseAutomatically", true)));
            

            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("AC8C6468-26FF-406D-8A79-C9F3BDE24747"), cancelToDos, closureNotice));
            SetTransition(wf, BuildTransition(new Guid("BC9D7579-26FF-406D-8A79-C9F3BDE24747"), closureNotice, closeCaseToDo));
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
