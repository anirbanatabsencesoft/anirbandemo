﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Workflows.jsPlumb;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        public static void ConfigureCoreWorkflows()
        {
            // Get all methods that need to build workflow data, w/ the [WorkflowDataMethod] attribute
            var methods = typeof(WorkflowData).GetMethods(BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod | BindingFlags.DeclaredOnly)
                .Where(m => m.GetCustomAttribute<WorkflowDataMethodAttribute>() != null)
                .Where(m => m.IsStatic && m.IsPrivate)
                .Where(m => !m.GetParameters().Any())
                .ToList();
            // Invoke them all
            methods.ForEach(m => m.Invoke(null, null));

            DeleteWorkflowByCode("CaseCreatedStd");
            DeleteWorkflowByCode("CaseCreatedAccommodation");
        }

        #region Internals

        /// <summary>
        /// Stages the workflow.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="criteria">The criteria.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        internal static Workflow StageWorkflow(
            string code,
            string name,
            string description,
            EventType eventType,
            List<Rule> criteria = null,
            string customerId = null,
            string employerId = null)
        {
            Console.WriteLine("Configuring Workflow '{0} - {1}'", name, code);
            Workflow wf = Workflow.AsQueryable().Where(w => w.Code.ToUpper() == code.ToUpper() && w.CustomerId == customerId && w.EmployerId == employerId).FirstOrDefault() ?? new Workflow()
            {
                Code = code,
                CustomerId = customerId,
                EmployerId = employerId
            };
            wf.Name = name;
            wf.Description = description;
            wf.TargetEventType = eventType;
            wf.Criteria = criteria ?? wf.Criteria ?? new List<Rule>(0);
            if (criteria != null)
                foreach (var c in criteria)
                    wf.Criteria.AddIfNotExists(c, r => r.Name == c.Name || r.Id == c.Id);

            wf.Saved += new Action<object, EntityEventArgs<Workflow>>((sender, args) =>
                Console.WriteLine("Done Configuring Workflow '{0} - {1}' with Id '{2}'", args.Entity.Code, args.Entity.Name, args.Entity.Id)
            );

            return wf;
        }

        /// <summary>
        /// Builds the activity.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="activityId">The activity identifier.</param>
        /// <param name="conditions">The conditions.</param>
        /// <returns></returns>
        internal static WorkflowActivity BuildActivity(Guid workflowActivityId, string name, string activityId, RuleGroup conditions = null)
        {
            WorkflowActivity activity = new WorkflowActivity();
            activity.Id = workflowActivityId;
            activity.Name = name;
            activity.ActivityId = activityId;
            activity.Rules = conditions;
            return activity;
        }

        /// <summary>
        /// Builds the Workflow activity based on the activity class
        /// </summary>
        /// <param name="a"></param>
        /// <param name="conditions"></param>
        /// <returns></returns>
        internal static WorkflowActivity BuildActivity(Guid workflowActivityId, Activity a, RuleGroup conditions = null)
        {
            return BuildActivity(workflowActivityId, a.Name, a.Id, conditions);
        }

        /// <summary>
        /// Builds the Workflow activity based on the activity class
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="metaBuilder">The meta builder.</param>
        /// <returns></returns>
        internal static WorkflowActivity BuildActivity(Guid workflowActivityId, Activity a, Action<BsonDocument> metaBuilder)
        {
            return BuildActivity(workflowActivityId, a.Name, a.Id, metaBuilder);
        }

        /// <summary>
        /// Builds the activity.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="activityId">The activity identifier.</param>
        /// <param name="metaBuilder">The meta builder.</param>
        /// <returns></returns>
        internal static WorkflowActivity BuildActivity(Guid workflowActivityId, string name, string activityId, Action<BsonDocument> metaBuilder)
        {
            var activity = BuildActivity(workflowActivityId, name, activityId, conditions: null);
            if (metaBuilder != null)
                metaBuilder(activity.Metadata);
            return activity;
        }

        /// <summary>
        /// Sets the activity.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="activity">The activity.</param>
        /// <returns></returns>
        internal static WorkflowActivity SetActivity(Workflow wf, WorkflowActivity activity)
        {
            if (wf.Activities == null)
                wf.Activities = new List<WorkflowActivity>(1);

            WorkflowActivity target = wf.Activities.FirstOrDefault(a => a.Id == activity.Id);

            if (target == null)
                return wf.Activities.AddFluid(activity);

            target.ActivityId = activity.ActivityId;
            target.Name = activity.Name;
            target.Rules = activity.Rules;
            target.Metadata = (target.Metadata ?? new BsonDocument()).Merge((activity.Metadata ?? new BsonDocument()), true);

            return target;
        }
        /// <summary>
        /// Remove the activity.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="workflowActivityId">The ActivityId.</param>
        /// <returns></returns>
        internal static void RemoveActivity(Workflow wf, Guid workflowActivityId)
        {           
            WorkflowActivity target = wf.Activities.FirstOrDefault(a => a.Id == workflowActivityId);
            if (target != null)
            {
                wf.Activities.Remove(target);
            }
            
        }
        /// <summary>
        /// Remove the transition.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="workflowTransitionId">The transtionId.</param>
        /// <returns></returns>
        internal static void RemoveTransition(Workflow wf, Guid workflowTransitionId)
        {
            Transition target = wf.Transitions.FirstOrDefault(t => t.Id == workflowTransitionId);
            if (target != null)
            {
                wf.Transitions.Remove(target);
            }
        }

        /// <summary>
        /// Builds the transition.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="outcome">The outcome.</param>
        /// <returns></returns>
        internal static Transition BuildTransition(Guid transitionId, WorkflowActivity source, WorkflowActivity target, string outcome = Activity.CompleteOutcomeValue)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            return new Transition()
            {
                Id = transitionId,
                SourceActivityId = source == null ? new Guid?() : source.Id,
                TargetActivityId = target.Id,
                ForOutcome = string.IsNullOrEmpty(outcome) ? Activity.CompleteOutcomeValue : (outcome.Equals(ConditionActivity.NAOutcomeValue) ? "" : outcome)
            };
        }

        /// <summary>
        /// Sets the transition.
        /// </summary>
        /// <param name="wf">The wf.</param>
        /// <param name="transition">The transition.</param>
        /// <returns></returns>
        internal static Transition SetTransition(Workflow wf, Transition transition)
        {
            if (wf.Transitions == null)
                wf.Transitions = new List<Transition>(1);

            Transition target = wf.Transitions.FirstOrDefault(t => t.Id == transition.Id);

            if (target == null)
                return wf.Transitions.AddFluid(transition);

            target.SourceActivityId = transition.SourceActivityId;
            target.TargetActivityId = transition.TargetActivityId;
            target.ForOutcome = transition.ForOutcome;
            target.Metadata = (target.Metadata ?? new BsonDocument()).Merge(transition.Metadata ?? new BsonDocument());

            return target;
        }

        internal static void DeleteWorkflowByCode(string code)
        {
            Workflow wf = Workflow.GetByCode(code, null, null);
            if (wf != null)
                wf.Delete();
        }



        internal static Rule BuildToDoItemRule(ToDoItemType type)
        {
            return new Rule()
            {
                Name = string.Format("ToDo Item type is {0}", type.ToString().SplitCamelCaseString()),
                Description = string.Format("Checks that the ToDo Item type is {0}", type.ToString().SplitCamelCaseString()),
                LeftExpression = "EventTarget.ItemType",
                Operator = "==",
                RightExpression = string.Format("{0}.{1}", type.GetType().Name, Enum.GetName(type.GetType(), type))
            };
        }

        internal static Rule BuildSelfServiceRule()
        {
            return new Rule()
            {
                Name = "Is Employee Self Service",
                Description = "Verifies that the event occurred in self service",
                LeftExpression = "Event.IsSelfService",
                Operator = "==",
                RightExpression = "true"
            };
        }

        internal static Rule BuildAccommodationRule(string accommodationCode)
        {
            string name = AccommodationType.GetByCode(accommodationCode).Name;
            return new Rule()
            {
                Name = string.Format("Is {0}", name),
                Description = string.Format("Checks that the requested accommodation is for {0}", name),
                LeftExpression = "EventTarget.Type.Code",
                Operator = "==",
                RightExpression = string.Format("'{0}'", accommodationCode)
            };
        }

        internal static string BuildWorkflowDesignerData(this Workflow wf)
        {
            int top = 50;
            int left = 250;

            jsPlumbContainer container = new jsPlumbContainer();
            foreach (var transition in wf.Transitions)
            {
                container.Edges.Add(new Edge(transition));
            }

            using (var workflowService = new WorkflowService())
            {
                foreach (var activity in wf.Activities)
                {
                    Activity a = workflowService.GetActivity(activity.ActivityId);
                    container.Nodes.Add(new Node(activity, a.Template, top, left));

                    top += 200;
                }
            }


            return container.ToJSON();
        }

        /// <summary>
        /// Build the Rule for check the Case is not eligible.
        /// </summary>
        /// <returns></returns>
        internal static Rule BuildCaseCreatedIneligibleRule()
        {
            return new Rule()
            {
                Name = "Is Eligible?",
                Description = "Checks whether any policy on any segment of the case is not eligible",
                LeftExpression = "IsEligible()",
                Operator = "==",
                RightExpression = "false",
            };
        }
        /// <summary>
        /// Determines if this is an accommodation only case"
        /// </summary>
        /// <returns></returns>
        internal static Rule BuildIsAccommodationRule(string rightExpression = "false")
        {
            return new Rule()
            {
                Name = "Is Accommodation Only",
                Description = "Determines if this is an accommodation only case",
                LeftExpression = "Case.IsAccommodation",
                Operator = "==",
                RightExpression = rightExpression,
            };
        }
        #endregion


        [AttributeUsage(AttributeTargets.Method)]
        private class WorkflowDataMethodAttribute : Attribute { }
    }
}
