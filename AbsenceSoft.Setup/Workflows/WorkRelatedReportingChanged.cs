﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void WorkRelatedReportingChanged()
        {
            //
            // Work Related Reporting Changed Workflow
            Workflow wf = StageWorkflow("WorkRelatedReportingChanged", "Work Related Reporting Changed", "Workflow that runs when a case's work related illness/injury is added or changed.", EventType.WorkRelatedReportingChanged, new List<Rule>(1)
            {
                new Rule()
                {
                    Name = "Is Work Related",
                    Description = "Determines whether or not the case is work related",
                    LeftExpression = "IsWorkRelated",
                    Operator = "==",
                    RightExpression = "true"
                },
                new Rule()
                {
                    Name = "Work Related Reporting Feature Enabled",
                    Description = "Checks that the employer has the work related reporting feature enabled",
                    LeftExpression = "Employer.HasFeature(Feature.WorkRelatedReporting)",
                    Operator = "==",
                    RightExpression = "true"
                }
            });

            //
            // Case Review - Send OSHA Form 301
            var caseReview = SetActivity(wf, BuildActivity(new Guid("97995A49-1393-498F-8871-FBA9D860D931"), new CaseReviewActivity(), m => m
                .SetRawValue("Time", 7)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDateMeta", "IllnessOrInjuryDate")
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Run and Send OSHA Form 301")
                .SetRawValue("Priority", ToDoItemPriority.High)
                .SetRawValue("Unique", true)));

            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
