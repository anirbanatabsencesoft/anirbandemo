﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void PaperworkSent()
        {
            //
            // Paperwork Sent
            Workflow wf = StageWorkflow("PaperworkSent", "Paperwork Sent", "Workflow that runs when a communication includes paperwork and is sent, for each paperwork sent out.",
                EventType.PaperworkSent);
            //
            // Has Due Date
            var hasDueDate = SetActivity(wf, BuildActivity(new Guid("E953E206-CFD9-4F84-A934-892B2FE1DF33"), "Has Due Date?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "No Due Date",
                Description = "Determines if this paperwork has no due date",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "No Due Date",
                        Description = "Determines if this paperwork has no due date; that the due date is null",
                        LeftExpression = string.Format("MetadataValueIs('PaperworkDueDate','{0}',null)", typeof(DateTime?).AssemblyQualifiedName.Replace("'", "\\'")),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Paperwork Due
            var paperworkDue = SetActivity(wf, BuildActivity(new Guid("BC67A7C3-3424-4D85-B066-C2910FBE0EF3"), "Paperwork Due", new PaperworkDueActivity().Id, m => m
                .SetRawValue("Time", 15)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDateMeta", "PaperworkDueDate")
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Paperwork '{{WorkflowInstance.DynamicState.PaperworkName}}' Due")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Requires Review?
            var requiresReview = SetActivity(wf, BuildActivity(new Guid("3B0F1374-58B2-4175-9949-890182463128"), "Requires Review?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Requires Review",
                Description = "Determines if this paperwork requires a paperwork review",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Requires Review",
                        Description = "Determines if this paperwork requires a review",
                        LeftExpression = string.Format("MetadataValueIs('RequiresReview','{0}',(System.Object)true)", typeof(bool).AssemblyQualifiedName),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Paperwork Review
            var paperworkReview = SetActivity(wf, BuildActivity(new Guid("B8D8C335-6C21-43E7-BE97-27FB362C2B85"), "Paperwork Review", new PaperworkReviewActivity().Id, m => m
                .SetRawValue("Time", 2)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Review Paperwork: {{WorkflowInstance.DynamicState.PaperworkName}}")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Is For Approval Decision
            var isForApprovalDecision = SetActivity(wf, BuildActivity(new Guid("B8D8C335-6C21-43E7-BE97-27FB362C2B98"), "Is For Approval Decision?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is For Approval Decision",
                Description = "Determines if this paperwork requires an approval Decision",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is For Approval Decision",
                        Description = "Determines if this paperwork requires an approval Decision",
                        LeftExpression = string.Format("MetadataValueIs('IsForApprovalDecision','{0}',(System.Object)true)", typeof(bool).AssemblyQualifiedName),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //

            // Close Case?
            var ifCloseCase = SetActivity(wf, BuildActivity(new Guid("62FCDB59-4FC0-4445-90B8-A25EB23E5D28"), "Close Case?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Close Case",
                Description = "Determines if the user input directed the case should be closed",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Close Case",
                        Description = "Determines if the user input directed the case should be closed",
                        LeftExpression = string.Format("MetadataValueIs('CloseCase','{0}',(System.Object)true)", typeof(bool).AssemblyQualifiedName),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Close Case
            var closeCase = SetActivity(wf, BuildActivity(new Guid("0DFAC0E6-6E0C-499E-B812-F8EB67CBB77A"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Close Case, Paperwork '{{WorkflowInstance.DynamicState.PaperworkName}}' Not Received")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)
                .SetRawValue("CloseAutomatically", false)));
            //
            // Incomplete Cert
            var incompleteCert = SetActivity(wf, BuildActivity(new Guid("1EF188FF-05D8-4AE4-8C97-BC3C7394C7DC"), "Incomplete Cert", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Incomplete Certification Notice")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "INCOMPCERT")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            var isAccomm = SetActivity(wf, BuildActivity(new Guid("20117E61-860E-4A61-8EDA-555980E4AA48"), "Is Accommodation Only?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Accommodation Only",
                Description = "Determines if this is an accommodation only case",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Accommodation Only",
                        Description = "Determines if this is an accommodation only case",
                        LeftExpression = "Case.IsAccommodation",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            var sendAccommNotReceivedLetter = SetActivity(wf, BuildActivity(new Guid("0FA912E3-116A-45D1-9898-F253B8350384"), new SendCommunicationActivity(), m => m
                .SetRawValue("Time", 7)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Accommodation Certification Not Received Letter to Employee")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOM-NOTRECEIVED")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            var sendDocNotReceivedLetter = SetActivity(wf, BuildActivity(new Guid("59DF9B64-C71E-4388-929B-0C592813E366"), new SendCommunicationActivity(), m => m
                .SetRawValue("Time", 7)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Documentation Not Received Letter to Employee")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "FAILCERT")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            //
            // Approved
            var approved = SetActivity(wf, BuildActivity(new Guid("AF9197E0-22B4-4AB1-A3B8-229505B215EF"), "Approved", new CaseReviewActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Paperwork Approved, Make Approval Decision")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Denied
            var denied = SetActivity(wf, BuildActivity(new Guid("739F100A-02FF-4ED3-8AB0-B3D2635BC4F2"), "Denied", new CaseReviewActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Paperwork Denied, Make Approval Decision")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("3FA2F202-068F-4C8C-A9E4-60E7B4FD170E"), hasDueDate, paperworkDue, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("34054DB2-2D8E-4664-8BA1-6FBFEE683D2D"), paperworkDue, requiresReview, PaperworkDueActivity.PaperworkReceivedOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("5F68039A-6717-452A-9E83-8F1AD2B2F638"), requiresReview, paperworkReview, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A9F9DF17-95D1-4446-9D2F-E1C09C8484F4"), paperworkDue, ifCloseCase, PaperworkDueActivity.PaperworkNotReceivedOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("98A50722-7255-4759-BF00-9DB446F06917"), ifCloseCase, isAccomm, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("F5B6E502-B234-4A72-A1E4-02CF4C736C99"), isAccomm, sendAccommNotReceivedLetter, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A60ACA66-4728-4E51-A0E8-3A420E01EFDB"), isAccomm, sendDocNotReceivedLetter, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4280B7F8-46B8-4CF0-8DC7-0AAD48F2254F"), sendAccommNotReceivedLetter, paperworkDue, SendCommunicationActivity.CompleteOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4D6708D3-9368-4F26-9917-75E29FF1FEDD"), sendDocNotReceivedLetter, paperworkDue, SendCommunicationActivity.CompleteOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("EDE126FB-4D81-4960-9053-509D7C5FFC8F"), ifCloseCase, closeCase, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("04AB6159-63A8-4A5B-910E-D2020E124439"), paperworkReview, incompleteCert, PaperworkReviewActivity.IncompleteOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("5F68039A-6717-452A-9E83-8F1AD2B2F567"), paperworkReview, isForApprovalDecision, PaperworkReviewActivity.ApprovedOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("5F68039A-6717-452A-9E83-8F1AD2B2F345"), isForApprovalDecision, approved, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("5D2FA0EA-C07A-4951-9B2A-5F255DD35D1B"), paperworkReview, denied, PaperworkReviewActivity.DeniedOutcomeValue));
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
