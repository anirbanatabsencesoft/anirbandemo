﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseCanceled()
        {
            //
            // CaseCancelled Workflow
            Workflow wf = StageWorkflow("CaseCancelled", "Case Cancelled", "Workflow that runs when a case is cancelled.", EventType.CaseCanceled);
            //
            // Cancel Active ToDos
            var cancelToDos = BuildActivity(new Guid("07F49D2A-6EF8-4A17-84D0-8CD0676D5CE5"), "Cancel Open ToDos", new ClearToDosActivity().Id);
            cancelToDos.Metadata
                .SetRawValue("CloseAll", true)
                .SetRawValue("ResultText", "CASE CANCELLED");
            cancelToDos = SetActivity(wf, cancelToDos);
            //
            // Spouse Case Re-Calc
            var reCalc = BuildActivity(new Guid("21306BE0-366E-4C71-B646-B864E5EA3D5A"), "ReCalc Spouse Case", new RunCalcsActivity().Id);
            reCalc.Metadata.SetRawValue("Spouse", true);
            reCalc = SetActivity(wf, reCalc);
            //
            // Spouse Case Note
            var spouseCaseNote = BuildActivity(new Guid("5EC985C6-539D-4CCA-A4C1-13AF232B8B03"), "Spouse Case Note", new CaseNoteActivity().Id);
            spouseCaseNote.Metadata
                .SetRawValue("Template", "Spouse's related case was cancelled, Case # {{Case.CaseNumber}}.\nCase review task created to review possible changes to entitlement of policies on this case.")
                .SetRawValue("Category", NoteCategoryEnum.CaseSummary)
                .SetRawValue("Public", true)
                .SetRawValue("Spouse", true)
                .SetRawValue("Unique", true);
            spouseCaseNote = SetActivity(wf, spouseCaseNote);
            //
            // Spouse Case Review ToDo
            var spouseToDo = BuildActivity(new Guid("8E9715B2-AC08-42ED-97C8-64CBDD1288E3"), "Spouse Case Review", new CaseReviewActivity().Id);
            spouseToDo.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Spouse Case Cancelled, Review Case Changes")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", true)
                .SetRawValue("Unique", true);
            spouseToDo = SetActivity(wf, spouseToDo);
            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("C06D5050-1B1C-4EED-9C32-3F4F51CEA1C0"), cancelToDos, reCalc));
            SetTransition(wf, BuildTransition(new Guid("D00A956E-7891-4721-96CF-71A8C0A081A5"), reCalc, spouseCaseNote));
            SetTransition(wf, BuildTransition(new Guid("D09DFFE6-1DF6-430E-9900-E1CB6468A908"), spouseCaseNote, spouseToDo));
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
