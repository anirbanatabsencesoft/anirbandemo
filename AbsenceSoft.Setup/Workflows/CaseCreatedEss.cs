﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseCreatedEss()
        {
            //
            // Workflow
            Workflow wf = StageWorkflow("CaseCreatedEss", "Self-Service Case Created", "Workflow that runs when a self-service (ESS) case is created",
                EventType.CaseCreated, new List<Rule>(1)
                {
                    BuildSelfServiceRule()
                });
            //
            // Work Schedule Incorrect?
            var workScheduleIncorrect = SetActivity(wf, BuildActivity(new Guid("0CBAB207-A3D5-49D5-9635-ABA713A30B57"), "Work Schedule Incorrect?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Work Schedule Incorrect",
                Description = "Determines if the user has indicated the work schedule is incorrect",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Work Schedule Incorrect",
                        Description = "Determines if the user has indicated the work schedule is incorrect",
                        LeftExpression = "IsMetadataValueTrue('IsWorkScheduleWrong')",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Review Work Schedule
            var scheduleReview = SetActivity(wf, BuildActivity(new Guid("A79AD2C0-4DF2-4AA6-B766-60A93873B2C1"), "Review Work Schedule", new ReviewWorkScheduleActivity().Id, m => m
                .SetRawValue("Title", "Review Work Schedule of '{{Employee.FullName}}'")
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("7AE694C1-6254-40DA-AE05-27B3999627B0"), workScheduleIncorrect, scheduleReview, ConditionActivity.YesOutcomeValue));
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
