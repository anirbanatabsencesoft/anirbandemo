﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseCreated()
        {
            //
            // Workflow
            Workflow wf = StageWorkflow("CaseCreated", "Case Created", "Workflow that runs when a case is created",
                EventType.CaseCreated);


            RemoveTransition(wf, new Guid("1B66A02E-A63C-44C2-8CB6-8E6E882444EB"));
            RemoveTransition(wf, new Guid("170510ED-53C8-4639-B5BB-2D477FEBC365"));

            RemoveActivity(wf, new Guid("6E809FE3-8170-41CB-ADBB-A9C120C321AD"));
            RemoveActivity(wf, new Guid("E8B18508-4197-4646-B9F5-5F4B0F3BB0F5"));


            //
            // Is Accommodation
            var isAccomm = SetActivity(wf, BuildActivity(new Guid("20117E61-860E-4A61-8EDA-555980E4AA48"), "Is Accommodation Only?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Accommodation Only",
                Description = "Determines if this is an accommodation only case",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    BuildIsAccommodationRule("true")
                }
            }));

            // Is USERRA Eligible
            var isUSERRAEligible = SetActivity(wf, BuildActivity(new Guid("F3497F80-419A-43B3-82A1-7B964CA180D6"), "Is USERRA Eligible", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is USERRA Eligible",
                Description = "Determines if any policy on the case is USERRA eligible, if any",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {

                    new Rule()
                    {
                        Name = "Is USERRA Eligible",
                        Description = "Checks whether any policy on any segment of the case is USERRA eligible",
                        LeftExpression = "HasSpecificPolicy('USERRA')",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            // USERRA Eligibility Packet
            var userraPacket = SetActivity(wf, BuildActivity(new Guid("2B15D309-CBC1-49CD-8DD6-60F0DF560302"), "Send USERRA Eligibility Packet", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send USERRA Eligibility Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "USERRANOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //

            //
            // Is Eligible
            var isEligible = SetActivity(wf, BuildActivity(new Guid("0986CFBE-B6AE-414A-A061-EC8BA85EA467"), "Is Eligible", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Eligible",
                Description = "Determines if any policy on the case is eligible, if any",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Eligible",
                        Description = "Checks whether any policy on any segment of the case is eligible",
                        LeftExpression = "IsEligible()",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            //
            // Eligibility Packet
            var eligibilityPacket = SetActivity(wf, BuildActivity(new Guid("08C19189-C0FB-4CD9-B446-6663A7E8F01A"), "Send Eligibility Packet", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Eligibility Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ELGNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //


            //
            // HR Notice
            var hrNotice = SetActivity(wf, BuildActivity(new Guid("B8669D7F-EE40-4D54-865B-E49860B51F95"), "HR Communication", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "HR Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "HRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "HR" })
                .SetRawValue("Unique", true)));
            //
            // Manager Notice
            var mgrNotice = SetActivity(wf, BuildActivity(new Guid("421464CE-C544-4337-A72B-8820839CF330"), "Manager Communication", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Manager Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "MGRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "SUPERVISOR" })
                .SetRawValue("Unique", true)));
            //
            // Is Administrative
            var isAdmin = SetActivity(wf, BuildActivity(new Guid("229C3C10-CD9E-48AA-BC7F-F424B18EA079"), "Is Administrative", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Administrative",
                Description = "Determines if the leave is an administrative leave",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Administrative",
                        Description = "Determines if the leave is an administrative leave",
                        LeftExpression = "Case.IsInformationOnly",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Return to Work ToDo
            var rtwToDo = SetActivity(wf, BuildActivity(new Guid("1FDF587D-658B-49BA-9DF8-5E42D0A2ED61"), "Return to Work", new ReturnToWorkActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Return to Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("HideDays", -7)
                .SetRawValue("TargetHideDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Unique", true)));

            //
            // Condition (HasWorkRestrictions)
            var hasWorkRestrictions = SetActivity(wf, BuildActivity(new Guid("229C3C10-CF9E-48AB-BC7F-F424B18EA079"), "Has Work Restrictions", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Has Work Restrictions",
                Description = "Determines if the employee has work restrictions from the RTW ToDo item response",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Has Work Restrictions",
                        Description = "Determines if the HasWorkRestrictions flag is set on the ToDo item",
                        LeftExpression = string.Format("MetadataValueIs('HasWorkRestrictions','{0}',(System.Object)true)", typeof(bool).AssemblyQualifiedName),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            //
            // Enter Work Restrictions
            var enterWorkRestrictions = SetActivity(wf, BuildActivity(new Guid("856DCF36-7578-434C-9CB3-9B174D123476"), "Enter Work Restrictions", new EnterWorkRestrictionsActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Enter Work Restrictions")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var recertifyTodo = SetActivity(wf, BuildActivity(new Guid("FAE9E0AA-BCA7-479A-AC37-14433D46DC4F"), "Recertify", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Recertification Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "RECERT")
                .SetRawValue("Unique", true)));

            var verifyRTW = SetActivity(wf, BuildActivity(new Guid("856DCF36-7578-434C-9CB3-9B174D070B76"), "Verify Return To Work", new VerifyReturnToWorkActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Verify Return To Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var closeCase = SetActivity(wf, BuildActivity(new Guid("AB531810-48E2-48A4-AB02-A7B18273895D"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Close Case, Employee Will Not Return To Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var returnedToWork = SetActivity(wf, BuildActivity(new Guid("E19A949F-08DC-44FE-BD6B-507C3559D656"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Employee Returned To Work, Close Case")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var didNotReturnToWork = SetActivity(wf, BuildActivity(new Guid("C701A27D-6130-42B8-B365-592CF3D3C245"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Employee Did Not Return To Work, Close Case")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));


            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("E6D369AD-A247-4A7C-9171-C2C6142C3D68"), isAccomm, isEligible, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("931F9804-8029-4A43-812B-4B86857D10E3"), isAccomm, hrNotice, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("2D7758D5-FA8C-4ADE-9669-6DB20267F1F8"), isAccomm, mgrNotice, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("B29A9698-8946-4F39-A8BE-B6E1FDE94379"), isEligible, isUSERRAEligible, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("438620DD-4160-4B48-BF1F-146C8EAC369D"), isUSERRAEligible, userraPacket, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("9808646C-CF01-4D7D-B332-8B5F4E5F04B4"), isUSERRAEligible, eligibilityPacket, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("F6FBA521-4F8E-49C6-BBF4-68660AC065C5"), isEligible, isAdmin, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("E1E378A0-336E-485C-B75B-EF75EDBEE3BF"), isAdmin, rtwToDo, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("63866285-5049-4402-AD75-9BF923D492EE"), rtwToDo, recertifyTodo, ReturnToWorkActivity.RecertifyOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("0E9F7403-C395-4EE7-9875-AF43C98CF5DE"), rtwToDo, verifyRTW, ReturnToWorkActivity.ReturnToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("697B2E5E-2FDF-4CC5-8E95-FB01883BED31"), rtwToDo, closeCase, ReturnToWorkActivity.NotReturningToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("83131240-13FD-41BF-96F8-C3624D701F53"), verifyRTW, returnedToWork, VerifyReturnToWorkActivity.RtwVerifiedYesOutcome));
            SetTransition(wf, BuildTransition(new Guid("A7F3837A-4C02-4486-9AD3-805B36D24B1A"), verifyRTW, didNotReturnToWork, VerifyReturnToWorkActivity.RtwVerifiedNoOutcome));
            SetTransition(wf, BuildTransition(new Guid("E1E378A0-336E-485C-B75B-EF75FABEE3BF"), rtwToDo, hasWorkRestrictions, ReturnToWorkActivity.ReturnToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("63866285-5049-4402-AD75-9BF9FAD492EE"), hasWorkRestrictions, enterWorkRestrictions, ConditionActivity.YesOutcomeValue));


            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
