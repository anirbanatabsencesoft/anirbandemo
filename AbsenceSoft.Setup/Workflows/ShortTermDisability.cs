﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void ShortTermDisability()
        {
            #region Case Created

            Workflow wf = StageWorkflow("ShortTermDisability", "Short Term Disability", "Workflow that runs when a case qualifies for Short Term Disability", 
                EventType.CaseCreated, new List<Rule>(2){
                new Rule()
                {
                    Name = "Is STD",
                    Description = "Checks that the case has a federal or state STD policy",
                    LeftExpression = "EventTarget.IsSTD",
                    Operator = "==",
                    RightExpression = "true"
                },
                new Rule()
                {
                    Name = "STD Feature Enabled",
                    Description = "Checks that the employer has the STD feature enabled",
                    LeftExpression = "Employer.HasFeature(Feature.ShortTermDisability)",
                    Operator = "==",
                    RightExpression = "true"
                }
            });

            //
            // STD Designate Primary HCP
            var stdDesignateHcp = SetActivity(wf, BuildActivity(new Guid("3B4A6403-F40C-4133-BF2F-BB3E1B09EDF6"), "STD Designate Primary HCP", new StdDesignatePrimaryHealthCareProviderActivity().Id, m => m
                .SetRawValue("Time", 2)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("Optional",true)
                .SetRawValue("Title", "STD Designate Primary HCP")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));
            //

            var stdDiagnosis = SetActivity(wf, BuildActivity(new Guid("038616D5-6C3B-4503-A16F-EDB38A9E60E0"), new StdDiagnosisActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "STD Diagnosis")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var stdHealthCondition = SetActivity(wf, BuildActivity(new Guid("4DAE187D-9F86-42B2-BA52-4FE208888A75"), new StdGeneralHealthConditionActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "STD General Health Condition")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var stdFollowUp = SetActivity(wf, BuildActivity(new Guid("411D87AD-79D9-4948-B78B-FB59743BEA86"), new StdFollowUpActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "STD Follow Up")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var stdContactsEE = SetActivity(wf, BuildActivity(new Guid("0AC840DF-96E5-4CF6-91D5-1150D09F29D3"), new StdCaseManagerContactsEmployeeActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "STD Contact Employee")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var stdContactsHCP = SetActivity(wf, BuildActivity(new Guid("9890C940-E2C7-4EF3-91D4-BD6B73BA576C"), new StdContactHealthCareProviderActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "STD Contact HCP")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var caseReview = SetActivity(wf, BuildActivity(new Guid("97995A49-1393-498F-8871-FBA9D870D931"), new CaseReviewActivity(), m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "RTW Prompt")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TargetHideDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("HideDays", -3)
                .SetRawValue("Unique", true)));

            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB37"), stdDiagnosis, stdHealthCondition));
            SetTransition(wf, BuildTransition(new Guid("A555CB6E-1C59-4F6F-B4B8-DE0DE37CBCBD"), stdDiagnosis, stdDesignateHcp));
            SetTransition(wf, BuildTransition(new Guid("23C6BED1-16DD-47C5-9813-6596648D782B"), stdHealthCondition, stdFollowUp));
            SetTransition(wf, BuildTransition(new Guid("7F6674C9-7E7A-404C-A989-CA0144521168"), stdFollowUp, stdContactsEE, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A7F07A46-8008-47A4-B532-CE4103507AB4"), stdFollowUp, stdContactsHCP, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A8C4477E-9106-4F58-A4D3-A42FD3994DAF"), stdFollowUp, caseReview, StdFollowUpActivity.StdLeaveNotCompleteOutcome));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();

            #endregion

            #region Policy Manually Added

            //
            // Need to perform the same steps if an STD policy is manually created
            wf = StageWorkflow("ShortTermDisabilityAdded", "Short Term Disability Added", "Workflow that runs when a case gets Short Term Disability added",
                EventType.PolicyManuallyAdded, new List<Rule>(3){
                new Rule()
                {
                    Name = "Is STD",
                    Description = "Checks that the case has a federal or state STD policy",
                    LeftExpression = "Case.IsSTD",
                    Operator = "==",
                    RightExpression = "true"
                },
                new Rule()
                {
                    Name = "Is STD",
                    Description = "Checks that the policy being added is an STD policy",
                    LeftExpression = "EventTarget.Policy.PolicyType",
                    Operator = "==",
                    RightExpression = "PolicyType.STD"
                },
                new Rule()
                {
                    Name = "STD Feature Enabled",
                    Description = "Checks that the employer has the STD feature enabled",
                    LeftExpression = "Employer.HasFeature(Feature.ShortTermDisability)",
                    Operator = "==",
                    RightExpression = "true"
                }
            });

            stdDesignateHcp = SetActivity(wf, stdDesignateHcp);
            stdDiagnosis = SetActivity(wf, stdDiagnosis);
            stdHealthCondition = SetActivity(wf, stdHealthCondition);
            stdFollowUp = SetActivity(wf, stdFollowUp);
            stdContactsEE = SetActivity(wf, stdContactsEE);
            stdContactsHCP = SetActivity(wf, stdContactsHCP);
            caseReview = SetActivity(wf, caseReview);

            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB31"), stdDiagnosis, stdHealthCondition));
            SetTransition(wf, BuildTransition(new Guid("A555CB6E-1C59-4F6F-B4B8-DE0DE37CBCB1"), stdDiagnosis, stdDesignateHcp));
            SetTransition(wf, BuildTransition(new Guid("23C6BED1-16DD-47C5-9813-6596648D7821"), stdHealthCondition, stdFollowUp));
            SetTransition(wf, BuildTransition(new Guid("7F6674C9-7E7A-404C-A989-CA0144521161"), stdFollowUp, stdContactsEE, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A7F07A46-8008-47A4-B532-CE4103507AB1"), stdFollowUp, stdContactsHCP, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A8C4477E-9106-4F58-A4D3-A42FD3994DA1"), stdFollowUp, caseReview, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();

            #endregion

            #region Policy Approved

            var approved_stdDesignateHcp = stdDesignateHcp.Clone();
            var approved_stdDiagnosis = stdDiagnosis.Clone();
            var approved_stdHealthCondition = stdHealthCondition.Clone();
            var approved_stdFollowUp = stdFollowUp.Clone();
            var approved_stdContactsEE = stdContactsEE.Clone();
            var approved_stdContactsHCP = stdContactsHCP.Clone();
            var approved_caseReview = caseReview.Clone();

            #endregion

            #region Policy Manually Deleted

            //
            // Now, if the STD policy is Removed, remove all STD related todo items.
            wf = StageWorkflow("ShortTermDisabilityRemoved", "Short Term Disability Removed", "Workflow that runs when a case qualifies for Short Term Disability",
                EventType.PolicyManuallyDeleted, new List<Rule>(3){
                new Rule()
                {
                    Name = "Is STD",
                    Description = "Checks that the case is no longer an STD case",
                    LeftExpression = "Case.IsSTD",
                    Operator = "==",
                    RightExpression = "false"
                },
                new Rule()
                {
                    Name = "Is STD",
                    Description = "Checks that the policy being removed is an STD policy",
                    LeftExpression = "EventTarget.Policy.PolicyType",
                    Operator = "==",
                    RightExpression = "PolicyType.STD"
                },
                new Rule()
                {
                    Name = "STD Feature Enabled",
                    Description = "Checks that the employer has the STD feature enabled",
                    LeftExpression = "Employer.HasFeature(Feature.ShortTermDisability)",
                    Operator = "==",
                    RightExpression = "true"
                }
            });

            stdDesignateHcp = SetActivity(wf, BuildActivity(new Guid("3B4A6403-F40C-4133-BF2F-BB3E1B09EDF6"), "STD Designate Primary HCP", new ClearToDosActivity().Id, m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDDesignatePrimaryHCP)));
            stdDiagnosis = SetActivity(wf, BuildActivity(new Guid("038616D5-6C3B-4503-A16F-EDB38A9E60E0"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDDiagnosis)));
            stdHealthCondition = SetActivity(wf, BuildActivity(new Guid("4DAE187D-9F86-42B2-BA52-4FE208888A75"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDGenHealthCond)));
            stdFollowUp = SetActivity(wf, BuildActivity(new Guid("411D87AD-79D9-4948-B78B-FB59743BEA86"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDFollowUp)));
            stdContactsEE = SetActivity(wf, BuildActivity(new Guid("0AC840DF-96E5-4CF6-91D5-1150D09F29D3"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDCaseManagerContactsEE)));
            stdContactsHCP = SetActivity(wf, BuildActivity(new Guid("9890C940-E2C7-4EF3-91D4-BD6B73BA576C"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.STDContactHCP)));
            caseReview = SetActivity(wf, BuildActivity(new Guid("97995A49-1393-498F-8871-FBA9D870D931"), new ClearToDosActivity(), m => m
                .SetRawValue("CloseAll", true)
                .SetRawValue("ToDoItemType", ToDoItemType.CaseReview)
                .SetRawValue("Title", "RTW Prompt")));

            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB32"), stdDiagnosis, stdHealthCondition));
            SetTransition(wf, BuildTransition(new Guid("A555CB6E-1C59-4F6F-B4B8-DE0DE37CBCB2"), stdDiagnosis, stdDesignateHcp));
            SetTransition(wf, BuildTransition(new Guid("23C6BED1-16DD-47C5-9813-6596648D7822"), stdDiagnosis, stdFollowUp));
            SetTransition(wf, BuildTransition(new Guid("7F6674C9-7E7A-404C-A989-CA0144521162"), stdDiagnosis, stdContactsEE));
            SetTransition(wf, BuildTransition(new Guid("A7F07A46-8008-47A4-B532-CE4103507AB2"), stdDiagnosis, stdContactsHCP));
            SetTransition(wf, BuildTransition(new Guid("A8C4477E-9106-4F58-A4D3-A42FD3994DA2"), stdDiagnosis, caseReview));
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();

            #endregion

            #region Policy Adjudicated

            //
            // Now, if the STD policy is Denied and case is no longer STD, remove all STD related todo items.
            wf = StageWorkflow("ShortTermDisabilityDetermined", "Short Term Disability Determined", "Workflow that runs when a case's STD policy is determined",
                EventType.PolicyAdjudicated, new List<Rule>(4){
                new Rule()
                {
                    Name = "Is STD Policy",
                    Description = "Checks that the policy being removed is an STD policy",
                    LeftExpression = "EventTarget.Policy.PolicyType",
                    Operator = "==",
                    RightExpression = "PolicyType.STD"
                },
                new Rule()
                {
                    Name = "STD Feature Enabled",
                    Description = "Checks that the employer has the STD feature enabled",
                    LeftExpression = "Employer.HasFeature(Feature.ShortTermDisability)",
                    Operator = "==",
                    RightExpression = "true"
                }
            });

            var whichCondition = SetActivity(wf, BuildActivity(new Guid("2F0A49C9-E52F-436D-9D81-87FAEF60AB33"), "Is Denied?", new ConditionActivity().Id, new RuleGroup()
            {
                SuccessType = RuleGroupSuccessType.And,
                Name = "STD Denied?",
                Description = "Is STD Denied?",
                Rules = new List<Rule>(2)
                {
                    new Rule()
                    {
                        Name = "Is STD",
                        Description = "Checks that the case is no longer an STD case",
                        LeftExpression = "Case.IsSTD",
                        Operator = "==",
                        RightExpression = "false"
                    },
                    new Rule()
                    {
                        Name = "Is STD Denied",
                        Description = "Checks that the policy being determined is denied",
                        LeftExpression = "IsDetermination(AdjudicationStatus.Denied)",
                        Operator = "==",
                        RightExpression = "true"
                    }
                }
            }));

            stdDesignateHcp = SetActivity(wf, stdDesignateHcp);
            stdDiagnosis = SetActivity(wf, stdDiagnosis);
            stdHealthCondition = SetActivity(wf, stdHealthCondition);
            stdFollowUp = SetActivity(wf, stdFollowUp);
            stdContactsEE = SetActivity(wf, stdContactsEE);
            stdContactsHCP = SetActivity(wf, stdContactsHCP);
            caseReview = SetActivity(wf, caseReview);

            approved_stdDesignateHcp = SetActivity(wf, approved_stdDesignateHcp);
            approved_stdDiagnosis = SetActivity(wf, approved_stdDiagnosis);
            approved_stdHealthCondition = SetActivity(wf, approved_stdHealthCondition);
            approved_stdFollowUp = SetActivity(wf, approved_stdFollowUp);
            approved_stdContactsEE = SetActivity(wf, approved_stdContactsEE);
            approved_stdContactsHCP = SetActivity(wf, approved_stdContactsHCP);
            approved_caseReview = SetActivity(wf, approved_caseReview);

            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52F-436D-9D81-87EAEF60AB33"), whichCondition, stdDiagnosis, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB33"), whichCondition, stdHealthCondition, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A555CB6E-1C59-4F6F-B4B8-DE0DE37CBCB3"), whichCondition, stdDesignateHcp, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("23C6BED1-16DD-47C5-9813-6596648D7823"), whichCondition, stdFollowUp, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("7F6674C9-7E7A-404C-A989-CA0144521163"), whichCondition, stdContactsEE, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A7F07A46-8008-47A4-B532-CE4103507AB3"), whichCondition, stdContactsHCP, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A8C4477E-9106-4F58-A4D3-A42FD3994DA3"), whichCondition, caseReview, ConditionActivity.YesOutcomeValue));

            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB36"), whichCondition, approved_stdDiagnosis, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("2F0A49C9-E52D-436D-9D81-87EAEF60AB36"), whichCondition, approved_stdHealthCondition, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A555CB6E-1C59-4F6F-B4B8-DE0DE37CBCB6"), whichCondition, approved_stdDesignateHcp, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("23C6BED1-16DD-47C5-9813-6596648D7826"), approved_stdHealthCondition, approved_stdFollowUp));
            SetTransition(wf, BuildTransition(new Guid("7F6674C9-7E7A-404C-A989-CA0144521166"), approved_stdFollowUp, approved_stdContactsEE, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A7F07A46-8008-47A4-B532-CE4103507AB6"), approved_stdFollowUp, approved_stdContactsHCP, StdFollowUpActivity.StdLeaveNotCompleteOutcome));
            SetTransition(wf, BuildTransition(new Guid("A8C4477E-9106-4F58-A4D3-A42FD3994DA6"), approved_stdFollowUp, approved_caseReview, StdFollowUpActivity.StdLeaveNotCompleteOutcome));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();

            #endregion
        }
    }
}
