﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void EquipmentOrSoftwareRequested()
        {
            Workflow wf = StageWorkflow("EquipmentOrSoftwareRequested", "Equipment or Software Requested", "Workflow that runs when equipment or software is requested",
                EventType.AccommodationCreated, new List<Rule>(1){
                    BuildAccommodationRule("EOS")
                });

            // order equipment or software
            var orderEquipmentOrSoftware = BuildActivity(new Guid("4D1CB773-BB17-4298-80BB-105E7262EBB0"), "Equipment or Software Ordered", new EquipmentSoftwareOrderedActivity().Id);
            orderEquipmentOrSoftware.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Accommodation Equipment / Software Ordered")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            orderEquipmentOrSoftware = SetActivity(wf, orderEquipmentOrSoftware);

            // install equipment or software
            var installEquipmentOrSoftware = BuildActivity(new Guid("5A3A6A7F-F9D3-4756-84AC-C9A529B759C2"), "Equipment or Software Installed", new EquipmentSoftwareInstalledActivity().Id);
            installEquipmentOrSoftware.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Accommodation Equipment / Software Installed")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            installEquipmentOrSoftware = SetActivity(wf, installEquipmentOrSoftware);

            SetTransition(wf, BuildTransition(new Guid("FF1203CE-D673-4646-ADDF-E5CDFD3B3F14"), orderEquipmentOrSoftware, installEquipmentOrSoftware));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
    
}
