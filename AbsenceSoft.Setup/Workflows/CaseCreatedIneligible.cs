﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseCreatedIneligible()
        {
            //
            // Workflow
            Workflow wf = StageWorkflow("CaseCreatedNotElig", "Case Created - Not Eligible", "Workflow to run when there are no eligible policies",
                EventType.CaseCreated, new List<Rule>(2)
                {
                    BuildCaseCreatedIneligibleRule(),
                    BuildIsAccommodationRule()
                });

            //


            // InEligibility Notice
            var inEligibilityNotice = SetActivity(wf, BuildActivity(new Guid("E8B18508-4197-4646-B9F5-5F4B0F3BB0F5"), "Send Ineligibility Notice", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Ineligibility Notice")
                .SetRawValue("TemplateCode", "ELGNOTELG")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));


            var closeCaseNotEligible = SetActivity(wf, BuildActivity(new Guid("AB531810-48E2-48A4-AB02-A7B18273895D"), "Close Case - Not Eligible", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Close Case - Not Eligible")
                .SetRawValue("Unique", true)));
                
            //
            // Transitions       
            SetTransition(wf, BuildTransition(new Guid("697B2E5E-2FDF-4CC5-8E95-FB01883BED31"), inEligibilityNotice, closeCaseNotEligible, ConditionActivity.CompleteOutcomeValue));

            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
