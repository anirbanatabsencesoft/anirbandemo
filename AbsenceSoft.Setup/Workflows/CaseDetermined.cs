﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseDetermined()
        {
            Workflow wf = StageWorkflow("CaseDetermination", "Case Determination", "Workflow that runs when a case determination is made",
                EventType.CaseAdjudicated);

            var approved = SetActivity(wf, BuildActivity(new Guid("C75E56D9-0700-4F22-85C6-6769C3E439C7"), "Is Approved?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Approved",
                Description = "Determines whether the case is approved",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1){
                    new Rule(){
                        Name = "Is Approved",
                        Description = "Checks that the requested case is approved",
                        LeftExpression = "EventTarget.IsFullyDenied",
                        Operator = "!=",
                        RightExpression = "true"
                    }
                }
            }));            

            // Sent approved letter
            var approvedLetter = SetActivity(wf, BuildActivity(new Guid("0C825403-98C9-43DC-842B-B48E335C7562"), "Send Communication", new SendCommunicationActivity().Id, m => m
                    .SetRawValue("Time", 1)
                    .SetRawValue("Units", ToDoResponseTime.Hours)
                    .SetRawValue("Optional", true)
                    .SetRawValue("Title", "Send Designation Notice Leave Approved")
                    .SetRawValue("Priority", ToDoItemPriority.Normal)
                    .SetRawValue("TemplateCode", "APPROVED")
                    .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                    .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            // Denied letter
            var deniedLetter = SetActivity(wf, BuildActivity(new Guid("142443F2-2B6C-48D1-8CC8-5083FB6D8FCD"), "Send Communication", new SendCommunicationActivity().Id, m => m
                    .SetRawValue("Time", 1)
                    .SetRawValue("Units", ToDoResponseTime.Hours)
                    .SetRawValue("Optional", true)
                    .SetRawValue("Title", "Send Designation Notice Leave Denied")
                    .SetRawValue("Priority", ToDoItemPriority.Normal)
                    .SetRawValue("TemplateCode", "DENIED")
                    .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                    .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

             // Denied Documentation letter
            var deniedDocumentationLetter = SetActivity(wf, BuildActivity(new Guid("FC1FF8EE-B883-4D59-BB0D-F69D0AA59904"), "Send Communication", new SendCommunicationActivity().Id, m => m
                    .SetRawValue("Time", 1)
                    .SetRawValue("Units", ToDoResponseTime.Hours)
                    .SetRawValue("Optional", true)
                    .SetRawValue("Title", "Documentation Not Received Denial")
                    .SetRawValue("Priority", ToDoItemPriority.Normal)
                    .SetRawValue("TemplateCode", "DENIED-DOCUMENTATION")
                    .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                    .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));

            SetTransition(wf, BuildTransition(new Guid("DB194D81-8690-498C-A4A8-C58788CE83E2"), approved, approvedLetter, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4DB9A075-C05B-4232-A0E7-6C5AD900F6B1"), approved, deniedDocumentationLetter, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("AEA6EE6B-93C3-4453-B603-596DA4C6C166"), approved, deniedLetter, ConditionActivity.NoOutcomeValue));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
