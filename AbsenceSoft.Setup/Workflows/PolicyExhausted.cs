﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void PolicyExhausted()
        {
            //
            // CaseClosed Workflow
            Workflow wf = StageWorkflow("PolicyExhausted", "Policy Exhausted", "Workflow that runs when a policy is marked as exhausted or exhaustion changes.", 
                EventType.PolicyExhausted);
            //
            // Send Exhaustion Notice
            var exhaustionNotice = SetActivity(wf, BuildActivity(new Guid("2DCAAAEF-A46F-4BF9-8365-76B1EB05A227"), "Send Notice Leave Exhausted", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDateMeta", "FirstExhaustionDate")
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Eligibility Notice Leave Exhausted")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ELGLEXH")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("HideDays", -7)
                .SetRawValue("TargetHideDateMeta", "FirstExhaustionDate")
                .SetRawValue("Unique", true)));
            
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
