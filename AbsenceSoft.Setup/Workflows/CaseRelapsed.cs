﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseRelapsed()
        {
            //
            // Workflow
            Workflow wf = StageWorkflow("CaseRelapsed", "Case Relapsed", "Workflow that runs when a case is relapsed", EventType.CaseRelapsed);

            //
            // Is Accommodation
            var isAccomm = SetActivity(wf, BuildActivity(new Guid("65ADEEEB-F3BB-4BF6-A1E7-C12259A8F49D"), "Is Accommodation Only?", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Accommodation Only",
                Description = "Determines if this is an accommodation only case",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    BuildIsAccommodationRule("true")
                }
            }));

            // Is USERRA Eligible
            var isUSERRAEligible = SetActivity(wf, BuildActivity(new Guid("823B71CB-418A-485C-872A-E0A0B9F75AB2"), "Is USERRA Eligible", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is USERRA Eligible",
                Description = "Determines if any policy on the case is USERRA eligible, if any",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>()
                {

                    new Rule()
                    {
                        Name = "Is USERRA Eligible",
                        Description = "Checks whether any policy on any segment of the case is USERRA eligible",
                        LeftExpression = "HasSpecificPolicy('USERRA')",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            // USERRA Eligibility Packet
            var userraPacket = SetActivity(wf, BuildActivity(new Guid("3D8A9D41-D054-4602-AD3E-145906269876"), "Send USERRA Eligibility Packet", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send USERRA Eligibility Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "USERRANOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //

            //
            // Is Eligible
            var isEligible = SetActivity(wf, BuildActivity(new Guid("71F355DD-2F4C-4033-98E3-FB75D297736F"), "Is Eligible", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Eligible",
                Description = "Determines if any policy on the case is eligible, if any",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Eligible",
                        Description = "Checks whether any policy on any segment of the case is eligible",
                        LeftExpression = "IsEligible()",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            //
            // Eligibility Packet
            var eligibilityPacket = SetActivity(wf, BuildActivity(new Guid("2F17281E-22BC-4496-A1B4-7D4B3A3B6835"), "Send Eligibility Packet", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Eligibility Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ELGNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true)));
            //


            //
            // HR Notice
            var hrNotice = SetActivity(wf, BuildActivity(new Guid("462415BD-619C-4B2B-859B-2DFAD5CB08C4"), "HR Communication", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "HR Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "HRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "HR" })
                .SetRawValue("Unique", true)));
            //
            // Manager Notice
            var mgrNotice = SetActivity(wf, BuildActivity(new Guid("9AEB256F-0B60-488F-9D9D-3B08AD312A32"), "Manager Communication", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Manager Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "MGRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new[] { "SUPERVISOR" })
                .SetRawValue("Unique", true)));
            //
            // Is Administrative
            var isAdmin = SetActivity(wf, BuildActivity(new Guid("1EFA90FE-21A4-45B6-BF5D-F9AB7F925ABB"), "Is Administrative", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Administrative",
                Description = "Determines if the leave is an administrative leave",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Administrative",
                        Description = "Determines if the leave is an administrative leave",
                        LeftExpression = "Case.IsInformationOnly",
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));
            //
            // Return to Work ToDo
            var rtwToDo = SetActivity(wf, BuildActivity(new Guid("26D6D5F1-486F-44D3-9E1D-F77BAA9AC620"), "Return to Work", new ReturnToWorkActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Return to Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("HideDays", -7)
                .SetRawValue("TargetHideDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Unique", true)));

            //
            // Condition (HasWorkRestrictions)
            var hasWorkRestrictions = SetActivity(wf, BuildActivity(new Guid("6EBC234B-2049-4529-8602-889916DA5D41"), "Has Work Restrictions", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Has Work Restrictions",
                Description = "Determines if the employee has work restrictions from the RTW ToDo item response",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Has Work Restrictions",
                        Description = "Determines if the HasWorkRestrictions flag is set on the ToDo item",
                        LeftExpression = string.Format("MetadataValueIs('HasWorkRestrictions','{0}',(System.Object)true)", typeof(bool).AssemblyQualifiedName),
                        Operator = "==",
                        RightExpression = "true",
                    }
                }
            }));

            //
            // Enter Work Restrictions
            var enterWorkRestrictions = SetActivity(wf, BuildActivity(new Guid("803A2C29-0C6A-4700-A53D-CACD4C63DF57"), "Enter Work Restrictions", new EnterWorkRestrictionsActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Enter Work Restrictions")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var recertifyTodo = SetActivity(wf, BuildActivity(new Guid("FEFCBFFA-4B18-4683-8056-9C376909C8F9"), "Recertify", new SendCommunicationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Recertification Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "RECERT")
                .SetRawValue("Unique", true)));

            var verifyRTW = SetActivity(wf, BuildActivity(new Guid("0E0587E2-8D93-4811-BAF0-19E25F3E23F9"), "Verify Return To Work", new VerifyReturnToWorkActivity().Id, m => m
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDate", CaseEventType.EstimatedReturnToWork)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Verify Return To Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var closeCase = SetActivity(wf, BuildActivity(new Guid("567DBC88-4E6D-4B6C-9AD8-B2B2FA2AB272"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Close Case, Employee Will Not Return To Work")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var returnedToWork = SetActivity(wf, BuildActivity(new Guid("98DBFCC2-429F-49F6-8540-05F8105454DA"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Employee Returned To Work, Close Case")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            var didNotReturnToWork = SetActivity(wf, BuildActivity(new Guid("B72379A4-069D-4189-A15E-C3EFA0C7AD50"), "Close Case", new CloseCaseActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Employee Did Not Return To Work, Close Case")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));


            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("E7895C48-C436-4537-A9FB-103215C0C4D7"), isAccomm, isEligible, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4952C8D9-8E33-42A7-897D-23FC90A3CABF"), isAccomm, hrNotice, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("2A0000DD-8F02-4648-86FE-D124C9425E97"), isAccomm, mgrNotice, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("F9A8D87F-168D-4EB2-A694-C677D392D5E2"), isEligible, isUSERRAEligible, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4302ED7A-FCCD-48CD-9EE0-0A26A9AEC86E"), isUSERRAEligible, userraPacket, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("217C7298-A661-4209-8119-C38D78D3B413"), isUSERRAEligible, eligibilityPacket, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("4FE566DB-AF0F-48D5-BEDD-F8D0F81C7DE5"), isEligible, isAdmin, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("B2E11009-53A2-47FB-B3CE-0B769B8EDBF3"), isAdmin, rtwToDo, ConditionActivity.NoOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("A69CB230-82BF-43F2-B6A4-7AFEAA0B4B3C"), rtwToDo, recertifyTodo, ReturnToWorkActivity.RecertifyOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("8AB802D3-B245-4A9E-8127-CE4E03BDD41C"), rtwToDo, verifyRTW, ReturnToWorkActivity.ReturnToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("6A54571C-7591-40A9-88C0-3F0BF3713ACD"), rtwToDo, closeCase, ReturnToWorkActivity.NotReturningToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("963F2B60-1413-4D2A-A502-2BE255CF4985"), verifyRTW, returnedToWork, VerifyReturnToWorkActivity.RtwVerifiedYesOutcome));
            SetTransition(wf, BuildTransition(new Guid("A290374A-EA23-4D73-8967-365945F45838"), verifyRTW, didNotReturnToWork, VerifyReturnToWorkActivity.RtwVerifiedNoOutcome));
            SetTransition(wf, BuildTransition(new Guid("35907E8A-182F-487B-87E8-2D092B8C7EC0"), rtwToDo, hasWorkRestrictions, ReturnToWorkActivity.ReturnToWorkOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("D92D64B4-B565-450A-953B-E574B2BB442C"), hasWorkRestrictions, enterWorkRestrictions, ConditionActivity.YesOutcomeValue));


            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
