﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void ReviewWorkSchedule()
        {
            Workflow wf = StageWorkflow("ReviewWorkSchedule", "Review Work Schedule", "Workflow that runs when an employee changes their info in self service",
                EventType.WorkScheduleChanged, new List<Rule>(1)
                {
                    BuildSelfServiceRule()
                });

            var reviewEmployeeWorkSchedule = BuildActivity(new Guid("FF72C8C2-9B27-485F-8EDC-AFFDDAFE5CE9"), "Review Employee Information", new ReviewWorkScheduleActivity().Id);
            reviewEmployeeWorkSchedule.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Review Work Schedule Of {{Employee.FullName}}")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            reviewEmployeeWorkSchedule = SetActivity(wf, reviewEmployeeWorkSchedule);

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
    
}
