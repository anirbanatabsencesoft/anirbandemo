﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseExtended()
        {
            Workflow wf = StageWorkflow("CaseExtended", "Case Extended", "Workflow that runs when a case is extended.", EventType.CaseExtended);
            //
            // Is Eligible Condition Activity
            var isElg = SetActivity(wf, BuildActivity(new Guid("F9AAC534-5E2F-4654-BDB6-110949A13C34"), "Is Eligible", new ConditionActivity().Id, new RuleGroup()
            {
                Name = "Is Eligible",
                Description = "Determines whether or not the case has any eligible policies",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(2)
                {
                    //
                    // If any open, eligible policies in the new or affected segment
                    new Rule()
                    {
                        Name = "Is Eligible",
                        Description = "Determines whether or not the case has any eligible policies",
                        LeftExpression = "IsEligible()",
                        Operator = "==",
                        RightExpression = "true"
                    },

                    new Rule()
                    {
                        Name = "Not Pending",
                        Description = "Determines whether or not the case has no pending policies",
                        LeftExpression = "IsPolicyPending()",
                        Operator = "==",
                        RightExpression = "false"
                    },

                }
            }));
            //
            // Send ELGNOTICE, "Send Eligibility Packet"
            var elg = SetActivity(wf, BuildActivity(new Guid("F0A9B691-A7F6-45CA-ABE5-A773F93AB550"), "Send Eligibility Packet", new SendCommunicationActivity().Id));
            elg.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Eligibility Packet")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ELGNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true);
            //
            // Send HRNOTICE
            var hr = SetActivity(wf, BuildActivity(new Guid("4470EE22-8630-4AFC-9075-57DFF57D84D6"), "HR Communication", new SendCommunicationActivity().Id));
            hr.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "HR Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "HRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new List<object>(1) { "HR" })
                .SetRawValue("Unique", true);
            //
            // Send MGRNOTICE
            var mgr = BuildActivity(new Guid("87BA5821-31F0-45E9-8837-C9448514CDED"), "Manager Communication", new SendCommunicationActivity().Id);
            mgr.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Manager Communication")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "MGRNOTICE")
                .SetRawValue("DefaultSendMethod", CommunicationType.Email)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("To", new List<object>(1) { "SUPERVISOR" })
                .SetRawValue("Unique", true);
            mgr = SetActivity(wf, mgr);
            //
            // Transitions
            SetTransition(wf, BuildTransition(new Guid("0768C565-FCC7-465A-87A3-40307830A56E"), isElg, elg, ConditionActivity.YesOutcomeValue));
            // HR goes out no matter what, hopefully the designer can handle this
            SetTransition(wf, BuildTransition(new Guid("E1BDC5D5-3726-4466-8B24-75A7183E45ED"), isElg, hr, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("36AA53F4-F6B7-46FC-A978-AB4E99518C65"), isElg, hr, ConditionActivity.NAOutcomeValue));
            // MGR goes out no matter what, hopefully the designer can handle this
            SetTransition(wf, BuildTransition(new Guid("F82F83A2-5B9F-452D-BD3C-28C9620F2475"), isElg, mgr, ConditionActivity.YesOutcomeValue));
            SetTransition(wf, BuildTransition(new Guid("D34BA9D6-0BB7-4D84-837B-57FD76FFD121"), isElg, mgr, ConditionActivity.NAOutcomeValue));
            
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
