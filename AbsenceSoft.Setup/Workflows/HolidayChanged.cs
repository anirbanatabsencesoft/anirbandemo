﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using System;
using System.Collections.Generic;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void HolidayChanged()
        {
            //
            // Holiday Changed Workflow
            Workflow wf = StageWorkflow("HolidayChanged", "Holiday Changed", "Workflow that runs when a holiday is added or updated.", EventType.HolidayChanged, new List<Rule>(2)
            {
                new Rule()
                {
                    Name = "Case Is Open",
                    Description = "Determines if the case is actually open",
                    LeftExpression = "Case.Status",
                    Operator = "==",
                    RightExpression = "CaseStatus.Open"
                },
                new Rule()
                {
                    Name = "Case Is Inquiry",
                    Description = "Determines if the case is an Inquiry",
                    LeftExpression = "Case.Status",
                    Operator = "==",
                    RightExpression = "CaseStatus.Inquiry"
                }
            });
            wf.CriteriaSuccessType = RuleGroupSuccessType.Or;
            //
            // Re-Calc Cases
            var reCalc = SetActivity(wf, BuildActivity(new Guid("3DF1B285-3AD0-44F3-9116-210544B975FC"), "ReCalc Case", new RunCalcsActivity().Id));
            
            //
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
