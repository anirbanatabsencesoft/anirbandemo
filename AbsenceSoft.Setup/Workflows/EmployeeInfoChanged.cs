﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void EmployeeInfoChanged()
        {
            Workflow wf = StageWorkflow("EmployeeInfoChanged", "Employee Info Changed", "Workflow that runs when an employee changes their info in self service",
                EventType.EmployeeChanged, new List<Rule>(1)
                {
                    BuildSelfServiceRule()
                });

            var reviewEmployeeInformation = SetActivity(wf, BuildActivity(new Guid("C797832D-2006-4E2A-9616-36DEC401C59D"), "Review Employee Information", new ReviewEmployeeInformationActivity().Id, m => m
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Review Employee Information Changed By {{Event.CreatedBy.DisplayName}}")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Unique", true)));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }

}
