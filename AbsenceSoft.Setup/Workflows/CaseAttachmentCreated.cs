﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseAttachmentCreated()
        {
            Workflow wf = StageWorkflow("CaseAttachmentCreated", "Case Attachment Created", "Workflow that runs when a case attachment is created in employee self service",
                EventType.AttachmentCreated, new List<Rule>(1){
                    BuildSelfServiceRule()
                });

            var reviewCaseAttachmentSchedule = BuildActivity(new Guid("930AC806-42AE-4DAB-BDD7-AEAE0B20A81C"), "Review Case Attachment", new ReviewCaseAttachmentActivity().Id);
            reviewCaseAttachmentSchedule.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Review Case Attachment '{{EventTarget.FileName}}', uploaded by {{Employee.FullName}}")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            
            reviewCaseAttachmentSchedule = SetActivity(wf, reviewCaseAttachmentSchedule);

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }

}
