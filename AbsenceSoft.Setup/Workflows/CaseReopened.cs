﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void CaseReopened()
        {
            Workflow wf = StageWorkflow("CaseReopened", "Case Reopened", "Workflow that runs when a case is reopened.", EventType.CaseReopened);
            //
            // Set all Canceled ToDo items with result, "CASE CANCELLED" to Pending
            var cancelledCaseToDos = BuildActivity(new Guid("987B58C6-B9D2-4F60-824A-0F9918207A71"), "Reopen ToDos", new UpdateToDosActivity().Id);
            cancelledCaseToDos.Metadata
                .SetRawValue("UpdateAll", true)
                .SetRawValue("Status", ToDoItemStatus.Pending)
                .SetRawValue("WhereStatus", ToDoItemStatus.Cancelled)
                .SetRawValue("WhereResultText", "CASE CANCELLED");
            cancelledCaseToDos = SetActivity(wf, cancelledCaseToDos);

            //
            // Set all Canceled ToDo items with result, "CASE CLOSED" to Pending
            var closedCaseToDos = BuildActivity(new Guid("DD4D80CB-EBBF-404A-8F2F-C1115B9F75E8"), "Reopen ToDos", new UpdateToDosActivity().Id);
            closedCaseToDos.Metadata
                .SetRawValue("UpdateAll", true)
                .SetRawValue("Status", ToDoItemStatus.Pending)
                .SetRawValue("WhereStatus", ToDoItemStatus.Cancelled)
                .SetRawValue("WhereResultText", "CASE CLOSED");
            closedCaseToDos = SetActivity(wf, closedCaseToDos);

            SetTransition(wf, BuildTransition(new Guid("E39EE101-07D7-4237-8643-A2DCC06891A4"), cancelledCaseToDos, closedCaseToDos));
            
            // Save
            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
