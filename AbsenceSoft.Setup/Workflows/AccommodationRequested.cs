﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Workflows.Activities;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        [WorkflowDataMethod]
        private static void AccommodationRequested()
        {
            Workflow wf = StageWorkflow("AccommodationRequested", "Accommodation Requested", "Workflow that runs when an accommodation is requested",
                EventType.AccommodationCreated);

            var ack = BuildActivity(new Guid("3D35A14A-7266-4D34-8E9E-5F62C2953426"), "Send Acknowledgement Letter", new SendCommunicationActivity().Id, new RuleGroup()
            {
                Name = "Is Case Only",
                Description = "Determines if this is a Case and not an Inquiry",
                SuccessType = RuleGroupSuccessType.And,
                Rules = new List<Rule>(1)
                {
                    new Rule()
                    {
                        Name = "Is Case Only",
                        Description = "Determines if this is a Case and not an Inquiry",
                        LeftExpression = "Case.Status",
                        Operator = "!=",
                        RightExpression = "CaseStatus.Inquiry",
                    }
                }
            });
            ack.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.Hours)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Send Accommodation Acknowledgement Letter to Employee")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("TemplateCode", "ACCOMACK")
                .SetRawValue("DefaultSendMethod", CommunicationType.Mail)
                .SetRawValue("SendAutomatically", false)
                .SetRawValue("Unique", true);
            ack = SetActivity(wf, ack);

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
