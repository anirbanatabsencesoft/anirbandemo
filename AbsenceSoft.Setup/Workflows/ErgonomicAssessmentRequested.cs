﻿using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Logic.Workflows.Activities.ToDos;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Setup.Workflows
{
    public static partial class WorkflowData
    {
        /// <summary>
        /// Creates a workflow that responds to an Accommodation Created event with a rule that it must be an ergonomic assessment
        /// </summary>
        [WorkflowDataMethod]
        private static void ErgonomicAssessmentRequested()
        {
            Workflow wf = StageWorkflow("ErgonomicAssessmentRequested", "Ergonomic Assessment Requested", "Workflow that runs when an ergonomic assessment is requested",
                EventType.AccommodationCreated, new List<Rule>(1) {
                BuildAccommodationRule("EA")
            });

            // Schedule an ergonomic assessment
            var scheduleErgonomicAssessmentActivity = BuildActivity(new Guid("2C603A77-D822-4C68-B3B0-D91455B51BBA"), "Schedule Ergonomic Assessment", new ScheduleErgonomicAssessmentActivity().Id);
            scheduleErgonomicAssessmentActivity.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Schedule Ergonomic Assessment")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            scheduleErgonomicAssessmentActivity = SetActivity(wf, scheduleErgonomicAssessmentActivity);

            // Complete the ergonomic assessment
            var completeErgonomicAssessmentActivity = BuildActivity(new Guid("FC15F76D-0FFD-4594-8D90-BD04D76177B2"), "Complete Ergonomic Assessment", new CompleteErgonomicAssessmentActivity().Id);
            completeErgonomicAssessmentActivity.Metadata
                .SetRawValue("Time", 0)
                .SetRawValue("Units", ToDoResponseTime.CalendarDays)
                .SetRawValue("TargetDueDateMeta", "ErgonomicAssessmentDate")
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Complete Ergonomic Assessment")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            completeErgonomicAssessmentActivity = SetActivity(wf, completeErgonomicAssessmentActivity);

            // Review the ergonomic assessment
            var createErgonomicAssessmentReviewActivity = BuildActivity(new Guid("A619955C-6EBF-46E4-A5C4-85980BD7A690"), "Case Review", new CaseReviewActivity().Id);
            createErgonomicAssessmentReviewActivity.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Perform Accommodation Determination")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            createErgonomicAssessmentReviewActivity = SetActivity(wf, createErgonomicAssessmentReviewActivity);

            // No ergonomic assessment done, just do the case
            var createCaseReviewActivity = BuildActivity(new Guid("D38C0B2D-178B-4BE1-8F96-5009038231D6"), "Case Review", new CaseReviewActivity().Id);
            createCaseReviewActivity.Metadata
                .SetRawValue("Time", 1)
                .SetRawValue("Units", ToDoResponseTime.WorkingDays)
                .SetRawValue("Optional", true)
                .SetRawValue("Title", "Perform Accommodation Determination, No ergonomic assessment")
                .SetRawValue("Priority", ToDoItemPriority.Normal)
                .SetRawValue("Spouse", false)
                .SetRawValue("Unique", true);
            createCaseReviewActivity = SetActivity(wf, createCaseReviewActivity);

            // Transitions
            SetTransition(wf, BuildTransition(new Guid("5AF5F8D5-F0A1-4D9B-9A7B-A1EFB615BB7D"), scheduleErgonomicAssessmentActivity, completeErgonomicAssessmentActivity));
            SetTransition(wf, BuildTransition(new Guid("56FBF727-B33C-4A66-A98A-808A2682224B"), completeErgonomicAssessmentActivity, createErgonomicAssessmentReviewActivity, CompleteErgonomicAssessmentActivity.ErgonomicAssessmentCompletedOutcome));
            SetTransition(wf, BuildTransition(new Guid("0E29BFEF-2336-4377-AC96-8A941904B6EC"), completeErgonomicAssessmentActivity, createCaseReviewActivity, CompleteErgonomicAssessmentActivity.ErgonomicAssessmentNotCompletedOutcome));

            wf.DesignerData = wf.BuildWorkflowDesignerData();
            wf.Save();
        }
    }
}
