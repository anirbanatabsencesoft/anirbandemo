﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Setup.Accommodation
{
	public static class AccommodationTypeData
	{
		public static void SetupData()
		{
			AccommodationType q;

            q = AccommodationType.GetById("000000000000000000000001") ?? new AccommodationType();
            q.CustomerId = null;
            q.EmployerId = null;
			q.Id = "000000000000000000000001";
			q.Code = "EA";
            q.Name = "Ergonomic Assessment";
            q.CaseTypeFlag = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative;
			q.Save();
			Console.WriteLine("Saved Accommodation Type '{0}' with Id '{1}'", q.Code, q.Id);

            q = AccommodationType.GetById("000000000000000000000002") ?? new AccommodationType();
            q.CustomerId = null;
            q.EmployerId = null;
			q.Id = "000000000000000000000002";
			q.Code = "EOS";
            q.Name = "Equipment Or Software";
            q.CaseTypeFlag = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative;
			q.Save();
            Console.WriteLine("Saved Accommodation Type '{0}' with Id '{1}'", q.Code, q.Id);

            q = AccommodationType.GetById("000000000000000000000003") ?? new AccommodationType();
            q.CustomerId = null;
            q.EmployerId = null;
			q.Id = "000000000000000000000003";
			q.Code = "SC";
            q.Name = "Schedule Change";
            q.CaseTypeFlag = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative;
			q.Save();
            Console.WriteLine("Saved Accommodation Type '{0}' with Id '{1}'", q.Code, q.Id);

            q = AccommodationType.GetById("000000000000000000000004") ?? new AccommodationType();
            q.CustomerId = null;
            q.EmployerId = null;
			q.Id = "000000000000000000000004";
			q.Code = "OTR";
            q.Name = "Other";
            q.CaseTypeFlag = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative;
			q.Save();
            Console.WriteLine("Saved Accommodation Type '{0}' with Id '{1}'", q.Code, q.Id);

            q = AccommodationType.GetById("000000000000000000000005") ?? new AccommodationType();
            q.CustomerId = null;
            q.EmployerId = null;
			q.Id = "000000000000000000000005";
			q.Code = "LEV";
            q.Name = "Leave";
            q.CaseTypeFlag = CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced | CaseType.Administrative;
			q.Save();
            Console.WriteLine("Saved Accommodation Type '{0}' with Id '{1}'", q.Code, q.Id);
		}
	}
}
