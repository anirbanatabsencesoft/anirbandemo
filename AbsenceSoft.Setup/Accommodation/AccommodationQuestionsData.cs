﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Setup.Accommodation
{
    public static class AccommodationQuestionsData
    {
        public static void SetupData()
        {
            AccommodationQuestion q;

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000001").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000001";
            q.Question = "Did you talk to the employee?";
            q.Order = 1;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question,q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000002").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000002";
            q.Question = "Did you obtain medical documentation of the need for the accommodation?";
            q.Order = 2;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question,q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000003").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000003";
            q.Question = "Have you considered the requested accommodations?";
            q.Order = 3;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000004").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000004";
            q.Question = "Is the accommodation reasonable?";
            q.Order = 4;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000005").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000005";
            q.Question = "Have you discussed it with Human Resources?";
            q.Order = 5;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000006").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000006";
            q.Question = "Have you discussed it with Operations?";
            q.Order = 6;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000007").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000007";
            q.Question = "Did you complete the accommodation determination?";
            q.Order = 7;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000008").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000008";
            q.Question = "Did you communicate the accommodation determination to the employee?";
            q.Order = 8;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

            q = AccommodationQuestion.AsQueryable().Where(p => p.Id == "000000000000000000000009").FirstOrDefault() ?? new AccommodationQuestion();
            q.Id = "000000000000000000000009";
            q.Question = "Monitor the accommodation?";
            q.Order = 9;

            q.Save();
            Console.WriteLine("Saved Accommodation Question '{0}' with Id '{1}'", q.Question, q.Id);

        }

    }
}
