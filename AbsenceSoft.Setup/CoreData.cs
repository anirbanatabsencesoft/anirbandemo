﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Setup.Policies;
using AbsenceSoft.Setup.Workflows;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AbsenceSoft.Setup
{
    public static class CoreData
    {
        private static void SetEmployer(User user, string employerId)
        {
            if (user.Employers == null)
                user.Employers = new List<EmployerAccess>();
            if (!user.Employers.Any(e => e.EmployerId == employerId))
                user.Employers.Add(new EmployerAccess()
                {
                    EmployerId = employerId,
                    AutoAssignCases = true
                });
            if (user.Employers.Count(e => e.EmployerId == employerId) > 1)
            {
                var first = user.Employers.First(e => e.EmployerId == employerId);
                user.Employers.RemoveAll(e => e.EmployerId == employerId && e.Id != first.Id);
            }
        }
        public static void SetupData(bool buildCommunicationsAndPaperwork = false)
        {
            Console.WriteLine("Creating user 'System User (admin@absencesoft.com)'");

            ///Forcibly updating the system user to have a CustomerId so it doesn't bomb when we try to retrieve it
            /// At one point we did not have a customer id on user, so we need to handle that scenario
            User.Repository.Collection.Update(User.Query.EQ(u => u.Email, "admin@absencesoft.com"), Update.Set("CustomerId", new ObjectId()));

            User systemUser = User.AsQueryable().Where(u => u.Email == "admin@absencesoft.com").FirstOrDefault() ?? new User();
            systemUser.Id = "000000000000000000000000";
            systemUser.Email = "admin@absencesoft.com";
            systemUser.FirstName = "System";
            systemUser.LastName = "User";
            systemUser.CreatedById = "000000000000000000000000";
            systemUser.ModifiedById = "000000000000000000000000";
            systemUser.CustomerId = "000000000000000000000000";
            systemUser.Save();
            Console.WriteLine("Saved user 'System User (admin@absencesoft.com)' with Id '{0}'", systemUser.Id);


            Console.WriteLine("Creating user 'Site Admin (siteadmin@absencesoft.com)'");
            User adminUser = User.AsQueryable().Where(u => u.Email == "siteadmin@absencesoft.com").FirstOrDefault() ?? new User();
            if (adminUser.IsNew)
            {
                // Don't override any changes made through the UI
                adminUser.Email = "siteadmin@absencesoft.com";
                adminUser.FirstName = "System";
                adminUser.LastName = "User";
                adminUser.UserType = UserType.Admin;
                adminUser.Password = new CryptoString() { PlainText = "!Complex001" };
                adminUser.Roles = new List<string>() { Role.SystemAdministrator.Id };
                adminUser.CreatedById = "000000000000000000000000";
                adminUser.ModifiedById = "000000000000000000000000";
                adminUser.Save();
                Console.WriteLine("Saved user 'System User (siteadmin@absencesoft.com)' with Id '{0}'", adminUser.Id);
            }


#if !RELEASE
            Console.WriteLine("Creating user 'Developer (dev@absencesoft.com)'");
            User devUser = User.AsQueryable().Where(u => u.Email == "dev@absencesoft.com").FirstOrDefault() ?? new User();
            devUser.Id = "000000000000000000000001";
            devUser.Email = "dev@absencesoft.com";
            devUser.FirstName = "Developer";
            devUser.LastName = "User";
            devUser.CreatedById = "000000000000000000000000";
            devUser.ModifiedById = "000000000000000000000000";
            devUser.Password = new CryptoString() { PlainText = "!Complex001" };
            devUser.Roles = new List<string>() { Role.SystemAdministrator.Id };
            devUser.CustomerId = "000000000000000000000002";
            SetEmployer(devUser, "000000000000000000000002");
            devUser.Save();
            Console.WriteLine("Saved user 'Developer (dev@absencesoft.com)' with Id '{0}'", devUser.Id);

            Console.WriteLine("Creating user 'Employee Self Service (ess@absencesoft.com)'");
            User essUser = User.AsQueryable().Where(u => u.Email == "ess@absencesoft.com").FirstOrDefault() ?? new User();
            essUser.Id = "000000000000000000000010";
            essUser.Email = "ess@absencesoft.com";
            essUser.FirstName = "Employee";
            essUser.LastName = "Self Service";
            essUser.UserType = UserType.SelfService;
            essUser.CreatedById = "000000000000000000000000";
            essUser.ModifiedById = "000000000000000000000000";
            essUser.Password = new CryptoString() { PlainText = "!Complex001" };
            essUser.Roles = new List<string>() { Role.SystemAdministrator.Id };
            essUser.CustomerId = "000000000000000000000002";
            SetEmployer(essUser, "000000000000000000000002");
            essUser.Save();
            Console.WriteLine("Saved user 'Employee Self Service (ess@absencesoft.com)' with Id '{0}'", essUser.Id);

            var essManagerRole = CreateESSManagerRole();
            var essRole = CreateESSRole();

            Console.WriteLine("Creating user 'Employee Manager (ess.manager@absencesoft.com)'");
            User essManagerUser = User.AsQueryable().Where(u => u.Email == "ess.manager@absencesoft.com").FirstOrDefault() ?? new User();
            essManagerUser.Id = "000000000000000000000011";
            essManagerUser.Email = "ess.manager@absencesoft.com";
            essManagerUser.FirstName = "Employee";
            essManagerUser.LastName = "Manager";
            essManagerUser.UserType = UserType.SelfService;
            essManagerUser.CreatedById = "000000000000000000000000";
            essManagerUser.ModifiedById = "000000000000000000000000";
            essManagerUser.Password = new CryptoString() { PlainText = "!Complex001" };
            essManagerUser.Roles = new List<string>() { essManagerRole.Id };
            essManagerUser.CustomerId = "000000000000000000000002";
            SetEmployer(essManagerUser, "000000000000000000000002");
            essManagerUser.Employers.First().EmployeeId = "000000000000000000000002";
            essManagerUser.Employers.First().ContactOf = new List<string>() { "000000000000000000000366" };
            essManagerUser.Save();
            Console.WriteLine("Saved user 'Employee Manager (ess.manager@absencesoft.com)' with Id '{0}'", essManagerUser.Id);

            Console.WriteLine("Creating user 'ESS Employee (ess.employee@absencesoft.com)'");
            User essEmployeeUser = User.AsQueryable().Where(u => u.Email == "ess.employee@absencesoft.com").FirstOrDefault() ?? new User();
            essEmployeeUser.Id = "000000000000000000000012";
            essEmployeeUser.Email = "ess.employee@absencesoft.com";
            essEmployeeUser.FirstName = "ESS";
            essEmployeeUser.LastName = "Employee";
            essEmployeeUser.UserType = UserType.SelfService;
            essEmployeeUser.CreatedById = "000000000000000000000000";
            essEmployeeUser.ModifiedById = "000000000000000000000000";
            essEmployeeUser.Password = new CryptoString() { PlainText = "!Complex001" };
            essEmployeeUser.Roles = new List<string>() { essRole.Id };
            essEmployeeUser.CustomerId = "000000000000000000000002";
            SetEmployer(essEmployeeUser, "000000000000000000000002");
            essEmployeeUser.Employers.First().EmployeeId = "000000000000000000000366";
            essEmployeeUser.Save();
            Console.WriteLine("Saved user 'ESS Employee (ess.employee@absencesoft.com)' with Id '{0}'", essEmployeeUser.Id);
#endif

#if !RELEASE
            Console.WriteLine("Creating user 'Developer (chad@scharfholdings.com)'");
            User chadUser = User.AsQueryable().Where(u => u.Email == "chad@scharfholdings.com").FirstOrDefault() ?? new User();
            chadUser.Id = "000000088000000000000000";
            chadUser.Email = "chad@scharfholdings.com";
            chadUser.FirstName = "Chad";
            chadUser.LastName = "Scharf";
            chadUser.CreatedById = "000000000000000000000000";
            chadUser.ModifiedById = "000000000000000000000000";
            chadUser.Password = new CryptoString() { PlainText = "!Complex001" };
            chadUser.Roles = new List<string>() { Role.SystemAdministrator.Id };
            chadUser.CustomerId = "000000000000000000000002";
            SetEmployer(chadUser, "000000000000000000000002");
            chadUser.Save();
            Console.WriteLine("Saved user 'Chad (chad@scharfholdings.com)' with Id '{0}'", chadUser.Id);
#endif
            Console.WriteLine("Creating user 'AMC1 (amc1@absencesoft.com)'");
            User AMC1 = User.AsQueryable().Where(u => u.Email == "amc1@absencesoft.com").FirstOrDefault() ?? new User();
            AMC1.Id = "000000088100000000000000";
            AMC1.Email = "amc1@absencesoft.com";
            AMC1.FirstName = "AMC1";
            AMC1.LastName = "LastName";
            AMC1.CreatedById = "000000000000000000000000";
            AMC1.ModifiedById = "000000000000000000000000";
            if (AMC1.IsNew)
                AMC1.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                AMC1.Password = new CryptoString() { PlainText = "!Complex001" };
#endif
            if (AMC1.Roles != null && !AMC1.Roles.Any())
            {
                AMC1.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }

            AMC1.CustomerId = "000000000000000000000002";
            SetEmployer(AMC1, "000000000000000000000002");
            AMC1.Save();
            Console.WriteLine("Saved user 'AMC1 (amc1@absencesoft.com)' with Id '{0}'", AMC1.Id);

            Console.WriteLine("Creating user 'AMC2 (amc2@absencesoft.com)'");
            User AMC2 = User.AsQueryable().Where(u => u.Email == "amc2@absencesoft.com").FirstOrDefault() ?? new User();
            AMC2.Id = "000000088200000000000000";
            AMC2.Email = "amc2@absencesoft.com";
            AMC2.FirstName = "AMC2";
            AMC2.LastName = "LastName";
            AMC2.CreatedById = "000000000000000000000000";
            AMC2.ModifiedById = "000000000000000000000000";
            if (AMC2.IsNew)
                AMC2.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                AMC2.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (AMC2.Roles != null && !AMC2.Roles.Any())
                AMC2.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(AMC2, "000000000000000000000002");
            AMC2.CustomerId = "000000000000000000000002";
            AMC2.Save();
            Console.WriteLine("Saved user 'AMC2 (amc2@absencesoft.com)' with Id '{0}'", AMC2.Id);

            Console.WriteLine("Creating user 'AMC3 (amc3@absencesoft.com)'");
            User AMC3 = User.AsQueryable().Where(u => u.Email == "amc3@absencesoft.com").FirstOrDefault() ?? new User();
            AMC3.Id = "000000088300000000000000";
            AMC3.Email = "amc3@absencesoft.com";
            AMC3.FirstName = "AMC3";
            AMC3.LastName = "LastName";
            AMC3.CreatedById = "000000000000000000000000";
            AMC3.ModifiedById = "000000000000000000000000";
            if (AMC3.IsNew)
                AMC3.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                AMC3.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (AMC3.Roles != null && !AMC3.Roles.Any())
                AMC3.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(AMC3, "000000000000000000000002");
            AMC3.CustomerId = "000000000000000000000002";
            AMC3.Save();
            Console.WriteLine("Saved user 'AMC3 (amc3@absencesoft.com)' with Id '{0}'", AMC3.Id);

            Console.WriteLine("Creating user 'AMC4 (amc4@absencesoft.com)'");
            User AMC4 = User.AsQueryable().Where(u => u.Email == "amc4@absencesoft.com").FirstOrDefault() ?? new User();
            AMC4.Id = "000000088400000000000000";
            AMC4.Email = "amc4@absencesoft.com";
            AMC4.FirstName = "AMC4";
            AMC4.LastName = "LastName";
            AMC4.CreatedById = "000000000000000000000000";
            AMC4.ModifiedById = "000000000000000000000000";
            if (AMC4.IsNew)
                AMC4.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                AMC4.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (AMC4.Roles != null && !AMC4.Roles.Any())
                AMC4.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(AMC4, "000000000000000000000002");
            AMC4.CustomerId = "000000000000000000000002";
            AMC4.Save();
            Console.WriteLine("Saved user 'AMC4 (amc4@absencesoft.com)' with Id '{0}'", AMC4.Id);

            Console.WriteLine("Creating user 'AMC5 (amc5@absencesoft.com)'");
            User AMC5 = User.AsQueryable().Where(u => u.Email == "amc5@absencesoft.com").FirstOrDefault() ?? new User();
            AMC5.Id = "000000088500000000000000";
            AMC5.Email = "amc5@absencesoft.com";
            AMC5.FirstName = "AMC5";
            AMC5.LastName = "LastName";
            AMC5.CreatedById = "000000000000000000000000";
            AMC5.ModifiedById = "000000000000000000000000";
            if (AMC5.IsNew)
                AMC5.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                AMC5.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (AMC5.Roles != null && !AMC5.Roles.Any())
                AMC5.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(AMC5, "000000000000000000000002");
            AMC5.CustomerId = "000000000000000000000002";
            AMC5.Save();
            Console.WriteLine("Saved user 'AMC5 (amc5@absencesoft.com)' with Id '{0}'", AMC5.Id);


            //STDCM1
            Console.WriteLine("Creating user 'STDCM1 (stdcm1@absencesoft.com)'");
            User STDCM1 = User.AsQueryable().Where(u => u.Email == "stdcm1@absencesoft.com").FirstOrDefault() ?? new User();
            STDCM1.Id = "000000088600000000000000";
            STDCM1.Email = "stdcm1@absencesoft.com";
            STDCM1.FirstName = "STDCM1";
            STDCM1.LastName = "LastName";
            STDCM1.CreatedById = "000000000000000000000000";
            STDCM1.ModifiedById = "000000000000000000000000";
            if (STDCM1.IsNew)
                STDCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                STDCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (STDCM1.Roles != null && !STDCM1.Roles.Any())
                STDCM1.Roles = new List<string>() { Role.SystemAdministrator.Id };
            STDCM1.CustomerId = "000000000000000000000002";
            SetEmployer(STDCM1, "000000000000000000000002");
            STDCM1.Save();
            Console.WriteLine("Saved user 'STDCM1 (stdcm1@absencesoft.com)' with Id '{0}'", STDCM1.Id);

            Console.WriteLine("Creating user 'STDCM2 (stdcm2@absencesoft.com)'");
            User STDCM2 = User.AsQueryable().Where(u => u.Email == "stdcm2@absencesoft.com").FirstOrDefault() ?? new User();
            STDCM2.Id = "000000088700000000000000";
            STDCM2.Email = "stdcm2@absencesoft.com";
            STDCM2.FirstName = "STDCM2";
            STDCM2.LastName = "LastName";
            STDCM2.CreatedById = "000000000000000000000000";
            STDCM2.ModifiedById = "000000000000000000000000";
            if (STDCM2.IsNew)
                STDCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                STDCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (STDCM2.Roles != null && !STDCM2.Roles.Any())
                STDCM2.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(STDCM2, "000000000000000000000002");
            STDCM2.CustomerId = "000000000000000000000002";
            STDCM2.Save();
            Console.WriteLine("Saved user 'STDCM2 (stdcm2@absencesoft.com)' with Id '{0}'", STDCM2.Id);

            Console.WriteLine("Creating user 'STDCM3 (stdcm3@absencesoft.com)'");
            User STDCM3 = User.AsQueryable().Where(u => u.Email == "stdcm3@absencesoft.com").FirstOrDefault() ?? new User();
            STDCM3.Id = "000000088800000000000000";
            STDCM3.Email = "stdcm3@absencesoft.com";
            STDCM3.FirstName = "STDCM3";
            STDCM3.LastName = "LastName";
            STDCM3.CreatedById = "000000000000000000000000";
            STDCM3.ModifiedById = "000000000000000000000000";
            if (STDCM3.IsNew)
                STDCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                STDCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (STDCM3.Roles != null && !STDCM3.Roles.Any())
            {
                STDCM3.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(STDCM3, "000000000000000000000002");
            STDCM3.CustomerId = "000000000000000000000002";
            STDCM3.Save();
            Console.WriteLine("Saved user 'STDCM3 (stdcm3@absencesoft.com)' with Id '{0}'", STDCM3.Id);

            //LTDCM1
            Console.WriteLine("Creating user 'LTDCM1 (ltdcm1@absencesoft.com)'");
            User LTDCM1 = User.AsQueryable().Where(u => u.Email == "ltdcm1@absencesoft.com").FirstOrDefault() ?? new User();
            LTDCM1.Id = "000000099100000000000000";
            LTDCM1.Email = "ltdcm1@absencesoft.com";
            LTDCM1.FirstName = "LTDCM1";
            LTDCM1.LastName = "LastName";
            LTDCM1.CreatedById = "000000000000000000000000";
            LTDCM1.ModifiedById = "000000000000000000000000";
            if (LTDCM1.IsNew)
                LTDCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                LTDCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (LTDCM1.Roles != null && !LTDCM1.Roles.Any())
            {
                LTDCM1.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(LTDCM1, "000000000000000000000002");
            LTDCM1.CustomerId = "000000000000000000000002";
            LTDCM1.Save();
            Console.WriteLine("Saved user 'LTDCM1 (LTDCM1@absencesoft.com)' with Id '{0}'", LTDCM1.Id);

            //LTDCM2
            Console.WriteLine("Creating user 'LTDCM2 (LTDCM2@absencesoft.com)'");
            User LTDCM2 = User.AsQueryable().Where(u => u.Email == "ltdcm2@absencesoft.com").FirstOrDefault() ?? new User();
            LTDCM2.Id = "000000099200000000000000";
            LTDCM2.Email = "ltdcm2@absencesoft.com";
            LTDCM2.FirstName = "LTDCM2";
            LTDCM2.LastName = "LastName";
            LTDCM2.CreatedById = "000000000000000000000000";
            LTDCM2.ModifiedById = "000000000000000000000000";
            if (LTDCM2.IsNew)
                LTDCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                LTDCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (LTDCM2.Roles != null && !LTDCM2.Roles.Any())
                LTDCM2.Roles = new List<string>() { Role.SystemAdministrator.Id };
            SetEmployer(LTDCM2, "000000000000000000000002");
            LTDCM2.CustomerId = "000000000000000000000002";
            LTDCM2.Save();
            Console.WriteLine("Saved user 'LTDCM2 (LTDCM2@absencesoft.com)' with Id '{0}'", LTDCM2.Id);

            //LTDCM3
            Console.WriteLine("Creating user 'LTDCM3 (LTDCM3@absencesoft.com)'");
            User LTDCM3 = User.AsQueryable().Where(u => u.Email == "ltdcm3@absencesoft.com").FirstOrDefault() ?? new User();
            LTDCM3.Id = "000000099300000000000000";
            LTDCM3.Email = "ltdcm3@absencesoft.com";
            LTDCM3.FirstName = "LTDCM3";
            LTDCM3.LastName = "LastName";
            LTDCM3.CreatedById = "000000000000000000000000";
            LTDCM3.ModifiedById = "000000000000000000000000";
            if (LTDCM3.IsNew)
                LTDCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                LTDCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (LTDCM3.Roles != null && !LTDCM3.Roles.Any())
            {
                LTDCM3.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(LTDCM3, "000000000000000000000002");
            LTDCM3.CustomerId = "000000000000000000000002";
            LTDCM3.Save();
            Console.WriteLine("Saved user 'LTDCM3 (LTDCM3@absencesoft.com)' with Id '{0}'", LTDCM3.Id);

            //CSR
            Console.WriteLine("Creating user 'CSR (CSR@absencesoft.com)'");
            User CSR = User.AsQueryable().Where(u => u.Email == "csr@absencesoft.com").FirstOrDefault() ?? new User();
            CSR.Id = "000000099400000000000000";
            CSR.Email = "csr@absencesoft.com";
            CSR.FirstName = "CSR";
            CSR.LastName = "LastName";
            CSR.CreatedById = "000000000000000000000000";
            CSR.ModifiedById = "000000000000000000000000";
            if (CSR.IsNew)
                CSR.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                CSR.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (CSR.Roles != null && !CSR.Roles.Any())
            {
                CSR.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(CSR, "000000000000000000000002");
            CSR.CustomerId = "000000000000000000000002";
            CSR.Save();
            Console.WriteLine("Saved user 'CSR (CSR@absencesoft.com)' with Id '{0}'", CSR.Id);

            //ACM
            Console.WriteLine("Creating user 'ACM1 (ACM1@absencesoft.com)'");
            User ACM1 = User.AsQueryable().Where(u => u.Email == "acm1@absencesoft.com").FirstOrDefault() ?? new User();
            ACM1.Id = "000000099500000000000000";
            ACM1.Email = "acm1@absencesoft.com";
            ACM1.FirstName = "ACM1";
            ACM1.LastName = "LastName";
            ACM1.CreatedById = "000000000000000000000000";
            ACM1.ModifiedById = "000000000000000000000000";
            if (ACM1.IsNew)
                ACM1.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                ACM1.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (ACM1.Roles != null && !ACM1.Roles.Any())
            {
                ACM1.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(ACM1, "000000000000000000000002");
            ACM1.CustomerId = "000000000000000000000002";
            ACM1.Save();
            Console.WriteLine("Saved user 'ACM1 (ACM1@absencesoft.com)' with Id '{0}'", ACM1.Id);


            //ACM2
            Console.WriteLine("Creating user 'ACM2 (ACM2@absencesoft.com)'");
            User ACM2 = User.AsQueryable().Where(u => u.Email == "acm2@absencesoft.com").FirstOrDefault() ?? new User();
            ACM2.Id = "000000099600000000000000";
            ACM2.Email = "acm2@absencesoft.com";
            ACM2.FirstName = "ACM2";
            ACM2.LastName = "LastName";
            ACM2.CreatedById = "000000000000000000000000";
            ACM2.ModifiedById = "000000000000000000000000";
            if (ACM2.IsNew)
                ACM2.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                ACM2.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (ACM2.Roles != null && !ACM2.Roles.Any())
            {
                ACM2.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(ACM2, "000000000000000000000002");
            ACM2.CustomerId = "000000000000000000000002";
            ACM2.Save();
            Console.WriteLine("Saved user 'ACM2 (ACM2@absencesoft.com)' with Id '{0}'", ACM2.Id);


            //ACM3
            Console.WriteLine("Creating user 'ACM3 (ACM3@absencesoft.com)'");
            User ACM3 = User.AsQueryable().Where(u => u.Email == "acm3@absencesoft.com").FirstOrDefault() ?? new User();
            ACM3.Id = "000000099700000000000000";
            ACM3.Email = "acm3@absencesoft.com";
            ACM3.FirstName = "ACM3";
            ACM3.LastName = "LastName";
            ACM3.CreatedById = "000000000000000000000000";
            ACM3.ModifiedById = "000000000000000000000000";
            if (ACM3.IsNew)
                ACM3.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                ACM3.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (ACM3.Roles != null && !ACM3.Roles.Any())
            {
                ACM3.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(ACM3, "000000000000000000000002");
            ACM3.CustomerId = "000000000000000000000002";
            ACM3.Save();
            Console.WriteLine("Saved user 'ACM3 (ACM3@absencesoft.com)' with Id '{0}'", ACM3.Id);


            //WCM
            Console.WriteLine("Creating user 'WCM1 (WCM1@absencesoft.com)'");
            User WCM1 = User.AsQueryable().Where(u => u.Email == "wcm1@absencesoft.com").FirstOrDefault() ?? new User();
            WCM1.Id = "000000099800000000000000";
            WCM1.Email = "wcm1@absencesoft.com";
            WCM1.FirstName = "WCM1";
            WCM1.LastName = "LastName";
            WCM1.CreatedById = "000000000000000000000000";
            WCM1.ModifiedById = "000000000000000000000000";
            if (WCM1.IsNew)
                WCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                WCM1.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (WCM1.Roles != null && !WCM1.Roles.Any())
            {
                WCM1.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(WCM1, "000000000000000000000002");
            WCM1.CustomerId = "000000000000000000000002";
            WCM1.Save();
            Console.WriteLine("Saved user 'WCM1 (WCM1@absencesoft.com)' with Id '{0}'", WCM1.Id);


            //WCM2
            Console.WriteLine("Creating user 'WCM2 (WCM2@absencesoft.com)'");
            User WCM2 = User.AsQueryable().Where(u => u.Email == "wcm2@absencesoft.com").FirstOrDefault() ?? new User();
            WCM2.Id = "000000099900000000000000";
            WCM2.Email = "wcm2@absencesoft.com";
            WCM2.FirstName = "WCM2";
            WCM2.LastName = "LastName";
            WCM2.CreatedById = "000000000000000000000000";
            WCM2.ModifiedById = "000000000000000000000000";
            if (WCM2.IsNew)
                WCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                WCM2.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (WCM2.Roles != null && !WCM2.Roles.Any())
            {
                WCM2.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(WCM2, "000000000000000000000002");
            WCM2.CustomerId = "000000000000000000000002";
            WCM2.Save();
            Console.WriteLine("Saved user 'WCM2 (WCM2@absencesoft.com)' with Id '{0}'", WCM2.Id);


            //WCM3
            Console.WriteLine("Creating user 'WCM3 (WCM3@absencesoft.com)'");
            User WCM3 = User.AsQueryable().Where(u => u.Email == "wcm3@absencesoft.com").FirstOrDefault() ?? new User();
            WCM3.Id = "000000099910000000000000";
            WCM3.Email = "wcm3@absencesoft.com";
            WCM3.FirstName = "WCM3";
            WCM3.LastName = "LastName";
            WCM3.CreatedById = "000000000000000000000000";
            WCM3.ModifiedById = "000000000000000000000000";
            if (WCM3.IsNew)
                WCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#if !RELEASE
            else
                WCM3.Password = new CryptoString() { PlainText = "!Complex001" };
#endif

            if (WCM3.Roles != null && !WCM3.Roles.Any())
            {
                WCM3.Roles = new List<string>() { Role.SystemAdministrator.Id };
            }
            SetEmployer(WCM3, "000000000000000000000002");
            WCM3.CustomerId = "000000000000000000000002";
            WCM3.Save();
            Console.WriteLine("Saved user 'WCM3 (WCM3@absencesoft.com)' with Id '{0}'", WCM3.Id);

            UpdateAbsenceReason("EHC", "000000000000000001000000", "Employee Health Condition", null, null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("FHC", "000000000000000001000001", "Family Health Condition", null, AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("PREGMAT", "000000000000000001000002", "Pregnancy/Maternity", null, AbsenceReasonFlag.RequiresPregnancyDetails, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("BONDING", "000000000000000001000022", "Bonding", null, AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("ADOPT", "000000000000000001000003", "Adoption/Foster Care", null, AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("MILITARY", "000000000000000001000004", "Care for Injured Servicemember", "Military", AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("EXIGENCY", "000000000000000001000005", "Qualifying Exigency", "Military", AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("PARENTAL", "000000000000000001000006", "Parental / School", "Other", AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("DOM", "000000000000000001000007", "Domestic Violence", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("ORGAN", "000000000000000001000008", "Organ Donor", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("BONE", "000000000000000001000009", "Marrow Donor", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("BLOOD", "000000000000000001000010", "Blood Donor", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("RESERVETRAIN", "000000000000000001000011", "Reserve Training", "Military", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("CRIME", "000000000000000001000012", "Criminal Proceeding", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("CEREMONY", "000000000000000001000013", "Ceremony", "Military", AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("PERSONAL", "000000000000000001000014", "Personal", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("EDUCATIONAL", "000000000000000001000015", "Educational", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("JURY", "000000000000000001000016", "Jury Duty", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("MILITARY", "000000000000000001000017", "Military", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("UNION", "000000000000000001000018", "Union", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("BEREAVEMENT", "000000000000000001000019", "Bereavement", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("ADMIN", "000000000000000001000020", "Administrative", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("ACCOMM", "000000000000000001000021", "Accommodation Request", "Administrative", AbsenceReasonFlag.ShowAccommodation, CaseType.Administrative);
            UpdateAbsenceReason("ACTIVEDUTY", "000000000000000001000031", "Active Duty (Self)", "Military", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("CIVICSERVICE", "000000000000000001000023", "Civic Service", "Other", null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced);
            UpdateAbsenceReason("PARENTALLEAVE", "000000000000000001000032", "Parental Leave", null, AbsenceReasonFlag.RequiresRelationship, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced,"CA");
            UpdateAbsenceReason("LFTHRMEDCOND", "000000000000000001000033", "Life Threatening Medical Condition", null, null, CaseType.Consecutive | CaseType.Intermittent | CaseType.Reduced, "CA");

            UpdateNoteCategories();
            CreateDemandData();
            UpdateCaseCategories();
            UpdateDenialReasons();
            UpdateEmployeeClasses();

            Type pBaseType = typeof(PolicyBase);
            // Place STD first in the list
            foreach (var policyBuilder in Assembly.GetAssembly(pBaseType).GetTypes().Where(t => t != pBaseType && t.IsSubclassOf(pBaseType)).OrderBy(t => t.Name == "STD" ? "_STD" : t.Name))
            {
                PolicyBase pBase = Activator.CreateInstance(policyBuilder) as PolicyBase;
                pBase.BuildPolicies();
            }

            if (buildCommunicationsAndPaperwork)
            {
                CommunicationData.BuildTemplates();
            }

            DiagnosisData.SetupData();

            WorkflowData.ConfigureCoreWorkflows();

            UpdateOshaFormFieldByCode();
        }

        private static Role CreateESSManagerRole()
        {
            Role essManagerRole = Role.AsQueryable().FirstOrDefault(r => r.Name == "ESS Manager" && r.CustomerId == "000000000000000000000002") ?? new Role();
            essManagerRole.CustomerId = "000000000000000000000002";
            essManagerRole.Name = "ESS Manager";
            essManagerRole.CreatedById = User.System.Id;
            essManagerRole.ModifiedById = User.System.Id;
            essManagerRole.Type = RoleType.SelfService;
            if(essManagerRole.Permissions == null)
                essManagerRole.Permissions = new List<string>();

            essManagerRole.Permissions.AddIfNotExists(Permission.MyEmployeesDashboard.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.ViewTeam.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.ViewCase.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.EditCase.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.CreateCase.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.ViewAllEmployees.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.ViewEmployee.Id);
            essManagerRole.Permissions.AddIfNotExists(Permission.AttachFile.Id);

            return essManagerRole.Save();
        }

        private static Role CreateESSRole()
        {
            Role essRole = Role.AsQueryable().FirstOrDefault(r => r.Name == "ESS Employee" && r.CustomerId == "000000000000000000000002") ?? new Role();
            essRole.CustomerId = "000000000000000000000002";
            essRole.Name = "ESS Employee";
            essRole.CreatedById = User.System.Id;
            essRole.ModifiedById = User.System.Id;
            essRole.Type = RoleType.SelfService;
            if (essRole.Permissions == null)
                essRole.Permissions = new List<string>();

            essRole.Permissions.AddIfNotExists(Permission.ViewCase.Id);
            essRole.Permissions.AddIfNotExists(Permission.CreateCase.Id);
            essRole.Permissions.AddIfNotExists(Permission.EditCase.Id);
            essRole.Permissions.AddIfNotExists(Permission.ViewEmployee.Id);
            essRole.Permissions.AddIfNotExists(Permission.AddITOR.Id);

            return essRole.Save();
        }


        private static void UpdateAbsenceReason(string code, string id, string name, string category = null, AbsenceReasonFlag? flags = null, CaseType? caseTypes = null, string country = null)
        {
            AbsenceReason ar = AbsenceReason.GetById(id) ?? new AbsenceReason();
            ar.Id = id;
            ar.Name = name;
            ar.Code = code;
            ar.Category = category;
            ar.EmployerId = null;
            ar.CustomerId = null;
            ar.Country = country;
            ar.CreatedById = "000000000000000000000000";
            ar.ModifiedById = "000000000000000000000000";
            ar.Flags = flags;
            ar.CaseTypes = caseTypes;
            ar.Save();
        }

        private static void DeleteAbsenceReasonByCode(string code)
        {
            AbsenceReason ar = AbsenceReason.AsQueryable().FirstOrDefault(a => a.Code == code && a.CustomerId == null);
            if (ar != null)
                ar.Delete();
        }

        public static Employee UpsertEmployee(Employee emp)
        {
            if (emp == null)
                return emp;

            Console.WriteLine("Upserting employee '{0} {1}'", emp.FirstName, emp.LastName);
            Employee me = Employee.GetById(emp.Id) ?? new Employee() { Id = emp.Id };
            me.CreatedById = emp.CreatedById;
            me.CustomerId = emp.CustomerId;
            me.DoB = emp.DoB;
            me.EmployeeNumber = emp.EmployeeNumber;
            me.EmployerId = emp.EmployerId;
            me.FirstName = emp.FirstName;
            me.Gender = emp.Gender;
            me.HireDate = emp.HireDate;
            me.IsExempt = emp.IsExempt;
            me.IsKeyEmployee = emp.IsKeyEmployee;
            me.JobTitle = emp.JobTitle;
            me.LastName = emp.LastName;
            me.Meets50In75MileRule = emp.Meets50In75MileRule;
            me.MiddleName = emp.MiddleName;
            me.MilitaryStatus = emp.MilitaryStatus;
            me.ModifiedById = emp.ModifiedById;
            me.PayType = emp.PayType;
            me.PriorHours = emp.PriorHours;
            me.RehireDate = emp.RehireDate;
            me.Salary = emp.Salary;
            me.ServiceDate = emp.ServiceDate;
            me.SpouseEmployeeNumber = emp.SpouseEmployeeNumber;
            me.Ssn = emp.Ssn;
            me.Status = emp.Status;
            me.TerminationDate = emp.TerminationDate;
            me.WorkCountry = emp.WorkCountry;
            me.WorkSchedules = emp.WorkSchedules;
            me.WorkState = emp.WorkState;
            me.EmployeeClassCode = emp.EmployeeClassCode;
            me.Info = emp.Info;
            me = me.Save();
            Console.WriteLine("Saved employee '{0} {1}' with Id '{2}'", me.FirstName, me.LastName, me.Id);
            return me;
        }

        public static DiagnosisCode UpsertDiagnosis(DiagnosisCode code)
        {
            if (code == null)
                return code;

            Console.WriteLine("Upserting Diagnosis Code: {0}", code.Code);
            DiagnosisCode i = DiagnosisCode.AsQueryable().Where(ic => ic.Code == code.Code).FirstOrDefault() ?? new DiagnosisCode();

            i.CodeType = code.CodeType;
            i.Code = code.Code;
            i.Name = code.Name;
            i.Description = code.Description;
            i.DurationType = code.DurationType;
            i.MaxDuration = code.MaxDuration;
            i.Save();

            Console.WriteLine("Diagnosis Code Saved: {0}", i.Code);

            return i;
        }

        public static PaySchedule UpsertPaySchedule(PaySchedule ps)
        {
            if (ps == null)
                return null;

            Console.WriteLine("Upserting Payschedule: {0}", ps.Name);
            ps.Save();
            Console.WriteLine("Pay Schedule saved: {0}", ps.Name);
            return ps;
        }

        public static List<Data.Time> GetTimes(DateTime startDate, int hoursPerWeek)
        {
            DateTime scheduleStart = startDate.GetFirstDayOfWeek();
            int remainderMinutes = (hoursPerWeek * 60) % 5;
            int minutesPerDay = (int)Math.Floor((double)((hoursPerWeek * 60) / 5));

            List<Data.Time> times = new List<Data.Time>()
            {
                new Data.Time() { SampleDate = scheduleStart.AddDays(0), TotalMinutes = 0 },
                new Data.Time() { SampleDate = scheduleStart.AddDays(1), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(2), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(3), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(4), TotalMinutes = minutesPerDay },
                new Data.Time() { SampleDate = scheduleStart.AddDays(5), TotalMinutes = minutesPerDay + remainderMinutes },
                new Data.Time() { SampleDate = scheduleStart.AddDays(6), TotalMinutes = 0 },
            };

            return times;
        }

        internal static void UpdateNoteCategories()
        {
            UpdateNoteCategoryByCode("CaseSummary", "Case Summary", "Summarizes a Case", EntityTarget.Case);
            UpdateNoteCategoryByCode("EmployeeConversation", "Employee Conversation", "A conversation with an employee", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("ManagerConversation", "Manager Conversation", "A conversation with a manager", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("HRConversation", "HR Conversation", "A conversation with HR", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("Medical", "Medical", "Medical", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("TimeOffRequest", "Time Off Request", "A Time Off Request", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("Certification", "Certification", "Certification", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("InteractiveProcess", "Interactive Process", "Part of the Interactive Process", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("WorkRestriction", "Work Restriction", "A note related to Work Restriction", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("OperationsConversation", "Operations Conversation", "A conversation with operations", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("HealthCareProviderConversation", "Health Care Provider Conversation", "A conversation with a Health Care Provider", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("WorkerCompensationConversation", "Worker Compensation Conversation", "A conversation with Workers Compensation", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("SafetyConversation", "Safety Conversation", "A conversation about safety", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("LegalConversation", "Legal Conversation", "A conversation with Legal", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("ERCConversation", "ERC Conversation", "A conversation with ERC", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("EmployeeRelationsConversation", "Employee Relations Conversation", "A conversation about Employee Relations", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("BenefitsConversation", "Benefits Conversation", "A conversation about benefits", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("LOAConversation", "Leave of Absence Conversation", "A conversation about a Leave of Absence", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("FacilitiesConversation", "Facilities Conversation", "A conversation about facilities", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("PayrollConversation", "Payroll Conversation", "A conversation with payroll", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("STDLOAVendorConversation", "STD LOA Vendor Conversation", "A conversation with a short term disability leave of absence vendor", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("LTDVendorConversation", "LTD Vendor Conversation", "A conversation with a long term disability vendor", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("Referral", "Referral", "Referral", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("DisabilityNote", "Disability Note", "Disability Note for STD", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("Diagnosis", "Diagnosis", "Notes for Diagnosis", EntityTarget.Employee | EntityTarget.Case);
            UpdateNoteCategoryByCode("Other", "Other", "Other", EntityTarget.Employee | EntityTarget.Case);
        }

        internal static void UpdateOshaFormFieldByCode()
        {
            UpdateOshaFormFieldByCode(OshaFormFieldService.InjuredBodyPart, "Part of Body", OshaFormType.Osha301, new List<string>());
            UpdateOshaFormFieldByCode(OshaFormFieldService.WhatHarmedTheEmployee, "Cause of Injury", OshaFormType.Osha301, new List<string>());
        }

        private static void UpdateNoteCategoryByCode(string code, string name, string description, EntityTarget target)
        {
            NoteCategory category = NoteCategory.GetByCode(code) ?? new NoteCategory();

            category.Code = code;
            category.Name = name;
            category.Description = description;
            category.Target = target;
            category.Save();
        }

        private static void CreateDemandData()
        {
            List<Customer> customers = Customer.AsQueryable().ToList();
            using (var demandService = new DemandService(null))
            {
                foreach (var customer in customers)
                {
                    int numberOfDemandTypes = DemandType.AsQueryable().Count(dt => dt.CustomerId == customer.Id);
                    if (numberOfDemandTypes <= 0)
                    {
                        demandService.SetupDemandData(customer.Id);
                    }
                }
            }

        }

        internal static void UpdateCaseCategories()
        {
            UpdateCaseClosureCategoryByCode("NOLONG", "No longer necessary", "No longer necessary");
            UpdateCaseClosureCategoryByCode("CASECOMP", "Case complete", "Case complete");

        }
        private static void UpdateCaseClosureCategoryByCode(string code, string name, string description)
        {
            CaseCategory reason = CaseCategory.GetByCode(code) ?? new CaseCategory();

            reason.Code = code;
            reason.Name = name;
            reason.Description = description;
            reason.Save();
        }

        internal static void UpdateDenialReasons()
        {
            //Add Adjudication Denial Reason
            UpdateDenialReasonByCode("OTHER", "Other", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("EXHAUSTED", "Exhausted", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("NOTAVALIDHEALTHCAREPROVIDER", "Not A Valid Healthcare Provider", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("NOTASERIOUSHEALTHCONDITION", "Not A Serious Health Condition", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("ELIMINATIONPERIOD", "Elimination Period", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("PERUSECAP", "Per Use Cap", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("INTERMITTENTNONALLOWED", "Intermittent Non Allowed", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("PAPERWORKNOTRECEIVED", "Paperwork Not Received", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("NOTANELIGIBLEFAMILYMEMBER", "Not An Eligible Family Member", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("MAXOCCURRENCEREACHED", "Max Occurrence Reached", DenialReasonTarget.Policy);
            UpdateDenialReasonByCode("NOTAPPLICABLE", "Not Applicable", DenialReasonTarget.Policy);
            //add Accommodation Adjudication Denial Reason
            UpdateDenialReasonByCode("OTHER", "Other", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("NOTAREASONABLEREQUEST", "Not A Reasonable Request", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("INELIGIBLEHEALTHCONDITION", "Ineligible Health Condition", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("FALSIFIEDDOCUMENTATION", "Falsified Documentation", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("REGULATORYPROHIBITIONAGAINSTREQUEST", "Regulatory Prohibition Against Request", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("ESSENTIALJOBFUNCTIONCANNOTACCOMMODATE", "Essential Job Function Can Not Accommodate", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("SITECANNOTACCOMMODATE", "Site Cannot Accommodate", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("NOPOSITIONFITRESTRICTIONS", "No Position Fit Restrictions", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("CANNOTACCOMMODATE", "Cannot Accommodate", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("NOPAPERWORKPROVIDED", "No Paperwork Provided", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("EMPLOYEEDIDNOTPARTICIPATE", "Employee Did Not Participate", DenialReasonTarget.Accommodation);
            //new added for Accommodation
            UpdateDenialReasonByCode("DOCUMENTATIONNOTPROVIDED", "Documentation Not Provided", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("INCOMPLETEDOCUMENTATION", "Incomplete Documentation", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("NOTAREASONABLEACCOMMODATION", "Not A Reasonable Accommodation", DenialReasonTarget.Accommodation);
            UpdateDenialReasonByCode("EMPLOYEEDIDNOTACCEPTACCOMMODATIONSUGGESTION", "Employee Did Not Accept Accommodation Suggestion", DenialReasonTarget.Accommodation);
        }

        public static void UpdateEmployeeClasses()
        {
            //Add Employee Classe
            UpdateEmployeeClassByCode("FULLTIME", "Full Time", 1);
            UpdateEmployeeClassByCode("PARTTIME", "Part Time", 2);
            UpdateEmployeeClassByCode("PERDIEM", "Per Diem", 3);
        }

        private static void UpdateDenialReasonByCode(string code, string description, DenialReasonTarget target)
        {
            DenialReason reason = DenialReason.GetByCode(code);
            if (reason == null)
            {
                reason = new DenialReason();
                reason.Code = code;
                reason.Description = description;
                reason.Target = target;
                if (code == AdjudicationDenialReason.Exhausted || code == AdjudicationDenialReason.Other || code == AdjudicationDenialReason.EliminationPeriod ||
                    code == AdjudicationDenialReason.PerUseCap || code == AdjudicationDenialReason.MaxOccurrenceReached)
                {
                    reason.Locked = true;
                }
            }
            else
            {
                reason.Target = reason.Target != DenialReasonTarget.None ? reason.Target | target : target;
            }
            reason.Save();
        }

        private static void UpdateOshaFormFieldByCode(string code, string name, OshaFormType category, List<string> options)
        {
            OshaFormField osha = OshaFormField.GetByCode(code) ?? new OshaFormField();
            osha.Code = code;
            osha.Name = name;
            osha.Category = category;
            osha.Options = options;
            osha.CustomerId = null;
            osha.EmployerId = null;
            osha.IsCustom = false;
            osha.Save();
        }

        private static void UpdateEmployeeClassByCode(string code, string name, int order)
        {
            EmployeeClass employeeClass = EmployeeClass.GetByCode(code);
            if (employeeClass == null)
            {
                employeeClass = new EmployeeClass();
                employeeClass.Code = code;
                employeeClass.Name = name;
                employeeClass.IsLocked = false;
                employeeClass.Order = order;
            }
            else
            {
                employeeClass.Code = code;
                employeeClass.Name = name;
                employeeClass.Order = order;
            }
            employeeClass.Save();
        }
    }
}
