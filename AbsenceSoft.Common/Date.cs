﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft
{
    public static class Date
    {
        /// <summary>
        /// Represents the Unix Epoch date of Jan 1, 1970 UTC.
        /// </summary>
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Gets the elapsed time in milliseconds between this date and another.
        /// </summary>
        /// <param name="startDate">The date this method extends.</param>
        /// <param name="endDate">The date to measure through.</param>
        /// <returns>The total number of milliseconds elapsed between the start and end dates.</returns>
        public static long Elapsed(this DateTime startDate, DateTime endDate)
        {
            return (long)(endDate - startDate).TotalMilliseconds;
        }

        /// <summary>
        /// Gets the elapsed time in milliseconds between this date and the current date/time.
        /// Automatically determines if UTC or Local should be used.
        /// </summary>
        /// <param name="startDate">The date this method extends.</param>
        /// <returns>The total number of milliseconds elapsed between the start and now.</returns>
        public static long Elapsed(this DateTime startDate)
        {
            return (long)startDate.Elapsed(startDate.Kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now);
        }

        /// <summary>
        /// Gets the elapsed time in milliseconds since the Unix Epoch.
        /// </summary>
        /// <param name="dt">The date to measure through.</param>
        /// <returns></returns>
        public static long ToUnixDate(this DateTime dt)
        {
            return (long)(dt - UnixEpoch).TotalMilliseconds;
        }

        /// <summary>
        /// Gets the DateTime from the total milliseconds since the unix epoch date.
        /// </summary>
        /// <param name="milliseconds">Total milliseconds since the unix epoch date.</param>
        /// <returns>The DateTime from the total milliseconds since the unix epoch date.</returns>
        public static DateTime FromUnixDate(this long milliseconds)
        {
            return UnixEpoch.AddMilliseconds(milliseconds);
        }

        /// <summary>
        /// Returns the minimum value out of the date parameter values provided.
        /// </summary>
        /// <param name="dates">The dates.</param>
        /// <returns>The minimum value out of the date parameter values provided.</returns>
        public static DateTime Min(params DateTime[] dates)
        {
            if (!dates.Any())
                return DateTime.MinValue;
            return dates.Min();
        }

        /// <summary>
        /// Returns the minimum value out of the date parameter values provided.
        /// </summary>
        /// <param name="dates">The dates.</param>
        /// <returns>The minimum value out of the date parameter values provided.</returns>
        public static DateTime? Min(params DateTime?[] dates)
        {
            if (!dates.Any(r => r.HasValue))
                return null;
            return dates.Where(r => r.HasValue).Min();
        }

        /// <summary>
        /// Returns the maximum value out of the date parameter values provided.
        /// </summary>
        /// <param name="dates">The dates.</param>
        /// <returns>The maximum value out of the date parameter values provided.</returns>
        public static DateTime Max(params DateTime[] dates)
        {
            if (!dates.Any())
                return DateTime.MaxValue;
            return dates.Max();
        }

        /// <summary>
        /// Returns the maximum value out of the date parameter values provided.
        /// </summary>
        /// <param name="dates">The dates.</param>
        /// <returns>The maximum value out of the date parameter values provided.</returns>
        public static DateTime? Max(params DateTime?[] dates)
        {
            if (!dates.Any(r => r.HasValue))
                return null;
            return dates.Where(r => r.HasValue).Max();
        }

        /// <summary>
        /// Gets the number of leap days between the 2 dates. Useful for adjusting stuff.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>The total number of instances of Feb 29 between the startDate and endDate.</returns>
        public static int NumberOfLeapDays(DateTime startDate, DateTime endDate)
        {
            return startDate.AllYearsInRange(endDate)
                .Where(y => DateTime.IsLeapYear(y))
                .Select(y => new DateTime(y, 2, 29, 0, 0, 0, DateTimeKind.Utc))
                .Where(d => d.DateInRange(startDate, endDate))
                .Count();
        }

        /// <summary>
        /// Gets the total cumulative timeframe of all date ranges minus gaps and overlap between ranges.
        /// </summary>
        /// <param name="ranges">The date ranges which to measure total days.</param>
        /// <returns>A timespan representing the total amount of time between all date ranges minus gaps and overlap.</returns>
        public static TimeSpan GetCumulativeTimeframeMinusGapsAndOverlap(params DateRange[] ranges)
        {
            if (ranges == null || ranges.Length == 0)
                return TimeSpan.Zero;

            var sorted = ranges
                .Where(r => r.EndDate.HasValue)
                .OrderBy(r => r.StartDate)
                .ThenBy(r => r.EndDate)
                .ToList();

            if (!sorted.Any())
                return TimeSpan.Zero;

            DateRange d = sorted[0];
            TimeSpan time = d.Timeframe;

            for (var i = 1; i < sorted.Count; i++)
            {
                var d2 = sorted[i];

                if (!d.StartDate.DateRangesOverLap(d.EndDate, d2.StartDate, d2.EndDate))
                {
                    time += d2.Timeframe;
                    continue;
                }

                var days = (double)d2.StartDate.AllDatesInRange(d2.EndDate.Value).Except(d.StartDate.AllDatesInRange(d.EndDate.Value)).Count();
                time += TimeSpan.FromDays(days);

                d = sorted[i];
            }

            return time;
        }
       

        public static DateTime NewUtcDate(int year, int month, int day) { return new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc); }
 

        /// <summary>
        /// Builds the dates overlap range query. Used by most reports. Assumes the StartDate and EndDate field names are "StartDate" and "EndDate" but builders are included.
        /// </summary>
        /// <param name="startDate">The start date of the range to test.</param>
        /// <param name="endDate">The end date of the range to test, or <c>null</c> if perpetual.</param>
        /// <returns>An IMongoQuery containing an OR condition with 4 AND conditions for date range overlap logic.</returns>
        public static IMongoQuery BuildDatesOverlapRangeQuery(DateTime startDate, DateTime? endDate)
        {
            // If there's no End Date, then wow, duh, if the End Date of our target end date is greater than equal to our target start date
            //  then it's going to overlap no matter what
            if (!endDate.HasValue)
                return Query.Or(
                    Query.GTE("EndDate", new BsonDateTime(startDate)),
                    Query.EQ("EndDate", BsonNull.Value),
                    Query.NotExists("EndDate")
                );

            // Otherwise, we need to see if the combined date ranges overlap in either direction during any segment of time.
            return Query.Or(
                Query.And(Query.GTE("StartDate", new BsonDateTime(startDate)), Query.LTE("StartDate", new BsonDateTime(endDate.Value))),
                Query.And(Query.GTE("EndDate", new BsonDateTime(startDate)), Query.LTE("EndDate", new BsonDateTime(endDate.Value))),
                Query.And(Query.LTE("StartDate", new BsonDateTime(startDate)), Query.GTE("EndDate", new BsonDateTime(startDate))),
                Query.And(Query.LTE("StartDate", new BsonDateTime(endDate.Value)), Query.GTE("EndDate", new BsonDateTime(endDate.Value)))
            );
        }
    }
}
