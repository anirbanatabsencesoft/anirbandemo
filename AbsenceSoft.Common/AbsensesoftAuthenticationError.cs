﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    [Serializable]
    [ComVisible(true)]
    public class AbsensesoftAuthenticationError : AbsenceSoftException
    {
        public const string AbsensesoftAuthenticationErrorMessage = "Account verification error. Please verify that your UserId/Email and current password are correct.  If this error persists, please contact your administrator and ensure you are an authorized user.";

        public AbsensesoftAuthenticationError() :
            base(AbsensesoftAuthenticationErrorMessage)
        {

        }
    }
}
