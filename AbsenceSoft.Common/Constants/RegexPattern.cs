﻿namespace AbsenceSoft.Common.Constants
{
    public static class RegexPattern
    {
        public static readonly string Url = @"^http(s)?://([\w-]+.)+[\w-]+(/[\w-./?%&=])?$";
    }
}
