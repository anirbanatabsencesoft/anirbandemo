﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace AbsenceSoft
{
    /// <summary>
    /// Base class for all dynamic awesomeness!!! Anything that needs to work like an ExpandoObject, and
    /// be BSON serializable as well as JSON serializable for MongoDB and REST, AJAX, etc.
    /// </summary>
    [Serializable, BsonIgnoreExtraElements(false, Inherited = true)]
    public class DynamicAwesome : DynamicObject, IDictionary<string, object>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicAwesome"/> class.
        /// </summary>
        public DynamicAwesome() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicAwesome"/> class.
        /// </summary>
        /// <param name="seed">The seed.</param>
        public DynamicAwesome(BsonDocument seed) : this()
        {
            MetaData = seed ?? MetaData;
        }

        /// <summary>
        /// Gets or sets all of the result fields to be serialized inline with this item.
        /// This item is serialized as part of the Bson serializer when storing this result to
        /// MongoDB so all of the fields get stored as actual properties the same object graph.
        /// </summary>
        [BsonExtraElements, JsonIgnore]
        private BsonDocument MetaData = new BsonDocument(); //end: MetaData

        /// <summary>
        /// Gets or sets all of the result fields to be serialized inline with this item.
        /// This does not get serialized into Bson, but however is used to dynamically build
        /// a Json string from the current Bson metadata document.
        /// </summary>
        [JsonExtensionData, BsonIgnore]
        private IDictionary<string, object> AdditionalData
        {
            get
            {
                Dictionary<string, object> tokens = new Dictionary<string, object>();
                foreach (var key in this.MetaData.Names)
                {
                    BsonValue bson = this.MetaData.GetValue(key);
                    object val = BsonTypeMapper.MapToDotNetValue(bson);
                    tokens.Add(key, val);
                }
                return tokens;
            }
            set
            {
                if (value == null || value.Count == 0)
                {
                    this.MetaData.Clear();
                    return;
                }
                foreach (var token in value)
                    this.MetaData.SetRawValue(token.Key, token.Value);
            }
        }//end: AdditionalData

        /// <summary>
        /// Gets or sets a value from the dynamic member name for this
        /// dynamic awesome instance.
        /// </summary>
        /// <param name="member">The dynamic member name to get or set the value for</param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {
                if (this.MetaData.Contains(key))
                    return this.MetaData.GetValue(key);
                return null;
            }
            set
            {
                this.MetaData.Set(key, BsonValue.Create(value));
            }
        }//end: this[string member]

        #region DynamicObject Implementation

        /// <summary>
        /// Returns the enumeration of all dynamic member names.
        /// </summary>
        /// <returns>A sequence that contains dynamic member names.</returns>
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return this.MetaData.Names;
        }//end: GetDynamicMemberNames

        /// <summary>
        /// Provides the implementation for operations that delete an object by index.
        /// This method is not intended for use in C# or Visual Basic.
        /// </summary>
        /// <param name="binder">Provides information about the deletion.</param>
        /// <param name="indexes">The indexes to be deleted.</param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a language-specific run-time exception is thrown.)
        /// </returns>
        public override bool TryDeleteIndex(DeleteIndexBinder binder, object[] indexes)
        {
            if (indexes.Length != 1 && indexes[0] != null)
                return false;
            string prop = indexes[0].ToString();
            try
            {
                if (this.MetaData.Names.Contains(prop))
                    this.MetaData.Remove(prop);
                return true;
            }
            catch
            {
                return base.TryDeleteIndex(binder, indexes);
            }
        }//end: TryDeleteIndex

        /// <summary>
        /// Provides the implementation for operations that delete an object member.
        /// This method is not intended for use in C# or Visual Basic.
        /// </summary>
        /// <param name="binder">Provides information about the deletion.</param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a language-specific run-time exception is thrown.)
        /// </returns>
        public override bool TryDeleteMember(DeleteMemberBinder binder)
        {
            try
            {
                if (this.MetaData.Names.Contains(binder.Name))
                    this.MetaData.Remove(binder.Name);
                return true;
            }
            catch
            {
                return base.TryDeleteMember(binder);
            }
        }//end: TryDeleteMember

        /// <summary>
        /// Provides the implementation for operations that get a value by index. Classes
        /// derived from the System.Dynamic.DynamicObject class can override this method
        /// to specify dynamic behavior for indexing operations.
        /// </summary>
        /// <param name="binder">Provides information about the operation.</param>
        /// <param name="indexes">
        /// The indexes that are used in the operation. For example, for the sampleObject[3]
        /// operation in C# (sampleObject(3) in Visual Basic), where sampleObject is
        /// derived from the DynamicObject class, indexes[0] is equal to 3.
        /// </param>
        /// <param name="result">The result of the index operation.</param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a run-time exception is thrown.)
        /// </returns>
        public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
        {
            if (indexes.Length != 1 && indexes[0] != null)
            {
                result = null;
                return false;
            }
            string prop = indexes[0].ToString();
            BsonValue val;
            if (this.MetaData.TryGetValue(prop, out val))
            {
                result = BsonTypeMapper.MapToDotNetValue(val);
                return true;
            }
            return base.TryGetIndex(binder, indexes, out result);
        }//end: TryGetIndex

        /// <summary>
        /// Provides the implementation for operations that get member values. Classes
        /// derived from the System.Dynamic.DynamicObject class can override this method
        /// to specify dynamic behavior for operations such as getting a value for a
        /// property.
        /// </summary>
        /// <param name="binder">
        /// Provides information about the object that called the dynamic operation.
        /// The binder.Name property provides the name of the member on which the dynamic
        /// operation is performed. For example, for the Console.WriteLine(sampleObject.SampleProperty)
        /// statement, where sampleObject is an instance of the class derived from the
        /// System.Dynamic.DynamicObject class, binder.Name returns "SampleProperty".
        /// The binder.IgnoreCase property specifies whether the member name is case-sensitive.
        /// </param>
        /// <param name="result">
        /// The result of the get operation. For example, if the method is called for
        /// a property, you can assign the property value to result.
        /// </param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a run-time exception is thrown.)
        /// </returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            BsonValue val;
            if (this.MetaData.TryGetValue(binder.Name, out val))
            {
                result = BsonTypeMapper.MapToDotNetValue(val);
                return true;
            }
            return base.TryGetMember(binder, out result);
        }//end: TryGetMember

        /// <summary>
        /// Provides the implementation for operations that set a value by index. Classes
        /// derived from the System.Dynamic.DynamicObject class can override this method
        /// to specify dynamic behavior for operations that access objects by a specified
        /// index.
        /// </summary>
        /// <param name="binder">Provides information about the operation.</param>
        /// <param name="indexes">
        /// The indexes that are used in the operation. For example, for the sampleObject[3]
        /// = 10 operation in C# (sampleObject(3) = 10 in Visual Basic), where sampleObject
        /// is derived from the System.Dynamic.DynamicObject class, indexes[0] is equal
        /// to 3.
        /// </param>
        /// <param name="value">
        /// The value to set to the object that has the specified index. For example,
        /// for the sampleObject[3] = 10 operation in C# (sampleObject(3) = 10 in Visual
        /// Basic), where sampleObject is derived from the System.Dynamic.DynamicObject
        /// class, value is equal to 10.
        /// </param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a language-specific run-time exception is thrown.
        /// </returns>
        public override bool TrySetIndex(SetIndexBinder binder, object[] indexes, object value)
        {
            if (indexes.Length != 1 && indexes[0] != null)
                return false;
            string prop = indexes[0].ToString();
            this.MetaData.SetRawValue(prop, value);
            return true;
        }//end: TrySetIndex

        /// <summary>
        /// Provides the implementation for operations that set member values. Classes
        /// derived from the System.Dynamic.DynamicObject class can override this method
        /// to specify dynamic behavior for operations such as setting a value for a
        /// property.
        /// </summary>
        /// <param name="binder">
        /// Provides information about the object that called the dynamic operation.
        /// The binder.Name property provides the name of the member to which the value
        /// is being assigned. For example, for the statement sampleObject.SampleProperty
        /// = "Test", where sampleObject is an instance of the class derived from the
        /// System.Dynamic.DynamicObject class, binder.Name returns "SampleProperty".
        /// The binder.IgnoreCase property specifies whether the member name is case-sensitive.
        /// </param>
        /// <param name="value">
        /// The value to set to the member. For example, for sampleObject.SampleProperty
        /// = "Test", where sampleObject is an instance of the class derived from the
        /// System.Dynamic.DynamicObject class, the value is "Test".
        /// </param>
        /// <returns>
        /// <c>true</c> if the operation is successful; otherwise, <c>false</c>. If this method returns
        /// <c>false</c>, the run-time binder of the language determines the behavior. (In most
        /// cases, a language-specific run-time exception is thrown.)
        /// </returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this.MetaData.SetRawValue(binder.Name, value);
            return true;
        }//end: TrySetMember

        #endregion DynamicObject Implementation

        #region IDictionary<string, object> Implementation

        /// <summary>
        /// Adds an element with the provided key and value to the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        public void Add(string key, object value)
        {
            this.MetaData.Set(key, BsonValue.Create(value));
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the <see cref="T:System.Collections.Generic.IDictionary`2" />.</param>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the key; otherwise, false.
        /// </returns>
        public bool ContainsKey(string key)
        {
            return this.MetaData.Contains(key);
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        public ICollection<string>Keys { get { return this.MetaData.Names.ToList(); } }

        /// <summary>
        /// Removes the element with the specified key from the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>
        /// true if the element is successfully removed; otherwise, false.  This method also returns false if <paramref name="key" /> was not found in the original <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </returns>
        public bool Remove(string key)
        {
            this.MetaData.Remove(key);
            return true;
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <param name="value">When this method returns, the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value" /> parameter. This parameter is passed uninitialized.</param>
        /// <returns>
        /// true if the object that implements <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the specified key; otherwise, false.
        /// </returns>
        public bool TryGetValue(string key, out object value)
        {
            BsonValue bson;
            if (this.MetaData.TryGetValue(key, out bson))
            {
                value = BsonTypeMapper.MapToDotNetValue(bson);
                return true;
            }
            value = null;
            return false;
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        public ICollection<object> Values
        {
            get { return this.MetaData.Values.Select(v => BsonTypeMapper.MapToDotNetValue(v)).ToList(); }
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(KeyValuePair<string, object> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            this.MetaData.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(KeyValuePair<string, object> item)
        {
            return this.MetaData.Contains(item.Key);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            int idx = arrayIndex;
            foreach (var kvp in AdditionalData)
            {
                if (idx >= array.Length)
                    break;
                array[idx] = new KeyValuePair<string, object>(kvp.Key, kvp.Value);
                idx++;
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count { get { return this.MetaData.ElementCount; } }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly { get { return false; } }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(KeyValuePair<string, object> item)
        {
            this.MetaData.Remove(item.Key);
            return true;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return this.AdditionalData.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.AdditionalData.GetEnumerator();
        }

        #endregion IDictionary<string, object> Implementation

        /// <summary>
        /// Clones this Dynamic Awesome by doing a DeepClone on the underlying super-duper equally awesome BsonDocument 'n' stuff dude.
        /// </summary>
        /// <returns></returns>
        public DynamicAwesome Clone()
        {
            return new DynamicAwesome()
            {
                MetaData = this.MetaData.DeepClone().AsBsonDocument
            };
        }

        /// <summary>
        /// Casts the BsonValue to a BsonDocument (throws an InvalidCastException if the cast is not valid).
        /// </summary>
        /// <value>
        /// Gets the current instance as a BSON document.
        /// </value>
        [BsonIgnore, JsonIgnore]
        public BsonDocument AsBsonDocument
        {
            get { return this.MetaData.DeepClone().AsBsonDocument; }
        }

        /// <summary>
        /// Applies the specified document state.
        /// </summary>
        /// <param name="documentState">State of the document.</param>
        public void Apply(BsonDocument documentState)
        {
            if (documentState != null)
                this.MetaData.Merge(documentState);
        }
    }//end: DynamicAwesome
}//end: namespace
