﻿using System.Linq;

namespace AbsenceSoft
{
    public static class StringUtils
    {
        public static string LowercaseFirstNotNullString(params string[] strings)
        {
            string firstNotNullString = strings.FirstOrDefault(s => !string.IsNullOrWhiteSpace(s));
            if (firstNotNullString != null)
            {
                return firstNotNullString.ToLowerInvariant();
            }

            return null;
        }
    }
}
