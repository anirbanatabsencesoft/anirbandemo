﻿using System;
namespace AbsenceSoft.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToYearMonthDayWithoutSeperator(this DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMdd");
        }
    }
}
