﻿using AbsenceSoft.Common.Constants;
using System;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsValidUrl(this string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return false;
            }
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        }
    }
}
