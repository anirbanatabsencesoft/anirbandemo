﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    /// <summary>
    /// The exception used for any AbsenceSoft errors to be thrown from the logic or data tiers.
    /// </summary>
    [Serializable]
    [ComVisible(true)]
    public class AbsenceSoftException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftException"/> class.
        /// </summary>
        public AbsenceSoftException() : base("An error has ocurred") { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public AbsenceSoftException(string message) : base(message ?? "An error has ocurred") { }

        public AbsenceSoftException(IEnumerable<string> messages) :
            base(string.Join("\n", messages))
        { }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected AbsenceSoftException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftException"/> class with a specified error message 
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception. If the innerException
        /// parameter is not a null reference, the current exception is raised in a catch
        /// block that handles the inner exception.</param>
        public AbsenceSoftException(string message, Exception innerException) : base(message ?? "An error has ocurred", innerException) { }
    }
}
