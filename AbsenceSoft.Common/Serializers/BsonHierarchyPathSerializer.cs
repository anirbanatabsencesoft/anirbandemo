﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;

namespace AbsenceSoft.Common.Serializers
{
    /// <summary>
    /// Represents a serializer for HierarchyPaths.
    /// </summary>
    public class BsonHierarchyPathSerializer : SerializerBase<object>
    {
        /// <summary>
        /// The __instance
        /// </summary>
        private static BsonHierarchyPathSerializer __instance = new BsonHierarchyPathSerializer();

        /// <summary>
        /// Gets an instance of the <see cref="BsonHierarchyPathSerializer"/> class.
        /// </summary>
        public static BsonHierarchyPathSerializer Instance { get { return BsonHierarchyPathSerializer.__instance; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            //base.VerifyTypes(nominalType, actualType, typeof(HierarchyPath));
            BsonType currentBsonType = context.Reader.GetCurrentBsonType();
            BsonType bsonType = currentBsonType;
            if (bsonType == BsonType.String)
                return HierarchyPath.Parse(context.Reader.ReadString());
            string message = string.Format("Cannot deserialize HierarchyPath from BsonType {0}.", currentBsonType);
            throw new AbsenceSoftException(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="value"></param>
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            if (value == null)
                context.Writer.WriteNull();
            else if (!(value is HierarchyPath))
                throw new ArgumentException("Value must be of type HierarchyPath", "value");
            else
            {
                var path = (HierarchyPath)value;
                if (path.IsNull)
                    context.Writer.WriteNull();
                else
                    context.Writer.WriteString(path.Path);
            }
        }
    }
}
