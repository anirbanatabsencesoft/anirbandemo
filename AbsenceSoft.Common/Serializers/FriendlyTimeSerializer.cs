﻿using Newtonsoft.Json;
using Newtonsoft.Json.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common.Serializers
{
    public class FriendlyTimeSerializer : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(string) || objectType == typeof(int) || objectType == typeof(int?));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.Null)
            {
                string value = reader.Value as string;
                if (string.IsNullOrWhiteSpace(value))
                    return 0;

                return value.ParseFriendlyTime();
            }

            if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
                return null;

            throw new JsonSerializationException(string.Format("Cannot convert null value to {0}.", objectType));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            if (value is string)
            {
                writer.WriteValue(value);
                return;
            }

            int intVal = Convert.ToInt32(value);
            writer.WriteValue(intVal.ToFriendlyTime());
        }
    }
}
