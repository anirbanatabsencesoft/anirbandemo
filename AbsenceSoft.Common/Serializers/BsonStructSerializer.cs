﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Linq;
using System.Reflection;

namespace AbsenceSoft.Common.Serializers
{
    public class BsonStructSerializer : SerializerBase<Object>
    {
        /// <summary>
        /// The __instance
        /// </summary>
        private static BsonStructSerializer __instance = new BsonStructSerializer();

        /// <summary>
        /// Gets an instance of the <see cref="BsonStructSerializer"/> class.
        /// </summary>
        public static BsonStructSerializer Instance { get { return BsonStructSerializer.__instance; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override Object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var obj = Activator.CreateInstance(base.ValueType);

            context.Reader.ReadStartDocument();
            while (context.Reader.ReadBsonType() != BsonType.EndOfDocument)
            {
                var name = context.Reader.ReadName();

                var field = base.ValueType.GetField(name);
                if (field != null)
                {
                    var value = BsonSerializer.Deserialize(context.Reader, field.FieldType);
                    field.SetValue(obj, value);
                }

                var prop = base.ValueType.GetProperty(name);
                if (prop != null && prop.CanWrite)
                {
                    var value = BsonSerializer.Deserialize(context.Reader, prop.PropertyType);
                    prop.SetValue(obj, value, null);
                }
            }
            context.Reader.ReadEndDocument();

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="value"></param>
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Object value)
        {
            var fields = args.NominalType.GetFields(BindingFlags.Instance | BindingFlags.Public)
                .Where(f => f.GetCustomAttribute<BsonIgnoreAttribute>() == null)
                .ToList();
            var props = args.NominalType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => p.CanWrite)
                .Where(p => p.GetCustomAttribute<BsonIgnoreAttribute>() == null)
                .ToList();

            context.Writer.WriteStartDocument();
            foreach (var field in fields)
            {
                context.Writer.WriteName(field.Name);
                BsonSerializer.Serialize(context.Writer, field.FieldType, field.GetValue(value));
            }
            foreach (var prop in props)
            {
                context.Writer.WriteName(prop.Name);
                BsonSerializer.Serialize(context.Writer, prop.PropertyType, prop.GetValue(value, null));
            }
            context.Writer.WriteEndDocument();
        }
    }
}
