﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common.Serializers
{
    public static class JsonSettings
    {
        private static object _sync = new object();
        private static JsonSerializerSettings _settings;

        /// <summary>
        /// Gets the serializer settings.
        /// </summary>
        /// <value>
        /// The serializer settings.
        /// </value>
        public static JsonSerializerSettings SerializerSettings
        {
            get
            {
                if (_settings == null)
                {
                    lock (_sync)
                    {
                        if (_settings == null)
                        {
                            _settings = new JsonSerializerSettings();
                            _settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                            _settings.Converters.Add(new BsonNullConverter());
                        }
                    }
                }
                return _settings;
            }
        }
    }
}
