﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;

namespace AbsenceSoft.Common.Serializers
{
    public class TimeOfDaySerializer : SerializerBase<TimeOfDay>
    {
        // private static fields
        private static TimeOfDaySerializer __instance = new TimeOfDaySerializer();

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOfDaySerializer"/> class.
        /// </summary>
        public TimeOfDaySerializer() : base() { }

        /// <summary>
        /// Deserializes an object from a BsonReader.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override TimeOfDay Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            //VerifyTypes(nominalType, actualType, typeof(TimeOfDay));

            var bsonType = context.Reader.GetCurrentBsonType();
            TimeOfDay value;
            switch (bsonType)
            {
                case BsonType.String:
                    value = new TimeOfDay(context.Reader.ReadString());
                    break;
                case BsonType.Document:
                    context.Reader.ReadStartDocument();
                    int h = context.Reader.ReadInt32("Hour");
                    int m = context.Reader.ReadInt32("Minutes");
                    value = new TimeOfDay(h, m);
                    context.Reader.ReadEndDocument();
                    break;
                default:
                    var message = string.Format("Cannot deserialize TimeOfDay from BsonType {0}.", bsonType);
                    throw new BsonSerializationException(message);
            }

            return value;
        }

        /// <summary>
        /// Serializes an object to a BsonWriter.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="value"></param>
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, TimeOfDay value)
        {
            var time = (TimeOfDay)value;
            context.Writer.WriteString(time.ToString());
        }
    }
}
