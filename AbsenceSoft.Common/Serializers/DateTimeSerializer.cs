﻿using System;
using System.Globalization;
using System.IO;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;

namespace AbsenceSoft.Common.Serializers
{
    /// <summary>
    /// Represents a serializer for DateTimes that doesn't go all ape-shit
    /// and blow up converting DateTime with TimeOfDay not zero and instead
    /// just quietly lobs it off just like it should.
    /// </summary>
    public class DateTimeSerializer : MongoDB.Bson.Serialization.Serializers.DateTimeSerializer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateOnly"></param>
        /// <param name="representation"></param>
        public DateTimeSerializer(bool dateOnly, BsonType representation) : base(dateOnly, representation)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override DateTime Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            //VerifyTypes(nominalType, actualType, typeof(DateTime));
            //var dateTimeSerializationOptions = EnsureSerializationOptions<DateTimeSerializationOptions>(options);

            var bsonType = context.Reader.GetCurrentBsonType();
            DateTime value;
            switch (bsonType)
            {
                case BsonType.DateTime:
                    // use an intermediate BsonDateTime so MinValue and MaxValue are handled correctly
                    value = new BsonDateTime(context.Reader.ReadDateTime()).ToUniversalTime();
                    if (DateOnly)
                        value = value.Date;
                    break;
                case BsonType.Document:
                    context.Reader.ReadStartDocument();
                    context.Reader.ReadDateTime("DateTime"); // ignore value (use Ticks instead)
                    value = new DateTime(context.Reader.ReadInt64("Ticks"), DateTimeKind.Utc);
                    context.Reader.ReadEndDocument();
                    break;
                case BsonType.Int64:
                    value = DateTime.SpecifyKind(new DateTime(context.Reader.ReadInt64()), DateTimeKind.Utc);
                    break;
                case BsonType.String:
                    // note: we're not using XmlConvert because of bugs in Mono
                    if (DateOnly)
                    {
                        value = DateTime.SpecifyKind(DateTime.ParseExact(context.Reader.ReadString(), "yyyy-MM-dd", null), DateTimeKind.Utc);
                    }
                    else
                    {
                        var formats = new string[] { "yyyy-MM-ddK", "yyyy-MM-ddTHH:mm:ssK", "yyyy-MM-ddTHH:mm:ss.FFFFFFFK" };
                        value = DateTime.ParseExact(context.Reader.ReadString(), formats, null, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
                    }
                    break;
                default:
                    var message = string.Format("Cannot deserialize DateTime from BsonType {0}.", bsonType);
                    throw new BsonSerializationException(message);
            }

            if (DateOnly)
            {
                value = DateTime.SpecifyKind(value, DateTimeKind.Utc); // not ToLocalTime or ToUniversalTime!
            }
            else
            {
                DateTimeKind kind = DateTimeKind.Utc;
                switch (kind)
                {
                    case DateTimeKind.Local:
                    case DateTimeKind.Unspecified:
                        value = DateTime.SpecifyKind(BsonUtils.ToLocalTime(value), kind);
                        break;
                    case DateTimeKind.Utc:
                        value = BsonUtils.ToUniversalTime(value);
                        break;
                }
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="value"></param>
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, DateTime value)
        {
            var dateTime = (DateTime)value;
            //var dateTimeSerializationOptions = EnsureSerializationOptions<DateTimeSerializationOptions>(options);

            DateTime utcDateTime;
            if (DateOnly)
            {
                if (dateTime.TimeOfDay != TimeSpan.Zero)
                {
                    dateTime = dateTime.Date;
                }
                utcDateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc); // not ToLocalTime
            }
            else
            {
                utcDateTime = BsonUtils.ToUniversalTime(dateTime);
            }
            var millisecondsSinceEpoch = BsonUtils.ToMillisecondsSinceEpoch(utcDateTime);

            switch (Representation)
            {
                case BsonType.DateTime:
                    context.Writer.WriteDateTime(millisecondsSinceEpoch);
                    break;
                case BsonType.Document:
                    context.Writer.WriteStartDocument();
                    context.Writer.WriteDateTime("DateTime", millisecondsSinceEpoch);
                    context.Writer.WriteInt64("Ticks", utcDateTime.Ticks);
                    context.Writer.WriteEndDocument();
                    break;
                case BsonType.Int64:
                    context.Writer.WriteInt64(utcDateTime.Ticks);
                    break;
                case BsonType.String:
                    if (DateOnly)
                    {
                        context.Writer.WriteString(dateTime.ToString("yyyy-MM-dd"));
                    }
                    else
                    {
                        // we're not using XmlConvert.ToString because of bugs in Mono
                        if (dateTime == DateTime.MinValue || dateTime == DateTime.MaxValue)
                        {
                            // serialize MinValue and MaxValue as Unspecified so we do NOT get the time zone offset
                            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
                        }
                        else if (dateTime.Kind == DateTimeKind.Unspecified)
                        {
                            // serialize Unspecified as Local se we get the time zone offset
                            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
                        }
                        context.Writer.WriteString(dateTime.ToString("yyyy-MM-ddTHH:mm:ss.FFFFFFFK"));
                    }
                    break;
                default:
                    var message = string.Format("'{0}' is not a valid DateTime representation.", Representation);
                    throw new BsonSerializationException(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            var serializer = new AbsenceSoft.Common.Serializers.DateTimeSerializer(true, BsonType.DateTime);

            try
            {
                BsonSerializer.RegisterSerializer(typeof(DateTime), serializer);
            }
            catch (Exception)
            {
                // Swallow that exception like you're making $2.
            }
        }
    }
}
