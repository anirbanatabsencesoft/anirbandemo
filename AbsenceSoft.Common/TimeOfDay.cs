﻿using AbsenceSoft.Common.Serializers;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Text;

namespace AbsenceSoft.Common
{
    [Serializable]
    [BsonSerializer(typeof(TimeOfDaySerializer))]
    public struct TimeOfDay
    {
        /// <summary>
        /// The _ hour
        /// </summary>
        [BsonIgnore]
        private int _Hour;

        /// <summary>
        /// The _ minutes
        /// </summary>
        [BsonIgnore]
        private int _Minutes;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOfDay"/> struct.
        /// </summary>
        /// <param name="hour">The hour.</param>
        /// <param name="minutes">The minutes.</param>
        public TimeOfDay(int hour, int minutes)
        {
            _Hour = hour;
            _Minutes = minutes;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOfDay"/> struct.
        /// </summary>
        /// <param name="hour">The hour.</param>
        /// <param name="minutes">The minutes.</param>
        /// <param name="pm">if set to <c>true</c> [pm].</param>
        public TimeOfDay(int hour, int minutes, bool pm)
        {
            _Minutes = minutes;
            _Hour = hour;
            PM = pm;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOfDay"/> struct.
        /// </summary>
        /// <param name="time">The time.</param>
        public TimeOfDay(string time)
        {
            _Hour = 0;
            _Minutes = 0;
            if (string.IsNullOrWhiteSpace(time))
                return;
            string[] timeParts = time.Split(':');
            if (timeParts.Length == 2)
            {
                int h, m;
                if (int.TryParse(timeParts[0], out h))
                    _Hour = h;
                string[] mParts = timeParts[1].Split(' ');
                if (int.TryParse(mParts[0], out m))
                    _Minutes = m;

                if (mParts.Length > 1 && !string.IsNullOrWhiteSpace(mParts[1]) && mParts[1].Equals("PM", StringComparison.CurrentCultureIgnoreCase))
                    _Hour = _Hour >= 12 ? _Hour : _Hour + 12;
            }
        }

        /// <summary>
        /// Gets or sets the hour.
        /// </summary>
        /// <value>
        /// The hour.
        /// </value>
        public int Hour { get { return _Hour; } set { _Hour = value; } }

        /// <summary>
        /// Gets or sets the minutes.
        /// </summary>
        /// <value>
        /// The minutes.
        /// </value>
        public int Minutes { get { return _Minutes; } set { _Minutes = value; } }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TimeOfDay"/> is pm.
        /// </summary>
        /// <value>
        ///   <c>true</c> if pm; otherwise, <c>false</c>.
        /// </value>
        public bool PM { get { return _Hour >= 12; } set { _Hour = _Hour >= 12 ? _Hour : _Hour + 12; } }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder time = new StringBuilder();
            int h = _Hour;
            if (h > 12)
                h = h - 12;
            return string.Concat(h.ToString().PadLeft(2, '0'), ":", _Minutes.ToString().PadLeft(2, '0'), " ", _Hour >= 12 ? "PM" : "AM");
        }

        /// <summary>
        /// Gets a default <see cref="TimeOfDay"/>.
        /// </summary>
        /// <value>
        /// A default <see cref="TimeOfDay"/>.
        /// </value>
        public static TimeOfDay Default { get { return new TimeOfDay(0, 0); } }

        /// <summary>
        /// Parses the specified time.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        public static TimeOfDay Parse(string time)
        {
            return new TimeOfDay(time);
        }

        /// <summary>
        /// Tries to parse the time into a <see cref="TimeOfDay"/>. If unparsable then <paramref name="target"/>
        /// is set to <c>TimeOfDay.Default</c>.
        /// </summary>
        /// <param name="time">The time to parse.</param>
        /// <param name="target">The target <see cref="TimeOfDay"/> to set if parsed successfully.</param>
        /// <returns><c>true</c> if parsed and set successfully; otherwise <c>false</c>.</returns>
        public static bool TryParse(string time, out TimeOfDay target)
        {
            target = TimeOfDay.Default;
            if (string.IsNullOrWhiteSpace(time))
                return false;
            try
            {
                target = new TimeOfDay(time);
                return true;
            }
            catch { }

            return false;
        }
    }
}
