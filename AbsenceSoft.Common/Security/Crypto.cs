﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Globalization;
using AbsenceSoft.Common.Properties;

namespace AbsenceSoft.Common.Security
{
    public static class Crypto
    {
        /// <summary>
        /// Encrypts the specified plain text.
        /// </summary>
        /// <param name="plain">The plain text.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Certificate public key not found</exception>
        public static string Encrypt(this string plain)
        {
            if (string.IsNullOrWhiteSpace(plain))
                return plain;

            X509Certificate2 cert = GetEncryptionCert();
            using (RSACryptoServiceProvider provider = (RSACryptoServiceProvider)cert.PublicKey.Key)
            {
                if (provider == null)
                    throw new ApplicationException("Certificate public key not found");
                byte[] cipherData = provider.Encrypt(Encoding.UTF8.GetBytes(plain), true);
                return Convert.ToBase64String(cipherData);
            }
        }

        /// <summary>
        /// Decrypts the specified cipher.
        /// </summary>
        /// <param name="cipher">The cipher.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Certificate private key not found or is not exportable</exception>
        public static string Decrypt(this string cipher)
        {
            if (string.IsNullOrWhiteSpace(cipher))
                return cipher;

            X509Certificate2 cert = GetEncryptionCert();
            using (RSACryptoServiceProvider provider = (RSACryptoServiceProvider)cert.PrivateKey)
            {
                if (provider == null)
                    throw new ApplicationException("Certificate private key not found or is not exportable");
                byte[] plainData = provider.Decrypt(Convert.FromBase64String(cipher), true);
                return Encoding.UTF8.GetString(plainData);
            }
        }

        /// <summary>
        /// Hashes the specified plain text.
        /// </summary>
        /// <param name="plain">The plain text.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Certificate private key not found or is not exportable</exception>
        public static string Sha1Hash(this string plain)
        {
            if (string.IsNullOrWhiteSpace(plain))
                return plain;

            byte[] hashValue;
            using (SHA1Managed hash = new SHA1Managed())
                hashValue = hash.ComputeHash(Encoding.UTF8.GetBytes(plain));

            StringBuilder data = new StringBuilder();
            foreach (byte hexdigit in hashValue)
                data.Append(hexdigit.ToString("X2", CultureInfo.InvariantCulture.NumberFormat));
            return data.ToString();
        }

        /// <summary>
        /// Hash with Sha256
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Sha256Hash(this string plain)
        {
            if (string.IsNullOrWhiteSpace(plain))
                return plain;

            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(plain));

                // Convert byte array to a string   
                StringBuilder data = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    data.Append(bytes[i].ToString("x2"));
                }
                return data.ToString();
            }
        }

        /// <summary>
        /// Compares the hash.
        /// </summary>
        /// <param name="hash">The hash.</param>
        /// <param name="plain">The plain.</param>
        /// <returns></returns>
        public static bool CompareHash(this string hash, string plain)
        {
            if (plain.Sha1Hash() == hash)
            {
                return true;
            }

            if (plain.Sha256Hash() == hash)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the cert.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">Encryption certificate not found</exception>
        private static X509Certificate2 GetEncryptionCert()
        {
            var cert = GetCertBySubjectName(Settings.Default.x509CertSubjectName);
            if (cert != null)
                return cert;
            Log.Error("Encryption certificate not found");
            throw new ApplicationException("Encryption certificate not found");
        }

        /// <summary>
        /// Gets the cert by it's thumbprint.
        /// </summary>
        /// <param name="thumbprint">The thumbprint.</param>
        /// <returns></returns>
        public static X509Certificate2 GetCertByThumbprint(string thumbprint)
        {
            X509Store store = null;
            try
            {
                store = new X509Store(Settings.Default.x509CertStoreLocation);
                store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certCollection = store.Certificates;

                X509Certificate2Collection certColl = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);

                if (certColl.Count > 0)
                    return certColl[0];
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error getting certificate by thumbprint, {0}", thumbprint), ex);
            }
            finally
            {
                if (store != null)
                    store.Close();
            }
            return null;
        }

        /// <summary>
        /// Gets the cert by subject name.
        /// </summary>
        /// <param name="subject">The subject name.</param>
        /// <returns></returns>
        public static X509Certificate2 GetCertBySubjectName(string subject)
        {
            X509Store store = null;
            try
            {
                store = new X509Store(Settings.Default.x509CertStoreLocation);
                store.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certCollection = store.Certificates;

                X509Certificate2Collection certColl = store.Certificates.Find(X509FindType.FindBySubjectName, subject, false);

                if (certColl.Count > 0)
                    return certColl[0];
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error getting certificate by subject name, {0}", subject), ex);
            }
            finally
            {
                if (store != null)
                    store.Close();
            }
            return null;
        }
    }
}
