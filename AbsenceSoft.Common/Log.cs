﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    /// <summary>
    /// Logger for all that log to the logs for logging.
    /// </summary>
    public static class Log
    {
        private static log4net.ILog _logger;

        /// <summary>
        /// Initializes the <see cref="Log"/> class.
        /// </summary>
        static Log()
        {
            _logger = log4net.LogManager.GetLogger(typeof(Log));
        }

        /// <summary>
        /// Errors the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Error(object msg)
        {
            _logger.Error(msg);
        }

        /// <summary>
        /// Log a formatted error message
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void Error(string msg, object[] args)
        {
            _logger.ErrorFormat(msg, args);
        }

        /// <summary>
        /// Log a formatted error message
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void Error(Exception ex, string msg, params object[] args)
        {
            _logger.Error(string.Format(msg, args), ex);
        }

        /// <summary>
        /// Logs an error to mongoDB
        /// </summary>
        /// <param name="msg">The message to log.</param>
        /// <param name="ex"></param>
        public static void Error(object msg, Exception ex)
        {
            _logger.Error(msg, ex);
        }

        /// <summary>
        /// Errors the specified ex.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void Error(Exception ex)
        {
            _logger.Error(ex.Message, ex);
        }

        /// <summary>
        /// Warns the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Warn(object msg)
        {
            _logger.Warn(msg);
        }

        /// <summary>
        /// Warns the specified format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void Warn(string format, params object[] args)
        {
            _logger.Warn(string.Format(format, args));
        }

        /// <summary>
        /// Informations the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Info(object msg)
        {
            _logger.Info(msg);
        }

        /// <summary>
        /// Informations the specified format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void Info(string format, params object[] args)
        {
            _logger.Info(string.Format(format, args));
        }

        /// <summary>
        /// Logs as it is
        /// </summary>
        /// <param name="message"></param>
        public static void Info(string message)
        {
            _logger.Info(message);
        }

        /// <summary>
        /// Debugs the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public static void Debug(object msg)
        {
            _logger.Debug(msg);
        }

        /// <summary>
        /// Debugs the specified format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        public static void Debug(string format, params object[] args)
        {
            _logger.Debug(string.Format(format, args));
        }
    }
}
