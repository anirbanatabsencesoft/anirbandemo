﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    public class BasicPrincipal : IPrincipal
    {
        public IIdentity Identity { get; set; }

        public bool IsInRole(string role)
        {
            return Roles != null && Roles.Any(r => r == role);
        }

        public IEnumerable<string> Roles { get; set; }
    }
}
