﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    public static class Enums<TEnum> where TEnum : struct, IConvertible
    {
        /// <summary>
        /// Initializes the <see cref="Enums{TEnum}"/> class.
        /// </summary>
        static Enums()
        {
            ValidateTEnumType();
        }

        /// <summary>
        /// Validates the type of the t is indeed an enum.
        /// </summary>
        /// <exception cref="System.ArgumentException">T must be an enumerated type</exception>
        private static void ValidateTEnumType()
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("T must be an enumerated type");
        }

        /// <summary>
        /// Helper function to turn an enum into a select list
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="theEnum"></param>
        /// <returns></returns>
        public static Dictionary<string, object> GetOptions()
        {
            return Enum.GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .ToDictionary(p => p.ToString(), p => (object)p.ToString().SplitCamelCaseString());
        }

        /// <summary>
        /// Gets the character options.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, char> GetCharOptions()
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            Dictionary<string, char> enumOptions = new Dictionary<string, char>();
            Enum.GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .ForEach(p => enumOptions.Add((Convert.ToInt32(p)).ToString(), (char)Convert.ToInt32(p)));

            return enumOptions;
        }

        /// Helper function to turn an enum into a select list
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="theEnum"></param>
        /// <returns></returns>
        public static Dictionary<string, object> GetEnumOptions()
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            Dictionary<string, object> enumOptions = new Dictionary<string, object>();
            Enum.GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .ForEach(p => enumOptions.Add((Convert.ToInt32(p)).ToString(), p.ToString().SplitCamelCaseString()));

            return enumOptions;
        }
    }
}
