﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Configuration;

namespace AbsenceSoft
{
    /// <summary>
    /// Provides extension methods for dates.
    /// </summary>
    public static class DateExtensions
    {
        static string timeZoneId = ConfigurationManager.AppSettings["TimeZoneId"] ?? "Mountain Standard Time";
        /// <summary>
        /// To the UI string.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string ToUIString(this DateTime date)
        {
            if (date == DateTime.MaxValue || date == DateTime.MinValue || date == default(DateTime))
                return string.Empty;
            return date.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// To the UI string.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string ToUIString(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToUIString();
            return string.Empty;
        }

        /// <summary>
        /// To the UI string.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="asNull">As null string value to return.</param>
        /// <returns></returns>
        public static string ToUIString(this DateTime date, string asNull)
        {
            if (date == DateTime.MaxValue || date == DateTime.MinValue || date == default(DateTime))
                return asNull ?? string.Empty;
            return date.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// To the UI string.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="asNull">As null string value to return.</param>
        /// <returns></returns>
        public static string ToUIString(this DateTime? date, string asNull)
        {
            if (date.HasValue)
                return date.Value.ToUIString(asNull);
            return asNull ?? string.Empty;
        }

        /// <summary>
        /// Returns the first day of the week based on the specifed first day
        /// </summary>
        /// <param name="dayInWeek">The day in week.</param>
        /// <param name="firstDay">The first day.</param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfWeek(this DateTime dayInWeek, DayOfWeek? firstDay)
        {
            if (!firstDay.HasValue)
                firstDay = DayOfWeek.Sunday;

            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        /// <summary>
        /// Gets the first date of the week of the specified date for the current culture. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInWeek">The date this method extends.</param>
        /// <returns>A date representing the first day of the week for the week of the specified date.</returns>
        public static DateTime GetFirstDayOfWeek(this DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
            return dayInWeek.GetFirstDayOfWeek(defaultCultureInfo);
        }//GetFirstDayOfWeek

        /// <summary>
        /// Gets the first date of the week of the specified date for the specified culture. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInWeek">The date this method extends.</param>
        /// <param name="cultureInfo">The culture to determine the start of the week.</param>
        /// <returns>A date representing the first day of the week for the week of the specified date.</returns>
        public static DateTime GetFirstDayOfWeek(this DateTime dayInWeek, CultureInfo cultureInfo)
        {
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            return dayInWeek.GetFirstDayOfWeek(firstDay);
        }//GetFirstDayOfWeek

        /// <summary>
        /// Gets the last day of week of the specified date for the current culture. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInWeek">The date this method extends.</param>
        /// <returns>A date representing the last day of the week for the week of the specified date.</returns>
        public static DateTime GetLastDayOfWeek(this DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
            return dayInWeek.GetLastDayOfWeek(defaultCultureInfo);
        }

        /// <summary>
        /// Gets the last day of week of the specified date for the specified culture. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInWeek">The date this method extends.</param>
        /// <param name="cultureInfo">The culture to determine the end of the week.</param>
        /// <returns>A date representing the last day of the week for the week of the specified date and culture.</returns>
        public static DateTime GetLastDayOfWeek(this DateTime dayInWeek, CultureInfo cultureInfo)
        {
            return dayInWeek.GetFirstDayOfWeek(cultureInfo).AddDays(6);
        }

        /// <summary>
        /// Returns the first day of the week based on the specifed first day
        /// </summary>
        /// <param name="dayInWeek">The date this method extends.</param>
        /// <param name="firstDay">The first day of the week.</param>
        /// <returns></returns>
        public static DateTime GetLastDayOfWeek(this DateTime dayInWeek, DayOfWeek? firstDay)
        {
            return dayInWeek.GetFirstDayOfWeek(firstDay).AddDays(6);
        }

        /// <summary>
        /// Gets the first day of the month of the specified date this method extends. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInMonth">The date this method extends.</param>
        /// <returns>A date representing the first day of the month of the specified date.</returns>
        public static DateTime GetFirstDayOfMonth(this DateTime dayInMonth)
        {
            return new DateTime(dayInMonth.Year, dayInMonth.Month, 1,
                dayInMonth.Hour, dayInMonth.Minute, dayInMonth.Second, dayInMonth.Millisecond, dayInMonth.Kind);
        }//GetFirstDayOfMonth

        /// <summary>
        /// Gets the last day of the month of the specified date this method extends. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInMonth">The date this method extends.</param>
        /// <returns>A date representing the last day of the month of the specified date.</returns>
        public static DateTime GetLastDayOfMonth(this DateTime dayInMonth)
        {
            return new DateTime(dayInMonth.Year, dayInMonth.Month, DateTime.DaysInMonth(dayInMonth.Year, dayInMonth.Month),
                dayInMonth.Hour, dayInMonth.Minute, dayInMonth.Second, dayInMonth.Millisecond, dayInMonth.Kind);
        }//GetLastDayOfMonth

        /// <summary>
        /// Gets the first day of the year for the specified date this method extends. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInYear">The date this method extends.</param>
        /// <returns>A date representing the first day of the year of the specified date.</returns>
        public static DateTime GetFirstDayOfYear(this DateTime dayInYear)
        {
            return new DateTime(dayInYear.Year, 1, 1,
                dayInYear.Hour, dayInYear.Minute, dayInYear.Second, dayInYear.Millisecond, dayInYear.Kind);
        }//GetFirstDayOfYear

        /// <summary>
        /// Gets the last day of the year for the specified date this method extends. Copies the time of day and kind from
        /// the source date.
        /// </summary>
        /// <param name="dayInYear">The date this method extends.</param>
        /// <returns>A date representing the last day of the year of the specified date.</returns>
        public static DateTime GetLastDayOfYear(this DateTime dayInYear)
        {
            return new DateTime(dayInYear.Year, 12, DateTime.DaysInMonth(dayInYear.Year, 12),
                dayInYear.Hour, dayInYear.Minute, dayInYear.Second, dayInYear.Millisecond, dayInYear.Kind);
        }//GetLastDayOfYear


        /// <summary>
        /// Calculate the age in years for today's date
        /// </summary>
        /// <param name="dob"></param>
        /// <param name="targetDate"></param>
        /// <returns>The age in years, -1 if TargetDate is &lt;= dob </returns>
        public static int AgeInYears(this DateTime dob)
        {
            return dob.AgeInYears(DateTime.Today);
        }

        /// <summary>
        /// Calculate the age in years for a date to a target date
        /// </summary>
        /// <param name="dob"></param>
        /// <param name="targetDate"></param>
        /// <returns>The age in years, -1 if TargetDate is &lt;= dob </returns>
        public static int AgeInYears(this DateTime dob, DateTime TargetDate)
        {
            int rtVal = -1;

            // this should work for leap years
            if (dob < TargetDate)
            {
                rtVal = TargetDate.Year - dob.Year;
                if (TargetDate.Month < dob.Month || (TargetDate.Month == dob.Month && TargetDate.Day < dob.Day))
                    rtVal--;
            }

            return rtVal;
        }

        /// <summary>
        /// Alls the years in the specified range. If the end date is <c>null</c> then
        /// only the start date's year is returned.
        /// </summary>
        /// <param name="startDate">The start date (inclusive).</param>
        /// <param name="endDate">The end date (inclusive).</param>
        /// <returns></returns>
        public static List<int> AllYearsInRange(this DateTime startDate, DateTime? endDate)
        {
            if (!endDate.HasValue)
                return new List<int>(1) { startDate.Year };

            DateTime s = startDate;
            DateTime e = endDate.Value;
            if (s > e)
            {
                s = endDate.Value;
                e = startDate;
            }
            if (s == e)
                return new List<int>(1) { startDate.Year };
            if (s.Year == e.Year)
                return new List<int>(1) { startDate.Year };

            List<int> years = new List<int>((endDate.Value.Year - startDate.Year) + 1);
            int cur = startDate.Year;
            while (cur <= endDate.Value.Year)
            {
                years.Add(cur);
                cur++;
            }

            return years.Distinct().OrderBy(y => y).ToList();
        }

        /// <summary>
        /// Test if two date ranges overlap. 
        /// If an end date is null it is defaulted to DateTime.MaxValue
        /// 
        /// Dates are inclusive, It feels a bit weird having this as an extension
        /// but we would need some sort of singleton date function object instead so I am 
        /// making due
        /// </summary>
        /// <param name="thisStartDate">The start date to be tested</param>
        /// <param name="thisEndDate">The end date to be tested</param>
        /// <param name="inRangeStartDate">Start date of the range to test against</param>
        /// <param name="inRangeEndDate">The end date of the range to test against</param>
        /// <returns></returns>
        public static bool DateRangesOverLap(this DateTime thisStartDate, DateTime? thisEndDate, DateTime inRangeStartDate, DateTime? inRangeEndDate)
        {
            // unnull everthing
            DateTime ted = thisEndDate ?? DateTime.MaxValue;
            DateTime ired = inRangeEndDate ?? DateTime.MaxValue;

            return thisStartDate.DateInRange(inRangeStartDate, ired) ||
                ted.DateInRange(inRangeStartDate, ired) ||
                inRangeStartDate.DateInRange(thisStartDate, ted) ||
                ired.DateInRange(thisStartDate, ted);

            /*
            // if the source start is between the start and end of the target, or the end date is between the start and
            // end of the target then they overlap
            return (thisStartDate >= inRangeStartDate && thisStartDate < ired) ||
                    (ted >= inRangeStartDate && ted <= ired);
            */
        }

        /// <summary>
        /// Compare the date to a range and return true if the date
        /// is between the two dates (inclusive)
        /// </summary>
        /// <param name="theDate">The date to check</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End Date</param>
        /// <returns>true if the date is in the range (inclusive)</returns>
        public static bool DateInRange(this DateTime theDate, DateTime startDate, DateTime? endDate)
        {
            return theDate.DateInRange(startDate, endDate ?? DateTime.MaxValue);
        }

        /// <summary>
        /// Compare the date to a range and return true if the date
        /// is between the two dates (inclusive)
        /// </summary>
        /// <param name="theDate">The date to check</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End Date</param>
        /// <returns>true if the date is in the range (inclusive)</returns>
        public static bool DateInRange(this DateTime theDate, DateTime startDate, DateTime endDate)
        {
            return theDate >= startDate && theDate <= endDate;
        }

        /// <summary>
        /// Return a new date object that forces this date to at midnight.
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        public static DateTime ToMidnight(this DateTime theDate)
        {
            return new DateTime(theDate.Year, theDate.Month, theDate.Day, 0, 0, 0, DateTimeKind.Utc);
        }

        public static DateTime EndOfDay(this DateTime theDate)
        {
            return theDate.ToMidnight().AddDays(1).AddMilliseconds(-1);
        }
        /// <summary>
        /// Return a new date object that forces this date to at midnight.
        /// </summary>
        /// <param name="theDate"></param>
        /// <returns></returns>
        public static DateTime? ToMidnight(this DateTime? theDate)
        {
            if (theDate == null) return null;
            return new DateTime(theDate.Value.Year, theDate.Value.Month, theDate.Value.Day, 0, 0, 0, DateTimeKind.Utc);
        }
        /// <summary>
        /// Gets the same date, except for leap years, replaces the 29th with the 28th for that same year.
        /// </summary>
        /// <param name="theDate">The date this method extends.</param>
        /// <returns>The same date if not a leap year or is not Feb 29, otherwise Feb 28 of that same year.</returns>
        public static DateTime NonLeapYearDate(this DateTime theDate)
        {
            if (DateTime.IsLeapYear(theDate.Year) && theDate.Month == 2 && theDate.Day == 29)
                return new DateTime(theDate.Year, 2, 28, theDate.Hour, theDate.Minute, theDate.Second, theDate.Kind);
            return theDate;
        }

        /// <summary>
        /// Gets the same date, except for leap years, excatly n years in the past, replacing any instance of Feb 29th with the 28th.
        /// </summary>
        /// <param name="theDate">The date this method extends.</param>
        /// <returns>The same date, excatly n years in the past, if not a leap year or is not Feb 29, otherwise Feb 28 of n years ago.</returns>
        public static DateTime SameDayNYearsAgo(this DateTime theDate, int yearsAgo)
        {
            DateTime safeDate = theDate.NonLeapYearDate();
            return new DateTime(safeDate.Year - yearsAgo, safeDate.Month, safeDate.Day, safeDate.Hour, safeDate.Minute, safeDate.Second, safeDate.Kind);
        }

        /// <summary>
        /// Gets the same date, except for leap years, excatly 1 year in the past, replacing any instance of Feb 29th with the 28th.
        /// </summary>
        /// <param name="theDate">The date this method extends.</param>
        /// <returns>The same date, excatly 1 year in the past, if not a leap year or is not Feb 29, otherwise Feb 28 of that prior year.</returns>
        public static DateTime SameDayLastYear(this DateTime theDate)
        {
            return theDate.SameDayNYearsAgo(1);
        }

        /// <summary>
        /// Gets a UI friendly time based on total minutes. E.g. 80 minues = "1h 20m".
        /// </summary>
        /// <param name="minutes">The minutes to convert to a friendly time string that this method extends</param>
        /// <returns>A friendly time string based on total minutes.</returns>
        public static string ToFriendlyTime(this int minutes)
        {
            if (minutes == 0)
                return "0h";

            var hr = Convert.ToInt32(Math.Floor((decimal)(minutes / 60)));
            var min = minutes % 60;
            if (hr == 0)
                return string.Concat(min, "m");
            else if (min == 0)
                return string.Concat(hr, "h");
            return string.Format("{0}h {1}m", hr, min);
        }
        /// <summary>
        /// Gets a UI friendly time based on total minutes. E.g. 80 minues = "1h 20m".
        /// </summary>
        /// <param name="minutes">The minutes to convert to a friendly time string that this method extends</param>
        /// <returns>A friendly time string based on total minutes.</returns>
        public static string ToFriendlyTime(this double minutes) { return Convert.ToInt32(minutes).ToFriendlyTime(); }

        /// <summary>
        /// Gets a really UI friendly time for a timespan instead of minutes, for more precise durations and stuff.
        /// </summary>
        /// <param name="ts">The timespan to convert to a really friendly time string that this method extends</param>
        /// <returns>A really friendly time string based on total elapsed time.</returns>
        public static string ToReallyFriendlyTime(this TimeSpan ts)
        {
            if (ts.TotalSeconds == 0)
                return "0s";

            var hr = Convert.ToInt32(Math.Floor(ts.TotalHours));
            var min = ts.Minutes;
            var sec = ts.Seconds;
            StringBuilder time = new StringBuilder();
            if (hr > 0)
                time.AppendFormat("{0}h", hr);
            if (min > 0)
                time.AppendFormat("{0}{1}m", time.Length > 0 ? " " : "", min);
            if (sec > 0)
                time.AppendFormat("{0}{1}s", time.Length > 0 ? " " : "", sec);
            return time.ToString();
        } // ToReallyFriendlyTime

        /// <summary>
        /// Parses a friendly time (e.g. "1h 20m") to a total number of minutes (e.g. 80).
        /// </summary>
        /// <param name="friendlyTime">The friendly time this method extends.</param>
        /// <returns>The total minutes the friendly time span represents</returns>
        /// <exception cref="System.ArgumentNullException">friendlyTime</exception>
        public static int ParseFriendlyTime(this string friendlyTime)
        {
            if (string.IsNullOrWhiteSpace(friendlyTime))
                throw new ArgumentNullException("friendlyTime");

            string[] parts = friendlyTime.Split(' ');
            int minutes = 0;
            foreach (var part in parts)
            {
                if (part.EndsWith("h"))
                    minutes += (int.Parse(part.Replace("h", "")) * 60);
                if (part.EndsWith("m"))
                    minutes += int.Parse(part.Replace("m", ""));
            }

            return minutes;
        }

        /// <summary>
        /// Gets the years.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static IEnumerable<int> GetYears(this DateTime startDate, DateTime endDate)
        {

            DateTime testDate = startDate;
            while (testDate <= endDate)
            {
                yield return testDate.Year;
                testDate = testDate.AddYears(1);
            }
        }

        /// <summary>
        /// Gets the years.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static IEnumerable<int> GetYears(this DateTime startDate, DateTime? endDate)
        {
            DateTime testDate = startDate;
            while (testDate < (endDate ?? startDate.AddYears(1)))
            {
                yield return testDate.Year;
                testDate = testDate.AddYears(1);
            }
        }

        /// <summary>
        /// Wrapper for DateTime.IsLeapYear because I want to save myself some
        /// typing
        /// </summary>
        /// <param name="dt">The Date</param>
        /// <returns>True if a leap year</returns>
        public static bool IsLeapYear(this DateTime dt)
        {
            return DateTime.IsLeapYear(dt.Year);
        }

        /// <summary>
        /// Wrapper for DateTime.IsLeapYear because I want to save myself some
        /// typing
        /// </summary>
        /// <param name="dt">The Date</param>
        /// <returns>True if a leap year</returns>
        public static bool IsLeapYear(this DateTime? dt)
        {
            if (dt.HasValue)
                return dt.IsLeapYear();

            return false;
        }

        /// <summary>
        ///Checks for a value on the nullable datetime before formatting as a string
        /// </summary>
        /// <param name="theNullableDate">The Date</param>
        /// <returns>If the data is not null, returns a the formatted date else just returns null</returns>
        public static string ToString(this DateTime? theNullableDate, string format)
        {
            if (theNullableDate.HasValue)
                return theNullableDate.Value.ToString(format);

            return null;
        }

        /// <summary>
        /// Return whichever datetime is greater
        /// </summary>
        /// <param name="to"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static DateTime Max(this DateTime to, DateTime compareTo)
        {
            if (to > compareTo)
                return to;
            else
                return compareTo;
        }

        /// <summary>
        /// return whichever datetime is smaller
        /// </summary>
        /// <param name="to"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static DateTime Min(this DateTime to, DateTime compareTo)
        {
            if (to < compareTo)
                return to;
            else
                return compareTo;
        }

        /// <summary>
        /// Gets the total number of days the in year for the specified sample date.
        /// </summary>
        /// <param name="sampleDate">The sample date.</param>
        /// <returns></returns>
        public static int DaysInYear(this DateTime sampleDate)
        {
            return Convert.ToInt32(Math.Ceiling((sampleDate.GetLastDayOfYear() - sampleDate.GetFirstDayOfYear()).TotalDays));
        }

        /// <summary>
        /// Returns a list of all the dates in a month
        /// </summary>
        /// <param name="sampleDate"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> AllDatesInMonth(this DateTime sampleDate)
        {
            int days = DateTime.DaysInMonth(sampleDate.Year, sampleDate.Month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(sampleDate.Year, sampleDate.Month, day);
            }
        }

        /// <summary>
        /// Determines whether this instance is a weekday.
        /// </summary>
        /// <param name="sampleDate">The sample date.</param>
        /// <returns></returns>
        public static bool IsWeekday(this DateTime sampleDate)
        {
            return !sampleDate.IsWeekend();
        }

        /// <summary>
        /// Determines whether this instance is a weekend.
        /// </summary>
        /// <param name="sampleDate">The sample date.</param>
        /// <returns></returns>
        public static bool IsWeekend(this DateTime sampleDate)
        {
            return sampleDate.DayOfWeek == DayOfWeek.Saturday || sampleDate.DayOfWeek == DayOfWeek.Sunday;
        }

        /// <summary>
        /// Gets the nearest weekday to the current date.
        /// </summary>
        /// <param name="sampleDate">The sample date.</param>
        /// <returns></returns>
        public static DateTime ClosestWeekday(this DateTime sampleDate)
        {
            if (sampleDate.IsWeekday())
                return sampleDate;

            if (sampleDate.DayOfWeek == DayOfWeek.Saturday)
                return sampleDate.AddDays(-1);

            return sampleDate.AddDays(1);
        }

        /// <summary>
        /// Gets all of the dates in range between the 2 dates.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>A list of dates between the 2 dates, inclusive of the start and end date.</returns>
        public static List<DateTime> AllDatesInRange(this DateTime startDate, DateTime endDate)
        {
            if (endDate == startDate)
                return new List<DateTime>(1) { startDate.ToMidnight() };
            DateTime realStart = startDate.ToMidnight();
            DateTime realEnd = endDate.ToMidnight();
            if (endDate < startDate)
            {
                realStart = endDate;
                realEnd = startDate;
            }

            List<DateTime> dates = new List<DateTime>();

            while (startDate <= endDate)
                startDate = dates.AddFluid(startDate).AddDays(1);

            return dates;
        }

        /// <summary>
        /// Converts the date to the start date of a new DateRange prompting for the end date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public static DateRange AsRange(this DateTime startDate, DateTime? endDate)
        {
            return new DateRange(startDate, endDate);
        }

        /// <summary>
        /// Gets the fiscal quarter for the passed in date.
        /// </summary>
        /// <param name="date">The date to get the fiscal quarter for.</param>
        /// <returns>The fiscal quarter for the date this method extends.</returns>
        public static int? GetFiscalQuarter(this DateTime? date)
        {
            if (!date.HasValue)
                return null;

            return date.Value.GetFiscalQuarter();
        }

        /// <summary>
        /// Gets the fiscal quarter for the passed in date.
        /// </summary>
        /// <param name="date">The date to get the fiscal quarter for, or <c>null</c>.</param>
        /// <returns>The fiscal quarter for the date this method extends.</returns>
        public static int GetFiscalQuarter(this DateTime date)
        {
            return (int)Math.Ceiling(date.Month / 3.0);
        }

        /// <summary>
        /// Gets the week of the year from 1 to 52 for the passed in date.
        /// </summary>
        /// <param name="date">The date to get the week number for, or <c>null</c>.</param>
        /// <returns>The week of the year from 1 to 52 for the date this method extends.</returns>
        public static int? GetIso8601WeekOfYear(this DateTime? date)
        {
            if (!date.HasValue)
                return null;

            return date.Value.GetIso8601WeekOfYear();
        }

        /// <summary>
        /// Gets the week of the year from 1 to 52 for the passed in date.
        /// </summary>
        /// <param name="date">The date to get the week number for.</param>
        /// <returns>The week of the year from 1 to 52 for the date this method extends.</returns>
        public static int GetIso8601WeekOfYear(this DateTime date)
        {
            //var thursday = date.AddDays(3 - ((int)date.DayOfWeek + 6) % 7);
            //return 1 + (thursday.DayOfYear - 1) / 7;

            var cal = CultureInfo.InvariantCulture.Calendar;

            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = cal.GetDayOfWeek(date);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
                date = date.AddDays(3);

            // Return the week of our adjusted day
            return cal.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static double GetMonthDiff(this DateTime fromDate, DateTime toDate)
        {
            int wholeMonthsDiff = ((toDate.Year * 12) + toDate.Month) - ((fromDate.Year * 12) + fromDate.Month);
            if (toDate.Day < fromDate.Day)
                wholeMonthsDiff--;
            var diff = toDate - fromDate.AddMonths(wholeMonthsDiff);
            return Math.Abs(wholeMonthsDiff + (diff.Days / 30d));
        }


    

        public static DateTime ToLocalDateTime(this DateTime dt)
        {
            // dt.DateTimeKind should be Utc!
            var tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);          
            DateTime utc = new DateTime(dt.Year, dt.Month, dt.Day, 12, 0, 0);            
            DateTime spacificTime = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
            return spacificTime;           
        }

        public static DateTime ToUtcDateTime(this DateTime dt)
        {
            var tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            return TimeZoneInfo.ConvertTimeToUtc(dt, tzi);
        }

        public static DateTime RoundDownDateTime(this DateTime dateTime, int minutes)
        {
            return new DateTime(dateTime.Year, dateTime.Month,
                 dateTime.Day, dateTime.Hour, (dateTime.Minute / minutes) * minutes, 0);
        }

        public static DateTime AddWeeks(this DateTime dateTime, int count)
        {
            return dateTime.AddDays(7 * count);
        }

        public static string ToHours(this double minutes)
        {
            return string.Format("{0} hours", Math.Round(minutes / 60, 1));
        }

        /// <summary>
        /// Test if two date ranges overlap for certification details. 
        /// If an end date is null it is defaulted to DateTime.MaxValue
        /// 
        /// Dates are inclusive, It feels a bit weird having this as an extension
        /// but we would need some sort of singleton date function object instead so I am 
        /// making due
        /// </summary>
        /// <param name="thisStartDate">The start date to be tested</param>
        /// <param name="thisEndDate">The end date to be tested</param>
        /// <param name="inRangeStartDate">Start date of the range to test against</param>
        /// <param name="inRangeEndDate">The end date of the range to test against</param>
        /// <returns></returns>
        public static bool CertificateDateRangesOverLap(this DateTime thisStartDate, DateTime? thisEndDate, DateTime inRangeStartDate, DateTime? inRangeEndDate)
        {
            // unnull everthing
            DateTime ted = thisEndDate ?? DateTime.MaxValue;
            DateTime ired = inRangeEndDate ?? DateTime.MaxValue;

            return thisStartDate.DateInRange(inRangeStartDate, ired) ||
                ted.DateInRange(inRangeStartDate, ired);
        }

        public static DateTime AddWeeks(this DateTime dateTime, double count)
        {
            return dateTime.AddDays(7 * count);
        }

        public static bool IsSameTimeAs(this DateTime referenceDate, DateTime compareDate, long allowedDeltaInMilliSeconds = 0)
        {
            if (referenceDate.Kind != compareDate.Kind)
            {
                return false;
            }

            var difference = Math.Abs(referenceDate.Subtract(compareDate).TotalMilliseconds);

            return difference <= allowedDeltaInMilliSeconds;
        }

        public static DateTime IsSameDayAs(this DateTime referenceDate, DateTime compareDate, long allowedDeltaInMs = 0)
        {
            throw new NotImplementedException();
        }
    }
}
