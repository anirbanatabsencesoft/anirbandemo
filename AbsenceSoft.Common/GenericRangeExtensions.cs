﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    public static class GenericRangeExtensions
    {

        /// <summary>
        /// Ensure a value is forced to stay within a 
        /// given range.  If value exceeds min or max range, that
        /// min or max range will be used.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="min">The minumum value (inclusive)</param>
        /// <param name="max">The maximum value (inclusive)</param>
        /// <returns></returns>
        public static T Clamp<T>(this T value, T min, T max) where T : struct, IComparable
        {
            if(value.CompareTo(min) < 0)
            {
                return min;
            }
            if(value.CompareTo(max) > 0)
            {
                return max;
            }

            return value;
        }


        /// <summary>
        /// Determines if value is in the range of start and end (inclusive).
        /// </summary>
        /// <typeparam name="T">Type of value to test</typeparam>
        /// <param name="value">Value to test</param>
        /// <param name="start">Start of range (inclusive)</param>
        /// <param name="end">End of range (inclusive)</param>
        /// <returns>true if value is in range of start and end, false otherwise</returns>
        public static bool IsInRange<T>(this T value, T start, T end) where T : struct, IComparable
        {
            return value.CompareTo(start) >= 0 && value.CompareTo(end) <= 0;
        }

        /// <summary>
        /// Determines if value is in the range of start and end (inclusive).
        /// </summary>
        /// <typeparam name="T">Type of value to test</typeparam>
        /// <param name="value">Value to test</param>
        /// <param name="start">Start of range (inclusive)</param>
        /// <param name="end">End of range (inclusive)</param>
        /// <returns>true if value is in range of start and end, false otherwise</returns>
        public static bool IsInRange<T>(this Nullable<T> value, T start, T end) where T : struct, IComparable
        {
            if (value.HasValue)
            {
                return value.Value.IsInRange(start, end);
            }
            return false;
        }
    }
}
