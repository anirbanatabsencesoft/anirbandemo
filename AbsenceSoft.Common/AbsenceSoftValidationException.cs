﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    [Serializable]
    [ComVisible(true)]
    public class AbsenceSoftValidationException : AbsenceSoftException
    {
        public AbsenceSoftValidationException(string message, IEnumerable<ValidationResult> results) :
            base(message + ": " + Format(results))
        {
        }


        static string Format(IEnumerable<ValidationResult> results)
        {
            return string.Join("\n", results.Select(s => string.Format(
                "{0} ({1})",
                s.ErrorMessage,
                string.Join(",", s.MemberNames)
            )));
        }
    }
}
