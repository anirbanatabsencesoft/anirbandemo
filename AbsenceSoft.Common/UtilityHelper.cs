﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    /// <summary>
    /// All utility methods to be listed here
    /// </summary>
    public static class UtilityHelper
    {
        /// <summary>
        /// Append the missing JSON property with blank string
        /// </summary>
        /// <param name="arrayIn"></param>
        /// <returns></returns>
        public static JArray AppendMissingProperties(JArray arrayIn)
        {
            try
            {
                // find all distinct property names across all objects in the array
                List<string> names = new List<string>();
                foreach (JObject obj in arrayIn.Children<JObject>())
                {
                    foreach (JProperty prop in obj.Properties())
                    {
                        if(!names.Contains(prop.Name))
                            names.Add(prop.Name);
                    }
                }

                // copy objects to a new array, adding missing properties along the way
                JArray arrayOut = new JArray();
                foreach (JObject obj in arrayIn.Children<JObject>())
                {
                    JObject objOut = new JObject();
                    foreach (string name in names)
                    {
                        JToken val = obj[name];
                        if (val == null)
                        {
                            val = new JValue("");
                        }
                        objOut.Add(name, val);
                    }
                    arrayOut.Add(objOut);
                }

                return arrayOut;
            }
            catch
            {
                //on any error return the original array
                return arrayIn;
            }

        }

        /// <summary>
        /// Execute a synchronous menthod with timeout
        /// Added generics only for RunCalc now
        /// Use: ExecuteWithTimeout<Case, CaseChangeHandlers, Case>(s.RunCalcs, 300, c, handlers);
        /// We are expecting the task to be aborted in 5 mins i.e. 300 secs
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="func"></param>
        /// <param name="timeoutSeconds"></param>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static T3 ExecuteWithTimeout<T1, T2, T3>(Func<T1, T2, T3> func, int timeoutSeconds, T1 t1 = default(T1), T2 t2 = default(T2))
        {
            var source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            Thread threadToCancel = null;


            var t = Task.Factory.StartNew(() =>
            {
                threadToCancel = Thread.CurrentThread;
                return func(t1, t2);
            });

            Task.Factory.StartNew(() =>
            {
                while (threadToCancel != null)
                {
                    if (threadToCancel == null)
                        return;

                    if (token.IsCancellationRequested)
                    {
                        threadToCancel.Abort();
                        return;
                    }
                }
            });

            source.CancelAfter(timeoutSeconds * 1000);

            try

            {
                Boolean methodcompletedintime = t.Wait(timeoutSeconds * 1000);
                if (t != null && methodcompletedintime)
                {
                    return t.GetAwaiter().GetResult();
                }
                else if(methodcompletedintime == false)
                {
                    throw new Exception("Timeout Exception Occured");
                }

            }
            catch(ThreadAbortException)
            {
                //error may occur if there is timeout and the task is null/killed as thread aborted.
                //so trapping it.
            }

            return default(T3);
        }
    }
}
