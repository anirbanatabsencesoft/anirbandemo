﻿using AbsenceSoft.Common;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Text;
using AbsenceSoft.Common.Serializers;
using System.IO;
using MongoDB.Bson.Serialization;
using System.Xml;
using System.Dynamic;
using System.ComponentModel;

namespace AbsenceSoft
{
    public static class Extensions
    {
        /// <summary>
        /// Batches the specified collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="batchSize">Size of the batch.</param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> collection, int batchSize)
        {
            List<T> nextbatch = new List<T>(batchSize);
            foreach (T item in collection)
            {
                nextbatch.Add(item);
                if (nextbatch.Count == batchSize)
                {
                    yield return nextbatch;
                    nextbatch = new List<T>(batchSize);
                }
            }
            if (nextbatch.Count > 0)
                yield return nextbatch;
        }//Batch<T>


        /// <summary>
        /// Performs the specified action on each element of the System.Collections.Generic.IEnumerable of T.
        /// </summary>
        /// <typeparam name="T">The type of items contained in the collection</typeparam>
        /// <param name="collection">The IEnumerable collection this method extends</param>
        /// <param name="action">The System.Action of T delegate to perform on each element of the System.Collections.Generic.IEnumerable of T.</param>
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if (collection != null)
                foreach (var item in collection)
                    action(item);
        }//ForEach<T>

        /// <summary>
        /// Takes an object of type <c>T</c> that implements IDisposable
        /// and wraps an Action that takes a single parameter of the same
        /// instance of type <c>T</c> and properly calls the Dispose method
        /// afterwards. This is used simply for sytax sugar.
        /// </summary>
        /// <typeparam name="T">The <see cref="IDisposable"/> instance to wrap</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="usingBlock">The using block.</param>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        public static void Using<T>(this T instance, Action<T> usingBlock) where T : IDisposable
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            using (instance)
                usingBlock(instance);

        }//Using<T>


        /// <summary>
        /// Takes an object of type <c>T</c> that implements IDisposable
        /// and wraps a Func that takes a single parameter of the same
        /// instance of type <c>T</c> and properly calls the Dispose method
        /// afterwards. This is used simply for sytax sugar.
        /// </summary>
        /// <typeparam name="T">The <see cref="IDisposable"/> instance to wrap</typeparam>
        /// <typeparam name="U">The return type from the predicate function</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="usingBlock">The using block.</param>
        /// <exception cref="System.ArgumentNullException">instance</exception>
        public static U Using<T, U>(this T instance, Func<T, U> usingBlock) where T : IDisposable
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            using (instance)
                return usingBlock(instance);

        }//Using<T, U>


        /// <summary>
        /// Locks the specified sync object and executes the given
        /// protected action within a lock block.
        /// </summary>
        /// <param name="sync">The sync object to lock.</param>
        /// <param name="protectedAction">The protected action.</param>
        public static void Lock(this object sync, Action protectedAction)
        {
            if (sync == null)
                sync = new object();
            lock (sync)
            {
                protectedAction();
            }//end: lock
        }//Lock

        /// <summary>
        /// Locks the specified sync object and executes the given
        /// protected function within a lock block and returns the 
        /// function result. Pretty slick, I know.
        /// </summary>
        /// <param name="sync">The sync object to lock.</param>
        /// <param name="protectedFunc">The protected function.</param>
        public static T Lock<T>(this object sync, Func<T> protectedFunc)
        {
            if (sync == null)
                sync = new object();
            T retVal = default(T);

            lock (sync)
            {
                retVal = protectedFunc();
            }//end: lock
            return retVal;
        }//Lock<T>

        /// <summary>
        /// Does a lightweight syncronized read action against a value provided by
        /// a function using the ReaderWriterLockSlim.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="slim">The slim.</param>
        /// <param name="readFunc">The read function.</param>
        /// <returns></returns>
        public static T LockRead<T>(this ReaderWriterLockSlim slim, Func<T> readFunc)
        {
            T val;
            slim.EnterReadLock();
            val = readFunc();
            slim.ExitReadLock();
            return val;
        }//LockRead<T>

        /// <summary>
        /// Performs an exclusive write to a value provided by the writeAction.
        /// </summary>
        /// <param name="slim">The slim.</param>
        /// <param name="writeAction">The write action.</param>
        public static void LockWrite(this ReaderWriterLockSlim slim, Action writeAction)
        {
            slim.EnterWriteLock();
            writeAction();
            slim.ExitWriteLock();
        }//LockWrite

        /// <summary>
        /// Attempts to dequeue an item from a generic queue and sets it to an output
        /// parameter or <c>null</c> if no item left in the queue.
        /// </summary>
        /// <typeparam name="T">The queue type of <c>T</c></typeparam>
        /// <param name="queue">The queue of type <c>T</c> this method extends</param>
        /// <param name="value">The value to set with the item retrieved from the queue</param>
        /// <returns></returns>
        public static bool TryDequeue<T>(this Queue<T> queue, out T value)
        {
            value = default(T);

            if (queue == null || !queue.Any())
                return false;

            value = queue.Dequeue();
            return true;
        }//TryDequeue

        /// <summary>
        /// Gets a value for an object by property name. Nifty for unit tests,
        /// not that nifty for production 'cause reflection is slow.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T Get<T>(this object obj, string name)
        {
            if (obj == null || string.IsNullOrWhiteSpace(name))
                return default(T);

            var prop = obj.GetType().GetProperty(name);
            if (prop == null)
                return default(T);

            var val = prop.GetValue(obj);
            if (val == null)
                return default(T);

            if (typeof(T) == typeof(string) && !(val is T))
                val = val.ToString();

            return (T)val;
        }

        /// <summary>
        /// Adds a value to a list if it does not already exist.
        /// </summary>
        /// <typeparam name="T">The type of list to add the value to.</typeparam>
        /// <param name="list">The list that this method extends.</param>
        /// <param name="value">The value (non-default) to add.</param>
        public static void AddIfNotExists<T>(this List<T> list, T value)
        {
            if (list == null || value == null)
                return;
            if (list.Contains(value))
                return;
            list.Add(value);
        }

        /// <summary>
        /// Adds a value to a list if it does not already exist based on the passed
        /// in evaluation function.
        /// </summary>
        /// <typeparam name="T">The type of list to add the value to.</typeparam>
        /// <param name="list">The list that this method extends.</param>
        /// <param name="value">The value (non-default) to add.</param>
        /// <param name="eval">The function that evaluates whether this already exists in the list.</param>
        public static void AddIfNotExists<T>(this List<T> list, T value, Func<T, bool> eval)
        {
            if (list == null || value == null)
                return;
            if (list.Any(eval))
                return;
            list.Add(value);
        }

        public static void AddRangeIfNotExists<T>(this List<T> list, IEnumerable<T> range)
        {
            if (list == null || range == null)
                return;
            foreach (T item in range)
                list.AddIfNotExists(item);
        }

        public static void AddRangeIfNotExists<T>(this List<T> list, IEnumerable<T> range, Func<T, T, bool> eval)
        {
            if (list == null || range == null)
                return;
            foreach (T item in range)
                if (!list.Any(l => eval(l, item)))
                    list.Add(item);
        }

        /// <summary>
        /// Adds an item to the extended list and returns that same item back for
        /// a nice fluid interface.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <typeparam name="T">The type of list to add the value to.</typeparam>
        /// <param name="list">The list that this method extends.</param>
        /// <param name="value">The value (non-default) to add.</param>
        /// <returns>The original value that should be added</returns>
        public static T AddFluid<T>(this List<T> list, T value)
        {
            if (list == null || value == null)
                return value;
            list.AddIfNotExists(value);
            return value;
        }

        /// <summary>
        /// Adds an item to the extended list and returns that same item back for
        /// a nice fluid interface if the if parameter evaluates to true.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="list">The list that this method extends.</param>
        /// <param name="value">The value (non-default) to add.</param>
        /// <param name="if">If true, then adds, otherwise doesn't.</param>
        /// <returns>
        /// The original value that should be added
        /// </returns>
        public static T AddFluidIf<T>(this List<T> list, T value, Func<bool> @if)
        {
            if (@if())
                return list.AddFluid(value);
            return value;
        }

        /// <summary>
        /// Adds a range of values from collection to the extended list and returns the original
        /// list that the new values were added to for a nice fluid interface.
        /// </summary>
        /// <typeparam name="T">The type of list</typeparam>
        /// <param name="list">The list this method extends</param>
        /// <param name="collection">The collection of items to add to the list</param>
        /// <returns>The original list that the range of values are being added to</returns>
        public static List<T> AddRangeFluid<T>(this List<T> list, IEnumerable<T> collection)
        {
            if (list == null || collection == null || !collection.Any())
                return list;
            list.AddRange(collection);
            return list;
        }

        /// <summary>
        /// Adds a range of values from collection to the extended list and returns the original
        /// list that the new values were added to for a nice fluid interface.
        /// </summary>
        /// <typeparam name="T">The type of list</typeparam>
        /// <param name="list">The list this method extends</param>
        /// <param name="collection">The collection of items to add to the list</param>
        /// <returns>The original list that the range of values are being added to</returns>
        public static List<T> AddRangeFluidIf<T>(this List<T> list, IEnumerable<T> collection, Func<bool> @if)
        {
            if (@if())
                return list.AddRangeFluid(collection);
            return list;
        }

        /// <summary>
        /// Adds a string to the end of the list with the specified format arguments.
        /// </summary>
        /// <param name="list">The generic list of string this method extends</param>
        /// <param name="format">The format string to add to the end of the list</param>
        /// <param name="args">The format arguments to use when formatting the string</param>
        public static void AddFormat(this List<string> list, string format, params object[] args)
        {
            if (list != null && format != null)
                list.Add(string.Format(format, args));
        }//AddFormat

        /// <summary>
        /// Parses a camel cased or pascal cased string and returns an array of the words within the string.
        /// </summary>
        /// <example>
        /// The string "PascalCasing" will return an array with two elements, "Pascal" and "Casing".
        /// </example>
        /// <param name="source">The string that is camel cased that needs to be split</param>
        /// <returns>An arry of each word part</returns>
        public static string[] SplitCamelCase(this string source)
        {
            if (source == null)
                return new string[] { }; //Return empty array.

            if (source.Length == 0)
                return new string[] { "" };

            //return Regex.Replace(source, @"([a-z])([A-Z])", "$1|$2").Split('|');

            StringCollection words = new StringCollection();
            int wordStartIndex = 0;

            char[] letters = source.ToCharArray();
            int capsCount = char.IsUpper(letters[0]) ? 1 : 0;
            bool lastLower = !char.IsUpper(letters[0]);
            // Skip the first letter. we don't care what case it is.
            for (int i = 1; i < letters.Length; i++)
            {
                if (char.IsUpper(letters[i]))
                {
                    //Grab everything before the current index.
                    if (lastLower)
                    {
                        words.Add(new String(letters, wordStartIndex, i - wordStartIndex));
                        wordStartIndex = i;
                    }
                    capsCount++;
                    lastLower = false;
                }
                else
                {
                    if (!lastLower && capsCount > 1)
                    {
                        words.Add(new String(letters, wordStartIndex, i - wordStartIndex - 1));
                        wordStartIndex = i - 1;
                        capsCount = 1;
                        i--;
                    }
                    else
                        capsCount = 0;
                    lastLower = true;
                }
            }

            //We need to have the last word.
            words.Add(new String(letters, wordStartIndex, letters.Length - wordStartIndex));

            //Copy to a string array.
            string[] wordArray = new string[words.Count];
            words.CopyTo(wordArray, 0);
            return wordArray;
        }//SplitUpperCase

        /// <summary>
        /// Parses a camel cased or pascal cased string and returns a new string with spaces between the words in the string.
        /// </summary>
        /// <example>
        /// The string "PascalCasing" will return an array with two elements, "Pascal" and "Casing".
        /// </example>
        /// <param name="source">The string that is camel cased that needs to be split</param>
        /// <returns>A string with spaces between each word part</returns>
        public static string SplitCamelCaseString(this string source)
        {
            return string.Join(" ", source.SplitCamelCase()).Trim();
        }//SplitUpperCaseString

        /// <summary>
        /// Split exluding the list that shouldn't be splitted
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ignoreList"></param>
        /// <returns></returns>
        public static string SplitCamelCaseString(this string source, string[] ignoreList)
        {
            if (ignoreList.Contains(source.ToLower()))
                return source;
            else
                return SplitCamelCaseString(source);
        }

        /// <summary>
        /// Converts a hexadecimal string to its numeric 64-bit signed integer equivilent value.
        /// </summary>
        /// <param name="hexString">The hexadecimal string to be converted to a 64-bit signed integer.</param>
        /// <returns>A 64-bit signed integer that represents the hexadecimal string this method extends.</returns>
        public static long FromHex(this string hexString)
        {
            long num;
            if (long.TryParse(hexString, System.Globalization.NumberStyles.HexNumber, Thread.CurrentThread.CurrentCulture.NumberFormat, out num))
                return num;
            return 0;
        }

        /// <summary>
        /// Converts a 64-bit signed integer to its equivilent hexadecimal string (such as an ObjectId).
        /// </summary>
        /// <param name="number">The 64-bit signed integer this method extends to convert to a hexadecimal string.</param>
        /// <returns>A hexadecimal string that represents the 64-bit signed integer this method extends.</returns>
        public static string ToHex(this long number)
        {
            return number.ToString("X").ToLowerInvariant();
        }

        /// <summary>
        /// Converts a 64-bit signed integer to its equivilent hexadecimal string (such as an ObjectId).
        /// </summary>
        /// <param name="number">The 64-bit signed integer this method extends to convert to a hexadecimal string.</param>
        /// <returns>A hexadecimal string that represents the 64-bit signed integer this method extends.</returns>
        public static string ToHex(this string number)
        {
            long num;
            if (long.TryParse(number, out num))
                return num.ToString("X").ToLowerInvariant();

            return number;
        }

        /// <summary>
        /// Gets a raw value for a Bson document based on the property name, if it exists, otherwise
        /// returns the <c>default(T)</c>.
        /// </summary>
        /// <typeparam name="T">The type of value being expected back</typeparam>
        /// <param name="doc">The BsonDocument that this method extends and for which to extract the value from</param>
        /// <param name="name">The property name to pull the value from</param>
        /// <returns>A value for the named property of type T or <c>default(T)</c>.</returns>
        public static T GetRawValue<T>(this BsonDocument doc, string name)
        {
            if (doc != null && !string.IsNullOrWhiteSpace(name) && doc.Contains(name))
            {
                BsonValue val = doc[name];
                if (val != null && !val.IsBsonNull)
                    return val.GetRawValue<T>();
            }

            return default(T);
        }

        /// <summary>
        /// Gets a raw value for a Bson value if it is appropriate or valid, otherwise
        /// returns the <c>default(T)</c>.
        /// </summary>
        /// <typeparam name="T">The type of value being expected back</typeparam>
        /// <param name="value">The BsonValue that this method extends and for which to extract the value from.</param>
        /// <returns>
        /// The value of type T or <c>default(T)</c>.
        /// </returns>
        public static T GetRawValue<T>(this BsonValue value)
        {
            if (value != null)
            {
                // If it's an object Id -> string conversion, do it explicitly
                if (typeof(T) == typeof(string) && value.IsObjectId)
                    // Boxing match, lol, Box, unbox, box and re-box
                    return (T)(object)value.ToString();
                object rawValue = BsonTypeMapper.MapToDotNetValue(value);
                if (rawValue == null)
                    return default(T);
                if (rawValue is T)
                    return (T)rawValue;
                if (rawValue is Guid && typeof(T) == typeof(string))
                    return (T)(object)rawValue.ToString();
                Type tt = typeof(T);
                if (tt.IsGenericType && tt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    tt = tt.GetGenericArguments().First();
                if (rawValue.GetType() == tt)
                    return (T)rawValue;
                if (tt.IsEnum)
                {
                    if (rawValue is string)
                        try
                        {
                            return (T)Enum.Parse(tt, rawValue as string, true);
                        }
                        catch
                        {
                            return default(T);
                        }

                    if ((Enum.GetUnderlyingType(tt) == rawValue.GetType() || rawValue is char || rawValue is short || rawValue is int || rawValue is long)
                        && Enum.IsDefined(tt, rawValue))
                        return (T)Enum.ToObject(tt, rawValue);
                }
            }

            return default(T);
        }

        /// <summary>
        /// Gets a raw value for a Bson document based on the property name, if it exists, otherwise
        /// returns the default of type.
        /// </summary>
        /// <typeparam name="T">The type of value being expected back</typeparam>
        /// <param name="doc">The BsonDocument that this method extends and for which to extract the value from</param>
        /// <param name="name">The property name to pull the value from</param>
        /// <returns>A value for the named property of type or default of type.</returns>
        public static object GetRawValue(this BsonDocument doc, string name, Type type)
        {
            if (doc != null && !string.IsNullOrWhiteSpace(name) && doc.Contains(name))
            {
                BsonValue val = doc[name];
                if (val != null && !val.IsBsonNull)
                    return val.GetRawValue(type);
            }

            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// Gets a raw value for a Bson value if it is appropriate or valid, otherwise
        /// returns the default of type.
        /// </summary>
        /// <param name="value">The BsonValue that this method extends and for which to extract the value from.</param>
        /// <param name="type">The type of value being expected back.</param>
        /// <returns>
        /// The value cast to type or default of type.
        /// </returns>
        public static object GetRawValue(this BsonValue value, Type type)
        {
            if (value != null)
            {
                // If it's an object Id -> string conversion, do it explicitly
                if (type == typeof(string) && value.IsObjectId)
                    // Boxing match, lol, Box, unbox, box and re-box
                    return value.ToString();
                object rawValue = BsonTypeMapper.MapToDotNetValue(value);
                if (rawValue == null)
                    return type.IsValueType ? Activator.CreateInstance(type) : null;
                if (rawValue.GetType() == type)
                    return rawValue;
                Type tt = type;
                if (tt.IsGenericType && tt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    tt = tt.GetGenericArguments().First();
                if (rawValue.GetType() == tt)
                    return rawValue;
                if (tt.IsEnum)
                {
                    if (rawValue is string)
                        try
                        {
                            return Enum.Parse(tt, rawValue as string, true);
                        }
                        catch
                        {
                            return type.IsValueType ? Activator.CreateInstance(type) : null;
                        }

                    if ((Enum.GetUnderlyingType(tt) == rawValue.GetType() || rawValue is char || rawValue is short || rawValue is int || rawValue is long)
                        && Enum.IsDefined(tt, rawValue))
                        return Enum.ToObject(tt, rawValue);
                }
            }

            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// Sets a raw value in a BsonDocument or removes that property from the document
        /// if that value is null or default of <c>T</c>.
        /// </summary>
        /// <typeparam name="T">The type of value to set, implied from the value passed in.</typeparam>
        /// <param name="doc">The document this method extends</param>
        /// <param name="name">The property name to set</param>
        /// <param name="value">The value to set into the document for the given property name</param>
        /// <returns>The same BsonDocument this method extends for a fluid interface.</returns>
        public static BsonDocument SetRawValue<T>(this BsonDocument doc, string name, T value)
        {
            if (doc != null && !string.IsNullOrWhiteSpace(name))
            {
                if (value == null)
                    doc.Remove(name);
                else if (value is decimal)
                    // HACK: see http://www.cnblogs.com/liguo/archive/2013/05/24/3096646.html
                    doc.Set(name, new BsonDouble(Convert.ToDouble(value)));
                else
                    doc.Set(name, BsonValue.Create(value));
            }
            return doc;
        }

        /// <summary>
        /// Converts the current object instance to a JSON string.
        /// </summary>
        /// <param name="value">The object this method extends.</param>
        /// <returns>A JSON string representing that object.</returns>
        public static string ToJSON(this object value)
        {
            if (value == null)
                return null;
            return JsonConvert.SerializeObject(value, Newtonsoft.Json.Formatting.None, JsonSettings.SerializerSettings);
        }

        public static string ToJSON2(this object value)
        {
            if (value == null)
                return null;
            return JsonConvert.SerializeObject(value, Newtonsoft.Json.Formatting.Indented, JsonSettings.SerializerSettings);
        }


        /// <summary>
        /// Creates a deep copy clone of a current object into it's own instance and returns
        /// the result. Instance of type <c>T</c> must be serializable or at least a simple object
        /// type that JSON.NET can properly serialize and deserialize appropriately.
        /// </summary>
        /// <typeparam name="T">The inferred type of the instance to clone and return.</typeparam>
        /// <param name="instance">The instance this method extends that is to be cloned.</param>
        /// <returns>A deep cloned copy of the passed in instance of type <c>T</c>.</returns>
        public static T Clone<T>(this T instance) where T : new()
        {
            if (instance == null)
                return default(T);

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Newtonsoft.Json.JsonConvert.SerializeObject(instance));
 
        }

        private static readonly MethodInfo CloneMethod = typeof(Object).GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance);

        private static bool IsPrimitive(this Type type)
        {
            if (type == typeof(String)) return true;
            return (type.IsValueType & type.IsPrimitive);
        }

        private static Object Copy(this Object originalObject)
        {
            return InternalCopy(originalObject, new Dictionary<Object, Object>(new ReferenceEqualityComparer()));
        }
        private static Object InternalCopy(Object originalObject, IDictionary<Object, Object> visited)
        {
            if (originalObject == null) return null;
            var typeToReflect = originalObject.GetType();
            if (IsPrimitive(typeToReflect)) return originalObject;
            try
            {
                if (visited.ContainsKey(originalObject)) return visited[originalObject];
            }
            catch(Exception)
            {
                return null;
            }
            if (typeof(Delegate).IsAssignableFrom(typeToReflect)) return null;
            var cloneObject = CloneMethod.Invoke(originalObject, null);
            if (typeToReflect.IsArray)
            {
                var arrayType = typeToReflect.GetElementType();
                if (IsPrimitive(arrayType) == false)
                {
                    Array clonedArray = (Array)cloneObject;
                    clonedArray.ForEach((array, indices) => array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited), indices));
                }
            }
            visited.Add(originalObject, cloneObject);
            CopyFields(originalObject, visited, cloneObject, typeToReflect);
            RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect);
            return cloneObject;
        }

        private static void RecursiveCopyBaseTypePrivateFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect)
        {
            if (typeToReflect.BaseType != null)
            {
                RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect.BaseType);
                CopyFields(originalObject, visited, cloneObject, typeToReflect.BaseType, BindingFlags.Instance | BindingFlags.NonPublic, info => info.IsPrivate);
            }
        }

        private static void CopyFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy, Func<FieldInfo, bool> filter = null)
        {
            foreach (FieldInfo fieldInfo in typeToReflect.GetFields(bindingFlags))
            {
                if (filter != null && filter(fieldInfo) == false) continue;
                if (IsPrimitive(fieldInfo.FieldType)) continue;
                var originalFieldValue = fieldInfo.GetValue(originalObject);
                var clonedFieldValue = InternalCopy(originalFieldValue, visited);
                fieldInfo.SetValue(cloneObject, clonedFieldValue);
            }
        }

        private static void ForEach(this Array array, Action<Array, int[]> action)
        {
            if (array.LongLength == 0) return;
            ArrayTraverse walker = new ArrayTraverse(array);
            do action(array, walker.Position);
            while (walker.Step());
        }

        private class ArrayTraverse
        {
            public int[] Position;
            private int[] maxLengths;

            public ArrayTraverse(Array array)
            {
                maxLengths = new int[array.Rank];
                for (int i = 0; i < array.Rank; ++i)
                {
                    maxLengths[i] = array.GetLength(i) - 1;
                }
                Position = new int[array.Rank];
            }

            public bool Step()
            {
                for (int i = 0; i < Position.Length; ++i)
                {
                    if (Position[i] < maxLengths[i])
                    {
                        Position[i]++;
                        for (int j = 0; j < i; j++)
                        {
                            Position[j] = 0;
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        private static T Copy<T>(this T original)
        {
            return (T)Copy((Object)original);
        }

        private class ReferenceEqualityComparer : EqualityComparer<Object>
        {
            public override bool Equals(object x, object y)
            {
                return ReferenceEquals(x, y);
            }
            public override int GetHashCode(object obj)
            {
                if (obj == null) return 0;
                try
                {
                    return obj.GetHashCode();
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Deserializes the given Bson document and returns the .NET object instance that
        /// it represents.
        /// </summary>
        /// <typeparam name="T">The DotNet type to convert the document to.</typeparam>
        /// <param name="doc">The Bson document to convert.</param>
        /// <returns>An instance of the DotNet type of <typeparamref name="T"/>.</returns>
        public static T ToDotNetObject<T>(this BsonDocument doc)
        {
            return BsonSerializer.Deserialize<T>(doc);
        }

        /// <summary>
        /// Deserializes the given Bson document and returns the .NET object instance that
        /// it represents.
        /// </summary>
        /// <typeparam name="T">The DotNet type to convert the document to.</typeparam>
        /// <param name="doc">The Bson document to convert.</param>
        /// <returns>An instance of the DotNet type of <typeparamref name="T"/>.</returns>
        public static T ToDotNetObject<T>(this BsonValue doc)
        {
            return BsonSerializer.Deserialize<T>(doc.AsBsonDocument);
        }

        /// <summary>
        /// Filters a sequence of values based on a predicate and returns a list.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable of T to filter.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>An System.Collections.Generic.IEnumerable of T that contains elements from the input sequence that satisfy the condition.</returns>
        /// <exception cref="System.ArgumentNullException">source or predicate is null.</exception>
        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return source.Where(predicate).ToList();
        }

        /// <summary>
        /// Filters a sequence of values based on a predicate and returns a list. Each element's index is used in the 
        /// logic of the predicate function.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">An System.Collections.Generic.IEnumerable of T to filter.</param>
        /// <param name="predicate">A function to test each source element for a condition; the second parameter of the function represents the index of the source element.</param>
        /// <returns>An System.Collections.Generic.IEnumerable of T that contains elements from the input sequence that satisfy the condition.</returns>
        /// <exception cref="System.ArgumentNullException">source or predicate is null.</exception>
        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            return source.Where(predicate).ToList();
        }

        public static string FormatPhone(this string phoneNumber)
        {
            string phone = phoneNumber;
            if (string.IsNullOrWhiteSpace(phone))
                return phone;
            phone = phone.StripPhone();

            string ext = null;
            var parts = phone.Split('x', 'X');
            if (parts.Length > 1)
                ext = string.Join("", parts, 1, parts.Length - 1);
            phone = parts[0];

            string countryCode = null;
            if (phone.Length > 10)
            {
                countryCode = phone.Substring(0, phone.Length - 10);
                countryCode = countryCode.TrimStart('0');
                if (string.IsNullOrWhiteSpace(countryCode))
                    countryCode = null;
                phone = phone.Substring(phone.Length - 10, 10);
            }
            // Don't know what to do with this mess, all phones, even international are 10 digits in length
            if (phone.Length < 10)
                return string.Format("{0}{1}{2}", phone, string.IsNullOrWhiteSpace(ext) ? "x" : "", ext);

            string areaCode = null;
            string exchange = null;
            string number = null;
            string format = "({0}) {1}-{2}";

            if ((countryCode ?? "1") == "1")
            {
                // United states or +1 exchange numbering system countries
                areaCode = phone.Substring(0, 3);
                exchange = phone.Substring(3, 3);
                number = phone.Substring(6, 4);
                countryCode = null;
            }
            else
            {
                // International calling code countries, have a slightly different format
                //  and each country, and even each region/city may have a different STD short code
                //  or exchange length, so without having a database of different country code +
                //  regional formats, just remove all formatting (very common in European applications).
                number = phone;
                format = "{2}";
            }

            StringBuilder p = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(countryCode))
                p.AppendFormat("+{0} ", countryCode);
            p.AppendFormat(format, areaCode, exchange, number);
            if (!string.IsNullOrWhiteSpace(ext))
                p.AppendFormat(" x{0}", ext);

            return p.ToString();
        }

        public static string StripPhone(this string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
                return phoneNumber;

            return Regex.Replace(phoneNumber, @"[^0-9xX]", "");
        }

        public static IEnumerable<TSource> Distinct<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> knownKeys = new HashSet<TKey>();
            foreach (TSource element in source)
                if (knownKeys.Add(keySelector(element)))
                    yield return element;
        }

        /// <summary>
        /// Reads the contents of the stream into a byte array. Data is returned as a byte array. 
        /// An IOException is thrown if any of the underlying IO calls fail.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        /// <returns>A byte array containing the contents of the stream.</returns>
        /// <exception cref="NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        public static byte[] ReadAllBytes(this Stream source)
        {
            long originalPosition = source.Position;
            source.Position = 0;
            try
            {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = source.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = source.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                source.Position = originalPosition;
            }
        } // ReadAllBytes

        /// <summary>
        /// Gets the index of the first item that matches the passed in evaluation function for the given
        /// collection this method extends.
        /// </summary>
        /// <typeparam name="T">The type of T</typeparam>
        /// <param name="collection">The collection to enumerate and find the first match index for</param>
        /// <param name="evalulator">The evaulation function that determines the match</param>
        /// <returns>The 32-bit integer index of the matched item</returns>
        public static int IndexOf<T>(this IEnumerable<T> collection, Func<T, bool> evalulator)
        {
            int i = 0;
            foreach (T item in collection)
            {
                if (evalulator(item))
                    return i;
                i++;
            }
            return -1;
        } // IndexOf

        /// <summary>
        /// Converts the specified US State abbreviation to the most common/greater percentage time zone covering that US State/territory or region.
        /// </summary>
        /// <param name="usState">State of the US, territory or commonwealth/region (Abbreviation).</param>
        /// <param name="checkDate">The check date to determine if using Daylight Savings Time.</param>
        /// <returns>A Time Zone abbreviation for display</returns>
        public static string ToTimeZone(this string usState, DateTime? checkDate = null)
        {
            if (string.IsNullOrWhiteSpace(usState)) return "GMT";
            string tzSuffix = "T";
            if (checkDate.HasValue)
                tzSuffix = string.Concat((checkDate.Value.IsDaylightSavingTime() ? "D" : "S"), tzSuffix);

            Func<string, string> tz = new Func<string, string>(s => string.Concat(s, tzSuffix));

            switch (usState.ToUpperInvariant())
            {
                case "AL": return tz("C");
                case "AK": return tz("AK");
                case "AZ": return tz("M");
                case "AR": return tz("C");
                case "CA": return tz("P");
                case "CO": return tz("M");
                case "CT": return tz("E");
                case "DE": return tz("E");
                case "DC": return tz("E");
                case "FL": return tz("E");
                case "GA": return tz("E");
                case "HI": return "HST";
                case "ID": return tz("M");
                case "IL": return tz("C");
                case "IN": return tz("E");
                case "IA": return tz("C");
                case "KS": return tz("C");
                case "KY": return tz("E");
                case "LA": return tz("C");
                case "ME": return tz("E");
                case "MD": return tz("E");
                case "MA": return tz("E");
                case "MI": return tz("E");
                case "MN": return tz("C");
                case "MS": return tz("C");
                case "MO": return tz("C");
                case "MT": return tz("M");
                case "NE": return tz("C");
                case "NV": return tz("P");
                case "NH": return tz("E");
                case "NJ": return tz("E");
                case "NM": return tz("M");
                case "NY": return tz("E");
                case "NC": return tz("E");
                case "ND": return tz("C");
                case "OH": return tz("E");
                case "OK": return tz("C");
                case "OR": return tz("P");
                case "PA": return tz("E");
                case "RI": return tz("E");
                case "SC": return tz("E");
                case "SD": return tz("C");
                case "TN": return tz("C");
                case "TX": return tz("C");
                case "UT": return tz("M");
                case "VT": return tz("E");
                case "VA": return tz("E");
                case "WA": return tz("P");
                case "WV": return tz("E");
                case "WI": return tz("C");
                case "WY": return tz("M");
                case "AS": return tz("S");
                case "GU": return tz("Ch");
                case "MP": return tz("Ch");
                case "PR": return "AST";
                case "UM": return "AST";
                case "VI": return "AST";
                case "AA": return "AA";
                case "AP": return "AP";
                case "AE": return "AE";
                default: return "GMT";
            }

        } // ToTimeZone

        /// <summary>
        /// Helper function indicating whether an
        /// IEnumerable is empty or not (opposite of Any())
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool Empty<T>(this IEnumerable<T> values)
        {
            if(values == null)
            {
                throw new ArgumentNullException("values");
            }

            return !values.Any();
        }

        /// <summary>
        /// Calculates standard deviation for a population
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static double StdDevPop(this IEnumerable<double> values)
        {
            double ret = 0;
            int count = values.Count();
            if (count > 1)
            {
                //Compute the Average
                double avg = values.Average();

                //Perform the Sum of (value-avg)^2
                double sum = values.Sum(d => (d - avg) * (d - avg));

                //Put it all together
                ret = Math.Sqrt(sum / count);
            }
            return ret;
        }

        /// <summary>
        /// Repeats the string value the specified number of repitions and
        /// returns the resulting string.
        /// </summary>
        /// <param name="value">The value to repeat that this method extends.</param>
        /// <param name="repitions">The number of repitions.</param>
        /// <returns>
        /// The string value the specified number of repitions and
        /// returns the resulting string.
        /// </returns>
        public static string Repeat(this string value, int repitions)
        {
            StringBuilder val = new StringBuilder(value);
            for (int i = 1; i < repitions; i++)
                val.Append(value);
            return val.ToString();
        }

        /// <summary>
        /// Formats the specified string with the given arguments.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public static string Format(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Gets all numbers through and including the max from the min.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns></returns>
        public static IEnumerable<int> GetAllNumbersThrough(this int min, int max)
        {
            for (var i = min; i <= max; i++)
                yield return i;
        }

        /// <summary>
        /// Adds the specified key and value to the dictionary and returns the resulting dictionary instance for a fluid interface.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary this method extends.</param>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// dict
        /// or
        /// key
        /// </exception>
        public static Dictionary<TKey, TValue> AddFluid<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value)
        {
            if (dict == null)
                throw new ArgumentNullException("dict");
            if (key == null)
                throw new ArgumentNullException("key");
            if (dict.ContainsKey(key))
                return dict;
            dict.Add(key, value);
            return dict;
        }

        /// <summary>
        /// Adds the specified key and value to the dictionary if the test evaluates to <c>true</c>.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary this method extends.</param>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        /// <param name="test">if set to <c>true</c> adds the item, otherwise does not add anything.</param>
        /// <returns>The dictionary passed in for a fluid interface.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// dict
        /// or
        /// key
        /// </exception>
        public static Dictionary<TKey, TValue> AddIf<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value, bool test)
        {
            if (!test)
                return dict;
            if (dict == null)
                throw new ArgumentNullException("dict");
            if (key == null)
                throw new ArgumentNullException("key");
            if (dict.ContainsKey(key))
                return dict;
            return dict.AddFluid(key, value);
        }

        /// <summary>
        /// Adds the specified key and value to the dictionary if the test evaluates to <c>true</c>.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary this method extends.</param>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        /// <param name="test">Function that if returns <c>true</c> adds the item, otherwise does not add anything.</param>
        /// <returns>The dictionary passed in for a fluid interface.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// dict
        /// or
        /// key
        /// </exception>
        public static Dictionary<TKey, TValue> AddIf<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value, Func<bool> test)
        {
            if (test == null)
                return dict;
            return dict.AddIf(key, value, test());
        }

        /// <summary>
        /// Gets the string value from the inner text of the node specified by <paramref name="xpath"/> using
        /// the parent <paramref name="node"/> to execute the select single node method. If nothing found or value 
        /// is empty, returns <c>null</c>.
        /// </summary>
        /// <param name="node">The node to search within.</param>
        /// <param name="xpath">The xpath value to the node to grab the inner text from.</param>
        /// <returns>The inner text value or <c>null</c> if the node isn't found by the <paramref name="xpath"/>
        /// or the value is empty.</returns>
        public static string GetString(this XmlNode node, string xpath)
        {
            var referenceNode = node.SelectSingleNode(xpath);
            if (referenceNode != null && !string.IsNullOrWhiteSpace(referenceNode.InnerText))
                return referenceNode.InnerText;
            return null;
        }

        /// <summary>
        /// Gets the integer value parsed from the inner text of the node specified by <paramref name="xpath"/> using
        /// the parent <paramref name="node"/> to execute the select single node method. If nothing found or value 
        /// is empty, OR is not a valid integer value, returns <c>null</c>.
        /// </summary>
        /// <param name="node">The node to search within.</param>
        /// <param name="xpath">The xpath value to the node to grab the inner text from.</param>
        /// <returns>The inner text value parsed as an integer or <c>null</c> if the node isn't 
        /// found by the <paramref name="xpath"/> or the value is empty or the value is not a valid integer.</returns>
        public static int? GetInt32(this XmlNode node, string xpath)
        {
            var referenceNode = node.SelectSingleNode(xpath);
            if (referenceNode != null && !string.IsNullOrWhiteSpace(referenceNode.InnerText))
            {
                int val;
                if (int.TryParse(referenceNode.InnerText, out val))
                    return val;
            }
            return null;
        }


        /// <summary>
        /// Get the decimal value for a XML Node.
        /// Extract the string text and converts it to decimal.
        /// Otherwise returns null
        /// </summary>
        /// <param name="node"></param>
        /// <param name="xpath"></param>
        /// <returns></returns>
        public static decimal? GetDecimal(this XmlNode node, string xpath)
        {
            var referenceNode = node.SelectSingleNode(xpath);
            if (referenceNode != null && !string.IsNullOrWhiteSpace(referenceNode.InnerText))
            {
                decimal val;
                if (decimal.TryParse(referenceNode.InnerText, out val))
                    return val;
            }
            return null;
        }

        /// <summary>
        /// Converts the current JSON string to T.
        /// </summary>
        /// <param name="value">The object this method extends.</param>
        /// <returns>A JSON string representing that object.</returns>
        public static T DeserializeJson<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Split input string based on maxLength without breaking words
        /// </summary>
        /// <param name="input">string to be splitted</param>
        /// <param name="maxLength">maximum no. of characters</param>
        /// <returns>List of string</returns>
        public static List<string> SplitStringOnMaxLength(this string input, int maxLength)
        {
            List<string> columnRows = new List<string>();

            string[] chunks = input.Split(' ');
            StringBuilder rowChunk = new StringBuilder(0);
            IEnumerable<string> rowData;
            foreach (var chunk in chunks)
            {
                if (chunk.Length + rowChunk.Length + 1 > maxLength)
                {
                    //to avoid empty string
                    if (rowChunk.Length > 0)
                    {
                        //Split bigger string on MaxLength, if required, otherwise add
                        rowData = rowChunk.Length > maxLength ? rowChunk.ToString().SplitBiggerStringOnMaxLength(maxLength) : new List<string> { rowChunk.ToString() };
                        columnRows.AddRange(rowData);
                    }
                    rowChunk.Clear();
                }
                rowChunk.Append((rowChunk.Length == 0 ? "" : " ") + chunk);
            }
            if (rowChunk.Length > 0)
            {
                //Split bigger string on MaxLength, if required, otherwise add
                rowData = rowChunk.Length > maxLength ? rowChunk.ToString().SplitBiggerStringOnMaxLength(maxLength) : new List<string> { rowChunk.ToString() };
                columnRows.AddRange(rowData);
            }

            return columnRows;
        }

        /// <summary>
        /// Split bigger string having size greater than maxLength in multiple words and connect with "_"
        /// </summary>
        /// <param name="input">string to be splitted</param>
        /// <param name="maxLength">maximum no. of characters</param>
        /// <returns>List of string</returns>
        public static IEnumerable<string> SplitBiggerStringOnMaxLength(this string input, int maxLength)
        {
            List<string> columnRows = new List<string>();
            //word wrap character for the long words of a column
            string wordContinuedby = "-";
            List<string> chunks = input.Split(maxLength - 1);
            int index = 0;
            foreach (string chunk in chunks)
            {
                if (index < chunks.Count - 1)
                {
                    //continue words
                    columnRows.Add(string.Concat(chunk, wordContinuedby));
                }
                else
                {
                    //start of word
                    columnRows.Add(chunk);
                }
                index++;
            }
            return columnRows.ToArray();
        }

        /// <summary>
        /// Split string based on chunk Size
        /// </summary>
        /// <param name="input">string to be splitted</param>
        /// <param name="chunkSize">chunk Size</param>
        /// <returns>List of string</returns>
        public static List<string> Split(this string input, int chunkSize)
        {
            List<string> chunks = new List<string>();
            for (int i = 0; i < input.Length; i = i + chunkSize)
            {
                if (input.Length - i >= chunkSize)
                {
                    chunks.Add(input.Substring(i, chunkSize));
                }
                else
                {
                    chunks.Add(input.Substring(i, ((input.Length - i))));
                }
            }
            return chunks;
        }

        public static string ZeroIfEmpty(this string s)
        {
            return string.IsNullOrEmpty(s) ? "0" : s;
        }

        /// <summary>
        /// Converts an object to dynamic
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static dynamic ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();
            try
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
                    expando.Add(property.Name, property.GetValue(value));
            }
            catch
            {
                //just trapping the error
            }

            return expando as ExpandoObject;
        }

        /// <summary>
        /// Returns an object as dictionary
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static dynamic ToDictionary(this object value)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
                    dict.Add(property.Name, property.GetValue(value));
            }
            catch
            {
                //just trapping the error
            }

            return dict;
        }

 
        /// <summary>
        /// Get batch with group
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> BatchWithGroup<T>(this IEnumerable<T> items, int maxItems)
        {
            return items.Select((item, inx) => new { item, inx })
                        .GroupBy(x => x.inx / maxItems)
                        .Select(g => g.Select(x => x.item));
        }


        /// <summary>
        /// Get safely string from reader
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public static string GetStringOrEmpty(this Npgsql.NpgsqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        /// <summary>
        /// Return the datetime or null
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public static DateTime? GetDateTimeOrNull(this Npgsql.NpgsqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDateTime(colIndex);
            return null;
        }

        /// <summary>
        /// Return data from data reader
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(this Npgsql.NpgsqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetFieldValue<T>(colIndex);
            return default(T);
        }

        /// <summary>
        /// Returns byte array from Stream
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] ToByteArray(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Returns Streams from byte array
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Stream ToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes);
        }
    }
}
