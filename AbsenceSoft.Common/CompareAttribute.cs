﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Linq;

namespace AbsenceSoft.Common
{
    /// <summary>
    /// Used to compare two properties (numbers or dates).
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class CompareAttribute : ValidationAttribute
    {
        /// <summary>
        /// Gets or sets the source property.
        /// </summary>
        /// <value>
        /// The source property.
        /// </value>
        public String SourceProperty { get; set; }

        /// <summary>
        /// Gets or sets the match property.
        /// </summary>
        /// <value>
        /// The match property.
        /// </value>
        public String MatchProperty { get; set; }

        /// <summary>
        /// Gets or sets the compare operator.
        /// </summary>
        /// <value>
        /// The compare operator.
        /// </value>
        public Operator CompareOperator { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareAttribute"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="oper">The oper.</param>
        /// <param name="match">The match.</param>
        public CompareAttribute(string source, Operator oper, string match)
        {
            SourceProperty = source;
            MatchProperty = match;
            CompareOperator = oper;
        }

        public enum Operator
        {
            GreaterThan = 0,
            LessThan,
            GreaterThanEqualTo,
            LessThanEqualTo,
            EqualTo
        }

        /// <summary>
        /// Determines whether the specified value of the object is valid.
        /// </summary>
        /// <param name="value">The value of the object to validate.</param>
        /// <returns>
        /// true if the specified value is valid; otherwise, false.
        /// </returns>
        public override Boolean IsValid(Object value)
        {
            Type objectType = value.GetType();

            PropertyInfo[] properties = objectType.GetProperties();

            object sourceValue = new object();
            object matchValue = new object();

            Type sourceType = null;
            Type matchType = null;

            int counter = 0;

            foreach (PropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Name == SourceProperty || propertyInfo.Name == MatchProperty)
                {
                    if (counter == 0)
                    {
                        sourceValue = propertyInfo.GetValue(value, null);
                        sourceType = propertyInfo.GetValue(value, null).GetType();
                    }
                    if (counter == 1)
                    {
                        matchValue = propertyInfo.GetValue(value, null);
                        matchType = propertyInfo.GetValue(value, null).GetType();
                    }
                    counter++;
                    if (counter == 2)
                    {
                        break;
                    }
                }
            }

            if (sourceType != null && matchType != null)
            {

                switch (this.CompareOperator)
                {
                    case CompareAttribute.Operator.EqualTo:
                        return Convert.ChangeType(sourceValue, sourceType) == Convert.ChangeType(matchValue, matchType);
                    case Operator.GreaterThan:
                        if (IsNumber(sourceValue))
                        {
                            double source = (double)sourceValue;
                            double match = (double)matchValue;
                            return source > match;
                        }
                        else if (IsDate(sourceValue))
                        {
                            DateTime source = (DateTime)sourceValue;
                            DateTime match = (DateTime)matchValue;
                            return source > match;
                        }
                        else
                        {
                            return false;
                        }
                    case Operator.GreaterThanEqualTo:
                        if (IsNumber(sourceValue))
                        {
                            double source = (double)sourceValue;
                            double match = (double)matchValue;
                            return source >= match;
                        }
                        else if (IsDate(sourceValue))
                        {
                            DateTime source = (DateTime)sourceValue;
                            DateTime match = (DateTime)matchValue;
                            return source >= match;
                        }
                        else
                        {
                            return false;
                        }
                    case Operator.LessThan:
                        if (IsNumber(sourceValue))
                        {
                            double source = (double)sourceValue;
                            double match = (double)matchValue;
                            return source < match;
                        }
                        else if (IsDate(sourceValue))
                        {
                            DateTime source = (DateTime)sourceValue;
                            DateTime match = (DateTime)matchValue;
                            return source < match;
                        }
                        else
                        {
                            return false;
                        }
                    case Operator.LessThanEqualTo:
                        if (IsNumber(sourceValue))
                        {
                            double source = (double)sourceValue;
                            double match = (double)matchValue;
                            return source <= match;
                        }
                        else if (IsDate(sourceValue))
                        {
                            DateTime source = (DateTime)sourceValue;
                            DateTime match = (DateTime)matchValue;
                            return source <= match;
                        }
                        else
                        {
                            return false;
                        }
                }

            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified value is number.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private bool IsNumber(object value)
        {
            try
            {
                return value.ToString().ToCharArray().Where(x => !Char.IsDigit(x)).Count() == 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the specified date is date.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        private bool IsDate(object date)
        {
            try
            {
                DateTime dt;
                return DateTime.TryParse(date.ToString(), out dt);
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
