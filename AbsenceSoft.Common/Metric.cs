﻿using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;
using System;
using System.Configuration;

namespace AbsenceSoft.Common
{
    /// <summary>
    /// Represents a request or method metric for instrumentation
    /// </summary>
    [Serializable]
    public class Metric : Entity
    {
        /// <summary>
        /// The connection string
        /// </summary>
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["MongoServerSettings_Logging"].ConnectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Metric"/> class.
        /// </summary>
        public Metric()
        {
            StartDate = DateTime.UtcNow;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Metric"/> class.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        public Metric(string methodName)
            : this()
        {
            Method = methodName;
        }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        /// <value>
        /// The method.
        /// </value>
        [BsonElement("m"), BsonRequired]
        public string Method { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [BsonElement("s"), BsonRequired]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>
        /// The start time.
        /// </value>
        [BsonElement("st")]
        public double StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [BsonElement("e")]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        /// <value>
        /// The end time.
        /// </value>
        [BsonElement("et")]
        public double EndTime { get; set; }

        /// <summary>
        /// Gets or sets the elapsed time to completion.
        /// </summary>
        /// <value>
        /// The elapsed.
        /// </value>
        [BsonElement("t"), BsonRequired, BsonDefaultValue(0)]
        public double Elapsed { get; set; }

        /// <summary>
        /// Upserts the metric to the database and if it does not already exist, sets its new Id property as
        /// well.
        /// </summary>
        public virtual Metric Save(DateTime? end = null)
        {
            if (this.StartTime == 0)
                this.StartTime = this.StartDate.ToUnixDate();

            if (end.HasValue)
                this.EndDate = end;

            if (this.EndDate.HasValue)
            {
                this.EndTime = this.EndDate.Value.ToUnixDate();
                this.Elapsed = this.EndTime - this.StartTime;
            }

            return new MongoRepository<Metric>(ConnectionString).Update(this);
        }
    }
}
