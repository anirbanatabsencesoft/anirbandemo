﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    public class ErrorMessages
    {
        public const string UnhandledException = "An unexpected error has occurred.  Please contact manager or system administrator for assistance.";
    }
}
