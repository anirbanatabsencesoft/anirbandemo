﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    [Serializable]
    [ComVisible(true)]
    public class AbsenceSoftAggregateException : AbsenceSoftException
    {
        /// <summary>
        /// The primary list of messages that collects the aggregate messages for the exception.
        /// </summary>
        private List<string> _messages;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftAggregateException"/> class.
        /// </summary>
        public AbsenceSoftAggregateException() : base("One or more errors has occurred") { _messages = new List<string>(); }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftAggregateException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public AbsenceSoftAggregateException(string message) : base(message ?? "One or more errors has occurred") { _messages = new List<string>() { message }; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftAggregateException"/> class with a specified list of error messages.
        /// </summary>
        /// <param name="messages"></param>
        public AbsenceSoftAggregateException(params string[] messages) : base("One or more errors has occurred") { _messages = new List<string>(messages); }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftAggregateException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected AbsenceSoftAggregateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            _messages = info.GetValue("_messages", typeof(List<string>)) as List<string> ?? new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AbsenceSoft.AbsenceSoftAggregateException"/> class with a specified error message 
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception. If the innerException
        /// parameter is not a null reference, the current exception is raised in a catch
        /// block that handles the inner exception.</param>
        public AbsenceSoftAggregateException(string message, Exception innerException) : base(message ?? "One or more errors has occurred", innerException)
        {
            _messages = new List<string>() { message };
            Exception test = innerException;
            while (test != null)
            {
                if (!_messages.Contains(test.Message))
                    _messages.Add(test.Message);
                test = test.InnerException;
            }
        }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="System.Runtime.Serialization.SerializationInfo"/> with information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        /// <exception cref="System.ArgumentNullException">The info parameter is a null reference (Nothing in Visual Basic).</exception>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("_messages", _messages);
        }

        /// <summary>
        /// Gets the messages for this exception (because it's aggregate, meaning more than one thing is going on).
        /// </summary>
        public List<string> Messages
        {
            get
            {
                if (_messages == null)
                    _messages = new List<string>();
                return _messages;
            }
        }

        /// <summary>
        /// Gets a message that describes the current exception.
        /// </summary>
        public override string Message
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(base.Message))
                    return base.Message;
                return string.Join(Environment.NewLine, Messages);
            }
        }
    }
}
