﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AbsenceSoft.Common.Config
{
    /// <summary>
    /// Class to generalize the config abstraction to environment variables
    /// </summary>
    public static class ConnectionStrings
    {
        /// <summary>
        /// Returns the datawarehouse connection string
        /// </summary>
        public static string DataWarehouse
        {
            get { return ConfigurationManager.ConnectionStrings["DataWarehouse"].ConnectionString; }
        }

        /// <summary>
        /// AT2 connection string
        /// </summary>
        public static string AbsenceTracker
        {
            get { return ConfigurationManager.ConnectionStrings["AbsenceTracker"].ConnectionString; }
        }

        /// <summary>
        /// The mongo server. The transaction database.
        /// </summary>
        public static string MongoServer
        {
            get { return ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString; }
        }

        /// <summary>
        /// The logging to Mongo database
        /// </summary>
        public static string MongoLogging
        {
            get { return ConfigurationManager.ConnectionStrings["MongoServerSettings_Logging"].ConnectionString; }
        }

        /// <summary>
        /// The mongo audit db
        /// </summary>
        public static string MongoAudit
        {
            get { return ConfigurationManager.ConnectionStrings["MongoServerSettings_Audit"].ConnectionString; }
        }
    }
}
