﻿using AbsenceSoft;
using AbsenceSoft.Common.Serializers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Common
{
    [Serializable, BsonSerializer(typeof(BsonHierarchyPathSerializer))]
    public struct HierarchyPath : IComparable, ISerializable, IComparable<HierarchyPath>, IEquatable<HierarchyPath>, IComparable<string>, IEquatable<string>, IEnumerable<string>, IEnumerable, IConvertible, ICloneable
    {
        #region Constants

        /// <summary>
        /// The path separator character.
        /// </summary>
        private const char pathSeparator = '/';

        #endregion Constants

        #region Private Members

        /// <summary>
        /// The _path variable for storing the path.
        /// </summary>
        private readonly string _path;

        /// <summary>
        /// The _id variable for storing the current path identifier.
        /// </summary>
        private readonly string _id;

        /// <summary>
        /// The _parts variable for storing the parts of the path.
        /// </summary>
        private readonly string[] _parts;

        /// <summary>
        /// The _null variable for storing whether this instance represents a null value.
        /// </summary>
        private readonly bool _null;

        /// <summary>
        /// The _root variable for storing whether this instance represents the root path, '/'.
        /// </summary>
        private readonly bool _root;

        /// <summary>
        /// The separator path expression for use in regular expressions.
        /// </summary>
        private static readonly string _separatorPathExpression = Regex.Escape(pathSeparator.ToString());

        #endregion Private Members

        #region .ctor

        /// <summary>
        /// Initializes the <see cref="HierarchyPath"/> struct.
        /// </summary>
        static HierarchyPath()
        {
            Null = new HierarchyPath(null);
            _separatorPathExpression = Regex.Escape(pathSeparator.ToString());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyPath" /> struct by parsing the given path.
        /// </summary>
        /// <param name="path">The path to build the <see cref="HierarchyPath" /> instance from and/or parse.</param>
        /// <exception cref="System.ArgumentException">value passed is not a valid HierarchyPath string value. Must start and end with a path separator.</exception>
        private HierarchyPath(string path)
        {
            _parts = null;
            _id = null;
            _null = true;
            _root = false;
            string text;
            if (isPathValid(path, out text))
                _path = text;
            else
                throw new ArgumentException(string.Format("'{0}' is not valid. Must start and end with a path separator.", path), "path");
            if (_path != null)
            {
                _parts = _path.Split(pathSeparator);
                _id = _parts[_parts.Length - 1];
            }
            _null = _path == null;
            _root = !_null && _parts.Length == 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyPath"/> struct from binary serialization.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public HierarchyPath(SerializationInfo info, StreamingContext context)
        {
            _path = info.GetString("_path");
            _id = info.GetString("_id");
            _parts = info.GetValue("_parts", typeof(string[])) as string[];
            _null = info.GetBoolean("_null");
            _root = info.GetBoolean("_root");
        }

        #endregion .ctor

        #region Properties

        /// <summary>
        /// Gets the full hierarchy path including the current endpoint identifier.
        /// </summary>
        /// <value>
        /// The full hierarchy path including the current endpoint identifier.
        /// </value>
        public string Path { get { return _path; } }

        /// <summary>
        /// Gets the identifier for the current endpoint in the path.
        /// </summary>
        /// <value>
        /// The identifier for the current endpoint in the path.
        /// </value>
        public string Id { get { return _id; } }

        /// <summary>
        /// Gets a value indicating whether this instance is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is null; otherwise, <c>false</c>.
        /// </value>
        public bool IsNull { get { return _null; } }

        /// <summary>
        /// Gets a value indicating whether this instance is root.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is root; otherwise, <c>false</c>.
        /// </value>
        public bool IsRoot { get { return _root; } }

        /// <summary>
        /// A null path. This is the default value for a Hierarchy Path
        /// </summary>
        public static readonly HierarchyPath Null = new HierarchyPath(null);

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Gets a descendant hierarchy by appending the identifier to the current path and returning the result.
        /// </summary>
        /// <param name="id">The descendant identifier.</param>
        /// <returns>
        /// A descendant hierarchy using this hierarchy as the parent.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">id</exception>
        public HierarchyPath AddDescendant(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException("id");

            return new HierarchyPath(string.Concat(_path, id, pathSeparator));
        }

        /// <summary>
        /// Sets the parent of the current hierarchy and returns the result.
        /// </summary>
        /// <param name="parent">The new parent hierarchy this node should belong under.</param>
        /// <returns>
        /// A new instance of <see cref="HierarchyPath" /> with the parent set to the passed
        /// in <paramref name="parent" />.
        /// </returns>
        public HierarchyPath SetParent(HierarchyPath parent)
        {
            string path = string.Concat(parent._path, _id, pathSeparator);
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Sets the parent of the current hierarchy and returns the result.
        /// </summary>
        /// <param name="parentPath">The new parent path this node should belong under.</param>
        /// <returns>
        /// A new instance of <see cref="HierarchyPath" /> with the parent path set to the
        /// passed in <paramref name="parentPath" />.
        /// </returns>
        public HierarchyPath SetParent(string parentPath)
        {
            return SetParent(new HierarchyPath(parentPath));
        }

        /// <summary>
        /// Gets the parent hierarchy path as the immediate ancestor to this hierarchy path.
        /// </summary>
        /// <returns>
        /// The parent <see cref="HierarchyPath" /> or <c>HierarchyPath.Null</c>.
        /// </returns>
        public HierarchyPath GetParent()
        {
            return GetAncestor(1);
        }

        /// <summary>
        /// Gets the parent path of the current path
        /// </summary>        
        /// <returns>
        /// An ancestrial <see cref="HierarchyPath" /> the given number of levels up, or
        /// <c>HierarchyPath.Null</c>.
        /// </returns>
        public HierarchyPath GetParentPath()
        {
            string[] _reDimParts = new string[_parts.Length - 2];
            if (_reDimParts.Length - 1 < 1)
                return HierarchyPath.Null;
            Array.Copy(_parts, 1, _reDimParts, 0, _reDimParts.Length);
            string path = string.Concat(pathSeparator,
                string.Join(pathSeparator.ToString(), _reDimParts.Take(_reDimParts.Length - 1)),
                pathSeparator);
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Gets the ancestor hierarchy based on the specified number of levels. If the levels
        /// specified goes beyond the number of levels then a Null hierarchy path is returned.
        /// </summary>
        /// <param name="levels">The levels to return, defaults to <c>0x1</c>.</param>
        /// <returns>
        /// An ancestrial <see cref="HierarchyPath" /> the given number of levels up, or
        /// <c>HierarchyPath.Null</c>.
        /// </returns>
        public HierarchyPath GetAncestor(int levels = 0x1)
        {
            if (_parts.Length < levels)
                return HierarchyPath.Null;
            string path = string.Concat(pathSeparator,
                string.Join(pathSeparator.ToString(), _parts.Take(_parts.Length - levels)),
                pathSeparator);
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Determines whether this item is is a descendant of the specified hierarchy.
        /// </summary>
        /// <param name="hierarchy">The ancestor hierarchy to check.</param>
        /// <param name="includeSelf">if set to <c>true</c> [include self].</param>
        /// <returns>
        ///   <c>true</c> if this node is a descendent of the passed in path, otherwise <c>false</c>.
        /// </returns>
        public bool IsDescendantOf(HierarchyPath hierarchy, bool includeSelf = false)
        {
            // Everything being equal, let the caller tell us how to detal with it.
            if (this._path == hierarchy._path)
                return includeSelf;
            // Root or Null can't be the decsendent of anything else, duh
            if (this.IsNull || this.IsRoot)
                return false;
            // Can't be the descendent of null, obviously
            if (hierarchy.IsNull)
                return false;
            // Everyone is the descendent of root, so yeah, true
            if (hierarchy.IsRoot)
                return true;
            // Finally, let's determine if the paths match up
            return _path.StartsWith(hierarchy._path);
        }

        /// <summary>
        /// Determines whether this item is a descendant of the specified path.
        /// </summary>
        /// <param name="path">The ancestor path to check.</param>
        /// <param name="includeSelf">if set to <c>true</c> [include self].</param>
        /// <returns>
        ///   <c>true</c> if this node is a descendent of the passed in path, otherwise <c>false</c>.
        /// </returns>
        public bool IsDescendantOf(string path, bool includeSelf = false)
        {
            HierarchyPath hierarchy = new HierarchyPath(path);
            return IsDescendantOf(hierarchy, includeSelf);
        }

        /// <summary>
        /// Determines whether this item is an ancestor of the specified hierarchy.
        /// </summary>
        /// <param name="hierarchy">The descendent hierarchy to check.</param>
        /// <param name="includeSelf">if set to <c>true</c> [include self].</param>
        /// <returns>
        ///   <c>true</c> if this node is an ancestor of the passed in path, otherwise <c>false</c>.
        /// </returns>
        public bool IsAncestorOf(HierarchyPath hierarchy, bool includeSelf = false)
        {
            // Everything being equal, let the caller tell us how to detal with it.
            if (this._path == hierarchy._path)
                return includeSelf;
            // Null can't be the ancestor of anything else, duh
            if (this.IsNull)
                return false;
            // Can't be the ancestor of null or root, obviously
            if (hierarchy.IsNull || hierarchy.IsRoot)
                return false;
            // Everyone is the descendent of root, so yeah, true
            if (this.IsRoot)
                return true;
            // Finally, let's determine if the paths match up
            return hierarchy._path.StartsWith(_path);
        }

        /// <summary>
        /// Determines whether this item is an ancestor of the specified path.
        /// </summary>
        /// <param name="path">The descendent path to check.</param>
        /// <returns>
        ///   <c>true</c> if this node is an ancestor of the passed in path, otherwise <c>false</c>.
        /// </returns>
        public bool IsAncestorOf(string path)
        {
            HierarchyPath hierarchy = new HierarchyPath(path);
            return IsAncestorOf(hierarchy);
        }

        /// <summary>
        /// Gets the level of nesting that the hierarchy falls within.
        /// </summary>
        /// <returns>A number representing the level of nesting that the hierarchy falls within.</returns>
        public int GetLevel()
        {
            return _parts.Length;
        }

        /// <summary>
        /// Gets a <see cref="IMongoQuery" /> to assist in matching any documents that are one of the descendants
        /// of this current hierarchy path given the passed in predicate expression.
        /// </summary>
        /// <param name="predicate">The predicate expression for the property to match against.</param>
        /// <returns>
        /// A <see cref="IMongoQuery" /> that matches descendants based on the predicate property specified.
        /// </returns>
        public IMongoQuery GetAllDescendants(Expression<Func<HierarchyPath>> predicate)
        {
            MemberExpression member = predicate.Body as MemberExpression;
            return Query.Matches(member.Member.Name, GetDescendantExpression(null));
        }

        /// <summary>
        /// Gets a <see cref="IMongoQuery" /> to assist in matching any documents that are one of the descendants
        /// of this current hierarchy path given the passed in predicate expression.
        /// </summary>
        /// <param name="predicate">The predicate expression for the property to match against. This should be a
        /// string with a proeply formatted hierarchy path value.</param>
        /// <returns>
        /// A <see cref="IMongoQuery" /> that matches descendants based on the predicate property specified.
        /// </returns>
        public IMongoQuery GetAllDescendants(Expression<Func<string>> predicate)
        {
            MemberExpression member = predicate.Body as MemberExpression;
            return Query.Matches(member.Member.Name, GetDescendantExpression(null));
        }

        /// <summary>
        /// Gets a <see cref="IMongoQuery" /> to assist in matching any documents that are one of the descendants
        /// of this current hierarchy path given the passed in predicate expression.
        /// </summary>
        /// <param name="predicate">The predicate expression for the property to match against.</param>
        /// <param name="levels">The levels.</param>
        /// <returns>
        /// A <see cref="IMongoQuery" /> that matches descendants based on the predicate property specified.
        /// </returns>
        public IMongoQuery GetDescendants(Expression<Func<HierarchyPath>> predicate, int? levels = null)
        {
            MemberExpression member = predicate.Body as MemberExpression;
            return Query.Matches(member.Member.Name, GetDescendantExpression(levels));
        }        

        /// <summary>
        /// Gets a <see cref="IMongoQuery" /> to assist in matching any documents that are one of the descendants
        /// of this current hierarchy path given the passed in predicate expression.
        /// </summary>
        /// <param name="predicate">The predicate expression for the property to match against. This should be a
        /// string with a proeply formatted hierarchy path value.</param>
        /// <param name="levels">The levels.</param>
        /// <returns>
        /// A <see cref="IMongoQuery" /> that matches descendants based on the predicate property specified.
        /// </returns>
        public IMongoQuery GetDescendants(Expression<Func<string>> predicate, int? levels = null)
        {
            MemberExpression member = predicate.Body as MemberExpression;
            return Query.Matches(member.Member.Name, GetDescendantExpression(levels));
        }

        /// <summary>
        /// Gets the descendant regular expression for use in queries.
        /// </summary>
        /// <param name="levels">The levels.</param>
        /// <returns>
        /// A <see cref="BsonRegularExpression" /> for matching descendent values.
        /// </returns>
        public BsonRegularExpression GetDescendantExpression(int? levels)
        {
            if (_null)
                return null;
            string pattern = string.Format(@"^{1}{0}{1}(.*?{1}){2}$", 
                Regex.Escape(_path), _separatorPathExpression,
                levels == null 
                ? string.Format("(.+?{0})+", _separatorPathExpression)
                : string.Format("([^{0}]+?{0})", _separatorPathExpression).Repeat(levels.Value));
            return new BsonRegularExpression(pattern, "i");
        }

        /// <summary>
        /// Gets the ancestor regular expression for use in queries.
        /// </summary>
        /// <returns>A <see cref="BsonRegularExpression"/> for matching ancestors.</returns>
        public BsonRegularExpression GetAncestorExpression()
        {
            if (_null || _root)
                return null;

            int level = _parts.Length;
            List<string> patterns = new List<string>(level - 1);

            HierarchyPath parent = GetParent();
            while (!parent._null)
            {
                patterns.Add(string.Format(@"(^{1}{0}{1}$)", Regex.Escape(parent._path), _separatorPathExpression));
                parent = parent.GetParent();
            }

            if (!patterns.Any())
                return null;

            // They are automatically ordered from most specific to least
            return new BsonRegularExpression(string.Join("|", patterns), "i");
        }

        /// <summary>
        /// Gets the ancestors of the current path.
        /// </summary>
        /// <param name="includeSelf">if set to <c>true</c> [include self].</param>
        /// <param name="minLevel">The minimum level to obtain up to.</param>
        /// <returns></returns>
        public IEnumerable<HierarchyPath> GetAncestors(bool includeSelf = false, int? minLevel = null)
        {
            if (!_null)
            {
                if (includeSelf)
                    yield return this;
                HierarchyPath parent = GetParent();
                while (!parent._null && (minLevel ?? -1) <= parent.GetLevel())
                {
                    yield return parent;
                    parent = parent.GetParent();
                }
            }
        }

        #endregion Public Methods

        #region Public Static Methods

        /// <summary>
        /// Parses the specified path into a <see cref="HierarchyPath" /> instance.
        /// </summary>
        /// <param name="path">The path to parse.</param>
        /// <returns>
        /// The parsed instance of <see cref="HierarchyPath" />.
        /// </returns>
        public static HierarchyPath Parse(string path)
        {
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Tries to parse the given path into a <see cref="HierarchyPath" /> instance.
        /// </summary>
        /// <param name="path">The path to parse.</param>
        /// <param name="result">The resulting path if parse is successful, otherwise <c>HierarchyPath.Null</c>.</param>
        /// <returns>
        ///   <c>true</c> if the parse was successful and without error, otherwise, <c>false</c>.
        /// </returns>
        public static bool TryParse(string path, out HierarchyPath result)
        {
            result = HierarchyPath.Null;
            string s;
            if (isPathValid(path, out s))
            {
                try
                {
                    result = new HierarchyPath(s);
                    return true;
                }
                catch { return false; }
            }
            return false;
        }

        /// <summary>
        /// Gets a <see cref="HierarchyPath" /> instance with the specified identifier as the Root.
        /// </summary>
        /// <param name="id">The identifier to set as the root of the path.</param>
        /// <returns>
        /// A <see cref="HierarchyPath" /> instance.
        /// </returns>
        public static HierarchyPath Root(string id)
        {
            string path = string.Concat(pathSeparator, id, pathSeparator);
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns>A copy of the current instance.</returns>
        public static HierarchyPath Copy(HierarchyPath path)
        {
            return new HierarchyPath(path._path);
        }

        #endregion Public Static Methods

        #region Private Methods

        /// <summary>
        /// Determines whether the specified path is syntactically valid.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        /// <returns><c>true</c> if the specified path is syntactically valid, otherwise <c>false</c>.</returns>
        private static bool isPathValid(string path, out string text)
        {
            text = path;
            if (text != null && text.Length > 0)
            {
                if (string.IsNullOrWhiteSpace(text) || text[text.Length - 1] != pathSeparator || text[0] != pathSeparator)
                    return false;
                
                if (text.Length == 1 && text[0] == pathSeparator)
                    return false;
            }
            else
                if (string.IsNullOrWhiteSpace(text))
                    text = null;
            return true;
        }

        #endregion Private Methods

        #region Operators

        /// <summary>
        /// Performs an implicit conversion from <see cref="System.String"/> to <see cref="HierarchyPath"/>.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator HierarchyPath(string path)
        {
            return new HierarchyPath(path);
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="HierarchyPath"/> to <see cref="System.String"/>.
        /// </summary>
        /// <param name="hierarchy">The hierarchy.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator string(HierarchyPath hierarchy)
        {
            return hierarchy._path;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(HierarchyPath a, HierarchyPath b)
        {
            return a._path == b._path;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(HierarchyPath a, HierarchyPath b)
        {
            return a._path != b._path;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(string a, HierarchyPath b)
        {
            return a == b._path;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(string a, HierarchyPath b)
        {
            return a != b._path;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(HierarchyPath a, string b)
        {
            return a._path == b;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(HierarchyPath a, string b)
        {
            return a._path != b;
        }

        #endregion Operators

        #region System.Object Overrides

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (IsNull)
                return base.ToString();
            return _path;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (this._path == null) return 0;
            return this._path.GetHashCode();
        }

        #endregion System.Object Overrides

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("_path", _path);
            info.AddValue("_id", _id);
            info.AddValue("_parts", _parts);
            info.AddValue("_null", _null);
            info.AddValue("_root", _root);
        }

        #endregion ISerializable

        #region IComparable, IComparable<HierarchyPath>, IComparable<string>

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" /> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in the sort order.
        /// </returns>
        public int CompareTo(object obj)
        {
            if (obj is string)
                return this.CompareTo(obj as string);
            return this.CompareTo((HierarchyPath)obj);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(HierarchyPath other)
        {
            return string.Compare(_path, other._path);
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(string other)
        {
            return string.Compare(_path, other);
        }

        #endregion IComparable, IComparable<HierarchyPath>, IComparable<string>

        #region IEquatable<HierarchyPath>, IEquatable<string>

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(HierarchyPath other)
        {
            return string.Equals(_path, other._path);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(string other)
        {
            return string.Equals(_path, other);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is HierarchyPath)
                return this.Equals((HierarchyPath)obj);
            if (obj is string)
                return this.Equals(obj as string);
            return base.Equals(obj);
        }

        #endregion IEquatable<HierarchyPath>, IEquatable<string>

        #region IEnumerable<string>, IEnumerable

        /// <summary>
        /// Returns an enumerator that iterates through the collection of path parts.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<string> GetEnumerator()
        {
            if (_null) return Enumerable.Empty<string>().GetEnumerator();
            return _parts.AsEnumerable().GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection of path parts.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            if (_null) return Enumerable.Empty<string>().GetEnumerator();
            return this._parts.GetEnumerator();
        }

        #endregion IEnumerable<string>, IEnumerable

        #region IConvertible

        /// <summary>
        /// Returns the <see cref="T:System.TypeCode" /> for this instance.
        /// </summary>
        /// <returns>
        /// The enumerated constant that is the <see cref="T:System.TypeCode" /> of the class or value type that implements this interface.
        /// </returns>
        public TypeCode GetTypeCode()
        {
            return TypeCode.String;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance; no actual conversion is performed.
        /// </summary>
        /// <param name="provider">(Reserved) An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(IFormatProvider provider)
        {
            // provider is ignored
            return ToString();
        }

        /// <summary>
        /// Converts the value of this instance to an <see cref="T:System.Object" /> of the specified <see cref="T:System.Type" /> that has an equivalent value, using the specified culture-specific formatting information.
        /// </summary>
        /// <param name="conversionType">The <see cref="T:System.Type" /> to which the value of this instance is converted.</param>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> instance of type <paramref name="conversionType" /> whose value is equivalent to the value of this instance.
        /// </returns>
        /// <exception cref="System.InvalidCastException">Invalid cast from <see cref="HierarchyPath"/> to <paramref name="conversionType"/>.</exception>
        public object ToType(Type conversionType, IFormatProvider provider)
        {
            if (conversionType == this.GetType())
                return this;
            if (conversionType == typeof(string))
                return ToString();
            if (conversionType == typeof(BsonValue) || conversionType == typeof(BsonString))
                return new BsonString(ToString());
            throw new InvalidCastException(string.Format(CultureInfo.CurrentCulture, "Invalid cast from '{0}' to '{1}'.", new object[]
	        {
		        this.GetType().FullName,
		        conversionType.FullName
	        }));
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a Boolean.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a Boolean.</exception>
        public bool ToBoolean(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a Boolean.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to an 8-bit unsigned integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to an 8-bit unsigned integer.</exception>
        public byte ToByte(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to an 8-bit unsigned integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a unicode character.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a unicode character.</exception>
        public char ToChar(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a unicode character.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a DateTime.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a DateTime.</exception>
        public DateTime ToDateTime(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a DateTime.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a Decimal.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a Decimal.</exception>
        public decimal ToDecimal(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a Decimal.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a double-precision floating-point number.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a double-precision floating-point number.</exception>
        public double ToDouble(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a double-precision floating-point number.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 16-bit signed integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 16-bit signed integer.</exception>
        public short ToInt16(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a 16-bit signed integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 32-bit signed integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 32-bit signed integer.</exception>
        public int ToInt32(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a 32-bit signed integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 64-bit signed integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 64-bit signed integer.</exception>
        public long ToInt64(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a 64-bit signed integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to an 8-bit signed integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to an 8-bit signed integer.</exception>
        public sbyte ToSByte(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to an 8-bit signed integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a single-precision floating-point number.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a single-precision floating-point number.</exception>
        public float ToSingle(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a single-precision floating-point number.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 16-bit unsigned integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 16-bit unsigned integer.</exception>
        public ushort ToUInt16(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a 16-bit unsigned integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 32-bit unsigned integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 32-bit unsigned integer.</exception>
        public uint ToUInt32(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to a 32-bit unsigned integer.");
        }

        /// <summary>
        /// Not Supported. You cannot convert from a HierarchyPath to a 64-bit unsigned integer.
        /// </summary>
        /// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotImplementedException">You cannot convert from a HierarchyPath to a 64-bit unsigned integer..</exception>
        public ulong ToUInt64(IFormatProvider provider)
        {
            throw new NotImplementedException("You cannot convert from a HierarchyPath to UInt64.");
        }

        #endregion IConvertible

        #region ICloneable

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return this;
        }

        #endregion
    }
}
