﻿using System.Collections.Generic;
using Npgsql;

namespace AbsenceSoft.Common.Postgres
{
    /// <summary>
    /// ICommandRunner is a wrapper interface for data access to Postgres.
    /// Based on: https://github.com/robconery/pg-dvdrental/blob/master/membership/DataAccess/ICommandRunner.cs
    /// </summary>
    public interface ICommandRunner
    {
        string ConnectionStringName { get; set; }
        NpgsqlCommand BuildCommand(string sql, params object[] args);
        NpgsqlDataReader OpenReader(string sql, params object[] args);
        T ExecuteSingle<T>(string sql, params object[] args) where T : new();
        dynamic ExecuteSingleDynamic(string sql, params object[] args);
        IEnumerable<T> Execute<T>(string sql, params object[] args) where T : new();
        IEnumerable<dynamic> ExecuteDynamic(string sql, params object[] args);
        List<int> Transact(params NpgsqlCommand[] cmds);
    }
}
