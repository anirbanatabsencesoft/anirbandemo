﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using MongoDB.Bson;

namespace AbsenceSoft.Common.Postgres
{
    /// <summary>
    /// CommandExtensions are extension methods used for CommandRunner to help make access to Postgres simple.ICommandRunner is a wrapper interface for data access to Postgres.
    /// Based on: https://github.com/robconery/pg-dvdrental/blob/master/membership/DataAccess/Extensions.cs
    /// </summary>
    public static class CommandExtensions
    {
        /// <summary>
        /// Returns a single instance of type T from the data reader.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="reader">The data reader to pull the return type from.</param>
        /// <returns>A single instance of type T.</returns>
        public static T ToSingle<T>(this IDataReader reader) where T : new()
        {
            var item = new T();
            var properties = item.GetType().GetProperties();

            foreach (var propertyInfo in properties)
            {
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    if (!reader.GetName(i).Equals(propertyInfo.Name, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    var value = reader.GetValue(i);
                    propertyInfo.SetValue(item, value == DBNull.Value ? null : value);
                }
            }

            return item;
        }

        /// <summary>
        /// Returns a list of instances of type T from the data reader.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="reader">The data reader to pull the return type from.</param>
        /// <returns>A list of type T.</returns>
        public static List<T> ToList<T>(this IDataReader reader) where T : new()
        {
            var result = new List<T>();

            while (reader.Read())
                result.Add(reader.ToSingle<T>());

            return result;
        }

        /// <summary>
        /// Extension for adding a single paramter.
        /// </summary>
        /// <param name="command">The sql command object.</param>
        /// <param name="item">The item to add as a parameter.</param>
        public static void AddParam(this IDbCommand command, object item)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = string.Format("@{0}", command.Parameters.Count);

            if (item == null)
            {
                parameter.Value = DBNull.Value;
                command.Parameters.Add(parameter);
                return;
            }

            if (item is Guid)
            {
                parameter.Value = item.ToString();
                parameter.DbType = DbType.String;
                parameter.Size = 4000;
                command.Parameters.Add(parameter);
                return;
            }

            parameter.Value = item;
            var str = item as string;
            if (str != null)
                parameter.Size = str.Length > 4000 ? -1 : 4000;

            command.Parameters.Add(parameter);
        }

        /// <summary>
        /// Extension method for adding in a bunch of parameters.
        /// </summary>
        /// <param name="command">The sql command object.</param>
        /// <param name="args">The items to add as a parameter.</param>
        public static void AddParams(this IDbCommand command, params object[] args)
        {
            foreach (var item in args)
                AddParam(command, item);
        }

        /// <summary>
        /// Turns an IDataReader record into an ExpandoObject.
        /// </summary>
        /// <param name="reader">The IDataReader to convert a record from.</param>
        /// <returns>A dynamic ExpandoObject from a record.</returns>
        public static dynamic RecordToExpando(this IDataReader reader)
        {
            dynamic expando = new ExpandoObject();
            var dictionary = expando as IDictionary<string, object>;

            for (var i = 0; i < reader.FieldCount; i++)
                dictionary.Add(reader.GetName(i), DBNull.Value.Equals(reader[i]) ? null : reader[i]);

            return expando;
        }

        /// <summary>
        /// Turns an IDataReader record into a DynamicAwesome object.
        /// </summary>
        /// <param name="reader">The IDataReader to convert a record from.</param>
        /// <returns>A DynamicAwesome from a record.</returns>
        public static DynamicAwesome RecordToDynamicAwesome(this IDataReader reader)
        {
           var dictionary = new Dictionary<string, object>();

            for (var i = 0; i < reader.FieldCount; i++)
                dictionary.Add(reader.GetName(i), DBNull.Value.Equals(reader[i]) ? null : reader[i]);

            return new DynamicAwesome(dictionary.ToBsonDocument());
        }

        /// <summary>
        /// Turns an IDataReader into a dynamic list of things.
        /// </summary>
        /// <param name="reader">The IDataReader to convert.</param>
        /// <returns>A dynamic list.</returns>
        public static List<dynamic> ToExpandoList(this IDataReader reader)
        {
            var result = new List<dynamic>();

            while (reader.Read())
                result.Add(reader.RecordToExpando());

            return result;
        }
    }
}
