﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Npgsql;

namespace AbsenceSoft.Common.Postgres
{
    /// <summary>
    /// CommandRunner is a wrapper interface for data access to Postgres.
    /// Based on: https://github.com/robconery/pg-dvdrental/blob/master/membership/DataAccess/CommandRunner.cs
    /// </summary>
    public class CommandRunner : ICommandRunner
    {
        public string ConnectionStringName { get; set; }

        private string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString; }
        }

        /// <summary>
        /// Constructor - takes a connection string name.
        /// </summary>
        /// <param name="connectionStringName">Connection name as defined in your app.config or web.config.</param>
        public CommandRunner(string connectionStringName)
        {
            ConnectionStringName = connectionStringName;
        }

        /// <summary>
        /// Convenience method for building a command
        /// </summary>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>A NpgsqlCommand.</returns>
        public NpgsqlCommand BuildCommand(string sql, params object[] args)
        {
            var command = new NpgsqlCommand(sql);
            command.AddParams(args);
            return command;
        }

        /// <summary>
        /// Opens a connection and returns a data reader. 
        /// </summary>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns></returns>
        public NpgsqlDataReader OpenReader(string sql, params object[] args)
        {
            var connection = new NpgsqlConnection(ConnectionString);
            var command = BuildCommand(sql, args);
            command.Connection = connection;
            //Fix to run custom functions passed to query 
            command.CommandText = command.CommandText.Replace("\"\"", "");
            // defer opening to the last minute
            connection.Open();
            // use a reader here and yield back the projection
            // connection will close when reader is finished
            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }
        
        /// <summary>
        /// Returns a single record, typed as you need.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>A single record of type T.</returns>
        public T ExecuteSingle<T>(string sql, params object[] args) where T : new()
        {
            return Execute<T>(sql, args).First();
        }

        /// <summary>
        /// Returns a simple ExpandoObject with all results of a query.
        /// </summary>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>A single ExpandoObject.</returns>
        public dynamic ExecuteSingleDynamic(string sql, params object[] args)
        {
            return ExecuteDynamic(sql, args).First();
        }

        /// <summary>
        /// Executes a typed query.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>An enumeration of type T.</returns>
        public IEnumerable<T> Execute<T>(string sql, params object[] args) where T : new()
        {
            using (var reader = OpenReader(sql, args))
            {
                while (reader.Read())
                    yield return reader.ToSingle<T>();
            }
        }

        /// <summary>
        /// Executes a query returning items in an enumeration dynamic.
        /// </summary>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>An enumeration of ExpandoObject.</returns>
        public IEnumerable<dynamic> ExecuteDynamic(string sql, params object[] args)
        {
            using (var reader = OpenReader(sql, args))
            {
                while (reader.Read())
                    yield return reader.RecordToExpando();
            }
        }

        /// <summary>
        /// Executes a query returning items in an enumeration of DynamicAwesome.
        /// </summary>
        /// <param name="sql">The SQL to execute with param names as @0, @1, @2 etc.</param>
        /// <param name="args">The parameters to match the @ notations.</param>
        /// <returns>An enumeration of DynamicAwesome.</returns>
        public IEnumerable<DynamicAwesome> ExecuteDynamicAwesome(string sql, params object[] args)
        {
            using (var reader = OpenReader(sql, args))
            {
                while (reader.Read())
                    yield return reader.RecordToDynamicAwesome();
            }
        }

        /// <summary>
        /// A transaction helper that executes a series of commands in a single transaction.
        /// </summary>
        /// <param name="commands">Commands built with BuildCommand.</param>
        /// <returns>Number of recoreds effected for each command within the transaction.</returns>
        public List<int> Transact(params NpgsqlCommand[] commands)
        {
            var results = new List<int>();
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var command in commands)
                        {
                            command.Transaction = transaction;
                            command.Connection = connection;
                            results.Add(command.ExecuteNonQuery());
                        }

                        transaction.Commit();
                    }
                    catch (NpgsqlException)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }
    }
}
