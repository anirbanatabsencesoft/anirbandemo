﻿using AbsenceSoft.Common.Serializers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbsenceSoft
{
    /// <summary>
    /// Represents a date range in the application and will be used for future storage or start/end date thingies.
    /// </summary>
    [Serializable]
    public sealed class DateRange : IEquatable<DateRange>, IComparable<DateRange>, IComparable<DateTime>
    {
        /// <summary>
        /// The start date.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        private DateTime? _startDate;

        /// <summary>
        /// The end date.
        /// </summary>
        [BsonIgnore, JsonIgnore]
        private DateTime? _endDate;

        /// <summary>
        /// Initializes a new instance of the <see cref="DateRange"/> struct.
        /// </summary>
        /// <param name="StartDate">The start date.</param>
        /// <param name="EndDate">The end date.</param>
        public DateRange(DateTime StartDate, DateTime? EndDate = null)
        {
            if (StartDate > DateTime.MinValue)
                _startDate = StartDate;
            else
                _startDate = null;
            _endDate = EndDate;
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [BsonRequired]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime StartDate {
            get { return _startDate ?? DateTime.MinValue; }
            set
            {
                if (value > DateTime.MinValue)
                    _startDate = value;
                else
                    _startDate = null;
            }
        }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime? EndDate { get { return _endDate; } set { _endDate = value; } }

        /// <summary>
        /// Gets the elapsed timeframe from the start date to the end date. If the end date is null (perpetual) then
        /// returns a timespan of zero.
        /// </summary>
        /// <value>
        /// The elapsed timeframe or <c>TimeSpan.Zero</c>.
        /// </value>
        [BsonIgnore]
        public TimeSpan Timeframe { get { return !IsNull && _endDate.HasValue ? (_endDate.Value - _startDate.Value) : TimeSpan.Zero; } }

        /// <summary>
        /// Gets a value indicating whether this instance is perpetual (it goes on forever and ever and ever until happily ever after).
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is perpetual; otherwise, <c>false</c>.
        /// </value>
        [BsonIgnore]
        public bool IsPerpetual { get { return !IsNull && !EndDate.HasValue; } }

        /// <summary>
        /// Gets a value indicating whether this instance is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is null; otherwise, <c>false</c>.
        /// </value>
        public bool IsNull { get { return !_startDate.HasValue; } }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return ToString("(perpetual)");
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="perpetualText">The perpetual text.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(string perpetualText)
        {
            if (IsNull)
                return string.Empty;

            StringBuilder me = new StringBuilder(_startDate.ToString("MM/dd/yyyy"));
            if (_endDate.HasValue)
                me.AppendFormat(" - {0:MM/dd/yyyy}", _endDate);
            else
                me.AppendFormat(" {0}", perpetualText ?? "(perpetual)");
            return me.ToString();
        }

        /// <summary>
        /// Calculate the age in years for a date to a target date
        /// </summary>
        /// <returns>
        /// The age in years, -1 if TargetDate is &lt;= dob
        /// </returns>
        public int AgeInYears()
        {
            int rtVal = -1;

            // this should work for leap years
            if (!IsNull && StartDate < EndDate)
            {
                rtVal = EndDate.Value.Year - StartDate.Year;
                if (EndDate.Value.Month < StartDate.Month || (EndDate.Value.Month == StartDate.Month && EndDate.Value.Day < StartDate.Day))
                    rtVal--;
            }

            return rtVal;
        }

        /// <summary>
        /// Alls the years in the specified range. If the end date is <c>null</c> then
        /// only the start date's year is returned.
        /// </summary>
        /// <returns></returns>
        public List<int> AllYearsInRange()
        {
            if (IsNull)
                return new List<int>(0);

            if (!EndDate.HasValue)
                return new List<int>(1) { StartDate.Year };

            DateTime s = StartDate;
            DateTime e = EndDate.Value;
            if (s > e)
            {
                s = EndDate.Value;
                e = StartDate;
            }
            if (s == e)
                return new List<int>(1) { StartDate.Year };
            if (s.Year == e.Year)
                return new List<int>(1) { StartDate.Year };

            List<int> years = new List<int>((EndDate.Value.Year - StartDate.Year) + 1);
            int cur = StartDate.Year;
            while (cur <= EndDate.Value.Year)
            {
                years.Add(cur);
                cur++;
            }

            return years.Distinct().OrderBy(y => y).ToList();
        }

        /// <summary>
        /// Test if two date ranges overlap.
        /// If an end date is null it is defaulted to DateTime.MaxValue
        /// Dates are inclusive, It feels a bit weird having this as an extension
        /// but we would need some sort of singleton date function object instead so I am
        /// making due
        /// </summary>
        /// <param name="inRangeStartDate">Start date of the range to test against</param>
        /// <param name="inRangeEndDate">The end date of the range to test against</param>
        /// <returns></returns>
        public bool DateRangesOverLap(DateTime inRangeStartDate, DateTime? inRangeEndDate)
        {
            if (IsNull)
                return false;

            // unnull everthing
            DateTime ted = EndDate ?? DateTime.MaxValue;
            DateTime ired = inRangeEndDate ?? DateTime.MaxValue;

            return StartDate.DateInRange(inRangeStartDate, ired) ||
                ted.DateInRange(inRangeStartDate, ired) ||
                inRangeStartDate.DateInRange(StartDate, ted) ||
                ired.DateInRange(StartDate, ted);

            /*
            // if the source start is between the start and end of the target, or the end date is between the start and
            // end of the target then they overlap
            return (thisStartDate >= inRangeStartDate && thisStartDate < ired) ||
                    (ted >= inRangeStartDate && ted <= ired);
            */
        }

        /// <summary>
        /// Test if two date ranges overlap.
        /// If an end date is null it is defaulted to DateTime.MaxValue
        /// Dates are inclusive, It feels a bit weird having this as an extension
        /// but we would need some sort of singleton date function object instead so I am
        /// making due
        /// </summary>
        /// <param name="compare">The comparison date range.</param>
        /// <returns></returns>
        public bool DateRangesOverLap(DateRange compare)
        {
            var o = (compare ?? DateRange.Null);

            if (IsNull && (o == null || o.IsNull))
                return true;
            if (IsNull || o == null || o.IsNull)
                return false;

            return DateRangesOverLap(o.StartDate, o.EndDate);
        }

        /// <summary>
        /// Compare the date to a range and return true if the date
        /// is between the two dates (inclusive)
        /// </summary>
        /// <param name="theDate">The date to check</param>
        /// <returns>
        /// true if the date is in the range (inclusive)
        /// </returns>
        public bool DateInRange(DateTime theDate)
        {
            if (IsNull)
                return false;

            return theDate >= StartDate && theDate <= (EndDate ?? DateTime.MaxValue);
        }

        /// <summary>
        /// Determines whether this range includes today's date.
        /// </summary>
        /// <returns></returns>
        public bool IncludesToday(bool toMidnight = false)
        {
            if (IsNull)
                return false;

            return (toMidnight ? DateInRange(DateTime.UtcNow.ToMidnight()) : DateInRange(DateTime.UtcNow));
        }

        /// <summary>
        /// Determines whether this range includes date.
        /// </summary>
        /// <returns></returns>
        public bool IncludesDate(DateTime date)
        {
            if (IsNull)
                return false;

            return DateInRange(date.ToMidnight());
        }

        /// <summary>
        /// Gets all of the dates in range between the 2 dates.
        /// </summary>
        /// <returns>
        /// A list of dates between the 2 dates, inclusive of the start and end date.
        /// </returns>
        public List<DateTime> AllDatesInRange()
        {
            if (IsNull)
                return new List<DateTime>(0);

            if (EndDate == StartDate || EndDate == null)
                return new List<DateTime>(1) { StartDate.ToMidnight() };

            DateTime realStart = StartDate.ToMidnight();
            DateTime realEnd = EndDate.Value.ToMidnight();
            if (EndDate < StartDate)
            {
                realStart = EndDate.Value;
                realEnd = StartDate;
            }

            List<DateTime> dates = new List<DateTime>();

            while (StartDate <= EndDate)
                StartDate = dates.AddFluid(StartDate).AddDays(1);

            return dates;
        }

        /// <summary>
        /// Gets the null/empty date range value.
        /// </summary>
        /// <value>
        /// The null.
        /// </value>
        public static DateRange Null { get { return new DateRange(DateTime.MinValue, null); } }

        #region IEquatable<T>

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return IsNull;
            if (!(obj is DateRange))
                return false;
            return Equals((DateRange)obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(DateRange other)
        {
            return (other ?? DateRange.Null)._startDate.Equals(_startDate) && (other ?? DateRange.Null)._endDate.Equals(_endDate);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return _startDate.GetHashCode() + _endDate.GetHashCode();
        }

        #endregion IEquatable<T>

        #region IComparable<T>

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(DateRange other)
        {
            var o = (other ?? DateRange.Null);
            // Start dates being equal
            if (_startDate == o._startDate)
            {
                // End dates being the same, guess what, this is equal
                if (_endDate == o._endDate)
                    return 0;
                // Tie is settled by the end date, in this case comparison is backwards
                if (_endDate.HasValue && o._endDate.HasValue)
                    return EndDate.Value.CompareTo(o.EndDate.Value) * -1;
                // Just and end date for this instance, that means the other one is perpetual and is greater
                if (_endDate.HasValue)
                    return -1;
                // This instance is perpetual and therefore greater
                return 1;
            }
            // Both start dates having a value then do a direct comparison on the start dates
            if (_startDate.HasValue && o._startDate.HasValue)
                return StartDate.CompareTo(o.StartDate);
            // Only this instance has a start date, therefore it is greater
            if (_startDate.HasValue)
                return 1;
            // Otherwise, this instance is less than
            return -1;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(DateTime other)
        {
            // If this instance is null, then obviously it's less than
            if (IsNull)
                return -1;

            // If this date is in range, then it's the same/zero
            if (DateInRange(other))
                return 0;

            // If this date is before the date ranges start date, then this instance is greater than other
            if (other < StartDate)
                return 1;

            // Otherwise, this instance is less than
            return -1;
        }

        #endregion IComparable<T>

        #region Operators

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(DateRange left, DateRange right)
        {
            return (left ?? DateRange.Null)._startDate.Equals((right ?? DateRange.Null)._startDate) && (left ?? DateRange.Null)._endDate.Equals((right ?? DateRange.Null)._endDate);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(DateRange left, DateRange right)
        {
            return !(left ?? DateRange.Null)._startDate.Equals((right ?? DateRange.Null)._startDate) || !(left ?? DateRange.Null)._endDate.Equals((right ?? DateRange.Null)._endDate);
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(DateRange left, DateRange right)
        {
            return (left ?? DateRange.Null).CompareTo((right ?? DateRange.Null)) < 0;
        }

        /// <summary>
        /// Implements the operator &lt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <=(DateRange left, DateRange right)
        {
            return (left ?? DateRange.Null).CompareTo((right ?? DateRange.Null)) <= 0;
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(DateRange left, DateRange right)
        {
            return (left ?? DateRange.Null).CompareTo((right ?? DateRange.Null)) > 0;
        }

        /// <summary>
        /// Implements the operator &gt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >=(DateRange left, DateRange right)
        {
            return (left ?? DateRange.Null).CompareTo(right) >= 0;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) == 0;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) != 0;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) < 0;
        }

        /// <summary>
        /// Implements the operator &lt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <=(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) <= 0;
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) > 0;
        }

        /// <summary>
        /// Implements the operator &gt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >=(DateRange left, DateTime right)
        {
            return (left ?? DateRange.Null).CompareTo(right) >= 0;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) == 0;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) != 0;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) < 0;
        }

        /// <summary>
        /// Implements the operator &lt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <=(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) <= 0;
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) > 0;
        }

        /// <summary>
        /// Implements the operator &gt;=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >=(DateTime left, DateRange right)
        {
            return (right ?? DateRange.Null).CompareTo(left) >= 0;
        }

        #endregion
    }
}
