﻿using System;
using System.Security.Principal;

namespace AbsenceSoft.Common
{
    public class BasicIdentity : IIdentity
    {
        public string AuthenticationType { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Name { get; set; }
    }
}