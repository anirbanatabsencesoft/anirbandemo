﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    /// <summary>
    /// Random string generator
    /// </summary>
    [Serializable]
    public class RandomString
    {
        private static Random _r = new Random();
        private const string UPPERCASE = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        private const string LOWERCASE = "abcdefghijkmnopqrstuvwxyz";
        private const string NUMBERS = "0123456789";
        private const string SYMBOLS = @"~`!@#$%^&*()-_=+<>?:,./\[]{}|'";

        /// <summary>
        /// Prevents a default instance of the <see cref="RandomString"/> class from being created.
        /// </summary>
        private RandomString() { }

        /// <summary>
        /// Nexts the string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="lowerCase">if set to <c>true</c> [lower case].</param>
        /// <param name="upperCase">if set to <c>true</c> [upper case].</param>
        /// <param name="numbers">if set to <c>true</c> [numbers].</param>
        /// <param name="symbols">if set to <c>true</c> [symbols].</param>
        /// <param name="spaces">if set to <c>true</c> [spaces].</param>
        /// <returns></returns>
        private string NextString(int length, bool lowerCase, bool upperCase, bool numbers, bool symbols, bool spaces)
        {
            char[] charArray = new char[length];
            StringBuilder charPool = new StringBuilder();

            //Build character pool
            if (lowerCase)
                charPool.Append(LOWERCASE);

            if (upperCase)
                charPool.Append(UPPERCASE);

            if (numbers)
                charPool.Append(NUMBERS);

            if (symbols)
                charPool.Append(SYMBOLS);

            if (spaces)
                charPool.Append(" ");


            //Build the output character array
            for (int i = 0; i < charArray.Length; i++)
            {
                //Pick a random integer in the character pool
                int index = _r.Next(0, charPool.Length);

                //Set it to the output character array
                charArray[i] = charPool[index];
            }

            return new string(charArray);
        }

        /// <summary>
        /// Generates the specified length.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="mixedCase">if set to <c>true</c> [mixed case].</param>
        /// <param name="includeNumbers">if set to <c>true</c> [include numbers].</param>
        /// <param name="includeSpecialCharacters">if set to <c>true</c> [include special characters].</param>
        /// <param name="includeSpaces">if set to <c>true</c> [include spaces].</param>
        /// <returns></returns>
        public static string Generate(int length, bool mixedCase = true, bool includeNumbers = true, bool includeSpecialCharacters = false, bool includeSpaces = false)
        {
            return new RandomString().NextString(length, true, mixedCase, includeNumbers, includeSpecialCharacters, includeSpaces);
        }

        /// <summary>
        /// Generates the specified length.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="lowerCase">if set to <c>true</c> [lower case].</param>
        /// <param name="upperCase">if set to <c>true</c> [upper case].</param>
        /// <param name="includeNumbers">if set to <c>true</c> [include numbers].</param>
        /// <param name="includeSpecialCharacters">if set to <c>true</c> [include special characters].</param>
        /// <param name="includeSpaces">if set to <c>true</c> [include spaces].</param>
        /// <returns></returns>
        public static string Generate(int length, bool lowerCase = true, bool upperCase = true, bool includeNumbers = true, bool includeSpecialCharacters = false, bool includeSpaces = false)
        {
            return new RandomString().NextString(length, lowerCase, upperCase, includeNumbers, includeSpecialCharacters, includeSpaces);
        }

        /// <summary>
        /// Generates the key.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GenerateKey(int length)
        {
            StringBuilder key = new StringBuilder();
            while (key.Length < length)
                key.Append(Path.GetRandomFileName()).Replace(".", "");
            return key.Remove(length, key.Length - length).ToString();
        }

        /// <summary>
        /// Generates the secret.
        /// </summary>
        /// <returns></returns>
        public static string GenerateSecret()
        {
            StringBuilder pwd = new StringBuilder();
            pwd.Append(Guid.NewGuid());
            pwd.Append(Path.GetRandomFileName());
            byte[] encoded = Encoding.UTF8.GetBytes(pwd.ToString());
            return Convert.ToBase64String(encoded);
        }

        /// <summary>
        /// Generates a unique ID based on MongoDB's ObjectId type with some added fun.
        /// </summary>
        /// <returns></returns>
        public static string GenerateUniqueId()
        {
            //var rand = new RandomString();
            StringBuilder id = new StringBuilder();
            // Generate an Alpha Prefix
            //id.Append(rand.NextString(2, false, true, false, false, false));
            id.Append(MongoDB.Bson.ObjectId.GenerateNewId().GetHashCode().ToString().Replace("-", ""));
            // Generate an Alpha Suffix
            //id.Append(rand.NextString(1, false, true, false, false, false));
            return id.ToString();
        }
    }
}
