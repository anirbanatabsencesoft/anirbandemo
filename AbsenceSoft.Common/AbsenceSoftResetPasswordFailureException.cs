﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Common
{
    [Serializable]
    [ComVisible(true)]
    public class AbsenceSoftResetPasswordFailureException : AbsenceSoftException
    {
        private const string AbsenceSoftResetPasswordFailureMessage = "Reset Password error. Seems you have expired Reset Password link.";

        public AbsenceSoftResetPasswordFailureException() : base(AbsenceSoftResetPasswordFailureMessage)
        { }
    }
}