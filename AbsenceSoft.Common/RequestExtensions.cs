﻿using System.Collections.Specialized;
using System.Web;

namespace AbsenceSoft
{
    public static class RequestExtensions
    {
        /// <summary>
        /// Gets the client's true IP Address especially behind a firewall, load-balancer, or proxy server.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static string ClientIPAddress(this HttpRequest request)
        {
            request = request ?? (HttpContext.Current == null ? null : HttpContext.Current.Request);
            if (request == null || request.IsLocal)
                return "127.0.0.1";

            string ipaddress = string.Empty;

            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.ServerVariables.GetClientIPAddressFromNameValueCollection();
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.Headers.GetClientIPAddressFromNameValueCollection();
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.UserHostAddress;

            return ipaddress;
        }

        /// <summary>
        /// Gets the client's true IP Address especially behind a firewall, load-balancer, or proxy server.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static string ClientIPAddress(this HttpRequestBase request)
        {
            if (request == null)
                return ClientIPAddress((HttpRequest)null);

            if (request.IsLocal)
                return "127.0.0.1";

            string ipaddress = string.Empty;

            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.ServerVariables.GetClientIPAddressFromNameValueCollection();
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.Headers.GetClientIPAddressFromNameValueCollection();
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = request.UserHostAddress;

            return ipaddress;
        }

        /// <summary>
        /// Gets the client ip address from server variables, header or null.
        /// </summary>
        /// <param name="nvc">The NVC.</param>
        /// <returns></returns>
        private static string GetClientIPAddressFromNameValueCollection(this NameValueCollection nvc)
        {
            if (nvc == null)
                return string.Empty;

            string ipaddress = string.Empty;

            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = nvc.FetchValue("Http_X_Forwarded_For");
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = nvc.FetchValue("X_Forwarded_For");
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = nvc.FetchValue("Http-X-Forwarded-For");
            if (string.IsNullOrWhiteSpace(ipaddress))
                ipaddress = nvc.FetchValue("X-Forwarded-For");

            return ipaddress;
        }

        /// <summary>
        /// Determines whether the specified request is secure.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        ///   <c>true</c> if the specified request is secure; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSecure(this HttpRequest request)
        {
            if (request == null || (request.IsLocal && !request.IsSecureConnection))
                return false;

            if (request.IsSecureConnection)
                return true;

            if (request.ServerVariables.IsSecure())
                return true;
            if (request.Headers.IsSecure())
                return true;

            return request.Url.Scheme.ToLowerInvariant() == "https";
        }

        /// <summary>
        /// Determines whether the specified request is secure.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        ///   <c>true</c> if the specified request is secure; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSecure(this HttpRequestBase request)
        {
            if (request == null || (request.IsLocal && !request.IsSecureConnection))
                return false;

            if (request.IsSecureConnection)
                return true;

            if (request.ServerVariables.IsSecure())
                return true;
            if (request.Headers.IsSecure())
                return true;

            return request.Url.Scheme.ToLowerInvariant() == "https";
        }

        /// <summary>
        /// Determines whether the specified NVC is secure.
        /// </summary>
        /// <param name="nvc">The name value collection to check.</param>
        /// <returns>
        ///   <c>true</c> if the specified NVC is secure; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsSecure(this NameValueCollection nvc)
        {
            if (nvc == null) return false;

            string scheme = null;
            
            if (string.IsNullOrWhiteSpace(scheme))
                scheme = nvc.FetchValue("Http_X_Forwarded_Proto");
            if (string.IsNullOrWhiteSpace(scheme))
                scheme = nvc.FetchValue("X_Forwarded_Proto");
            if (string.IsNullOrWhiteSpace(scheme))
                scheme = nvc.FetchValue("Http-X-Forwarded-Proto");
            if (string.IsNullOrWhiteSpace(scheme))
                scheme = nvc.FetchValue("X-Forwarded-Proto");

            return scheme != null && scheme.ToLowerInvariant() == "https";
        }


        /// <summary>
        /// Fetches the value for the specified key in the target name value collection first
        /// by literal match, then upper case and finally by lower case; otherwise <c>String.Empty</c>.
        /// </summary>
        /// <param name="nvc">The NVC.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private static string FetchValue(this NameValueCollection nvc, string key)
        {
            if (nvc == null || string.IsNullOrWhiteSpace(key))
                return string.Empty;

            string val = string.Empty;

            val = nvc[key];
            if (string.IsNullOrWhiteSpace(val))
                val = nvc[key.ToUpperInvariant()];
            if (string.IsNullOrWhiteSpace(val))
                val = nvc[key.ToLowerInvariant()];

            return val;
        }
    }
}
