﻿using System;
using System.Text;

namespace AbsenceSoft.Common.HttpEncoding
{
    /// <summary>
    /// Creating copy in case internal implementation changes. This is specific implementation, not entire implementation.
    /// Is needed for UrlEncodeNonAscii function to rip out unicode characters from file urls.
    /// http://referencesource.microsoft.com/#System.Web/Util/HttpEncoder.cs,b5c8b7b5bb004908,references
    /// </summary>
    public class UrlEncoder {
        private static readonly UrlEncoder DefaultEncoder = new UrlEncoder();
 
        internal static void InitializeOnFirstRequest() {
            // Instantiate the encoder if it hasn't already been created. Note that this isn't storing the returned encoder
            // anywhere; it's just priming the Lazy<T> so that future calls to the Value property getter will return quickly
            // without going back to config.

            var encoder = DefaultEncoder;
        }
 
        private static bool IsNonAsciiByte(byte b) {
            return b >= 0x7F || b < 0x20;
        }
 
        //  Helper to encode the non-ASCII url characters only
        public string UrlEncodeNonAscii(string str, Encoding e) {
            if (string.IsNullOrEmpty(str))
                return str;

            if (e == null)
                e = Encoding.UTF8;

            var bytes = e.GetBytes(str);
            var encodedBytes = UrlEncodeNonAscii(bytes, 0, bytes.Length, false /* alwaysCreateNewReturnValue */);
            return Encoding.ASCII.GetString(encodedBytes);
        }
 
        internal byte[] UrlEncodeNonAscii(byte[] bytes, int offset, int count, bool alwaysCreateNewReturnValue) {
            if (!ValidateUrlEncodingParameters(bytes, offset, count)) {
                return null;
            }
 
            var cNonAscii = 0;
 
            // count them first
            for (var i = 0; i < count; i++) {
                if (IsNonAsciiByte(bytes[offset + i]))
                    cNonAscii++;
            }
 
            // nothing to expand?
            if (!alwaysCreateNewReturnValue && cNonAscii == 0)
                return bytes;
 
            // expand not 'safe' characters into %XX, spaces to +s
            var expandedBytes = new byte[count + cNonAscii * 2];
            var pos = 0;
 
            for (var i = 0; i < count; i++) {
                var b = bytes[offset + i];
 
                if (IsNonAsciiByte(b)) {
                    expandedBytes[pos++] = (byte)'%';
                    expandedBytes[pos++] = (byte)HttpEncoderUtility.IntToHex((b >> 4) & 0xf);
                    expandedBytes[pos++] = (byte)HttpEncoderUtility.IntToHex(b & 0x0f);
                }
                else {
                    expandedBytes[pos++] = b;
                }
            }
 
            return expandedBytes;
        }

        internal static bool ValidateUrlEncodingParameters(byte[] bytes, int offset, int count)
        {
            if (bytes == null && count == 0)
                return false;

            if (bytes == null)
                throw new ArgumentNullException("bytes");

            if (offset < 0 || offset > bytes.Length)
                throw new ArgumentOutOfRangeException("offset");

            if (count < 0 || offset + count > bytes.Length)
                throw new ArgumentOutOfRangeException("count");

            return true;
        }
    }
}
