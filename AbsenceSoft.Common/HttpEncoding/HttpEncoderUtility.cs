﻿using System.Diagnostics;

namespace AbsenceSoft.Common.HttpEncoding
{
    /// <summary>
    /// Creating copy in case internal implementation changes. This is specific implementation, not entire implementation.
    /// Is needed for UrlEncodeNonAscii function to rip out unicode characters from file urls.
    /// http://referencesource.microsoft.com/#System.Web/Util/HttpEncoderUtility.cs,3a94b289b95ec9f2
    /// </summary>
    internal static class HttpEncoderUtility
    {
        public static int HexToInt(char h)
        {
            return (h >= '0' && h <= '9') ? h - '0' :
            (h >= 'a' && h <= 'f') ? h - 'a' + 10 :
            (h >= 'A' && h <= 'F') ? h - 'A' + 10 :
            -1;
        }

        public static char IntToHex(int n)
        {
            Debug.Assert(n < 0x10);

            if (n <= 9)
                return (char)(n + '0');

            return (char)(n - 10 + 'a');
        }

        // Set of safe chars, from RFC 1738.4 minus '+'
        public static bool IsUrlSafeChar(char ch)
        {
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9'))
                return true;

            switch (ch)
            {
                case '-':
                case '_':
                case '.':
                case '!':
                case '*':
                case '(':
                case ')':
                    return true;
            }

            return false;
        }

        //  Helper to encode spaces only
        internal static string UrlEncodeSpaces(string str)
        {
            if (str != null && str.IndexOf(' ') >= 0)
                str = str.Replace(" ", "%20");

            return str;
        }
    }
}
