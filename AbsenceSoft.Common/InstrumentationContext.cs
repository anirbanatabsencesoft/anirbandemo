﻿using AbsenceSoft.Common;
using AbsenceSoft.Common.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft
{
    /// <summary>
    /// Mode of instrumentation
    /// </summary>
    public enum InstrumentationMode : int
    {
        /// <summary>
        /// The off mode
        /// </summary>
        Off = 0x0,
        /// <summary>
        /// The on mode
        /// </summary>
        On = 0x1
    }

    public sealed class InstrumentationContext : IDisposable
    {
        private Metric _metric;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstrumentationContext"/> class.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        public InstrumentationContext(string methodName)
        {
            _metric = new Metric(methodName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstrumentationContext"/> class.
        /// </summary>
        /// <param name="methodFormat">The method format.</param>
        /// <param name="args">The arguments.</param>
        public InstrumentationContext(string methodFormat, params object[] args)
        {
            _metric = new Metric(string.Format(methodFormat, args));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (Settings.Default.InstrumentationMode == InstrumentationMode.On)
            {
                try
                {
                    _metric.Save(DateTime.UtcNow);
                }
                catch (Exception ex)
                {
                    Log.Error("Error recording metrics", ex);
                }
            }
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodName">Name of the method.</param>
        /// <returns></returns>
        public static InstrumentationContext GetContext<T>(string methodName)
        {
            return new InstrumentationContext(string.Format("{0}.{1}", typeof(T).Name, methodName));
        }
    }
}
