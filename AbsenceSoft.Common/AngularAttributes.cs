﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Common
{
    #region HTML5 Input Types

    public enum HTML5InputType
    {
        Color = 0,
        Date = 1,
        DateTime = 2,
        //datetime-local = 3,
        Email = 4,
        Month = 5,
        Number = 6,
        Range = 7,
        Search = 8,
        Tel = 9,
        Time = 10,
        Url = 11,
        Week = 12,
        Text = 13,
        Radio = 14,
        Password = 15,
        Checkbox = 16,
        Submit = 17,
        Hidden = 18,
        Select = 19
    }

    #endregion HTML5 Input Types

    /// <summary>
    /// When added to a property, indicates to the rendering engine to add a "required" attribute to the markup.
    /// </summary>
    public class AngularRequired : Attribute
    {
    }

    public class AngularInputType : Attribute
    {
        /// <summary>
        /// Public accessor for the input type
        /// </summary>
        public HTML5InputType InputType { get; internal set; }

        /// <summary>
        /// Creates a new instance of an AngularInputType attribute
        /// </summary>
        /// <param name="inputType"></param>
        public AngularInputType(HTML5InputType inputType)
        {
            this.InputType = inputType;
        }
    }
}