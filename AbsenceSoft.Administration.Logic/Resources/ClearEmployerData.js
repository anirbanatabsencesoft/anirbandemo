﻿// change the employer id to get it here. ONLY GET THE STRING VALUE
var employerId = ObjectId('{{employerId}}');

// If configuration data should also be removed, set this to true. DO NOT CHANGE IT TO TRUE UNLESS INSTRUCTED BY CLIENT SERVICES TEAM.
var removeConfig = {{removeConfig}};

// We need to run operations based on employer id, so retrieve it here
var employer = db.Employer.findOne({ _id: employerId});
// See if this employer is the correct one
if (employer) {
    var q = { EmployerId: employer._id };
    db.Attachment.remove(q);
    db.Case.remove(q);
    db.CaseNote.remove(q);
    db.Communication.remove(q);
    var eIds = db.EligibilityUpload.distinct('_id', q);
    db.EligibilityUpload.remove(q);
    db.EligibilityUploadResult.remove({ EligibilityUploadId: { $in: eIds } });
    db.Employee.remove(q);
    db.EmployeeRestriction.remove(q);
    db.Organization.remove(q);
    db.EmployeeOrganization.remove(q);
    db.EmployeeContact.remove(q);
    db.EmployeeNote.remove(q);
    db.Event.remove(q);
    db.ToDoItem.remove(q);
    var customerId = db.Employer.findOne({ '_id': employer._id }).CustomerId;
    db.VariableScheduleTime.remove(q);
    db.WorkflowInstance.remove(q);
    db.EmployeeJob.remove(q);
    if (removeConfig) {
        if (employer.LogoId) {
            db.Document.remove({ $and: [q, { _id: { $nin: [employer.LogoId] } }] });
        } else {
            db.Document.remove(q);
        }
        db.AbsenceReason.remove(q);
        db.AccommodationQuestion.remove(q);
        db.AccommodationType.remove(q);
        db.ContactType.remove(q);
        db.CustomField.remove(q);
        db.DataUpdatePreference.remove(q);
        db.NotificationConfiguration.remove(q);
        db.Paperwork.remove(q);
        db.PaySchedule.remove(q);
        db.Policy.remove(q);
        db.Role.remove(q);
        db.SsoIdCacheEntry.remove(q);
        db.SsoProfile.remove(q);
        db.SsoSession.remove(q);
        db.EmployerServiceOption.remove(q);
        db.EmployerContact.remove(q);
        db.EmployerContactType.remove(q);
        db.Job.remove(q);
        db.Template.remove(q);
        db.Workflow.remove(q);
        db.Necessity.remove(q);
        db.OrganizationType.remove(q);
        db.ReportDefinition.remove(q);
        db.Restriction.remove(q);
    } else {
        // Ensure we preserve all template and paperwork documents
        var no_no_docs = [];
        if (employer.LogoId) { no_no_docs.push(employer.LogoId); }
        db.Template.find(q, { DocumentId: 1 }).forEach(function (t) { if (t.DocumentId) { no_no_docs.push(t.DocumentId); } });
        db.Paperwork.find(q, { FileId: 1 }).forEach(function (p) { if (p.FileId) { no_no_docs.push(p.FileId); } });
        db.Document.remove({ $and: [q, { _id: { $nin: no_no_docs } }] });
    }
}