﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;

namespace AbsenceSoft.Administration.Logic.Contracts
{
    public interface IAdminUserService
    {
        User GetUserById(string userId);
        Customer GetCustomerById(string customerId);
    }
}
