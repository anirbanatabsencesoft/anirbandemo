﻿using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Administration.Logic.Contracts
{
    public interface IAdminCustomerService
    {
        Customer GetCustomer(string customerId);
    }
}
