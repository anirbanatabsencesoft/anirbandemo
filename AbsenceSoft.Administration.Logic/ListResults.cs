﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    public class ListResults<T> where T: BaseEntity<T>, new()
    {
        public ListResults()
        {
            Ands = new List<IMongoQuery>();
        }

        public ListResults(ListCriteria criteria)
            :this()
        {
            Criteria = criteria;
        }

        public ListCriteria Criteria { get; private set; }

        public long Total { get; private set; }

        public List<T> Results { get; private set; }

        private List<IMongoQuery> Ands { get; set; }

        public void AddCriteria(IMongoQuery criteria)
        {
            if(criteria != null)
                Ands.Add(criteria);
        }

        private MongoCursor<T> Cursor { get; set; }

        internal void RunQuery()
        {
            IMongoQuery query = null;
            if (Ands.Count > 0)
                query = BaseEntity<T>.Query.And(Ands);

            Cursor = BaseEntity<T>.Query.Find(query);
            SetSort();
            SetSkip();

            Total = Cursor.Count();
            Results = Cursor.ToList();
        }

        private void SetSort()
        {
            if (string.IsNullOrEmpty(Criteria.SortBy))
                return;

            SortByBuilder sort = null;
            if(Criteria.Direction == AbsenceSoft.Data.Enums.SortDirection.Ascending)
            {
                sort = SortBy.Ascending(Criteria.SortBy);
            }
            else if(Criteria.Direction == AbsenceSoft.Data.Enums.SortDirection.Descending)
            {
                sort = SortBy.Descending(Criteria.SortBy);
            }

            Cursor.SetSortOrder(sort);
        }

        private void SetSkip()
        {
            int skipCount = (Criteria.PageNumber - 1) * Criteria.PageSize;
            Cursor.SetSkip(skipCount);
            Cursor.SetLimit(Criteria.PageSize);
        }
    }
}
