﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    public abstract class ListCriteria
    {
        public ListCriteria()
        {
            PageSize = 10;
            PageNumber = 1;
            Direction = SortDirection.Ascending;
        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public SortDirection Direction { get; set; }
        public string SortBy { get; set; }
    }
}
