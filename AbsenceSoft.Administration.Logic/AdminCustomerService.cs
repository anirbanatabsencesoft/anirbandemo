﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Administration.Logic.Properties;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data.Reporting;
using System.Configuration;
using Npgsql;
using System.Data;

namespace AbsenceSoft.Logic.Administration
{
    public class AdminCustomerService : CustomerService, IAdminCustomerService
    {
        public List<ReportCustomerDetails> GetCustomersDetail()
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["DataWarehouse"].ConnectionString))
            {
                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand("Show_CustomerDetails", conn);
                command.CommandType = CommandType.StoredProcedure;
                NpgsqlDataReader dr = command.ExecuteReader();
                List<ReportCustomerDetails> lstReportModel = new List<ReportCustomerDetails>();
                while (dr.Read())
                {
                    var reportModel = new ReportCustomerDetails
                    {
                        CustomerId = Convert.ToInt32(dr.GetValue(0)),
                        Customer = Convert.ToString(dr.GetValue(1)),
                        Employer = Convert.ToString(dr.GetValue(2)),
                        EmployeeCount6month = Convert.ToInt32(dr.GetValue(3)),
                        EmployeeCount3month = Convert.ToInt32(dr.GetValue(4)),
                        EmployeeCount1month = Convert.ToInt32(dr.GetValue(5)),
                        CaseCount = Convert.ToInt32(dr.GetValue(6)),
                        TodoCount = Convert.ToInt32(dr.GetValue(7)),
                        ActiveUsers = Convert.ToInt32(dr.GetValue(8))
                    };
                    lstReportModel.Add(reportModel);
                }
                
                conn.Close();
                Log.Info("Customers Details report fetched on {0} by {1}", DateTime.UtcNow, CurrentUser.DisplayName);
                return lstReportModel;
            }
        }

        /// <summary>
        /// Gets the billing details of customers based on date range 
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public List<ReportCustomerDetails> GetCustomersDetailByCriteria(DateTime? startDate, DateTime? endDate)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["BillingReport"].ConnectionString))
            {
                var _startDate = startDate.HasValue ? startDate.ToString("yyyy-MM-dd") : null;
                var _endDate = endDate.HasValue ? endDate.ToString("yyyy-MM-dd") : null;
                DateTime lastMonth = DateTime.UtcNow.AddMonths(-1);

                if (_startDate == null)
                {
                    _startDate = lastMonth.ToString("yyyy-MM-01");
                }
                if (_endDate == null)
                {
                    _endDate = lastMonth.ToString("yyyy-MM-" + DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month));
                }

                conn.Open();
                NpgsqlCommand command = new NpgsqlCommand("Show_CustomerDetails_New", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("p_start_date", NpgsqlTypes.NpgsqlDbType.Date, _startDate);
                command.Parameters.AddWithValue("p_end_date", NpgsqlTypes.NpgsqlDbType.Date, _endDate);
                NpgsqlDataReader reader = command.ExecuteReader();
                List<ReportCustomerDetails> reportCustomerDetails = new List<ReportCustomerDetails>();
                while (reader.Read())
                {
                    var reportCustomerDetail = new ReportCustomerDetails
                    {
                        Customer = Convert.ToString(reader.GetValue(0)),
                        Employer = Convert.ToString(reader.GetValue(1)),
                        EmployeeCount = Convert.ToInt32(reader.GetValue(2)),
                        CaseCount = Convert.ToInt32(reader.GetValue(3)),
                        TodoCount = Convert.ToInt32(reader.GetValue(4)),
                        ActiveUsers = Convert.ToInt32(reader.GetValue(5))
                    };
                    reportCustomerDetails.Add(reportCustomerDetail);
                }
                conn.Close();
                Log.Info("Customers Billing Details report fetched on {0} by {1}", DateTime.UtcNow, CurrentUser.DisplayName);
                return reportCustomerDetails;
            }
        }

        public ListResults<Customer> GetCustomers(CustomerCriteria criteria)
        {
            if (criteria == null)
                throw new ArgumentNullException("criteria");
            ListResults<Customer> customers = new ListResults<Customer>(criteria);
            customers.AddCriteria(Customer.Query.MatchesString(u => u.Name, criteria.Name));
            customers.RunQuery();

            return customers;
        }

        public ListResults<Employer> GetEmployers(CustomerCriteria criteria)
        {
            if (criteria == null)
                throw new ArgumentNullException("criteria");

            ListResults<Employer> employers = new ListResults<Employer>(criteria);
            employers.AddCriteria(Employer.Query.MatchesString(u => u.Name, criteria.Name));
            employers.RunQuery();
            return employers;
        }

        public List<Customer> GetAllCustomers()
        {
            return Customer.AsQueryable().ToList();
        }

        public Customer GetCustomer(string customerId)
        {
            return Customer.GetById(customerId);
        }

        public Customer SaveCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            return customer.Save();
        }

        public Employer SaveEmployer(Employer employer)
        {
            if (employer == null)
                throw new ArgumentNullException("employer");

            return employer.Save();
        }

        public Employer GetEmployer(string employerId)
        {
            return Employer.AsQueryable().FirstOrDefault(e => e.Id == employerId);
        }

        public List<Employer> GetEmployers(string customerId)
        {
            return Employer.AsQueryable().Where(e => e.CustomerId == customerId).ToList();
        }

        public void DeleteCustomer(string customerId)
        {
            if (string.IsNullOrEmpty(customerId))
                throw new ArgumentNullException("customerId");

            ObjectId objectId = new ObjectId();
            if (!ObjectId.TryParse(customerId, out objectId))
                throw new ArgumentException("customerId is not a valid objectId");

            Customer customerToDelete = Customer.GetById(customerId);
            // nothing to do
            if (customerToDelete == null)
                return;

            Log.Info("Customer {0} deleted on {1:d} by {2}", customerToDelete.Name, DateTime.UtcNow, CurrentUser.DisplayName);

            // We're doing a script and eval here because
            // 1) It's going to be faster than reflection to get every instance of data that inherits from BaseCustomerEntity
            // 2) Someone might forget/choose to inherit from BaseCustomerEntity but still have a CustomerId on it
            // 3) This is an internal tool only
            // 4) We can check that the string is a valid object Id and no one can inject into our script
            StringBuilder deleteCustomer = new StringBuilder();
            deleteCustomer.AppendFormat("var customerId = ObjectId('{0}');", customerId);
            deleteCustomer.Append("var collectionNames = db.getCollectionNames();");
            deleteCustomer.Append("for (var i = 0; i < collectionNames.length; i++){");
            deleteCustomer.Append("var collection = collectionNames[i];");
            deleteCustomer.Append("if (collection != 'system.indexes'){");
            deleteCustomer.Append("db.getCollection(collection).remove({ CustomerId: customerId});}}");
            deleteCustomer.Append("db.Customer.remove({_id:customerId});");
            BsonJavaScript deleteCustomerScript = new BsonJavaScript(deleteCustomer.ToString());
            EvalArgs evalArgs = new EvalArgs()
            {
                Code = deleteCustomerScript
            };
            Customer.Repository.Collection.Database.Eval(evalArgs);
        }

        public void ClearEmployerData(string employerId, bool removeConfig)
        {
            if (string.IsNullOrEmpty(employerId))
                throw new ArgumentNullException("employerId");

            ObjectId objectId = new ObjectId();
            if (!ObjectId.TryParse(employerId, out objectId))
                throw new ArgumentException("employerId is not a valid objectId");

            Employer employerToClear = Employer.GetById(employerId);

            // Nothing To Data
            if (employerToClear == null)
                return;
            Log.Info("Data for Employer {0} cleared on {1:d} by {2}. Remove Config option set to {3}",
                employerToClear.Name, DateTime.UtcNow, CurrentUser.DisplayName, removeConfig);

            // We're doing a script and eval here because
            // 1) It's going to be faster than reflection to get every instance of data that inherits from BaseEmployerEntity
            // 2) Someone might forget/choose to inherit from BaseCustomerEntity but still have a CustomerId on it
            // 3) This is an internal tool only
            // 4) We can check that the string is a valid object Id and no one can inject into our script
            string clearEmployerScript = Resources.ClearEmployerData;
            clearEmployerScript = clearEmployerScript.Replace("{{employerId}}", employerId);
            clearEmployerScript = clearEmployerScript.Replace("{{removeConfig}}", removeConfig.ToString().ToLower());
            BsonJavaScript clearEmployer = new BsonJavaScript(clearEmployerScript);
            EvalArgs evalArgs = new EvalArgs()
            {
                Code = clearEmployer
            };
            Employer.Repository.Collection.Database.Eval(evalArgs);
        }
    }
}
