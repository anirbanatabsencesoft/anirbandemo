﻿using AbsenceSoft.Data.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using System.Web;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Common;

namespace AbsenceSoft.Logic.Administration
{
    public class AdminCommunicationService:CommunicationService
    {
        public List<Template> GetDefaultCommunicationTemplates()
        {
            return Template.AsQueryable().Where(t => t.CustomerId == null && t.EmployerId == null).OrderBy(t => t.Name).ToList();
        }

        public List<Paperwork> GetDefaultPaperworkTemplates()
        {
            return Paperwork.AsQueryable().Where(t => t.CustomerId == null && t.EmployerId == null).OrderBy(t => t.Name).ToList();
        }

        public Document UploadCoreDocument(HttpPostedFileBase file)
        {
            if (file == null)
                throw new ArgumentNullException("file");

            Document document = new Document()
            {
                ContentLength = file.ContentLength,
                ContentType = file.ContentType,
                FileName = file.FileName,
                File = file.InputStream.ReadAllBytes()
            };

            /// Figure out how to upload without referencing AbsenceSoft.Logic
            return document.Upload();
        }

        public Template GetCoreTemplate(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            Template template = Template.GetById(id);
            if (template == null)
                return null;

            // At this time, this tool is not to be used to edit customer or employer specific configurations
            if (!string.IsNullOrEmpty(template.CustomerId) || !string.IsNullOrEmpty(template.EmployerId))
                return null;

            return template;
        }

        public Template SaveCoreTemplate(Template template)
        {
            if (template == null)
                throw new ArgumentNullException("template");

            if (!string.IsNullOrEmpty(template.CustomerId))
                throw new ArgumentException("Cannot save a template with a customer id");

            if (!string.IsNullOrEmpty(template.EmployerId))
                throw new ArgumentException("Cannot save a template with an employer id");

            return template.Save();
        }

        public void DeleteCoreTemplate(string id)
        {
            Template template = Template.Repository.GetById(id);
            if (template == null)
                return;

            if (!string.IsNullOrEmpty(template.CustomerId))
                throw new ArgumentException("Cannot delete a template with a customer id");

            if (!string.IsNullOrEmpty(template.EmployerId))
                throw new ArgumentException("Cannot delete a template with an employer id");

            Log.Info("Communication Template {0} deleted on {1:d} by {2}", template.Name, DateTime.UtcNow, CurrentUser.DisplayName);

            Template.Repository.Delete(id);
        }

        public Paperwork GetCorePaperwork(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            Paperwork paperwork = Paperwork.GetById(id);
            if (paperwork == null)
                return null;

            // At this time, this tool is not to be used to edit customer or employer specific configurations
            if (!string.IsNullOrEmpty(paperwork.CustomerId) || !string.IsNullOrEmpty(paperwork.EmployerId))
                return null;

            return paperwork;
        }



        public Paperwork SaveCorePaperwork(Paperwork paperwork)
        {
            if (paperwork == null)
                throw new ArgumentNullException("template");

            if (!string.IsNullOrEmpty(paperwork.CustomerId))
                throw new ArgumentException("Cannot save a template with a customer id");

            if (!string.IsNullOrEmpty(paperwork.EmployerId))
                throw new ArgumentException("Cannot save a template with an employer id");

            return paperwork.Save();
        }

        public void DeleteCorePaperwork(string id)
        {
            Paperwork template = Paperwork.Repository.GetById(id);
            if (template == null)
                return;

            if (!string.IsNullOrEmpty(template.CustomerId))
                throw new ArgumentException("Cannot delete a paperwork with a customer id");

            if (!string.IsNullOrEmpty(template.EmployerId))
                throw new ArgumentException("Cannot delete a paperwork with an employer id");

            Log.Info("Paperwork Template {0} deleted on {1:d} by {2}", template.Name, DateTime.UtcNow, CurrentUser.DisplayName);

            Paperwork.Repository.Delete(id);
        }
    }
}
