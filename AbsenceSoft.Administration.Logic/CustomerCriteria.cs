﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    public class CustomerCriteria:ListCriteria
    {
        public CustomerCriteria()
            :base()
        {
            SortBy = "Name";
            Direction = SortDirection.Ascending;
        }

        public string Name { get; set; }
    }
}
