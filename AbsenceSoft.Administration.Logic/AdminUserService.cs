﻿using AbsenceSoft.Administration.Logic.Contracts;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Logic.Administration
{
    public class AdminUserService : LogicService, IAdminUserService
    {
        public ListResults<User> GetUsers(UserCriteria criteria)
        {
            if (criteria == null)
                throw new ArgumentNullException("criteria");

            ListResults<User> users = new ListResults<User>(criteria);
            users.AddCriteria(User.Query.EqualsString(u => u.CustomerId, criteria.CustomerId));
            users.AddCriteria(User.Query.EqualsString(u => u.EmployerId, criteria.EmployerId));
            users.AddCriteria(User.Query.MatchesString(u => u.FirstName, criteria.FirstName));
            users.AddCriteria(User.Query.MatchesString(u => u.LastName, criteria.LastName));
            users.AddCriteria(User.Query.MatchesString(u => u.Email, criteria.Email));
            users.RunQuery();

            return users;
        }

        /// <summary>
        /// Gets an admin role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Role GetAdminRole(string id)
        {
            return Role.AsQueryable().FirstOrDefault(r => r.Id == id && r.CustomerId == null);
        }

        /// <summary>
        /// Gets all roles not assigned to a customer
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetAdminRoles(bool includeSystemAdministrator = true)
        {
            var roles = Role.AsQueryable().Where(u => u.CustomerId == null).ToList();

            if (includeSystemAdministrator)
            {
                roles.Insert(0, Role.SystemAdministrator);
            }

            return roles;
        }

        /// <summary>
        /// Unlocks a user account based on a user Id
        /// </summary>
        /// <param name="userId"></param>
        public void UnlockAccount(string userId)
        {
            User userToUnlock = User.GetById(userId);
            if (userToUnlock == null)
                return;

            userToUnlock.LockedDate = null;
            userToUnlock.LockedIpAddress = null;
            if (userToUnlock.EndDate < DateTime.Now)
                userToUnlock.EndDate = null;

            userToUnlock.Save();
        }

        /// <summary>
        /// Saves an ADMIN user.  Will set CustomerId to null and UserType to Admin
        /// </summary>
        /// <param name="adminUser"></param>
        /// <returns></returns>
        public User SaveAdminUser(User adminUser)
        {
            if (adminUser == null)
                throw new ArgumentNullException("adminUser");

            adminUser.CustomerId = null;
            adminUser.UserType = UserType.Admin;

            if (adminUser.IsNew)
            {
                adminUser.Password = new Data.CryptoString()
                {
                    PlainText = RandomString.Generate(12, true, true, true, false)
                };
                using (var authService = new AuthenticationService())
                {
                    authService.AddNewUserMail(adminUser);
                }
            }

            return adminUser.Save();
        }

        /// <summary>
        /// Delete an ADMIN user.  Will appened 'Softdeleted-' string in emailId so that original email id can be rgister again in future. 
        /// </summary>
        /// <param name="adminUser"></param>
        /// <returns></returns>  
        public User DeleteAdminUser(User adminUser)
        {
            if (adminUser == null)
            {
                throw new ArgumentNullException("adminUser");
            }
            adminUser.IsDeleted = true;
            adminUser.Email = "Softdeleted-" + adminUser.Email;
            return adminUser.Save();
        }


        /// <summary>
        /// Saves an ADMIN role.  Will set CustomerId to null
        /// </summary>
        /// <param name="adminRole"></param>
        /// <returns></returns>
        public Role SaveAdminRole(Role adminRole)
        {
            adminRole.CustomerId = null;
            return adminRole.Save();
        }

        /// <summary>
        /// Gets all users not assigned to a customer
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAdminUsers()
        {
            return User.AsQueryable().Where(u => u.CustomerId == null && u.UserType == UserType.Admin).ToList();
        }

        public User GetUserById(string userId)
        {
            return User.GetById(userId);
        }
        public Customer GetCustomerById(string customerId)
        {
            return Customer.GetById(customerId);
        }
 

        public void ChangePassword(string id, string password)
        {
            var user = User.GetById(id);
            if (user == null)
                throw new ArgumentNullException("user");

            user.Password = new Data.CryptoString()
            {
                PlainText = password
            };
            user.UserFlags = Data.Enums.UserFlag.ChangePassword;
            user.LockedDate = null;

            user.Save();
        }
    }
}
