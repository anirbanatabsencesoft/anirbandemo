﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Logic.SSO;
using AbsenceSoft.Web;
using AbsenceSoft.Web.ViewRendering;
using Aspose.Words;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace AbsenceSoft
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;

            log4net.Config.XmlConfigurator.Configure();

            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            int idx = 0;
            var jsonFactory = ValueProviderFactories.Factories.FirstOrDefault(f => f.GetType() == typeof(JsonValueProviderFactory));
            if (jsonFactory != null)
            {
                idx = ValueProviderFactories.Factories.IndexOf(jsonFactory);
                ValueProviderFactories.Factories.RemoveAt(idx);
            }
            ValueProviderFactories.Factories.Insert(idx, new JsonNetValueProviderFactory());
            ModelBinders.Binders.DefaultBinder = new JsonNetModelBinder();

            SsoConfiguration.ConfigureSso();

            //Initialize Aspose Word license
            new License().SetLicense("Aspose.Words.lic");

            // Initialize Aspose PDF license object
            Aspose.Pdf.License license = new Aspose.Pdf.License();
            // Set license
            license.SetLicense("Aspose.Pdf.lic");
            // Set the value to indicate that license will be embedded in the application
            license.Embedded = true;

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomerViewEngine());
        }

        private bool RedirectSsl()
        {
            if (Request.IsLocal || Request.IsHealthCheck() || Request.IsSsl())
                return false;

            if (FormsAuthentication.RequireSSL && !Request.IsSsl())
            {
                if (!string.Equals(Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase) && !string.Equals(Request.HttpMethod, "HEAD", StringComparison.OrdinalIgnoreCase))
                    throw new InvalidOperationException(string.Format("The request for resource '{0}' with verb '{1}' must use HTTPS.", Request.RawUrl, Request.HttpMethod));

                string url = string.Concat("https://", Request.Url.Host, Request.RawUrl);
                Response.Redirect(url, true);
                // 301 = bad, should use 302 instead
                //Response.RedirectPermanent(url, true);
                return true;
            }

            return false;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //Add CSRF token identifier
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimsIdentity.DefaultNameClaimType;
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            // Determine if the person is coming in via SSL, if not, determine if they should, and if so, redirect the request.
            if (RedirectSsl())
                return;

            if (Request.Path.ToLowerInvariant().Contains("/bundles/"))
                return;
            
            // Create our instrumentation context for use to trace the entire request for this local path/URI
            string methodName = Request.Url.LocalPath;
            Context.Items["__instrumentationContext"] = new InstrumentationContext(methodName);

            // Set the expires header so we're not caching in IE, 'cause IE sucks
            Response.Expires = -1;

            // try this first here. It may fail since there is no user
            // and will default to using the subdomain
            //
            if (!this.PopulateCurrentCustomer())
                return;
        }

        protected void Application_OnPostAuthenticateRequest(object sender, EventArgs e)
        {
            var requestPath = Request.Path.ToLowerInvariant();
            if (requestPath.Contains("/bundles/"))
                return;

            // we try again here now that the User will be available
            this.PopulateCurrentCustomer();
            this.VerifyUserFlags();
        }


        private void VerifyUserFlags()
        {
            if (Request.HttpMethod != "GET" || Request.IsAjax())
                return;

            var u = Data.Security.User.Current;
            if (u != null)
            {
                if ((u.Must(UserFlag.CreateCustomer) || string.IsNullOrWhiteSpace(u.CustomerId)) && !IsPaths(Request.Path, "/signup/customer", "/logout"))
                {
                    Response.Redirect("~/Signup/Customer", true);
                    return;
                }
                if (u.Must(UserFlag.UpdateEmployer) && !string.IsNullOrWhiteSpace(u.CustomerId) && !IsPaths(Request.Path, "/signup/employer", "/logout", "/signup/logo"))
                {
                    Response.Redirect("~/Signup/Employer", true);
                    return;
                }
                if (u.Must(UserFlag.FirstEmployee))
                {
                    // TODO: Do something here, maybe force them to go to the Create Employee screen.
                }
                if (u.Must(UserFlag.ProductTour))
                {
                    // TODO: Do something here, maybe set some other flag for the master view to launch the interactive UI tour (when we have one)
                }
            }
        }//VerifyUserFlags

        private bool IsPaths(string path, params string[] paths)
        {
            string lPath = path.ToLowerInvariant();
            return paths.Any(p => lPath.Contains(p));
        }

        private bool PopulateCurrentCustomer()
        {
            User currentUser = Current.User();

            string subDomain = Request.Url.Host.Split('.')[0];
            if (Request.Url.HostNameType == UriHostNameType.IPv4 || Request.Url.HostNameType == UriHostNameType.IPv6)
                subDomain = Request.Url.Host.ToLowerInvariant();

            // first try using the currently authenticated user if any
            if (currentUser != null)
            {
                var cust = Customer.GetById(currentUser.CustomerId);
                if (cust != null && cust.IsDeleted)
                {
                    BadUrl();
                    return false;
                }
                else
                {
                    Current.Customer(cust);
                }
            }
            else
            {
                // Grab the current customer represented by the URL, if no customer is found for the URL, no worries, it'll just be null
                // if localhost, it won't work anyway so just leave blank
                if (!subDomain.Equals("localhost"))
                {
                    var cust = Customer.GetByUrl(subDomain);
                    if (cust != null && cust.IsDeleted)
                    {
                        BadUrl();
                        return false;
                    }
                    else
                    {
                        Current.Customer(cust);
                    }
                }
            }

            return true;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                Log.Error("Unhandled Exception Occurred", ex.GetBaseException());

                //if antiforgery related redirect to login
                if (ex is HttpAntiForgeryException)
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.Redirect("/login", true);
                }
            }
            else
            {
                Log.Error(new
                {
                    Message = "Unknown Exception Occurred",
                    StackTrace = new StackTrace(true).ToString()
                });
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Context.Items["__instrumentationContext"] != null)
                ((InstrumentationContext)Context.Items["__instrumentationContext"]).Dispose();
        }

        /// <summary>
        /// Says, "Hey, Mr. URL, you're a very bad person and I don't like you!"
        /// </summary>
        protected void BadUrl()
        {
            Response.Clear();
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            Response.StatusDescription = "This is not a valid URL for AbsenceTracker";
            Response.ContentType = "text/html";
            Response.ContentEncoding = Encoding.UTF8;
            Response.Expires = -1;
            Response.Write(string.Format(
                "<html>\n<head>\n<title>Invalid AbsenceTracker URL</title>\n</head>\n<body><h1>Invalid AbsenceTracker URL</h1><p>The currently requested URL, <b>{0}</b>, is invalid, was not recognized, was deactivated or is mis-configured. Please contact your Leave Administrator or AbsenceSoft Support Representative.</p><p>Thank You,<br/>AbsenceSoft<br/><a href=\"mailto:support@absencesoft.com\">support@absencesoft.com</a></p></body>\n</html>",
                Request.Url.ToString()));
            Response.Flush();
            Response.End();
        }
    }
}
