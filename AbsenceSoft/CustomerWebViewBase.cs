﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections;
using System.Web;
using System.Web.Mvc;
using System.Linq;

namespace AbsenceSoft
{
    /// <summary>
    /// The AbsenceSoft base customer web view base view renderer for Razor syntax views.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public class CustomerWebViewBase<TModel> : WebViewPage<TModel>
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        private string _CaseId { get; set; }
        private string CaseId
        {
            get
            {
                return _CaseId;
            }
            set
            {
                _CaseId = value;
                Case = null;
            }
        }
        private Case _Case { get; set; }
        private Case Case
        {
            set
            {
                if (value != null)
                    CaseId = value.Id;

                _Case = value;
            }
            get
            {
                if (_Case == null)
                    _Case = Case.GetById(CaseId);

                return _Case;
            }
        }

        private string _EmployeeId { get; set; }
        private string EmployeeId
        {
            get
            {
                return _EmployeeId;
            }
            set
            {
                Employee = null;
                _EmployeeId = value;
            }
        }
        private Employee _Employee { get; set; }
        private Employee Employee
        {
            set
            {
                if (value != null)
                    EmployeeId = value.Id;

                _Employee = value;
            }
            get
            {
                if (_Employee == null)
                    _Employee = Employee.GetById(EmployeeId);

                return _Employee;
            }
        }

        private Hashtable Content { get; set; }

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        public AbsenceSoft.Data.Security.User CurrentUser { get; set; }

        /// <summary>
        /// Initializes the current page.
        /// </summary>
        protected override void InitializePage()
        {
            CurrentUser = AbsenceSoft.Data.Security.User.Current ?? AbsenceSoft.Data.Security.User.System;
            EmployerId = EmployerId ?? (Employer.Current == null ? null : Employer.Current.Id);
            CustomerId = CustomerId ?? (Customer.Current == null ? CurrentUser.CustomerId : Customer.Current.Id);
            if (string.IsNullOrWhiteSpace(CustomerId) && !string.IsNullOrWhiteSpace(EmployerId))
                CustomerId = (Employer.Current ?? Employer.GetById(EmployerId)).CustomerId;
            base.InitializePage();
        }

        /// <summary>
        /// ts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultContent">The default content.</param>
        /// <param name="employerId">The employer identifier.</param>
        /// <returns></returns>
        public virtual HtmlString T(string key, string defaultContent, string employerId, string caseId, string employeeId)
        {
            employerId = employerId ?? EmployerId;
            if (caseId != CaseId)
                CaseId = caseId;

            if (employeeId != EmployeeId)
                EmployeeId = employeeId;


            using (ContentService svc = new ContentService(CustomerId, employerId, CurrentUser))
            {
                if (Content == null)
                    Content = svc.GetAllContent();
                
                string text = svc.Get(Content, key, UICulture);
                text = svc.PerformTextMerge(text, Case, Employee);
                return new HtmlString(text ?? defaultContent ?? "");
            }
        }

        public virtual HtmlString T(string key)
        {
            return this.T(key, null, null, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent)
        {
            return this.T(key, defaultContent, null, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent, string employerId)
        {
            return this.T(key, defaultContent, employerId, null, null);
        }

        public virtual HtmlString T(string key, string defaultContent, string employerId, string caseId)
        {
            return this.T(key, defaultContent, employerId, caseId, null);
        }

        public bool CurrentCustomerHasFeature(Feature feature)
        {
            return Customer.Current.HasFeature(feature);
        }

        public bool CurrentUserHasPermission(string permission)
        {
            return AbsenceSoft.Data.Security.User.Permissions.ProjectedPermissions.Contains(permission);
        }

        public bool HasFeatureAndPermission(Feature feature, string permission)
        {
            return CurrentCustomerHasFeature(feature) && CurrentUserHasPermission(permission);
        }


        /// <summary>
        /// Executes this instance.
        /// </summary>
        public override void Execute() { /*NoOp*/ }
    }

    /// <summary>
    /// The AbsenceSoft base customer web view base view renderer for Razor syntax views.
    /// </summary>
    public class CustomerWebViewBase : CustomerWebViewBase<object> { }
}