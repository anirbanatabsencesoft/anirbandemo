﻿using AbsenceSoft.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Helpers
{
    public static class AngularHelpers
    {
        #region Helper Extensions

        /// <summary>
        /// Similar to a TextboxFor helper, this renders out an Angular-js friendly model of the provided input type, paying 
        /// attention to any data annotations on the incoming object model.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static HtmlString AngularInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            return htmlHelper.AngularInputFor<TModel, TProperty>(expression, null, htmlAttributes);
        }

        /// <summary>
        /// Similar to a TextboxFor helper, this renders out an Angular-js friendly model of the provided input type, paying 
        /// attention to any data annotations on the incoming object model.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="type"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static HtmlString AngularInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object propValue = null, object htmlAttributes = null, Dictionary<string, string> selectValues = null, string selectedValue = null)
        {
            string output = string.Empty;

            // Ensure we have a valid member expression
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new InvalidOperationException("Expression must be a member expression");

            // See if the object is decorated with a [Required] annotation
            bool isRequired = memberExpression.Member.GetAttribute<AngularRequired>() != null;

            // Determine the input type
            var htmlInputTypeMember = memberExpression.Member.GetAttribute<AngularInputType>();
            HTML5InputType htmlInputType = HTML5InputType.Text;

            // If the prop was not decorated, default to text
            if (htmlInputTypeMember != null)
            {
                htmlInputType = htmlInputTypeMember.InputType;
            }

            var htmlAttribs = new StringBuilder();

            // Build out any properties passed in as html attributes
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(htmlAttributes))
            {
                htmlAttribs.AppendFormat("{0}=\"{1}\" ", property.Name.Replace('_', '-'), property.GetValue(htmlAttributes));
            }

            // Try to compile the lambda expression to get the output value
            TProperty value = default(TProperty);

            if (htmlHelper.ViewData != null && htmlHelper.ViewData.Model != null && propValue == null)
            {
                try
                {
                    value = expression.Compile()(htmlHelper.ViewData.Model);
                }
                catch
                {
                    // Inner property was null, no value, continue
                }
            }
            else // Value being overridden in helper
            {
                value = (TProperty)propValue;
            }

            // Build the output string
            if (htmlInputType == HTML5InputType.Select)
            {
                output = string.Format("<select {0}{1}>{2}</select>", isRequired ? " required " : "", htmlAttribs.ToString(), selectValues.ToOptionList(selectedValue));
            }
            else
            {
                output = string.Format("<input type='{0}' value='{1}'{2}{3}/>", Enum.GetName(typeof(HTML5InputType), htmlInputType).ToLower(), value, isRequired ? " required " : "", htmlAttribs.ToString());
            }

            return new HtmlString(output);
        }

        /// <summary>
        /// Compliments the AngularInputFor. If a Required attribute it set, will add a span indicating the field is required. Observes
        /// DisplayName data annotation.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static HtmlString AngularLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            string output = string.Empty;

            // Ensure we have a valid member expression
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new InvalidOperationException("Expression must be a member expression");

            // See if the object is decorated with a [Required] annotation
            bool isRequired = memberExpression.Member.GetAttribute<AngularRequired>() != null;

            // Try to get the display name from the data annotation
            var displayNameAttrib = memberExpression.Member.GetCustomAttribute<DisplayNameAttribute>();
            string displayName = memberExpression.Member.Name;

            if (displayNameAttrib != null)
            {
                displayName = displayNameAttrib.DisplayName;
            }

            var htmlAttribs = new StringBuilder();

            // Build out any properties passed in as html attributes
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(htmlAttributes))
            {
                htmlAttribs.AppendFormat("{0}=\"{1}\" ", property.Name.Replace('_', '-'), property.GetValue(htmlAttributes));
            }

            // Build the output string
            output = string.Format("<label for='{0}'>{1}{2}</label>", memberExpression.Member.Name, displayName, isRequired ? "<span class='required_field'></span>" : "<span class='optional_field'></span>", htmlAttribs.ToString());

            return new HtmlString(output);
        }

        #endregion Helper Extensions

        #region Private Methods

        /// <summary>
        /// Converts a dictionary of strings into an option list for a select
        /// </summary>
        /// <param name="list"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        private static string ToOptionList(this Dictionary<string, string> list, string selectedValue = null)
        {
            StringBuilder optList = new StringBuilder();
            list.ForEach(l => {
                optList.AppendFormat("<option value='{0}'{1}>{2}</option>", l.Key, !string.IsNullOrWhiteSpace(selectedValue) && selectedValue == l.Value ? " selected=selected " : string.Empty, l.Value);
            });
            return optList.ToString();
        }

        /// <summary>
        /// Gets an attribute from a model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="provider"></param>
        /// <returns></returns>
        private static T GetAttribute<T>(this ICustomAttributeProvider provider) where T : Attribute
        {
            var attributes = provider.GetCustomAttributes(typeof(T), true);
            return attributes.Length > 0 ? attributes[0] as T : null;
        }

        #endregion Private Methods
    }
}