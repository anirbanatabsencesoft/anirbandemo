﻿using AbsenceSoft.Common;
using AbsenceSoft.Controllers;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AbsenceSoft
{
    public static class MvcControllerExtensions
    {
        public static ActionResult Try(this Controller controller, Action action)
        {
            try
            {
                action();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (AbsenceSoftException ex)
            {
                return controller.BadRequest(ex);
            }
            catch (Exception ex)
            {
                return controller.ServerError(ex);
            }
        }

        public static ActionResult ServerError(this Controller controller, Exception ex)
        {
            Log.Error("Server Error", ex);
            if (controller.HttpContext.Request.IsAjax())
            {
                return new StatusAndDataActionResult(HttpStatusCode.InternalServerError, GetErrorModel(ErrorMessages.UnhandledException));
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public static ActionResult BadRequest(this Controller controller, AbsenceSoftException ex)
        {
            if (controller.HttpContext.Request.IsAjax())
            {
                return new StatusAndDataActionResult(HttpStatusCode.BadRequest, GetErrorModel(ex.Message));
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public static ActionResult Ok(this Controller controller)
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public static ActionResult Forbidden(this Controller controller)
        {
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        public static ActionResult ExecuteWithEmployerAccess(this Controller controller, string employerId, Permission permission, Func<ActionResult> action)
        {
            var user = User.Current;
            if (user.HasEmployerAccess(employerId))
            {
                return action();
            }

            return controller.Forbidden();
        }

        private static object GetErrorModel(string message)
        {
            return new
            {
                error = new
                {
                    Message = message
                }
            };
        }
    }
}