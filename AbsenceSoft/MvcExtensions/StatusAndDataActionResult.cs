﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AbsenceSoft
{
    public class StatusAndDataActionResult : HttpStatusCodeResult
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        public StatusAndDataActionResult(HttpStatusCode statusCode, object data) : base(statusCode)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            this.Data = data;

        }

        public object Data { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            base.ExecuteResult(context);
            context.HttpContext.Response.Write(jsSerializer.Serialize(Data));
            context.HttpContext.Response.AddHeader("content-type", "application/json");
        }
    }
}