﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Common
{
    public class Messaging
    {
        private HttpContextBase _context;
        private HttpSessionStateBase _session;
        public Messaging() : this(null) { }
        public Messaging(HttpContextBase context)
        {
            _context = context ?? new HttpContextWrapper(HttpContext.Current);
            _session = context == null ? null : _context.Session;
        }

        public bool HasMessages { get { return HasError || HasWarning || HasInfo || HasSuccess; } }

        public bool HasError { get { return Errors.Any(); } }
        public bool HasWarning { get { return Warnings.Any(); } }
        public bool HasInfo { get { return Info.Any(); } }
        public bool HasSuccess { get { return Success.Any(); } }

        public Queue<string> Errors { get { return getQueue("_messaging_Errors"); } }

        public Queue<string> Warnings { get { return getQueue("_messaging_Warnings"); } }

        public Queue<string> Info { get { return getQueue("_messaging_Info"); } }

        public Queue<string> Success { get { return getQueue("_messaging_Success"); } }

        private Queue<string> getQueue(string name)
        {
            if (_context != null)
            {
                // Allows persistence if we have a session to do so with.
                if (_session != null)
                {
                    if (_session[name] == null)
                        _session[name] = new Queue<string>();

                    return _session[name] as Queue<string>;
                }

                if (_context.Items[name] == null)
                    _context.Items[name] = new Queue<string>();

                return _context.Items[name] as Queue<string>;
            }
            return new Queue<string>();
        }

        public void AddError(string message, params string[] formatArgs)
        {
            Errors.Enqueue(formatArgs.Length > 0 ? string.Format(message, formatArgs) : message);
        }

        public void AddWarning(string message, params string[] formatArgs)
        {
            Warnings.Enqueue(formatArgs.Length > 0 ? string.Format(message, formatArgs) : message);
        }

        public void AddInfo(string message, params string[] formatArgs)
        {
            Info.Enqueue(formatArgs.Length > 0 ? string.Format(message, formatArgs) : message);
        }

        public void AddSuccess(string message, params string[] formatArgs)
        {
            Success.Enqueue(formatArgs.Length > 0 ? string.Format(message, formatArgs) : message);
        }

        public static Messaging Current { get { return new Messaging(); } }
    }
}