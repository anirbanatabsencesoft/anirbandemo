﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Common
{
    public class JsonNetModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var request = controllerContext.RequestContext.HttpContext.Request;
            if (IsJsonRequest(request))
            {
                request.InputStream.Seek(0, SeekOrigin.Begin);
                string json = new StreamReader(request.InputStream).ReadToEnd();
                if (!string.IsNullOrWhiteSpace(json))
                {
                    //HACK: Work-around to allow url binding to function. I.e: for ids
                    if (json.StartsWith("{") && bindingContext.ModelType != typeof(string))
                    {
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                        var returnObj = JsonConvert.DeserializeObject(json, bindingContext.ModelType, settings);

                        var validationResults = new HashSet<ValidationResult>();

                        if (!Validator.TryValidateObject(returnObj, new ValidationContext(returnObj, null, null), validationResults, true))
                        {
                            foreach (var result in validationResults)
                            {
                                foreach (string memberName in result.MemberNames)
                                {
                                    bindingContext.ModelState.AddModelError(memberName, result.ErrorMessage);
                                }
                            }
                        }

                        return returnObj;
                    }
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }

        private static bool IsJsonRequest(HttpRequestBase request)
        {
            if (request.ContentLength <= 0)
                return false;

            return request.ContentType.Contains("application/json");
        }
    }
}