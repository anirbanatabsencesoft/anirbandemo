﻿using AbsenceSoft.Common.Serializers;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Common
{
    /// <summary>
    /// Represents a class that is used to send JSON-formatted content to the response using NewtonSoft's JSON.net library.
    /// </summary>
    public class JsonNetResult : JsonResult
    {
        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="context">The context within which the result is executed.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="context" /> parameter is null.</exception>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            try
            {
                HttpResponseBase response = context.HttpContext.Response;
                if (!string.IsNullOrEmpty(this.ContentType))
                    response.ContentType = this.ContentType;
                else
                    response.ContentType = "application/json";

                if (this.ContentEncoding != null)
                    response.ContentEncoding = this.ContentEncoding;

                if (this.Data != null)
                {
                    // All other properties are ignored
                    response.Write(JsonConvert.SerializeObject(this.Data, Formatting.None, JsonSettings.SerializerSettings));
                }
            }
            catch (InvalidCastException castEx)
            {
                StringBuilder error = new StringBuilder();
                error.AppendLine("Error serializing object for JSON response.");
                if (this.Data != null)
                {
                    error.AppendLine(string.Format("Data of type '{0}' was not able to serialized.", this.Data.GetType().FullName));
                    error.AppendLine(string.Format("Data ToString() is '{0}'.", this.Data.ToString()));
                    try
                    {
                        using (var sw = new System.IO.StringWriter())
                        {
                            using (var jw = new MongoDB.Bson.IO.JsonWriter(sw))
                            {
                                MongoDB.Bson.Serialization.BsonSerializer.Serialize(jw, Data.GetType(), Data);
                            }
                        }
                    }
                    catch{ }
                }
                Log.Error(error.ToString(), castEx);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
