﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Common
{
    public class RequiredIfCustomerHasFeatureAttribute:RequiredAttribute, IClientValidatable
    {
        private Feature RequiredFeature { get; set; }

        public RequiredIfCustomerHasFeatureAttribute(Feature feature)
        {
            this.RequiredFeature = feature;
        }

        public override bool IsValid(object value)
        {
            if(Current.Customer().HasFeature(RequiredFeature))
                return base.IsValid(value);

            return true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            List<ModelClientValidationRule> rules = new List<ModelClientValidationRule>();
            if (Current.Customer().HasFeature(RequiredFeature))
            {
                var requiredRule = new ModelClientValidationRequiredRule(string.Format("{0} field is required.", metadata.DisplayName));
                rules.Add(requiredRule);
            }

            return rules;
        }
    }
}