﻿using AbsenceSoft.Filters;
using AbsenceSoft.Web.Filters;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new IpRangeFilterAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new HandlePasswordChangeRequiredAttribute());
            filters.Add(new AuditViewAttribute());
            filters.Add(new RenewSessionGlobalFilter( AT.Entities.Authentication.ApplicationType.Portal));
        }
    }
}
