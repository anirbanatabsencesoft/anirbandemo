﻿using AngularTemplates.Bundling;
using AngularTemplates.Compile;
using System.Web.Optimization;

namespace AbsenceSoft
{
    public class BundleConfig
    {
        public const string AngularAppRoot = "~/Scripts/app-angular/";
        public const string AngularVirtualAppPath = AngularAppRoot + "main";

        public const string BootstrapPath = "~/Bundles/Bootstrap";
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Load jQuery unobtrusive
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery.cookie.js",
                    "~/Scripts/jquery.validate.js",
                    "~/Scripts/jquery.validate.unobtrusive.js",
                    "~/Scripts/jquery.unobtrusive-ajax.js",
                    "~/Scripts/additional-methods.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-datepicker.js",
                 "~/Scripts/bootstrap-datepicker.js",
                 "~/Scripts/bootstrap-datetimepicker.min.js",
                 "~/Scripts/bootstrap-timepicker.min.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/styles/app")
                .Include("~/Content/css/jsPlumbToolkit-defaults.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/select2.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/select2-bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-custom/_main.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-datepicker.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-datetimepicker.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-timepicker.min.css", new CssRewriteUrlTransform()));




            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js"
                , "~/Scripts/angular-resource.js"
                , "~/Scripts/angular-route.js"
                , "~/Scripts/ui-bootstrap-tpls-0.13.2.js"
                , "~/Scripts/angular-sanitize.js"
                , "~/Scripts/angular.filter.js"
            ));


            //
            // include app.js first, then modules and finally the rest of the
            // entirely of the app-angular directory tree
            // Load order should not matter for any other NG files!
            bundles.Add(new ScriptBundle("~/bundles/app-angular/main")
                .Include(AngularAppRoot + "/app.js")
                .IncludeDirectory(AngularAppRoot, "*module.js", true)
                .IncludeDirectory(AngularAppRoot, "*.js", true)
            );
            //
            // pre-compile angular templates
            var options = new TemplateCompilerOptions
            {
                ModuleName = "App",
                Prefix = "/",
                Standalone = false,
                LowercaseTemplateName = true
            };
            bundles.Add(new TemplateBundle("~/bundles/ngtemplates", options)
                .IncludeDirectory(AngularAppRoot, "*.html", true)
            );

            /// The old bundle for everything not angular
            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/lodash.js",
                "~/Scripts/moment.js",
                "~/Scripts/select2.js",
                "~/Scripts/Global/*.js",
                "~/Scripts/jquery.tmpl.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/vendor/jquery.ui.widget.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/jquery.iframe-transport.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/jquery.fileupload.js",
                "~/Scripts/tinymce/tinymce.min.js",
                "~/Scripts/Custom/EmployeeInfo.js",
                "~/Scripts/Custom/ChangePassword.js"));

            /// Global bundle for NON app related stuff
            bundles.Add(new ScriptBundle("~/bundles/global").Include(
                "~/Scripts/Global/JQueryAjaxSetup.js",
                "~/Scripts/tinymce/tinymce.js",
                "~/Scripts/select2.js",
                "~/Scripts/lodash.js",
                "~/Scripts/moment.js",
                 "~/Scripts/CommonWeb.js",
                "~/Scripts/noty/packaged/jquery.noty.packaged.js",
                "~/Scripts/jquery.tmpl.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/vendor/jquery.ui.widget.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/jquery.iframe-transport.js",
                "~/Scripts/jQuery-File-Upload-9.5.6/jquery.fileupload.js",
                "~/Scripts/Global/noty.js",
                "~/Scripts/Global/common.js",
                "~/Scripts/Global/dom.jsPlumb-1.7.6.js",
                "~/Scripts/Global/jsPlumbToolkit-1.0.7-min.js",
                "~/Scripts/Global/jquery.fileDownload.js",
                "~/Scripts/Global/bootstrap-tree.js",
                "~/Scripts/Global/papaparse.min.js",
                "~/Scripts/Global/jquery.are-you-sure.js",
                "~/Scripts/Global/app.js",
                "~/Scripts/Global/admin.js",
                "~/Scripts/Global/timetracker.popover.js",
                "~/Scripts/Global/api-framework.js")
                .IncludeDirectory("~/Scripts/Widgets", "*.js"));

            bundles.Add(new ScriptBundle("~/bundles/widgets")
                 .IncludeDirectory("~/Scripts/Widgets", "*.js"));

            /// This is the bundle for page specific javascript
            bundles.Add(new ScriptBundle("~/bundles/app")
                .IncludeDirectory("~/Scripts/Reports", "*.js")
                .IncludeDirectory("~/Scripts/Cases", "*.js")
                .IncludeDirectory("~/Scripts/Todos", "*.js")
                .IncludeDirectory("~/Scripts/Demands", "*.js")
                .IncludeDirectory("~/Scripts/Jobs", "*.js")
                .IncludeDirectory("~/Scripts/Employees", "*.js")
                .IncludeDirectory("~/Scripts/NoteCategory", "*.js")
                .IncludeDirectory("~/Scripts/Consultations", "*.js")
                .IncludeDirectory("~/Scripts/Organization", "*.js")
                .IncludeDirectory("~/Scripts/OrganizationAnnualInfo", "*.js")
                .IncludeDirectory("~/Scripts/CaseAssignment", "*.js")
                .IncludeDirectory("~/Scripts/CustomContent", "*.js")
                .IncludeDirectory("~/Scripts/EmployerContact", "*.js")
                .IncludeDirectory("~/Scripts/Administration", "*.js")
                .IncludeDirectory("~/Scripts/Templates", "*.js")
                .IncludeDirectory("~/Scripts/Workflow", "*.js")
                .IncludeDirectory("~/Scripts/Policies", "*.js")
                .IncludeDirectory("~/Scripts/Necessity", "*.js")
                .IncludeDirectory("~/Scripts/CustomFields", "*.js")
                .IncludeDirectory("~/Scripts/AccommodationTypes", "*.js")
                .IncludeDirectory("~/Scripts/PolicyCrosswalk", "*.js")
                .IncludeDirectory("~/Scripts/RiskProfiles", "*.js")
                .IncludeDirectory("~/Scripts/Search", "*.js")
                .IncludeDirectory("~/Scripts/SsoProfile", "*.js")
                .IncludeDirectory("~/Scripts/BulkDownload", "*.js")
                .IncludeDirectory("~/Areas/Reporting/Scripts", "*.js", true)
                .IncludeDirectory("~/Scripts/IpRange", "*.js")
                .IncludeDirectory("~/Scripts/Shared", "*.js", true)
                 .IncludeDirectory("~/Scripts/CaseCategory", "*.js")
                .Include("~/Scripts/Chart/jsapi.js",
                "~/Scripts/Chart/DashboardChart.js"));

           

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
