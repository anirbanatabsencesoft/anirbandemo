﻿using AbsenceSoft.Data.Policies;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.PolicyCrosswalk
{
    public class PolicyCrosswalkTypeViewModel:IViewModel, IDataTransformModel<PolicyCrosswalkType>
    {
        public PolicyCrosswalkTypeViewModel()
        {

        }

        public PolicyCrosswalkTypeViewModel(PolicyCrosswalkType type)
            :this()
        {
            if (type == null)
                return;

            Id = type.Id;
            CustomerId = type.CustomerId;
            EmployerId = type.EmployerId;
            Name = type.Name;
            Code = type.Code;
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string DisplayType { get { return "Policy Crosswalk Type"; } }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        public PolicyCrosswalkType ApplyToDataModel(PolicyCrosswalkType type)
        {
            if (type == null)
                type = new PolicyCrosswalkType();

            type.Name = Name;
            type.Code = Code;

            return type;
        }
    }
}