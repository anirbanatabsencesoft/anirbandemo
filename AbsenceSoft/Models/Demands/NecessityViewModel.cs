﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Demands
{
    public class NecessityViewModel:IViewModel, IDataTransformModel<Necessity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityViewModel"/> class.
        /// </summary>
        public NecessityViewModel() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="NecessityViewModel"/> class.
        /// </summary>
        /// <param name="necessity">The necessity.</param>
        public NecessityViewModel(Necessity necessity) : this()
        {
            if (necessity == null)
                return;

            Id = necessity.Id;
            CustomerId = necessity.CustomerId;
            EmployerId = necessity.EmployerId;

            Name = necessity.Name;
            Description = necessity.Description;
            Code = necessity.Code;
            Type = necessity.Type;
        }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DisplayName("Description"), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [Required, DisplayName("Type"), UIHint("EnumButtons")]
        public NecessityType? Type { get; set; }

        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string DisplayType { get { return "necessity"; } }

        public Necessity ApplyToDataModel(Necessity necessity)
        {
            if (necessity == null)
                necessity = new Necessity();

            necessity.Code = Code;
            necessity.Name = Name;
            necessity.Description = Description;
            necessity.Type = Type.Value;

            return necessity;
        }
    }
}