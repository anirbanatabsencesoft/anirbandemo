﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Models.Demands
{
    public class WorkRestrictionViewModel : AppliedDemandViewModel
    {

        public WorkRestrictionViewModel()
            : base()
        {
            this.Values = new List<AppliedDemandValueViewModel>();
            this.DuplicateRestrictions = new List<WorkRestrictionViewModel>();
        }

        public WorkRestrictionViewModel(string employeeId)
            : this()
        {
            this.EmployeeId = employeeId;
        }

        public WorkRestrictionViewModel(EmployeeRestriction restriction, Demand demand, List<DemandType> demandTypes)
            : this(restriction)
        {
            this.DemandId = demand.Id;
            this.DemandName = demand.Name;
            if (restriction == null)
            {
                BuildValuesModel(demand, demandTypes);
            }
        }

        public WorkRestrictionViewModel(EmployeeRestriction restriction)
            : this()
        {
            if (restriction == null || restriction.Restriction == null)
                return;

            this.Id = restriction.Id;
            this.CaseId = restriction.CaseId;
            this.EmployeeId = restriction.EmployeeId;

            this.DemandId = restriction.Restriction.DemandId;
            this.DemandName = restriction.Restriction.Demand.Name;
            if (restriction.Restriction.Dates != null)
            {
                this.StartDate = restriction.Restriction.Dates.StartDate;
                this.OverriddenStartDate = restriction.Restriction.Dates.StartDate;
                this.EndDate = restriction.Restriction.Dates.EndDate;
                this.Dates = restriction.Restriction.Dates.ToString();
                this.CreatedDate = restriction.CreatedDate;
            }

            if (restriction.Restriction.Values != null)
            {
                this.Values = restriction.Restriction.Values.Select(v => new AppliedDemandValueViewModel(v.DemandTypeId, v.Type, v.Text, v.Applicable, v.Schedule, v.DemandItem, v.ToString())).ToList();
                BuildValuesModel();
            }
        }

        [Required]
        public string EmployeeId { get; set; }
        public string CaseId { get; set; }
        public string Dates { get; set; }
        public List<WorkRestrictionViewModel> DuplicateRestrictions { get; set; }
        public DateTime CreatedDate { get; set; }

        public EmployeeRestriction ApplyToDataModel(EmployeeRestriction workRestriction = null, bool isBulk = false)
        {
            var oldRestriction = workRestriction == null ? null : workRestriction.Clone();

            if (workRestriction == null)
                workRestriction = new EmployeeRestriction()
                {
                    Restriction = new WorkRestriction(),
                };

            if (!string.IsNullOrWhiteSpace(this.CaseId))
            {
                Case theCase = Case.GetById(this.CaseId);
                workRestriction.CaseId = theCase.Id;
                workRestriction.CaseNumber = theCase.CaseNumber;
                workRestriction.EmployeeId = theCase.Employee.Id;
                workRestriction.CustomerId = theCase.CustomerId;
                workRestriction.EmployerId = theCase.EmployerId;
            }
            else
            {
                workRestriction.EmployeeId = this.EmployeeId;
                workRestriction.CustomerId = Customer.Current.Id;
                workRestriction.EmployerId = Employer.CurrentId;
            }

            workRestriction.Restriction.DemandId = this.DemandId;
            DateTime startDate = this.OverriddenStartDate.HasValue ? this.OverriddenStartDate.Value : StartDate.Value;
            workRestriction.Restriction.Dates = string.IsNullOrWhiteSpace(startDate.ToString()) ? DateRange.Null : new DateRange(startDate, EndDate);
            ApplyValuesToDataModel(workRestriction.Restriction);

            // Do we need to save this as a historical item?
            if (isBulk && oldRestriction != null)
            {
                bool changed = false;
                changed = changed || oldRestriction.Restriction.Dates != workRestriction.Restriction.Dates;
                changed = changed || oldRestriction.Restriction.DemandId != workRestriction.Restriction.DemandId;
                changed = changed || oldRestriction.Restriction.Type != workRestriction.Restriction.Type;
                var oldVals = oldRestriction.Restriction.ToString().Split(',').Select(v => v.Trim());
                var newVals = workRestriction.Restriction.ToString().Split(',').Select(v => v.Trim());
                changed = changed || oldVals.Except(newVals).Any();
                changed = changed || newVals.Except(oldVals).Any();
                if (changed)
                    workRestriction.Clean();
            }

            return workRestriction;
        }


        private void ApplyValuesToDataModel(WorkRestriction workRestriction)
        {
            if (this.Values == null || this.Values.Count() == 0)
            {
                workRestriction.Values = null;
                return;
            }

            List<AppliedDemandValue<WorkRestrictionValue>> values = new List<AppliedDemandValue<WorkRestrictionValue>>(this.Values.Count());
            foreach (var value in this.Values)
            {
                values.Add(BuildWorkRestrictionValue(value));
            }

            workRestriction.Values = values;
        }

        /// <summary>
        /// Builds a work restriction value from the UI model
        /// </summary>
        /// <param name="valueModel"></param>
        /// <returns></returns>
        private AppliedDemandValue<WorkRestrictionValue> BuildWorkRestrictionValue(AppliedDemandValueViewModel valueModel)
        {
            AppliedDemandValue<WorkRestrictionValue> value = new AppliedDemandValue<WorkRestrictionValue>()
            {
                Type = valueModel.Type,
                DemandTypeId = valueModel.DemandTypeId
            };
            switch (valueModel.Type)
            {
                case DemandValueType.Text:
                    value.Text = valueModel.Text;
                    break;
                case DemandValueType.Boolean:
                    value.Applicable = valueModel.Applicable;
                    break;
                case DemandValueType.Value:
                    if (valueModel.DemandItemId.HasValue)
                    {
                        value.DemandItem = value.DemandType.Items.FirstOrDefault(i => i.Id == valueModel.DemandItemId.Value);
                    }
                    break;
                case DemandValueType.Schedule:
                    if (valueModel.DurationType.HasValue && valueModel.FrequencyType.HasValue)
                        {
                        value.Schedule = new ScheduleDemand()
                        {
                            Duration = valueModel.Duration,
                            DurationType = valueModel.DurationType.Value,
                            Frequency = valueModel.Frequency,
                            FrequencyType = valueModel.FrequencyType.Value,
                            FrequencyUnitType = valueModel.FrequencyUnitType ?? DayUnitType.Workday,
                            Occurances = valueModel.Occurances
                        };
                    }
                    break;
                default:
                    break;
            }

            return value;
        }


    }
}