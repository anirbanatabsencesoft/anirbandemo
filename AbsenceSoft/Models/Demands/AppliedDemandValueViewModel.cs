﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsenceSoft.Models.Demands
{
    public class AppliedDemandValueViewModel
    {
        public AppliedDemandValueViewModel()
        {

        }

        public AppliedDemandValueViewModel(string id, DemandValueType type, string text, bool? applicable, ScheduleDemand scheduleDemand, DemandItem demandItem, string appliedValue)
        {
            this.DemandTypeId = id;
            this.Type = type;
            this.Text = text;
            this.Applicable = applicable;
            this.Value = appliedValue;

            if (scheduleDemand != null)
            {
                this.Duration = scheduleDemand.Duration;
                this.DurationType = scheduleDemand.DurationType;
                this.Frequency = scheduleDemand.Frequency;
                this.FrequencyType = scheduleDemand.FrequencyType;
                this.FrequencyUnitType = scheduleDemand.FrequencyUnitType;
                this.Occurances = scheduleDemand.Occurances;
            }

            if (demandItem != null)
            {
                this.DemandItemId = demandItem.Id;
            }

        }

        public string DemandTypeId { get; set; }
        public string Name { get; set; }
        public DemandValueType Type { get; set; }

        public Guid? DemandItemId { get; set; }
       
        public List<DemandItem> DemandItems { get; set; }
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        public string Value { get; set; }

        public bool? Applicable { get; set; }

        /// <summary>
        /// Gets or sets the number of occurrances that are expected to occur through the specified frequency.
        /// </summary>
        public int Occurances { get; set; }

        /// <summary>
        /// Gets or sets the frequency of the occurances as they are certified/expected to occur.
        /// </summary>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure the frequency of the occurrances.
        /// </summary>
        public Unit? FrequencyType { get; set; }

        /// <summary>
        /// set the Frequency Type to work days or calendar days (used when setting the period window test) 
        /// default is work days.
        /// </summary>
        public DayUnitType? FrequencyUnitType { get; set; }

        /// <summary>
        /// Gets or sets the certified duration for each occurance to occur within the bounds of.
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Gets or sets the units used to measure duration of each occurrance.
        /// </summary>
        public Unit? DurationType { get; set; }

        internal bool HasValue
        {
            get
            {
                switch (this.Type)
                {
                    case DemandValueType.Text:
                        return !string.IsNullOrEmpty(this.Text);
                    case DemandValueType.Boolean:
                        return this.Applicable.HasValue;
                    case DemandValueType.Value:
                        return this.DemandItemId.HasValue;
                    case DemandValueType.Schedule:
                        return this.FrequencyType.HasValue && this.DurationType.HasValue;
                    default:
                        return false;
                }
            }

        }
    }
}
