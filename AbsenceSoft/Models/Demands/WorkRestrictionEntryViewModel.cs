﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Demands
{
    public class WorkRestrictionEntryViewModel
    {
        public List<WorkRestrictionViewModel> WorkRestrictions { get; set; }
        public string CaseId { get; set; }
        public string ToDoItemId { get; set; }
    }
}