﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Demands
{
    public class JobRestrictedNecessityViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobRestrictedNecessityViewModel"/> class.
        /// </summary>
        public JobRestrictedNecessityViewModel() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="JobRestrictedNecessityViewModel"/> class.
        /// </summary>
        /// <param name="necessity">The necessity.</param>
        public JobRestrictedNecessityViewModel(JobRestrictedNecessity necessity)
        {
            if (necessity == null)
                return;
            NecessityId = necessity.NecessityId;
            Reason = necessity.Reason;
        }

        /// <summary>
        /// Gets or sets the necessity code.
        /// </summary>
        /// <value>
        /// The necessity code.
        /// </value>
        [Required, UIHint("Necessity"), DisplayName("Necessity")]
        public string NecessityId { get; set; }

        /// <summary>
        /// Gets or sets the reason why this is restricted for the job it is applied to.
        /// </summary>
        /// <value>
        /// The reason.
        /// </value>
        [Required]
        public string Reason { get; set; }

        /// <summary>
        /// Applies to data model.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public void ApplyToDataModel(List<JobRestrictedNecessity> collection)
        {
            var existing = collection == null ? null : collection.FirstOrDefault(c => c.NecessityId == NecessityId);
            if (existing == null)
                (collection ?? new List<JobRestrictedNecessity>()).Add(new JobRestrictedNecessity()
                {
                    NecessityId = NecessityId,
                    Reason = Reason
                });
            else
                existing.Reason = Reason;
        }
    }
}