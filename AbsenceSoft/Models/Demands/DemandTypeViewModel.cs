﻿using AbsenceSoft.Data.Jobs;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.Models.Demands
{
    public class DemandTypeViewModel : IViewModel, IDataTransformModel<DemandType>, IValidatableObject
    {
        public DemandTypeViewModel() {
            DisplayType = "Demand Type";
        }

        public DemandTypeViewModel(DemandType demandType)
            : this()
        {
            Id = demandType.Id;
            Code = demandType.Code;
            Order = demandType.Order;
            Name = demandType.Name;
            Description = demandType.Description;
            CustomerId = demandType.CustomerId;
            EmployerId = demandType.EmployerId;
            Type = demandType.Type;
            
            RequirementLabel = demandType.RequirementLabel;
            RestrictionLabel = demandType.RestrictionLabel;

            if (demandType.Items != null)
                Items = demandType.Items.Select(dt => new DemandItemViewModel(dt)).ToList();
        }

        public string Id { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int? Order { get; set; }

        [DisplayName("Requirement Label")]
        public string RequirementLabel { get; set; }

        [DisplayName("Restriction Label")]
        public string RestrictionLabel { get; set; }

        public string CustomerId { get; set; }

        [DisplayName("Employer"), UIHint("Employers")]
        public string EmployerId { get; set; }

        public string DisplayType { get; private set; }

        [Required, UIHint("EnumButtons")]
        public DemandValueType? Type { get; set; }

        public string AddNewValue { get; set; }

        public List<DemandItemViewModel> Items { get; set; }

        public DemandType ApplyToDataModel(DemandType demandType)
        {
            demandType.Name = Name;
            demandType.Code = Code;
            demandType.Description = Description;
            demandType.Order = Order;
            demandType.Type = Type ?? default(DemandValueType);

            demandType.RestrictionLabel = RestrictionLabel;
            demandType.RequirementLabel = RequirementLabel;

            ApplyDemandItemsToDataModel(demandType);

            return demandType;
        }

        private void ApplyDemandItemsToDataModel(DemandType demandType){
            if (Items == null
                || (Type != null && Type.Value != DemandValueType.Value))
            {
                demandType.Items = null;
                return;
            }

            demandType.Items = demandType.Items == null
                ? new List<DemandItem>()
                : demandType.Items.Where(i => Items.Any(ivm => ivm.Id == i.Id)).ToList();

            foreach (var item in Items)
            {
                var matchingItem = demandType.Items.FirstOrDefault(i => i.Id == item.Id);
                if (matchingItem != null)
                {
                    matchingItem.Name = item.Name;
                    matchingItem.Score = item.Score;
                    matchingItem.Value = item.Value;
                    matchingItem.Classification = item.Classification;
                }
                else
                {
                    demandType.Items.Add(
                        new DemandItem
                        {
                            Name = item.Name,
                            Score = item.Score,
                            Value = item.Value,
                            Classification = item.Classification
                        });
                }
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if(this.Type.Value.ToString() == "Value" && this.Items == null)
            {
              results.Add(new ValidationResult("Please Add new value.", new List<string>() { "AddNewValue" }));
            }

            return results;
        }
    }
}