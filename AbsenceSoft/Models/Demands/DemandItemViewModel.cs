﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Demands
{
    public class DemandItemViewModel
    {
        public DemandItemViewModel()
        {

        }

        public DemandItemViewModel(DemandItem demandItem):this()
        {
            if (demandItem == null)
                return;

            this.Id = demandItem.Id;
            this.Name = demandItem.Name;
            this.Value = demandItem.Value;
            this.Score = demandItem.Score;
            this.Classification = demandItem.Classification;
        }

        public Guid? Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Value { get; set; }
        public decimal? Score { get; set; }

        public JobClassification? Classification { get; set; }
    }
}