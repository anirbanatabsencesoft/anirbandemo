﻿using AbsenceSoft.Data.Jobs;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Demands
{
    public class DemandViewModel : IViewModel, IDataTransformModel<Demand>, IValidatableObject
    {
        public DemandViewModel()
        {
            DisplayType = "Physical Demand";
            DemandTypes = new List<string>();
        }

        public DemandViewModel(Demand demand) : this()
        {
            if (demand == null)
                return;

            Id = demand.Id;
            Code = demand.Code;
            Order = demand.Order;
            Name = demand.Name;
            Description = demand.Description;
            Category = demand.Category;
            CustomerId = demand.CustomerId;
            EmployerId = demand.EmployerId;
            DemandTypes = demand.Types;
        }

        public string Id { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public int? Order { get; set; }

        public string CustomerId { get; set; }

        [UIHint("Employers")]
        public string EmployerId { get; set; }

        public string DisplayType { get; private set; }

        [UIHint("DemandTypes"), DisplayName("Demand Types"), Required]
        public List<string> DemandTypes { get; set; }

        public Demand ApplyToDataModel(Demand demand)
        {
            if (demand == null)
                demand = new Demand();

            demand.Name = Name;
            demand.Code = Code;
            demand.Description = Description;
            demand.Order = Order;
            demand.Types = DemandTypes;

            return demand;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            ValidateCode(results);
            return results;
        }

        private void ValidateCode(List<ValidationResult> results)
        {
            Customer customer = Current.Customer();
            Employer employer = Current.Employer();

            //Verify if this is a duplicate demand, by checking Code field
            Demand oldDemand = Demand.GetByCode(Code, (customer == null ? null : customer.Id), (employer == null ? null : employer.Id), true);

            if (oldDemand != null)
            {
                results.Add(new ValidationResult("Duplicate demand code, please enter new code.", new List<string>() { "Code" }));
            }
        }
    }
}