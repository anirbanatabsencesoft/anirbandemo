﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AbsenceSoft.Models.AccommodationTypes
{
    public class AccommodationTypeViewModel : IViewModel, IDataTransformModel<AccommodationType>, IValidatableObject
    {
        public AccommodationTypeViewModel()
        {

        }

        public AccommodationTypeViewModel(AccommodationType type)
            : this()
        {
            if (type == null)
            {
                Duration = AccommodationDuration.Both;
                return;
            }

            Id = type.Id;
            EmployerId = type.EmployerId;
            CustomerId = type.CustomerId;
            Name = type.Name;
            Code = type.Code;
            Order = type.Order;
            PreventNew = type.PreventNew;
            IsDisabled = IsDisabledInContext(type.Code);
            Duration = type.Duration == 0 ? AccommodationDuration.Both : type.Duration;            

            if (type.Categories != null)
                Categories = string.Join(Environment.NewLine, type.Categories.Select(c => c.Value));
        }

        private bool IsDisabledInContext(string code)
        {
            var currentEmployer = Current.Employer();
            var currentCustomer = Current.Customer();
            bool isReasonDisabled = false;

            if (currentEmployer != null)
            {
                isReasonDisabled = currentEmployer.SuppressAccommodationTypes.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            else if (currentCustomer != null && !isReasonDisabled)
            {
                isReasonDisabled = currentCustomer.SuppressAccommodationTypes.Contains(code, StringComparer.InvariantCultureIgnoreCase);
            }
            return isReasonDisabled;
        }

        public bool IsAccommodationTypeSuppressedAtCustomerLevel()
        {
            if (Current.Employer() != null)
            {
                var currentCustomer = Current.Customer();
                return (currentCustomer.SuppressAccommodationTypes.Contains(this.Code, StringComparer.InvariantCultureIgnoreCase));
            }
            return false;
        }

        public string Id { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        public string DisplayType { get { return "accommodation type"; } }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DisplayName("Categories (one per line)")]
        public string Categories { get; set; }

        public int? Order { get; set; }

        [DisplayName("Prevent New")]
        public bool PreventNew { get; set; }

        public bool IsDisabled { get; set; }

        public AccommodationDuration Duration { get; set; }//Temporary, Permanent

        public AccommodationType ApplyToDataModel(AccommodationType type)
        {
            if (type == null)
                type = new AccommodationType();

            type.Name = Name;
            type.Code = Code;
            type.Order = Order;
            type.PreventNew = PreventNew;
            if (string.IsNullOrEmpty(Categories))
            {
                type.Categories = null;
            }
            else
            {
                var currentCategories = Categories.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                type.Categories = new SortedList<int, string>(currentCategories.Length);
                for (int i = 0; i < currentCategories.Length; i++)
                {
                    type.Categories.Add(i, currentCategories[i].Trim());
                }
            }

            type.Duration = Duration;
            return type;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(Categories))
                return errors;

            var categoriesError = new List<string>() { "Categories" };
            Regex r = new Regex(@"^[a-zA-Z0-9\s]*$");
            if (!r.IsMatch(Categories))
            {
                errors.Add(new ValidationResult("Categories must only contain alphanumeric characters or spaces.", categoriesError));
            }

            IEnumerable<string> categoryList = Categories.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            if (categoryList.Contains(Name))
            {
                errors.Add(new ValidationResult("Categories can not contain the name of the accommodation type.", categoriesError));
            }

            IEnumerable<string> duplicateCategories = categoryList.GroupBy(c => c)
                .Where(c => c.Count() > 1)
                .Select(c => c.Key);

            if (duplicateCategories.Count() > 0)
            {
                errors.Add(new ValidationResult("Categories can not contain any duplicate entries.", categoriesError));
            }

            return errors;
        }
    }
}