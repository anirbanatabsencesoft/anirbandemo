﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class JsonErrorModel
    {
        public string message { get; set; }
        public Exception error { get; set; }
    }
}