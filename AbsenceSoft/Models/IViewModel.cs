﻿namespace AbsenceSoft.Models
{
    public interface IViewModel
    {

        string Id { get; set; }
        string CustomerId { get; set; }
        string EmployerId { get; set; }
        string DisplayType { get; }
    }
}
