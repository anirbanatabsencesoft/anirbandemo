﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    [Serializable]
    public class EmployeeParentedModel : MultiParentedModelBase
    {
        public String EmployeeId { get; set; }
        public String EmployeeFirstName { get; set; }
        public String EmployeeLastName { get; set; }
        public String EmployeeNumber { get; set; }
    }
}