﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Communications
{
    public class CreateCummunicationViewModel : CaseParentedModel
    {
        public CreateCummunicationViewModel()
        {
            CommunicationTypeValue = (int)Data.Enums.CommunicationType.Email;
            Paperwork = new List<CommunicationPaperwork>();
            CaseAttachments = new List<Attachment>();
            SelectedCaseAttachmentIds = new List<string>();
        }
        public CreateCummunicationViewModel(Communication comm)
        {
            if (comm == null)
                comm = new Communication();            
            CommunicationId = comm.Id;
            CommunicationName = comm.Name;
            Subject = comm.Subject;
            Message = comm.Body;
            EmailRepliesType = comm.EmailRepliesType;
            SendECommunication = comm?.Case?.SendECommunication;
            if (SendECommunication.HasValue && SendECommunication.Value)
            {
                CommunicationTypeValue = (int)CommunicationType.Email;
            }            
            else
            {
                CommunicationTypeValue = (int)comm.CommunicationType;
            }
            Recipients = comm.Recipients ?? new List<AbsenceSoft.Data.Contact>();
            CCRecipients = comm.CCRecipients ?? new List<AbsenceSoft.Data.Contact>();
            Paperwork = comm.Paperwork ?? new List<CommunicationPaperwork>();
            AttachmentId = comm.AttachmentId;
            CreatedByName = GetCreatedByName(comm);
            if (comm.Id == null && !comm.IsDraft)
                Public = !Current.Customer().DefaultCommunicationToConfidential;
            else
                Public = comm.Public;
            CaseId = comm.CaseId;
            TemplateKey = comm.Template;
            ToDoItemId = comm.ToDoItemId;
            if(comm.ModifiedDate != DateTime.MinValue)        
                ModifiedDate = comm.ModifiedDate;

            CaseAttachments = Attachment.AsQueryable().Where(a => a.CaseId == comm.CaseId).OrderBy(a => a.AttachmentType).ThenByDescending(a => a.CreatedDate).ToList();

            if (comm.CaseAttachments != null)
            {
                SelectedCaseAttachmentIds = comm.CaseAttachments.Select(attach => attach.Id).ToList();
            }
            CopyCaseData(comm.Case);

            if (string.IsNullOrEmpty(Subject))
                Subject = CommunicationName;
        }

        private string GetCreatedByName(Communication comm)
        {
            if (!string.IsNullOrWhiteSpace(comm.CreatedByName))
                return comm.CreatedByName;

            if (comm.CreatedBy != null)
                return comm.CreatedBy.DisplayName;

            return string.Empty;
        }

        private void CopyCaseData(Case theCase)
        {
            if (theCase == null)
                return;

            CaseNumber = theCase.CaseNumber;
            CaseCreatedDate = theCase.CreatedDate;
            LeaveStartDate = theCase.StartDate;
            LeaveStartDateAsText = theCase.StartDate.ToUIString();
            LeaveEndDate = theCase.EndDate;
            LeaveEndDateAsText = theCase.EndDate.ToUIString();
            CopyReasonData(theCase.Reason);
            CopyEmployeeData(theCase.Employee);
        }

        private void CopyReasonData(AbsenceReason reason)
        {
            if (reason == null)
                return;

            CaseReason = reason.Name;
            ReasonCode = reason.Code;
        }

        private void CopyEmployeeData(Employee employee)
        {
            if (employee == null)
                return;

            EmployeeId = employee.Id;
            EmployeeFirstName = employee.FirstName;
            EmployeeLastName = employee.LastName;

            if(employee.Info != null)
                EmployeeAddress = employee.Info.Address;
        }

        public bool IsResend { get; set; }
    
        public string CommunicationId { get; set; }
        public string AttachmentId { get; set; }
        public string LeaveStartDateAsText { get; set; }
        public string LeaveEndDateAsText { get; set; }
        public string CommunicationName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string ToDoItemId { get; set; }
        public string TemplateKey { get; set; }
        public List<AbsenceSoft.Data.Contact> Recipients { get; set; }
        public List<AbsenceSoft.Data.Contact> CCRecipients { get; set; }
        public int? CommunicationTypeValue { get; set; }
        public List<CommunicationPaperwork> Paperwork { get; set; }
        public string CreatedByName { get; set; }
        public bool Public { get; set; }
        public List<Attachment> CaseAttachments { get; set; }
        public List<string> SelectedCaseAttachmentIds { get; set; }
        public DateTime? ModifiedDate { get; set; }
        /// <summary>
        /// Gets or sets the type of the email replies.
        /// </summary>
        /// <value>
        /// The type of the email replies.
        /// </value>
        public EmailReplies? EmailRepliesType { get; set; }

        public Communication BuildCommunication(Communication comm = null)
        {
            if(comm == null)
                comm = new Communication();

            comm.Id = CommunicationId;
            comm.AttachmentId = AttachmentId;
            comm.CaseId = CaseId;
            comm.EmployeeId = comm.Case?.Employee?.Id;
            comm.EmployeeNumber = comm.Case?.Employee?.EmployeeNumber;
			comm.CustomerId = comm.Case?.CustomerId;
            comm.EmployerId = comm.Case?.EmployerId;
            comm.Body = Message;
            comm.Subject = Subject;
            comm.Recipients = Recipients ?? new List<Data.Contact>();
            comm.CCRecipients = CCRecipients ?? new List<Data.Contact>();
            comm.Paperwork = Paperwork ?? new List<CommunicationPaperwork>();
            comm.CaseAttachments = CaseAttachments ?? new List<Attachment>();
            comm.Name = CommunicationName;
            if(CommunicationTypeValue.HasValue)
            {
                comm.CommunicationType = (CommunicationType)CommunicationTypeValue;
            }
            comm.CreatedByName = CreatedByName;
            comm.Public = Public;
            comm.EmailRepliesType = EmailRepliesType;
            if (!string.IsNullOrWhiteSpace(ToDoItemId))
                comm.ToDoItemId = ToDoItemId;

            if (!string.IsNullOrWhiteSpace(TemplateKey))
                comm.Template = TemplateKey;

            return comm;
        }
    }
}