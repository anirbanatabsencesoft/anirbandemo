﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Communications
{
    [Serializable]
    public class PaperworkReviewModel
    {
        public Communication Communication { get; set; }
        public CommunicationPaperwork Paperwork { get; set; }
        public Data.Cases.Case Case { get; set; }
        public Certification NewCertification { get; set; }
    }
}