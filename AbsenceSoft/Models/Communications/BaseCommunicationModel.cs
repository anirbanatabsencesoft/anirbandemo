﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Communications
{
    [Serializable]
    public class BaseCommunicationModel : CaseParentedModel
    {
        public string CommunicationId { get; set; }
    }
}