﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Communications;

namespace AbsenceSoft.Models.Communications
{
    public class NotificationConfigurations
    {
        public List<NotificationConfiguration> UserNotifications { get; set; }
    }
}