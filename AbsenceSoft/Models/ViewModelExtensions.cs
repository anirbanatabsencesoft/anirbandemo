﻿using AbsenceSoft.Web;

namespace AbsenceSoft.Models
{
    public static class ViewModelExtensions
    {
        public static bool IsSaveable(this IViewModel model)
        {
            var currentCustomer = Current.Customer();
            var currentEmployer = Current.Employer();
            return  !currentCustomer.IsDeleted
                && (currentEmployer == null || !currentEmployer.IsDeleted);
        }

        public static bool IsDeleteable(this IViewModel model) 
        {
            var currentEmployer = Current.Employer();
            return model.IsSaveable()
                && !string.IsNullOrEmpty(model.Id)
                && !string.IsNullOrEmpty(model.CustomerId)
                && (currentEmployer == null
                    || !string.IsNullOrEmpty(model.EmployerId));
        }

        public static bool IsCustom(this IViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Id))
                return true;

            return !string.IsNullOrWhiteSpace(model.Id) && !string.IsNullOrWhiteSpace(model.CustomerId);
        }
    }
}