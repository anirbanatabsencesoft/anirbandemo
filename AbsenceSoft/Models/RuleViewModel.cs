﻿using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public abstract class RuleViewModel : IDataTransformModel<RuleExpression>
    {
        public RuleViewModel()
        {

        }

        public RuleViewModel(RuleExpression rule)
        {
            RuleName = rule.RuleName;
            Name = rule.Name;
            Operator = rule.RuleOperator;
            Value = rule.Value;
            Parameters = rule.Parameters;
            IsCustom = rule.IsCustom;
        }

        internal string GetExpressionGroupName(RuleExpression rule, IEnumerable<RuleExpressionGroup> groups)
        {
            foreach (var group in groups)
            {
                if (group.Expressions.Any(e => e.Name == rule.Name))
                    return group.Title;
            }
            return null;
        }

        [Required, DisplayName("Rule Name")]
        public string RuleName { get; set; }

        public string Group { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Operator { get; set; }

        public object Value { get; set; }

        public bool IsCustom { get; set; }

        [Required]
        public string StringValue { get; set; }

        public List<RuleExpressionParameter> Parameters { get; set; }

        public RuleExpression ApplyToDataModel(RuleExpression rule = null)
        {
            if (rule == null)
                rule = new RuleExpression();

            rule.RuleName = RuleName;
            rule.RuleOperator = Operator;
            rule.StringValue = StringValue;
            rule.Parameters = Parameters;
            return rule;
        }
    }
}