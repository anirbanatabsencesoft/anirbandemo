﻿using System;
using System.Collections.Generic;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Models.Search
{
    public class SearchResultModel
    {
        public SearchResultModel(SearchResult searchResult) 
        {
            Title = searchResult.Title;
            Description = searchResult.Description;
            LastName = searchResult.LastName;
            DOB = searchResult.DOB;
            CaseNumber = searchResult.CaseNumber;
        }
        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string Employer { get; set; }

        public string LastName { get; set; }

        public DateTime? DOB { get; set; }

        public string CaseNumber { get; set; }
    }
}