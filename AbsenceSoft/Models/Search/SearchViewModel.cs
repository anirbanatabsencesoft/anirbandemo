﻿using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;
using System.Text;

namespace AbsenceSoft.Models.Search
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            Results = new List<SearchResult>();
            FacetResults = new List<SearchResult>();
            SearchCount = 0;
            SearchText = string.Empty;
            LastName = null;
            Employers = null;
            DOB = null;
            Page = 1;
            PageSize = 30;
            SortBy = "cn";
        }

        public SearchViewModel(SearchResults result)
        {
            Results = result.Results.ToList();
            FacetResults = result.FacetResults.ToList();

            Page = result.Criteria.PageNumber;
            PageSize = result.Criteria.PageSize;
            SortBy = result.Criteria.SortBy != null ? result.Criteria.SortBy : "cn";

            SearchCount = result.Criteria.Get<long?>("SearchCount");
            SearchText = result.Criteria.Get<string>("SearchText");
            LastName = result.Criteria.Get<string>("LastName");
            Employers = result.Criteria.Get<string[]>("Employers");
            DOB = result.Criteria.Get<DateTime?>("DateOfBirth");
        }

        public long? SearchCount { get; set; }

        [Display(Name = "Search")]
        public string SearchText { get; set; }

        public List<SearchResult> Results { get; set; }

        public List<SearchResult> FacetResults { get; set; }

        public string LastName { get; set; }

        public DateTime? DOB { get; set; }

        public string[] Employers { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public string SortBy { get; set; }
    }
}