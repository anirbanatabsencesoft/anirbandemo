﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Consultations
{
    [Serializable]
    public class EmployeeConsultationViewModel
    {
        public EmployeeConsultationViewModel()
        {

        }

        public EmployeeConsultationViewModel(EmployeeConsultation employeeConsultation)
        {
            this.Id = employeeConsultation.Id;
            this.ConsultationTypeId = employeeConsultation.ConsultationTypeId;
            this.Consult = employeeConsultation.Consult;
            this.Comments = employeeConsultation.Comments;
            this.EmployeeNumber = employeeConsultation.EmployeeNumber;
            this.HasConsult = true;
        }

        public EmployeeConsultationViewModel(ConsultationType consultationType, string employeeNumber)
            :this()
        {
            this.ConsultationTypeId = consultationType.Id;
            this.EmployeeNumber = employeeNumber;
            this.Consult = consultationType.Consult;
        }

        public string Id { get; set; }

        [Required]
        public string ConsultationTypeId { get; set; }

        [Required]
        public string Consult { get; set; }


        public bool HasConsult { get; set; }
        
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [Required]
        public string EmployeeNumber { get; set; }

        public EmployeeConsultation ApplyToDataModel(EmployeeConsultation employeeConsultation)
        {
            if (employeeConsultation == null)
                employeeConsultation = new EmployeeConsultation();
            
            employeeConsultation.ConsultationTypeId = this.ConsultationTypeId;
            employeeConsultation.Consult = this.Consult;
            employeeConsultation.Comments = this.Comments;
            employeeConsultation.CustomerId = Customer.Current.Id;
            employeeConsultation.EmployeeNumber = this.EmployeeNumber;

            return employeeConsultation;
        }
    }
}
