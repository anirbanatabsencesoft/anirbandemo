﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Data.Enums;
using System.ComponentModel;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Consultations
{
    [Serializable]
    public class ConsultationTypeViewModel
    {
        public ConsultationTypeViewModel()
        {

        }

        public ConsultationTypeViewModel(ConsultationType consultationType)
        {
            this.Id = consultationType.Id;
            this.Consult = consultationType.Consult;
            this.EmployerId = consultationType.EmployerId;
        }

        public string Id { get; set; }

        [Required]
        public string Consult { get; set; }

        [DisplayName("Employer"), UIHint("Employers")]
        public string EmployerId { get; set; }

        public ConsultationType ApplyToDataModel(ConsultationType consultationType)
        {
            if (consultationType == null)
                consultationType = new ConsultationType();

            consultationType.Consult = this.Consult;
            consultationType.CustomerId = Customer.Current.Id;
            consultationType.EmployerId = this.EmployerId;

            return consultationType;
        }
    }
}
