﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Consultations
{
    public class ConsultationEntryViewModel
    {
        public ConsultationEntryViewModel()
        {
            this.Consultations = new List<EmployeeConsultationViewModel>();
        }

        public ConsultationEntryViewModel(string employeeId)
            :this()
        {
            this.EmployeeId = employeeId;
        }

        public ConsultationEntryViewModel(string employeeId, string caseId)
            :this(employeeId)
        {
            this.CaseId = caseId;
        }

        public ConsultationEntryViewModel(List<ConsultationType> consultations, string employeeNumber)
        {
            this.Consultations = consultations.Select(c => new EmployeeConsultationViewModel(c, employeeNumber)).ToList();
        }

        public string CaseId { get; set; }

        [Required]
        public string EmployeeId { get; set; }

        public List<EmployeeConsultationViewModel> Consultations { get; set; }
    }
}