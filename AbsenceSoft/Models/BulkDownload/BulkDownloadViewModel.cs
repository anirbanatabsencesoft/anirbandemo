﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Processing;

namespace AbsenceSoft.Models.BulkDownload
{
    public class BulkDownloadViewModel
    {
        public BulkDownloadViewModel()
        {

        }

        public BulkDownloadViewModel(CaseExportRequest cb)
            :this()
        {
            if (cb == null)
                return;

            Id = cb.Id;
            DownloadType = cb.DownloadType;
            CreatedDate = cb.CreatedDate;
            Status = cb.Status;
            DocumentId = cb.DocumentId;
        }

        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string CaseId { get; set; }
        public string CaseIds { get; set; } = null;
        public string CaseExportRequestId { get; set; }
        public CaseExportFormat DownloadType { get; set; }
        public ProcessingStatus Status { get; set; }
        public string DocumentId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateString
        {
            get
            {
                return CreatedDate.ToString("MMMM dd, yyyy h:mm:ss tt");
            }
        }
    }
}