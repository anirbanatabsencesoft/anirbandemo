﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.BulkDownload
{
    public class CaseBulkDownloadViewModel
    {
        public CaseBulkDownloadViewModel()
        {
            BulkDownloads = new List<BulkDownloadViewModel>();
        }

        public string CaseId { get; set; }
        public List<BulkDownloadViewModel> BulkDownloads { get; set; }
    }
}