﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class STDGenHealthCondModel : BaseIsCompleteModel
    {
        public STDGenHealthCondModel() : base() { }

        public STDGenHealthCondModel(ToDoItem wfi) : base(wfi) { }

        public string GenHealthCond { get; set; }

        public DateTime? ConditionStartDate { get; set; }
    }
}