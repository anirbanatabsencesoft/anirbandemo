﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class ErgonomicAssessmentScheduledModel : BaseTodoModel
    {
        public ErgonomicAssessmentScheduledModel() : base() { }

        public ErgonomicAssessmentScheduledModel(ToDoItem wfi) : base(wfi) { }

        public DateTime? ErgonomicAssessmentScheduledDate { get; set; }
    }
}