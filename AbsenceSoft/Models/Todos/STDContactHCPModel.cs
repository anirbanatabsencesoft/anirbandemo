﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class STDContactHCPModel : BaseIsCompleteModel
    {
        public STDContactHCPModel() : base() { }

        public STDContactHCPModel(ToDoItem wfi) : base(wfi) { }

        public string HCPName { get; set; }
        public string HCPPhone { get; set; }
        public string HCPContactName { get; set; }
    }
}