﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class BaseIsCompleteModel : BaseTodoModel
    {
        public BaseIsCompleteModel() : base() { }

        public BaseIsCompleteModel(ToDoItem wfi) : base(wfi) { }

        public bool Completed { get; set; }
    }
}