﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class VerifyReturnToWorkModel : BaseTodoModel
    {
        public VerifyReturnToWorkModel() : base() { }

        public VerifyReturnToWorkModel(ToDoItem wfi) : base(wfi) { }

        public bool EmployeeReturned { get; set; }
        public bool EmployeeDidNotReturn { get; set; }

        public DateTime? VerifyReturnToWorkDate { get; set; }
    }
}