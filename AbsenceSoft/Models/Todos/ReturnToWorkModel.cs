﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models.Cases;

namespace AbsenceSoft.Models.Todos
{
    public class ReturnToWorkModel : BaseTodoModel
    {
        public ReturnToWorkModel() : base() 
		{
		}

        public ReturnToWorkModel(ToDoItem wfi) : base(wfi)
        {
            if (wfi != null && wfi.Employer != null)
                ShowAccommodation = wfi.Employer.HasFeature(Data.Enums.Feature.ADA);
            if (wfi.Case.IsAccommodation)
                RequireAccommodations = true;

            var evt = wfi.Case.FindCaseEvent(Data.Enums.CaseEventType.ReturnToWork) ??
                wfi.Case.FindCaseEvent(Data.Enums.CaseEventType.EstimatedReturnToWork);
            if (evt != null)
            {
                ReturnToWorkDate = evt.EventDate;
                ReturnToWorkDateText = evt.EventDate.ToUIString();
            }
            else
                ReturnToWorkDateText = null;
        }

        public bool Recertify { get; set; }
        public bool WillReturnToWork { get; set; }
        public bool WillNotReturnToWork { get; set; }

        public DateTime? ReturnToWorkDate { get; set; }
        public string ReturnToWorkDateText { get; set; }
        public bool ReleaseRequired { get; set; }
        public bool RequireAccommodations { get; set; }
        public bool ShowAccommodation { get; set; }

        public bool HasWorkRestrictions { get; set; }

    }
}