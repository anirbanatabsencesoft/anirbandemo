﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Models.Todos
{
    [Serializable]
    public class ReassignTodosModel
    {
        [Required(ErrorMessage = "Please select a user to be assigned.")]
        public string UserId { get; set; }
        public List<ReassignTodo> Todos { get; set; }       
    }

    [Serializable]
    public class ReassignTodo
    {
        public string TodoId { get; set; }
        public bool ApplyStatus { get; set; }
    }
}