﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class CaseNoteReviewModel : BaseTodoModel
    {
        public CaseNoteReviewModel() : this(null) { }
        public CaseNoteReviewModel(ToDoItem wfi)
            : base(wfi)
        {
            this.Update = false;
            this.Delete = false;
            if (wfi == null)
                return;

            var noteId = wfi.Metadata.GetRawValue<string>("NoteId");
            if (!string.IsNullOrWhiteSpace(noteId))
                Note = CaseNote.GetById(noteId);
        }

        public CaseNote Note { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }
}