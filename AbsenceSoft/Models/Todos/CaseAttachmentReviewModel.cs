﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class CaseAttachmentReviewModel : BaseTodoModel
    {
        public CaseAttachmentReviewModel() : this(null) { }
        public CaseAttachmentReviewModel(ToDoItem wfi) : base(wfi)
        {
            this.Update = false;
            this.Delete = false;
            if (wfi == null)
                return;

            BsonValue doc;
            if (wfi.Metadata.TryGetValue("Attachment", out doc))
                this.Attachment = doc.ToDotNetObject<Attachment>();
        }

        public Attachment Attachment { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }
}