﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.ToDo;

namespace AbsenceSoft.Models.Todos
{
    public class CaseClosedModel : BaseTodoModel
    {
        public CaseClosedModel() : base() { }

        public CaseClosedModel(ToDoItem wfi) : base(wfi) { }

        public bool CloseTheCase { get; set; }
        public DateTime? CaseClosedDate { get; set; }
        public string Reason { get; set; }
        public string OtherReasonText { get; set; }
        

        public DateTime ActualRTW { get; set; }

        public List<string> ValidationMessages { get; set; }
        public bool HasAnyResults { get; set; }
        public bool EnabledCaseClosureCategories { get; set; }
    }
}