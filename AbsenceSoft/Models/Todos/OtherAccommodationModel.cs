﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class OtherAccommodationModel : BaseTodoModel
    {
        public OtherAccommodationModel() : base() { }

        public OtherAccommodationModel(ToDoItem wfi) : base(wfi) { }

        public bool Completed { get; set; }
    }
}