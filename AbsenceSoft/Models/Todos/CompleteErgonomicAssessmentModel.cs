﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class CompleteErgonomicAssessmentModel : BaseTodoModel
    {
        public CompleteErgonomicAssessmentModel() : base() { }

        public CompleteErgonomicAssessmentModel(ToDoItem wfi) : base(wfi) { }

        public DateTime? AssessmentDate { get; set; }
        public string ResponseText { get; set; }
    }
}