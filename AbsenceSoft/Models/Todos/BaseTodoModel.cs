﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Models.Todos
{
    public class BaseTodoModel
    {
        public BaseTodoModel() { }
        public BaseTodoModel(ToDoItem item)
        {
            if (item == null)
                return;

            Id = item.Id;
            Title = item.Title;
            AssignedTo = item.AssignedTo != null ? item.AssignedTo.DisplayName.ToString() : string.Empty;
            Status = (int)item.Status;
            StatusText = item.Status.ToString().SplitCamelCaseString();
            Priority = (int)item.Priority;
            PriorityText = item.Priority.ToString().SplitCamelCaseString();
            DueDate = item.DueDate;
            DueDateText = item.DueDate.ToUIString();
            Optional = item.Optional;
            ItemType = item.ItemType;
            CancelReason = item.CancelReason;
            ModifiedBy = item.ModifiedBy.DisplayName;
            CreatedDate = item.CreatedDate;

            Case c = item.Case;
            if (c != null)
            {
                CaseId = c.Id;
                CaseNumber = c.CaseNumber;
                CaseTitle = c.Title;
                CaseIsInquiry = (c.Status == CaseStatus.Inquiry);
                StartDate = c.StartDate;
                StartDateText = c.StartDate.ToUIString();
                EndDate = c.EndDate;
                EndDateText = c.EndDate.ToUIString();
                CaseType = c.CaseType;

                Employee e = c.Employee;
                if (e != null)
                {
                    EmployeeId = e.Id;
                    EmployeeNumber = e.EmployeeNumber;
                    EmployeeFirstName = e.FirstName;
                    EmployeeLastName = e.LastName;
                    EmployerId = e.EmployerId;
                    EmployerName = e.EmployerName;
                }
            }

            
        }

        // Todos Item Stuff
        public string Id { get; set; }
        public string Title { get; set; }
        public string AssignedTo { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public int Priority { get; set; }
        public string PriorityText { get; set; }
        public DateTime DueDate { get; set; }
        public string DueDateText { get; set; }
        public bool Optional { get; set; }
        public ToDoItemType ItemType { get; set; }
        public string ResultText { get; set; }

        [Required(ErrorMessage = "You must provide a reason when canceling the ToDo", AllowEmptyStrings = false)]
        public string CancelReason { get; set; }

        public string ModifiedBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CreatedDate { get; set; }

        // Case Info
        public string CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string CaseTitle { get; set; }
        public bool CaseIsInquiry { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateText { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndDateText { get; set; }
        public string CaseType { get; set; }

        // Employee Info
        public string EmployeeId { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployerId { get; set; }
        public string EmployerName { get; set; }
    }
}