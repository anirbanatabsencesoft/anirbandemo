﻿using AbsenceSoft.Data.Questions;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class QuestionModel:BaseTodoModel
    {
        public QuestionModel() 
        {
        }

        public QuestionModel(Question ques) 
        {
            Title = ques.Title;
            AnswerOptions = ques.AnswerOptions;
        }

        public string Title { get; set; }

        public List<string> AnswerOptions { get; set; }

        public virtual Question ApplyToDataModel(Question entity = null)
        {
            if (entity == null)
                entity = new Question();
            entity.Title = Title;
            entity.AnswerOptions = AnswerOptions;
            return entity;
        }


    }
}