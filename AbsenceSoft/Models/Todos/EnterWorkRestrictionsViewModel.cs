﻿using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Demands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class EnterWorkRestrictionsViewModel : BaseTodoModel
    {
        public EnterWorkRestrictionsViewModel() : base()
        {
        }
        public EnterWorkRestrictionsViewModel(ToDoItem item) : base(item)
        {
        }
    }
}