﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class PaperworkDueModel : BaseTodoModel
    {
        public PaperworkDueModel() : base() { }
        public PaperworkDueModel(ToDoItem item)
            : base(item)
        {
            CommunicationId = item.Metadata.GetRawValue<string>("CommunicationId");
            PaperworkId = item.Metadata.GetRawValue<string>("PaperworkId");
            PaperworkDue = item.DueDate;
            PaperworkDueText = item.DueDate.ToUIString();
            NewDueDate = NewDueDate == default(DateTime) ? DateTime.Now.Date : NewDueDate;
        }

        public string CommunicationId { get; set; }
        public string PaperworkId { get; set; }
        public string ReturnAttachmentId { get; set; }
        public DateTime PaperworkDue { get; set; } // should be the same as the task due date
        public string PaperworkDueText { get; set; }

        // POST back fields
        public bool PaperworkReceived { get; set; }
        public bool ExtendGracePeriod { get; set; }
        public bool CloseCase { get; set; }
        public DateTime NewDueDate { get; set; }
    }
}