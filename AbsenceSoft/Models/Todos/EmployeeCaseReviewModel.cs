﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class EmployeeCaseReviewModel : BaseTodoModel
    {
        public EmployeeCaseReviewModel() : this(null) { }
        public EmployeeCaseReviewModel(ToDoItem wfi)
            : base(wfi)
        {
            this.Update = false;
            this.Delete = false;
            if (wfi == null)
                return;

            this.EmployeeCaseId = wfi.Metadata.GetRawValue<string>("EmployeeCaseId");

        }

        public string EmployeeCaseId { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }
}