﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class EmployeeWorkScheduleReviewModel : BaseTodoModel
    {
        public EmployeeWorkScheduleReviewModel() : this(null) { }
        public EmployeeWorkScheduleReviewModel(ToDoItem wfi)
            : base(wfi)
        {
            this.Update = false;
            this.Delete = false;
            if (wfi == null)
                return;

            Employee emp = Employee.GetById(wfi.EmployeeId);
            this.EmployeeInfo = new CreateEmployeeModel(emp);
        }

        public CreateEmployeeModel EmployeeInfo { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }
}