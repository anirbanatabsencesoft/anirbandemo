﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class HRISScheduleChangeModel : BaseTodoModel
    {
        public HRISScheduleChangeModel() : base() { }

        public HRISScheduleChangeModel(ToDoItem wfi) : base(wfi) { }

        public bool Completed { get; set; }
    }
}