﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Models.Demands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AbsenceSoft.Models.Administration;

namespace AbsenceSoft.Models.Todos
{
    public class PaperWorkTodoReviewModel : BaseTodoModel
    {
        public PaperWorkTodoReviewModel() : base()
        {
            CertificationDetail = new CertificationDetailTodoModel();
            Fields = new List<PaperworkFieldModel>();
            ReAssign = new ReassignTodosModel();
            HasCertifications = "false";
        }
        public PaperWorkTodoReviewModel(ToDoItem wfi) : base(wfi)
        {
            Fields = new List<PaperworkFieldModel>();
            CertificationDetail = new CertificationDetailTodoModel();
            HasCertifications = "false";
        }

        public string ActionType { get; set; }
        public CertificationDetailTodoModel CertificationDetail { get; set; }
        //[RequiredFields]
        public List<PaperworkFieldModel> Fields { get; set; }

        public string HasCertifications { get; set; }

        public ReassignTodosModel ReAssign { get; set; }
        public List<UsersListVM> UserList { get; set; }
        public DateTime? CaseStartDate { get; set; }
        public DateTime? CaseEndDate { get; set; }
    }

    public class PaperworkFieldModel
    {
        public PaperworkField Field { get; set; }
        public List<WorkRestrictionViewModel> WorkRestrictions { get; set; }
    }

    // TODO: fix serverside validation for fields, uncomment attributes
    //public class RequiredFieldsAttribute : ValidationAttribute
    //{
    //    public RequiredFieldsAttribute() : base("Some fields are required.") { }

    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        var model = validationContext.ObjectInstance as PaperWorkTodoReviewModel;
    //        if (model.Fields != null && model.Fields.Count > 0)
    //        {
    //            foreach (var currentField in model.Fields)
    //            {
    //                if (currentField.Required && string.IsNullOrEmpty(currentField.Value))
    //                    return new ValidationResult(base.ErrorMessage);
    //            }
    //        }
    //        return ValidationResult.Success;
    //    }
    //}
}