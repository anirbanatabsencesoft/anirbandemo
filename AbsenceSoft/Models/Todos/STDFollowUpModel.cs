﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class STDFollowUpModel : BaseIsCompleteModel
    {
        public STDFollowUpModel() : base() { }

        public STDFollowUpModel(ToDoItem wfi) : base(wfi) { }

        public DateTime? ContactEmployeeDueDate { get; set; }
        public DateTime? ContactHCPDueDate { get; set; }        
    }
}