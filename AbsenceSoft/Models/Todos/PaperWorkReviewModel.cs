﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class PaperWorkReviewModel : BaseTodoModel
    {
        public PaperWorkReviewModel() : base() { }
        public PaperWorkReviewModel(ToDoItem wfi) : base(wfi) { }

        public bool PaperworkIncomplete { get; set; }
        public bool PaperworkApproved { get; set; }
        public bool PaperworkDenied { get; set; }        
    }
}