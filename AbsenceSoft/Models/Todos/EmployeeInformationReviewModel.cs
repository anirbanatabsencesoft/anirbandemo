﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Employees;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class EmployeeInformationReviewModel : BaseTodoModel
    {
        public EmployeeInformationReviewModel() : this(null) { }
        public EmployeeInformationReviewModel(ToDoItem wfi)
            : base(wfi)
        {
            this.Update = false;
            this.Delete = false;
            if (wfi == null)
                return;

            BsonValue doc;
            if (wfi.Metadata.TryGetValue("OldValues", out doc))
            {
                this.OldValues = doc.ToDotNetObject<EmployeeModel>();
            }

            if (wfi.Metadata.TryGetValue("NewValues", out doc))
            {
                this.NewValues = doc.ToDotNetObject<EmployeeModel>();
            }

            this.EmployeeId = wfi.Metadata.GetRawValue<string>("EmployeeId");
            
        }

        public EmployeeModel OldValues { get; set; }
        public EmployeeModel NewValues { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }

    public class EmployeeInformation  : BaseEmployeeModel
    {
        
    }
}