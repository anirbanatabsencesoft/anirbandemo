﻿using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Todos
{
    public class EquipmentSoftwareOrderedModel : BaseTodoModel
    {
        public EquipmentSoftwareOrderedModel() : base() { }

        public EquipmentSoftwareOrderedModel(ToDoItem wfi) : base(wfi) { }

        public bool Completed { get; set; }
    }
}