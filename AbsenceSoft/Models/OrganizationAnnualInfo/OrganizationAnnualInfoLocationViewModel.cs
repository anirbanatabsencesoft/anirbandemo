﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.OrganizationAnnualInfos
{
    public class OrganizationAnnualInfoLocationViewModel
    {
        public OrganizationAnnualInfoLocationViewModel()
        {
            this.OrganizationAnnualInfoYears = new List<OrganizationAnnualInfoYearsViewModel>();
        }

        /// <summary>
        /// Gets or sets the organization annual information years.
        /// </summary>
        /// <value>
        /// The organization annual information years.
        /// </value>
        public List<OrganizationAnnualInfoYearsViewModel> OrganizationAnnualInfoYears { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public string Location { get; set; }

    }
}