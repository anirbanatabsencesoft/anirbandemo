﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.OrganizationAnnualInfos
{
    public class OrganizationAnnualInfoViewModel :  IDataTransformModel<OrganizationAnnualInfo>
    {
        public OrganizationAnnualInfoViewModel()
        {

        }

        public OrganizationAnnualInfoViewModel(OrganizationAnnualInfo organizationAnnualInfo)
            : this()
        {
            if (organizationAnnualInfo == null)
                return;

            this.Id = organizationAnnualInfo.Id;
            this.EmployerId = organizationAnnualInfo.EmployerId;
            this.OrganizationCode = organizationAnnualInfo.OrganizationCode;
            this.Year = organizationAnnualInfo.Year;
            this.AverageEmployeeCount = organizationAnnualInfo.AverageEmployeeCount;
            this.TotalHoursWorked = organizationAnnualInfo.TotalHoursWorked;

        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets or sets the org code.
        /// </summary>
        /// <value>
        /// The org code.
        /// </value>
        public string OrganizationCode { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets the average employee count.
        /// </summary>
        /// <value>
        /// The average employee count.
        /// </value>
        public int? AverageEmployeeCount { get; set; }

        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        public decimal? TotalHoursWorked { get; set; }


        public OrganizationAnnualInfo ApplyToDataModel(OrganizationAnnualInfo organizationAnnualInfo = null)
        {
            if (organizationAnnualInfo == null)
                organizationAnnualInfo = new OrganizationAnnualInfo();

            organizationAnnualInfo.OrganizationCode = this.OrganizationCode;
            organizationAnnualInfo.Year = this.Year;
            organizationAnnualInfo.AverageEmployeeCount = this.AverageEmployeeCount;
            organizationAnnualInfo.TotalHoursWorked = this.TotalHoursWorked;

            return organizationAnnualInfo;
        }

      

        public bool IsValidForBulkUpload
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.OrganizationCode)
                    && this.Year >0
                    && this.AverageEmployeeCount.HasValue
                    && this.TotalHoursWorked.HasValue;
            }
        }
    }
}