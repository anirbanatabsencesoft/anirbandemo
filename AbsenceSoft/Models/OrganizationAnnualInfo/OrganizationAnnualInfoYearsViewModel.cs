﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.OrganizationAnnualInfos
{
    /// <summary>
    /// 
    /// </summary>
    public class OrganizationAnnualInfoYearsViewModel : IDataTransformModel<OrganizationAnnualInfo>
    {
        public OrganizationAnnualInfoYearsViewModel()
        {
      
        }


        public OrganizationAnnualInfo ApplyToDataModel(OrganizationAnnualInfo organizationAnnualInfo = null)
        {
            if (organizationAnnualInfo == null)
                organizationAnnualInfo = new OrganizationAnnualInfo();

            organizationAnnualInfo.OrganizationCode = this.OrganizationCode;
            organizationAnnualInfo.Year = this.Year;
            organizationAnnualInfo.AverageEmployeeCount = this.AverageEmployeeCount;
            organizationAnnualInfo.TotalHoursWorked = this.TotalHoursWorked;

            return organizationAnnualInfo;
        }
        /// <summary>
        /// Gets or sets the name of the year.
        /// </summary>
        /// <value>
        /// The name of the year.
        /// </value>
        public string YearName { get; set; }

        /// <summary>
        /// Gets or sets the org code.
        /// </summary>
        /// <value>
        /// The org code.
        /// </value>
        public string OrganizationCode { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets the average employee count.
        /// </summary>
        /// <value>
        /// The average employee count.
        /// </value>
        public int? AverageEmployeeCount { get; set; }

        /// <summary>
        /// Gets or sets the total hours worked.
        /// </summary>
        /// <value>
        /// The total hours worked.
        /// </value>
        public decimal? TotalHoursWorked { get; set; }
    }
}