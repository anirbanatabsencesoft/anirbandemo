﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.OrganizationAnnualInfos
{
    /// <summary>
    /// 
    /// </summary>
    public class BulkUploadAnnualInfo
    {
        public BulkUploadAnnualInfo()
        {
            this.AnnualInfos = new List<OrganizationAnnualInfoViewModel>();
        }

        /// <summary>
        /// Gets or sets the annual infos.
        /// </summary>
        /// <value>
        /// The annual infos.
        /// </value>
        public List<OrganizationAnnualInfoViewModel> AnnualInfos { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BulkUploadAnnualInfo"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [partial success].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [partial success]; otherwise, <c>false</c>.
        /// </value>
        public bool PartialSuccess { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }
    }
}