﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.OrganizationAnnualInfos
{
  

    /// <summary>
    /// 
    /// </summary>
    public class OrganizationAnnualInfoEntryViewModel
    {
        public OrganizationAnnualInfoEntryViewModel()
        {
            this.OrganizationAnnualInfoLocations = new List<OrganizationAnnualInfoLocationViewModel>();
        }

        public OrganizationAnnualInfoEntryViewModel(List<OrganizationAnnualInfo> savedAnnualInfo,List<int> years,int numberOfYearsBack)
        {
            this.OrganizationAnnualInfoLocations = new List<OrganizationAnnualInfoLocationViewModel>();
            this.Years = years;
            this.NumberOfYearsBack += numberOfYearsBack;
            var locations = savedAnnualInfo.Select(pm => pm.OrganizationCode).Distinct().ToList();

            foreach (string location in locations)
            {
                var locationViewModel = new OrganizationAnnualInfoLocationViewModel();
                locationViewModel.Location = location;
                foreach (int year in Years)
                {
                    var annualInfo = savedAnnualInfo.Where(c => c.OrganizationCode == location && c.Year == year)
                        .FirstOrDefault();

                    var yearViewModel = new OrganizationAnnualInfoYearsViewModel();
                    yearViewModel.YearName = year.ToString();
                    yearViewModel.Year = year;
                    yearViewModel.OrganizationCode = location;
                    if (annualInfo != null)
                    {
                        yearViewModel.AverageEmployeeCount = annualInfo.AverageEmployeeCount;
                        yearViewModel.TotalHoursWorked = annualInfo.TotalHoursWorked;
                    }

                    locationViewModel.OrganizationAnnualInfoYears.Add(yearViewModel);
                }
                OrganizationAnnualInfoLocations.Add(locationViewModel);
            }
        }



        /// <summary>
        /// Gets or sets the organization annual information locations.
        /// </summary>
        /// <value>
        /// The organization annual information locations.
        /// </value>
        public List<OrganizationAnnualInfoLocationViewModel> OrganizationAnnualInfoLocations { get; set; }

        /// <summary>
        /// Gets or sets the years.
        /// </summary>
        /// <value>
        /// The years.
        /// </value>
        public List<int> Years { get; set; }

        /// <summary>
        /// Gets or sets the current year.
        /// </summary>
        /// <value>
        /// The current year.
        /// </value>
        public int NumberOfYearsBack { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="OrganizationAnnualInfoEntryViewModel"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [partial success].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [partial success]; otherwise, <c>false</c>.
        /// </value>
        public bool PartialSuccess { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }
    }

  

  
}