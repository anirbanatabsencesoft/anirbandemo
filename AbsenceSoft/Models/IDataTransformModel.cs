﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public interface IDataTransformModel<TEntity> where TEntity:class
    {
        TEntity ApplyToDataModel(TEntity entity);
    }
}