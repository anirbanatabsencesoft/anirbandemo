﻿using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Models.Signup
{
    public class CreateCustomerModel
    {
        public CreateCustomerModel()
        {
            Country = "US";
        }
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        public string SubDomain { get; set; }


        [Required(AllowEmptyStrings = false)]
        public string Phone { get; set; }

        public string Fax { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string City { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string State { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string PostalCode { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Country { get; set; }

        public string LogoId { get; set; }

        public bool IsTpaCustomerWithEmployerServiceOptions { get; set; }

        public string Error { get; set; }

    }
}