﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbsenceSoft.Models.Signup
{
    public class CreateUserModel
    {
        public CreateUserModel()
        {
            ApplyRoles = new List<RoleModel>();
            Employers = new List<string>();
        }

        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string JobTitle { get; set; }
        public string Error { get; set; }
        public DateTime? EndDate { get; set; }
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string SubDomain { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public bool AutoAssignCases { get; set; }
        public string AssignedEmployeeId { get; set; }
        public string AssignedEmployeeEmployerId { get; set; }
        public bool IsSelfServiceUser { get; set; }
        public List<RoleModel> ApplyRoles { get; set; }
        public List<string> Employers { get; set; }

        /// <summary>
        /// Gets or sets to do notification frequency.
        /// </summary>
        /// <value>
        /// To do notification frequency.
        /// </value>
        public NotificationFrequencyType? ToDoNotificationFrequency { get; set; }

        /// <summary>
        /// Applies the create model changes to the User model.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public User ApplyToDataModel(User user)
        {
            user.Email = Email.ToLower();
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.Title = Title;
            user.JobTitle = JobTitle;
            user.ContactInfo = user.ContactInfo ?? new Contact();
            user.ContactInfo.WorkPhone = Phone;
            user.ContactInfo.Fax = Fax;
            user.EndDate = EndDate;
            user.ModifiedById = User.DefaultUserId;
            user.ContactInfo = user.ContactInfo ?? new Contact();
            user.ContactInfo.WorkPhone = Phone;
            user.ContactInfo.Fax = Fax;
            user.UserType = IsSelfServiceUser ? UserType.SelfService : UserType.Portal;

            UpdateUserRoles(user);
            UpdateUserEmployerAccess(user);

            return user;
        }

        /// <summary>
        /// Updates the user roles.
        /// </summary>
        /// <param name="user">The user.</param>
        private void UpdateUserRoles(User user)
        {
            if (ApplyRoles != null)
            {
                user.Roles = ApplyRoles.Where(ar => ar.Apply).Select(ar => ar.Id).ToList();
            }
            else
            {
                user.Roles = new List<string>();
            }
        }

        /// <summary>
        /// Updates the user employer access list, including the assigned employee id and auto assign property.
        /// </summary>
        /// <param name="user">The user.</param>
        private void UpdateUserEmployerAccess(User user)
        {
            user.Employers = user.Employers.Where(e => Employers.Any(ea => ea == e.EmployerId)).ToList();

            foreach (var employerId in Employers)
            {
                var employerAccess = user.Employers.FirstOrDefault(e => e.EmployerId == employerId)
                    ?? user.Employers.AddFluid(new EmployerAccess { EmployerId = employerId });

                employerAccess.AutoAssignCases = AutoAssignCases;

                if (!string.IsNullOrEmpty(AssignedEmployeeEmployerId) && employerAccess.EmployerId == AssignedEmployeeEmployerId)
                {
                    employerAccess.EmployeeId = AssignedEmployeeId;
                }
                else
                {
                    employerAccess.EmployeeId = null;
                }
            }
        }
    }
}