﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System.Collections.Generic;

namespace AbsenceSoft.Models.Signup
{
    public class UpdateEmployerModel
    {
        public UpdateEmployerModel()
        {
            Holidays = new List<Holiday>();
        }

        public string Id { get; set; }
        public string UserId { get; set; }
        public string SignatureName { get; set; }
        public int? MaxEmployees { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Country { get; set; }
        public string ReferenceCode { get; set; }
        public bool EnableSpouseAtSameEmployerRule { get; set; }
        public bool Enable50In75MileRule { get; set; }
        public PeriodType FMLPeriodType { get; set; }
        public Month? ResetMonth { get; set; }
        public int? ResetDayOfMonth { get; set; }
        public double FTWeeklyWorkHours { get; set; }
        public bool AllowIntermittentForBirthAdoptionOrFosterCare { get; set; }
        public bool IgnoreScheduleForPriorHoursWorked { get; set; }
        public bool IgnoreAverageMinutesWorkedPerWeek { get; set; }
        public bool IsPubliclyTradedCompany { get; set; }
        public string DefaultPayScheduleId { get; set; }
        public List<Holiday> Holidays { get; set; }
        public string Url { get; set; }
        public string LogoId { get; set; }
        public bool IsTpaCustomerWithEmployerServiceOptions { get; set; }
        public string CustomerServiceOptionId { get; set; }
        public string Error { get; set; }
    }
}