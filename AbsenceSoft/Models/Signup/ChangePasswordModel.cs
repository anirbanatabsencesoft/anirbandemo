﻿using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Signup
{
    public class ChangePasswordModel : IValidatableObject
    {
        [Required]
        [DisplayName("UserId/E-mail")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string UserId { get; set; }

        [Required]
        [DisplayName("Current Password"), UIHint("EyeToggle")]
        public string CurrentPassword { get; set; }

        [Required]
        [Display(Name = "New Password"), UIHint("EyeToggle")]
        public string NewPassword { get; set; }

        [Required]
        [DisplayName("Confirm New Password"), UIHint("EyeToggle")]
        public string ConfirmNewPassword { get; set; }

        public string Error { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (this.NewPassword != this.ConfirmNewPassword)
                results.Add(new ValidationResult("Confirmation password does not match the new password provided."));
            
            if (this.CurrentPassword == this.NewPassword)
                results.Add(new ValidationResult("New password cannot match old password."));

            

            using (AdministrationService svc = new AdministrationService())
            {
                var user = User.Current ?? User.AsQueryable().FirstOrDefault(u => u.Email == this.UserId);
                if (!svc.CheckPassword(user, this.CurrentPassword))
                    results.Add(new ValidationResult("Current password is wrong."));
            }

            return results;
        }
    }
}