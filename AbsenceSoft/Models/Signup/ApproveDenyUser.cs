﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Signup
{
    public class ApproveDenyUser
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public bool IsApprove { get; set; }
        public string CustomerId { get; set;}
        public string ResetKey { get; set; }
    }
}