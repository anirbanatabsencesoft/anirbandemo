﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Sso
{
    public class SsoProfileViewModel : IViewModel, IValidatableObject
    {
        public SsoProfileViewModel()
        {
            IsEss = true;
            IsPortal = true;
        }

        public SsoProfileViewModel(SsoProfile profile)
            :this()
        {
            if(profile == null)
            {
                IsNew = true;
                return;
            }

            IdPEntityId = profile.IdpEntityId;
            SingleSignOnServiceUrl = profile.SingleSignOnServiceUrl;
            SingleSignOnServiceBinding = profile.SingleSignOnServiceBinding;
            RequestedAuthnContext = profile.RequestedAuthnContext;
            SingleLogoutServiceUrl = profile.SingleLogoutServiceUrl;
            SingleLogoutServiceResponseUrl = profile.SingleLogoutServiceResponseUrl;
            SingleLogoutServiceBinding = profile.SingleLogoutServiceBinding;
            LogoutRequestLifetime = profile.LogoutRequestLifeTime.TotalMilliseconds;
            NameIdFormat = profile.NameIDFormat;
            DigestMethod = profile.DigestMethod;
            SignatureMethod = profile.SignatureMethod;
            KeyEncryptionMethod = profile.KeyEncryptionMethod;
            DataEncryptionMethod = profile.DataEncryptionMethod;
            ClockSkew = profile.ClockSkew.TotalMilliseconds;
            ArtifactResolutionServiceUrl = profile.ArtifactResolutionServiceUrl;
            EntityId = profile.EntityId;
            SelfServiceLoginSourceName = profile.SelfServiceLoginSourceName;
            PortalLoginSourceName = profile.PortalLoginSourceName;
            LoginSourceShowLogo = profile.LoginSourceShowLogo;
            IsEss = profile.IsEss;
            IsPortal = profile.IsPortal;
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string DisplayType { get { return "SSO Profile"; } }
        public string EmployerId { get; set; }
        
        public bool IsNew { get; set; }
        public bool Saved { get; set; }

        public string EntityId { get; set; }

        [DisplayName("IdP ENTITY ID"), Required]
        public string IdPEntityId { get; set; }

        [DisplayName("SINGLE SIGNON SERVICE URL"), Required]
        public string SingleSignOnServiceUrl { get; set; }

        [DisplayName("SINGLE SIGNON SERVICE BINDING"), Required]
        public string SingleSignOnServiceBinding { get; set; }

        [DisplayName("REQUESTED AUTHN CONTEXT")]
        public string RequestedAuthnContext { get; set; }

        [DisplayName("SINGLE LOGOUT SERVICE URL"), Required]
        public string SingleLogoutServiceUrl { get; set; }

        [DisplayName("SINGLE LOGOUT SERVICE RESPONSE URL")]
        public string SingleLogoutServiceResponseUrl { get; set; }

        [DisplayName("SINGLE LOGOUT SERVICE BINDING"), Required]
        public string SingleLogoutServiceBinding { get; set; }

        [DisplayName("LOGOUT REQUEST LIFETIME (ms)"), Required]
        public double? LogoutRequestLifetime { get; set; }

        [DisplayName("NAMEID FORMAT"), Required]
        public string NameIdFormat { get; set; }

        [DisplayName("DIGEST METHOD"), Required]
        public string DigestMethod { get; set; }

        [DisplayName("SIGNATURE METHOD"), Required]
        public string SignatureMethod { get; set; }

        [DisplayName("KEY ENCRYPTION METHOD"), Required]
        public string KeyEncryptionMethod { get; set; }

        [DisplayName("DATA ENCRYPTION METHOD"), Required]
        public string DataEncryptionMethod { get; set; }

        [DisplayName("CLOCK SKEW (ms)"), Required]
        public double? ClockSkew { get; set; }

        [DisplayName("ARTIFACT RESOLUTION SERVICE URL")]
        public string ArtifactResolutionServiceUrl { get; set; }

        [DisplayName("IDP ESS LOGIN PORTAL NAME")]
        public string SelfServiceLoginSourceName { get; set; }

        [DisplayName("IDP LOGIN PORTAL NAME")]
        public string PortalLoginSourceName { get; set; }

        [DisplayName("SHOW LOGO FOR SSO LOGIN")]
        public bool LoginSourceShowLogo { get; set; }

        [DisplayName("SSO PROFILE APPLIES TO ESS")]
        public bool IsEss { get; set; }

        [DisplayName("SSO PROFILE APPLIES TO PORTAL")]
        public bool IsPortal { get; set; }

        public string RoleName { get; set; }

        public string EmployerReferenceCode { get; set; }
        
        public SsoProfile ApplyToDataModel(SsoProfile profile, HttpPostedFileBase partnerCertificate)
        {
            if (profile == null)
                profile = new SsoProfile();

            profile.IdpEntityId = IdPEntityId;
            profile.SingleSignOnServiceUrl = SingleSignOnServiceUrl;
            profile.SingleSignOnServiceBinding = SingleSignOnServiceBinding;
            profile.RequestedAuthnContext = RequestedAuthnContext;
            profile.SingleLogoutServiceUrl = SingleLogoutServiceUrl;
            profile.SingleLogoutServiceResponseUrl = SingleLogoutServiceResponseUrl;
            profile.SingleLogoutServiceBinding = SingleLogoutServiceBinding;
            profile.LogoutRequestLifeTime = TimeSpan.FromMilliseconds(LogoutRequestLifetime.Value);
            profile.NameIDFormat = NameIdFormat;
            profile.DigestMethod = DigestMethod;
            profile.SignatureMethod = SignatureMethod;
            profile.KeyEncryptionMethod = KeyEncryptionMethod;
            profile.DataEncryptionMethod = DataEncryptionMethod;
            profile.ClockSkew = TimeSpan.FromMilliseconds(ClockSkew.Value);
            profile.ArtifactResolutionServiceUrl = ArtifactResolutionServiceUrl;
            profile.SelfServiceLoginSourceName = SelfServiceLoginSourceName;
            profile.PortalLoginSourceName = PortalLoginSourceName;
            profile.LoginSourceShowLogo = LoginSourceShowLogo;
            profile.IsPortal = IsPortal;
            profile.IsEss = IsEss;

            if (partnerCertificate != null)
                profile.PartnerCertificate = partnerCertificate.InputStream.ReadAllBytes();

            return profile;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (!IsPortal && !IsEss)
            {
                results.Add(new ValidationResult("You must select whether this profile applies to the portal and/or ESS"));
            }

            return results;
        }
    }
}