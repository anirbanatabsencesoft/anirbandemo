﻿namespace AbsenceSoft.Models
{
    public interface IDisableableViewModel
    {
        bool Suppressed { get; set; }
        string Code { get; set; }
        string ClassTypeName { get; set; }
    }
}