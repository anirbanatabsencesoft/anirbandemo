﻿using AbsenceSoft.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
	[Serializable]
	public class NoteModel
	{
		[DisplayName("Text")]
		[AngularRequired]
		[AngularInputType(HTML5InputType.Text)]
		public string NoteId { get; set; }
		public String Notes { get; set; }
		public string CreatedDate { get; set; }
		public bool Public { get; set; }
		public string CategoryText { get; set; }
        public string CategoryCode { get; set; }
        public int? Category { get; set; }
		public string CreatedBy { get; set; }
		public string EmployeeNumber { get; set; }
		public string CaseNumber { get; set; }
        public string EmployeeId { get; set; }
        public string TemplateId { get; set; }
    }
}