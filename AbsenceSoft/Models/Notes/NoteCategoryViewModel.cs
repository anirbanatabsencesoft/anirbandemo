﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Notes;
using AbsenceSoft.Logic.Tasks;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
    public class NoteCategoryViewModel:IViewModel, IValidatableObject, IDataTransformModel<NoteCategory>, IDisableableViewModel
    {
        public NoteCategoryViewModel()
        {
            ChildCategories = new List<NoteCategoryViewModel>();
        }

        public NoteCategoryViewModel(NoteCategory category)
            : this()
        {
            if (category == null)
                return;

            Id = category.Id;
            CustomerId = category.CustomerId;
            Code = category.Code;
            Name = category.Name;
            Description = category.Description;
            EmployerId = category.EmployerId;
            ParentCode = category.ParentCode;
            TargetsEmployee = (category.Target & EntityTarget.Employee) == EntityTarget.Employee;
            TargetsCase = (category.Target & EntityTarget.Case) == EntityTarget.Case;
            Target = null;
            HideInESS = category.HideInESS;
            NoteCategoryRoles = category.RoleIds;
            Suppressed = IsDisabled = category.IsSuppressed(Current.Customer(), Current.Employer());
            
            if (category.ChildCategories != null)
                ChildCategories = category.ChildCategories.Select(nc => new NoteCategoryViewModel(nc)).ToList();
        }

        public NoteCategory ApplyToDataModel(NoteCategory category)
        {
            if (category == null)
                category = new NoteCategory();

            category.Name = Name;
            category.ParentCode = ParentCode;
            category.Description = Description;
            category.Target = SetTarget();
            category.HideInESS = HideInESS;
            category.RoleIds = NoteCategoryRoles;
            category.Code = Code;

            return category;
        }

        public string DisplayType { get { return "Note Category"; } }

        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        public string Target { get; set; }

        [DisplayName("Employee")]
        public bool TargetsEmployee { get; set; }

        [DisplayName("Case")]
        public bool TargetsCase { get; set; }

        [DisplayName("Hide In ESS")]
        public bool? HideInESS { get; set; }

        public bool IsDisabled { get; set; }

        [DisplayName("Parent Note Category"), UIHint("NoteCategory")]
        public string ParentCode { get; set; }

        [DisplayName("Enable Template?")]
        public bool Suppressed { get; set; }

        public string ClassTypeName { get; set; } = "NoteCategory";

        [DisplayName("Roles"), UIHint("Roles")]
        public List<string> NoteCategoryRoles { get; set; }

        public List<NoteCategoryViewModel> ChildCategories { get; set; }

        public bool IsCustom
        {
            get
            {
                return !string.IsNullOrEmpty(this.CustomerId);
            }
        }

        public bool HasChildren()
        {
            return NoteCategory.AsQueryable().Count(nc => nc.ParentCode != null && nc.ParentCode == Code) > 0;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var noteService = new NotesService(Current.Customer(), Current.Employer(), Current.User()))
            {

                if (!string.IsNullOrEmpty(Id))
                {
                    NoteCategory savedNoteCategory = noteService.GetNoteCategoryById(Id);
                    if (ParentCode != savedNoteCategory.ParentCode && HasChildren())
                    {
                        results.Add(new ValidationResult("You can't change the parent note category when this category has children", new List<string>() { "ParentCode" }));
                    }
                    if (ParentCode == Code)
                    {
                        results.Add(new ValidationResult("Category can not be a parent of itself", new List<string>() { "ParentCode" }));
                    }
                }

                
                
                if (noteService.NoteCategoryCodeIsInUse(Id, CustomerId, Code))
                    results.Add(new ValidationResult("This code is already in use by a different note category.", new List<string>() { "Code" }));

                if (!TargetsCase && !TargetsEmployee)
                    results.Add(new ValidationResult("You must select whether this note category targets the case, employee, or both."));
            }

            return results;
        }

        private EntityTarget SetTarget()
        {
            EntityTarget target = EntityTarget.None;
            if (TargetsCase)
                target = target | EntityTarget.Case;

            if (TargetsEmployee)
                target = target | EntityTarget.Employee;

            return target;
        }        
    }
}