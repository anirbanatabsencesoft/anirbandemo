﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
    public class CreateNoteViewModel
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string CaseId { get; set; }
        public string Notes { get; set; }
        public bool Public { get; set; }
        public int? Category { get; set; }
        public string CategoryCode { get; set; }
        public string TemplateId { get; set; }
    }
}