﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
    public class DeleteCaseIntProcNoteViewModel
    {
        public string QuestionId { get; set; }

        public string CaseId { get; set; }
    }
}