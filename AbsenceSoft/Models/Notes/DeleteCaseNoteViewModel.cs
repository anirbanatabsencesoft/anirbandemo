﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
    public class DeleteCaseNoteViewModel
    {
        public string Id { get; set; }
    }
}