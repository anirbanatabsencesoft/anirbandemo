﻿using AbsenceSoft.Data.Notes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Notes
{
    public class NoteTemplateViewModel
    {
        public NoteTemplateViewModel()
        {
            this.Categories = new List<string>();
        }

        public NoteTemplateViewModel(NoteTemplate template)
        {
            this.Id = template.Id;
            this.Name = template.Name;
            this.Description = template.Description;
            this.Template = template.Template;
            this.EmployerId = template.EmployerId;
            this.Categories = template.Categories;
        }

        public NoteTemplate ApplyToDataModel(NoteTemplate template)
        {
            template.Name = this.Name;
            template.Description = this.Description;
            template.Template = this.Template;
            template.EmployerId = this.EmployerId;
            template.Categories = this.Categories;
            return template;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Description { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Template { get; set; }

        [DisplayName("Employer"), UIHint("Employers")]
        public string EmployerId { get; set; }

        [UIHint("NoteCategory"), Required]
        public List<string> Categories { get; set; }
    }
}