﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class ContactViewModel
    {
        public ContactViewModel()
        {

        }

        public ContactViewModel(EmployerContact contact)
            : this()
        {
            if (contact == null || contact.Contact == null)
                return;

            this.Id = contact.Id;
            this.EmployerId = contact.EmployerId;
            this.FirstName = contact.Contact.FirstName;
            this.LastName = contact.Contact.LastName;
            this.Phone = contact.Contact.WorkPhone;
            this.Email = contact.Contact.Email;
            this.Fax = contact.Contact.Fax;
            this.ContactTypeCode = contact.ContactTypeCode;
            this.EndDate = contact.Contact.EndDate;
        }

        public string Id { get; set; }

        public string EmployerId { get; set; }

        public string EmployerReferenceCode { get; set; }

        [Required, DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required, DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Fax { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required, UIHint("EmployerContactTypeCode"), DisplayName("Contact Type")]
        public string ContactTypeCode { get; set; }

        [DisplayName("End Date"),DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? EndDate { get; set; }

        public EmployerContact ApplyToDataModel(EmployerContact contact = null)
        {
            if (contact == null)
                contact = new EmployerContact();

            contact.ContactTypeCode = this.ContactTypeCode;
            contact.ContactTypeName = null;
            contact.Contact = new Data.Contact()
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                WorkPhone = this.Phone,
                Fax = this.Fax,
                Email = this.Email,
                EndDate = this.EndDate
            };

            if (!string.IsNullOrEmpty(this.EmployerReferenceCode))
            {
                using (var employerService = new EmployerService(Current.User()))
                {
                    contact.EmployerId = employerService.GetEmployerIdByReferenceCode(this.EmployerReferenceCode);
                }
            }

            return contact;
        }

        public bool IsValidForBulkUpload
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.EmployerReferenceCode)
                    && !string.IsNullOrWhiteSpace(this.Email)
                    && !string.IsNullOrWhiteSpace(this.FirstName)
                    && !string.IsNullOrWhiteSpace(this.LastName)
                    && !string.IsNullOrWhiteSpace(this.ContactTypeCode);
            }
        }
    }
}