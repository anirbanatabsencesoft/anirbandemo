﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Data.Rules;

namespace AbsenceSoft.Models.Policies
{
    public class PolicyRules
    {
        public string EmployerId { get; set; }

        public List<Rule> Rules { get; set; }       
    }
}