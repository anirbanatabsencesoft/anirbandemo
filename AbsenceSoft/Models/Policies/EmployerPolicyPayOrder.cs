﻿using AbsenceSoft.Data.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Policies
{
    public class EmployerPolicyPayOrder
    {
        public string EmployerId { get; set; }
        public List<PolicyPayOrder> PolicyPayOrder { get; set; }
    }
}