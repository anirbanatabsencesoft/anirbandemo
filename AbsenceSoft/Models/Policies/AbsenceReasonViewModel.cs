﻿using AbsenceSoft.Data.Policies;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web;
using System.ComponentModel;

namespace AbsenceSoft.Models.Policies
{
    public class AbsenceReasonViewModel : IViewModel, IDataTransformModel<AbsenceReason>
    {
        public AbsenceReasonViewModel()
        {

        }

        public AbsenceReasonViewModel(AbsenceReason reason)
        {
            if (reason == null)
                return;

            Id = reason.Id;
            CustomerId = reason.CustomerId;
            EmployerId = reason.EmployerId;
            Name = reason.Name;
            Code = reason.Code;
            Category = reason.Category;
            Country = reason.Country;
            IsDisabled = IsDisabledInContext(reason.Code);
            RequiresRelationship = HasAbsenceReasonFlag(reason.Flags, AbsenceReasonFlag.RequiresRelationship);
            HideInESS = HasAbsenceReasonFlag(reason.Flags, AbsenceReasonFlag.HideInEmployeeSelfServiceSelection);
            EditAccommodationType = HasAbsenceReasonFlag(reason.Flags, AbsenceReasonFlag.ShowAccommodation);
            AdministrativeCases = HasCaseTypeFlag(reason.CaseTypes, CaseType.Administrative);
            ConsecutiveCases = HasCaseTypeFlag(reason.CaseTypes, CaseType.Consecutive);
            IntermittentCases = HasCaseTypeFlag(reason.CaseTypes, CaseType.Intermittent);
            ReducedCases = HasCaseTypeFlag(reason.CaseTypes, CaseType.Reduced);
        }

        private bool IsDisabledInContext(string code)
        {
            var currentEmployer = Current.Employer();
            var currentCustomer = Current.Customer();

            bool isReasonDisabled = false;
            if (currentEmployer != null)
            {
                isReasonDisabled =currentEmployer.SuppressReasons.Contains(code);
            }

            if (currentCustomer != null && !isReasonDisabled)
            {
                isReasonDisabled = currentCustomer.SuppressReasons.Contains(code);
            }

            return isReasonDisabled;
        }

        private bool HasAbsenceReasonFlag(AbsenceReasonFlag? flags, AbsenceReasonFlag requiredFlag)
        {
            if (!flags.HasValue)
                return false;

            return (flags & requiredFlag) == requiredFlag;
        }

        private bool HasCaseTypeFlag(CaseType? flags, CaseType requiredFlag)
        {
            if (!flags.HasValue)
                return false;

            return (flags & requiredFlag) == requiredFlag;
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string DisplayType { get { return "absence reason"; } }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        public string Category { get; set; }

        [DisplayName("New Category")]
        public string NewCategory { get; set; }

        [DisplayName("Administrative")]
        public bool AdministrativeCases { get; set; }

        [DisplayName("Consecutive")]
        public bool ConsecutiveCases { get; set; }

        [DisplayName("Intermittent")]
        public bool IntermittentCases { get; set; }

        [DisplayName("Reduced")]
        public bool ReducedCases { get; set; }

        [DisplayName("Requires Relationship")]
        public bool RequiresRelationship { get; set; }

        [DisplayName("Edit Accommodation Type")]
        public bool EditAccommodationType { get; set; }

        [DisplayName("Hide in ESS")]
        public bool HideInESS { get; set; }

        public bool IsDisabled { get; set; }

        public string Country { get; set; }

        public AbsenceReason ApplyToDataModel(AbsenceReason absenceReason)
        {
            if (absenceReason == null)
                absenceReason = new AbsenceReason();

            absenceReason.Name = Name;
            absenceReason.Code = Code;
            absenceReason.Country = Country;

            absenceReason.CaseTypes = SetCaseTypes();
            absenceReason.Flags = SetFlags();
            absenceReason.Category = SetCategory();
            
            return absenceReason;
        }

        private string SetCategory()
        {
            if (!string.IsNullOrEmpty(NewCategory))
            {
                return NewCategory;
            }
            
            return Category;
        }

        private AbsenceReasonFlag SetFlags()
        {
            AbsenceReasonFlag flag = AbsenceReasonFlag.None;

            if (RequiresRelationship)
                flag = flag | AbsenceReasonFlag.RequiresRelationship;

            if (EditAccommodationType)
                flag = flag | AbsenceReasonFlag.ShowAccommodation;

            if (HideInESS)
                flag = flag | AbsenceReasonFlag.HideInEmployeeSelfServiceSelection;

            return flag;
        }

        private CaseType SetCaseTypes()
        {
            CaseType caseTypes = CaseType.None;

            if (AdministrativeCases)
                caseTypes = caseTypes | CaseType.Administrative;

            if (ConsecutiveCases)
                caseTypes = caseTypes | CaseType.Consecutive;

            if (IntermittentCases)
                caseTypes = caseTypes | CaseType.Intermittent;

            if (ReducedCases)
                caseTypes = caseTypes | CaseType.Reduced;

            return caseTypes;

        }

        public bool IsAbsenceReasonSuppressedAtCustomerLevel()
        {
            if (Current.Employer() != null)
            {
                var currentCustomer = Current.Customer();
                return (currentCustomer.SuppressReasons.Contains(this.Code, StringComparer.InvariantCultureIgnoreCase));
            }
            return false;
        }
    }
}