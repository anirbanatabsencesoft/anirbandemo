﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Policies
{
    [Serializable]
    public class RemoveAbsReasonModel
    {
        public string EmployerId { get; set; }
        public string PolicyId { get; set; }
        public string AbsReasonId { get; set; }
        public string AbsReasonCode { get; set; }
    }
}