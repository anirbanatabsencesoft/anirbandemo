﻿using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Policies
{
    public class PolicyRuleExpressions
    {
        public PolicyRuleExpressions()
        {
            this.Expressions = new List<RuleExpression>();
        }

        public string EmployerId { get; set; }
        public List<RuleExpression> Expressions { get; set; }
    }
}