﻿using AbsenceSoft.Common.Serializers;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    [Serializable]
    public class ScheduleModel
    {
        public ScheduleModel()
        {
            Times = new List<TimeModel>();
        }
        public Guid Id { get; set; }
        public ScheduleType ScheduleType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<TimeModel> Times { get; set; }
        public string TextRepresentation { get; set; }
        public string EmployeeNumber { get; set; }
        public string FteMinutesPerWeek { get; set; }
        public FteWeeklyDuration FteWeeklyDuration { get; set; }        
    }

    [Serializable]
    public class TimeModel
    {
        public Guid Id { get; set; }
        public string TotalMinutes { get; set; }
        public DateTime SampleDate { get; set; }
    }

    public static class ScheduleModelExtensions
    {
        public static Schedule ToSchedule(this ScheduleModel model)
        {
            return new Schedule()
            {
                ScheduleType = model.ScheduleType,
                EndDate = model.EndDate,
                StartDate = model.StartDate,
                Id = model.Id,
                Times = model.Times.Select(t => t.ToTime()).ToList()
            };
        }

        public static Time ToTime(this TimeModel model)
        {
            return new Time()
            {
                SampleDate = model.SampleDate,
                TotalMinutes = model.TotalMinutes.ParseFriendlyTime(),
                Id = model.Id
            };
        }
        
        public static ScheduleModel ToModel(this Schedule schedule)
        {
            return new ScheduleModel()
            {
                ScheduleType = schedule.ScheduleType,
                EndDate = schedule.EndDate,
                StartDate = schedule.StartDate,
                Id = schedule.Id,
                Times = schedule.Times.Select(t => t.ToModel()).ToList(),
                TextRepresentation = schedule.ToString()
            };
        }

        public static TimeModel ToModel(this Time time)
        {
            return new TimeModel()
            {
                SampleDate = time.SampleDate,
                TotalMinutes = time.TotalMinutes.HasValue ? time.TotalMinutes.Value.ToFriendlyTime() : null,
                Id = time.Id
            };
        }
    }
}