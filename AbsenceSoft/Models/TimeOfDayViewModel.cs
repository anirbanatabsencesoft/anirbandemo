﻿using AbsenceSoft.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class TimeOfDayViewModel
    {
        public TimeOfDayViewModel() { }

        public TimeOfDayViewModel(TimeOfDay? time)
        {
            if (time == null)
                return;

            PM = time.Value.PM;
            Hour = time.Value.Hour;
            Minutes = time.Value.Minutes;
            if (PM)
                Hour -= 12;
        }
        public TimeOfDay? ApplyModel(TimeOfDay? time)
        {
            if (!Hour.HasValue && !Minutes.HasValue)
                return time;

            int h = Hour ?? 1;
            int m = Minutes ?? 0;
            if (PM)
                h += 12;

            return new TimeOfDay(h, m);
        }

        [Range(1, 12)]
        public int? Hour { get; set; }
        [Range(0, 59)]
        public int? Minutes { get; set; }
        public bool PM { get; set; }
    }
}