﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class HyperlinkViewModel : IValidatableObject
    {
        public HyperlinkViewModel()
        {

        }

        public HyperlinkViewModel(Hyperlink link)
        {
            this.Text = link.Text;
            this.Url = link.Url;
            this.Title = link.Title;
        }

        [Required]
        public string Text { get; set; }

        [Required]
        public string Url { get; set; }

        public string Title { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (!IsValidUrl)
            {
                results.Add(new ValidationResult("Url is not valid", new List<string>() { "Url" }));
            }

            return results;
        }

        private bool IsValidUrl
        {
            get
            {
                Uri uriResult;
                bool result = Uri.TryCreate(this.Url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                return result;
            }

        }
    }
}