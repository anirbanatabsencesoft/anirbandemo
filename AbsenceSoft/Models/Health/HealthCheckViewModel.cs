﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Health
{
    public class HealthCheckViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HealthCheckViewModel"/> class.
        /// </summary>
        public HealthCheckViewModel()
        {
            ShowExceptionDetail = false;
            DatabaseOK = true;
            DatabaseMessage = new List<string>();
            DataWarehouseOK = true;
            DataWarehouseMessage = new List<string>();
            S3OK = true;
            S3Message = new List<string>();
            ServerOK = true;
            ServerMessage = new List<string>();
            SmtpOK = true;
            SmtpMessage = new List<string>();
            SqsOK = true;
            SqsMessage = new List<string>();
            DnsMessage = new List<string>();
            DnsOK = true;
        }

        public bool ShowExceptionDetail { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is ok.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is ok; otherwise, <c>false</c>.
        /// </value>
        public bool IsOK
        {
            get
            {
                return DatabaseOK
                    && DataWarehouseOK
                    && S3OK
                    && ServerOK
                    && SmtpOK
                    && SqsOK
                    && DnsOK;
            }
        }

        public bool ServerOK { get; set; }
        public List<string> ServerMessage { get; set; }

        public bool DnsOK { get; set; }
        public List<string> DnsMessage { get; set; }

        public bool DatabaseOK { get; set; }
        public List<string> DatabaseMessage { get; set; }

        public bool DataWarehouseOK { get; set; }
        public List<string> DataWarehouseMessage { get; set; }

        public bool S3OK { get; set; }
        public List<string> S3Message { get; set; }

        public bool SqsOK { get; set; }
        public List<string> SqsMessage { get; set; }

        public bool SmtpOK { get; set; }
        public List<string> SmtpMessage { get; set; }
    }
}