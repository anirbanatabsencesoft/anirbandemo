﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Web;
using System.Linq;
using AbsenceSoft.Data;
using System.Collections.Generic;

namespace AbsenceSoft.Models
{
    public static class DisableableViewModelExtensions
    {
        public static bool IsDisableable(this IDisableableViewModel model)
        {
            Customer customer = Current.Customer();
            Employer employer = Current.Employer();
            var entities = customer.SuppressedEntities;
            bool isAtEmployerLevel = employer != null;
            bool isSuppressedAtCustomerLevel = IsSuppressedAtCustomerLevel(entities, model.Code, model.ClassTypeName);
            if (isAtEmployerLevel && isSuppressedAtCustomerLevel)
                return true;
            else
                return false;
        }

        private static bool IsSuppressedAtCustomerLevel(List<SuppressedEntity> entities, string Code, string ClassTypeName)
        {
            if (entities == null || entities.Count == 0)
                return false;

            return entities.FirstOrDefault(se => se.EntityCode == Code && se.EntityName == ClassTypeName) != null;
        }
    }
}