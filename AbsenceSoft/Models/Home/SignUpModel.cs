﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Home
{
    public class SignUpModel
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public CustomerData Customer { get; set; }
        public EmployerData Employer { get; set; }

        public class CustomerData
        {
            public String CompanyName { get; set; }
            public String TaxId { get; set; }
            public String Subdomain { get; set; }
            public ContactData Contact { get; set; }
            public ContactData BillingContact { get; set; }
            public bool UseContactAsBillingContact { get; set; }
        }
        public class EmployerData
        {
            public String CompanyName { get; set; }
            public String TaxId { get; set; }
            public ContactData Contact { get; set; }
            public FmlPeriodType FMLPeriodType { get; set; }
            public bool Enable50In75MileRule { get; set; }
            public int FMLPeriodResetMonth { get; set; }
            public int FMLPeriodResetDayOfMonth { get; set; }
            public bool AllowIntermittentForBirthAdoptionOrFosterCare { get; set; }
            public bool IgnoreScheduleForPriorHoursWorked { get; set; }

            public bool IgnoreAverageMinutesWorkedPerWeek { get; set; }
            
            public class FmlPeriodType
            {
                public int Id { get; set; }
                public String Text { get; set; }
            }
        }
        public class ContactData
        {
            public String Title { get; set; }
            public String FirstName { get; set; }
            public String MiddleName { get; set; }
            public String LastName { get; set; }
            public String Email { get; set; }
            public String WorkPhone { get; set; }
            public String Fax { get; set; }
            public String Address1 { get; set; }
            public String Address2 { get; set; }
            public String City { get; set; }
            public String State { get; set; }
            public String PostalCode { get; set; }
        }
    }
}