﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Dashboard
{
    public class ToDo
    {
        public string Employee { get; set; }    

        public string ToDoText { get; set; }

        public String Due { get; set; }

        public string Status { get; set; }
    }

    public enum ToDoStatus
    {
        All_Open,
        Pending,
        Overdue,
        Completed,
        Canceled
    }

    public class ToDoManager
    {
        public IEnumerable<ToDo> BuildTempData()
        {
            List<ToDo> tempToDoList = new List<ToDo> { 
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() },
                new ToDo { Employee="Gifford, Becky", ToDoText="Review medical certificate", Due = "12/12/2014", Status= ToDoStatus.Overdue.ToString() },
                new ToDo { Employee="Marcia Cifuentes", ToDoText="Send communication", Due = "5/30/14", Status= ToDoStatus.Pending.ToString() },
                new ToDo { Employee="Bethany Lavallan", ToDoText="Review medical certificate", Due = "6/12/14", Status= ToDoStatus.Completed.ToString() }
            };

            return tempToDoList.AsQueryable();
        }
    }
}