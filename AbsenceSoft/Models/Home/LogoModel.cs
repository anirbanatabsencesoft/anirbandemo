﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Home
{
    public class LogoModel
    {
        public string LogoUrl { get; set; }
        public string LogoName { get; set; }
        public bool IncludeHomeLink { get; set; }
    }
}