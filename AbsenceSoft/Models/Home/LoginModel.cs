﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Home
{
    public class LoginModel
    {
        public LoginModel()
        {

        }

        public LoginModel(SsoProfile profile)
            : this()
        {
            if (profile == null)
                return;

            ShowSsoButton = !string.IsNullOrEmpty(profile.SingleSignOnServiceUrl);
            ShowLogo = profile.LoginSourceShowLogo;
            if (!string.IsNullOrEmpty(profile.PortalLoginSourceName))
            {
                PortalName = profile.PortalLoginSourceName;
            }
            else
            {
                PortalName = "SSO";
            }
        }

        [Required, EmailAddress, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        public bool ShowSsoButton { get; set; }

        public bool ShowLogo { get; set; }

        public string PortalName { get; set; }
    }
}