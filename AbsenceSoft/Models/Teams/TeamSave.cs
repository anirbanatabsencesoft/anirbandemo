﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Teams
{
    public class TeamSave : IValidatableObject
    {
        public Team Team { get; set; }
        public List<TeamMember> TeamMembers { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            ValidateTeamName(results);
            return results;
        }
        private void ValidateTeamName(List<ValidationResult> results)
        {
            using (var teamService = new TeamService(Current.User()))
            {
                if (!teamService.NameIsInUse(Team.Name, Team.Id))
                    return;

                results.Add(new ValidationResult("Duplicate Team name , please choose another name", new List<string>() { "Name" }));
            }
        }
    }
}