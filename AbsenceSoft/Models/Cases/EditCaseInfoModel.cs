﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class EditCaseInfoModel
    {
        public string Description { get; set; }
        public string ShortSummary { get; set;}
  
    }
}