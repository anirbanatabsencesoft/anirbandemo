﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class PolicySummaryModel
    {
        public string PolicyCode { get; set; }
        public string PolicyName { get; set; }
        public string TimeUsedText { get; set; }
        public string TimeRemainingText { get; set; }
        public string HoursUsedText { get; set; }
        public string HoursRemainingText { get; set; }
        public bool ExcludeFromTimeConversion { get; set; }
    }
}