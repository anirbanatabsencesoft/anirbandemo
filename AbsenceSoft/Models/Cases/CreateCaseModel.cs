﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Models.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class CreateCaseModel : CasePoliciesDataModel
    {
        public CreateCaseModel():base()
        {
            Todos = new List<ToDoItem>();
            Job = new EmployeeJobViewModel();
        }

        public String Status { get; set; }

        //We need this flag because when converting a Inquiry to Case,  need to change Status to Open,else system will not display those
        //absence reason.but at the same time  need a flag to mark it as Inquiry , so if user click on cancel before Create Case. can keep
        //it in Inquiry state
        public bool IsInquiry { get; set; }
        // All of these will be used in the post       
        // **** Added for employee case review on ess case create ui
        public string CaseId { get; set; }       
        public bool IsESSCaseReview { get; set; }
        // **** Added for employee case review on ess case create ui
        public string EmployeeId { get; set; }
		public string EmployerId { get; set; }
        public string ShortDescription { get; set; }
        public string Summary { get; set; }
        public int? CaseTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String AbsenceReasonId { get; set; }
		public String EmployeeRelationshipCode { get; set; }
		public String EmployeeRelationshipName { get; set; }
		public String EmployeeRelationshipFirstName { get; set; }
		public String EmployeeRelationshipLastName { get; set; }
        public DateTime? EmployeeRelationshipDateOfBirth { get; set; }
        public DateTime? AdoptionDate { get; set; }

        public string EmployeeContactId { get; set; }
        public int? MilitaryStatusId { get; set; }
        public bool? IsWorkRelated { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public DateTime? ActualDeliveryDate { get; set; }
        public bool? WillUseBonding { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public string SpouseCaseId { get; set; }
        public string SpouseCaseNumber { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public virtual ScheduleViewModel WorkSchedule { get; set; }
        public string FTWeeklyWorkTime { get; set; }
        public bool? IsAccommodation { get; set; }
        public bool? IsSTD { get; set; }
        public virtual DisabilityModel Disability { get; set; }
        public virtual AccommodationRequestViewModel AccommodationRequest { get; set; }

		public bool NoFeaturesEnabled { get { return !(ADAFeatureEnabled && STDFeatureEnabled);} }
        public bool ADAFeatureEnabled { get; set; }
        public bool STDFeatureEnabled { get; set; }
        public bool GuidelinesDataEnabled { get; set; }
        public bool STDPayFeatureEnabled { get; set; }
        public bool LOAFeatureEnabled { get; set; }
        public bool AccommodationTypeCategoriesFeatureEnabled { get; set; }

        public bool ShowIsWorkRelated { get; set; }
        public bool ShowContactSection { get; set; }
        public bool ShowOtherQuestions { get; set; }

        //*********** Parrot fields ************//
        public string EmployeePrimaryPhone { get; set; }
        public string EmployeeSecondaryPhone { get; set; }
        public string EmployeePrimaryEmail { get; set; }
        public string EmployeeSecondaryEmail { get; set; }
        public string EmployeePersonalEmail { get; set; }
        public string EmployeeOfficeLocation { get; set; }
        public string ContactPreference { get; set; }
        public string TimePreference { get; set; }
        public bool? SendECommunication { get; set; }

        public bool? IsContactInformationCorrect { get; set; }
        public Address NewAddress { get; set; }
        public bool? IsAltAddressCorrect { get; set; }
        public Address NewAltAddress { get; set; }
        public bool? IsEmpSafeToBeAtWork { get; set; }
        public bool? IsEmployeeAtWork { get; set; }
        public string WorkplaceModificationDesc { get; set; }
        public bool? IsEmployeeOnLeave { get; set; }
        public string EmpOtherLeaveCaseNumber { get; set; }
        public string EmployeeAskForDesc { get; set; }
        public bool? DidEmployeeSelfIdentify { get; set; }
        public DateTime? DateIdentified { get; set; }
        public bool? HasPerformanceIssue { get; set; }
        public string PerformanceIssueDesc { get; set; }
        public bool? HasWorkConcerns { get; set; }
        public string WorkConcernsDesc { get; set; }
        public string CurrentSchedule { get; set; }

        public bool EmployeeHasFTESchedule { get; set; }

        public CreateContactViewModel CaseReporter { get; set; }
        public AuthorizedSubmitterModel AuthorizedSubmitter { get; set; }
        public List<CustomField> CustomFields { get; set; }

        public List<ToDoItem> Todos { get; set; }
        public bool HasAdministrativeReasons { get; set; }

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        [UIHint("EmployeeJob")]
        public EmployeeJobViewModel Job { get; set; }

        public Relapse Relapse { get; set; }

    }    
}