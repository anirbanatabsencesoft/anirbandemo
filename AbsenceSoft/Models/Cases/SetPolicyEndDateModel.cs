﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class SetPolicyEndDateModel
    {
        public string CaseId { get; set; }
        public string PolicyCode { get; set; }
        public DateTime? EndDate { get; set; }
    }
}