﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class SaveCasePolicyEditsModel
    {
        public String CaseId { get; set; }
        public String PolicyId { get; set; }
        public Guid RuleGroupId { get; set; }
        public Guid RuleId { get; set; }
        public int? OverrideValue { get; set; }
        public String OverrideNotes { get; set; }
    }
}