﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class PayUpdate
    {
        public string PolicyId { get; set; }
        public decimal Amount { get; set; }

        public string CaseId { get; set; }
    }
}