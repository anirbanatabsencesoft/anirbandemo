﻿using AbsenceSoft.Models.Contacts;

namespace AbsenceSoft.Models.Cases
{
    public class EditCaseReporterInfoModel
    {
        public CreateContactViewModel CaseReporter { get; set; }
        public AuthorizedSubmitterModel AuthorizedSubmitter { get; set; }
    }
}