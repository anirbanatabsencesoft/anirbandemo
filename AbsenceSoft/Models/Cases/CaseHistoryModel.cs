﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseHistoryModel
    {
        public CaseHistoryModel()
        {
            this.Cases = new List<CaseHistoryItemModel>();
        }

        public String EmployeeId { get; set; }
        public List<CaseHistoryItemModel> Cases { get; set; }
    }
}