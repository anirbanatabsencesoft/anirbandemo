﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CasePoliciesDataModel
    {
        public CasePoliciesDataModel()
        {
            this.AppliedPolicies = new List<AppliedPolicyModel>();
            this.ManuallyAppliedPolicies = new List<AppliedPolicyModel>();
            this.AvailableManualPolicies = new List<AppliedPolicyModel>();
        }

        public List<AppliedPolicyModel> AppliedPolicies { get; set; }
        public List<AppliedPolicyModel> ManuallyAppliedPolicies { get; set; }
        public List<AppliedPolicyModel> AvailableManualPolicies { get; set; }
    }
}