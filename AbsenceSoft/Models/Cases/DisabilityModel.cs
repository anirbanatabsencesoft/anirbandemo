﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class DisabilityModel
    {
        public DisabilityModel()
        {
            SecondaryDiagnosis = new List<DiagnosisCodeModel>();
        }

        public string CaseId { get; set; }
        public bool? IsSTD { get; set; }
        public DiagnosisCodeModel PrimaryDiagnosis { get; set; }
        public List<DiagnosisCodeModel> SecondaryDiagnosis { get; set; }
        public string PrimaryDiagnosisId { get; set; }
        public List<string> SecondaryDiagnosisIds { get; set; }
        public bool? MedicalComplications { get; set; }
        public bool? Hospitalization { get; set; }
        public string GeneralHealthCondition { get; set; }
        public DateTime? ConditionStartDate { get; set; }
        public string HCPContactTypeCode { get; set; }
        public string HCPContactId { get; set; }
        public string HCPCompanyName { get; set; }
        public string HCPFirstName { get; set; }
        public string HCPLastName { get; set; }
        public string HCPPhone { get; set; }
        public string HCPFax { get; set; }
        public bool? IsPrimary { get; set; }
        public string HCPContactPersonName { get; set; }
        public string PrimaryPathId { get; set; }
        public string PrimaryPathText { get; set; }
        public string PrimaryPathMinDays { get; set; }
        public string PrimaryPathMaxDays { get; set; }
        public string PrimaryPathDaysUIText { get; set; }  
    }

    public class DiagnosisCodeModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public static class DisabilityModelExtensions
    {
        public static DisabilityModel ToModel(this DisabilityInfo disability)
        {
            return new DisabilityModel()
            {
                PrimaryDiagnosis = new DiagnosisCodeModel(){ Id = disability.PrimaryDiagnosis.Id.ToString(), Description = disability.PrimaryDiagnosis.UIDescription },
                SecondaryDiagnosis = new List<DiagnosisCodeModel>(),
                MedicalComplications = disability.MedicalComplications,
                Hospitalization = disability.Hospitalization,
                GeneralHealthCondition = disability.GeneralHealthCondition,
                ConditionStartDate = disability.ConditionStartDate
            };
        }
    }

}