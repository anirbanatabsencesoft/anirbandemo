﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CasePolicySummaryModel
    {
        public CasePolicySummaryModel() { Messages = new List<string>(); }
        public string Policy { get; set; }
        public string PolicyCode { get; set; }
        public List<CasePolicySummaryDetailModel> Detail { get; set; }
        public List<string> Messages { get; set; }
    }

    public class CasePolicySummaryDetailModel
    {
        public DateTime StartDate { get; set; }
        public string StartDateText { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndDateText { get; set; }
        public string CaseType { get; set; }
        public string Status { get; set; }
        public string DenialReason { get; set; }
        public string DenialExplanation { get; set; }
    }
}