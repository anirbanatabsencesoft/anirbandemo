﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class CaseChangeModel
    {
        public string CaseId { get; set; }
        public CaseType CaseType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsStartDateNew { get; set; }
        public bool IsEndDateNew { get; set; }
        public bool ModifyAccommodations { get; set; }
        public bool ModifyDecisions { get; set; }
        public CaseStatus Status { get; set; }
        public ScheduleViewModel WorkSchedule { get; set; }
    }
}