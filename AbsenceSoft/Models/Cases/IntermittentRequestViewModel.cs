﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Common.Serializers;
using Newtonsoft.Json;
using System.ComponentModel;

namespace AbsenceSoft.Models.Cases
{
    public class IntermittentRequestViewModel
    {
        public IntermittentRequestViewModel()
        {
            Details = new List<IntermittentRequestDetailViewModel>();
        }

        public Guid Id { get; set; } 
        public string CaseId { get; set; }
        public DateTime RequestDate { get; set; }
        public IntermittentType? IntermittentType { get; set; }
        public string Notes { get; set; }
        public List<IntermittentRequestDetailViewModel> Details { get; set; }
        public bool IsTimeEqual { get; set; }      

        //flag to indicate that warnings exist but the user would like to procede anyway
        public bool WarningsApproved { get; set; }

        // Fallback stuff
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int PendingTime { get; set; }
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int ApprovedTime { get; set; }
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int DeniedTime { get; set; }
        public string DenialReasonCode { get; set; }
        public string DenialReasonOther { get; set; }

        /// <summary>
        /// Gets or sets the start time of event.
        /// </summary> 
        public TimeOfDayViewModel StartTimeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the end time of event.
        /// </summary>
        public TimeOfDayViewModel EndTimeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the edit mode of operation
        /// </summary>
        public bool? IsEditMode { get; set; }

    }

    public class IntermittentRequestDetailViewModel
    {
        public string PolicyCode { get; set; }
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int PendingTime { get; set; }
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int ApprovedTime { get; set; }
        [JsonConverter(typeof(FriendlyTimeSerializer))]
        public int DeniedTime { get; set; }
        public string DenialReasonCode { get; set; }
        public string DenialReasonOther { get; set; }
        public string PolicyName { get; set; }
        public string Available { get; set; }

        /// <summary>
        /// Gets or sets the start time of event.
        /// </summary> 
        public TimeOfDayViewModel StartTimeOfEvent { get; set; }

        /// <summary>
        /// Gets or sets the end time of event.
        /// </summary>
        public TimeOfDayViewModel EndTimeOfEvent { get; set; }
    }

}