﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.ToDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseEssCreateModel : BaseCaseEss
    {

        public CaseEssCreateModel()
        {
            Todos = new List<ToDoItem>();
        }
        // All of these will be used in the post       

        public bool ADAFeatureEnabled { get; set; }
        public bool STDFeatureEnabled { get; set; }
        public bool LOAFeatureEnabled { get; set; }

        public bool ShowIsWorkRelated { get; set; }
        public bool ShowContactSection { get; set; }
        public bool ShowOtherQuestions { get; set; }

        public bool? IsEmployeeAtWork { get; set; }
        public string WorkplaceModificationDesc { get; set; }
        public string EmployeeAskForDesc { get; set; }
        public bool? HasPerformanceIssue { get; set; }
        public string PerformanceIssueDesc { get; set; }
        public bool? HasWorkConcerns { get; set; }
        public string WorkConcernsDesc { get; set; }
        public string CurrentSchedule { get; set; }
        public string EmpOtherLeaveCaseNumber { get; set; }
        public List<ToDoItem> Todos { get; set; }
        public bool? SendECommunication { get; set; }
    }
}