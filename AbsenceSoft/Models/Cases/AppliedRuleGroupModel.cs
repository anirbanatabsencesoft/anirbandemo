﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class AppliedRuleGroupModel
    {
        public AppliedRuleGroupModel()
        {
            this.Rules = new List<AppliedRuleModel>();
        }

        public Guid Id { get; set; }
        public List<AppliedRuleModel> Rules { get; set; }
        public string Description { get; set; }
    }
}