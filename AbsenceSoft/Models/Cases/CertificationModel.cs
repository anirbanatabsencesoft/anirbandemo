﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CertificationModel
    {
        public CertificationModel()
        {
            this.Certifications = new List<CertificationDetailModel>();
        }
        public string CaseId { get; set; }
        public List<CertificationDetailModel> Certifications { get; set; }
        public bool HasAnyResults { get; set; }

        public bool IsCertificateIncomplete { get; set; }
    }

    public class CertificationDetailModel
    {
        public Guid? Id { get; set; }
        public DateTime? StartDate { get; set; } 
        public DateTime? EndDate { get; set; }
        public int? Occurances { get; set; }
        public int? Frequency { get; set; }
        public Unit? FrequencyType { get; set; }
        public DayUnitType? FrequencyUnitType { get; set; }
        public double? Duration { get; set; }
        public Unit? DurationType { get; set; }
        public string Notes { get; set; }
        public IntermittentType? IntermittentType { get; set; }
        public string Intermittent { get { return !IntermittentType.HasValue ? "" : IntermittentType.ToString().SplitCamelCaseString();}}

    }
    
}