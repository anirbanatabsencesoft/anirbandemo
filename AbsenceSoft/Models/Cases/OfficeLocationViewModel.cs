﻿using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;


namespace AbsenceSoft.Models.Cases
{
    public class OfficeLocationViewModel
    {
        public OfficeLocationViewModel()
        {
        }

        public OfficeLocationViewModel(AbsenceSoft.Data.Customers.Organization organization)
        {
            this.Id = organization.Id;
            this.Code = organization.Code;
            this.Name = organization.Name;
            this.EmployerId = organization.EmployerId;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string EmployerId { get; set; }
    }
}