﻿using System.ComponentModel.DataAnnotations;

namespace AbsenceSoft.Models.Cases
{
    public class CaseAssignmentRuleSetOrderViewModel
    {
        public CaseAssignmentRuleSetOrderViewModel()
        {
        }


        [ScaffoldColumn(false)]
        public string Code { get; set; }


        [ScaffoldColumn(false)]
        public int CaseAssignmentRuleOrder { get; set; }

    }
}