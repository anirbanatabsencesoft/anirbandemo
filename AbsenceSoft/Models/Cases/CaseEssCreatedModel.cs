﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
	public class CaseEssCreatedModel
	{
		public string CaseId{ get; set; }
		public string CaseNumber { get; set; }
		public string EmployeeId { get; set; }
		public string EmployerName { get; set; }
		public Address EmployerAddress { get; set; }
		public string HRContactName { get; set; }
		public string HRContactPhone { get; set; }
	}
}