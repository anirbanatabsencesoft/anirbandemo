﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class PolicyViewModel
    {
        public PolicyViewModel() { }

        public PeriodType EmployerPeriodType { get; set; }
        public Policy Policy { get; set; }

    }
}