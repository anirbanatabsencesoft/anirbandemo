﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class WaiveWaitingPeriodModel
    {
        public string CaseId { get; set; }
        public bool Waive { get; set; }
    }
}