﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class EditAdditionalInfoModel
    {
        public string ContactPreference { get; set; }
        public string TimePreference { get; set;}
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public bool? SendECommunication { get; set; }

        public void ApplyAddressInfoToCase(Case theCase)
        {
            if (HasAddressInformation)
            {
                theCase.Employee.Info.AltAddress = new Data.Address()
                {
                    Address1 = Address1,
                    Address2 = Address2,
                    City = City,
                    State = State,
                    Country = Country,
                    PostalCode = PostalCode
                };
            }
            else
            {
                theCase.Employee.Info.AltAddress = null;
            }
        }

        private bool HasAddressInformation
        {
            get
            {
                return !string.IsNullOrEmpty(Address1)
                    || !string.IsNullOrEmpty(Address2)
                    || !string.IsNullOrEmpty(City)
                    || !string.IsNullOrEmpty(State)
                    || !string.IsNullOrEmpty(Country)
                    || !string.IsNullOrEmpty(PostalCode);
                    

            }
        }
    }
}