﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class AbsenceReasonModel
    {
        public AbsenceReasonModel()
        {
            this.Children = new List<AbsenceReasonModel>();
        }
        public AbsenceReasonModel(AbsenceSoft.Data.Policies.AbsenceReason absenceReasons)
        {
            this.Id = absenceReasons.Id;
            this.Code = absenceReasons.Code;
            this.Name = absenceReasons.Name;
            this.Category = absenceReasons.Category;
        }
        public List<AbsenceReasonModel> Children { get; set; }
        public String Id { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public String Category { get; set; }
        public bool IsCategory { get; set; }
        public bool IsChild { get; set; }
        public String HelpText { get; set; }
        public bool IsForFamilyMember { get; set; }
        public bool IsHiddenInESSSelect { get; set; }
        public bool IsToShowAccomodationAlways { get; set; }
    }
}