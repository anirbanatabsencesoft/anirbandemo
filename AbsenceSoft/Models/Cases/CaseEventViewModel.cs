﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseEventViewModel
    {
        public CaseEventViewModel()
        {

        }

        public CaseEventViewModel(CaseEvent theEvent)
            : this()
        {
            if (theEvent == null)
                return;

            Name = theEvent.EventType.ToString().SplitCamelCaseString();
            EventType = theEvent.EventType;
            EventDate = theEvent.EventDate;
        }


        public string Name { get; set; }

        public CaseEventType EventType { get; set; }

        public DateTime EventDate { get; set; }

        public bool Delete { get; set; }

        public bool IsNew { get; set; }
    }
}