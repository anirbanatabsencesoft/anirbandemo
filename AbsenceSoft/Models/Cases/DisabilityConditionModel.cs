﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Models.Cases
{
    public class DisabilityConditionModel
    {
        public string CaseId { get; set; }
        public string GeneralHealthCondition { get; set; }
        public DateTime? ConditionStartDate { get; set; }
        public bool? MedicalComplications { get; set; }
        public bool? Hospitalization { get; set; }
        public JobClassification? ActivityLevel { get; set; }
        public string HCPContactId { get; set; }
        public string HCPContactType { get; set; }
        public string HCPCompanyName { get; set; }
        public string HCPFirstName { get; set; }
        public string HCPLastName { get; set; }
        public string HCPPhone { get; set; }
        public string HCPFax { get; set; }
        public bool IsPrimary { get; set; }
        public string HCPContactPersonName { get; set; }
    }
}