﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AbsenceSoft.Models.Cases
{
    public class CaseAssignmentRuleSetViewModel
    {
        public CaseAssignmentRuleSetViewModel()
        {
        }
        public CaseAssignmentRuleSetViewModel(string customerId) : this()
        {
            this.CustomerId = customerId;
            this.CaseAssignmentRules = new List<CaseAssignmentRuleViewModel>();
            GetCaseAssigneeTypes();
        }

        public CaseAssignmentRuleSetViewModel(CaseAssignmentRuleSet caseAssignmentRuleSet)
            : this(caseAssignmentRuleSet.CustomerId)
        {
            this.Id = caseAssignmentRuleSet.Id;
            this.Name = caseAssignmentRuleSet.Name;
            this.CustomerId = caseAssignmentRuleSet.CustomerId;
            this.Code = caseAssignmentRuleSet.Code;
            Behavior = caseAssignmentRuleSet.Behavior;
            this.Description = caseAssignmentRuleSet.Description;
            this.CaseAssigneeTypeCode = caseAssignmentRuleSet.CaseAssigneeTypeCode;
            this.CaseAssignmentRules = caseAssignmentRuleSet.CaseAssignmentRules.Select(rule => new CaseAssignmentRuleViewModel()
            {
                Id = rule.Id.ToString(),
                Name = rule.Name,
                Behavior = rule.Behavior,
                Description = rule.Description,
                RuleType = rule.RuleType,
                RuleValues = rule.RuleValues,
                Value = rule.Value,
                Value2 = rule.Value2,
                CaseAssignmentType = rule.CaseAssignmentType == null ? string.Empty : ((int)rule.CaseAssignmentType).ToString(),
                CaseAssignmentId = rule.CaseAssignmentId ?? string.Empty,
                Order = rule.Order,
                CustomFieldId = rule.CustomFieldId,
                CustomFieldType = rule.CustomFieldType
            }).ToList();
        }

        public void GetCaseAssigneeTypes()
        {
            this.CaseAssigneeTypes = new Dictionary<string, string>();
            this.CaseAssigneeTypes.Add("", "Select One...");
            CaseAssigneeType.AsQueryable().Where(cat => cat.CustomerId == CustomerId).OrderBy(cat => cat.Name).ToList().ForEach(type => this.CaseAssigneeTypes.Add(type.Code, type.Name));
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Case Assignee Type")]
        public string CaseAssigneeTypeCode { get; set; }

        public string CustomerId { get; set; }
        public CaseAssignmentRuleBehavior? Behavior { get; set; }

        public List<CaseAssignmentRuleViewModel> CaseAssignmentRules { get; set; }

        public Dictionary<string, string> CaseAssigneeTypes
        { get; set; }

        public Dictionary<string, string> Employers { get; set; }

        public Dictionary<string, string> WorkStates { get; set; }

        public Dictionary<string, string> RuleTypes { get; set; }

        public Dictionary<string, string> CaseTypes { get; set; }
        public Dictionary<string, string> Policies { get; set; }

        public Dictionary<string, string> PolicyTypes { get; set; }

        public Dictionary<string, string> AssignmentTypes { get; set; }

        public Dictionary<string, string> AssignmentTeams { get; set; }
        public Dictionary<string, string> AssignmentUsers { get; set; }

        public List<OfficeLocationViewModel> OfficeLocations { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public Dictionary<string, string> AccommodationTypes { get; set; }

        public CaseAssignmentRuleSet ApplyToDataModel(CaseAssignmentRuleSet caseAssignmentRuleSet)
        {
            caseAssignmentRuleSet.Name = this.Name;
            caseAssignmentRuleSet.Behavior = Behavior;
            caseAssignmentRuleSet.CustomerId = Customer.Current.Id;
            caseAssignmentRuleSet.Description = this.Description;
            caseAssignmentRuleSet.Code = this.Code;
            caseAssignmentRuleSet.CaseAssigneeTypeCode = this.CaseAssigneeTypeCode;
            if (this.CaseAssignmentRules != null)
            {
                List<CaseAssignmentRule> rules = new List<CaseAssignmentRule>();
                this.CaseAssignmentRules.ForEach(ruleViewModel => rules.Add(new CaseAssignmentRule()
                {
                    Id = (string.IsNullOrWhiteSpace(ruleViewModel.Id) ? Guid.NewGuid() : new Guid(ruleViewModel.Id)),
                    Name = ruleViewModel.Name,
                    Behavior = ruleViewModel.Behavior,
                    Description = ruleViewModel.Description,
                    RuleType = ruleViewModel.RuleType,
                    RuleValues = ruleViewModel.RuleValues,
                    Value = ruleViewModel.Value,
                    Value2 = ruleViewModel.Value2,
                    CaseAssignmentType = (ruleViewModel.CaseAssignmentType == null ? null : (AbsenceSoft.Data.Enums.CaseAssigmentType?)int.Parse(ruleViewModel.CaseAssignmentType)),
                    CaseAssignmentId = ruleViewModel.CaseAssignmentId,
                    Order = ruleViewModel.Order,
                    CustomFieldId = ruleViewModel.CustomFieldId,
                    CustomFieldType = ruleViewModel.CustomFieldType
                }));
                caseAssignmentRuleSet.CaseAssignmentRules = rules;
            }
            else
            {
                caseAssignmentRuleSet.CaseAssignmentRules = null;
            }
            return caseAssignmentRuleSet;
        }
    }



}