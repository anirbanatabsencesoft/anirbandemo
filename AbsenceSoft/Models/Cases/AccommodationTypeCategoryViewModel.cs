﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class AccommodationTypeCategoryViewModel
    {
        public AccommodationTypeCategoryViewModel(AccommodationType type)
        {
            // copy properties
            Name = type.Name;
            ParentName = type.ParentName;
            AccommodationTypeCode = type.Code;
            Order = type.Order;

            if (type.Children != null && type.Children.Any())
            {
                Children = type.Children.Select(c => new AccommodationTypeCategoryViewModel(c)).ToList();
            }

        }
        public string Name { get; set; }
        public string ParentName { get; set; }
       
        public string AccommodationTypeCode { get; set; }
        public int? Order { get; set; }
        public List<AccommodationTypeCategoryViewModel> Children { get; set; }
    }
}