﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class CaseHistoryItemModel
    {
        public string CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string CaseType { get; set; }
        public string EmployeeName { get; set; }
        public string ReasonId { get; set; }
        public string Reason { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Status { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedDate { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
    }
}