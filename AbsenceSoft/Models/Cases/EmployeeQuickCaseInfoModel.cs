﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class EmployeeQuickCaseInfoModel
    {
        public string CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
    }
}