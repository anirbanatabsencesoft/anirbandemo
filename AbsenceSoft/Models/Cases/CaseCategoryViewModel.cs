﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseCategoryViewModel : IViewModel, IValidatableObject, IDataTransformModel<CaseCategory>, IDisableableViewModel
    {
        public CaseCategoryViewModel()
        {

        }

        public CaseCategoryViewModel(CaseCategory category)
            : this()
        {
            if (category == null)
                return;

            Id = category.Id;
            CustomerId = category.CustomerId;
            Code = category.Code;
            Name = category.Name;
            Description = category.Description;
            EmployerId = category.EmployerId;
            IsDisabled = category.IsSuppressed(Current.Customer(), Current.Employer());
            Suppressed = category.Suppressed;
            ClassTypeName = typeof(Data.Cases.CaseCategory).Name;
            AbsenceReasons = category.ReasonIds;

        }
        public CaseCategory ApplyToDataModel(CaseCategory category)
        {
            if (category == null)
                category = new CaseCategory();

            category.Name = Name;
            category.Description = Description;
            category.Code = Code;
            category.ReasonIds = AbsenceReasons;
            category.Suppressed = Suppressed;
            return category;
        }

        public bool Suppressed { get; set; }

        public string ClassTypeName { get; set; }

        public string DisplayType { get { return "Code Category"; } }

        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DisplayName("Absence Reason"), UIHint("Absence Reason")]
        public List<string> AbsenceReasons { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool IsDisabled { get; set; }

        public bool IsCustom
        {
            get
            {
                return !string.IsNullOrEmpty(this.CustomerId);
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var caseService = new CaseService(Current.Customer(), Current.Employer(), Current.User()))
            {


                if (caseService.CaseCategoryCodeIsInUse(Id, CustomerId, Code))
                    results.Add(new ValidationResult("This code is already in use by a different note category.", new List<string>() { "Code" }));


            }

            return results;
        }

    }
}