﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseTimeTrackerModel
    {
        public string CaseId { get; set; }
        public DateTime? AsOfDate { get; set; }
    }
}