﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CertificationDetailTodoModel
    {
        //public CertificationDetailTodoModel(object parent)
        //{
        //    this.Parent = parent;
        //}
        //public object Parent { get; set; }

        public Guid? Id { get; set; }
        //[RequiredIfCertificate]
        public DateTime? StartDate { get; set; }
        //[RequiredIfCertificate]
        public DateTime? EndDate { get; set; }
        //[RequiredIfCertificate]
        public int? Occurances { get; set; }
        //[RequiredIfCertificate]
        public int? Frequency { get; set; }
        //[RequiredIfCertificate]
        public Unit? FrequencyType { get; set; }
        public DayUnitType? FrequencyUnitType { get; set; }
        //[RequiredIfCertificate]
        public double? Duration { get; set; }
        //[RequiredIfCertificate]
        public Unit? DurationType { get; set; }
        //[RequiredIfCertificate]
        public string Notes { get; set; }
        //[RequiredIfCertificate]
        public IntermittentType? IntermittentType { get; set; }
    }

    
}