﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class OshaFormFieldViewModel : IViewModel, IDataTransformModel<OshaFormField>, IValidatableObject
    {
        public OshaFormFieldViewModel()
        {

        }

        public OshaFormFieldViewModel(OshaFormField type)
            :this()
        {
            if (type == null)
            {
                return;
            }

            Id = type.Id;
            Name = type.Name;
            Code = type.Code;
            IsDisabled = false;

            if (type.Options != null)
            {
                Options = string.Join(Environment.NewLine, type.Options.Select(c => c));
            }
        }

        public string Id { get; set; }
        public string EmployerId { get; set; }
        public string CustomerId { get; set; }
        public string DisplayType { get { return "osha reports"; } }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DisplayName("Options (one per line)")]
        public string Options { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsCustom
        {
            get
            {
                return !string.IsNullOrEmpty(this.CustomerId);
            }
        }
        public OshaFormField ApplyToDataModel(OshaFormField oshaFormField)
        {
            if (oshaFormField == null)
            {
                oshaFormField = new OshaFormField();
            }

            oshaFormField.Name = Name;
            oshaFormField.Code = Code;
            if (string.IsNullOrEmpty(Options))
            {
                oshaFormField.Options = null;
            }
            else
            {
                var options = Options.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                oshaFormField.Options = new List<string>(options.Length);
                for (int i = 0; i < options.Length; i++)
                {
                    oshaFormField.Options.Add(options[i].Trim());
                }
            }
            return oshaFormField;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(Options))
            {
                return errors;
            }

            var optionsError = new List<string>() { "Options" };
            Regex r = new Regex(@"^[a-zA-Z0-9\s]*$");
            if (!r.IsMatch(Options))
            {
                errors.Add(new ValidationResult("Options must only contain alphanumeric characters or spaces.", optionsError));
            }

            IEnumerable<string> options = Options.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            IEnumerable<string> duplicateOptions = options.GroupBy(c => c)
                .Where(c => c.Count() > 1)
                .Select(c => c.Key);
            
            if(duplicateOptions.Count() > 0)
            {
                errors.Add(new ValidationResult("Options can not contain any duplicate entries.", optionsError));
            }

            return errors;
        }
    }
}