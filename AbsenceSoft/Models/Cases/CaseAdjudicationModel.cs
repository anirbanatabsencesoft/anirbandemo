﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class CaseAdjudicationModel
    {
        public CaseAdjudicationModel()
        {
            this.Policies = new List<CaseAdjudicationPolicyModel>();           
        }

        public AbsenceSoft.Data.Cases.Case Case { get; set; }
        public string CaseId { get; set; }
        [Required]
        public DateTime? ApprovedStartDate { get; set; }
        [Required]
        public DateTime? ApprovedEndDate { get; set; }
        [Required]
        public DateTime? DeniedStartDate { get; set; }
        [Required]
        public DateTime? DeniedEndDate { get; set; }
        [Required]
        public DateTime? PendingStartDate { get; set; }
        [Required]
        public DateTime? PendingEndDate { get; set; }
        public string AdjudicationDenialReasonCode { get; set; }
        public string AdjudicationDenialReasonName { get; set; }
        public string DenialExplanation { get; set; }

        public List<CaseAdjudicationPolicyModel> Policies { get; set; }
    }

    [Serializable]
    public class CaseAdjudicationPolicyModel
    {
        public string PolicyCode { get; set; }
        public bool ApplyStatus { get; set; }
        public string AdjudicationDenialReasonCode { get; set; }
        public string AdjudicationDenialReasonName { get; set; }
        public string DenialExplanation { get; set; }
    }
}