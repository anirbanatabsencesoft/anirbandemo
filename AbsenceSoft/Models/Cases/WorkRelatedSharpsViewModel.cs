﻿using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Cases
{
    public class WorkRelatedSharpsViewModel : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedSharpsViewModel"/> class.
        /// </summary>
        public WorkRelatedSharpsViewModel()
        {
            InjuryLocation = Enum.GetValues(typeof(WorkRelatedSharpsInjuryLocation)).OfType<WorkRelatedSharpsInjuryLocation>().Select(r => new WorkRelatedSharpsInjuryLocationViewModel()
            {
                Value = r
            }).ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkRelatedSharpsViewModel"/> class.
        /// </summary>
        /// <param name="sharps">The sharps.</param>
        public WorkRelatedSharpsViewModel(WorkRelatedSharpsInfo sharps) : this()
        {
            if (sharps == null)
                return;

            this.InjuryLocation = Enum.GetValues(typeof(WorkRelatedSharpsInjuryLocation)).OfType<WorkRelatedSharpsInjuryLocation>().Select(r => new WorkRelatedSharpsInjuryLocationViewModel()
            {
                Selected = sharps.InjuryLocation != null && sharps.InjuryLocation.Contains(r),
                Value = r
            }).ToList();
            this.TypeOfSharp = sharps.TypeOfSharp;
            this.BrandOfSharp = sharps.BrandOfSharp;
            this.ModelOfSharp = sharps.ModelOfSharp;
            this.BodyFluidInvolved = sharps.BodyFluidInvolved;
            this.HaveEngineeredInjuryProtection = sharps.HaveEngineeredInjuryProtection;
            this.WasProtectiveMechanismActivated = sharps.WasProtectiveMechanismActivated;
            this.WhenDidTheInjuryOccur = sharps.WhenDidTheInjuryOccur;
            this.JobClassification = sharps.JobClassification;
            this.JobClassificationOther = sharps.JobClassificationOther;
            this.LocationAndDepartment = sharps.LocationAndDepartment;
            this.LocationAndDepartmentOther = sharps.LocationAndDepartmentOther;
            this.Procedure = sharps.Procedure;
            this.ProcedureOther = sharps.ProcedureOther;
            this.ExposureDetail = sharps.ExposureDetail;
        }

        /// <summary>
        /// Applies the model.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <returns></returns>
        public WorkRelatedSharpsInfo ApplyModel(WorkRelatedSharpsInfo info)
        {
            if (info == null)
                info = new WorkRelatedSharpsInfo();

            info.InjuryLocation = this.InjuryLocation == null ? null : this.InjuryLocation.Where(r => r.Selected).Select(r => r.Value).ToList();
            info.TypeOfSharp = this.TypeOfSharp;
            info.BrandOfSharp = this.BrandOfSharp;
            info.ModelOfSharp = this.ModelOfSharp;
            info.BodyFluidInvolved = this.BodyFluidInvolved;
            info.HaveEngineeredInjuryProtection = this.HaveEngineeredInjuryProtection;
            info.WasProtectiveMechanismActivated = this.WasProtectiveMechanismActivated;
            info.WhenDidTheInjuryOccur = this.WhenDidTheInjuryOccur;
            info.JobClassification = this.JobClassification;
            info.JobClassificationOther = this.JobClassificationOther;
            info.LocationAndDepartment = this.LocationAndDepartment;
            info.LocationAndDepartmentOther = this.LocationAndDepartmentOther;
            info.Procedure = this.Procedure;
            info.ProcedureOther = this.ProcedureOther;
            info.ExposureDetail = this.ExposureDetail;

            return info;
        }

        public class WorkRelatedSharpsInjuryLocationViewModel
        {
            /// <summary>
            /// Gets or sets the value.
            /// </summary>
            /// <value>
            /// The value.
            /// </value>
            public WorkRelatedSharpsInjuryLocation Value { get; set; }

            /// <summary>
            /// Gets the text.
            /// </summary>
            /// <value>
            /// The text.
            /// </value>
            public string Text { get { return Value.ToString().SplitCamelCaseString(); } }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="WorkRelatedSharpsInjuryLocationViewModel"/> is selected.
            /// </summary>
            /// <value>
            ///   <c>true</c> if selected; otherwise, <c>false</c>.
            /// </value>
            public bool Selected { get; set; }

            /// <summary>
            /// Returns a <see cref="System.String" /> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String" /> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return Text;
            }
        }

        /// <summary>
        /// Gets or sets the injury location.
        /// </summary>
        /// <value>
        /// The injury location.
        /// </value>
        public List<WorkRelatedSharpsInjuryLocationViewModel> InjuryLocation { get; set; }

        /// <summary>
        /// Gets or sets the type of sharp.
        /// </summary>
        /// <value>
        /// The type of sharp.
        /// </value>
        [DisplayName("Type of Sharp")]
        public string TypeOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the brand of sharp.
        /// </summary>
        /// <value>
        /// The brand of sharp.
        /// </value>
        [DisplayName("Brand of Sharp")]
        public string BrandOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the model of sharp.
        /// </summary>
        /// <value>
        /// The model of sharp.
        /// </value>
        [DisplayName("Model of Sharp")]
        public string ModelOfSharp { get; set; }

        /// <summary>
        /// Gets or sets the body fluid involved.
        /// </summary>
        /// <value>
        /// The body fluid involved.
        /// </value>
        [DisplayName("Body Fluid Involved")]
        public string BodyFluidInvolved { get; set; }

        /// <summary>
        /// Gets or sets the have engineered injury protection.
        /// </summary>
        /// <value>
        /// The have engineered injury protection.
        /// </value>
        [DisplayName("Has engineered protective mechansim")]
        public bool? HaveEngineeredInjuryProtection { get; set; }

        /// <summary>
        /// Gets or sets the was protective mechanism activated.
        /// </summary>
        /// <value>
        /// The was protective mechanism activated.
        /// </value>
        [DisplayName("Was the protective mechansim activated?")]
        public bool? WasProtectiveMechanismActivated { get; set; }

        /// <summary>
        /// Gets or sets the when did the injury occur.
        /// </summary>
        /// <value>
        /// The when did the injury occur.
        /// </value>
        [DisplayName("When did the Injury Occur?")]
        public WorkRelatedSharpsWhen? WhenDidTheInjuryOccur { get; set; }

        /// <summary>
        /// Gets or sets the job classification.
        /// </summary>
        /// <value>
        /// The job classification.
        /// </value>
        [DisplayName("Job Classification")]
        public WorkRelatedSharpsJobClassification? JobClassification { get; set; }

        /// <summary>
        /// Gets or sets the job classification other.
        /// </summary>
        /// <value>
        /// The job classification other.
        /// </value>
        public string JobClassificationOther { get; set; }

        /// <summary>
        /// Gets or sets the location and department.
        /// </summary>
        /// <value>
        /// The location and department.
        /// </value>
        [DisplayName("Location/Department")]
        public WorkRelatedSharpsLocation? LocationAndDepartment { get; set; }

        /// <summary>
        /// Gets or sets the location and department other.
        /// </summary>
        /// <value>
        /// The location and department other.
        /// </value>
        public string LocationAndDepartmentOther { get; set; }

        /// <summary>
        /// Gets or sets the procedure.
        /// </summary>
        /// <value>
        /// The procedure.
        /// </value>
        [DisplayName("Procedure")]
        public WorkRelatedSharpsProcedure? Procedure { get; set; }

        /// <summary>
        /// Gets or sets the procedure other.
        /// </summary>
        /// <value>
        /// The procedure other.
        /// </value>
        public string ProcedureOther { get; set; }

        /// <summary>
        /// Gets or sets the exposure detail.
        /// </summary>
        /// <value>
        /// The exposure detail.
        /// </value>
        [DisplayName("Exposure Detail")]
        public string ExposureDetail { get; set; }

        /// <summary>
        /// Determines whether the specified object is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();

            //string prefix = validationContext.MemberName;
            //if (!string.IsNullOrWhiteSpace(prefix))
            //    prefix = "." + prefix;
            //else
            //    prefix = "";

            //if (InjuryLocation == null || !InjuryLocation.Any(l => l.Selected))
            //    yield return new ValidationResult("At least 1 'Injury Location' must be selected", new[] { prefix + "InjuryLocation" });

            //if (HaveEngineeredInjuryProtection == true && !WhenDidTheInjuryOccur.HasValue)
            //    yield return new ValidationResult("'When did the Injury Occur?' is required", new[] { prefix + "WhenDidTheInjuryOccur" });

            //if (JobClassification == WorkRelatedSharpsJobClassification.Other && string.IsNullOrWhiteSpace(JobClassificationOther))
            //    yield return new ValidationResult("'Job Classification Other' is required", new[] { prefix + "JobClassificationOther" });

            //if (LocationAndDepartment == WorkRelatedSharpsLocation.Other && string.IsNullOrWhiteSpace(LocationAndDepartmentOther))
            //    yield return new ValidationResult("'Location/Department Other' is required", new[] { prefix + "LocationAndDepartmentOther" });

            //if (Procedure == WorkRelatedSharpsProcedure.Other && string.IsNullOrWhiteSpace(ProcedureOther))
            //    yield return new ValidationResult("'Procedure Other' is required", new[] { prefix + "ProcedureOther" });
        }
    }
}