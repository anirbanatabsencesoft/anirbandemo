﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.ToDo;
using AbsenceSoft.Models.Contacts;
using AbsenceSoft.Models.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Policies;
using AbsenceSoft.Models.RiskProfiles;
using MongoDB.Bson;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class RevisedCaseModel : CasePoliciesDataModel
    {
        public RevisedCaseModel() : base()
        {
            Todos = new List<ToDoItem>();
            Job = new EmployeeJobViewModel();
        }

        public String Status { get; set; }

        //We need this flag because when converting a Inquiry to Case,  need to change Status to Open,else system will not display those
        //absence reason.but at the same time  need a flag to mark it as Inquiry , so if user click on cancel before Create Case. can keep
        //it in Inquiry state
        public bool IsInquiry { get; set; }
        // All of these will be used in the post       
        // **** Added for employee case review on ess case create ui
        public string CaseId { get; set; }
        public bool IsESSCaseReview { get; set; }
        // **** Added for employee case review on ess case create ui
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public string ShortDescription { get; set; }
        public string Summary { get; set; }
        public int? CaseTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String AbsenceReasonId { get; set; }
        public String EmployeeRelationshipCode { get; set; }
        public String EmployeeRelationshipName { get; set; }
        public String EmployeeRelationshipFirstName { get; set; }
        public String EmployeeRelationshipLastName { get; set; }
        public DateTime? EmployeeRelationshipDateOfBirth { get; set; }
        public DateTime? AdoptionDate { get; set; }

        public string EmployeeContactId { get; set; }
        public int? MilitaryStatusId { get; set; }
        public bool? IsWorkRelated { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public DateTime? ActualDeliveryDate { get; set; }
        public bool? WillUseBonding { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public string SpouseCaseId { get; set; }
        public string SpouseCaseNumber { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public virtual ScheduleViewModel WorkSchedule { get; set; }
        public string FTWeeklyWorkTime { get; set; }
        public bool? IsAccommodation { get; set; }
        public bool? IsSTD { get; set; }
        public virtual DisabilityModel Disability { get; set; }
        public virtual AccommodationRequestViewModel AccommodationRequest { get; set; }

        public bool NoFeaturesEnabled { get { return !(ADAFeatureEnabled && STDFeatureEnabled); } }
        public bool ADAFeatureEnabled { get; set; }
        public bool STDFeatureEnabled { get; set; }
        public bool GuidelinesDataEnabled { get; set; }
        public bool STDPayFeatureEnabled { get; set; }
        public bool LOAFeatureEnabled { get; set; }
        public bool EmployerTextFeatureEnabled { get; set; }
        public bool AccommodationTypeCategoriesFeatureEnabled { get; set; }

        public bool ShowIsWorkRelated { get; set; }
        public bool ShowContactSection { get; set; }
        public bool ShowOtherQuestions { get; set; }

        //*********** Parrot fields ************//
        public string EmployeePrimaryPhone { get; set; }
        public string EmployeeSecondaryPhone { get; set; }
        public string EmployeePrimaryEmail { get; set; }
        public string EmployeeSecondaryEmail { get; set; }
        public bool PhoneAlert { get; set; } = true;
        public bool EmailAlert { get; set; } = true;
        public bool TextAlert { get; set; } = true;
        public string EmployeePersonalEmail { get; set; }
        public string EmployeeOfficeLocation { get; set; }
        public string ContactPreference { get; set; }
        public string TimePreference { get; set; }
        public bool? SendECommunication { get; set; }

        public bool? IsContactInformationCorrect { get; set; }
        public Address NewAddress { get; set; }
        public bool? IsAltAddressCorrect { get; set; }
        public Address NewAltAddress { get; set; }
        public bool? IsEmpSafeToBeAtWork { get; set; }
        public bool? IsEmployeeAtWork { get; set; }
        public string WorkplaceModificationDesc { get; set; }
        public bool? IsEmployeeOnLeave { get; set; }
        public string EmpOtherLeaveCaseNumber { get; set; }
        public string EmployeeAskForDesc { get; set; }
        public bool? DidEmployeeSelfIdentify { get; set; }
        public DateTime? DateIdentified { get; set; }
        public bool? HasPerformanceIssue { get; set; }
        public string PerformanceIssueDesc { get; set; }
        public bool? HasWorkConcerns { get; set; }
        public string WorkConcernsDesc { get; set; }
        public string CurrentSchedule { get; set; }

        public CreateContactViewModel CaseReporter { get; set; }
        public AuthorizedSubmitterModel AuthorizedSubmitter { get; set; }
        public List<CustomField> CustomFields { get; set; }

        public List<ToDoItem> Todos { get; set; }
        public bool HasAdministrativeReasons { get; set; }

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        [UIHint("EmployeeJob")]
        public EmployeeJobViewModel Job { get; set; }

        //Information related to new user request for re-opening a Case
        public Relapse Relapse { get; set; }

        //Existing Case information with modifications related to re-opening the case. Will be used while saving the case
        public Case RelapseCase { get; set; }

        //Cause of Leave request
        public AbsenceReason Reason { get; set; }

        public string CaseType { get; set; }

        public string CaseNumber { get; set; }

        public RiskProfileViewModel RiskProfile { get; set; }

        public string StatusString { get; set; }

        public object CaseAutoAssignedTo { get; set; }

        public string AssignedToName { get; set; }

        public string ShortSummary { get; set; }

        public EmployeeContact Contact { get; set; }

        public string AccommodationCategory { get; set; }

        public DateTime cdt { get; set; }

        public DateTime mdt { get; set; }

        public DateTime? ClosedDt { get; set; }

        public DateTime? CancelDt { get; set; }

        public string ClosureReasonDetails { get; set; }

        public List<CaseEventViewModel> CaseEvents { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        //Remove after confirmation
        public string State { get; set; }
        //Remove after confirmation
        public string Country { get; set; }
        //Remove after confirmation
        public string PostalCode { get; set; }

        public BsonDocument MetaData { get; set; }

        public bool RelapseDateChanged { get; set; }

        public DateTime? ExistingStartDate { get; set; }

        public DateTime? ExistingEndDate { get; set; }

        public bool EmployeeHasFTESchedule { get; set; }
        public string EmployeeFaxNumber { get;  set; }
        public CaseChangeModel ChangeCaseData { get; set; }
    }
}