﻿namespace AbsenceSoft.Models.Cases
{
    public class AuthorizedSubmitterModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }
}