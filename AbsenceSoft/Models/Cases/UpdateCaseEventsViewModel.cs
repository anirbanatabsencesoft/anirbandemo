﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class UpdateCaseEventsViewModel
    {
        public string CaseId { get; set; }

        public List<CaseEventViewModel> Events { get; set; }
    }
}