﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseEssReviewModel
    {
        // All of these will be used in the post
        public string CaseId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public bool? IsContactInformationCorrect { get; set; }
        public string NewAddress { get; set; }
        public string NewCity { get; set; }
        public string NewState { get; set; }
        public string NewZipcode { get; set; }
        public string NewWorkPhone { get; set; }
        public string NewEmail { get; set; }
        public string LeaveFor { get; set; }
        public int? CaseTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String AbsenceReasonId { get; set; }
        public string EmployeeRelationshipCode { get; set; }
        public string EmployeeContactId { get; set; }
        public String EmployeeRelationshipFirstName { get; set; }
        public String EmployeeRelationshipLastName { get; set; }
        public DateTime? EmployeeRelationshipDOB { get; set; }
        public string ShortDescription { get; set; }
        public int? MilitaryStatusId { get; set; }
        public bool? IsWorkRelated { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public DateTime? ActualDeliveryDate { get; set; }
        public bool? WillUseBonding { get; set; }
        public DateTime? BondingStartDate { get; set; }
        public DateTime? BondingEndDate { get; set; }
        public DateTime? AdoptionDate { get; set; }
        public bool? IsWorkScheduleCorrect { get; set; }
        public CaseStatus Status { get; set; }
        public bool? IsAccommodation { get; set; }
    }
}