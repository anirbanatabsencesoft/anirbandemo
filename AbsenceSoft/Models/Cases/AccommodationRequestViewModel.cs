﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Common.Serializers;
using Newtonsoft.Json;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Cases
{
    public class AccommodationRequestViewModel
    {
        public AccommodationRequestViewModel()
        { 
            Accommodations = new List<AccommodationViewModel>(); 
        }

        public string CaseId { get; set; }
        public CaseStatus Status { get; set; }//Inquiry, Open, Closed, Cancelled
        public AccommodationCancelReason? CancelReason { get; set; }
        public string OtherDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string GeneralHealthCondition { get; set; }
        public string Description { get; set; }
        public string HCPContactId { get; set; }
        public string HCPContactTypeCode { get; set; }
        public string HCPCompanyName { get; set; }
        public string HCPFirstName { get; set; }
        public string HCPLastName { get; set; }
        public string HCPPhone { get; set; }
        public string HCPFax { get; set; }
        public bool IsPrimary { get; set; }
        public List<AccommodationViewModel> Accommodations { get; set; }
        public bool HasAnyRequests { get; set; }
        public bool HasAnyAccommodations { get; set; }
        public List<IntProcStepGroupViewModel> AdditionalQuestions { get; set; }
        public AccommodationType Type { get; set; }//Ergonomic Assessment, Equipment or Software, Schedule Change or Other
    }

    public class AccommodationViewModel
    {
        public AccommodationViewModel() { AccommodationUsage = new List<AccommodationUsageViewModel>(); }
        public string Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public AccommodationType Type { get; set; }//Ergonomic Assessment, Equipment or Software, Schedule Change or Other
        public AccommodationDuration Duration { get; set; }//Temporary, Permanent
        public bool? IsWorkRelated { get; set; }
        public string Description { get; set; }
        public string Resolution { get; set; }
        public bool Resolved { get; set; }
        public DateTime? ResolvedDate { get; set; }
        public bool Granted { get; set; }
        public DateTime? GrantedDate { get; set; }
        public decimal? Cost { get; set; }
        public CaseStatus Status { get; set; }
        public AccommodationCancelReason? CancelReason { get; set; }
        public string OtherReasonDesc { get; set; }
        public List<AccommodationUsageViewModel> AccommodationUsage { get; set; }
        public List<IntProcStepGroupViewModel> InteractiveProcessGroups { get; set; }
        public DateTime? ApprovedStartDate { get; set; }
        public DateTime? ApprovedEndDate { get; set; }
        public DateTime? PendingStartDate { get; set; }
        public DateTime? PendingEndDate { get; set; }
        public DateTime? DeniedStartDate { get; set; }
        public DateTime? DeniedEndDate { get; set; }
        public string DenialReasonCode { get; set; }
        public string DenialReasonName { get; set; }
        public string DenialExplanation { get; set; }
    }

    public class AccommodationUsageViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public AdjudicationStatus AdjudicationStatus { get; set; }
        public string DenialReasonCode { get; set; }
        public string DenialReasonName { get; set; }
        public string DenialReasonOther { get; set; }
    }

    public class IntProcStepGroupViewModel
    {
        public IntProcStepGroupViewModel()
        {
            this.InteractiveProcessSteps = new List<IntProcStepViewModel>();
        }
        public string Name { get; set; }
        public List<IntProcStepViewModel> InteractiveProcessSteps { get; set; }
    }

    public class IntProcStepViewModel
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string ReportLabel { get; set; }
        public bool? Answer { get; set; }
        public string SAnswer { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DecisionOverturned { get; set; }
        public DateTime? DateOfInitialDecision { get; set; }
        public string Time { get; set; }
        public string Provider { get; set; }
        public int Order { get; set; }
        public string Note { get; set; }
        public List<IntProcNote> Notes { get; set; }
        public bool QuestionVisible { get; set; }
        public bool NoteVisible { get; set; }
        public EmployeeContact Contact { get; set; }
        public List<string> Choices { get; set; }
        public bool PromptDate { get; set; }
        public bool PromptTime { get; set; }
        public bool PromptProvider { get; set; }
        public bool PromptDateDecisionOverturned { get; set; }
        public bool PromptDateOfInitialDecision { get; set; }
        public bool IsYesNo { get; set; }
    }

    public class IntProcNote
    {
        public string CreateDate { get; set; }
        public string Note { get; set; }
        public string ContactInfo { get; set; }
        public bool? Answer { get; set; }
        public string SAnswer { get; set; }
    }
}