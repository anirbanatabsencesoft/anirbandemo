﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class ReassignCasesModel
    {
        public string UserId { get; set; }
        public string CaseAssigneeTypeCode { get; set; }
        public List<ReassignCase> Cases { get; set; }        
    }

    [Serializable]
    public class ReassignCase
    {
        public string CaseId { get; set; }
        public bool ApplyStatus { get; set; }
        public bool SuppressThresholdValidation { get; set; }
    }

    [Serializable]
    public class BulkUpdateCaseAssignment
    {
        public string CaseId { get; set; }
        public bool ApplyStatus { get; set; }
        public string UserId { get; set; }
    }
}