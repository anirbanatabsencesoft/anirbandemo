﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace AbsenceSoft.Models.Cases
{
    public class CaseAssigneeTypeViewModel
    {
        public CaseAssigneeTypeViewModel()
        {

        }

        public CaseAssigneeTypeViewModel(CaseAssigneeType caseAssignee)
            : base()
        {
            this.Id = caseAssignee.Id;
            this.Name = caseAssignee.Name;
            this.CustomerId = caseAssignee.CustomerId;
            this.Code = caseAssignee.Code;
            this.Description = caseAssignee.Description;
            this.ThresholdCaseCount = caseAssignee.ThresholdCaseCount;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Threshold Case Count must be a non negative whole number.")]        
        [DisplayName("Threshold Case Count")]
        public int? ThresholdCaseCount { get; set; }

        public string CustomerId { get; set; }

        public CaseAssigneeType ApplyToDataModel(CaseAssigneeType caseAssigneeType)
        {
            caseAssigneeType.Name = this.Name;
            caseAssigneeType.CustomerId = Customer.Current.Id;
            caseAssigneeType.Description = this.Description;
            caseAssigneeType.Code = this.Code;
            caseAssigneeType.ThresholdCaseCount = this.ThresholdCaseCount;
            return caseAssigneeType;
        }
    }
}