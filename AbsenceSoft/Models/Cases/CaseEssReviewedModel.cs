﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseEssReviewedModel : BaseCaseEss
    {
        public string CaseId { get; set; }

        // All of these will be used in the post
        public CaseStatus Status {get; set;}
        public CaseCancelReason? CancelReason { get; set; }
    }
}