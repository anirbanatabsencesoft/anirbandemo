﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AbsenceSoft.Logic.Customers;

namespace AbsenceSoft.Models.Cases
{
    public class EmployeeClassViewModel : IViewModel, IValidatableObject, IDataTransformModel<EmployeeClass>, IDisableableViewModel
    {
        public EmployeeClassViewModel()
        {

        }

        public EmployeeClassViewModel(EmployeeClass employeeClass)
            : this()
        {
            if (employeeClass == null)
                return;

            Id = employeeClass.Id;
            CustomerId = employeeClass.CustomerId;
            Code = employeeClass.Code;
            Name = employeeClass.Name;
            Description = employeeClass.Description;
            EmployerId = employeeClass.EmployerId;
            IsDisabled = IsDisabledInContext(employeeClass);
            Suppressed = employeeClass.Suppressed;
            ClassTypeName = typeof(Data.Customers.EmployeeClass).Name;
            Order = employeeClass.Order;
            Locked = employeeClass.IsLocked;
        }

        private bool IsDisabledInContext(EmployeeClass employeeClass)
        {
            using (var employeeClassService = new EmployeeClassService(Current.Customer(), Current.Employer(), Current.User()))
            {
                return employeeClassService.IsDisabledInContext(employeeClass.Code);
            }                
        }

        

        public EmployeeClass ApplyToDataModel(EmployeeClass employeeClass)
        {
            if (employeeClass == null)
                employeeClass = new EmployeeClass();

            employeeClass.Description = Description;
            employeeClass.Code = Code;
            employeeClass.Name = Name;
            employeeClass.Suppressed = Suppressed;
            employeeClass.Order = Order;
            employeeClass.IsLocked = Locked;
            return employeeClass;
        }

        public bool IsEmployeeClassSuppressedAtCustomerLevel(string code)
        {
            using (var employeeClassService = new EmployeeClassService(Current.Customer(), Current.Employer(), Current.User()))
            {
                return employeeClassService.IsEmployeeClassDisabledInCurrentContext(code);
            }
        }
        public bool Suppressed { get; set; }

        public string ClassTypeName { get; set; }

        public string DisplayType { get { return "Employee Class"; } }

        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string EmployerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int? Order { get; set; }

        public bool IsDisabled { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var employeeClassService = new EmployeeClassService(Current.Customer(), Current.Employer(), Current.User()))
            {
                if (String.IsNullOrWhiteSpace(Code))
                {
                    results.Add(new ValidationResult("This Code should not be null.", new List<string>() { "Code" }));
                    return results;
                }
                if (employeeClassService.EmployeeClassIsInUse(Id, Code))
                    results.Add(new ValidationResult("This code is already in use by a different employee class.", new List<string>() { "Code" }));
            }
            return results;
        }

        public bool Locked { get; set; }

    }
}