﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class UpdatePayScheduleModel
    {
        public string CaseId { get; set; }
        public string PayScheduleId { get; set; }
    }
}