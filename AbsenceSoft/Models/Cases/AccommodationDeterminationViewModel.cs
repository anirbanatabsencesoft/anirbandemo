﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Common.Serializers;
using Newtonsoft.Json;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Models.Cases
{
    public class AccommodationDeterminationViewModel
    {
        public string CaseId { get; set; }
        public DateTime? ApprovedStartDate { get; set; }
        public DateTime? ApprovedEndDate { get; set; }
        public DateTime? DeniedStartDate { get; set; }
        public DateTime? DeniedEndDate { get; set; }
        public DateTime? PendingStartDate { get; set; }
        public DateTime? PendingEndDate { get; set; }
        public bool? Resolved { get; set; }
        public DateTime? ResolvedDate { get; set; }
        public bool? Granted { get; set; }
        public DateTime? GrantedDate { get; set; }
        public string Resolution { get; set; }
        public AdjudicationStatus AdjudicationStatus { get; set; }
        public string DenialReasonCode { get; set; }
        public string DenialReasonName { get; set; }
        public string DenialExplanation { get; set; }
        public List<AccommodationAdjudicationPolicyModel> Accommodations { get; set; }
    }

    [Serializable]
    public class AccommodationAdjudicationPolicyModel
    {
        public string AccommodationId { get; set; }
        public bool ApplyStatus { get; set; }
    }
}