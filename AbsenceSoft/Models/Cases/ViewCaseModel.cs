﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Models.Employees;
using AbsenceSoft.Models.Demands;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class ViewCaseModel
    {
        [Required]
        public String Id { get; set; }
        public bool ShowNewCaseCommunicationsModal { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CaseNumber { get; set; }
        public String Status { get; set; }
        public CaseStatus CaseStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public string ShortSummary { get; set; }
        public String Reason { get; set; }
        public String SubReason { get; set; }
        public String EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public bool IsIntermittent { get; set; }
        public String LeaveType { get; set; }
        public string SpouseCaseId { get; set; }
        public string SpouseCaseNumber { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public bool IsAccommodation { get; set; }
        public bool? IsSTD { get; set; }
        public virtual DisabilityModel Disability { get; set; }
        
        public bool NoFeaturesEnabled { get; set; }
        public bool ADAFeatureEnabled { get; set; }
        public bool STDFeatureEnabled { get; set; }
        public bool GuidelinesDataEnabled { get; set; }
        public bool STDPayFeatureEnabled { get; set; }
        public bool LOAFeatureEnabled { get; set; }
        public bool RiskProfileFeatureEnabled { get; set; }
        public bool WorkRestrictionFeatureEnabled { get; set; }
        public bool AccommodationTypeCategoriesFeatureEnabled { get; set; }

        public List<WorkRestrictionViewModel> WorkRestrictions { get; set; }
        public List<EmployeeNecessity> EmployeeNecessities { get; set; }
        [UIHint("EmployeeJob")]
        public EmployeeJobViewModel EmployeeJob { get; set; }

        public bool ShowIsWorkRelated { get; set; }
        public bool ShowContactSection { get; set; }
        public bool ShowOtherQuestions { get; set; }
        public bool ShowAccomCategory { get; set; }
        public bool ShowAccomAdditionalInfo { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public string EmployeeName { get; set; }

        public WorkRelatedViewModel WorkRelatedInfo { get; set; }

        public string MessageOnViewLoad { get; set; }

        public List<Relapse> Relapses { get; set; }

        public List<Time> MissedTimeOverride { get; set; }

        public double? FTWeeklyWorkHours { get; set; }

    }
}
