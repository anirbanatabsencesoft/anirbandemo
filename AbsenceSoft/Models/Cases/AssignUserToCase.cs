﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class AssignUserToCase
    {
        public string CaseId { get; set; }
        public string UserId { get; set; }
        public string CaseAssignmentRoleId { get; set; }
    }
}