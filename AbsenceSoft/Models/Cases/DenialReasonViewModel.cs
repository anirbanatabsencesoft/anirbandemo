﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class DenialReasonViewModel : IViewModel, IValidatableObject, IDataTransformModel<DenialReason>, IDisableableViewModel
    {
        public DenialReasonViewModel()
        {

        }

        public DenialReasonViewModel(DenialReason denialReason)
            : this()
        {
            if (denialReason == null)
                return;

            Id = denialReason.Id;
            CustomerId = denialReason.CustomerId;
            Code = denialReason.Code;
            Description = denialReason.Description;
            EmployerId = denialReason.EmployerId;
            IsDisabled = IsDisabledInContext(denialReason.Code);
            Suppressed = denialReason.Suppressed;
            ClassTypeName = typeof(Data.Cases.DenialReason).Name;
            AbsenceReasons = denialReason.AbsenceReasons;
            Order = denialReason.Order;
            PolicyTypes = denialReason.PolicyTypes;
            PolicyCodes = denialReason.PolicyCodes;
            AccommodationTypes = denialReason.AccommodationTypes;
            IsPolicy = (denialReason.Target & DenialReasonTarget.Policy) == DenialReasonTarget.Policy;
            IsAccommodation = (denialReason.Target & DenialReasonTarget.Accommodation) == DenialReasonTarget.Accommodation;
            Locked = denialReason.Locked;
        }

        private bool IsDisabledInContext(string code)
        {
            var currentEmployer = Current.Employer();
            var currentCustomer = Current.Customer();
            bool isReasonDisabled = false;

            if (currentEmployer != null)
            {
                var denialReason = currentEmployer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "DenialReason" && p.EntityCode == code.ToUpperInvariant());
                if (denialReason != null)
                {
                    isReasonDisabled = true;
                }
            }
            else if (currentCustomer != null && !isReasonDisabled)
            {
                var denialReason = currentCustomer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "DenialReason" && p.EntityCode == code.ToUpperInvariant());
                if (denialReason != null)
                {
                    isReasonDisabled = true;
                }
            }
            return isReasonDisabled;
        }

        public DenialReason ApplyToDataModel(DenialReason denialReason)
        {
            if (denialReason == null)
                denialReason = new DenialReason();

            denialReason.Description = Description;
            denialReason.Code = Code;
            denialReason.AbsenceReasons = AbsenceReasons;
            denialReason.Suppressed = Suppressed;
            denialReason.Order = Order;
            denialReason.PolicyTypes = PolicyTypes;
            denialReason.PolicyCodes = PolicyCodes;
            denialReason.AccommodationTypes = AccommodationTypes;
            denialReason.Target = SetTarget();
            denialReason.Locked = Locked;
            return denialReason;
        }

        public bool IsDenialReasonSuppressedAtCustomerLevel(string code)
        {
            bool isDisabled = false;
            if (code != null && code.Trim().Length > 0)
            {
                if (Current.Employer() != null)
                {
                    var currentCustomer = Current.Customer();
                    var denialReason = currentCustomer.SuppressedEntities.FirstOrDefault(p => p.EntityName == "DenialReason" && p.EntityCode == code.ToUpperInvariant());
                    if (denialReason != null)
                    {
                        isDisabled = true;
                    }
                }
            }            
            return isDisabled;
        }
        public bool Suppressed { get; set; }

        public string ClassTypeName { get; set; }

        public string DisplayType { get { return "Denial Reason"; } }

        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string EmployerId { get; set; }
        
        [Required, CodeFormat]
        public string Code { get; set; }

        [DisplayName("Absence Reason")]
        public List<string> AbsenceReasons { get; set; }

        [DisplayName("Policy Type"), UIHint("Policy Type")]
        public List<string> PolicyTypes { get; set; }

        [DisplayName("Policy Code"), UIHint("Policy Code")]
        public List<string> PolicyCodes { get; set; }

        [DisplayName("Accommodation Type"), UIHint("Accommodation Type")]
        public List<string> AccommodationTypes { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int? Order { get; set; }

        public bool IsDisabled { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            using (var caseService = new CaseService(Current.Customer(), Current.Employer(), Current.User()))
            {
                if (caseService.DenialReasonCodeIsInUse(Id, CustomerId, Code))
                    results.Add(new ValidationResult("This code is already in use by a different denial reason.", new List<string>() { "Code" }));
            }
            if (!IsPolicy && !IsAccommodation)
                results.Add(new ValidationResult("You must select whether this denial reason targets the policy, accommodation, or both."));
            return results;
        }

        [DisplayName("Policy")]
        public bool IsPolicy { get; set; }

        [DisplayName("Accommodation")]
        public bool IsAccommodation { get; set; }

        public bool Locked { get; set; }

        private DenialReasonTarget SetTarget()
        {
            DenialReasonTarget target = DenialReasonTarget.None;
            if (IsPolicy)
                target = target | DenialReasonTarget.Policy;

            if (IsAccommodation)
                target = target | DenialReasonTarget.Accommodation;

            return target;
        }
    }
}