﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class EditCaseModel : CasePoliciesDataModel
    {
        public String Id { get; set; }
    }

    [Serializable]
    public class DeleteCasePolicy
    {
        public string Code { get; set; }
    }
}