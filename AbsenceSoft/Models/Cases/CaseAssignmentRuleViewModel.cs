﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class CaseAssignmentRuleViewModel
    {
        public CaseAssignmentRuleViewModel()
        {

        }
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string RuleType
        { get; set; }

        public string[] RuleValues
        { get; set; }

        public string Value
        { get; set; }

        public string Value2
        { get; set; }
        public string CaseAssignmentType
        { get; set; }
        public string CaseAssignmentId
        { get; set; }
        public string CustomFieldId { get; set; }
        public string CustomFieldType { get; set; }
        public int Order { get; set; }
        public CaseAssignmentRuleBehavior? Behavior { get; set; }

        public Dictionary<string, string> Employers { get; set; }

        public Dictionary<string, string> WorkStates { get; set; }

        public Dictionary<string, string> RuleTypes { get; set; }

        public Dictionary<string, string> CaseTypes { get; set; }
        public Dictionary<string, string> Policies { get; set; }

        public Dictionary<string, string> PolicyTypes { get; set; }

        public Dictionary<string, string> AssignmentTypes { get; set; }

        public Dictionary<string, string> AssignmentTeams { get; set; }

        public Dictionary<string, string> AssignmentUsers { get; set; }

        public List<OfficeLocationViewModel> OfficeLocations { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public Dictionary<string, string> AccommodationTypes { get; set; }

    }
}