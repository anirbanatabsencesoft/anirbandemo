﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class EmployeeCasesModel
    {
        public EmployeeCasesModel()
        {
            Cases = new List<Data.Cases.Case>();
        }

        public AbsenceSoft.Data.Customers.Employee Employee { get; set; }

        public List<Data.Cases.Case> Cases { get; set; }
    }
}