﻿using AbsenceSoft.Data.Cases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class DiagnosisGuidlinesModel
    {
        public DiagnosisGuidlinesModel()
        {
            RTWBestPracticeList = new List<string>();
        }

        public DiagnosisGuidelines Guidelines { get; set; }
        public List<string> RTWBestPracticeList { get; set; }       
    }
}