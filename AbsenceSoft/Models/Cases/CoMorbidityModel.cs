﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Logic;
using AbsenceSoft.Data.Cases;

namespace AbsenceSoft.Models.Cases
{
    public class CoMorbidityModel
    {
        public string CaseId { get; set; }
        public string EmployeeId { get; set; }
        public CoMorbidityGuideline.CoMorbidityArgs Args { get; set; }
    }
}