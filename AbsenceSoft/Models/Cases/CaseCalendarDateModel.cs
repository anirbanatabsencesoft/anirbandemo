﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    [Serializable]
    public class CaseCalendarDateModel
    {
        public CaseCalendarDateModel()
        {
            this.Cases = new List<Case>();
        }
        public List<Case> Cases { get; set; }
        public DateTime Date { get; set; }
        public bool HasApprovedUsage { get; set; }
        public bool HasDeniedUsage { get; set; }
        public bool HasPendingUsage { get; set; }
        public bool HasConsecutiveSegment { get; set; }
        public bool HasIntermittentSegment { get; set; }
        public bool IsHoliday { get; set; }
        public string HolidayName { get; set; }

        [Serializable]
        public class Case
        {
            public Case()
            {
                this.Policies = new List<Policy>();
                this.UserRequests = new List<UserRequest>();
                
            }
            public List<Policy> Policies { get; set; }
            public List<UserRequest> UserRequests { get; set; }
            public String CaseId { get; set; }
            public String CaseNumber { get; set; }
            public String ReasonName { get; set; }
            public CaseType CaseType { get; set; }
            public List<Time> MissedTimeOverride { get; set; }

            [Serializable]
            public class Policy
            {
                public String PolicyCode { get; set; }
                public String PolicyName { get; set; }
                public AdjudicationStatus Determination { get; set; }
                public String DeterminationText { get; set; }
                public DateTime StartDate { get; set; }
                public DateTime EndDate { get; set; }
                public double HoursUsed { get; set; }
                public string UsedTypeText { get; set; }
            }

            [Serializable]
            public class UserRequest
            {
                public DateTime? RequestDate { get; set; }
                public int? TotalMinutes { get; set; }
                public IntermittentType? IntermittentType { get; set; }
                public string IntermittentTypeText { get; set; }

                public string TotalTime
                {
                    get
                    {
                        int? hours = this.TotalMinutes / 60;
                        int? minutes = this.TotalMinutes % 60;
                        if (minutes == 0)
                            return string.Format("{0:0} hours", hours);

                        return string.Format("{0:0} hours and {1:0} minutes", hours, minutes);
                    }
                }
            }
        }
    }

    public class CaseCalendarRequestModel
    {
        public String CaseId { get; set; }
        public String EmployeeId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }

    public class CaseCalendarReponseModel
    {
        public CaseCalendarReponseModel()
        {
            this.Dates = new List<CaseCalendarDateModel>();
        }
        public List<CaseCalendarDateModel> Dates { get; set; }
        public bool HasAnyResults { get; set; }
    }
}