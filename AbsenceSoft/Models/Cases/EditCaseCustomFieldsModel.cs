﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Cases
{
    public class EditCaseCustomFieldsModel
    {
        public List<CustomField> CustomFields { get; set; }
    }
}