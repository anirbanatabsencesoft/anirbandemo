﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class NewJobViewModel:IValidatableObject
    {
        [Required]
        public string EmployeeId { get; set; }

        [Required]
        public string JobCode { get; set; }

        [Required, DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        public string CaseId { get; set; }

        public string JobName { get; set; }

        [DisplayName("Title")]
        public string JobTitle { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (this.StartDate.Value > this.EndDate)
            {
                results.Add(new ValidationResult("End Date must be after Start Date", new List<string>() { "EndDate" }));
            }
            return results;
        }
    }
}