﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Models.Demands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class JobRequirementViewModel: AppliedDemandViewModel
    {
        public JobRequirementViewModel()
            : base()
        {
            this.Values = new List<AppliedDemandValueViewModel>();
        }

        public JobRequirementViewModel(JobRequirement requirement, Demand demand, List<DemandType> demandTypes)
            :this(requirement)
        {
            this.DemandId = demand.Id;
            this.DemandName = demand.Name;
            if (requirement == null)
            {
                BuildValuesModel(demand, demandTypes, d => d.RequirementLabel);
            }
        }

        public JobRequirementViewModel(JobRequirement requirement)
            : this()
        {
            if (requirement == null)
                return;

            this.DemandId = requirement.DemandId;
            if (requirement.Dates != null)
            {
                this.StartDate = requirement.Dates.StartDate;
                this.OverriddenStartDate = requirement.Dates.StartDate;
                this.EndDate = requirement.Dates.EndDate;
                this.DateString = requirement.Dates.ToString();
            }

            if (requirement.Values != null)
            {
                this.Values = requirement.Values.Select(v => new AppliedDemandValueViewModel(v.DemandTypeId, v.Type, v.Text, v.Applicable, v.Schedule, v.DemandItem, v.ToString())).ToList();
                BuildValuesModel(label: d => d.RequirementLabel);
            }
        }

        public JobRequirement ApplyToDataModel(JobRequirement requirement = null)
        {
            if (requirement == null)
                requirement = new JobRequirement();

            requirement.DemandId = this.DemandId;
            DateTime startDate = this.OverriddenStartDate.HasValue ? this.OverriddenStartDate.Value : StartDate.Value;
            requirement.Dates = string.IsNullOrWhiteSpace(startDate.ToString()) ? DateRange.Null : new DateRange(startDate, this.EndDate);

            ApplyValuesToDataModel(requirement);

            return requirement;
        }

        private void ApplyValuesToDataModel(JobRequirement requirement)
        {
            if (this.Values == null || this.Values.Count() == 0)
            {
                requirement.Values = null;
                return;
            }

            List<AppliedDemandValue<JobRequirementValue>> values = new List<AppliedDemandValue<JobRequirementValue>>(this.Values.Count());
            foreach (var value in this.Values)
            {
                values.Add(BuildJobRequirementValue(value));
            }

            requirement.Values = values;
        }

        /// <summary>
        /// Builds a work restriction value from the UI model
        /// </summary>
        /// <param name="valueModel"></param>
        /// <returns></returns>
        private AppliedDemandValue<JobRequirementValue> BuildJobRequirementValue(AppliedDemandValueViewModel valueModel)
        {
            AppliedDemandValue<JobRequirementValue> value = new AppliedDemandValue<JobRequirementValue>()
            {
                Type = valueModel.Type,
                DemandTypeId = valueModel.DemandTypeId
            };
            switch (valueModel.Type)
            {
                case DemandValueType.Text:
                    value.Text = valueModel.Text;
                    break;
                case DemandValueType.Boolean:
                    value.Applicable = valueModel.Applicable;
                    break;
                case DemandValueType.Value:
                    if (valueModel.DemandItemId.HasValue)
                    {
                        value.DemandItem = value.DemandType.Items.FirstOrDefault(i => i.Id == valueModel.DemandItemId.Value);
                    }
                    break;
                case DemandValueType.Schedule:
                    if (valueModel.DurationType.HasValue && valueModel.FrequencyType.HasValue && valueModel.FrequencyUnitType.HasValue)
                    {
                        value.Schedule = new ScheduleDemand()
                        {
                            Duration = valueModel.Duration,
                            DurationType = valueModel.DurationType.Value,
                            Frequency = valueModel.Frequency,
                            FrequencyType = valueModel.FrequencyType.Value,
                            FrequencyUnitType = valueModel.FrequencyUnitType.Value,
                            Occurances = valueModel.Occurances
                        };
                    }
                    break;
                default:
                    break;
            }

            return value;
        }
    }
}