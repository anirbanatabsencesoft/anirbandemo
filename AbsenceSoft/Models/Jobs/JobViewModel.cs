﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AbsenceSoft.Web;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Models.Jobs
{
    public class JobViewModel
    {
        public JobViewModel()
        {
            Requirements = new List<JobRequirementViewModel>();
            Necessities = new List<JobNecessityViewModel>();
            Links = new List<HyperlinkViewModel>();
            IndustryCodes = new List<JobIndustryCodeViewModel>();
        }

        public JobViewModel(Job theJob)
            :this()
        {
            
            if (theJob != null)
            {
                Id = theJob.Id;
                Name = theJob.Name;
                Code = theJob.Code;
                Activity = theJob.Activity;
                Description = theJob.Description;
                OfficeLocationCode = theJob.OrganizationCode;
                if(theJob.Links != null)
                    Links = theJob.Links.Select(l => new HyperlinkViewModel(l)).ToList();

                if(theJob.RestrictedNecessities != null)
                    Necessities = theJob.RestrictedNecessities.Select(rn => new JobNecessityViewModel(rn)).ToList();

                if (theJob.IndustryCodes != null)
                    IndustryCodes = theJob.IndustryCodes.Select(ic => new JobIndustryCodeViewModel(ic)).ToList();
            }
            string employerId = null;
            if (theJob != null)
                employerId = theJob.EmployerId;

            BuildRequirements(employerId, theJob);
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        /// <summary>
        /// The classified activity level of the job
        /// </summary>
        [DisplayName("Job Activity"), UIHint("EnumButtons")]
        public JobClassification? Activity { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public List<JobNecessityViewModel> Necessities { get; set; }
        
        public List<JobRequirementViewModel> Requirements { get; set; }

        public List<HyperlinkViewModel> Links { get; set; }

        public List<JobIndustryCodeViewModel> IndustryCodes { get; set; }

        [UIHint("Organization"), DisplayName("Office Locations")]
        public string OfficeLocationCode { get; set; }
        
        internal Job ApplyToDataModel(Job job)
        {
            if (job == null)
                job = new Job();

            job.Name = Name;
            job.Code = Code;
            job.Description = Description;
            job.OrganizationCode = OfficeLocationCode;
            job.Activity = Activity;
            ApplyRequirementsToDataModel(job);
            ApplyLinksToDataModel(job);
            ApplyNecessitiesToDataModel(job);
            ApplyIndustryCodesToDataModel(job);
            return job;
        }

        internal void BuildRequirements(string employerId, Job theJob = null)
        {
            using (var demandService = new DemandService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var demands = demandService.GetDemands();
                var demandTypes = demandService.GetDemandTypes();
                foreach (var demand in demands)
                {
                    JobRequirement matchingRequirement = null;
                    if(theJob != null && theJob.Requirements != null)
                        matchingRequirement = theJob.Requirements.FirstOrDefault(r => r.DemandId == demand.Id);
                    
                    var viewModel = new JobRequirementViewModel(matchingRequirement, demand, demandTypes);
                    if (viewModel.StartDate == null)
                        viewModel.StartDate = DateTime.UtcNow.ToMidnight();

                    Requirements.Add(viewModel);
                }
            }
        }

        internal void RebuildViewModel()
        {
            using (var demandService = new DemandService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var demandTypes = demandService.GetDemandTypes();
                foreach (var requirement in Requirements)
                {
                    requirement.BuildValuesModel(null, demandTypes, d => d.RequirementLabel);
                }
            }
        }

        private void ApplyRequirementsToDataModel(Job job)
        {
            var requirementsWithValues = Requirements.Where(r => r.HasValues).ToList();
            var requirements = new List<JobRequirement>(requirementsWithValues.Count);
            foreach (var requirement in requirementsWithValues)
            {
                var req = job.Requirements.FirstOrDefault(r => r.DemandId == requirement.DemandId);
                req = requirement.ApplyToDataModel(req);
                requirements.Add(req);
            }

            job.Requirements = requirements;
        }

        private void ApplyLinksToDataModel(Job job)
        {
            if (Links == null || Links.Count == 0)
            {
                job.Links = null;
            }
            else
            {
                job.Links = Links.Select(
                    l => new Hyperlink
                    {
                        Title = l.Title,
                        Text = l.Text,
                        Url = l.Url
                    }).ToList();
            }
        }

        private void ApplyNecessitiesToDataModel(Job job)
        {
            if (Necessities == null || Necessities.Count == 0)
            {
                job.RestrictedNecessities = null;
            }
            else
            {
                job.RestrictedNecessities = Necessities.Select(
                    n => new JobRestrictedNecessity
                    {
                        NecessityId = n.NecessityId,
                        Reason = n.Reason
                    }).ToList();
            }
        }

        private void ApplyIndustryCodesToDataModel(Job job)
        {
            if (IndustryCodes == null || IndustryCodes.Count == 0)
            {
                job.IndustryCodes = null;
            }
            else
            {
                job.IndustryCodes = IndustryCodes.Select(
                    ic => new JobIndustryCode
                    {
                        CodeType = ic.CodeType,
                        Code = ic.Code,
                        Name = ic.Name
                    }).ToList();
            }
        }
    }
}