﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class JobAssignmentViewModel
    {
        public JobAssignmentViewModel()
        {
            this.EvaluatedJobs = new List<EvaluatedJobViewModel>();
        }

        public JobAssignmentViewModel(string employeeId, string caseId)
            :this(employeeId, caseId, null, null)
        {
            
        }

        public JobAssignmentViewModel(EmployeeJob job, List<JobEvaluationResult> evaluatedJobs)
            : this(null, null, job, evaluatedJobs)
        {
            
        }

        public JobAssignmentViewModel(string employeeId, string caseId, EmployeeJob job, List<JobEvaluationResult> evaluatedJobs)
        {
            this.CaseId = caseId;
            this.EmployeeId = employeeId;
            if (job != null)
                this.Job = new JobViewModel(job.Job);
            
            if(evaluatedJobs != null)
                this.EvaluatedJobs = evaluatedJobs.Select(ej => new EvaluatedJobViewModel(ej)).ToList();
        }


        public JobViewModel Job { get; set; }
        public List<EvaluatedJobViewModel> EvaluatedJobs {get; set;}
        public bool HasValidJobs { get { return this.EvaluatedJobs.Any(ej => ej.MeetsRestrictions); } }
        public string CaseId { get; set;}
        public string EmployeeId { get; set; }
        public string NewJobId { get; set; }
    }
}