﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class JobIndustryCodeViewModel
    {
        public JobIndustryCodeViewModel()
        {

        }

        public JobIndustryCodeViewModel(JobIndustryCode industryCode)
        {
            this.CodeType = industryCode.CodeType;
            this.Name = industryCode.Name;
            this.Code = industryCode.Code;
        }

        [DisplayName("Code Type"), Required]
        public string CodeType { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }
    }
}