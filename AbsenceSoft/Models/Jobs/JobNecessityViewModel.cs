﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class JobNecessityViewModel
    {
        public JobNecessityViewModel()
        {

        }

        public JobNecessityViewModel(JobRestrictedNecessity jobNecessity)
        {
            Id=jobNecessity.Id;
            this.NecessityId = jobNecessity.NecessityId;
            this.Name = jobNecessity.Necessity.Name;
            this.Type = jobNecessity.Necessity.Type;
            this.Reason = jobNecessity.Reason;
        }

        public Guid Id { get; set; }

        [DisplayName("Necessity"), UIHint("Necessity"), Required]
        public string NecessityId { get; set; }

        public string Name { get; set; }

        public NecessityType Type { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Reason { get; set; }

        
    }
}