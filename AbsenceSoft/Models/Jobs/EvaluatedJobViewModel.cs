﻿using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    public class EvaluatedJobViewModel
    {
        public EvaluatedJobViewModel()
        {
            this.Job = new JobViewModel();
            this.Reasons = new List<string>();
        }

        public EvaluatedJobViewModel(JobEvaluationResult evaluation)
            :this()
        {
            this.MeetsRestrictions = evaluation.MeetsRestrictions;
            this.Job = new JobViewModel(evaluation.Job);
            this.Reasons = evaluation.Reasons;
        }

        public bool MeetsRestrictions { get; set; }

        public List<string> Reasons { get; set; }

        public JobViewModel Job { get; set; }
    }
}