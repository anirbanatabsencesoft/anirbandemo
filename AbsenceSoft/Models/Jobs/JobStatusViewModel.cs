﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Jobs
{
    [Serializable]
    public class JobStatusViewModel : IValidatableObject
    {
        /// <summary>
        /// Gets or sets the employee job identifier.
        /// </summary>
        /// <value>
        /// The employee job identifier.
        /// </value>
        public string EmployeeJobId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DisplayName("Status"), UIHint("EnumButtons")]
        public virtual AdjudicationStatus Status { get; set; }

        public virtual string StatusString
        {
            get
            {
                return this.Status.ToString().SplitCamelCaseString();
            }
        }

        /// <summary>
        /// Gets or sets the denial reason.
        /// </summary>
        /// <value>
        /// The denial reason.
        /// </value>
        [DisplayName("Reason"), UIHint("Enum")]
        public virtual JobDenialReason? DenialReason { get; set; }

        /// <summary>
        /// Gets or sets the denial reason other.
        /// </summary>
        /// <value>
        /// The denial reason other.
        /// </value>
        [DisplayName("Other Reason")]
        public virtual string DenialReasonOther { get; set; }

        /// <summary>
        /// Determines whether the specified object is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (this.Status == AdjudicationStatus.Denied && this.DenialReason == null)
                results.Add(new ValidationResult("Denial reason is required", new[] { "DenialReason" }));

            if (this.DenialReason == JobDenialReason.Other && string.IsNullOrWhiteSpace(this.DenialReasonOther))
                results.Add(new ValidationResult("You must specify the other reason for denial", new[] { "DenialReasonOther" }));

            return results;
        }
    }
}