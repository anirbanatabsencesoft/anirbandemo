﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.IpRange
{
    public class IpRangeListViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeListViewModel"/> class.
        /// </summary>
        public IpRangeListViewModel() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeListViewModel"/> class.
        /// </summary>
        /// <param name="ipRange">The necessity.</param>
        public IpRangeListViewModel(List<IpRangeViewModel> ipRanges)
            : this()
        {
            if (ipRanges == null)
                return;

            IpRanges = ipRanges;
        }

        public List<IpRangeViewModel> IpRanges { get; set; }

        public bool WarnYourNotInThisList { get; set; }
    }
}