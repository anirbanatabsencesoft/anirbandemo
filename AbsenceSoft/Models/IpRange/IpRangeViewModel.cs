﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.IpRange
{
    public class IpRangeViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeViewModel"/> class.
        /// </summary>
        public IpRangeViewModel()
        {
            IsEnabled = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IpRangeViewModel"/> class.
        /// </summary>
        /// <param name="ipRange">The necessity.</param>
        public IpRangeViewModel(AbsenceSoft.Data.Security.IpRange ipRange)
            : this()
        {
            if (ipRange == null)
                return;

            IpRangeId = ipRange.Id;
            Name = ipRange.Name;
            Ip1 = ipRange.Ip1;
            Ip2 = ipRange.Ip2;
            Ip3 = ipRange.Ip3;
            Ip41 = ipRange.Ip41;
            Ip42 = ipRange.Ip42;
            CustomerId = ipRange.CustomerId;
            IsEnabled = ipRange.IsEnabled;
        }

        /// <summary>
        /// Gets or sets the necessity ID.
        /// </summary>
        /// <value>
        /// The necessity ID.
        /// </value>
        public string IpRangeId { get; set; }
        [Required(ErrorMessage ="IP address section 1 is required")]
        [Range(0, 255, ErrorMessage = "IP address section 1 must be between 0 and 255")]
        public short? Ip1 { get; set; }
        [Required(ErrorMessage = "IP address section 2 is required")]
        [Range(0, 255, ErrorMessage = "IP address section 2 must be between 0 and 255")]
        public short? Ip2 { get; set; }
        [Required(ErrorMessage = "IP address section 3 is required")]
        [Range(0, 255, ErrorMessage = "IP address section 3 must be between 0 and 255")]
        public short? Ip3 { get; set; }
        [Required(ErrorMessage = "IP address section 4 the range low end is required")]
        [Range(0, 255, ErrorMessage = "IP address section 4 the range low end must be between 0 and 255")]
        public short? Ip41 { get; set; }
        [Required(ErrorMessage = "IP address section 4 the range high end required")]
        [Range(0, 255, ErrorMessage = "IP address section 4 the range high end must be between 0 and 255")]
        public short? Ip42 { get; set; }

        [Display(Name = "Is Enabled?")]
        [Required]
        public bool? IsEnabled { get; set; }

        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required, DisplayName("Name")]
        public string Name { get; set; }
 
    }
}