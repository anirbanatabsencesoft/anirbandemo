﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.IpRange
{
    public class IpRangeConfirmationViewModel
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public string Warning { get; set; }
    }
}