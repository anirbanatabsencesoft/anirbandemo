﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Workflows;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowDesignViewModel : IViewModel, IDataTransformModel<Data.Workflows.Workflow>
    {
        public WorkflowDesignViewModel()
        {

        }

        public WorkflowDesignViewModel(Data.Workflows.Workflow wf)
            :this()
        {
            Id = wf.Id;
            CustomerId = wf.CustomerId;
            EmployerId = wf.EmployerId;
            Name = wf.Name;
            TargetEventType = wf.TargetEventType;
            DesignerData = wf.DesignerData;

        }

        public string CustomerId { get; set; }

        public string DisplayType { get { return "workflow"; } }

        public string EmployerId { get; set; }

        public string Id { get; set; }

        public string DesignerData { get; set; }

        public EventType TargetEventType { get; private set; }

        public string Name { get; private set; }

        public IEnumerable<WorkflowTransitionViewModel> Transitions { get; set; }
        public IEnumerable<WorkflowActivityViewModel> Activities { get; set; }

        public Data.Workflows.Workflow ApplyToDataModel(Data.Workflows.Workflow wf)
        {
            if (wf == null)
                throw new ArgumentNullException("wf", "You can't design a workflow on a non existent workflow");

            wf.DesignerData = DesignerData;
            wf.Transitions = Transitions.Select(t => t.ApplyToDataModel()).ToList();
            wf.Activities = Activities.Select(a => a.ApplyToDataModel()).ToList();

            return wf;
        }

    }
}