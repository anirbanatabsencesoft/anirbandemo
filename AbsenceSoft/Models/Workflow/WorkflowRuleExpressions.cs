﻿using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowRuleExpressions
    {
        public EventType EventType { get; set; }
        public List<WorkflowRuleViewModel> RuleExpressions { get; set; }
        public List<Rule> Rules { get; set; }
        public string EmployerId { get; set; }
    }
}