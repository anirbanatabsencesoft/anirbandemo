﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowCopyModel
    {
        public string Code { get; set; }
        public string NewCode { get; set; }
        public string NewName { get; set; }
        public string NewDescription { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
    }
}