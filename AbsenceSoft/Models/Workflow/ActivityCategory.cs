﻿using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class ActivityCategory
    {
        public string CategoryName { get; set; }
        public List<Activity> Activities { get; set; }
    }
}