﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Workflows;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Web.Attributes;
using System.ComponentModel;
using AbsenceSoft.Web;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Enums;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowViewModel : IViewModel, IDataTransformModel<Data.Workflows.Workflow>, IDisableableViewModel
    {
        public bool IsDisabled { get; set; }

        public WorkflowViewModel()
        {
            Rules = new List<WorkflowRuleViewModel>();
        }

        public WorkflowViewModel(Data.Workflows.Workflow wf)
            : this()
        {
            if (wf == null)
                return;

            Id = wf.Id;
            CustomerId = wf.CustomerId;
            EmployerId = wf.EmployerId;
            Code = wf.Code;
            Name = wf.Name;
            Description = wf.Description;
            TargetEventType = wf.TargetEventType;
            EffectiveDate = wf.EffectiveDate;
            CriteriaName = wf.CriteriaName;
            CriteriaSuccessType = wf.CriteriaSuccessType;
            Order = wf.Order;
            IsDisabled = Suppressed = IsDisabledInContext(wf.Code);
            ClassTypeName = typeof(Data.Workflows.Workflow).Name;
            SetRules(wf);

        }

        /// <summary>
        /// Returns if the Workflow is disabled
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool IsDisabledInContext(string code)
        {
            var currentEmployer = Current.Employer();
            var currentCustomer = Current.Customer();

            bool isReasonDisabled = false;
            if (currentEmployer != null)
            {
                isReasonDisabled = currentEmployer.SuppressWorkflows.Contains(code);
            }

            if (currentCustomer != null && !isReasonDisabled)
            {
                isReasonDisabled = currentCustomer.SuppressWorkflows.Contains(code);
            }

            return isReasonDisabled;
        }

        private void SetRules(Data.Workflows.Workflow wf)
        {
            if (wf.Criteria == null)
                return;

            using (var workflowService = new WorkflowService(Current.Customer(), Current.Employer(), Current.User()))
            {
                Rules = wf.Criteria
                    .Select(c => workflowService.GetExpression(c))
                    .Where(r => r != null)
                    .Select(e => new WorkflowRuleViewModel(e, wf.TargetEventType)).ToList();
            }
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string DisplayType { get { return "workflow"; } }

        [Required, CodeFormat]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        public string ClassTypeName { get; set; }

        public string Description { get; set; }

        [DisplayName("Effective Date")]
        public DateTime? EffectiveDate { get; set; }

        [Required, DisplayName("Event Type")]
        public EventType? TargetEventType { get; set; }

        [DisplayName("Condition Name")]
        public string CriteriaName { get; set; }
        [DisplayName("Success")]
        public RuleGroupSuccessType CriteriaSuccessType { get; set; }

        public int? Order { get; set; }
        public List<WorkflowRuleViewModel> Rules { get; set; }
        public bool Suppressed { get; set; }

        public Data.Workflows.Workflow ApplyToDataModel(Data.Workflows.Workflow workflow)
        {
            if (workflow == null)
                workflow = new Data.Workflows.Workflow();

            workflow.Code = Code;
            workflow.Name = Name;
            workflow.Description = Description;
            workflow.TargetEventType = TargetEventType.Value;
            workflow.EffectiveDate = EffectiveDate;
            workflow.CriteriaName = CriteriaName;
            workflow.CriteriaSuccessType = CriteriaSuccessType;
            workflow.Order = Order;
            workflow.Suppressed = Suppressed;
            ApplyRulesToDataModel(workflow);
            return workflow;
        }

        private void ApplyRulesToDataModel(Data.Workflows.Workflow workflow)
        {
            if (Rules == null || Rules.Count == 0)
            {
                workflow.Criteria = new List<Data.Rules.Rule>(0);
                return;
            }

            workflow.Criteria = new List<Data.Rules.Rule>(Rules.Count);
            using (var workflowService = new WorkflowService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var allExpressions = workflowService.GetExpressionsByEventType(TargetEventType.Value);
                foreach (var ruleExpression in Rules)
                {
                    var matchingExpression = allExpressions
                        .First(rg => rg.Title == ruleExpression.Group).Expressions
                        .FirstOrDefault(r => r.Name == ruleExpression.Name);
                    if (matchingExpression.IsCustom) ruleExpression.Parameters = matchingExpression.Parameters;
                    var appliedRule = ruleExpression.ApplyToDataModel(matchingExpression);
                    var convertedRule = (Data.Rules.Rule)workflowService.GetRule(appliedRule);

                    workflow.Criteria.Add(convertedRule);
                }
            }

        }

        public bool IsWorkflowSuppressedAtCustomerLevel()
        {
            if (Current.Employer() != null)
            {
                var currentCustomer = Current.Customer();
                return (currentCustomer.SuppressWorkflows.Contains(this.Code, StringComparer.InvariantCultureIgnoreCase));
            }
            return false;
        }
    }
}