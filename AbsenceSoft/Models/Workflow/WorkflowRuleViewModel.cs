﻿using AbsenceSoft.Data.Rules;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.Workflows;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowRuleViewModel:RuleViewModel
    {
        public WorkflowRuleViewModel()
        {

        }

        public WorkflowRuleViewModel(RuleExpression rule, EventType eventType)
            :base(rule)
        {
            using (var service = new WorkflowService(Current.Customer(), Current.Employer(), Current.User()))
            {
                Group = base.GetExpressionGroupName(rule, service.GetExpressionsByEventType(eventType));
            }
             
        }

        public WorkflowRuleViewModel(RuleExpression rule, EventType eventType, Employer employer)
            : base(rule)
        {
            using (var service = new WorkflowService(Current.Customer(), employer, Current.User()))
            {
                Group = base.GetExpressionGroupName(rule, service.GetExpressionsByEventType(eventType));
            }
        }
    }
}