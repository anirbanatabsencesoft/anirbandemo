﻿using AbsenceSoft.Data.Questions;
using AbsenceSoft.Data.Workflows;
using AbsenceSoft.Models.Todos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowActivityViewModel:IDataTransformModel<WorkflowActivity>
    {
        public Guid Id { get; set; }

        public string ActivityId { get; set; }

        public string Name { get; set; }

        public RuleGroupViewModel Rules { get; set; }

        public QuestionModel Question { get; set; }


        [JsonExtensionData]
        private IDictionary<string, object> AdditionalData { get; set; }

        public WorkflowActivity ApplyToDataModel(WorkflowActivity activity = null)
        {
            if (activity == null)
                activity = new WorkflowActivity();

            activity.Id = Id;
            activity.ActivityId = ActivityId;
            activity.Name = Name;

            if (Rules != null)
                activity.Rules = Rules.ApplyToDataModel(activity.Rules);
            if (Question != null)
                activity.Question = Question.ApplyToDataModel(activity.Question);


            SetActivityMetadata(activity);
            
            return activity;
        }

        private void SetActivityMetadata(WorkflowActivity activity)
        {
            if (AdditionalData == null)
                return;
            
            foreach (var data in AdditionalData)
            {
                if (data.Value is long && (long)data.Value < int.MaxValue)
                {
                    activity.Metadata.SetRawValue<int>(data.Key, Convert.ToInt32(data.Value));
                }
                else if(data.Value is JArray)
                {
                    List<string> strings = ((JArray)data.Value).Select(jt => jt.ToString()).ToList();
                    string csv = string.Join(",", strings);
                    activity.Metadata.SetRawValue(data.Key, csv);
                }
                else
                {
                    activity.Metadata.SetRawValue(data.Key, data.Value);
                }
            }
        }
    }
}