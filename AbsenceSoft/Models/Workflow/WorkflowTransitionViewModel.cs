﻿using AbsenceSoft.Data.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowTransitionViewModel:IDataTransformModel<Transition>
    {
        public Guid SourceActivityId { get; set; }
        public Guid TargetActivityId { get; set; }
        public string ForOutcome { get; set; }

        public Transition ApplyToDataModel(Transition transition = null)
        {
            if (transition == null)
                transition = new Transition();

            transition.SourceActivityId = SourceActivityId;
            transition.TargetActivityId = TargetActivityId;
            transition.ForOutcome = ForOutcome;

            return transition;
        }
    }
}