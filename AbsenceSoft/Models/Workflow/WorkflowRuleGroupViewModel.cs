﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class WorkflowRuleGroupViewModel:IDataTransformModel<RuleGroup>
    {
        public string Name { get; set; }
        public RuleGroupSuccessType SuccessType { get; set; }
        public List<Rule> Rules { get; set; }

        public RuleGroup ApplyToDataModel(RuleGroup ruleGroup = null)
        {
            if (ruleGroup == null)
                ruleGroup = new RuleGroup();

            ruleGroup.Name = Name;
            ruleGroup.SuccessType = SuccessType;
            ruleGroup.Rules = Rules;

            return ruleGroup;
        }
    }
}