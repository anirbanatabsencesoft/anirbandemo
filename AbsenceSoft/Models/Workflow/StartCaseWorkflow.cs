﻿using AbsenceSoft.Logic.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Workflow
{
    public class StartCaseWorkflow
    {
        public string CaseId { get; set; }
        public string WorkflowId { get; set; }
        public List<WorkflowCriteria> Criteria { get; set; }
    }
}