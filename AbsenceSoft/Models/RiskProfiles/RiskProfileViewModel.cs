﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.RiskProfiles;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.RiskProfiles;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.RiskProfiles
{
    public class RiskProfileViewModel : IViewModel, IDataTransformModel<RiskProfile>
    {
        public RiskProfileViewModel()
        {
            RuleGroups = new List<RiskProfileRuleGroupViewModel>();
        }

        public RiskProfileViewModel(RiskProfile profile)
            : this()
        {
            if (profile == null)
                return;

            Id = profile.Id;
            CustomerId = profile.CustomerId;
            EmployerId = profile.EmployerId;

            Name = profile.Name;
            Code = profile.Code;
            Color = profile.Color;
            Icon = profile.Icon;
            Description = profile.Description;
            Order = profile.Order;

            TargetsCase = (profile.Target & EntityTarget.Case) == EntityTarget.Case;
            TargetsEmployee = (profile.Target & EntityTarget.Employee) == EntityTarget.Employee;
            
        }

        public RiskProfileViewModel(RiskProfile profile, IEnumerable<RuleExpressionGroup> expressionGroups)
            :this(profile)
        {
            SetRuleGroups(profile, expressionGroups);
        }

        private void SetRuleGroups(RiskProfile profile, IEnumerable<RuleExpressionGroup> expressionGroups)
        {
            if (profile.RuleGroups == null)
                return;

            RuleGroups = profile.RuleGroups.Select(rg => new RiskProfileRuleGroupViewModel(rg, expressionGroups)).ToList();
        }

        public string DisplayType { get { return "risk profile"; } }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Color { get; set; }

        public IEnumerable<SelectListItem> Colors
        {
            get
            {
                yield return new SelectListItem() { Text = "Green", Value = "green" };
                yield return new SelectListItem() { Text = "Yellow", Value = "yellow" };
                yield return new SelectListItem() { Text = "Orange", Value = "orange" };
                yield return new SelectListItem() { Text = "Red", Value = "red" };
                yield return new SelectListItem() { Text = "Dark Red", Value = "darkred" };
                yield return new SelectListItem() { Text = "Aqua", Value = "aqua" };
                yield return new SelectListItem() { Text = "Blue", Value = "blue" };
                yield return new SelectListItem() { Text = "Teal", Value = "teal" };
                yield return new SelectListItem() { Text = "Violet", Value = "violet" };
                yield return new SelectListItem() { Text = "Navy", Value = "navy" };
            }
        }

        [Required, CodeFormat]
        public string Code { get; set; }

        [Required]
        public string Icon { get; set; }

        public IEnumerable<SelectListItem> Icons
        {
            get
            {
                yield return new SelectListItem() { Text = "Checkmark", Value = "check" };
                yield return new SelectListItem() { Text = "Ban", Value = "ban" };
                yield return new SelectListItem() { Text = "Warning", Value = "exclamation-circle" };
                yield return new SelectListItem() { Text = "Partial", Value = "adjust" };
                yield return new SelectListItem() { Text = "Flag", Value = "flag" };
                yield return new SelectListItem() { Text = "Minus", Value = "minus-circle" };
                yield return new SelectListItem() { Text = "Stop", Value = "stop-circle" };
            }
        }

        public string Description { get; set; }



        [Required]
        public int? Order { get; set; }

        [DisplayName("Employee")]
        public bool TargetsEmployee { get; set; }

        [DisplayName("Case")]
        public bool TargetsCase { get; set; }

        public List<RiskProfileRuleGroupViewModel> RuleGroups { get; set; }

        public RiskProfile ApplyToDataModel(RiskProfile profile)
        {
            if (profile == null)
                profile = new RiskProfile();


            profile.Name = Name;
            profile.Code = Code;
            profile.Color = Color;
            profile.Icon = Icon;
            profile.Description = Description;
            profile.Target = SetTarget();
            
            if (profile.IsNew)
            {
                using (var riskProfileService = new RiskProfileService(Current.Customer(), Current.Employer(), Current.User()))
                {
                    profile.Order = riskProfileService.GetNextOrder();
                }
            }

            profile.RuleGroups = RuleGroups.Select(r => r.ApplyToDataModel()).ToList();

            return profile;
        }

        private EntityTarget SetTarget()
        {
            EntityTarget target = EntityTarget.None;

            if (TargetsCase)
                target = target | EntityTarget.Case;

            if (TargetsEmployee)
                target = target | EntityTarget.Employee;

            return target;
        }
    }
}