﻿using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.RiskProfiles;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.RiskProfiles
{
    public class RiskProfileRuleViewModel:RuleViewModel
    {
        public RiskProfileRuleViewModel()
            :base()
        {

        }

        public RiskProfileRuleViewModel(RuleExpression rule, IEnumerable<RuleExpressionGroup> ruleExpressionGroup)
            :base(rule)
        {
            Group = GetExpressionGroupName(rule, ruleExpressionGroup);
        }
    }
}