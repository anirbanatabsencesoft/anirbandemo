﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.RiskProfiles
{
    public class UpdateRiskProfilesOrder
    {
        public List<RiskProfileViewModel> RiskProfiles { get; set; }
    }
}