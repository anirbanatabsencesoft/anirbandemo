﻿using AbsenceSoft.Data.Rules;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Logic.RiskProfiles;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.RiskProfiles
{
    public class RiskProfileRuleGroupViewModel:RuleGroupViewModel, IDataTransformModel<RuleGroup>
    {
        public RiskProfileRuleGroupViewModel()
            :base()
        {

        }

        public RiskProfileRuleGroupViewModel(RuleGroup group, IEnumerable<RuleExpressionGroup> expressionGroups)
            :base(group)
        {
            if (group.Rules != null)
            {
                using (var riskProfileService = new RiskProfileService(Current.Customer(), Current.Employer(), Current.User()))
                {
                    Rules = group.Rules
                        .Select(r => riskProfileService.GetExpression(r, expressionGroups))
                        .Where(e => e != null)
                        .Select(e => new RiskProfileRuleViewModel(e, expressionGroups)).ToList();
                }
            }
                
        }

        public List<RiskProfileRuleViewModel> Rules { get; set; }

        public override RuleGroup ApplyToDataModel(RuleGroup entity = null)
        {
            entity = base.ApplyToDataModel(entity);
            if (Rules == null)
                return entity;
            entity.Rules = new List<Rule>();
            using (var riskProfileService = new RiskProfileService(Current.Customer(), Current.Employer(), Current.User()))
            {
                var allExpressions = riskProfileService.GetRuleExpressions();
                foreach (var rule in Rules)
                {
                    var matchingExpression = allExpressions
                        .First(rg => rg.Title == rule.Group).Expressions
                        .FirstOrDefault(r => r.Name == rule.Name);
                    if (matchingExpression.IsCustom) rule.Parameters = matchingExpression.Parameters;
                    var appliedRule = rule.ApplyToDataModel(matchingExpression);
                    var convertedRule = riskProfileService.GetRule(appliedRule);
                    entity.Rules.Add(convertedRule);
                }
            }
            return entity;
        }
    }
}