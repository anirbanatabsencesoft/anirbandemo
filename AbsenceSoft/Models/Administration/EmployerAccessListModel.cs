﻿using System.Collections.Generic;

namespace AbsenceSoft.Models.Administration
{
    public class EmployerAccessListModel
    {
        public EmployerAccessListModel()
        {
            EmployerAccessList = new List<EmployerAccessModel>();
        }

        public List<EmployerAccessModel> EmployerAccessList { get; set; }
    }
}