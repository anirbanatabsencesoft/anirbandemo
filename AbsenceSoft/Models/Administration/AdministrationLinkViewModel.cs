﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    public class AdministrationLinkViewModel
    {
        public AdministrationLinkViewModel()
        {

        }

        public AdministrationLinkViewModel(string linkText, string routeName)
            :this()
        {
            LinkText = linkText;
            RouteName = routeName;
            DisplayForCustomer = true;
            DisplayForEmployer = true;
            LinkUrl = string.Empty;
        }

        public string LinkText { get; set; }
        public string RouteName { get; set; }

        ///The following two booleans are a placeholder gap for admin pages that we aren't refactoring in the first go around
        ///Like policies, and IP ranges

        /// <summary>
        /// Whether this link should be displayed for customers
        /// Default: true
        /// </summary>
        public bool DisplayForCustomer { get; set; }

        /// <summary>
        /// Whether this link should be display for employers
        /// Default: true
        /// </summary>
        public bool DisplayForEmployer { get; set; }

        public string LinkUrl { get; set; }
    }
}