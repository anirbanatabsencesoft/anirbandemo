﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    [Serializable]
    public class EmployerHolidayViewModel : IValidatableObject
    {
        public EmployerHolidayViewModel() { }
        public EmployerHolidayViewModel(Holiday holiday)
        {
            if (holiday == null)
                return;
            Id = holiday.Id;
            Name = holiday.Name;
            StartDate = holiday.StartDate.ToUIString();
            EndDate = holiday.EndDate.ToUIString();
            Date = holiday.Date.ToUIString();
            Month = holiday.Month;
            WeekOfMonth = holiday.WeekOfMonth;
            DayOfMonth = holiday.DayOfMonth;
            DayOfWeek = holiday.DayOfWeek;
            OrClosestWeekday = holiday.OrClosestWeekday;
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Date { get; set; }
        public Month? Month { get; set; }
        public int? DayOfMonth { get; set; }
        public int? WeekOfMonth { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public bool OrClosestWeekday { get; set; }

        public string SpecificDate { get; set; }

        public Holiday ToHoliday(Holiday holiday = null)
        {
            holiday = holiday ?? new Holiday() { Id = Id };
            holiday.Name = Name;
            DateTime dt;
            if (DateTime.TryParse(StartDate, out dt))
                holiday.StartDate = dt.ToMidnight();
            if (DateTime.TryParse(EndDate, out dt))
                holiday.EndDate = dt.ToMidnight();
            if (DateTime.TryParse(Date, out dt))
                holiday.Date = dt.ToMidnight();
            holiday.WeekOfMonth = WeekOfMonth;
            holiday.Month = Month;
            holiday.DayOfMonth = DayOfMonth;
            holiday.DayOfWeek = DayOfWeek;
            holiday.OrClosestWeekday = OrClosestWeekday;
            return holiday;
        }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            var holiday = validationContext.ObjectInstance as EmployerHolidayViewModel;
            if (holiday == null)
                yield break;

            if (holiday.Month.HasValue && holiday.DayOfMonth.HasValue)
                if (holiday.Month.Value == Data.Enums.Month.February && holiday.DayOfMonth.Value == 29)
                    yield return new ValidationResult("Holidays cannot occur on a leap year day", new[] { "Month", "DayOfMonth" });
        }
    }
}