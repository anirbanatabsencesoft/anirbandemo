﻿namespace AbsenceSoft.Models.Administration
{
    public class UserIsAuthenticatedViewModel
    {
        public bool IsUserAuthenticated { get; set; }
        public bool WillExpire { get; set; }
        public long ExpirationTime { get; set; }
        public bool WillExpireSameDayOrWithinNextMeridiem { get; set; }
    }
}