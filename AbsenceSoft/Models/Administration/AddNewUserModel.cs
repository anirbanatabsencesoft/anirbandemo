﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    public class AddNewUserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Error { get; set; }
        public DateTime? EndDate { get; set; }
        public List<RoleModel> ApplyRoles { get; set; }
        public List<string> Employers { get; set; }
        public bool AutoAssignCases { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string AssignedEmployeeId { get; set; }
        public string AssignedEmployeeEmployerId { get; set; }
        public bool IsSelfServiceUser { get; set; }

        public NotificationFrequencyType? ToDoNotificationFrequency { get; set; }
    }
}