﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Web;

namespace AbsenceSoft.Models.Administration
{
    public class CustomerViewModel : IDataTransformModel<Customer>
    {
        public CustomerViewModel()
        {

        }

        public CustomerViewModel(Customer customer)
            : this()
        {
            var administrationService = new AdministrationService(Current.User());
            UserPermissionsList = administrationService.GetUserPermissionsList();
            
            Name = customer.Name;
            Url = customer.Url;
            LogoId = customer.LogoId;

            ShowIsWorkRelated = customer.Metadata.GetRawValue<bool>("ShowIsWorkRelated");
            ShowContactSection = customer.Metadata.GetRawValue<bool>("ShowContactSection");
            ShowOtherQuestions = customer.Metadata.GetRawValue<bool>("ShowOtherQuestions");

            DefaultNoteToConfidential = customer.DefaultNoteToConfidential;
            DefaultCommunicationToConfidential = customer.DefaultCommunicationToConfidential;
            DefaultAttachmentToConfidential = customer.DefaultAttachmentToConfidential;
            EmailRepliesType = customer.EmailRepliesType;
            if (customer.Contact != null)
            {
                PhoneNumber = customer.Contact.WorkPhone;
                Fax = customer.Contact.Fax;

                if (customer.Contact.Address != null)
                {
                    var addressInformation = customer.Contact.Address;
                    Address = addressInformation.Address1;
                    Address2 = addressInformation.Address2;
                    City = addressInformation.City;
                    State = addressInformation.State;
                    PostalCode = addressInformation.PostalCode;
                    Country = addressInformation.Country;
                }
            }

            ToDoTemplateCode = customer.ToDoTemplateCode;
            NewUserEmailTemplateCode = customer.NewUserEmailTemplateCode;
        }

        [Required]
        public string Name { get; set; }
        public string Url { get; set; }

        [Required]
        public string Address { get; set; }
        public string Address2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Fax { get; set; }

        public string LogoId { get; set; }

        [DisplayName("Show Is Work Related Section")]
        public bool ShowIsWorkRelated { get; set; }

        [DisplayName("Show Contact Section")]
        public bool ShowContactSection { get; set; }

        [DisplayName("Show Other Questions")]
        public bool ShowOtherQuestions { get; set; }

        [DisplayName("Default Note To Confidential")]
        public bool DefaultNoteToConfidential { get; set; }

        [DisplayName("Default Communication To Confidential")]
        public bool DefaultCommunicationToConfidential { get; set; }

        [DisplayName("Default Attachment To Confidential")]
        public bool DefaultAttachmentToConfidential { get; set; }
        

        [DisplayName("Communication Email Replies go to")]
        public EmailReplies? EmailRepliesType { get; set; }

        [DisplayName("ToDo Notification Template")]
        public string ToDoTemplateCode { get; set; }

        [DisplayName("New User Email Template")]
        public string NewUserEmailTemplateCode { get; set; }

        public List<string> UserPermissionsList { get; set; }

        public Customer ApplyToDataModel(Customer entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
                
            entity.Metadata.SetRawValue("ShowIsWorkRelated", ShowIsWorkRelated);
            entity.Metadata.SetRawValue("ShowContactSection", ShowContactSection);
            entity.Metadata.SetRawValue("ShowOtherQuestions", ShowOtherQuestions);

            entity.NewUserEmailTemplateCode = NewUserEmailTemplateCode;
            entity.ToDoTemplateCode = ToDoTemplateCode;
            entity.DefaultNoteToConfidential = DefaultNoteToConfidential;
            entity.DefaultCommunicationToConfidential = DefaultCommunicationToConfidential;
            entity.DefaultAttachmentToConfidential = DefaultAttachmentToConfidential;
            entity.EmailRepliesType = EmailRepliesType;
            entity.Name = Name;
            entity.LogoId = LogoId;

            if (entity.Contact == null)
            {
                entity.Contact = new Contact();
            }
                
            entity.Contact.WorkPhone = PhoneNumber;
            entity.Contact.Fax = Fax;

            if (entity.Contact.Address == null)
            {
                entity.Contact.Address = new Address();
            }

            entity.Contact.Address.Address1 = Address;
            entity.Contact.Address.Address2 = Address2;
            entity.Contact.Address.Country = Country;
            entity.Contact.Address.City = City;
            entity.Contact.Address.State = State;
            entity.Contact.Address.PostalCode = PostalCode;

            return entity;
        }
    }
}