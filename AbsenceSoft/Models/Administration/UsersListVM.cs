﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    public class UsersListVM
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsLoggedInUser { get; set; }
        public string Error { get; set; }
        public string DisplayName { get; set; }
        public List<string> Roles { get; set; }
        public List<string> RolesName { get; set; }
    }
}