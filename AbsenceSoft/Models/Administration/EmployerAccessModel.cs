﻿using System.Collections.Generic;

namespace AbsenceSoft.Models.Administration
{
    public class EmployerAccessModel
    {
        public EmployerAccessModel()
        {
            Roles = new List<string>();
        }

        public string UserId { get; set; }

        public string EmployerId { get; set; }

        public string Error { get; set; }

        public bool AutoAssignCases { get; set; }

        public List<string> Roles { get; set; }

        public List<string> OrgCodes { get; set; }
    }
}