﻿using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    public class EmployerViewModel : IValidatableObject
    {
        public EmployerViewModel()
        {
            IsNew = true;
        }

        public EmployerViewModel(Employer employer)
            : this()
        {
            if (employer == null)
                return;

            CopyBasicInformation(employer);
            if (employer.Contact != null)
            {
                CopyContactInformation(employer.Contact);
            }

            if (Current.Customer().HasFeature(Feature.EmployerServiceOptions))
            {
                using (var serviceOptionService = new EmployerServiceOptionService(Current.Customer(), Current.Employer(), Current.User()))
                {
                    CustomerServiceOptionId = serviceOptionService.GetCustomerServiceOptionId(employer.Id);

                    if (!string.IsNullOrEmpty(CustomerServiceOptionId))
                    {
                        using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
                        {
                            var customerServiceOption = customerServiceOptionService.GetCustomerServiceOptionById("ServiceOptionIndicator", CustomerServiceOptionId);
                            if (customerServiceOption != null)
                                CustomerServiceOption = customerServiceOption.Value ?? string.Empty;
                        }
                    }
                }
            }

        }

        private void CopyBasicInformation(Employer employer)
        {
            Id = employer.Id;
            IsNew = employer.IsNew;
            Name = employer.Name;
            Url = employer.Url;
            LogoId = employer.LogoId;
            ReferenceCode = employer.ReferenceCode;
            MaxEmployees = employer.MaxEmployees;
            FMLPeriodType = employer.FMLPeriodType;
            ResetMonth = employer.ResetMonth;
            ResetDayOfMonth = employer.ResetDayOfMonth;
            WeeklyWorkHours = employer.FTWeeklyWorkHours;
            SpouseAtSameEmployer = employer.EnableSpouseAtSameEmployerRule;
            FiftyInSeventyFive = employer.Enable50In75MileRule;
            AllowIntermittentForBirthAdoptionOrFosterCare = employer.AllowIntermittentForBirthAdoptionOrFosterCare;
            IgnoreSystemCalculationsForTotalHoursWorked = employer.IgnoreScheduleForPriorHoursWorked;
            IgnoreAverageMinutesWorkedPerWeek = employer.IgnoreAverageMinutesWorkedPerWeek;
            IsDeleted = employer.IsDeleted;
            EmailRepliesType = employer.EmailRepliesType;
            ToDoTemplateCode = employer.ToDoTemplateCode;
            StartDate = employer.StartDate;
            EndDate = employer.EndDate;
            IsDisabled = employer.IsDisabled;
        }

        private void CopyContactInformation(Contact contact)
        {
            PhoneNumber = contact.WorkPhone;
            Fax = contact.Fax;

            if (contact.Address != null)
            {
                CopyAddressInformation(contact.Address);
            }

        }

        private void CopyAddressInformation(Address address)
        {
            Address = address.Address1;
            Address2 = address.Address2;
            City = address.City;
            State = address.State;
            PostalCode = address.PostalCode;
            Country = address.Country;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            ValidateDates(results);
            ValidatePeriodType(results);
            ValidateReferenceCode(results);
            ValidateName(results);
            ValidateUrl(results);
            return results;
        }

        private void ValidatePeriodType(List<ValidationResult> results)
        {
            if (FMLPeriodType.Value != PeriodType.FixedResetPeriod)
            {
                return;
            }
                

            if (!ResetMonth.HasValue)
            {
                results.Add(new ValidationResult("Reset Month is required when the FML Period Type is Fixed Reset Period", new List<string>() { "ResetMonth" }));
            }

            if (!ResetDayOfMonth.HasValue)
            {
                results.Add(new ValidationResult("Reset Day Of Month is required when the FML Period Type is Fixed Reset Period", new List<string>() { "ResetDayOfMonth" }));
            }
        }

        private void ValidateReferenceCode(List<ValidationResult> results)
        {
            if (string.IsNullOrEmpty(ReferenceCode))
            {
                return;
            }
                

            using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
            {
                if (!employerService.ReferenceCodeIsInUse(ReferenceCode))
                {
                    return;
                }
                    

                results.Add(new ValidationResult("Reference Code is already assigned to an employer", new List<string>() { "ReferenceCode" }));
            }
        }

        private void ValidateName(List<ValidationResult> results)
        {
            using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
            {
                if (!employerService.NameIsInUse(Name))
                {
                    return;
                }
                    

                results.Add(new ValidationResult("Name is already assigned to an employer", new List<string>() { "Name" }));
            }
        }

        private void ValidateUrl(List<ValidationResult> results)
        {
            using (var employerService = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
            {
                if (!employerService.IsSubDomainInUse(Url, Id))
                {
                    return;
                }
                    

                results.Add(new ValidationResult("Subdomain is already in use", new List<string>() { "Url" }));
            }
        }

        private void ValidateDates(List<ValidationResult> results)
        {
            if(StartDate.HasValue && EndDate.HasValue && StartDate > EndDate)
            {
                results.Add(new ValidationResult("Start Date must be before End Date", new List<string>() { "StartDate" }));
            }
        }

        public string Id { get; set; }
        public string LogoId { get; set; }

        [Required]
        public string Name { get; set; }

        [RegularExpression("[a-z\\-]+", ErrorMessage = "You may only use lowercase characters a through z or - in the url.")]
        public string Url { get; set; }

        [Required]
        public string ReferenceCode { get; set; }
        public int? MaxEmployees { get; set; }

        [Required, DisplayName("FML Period Type")]
        public PeriodType? FMLPeriodType { get; set; }
        public Month? ResetMonth { get; set; }
        public int? ResetDayOfMonth { get; set; }

        [Required, DisplayName("Weekly Work Hours")]
        public double? WeeklyWorkHours { get; set; }

        [Required]
        public string Address { get; set; }
        public string Address2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required]
        public string Country { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Fax { get; set; }

        [DisplayName("Spouse At Same Employer Rule?")]
        public bool SpouseAtSameEmployer { get; set; }

        [DisplayName("50 in 75 Rule?")]
        public bool FiftyInSeventyFive { get; set; }

        [DisplayName("Allow Intermittent For Birth, Adoption, Or Foster Care?")]
        public bool AllowIntermittentForBirthAdoptionOrFosterCare { get; set; }

        [DisplayName("Ignore System Calculations For Total Hours Worked in Last 12 Months")]
        public bool IgnoreSystemCalculationsForTotalHoursWorked { get; set; }

        [DisplayName("Ignore System Calculations For Average Minutes Worked Per Week")]
        public bool IgnoreAverageMinutesWorkedPerWeek { get; set; }

        [DisplayName("Communication Email Replies go to")]
        public EmailReplies? EmailRepliesType { get; set; }

        [DisplayName("Service Type")]
        public string CustomerServiceOptionId { get; set; }

        [DisplayName("Employer Service Type")]
        public string CustomerServiceOption { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? EndDate { get; set; }

        public bool IsDisabled { get; set; }

        

        public bool IsDeleted { get; set; }
        public bool IsNew { get; set; }

        [DisplayName("ToDo Notification Template")]
        public string ToDoTemplateCode { get; set; }

        public Employer ApplyToDataModel(Employer employer = null)
        {
            if (employer == null)
                employer = new Employer();

            ApplyBasicInfo(employer);
            employer.Contact = ApplyContactInfo(employer.Contact);

            return employer;
        }

        private void ApplyBasicInfo(Employer employer)
        {
            employer.Name = Name;
            employer.LogoId = LogoId;
            employer.Url = Url;
            employer.ReferenceCode = ReferenceCode;
            employer.MaxEmployees = MaxEmployees;
            employer.FMLPeriodType = FMLPeriodType.Value;
            if (FMLPeriodType.Value == PeriodType.FixedResetPeriod)
            {
                employer.ResetMonth = ResetMonth;
                employer.ResetDayOfMonth = ResetDayOfMonth;
            }

            employer.FTWeeklyWorkHours = WeeklyWorkHours;
            employer.EnableSpouseAtSameEmployerRule = SpouseAtSameEmployer;
            employer.Enable50In75MileRule = FiftyInSeventyFive;
            employer.AllowIntermittentForBirthAdoptionOrFosterCare = AllowIntermittentForBirthAdoptionOrFosterCare;
            employer.IgnoreScheduleForPriorHoursWorked = IgnoreSystemCalculationsForTotalHoursWorked;
            employer.IgnoreAverageMinutesWorkedPerWeek = IgnoreAverageMinutesWorkedPerWeek;
            employer.ReferenceCode = ReferenceCode;
            employer.EmailRepliesType = EmailRepliesType;
            employer.ToDoTemplateCode = ToDoTemplateCode;
            employer.EndDate = EndDate;
            employer.StartDate = StartDate;
        }

        private Contact ApplyContactInfo(Contact contact)
        {
            if (contact == null)
                contact = new Contact();

            contact.WorkPhone = PhoneNumber;
            contact.Fax = Fax;
            contact.Address = ApplyAddressInfo(contact.Address);
            return contact;
        }

        private Address ApplyAddressInfo(Address address)
        {
            if (address == null)
                address = new Address();

            address.Address1 = Address;
            address.Address2 = Address2;
            address.City = City;
            address.State = State;
            address.PostalCode = PostalCode;
            address.Country = Country;

            return address;
        }
    }
}