﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Administration
{
    public class SSOProfileModel
    {
        
        public string CustomerId { get; set; }
        [AllowHtml]
        public string Message { get; set; }

    }
}