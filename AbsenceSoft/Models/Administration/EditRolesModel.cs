﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
	[Serializable]
	public class EditRolesModel
	{
		[Required(AllowEmptyStrings = false)]
		public string CustomerId { get; set; }

		public string RoleId { get; set; }

		[Required(AllowEmptyStrings =false)]
		public string RoleName { get; set; }

		public List<PermissionModel> Permissions { get; set; }

		public bool Success { get; set; }

		public List<string> ContactTypes { get; set; }

		public bool RevokeAccessIfNoContactsOfTypes { get; set; }

		public RoleType Type { get; set; }

		//All items for display
		public List<RoleModel> AllRoles { get; set; }

		public List<PermissionModel> AllPermissions { get; set; }

		public List<PermissionCategoryModel> AllCategories { get; set; }

		public bool IsESSDataVisibilityInPortalFeatureEnabled { get; set; }

		/// <summary>
		/// Gets or sets the always assign.
		/// </summary>
		/// <value>
		/// The AlwaysAssign.
		/// </value>
		public bool AlwaysAssign { get; set; }

		/// <summary>
		/// Gets or sets the default role.
		/// </summary>
		/// <value>
		/// The DefaultRole.
		/// </value>
		public bool DefaultRole { get; set; }
	}

	[Serializable]
	public class RoleModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public bool Apply { get; set; }
		public RoleType Type { get; set; }

		/// <summary>
		/// Gets or sets the always assign.
		/// </summary>
		/// <value>
		/// The AlwaysAssign.
		/// </value>
		public bool AlwaysAssign { get; set; }

		/// <summary>
		/// Gets or sets the default role.
		/// </summary>
		/// <value>
		/// The DefaultRole.
		/// </value>
		public bool DefaultRole { get; set; }
	}

	[Serializable]
	public class PermissionModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Category { get; set; }
		public string HelpText { get; set; }
		public bool Apply { get; set; }
		public RoleType[] RoleTypes { get; set; }
	}

	public class PermissionCategoryModel
	{
		public string Category { get; set; }
		public List<RoleType> RoleTypes { get; set; }
	}
}