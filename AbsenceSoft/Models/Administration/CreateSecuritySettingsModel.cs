﻿using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Administration
{
    public class CreateSecuritySettingsModel
    {
        public CreateSecuritySettingsModel()
        {
            PasswordPolicy = new PasswordPolicy();
            SecuritySettings = new SecuritySettings();
        }

        [Required(ErrorMessage = @"Customer Id must exist and should not be modified")]
        public string CustomerId { get; set; }

        public string Error { get; set; }

        public PasswordPolicy PasswordPolicy { get; set; }

        public SecuritySettings SecuritySettings { get; set; }
    }
}