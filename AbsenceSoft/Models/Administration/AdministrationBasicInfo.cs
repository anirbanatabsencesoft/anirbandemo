﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    public class AdministrationBasicInfo
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CoAddressOne { get; set; }
        public string CoAddressTwo { get; set; }
        public string CoCityStateZip { get; set; }
        public string CoURL { get; set; }

        public List<EmployerInfo> EmployersInfo { get; set; }

        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public bool IsTpaCustomerWithEmployerServiceOptions { get; set; }
    }

    public class EmployerInfo
    {
        public EmployerInfo()
        {

        }

        public EmployerInfo(Employer employer)
        {
            this.EmployerId = employer.Id;
            this.EmployerName = employer.Name;
        }

        public string EmployerId { get; set; }
        public string EmployerName { get; set; }
        public string EmpAddressOne { get; set; }
        public string EmpAddressTwo { get; set; }
        public string EmpCityStateZip { get; set; }
        public string EmpURL { get; set; }
        public string ReferenceCode { get; set; }
        public string CusomerServiceOptionId { get; set; }

    }

}