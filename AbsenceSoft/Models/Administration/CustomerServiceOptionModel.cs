﻿using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AbsenceSoft.Models.Administration
{
    public class CustomerServiceOptionModel : IValidatableObject
    {
        public string Id { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Order must have a positive value.")]
        public int Order { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            ValidateValue(results);
            return results;
        }

        private void ValidateValue(List<ValidationResult> results)
        {
            using (var customerServiceOptionService = new CustomerServiceOptionService(Current.User()))
            {
                if (customerServiceOptionService.GetCustomerServiceOptions().FirstOrDefault(m => m.Value == Value) == null)
                    return;

                results.Add(new ValidationResult("Service type with same value already exist, please provide a different name", new List<string>() { "Value" }));
            }
        }
    }
}