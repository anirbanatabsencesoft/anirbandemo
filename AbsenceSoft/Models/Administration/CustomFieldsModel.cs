﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Administration
{
    [Serializable]
    public class CustomFieldsModel
    {
        public List<EmployerVM> Employers { get; set; }
    }

    [Serializable]
    public class EmployerVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<CustomFieldModel> Fields { get; set; }
    }

    [Serializable]
    public class CustomFieldModel
    {
        public string EmployerId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string HelpText { get; set; }
        public int DataType { get; set; }
        public int Page { get; set; }
        public int ValueType { get; set; }
        public List<ListItem> ListValues { get; set; }
        public int? FieldOrder { get; set; }
        public bool IsRequired { get; set; }
        public bool IsCollectedAtIntake { get; set; }
        public int Target { get; set; }
    }

    [Serializable]
    public class CustomFieldAddEditModel
    {
        public string[] EmployerId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string HelpText { get; set; }
        public int DataType { get; set; }
        public int Page { get; set; }
        public int ValueType { get; set; }
        public List<ListItem> ListValues { get; set; }
        public int? FieldOrder { get; set; }
        public bool IsRequired { get; set; }
        public bool IsCollectedAtIntake { get; set; }
        public int Target { get; set; }
    }

    [Serializable]
    public class ListItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}