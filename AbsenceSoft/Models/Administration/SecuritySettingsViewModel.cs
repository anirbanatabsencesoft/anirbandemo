﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Administration
{
    public class SecuritySettingsViewModel:IDataTransformModel<Customer>
    {
        public SecuritySettingsViewModel()
        {

        }

        public SecuritySettingsViewModel(Customer customer)
            :this()
        {
            if (customer.SecuritySettings != null)
            {
                if (customer.SecuritySettings.ShowInactiveLogoutPrompt.HasValue)
                    ShowInactiveLogoutPrompt = customer.SecuritySettings.ShowInactiveLogoutPrompt.Value;

                if (customer.SecuritySettings.ShowSelfServiceInactiveLogoutPrompt.HasValue)
                    ShowSelfServiceInactiveLogoutPrompt = customer.SecuritySettings.ShowSelfServiceInactiveLogoutPrompt.Value;

                InactiveLogoutPeriod = customer.SecuritySettings.InactiveLogoutPeriod;
                SelfServiceInactiveLogoutPeriod = customer.SecuritySettings.SelfServiceInactiveLogoutPeriod;
                DisableLoginPage = customer.SecuritySettings.DisableLoginPage;
                DisableESSLoginPage = customer.SecuritySettings.DisableESSLoginPage;
                DisabledLoginPageText = customer.SecuritySettings.DisabledLoginPageText;
                EnableESSSelfRegistration = customer.SecuritySettings.EnableSelfServiceRegistration;
            }

            if (customer.PasswordPolicy != null)
            {
                ExpirationDays = customer.PasswordPolicy.ExpirationDays;
                MinimumLength = customer.PasswordPolicy.MinimumLength;
                ReuseLimitCount = customer.PasswordPolicy.ReuseLimitCount;
                ReuseLimitDays = customer.PasswordPolicy.ReuseLimitDays;
            }

            EnableAllEmployersByDefault = customer.EnableAllEmployersByDefault;
            
        }

        [DisplayName("Prompt users before logging them out?")]
        public bool ShowInactiveLogoutPrompt { get; set; }

        [DisplayName("Logout period")]
        [Range(typeof(int), "10","10080")]
        public int? InactiveLogoutPeriod { get; set; }

        [DisplayName("Prompt users before logging them out of the self-service section?")]
        public bool ShowSelfServiceInactiveLogoutPrompt { get; set; }

        [DisplayName("Self-service Logout Period")]
        [Range(typeof(int), "10", "10080")]
        public int? SelfServiceInactiveLogoutPeriod { get; set; }

        [DisplayName("Expiration")]
        public int ExpirationDays { get; set; }

        [DisplayName("Enable All Employers By Default")]
        public bool EnableAllEmployersByDefault { get; set; }

        [DisplayName("Disable Login Page")]
        public bool DisableLoginPage { get; set; }

        [DisplayName("Disable ESS Login Page")]
        public bool DisableESSLoginPage { get; set; }

        [DisplayName("Enable ESS Self Registration")]
        public bool EnableESSSelfRegistration { get; set; }

        [DisplayName("Disabled Login Message")]
        public string DisabledLoginPageText { get; set; }


        public IEnumerable<SelectListItem> AvailableExpirationDays
        {
            get
            {
                yield return new SelectListItem()
                {
                    Text = "Never",
                    Value = "0"
                };

                yield return new SelectListItem()
                {
                    Text = "30 Days",
                    Value = "30"
                };

                yield return new SelectListItem()
                {
                    Text = "60 Days",
                    Value = "60"
                };

                yield return new SelectListItem()
                {
                    Text = "90 Days",
                    Value = "90"
                };
            }
        }

        [DisplayName("Minimum Length")]
        public int MinimumLength { get; set; }

        [DisplayName("Reuse Limit (Days)")]
        public int ReuseLimitDays { get; set; }



        [DisplayName("Reuse Limite (Recent Passwords")]
        public int ReuseLimitCount { get; set; }

        public Customer ApplyToDataModel(Customer customer)
        {
            customer.EnableAllEmployersByDefault = EnableAllEmployersByDefault;

            if (customer.SecuritySettings == null)
                customer.SecuritySettings = new SecuritySettings();

            customer.SecuritySettings.ShowInactiveLogoutPrompt = ShowInactiveLogoutPrompt;
            customer.SecuritySettings.InactiveLogoutPeriod = InactiveLogoutPeriod;
            customer.SecuritySettings.ShowSelfServiceInactiveLogoutPrompt = ShowSelfServiceInactiveLogoutPrompt;
            customer.SecuritySettings.SelfServiceInactiveLogoutPeriod = SelfServiceInactiveLogoutPeriod;
            customer.SecuritySettings.DisableLoginPage = DisableLoginPage;
            customer.SecuritySettings.DisableESSLoginPage = DisableESSLoginPage;
            customer.SecuritySettings.DisabledLoginPageText = DisabledLoginPageText;
            customer.SecuritySettings.EnableSelfServiceRegistration = EnableESSSelfRegistration;

            if (customer.PasswordPolicy == null)
                customer.PasswordPolicy = new PasswordPolicy();

            customer.PasswordPolicy.ExpirationDays = ExpirationDays;
            customer.PasswordPolicy.MinimumLength = MinimumLength;
            customer.PasswordPolicy.ReuseLimitDays = ReuseLimitDays;
            customer.PasswordPolicy.ReuseLimitCount = ReuseLimitCount;


            
            return customer;
        }
    }
}