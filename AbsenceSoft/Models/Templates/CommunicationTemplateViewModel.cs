﻿using AbsenceSoft.Data.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Logic;
using AbsenceSoft.Data.Communications;
using System.Collections.Generic;
using System.ComponentModel;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.Models.Templates
{
    public class CommunicationTemplateViewModel:IViewModel, IDataTransformModel<Template>, IDisableableViewModel
    {
        public CommunicationTemplateViewModel() { }

        public CommunicationTemplateViewModel(Template template)
        {
            if (template == null)
                return;

            Id = template.Id;
            CustomerId = template.CustomerId;
            EmployerId = template.EmployerId;
            Code = template.Code;
            Name = template.Name;
            Subject = template.Subject;
            Message = template.Message;
            DocType = template.DocType;
            DocumentId = template.DocumentId;
            DocumentName = template.DocumentName;
            AttachLatestIncompletePaperwork = template.AttachLatestIncompletePaperwork;
            IsLOASpecific = template.IsLOASpecific;
            Paperwork = template.PaperworkCodes;
            Suppressed = template.Suppressed;
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }
        public string ClassTypeName { get; set; } = "Template";
        [Required]
        public string Name { get; set; }
        public string Subject { get; set; }

        //[StringLength(160, ErrorMessage = "Please enter maximum 160 characters")]
        public string Message { get; set; }

        [DisplayName("Document Type")]
        public DocumentType DocType { get; set; }

        public string DocumentId { get; set; }
        public string DocumentName { get; set; }

        [DisplayName("Attach Latest Incomplete Paperwork?")]
        public bool AttachLatestIncompletePaperwork { get; set; }

        [DisplayName("Is Leave of Absence Specific?")]
        public bool IsLOASpecific { get; set; }

        public List<string> Paperwork { get; set; }

        public string DisplayType { get { return "communication"; } }

        [DisplayName("Enable Template?")]
        public bool Suppressed { get; set; }
      
        public Template ApplyToDataModel(Template template)
        {
            if (template == null)
                template = new Template();

            template.Name = Name;
            template.Subject = Subject;
            template.Message = Message;
            template.Code = Code;
            template.AttachLatestIncompletePaperwork = AttachLatestIncompletePaperwork;
            template.IsLOASpecific = IsLOASpecific;

            template.DocType = DocType;
            template.DocumentId = DocumentId;
            template.DocumentName = DocumentName;
            template.PaperworkCodes = Paperwork;
            template.Suppressed = Suppressed;

            return template;
        }
    }
}