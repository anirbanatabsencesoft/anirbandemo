﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Templates
{
    public class PaperworkTemplateCriteriaViewModel
    {
        public PaperworkTemplateCriteriaViewModel()
        {

        }

        public PaperworkTemplateCriteriaViewModel(PaperworkCriteria criteria)
        {
            if (criteria == null)
                return;

            Criteria = criteria.Criteria;
            CriteriaType = criteria.CriteriaType;
        }

        [Required]
        public string Criteria { get; set; }

        public PaperworkCriteriaType CriteriaType { get; set; }

        public PaperworkCriteria ApplyToDataModel(PaperworkCriteria criteria = null)
        {
            if (criteria == null)
                criteria = new PaperworkCriteria();

            criteria.Criteria = Criteria;
            criteria.CriteriaType = CriteriaType;

            return criteria;
        }
    }
}