﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Templates
{
    public class PaperworkFieldViewModel : IDataTransformModel<PaperworkField>
    {
        public PaperworkFieldViewModel()
        {

        }

        public PaperworkFieldViewModel(PaperworkField field)
            :this()
        {
            if (field == null)
                return;

            Name = field.Name;
            Type = field.Type;
            Required = field.Required;
            Order = field.Order;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public PaperworkFieldType? Type { get; set; }

        public bool Required { get; set; }

        public int? Order { get; set; }

        public PaperworkField ApplyToDataModel(PaperworkField entity = null)
        {
            if (entity == null)
                entity = new PaperworkField();

            entity.Name = Name;
            entity.Type = Type.Value;
            entity.Required = Required;
            entity.Order = Order;

            return entity;
        }
    }
}