﻿using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Templates
{
    public class PaperworkTemplateCriteriaGroupViewModel
    {
        public PaperworkTemplateCriteriaGroupViewModel()
        {

        }

        public PaperworkTemplateCriteriaGroupViewModel(PaperworkCriteriaGroup criteriaGroup)
        {
            if (criteriaGroup == null)
                return;

            Id = criteriaGroup.Id;
            Name = criteriaGroup.Name;
            SuccessType = criteriaGroup.SuccessType;

            if (criteriaGroup.Criteria != null)
                Criteria = criteriaGroup.Criteria.Select(c => new PaperworkTemplateCriteriaViewModel(c)).ToList();
        }

        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public RuleGroupSuccessType SuccessType { get; set; }

        public List<PaperworkTemplateCriteriaViewModel> Criteria { get; set; }

        public PaperworkCriteriaGroup ApplyToDataModel(PaperworkCriteriaGroup group = null)
        {
            if (group == null)
                group = new PaperworkCriteriaGroup();

            group.Name = Name;
            group.SuccessType = SuccessType;

            if(Criteria == null || Criteria.Count == 0)
            {
                group.Criteria = new List<PaperworkCriteria>();
            }
            else
            {
                group.Criteria = Criteria.Select(c => c.ApplyToDataModel()).ToList();
            }

            return group;
        }
    }
}