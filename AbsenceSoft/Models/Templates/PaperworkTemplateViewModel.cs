﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Logic;
using AbsenceSoft.Data.Communications;
using System.ComponentModel;
using AbsenceSoft.Web.Attributes;

namespace AbsenceSoft.Models.Templates
{
    public class PaperworkTemplateViewModel : IViewModel, IDataTransformModel<Paperwork>
    {
        public PaperworkTemplateViewModel()
        {
            Criteria = new List<PaperworkTemplateCriteriaGroupViewModel>();
            Fields = new List<PaperworkFieldViewModel>();
        }

        public PaperworkTemplateViewModel(Paperwork paperwork)
            : this()
        {
            if (paperwork == null)
                return;

            Id = paperwork.Id;
            CustomerId = paperwork.CustomerId;
            EmployerId = paperwork.EmployerId;
            Name = paperwork.Name;
            Code = paperwork.Code;
            FileId = paperwork.FileId;
            FileName = paperwork.FileName;
            DocType = paperwork.DocType;
            Description = paperwork.Description;
            EnclosureText = paperwork.EnclosureText;
            RequiresReview = paperwork.RequiresReview;
            IsForApprovalDecision = paperwork.IsForApprovalDecision;
            ReturnDateAdjustment = paperwork.ReturnDateAdjustmentDays;

            if (paperwork.Criteria != null)
                Criteria = paperwork.Criteria.Select(c => new PaperworkTemplateCriteriaGroupViewModel(c)).ToList();

            if (paperwork.Fields != null)
                Fields = paperwork.Fields.Select(f => new PaperworkFieldViewModel(f)).ToList();

        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string DisplayType { get { return "paperwork"; } }

        [Required]
        public string Name { get; set; }

        [Required, CodeFormat]
        public string Code { get; set; }

        public string Description { get; set; }

        [DisplayName("Enclosure Text")]
        public string EnclosureText { get; set; }

        [DisplayName("Requires Review?")]
        public bool RequiresReview { get; set; }

        [DisplayName("Is For Approval Decision?")]
        public bool IsForApprovalDecision { get; set; }
        

        [DisplayName("Return Date Adjustment")]
        public int? ReturnDateAdjustment { get; set; }

        public string FileId { get; set; }

        [DisplayName("File Name")]
        public string FileName { get; set; }

        [DisplayName("Document Type")]
        public DocumentType DocType { get; set; }

        public List<PaperworkTemplateCriteriaGroupViewModel> Criteria { get; set; }

        public List<PaperworkFieldViewModel> Fields { get; set; }

        public Paperwork ApplyToDataModel(Paperwork paperwork)
        {
            if (paperwork == null)
                paperwork = new Paperwork();

            paperwork.Name = Name;
            paperwork.Code = Code;
            paperwork.Description = Description;
            paperwork.EnclosureText = EnclosureText;
            paperwork.RequiresReview = RequiresReview;
            paperwork.IsForApprovalDecision = IsForApprovalDecision;
            paperwork.ReturnDateAdjustmentDays = ReturnDateAdjustment;

            paperwork.DocType = DocType;
            paperwork.FileId = FileId;
            paperwork.FileName = FileName;

            ApplyCriteriaToDataModel(paperwork);
            ApplyFieldsToDataModel(paperwork);

            return paperwork;
        }

        /// <summary>
        /// Replaces the current criteria with the criteria that came back from the UI
        /// </summary>
        /// <param name="paperwork"></param>
        private void ApplyCriteriaToDataModel(Paperwork paperwork)
        {
            if(Criteria == null || Criteria.Count == 0)
            {
                paperwork.Criteria = new List<PaperworkCriteriaGroup>(0);
                return;
            }

            paperwork.Criteria = Criteria.Select(c => new PaperworkCriteriaGroup()
            {
                Name = c.Name,
                SuccessType = c.SuccessType,
                Criteria = c.Criteria != null ? c.Criteria.Select(cc => cc.ApplyToDataModel()).ToList() : new List<PaperworkCriteria>()
            }).ToList();

        }

        private void ApplyFieldsToDataModel(Paperwork paperwork)
        {
            if(Fields == null || Fields.Count == 0)
            {
                paperwork.Fields = new List<PaperworkField>();
                return;
            }

            paperwork.Fields = Fields.Select(f => f.ApplyToDataModel()).ToList();
        }
    }
}