﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Organization
{
    public class OrganizationViewModel
    {
        public OrganizationViewModel()
        {

        }

        public OrganizationViewModel(AbsenceSoft.Data.Customers.Organization org)
            : base()
        {
            this.Id = org.Id;
            this.Name = org.Name;
            this.EmployerId = org.EmployerId;
            this.Code = org.Code;
            this.TypeCode = org.TypeCode;
            this.Description = org.Description;
            this.SicCode = this.SicCode;
            this.NaicsCode = this.NaicsCode;
            this.Address1 = org.Address.Address1;
            this.Address2 = org.Address.Address2;
            this.City = org.Address.City;
            this.PostalCode = org.Address.PostalCode;
            this.State = org.Address.State;
            this.Country = org.Address.Country;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string EmployerId { get; set; }
        
        public string TypeCode { get; set; }

        public string ParentCode { get; set; }

        /// <summary>
        /// Gets or sets the sic code.
        /// </summary>
        /// <value>
        /// The sic code.
        /// </value>
        public string SicCode { get; set; }

        /// <summary>
        /// Gets or sets the naics code.
        /// </summary>
        /// <value>
        /// The naics code.
        /// </value>
        public string NaicsCode { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }

       
        public AbsenceSoft.Data.Customers.Organization ApplyToDataModel(AbsenceSoft.Data.Customers.Organization org)
        {
            org.Name = this.Name;
            org.CustomerId = Customer.Current.Id;
            org.EmployerId = this.EmployerId;
            org.Description = this.Description;
            org.Code = this.Code;
            org.SicCode = this.SicCode;
            org.NaicsCode = this.NaicsCode;
            org.Address.Address1 = this.Address1;
            org.Address.Address2 = this.Address2;
            org.Address.City = this.City;
            org.Address.PostalCode = this.PostalCode;
            org.Address.State = this.State;
            org.Address.Country = this.Country;

            if (!string.IsNullOrWhiteSpace(ParentCode) && ParentCode != "-1" && ParentCode.ToLower().Trim()!=Code.ToLower().Trim())
            {
                HierarchyPath parentPath = AbsenceSoft.Data.Customers.Organization.GetByCode(ParentCode, Customer.Current.Id, this.EmployerId).Path;
                org.Path = parentPath.AddDescendant(this.Code);
            }
            else
                org.Path = HierarchyPath.Root(this.Code);
            org.TypeCode = TypeCode;

            return org;
        }

        
    }
}