﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Organization
{
    public class OrganizationTypeViewModel
    {
        public OrganizationTypeViewModel()
        {

        }

        public OrganizationTypeViewModel(AbsenceSoft.Data.Customers.OrganizationType org)
            : base()
        {
            this.Id = org.Id;
            this.Name = org.Name;
            this.EmployerId = org.EmployerId;
            this.Code = org.Code;            
            this.Description = org.Description;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string EmployerId { get; set; }
        
        public string TypeCode { get; set; }
        

        public AbsenceSoft.Data.Customers.OrganizationType ApplyToDataModel(AbsenceSoft.Data.Customers.OrganizationType orgType)
        {
            orgType.Name = this.Name;
            orgType.CustomerId = Customer.Current.Id;
            orgType.EmployerId = this.EmployerId;
            orgType.Description = this.Description;
            orgType.Code = this.Code;            
            return orgType;
        }

        
    }
}