﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Customers;

namespace AbsenceSoft.Models.Organization
{
    public class OrganizationVisibilityViewModel
    {
        public List<OrganizationVisibilityTree> OrganizationVisibilityTrees { get; set; }        
    }
}