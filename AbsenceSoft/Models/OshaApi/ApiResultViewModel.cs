﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Logic.WorkRelated;

namespace AbsenceSoft.Models.OshaApi
{
    public class ApiResultViewModel
    {
        public List<SubmissionResponse> Form300AResponses { get; private set; }

        public ApiResultViewModel(List<SubmissionResponse> responses)
        {
            Form300AResponses = responses;
        }
    }
}