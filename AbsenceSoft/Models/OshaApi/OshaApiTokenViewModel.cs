﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models.Organization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.OshaApi
{
    public class OshaApiTokenViewModel
    {
        public OshaApiTokenViewModel()
        {
        
        }
        public OshaApiTokenViewModel(string token)
        {
            ApiToken = token;
        }
        
        public string EmployerId{ get; set; }
        [Required]
        [DisplayName("Bearer Token")]
        public string ApiToken { get; set; }
        public bool IsNew { get; set; }
        public bool Saved { get; set; }
    }
}