﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Cases;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AbsenceSoft.Data.Communications;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.WorkRelated;

namespace AbsenceSoft.Models.OshaApi
{
    public class Osha300AFormPreviewModel
    {
        public Osha300AFormPreviewModel()
        {
            Osha300AFormPreviewList = new List<Osha300AFormPreview>();
        }
 
        public List<Osha300AFormPreview> Osha300AFormPreviewList { get; set; }
    }
}