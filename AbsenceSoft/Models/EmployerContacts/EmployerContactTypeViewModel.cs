﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.EmployerContacts
{
    public class EmployerContactTypeViewModel
    {
        public EmployerContactTypeViewModel()
        {

        }

        public EmployerContactTypeViewModel(EmployerContactType contactType)
            :this()
        {
            this.Id = contactType.Id;
            this.Name = contactType.Name;
            this.Code = contactType.Code;
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public EmployerContactType ApplyToDataModel(EmployerContactType contactType)
        {
            if (contactType == null)
                contactType = new EmployerContactType();

            contactType.Code = this.Code.ToUpper().Replace(" ", "");
            contactType.Name = this.Name;

            return contactType;
        }
    }
}