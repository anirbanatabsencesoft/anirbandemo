﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.EmployerContacts
{
    public class BulkUploadEmployerContact
    {
        public BulkUploadEmployerContact()
        {
            this.Contacts = new List<ContactViewModel>();
        }

        public List<ContactViewModel> Contacts { get; set; }
        public bool Success { get; set; }
        public bool PartialSuccess { get; set; }
        public string Message { get; set; }
    }
}