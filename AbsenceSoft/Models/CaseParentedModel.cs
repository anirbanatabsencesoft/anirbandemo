﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
	[Serializable]
	public class CaseParentedModel : MultiParentedModelBase
	{
        public String CaseId { get; set; }
        public String Status { get; set; }
        public String CaseNumber { get; set; }
		public String CaseReason { get; set; }
		public String ReasonCode { get; set; }
		public DateTime LeaveStartDate { get; set; }
		public DateTime? LeaveEndDate { get; set; }
		public String EmployeeId { get; set; }
		public String EmployeeFirstName { get; set; }
		public String EmployeeLastName { get; set; }
		public DateTime CaseCreatedDate { get; set; }
		public Address EmployeeAddress { get; set; }
		public string EmployerId { get; set; }
		public string EmployerName { get; set; }
		public bool IsTPA { get; set; }
        public bool? SendECommunication { get; set; }
	}
}