﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AbsenceSoft.Models
{
    public static class StringExtension
    {
        private static readonly Regex EmailExpresion = new Regex(@"^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", RegexOptions.Singleline | RegexOptions.Compiled);
        public static bool IsEmail(this string target)
        {
            return !string.IsNullOrWhiteSpace(target) && EmailExpresion.IsMatch(target);
        }
    }
}