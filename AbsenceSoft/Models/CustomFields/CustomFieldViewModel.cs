﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.CustomFields
{
    public class CustomFieldViewModel : IViewModel, IDataTransformModel<CustomField>
    {
        public CustomFieldViewModel()
        {
            
        }

        public CustomFieldViewModel(CustomField customField)
            : this()
        {
            if (customField == null)
                return;

            Id = customField.Id;
            CustomerId = customField.CustomerId;
            EmployerId = customField.EmployerId;
            Code = customField.Code;
            Name = customField.Name;
            Description = customField.Description;
            HelpText = customField.HelpText;
            IsCollectedAtIntake = customField.IsCollectedAtIntake;
            IsRequired = customField.IsRequired;
            DataType = customField.DataType;
            ValueType = customField.ValueType;
            FileOrder = customField.FileOrder;
            TargetsEmployee = (customField.Target & EntityTarget.Employee) == EntityTarget.Employee;
            TargetsCase = (customField.Target & EntityTarget.Case) == EntityTarget.Case;
            
            if (customField.ListValues != null)
            {
                Values = string.Join("~ ", customField.ListValues.Select(lv => lv.Value));
                ListValues = customField.ListValues;
            }
            SelectedValue = customField.SelectedValue;
        }

        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string EmployerId { get; set; }
        public string DisplayType { get { return "custom field"; } }


        private string _code;
        [Required, CodeFormat]
        public string Code { get => this._code;  set => this._code = value.Trim();  }


        private string _name;
        [Required]
        public string Name { get => this._name; set => this._name = value.Trim();  }

        public string Description { get; set; }

        [DisplayName("Help Text")]
        public string HelpText { get; set; }

        [DisplayName("Employee")]
        public bool TargetsEmployee { get; set; }

        [DisplayName("Case")]
        public bool TargetsCase { get; set; }

        [Required, DisplayName("Data Type")]
        public CustomFieldType? DataType { get; set; }

        [Required, DisplayName("Value Type")]
        public CustomFieldValueType? ValueType { get; set; }


        [DisplayName("File Order")]
        public int? FileOrder { get; set; }

        [DisplayName("Is Collected At Intake")]
        public bool IsCollectedAtIntake { get; set; }

        [DisplayName("Is Required")]
        public bool IsRequired { get; set; }

        [DisplayName("Enter Values")]
        public string NewValue { get; set; }

        public List<ListItem> ListValues { get; set; }

        public string Values { get; set; }

        public object SelectedValue  {get;set;}

        public CustomField ApplyToDataModel(CustomField customField)
        {
            if (customField == null)
                customField = new CustomField();

            customField.Code = Code;
            customField.Name = Name;
            customField.Label = Name;
            customField.Description = Description;
            customField.HelpText = HelpText;
            customField.IsCollectedAtIntake = IsCollectedAtIntake;
            customField.IsRequired = IsRequired;
            customField.DataType = DataType.Value;
            customField.ValueType = ValueType.Value;
            customField.FileOrder = FileOrder;
            customField.Target = SetTarget();

            if (string.IsNullOrEmpty(Values))
            {
                customField.ListValues = new List<ListItem>();
            }
            else
            {
                customField.ListValues = Values.Split('~').Select(nv => new ListItem()
                {
                    Key = nv,
                    Value = nv
                }).ToList();
            }

            return customField;
        }

        private EntityTarget SetTarget()
        {
            EntityTarget target = EntityTarget.None;
            if (TargetsCase)
                target = target | EntityTarget.Case;

            if (TargetsEmployee)
                target = target | EntityTarget.Employee;

            return target;
        }
    }
}