﻿
using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Contacts
{
    public class ContactsQuickFindViewModel
    {
        public ContactsQuickFindViewModel()
        {
            children = new List<ContactsChildrenQuickFindViewModel>();
        }

        public ContactsQuickFindViewModel(string contactText)
        {
            text = contactText;
        }

        public string text { get; set; }
        public List<ContactsChildrenQuickFindViewModel> children { get; set; }

        internal void AddEmailChild(string caption, string targetEmail, Employee employee)
        {
            if (string.IsNullOrWhiteSpace(targetEmail))
            {
                return;
            }
            children.AddIfNotExists(new ContactsChildrenQuickFindViewModel
            {
                text = string.Format(
                      "{0} {1} {2} ({3})",
                      employee.FirstName,
                      employee.LastName,                      
                      targetEmail,
                      caption
                ),
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = targetEmail,
                TypeCode = "SELF",
                id = employee.Id
            }, g => g.Email == targetEmail);
        }
    }
    public class ContactsChildrenQuickFindViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string TypeCode { get; set; }

        public string text { get; set; }
        public string id { get; set; }
    }
}