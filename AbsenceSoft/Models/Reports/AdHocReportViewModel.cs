﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using AbsenceSoft.Logic.Reporting;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportViewModel
    {
        public AdHocReportViewModel()
        {
            this.Type = new ReportTypeViewModel();
        }

        public AdHocReportViewModel(ReportDefinition definition)
            :base()
        {
            this.Id = definition.Id;
            this.Name = definition.Name;
            this.Type = new ReportTypeViewModel(definition.ReportType);
            this.Fields = definition.Fields.Select(f => new AdHocReportField(f)).ToList();
            if(definition.Filters != null)
                this.Filters = definition.Filters.Select(f => new AdHocReportFilterViewModel(f)).ToList();
        }

        public AdHocReportViewModel(ReportType type)
        {
            this.Type = new ReportTypeViewModel(type);
        }

        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string JsonReportData { get; set; }
        public ReportTypeViewModel Type { get; set; }

        [Required]
        public List<AdHocReportField> Fields { get; set; }

        public List<AdHocReportFilterViewModel> Filters { get; set; }
        public AdHocReportResultViewModel Results {get; set;}

        public ReportDefinition ApplyToDataModel(ReportDefinition definition, ReportType type)
        {
            if (definition == null)
                definition = new ReportDefinition();

            definition.Fields = this.Fields.Select(f => new SavedReportField(){
              Name = f.Name,
              Order = f.Order
            }).ToList();
                        
            definition.Filters = this.Filters != null ? this.Filters.Select(f => f.ApplyToDataModel()).ToList() : null;
            
            definition.ReportType = type;
            return definition;
        }

        public ReportDefinition CreateReportDefinition()
        {
            using (var reportingService = new AdHocReportingService(Current.User()))
            {
                ReportType type = reportingService.GetReportTypeById(this.Type.Id);
                ReportDefinition definition = reportingService.GetReportById(this.Id);
                return this.ApplyToDataModel(definition, type);
            }
        }
    }
}