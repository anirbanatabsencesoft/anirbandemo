﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportListViewModel
    {
        public string Title { get; set; }
        public string NoReportsMessage { get; set; }
        public int NumberOfColumns { get; set; }
        public List<AdHocReportViewModel> Reports { get; set; }
    }
}