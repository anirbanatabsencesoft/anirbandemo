﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportFiltersViewModel
    {
        public AdHocReportFiltersViewModel()
        {
            this.Categories = new List<AdHocReportCategory>();
            this.Filters = new List<AdHocReportFilterViewModel>();
        }

        public List<AdHocReportCategory> Categories { get; set; }
        public List<AdHocReportFilterViewModel> Filters { get; set; }
    }
}