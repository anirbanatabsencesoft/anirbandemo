﻿using AbsenceSoft.Data.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportResultViewModel
    {
        public AdHocReportResultViewModel()
        {

        }

        public AdHocReportResultViewModel(AdHocReportResult reportResult)
        {
            this.CompletedIn = reportResult.CompletedIn;
            this.Error = reportResult.Error;
            this.RecordCount = reportResult.RecordCount;
            this.Success = reportResult.Success;
            this.Items = reportResult.Items;
        }

        public double? CompletedIn { get; set; }
        public string Error { get; set; }
        public long? RecordCount { get; set; }
        public bool Success { get; set; }
        public List<AdHocResultData> Items { get; set; }
        
    }
}