﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models.Organization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Reports
{
    public class OshaReportViewModel
    {
        public OshaReportViewModel()
        {
            var context = HttpContext.Current;
            var user = context.User as AbsenceSoft.Data.Security.User;
            this.Years = new List<int>();
            this.OfficeLocations = new List<OrganizationViewModel>();

        }

        [Required, DisplayName("SELECT REPORT")]
        public string Form { get; set; }

        /// <summary>
        /// Gets or sets the selected location.
        /// </summary>
        /// <value>
        /// The selected location.
        /// </value>
        [DisplayName("SELECT LOCATION")]
        public List<string> SelectedLocation { get; set; }

        /// <summary>
        /// Gets or sets the selected year.
        /// </summary>
        /// <value>
        /// The selected year.
        /// </value>
        [Required, DisplayName("SELECT YEAR")]
        public List<int> SelectedYear { get; set; }



        [DisplayName("EMPLOYER")]
        public string EmployerId
        {
            get;
            set;
        }



        public string Results { get; set; }

        public IEnumerable<Tuple<string, string>> Forms
        {
            get
            {
                yield return Tuple.Create("300", "Form 300-Log of Work-Related Injuries and Illnesses");
                yield return Tuple.Create("300a", "Form 300a-Summary of Work-Related Injuries and Illnesses");
                yield return Tuple.Create("300aefile", "Form 300a E-File");
            }
        }

        public List<int> Years { get; set; }

        public List<OrganizationViewModel> OfficeLocations { get; set; }

    }
}