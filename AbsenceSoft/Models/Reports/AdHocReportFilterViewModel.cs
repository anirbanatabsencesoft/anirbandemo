﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportFilterViewModel
    {
        public AdHocReportFilterViewModel()
        {

        }

        public AdHocReportFilterViewModel(ReportFilter reportFilter)
            : base()
        {
            this.ControlType = reportFilter.ControlType;
            this.Operator = reportFilter.Operator;
            this.Operators = reportFilter.Operators.Select(o => new SelectListItem()
            {
                Text = o.Value,
                Value = o.Key,
                Selected = reportFilter.Value != null && o.Key == reportFilter.Value.ToString()
            });

            if (reportFilter.Options != null)
            {
                this.Options = reportFilter.Options.Where(s=>s.Text != null && s.Value != null).Select(s => new SelectListItem()
                {
                    Text = s.Text,
                    Value = s.Value.ToString(),
                    Selected = s.Value == reportFilter.Value
                });
            }
            this.FieldName = reportFilter.FieldName;
            if(reportFilter.Value != null)
                this.Value = reportFilter.Value.ToString();
        }

        public string FieldName { get; set; }
        public ControlType ControlType { get; set; }
        public IEnumerable<SelectListItem> Operators { get; set; }
        public IEnumerable<SelectListItem> Options { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }

        public ReportFilter ApplyToDataModel(ReportFilter filter = null)
        {
            if (filter == null)
                filter = new ReportFilter();

            filter.FieldName = this.FieldName;
            filter.ControlType = this.ControlType;
            filter.Operator = this.Operator;
            filter.Value = this.Value;

            return filter;
        }
    }
}