﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class AdHocReportCategory
    {
        public string Name { get; set; }

        public List<AdHocReportField> Fields { get; set; }
    }
}