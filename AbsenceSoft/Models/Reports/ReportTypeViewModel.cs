﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Reports
{
    public class ReportTypeViewModel
    {
        public ReportTypeViewModel()
        {

        }

        public ReportTypeViewModel(ReportType type, bool active = false)
        {
            if (type == null)
                return;

            this.Id = type.Id;
            this.Name = type.Name;
            this.Key = type.Key;
            this.AdHocReportType = type.AdHocReportType;
            this.Active = active;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public AdHocReportType? AdHocReportType {get; set;}
        public bool Active { get; set; }

        public List<AdHocReportCategory> Categories { get; set; }
    }
}