﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Logic.Cases;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.RiskProfiles;
using AbsenceSoft.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class BaseEmployeeModel
    {
        public BaseEmployeeModel()
        {

        }

        public BaseEmployeeModel(AbsenceSoft.Data.Customers.Employee employee)
        {
            this.CustomerId = employee.CustomerId;
            this.Id = employee.Id;
            this.FirstName = employee.FirstName;
            this.LastName = employee.LastName;
            this.EmployeeNumber = employee.EmployeeNumber;
            this.CustomerId = employee.CustomerId;
            this.EmployerId = employee.EmployerId;
            this.EmployerName = employee.EmployerName;
            this.IsTPA = employee.Customer.HasFeature(Feature.MultiEmployerAccess);
            this.IsESS = employee.Customer.HasFeature(Feature.EmployeeSelfService);
            this.Email = employee.Info.Email;
            this.AltEmail = employee.Info.AltEmail;
            this.FirstName = employee.FirstName;
            this.MiddleName = employee.MiddleName;
            this.LastName = employee.LastName;
            this.Gender = employee.Gender;
            this.Address = employee.Info.Address;
            this.AltAddress = employee.Info.AltAddress;
            this.WorkPhone = employee.Info.WorkPhone;
            this.HomePhone = employee.Info.HomePhone;
            this.CellPhone = employee.Info.CellPhone;
            this.AltPhone = employee.Info.AltPhone;
            this.ServiceDate = employee.ServiceDate;
            this.HireDate = employee.HireDate;
            this.RehireDate = employee.RehireDate;
            this.WorkState = employee.WorkState;
            this.WorkCountry = employee.WorkCountry;
            this.WorkCounty = employee.WorkCounty;
            this.WorkCity = employee.WorkCity;
            this.ResidenceCounty = employee.ResidenceCounty;

            if (employee.WorkSchedules != null && employee.WorkSchedules.Count > 0)
            {
                var activeSchedule = employee.WorkSchedules.FirstOrDefault(s => DateTime.UtcNow.DateInRange(s.StartDate, s.EndDate));
                if (activeSchedule == null)
                    activeSchedule = employee.WorkSchedules.OrderBy(d => d.StartDate).FirstOrDefault();
                if (activeSchedule != null)
                {
                    this.WorkSchedule = activeSchedule.ToModel();
                    if (activeSchedule.ScheduleType == ScheduleType.FteVariable)
                    {                        
                        LeaveOfAbsence loa = new LeaveOfAbsence() { Employee = employee, Employer = employee.Employer, Customer = employee.Customer, WorkSchedule = employee.WorkSchedules };
                        activeSchedule.Times = loa.MaterializeSchedule(DateTime.UtcNow.GetFirstDayOfWeek(), DateTime.UtcNow.GetLastDayOfWeek(), policyBehavior: PolicyBehavior.Ignored);
                        WorkSchedule.FteMinutesPerWeek = activeSchedule.FteMinutesPerWeek.ToFriendlyTime();
                        WorkSchedule.FteWeeklyDuration = activeSchedule.FteWeeklyDuration;
                        WorkSchedule.Times = activeSchedule.Times.Select(t => t.ToModel()).ToList();
                    }
                }
            }

            this.StartDayOfWeek = employee.StartDayOfWeek;
            this.WorkSchedule = this.WorkSchedule ?? new ScheduleModel();
            this.WorkSchedules = employee.WorkSchedules == null ? new List<ScheduleModel>() : employee.WorkSchedules.Select(w => w.ToModel()).ToList();
            this.PayType = employee.PayType;
            this.WorkType = employee.EmployeeClassName;
            this.Salary = employee.Salary;
            this.Meets50In75MileRule = employee.Meets50In75MileRule;
            this.OfficeLocation = employee.GetOfficeLocationDisplay();

            if (employee.PriorHours != null)
            {
                // We need to capture the most recent prior hours worked, NOT the last in the array
                //  which has no guaranteed order.
                var prior = employee.PriorHours.OrderBy(p => p.AsOf).LastOrDefault();
                if (prior != null)
                {
                    this.PriorHoursWorked = prior.HoursWorked;
                    this.PriorHoursWorkedAsOf = prior.AsOf;
                }
            }

            this.CalculatedHoursWorked = employee.WorkSchedules == null || !employee.WorkSchedules.Any() ? null : new double?(LeaveOfAbsence.TotalHoursWorkedLast12Months(employee));
            this.Status = employee.Status.ToString();

            if (employee.MinutesWorkedPerWeek != null && employee.MinutesWorkedPerWeek.Any()) 
            {
                var hoursWorkedPerWeek = employee.MinutesWorkedPerWeek.OrderBy(o=>o.AsOf).LastOrDefault();
                if (hoursWorkedPerWeek != null)
                {
                    this.WorkedPerWeek = hoursWorkedPerWeek.MinutesWorked;
                    this.WorkedPerWeekAsOf = hoursWorkedPerWeek.AsOf;
                    this.WorkedPerWeekFriendlyTime = ((int)this.WorkedPerWeek).ToFriendlyTime();
                    this.CalculatedWorkedPerWeekAsOf = this.WorkedPerWeekAsOf;
                }

                this.CalculatedWorkedPerWeek = LeaveOfAbsence.TotalMinutesWorkedPerWeek(employee).ToFriendlyTime();

                if (Current.Employer() != null)
                {
                    IgnoreWorkedPerWeek = Current.Employer().IgnoreAverageMinutesWorkedPerWeek;
                }
              
                if (!this.IgnoreWorkedPerWeek)
                {
                    this.CalculatedWorkedPerWeekAsOf = DateTime.UtcNow;
                }
          
            }

        
       
            this.IsKeyEmployee = employee.IsKeyEmployee;
            this.IsExempt = employee.IsExempt;
            this.DOB = employee.DoB;
            if (employee.Ssn != null)
            {
                this.Ssn = employee.Ssn.PlainText;

                if (employee.Ssn.Encrypted != null)
                {
                    this.Ssn = AbsenceSoft.Common.Security.Crypto.Decrypt(employee.Ssn.Encrypted);
                }
            }
            this.MilitaryStatus = employee.MilitaryStatus;
            if (!string.IsNullOrWhiteSpace(employee.SpouseEmployeeNumber))
            {
                var spouse = AbsenceSoft.Data.Customers.Employee.AsQueryable().Where(e =>
                    e.CustomerId == employee.CustomerId &&
                    e.EmployerId == employee.EmployerId &&
                    e.EmployeeNumber == employee.SpouseEmployeeNumber).FirstOrDefault();
                if (spouse != null)
                {
                    this.SpouseEmployeeId = spouse.Id;
                    this.SpouseEmployeeNumber = spouse.EmployeeNumber;
                    this.SpouseFirstName = spouse.FirstName;
                    this.SpouseLastName = spouse.LastName;
                }
            }

            if (employee.CustomFields == null)
                employee.CustomFields = new List<CustomField>();

            //TODO: Employee
            List<CustomField> fields = null;
            using (EmployerService svc = new EmployerService(Current.Customer(), Current.Employer(), Current.User()))
            {
                fields = svc.GetCustomFields(EntityTarget.Employee).ToList();
            }
            if (employee.CustomFields.Any())
            {
                foreach (CustomField fld in fields)
                {
                    CustomField field = employee.CustomFields.Where(x => x.Id.Equals(fld.Id)).FirstOrDefault();
                    if (field != null)
                        fld.SelectedValue = field.SelectedValue;
                }
            }
            this.CustomFields = fields;

            if (employee.PaySchedule != null)
            {
                this.PaySchedule = employee.PaySchedule.Name;
                this.PayScheduleId = employee.PaySchedule.Id;
            }
            else if (employee.Employer.DefaultPaySchedule != null)
            {
                this.PaySchedule = employee.Employer.DefaultPaySchedule.Name;
                this.PayScheduleId = employee.Employer.DefaultPayScheduleId;
            }

            this.CostCenterCode = employee.CostCenterCode;
            this.Department = employee.Department;

            this.Job = new EmployeeJobViewModel(employee.GetCurrentJob())
            {
                EmployeeNumber = employee.EmployeeNumber,
                EmployerId = employee.EmployerId
            };

            if (string.IsNullOrEmpty(Job.Title))
                Job.Title = employee.JobTitle;

            if (!Job.Activity.HasValue || (employee.JobActivity != null && employee.JobActivity != Job.Activity))
            {
                Job.Activity = employee.JobActivity;
            }
            this.Job.IHasJobHistory = employee.GetJobs().Count > 0;
            this.JobHistory = employee.GetJobs().Select(j => new EmployeeJobViewModel(j)).ToList();
            if (this.JobHistory != null)
            {
                foreach (EmployeeJobViewModel employeeJobViewModel in this.JobHistory)
                {
                    employeeJobViewModel.EmployeeId = employee.Id;
                }
            }

            if (employee.RiskProfile != null)
                RiskProfile = new RiskProfileViewModel(employee.RiskProfile);

            this.IsFlightCrew = employee.IsFlightCrew;
            this.TerminationDate = employee.TerminationDate;
        }

        public bool PaidLeaveFeatureEnabled { get { return Customer.Current.HasFeature(Feature.ShortTermDisablityPay); } }

        public virtual string Id { get; set; }

        [AngularInputType(HTML5InputType.Text)]
        [DisplayName("Employee Number")]
        public virtual string EmployeeNumber { get; set; }
        public virtual string CustomerId { get; set; }
        public virtual string EmployerId { get; set; }
        public virtual string EmployerName { get; set; }
        public virtual string ServiceOptionType { get; set; }
        public virtual string EmployerReferenceCode { get; set; }
        public virtual bool IsTPA { get; set; }
        public virtual bool IsESS { get; set; }
        public List<string> EmployeeCurrentLevelOneOrganizations { get; set; }

        [AngularRequired]
        [AngularInputType(HTML5InputType.Email)]
        [DisplayName("Email Address")]
        public virtual string Email { get; set; }

        [AngularInputType(HTML5InputType.Email)]
        [DisplayName("Alternate Email Address")]
        public virtual string AltEmail { get; set; }

        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        [DisplayName("First Name")]
        public virtual string FirstName { get; set; }

        [DisplayName("Middle Name")]
        [AngularInputType(HTML5InputType.Text)]
        public virtual string MiddleName { get; set; }

        [DisplayName("Last Name")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        public virtual string LastName { get; set; }

        [AngularInputType(HTML5InputType.Radio)]
        public virtual Gender? Gender { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address AltAddress { get; set; }

        [DisplayName("Work Phone")]
        [AngularInputType(HTML5InputType.Tel)]
        public virtual string WorkPhone { get; set; }

        [DisplayName("Home Phone")]
        [AngularInputType(HTML5InputType.Tel)]
        public virtual string HomePhone { get; set; }

        [DisplayName("Cell Phone")]
        [AngularInputType(HTML5InputType.Tel)]
        public virtual string CellPhone { get; set; }

        [DisplayName("Alternate Phone")]
        [AngularInputType(HTML5InputType.Tel)]
        public virtual string AltPhone { get; set; }

        [DisplayName("Service Date")]
        [AngularInputType(HTML5InputType.Text)]
        [AbsenceSoft.Common.Compare("ServiceDate", Common.CompareAttribute.Operator.GreaterThan, "DOB")]
        [AbsenceSoft.Common.Compare("ServiceDate", Common.CompareAttribute.Operator.GreaterThanEqualTo, "HireDate")]
        public virtual DateTime? ServiceDate { get; set; }

        [DisplayName("Hire Date")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        [AbsenceSoft.Common.Compare("HireDate", Common.CompareAttribute.Operator.GreaterThan, "DOB")]
        public virtual DateTime? HireDate { get; set; }

        [DisplayName("Re-hire Date")]
        [AngularInputType(HTML5InputType.Text)]
        [AbsenceSoft.Common.Compare("RehireDate", Common.CompareAttribute.Operator.GreaterThanEqualTo, "HireDate")]
        public virtual DateTime? RehireDate { get; set; }

        [DisplayName("Work State")]
        [AngularRequired]
        public virtual string WorkState { get; set; }

        [DisplayName("Work Country")]
        [AngularRequired]
        public virtual string WorkCountry { get; set; }

        [DisplayName("Work County")]
        public virtual string WorkCounty { get; set; }

        [DisplayName("Work City")]
        public virtual string WorkCity { get; set; }
        
        [DisplayName("Residence County")]
        public virtual string ResidenceCounty { get; set; }

        [AngularRequired]
        [DisplayName("Date of Birth")]
        [AngularInputType(HTML5InputType.Text)]
        [AbsenceSoft.Common.Compare("DOB", Common.CompareAttribute.Operator.GreaterThan, "Today")]
        public virtual DateTime? DOB { get; set; }

        [DisplayName("Exempt?")]
        [AngularInputType(HTML5InputType.Radio)]
        [AngularRequired]
        public virtual bool IsExempt { get; set; }

        [DisplayName("Key Employee?")]
        [AngularInputType(HTML5InputType.Radio)]
        public virtual bool IsKeyEmployee { get; set; }

        [DisplayName("Meets 50 in 75 Mile Rule?")]
        [AngularInputType(HTML5InputType.Radio)]
        [AngularRequired]
        public virtual bool Meets50In75MileRule { get; set; }

        [DisplayName("Prior Hours Worked")]
        [AngularInputType(HTML5InputType.Number)]
        public virtual double? PriorHoursWorked { get; set; }

        [DisplayName("Prior Hours Worked As Of")]
        [AngularInputType(HTML5InputType.Date)]
        public virtual DateTime? PriorHoursWorkedAsOf { get; set; }

        [DisplayName("Prior Hours Worked")]
        [AngularInputType(HTML5InputType.Number)]
        public virtual double? CalculatedHoursWorked { get; set; }

        [DisplayName("Worked Per Week")]
        [AngularInputType(HTML5InputType.Number)]
        public virtual int? WorkedPerWeek { get; set; }

        [DisplayName("Worked Per Week As Of")]
        [AngularInputType(HTML5InputType.Date)]
        public virtual DateTime? WorkedPerWeekAsOf { get; set; }

        public virtual string CalculatedWorkedPerWeek { get; set; }

        public virtual string WorkedPerWeekFriendlyTime { get; set; }

        public virtual bool IgnoreWorkedPerWeek { get; set; }

        public virtual DateTime? CalculatedWorkedPerWeekAsOf { get; set; }

        [AngularInputType(HTML5InputType.Radio)]
        [AngularRequired]
        public virtual PayType? PayType { get; set; }

        [AngularInputType(HTML5InputType.Radio)]
        [AngularRequired]
        public string WorkType { get; set; }

        [DisplayName("Salary")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Number)]
        public virtual double? Salary { get; set; }

        public string PaySchedule { get; set; }

        [DisplayName("Social Security Number")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        public virtual string Ssn { get; set; }

        [DisplayName("Office Location")]
        [AngularRequired]
        [AngularInputType(HTML5InputType.Text)]
        public virtual string OfficeLocation { get; set; }

        [DisplayName("Military Status")]
        [AngularInputType(HTML5InputType.Radio)]
        [AngularRequired]
        public virtual MilitaryStatus? MilitaryStatus { get; set; }

        public DayOfWeek? StartDayOfWeek { get; set; }
        public virtual ScheduleModel WorkSchedule { get; set; }
        public virtual List<ScheduleModel> WorkSchedules { get; set; }
        public virtual List<TimeModel> VariableTimes { get; set; }

        public DateTime Today { get { return DateTime.Now; } }

        public string SpouseEmployeeId { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public string SpouseEmployeeNumber { get; set; }
        public string Status { get; set; }

        public virtual List<CustomField> CustomFields { get; set; }

        public string PayScheduleId { get; set; }

        public string CostCenterCode { get; set; }

        public string Department { get; set; }

        public RiskProfileViewModel RiskProfile { get; set; }

        [UIHint("EmployeeJob")]
        public virtual EmployeeJobViewModel Job { get; set; }

        public virtual List<EmployeeJobViewModel> JobHistory { get; set; }
               
        public virtual bool? IsFlightCrew { get; set; }
        public virtual DateTime? TerminationDate { get; set; }
    }
}