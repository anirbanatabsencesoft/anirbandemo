﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class ViewEmployeeModel : BaseEmployeeModel
    {
        public ViewEmployeeModel() : base()
        {

        }

        public ViewEmployeeModel(AbsenceSoft.Data.Customers.Employee employee)
            : base(employee)
        {

        }
    }
}