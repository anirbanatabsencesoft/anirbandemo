﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class PayScheduleModel
    {
        public string EmployeeId { get; set; }
        public string EmployerId { get; set; }
        public string PayScheduleId { get; set; }
    }
}