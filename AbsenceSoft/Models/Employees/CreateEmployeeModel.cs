﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class CreateEmployeeModel : BaseEmployeeModel
    {
        public CreateEmployeeModel() : base()
        {

        }

        public CreateEmployeeModel(AbsenceSoft.Data.Customers.Employee employee) : base(employee)
        {

        }

        public bool HasConsultsFeature { get; set; }
        public bool HasRiskProfileFeature { get; set; }

        /// <summary>
        /// Determine if flight crew feature is enabled or not.     
        /// </summary>
        public bool HasFlightCrewFeature { get; set; }

        /// <summary>
        /// Employee organization info list
        /// </summary>
        public List<EmployeeOrganizationViewModel> EmployeeOrganizationsList { get; set; }

        
        /// <summary>
        /// Employee current organization info list
        /// </summary>
        public List<EmployeeOrganizationViewModel> EmployeeCurrentOrganizationsList { get; set; }

        /// <summary>
        /// Employer "IgnoreScheduleForPriorHoursWorked" for AT-6510 
        /// </summary>
        public bool HasIgnoreScheduleForPriorHoursWorked { get; set; }

        public bool HasOrgDataVisibility { get; set; }
    }
}