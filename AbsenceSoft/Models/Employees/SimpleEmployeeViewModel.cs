﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class SimpleEmployeeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime? HireDate { get; set; }
        public String Url { get; set; }

        public static string GetEntityFieldName(string fieldName)
        {
            if (string.IsNullOrWhiteSpace(fieldName))
                return null;
            switch (fieldName)
            {
                case "Id": return "Id";
                case "LastName":
                case "Name": return "LastName";
                case "FirstName": return "FirstName";
                case "EmployeeNumber": return "EmployeeNumber";
                case "HireDate": return "HireDate";
                default:
                    return null;
            }
        }
    }

}