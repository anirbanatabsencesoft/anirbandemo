﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models.Jobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    [Serializable]
    public class EmployeeJobViewModel : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeJobViewModel"/> class.
        /// </summary>
        public EmployeeJobViewModel()
        {
            Status = new JobStatusViewModel() { Status = AdjudicationStatus.Pending };
            TheJob = new JobViewModel();
            IHasJobHistory = false;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeJobViewModel"/> class.
        /// </summary>
        /// <param name="job">The job.</param>
        public EmployeeJobViewModel(EmployeeJob job) : this()
        {
            if (job == null)
                return;

            Id = job.Id;
            Code = job.JobCode;
            Name = job.JobName;
            Title = job.JobTitle;
            if (job.Dates != null && !job.Dates.IsNull)
            {
                StartDate = job.Dates.StartDate;
                EndDate = job.Dates.EndDate;
            }
            OfficeLocation = job.OfficeLocationCode;
            OfficeLocations = new EmployeeOrganizationViewModel();
            OfficeLocations.Code = job.OfficeLocationCode;
            Position = job.JobPosition;
            if (job.Status != null)
            {
                Status = new JobStatusViewModel();
                Status.Status = job.Status.Value;
                Status.DenialReason = job.Status.Reason;
                Status.DenialReasonOther = job.Status.Reason != JobDenialReason.Other ? null : job.Status.Comments;
            }
            else
                Status = new JobStatusViewModel() { Status = AdjudicationStatus.Pending };
            Status.EmployeeJobId = job.Id;

            EmployeeNumber = job.EmployeeNumber;
            EmployerId = job.EmployerId;
            EmployeeId = job.Employee.Id;

            if(job.Job != null)
            {
                TheJob = new JobViewModel(job.Job);
                Activity = job.Job.Activity;
            }
        }

        /// <summary>
        /// Gets or sets the job Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the job code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        [DisplayName("Job"), UIHint("Job")]
        public virtual string Code { get; set; }
        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public virtual string Name { get; set; }
        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DisplayName("Job Title")]
        public virtual string Title { get; set; }

        /// <summary>
        /// The classified activity level of the job
        /// </summary>
        [DisplayName("Job Activity"), UIHint("EnumButtons")]
        public JobClassification? Activity { get; set; }

        [DisplayName("Position")]
        public virtual int? Position { get; set; }

        /// <summary>
        /// End current job
        /// </summary>
        [DisplayName("End Current Job(s)"), UIHint("EndCurrentJob")]
        public virtual string EndCurrentJob { get; set; }

        /// <summary>
        /// End current job checked
        /// </summary>
        [DisplayName("End Current Job"), UIHint("isEndCurrentJob")]
        public virtual bool isEndCurrentJob { get; set; }

        /// <summary>
        /// Delete employee job
        /// </summary>
        [DisplayName("Delete this job on Save"), UIHint("DeleteEmployeeJob")]
        public virtual string DeleteEmployeeJob { get; set; }

        [DisplayName("Delete this job on Save"), UIHint("isDeleteEmployeeJob")]
        public virtual bool isDeleteEmployeeJob { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [Display(Name = "Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public virtual DateTime? StartDate { get; set; }
        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [Display(Name = "End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public virtual DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the office locations.
        /// </summary>
        public EmployeeOrganizationViewModel OfficeLocations { get; set; }

        /// <summary>
        /// Gets or sets the office location.
        /// </summary>
        /// <value>
        /// The office location.
        /// </value>
        [DisplayName("Location")]
        public virtual string OfficeLocation { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DisplayName("Status")]
        public virtual JobStatusViewModel Status { get; set; }

        public JobViewModel TheJob { get; set; }

        /// <summary>
        /// Gets the display.
        /// </summary>
        /// <value>
        /// The display.
        /// </value>
        [DisplayName("Job")]
        public virtual string Display
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Name))
                    return Code;
                if (string.IsNullOrWhiteSpace(Code))
                    return Name;
                return string.Format("{1} ({0})", Code, Name);
            }
        }

        /// <summary>
        /// Gets the status hint.
        /// </summary>
        /// <value>
        /// The status hint.
        /// </value>
        public virtual string StatusHint
        {
            get
            {
                if (Status == null)
                    return "Pending";
                if (Status.Status != AdjudicationStatus.Denied) return Status.Status.ToString();
                if (Status.DenialReason.HasValue && Status.DenialReason.Value != JobDenialReason.Other)
                    return string.Format("Denied - {0}", Status.DenialReason.ToString().SplitCamelCaseString());
                if (string.IsNullOrWhiteSpace(Status.DenialReasonOther))
                    return "Denied";
                return string.Format("Denied - {0}", Status.DenialReasonOther);
            }
        }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        /// <value>
        /// The employee number.
        /// </value>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        /// <value>
        /// The employee id.
        /// </value>
        public string EmployeeId { get; set; }
        /// <summary>
        /// Gets or sets the employer identifier.
        /// </summary>
        /// <value>
        /// The employer identifier.
        /// </value>
        public string EmployerId { get; set; }

        /// <summary>
        /// Gets a value indicating whether I haz job.
        /// </summary>
        /// <value>
        ///   <c>true</c> if I haz job; otherwise, <c>false</c>.
        /// </value>
        public bool IHazJob
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Code);
            }
        }

        public bool IHasJobHistory { get; set; }

        /// <summary>
        /// Applies to data model.
        /// </summary>
        /// <param name="empJob">The emp job.</param>
        /// <param name="employeeNumber">The employee number.</param>
        /// <param name="serviceDate">The service date.</param>
        /// <param name="isPreviouslyAddedJob">The flag for updating the job.</param>
        /// <returns></returns>
        public EmployeeJob ApplyToDataModel(EmployeeJob empJob, DateTime? serviceDate, bool? isPreviouslyAddedJob = false)
        {
            if (string.IsNullOrWhiteSpace(Code))
                return null;

            if (empJob != null && !ChangedEmployeeJob(empJob))
                return null;

            if (serviceDate == null)
                serviceDate = DateTime.Now;

            if (empJob != null && empJob.JobCode != Code && !isPreviouslyAddedJob.Value)
            {
                if (empJob.Dates == null)
                    empJob.Dates = serviceDate.HasValue ? new DateRange(serviceDate.Value) : DateRange.Null;

                empJob.Dates.EndDate = DateTime.Now;
                empJob.Save();
                empJob = null;
            }

            /* Get job detail and Update the employee job detail */
            else if (empJob != null && empJob.JobCode != Code && isPreviouslyAddedJob.Value)
            {
                Job job = Job.GetByCode(this.Code, Customer.Current.Id, empJob.EmployerId);
                empJob.Job = job;
                empJob.JobCode = job.Code;
                empJob.JobName = job.Name;                
            }

            empJob = empJob ?? new EmployeeJob(Job.GetByCode(this.Code, Customer.Current.Id, empJob != null ? empJob.EmployerId ?? (this.EmployerId ?? Employer.Current.Id) : this.EmployerId ?? Employer.Current.Id));
            empJob.JobTitle = this.Title;
            empJob.EmployeeNumber = this.EmployeeNumber;
            empJob.OfficeLocationCode = this.OfficeLocations != null ? this.OfficeLocations.Code : this.OfficeLocation;
            empJob.JobPosition = this.Position;
            empJob.Dates = this.StartDate.HasValue ? new DateRange(this.StartDate ?? serviceDate.Value, this.EndDate) : DateRange.Null;
            if (Status != null && Status.Status != empJob.Status.Value)
            {
                empJob.Status.Value = Status.Status;
                empJob.Status.Reason = Status.DenialReason;
                empJob.Status.Comments = Status.DenialReasonOther;
                empJob.Status.User = User.Current;
                empJob.Status.Date = DateTime.UtcNow;
            }

            return empJob;
        }


        /// <summary>
        /// Changeds the employee job.
        /// </summary>
        /// <param name="job">The job.</param>
        /// <returns></returns>
        private bool ChangedEmployeeJob(EmployeeJob job)
        {
            if (this.Code != job.JobCode)
            {
                return true;
            }                

            if (this.Position != job.JobPosition)
            {
                return true;
            }

            if (this.OfficeLocations != null && this.OfficeLocations.Code != job.OfficeLocationCode)
            {
                return true;
            }          

            if (job.Dates == null)
            {
                return true;
            }

            if (job.Dates.StartDate != this.StartDate)
            {
                return true;
            }

            if (job.Dates.EndDate != this.EndDate)
            {
                return true;
            }

            if (Status != null && job.Status.Value != Status.Status)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified object is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (this.EndDate.HasValue && this.StartDate.HasValue && this.StartDate.Value > this.EndDate.Value)
                results.Add(new ValidationResult("Job Start Date must come before the End Date", new[] { "StartDate", "EndDate" }));
            if (this.Status.Status == AdjudicationStatus.Denied && this.Status.DenialReason == null)
                results.Add(new ValidationResult("Please Select a Denial Reason", new[] { "Status" }));
            return results;
        }
    }
}