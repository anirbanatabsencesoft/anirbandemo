﻿using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Models.Demands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class EmployeeNecessityViewModel : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNecessityViewModel"/> class.
        /// </summary>
        public EmployeeNecessityViewModel() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeNecessityViewModel"/> class.
        /// </summary>
        /// <param name="necessity">The necessity.</param>
        public EmployeeNecessityViewModel(EmployeeNecessity necessity) : this()
        {
            if (necessity == null)
                return;

            Id = necessity.Id;
            EmployeeId = necessity.EmployeeId;
            CaseId = necessity.CaseId;
            NecessityId = necessity.NecessityId;
            Name = necessity.Name;
            Description = necessity.Description;
            Type = necessity.Type.ToString().SplitCamelCaseString();
            if (necessity.Dates != null && !necessity.Dates.IsNull)
            {
                StartDate = necessity.Dates.StartDate;
                EndDate = necessity.Dates.EndDate;
            }
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the case identifier.
        /// </summary>
        /// <value>
        /// The case identifier.
        /// </value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the necessity ID.
        /// </summary>
        /// <value>
        /// The necessity ID.
        /// </value>
        [Required, UIHint("Necessity"), DisplayName("Necessity")]
        public string NecessityId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DisplayName("Name"), ReadOnly(true)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DisplayName("Description"), ReadOnly(true)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [DisplayName("Type"), ReadOnly(true)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [DisplayName("Start Date"), Required, DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        [DisplayName("End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Validates the specified validation context.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(NecessityId))
                return results;

            if (this.EndDate.HasValue && this.StartDate.HasValue && this.StartDate.Value > this.EndDate.Value)
                results.Add(new ValidationResult("Employee Necessity Start Date must come before the End Date", new[] { "StartDate", "EndDate" }));

            return results;
        }
    }
}