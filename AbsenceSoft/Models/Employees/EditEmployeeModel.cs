﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Employees
{
    public class EditEmployeeModel : BaseEmployeeModel
    {
        public EditEmployeeModel() : base()
        {

        }

        public EditEmployeeModel(AbsenceSoft.Data.Customers.Employee employee) : base(employee)
        {

        }
    }
}