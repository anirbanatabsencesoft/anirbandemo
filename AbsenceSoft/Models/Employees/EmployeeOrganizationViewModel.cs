﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsenceSoft.Data.Customers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace AbsenceSoft.Models.Employees
{
    [Serializable]
    public class EmployeeOrganizationViewModel : IValidatableObject
    {
        public EmployeeOrganizationViewModel()
        {

        }

        public EmployeeOrganizationViewModel(EmployeeOrganization org) : this()
        {
            if (org == null)
                return;

            this.Name = org.Name;
            this.Code = org.Code;
            if (org.Dates != null)
            {
                this.StartDate = org.Dates.StartDate;
                this.EndDate = org.Dates.EndDate;
            }

            this.TypeCode = org.TypeCode;

            this.Affilation = org.Affiliation.ToString();
        }        

        public EmployeeOrganizationViewModel(EmployeeOrganization org,string  parentName, string parentCode, string parentType) : this()
        {
            if (org == null)
                return;

            this.Name = org.Name;
            this.Code = org.Code;
            if (org.Dates != null)
            {
                this.StartDate = org.Dates.StartDate;
                this.EndDate = org.Dates.EndDate;
            }

            this.TypeCode = org.TypeCode;
            this.Affilation = org.Affiliation.ToString();
            this.ParentOrgName = parentName;
            this.ParentOrgCode = parentCode;
            this.ParentOrgTypeCode = parentType;
        }

        public string Name { get; set; }
        public string EmployerId { get; set; }

        [UIHint("Organization"), DisplayName("Location")]
        public string Code { get; set; }

        [DisplayName("Start Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Organization Type code
        /// </summary>
        public string TypeCode { get; set; }
        public bool IHazOrg
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Code);
            }
        }

        /// <summary>
        /// Organization Affiliations
        /// </summary>
        public string Affilation { get; set; }

        /// <summary>
        /// Parent Organization Name
        /// </summary>
        public string ParentOrgName { get; set; }

        /// <summary>
        /// Parent Organization Code
        /// </summary>
        public string ParentOrgCode { get; set; }

        /// <summary>
        /// Parent Organization Type Code
        /// </summary>
        public string ParentOrgTypeCode { get; set; }

        public EmployeeOrganization ApplyToDataModel(EmployeeOrganization employeeOrg, string employeeNumber, DateTime? serviceDate)
        {
            if (string.IsNullOrWhiteSpace(Code))
                return null;

            if (employeeOrg != null && !ChangedEmployeeOrganization(employeeOrg))
                return null;

            if (serviceDate == null)
                serviceDate = DateTime.UtcNow;

            if (employeeOrg != null && employeeOrg.Code != this.Code) // Different code, not just different dates
            {
                if (employeeOrg.Dates == null)
                    employeeOrg.Dates = serviceDate.HasValue ? new DateRange(serviceDate.Value) : DateRange.Null;

                employeeOrg.Dates.EndDate = this.StartDate ?? DateTime.UtcNow;
                employeeOrg.Save();
                employeeOrg = null;
            }

            employeeOrg = employeeOrg ?? new EmployeeOrganization(AbsenceSoft.Data.Customers.Organization.GetByCode(this.Code, Customer.Current.Id, Employer.CurrentId ?? this.EmployerId));
            employeeOrg.EmployeeNumber = employeeNumber;
            employeeOrg.Dates = this.StartDate.HasValue ? new DateRange(this.StartDate ?? serviceDate.Value, this.EndDate) : DateRange.Null;

            return employeeOrg;
        }


        private bool ChangedEmployeeOrganization(EmployeeOrganization org)
        {
            if (string.IsNullOrWhiteSpace(Code))
                return false;

            if (this.Code != org.Code)
                return true;

            if (org.Dates == null)
                return true;

            if (org.Dates.StartDate != this.StartDate)
                return true;

            if (org.Dates.EndDate != this.EndDate)
                return true;

            return false;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(Code))
                return results;

            if (this.EndDate.HasValue && this.StartDate.HasValue && this.StartDate.Value > this.EndDate.Value)
                results.Add(new ValidationResult("Organization Start Date must come before the End Date", new[] { "StartDate", "EndDate" }));

            return results;
        }
    }
}