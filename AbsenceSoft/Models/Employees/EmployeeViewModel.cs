﻿using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Models.Organization;
using AbsenceSoft.Models.RiskProfiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.CustomFields;
using AbsenceSoft.Web;

namespace AbsenceSoft.Models.Employees
{
    public class EmployeeViewModel : IValidatableObject
    {
        #region Constructors
        public EmployeeViewModel()
        {
            Schedule = new ScheduleViewModel();
            Organization = new EmployeeOrganizationViewModel();
            Job = new EmployeeJobViewModel();
            EmployeeCustomFields = new List<CustomFieldViewModel>();
            EmployeeClasses = new List<EmployeeClass>();
        }

        public EmployeeViewModel(Employee emp) : this()
        {
            if (emp == null)
                throw new ArgumentNullException("emp");

            Id = emp.Id;
            CustomerId = emp.CustomerId;
            EmployerId = emp.EmployerId;
            EmployerName = emp.EmployerName;
            FirstName = emp.FirstName;
            MiddleName = emp.MiddleName;
            LastName = emp.LastName;
            EmployeeNumber = emp.EmployeeNumber;
            OriginalEmployeeNumber = emp.EmployeeNumber;
            FullName = emp.FullName;
            DateOfBirth = emp.DoB;
            SSN = emp.Ssn != null && !string.IsNullOrWhiteSpace(emp.Ssn.Encrypted) ? AbsenceSoft.Common.Security.Crypto.Decrypt(emp.Ssn.Encrypted) : null;
            ServiceDate = emp.ServiceDate;
            Gender = emp.Gender;
            MilitaryStatus = emp.MilitaryStatus;

            AddressId = emp.Info.Address.Id;
            Address1 = emp.Info.Address.Address1;
            Address2 = emp.Info.Address.Address2;
            City = emp.Info.Address.City;
            State = emp.Info.Address.State;
            PostalCode = emp.Info.Address.PostalCode;
            Country = emp.Info.Address.Country ?? "US";

            AltAddressId = emp.Info.AltAddress.Id;
            AltAddress1 = emp.Info.AltAddress.Address1;
            AltAddress2 = emp.Info.AltAddress.Address2;
            AltCity = emp.Info.AltAddress.City;
            AltState = emp.Info.AltAddress.State;
            AltPostalCode = emp.Info.AltAddress.PostalCode;
            AltCountry = emp.Info.AltAddress.Country;

            Email = emp.Info.Email;
            AltEmail = emp.Info.AltEmail;
            WorkPhone = emp.Info.WorkPhone;
            CellPhone = emp.Info.CellPhone;
            HomePhone = emp.Info.HomePhone;
            AltPhone = emp.Info.AltPhone;

            WorkCountry = emp.WorkCountry;
            WorkState = emp.WorkState;
            OfficeLocation = emp.Info.OfficeLocation;
            Meets50In75Rule = emp.Meets50In75MileRule;
            WorkCounty = emp.WorkCounty;
            WorkCity = emp.WorkCity;
            ResidenceCounty = emp.ResidenceCounty;
            HireDate = emp.HireDate;
            RehireDate = emp.RehireDate;
            TerminationDate = emp.TerminationDate;
            ServiceDate = emp.ServiceDate;
            PayType = emp.PayType;
            Salary = emp.Salary;
            IsExempt = emp.IsExempt;
            EmployeeClassCode = emp.EmployeeClassCode;
            EmployeeClassName = emp.EmployeeClassName;
            IsKeyEmployee = emp.IsKeyEmployee;
            PayScheduleId = emp.PayScheduleId;
            EmploymentStatus = emp.Status;
            SpouseEmployeeNumber = emp.SpouseEmployeeNumber;
            CostCenter = emp.CostCenterCode;
            Department = emp.Department;
            StartDayOfWeek = emp.StartDayOfWeek;
            IsFlightCrew = emp.IsFlightCrew;
            

            if (emp.RiskProfile != null)
                RiskProfile = new RiskProfileViewModel(emp.RiskProfile);

            // Our UI only shows one, so lets make sure we're only displaying the most recent one
            if (emp.PriorHours.Any())
            {
                // We need to capture the most recent prior hours worked, NOT the last in the array
                //  which has no guaranteed order.
                var priorHours = emp.PriorHours.OrderBy(p => p.AsOf).Last();

                PriorHoursWorked = priorHours.HoursWorked;
                PriorHoursAsOf = priorHours.AsOf;
            }
            // Our UI only shows one, so lets make sure we're only displaying the most recent one
            if (emp.MinutesWorkedPerWeek.Any())
            {
                var workedPerWeek = emp.MinutesWorkedPerWeek.Last();
                WorkedPerWeek = workedPerWeek.MinutesWorked;
                WorkedPerWeekFriendlyTime = workedPerWeek.MinutesWorked.ToFriendlyTime();
                WorkedPerWeekAsOf = workedPerWeek.AsOf;
            }

            if (emp.WorkSchedules.Any())
            {
                var workSchedule = emp.WorkSchedules.OrderByDescending(p => p.StartDate).First();
                Schedule = new ScheduleViewModel(Id, workSchedule);
            }
            else
            {
                Schedule.BuildTimesLists();
            }

            Organization = new EmployeeOrganizationViewModel(emp.GetOfficeLocation());
            Job = new EmployeeJobViewModel(emp.GetCurrentJob())
            {
                EmployeeNumber = emp.EmployeeNumber,
                EmployerId = emp.EmployerId
            };
            if(string.IsNullOrEmpty(Job.Title))
                Job.Title = emp.JobTitle;
            if (!Job.Activity.HasValue)
                Job.Activity = emp.JobActivity;

            Job.IHasJobHistory = (!string.IsNullOrWhiteSpace(emp.Id) &&  emp.GetJobs().Any());
        }

        public EmployeeViewModel(Employee emp, List<OrganizationVisibilityTree> empOrganizations)
            : this(emp)
        {
            EmployeeCurrentOrganizations = empOrganizations;
            AssignOrganizations = new List<OrganizationVisibilityTree>();
            OrganizationTypes = new Dictionary<string, string>();
            OrganizationTypes.Add("", "Select One...");
            OrganizationType.AsQueryable().Where(orgType => orgType.EmployerId == emp.EmployerId).OrderBy(orgType => orgType.Name).ToList().ForEach(type=>OrganizationTypes.Add(type.Code,type.Name));
        }

        #endregion

        #region Properties

        #region Employee Info
        public string Id { get; set; }

        [Display(Name = "Employer"), UIHint("Employers"), Required]
        public string EmployerId { get; set; }

        [Display(Name = "Employer")]
        public string EmployerName { get; set; }

        [Display(Name = "CustomerId"), Required]
        public string CustomerId { get; set; }

        [Display(Name = "Employee ID"), Required]
        public string EmployeeNumber { get; set; }

        public string OriginalEmployeeNumber { get; set; }

        [Display(Name = "First Name"), Required]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name"), Required]
        public string LastName { get; set; }

        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Display(Name = "Date Of Birth"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "SSN"), UIHint("EyeToggle")]
        public string SSN { get; set; }

        [Display(Name = "Military Status")]
        public MilitaryStatus MilitaryStatus { get; set; }

        [UIHint("EnumButtons")]
        public Gender? Gender { get; set; }

        [Display(Name = "Airline Flight Crew")]
        public bool? IsFlightCrew { get; set; }
        #endregion

        #region Contact Info

        [Display(Name = "Work Email"), EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Pers Email"), EmailAddress]
        public string AltEmail { get; set; }

        [Display(Name = "Work Phone"), Phone]
        public string WorkPhone { get; set; }

        [Display(Name = "Cell Phone"), Phone]
        public string CellPhone { get; set; }

        [Display(Name = "Home Phone"), Phone]
        public string HomePhone { get; set; }

        [Display(Name = "Alt Phone"), Phone]
        public string AltPhone { get; set; }

        #region Address

        public Guid AddressId { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        public string City { get; set; }

        [UIHint("State")]
        public string State { get; set; }

        [Display(Name = "Zip")]
        public string PostalCode { get; set; }

        [UIHint("Country")]
        public string Country { get; set; }

        #endregion

        #region AltAddress

        public Guid AltAddressId { get; set; }

        [Display(Name = "Address 1")]
        public string AltAddress1 { get; set; }

        [Display(Name = "Address 2")]
        public string AltAddress2 { get; set; }

        [Display(Name = "City")]
        public string AltCity { get; set; }

        [Display(Name = "State"), UIHint("State")]
        public string AltState { get; set; }

        [Display(Name = "Zip"), RegularExpression(@"^\d{5}(?:-\d{4})?$", ErrorMessage = "Zip Code should be in the format XXXXX-XXXX.")]
        public string AltPostalCode { get; set; }

        [Display(Name = "Country"), UIHint("Country")]
        public string AltCountry { get; set; }
        #endregion

        #endregion

        #region Job Info
        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Work Country"), UIHint("Country")]
        public string WorkCountry { get; set; }

        [Display(Name = "Work County")]
        public string WorkCounty { get; set; }

        [Display(Name = "Work City")]
        public string WorkCity { get; set; }

        [Display(Name = "Residence County")]
        public string ResidenceCounty { get; set; }

        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Work State"), UIHint("State")]
        public string WorkState { get; set; }

        [Display(Name = "Office Location")]
        public string OfficeLocation { get; set; }

        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Meets 50/75 Rule")]
        public bool? Meets50In75Rule { get; set; }

        [Display(Name = "Hire Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? HireDate { get; set; }

        [Display(Name = "Re-hire Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? RehireDate { get; set; }

        [Display(Name = "Termination Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? TerminationDate { get; set; }

        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Service Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ServiceDate { get; set; }

        [Display(Name = "Prior Hours Worked")]
        public double? PriorHoursWorked { get; set; }

        [Display(Name = "Prior Hours As Of"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? PriorHoursAsOf { get; set; }


        public int? WorkedPerWeek { get; set; }

        [Display(Name = "Worked Per Week")]
        public string WorkedPerWeekFriendlyTime { get; set; }

        [Display(Name = "Worked Per Week As Of"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? WorkedPerWeekAsOf { get; set; }

        [Display(Name = "Pay Type"), UIHint("EnumButtons")]
        public PayType? PayType { get; set; }

        [Display(Name = "Salary"), UIHint("EyeToggle")]
        public double? Salary { get; set; }

        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Exempt")]
        public bool? IsExempt { get; set; }

        public string WorkType { get; set; }

        [RequiredIfCustomerHasFeature(Feature.LOA)]
        [Display(Name = "Key Employee")]
        public bool? IsKeyEmployee { get; set; }

        [Display(Name = "Pay Schedule"), UIHint("PaySchedule")]
        public string PayScheduleId { get; set; }

        [Display(Name = "Employment Status"), Required]
        public EmploymentStatus EmploymentStatus { get; set; }

        [Display(Name = "Employee Spouse")]
        public string SpouseEmployeeNumber { get; set; }

        [Display(Name = "Cost Center")]
        public string CostCenter { get; set; }

        [Display(Name = "Department")]
        public string Department { get; set; }

        #endregion

        [DisplayName("Start Day Of Week")]
        public DayOfWeek? StartDayOfWeek { get; set; }

        [UIHint("ScheduleEditor"), Required]
        public ScheduleViewModel Schedule { get; set; }

        public EmployeeOrganizationViewModel Organization { get; set; }

        [UIHint("EmployeeJob")]
        public EmployeeJobViewModel Job { get; set; }

        public List<EmployeeJobViewModel> JobHistory { get; set; }

        [UIHint("EmployeeCurrentOrganizations")]
        public List<OrganizationVisibilityTree> EmployeeCurrentOrganizations { get; set; }

        [UIHint("AssignOrganizations")]
        public List<OrganizationVisibilityTree> AssignOrganizations { get; set; }

        public Dictionary<string, string> OrganizationTypes { get; set; }

        public RiskProfileViewModel RiskProfile { get; set; }

        public List<CustomFieldViewModel> EmployeeCustomFields { get; set; }
        [Display(Name = "Employee Class")]
        public List<EmployeeClass> EmployeeClasses { get; set; }

        //[Display(Name = "Employee Class"), UIHint("EmployeeClass")]
        //public string EmployeeClassId { get; set; }

        /// <summary>
        /// Gets or sets the code of employee class.
        /// </summary>
        [Display(Name = "Employee Class"), UIHint("EmployeeClass")]
        public string EmployeeClassCode { get; set; }

        /// <summary>
        /// Gets or sets the description of employee class.
        /// </summary>
        public string EmployeeClassName { get; set; }

        /// <summary>
        /// Gets or sets the description of employee class.
        /// </summary>
        public string EmployeeClassDescription { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Applies the changes done in the view to the employee data model
        /// </summary>
        /// <param name="e">The employee to apply the changes to</param>
        /// <returns>The employee object with the changes applied</returns>
        public Employee ApplyToDataModel(Employee e)
        {
            if (e == null)
                throw new ArgumentNullException("e");

            if (e.Id != Id)
                throw new AbsenceSoftException("The id for the passed in employee does not match the employee being edited in the UI");

            ApplyEmployeeToDataModel(e);
            ApplyEmployeeInfoToDataModel(e);
            ApplyAddressToDataModel(e);
            ApplyAltAddressToDataModel(e);
            ApplyPotentiallyNullValues(e);
            ApplyCustomFieldToDataModel(e);
            return e;
        }

        private void ApplyCustomFieldToDataModel(Employee emp)
        {
            using (EmployerService svc = new EmployerService())
            {
                svc.CurrentEmployer = svc.GetById(EmployerId);
                List<CustomField> configurationFields = null;
                if (string.IsNullOrEmpty(emp.Id))
                {
                    //In Employee add mode only custom fields with collectedAtIntake true or false
                    configurationFields = svc.GetCustomFields(EntityTarget.Employee, true);
                }
                else
                {
                    configurationFields = svc.GetCustomFields(EntityTarget.Employee);
                }


                if (configurationFields != null)
                { 
                    foreach (CustomField fld in configurationFields)
                    {
                        CustomFieldViewModel employeeSavedFields = EmployeeCustomFields.FirstOrDefault(x => x.Code == fld.Code);
                        if (employeeSavedFields != null && employeeSavedFields.SelectedValue != null)
                        {
                            // Selected value is an object type, so it comes back as object and we get {system.string[0]} in it.
                            //We need to fetch the first string from the object.
                            fld.SelectedValue = ((string[])employeeSavedFields.SelectedValue)[0];
                        }
                    }
                }
                emp.CustomFields = configurationFields;
            }
        }

        private void ApplyEmployeeToDataModel(Employee e)
        {
            e.CustomerId = CustomerId;
            e.EmployerId = EmployerId;
            e.EmployeeNumber = EmployeeNumber.Trim();
            e.FirstName = FirstName;
            e.MiddleName = MiddleName;
            e.LastName = LastName;
            e.DoB = DateOfBirth;
            e.Ssn = SSN != null ? new CryptoString(SSN.Replace("-", "")).Hash().Encrypt() : null;
            e.Gender = Gender;
            e.MilitaryStatus = MilitaryStatus;
            e.WorkCountry = WorkCountry;
            e.WorkState = WorkState;
            e.WorkCounty = WorkCounty;
            e.WorkCity = WorkCity;
            e.ResidenceCounty = ResidenceCounty;
            e.HireDate = HireDate;
            e.RehireDate = RehireDate;
            e.TerminationDate = TerminationDate;
            e.ServiceDate = ServiceDate;
            e.PayType = PayType;
            e.Salary = Salary;
            e.PayScheduleId = PayScheduleId;
            e.Status = EmploymentStatus;
            e.SpouseEmployeeNumber = SpouseEmployeeNumber;
            e.CostCenterCode = CostCenter;
            e.Department = Department;
            e.EmployeeClassCode = EmployeeClassCode;
            e.EmployeeClassName = EmployeeClassName;
            e.StartDayOfWeek = StartDayOfWeek;
            e.IsFlightCrew = IsFlightCrew;
        }

        private void ApplyEmployeeInfoToDataModel(Employee e)
        {
            e.Info.Email = Email;
            e.Info.AltEmail = AltEmail;
            e.Info.WorkPhone = WorkPhone;
            e.Info.CellPhone = CellPhone;
            e.Info.HomePhone = HomePhone;
            e.Info.AltPhone = AltPhone;
            e.Info.OfficeLocation = OfficeLocation;
        }

        private void ApplyAddressToDataModel(Employee e)
        {
            e.Info.Address.Address1 = Address1;
            e.Info.Address.Address2 = Address2;
            e.Info.Address.City = City;
            e.Info.Address.State = State;
            e.Info.Address.PostalCode = PostalCode;
            e.Info.Address.Country = Country;
        }

        private void ApplyAltAddressToDataModel(Employee e)
        {
            e.Info.AltAddress.Address1 = AltAddress1;
            e.Info.AltAddress.Address2 = AltAddress2;
            e.Info.AltAddress.City = AltCity;
            e.Info.AltAddress.State = AltState;
            e.Info.AltAddress.PostalCode = AltPostalCode;
            e.Info.AltAddress.Country = AltCountry;
        }

        private void ApplyPotentiallyNullValues(Employee e)
        {
            if (Meets50In75Rule.HasValue)
                e.Meets50In75MileRule = Meets50In75Rule.Value;

            if (IsExempt.HasValue)
                e.IsExempt = IsExempt.Value;

            if (IsKeyEmployee.HasValue)
                e.IsKeyEmployee = IsKeyEmployee.Value;

            if (Job != null)
            {
                e.JobTitle = Job.Title;
                e.JobActivity = Job.Activity;
            }

            if (PriorHoursWorked.HasValue)
                ApplyPriorHoursToDataModel(e);

            if (WorkedPerWeekFriendlyTime != null)
                ApplyWorkedPerWeekToDataModel(e);
        }

        /// <summary>
        /// Updates the Prior Hours on the employee based on the matching date
        /// </summary>
        /// <param name="e"></param>
        private void ApplyPriorHoursToDataModel(Employee e)
        {
            DateTime priorHoursAsOf = (PriorHoursAsOf ?? DateTime.UtcNow).ToMidnight();
            var priorHoursMatchingDate = e.PriorHours.FirstOrDefault(p => p.AsOf.ToMidnight() == priorHoursAsOf);
            if (priorHoursMatchingDate != null)
            {
                e.PriorHours.Remove(priorHoursMatchingDate);
                priorHoursMatchingDate.HoursWorked = PriorHoursWorked.Value;
            }
            else
            {
                priorHoursMatchingDate = new PriorHours()
                {
                    AsOf = priorHoursAsOf,
                    HoursWorked = PriorHoursWorked.Value
                };
            }

            e.PriorHours.Add(priorHoursMatchingDate);
        }

        private void ApplyWorkedPerWeekToDataModel(Employee e)
        {
            DateTime minutesWorkedPerWeekAsOf = (WorkedPerWeekAsOf ?? DateTime.UtcNow).ToMidnight();
            var workedPerWeekMatchingDate = e.MinutesWorkedPerWeek.FirstOrDefault(p => p.AsOf.ToMidnight() == minutesWorkedPerWeekAsOf);
            WorkedPerWeek = WorkedPerWeekFriendlyTime.ParseFriendlyTime();
            if (workedPerWeekMatchingDate != null)
            {
                e.MinutesWorkedPerWeek.Remove(workedPerWeekMatchingDate);
                workedPerWeekMatchingDate.MinutesWorked = WorkedPerWeek.Value;
            }
            else
            {
                workedPerWeekMatchingDate = new MinutesWorkedPerWeek()
                {
                    AsOf = minutesWorkedPerWeekAsOf,
                    MinutesWorked = WorkedPerWeek.Value
                };
            }

            e.MinutesWorkedPerWeek.Add(workedPerWeekMatchingDate);
        }

        /// <summary>
        /// Validates stuff that can't be handled by simple data annotation
        /// Look at the method to see what it does
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (DateOfBirth > ServiceDate)
            {
                results.Add(new ValidationResult("Date of Birth is after Service Date. An employee may not be in service before they are born"));
            }

            if (SSNExists())
            {
                results.Add(new ValidationResult("SSN already exists for another employee."));
            }

            if (GetEmployeeOnlyByEmail(Email))
            {
                results.Add(new ValidationResult("Work Email already exists for another employee."));
            }

            if (!string.IsNullOrWhiteSpace(Country) && !string.IsNullOrWhiteSpace(PostalCode))
            {
                if (Country.Trim().ToUpper() == "US" && !Regex.IsMatch(PostalCode, @"^\d{5}(?:[-\s]\d{4})?$"))
                {
                    results.Add(new ValidationResult("Valid format is XXXXX-XXXX.", new[] { "PostalCode" }));
                }
                else if (Country.Trim().ToUpper() == "CA" && !Regex.IsMatch(PostalCode, @"^[A-Za-z]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$"))
                {
                    results.Add(new ValidationResult("Valid format is AXA XAX OR AXAXAX", new[] { "PostalCode" }));
                }
            }

            using (var employerService = new EmployerService(Current.User()))
            {
                var employer = employerService.GetById(EmployerId);
                if (employer.IsDisabled)
                {
                    results.Add(new ValidationResult("You can not add a new employee for a disabled employer", new List<string>() { "EmployerId" }));
                }
            }
            if (Schedule.ScheduleType == ScheduleType.Variable && Schedule.Times.Sum(t => !string.IsNullOrEmpty(t.TotalMinutes) ? t.TotalMinutes.ParseFriendlyTime() : 0) == 0)
            {
                results.Add(new ValidationResult("Please fill at least one scheduled minute from the weekly schedule blocks", new[] { "Schedule.Times[0].TotalMinutes" }));
            }
            return results;
        }

        private bool SSNExists()
        {
            if (SSN != null)
            {
                var ssn = Employee.AsQueryable().Where(ca => ca.Ssn.Hashed == new CryptoString(this.SSN.Replace("-", "")).Hash().Hashed && ca.Id != this.Id).ToList();
                return ssn.Any();
            }

            return false;
        }        

        /// <summary>
        /// Get Emp by work emailId
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private Boolean GetEmployeeOnlyByEmail(string email)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                var empList = Employee.AsQueryable().Where(ca => ca.Info.Email == email.ToLowerInvariant() && ca.Id != this.Id).ToList();
                if (empList.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

    }
}
