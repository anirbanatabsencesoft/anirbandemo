﻿using AbsenceSoft.Data.Enums;
using AbsenceSoft.Data.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class RuleGroupViewModel:IDataTransformModel<RuleGroup>
    {
        public RuleGroupViewModel()
        {

        }

        public RuleGroupViewModel(RuleGroup group)
        {
            Name = group.Name;
            Description = group.Description;
            SuccessType = group.SuccessType;
            Rules = group.Rules;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public RuleGroupSuccessType SuccessType { get; set; }
        public List<Rule> Rules { get; set; }

        public virtual RuleGroup ApplyToDataModel(RuleGroup entity = null)
        {
            if (entity == null)
                entity = new RuleGroup();

            entity.Name = Name;
            entity.Description = Description;
            entity.SuccessType = SuccessType;
            entity.Rules = Rules;

            return entity;
        }
    }
}