﻿using AbsenceSoft.Data.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.CustomContent
{
    public class CustomContentViewModel
    {
        public CustomContentViewModel()
        {
            this.Keys = Content.GetAllPossibleKeys().Select(s => new SelectListItem()
            {
                Text = s,
                Value = s,
                Selected = s == Name
            });
        }

        public CustomContentViewModel(Content content)
            :this()
        {
            this.Id = content.Id;
            this.Name = content.Key;
            this.Value = content.Value;
            this.EmployerId = content.EmployerId;
            foreach (var key in Keys)
            {
                key.Selected = key.Value == this.Name;
            }
        }

        public string Id { get; set; }

        [Display(Name="Employer"), UIHint("Employers")]
        public string EmployerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [AllowHtml, DataType(DataType.MultilineText)]
        public string Value { get; set; }

        public IEnumerable<SelectListItem> Keys { get; set; }

        internal Content ApplyToDataModel(Content content)
        {
            if (content == null)
                content = new Content();

            content.Id = this.Id;
            content.EmployerId = this.EmployerId;
            content.Key = this.Name;
            content.Value = this.Value;

            return content;
        }
    }
}