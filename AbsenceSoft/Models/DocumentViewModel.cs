﻿using AbsenceSoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models
{
    public class DocumentViewModel
    {
        public DocumentViewModel()
        {

        }

        public DocumentViewModel(Document doc)
            :this()
        {
            this.Id = doc.Id;
            this.Name = doc.FileName;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        
    }
}