﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Attachments
{
    [Serializable]
    public class AttachmentModel
    {
        public String FileId { get; set; }
        public AttachmentType AttachmentType { get; set; }
        public String ContentType { get; set; }
        public Int64 ContentLength { get; set; }
        public String FileName { get; set; }
        public String Description { get; set; }
        public String CreatedDate { get; set; }
    }
}