﻿using AbsenceSoft.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbsenceSoft.Models.Attachments
{
    [Serializable]
    public class CreateAttachmentViewModel
    {
        public string Id { get; set; }
        public AttachmentType AttachmentType { get; set; }
        public String Description { get; set; }
        public bool Public { get; set;}
        public int AttachmentId { get; set; } // index within newly uploaded file list, to implement post processing in javascript
    }
}