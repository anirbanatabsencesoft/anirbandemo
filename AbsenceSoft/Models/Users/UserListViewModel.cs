﻿using AbsenceSoft.Data.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Models.Users
{
    public class UserListViewModel
    {
        public string Role { get; set; }

        public List<Role> Roles { get; set; }

        public IEnumerable<SelectListItem> SelectableRoles { get
            {
                if (Roles == null)
                    return null;

                return Roles.Select(r => new SelectListItem()
                {
                    Text = r.Name,
                    Value = r.Id
                });
            } }
    }
}