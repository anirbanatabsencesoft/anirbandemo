﻿(function (AbsenceSoft, $) {
    AbsenceSoft.PolicyCrosswalkType = AbsenceSoft.PolicyCrosswalkType || {};

    var policyCrosswalkTypeStart = function () {
        if ($('#editPolicyCrosswalkTypeForm').length < 1)
            return;

        $('#editPolicyCrosswalkTypeForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        AbsenceSoft.PageStart('#editPolicyCrosswalkTypeForm');
    }

    AbsenceSoft.PolicyCrosswalkType.Start = policyCrosswalkTypeStart;

    $(document).ready(policyCrosswalkTypeStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);