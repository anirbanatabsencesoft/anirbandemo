﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EmployeeJob = AbsenceSoft.EmployeeJob || {};

  
    AbsenceSoft.EmployeeJob.Init = function (container) {

        var $editors = $('.employee-job-editor', container);
        if ($editors.length == 0)
            $editors = $('.employee-job-status-editor', container);
        if ($editors.length == 0)
            return;

        $editors.each(function (i, m) {
            var $model = $(m);
            var $jobCode = $model.find('.job-code');
            var $dateRow = $model.find('.jobDates');
            var $statusRow = $model.find('.jobStatus');
            var $reasonRow = $model.find('.jobDenialReason');
            var $reason = $reasonRow.find('select');
            var $otherRow = $model.find('.jobDenialReasonOther');

            var $statusSelectedAfterValidation = $model.find('.jobStatus input[type=radio]:checked');

            if ($statusSelectedAfterValidation.val() != "Denied") {
                if ($reasonRow.is(':visible')) {
                    $reasonRow.fadeOut();
                }
                if ($otherRow.is(':visible')) {
                    $otherRow.fadeOut();
                }
            }

            var $status = $model.find('.jobStatus input[type=radio]');
            $jobCode.on('change', function () {
                var $j = $(this);
                if ($j.val()) {
                    $dateRow.fadeIn();
                    $statusRow.fadeIn();
                    if ($reason.val() == '0') {
                        $otherRow.fadeIn();
                    }
                } else {
                    $dateRow.fadeOut();
                    $statusRow.fadeOut();
                    $otherRow.fadeOut();
                }
            });
            $reason.on('change', function () {
                var $r = $(this);
                if ($reason.val() == '0') {
                    $otherRow.fadeIn();
                } else {
                    if ($otherRow.is(':visible')) {
                        $otherRow.fadeOut();
                    }
                }
            });
            $status.on('change', function () {
                if ($(this).val() == "Denied") {
                    $reasonRow.fadeIn();
                    if ($reason.val() == '0') {
                        $otherRow.fadeIn();
                    } else {
                        if ($otherRow.is(':visible')) {
                            $otherRow.fadeOut();
                        }
                    }
                } else {
                    $reasonRow.fadeOut();
                    if ($otherRow.is(':visible')) {
                        $otherRow.fadeOut();
                    }
                }
            });
        });
    };

    $(document).ready(function () {
        AbsenceSoft.EmployeeJob.Init();
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);