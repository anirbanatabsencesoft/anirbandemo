﻿(function (AbsenceSoft, $) {

    var $context = null;
    var employeeRestrictions = [];
    var caseId = null;
    var employeeId = null;
    var caseEndDate = null;
    
    //Suppress Required Validation on hidden fields.
    jQuery.validator.defaults.ignore = ":hidden";
    $(document).ready(function () {
        $context = AbsenceSoft.WorkRelatedViewModel.$context = $('.work-related-editor');
        if ($context.length == 0) {
            return;
        }
        caseEndDate = Date.parse($context.find('#CaseEndDate').val());
        $.validator.addMethod('validateDateRange', function (value, element) {
            var CurrentDate = new Date();
            $.validator.messages.validateDateRange = '';
            var isValid = true;
            if (value) {
                if (Date.parse(value) >= CurrentDate) {
                    isValid = false;
                    $.validator.messages.validateDateRange = 'Injury Date cannot be after the Current Date.';
                }
                if (Date.parse(value) > caseEndDate) {
                    isValid = false;
                    $.validator.messages.validateDateRange = 'Injury Date cannot be after the Case End Date.';
                }
            }
            return this.optional(element) || isValid;
        }, $.validator.messages.validateDateRange);

        $('#IllnessOrInjuryDate').rules('add', {
            validateDateRange: true
        });
        caseId = $context.find('#CaseId').val();
        employeeId = $context.find('#EmployeeId').val();
        $context.find('.sharps-question').on('change', 'input[type=radio]', AbsenceSoft.WorkRelatedViewModel.IsSharpsChanged);
        $context.find('.sharps-protection').on('change', 'input[type=radio]', AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtectionChanged);
        $context.find('.job-classification').on('change', AbsenceSoft.WorkRelatedViewModel.JobClassChanged);
        $context.find('.location-and-dept').on('change', AbsenceSoft.WorkRelatedViewModel.LocationChanged);
        $context.find('.procedure').on('change', AbsenceSoft.WorkRelatedViewModel.ProcedureChanged);
        $context.find('#IllnessOrInjuryDate').on('change', AbsenceSoft.WorkRelatedViewModel.IllnessOrInjuryDateChanged);

        $.ajax({
            url: '/Cases/' + caseId + '/EmployeeRestrictions/' + employeeId,
            success: function (result) {
                employeeRestrictions = result;
            },
            failure: function (err) {

            }
        });
    });

    AbsenceSoft.WorkRelatedViewModel = AbsenceSoft.WorkRelatedViewModel || {};

    AbsenceSoft.WorkRelatedViewModel.IsSharpsChanged = function () {
        var $radio = $(this);
        AbsenceSoft.WorkRelatedViewModel.IsSharps = ($radio.val() === true || $radio.val() === "true" || $radio.val() === "True");
        if (AbsenceSoft.WorkRelatedViewModel.IsSharps) {
            $context.find('.i-have-sharps:hidden').slideDown();
        } else {
            $context.find('.i-have-sharps:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtectionChanged = function () {
        var $radio = $(this);
        AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtection = ($radio.val() === true || $radio.val() === "true" || $radio.val() === "True");
        if (AbsenceSoft.WorkRelatedViewModel.HasEngineeredProtection) {
            $context.find('.HaveEngineeredInjuryProtection:hidden').slideDown();
        } else {
            $context.find('.HaveEngineeredInjuryProtection:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.JobClassChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.job-classification-other:hidden').slideDown();
        } else {
            $context.find('.job-classification-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.LocationChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.location-and-dept-other:hidden').slideDown();
        } else {
            $context.find('.location-and-dept-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.ProcedureChanged = function () {
        var $this = $(this);
        var isOther = $this.val() === "0";
        if (isOther) {
            $context.find('.procedure-other:hidden').slideDown();
        } else {
            $context.find('.procedure-other:visible').slideUp();
        }
    };

    AbsenceSoft.WorkRelatedViewModel.IllnessOrInjuryDateChanged = function () {
        var $this = $(this);
        $('#frmWorkRelatedInfo').validate().element(this);
        AbsenceSoft.WorkRelatedViewModel.CalculateDaysAwayFromWork(($this).val());
        AbsenceSoft.WorkRelatedViewModel.CalculateDaysOnJobRestriction(($this).val());
    };

    AbsenceSoft.WorkRelatedViewModel.CalculateDaysAwayFromWork = function (injuryDate) {
        var dateOfInjury = Date.parse(injuryDate);
        if (isNaN(dateOfInjury))
            dateOfInjury = ''; // No Injury Date
        $.ajax({
            url: '/Cases/' + caseId + '/GetDaysAwayFromWork?dateOfInjury=' + dateOfInjury,
            success: function (result) {
                $('#DaysAwayFromWork').val(result.CalculatedGetDaysAwayFromWork);
                $("#DaysAwayFromWorkCalculated").text(result.CalculatedGetDaysAwayFromWork);
            },
            failure: function (err) {

            }
        });
    }

    AbsenceSoft.WorkRelatedViewModel.CalculateDaysOnJobRestriction = function (injuryDate) {        
        var dateOfInjury = Date.parse(injuryDate);
        if (isNaN(dateOfInjury))
            dateOfInjury = ''; // No Injury Date
        $.ajax({
            url: '/Cases/' + caseId + '/GetCalculatedDaysOnJobTransferOrRestriction?dateOfInjury=' + dateOfInjury,
            success: function (result) {
                $("#CalculatedDaysOnJobTransferOrRestriction").text(result.CalculatedDaysOnJobTransferOrRestriction);
            },
            failure: function (err) {

            }
        });
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);