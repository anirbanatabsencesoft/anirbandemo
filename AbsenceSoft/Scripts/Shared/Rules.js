﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Rules = AbsenceSoft.Rules || {};
    var expressionGroups = [];
    var dataValues = [];
    var getAvailableExpressionGroups = function (employerId, callback) {
        var url = '/RiskProfiles/Expressions/';
        if (employerId)
            url += employerId;

        return $.ajax({
            type: 'GET',
            url:  url,
            success: setAvailableExpressionGroups(callback)
        })
    };

    var setAvailableExpressionGroups = function (callback) {
        return function (data) {
            expressionGroups = $.map(data, function (expressionGroup) {
                expressionGroup.id = expressionGroup.id || expressionGroup.Title;
                expressionGroup.text = expressionGroup.text || expressionGroup.Title;
                return expressionGroup;
            });

            if (callback)
                callback();
        }
    };

    var getExpressionsByGroupName = function (name) {
        for (var i = 0; i < expressionGroups.length; i++) {
            var currentGroup = expressionGroups[i];
            if (currentGroup.Title == name) {
                return $.map(currentGroup.Expressions, function (expression) {
                    expression.id = expression.id || expression.Name;
                    expression.text = expression.text || expression.Prompt;
                    return expression;
                });
            }
        }
    }

    var getExpressionsByGroupName = function (name) {
        for (var i = 0; i < expressionGroups.length; i++) {
            var currentGroup = expressionGroups[i];
            if (currentGroup.Title == name) {
                return $.map(currentGroup.Expressions, function (expression) {
                    expression.id = expression.id || expression.Name;
                    expression.text = expression.text || expression.Prompt;
                    return expression;
                });
            }
        }
    }

    var getExpressionByGroupAndName = function (group, expression) {
        var expressions = getExpressionsByGroupName(group);
        if (!expressions)
            return;

        for (var i = 0; i < expressions.length; i++) {
            var currentExpression = expressions[i];
            if (currentExpression.Name == expression) {
                return currentExpression;
            }
        }
    }

    var getExpressionOperators = function (group, expression) {
        var currentExpression = getExpressionByGroupAndName(group, expression);
        return transformOperators(currentExpression.Operators);
    }

    var transformOperators = function (operatorsObject) {
        var operatorsArray = [];
        for (var property in operatorsObject) {
            if (operatorsObject.hasOwnProperty(property)) {
                var operator = {
                    id: property,
                    text: operatorsObject[property]
                };
                operatorsArray.push(operator);
            }
        }
        return operatorsArray;
    }

    var bindMultiSelect2ForExpression = function (container) {

        if (container != 'undefined') {
            container.find('input.multiplevalues').select2('destroy').select2({
                data: dataValues,
                allowClear: true,
                minimumInputLength: 1,
                dropdownCssClass: "bigdrop",
                multiple: true,
                placeholder: 'Select value'
            });
        }
    }

    var createRuleGroup = function (ruleGroup) {
        if (!ruleGroup) {
            ruleGroup = {
                Name: '',
                SuccessType: '',
                Description: '',
                Rules: []
            }
        }
            
        var ruleGroupContainer = '<div class="rule-group-container">' +
            '<div class="row">' +
                '<div class="col-xs-4">' +
                    '<div class="row">' +
                        '<div class="col-xs-4">RULE GROUP:</div>' +
                        '<div class="col-xs-8"><input type="text" class="form-control rule-group-name" placeholder="Rule Group Name" name="RuleGroupName" value="' + ruleGroup.Name + '" /></div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-4">SUCCESS TYPE:</div>' +
                        '<div class="col-xs-8"><input type="hidden" class="form-control rule-group-success-type" value="' + ruleGroup.SuccessType + '" /></div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-6">' +
                    '<textarea class="form-control rule-group-description" placeholder="Rule Group Description" name="RuleGroupDescription">' + ruleGroup.Description + '</textarea>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<button class="btn btn-primary btn-danger delete-rule-group">Delete Group</button><br />' +
                    '<button class="btn btn-primary addnew-admin-btn add-rule">New Rule</button>' +
                '</div>';

        var rulesHtml = '';
        for (var i = 0; i < ruleGroup.Rules.length; i++) {
            rulesHtml += createRule(ruleGroup.Rules[i]);
        }
        var rulesContainer = '<div class="col-xs-12 rules-container">' + rulesHtml + '</div>';
        ruleGroupContainer += rulesContainer;
        ruleGroupContainer += '</div>';
        return ruleGroupContainer;
    }

    var createRule = function (rule) {
        if (!rule) {
            rule = {
                Group: '',
                RuleName: '',
                Name: '',
                StringValue: '',
                Operator: '',
                Value: '',
            };
        };

        var ruleContainer =
            '<div class="rule-container admin-rule-group">' +
                '<div class="form-group">' +
                    '<div class="col-xs-1">Rule Name</div>' +
                    '<div class="col-xs-8"><input type="text" class="form-control rule-name" name="RuleName" value="' + rule.RuleName + '" /></div>' +
                    '<div class="col-xs-3 text-left"><button class="btn btn-primary btn-danger btn-remove-rule">Remove Rule</button></div>' +
                '</div>' +
                '<div class="form-group">' +
                    '<div class="col-xs-3"><input type="hidden" class="rule-group form-control" value="' + rule.Group + '" /></div>' +
                    '<div class="col-xs-3"><input type="hidden" class="rule-expression form-control" value="' + rule.Name + '" /></div>' +
                    '<div class="col-xs-3"><input type="hidden" class="rule-operator form-control" value="' + rule.Operator + '" /></div>' +
                    '<div class="col-xs-3 rule-value-container">' + buildRuleInput(rule) + "</div>" +
                '</div>' +
            '</div>';

        return ruleContainer;
    }

    var buildRuleInput = function (rule) {
        var expression = getExpressionByGroupAndName(rule.Group, rule.Name);
        if (!expression)
            return '';

        switch (expression.ControlType) {
            case 1:
                /// text box
                return '<input type="text" class="rule-value form-control" value="' + rule.Value + '" />';
            case 2:
                var select = '<select class="rule-value form-control select-list"><option>Select A Value</option>';
                for (var i = 0; i < expression.Options.length; i++) {
                    var currentOption = expression.Options[i];
                    if (currentOption.Value == rule.Value) {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    } else {
                        select += '<option value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                }

                select += '</select>';

                return select;
                /// select list
            case 4:
                /// date
                return '<input type="text" class="rule-value form-control date-editor" value="' + rule.Value + '" />';
            case 6:
                /// numeric
                return '<input type="number" class="rule-value form-control order-nopad" value="' + rule.Value + '" />';
            case 8:
                /// checkbox
                if (expression.Value) {
                    return '<input type="checkbox" class="rule-value form-control" name="RuleValue" checked />';
                } else {
                    return '<input type="checkbox" class="rule-value form-control" name="RuleValue" />';
                }
            case 12:
                for (var i = 0; i < expression.Options.length; i++) {
                    var currentOption = expression.Options[i];
                    dataValues.push({ id: currentOption.Value, text: currentOption.Text });
                }
                return '<div class="select2-wrap"><input type="text" class="rule-value form-control select2 bigdrop multiplevalues" id="expressionValues" value="' + rule.Value + '" /></div>';
            default:
                return '';
        }
    };

    var bindRuleGroupSuccessType = function () {
        $('.rule-group-success-type').select2('destroy')
        .select2({
            data: [
                { id: 0, text: 'All Rules Must Pass' },
                { id: 1, text: 'One Rule Must Pass' },
                { id: 2, text: 'All Rules Must Fail' },
                { id: 3, text: 'One Rule Must Fail' },
            ],
            placeholder: 'Select A Success Type'
        })
    }

    var bindOperators = function () {
        $('.rule-operator').each(function (i, operator) {
            createSelect2ForOperators(operator);
        });
    }

    

    var createSelect2ForOperators = function (operator) {
        var ruleContainer = $(operator).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var expression = ruleContainer.find('.rule-expression').not('.select2-container').val();
        var expressionOperators = getExpressionOperators(group, expression);
        ruleContainer.find('.rule-operator')
            .select2('destroy')
            .select2({
                data: expressionOperators,
                placeholder: 'Select An Operator'
            });
    }

    var bindExpressions = function () {
        $('.rule-expression').each(function (i, expression) {
            createSelect2ForExpressions(expression);
        });
    }

    var createSelect2ForExpressions = function (expression) {
        var ruleContainer = $(expression).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var groupExpressions = getExpressionsByGroupName(group);
        ruleContainer.find('.rule-expression')
        .select2('destroy')
        .select2({
            data: groupExpressions,
            placeholder: 'Select An Expression'
        })
        .change(expressionChanged);
        bindMultiSelect2ForExpression(ruleContainer);
    }

    var expressionChanged = function () {
        createSelect2ForOperators(this);
        var ruleContainer = $(this).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var expression = $(this).val();
        var rule = {
            Group: group,
            Name: expression,
            Value: ''
        };
        ruleContainer.find('.rule-value-container').html(buildRuleInput(rule));
        bindMultiSelect2ForExpression(ruleContainer);
        AbsenceSoft.PageStart(ruleContainer);
    };

    var bindSelect2Values = function () {
        $('.select-list.rule-value')
            .not('.select2-container')
            .not('.select2-offscreen').select2();
    }

    var removeRuleGroup = function () {
        $(this).parents('.rule-group-container').remove();
        $('#editRiskProfileForm').trigger('checkform.areYouSure');
        return false;
    }

    var addRule = function () {
        $(this).parents('.rule-group-container').find('.rules-container').append(createRule());
        bindRulesEngine();
        $("#editRiskProfileForm").trigger('rescan.areYouSure');
        return false;
    }

    var bindRulesEngine = function () {
        bindRuleGroupSuccessType();
        $('.delete-rule-group').unbind().click(removeRuleGroup);
        $('.add-rule').unbind().click(addRule);
        $('.btn-remove-rule').unbind().click(removeRule);
        /// Only bind to the ones that haven't been bound to yet
        $('.rule-group')
            .not('.select2-offscreen')
            .not('.select2-container')
            .select2({
                data: expressionGroups,
                placeholder: 'Select A Group'
            })
        .change(ruleGroupChanged);
    };

    var removeRule = function () {
        $(this).parents('.rule-container').remove();
        $('#editRiskProfileForm').trigger('checkform.areYouSure');
        return false;
    }

    var ruleGroupChanged = function () {
        createSelect2ForExpressions(this);
        var ruleContainer = $(this).parents('.rule-container');
        ruleContainer.find('.rule-operator').select2('destroy');
        ruleContainer.find('.rule-value-container').html('');
    };

    var ruleGroupsValid = function () {
        var ruleGroupsValid = true;
        $('.rule-group-container').each(function (i, ruleGroup) {
            var $ruleGroup = $(ruleGroup);
            ruleGroupsValid = ruleGroupsValid && checkValidity($ruleGroup, '.rule-group-name');
            ruleGroupsValid = ruleGroupsValid && checkValidity($ruleGroup, '.rule-group-success-type');
            ruleGroupsValid = ruleGroupsValid && checkValidity($ruleGroup, '.rule-group-description');
            ruleGroupsValid = ruleGroupsValid && rulesValid($ruleGroup);
        });

        if (!ruleGroupsValid) {
            $('#rulesInvalid').removeClass('hide');
        } else {
            $('#rulesInvalid').addClass('hide');
        }

        return ruleGroupsValid;
    }

    var rulesValid = function (ruleGroup) {
        var rulesValid = true;
        ruleGroup.find('.rule-container').each(function (i, rule) {
            var $rule = $(rule);
            rulesValid = rulesValid && checkValidity($rule, '.rule-name');
            rulesValid = rulesValid && checkValidity($rule, '.rule-group');
            rulesValid = rulesValid && checkValidity($rule, '.rule-expression');
            rulesValid = rulesValid && checkValidity($rule, '.rule-operator');
            rulesValid = rulesValid && checkValidity($rule, '.rule-value');
        });

        return rulesValid;
    };

    var checkValidity = function (rule, selector) {
        var value = rule.find(selector).not('.select2-container').val();
        if (!value) {
            rule.find(selector).addClass('admin-rule-invalid');
            return false;
        } else {
            rule.find(selector).removeClass('admin-rule-invalid');
            return true;
        }
    }

    var gatherRuleGroups = function () {
        var ruleGroups = []
        $('.rule-group-container').each(function (i, ruleGroup) {
            var $ruleGroup = $(ruleGroup);
            var group = {
                Name: $ruleGroup.find('.rule-group-name').val(),
                SuccessType: $ruleGroup.find('.rule-group-success-type').not('.select2-container').val(),
                Description: $ruleGroup.find('.rule-group-description').val(),
                Rules: gatherRules($ruleGroup)
            }
            ruleGroups.push(group);
        })

        return ruleGroups;
    }

    var gatherRules = function (parent) {
        var rules = [];
        parent.find('.rule-container').each(function (i, rule) {
            var $rule = $(rule);
            var value = $rule.find('.rule-value').not('.select2-container');
            var selectedValue = null;
            if (value.is(':checkbox'))
                selectedValue = value.is(':checked');
            else
                selectedValue = value.val();

            var rule = {
                Group: $rule.find('.rule-group').not('.select2-container').val(),
                RuleName: $rule.find('.rule-name').val(),
                Name: $rule.find('.rule-expression').not('.select2-container').val(),
                Operator: $rule.find('.rule-operator').not('.select2-container').val(),
                StringValue: selectedValue
            };
            rules.push(rule);
        });

        return rules;
    };

    var bindExistingRuleGroups = function () {
        bindRuleGroupSuccessType();
        bindSelect2Values();
        bindOperators();
        bindExpressions();
        bindRulesEngine();
    };

    AbsenceSoft.Rules.BindExistingRuleGroups = bindExistingRuleGroups;
    AbsenceSoft.Rules.GetExpressionGroups = getAvailableExpressionGroups;
    AbsenceSoft.Rules.CreateRuleGroup = createRuleGroup;
    AbsenceSoft.Rules.BindOperators = bindOperators;
    AbsenceSoft.Rules.BindExpressions = bindExpressions;
    AbsenceSoft.Rules.BindSelect2Values = bindSelect2Values;
    AbsenceSoft.Rules.BindRulesEngine = bindRulesEngine;
    AbsenceSoft.Rules.RuleGroupsValid = ruleGroupsValid;
    AbsenceSoft.Rules.GatherRuleGroups = gatherRuleGroups;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery)