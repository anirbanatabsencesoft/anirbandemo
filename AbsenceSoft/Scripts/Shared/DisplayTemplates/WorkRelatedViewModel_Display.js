﻿(function (AbsenceSoft, $) {


    AbsenceSoft.WorkRelatedViewModel = AbsenceSoft.WorkRelatedViewModel || {};

    AbsenceSoft.WorkRelatedViewModel.ShowMore = function () {
        $('#work-related-more').slideDown();
        $('#work-related-more-link').hide();
        $('#work-related-less-link').show();
    };

    AbsenceSoft.WorkRelatedViewModel.ShowLess = function () {
        $('#work-related-more').slideUp();
        $('#work-related-less-link').hide();
        $('#work-related-more-link').show();
    };


    var $context = null;

    $(document).ready(function () {
        $context = AbsenceSoft.WorkRelatedViewModel.$context = $('.work-related-display');
        if ($context.length == 0)
            return;

      

     
      
    });

   

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);