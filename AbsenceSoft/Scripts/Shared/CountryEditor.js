﻿(function (AbsenceSoft, $) {
    var countries = [];
    var getCountryByCountryCode = function (countryCode) {
        for (var i = 0; i < countries.length; i++) {
            var country = countries[i];
            if (country.Code == countryCode)
                return country;
        }

        return undefined;
    };

    var loadCountries = function () {
        return $.ajax({
            type: 'GET',
            url: '/Lookups/ListCountriesAndRegions',
            success: function (data) {
                countries = data;
            }
        });
    };

    var buildRegionDropDown = function (currentEditor, country) {
        var regionName = country.RegionName || "Region";

        var dropDownList = '<select name="' + currentEditor.attr('name')
            + '" class="' + currentEditor.attr('class')
            + '" id="' + currentEditor.attr('id')
            + '" placeholder="' + regionName + '">'
            + '<option>Select a ' + regionName + '</option>';

        var regions = country.Regions;
        for (var i = 0; i < regions.length; i++) {
            var region = regions[i];
            var option = '<option value="' + region.Code + '">' + region.Name + '</option>';
            dropDownList += option;
        }

        dropDownList += '</select>';

        return dropDownList;
    }

    var buildRegionTextBox = function (currentEditor, country) {
        var regionName = country.RegionName || "Region";
        return '<input type="text" name="' + currentEditor.attr('name')
            + '" class="' + currentEditor.attr('class')
            + '" id="' + currentEditor.attr('id')
            + '" placeholder="' + regionName + '" />';
    }

    var buildRegionLabel = function (currentLabel, country) {
        var regionName = country.RegionName || "Region";
        console.log(regionName);
        return '<label class="' + currentLabel.attr('class')
            + '" id="' + currentLabel.attr('id')
            + '" for="' + currentLabel.attr('for')
            + '">' + regionName + '</label>';
    }

    var buildRegionEditorByCountry = function (countryCode, currentEditor) {
        var country = getCountryByCountryCode(countryCode);
        if (!country)
            return currentEditor;

        var regions = country.Regions;
        if (regions.length > 0)
            return buildRegionDropDown(currentEditor, country);
        else
            return buildRegionTextBox(currentEditor, country);

    }

    var buildRegionLabelByCountry = function (countryCode, currentLabel) {
        var country = getCountryByCountryCode(countryCode);
        console.log(country);
        if (!country)
            return currentLabel;

        return buildRegionLabel(currentLabel, country);
    }

    var getRegionEditorByCountry = function (country, currentEditor) {
        if (countries.length == 0) {
            loadCountries().done(function () {
                var regionEditor = buildRegionEditorByCountry(country, currentEditor);
                currentEditor.replaceWith(regionEditor);
            });
        } else {
            var regionEditor = buildRegionEditorByCountry(country, currentEditor);
            currentEditor.replaceWith(regionEditor);
        }
    };

    var getRegionLabelByCountry = function (country, currentLabel) {
        if (countries.length == 0) {
            loadCountries().done(function () {
                var regionLabel = buildRegionLabelByCountry(country, currentLabel);
                currentLabel.replaceWith(regionLabel);
            });
        } else {
            var regionLabel = buildRegionLabelByCountry(country, currentLabel);
            currentLabel.replaceWith(regionLabel);
        }
    }


    AbsenceSoft.CountryEditor = AbsenceSoft.CountryEditor || {};
    AbsenceSoft.CountryEditor.GetRegionEditorByCountry = getRegionEditorByCountry;
    AbsenceSoft.CountryEditor.GetRegionLabelByCountry = getRegionLabelByCountry;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);