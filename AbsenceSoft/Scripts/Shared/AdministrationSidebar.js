﻿(function (AbsenceSoft, $) {
    var filterEmployerList = function () {
        var filterText = $(this).val().toLowerCase();
        var matchingEmployers = 0;
        $('.employer-administration').addClass('hide');
        $('.employer-link').each(function (i, link) {
            var $link = $(link);
            var matchesFilter = $link.text().toLowerCase().indexOf(filterText) > -1 || !filterText;
            if (matchingEmployers < 10 && matchesFilter) {
                $link.parents('.employer-administration').removeClass('hide');
                matchingEmployers++;
            }
        });
    }

    $(document).ready(function () {
        $('#employerSearch').keyup(filterEmployerList);
    })

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);