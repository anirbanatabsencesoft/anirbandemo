﻿(function (AbsenceSoft, $) {
    var showCancelToDo = function () {
        $("#cancelToDoForm").removeClass('hide');
    };


    var cancelToDo = function (event) {
        var form = $(this);
        if (!form.valid())
            return;

        var token = $('input[name="__RequestVerificationToken"]').first().val();

        var todo = {
            Id: $('#canceledToDoId').val(),
            Status: 3,
            CancelReason: $('#cancelToDoReason').val()
        };

        // other headers omitted
        
        $.ajax({
            type: "POST",
            url: '/Todos/ChangeStatus',
            data: todo,            
            beforeSend: function (xhr) { xhr.setRequestHeader("X-XSRF-Token", token); },
            success: function (data) {
                history.back();
            },
            error: function (message) {
                alert(JSON.stringify(message));
            }
        });

        event.preventDefault();
        return false;
    };

    $(document).ready(function () {
        $('#cancelThisToDo').click(showCancelToDo); 
        $('#cancelToDoForm').submit(cancelToDo); 
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);