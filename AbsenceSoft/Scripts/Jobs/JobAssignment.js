﻿(function (AbsenceSoft, $) {
    var currentAvailableRows = 3;
    var currentUnavailableRows = 3;

    AbsenceSoft.JobAssignment = AbsenceSoft.JobAssignment || {};

    $(function () {
        $('#bodyAvailableJobs tr:lt(3)').show();

        if ($("#bodyUnavailableJobs tr").length == 0) {
            $("#divUnavailableJobs").hide();
        }
        else {
            $('#bodyUnavailableJobs tr:lt(3)').show();

            if (currentUnavailableRows >= $("#bodyUnavailableJobs tr").length) {
                $("#btnViewMoreUnavailableJobs").hide();
            }
        }

        if (currentAvailableRows >= $("#bodyAvailableJobs tr").length) {
            $("#btnViewMoreAvailableJobs").hide();
        }


        $("#btnViewMoreAvailableJobs").click(function () {

            var count = $("#bodyAvailableJobs tr").length;

            if (count > currentAvailableRows)
                $('#bodyAvailableJobs tr').slice(0, currentAvailableRows + 3).show();
            else
                return;

            currentAvailableRows = currentAvailableRows + 3;

            if (currentAvailableRows >= count) {
                $("#btnViewMoreAvailableJobs").hide();
            }
        });

        $("#btnViewMoreUnavailableJobs").click(function () {

            var count = $("#bodyUnavailableJobs tr").length;

            if (count > currentAvailableRows)
                $('#bodyUnavailableJobs tr').slice(0, currentUnavailableRows + 3).show();
            else
                return;

            currentUnavailableRows = currentUnavailableRows + 3;

            if (currentUnavailableRows >= count) {
                $("#btnViewMoreUnavailableJobs").hide();
            }
        });
    });


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);