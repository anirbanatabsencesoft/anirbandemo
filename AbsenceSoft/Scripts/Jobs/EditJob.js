﻿(function (AbsenceSoft, $) {
    var showInvalidTab = function () {
        var invalidFields = $('.input-validation-error');
        if (invalidFields.length > 0)
        {
            var firstInvalid = invalidFields[0];
            var tab = $(firstInvalid).parents('.tab-pane');
            $('#' + tab.attr('id') + '-tab').tab('show');
        }
    }


    $(document).ready(function () {
        $('#editJobForm').submit(showInvalidTab)
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);