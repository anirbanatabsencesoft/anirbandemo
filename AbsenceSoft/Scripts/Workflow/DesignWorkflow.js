﻿(function (AbsenceSoft, $) {
    AbsenceSoft.DesignWorkflow = AbsenceSoft.DesignWorkflow || {};
    var contactTypes = [];
    var assigneeTypes = [];
    var toolkit = {};
    var renderer = {};
    var nodeData = {};
    var node = {};

    var nodeIdFunction = function (n) {
        return n.id;
    }

    var nodeTypeFunction = function (n) {
        return n.type;
    }

    var toolkitConfiguration = {
        idFunction: nodeIdFunction,
        typeFunction: nodeTypeFunction,
        nodeFactory: function (type, data, callback) {
            callback(data);
        }
    };

    var nodeConfiguration = {
        droppables: [],
        dragOptions: {
            zIndex: 50000,
            cursor: "move",
            clone: true
        },
        typeExtractor: function (el) {
            return el.getAttribute("jtk-node-type").toLowerCase();
        },
        dataGenerator: function (type, draggedElement) {
            /// every 15 characters should add another 120 pixels of width
            var text = draggedElement.innerHTML.trim();
            var width = calculateNodeWidth(text);
            return {
                text: text,
                activityId: draggedElement.getAttribute("jtk-activity-id"),
                w: width,
                h: 80
            };
        }
    }

    var calculateNodeWidth = function (text) {
        return (Math.floor(text.length / 15) + 1) * 120;
    }

    var rendererConfiguration = {
        container: "workflow-designer",
        view: {
            nodes: {
                "start": {
                    template: "tmplStart",
                    parameters: {
                        w: 50,
                        h: 50
                    }
                },
                "selectable": {
                    events: {
                        tap: function (params) {
                            toolkit.toggleSelection(params.node);
                        }
                    }
                },
                "question": {
                    parent: "selectable",
                    template: "tmplQuestion",
                    parameters: {
                        w: 120,
                        h: 120
                    }
                },
                "action": {
                    parent: "selectable",
                    template: "tmplAction",
                    parameters: {
                        w: 120,
                        h: 70
                    }
                },
                "output": {
                    parent: "selectable",
                    template: "tmplOutput",
                    parameters: {
                        w: 120,
                        h: 70
                    }
                }
            },
            // There are two edge types defined - 'yes' and 'no', sharing a common
            // parent.
            edges: {
                "default": {
                    connector: ["Flowchart", { cornerRadius: 5 }],
                    paintStyle: { lineWidth: 2, strokeStyle: "#f76258", outlineWidth: 3, outlineColor: "transparent" },	//	paint style for this edge type.
                    hoverPaintStyle: { lineWidth: 2, strokeStyle: "rgb(67,67,67)" }, // hover paint style for this edge type.
                    events: {
                        "dblclick": function (params) {
                            jsPlumbToolkit.Dialogs.show({
                                id: "dlgConfirm",
                                data: {
                                    msg: "Delete Connection"
                                },
                                onOK: function () {
                                    toolkit.removeEdge(params.edge);
                                }
                            });
                        }
                    },
                    overlays: [
                        ["Arrow", { location: 1, width: 10, length: 10 }],
                        ["Arrow", { location: 0.3, width: 10, length: 10 }]
                    ]
                },
                "connection": {
                    parent: "default",
                    overlays: [
                        [
                            "Label", {
                                label: "${outcome}",
                                events: {
                                    click: function (params) {
                                        selectOutcome(params.edge, params.edge.source.data.activityId);
                                    }
                                }
                            }
                        ]
                    ]
                }
            },

            ports: {
                "start": {
                    endpoint: "Blank",
                    anchor: "Continuous",
                    uniqueEndpoint: true,
                    edgeType: "default"
                },
                "source": {
                    endpoint: "Blank",
                    paintStyle: { fillStyle: "#84acb3" },
                    anchor: "AutoDefault",
                    maxConnections: -1,
                    edgeType: "connection"
                },
                "target": {
                    maxConnections: -1,
                    endpoint: "Blank",
                    anchor: "AutoDefault",
                    paintStyle: { fillStyle: "#84acb3" },
                    isTarget: true
                }
            }
        },
        // Layout the nodes using an absolute layout
        layout: {
            type: "Absolute"
        },
        events: {
            canvasClick: function (e) {
                toolkit.clearSelection();
            },
            edgeAdded: function (params) {
                if (params.addedByMouse && params.source.data.type != "start") {
                    selectOutcome(params.edge, params.source.data.activityId);
                }
            }
        },
        miniview: {
            container: "miniview"
        },
        consumeRightClick: false,
        dragOptions: {
            filter: ".jtk-draw-handle, .node-action, .node-action i"
        }
    };


    var selectOutcome = function (edge, activityId) {
        jsPlumbToolkit.Dialogs.show({
            id: "dlgOutcome",
            data: {
                text: edge.data.outcome || "",
                list: getPossibleOutcomes(activityId, edge.data.outcome)
            },
            onOK: function (data) {
                toolkit.updateEdge(edge, { outcome: data.outcome });
            },
            onCancel: function () {
                if (!edge.data.outcome)
                    toolkit.removeEdge(edge);
            }
        });
    };

    var getPossibleOutcomes = function (edge, activityId) {
        jsPlumbToolkit.Dialogs.show({
            id: "dlgOutcome",
            data: {
                text: edge.data.outcome || "",
                list: getPossibleOutcomes(activityId, edge.data.outcome)
            },
            onOK: function (data) {
                toolkit.updateEdge(edge, { outcome: data.outcome });
            },
            onCancel: function () {
                if (!edge.data.outcome)
                    toolkit.removeEdge(edge);
            }
        });
    };

    var getPossibleOutcomes = function (activityId, selectedItem) {
        for (var i = 0; i < AbsenceSoft.DesignWorkflow.Activities.length; i++) {
            var activity = AbsenceSoft.DesignWorkflow.Activities[i];
            if (activity.Id == activityId)
                return transformOutcomes(activity.PossibleOutcomes, selectedItem);
        }

        return [];
    }

    var transformOutcomes = function (outcomes, selectedItem) {
        var transformedOutcomes = [];
        for (var i = 0; i < outcomes.length; i++) {
            transformedOutcomes.push({
                id: outcomes[i],
                value: outcomes[i],
                selected: (outcomes[i] == selectedItem)
            })
        }

        return transformedOutcomes;
    }

    var initDesigner = function () {
        getWorkflows();
        getCommunications();
        getContactTypes();
        getCaseAssigneeTypes();
        getQuestionActivityOutcomes();
        AbsenceSoft.WorkflowRules.GetExpressionGroups($('#targetEventType').val(), $('#employerId').val());
        jsPlumbToolkit.ready(function () {
            var newToolkit = jsPlumbToolkit.newInstance(toolkitConfiguration);
            var newRenderer = window.renderer = newToolkit.render(rendererConfiguration);
            nodeConfiguration.droppables = jsPlumb.getSelector('.dev-activity');
            newRenderer.registerDroppableNodes(nodeConfiguration);
            jsPlumbToolkit.Dialogs.initialize({
                selector: ".dlg"
            });

            newRenderer.bind("modeChanged", function (mode) {
                jsPlumb.removeClass(jsPlumb.getSelector("[mode]"), "selected-mode");
                jsPlumb.addClass(jsPlumb.getSelector("[mode='" + mode + "']"), "selected-mode");
            });

            // pan mode/select mode
            jsPlumb.on(".controls", "tap", "[mode]", function () {
                newRenderer.setMode(this.getAttribute("mode"));
            });

            // on home button click, zoom content to fit.
            jsPlumb.on(".controls", "tap", "[reset]", function () {
                newToolkit.clearSelection();
                newRenderer.zoomToFit();
            });

            toolkit = newToolkit;
            renderer = newRenderer;
            jsPlumb.on("#workflow-designer", "tap", ".node-delete, .node-delete i", deleteNode);
            jsPlumb.on("#workflow-designer", "tap", ".node-edit, .node-edit i", editNodeConfiguration);
            populateDesignerData();

        });
    }

    var getQuestionActivityOutcomes = function () {
        if (AbsenceSoft.DesignWorkflow.DesignerData != undefined) {
            if (AbsenceSoft.DesignWorkflow.DesignerData.nodes.length > 0) {
                $.each(AbsenceSoft.DesignWorkflow.DesignerData.nodes, function (index, object) {
                    if (object.activityId == "QuestionActivity") {
                        $.each(AbsenceSoft.DesignWorkflow.Activities, function (activityIndex, obj) {
                            if (obj.Id == "QuestionActivity" && object.Question != undefined) {
                                AbsenceSoft.DesignWorkflow.Activities[activityIndex].PossibleOutcomes = object.Question.AnswerOptions;
                                //console.log(AbsenceSoft.DesignWorkflow.Activities[activityIndex].PossibleOutcomes);
                            }
                        });
                    }
                });
            }
        }
    };
    var populateDesignerData = function () {
        if (AbsenceSoft.DesignWorkflow.DesignerData) {
            toolkit.load({
                data: AbsenceSoft.DesignWorkflow.DesignerData
            });
        }
    }

    var getCommunications = function () {
        $.ajax({
            type: 'GET',
            url: '/Lookups/GetPacketTypes',
            success:populateCommunications
        })
    }

    var populateCommunications = function (communications) {
        var transformedCommunications = [];
        for (var i = 0; i < communications.length; i++) {
            var comm = communications[i];
            transformedCommunications.push({
                id: comm.Value,
                text: comm.Text
            });
        }
        $('.activity-template-code').select2({
            allowClear: true,
            dropdownCssClass: "bigdrop",
            multiple: false,
            data: transformedCommunications
        });
    }

    var getCaseAssigneeTypes = function () {
        $.ajax({
            type: 'GET',
            url: '/Lookups/CaseAssigneeTypes',
            success: populateCaseAssigneeTypes
        });
    }

    var populateCaseAssigneeTypes = function (caseAssigneeTypes) {
        var transformedAssigneeTypes = [];
        for (var i = 0; i < caseAssigneeTypes.length; i++) {
            var type = caseAssigneeTypes[i];
            transformedAssigneeTypes.push({
                id: type.Code,
                text: type.Name
            });
        }
        $('.activity-assignee-type-code').select2({
            allowClear: true,
            dropdownCssClass: "bigdrop",
            multiple: false,
            data: transformedAssigneeTypes
        });
        $('.activity-assignee-type-codes').select2({
            allowClear: true,
            dropdownCssClass: "bigdrop",
            multiple: false,
            data: transformedAssigneeTypes
        });
    }

    var getContactTypes = function () {
        $.ajax({
            type: 'GET',
            url: '/Lookups/AdminContactTypes',
            success: populateContactTypes
        })
    }

    var populateContactTypes = function (adminContactTypes) {
        var transformedContactTypes = [{
            id: "Self",
            text: "Employee"
        }];
        for (var i = 0; i < adminContactTypes.length; i++) {
            var type = adminContactTypes[i];
            transformedContactTypes.push({
                id: type.Code,
                text: type.Name
            });
        }
        contactTypes = transformedContactTypes;
    }

    var getWorkflows = function () {
        $.ajax({
            type: 'GET',
            url: '/Workflow/ListAll/' + $('#employerId').val(),
            success: populateWorkflows
        })
    }

    var populateWorkflows = function (workflows) {
        var transformedWorkflows = [];
        for (var i = 0; i < workflows.length; i++) {
            var workflow = workflows[i];
            transformedWorkflows.push({
                id: workflow.Code,
                text: workflow.Name
            });
        }
        $('.activity-workflow-code').select2({
            allowClear: true,
            dropdownCssClass: "bigdrop",
            multiple: false,
            data: transformedWorkflows
        });
    }

    var deleteNode = function () {
        var info = renderer.getObjectInfo(this);
        jsPlumbToolkit.Dialogs.show({
            id: "dlgConfirm",
            data: {
                msg: "Delete '" + info.obj.data.text + "'"
            },
            onOK: function () {
                toolkit.removeNode(info.obj);
            }
        });
    }

    var getNodeId = function () {
        return "#dlg" + nodeData.activityId;
    }

    var editNodeConfiguration = function () {
        var info = renderer.getObjectInfo(this);
        node = info.obj
        nodeData = info.obj.data;
        var id = getNodeId();
        $(id).modal('show');
        populateConfigurationData(id);
    };

    var populateConfigurationData = function (nodeId) {
        $('.activity-time', nodeId).val(nodeData.Time);
        $('.activity-units', nodeId).val(nodeData.Units);
        $('.activity-target-due-date', nodeId).val(nodeData.TargetDueDate);
        $('.activity-title', nodeId).val(nodeData.Title);
        $('.activity-priority', nodeId).val(nodeData.Priority);
        $('.activity-hide-days', nodeId).val(nodeData.HideDays);
        $('.activity-target-hide-date', nodeId).val(nodeData.TargetHideDate);
        $('.activity-note-template', nodeId).val(nodeData.Template);
        $('.activity-note-category', nodeId).val(nodeData.Category);
        $('.activity-item-type', nodeId).val(nodeData.ToDoItemType);
        $('.activity-update-all', nodeId).val(nodeData.UpdateAll);
        $('.activity-status', nodeId).val(nodeData.Status);
        $('.activity-where-status', nodeId).val(nodeData.WhereStatus);
        $('.activity-result-text', nodeId).val(nodeData.WhereResultText);
        $('.activity-template-code', nodeId).select2('val', nodeData.TemplateCode);
        $('.activity-workflow-code', nodeId).select2('val', nodeData.WorkflowCode);
        $('.activity-assignee-type-code', nodeId).select2('val', nodeData.CaseAssigneeTypeCode);
        $('.activity-assignee-type-codes', nodeId).select2('val', nodeData.CaseAssigneeTypeCodes);
        $('.activity-send-method', nodeId).val(nodeData.DefaultSendMethod);

        // Webhooks
        $('.activity-endpoint-url', nodeId).val(nodeData.EndpointUrl);
        $('.activity-event-name', nodeId).val(nodeData.EventName);
        $('.activity-friendly-description', nodeId).val(nodeData.FriendlyDescription);
        if (nodeData["CustomHeaders"]  && Object.keys(JSON.parse(nodeData["CustomHeaders"])).length > 0) 
        {
            if ($('div .row .namevalueTemplate').length > 3) {
                deteleExtraRowforCustomHeader();
            }
            var obj = JSON.parse(nodeData["CustomHeaders"]);
            var counter = 0;
            $.each(obj, function (key, value) {
                if (counter > 2) {
                    addNewNameValueTemplate();
                }
                $("div .namevalueTemplate input[class*= header-name]")[counter].value = key;
                $("div .namevalueTemplate input[class*= header-value]")[counter].value = value;
                counter++;
            });
        }
        else {
            deteleExtraRowforCustomHeader();
        }   
        populateCheckboxConfiguration('.activity-optional', nodeId, nodeData.Optional);
        populateCheckboxConfiguration('.activity-unique', nodeId, nodeData.Unique);
        populateCheckboxConfiguration('.activity-note-confidential', nodeId, nodeData.Confidential);
        populateCheckboxConfiguration('.activity-note-apply-to-spouse', nodeId, nodeData.Spouse);
        populateCheckboxConfiguration('.activity-close-all', nodeId, nodeData.CloseAll);
        populateCheckboxConfiguration('.activity-close-automatically', nodeId, nodeData.CloseAutomatically);
        populateCheckboxConfiguration('.activity-approve-automatically', nodeId, nodeData.ApproveAutomatically);
        populateCheckboxConfiguration('.activity-send-automatically', nodeId, nodeData.SendAutomatically);
        populateCheckboxConfiguration('.activity-autocomplete', nodeId, nodeData.Autocomplete);

        populateSelect2Configuration('.activity-email-to', nodeId, nodeData.To, 'Contacts');
        populateSelect2Configuration('.activity-email-cc', nodeId, nodeData.CC, 'Contacts');
        if (nodeData.activityId == "QuestionActivity") {
            populateQuestionConfiguration(nodeId);
        }
        else { populateRulesConfiguration(nodeId); }

        toggleSendAutomaticallyAvailable(nodeData);
        //toggleSendAutomaticallyInformation();
    };

    var populateQuestionConfiguration = function (context) {
        $('#question').val('');
        $('#answer1').val('');
        $("#tableAnswerOption").html('');
        $("#AddAnswerRowError").addClass("hide");
        if (nodeData.Question) {
            $('#question').val(nodeData.Question.Title);
            if (nodeData.Question.AnswerOptions.length > 0) {
                $.each(nodeData.Question.AnswerOptions, function (index, object) {
                    if (nodeData.Question.AnswerOptions[index] !== "") {

                        var ansOptionCount = $("#tableAnswerOption button").length;
                        var buttonId = 0;
                        var htmlRow = $("#tableAnswerOption").html();
                        if (ansOptionCount < 10) {
                            if (ansOptionCount > 0) {
                                if ($("#btnAnswer_" + ansOptionCount).length > 0) {
                                    buttonId = ansOptionCount + 1;
                                }
                                else {
                                    buttonId = ansOptionCount;
                                }

                            }
                            else { buttonId = buttonId + 1; }
                            htmlRow += '<button class="btn" style="margin: 10px;" id="btnAnswer_' + buttonId + '" value="' + nodeData.Question.AnswerOptions[index] + '">' + nodeData.Question.AnswerOptions[index] + ' &nbsp; &nbsp; <i class="fa fa-times" aria-hidden="true"  onClick="AbsenceSoft.DesignWorkflow.removeAnsweroption(' + buttonId + ')"></i> </button>';
                            $("#tableAnswerOption").html(htmlRow);
                        }
                    }
                });
            }
        }
    };
    var deteleExtraRowforCustomHeader = function () {
        for (i = 0; i < $('div .row .namevalueTemplate').length;) {
            if (i > 2) {
                $("div .namevalueTemplate")[i].remove();
                i = 3;
            }
            else {
                $("div .namevalueTemplate input[class*= header-name]")[i].value = '';
                $("div .namevalueTemplate input[class*= header-value]")[i].value = '';
                i++;
            }
        }
    }

var populateCheckboxConfiguration = function (selector, context, value) {
    if (value) {
        $(selector, context).attr('checked', true);
    } else {
        $(selector, context).attr('checked', false);
    }
}

var populateSelect2Configuration = function (selector, context, searchChoices) {
    var availableSearchChoices = contactTypes;
    if (searchChoices) {
        for (var i = 0; i < searchChoices.length; i++) {
            var choice = searchChoices[i];
            availableSearchChoices.push({
                id: choice,
                text: choice
            })
        }
    }

    bindSelect2(selector, context, availableSearchChoices);
    $(selector, context).select2('val', searchChoices);
}

var bindSelect2 = function (selector, context, data) {
    $(selector, context).select2({
        allowClear: true,
        minimumInputLength: 2,
        dropdownCssClass: "bigdrop",
        multiple: true,
        tags: true,
        tokenSeparators: [',', ' '],
        createSearchChoice: function (term) {
            if (term.indexOf('@') > -1) {
                var trimmedTerm = $.trim(term)
                return {
                    id: trimmedTerm,
                    text: trimmedTerm
                }
            }
        },
        data: data
    });
};

var populateRulesConfiguration = function (context) {
    $('#rulesContainer').html('');
    $('.activity-rules-name', context).val('');
    $('.activity-rules-success-type', context).val('');

    if (nodeData.Rules) {
        $('.activity-rules-name', context).val(nodeData.Rules.Name);
        $('.activity-rules-success-type', context).val(nodeData.Rules.SuccessType);
        loadRuleExpressions();
    }
}

var loadRuleExpressions = function () {
    if (!nodeData.Rules || nodeData.Rules.length < 1)
        return;

    $.ajax({
        type:'POST',
        url: '/Workflow/Expressions',
        data: {
            Rules: nodeData.Rules,
            EventType: $('#targetEventType').val(),
            EmployerId: $('#employerId').val()
        },
        success:populateRuleExpressions
    });
}

var populateRuleExpressions = function (expressions) {

    for (var i = 0; i < expressions.length; i++) {
        addRule(expressions[i], true);
    }

        AbsenceSoft.WorkflowRules.BindExistingRules();
    }

    var addNewAnswerOptions = function () {
        addAnswerOptions();
        $('#editWorkflowForm').trigger('rescan.areYouSure');
        return false;
    }

    function removeAnsweroption(id) {
        $("#btnAnswer_"+ id).remove();
    }
    
    function addAnswerOptions() {
        var ansOptionCount = $("#tableAnswerOption button").length;
        var buttonId = 0;
        var htmlRow = $("#tableAnswerOption").html();
        if (ansOptionCount < 10) {
            if (ansOptionCount > 0) {
                if ($("#btnAnswer_" + ansOptionCount).length > 0) {
                    buttonId = ansOptionCount + 1;
                }
                else {
                    buttonId = ansOptionCount;
                }
            }
            else { buttonId = buttonId + 1; }
            if ($("#answer1").val() != "") {
                $("#AddAnswerRowError").addClass("hide");
                htmlRow += '<button class="btn" style="margin: 10px;" id="btnAnswer_' + buttonId + '" value="' + $("#answer1").val() + '">' + $("#answer1").val() + ' &nbsp; &nbsp; <i class="fa fa-times" aria-hidden="true" onClick="AbsenceSoft.DesignWorkflow.removeAnsweroption(' + buttonId + ')"></i> </button>';
                $("#tableAnswerOption").html(htmlRow);
                $("#answer1").val("");
            }
            else {
                $("#AddAnswerRowError").html("Blank or empty answer is not allowed");
                $("#AddAnswerRowError").removeClass("hide");
                return false;
            }
        }
        else {
            $("#AddAnswerRowError").html("Maximum of 10 answers allowed");
            $("#AddAnswerRowError").removeClass("hide");
            return false;
        }
        
    }

var addNewRule = function () {
    addRule();
    $('.modal').stop().animate({ scrollTop: $('body').prop("scrollHeight") }, 1000);
    return false;
}

var addNewNameValueTemplate = function () {
    $($('.namevalueTemplate:last').clone()).insertAfter(".namevalueTemplate:last").find("input[type='text']").val("");
}
var addRule = function (rule, skipBinding) {
    var createdRule = AbsenceSoft.WorkflowRules.CreateRule(rule);
    $('#rulesContainer').append(createdRule);

    if (!skipBinding)
        AbsenceSoft.WorkflowRules.BindRulesEngine();
}

var saveNodeConfiguration = function () {
    var id = getNodeId();
    nodeData.text = $('.activity-title', id).val() || $('.activity-rules-name').val() || nodeData.text;
    nodeData.w = calculateNodeWidth(nodeData.text);
    saveNodeData(id);

};
var createNewDictNameValueCustomheader = function () {
    
    var keyValuePairs = {};
    $("div .row .namevalueTemplate").each(function (index, element) {
        var header = $(element);
        var key = header.find(".header-name").val();
        var value = header.find(".header-value").val();
        if (key != null && value != null && key!="" && value!="")
        keyValuePairs[key] = value;
    });
    return keyValuePairs;

};

var saveNodeData = function (nodeId) {
    saveNodeProperty('.activity-time', nodeId, 'Time');
    saveNodeProperty('.activity-units', nodeId, 'Units');
    saveNodeProperty('.activity-target-due-date', nodeId, 'TargetDueDate');
    saveNodeProperty('.activity-title', nodeId, 'Title');
    saveNodeProperty('.activity-priority', nodeId, 'Priority');
    saveNodeProperty('.activity-hide-days', nodeId, 'HideDays');
    saveNodeProperty('.activity-target-hide-date', nodeId, 'TargetHideDate');
    saveNodeProperty('.activity-note-template', nodeId, 'Template');
    saveNodeProperty('.activity-note-category', nodeId, 'Category');
    saveNodeProperty('.activity-item-type', nodeId, 'ToDoItemType');
    saveNodeProperty('.activity-update-all', nodeId, 'UpdateAll');
    saveNodeProperty('.activity-status', nodeId, 'Status');
    saveNodeProperty('.activity-where-status', nodeId, 'WhereStatus');
    saveNodeProperty('.activity-result-text', nodeId, 'WhereResultText');
    saveNodeProperty('.activity-send-method', nodeId, 'DefaultSendMethod');

    // Webhooks
    saveNodeProperty('.activity-endpoint-url', nodeId, 'EndpointUrl');
    saveNodeProperty('.activity-event-name', nodeId, 'EventName');
    saveNodeProperty('.activity-friendly-description', nodeId, 'FriendlyDescription');
    //webhook custome Headers        
    saveCustomeHeadersNodeProperty(createNewDictNameValueCustomheader(), nodeId, 'CustomHeaders');

    saveNodeCheckbox('.activity-optional', nodeId, 'Optional');
    saveNodeCheckbox('.activity-unique', nodeId, 'Unique');
    saveNodeCheckbox('.activity-note-confidential', nodeId, 'Confidential');
    saveNodeCheckbox('.activity-apply-to-spouse', nodeId, 'Spouse');
    saveNodeCheckbox('.activity-close-all', nodeId, 'CloseAll');
    saveNodeCheckbox('.activity-close-automatically', nodeId, 'CloseAutomatically');
    saveNodeCheckbox('.activity-approve-automatically', nodeId, 'ApproveAutomatically');
    saveNodeCheckbox('.activity-send-automatically', nodeId, 'SendAutomatically');
    saveNodeCheckbox('.activity-autocomplete', nodeId, 'Autocomplete');

        saveNodeSelect2Data('.activity-template-code', nodeId, 'TemplateCode', false);
        saveNodeSelect2Data('.activity-workflow-code', nodeId, 'WorkflowCode', false);
        saveNodeSelect2Data('.activity-assignee-type-code', nodeId, 'CaseAssigneeTypeCode', false);
        saveNodeSelect2Data('.activity-assignee-type-codes', nodeId, 'CaseAssigneeTypeCodes', false);
        saveNodeSelect2Data('.activity-email-to', nodeId, 'To', true);
        saveNodeSelect2Data('.activity-email-cc', nodeId, 'CC', true);
        if (nodeData.activityId == "ConditionActivity") {
            saveRuleConfiguration(nodeId);
        }
        else if (nodeData.activityId == "QuestionActivity") {
            if (saveQuestionConfiguration(nodeId) === true) {
                resetNodeData();
            }
           
        }
        else {
            resetNodeData();
        }
    };
    var saveQuestionConfiguration = function (context) {

        var answerOptionCount = $("#tableAnswerOption button").length;
            var answerOptions = [];

                 $.each(
                     $("button[id*=btnAnswer_]"
                    ),
                function (index, val)
                {
                    var answer = $("#" + val.id).val();
                    if (answer !== undefined && answer != "") {
                        answerOptions.push(answer);
                    }

                });
            nodeData.Question = {
                Title: $("#question").val(),
                AnswerOptions: answerOptions            };
        if (nodeData.Question.AnswerOptions.length > 0) {
            for (var i = 0; i < AbsenceSoft.DesignWorkflow.Activities.length; i++) {
                var activity = AbsenceSoft.DesignWorkflow.Activities[i];
                if (activity.Id == "QuestionActivity")
                    activity.PossibleOutcomes = nodeData.Question.AnswerOptions;
            }
           
        }
         if (nodeData.Question.AnswerOptions.length < 2) {
             $("#AddAnswerRowError").html("At least two answers are required");
            $("#AddAnswerRowError").removeClass("hide");
            return false;
        }
        return true;
    };
    var saveCustomeHeadersNodeProperty = function (selector, context, property) {
        delete nodeData["CustomHeaders"];
        if (selector != null)
            nodeData[property] = JSON.stringify(selector);

}
var saveNodeProperty = function (selector, context, property) {
    var input = $(selector, context);
    var val = input.val();
    if (input.length > 0 && val) {
        nodeData[property] = val;
    } else {
        delete nodeData[property];
    }
}

var saveNodeCheckbox = function (selector, context, property) {
    var checkbox = $(selector, context);
    if (checkbox.length > 0) {
        nodeData[property] = checkbox.is(':checked');
    }
}

var saveNodeSelect2Data = function (selector, context, property, destroy) {
    var input = $(selector, context);
    var val = input.select2('val');
    if (input.length > 0 && val) {
        nodeData[property] = val;
    } else {
        delete nodeData[property];
    }

    if(destroy)
        input.select2('destroy');
}

var saveRuleConfiguration = function (context) {
    if (!AbsenceSoft.WorkflowRules.RulesValid()) {
        return;
    }
    nodeData.Rules = {
        Name: $('.activity-rules-name', context).val(),
        SuccessType: $('.activity-rules-success-type', context).val()
    };

    $.ajax({
        type: 'POST',
        url: '/Workflow/Rules',
        data: {
            RuleExpressions: AbsenceSoft.WorkflowRules.GatherRules(),
            EventType: $('#targetEventType').val(),
            EmployerId: $('#employerId').val()
        },
        success: updateRuleConfiguration
    });
};

var updateRuleConfiguration = function (rules) {
    nodeData.Rules.Rules = rules;
    resetNodeData();
};

var resetNodeData = function () {
    var id = getNodeId();
    toolkit.updateNode(node, nodeData);
    $(id).modal('hide');
    node = {};
    nodeData = {};
}

var toggleSendAutomaticallyAvailable = function () {
    var nodeId = getNodeId();
    var sendMethod = $('.activity-send-method', nodeId).val();

    //email
    if (sendMethod == 1) {
        $('.activity-email-selected', nodeId).removeClass('hide');
    } else {
        $('.activity-email-selected', nodeId).addClass('hide');
    }
}

var toggleSendAutomaticallyInformation = function () {
    var nodeId = getNodeId();
    if ($('.activity-send-automatically', nodeId).is(':checked')) {
        $('.activity-send-automatically-selected', nodeId).removeClass('hide');
    } else {
        $('.activity-send-automatically-selected', nodeId).addClass('hide');
    }
}

var filterAvailableEventTypes = function () {
    var filter = $(this).val().toLowerCase();
    if (!filter) {
        $('.dev-workflow-category').removeClass('hide');
        $('.dev-activity').removeClass('hide');
    } else {
        $('.dev-workflow-category').each(function (i, category) {
            var $category = $(category)
            $category.addClass('hide');
            $category.find('.dev-activity').each(function (j, activity) {
                var $activity = $(activity);
                var showActivity = $activity.text().toLowerCase().indexOf(filter) > -1;
                if (showActivity) {
                    $activity.removeClass('hide');
                    $category.removeClass('hide');
                    $category.find('.panel').addClass('in');
                } else {
                    $activity.addClass('hide');
                }
            });
        });
    }
};

var transformWorkflowActivities = function (workflowData) {
    var nontransferredProperties = ["activityId", "text", "id", "h", "left", "top", "type", "w"];
    var activities = [];
    for (var i = 0; i < workflowData.nodes.length; i++) {
        var node = workflowData.nodes[i];
        var activity = {
            ActivityId: node.activityId,
            Name: node.text,
            Id: node.id
        };
        if (node.Rules != undefined && node.Rules != null) activity.Rules = node.Rules;
        for (var property in node) {
            if (node.hasOwnProperty(property) && nontransferredProperties.indexOf(property) == -1) {
                var value = filterInt(node[property]);
                if (isNaN(value)) {
                    activity[property] = node[property];
                } else {
                    activity[property] = value;
                }
            }
        }
        activities.push(activity);
    }

    return activities;
};

var transformWorkflowTransitions = function (workflowData) {
    var transitions = [];
    for (var i = 0; i < workflowData.edges.length; i++) {
        var edge = workflowData.edges[i];
        transitions.push({
            SourceActivityId: edge.source,
            TargetActivityId: edge.target,
            ForOutcome: edge.data.outcome || "Complete"
        })
    }

    return transitions;
};

var filterInt = function (value) {
    if (/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
        return Number(value);
    return NaN;
};

var saveWorkflowDesign = function () {
    var workflowData = toolkit.exportData();
    var workflow = {
        Id: $('#workflowId').val(),
        EmployerId: $('#employerId').val(),
        TargetEventType: $('#targetEventType').val(),
        DesignerData: JSON.stringify(workflowData),
        Activities: transformWorkflowActivities(workflowData),
        Transitions: transformWorkflowTransitions(workflowData)
    };

    $.ajax({
        method: 'POST',
        url: '/Workflow/SaveWorkflowDesign',
        contentType: 'application/json',
        success: workflowDesignSaved,
        data: JSON.stringify(workflow)
    })
};

    var workflowDesignSaved = function (workflow) {
        noty({ text: 'Workflow Design Saved Successfully', layout: 'topRight', type: 'success', timeout: 10000 });
        var href = '/Workflow/DesignWorkflow/';
        if (workflow.EmployerId)
            href += workflow.EmployerId;

        href += '?id=';
        href += workflow.Id;
        var now = new Date().getTime();
        href += '&saved=true&dt=' + now;

        window.location.href = href;
    };

    $(document).ready(function () {
        if ($('#workflow-designer').length == 0)
            return;
        initDesigner();
        $('#filterText').on('keyup', filterAvailableEventTypes);
        $('.dev-configuration-save-changes').click(saveNodeConfiguration);
        $('#saveWorkflowDesign').click(saveWorkflowDesign);
        $('.activity-send-method').change(toggleSendAutomaticallyAvailable);
        //$('.activity-send-automatically').change(toggleSendAutomaticallyInformation); 
        $('#addNewRule').click(addNewRule);
        $('#addAnswerOption').click(addNewAnswerOptions);
        $('.addNew-NameValueTemplate').click(addNewNameValueTemplate);
        AbsenceSoft.DesignWorkflow.removeAnsweroption = removeAnsweroption;
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
