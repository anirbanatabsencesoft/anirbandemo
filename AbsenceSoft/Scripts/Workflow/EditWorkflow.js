﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditWorkflow = AbsenceSoft.EditWorkflow || {};
    var rulesBuilt = false;
    var expressionGroups = [];
    var areYouSureConfiguration = {
        silent: true,
        addRemoveFieldsMarksDirty: true
    };

    var setTogglesToMatchWorkflow = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    var toggleWorkflow = function () {
        var id = $('#workflowId').val();
        var employerId = $('#employerId').val();
        var code = $('#workflowCode').val();

        $.ajax({
            method: 'POST',
            url: '/Workflow/ToggleWorkflowSuppression/' + employerId,
            data: {
                Id: id,
                EmployerId: employerId,
                Code: code
            },
            success: setTogglesToMatchWorkflow
        });
    };

    /// If the links have the disabled text, they shouldn't be allowed to click them
    var cancelDesignOrRules = function () {
        $('#disabledDesignHint').removeClass('hide');
        return false;
    };

    var buildExistingRules = function () {
        if (rulesBuilt || !AbsenceSoft.EditWorkflow.Workflow || !AbsenceSoft.EditWorkflow.Workflow.Rules)
            return;

        for (var i = 0; i < AbsenceSoft.EditWorkflow.Workflow.Rules.length; i++) {
            addRule(AbsenceSoft.EditWorkflow.Workflow.Rules[i], true);
        }

        AbsenceSoft.WorkflowRules.BindExistingRules();
        $('#editWorkflowForm').trigger('reinitialize.areYouSure');
        rulesBuilt = true;
    };



    var addNewRule = function () {
        addRule();
        $('#editWorkflowForm').trigger('rescan.areYouSure');
        return false;
    }

    var addRule = function (rule, skipBinding) {
        var createdRule = AbsenceSoft.WorkflowRules.CreateRule(rule);
        $('#rulesContainer').append(createdRule);

        if (!skipBinding)
            AbsenceSoft.WorkflowRules.BindRulesEngine();
    }

    var saveWorkflow = function () {
        if (!$('#editWorkflowForm').valid() || !AbsenceSoft.WorkflowRules.RulesValid())
            return false;

        var workflow = {
            Id: $('#workflowId').val(),
            EmployerId: $('#employerId').val(),
            Name: $('#workflowName').val(),
            Code: $('#workflowCode').val(),
            TargetEventType: $('#targetEventType').val(),
            EffectiveDate: $('#effectiveDate').val(),
            Description: $('#description').val(),
            Rules: AbsenceSoft.WorkflowRules.GatherRules(),
            CriteriaName: $('#criteriaName').val(),
            CriteriaSuccessType: $('#criteriaSuccessType').val(),
            Order: $('#workflowOrder').val()
        };

        $.ajax({
            url: '/Workflow/SaveWorkflow',
            contentType: 'application/json',
            method: 'POST',
            data: JSON.stringify(workflow),
            success: workflowSaved
        })

        return false;
    };

    var workflowSaved = function (workflow) {
        var href = '/workflow/EditWorkflow/';
        if (workflow.EmployerId)
            href += workflow.EmployerId;

        href += '?id=';
        href += workflow.Id;
        var now = new Date().getTime();
        href += '&saved=true&dt='+now;

        window.location.href = href;
    }

    var checkForSaveSuccess = function () {
        if (window.location.href.indexOf('&saved=true') > -1) {
            noty({ text: 'Workflow Saved Successfully', layout: 'topRight', type: 'success', timeout: 10000 });
        }
    }

    var targetEventTypeChanged = function () {
        AbsenceSoft.WorkflowRules.GetExpressionGroups($('#targetEventType').val(), $('#employerId').val(), buildExistingRules);
        $('#ruleContainer').html('');
    }

    var workflowStart = function () {
        if ($('#editWorkflowForm').length == 0)
            return;

        $('#editWorkflowForm').areYouSure(areYouSureConfiguration)
            .on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields)
            .submit(saveWorkflow);

        $('.wf-disabled-toggle').click(toggleWorkflow);
        $('.wf-disabled-link, .wfr-disabled-link').click(cancelDesignOrRules);

        $('#targetEventType').change(targetEventTypeChanged);

        targetEventTypeChanged();
        AbsenceSoft.WorkflowRules.BindRulesEngine();

        $('#addNewRule').click(addNewRule);
        checkForSaveSuccess();
        $('.comm-disabled-toggle').click(toggleWorkflow);
    }

    AbsenceSoft.EditWorkflow.Start = workflowStart;

    if (location.href.indexOf('workflow') > 0) {
        $(document).ready(workflowStart);
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);