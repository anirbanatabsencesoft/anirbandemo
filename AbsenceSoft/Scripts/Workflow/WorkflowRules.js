﻿(function (AbsenceSoft, $) {
    AbsenceSoft.WorkflowRules = AbsenceSoft.WorkflowRules || {};
    var expressionGroups = [];
    var getAvailableExpressionGroups = function (targetEventType, employerId, callback) {
        if (!targetEventType)
            return;

        $.ajax({
            type: 'GET',
            url: '/Workflow/Expressions/' + employerId,
            data: {
                eventType: targetEventType
            },
            success: setAvailableExpressionGroups(callback)
        })
    };

    var setAvailableExpressionGroups = function (callback) {
        return function (data) {
            expressionGroups = $.map(data, function (expressionGroup) {
                expressionGroup.id = expressionGroup.id || expressionGroup.Title;
                expressionGroup.text = expressionGroup.text || expressionGroup.Title;
                return expressionGroup;
            });

            if (callback)
                callback();
        }
    };

    var getExpressionsByGroupName = function (name) {
        for (var i = 0; i < expressionGroups.length; i++) {
            var currentGroup = expressionGroups[i];
            if (currentGroup.Title == name) {
                return $.map(currentGroup.Expressions, function (expression) {
                    expression.id = expression.id || expression.Name;
                    expression.text = expression.text || expression.Prompt;
                    return expression;
                });
            }
        }
    }

    var getExpressionsByGroupName = function (name) {
        for (var i = 0; i < expressionGroups.length; i++) {
            var currentGroup = expressionGroups[i];
            if (currentGroup.Title == name) {
                return $.map(currentGroup.Expressions, function (expression) {
                    expression.id = expression.id || expression.Name;
                    expression.text = expression.text || expression.Prompt;
                    return expression;
                });
            }
        }
    }

    var getExpressionByGroupAndName = function (group, expression) {
        var expressions = getExpressionsByGroupName(group);
        if (!expressions)
            return;

        for (var i = 0; i < expressions.length; i++) {
            var currentExpression = expressions[i];
            if (currentExpression.Name == expression) {
                return currentExpression;
            }
        }
    }

    var getExpressionOperators = function (group, expression) {
        var currentExpression = getExpressionByGroupAndName(group, expression);
        return transformOperators(currentExpression.Operators);
    }

    var transformOperators = function (operatorsObject) {
        var operatorsArray = [];
        for (var property in operatorsObject) {
            if (operatorsObject.hasOwnProperty(property)) {
                var operator = {
                    id: property,
                    text: operatorsObject[property]
                };
                operatorsArray.push(operator);
            }
        }
        return operatorsArray;
    }

    var createRule = function (rule) {
        if (!rule) {
            rule = {
                Group: '',
                RuleName: '',
                Name: '',
                StringValue: '',
                Operator: '',
                Value: '',
            };
        };

        var ruleContainer =
            '<div class="rule-container admin-rule-group">' +
                '<div class="form-group">' +
                    '<div class="col-xs-1">Rule Name</div>' +
                    '<div class="col-xs-8"><input type="text" class="form-control rule-name" name="RuleName" value="' + rule.RuleName + '" /></div>' +
                    '<div class="col-xs-3 text-left"><button class="btn btn-primary btn-danger btn-remove-rule">Remove Rule</button></div>' +
                '</div>' +
                '<div class="form-group admin-rule-group-details">' +
                    '<div class="col-xs-3"><input type="hidden" class="rule-group form-control" value="' + rule.Group + '" /></div>' +
                    '<div class="col-xs-3"><input type="hidden" class="rule-expression form-control" value="' + rule.Name + '" /></div>'
        if (rule.Group != null && rule.Group != '' && rule.RuleName != null && rule.RuleName != '')
        {
            if (rule.Parameters == null || rule.Parameters.length == 0 || rule.IsCustom) {
                ruleContainer = ruleContainer +
                    '<div class="col-xs-3 rule-operator-container"><input type="hidden" class="rule-operator form-control" value="' + rule.Operator + '" /></div>' +
                    '<div class="col-xs-3 rule-value-container">' + buildRuleInput(rule) + "</div>"
            }
            else
            {                
                for (var i = 0; i < rule.Parameters.length ; i++) {
                    ruleContainer = ruleContainer +
                                    '<div class="col-xs-3 rule-value-container">' + buildRuleInputParameters(rule.Parameters[i], i) + "</div>";
                }
                ruleContainer = ruleContainer + '<div class="col-xs-3 rule-value-container">' + buildRuleInput(rule) + "</div>";
            }
        }        
        ruleContainer = ruleContainer +
                '</div>' +
            '</div>';
        MakeSelect2();
        return ruleContainer;
    }

    var buildRuleInput = function (rule) {
        var expression = getExpressionByGroupAndName(rule.Group, rule.Name);
        if (!expression)
            return '';

        switch (expression.ControlType) {
            case 1:
                /// text box
                return '<input type="text" class="rule-value form-control rule-data-value" value="' + (rule.Value==null?'':rule.Value) + '" />';
            case 2:
                var select = '<select class="rule-value form-control select-list rule-data-value"><option value="">Select A Value</option>';
                for (var i = 0; i < expression.Options.length; i++) {
                    var currentOption = expression.Options[i];
                    if (currentOption.Value == rule.Value) {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    } else {
                        select += '<option value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                }

                select += '</select>';

                return select;
                /// select list
            case 4:
                /// date
                return '<input type="text" class="rule-value form-control date-editor rule-data-value" value="' + (rule.Value == null ? '' : rule.Value) + '" />';
            case 6:
                /// numeric
                return '<input type="number" class="rule-value form-control order-nopad rule-data-value" value="' + (rule.Value == null ? '' : rule.Value) + '" />';
            case 8:
                /// checkbox
                if (rule.Value) {
                    if (expression.DisplayStyle) {
                        return '<input type="checkbox" class="rule-value form-control rule-data-value" name="RuleValue" checked style="display:' + expression.DisplayStyle +';"/>';
                    }
                    else {
                        return '<input type="checkbox" class="rule-value form-control rule-data-value" name="RuleValue" checked />';
                    }
                } else {
                    if (expression.DisplayStyle) {
                        return '<input type="checkbox" class="rule-value form-control rule-data-value" name="RuleValue" style="display:' + expression.DisplayStyle +';" />';
                    }
                    else {
                        return '<input type="checkbox" class="rule-value form-control rule-data-value" name="RuleValue" />';
                    }
                }
            case 10:
                return '<input type="hidden" class="rule-value form-control rule-data-value" name="RuleValue" value="' + (rule.Value == null ? '' : rule.Value) + '" />';
            case 12:
                var select = '<select class="rule-value form-control select-list rule-data-value select2multiple" multiple="multiple" placeholder="Select Values" >';
                for (var i = 0; i < expression.Options.length; i++) {
                    var currentOption = expression.Options[i];
                    if (rule.Value != null && rule.Value.toString() != "-1" && rule.Value.toString().indexOf(currentOption.Value) != -1) {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                    else if (rule.Value != null && rule.Value.toString() == "-1" && currentOption.Value.toString() == "-1") {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                    else {
                        select += '<option value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                }

                select += '</select>';

                return select;
            default:
                return '';
        }
    };

    var buildRuleInputParameters = function (parameter,index) {        
        if (!parameter)
            return '';        
        switch (parameter.ControlType) {
            case 1:
                /// text box
                return '<input type="text" class="rule-value form-control" value="' + parameter.Value + '" data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
            case 2:
                var select = '<select class="rule-value form-control select-list" data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"><option value="">Select A Value</option>';
                for (var i = 0; i < parameter.Options.length; i++) {
                    var currentOption = parameter.Options[i];
                    if (currentOption.Value == parameter.Value) {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    } else {
                        select += '<option value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                }

                select += '</select>';

                return select;
                /// select list
            case 4:
                /// date
                return '<input type="text" class="rule-value form-control date-editor" value="' + parameter.Value + '"  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
            case 6:
                /// numeric
                return '<input type="number" class="rule-value form-control order-nopad" value="' + parameter.Value + '"  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
            case 8:
                /// checkbox
                if (parameter.Value) {
                    return '<input type="checkbox" class="rule-value form-control" name="ParameterValue"' + index + ' checked  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
                } else {
                    return '<input type="checkbox" class="rule-value form-control" name="ParameterValue"' + index + '  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
                }
            case 10:
                //hidden
                if (parameter.Value) {
                    return '<input type="hidden" class="rule-value form-control" name="ParameterValue"' + index + ' checked  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
                } else {
                    return '<input type="hidden" class="rule-value form-control" name="ParameterValue"' + index + '  data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '"/>';
                }
            case 12:
                var select = '<select class="rule-value form-control select-list select2multiple" multiple="multiple" placeholder="Select Values" data-name="' + parameter.Name + '" data-typename="' + parameter.TypeName + '">';
                for (var i = 0; i < parameter.Options.length; i++) {
                    var currentOption = parameter.Options[i];
                    
                    if (parameter.Value != null && parameter.Value.toString() != "-1" && parameter.Value.toString().indexOf(currentOption.Value) != -1) {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                    else if (parameter.Value != null && parameter.Value.toString() == "-1" && currentOption.Value.toString() == "-1") {
                        select += '<option selected value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                    else {
                        select += '<option value="' + currentOption.Value + '">' + currentOption.Text + '</option>';
                    }
                }

                select += '</select>';

                return select;
            default:
                return '';
        }
    };
    var createRuleDetails = function (ruleDetail) {
        var ruleContainer = $(ruleDetail).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var expression = ruleContainer.find('.rule-expression').not('.select2-container').val();
        var currentExpression = getExpressionByGroupAndName(group, expression);
        var rule = {
            Group: group,
            RuleName: currentExpression.RuleName,
            Name: currentExpression.Name,
            StringValue: '',
            Operator: '',
            Value: currentExpression.Value,
        };
        if (currentExpression.Parameters == null || currentExpression.Parameters.length == 0 || currentExpression.IsCustom)
        {
            var html = '<div class="col-xs-3 rule-operator-container"><input type="hidden" class="rule-operator form-control" value="" /></div>' +
                                '<div class="col-xs-3 rule-value-container">' + buildRuleInput(rule) + "</div>";
            ruleContainer.find('.admin-rule-group-details').find('.rule-operator-container').remove();
            ruleContainer.find('.admin-rule-group-details').find('.rule-value-container').remove();            
            ruleContainer.find('.admin-rule-group-details').append(html);
        }
        else
        {
            ruleContainer.find('.admin-rule-group-details').find('.rule-operator-container').remove();
            ruleContainer.find('.admin-rule-group-details').find('.rule-value-container').remove();            
            var html = '';
            for (var i = 0; i < currentExpression.Parameters.length ; i++)
            {
                html = html +
                                '<div class="col-xs-3 rule-value-container">' + buildRuleInputParameters(currentExpression.Parameters[i], i) + "</div>";
            }
            html = html + '<div class="col-xs-3 rule-value-container">' + buildRuleInput(rule) + "</div>";
            ruleContainer.find('.admin-rule-group-details').append(html);
        }
        MakeSelect2();
    }

    var bindOperators = function () {
        $('.rule-operator').each(function (i, operator) {
            createSelect2ForOperators(operator);
        });
    }

    var createSelect2ForOperators = function (operator) {
        var ruleContainer = $(operator).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var expression = ruleContainer.find('.rule-expression').not('.select2-container').val();
        var expressionOperators = getExpressionOperators(group, expression);
        if (expressionOperators.length <= 0) {
            ruleContainer.find('.rule-operator')
            .select2('destroy');
        }
        else {
            ruleContainer.find('.rule-operator')
                .select2('destroy')
                .select2({
                    data: expressionOperators,
                    placeholder: 'Select An Operator'
                });
        }
    }

    var bindExpressions = function () {
        $('.rule-expression').each(function (i, expression) {
            createSelect2ForExpressions(expression);
        });
    }

    var createSelect2ForExpressions = function (expression) {
        var ruleContainer = $(expression).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var groupExpressions = getExpressionsByGroupName(group);
        ruleContainer.find('.rule-expression')
        .select2('destroy')
        .select2({
            data: groupExpressions,
            placeholder: 'Select An Expression'
        })
        .change(expressionChanged);
    }
    var MakeSelect2 = function () {
        $("select.select2multiple").each(function () {
            //$(this).select2();
            $(this).select2().change(function () {
                $('#editWorkflowForm').addClass('dirty');
                $('#editWorkflowForm').trigger('rescan.areYouSure');
                $('#editWorkflowForm').trigger('checkform.areYouSure');                
            }).on("open", function () {
                makedirty();
            });

        });
    };
    var makedirty = function () {
        $('#editWorkflowForm').addClass('dirty');
        $('#editWorkflowForm').trigger('rescan.areYouSure');
        $('#editWorkflowForm').trigger('checkform.areYouSure');
    }
    var expressionChanged = function () {
        createRuleDetails(this);
        createSelect2ForOperators(this);
        var ruleContainer = $(this).parents('.rule-container');
        var group = ruleContainer.find('.rule-group').not('.select2-container').val();
        var expression = $(this).val();
        var rule = {
            Group: group,
            Name: expression,
            Value: ''
        };        
        AbsenceSoft.PageStart(ruleContainer);        
    };

    var bindSelect2Values = function () {
        $('.select-list.rule-value')
            .not('.select2-container')
            .not('.select2-offscreen').select2();
    }

    var bindRulesEngine = function () {
        $('.btn-remove-rule').unbind().click(removeRule);
        /// Only bind to the ones that haven't been bound to yet
        $('.rule-group')
            .not('.select2-offscreen')
            .not('.select2-container')
            .select2({
                data: expressionGroups,
                placeholder: 'Select A Group'
            })
        .change(ruleGroupChanged);
    };

    var removeRule = function () {
        $(this).parents('.rule-container').remove();
        $('#editWorkflowForm').trigger('rescan.areYouSure');
    }

    var ruleGroupChanged = function () {
        createSelect2ForExpressions(this);
        var ruleContainer = $(this).parents('.rule-container');
        ruleContainer.find('.rule-expression').not('.select2-container').val('');
        ruleContainer.find('.rule-operator').select2('destroy');
        ruleContainer.find('.rule-value-container').html('');
    };

    var rulesValid = function () {
        var rulesValid = true;
        $('.rule-container').each(function (i, rule) {
            var $rule = $(rule);
            rulesValid = rulesValid && checkValidity($rule, '.rule-name');
            rulesValid = rulesValid && checkValidity($rule, '.rule-group');
            rulesValid = rulesValid && checkValidity($rule, '.rule-expression');
            rulesValid = rulesValid && checkValidity($rule, '.rule-operator');
            rulesValid = rulesValid && checkValidity($rule, '.rule-value');
        });
        if (!rulesValid) {
            $('#rulesInvalid').removeClass('hide');
        } else {
            $('#rulesInvalid').addClass('hide');
        }

        return rulesValid;
    };

    var checkValidity = function (rule, selector) {
        var sel = rule.find(selector).not('.select2-container');
        if (sel == undefined || sel == null || sel.length <= 0) return true;
        var valid = true;
        for (var i = 0; i < sel.length; i++) {
            var value = sel[i].value;
            if (!value) {
                //rule.find(selector).addClass('admin-rule-invalid');
                $(sel[i]).addClass('admin-rule-invalid');
                valid = false;
            } else {
                $(sel[i]).removeClass('admin-rule-invalid');
                //return true;
            }
        }
        return valid;
    }

    var gatherRules = function () {
        var rules = [];
        $('.rule-container').each(function (i, rule) {
            var $rule = $(rule);
            var value = $rule.find('.rule-value').not('.select2-container');
            var value2 = $rule.find('.rule-data-value').not('.select2-container');
            var selectedValue = null;
            if (value.is(':checkbox'))
                selectedValue = value.is(':checked');
            else
                selectedValue = value.val();

            if (selectedValue.length && selectedValue.length > 0) {
                selectedValue = selectedValue.toString();
                selectedValue = selectedValue.replace(/,/g, '~');
            }
            var rule = {
                Group: $rule.find('.rule-group').not('.select2-container').val(),
                RuleName: $rule.find('.rule-name').val(),
                Name: $rule.find('.rule-expression').not('.select2-container').val(),
                Operator: $rule.find('.rule-operator').not('.select2-container').val(),
                StringValue: selectedValue,
                Parameters: []
            };
            if (rule.Operator == undefined || rule.Operator == null) { rule.Operator = '=='; }
            if (value != undefined && value != null && value.length > 0)
            {
                jQuery.each(value, function (i, val) {
                    var dataAttr = $(val).data();
                    if (dataAttr != undefined && dataAttr != null && dataAttr.name != undefined && dataAttr.name != null)
                    {
                        var param = {};
                        param.Name = dataAttr.name;
                        if ($(val).is(':checkbox'))
                            param.StringValue = $(val).is(':checked');
                        else {
                            param.StringValue = $(val).val();
                            if (param.StringValue.length && param.StringValue.length > 0) {
                                param.StringValue = param.StringValue.toString();
                                param.StringValue = param.StringValue.replace(/,/g, '~');
                            }
                        }
                        param.TypeName = dataAttr.typename;
                        rule.Parameters.push(param);
                    }
                    
                });
            }
            if (value2 != undefined && value2 != null && value2.length > 0)
            {
                if (value.is(':checkbox'))
                    selectedValue = value.is(':checked');
                else
                    selectedValue = value.val();
                if (selectedValue.length && selectedValue.length > 0) {
                    selectedValue = selectedValue.toString();
                    selectedValue = selectedValue.replace(/,/g, '~');
                }
                rule.StringValue = selectedValue;
            }
            rules.push(rule);
        });

        return rules;
    };

    var bindExistingRules = function () {
        bindSelect2Values();
        bindOperators();
        bindExpressions();
        bindRulesEngine();
    };

    AbsenceSoft.WorkflowRules.BindExistingRules = bindExistingRules;
    AbsenceSoft.WorkflowRules.GetExpressionGroups = getAvailableExpressionGroups;
    AbsenceSoft.WorkflowRules.CreateRule = createRule;
    AbsenceSoft.WorkflowRules.BindOperators = bindOperators;
    AbsenceSoft.WorkflowRules.BindExpressions = bindExpressions;
    AbsenceSoft.WorkflowRules.BindSelect2Values = bindSelect2Values;
    AbsenceSoft.WorkflowRules.BindRulesEngine = bindRulesEngine;
    AbsenceSoft.WorkflowRules.RulesValid = rulesValid;
    AbsenceSoft.WorkflowRules.GatherRules = gatherRules;
    AbsenceSoft.WorkflowRules.MakeSelect2 = MakeSelect2;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery)