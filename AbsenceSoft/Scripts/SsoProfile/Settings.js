﻿(function (AbsenceSoft, $) {
    AbsenceSoft.SsoSettings = AbsenceSoft.SsoSettings || {};

    var fileUploadSettings = {
        autoUpload: false,
        singleFileUploads: true,
        replaceFileInput: false
    };

    var removeInterstitial = function () {
        $('#ssoInterstitial').remove();
        $('#ssoSettings').removeClass('hide');
    }

    var appendQueryParameters = function () {
        var exportEss = $('#exportEss').is(':checked');
        var regenerateCertificate = $('#regenerateCertificate').is(':checked');

        //get the current link
        var currentHref = $(this).attr('href');

        // In case the way to import a second time, it's just easier to remove the existing query string and start over
        var beginningOfQueryString = currentHref.indexOf('?');
        if (beginningOfQueryString > -1)
            currentHref = currentHref.substring(0, currentHref.indexOf('?'));

        // set the appropriate variables in the query string so MVC knows what to do
        var essQueryParameter = '?ess=' + exportEss;
        var regenerateQueryParameter = '&regenerateCertificate=' + regenerateCertificate;
        var finalQueryString = essQueryParameter + regenerateQueryParameter;
        currentHref += finalQueryString;

        // make sure we update the link before we continue with the click event
        $(this).attr('href', currentHref);
    }

    var fileUploaded = function (selector) {
        return function (e, data) {
            if (data.files.length != 1)
                return;

            var uploadedFile = data.files[0];
            $(selector).html(uploadedFile.name).removeClass('error');
        }
        
    }

    var checkForSave = function () {
        var saved = $('#ssoProfileSaved').val();
        if (saved) {
            noty({ type: 'success', layout: 'topRight', text: 'Your SSO settings have been successfully saved.', timeout: 6000 })
        }
    }

    var validateFile = function () {
        var fileName = $('#idpMetadataFile').val();
        if (!fileName) {
            $("#idpMetadataFileName").addClass('error');
            return false;
        }
    }

    AbsenceSoft.SsoSettings.Init = function (container, firstLoad) {
        $('#editSsoSettings').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);


        $('#removeSsoInterstitial').click(removeInterstitial);
        $('#exportSPMetadata').click(appendQueryParameters);
        $('#idpMetadataFile').fileupload(fileUploadSettings).on('fileuploadadd', fileUploaded('#idpMetadataFileName'));
        $('#partnerCertificateFile').fileupload(fileUploadSettings).on('fileuploadadd', fileUploaded('#partnerCertificateFileName'));
        $('#importIdPMetadata').submit(validateFile);
        checkForSave();
    }

    $(document).ready(function () {
        AbsenceSoft.SsoSettings.Init();
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);