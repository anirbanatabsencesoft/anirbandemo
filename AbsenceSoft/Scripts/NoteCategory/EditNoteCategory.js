﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditNoteCategory = AbsenceSoft.EditNoteCategory || {};

    var setTogglesToMatchNoteCategory = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    var toggleNoteCategory = function () {
        var employerId = $('#employerId').val();
        $.ajax({
            type: 'POST',
            url: '/NoteCategory/ToggleNoteCategorySuppression/' + employerId,
            data: {
                Code: $('#noteCategoryCode').val(),
                EmployerId: employerId
            },
            success: setTogglesToMatchNoteCategory
        });
    };

    var editNoteCategoryStart = function () {
        if ($('#editNoteCategoryForm').length < 1)
            return;

        $('#editNoteCategoryForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('.comm-disabled-toggle').click(toggleNoteCategory);
        $('#editNoteCategoryRoles').select2();
        AbsenceSoft.PageStart('#editNoteCategoryForm');
    }

    AbsenceSoft.EditNoteCategory.Start = editNoteCategoryStart;

    $(document).ready(editNoteCategoryStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);