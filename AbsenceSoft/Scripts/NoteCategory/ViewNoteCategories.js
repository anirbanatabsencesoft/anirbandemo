﻿(function (AbsenceSoft, $) {
    AbsenceSoft.ViewNoteCategories = AbsenceSoft.ViewNoteCategories || {};

    var filterNoteCategories = function (searchTerm) {
        return function () {
            var toBeFiltered = $(this);
            var text = toBeFiltered.text().toLowerCase();
            if (text.indexOf(searchTerm) > -1) {
                toBeFiltered.removeClass('hide').parents('.note-category-name').removeClass('hide');
                toBeFiltered.parents('.note-category-collapse').collapse('show');
            } else {
                toBeFiltered.addClass('hide');
                // Can't collapse it because maybe one of the parents matches and quite frankly it is an edge case I'm not worried about
            }
        }
    }

    var searchNoteCategories = function () {
        var searchTerm = $(this).val().trim().toLowerCase();
        if (!searchTerm) {
            $('.note-category-name').removeClass('hide');
            collapseAllNoteCategories();
        } else {
            $('.note-category-name').each(filterNoteCategories(searchTerm));
        }
    };

    var collapseAllNoteCategories = function () {
        $('.note-category-collapse').collapse('hide');
    };

    var expandAllNoteCategories = function () {
        $('.note-category-collapse').collapse('show');
    };
    

    $(document).ready(function () {
        $('#searchNoteCategories').keyup(searchNoteCategories);
        $('#collapseAllNoteCategories').click(collapseAllNoteCategories);
        $('#expandAllNoteCategories').click(expandAllNoteCategories);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);