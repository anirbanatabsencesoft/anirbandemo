﻿(function (AbsenceSoft, $) {
    AbsenceSoft.IpRangeEdit = AbsenceSoft.IpRangeEdit || {};

    var populateIpRange = function () {
        var requestIp = $('#currentIpRequest').val();

        var ipParts = requestIp.split('.');
        if (ipParts.length === 4) {
            $('#ipFirstEntry').val(ipParts[0]);
            $('#ipSecondEntry').val(ipParts[1]);
            $('#ipThirdEntry').val(ipParts[2]);
            $('#ipFourthEntry').val(ipParts[3]);
            $('#ipFifthEntry').val(ipParts[3]);
            $('#editIpRangeForm').trigger('checkform.areYouSure');
        }
        return false;
    }

    var editIpRangeStart = function () {
        if ($('#editIpRangeForm').length < 1)
            return;

        $('#editIpRangeForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#ipRangePopulate').click(populateIpRange);
        AbsenceSoft.PageStart('#editIpRangeForm');
    }

    AbsenceSoft.IpRangeEdit.Start = editIpRangeStart;

    $(document).ready(editIpRangeStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);