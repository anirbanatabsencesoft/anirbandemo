﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditRiskProfile = AbsenceSoft.EditRiskProfile || {};
    var areYouSureConfiguration = {
        silent: true,
        addRemoveFieldsMarksDirty: true
    };
    var ruleGroupsBuilt = false;
    var ruleGroupPromises = [];
    var updateColorDisplay = function () {
        var color = $(this).val();
        var display = $('#colorDisplay').removeClass();
        if (color) {
            display.addClass('rp-circle rp-' + color);
        }
    }

    var updateIconDisplay = function () {
        var icon = $(this).val();
        var display = $('#iconDisplay').removeClass();
        if (icon) {
            display.addClass('fa fa-' + icon);
        }
    }

    var bindExistingRuleGroups = function () {
            if (ruleGroupsBuilt || !AbsenceSoft.EditRiskProfile.Profile || !AbsenceSoft.EditRiskProfile.Profile.RuleGroups)
                return;
            for (var i = 0; i < AbsenceSoft.EditRiskProfile.Profile.RuleGroups.length; i++) {
                addRuleGroup(AbsenceSoft.EditRiskProfile.Profile.RuleGroups[i], true);
            }
            AbsenceSoft.Rules.BindExistingRuleGroups();
            $('#editRiskProfileForm').trigger('reinitialize.areYouSure');
            ruleGroupsBuilt = true;
    }

    var addNewRuleGroup = function () {
        addRuleGroup(null, false);
        $("#editRiskProfileForm").trigger('rescan.areYouSure');
        return false;
    }

    var addRuleGroup = function (ruleGroup, skipBinding) {
        var createdRuleGroup = AbsenceSoft.Rules.CreateRuleGroup(ruleGroup);
        $('#ruleGroupsContainer').append(createdRuleGroup);

        if (!skipBinding)
            AbsenceSoft.Rules.BindRulesEngine();
    }

    var saveRiskProfile = function () {
        if (!$('#editRiskProfileForm').valid() || !AbsenceSoft.Rules.RuleGroupsValid())
            return false;

        var employerId = $('#employerId').val();
        var riskProfile = {
            Id: $('#rpId').val(),
            CustomerId: $('#customerId').val(),
            EmployerId: employerId,
            Name: $('#rpName').val(),
            Code: $('#rpCode').val(),
            Color: $('#rpColor').val(),
            Icon: $('#rpIcon').val(),
            TargetsCase: $('#rpTargetsCase').is(':checked'),
            TargetsEmployee: $('#rpTargetsEmployee').is(':checked'),
            Description: $('#rpDescription').val(),
            RuleGroups: AbsenceSoft.Rules.GatherRuleGroups()
        }

        var url = '/RiskProfiles/SaveRiskProfile/';
        if(employerId)
            url += employerId
        $.ajax({
            url: url,
            method: 'POST',
            data: riskProfile,
            success: riskProfileSaved
        })

        return false;
    }

    var riskProfileSaved = function (riskProfile) {
        var href = '/RiskProfiles/EditRiskProfile/';
        if (riskProfile.EmployerId)
            href += riskProfile.EmployerId;

        href += '?id=';
        href += riskProfile.Id;
        href += '&saved=true';

        window.location.href = href;
    }

    var checkForSaveSuccess = function () {
        if (window.location.href.indexOf('&saved=true') > -1) {
            noty({ text: 'Risk Profile Saved Successfully', layout: 'topRight', type: 'success', timeout: 10000 });
        }
    }

    var loadRuleGroups = function () {
        var id = $('#rpId').val();
        var employerId = $('#employerId').val();
        /// It's a new RiskProfile, no need to get the rule groups, just get the expressions
        if (!id) {
            ruleGroupsBuilt = true;
            AbsenceSoft.Rules.GetExpressionGroups(employerId);
            return;
        }

        var href = '/RiskProfiles/RiskProfileExpressions';
        if (employerId)
            href += employerId;

        href += '?id=';
        href += id;

        $.ajax({
            url: href,
            method: 'GET',
            async: false,
            success: ruleGroupsLoaded,
            error: ruleGroupsFailedToLoad
        });
    }

    var ruleGroupsLoaded = function (riskProfile) {
        AbsenceSoft.EditRiskProfile.Profile = riskProfile;
    }

    var ruleGroupsFailedToLoad = function () {
        noty({ text: 'Unable to load existing rule groups', layout: 'topRight', type: 'error', timeout: 10000 });
    }

    var editRiskProfileStart = function () {
        if ($('#editRiskProfileForm').length < 1)
            return;

        $('#editRiskProfileForm').areYouSure(areYouSureConfiguration)
            .on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#rpColor').change(updateColorDisplay);
        $('#rpIcon').change(updateIconDisplay);

        $('#newRuleGroup').click(addNewRuleGroup);
        $('#editRiskProfileForm').submit(saveRiskProfile);
        loadRuleGroups();
        ruleGroupPromises.push(AbsenceSoft.Rules.GetExpressionGroups($('#employerId').val(), bindExistingRuleGroups));
        checkForSaveSuccess();
        AbsenceSoft.PageStart('#editRiskProfileForm');
    }

    AbsenceSoft.EditRiskProfile.Start = editRiskProfileStart;

    $(document).ready(editRiskProfileStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);