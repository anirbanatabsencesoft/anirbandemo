﻿(function (AbsenceSoft, $) {
    var sourceElement = undefined;

    var dragStart = function (e) {
        $(this).addClass('rp-dragging');
        $('.rp-dragfield').not(this).addClass('rp-drag-target');

        sourceElement = this;
        /// We're not using dataTransfer.setData or getData, because IE doesn't have proper support for it
        /// But we need to set something, or FF assumes we have nothing to transfer
        e.originalEvent.dataTransfer.setData('text/plain', $(this).html());
        e.originalEvent.dataTransfer.effectAllowed = 'move';
    }

    var dragEnd = function (e) {
        $(this).removeClass('rp-dragging');
        $('.rp-dragfield').removeClass('rp-drag-target rp-dragging rp-drag-over');
        sourceElement = null;
    }

    var allowDrop = function (e) {
        if (e.preventDefault)
            e.preventDefault();

        e.originalEvent.dataTransfer.dropEffect = 'move';
        return false;
    }

    var dropped = function (e) {
        if (e.stopPropagation)
            e.stopPropagation();

        if (sourceElement != this) {
            $(sourceElement).remove();
            $(this).after(sourceElement);
            bindDragEvents();
            renumberRiskProfiles();
        }

        return false;
    }

    var handleDragEnter = function (e) {
        $(this).addClass('rp-drag-over');
    }

    var handleDragLeave = function (e) {
        $(this).removeClass('rp-drag-over');
    }

    var renumberRiskProfiles = function () {
        var riskProfiles = [];
        $('.rp-dragfield').each(function (i, element) {
            var newOrder = i + 1;
            var $element = $(element);
            var rpOrder = $element.find('.rp-order');
            var oldOrder = rpOrder.html();
            if (oldOrder != newOrder) {
                rpOrder.html(newOrder);
                riskProfiles.push({
                    Id: $element.find('.rp-id').val(),
                    Order: newOrder
                });
            }
            
        });
        updateRiskProfilesOrder(riskProfiles);
    }

    var updateRiskProfilesOrder = function (riskProfiles) {
        $.ajax({
            url: '/RiskProfiles/UpdateRiskProfilesOrder/' + $('#employerId').val(),
            method: 'POST',
            data: {
                RiskProfiles: riskProfiles
            },
            success: alertSuccess
        });
    }

    var alertSuccess = function () {
        noty({type:'success', layout: 'topRight', text: 'Risk Profiles have been successfully reordered'})
    }

    var bindDragEvents = function () {
        $('.rp-dragfield').unbind('dragstart').on('dragstart', dragStart);
        $('.rp-dragfield').unbind('dragend').on('dragend', dragEnd);
        $('.rp-dragfield').unbind('dragover').on('dragover', allowDrop);
        $('.rp-dragfield').unbind('drop').on('drop', dropped);
        $('.rp-dragfield').unbind('dragenter').on('dragenter', handleDragEnter);
        $('.rp-dragfield').unbind('dragleave').on('dragleave', handleDragLeave);
    }

    $(document).ready(bindDragEvents);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);