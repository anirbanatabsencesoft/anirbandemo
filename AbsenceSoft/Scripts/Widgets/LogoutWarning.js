﻿//Validate the token with forms timeout
(function (AbsenceSoft, $) {
    /// call to the server to check authentication status
    var checkAuthentication = function () {
        setTimeout(function () {
            $.ajax({
                url: '/Administration/GetUserIsAuthenticatedOrNot',
                success: checkAuthenticationResponse,
                error: redirectToLoginPage
            });

        }, new Date().setMinutes(dtAbsouluteExpiration.getMinutes() + 3));
    }

    var originalExpirationTime = "";
    var dtAbsouluteExpiration = new Date();
    var noOfRerty= 0;
    /// redirects to login page
    redirectToLoginPage = function (data) {
        if (noOfRerty > 3) {
            originalExpirationTime = "";
            // Destroy onbeforeunload (IE Compliant way)
            window.onbeforeunload = undefined;
            // Redirect
            window.location.href = "/Login?showLogoutWarning=true";
        }
        else {
            noOfRerty = noOfRerty + 1;
            checkAuthentication();
        }
    }

    /// renews the session
    /// if the call fails, redirect to login page, otherwise we hide the modal
    var renewSession = function () {
        $.ajax({
            url: '/Administration/RenewSession',
            dataType: 'text',
            error: redirectToLoginPage,
            success: function () {
                originalExpirationTime = "";
                $('#formsAuthTimeout').modal('hide');
            }
        });

        return false;
    }


    /// if authenticated and will expire
    ///     pop modal, show when session will expire
    /// if not authenticated
    ///     redirect to login page and show the logout warning
    checkAuthenticationResponse = function (data) {
        if (originalExpirationTime === "") {
            originalExpirationTime = new Date(data.ExpirationTime);
        }        

      
        if (data.IsUserAuthenticated && data.WillExpire) {
            if (data.WillExpireSameDayOrWithinNextMeridiem) {
                $('.session-timeout').html(originalExpirationTime.toLocaleTimeString());
            } else {
                $('.session-timeout').html(originalExpirationTime.toLocaleString());
            }
            $('#formsAuthTimeout').modal('show');
        } else if (!data.IsUserAuthenticated) {
            noOfRerty = 4;
            redirectToLoginPage();
        }
      
    }

    /// if they click continue session
    /// renew the session
    var bindLogoutWarningEvents = function (element) {
        noOfRerty = 0;
        $('.continue-session', element).click(renewSession);
    };

    /// every minute, call out to Administration/GetUserIsAuthenticatedOrNot
    //setInterval(checkAuthentication, 60000);

    AbsenceSoft.LogoutWarning = AbsenceSoft.LogoutWarning || {};
    AbsenceSoft.LogoutWarning.Init = bindLogoutWarningEvents;
    AbsenceSoft.LogoutWarning.CheckAuthentication = checkAuthentication;


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);