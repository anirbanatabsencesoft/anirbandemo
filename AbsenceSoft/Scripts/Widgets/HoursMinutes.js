﻿///Parses a value on an input with class hours-minutes and turns it into a friendly interpretation
/// IE 480 becomes 8h
(function (absenceSoft, $) {
    var getFormattedDuration = function (currentDuration) {
        
        var getHourMinutes = function (h, m) {
            h = parseInt(h);
            if (h == undefined || h.toString() == "NaN" || h == "") { h = 0; }
            if (m != undefined && m.toString() != "NaN" && m != "") {
                m = parseFloat(m);
                if (m > 60) {
                    h = h + parseInt(m / 60);
                    m = m % 60;
                }
                m = parseInt(m);
                return h + "h " + m + "m";
            }
            return h + "h 0m";
        };

        var getForDecimalHours = function (pattern) {
            var matches = pattern.exec(currentDuration);
            if (matches != null && matches.length == 3) {
                var m = matches[2];
                if (m != undefined && m.toString() != "NaN" && m != "") {
                    m = parseFloat(parseFloat("0." + m) * 60);
                }
                return getHourMinutes(matches[1], m);
            }
            return "";
        };

        var getForDecimalMinutes = function (pattern) {
            var matches = pattern.exec(currentDuration);
            if (matches != null && matches.length == 2) {
                return getHourMinutes(0, matches[1]);
            }
            return "";
        };

        var result = "";

        // CASE1 : 3:30 = 3h 30m
        //var pattern = /^((?:\d){1,2})\:((?:\d){1,2})$/ig;
        var pattern = /^(\d+)\:?(\d*)$/ig;
        var matches = pattern.exec(currentDuration);
        if (matches != null && matches.length == 3) {
            return getHourMinutes(matches[1], matches[2]);
        }

        // CASE2 : 3.5 = 3h 30m
        // CASE20 : .5 = 0h 30m
        pattern = /^(\d*)\.?(\d{0,2})$/ig;
        result = getForDecimalHours(pattern);
        if (result != "") return result;

        //CASE3 : 3.5h = 3h 30m
        //CASE4 : 3.5hrs = 3h 30m
        //CASE5 : 3.5hours = 3h 30m
        //CASE6 : 3.5 h = 3h 30m
        //CASE7 : 3.5 hrs = 3h 30m
        //CASE8 : 3.5 hours = 3h 30m
        pattern = /^(\d+)\.?(\d{0,2})(?:hours|hrs|h| hours| hrs| h)$/ig;
        result = getForDecimalHours(pattern);
        if (result != "") return result;

        //CASE9 : 210min = 3h 30m
        //CASE10 : 210mins = 3h 30m
        //CASE11 : 210minutes = 3h 30m
        //CASE12 : 210 min = 3h 30m
        //CASE13 : 210 mins = 3h 30m
        //CASE14 : 210 minutes = 3h 30m
        //CASE20 : 5 m = 0h 5m
        //CASE21 : 5m = 0h 5m                
        pattern = /^(\d+)(?:mins|min|minutes|m| mins| min| minutes| m)$/ig
        result = getForDecimalMinutes(pattern);
        if (result != "") return result;

        //CASE15 : 3hours 30minutes = 3h 30m
        //CASE16 : 3 hours 30 minutes = 3h 30m
        pattern = /^(\d+) {0,1}(?:hours|hrs|h) *(\d+) {0,1}(?:mins|min|minutes)$/ig;
        matches = pattern.exec(currentDuration);
        if (matches != null && matches.length == 3) {
            return getHourMinutes(matches[1], matches[2]);
        }

        //CASE17 : 3h 0.5m = 3h 30m
        //CASE18 : 3hrs 0.5mins = 3h 30m
        pattern = /^(\d+) {0,1}(?:hours|hrs|hr|h) *(\d*\.*\d*) {0,1}(?:mins|min|minutes|m)$/ig;
        matches = pattern.exec(currentDuration);
        if (matches != null && matches.length == 3) {
            var m = matches[2];
            if (m != undefined && m.toString() != "NaN" && m != "") {
                var pattern2 = /\./ig;
                var matches2 = pattern2.exec(m);
                if (matches2 != null && matches2.length > 0) {
                    // minutes contains decimal place, so convert to minutes.
                    m = parseFloat(parseFloat(m) * 60);
                }
                else {
                    m = parseFloat(m); // no need for converions
                }
            }
            return getHourMinutes(matches[1], m);
        }

        return "";
    };

    var formatDuration = function () {
        var val = $(this).val();
        var formattedDuration = "";
        
        if (val && val.toString() != "NaN") /// use the to string comparison instead of isNaN, since isNaN won't work on things like 4h
            formattedDuration = getFormattedDuration(val);
        
        $(this).val(formattedDuration);
    };

    var bindHoursMinutesEvents = function (element) {
        $('.hours-minutes', element).blur(formatDuration);
    }

    AbsenceSoft.HoursMinutes = AbsenceSoft.HoursMinutes || {};
    AbsenceSoft.HoursMinutes.Init = bindHoursMinutesEvents;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);