﻿(function (AbsenceSoft, $) {
        var toggleInput = function () {
            var container = $(this).parents('.eye-toggle-container');
            var input = container.find(':input');
            var newInputType = 'password'
            if(input.attr('type') == 'password')
                newInputType = 'text'
            
            container.find(':input').attr({
                type: newInputType,
                autocomplete: 'off'
            });
            container.find('.glyphicon-eye-close').toggle();
            container.find('.glyphicon-eye-open').toggle();
        }

       
        var bindEyeToggleEvents = function (element) {
            $('.glyphicon-eye-close', element).click(toggleInput);
            $('.glyphicon-eye-open', element).click(toggleInput).hide();

            $('.glyphicon-eye-open', element).hover(
              function ()
                {
                    var container = $(this).parents('.eye-toggle-container');
                    container.find('.glyphicon-eye-open').css('cursor', 'pointer');
                });
            $('.glyphicon-eye-close', element).hover(
                function () {
                    var container = $(this).parents('.eye-toggle-container');
                    container.find('.glyphicon-eye-close').css('cursor', 'pointer');
                });
       
    }

    AbsenceSoft.EyeToggle = AbsenceSoft.EyeToggle || {};
    AbsenceSoft.EyeToggle.Init = bindEyeToggleEvents;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);