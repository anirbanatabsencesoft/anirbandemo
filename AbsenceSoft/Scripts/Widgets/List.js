﻿(function (AbsenceSoft, $){
    var getListData = function (listContainer) {
        var listData = {};
        var filterValues = listContainer.find('.list-filter');
        var sortValues = listContainer.find('.list-sort');
        for (var i = 0; i < filterValues.length; i++) {
            var filterValue = $(filterValues[i]);
            listData[filterValue.attr('name')] = filterValue.val();
        }

        /// figure out what and how we're sorting by
        for (var i = 0; i < sortValues.length; i++) {
            var sort = $(sortValues[i]);
            var sortIcon = sort.find('i');
            var hasSort = sortIcon.hasClass('glyphicon-chevron-up') || sortIcon.hasClass('glyphicon-chevron-down');
            if (hasSort) {
                listData.SortBy = sort.find('.sort-name').val();
                listData.SortDirection = sortIcon.hasClass('glyphicon-chevron-up') ? 1 : -1;
                break;
            }
        }

        if ($('#as_searched_term').val() != undefined)
            listData.SearchText = $('#as_searched_term').val();
       
        if ($('#employers').val() != undefined)
            listData.Employers = $('#employers').val();

        if ($('#last_name').val() != undefined)
            listData.LastName = $('#last_name').val();

        if ($('#dateOfBirth').val() != undefined)
            listData.DateOfBirth = $('#dateOfBirth').val();

        listData.PageSize = 30;
        listData.PageNumber = listContainer.find('.page').val();
        return JSON.stringify(listData);
    }

    var displayListData = function (listContainer) {
        return function (html) {
            listContainer.find('.list-display').append(html);
            var totalElement = listContainer.find('.return-total');
            var total = totalElement.val();
            totalElement.remove();
            listContainer.find('.total').val(total);
        }
    }
    
    /// resets the list before getting the original data again
    var resetList = function (listContainer) {
        var url = listContainer.find('.url').val();
        if (!url)
            return;

        listContainer.find('.list-display div').empty();
        listContainer.find('.page').val(1);
        getNextList(listContainer);
    }

    var timer;
    
    var filterList = function () {
        var list = $(this);
        
        if (timer) {
            clearTimeout(timer);
        }

        var timer = setTimeout(function () {
            var listContainer = list.parents('.item-list');
            resetList(listContainer);
        }, 300);
    }

    var sortList = function(){
        /// Check if we're flipping the sort or setting a new sort
        var listContainer = $(this).parents('.item-list');
        var sortIcon = $(this).find('i');
        if (sortIcon.hasClass('glyphicon-chevron-up')) {
            sortIcon.removeClass('glyphicon-chevron-up');
            sortIcon.addClass('glyphicon-chevron-down');
        } else if(sortIcon.hasClass('glyphicon-chevron-down')){
            sortIcon.removeClass('glyphicon-chevron-down');
            sortIcon.addClass('glyphicon-chevron-up');
        }else{
            listContainer.find('.list-sort i').removeClass('glyphicon-chevron-down glyphicon-chevron-up');
            sortIcon.addClass('glyphicon-chevron-up');
        }

        resetList(listContainer);
    }

    /// Checks that we've reached the bottom of the list before loading the next page of data
    var scrollList = function () {
        var listDisplay = $(this);
        var listContainer = listDisplay.parents('.item-list');
        if (reachedBottom(this) && hasMoreRecords(listContainer)) {
            var currentPage = listContainer.find('.page');
            var pageValue = currentPage.val() * 1; /// Turn it into a number
            currentPage.val(pageValue += 1);
            getNextList(listContainer);
        }
    }

    var reachedBottom = function (that) {
        var listDisplay = $(that);
        var reachedBottom = listDisplay.scrollTop() + listDisplay.innerHeight() >= (that.scrollHeight - 50);
        return reachedBottom;
    }

    var hasMoreRecords = function (listContainer) {
        var totalRecords = listContainer.find('.total').val();
        var currentPage = listContainer.find('.page').val();
        var hasMoreRecords = currentPage * 30 < totalRecords
        return hasMoreRecords;
    }
    
    var getNextList = function (listContainer) {
        var url = listContainer.find('.url').val(); 
        if (!url)
            return;
            
        $.ajax({
            async: false,
            url: url,
            data: getListData(listContainer),
            contentType:'application/json',
            method: 'POST',
            success: displayListData(listContainer),
            error: function(){
                $.noty({type:'error', message: 'Unable to retrieve list data'});
            }
        })
    };
    
    var bindListEvents = function (element) {
        var listContainer = $('.item-list', element);
        var listContainerFilter = $('.row-filter', element);
        listContainerFilter.find('input').keyup(filterList);        
        listContainerFilter.find('select').change(filterList);
        listContainer.find('.list-sort').click(sortList);
        listContainer.find('.list-display').scroll(scrollList);
        resetList(listContainer);
    }

    AbsenceSoft.List = AbsenceSoft.List || {};
    AbsenceSoft.List.Init = bindListEvents;


})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
