﻿/// Don't use this one, use the proper jQuery widget $(selector).repeat(options);
(function (AbsenceSoft, $) {
    /// nested repeaters need thought - 3rd parameter? or last index?
    var renumberInput = function (input, i) {
        var name = input.attr('name');
        if (!name) {
            return;
        }

        var beginBracketIndex = name.indexOf('[');
        var endBracketIndex = name.indexOf(']');
        if (beginBracketIndex == -1 || endBracketIndex == -1) {
            return;
        }

        name = name.replace(name.substring(beginBracketIndex, endBracketIndex + 1), '[' + i + ']');
        input.attr('name', name);
    }

    var renumberRepeatingInputElements = function (container) {
        var children = container.find('.absencesoft-repeating');
        for (var i = 0; i < children.length; i++) {
            var inputs = $(children[i]).find(':input');
            for (var j = 0; j < inputs.length; j++) {
                renumberInput($(inputs[j]), i);
            }
        }
    }

    var removeRepeatingSection = function () {
        var container = $(this).parents('.absencesoft-repeating-container');
        $(this).parents('.absencesoft-repeating').remove();
        renumberRepeatingInputElements(container);
        $(document).trigger('as.repeater.remove');

        return false;
    }

    var addRepeatingSection = function (sourceElementId, targetElementId) {
        var clone = $($('#' + sourceElementId).html());
        var container = $('#' + targetElementId).append(clone);
        renumberRepeatingInputElements(container);
        AbsenceSoft.PageStart(clone);
        return clone;
    }

    var bindRepeaterEvents = function (element) {
        $('.remove-button', element).click(removeRepeatingSection)
        $('.add-button', element).click(function () {
            var sourceElementId = $(this).data('source-element-id');
            var targetElementId = $(this).data('target-element-id');
            var clonedElement = addRepeatingSection(sourceElementId, targetElementId);
            $(document).trigger('as.repeater.add', clonedElement);
            // Add tooltips
            $(clonedElement)
                .find("[tooltip]")
                .each(function(index, item) {
                    $(item).tooltip({
                        title: $(item).attr("tooltip")
                    });
                });

            return false;
        });
    }

    AbsenceSoft.Repeater = AbsenceSoft.Repeater || {};
    AbsenceSoft.Repeater.Init = bindRepeaterEvents;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);