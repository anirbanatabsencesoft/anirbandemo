﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        $(".timetracker-popover").popover({
            html: true,
            placement: 'bottom',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function () {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-title").html();
            }
        });
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);