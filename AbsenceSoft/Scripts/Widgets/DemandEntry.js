﻿(function (AbsenceSoft, $) {
    var updateAllWorkRestrictionStartDates = function () {
        $('.restriction-start-date').val($(this).val());
    }

    var updateAllWorkRestrictionEndDates = function () {
        $('.restriction-end-date').val($(this).val());
    }

    var searchRestrictions = function () {
        var searchTerm = $(this).val().toLowerCase();

        var workRestrictions = $('.work-restriction-name');
        for (var i = 0; i < workRestrictions.length; i++) {
            var restriction = $(workRestrictions[i]);
            if (restriction.val().toLowerCase().indexOf(searchTerm) > -1) {
                restriction.parents('.wr-returns, .wre-returns').removeClass('hide');
            } else {
                restriction.parents('.wr-returns, .wre-returns').addClass('hide');
            }
        }
    }

    var permanentRestrictionsChange = function () {
        var checked = $(this).is(':checked');
        $('.permanent-restriction').prop('checked', checked);
        $('.restriction-end-date').val('').attr('disabled', checked);
        $('#restrictionsEndDate').val('').attr('disabled', checked);
        
    }

    var permanentRestrictionChange = function () {
        var checked = $(this).is(':checked');
        $(this).parents('.wr-returns, .wre-returns').find('.restriction-end-date').val('').attr('disabled', checked);
    }

    var showClearRestrictions = function () {
        $(this).parents('.wre-returns').find('.clear-restrictions').removeClass('hide');
    };

    var clearRestrictions = function () {
        var $this = $(this).addClass('hide');
        var inputContainer = $this.parents('.wre-returns').find('.wre-inputs');
        inputContainer.find('.active').removeClass('active');
        inputContainer.find('radio').prop('checked', false);
    }

    $(document).ready(function () {
        $('#restrictionsStartDate').change(updateAllWorkRestrictionStartDates);
        $('#restrictionsEndDate').change(updateAllWorkRestrictionEndDates);
        $('#restrictionSearch').keyup(searchRestrictions);
        $('#restrictionsPermanent').change(permanentRestrictionsChange);
        $('.permanent-restriction').change(permanentRestrictionChange);

        $('input', '#workRestrictionEntryForm .wre-inputs').change(showClearRestrictions)
        $('.clear-restrictions').click(clearRestrictions);
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);