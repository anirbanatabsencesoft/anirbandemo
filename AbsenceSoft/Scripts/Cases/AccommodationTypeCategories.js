﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AccommodationTypeCategories = AbsenceSoft.AccommodationTypeCategories || {};

    var hideOtherCollapsibleElements = function () {
        $(this).closest('.accommodation-type-container').find('.in').not(this).collapse('hide');
    };

    var bindHideOtherAccommodationTypeCategories = function () {
        $('.accommodation-type-categories').click(hideOtherCollapsibleElements);

        $('#EditAccommodation').on('shown.bs.modal', function () {
            bindHideOtherAccommodationTypeCategories();
          
        });
    };



    AbsenceSoft.AccommodationTypeCategories.Start = bindHideOtherAccommodationTypeCategories;
    
    $(document).ready(AbsenceSoft.AccommodationTypeCategories.Start)
    
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);


