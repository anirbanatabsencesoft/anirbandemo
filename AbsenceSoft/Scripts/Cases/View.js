﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {

        AbsenceSoft.Disability = AbsenceSoft.Disability || {};

        if ($('#healthConditionForm').length == 0)
            return;

        // initialize health/diagnosis section JS at page load
        AbsenceSoft.Disability.InitHealthDiagnosis = function (employeeId) {
            _stdEmployeeId = employeeId;

            $("#healthConditionForm").validate();
            $("#diagnosisForm").validate();

            $('.datepicker').datepicker();

            var formattedHCPPhone = AbsenceSoft.Disability.FormatPhoneOptionalExtension($('#HCPPhone').val());
            $('#HCPPhoneLabel').text(formattedHCPPhone);
            $('#HCPPhone').val(formattedHCPPhone);
            var formattedHCPFax = AbsenceSoft.Disability.FormatPhoneOptionalExtension($('#HCPFax').val());
            $('#HCPFaxLabel').text(formattedHCPFax);
            $('#HCPFax').val(formattedHCPFax);

            $("#healthConditionForm").submit(function (ev) {
                if ($("#healthConditionForm").valid())
                    AbsenceSoft.Disability.OnUpdateHealthCondition();
                ev.preventDefault();
            });

            AbsenceSoft.Disability.CreateValidators();
            $('#hcp-provider-add').hide();
            $('#hcp-provider-cancel').hide();
        }

        ///////////////////////////////////////////////////////
        // health section
        ///////////////////////////////////////////////////////

        // health section handlers
        AbsenceSoft.Disability.OnEditHealthCondition = function (selectedContactId, selectedContactTypeCode) {
            if ($("#diability-edit-health-condition").prop("class") == "odg-edit-hc") {
                $("#diability-edit-health-condition").prop("class", "odg-edit-hc-act");
                $.when(AbsenceSoft.Disability.LoadProviderContacts(), AbsenceSoft.Disability.LoadProviderContactTypes()).done(function (a1, a2) {
                    _stdContacts = a1[0];
                    _stdContactTypes = a2[0];
                    AbsenceSoft.Disability.FinishEditHealthCondition(selectedContactId, selectedContactTypeCode);
                });
            }
            else {
                AbsenceSoft.Disability.OnCancelUpdateHealthCondition();
            }
        };

        AbsenceSoft.Disability.OnCancelUpdateHealthCondition = function () {
            // disable top bar edit controls
            $("#disability-edit-condition").prop("disabled", false);
            $("#disability-edit-diagnosis").prop("disabled", false);

            $("#diability-edit-health-condition").prop("class", "odg-edit-hc");
            // show more/less controls since view only mode
            $('#healthMoreLess').show();

            // toggle add/cancel links
            $('#hcp-provider-add').hide();
            $('#hcp-provider-cancel').hide();

            // hide hcp controls
            $('#select-provider .hcp-provider').hide();
            $('#select-provider .hcp-provider-type').hide();

            // reset health section controls for view only mode
            $("#health-section .editmode").hide();
            $("#health-section .viewmode").show();
            $("#health-toggle-section .provider-section").show();
            $('#disability-diagnosis').show();
        }

        AbsenceSoft.Disability.OnUpdateHealthCondition = function (reload) {
            $("#ActivityLevel").val($("#ddlJobActivityLevel :selected").val());

            // clear errors controls
            $("#healthConditionError").html("").hide();
            var frm = $("#healthConditionForm");
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    // calling angularjs functions from jquery to update the diagnosis section of the page
                    if (angular.element($("#controller-div")).scope().Comorbidity.comorbidityArgs.Args !== null) {
                        angular.element($("#controller-div")).scope().Comorbidity.comorbidityArgs.Args.JobActivityLevel = $("#ActivityLevel").val();
                    }
                    angular.element($("#controller-div")).scope().Comorbidity.reCalculate();
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Update success.' });
                    if (reload == true) {
                        setTimeout(function () { location.reload(true); }, 1500); // give time for alert to show 
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "Unable to save the health condition: " + errorThrown;
                    $("#healthConditionError").html(msg).show();
                }
            });
        };

        AbsenceSoft.Disability.OnAddProvider = function () {
            // toggle add/cancel links
            $('#hcp-provider-add').hide();
            $('#hcp-provider-cancel').show();

            AbsenceSoft.Disability.ResetProviderControls();

            // show hcp provider dropdown
            $('#select-provider .hcp-provider').show();
        }

        AbsenceSoft.Disability.OnCancelProvider = function () {
            // toggle add/cancel links
            $('#hcp-provider-add').show();
            $('#hcp-provider-cancel').hide();

            // hide hcp controls
            $('#select-provider .hcp-provider').hide();
            $('#select-provider .hcp-provider-type').hide();
            $('#health-toggle-section .provider-section').hide();

            AbsenceSoft.Disability.ResetProviderControls();
        }

        AbsenceSoft.Disability.OnProviderSelected = function () {
            // get dropdown value
            var selectedOption = $('#HCPProvider option:selected');
            var valueSelected = selectedOption.val();

            // reset hcp edit controls
            if (valueSelected == "" || valueSelected == "new") {
                $('#HCPContactId').val("");
                $("#IsPrimary").prop("checked", false);
                $("#HCPCompanyName").val("");
                $("#HCPFirstName").val("");
                $("#HCPLastName").val("");
                $("#HCPPhone").val("");
                $("#HCPFax").val("");
                $("#HCPContactPersonName").val("");
            }

            if (valueSelected == "new") {
                // reset 2nd dropdown to default value
                $('#HCPContactType').val("");
                // show 2nd dropdown
                $('#select-provider .hcp-provider-type').show();
            }
            else {
                var contacts = AbsenceSoft.Disability.GetContacts();
                $.each(contacts, function (index, value) {
                    if (valueSelected == value.ContactId) {
                        // set hcp edit controls
                        $('#HCPContactId').val(valueSelected);
                        $("#HCPContactType").val(value.ContactTypeCode);
                        $("#IsPrimary").prop("checked", value.IsPrimary);
                        $("#HCPCompanyName").val(value.Company);
                        $("#HCPFirstName").val(value.FirstName);
                        $("#HCPLastName").val(value.LastName);
                        $("#HCPPhone").val(AbsenceSoft.Disability.FormatPhoneOptionalExtension(value.WorkPhone)); //other phone numbers work/cell?
                        $("#HCPFax").val(AbsenceSoft.Disability.FormatPhoneOptionalExtension(value.Fax));
                        $("#HCPContactPersonName").val(value.ContactPersonName);
                        // show 2nd dropdown and hcp edit controls
                        $('#select-provider .hcp-provider-type').show();
                        $('#health-toggle-section .provider-section').show();
                    }
                });
            }
        }

        AbsenceSoft.Disability.OnProviderTypeSelected = function () {
            // get dropdown value
            var selectedOption = $('#HCPContactType option:selected');
            var valueSelected = selectedOption.val();

            // init hcp edit controls
            if (valueSelected != "") {
                $('#health-toggle-section .provider-section').show();
            }
        }

        AbsenceSoft.Disability.OnHealthMoreLink = function () {
            $('#health-toggle-section').slideDown();
            $('#health-show-link').hide();
            $('#health-hide-link').show();
        }

        AbsenceSoft.Disability.OnHealthLessLink = function () {
            $('#health-toggle-section').hide();
            $('#health-hide-link').hide();
            $('#health-show-link').show();
        }

        // health section helpers
        AbsenceSoft.Disability.FinishEditHealthCondition = function (selectedContactId, selectedContactTypeCode, contactsResponseData, contactTypesResponseData) {
            // load hcp dropdowns
            var foundContact = AbsenceSoft.Disability.LoadSelectProviderContacts(selectedContactId);
            var foundCode = AbsenceSoft.Disability.LoadSelectProviderContactTypes(selectedContactTypeCode);

            // hide more/less controls since edit mode
            $('#healthMoreLess').hide();

            // init health section controls for edit mode
            $("#health-section .viewmode").hide();
            $("#health-section .editmode").show();
            $("#health-section .provider-section").hide();
            $('#disability-diagnosis').hide();

            // init hcp section
            if (foundContact) {
                // toggle add provider link
                $('#hcp-provider-add').hide();
                $('#hcp-provider-cancel').show();

                // show hcp controls
                $('#select-provider .hcp-provider').show();
                $('#select-provider .hcp-provider-type').show();
                $('#health-toggle-section .provider-section').show();

                AbsenceSoft.Disability.OnProviderSelected();
            }
            else {
                // toggle add provider link
                $('#hcp-provider-add').show();
                $('#hcp-provider-cancel').hide();

                // hide hcp controls
                $('#select-provider .hcp-provider').hide();
                $('#select-provider .hcp-provider-type').hide();
                $('#health-toggle-section .provider-section').hide();
            }

            // show all health condition fields
            AbsenceSoft.Disability.OnHealthMoreLink();
        }

        AbsenceSoft.Disability.LoadProviderContacts = function () {
            return $.ajax({
                type: 'GET',
                url: "/Employees/" + AbsenceSoft.Disability.GetEmployeeId() + "/Contacts/Medical",
            });
        };

        AbsenceSoft.Disability.LoadSelectProviderContacts = function (selectedContactId) {
            var providerContacts = AbsenceSoft.Disability.GetContacts();

            var providerSelect = {};
            providerSelect.ContactId = "";
            providerSelect.Company = '-SELECT-';
            var providerNew = {};
            providerNew.ContactId = "new";
            providerNew.Company = 'New Provider';
            providerContacts.unshift(providerSelect, providerNew);

            var dropdown = $('#HCPProvider');
            dropdown.empty();

            var foundContact = false;
            $.each(providerContacts, function (index, value) {
                var display = (value.Company != "" ? value.Company : value.Name);
                if (display != "") {
                    if (selectedContactId != null && selectedContactId == value.ContactId) {
                        foundContact = true;
                    }
                    dropdown
                        .append($("<option></option>")
                            .attr("value", value.ContactId)
                            .text(display)
                            .prop("selected", selectedContactId != null && selectedContactId == value.ContactId));
                }
            });

            return foundContact;
        };

        AbsenceSoft.Disability.LoadProviderContactTypes = function () {
            return $.ajax({
                type: 'GET',
                url: "/Employees/" + AbsenceSoft.Disability.GetEmployeeId() + "/ContactTypes/Medical",
            });
        };

        AbsenceSoft.Disability.LoadSelectProviderContactTypes = function (selectedContactTypeCode) {
            var providerContactTypes = AbsenceSoft.Disability.GetContactTypes();

            var typeSelect = {};
            typeSelect.Code = "";
            typeSelect.Name = "-SELECT-";
            providerContactTypes.unshift(typeSelect);

            var dropdown = $('#HCPContactType');
            dropdown.empty();

            var foundCode = false;
            $.each(providerContactTypes, function (index, value) {
                foundCode = selectedContactTypeCode != null && selectedContactTypeCode == value.Code;
                dropdown
                    .append($("<option></option>")
                        .attr("value", value.Code)
                        .text(value.Name)
                        .prop("selected", foundCode));
            });

            return foundCode;
        };

        AbsenceSoft.Disability.ResetProviderControls = function () {
            // set hcp dropdowns to default values
            $('#HCPProvider').val("");
            $('#HCPContactType').val("");

            // reset hcp edit controls
            $("#IsPrimary").prop("checked", false);
            $("#HCPCompanyName").val("");
            $("#HCPFirstName").val("");
            $("#HCPLastName").val("");
            $("#HCPPhone").val("");
            $("#HCPFax").val("");
            $("#HCPContactPersonName").val("");
        }

        AbsenceSoft.Disability.CreateValidators = function () {

            //TODO: move this to a shared js file
            jQuery.validator.addMethod("phoneUSExt", function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^(1-?)?(\([1-9]\d{2}\)|[1-9]\d{2})\s*-?([1-9]\d{2})-?(\d{4})(\s*x\s*\d{1,4})?$/);
            }, "Please Enter Valid Phone Number");

            jQuery.validator.addMethod("companyOrName", function (field, element) {
                // assure an existing or new contact is enabled
                if ($('#HCPProvider option:selected').val() == "") {
                    if ($('#HCPContactType option:selected').val() == "")
                        return true;
                }

                if (AbsenceSoft.Disability.IsNullOrEmpty($("#HCPCompanyName").val()) && AbsenceSoft.Disability.IsNullOrEmpty($("#HCPFirstName").val()))
                    return false;

                return true;
            }, "Please Enter Company or First Name");
        }

        AbsenceSoft.Disability.IsNullOrEmpty = function (s) {
            if (s == null || s == undefined || s == "")
                return true;
            return false;
        }

        AbsenceSoft.Disability.FormatPhoneOptionalExtension = function (phone) {
            if (phone != null && phone.length > 9) {
                var newPhone = phone.trim();
                if (phone.indexOf("1") === 0) {
                    var matches = phone.match(/^(1-?)?(\([1-9]\d{2}\)|[1-9]\d{2})\s*-?([1-9]\d{2})-?(\d{4})(\s*x\s*\d{1,4})?$/);
                    if (matches && matches.length > 4)
                        newPhone = phone.replace(/^(1-?)?(\([1-9]\d{2}\)|[1-9]\d{2})\s*-?([1-9]\d{2})-?(\d{4})(\s*x\s*\d{1,4})?$/, "$1-$2-$3-$4 $5");
                }
                else {
                    var matches = phone.match(/^(\([1-9]\d{2}\)|[1-9]\d{2})\s*-?([1-9]\d{2})-?(\d{4})(\s*x\s*\d{1,4})?$/);
                    if (matches && matches.length > 3)
                        newPhone = phone.replace(/^(\([1-9]\d{2}\)|[1-9]\d{2})\s*-?([1-9]\d{2})-?(\d{4})(\s*x\s*\d{1,4})?$/, "$1-$2-$3 $4");
                }
                return newPhone.trim();
            }
            return phone;
        }

        // properties
        var _stdEmployeeId = null;
        AbsenceSoft.Disability.GetEmployeeId = function () {
            return _stdEmployeeId;
        }

        var _stdContacts = null;
        AbsenceSoft.Disability.GetContacts = function () {
            return _stdContacts;
        }

        var _stdContactTypes = null;
        AbsenceSoft.Disability.GetContactTypes = function () {
            return _stdContactTypes;
        }


        AbsenceSoft.Disability.InitHealthDiagnosis($('#disability-employeeid').val());

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
