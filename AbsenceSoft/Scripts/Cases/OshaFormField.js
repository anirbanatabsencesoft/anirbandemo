﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Osha = AbsenceSoft.Osha || {};

    var OshaStart = function () {
        if ($('#editOshaForm').length < 1)
            return;

        $('#editOshaForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        AbsenceSoft.PageStart('#editOshaForm');
    }

    AbsenceSoft.Osha.Start = OshaStart;

    $(document).ready(OshaStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);