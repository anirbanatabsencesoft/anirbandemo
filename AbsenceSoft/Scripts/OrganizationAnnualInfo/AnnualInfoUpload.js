﻿(function (AbsenceSoft, $) {
  
    var hideUnableToParseFile = function () {
        $('#hideUnableToParseFile').addClass('hide');
    }

    var fileUploaded = function (e, data) {
        hideUnableToParseFile();
        $('#errorUploadingAnnualInfo').addClass('hide');
        if (data.files.length < 1)
            return;

        var fileToUpload = data.files[0];
        parseCSVFile(fileToUpload)
    };

    var unableToParseFile = function (error, file) {
        $('#hideUnableToParseFile').removeClass('hide').text(
            'Unable to parse the file. ' + error
            );
        setTimeout(hideUnableToParseFile, 30000)
    }

    var parseCSVFile = function (file) {
        $('#ecBulkUploadMessage')
            .removeClass('hide')
            .find('h4')
            .html('Parsing CSV File...');


        Papa.parse(file, {
            header: true,
            complete: bulkSaveAnnualinfo,
            error: unableToParseFile,
            skipEmptyLines: true
        });
    };

    var bulkSaveAnnualinfo = function (annualInfos) {
        $('#ecBulkUploadMessage')
            .find('h4')
            .html('Uploading Annual Info...');

        
        if (annualInfos.errors.length > 0) {
            unableToParseFile();
            return;
        }

        var parsedContacts = annualInfos.data;
       
        $.ajax({
            url: '/OrganizationAnnualInfo/' + $('#employerId').val() + '/BulkUpload',
            data: JSON.stringify({
                AnnualInfos: annualInfos.data
            }),
            method: 'POST',
            dataType:'json',
            contentType: 'application/json',
            success: annualinfoUploaded,
            error: failedToUploadannualInfo
        })
    };

    var annualinfoUploaded = function (data, textStatus, jqXHR) {
        if (data.Success) {
            $('#ecBulkUploadMessage').addClass('hide');
            noty({ type: 'success', layout: 'topRight', text: data.Message, timeout: 20000 });
            if (data.PartialSuccess) {
                $('#errorUploadingAnnualInfo').removeClass('hide');
            }
            location.reload(true);
        } else {
            failedToUploadannualInfo(data, textStatus, jqXHR);
        }
        
    }

    var failedToUploadannualInfo = function (data) {
        $('#ecBulkUploadMessage').addClass('hide');
        $('#errorUploadingAnnualInfo').removeClass('hide');
        noty({ type: 'error', layout: 'topRight', text: data.Message || 'Unable to upload annual info', timeout: 20000 });
    }
  
    var checkForSaveSuccess = function () {
        if (window.location.href.indexOf('&saved=True') > -1) {
            noty({ text: 'Saved Successfully', layout: 'topRight', type: 'success', timeout: 10000 });
        }
    }

   
  
    $(document).ready(function () {
        checkForSaveSuccess();
        $('#organizationAnnualinfoBulkUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);
    })

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);