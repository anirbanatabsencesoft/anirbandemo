﻿/*
 * TODO: To avoid having to update this in two places, can we generate some of this via T4?
 */
(function () {
    'use strict';

    angular
        .module('App')
        .constant('CustomFieldType', {
            Text: 1,
            Number: 2,
            Flag: 4,
            Date: 8
        })

        .constant('EmploymentStatus', {
            Active: 'Active',
            Inactive: 'Inactive',
            Terminated: 'Terminated',
            Leave: 'Leave'
        })

        
    ;
})();