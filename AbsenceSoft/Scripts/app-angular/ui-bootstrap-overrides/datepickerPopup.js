﻿//(function () {
//    'use strict';

//    angular
//        .module('ui.bootstrap.datepicker')

//    .directive('datepickerPopup', ['$compile', '$parse', '$document', '$position', 'dateFilter', 'datepickerPopupConfig', 'datepickerConfig',
//        function ($compile, $parse, $document, $position, dateFilter, datepickerPopupConfig, datepickerConfig) {
//            return {
//                restrict: 'EA',
//                require: 'ngModel',
//                link: function (originalScope, element, attrs, ngModel) {
//                    var scope = originalScope.$new(), // create a child scope so we are not polluting original one
//                        dateFormat,
//                        closeOnDateSelection = angular.isDefined(attrs.closeOnDateSelection) ? originalScope.$eval(attrs.closeOnDateSelection) : datepickerPopupConfig.closeOnDateSelection,
//                        appendToBody = angular.isDefined(attrs.datepickerAppendToBody) ? originalScope.$eval(attrs.datepickerAppendToBody) : datepickerPopupConfig.appendToBody;

//                    attrs.$observe('datepickerPopup', function (value) {
//                        dateFormat = value || datepickerPopupConfig.dateFormat;
//                        ngModel.$render();
//                    });

//                    scope.showButtonBar = angular.isDefined(attrs.showButtonBar) ? originalScope.$eval(attrs.showButtonBar) : datepickerPopupConfig.showButtonBar;

//                    originalScope.$on('$destroy', function () {
//                        $popup.remove();
//                        scope.$destroy();
//                    });

//                    // Looks like attrs.$observe isn't initializing these values
//                    angular.extend(scope, {
//                        currentText: datepickerPopupConfig.currentText,
//                        toggleWeeksText: datepickerPopupConfig.toggleWeeksText,
//                        clearText: datepickerPopupConfig.clearText,
//                        closeText: datepickerPopupConfig.closeText
//                    });

//                    attrs.$observe('currentText', function (text) {
//                        scope.currentText = angular.isDefined(text) ? text : datepickerPopupConfig.currentText;
//                    });
//                    attrs.$observe('toggleWeeksText', function (text) {
//                        scope.toggleWeeksText = angular.isDefined(text) ? text : datepickerPopupConfig.toggleWeeksText;
//                    });
//                    attrs.$observe('clearText', function (text) {
//                        scope.clearText = angular.isDefined(text) ? text : datepickerPopupConfig.clearText;
//                    });
//                    attrs.$observe('closeText', function (text) {
//                        scope.closeText = angular.isDefined(text) ? text : datepickerPopupConfig.closeText;
//                    });

//                    var getIsOpen, setIsOpen;
//                    if (attrs.isOpen) {
//                        getIsOpen = $parse(attrs.isOpen);
//                        setIsOpen = getIsOpen.assign;

//                        originalScope.$watch(getIsOpen, function updateOpen(value) {
//                            scope.isOpen = !!value;
//                        });
//                    }
//                    scope.isOpen = getIsOpen ? getIsOpen(originalScope) : false; // Initial state

//                    function setOpen(value) {
//                        if (setIsOpen) {
//                            setIsOpen(originalScope, !!value);
//                        } else {
//                            scope.isOpen = !!value;
//                        }
//                    }

//                    var documentClickBind = function (event) {
//                        if (scope.isOpen && event.target !== element[0]) {
//                            scope.$apply(function () {
//                                setOpen(false);
//                            });
//                        }
//                    };

//                    var elementFocusBind = function () {
//                        scope.$apply(function () {
//                            setOpen(true);
//                        });
//                    };

//                    // popup element used to display calendar
//                    var popupEl = angular.element('<div datepicker-popup-wrap><div datepicker></div></div>');
//                    popupEl.attr({
//                        'ng-model': 'date',
//                        'ng-change': 'dateSelection()'
//                    });
//                    var datepickerEl = angular.element(popupEl.children()[0]),
//                        datepickerOptions = {};
//                    if (attrs.datepickerOptions) {
//                        datepickerOptions = originalScope.$eval(attrs.datepickerOptions);
//                        datepickerEl.attr(angular.extend({}, datepickerOptions));
//                    }

//                    // TODO: reverse from dateFilter string to Date object
//                    function parseDate(viewValue) {
//                        if (!viewValue) {
//                            ngModel.$setValidity('date', true);
//                            return null;
//                        } else if (angular.isDate(viewValue)) {
//                            ngModel.$setValidity('date', true);
//                            return viewValue;
//                        } else if (angular.isString(viewValue)) {
//                            var date = new Date(viewValue);
//                            if (isNaN(date)) {
//                                ngModel.$setValidity('date', false);
//                                return undefined;
//                            } else {
//                                ngModel.$setValidity('date', true);
//                                return date;
//                            }
//                        } else {
//                            ngModel.$setValidity('date', false);
//                            return undefined;
//                        }
//                    }
//                    ngModel.$parsers.unshift(parseDate);

//                    // Inner change
//                    scope.dateSelection = function (dt) {
//                        if (angular.isDefined(dt)) {
//                            scope.date = dt;
//                        }
//                        ngModel.$setViewValue(scope.date);
//                        ngModel.$render();

//                        if (closeOnDateSelection) {
//                            setOpen(false);
//                        }
//                    };

//                    element.bind('input change keyup', function () {
//                        scope.$apply(function () {
//                            scope.date = ngModel.$modelValue;
//                        });
//                    });

//                    // Outter change
//                    ngModel.$render = function () {
//                        var date = ngModel.$viewValue ? dateFilter(ngModel.$viewValue, dateFormat) : '';
//                        element.val(date);
//                        scope.date = ngModel.$modelValue;
//                    };

//                    function addWatchableAttribute(attribute, scopeProperty, datepickerAttribute) {
//                        if (attribute) {
//                            originalScope.$watch($parse(attribute), function (value) {
//                                scope[scopeProperty] = value;
//                            });
//                            datepickerEl.attr(datepickerAttribute || scopeProperty, scopeProperty);
//                        }
//                    }
//                    addWatchableAttribute(attrs.min, 'min');
//                    addWatchableAttribute(attrs.max, 'max');
//                    if (attrs.showWeeks) {
//                        addWatchableAttribute(attrs.showWeeks, 'showWeeks', 'show-weeks');
//                    } else {
//                        scope.showWeeks = 'show-weeks' in datepickerOptions ? datepickerOptions['show-weeks'] : datepickerConfig.showWeeks;
//                        datepickerEl.attr('show-weeks', 'showWeeks');
//                    }
//                    if (attrs.dateDisabled) {
//                        datepickerEl.attr('date-disabled', attrs.dateDisabled);
//                    }

//                    function updatePosition() {
//                        scope.position = appendToBody ? $position.offset(element) : $position.position(element);
//                        scope.position.top = scope.position.top + element.prop('offsetHeight');
//                    }

//                    var documentBindingInitialized = false, elementFocusInitialized = false;
//                    scope.$watch('isOpen', function (value) {
//                        if (value) {
//                            updatePosition();
//                            $document.bind('click', documentClickBind);
//                            if (elementFocusInitialized) {
//                                element.unbind('focus', elementFocusBind);
//                            }
//                            element[0].focus();
//                            documentBindingInitialized = true;
//                        } else {
//                            if (documentBindingInitialized) {
//                                $document.unbind('click', documentClickBind);
//                            }
//                            element.bind('focus', elementFocusBind);
//                            elementFocusInitialized = true;
//                        }

//                        if (setIsOpen) {
//                            setIsOpen(originalScope, value);
//                        }
//                    });

//                    scope.today = function () {
//                        scope.dateSelection(new Date());
//                    };
//                    scope.clear = function () {
//                        scope.dateSelection(null);
//                    };

//                    var $popup = $compile(popupEl)(scope);
//                    if (appendToBody) {
//                        $document.find('body').append($popup);
//                    } else {
//                        element.after($popup);
//                    }
//                }
//            };
//        }])

//    ;



//})();