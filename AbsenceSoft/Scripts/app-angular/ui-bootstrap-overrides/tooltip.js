﻿(function () {
    'use strict';

    angular
      .module('ui.bootstrap.tooltip')
      .directive('tooltip', tooltip);

    tooltip.$inject = [];
    function tooltip() {
        var directive = {
            link: link,
            restrict: 'A'
        };

        function link(scope, element, attrs) {
            
            $(element).tooltip({
                title: getTitle
            });

            function getTitle() {
                return attrs.tooltip;
            }

        }

        return directive;
    }
})();

