﻿(function () {
    'use strict';
    angular

    .module('App')

    .constant('Calendar', {
        Default: {
            Months: [
                { Label: 'January', Value: 1 },
                { Label: 'February', Value: 2 },
                { Label: 'March', Value: 3 },
                { Label: 'April', Value: 4 },
                { Label: 'May', Value: 5 },
                { Label: 'June', Value: 6 },
                { Label: 'July', Value: 7 },
                { Label: 'August', Value: 8 },
                { Label: 'September', Value: 9 },
                { Label: 'October', Value: 10 },
                { Label: 'November', Value: 11 },
                { Label: 'December', Value: 12 }
            ],
            MonthWeeks: [
                { Label: 'first', Value: 1 },
                { Label: 'second', Value: 2 },
                { Label: 'third', Value: 3 },
                { Label: 'fourth', Value: 4 },
                { Label: 'last', Value: -1 }
            ],
            WeekDays: [
                { Label: 'Sunday', Value: 0 },
                { Label: 'Monday', Value: 1 },
                { Label: 'Tuesday', Value: 2 },
                { Label: 'Wednesday', Value: 3 },
                { Label: 'Thursday', Value: 4 },
                { Label: 'Friday', Value: 5 },
                { Label: 'Saturday', Value: 6 }
            ]

        }
    });

})();