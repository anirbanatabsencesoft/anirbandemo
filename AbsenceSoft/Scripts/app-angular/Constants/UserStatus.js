﻿(function (undefined) {
    'use strict';

    angular
        .module('App')
        .constant('UserStatus', {
            all: getUserStatuses(),
            hash: getUserStatuses().reduce(function (prev, next) {
                prev[next.Label] = next.Value;
                return prev;
            }, {})
        });

    function getUserStatuses() {
        return [
            { Label: 'Active', Value: 'Active' },
            { Label: 'Deleted', Value: 'Deleted' },
            { Label: 'Disabled', Value: 'Disabled' },
        ];
    }

})();