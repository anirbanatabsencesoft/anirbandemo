﻿(function (undefined) {
    'use strict';

    angular
        .module('App')
        .constant('EligibilityStatus', {
            all: all(),
            byLabel: all().reduce(function (prev, next) {
                prev[next.Label] = next.Value;
                return prev;
            }, {})
        });

    function all() {
        return [
            { Label: 'Pending', Value: 0 },
            { Label: 'Ineligible', Value: -1 },
            { Label: 'Eligible', Value: 1 },
        ];
    }

})();