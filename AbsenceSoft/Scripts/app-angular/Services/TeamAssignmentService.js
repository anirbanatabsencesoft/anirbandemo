﻿angular
.module('App.Services')
.factory('teamAssignmentService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    return {
        Save: function (team) {
            var resource = $resource('/Team/Save', {}, {
                'save': { method: 'POST', isArray: false }
            });

            return resource.save(team).$promise;
        },
        Delete: function (customerId, teamId) {
            var url = '/Team/' + customerId + '/Delete/' + teamId;
            return $http.delete(url);
        },
        GetCurrentTeamMembers: function (customerId, teamId) {
            var resource = $resource('/Team/' + customerId + '/Members/' + teamId, {}, {
                'get': { method: 'GET', isArray: true }
            });

            return resource.get().$promise;
        },
        GetUsersForAssignmentByTeamLead: function(){
            var resource = $resource('/MyTeam/Assignees/', {}, {
                'get': { method: 'GET', isArray: true }
            });

            return resource.get().$promise;
        },
        AddMyTeamMember: function (teamMember) {
            var resource = $resource('/MyTeam/AddTeamMember', {}, {
                'save': { method: 'POST', isArray: true }
            });

            return resource.save(teamMember).$promise;
        },
        RemoveMyTeamMember: function (teamMember) {
            var resource = $resource('/MyTeam/RemoveTeamMember', {}, {
                'remove': { method: 'POST', isArray: true }
            });

            return resource.remove(teamMember).$promise;
        }
        
    }
}]);
