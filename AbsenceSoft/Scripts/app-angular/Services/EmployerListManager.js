//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('employerListManager', ['$resource', '$q', 'administrationService', 'lookupService', '$timeout', '$filter', function ($resource, $q, administrationService, lookupService, $timeout, $filter) {

    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 1;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight);
        }

        scope.Employers = [];
        scope.EmployersFilter = {
            EmployerName: '',
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "Name",
            SortDir: "asc",
            LoadMore: true
        };
        var resetPaging = function () {
            scope.EmployersFilter.PageNumber = 1;
            scope.Employers = [];
        };
        scope.$watch('EmployersFilter.EmployerName', resetPaging);

        list(scope, false);
    };
    
    var list = function (scope, doPaging, errorCallback) {
        scope.EmployersFilter.DoPaging = doPaging;
        administrationService.EmployerList(
            {
                EmployerName: scope.EmployersFilter.EmployerName,
                PageNumber: scope.EmployersFilter.PageNumber,
                PageSize: scope.EmployersFilter.PageSize,
                SortBy: scope.EmployersFilter.SortExpression,
                SortDirection: parseInt(scope.EmployersFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.EmployersFilter.TotalRecords = data.Total;
                    scope.EmployersFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Employers.push(result);
                        });
                    } else {
                        scope.Employers = data.Results;                       
                    }
                    scope.HasLinks = false;
                    var employerWithLinks = $filter('filter')(scope.Employers, { DoExportComplete: true });
                    if(employerWithLinks.length > 0)
                        scope.HasLinks = true;
                    
                } else {
                    scope.Employers = [];
                    scope.EmployersFilter.LoadMore = false;
                    scope.EmployersFilter.TotalRecords = 0;
                }
            },
            errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        // sort
        if (scope.EmployersFilter.SortExpression == sortBy)
            scope.EmployersFilter.SortDir = (scope.EmployersFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.EmployersFilter.SortExpression = sortBy;
            scope.EmployersFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.EmployersFilter.TotalRecords / scope.EmployersFilter.PageSize);
        if (totalPages > 1) {
            if (scope.EmployersFilter.LoadMore && scope.EmployersFilter.PageNumber < totalPages) {
                scope.EmployersFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };

    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore,
    };
}])