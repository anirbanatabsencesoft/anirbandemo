//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('employeeService', ['$resource', '$q', function ($resource, $q) {
    var employeeResource = $resource('/employees/list', {}, {
        'list': { method: 'POST', isArray: false }
    });
    var createEmployee = function (postData) {
        var resource = $resource('/Employees/Create', {}, {
            'save': {
                method: 'POST', isArray: false
            }
        });
        return resource.save(postData).$promise;
    };
    var deleteEmployee = function (employeeId) {
        var resource = $resource('/Employees/' + employeeId + '/Delete', {}, {
            'save': {
                method: 'POST', isArray: false
            }
        });
        return resource.save().$promise;
    };
    var getEmployeeForEdit = function (employeeId) {
        var resource = $resource('/Employees/Employee/' + employeeId, {}, {
            'list': { method: 'GET', isArray: false }
        });
        return resource.list().$promise;
    };
    var editEmployee = function (employeeId, postData) {
        var resource = $resource('/Employees/' + employeeId + '/Edit', {}, {
            'save': {
                method: 'POST', isArray: false
            }
        });
        return resource.save(postData).$promise;
    };

    var GetTimeTrackingInfo = function (employeeId) {
        var resource = $resource('/Employees/' + employeeId + '/TimeTrackingInfo', {}, {
            'list': {
                method: 'GET', isArray: true
            }
        });

        return resource.list().$promise;
    };

    var GetEmployeeWorkSchedule = function (employeeId) {
        var resource = $resource('/Employees/GetEmployeeWorkSchedule/' + employeeId, {}, {
            'list': { method: 'GET', isArray: false }
        });
        return resource.list().$promise;
    };


    var GetNextPrevRangeForCalendarForEmployee = function (date, nxtOrPrev) {
        var dt = new Date(date);
            
        if (nxtOrPrev == "prev")
            var rangeStart = dt.firstOfMonth().addMonths(-1);
        else
            //var rangeStart = new Date(dt.getFullYear(), dt.getMonth(), 1);
            var rangeStart = dt.firstOfMonth().addMonths(1);
            
        // end should be 3 months forward (exclusive)
        var rangeEnd = rangeStart.addMonths(12);        //.toFakeUTCDay();

        return {
            Start: rangeStart,
            End: rangeEnd
        };
    };

    var EmployeeExistingRelationships = function (employeeId, absenceReasonCode) {
        var resource = $resource('/Case/Employee/' + employeeId + '/GetEmployeeContacts/?absenceReasonCode=' + absenceReasonCode, {}, {
            'list': { method: 'GET', isArray: true }
        });
        return resource.list().$promise;
    };

    var allEmployeeContacts = function (employeeId) {
        var resource = $resource('/Case/Employee/' + employeeId + '/GetAllEmployeeContacts', {}, {
            'list': { method: 'GET', isArray: true }
        });
        return resource.list().$promise;
    };

    var allEmployerContacts = function (employerId) {
        var resource = $resource('/Case/Employer/' + employerId + '/GetAllEmployerContacts', {}, {
            'list': { method: 'GET', isArray: true }
        });
        return resource.list().$promise;
    };

    var IsEmployeeInfoComplete = function (employeeId, absenceReasonCode) {
        var resource = $resource('/Employees/IsEmployeeInfoComplete/' + employeeId , {}, {
            'get': { method: 'GET', isArray: false }
        });
        return resource.get().$promise;
    };

    var UpdatePaySchedule = function (model) {
        var resource = $resource('/Employees/UpdatePaySchedule', {}, {
            'updatePaySchedule': { method: 'POST', isArray: false }
        });
        return resource.updatePaySchedule(model).$promise;
    };

    var getEmployeeInfo = function (employeeId, caseId) {
        var resource = $resource('/Employees/' + employeeId + '/GetInfo', {}, {
            'list': { method: 'GET', isArray: false }
        });
        return resource.list({caseId: caseId}).$promise;
    };

    var getEmployeeLocationHistory = function (employeeNumber, employerId) {
        var resource = $resource('/Employees/' + employerId +'/EmployeeActionHistory/' + employeeNumber , {}, {
            'list': { method: 'GET', isArray: true }
        });
        return resource.list().$promise;
    }

    var getEECustomFields = function (employerId) {
        var resource = $resource('/Administration/' + employerId + '/GetEECustomFields', {}, {
            'list': { method: 'GET', isArray: true }
        });
        return resource.list({page:1}).$promise;
    };

    var getEmployerFeature = function (employeeId) {
        var resource = $resource('/Employees/' + employeeId + '/GetEmployerFeature', {}, {
            'get': { method: 'GET', isArray: false }
        });
        return resource.get().$promise;
    };

    var getEmployeeVariableSchedule = function (employeeData) {
        var resource = $resource('/Employees/' + employeeData.EmployeeId + '/VariableSchedule/?month=' + employeeData.Month + '&year=' + employeeData.Year, {}, {
            'get': {method: 'GET', isArray: true}
        });
        return resource.get().$promise;
    };

    var getEmployeeJob = function (employeeData) {
        var resource = $resource('/Employees/' + employeeData.EmployeeId + '/EmployeeJob', {}, {
            'get': { method: 'GET', isArray: true }
        });
        return resource.get().$promise;
    };

    var calculateRiskProfile = function (employeeId, caseId) {
        var url = '/RiskProfiles/CalculateRiskProfile?employeeId=' + employeeId;
        if (caseId)
            url += '&caseId=' + caseId;
        var resource = $resource(url, {}, {
            'calculate': { method: 'POST', isArray: false }
        });

        return resource.calculate();
    }

    return {
        List: function (filterData) {
            return employeeResource.list(filterData);
        },
        CreateEmployee: createEmployee,
        DeleteEmployee: deleteEmployee,
        GetEmployeeForEdit: getEmployeeForEdit,
        EditEmployee: editEmployee,
        GetTimeTrackingInfo: GetTimeTrackingInfo,
        GetNextPrevRangeForCalendarForEmployee: GetNextPrevRangeForCalendarForEmployee,
        GetEmployeeWorkSchedule: GetEmployeeWorkSchedule,
        EmployeeExistingRelationships: EmployeeExistingRelationships,
        AllEmployeeContacts: allEmployeeContacts,
        AllEmployerContacts: allEmployerContacts,
        IsEmployeeInfoComplete: IsEmployeeInfoComplete,
        UpdatePaySchedule: UpdatePaySchedule,
        GetEmployeeInfo: getEmployeeInfo,
        GetEmployeeLocationHistory: getEmployeeLocationHistory,
        GetEECustomFields: getEECustomFields,
        GetEmployerFeature: getEmployerFeature,
        GetEmployeeVariableSchedule: getEmployeeVariableSchedule,
        CalculateRiskProfile: calculateRiskProfile
    };
}])

