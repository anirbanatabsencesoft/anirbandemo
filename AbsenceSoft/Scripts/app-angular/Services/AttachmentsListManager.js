//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('attachmentListManager', ['$resource', '$q', 'attachmentService', 'lookupService', function ($resource, $q, attachmentService, lookupService) {

    var init = function (scope, infiniteScrollOnBody) {

        scope.attachmentTypes = lookupService.GetAttachmentTypes().then(function (response) {
            scope.attachmentTypes = response;
        });

        var proposedPageSize = 5;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height() / 2;
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Attachments = [];
        scope.AttachmentsFilter = {
            CaseId: scope.CaseId,
            CreatedDate: '',
            CreatedDateAsDate: '',
            AttachmentType: -1,
            Description: '',
            FileName: '',
            AttachedBy: '',
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "cdt",
            SortDir: "desc",
            LoadMore: true
        };



        var resetPaging = function () {
            scope.AttachmentsFilter.PageNumber = 1;
            scope.Attachments = [];
        };
        scope.$watch('AttachmentsFilter.CreatedDate', resetPaging);
        scope.$watch('AttachmentsFilter.AttachmentType', resetPaging);
        scope.$watch('AttachmentsFilter.Description', resetPaging);
        scope.$watch('AttachmentsFilter.FileName', resetPaging);
        scope.$watch('AttachmentsFilter.AttachedBy', resetPaging);

        list(scope, false);
    };
    var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
    var list = function (scope, doPaging, errorCallback) {
        // convert the date        
        if (dateInputRegex.test(scope.AttachmentsFilter.CreatedDate)) {
            try {
                scope.AttachmentsFilter.CreatedDateAsDate = new Date(scope.AttachmentsFilter.CreatedDate).valueOf();
                isValidUpdteField = true;
            }
            catch (e) {

            }
        }

        scope.AttachmentsFilter.CreatedDateAsDate = null;
        var isValidDateField = false;
        if (scope.AttachmentsFilter.CreatedDate != null && scope.AttachmentsFilter.CreatedDate != '' && scope.AttachmentsFilter.CreatedDate != undefined) {
            var dueDate = new Date(scope.AttachmentsFilter.CreatedDate);
            var strDueDate = dueDate.getMonth() + "/" + dueDate.getDate() + "/" + dueDate.getFullYear();
            if (dateInputRegex.test(strDueDate)) {
                try {
                    scope.AttachmentsFilter.CreatedDateAsDate = new Date(scope.AttachmentsFilter.CreatedDate).valueOf();
                    isValidDateField = true;
                }
                catch (e) {
                }
            }
        }

        if (scope.AttachmentsFilter.CreatedDate != null && scope.AttachmentsFilter.CreatedDate != '' && scope.AttachmentsFilter.CreatedDate != undefined && isValidDateField == false) {
            scope.Attachments = [];
            scope.AttachmentsFilter.LoadMore = false;
            scope.AttachmentsFilter.TotalRecords = 0;
            return;
        }

        scope.AttachmentsFilter.DoPaging = doPaging;

        attachmentService.List(
           {
               CaseId: scope.AttachmentsFilter.CaseId,
               CreatedDate: scope.AttachmentsFilter.CreatedDateAsDate,
               AttachmentType: scope.AttachmentsFilter.AttachmentType == -1 ? null : scope.AttachmentsFilter.AttachmentType,
               Description: scope.AttachmentsFilter.Description,
               FileName: scope.AttachmentsFilter.FileName,
               AttachedBy: scope.AttachmentsFilter.AttachedBy,
               PageNumber: scope.AttachmentsFilter.PageNumber,
               PageSize: scope.AttachmentsFilter.PageSize,
               SortBy: scope.AttachmentsFilter.SortExpression,
               SortDirection: parseInt(scope.AttachmentsFilter.SortDir == "asc" ? 1 : -1)
           })
           .$promise.then(
           function (data, status) {
               if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                   scope.AttachmentsFilter.TotalRecords = data.Total;
                   scope.AttachmentsFilter.LoadMore = true;
                   if (doPaging) {
                       angular.forEach(data.Results, function (result) {
                           scope.Attachments.push(result);
                       });
                   } else {
                       scope.Attachments = data.Results;
                   }
               } else {
                   scope.Attachments = [];
                   scope.AttachmentsFilter.LoadMore = false;
                   scope.AttachmentsFilter.TotalRecords = 0;
               }
           },
           errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        //reset the paging
        scope.AttachmentsFilter.PageNumber = 1;
        scope.Attachments = [];

        // sort
        if (scope.AttachmentsFilter.SortExpression == sortBy)
            scope.AttachmentsFilter.SortDir = (scope.AttachmentsFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.AttachmentsFilter.SortExpression = sortBy;
            scope.AttachmentsFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.AttachmentsFilter.TotalRecords / scope.AttachmentsFilter.PageSize);
        if (totalPages > 1) {
            if (scope.AttachmentsFilter.LoadMore && scope.AttachmentsFilter.PageNumber < totalPages) {
                scope.AttachmentsFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore
    }
}]);