//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('policyService',[ '$resource', '$q', '$http', function ($resource, $q, $http) {
    var policyResource = $resource('/Policies/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var policyAbsReasonResource = $resource('/Policies/AbsReasonList', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var policyDeleteResource = $resource('/Policy/:EmployerId/Delete/:Code', {}, {
        'delete': { method: 'DELETE', isArray: false }
    });

    return {
        List: function (filterData) {
            return policyResource.list(filterData);
        },

        AbsReasonList: function (filterData) {
            return policyAbsReasonResource.list(filterData);
        },

        GetById: function (policyId, employerId) {

            var resource = $resource('/Policies/' + policyId + '/GetPolicyById/' + employerId, {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },

        Upsert: function (data) {

            var resource = $resource('/Policies/' + data.Id + '/UpsertPolicyPost', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(data).$promise;
        },

        ToggleEnabled: function (policyId, employerId) {
            var resource = $resource('/Policies/' + policyId + '/ToggleEnabledPolicy/' + employerId, {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save().$promise;
        },

        Delete: function (policyData) {
            return policyDeleteResource.delete(policyData).$promise;
        },

        CheckForDuplicatePolicy: function (code) {
            var url = '/Policy/' + code.EmployerId + '/Duplicate/' + code.Code + '?policyId=' + code.PolicyId;
            return $http.get(url);
        },

        RemoveAbsReason: function (data) {
            var resource = $resource('/Policies/' + data.PolicyId + '/RemoveAbsReason', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(data).$promise;
        },

        GetRuleExpressions: function (employerId) {

            var resource = $resource('/Policies/GetRuleExpressions', {}, {
                'list': {
                    method: 'GET', isArray: true
                }
            });

            return resource.list({ employerId: employerId }).$promise;
        },

        RuleToRuleExpression: function (rules) {
            var resource = $resource('/Policies/RuleToRuleExpression', {}, {
                'read': {
                    method: 'POST', isArray: true
                }
            });

            return resource.read(rules).$promise;
        },

        ValidateExpression: function (expression) {
            var resource = $resource('/Policies/RuleExpressionToRule', {}, {
                'read': {
                    method: 'POST', isArray: true
                }
            });

            return resource.read(expression).$promise;
        },
        GetPoliciesPayOrder: function (employerId) {
            var resource = $resource('/Policies/' + employerId + '/PoliciesPayOrder', {}, {
                'get': {
                    method: 'GET', isArray: true
                }
            });

            return resource.get().$promise;
        },
        SavePoliciesPayOrder: function (model) {
            var resource = $resource('/Policies/PoliciesPayOrder', {}, {
                'save': {
                    method: 'POST', isArray: true
                }
            });

            return resource.save(model).$promise;
        },
        CheckPolicyForConflicts: function (policy) {
            var url = '/Policies/' + policy.Id + '/CheckPolicyForConflicts';

            return $http.post(url, policy);
        },
        GetPolicyCrosswalkTypes: function (employerId) {
            var url = '/PolicyCrosswalk/GetAllCrosswalkTypes/' + employerId;
            return $http.get(url);
        }


    };
}]);