//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
    .module('App.Services')
    .factory('employeeListManager', ['$resource', '$q', 'employeeService', function ($resource, $q, employeeService) {

        var init = function (scope, infiniteScrollOnBody)
        {
            var proposedPageSize = 10;
            if (infiniteScrollOnBody) {
                var topHeaderOffeset = 110;
                var rowHeight = 19;
                var documentHeight = $(window).height();
                proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
            }

            scope.Employees = [];
            scope.EmployeesFilter = {
                Name: '',
                Employer: '',
                EmployeeNumber: '',
                HireDate: '',
                HireDateAsDate: null,
                DoPaging: false,
                PageNumber: 1,
                PageSize: proposedPageSize,
                TotalRecords: 0,
                SortExpression: "Name",
                SortDir: "asc",
                LoadMore: true
            };
            var resetPaging = function () {
                scope.EmployeesFilter.PageNumber = 1;
                scope.Employees = [];
            };
            scope.$watch('EmployeesFilter.Name', resetPaging);
            scope.$watch('EmployeesFilter.Employer', resetPaging);
            scope.$watch('EmployeesFilter.EmployeeNumber', resetPaging);
            scope.$watch('EmployeesFilter.HireDate', resetPaging);
            list(scope, false);
        };
        var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
        var list = function (scope, doPaging, errorCallback) {

            // convert the date
            scope.EmployeesFilter.HireDateAsDate = null;
            var isValidUpdteField = false;
            if (dateInputRegex.test(scope.EmployeesFilter.HireDate)) {
                try {
                    var HireDateAsDate = new Date(scope.EmployeesFilter.HireDate).valueOf();
                    scope.EmployeesFilter.HireDateAsDate = (HireDateAsDate - (new Date(HireDateAsDate).getTimezoneOffset()));
                    
                 isValidUpdteField = true;
             }
             catch (e) {

             }
         }

         if (scope.EmployeesFilter.HireDate != "" && isValidUpdteField == false) {
             scope.Employees = [];
             scope.EmployeesFilter.LoadMore = false;
             scope.EmployeesFilter.TotalRecords = 0;
             return;
         }
         
        scope.EmployeesFilter.DoPaging = doPaging;
        employeeService.List(
            {
                EmployerId: scope.EmployeesFilter.Employer,
                Name: scope.EmployeesFilter.Name,
                EmployeeNumber: scope.EmployeesFilter.EmployeeNumber,
                HireDate: scope.EmployeesFilter.HireDateAsDate,
                PageNumber: scope.EmployeesFilter.PageNumber,
                PageSize: scope.EmployeesFilter.PageSize,
                SortBy: scope.EmployeesFilter.SortExpression,
                SortDirection: parseInt(scope.EmployeesFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.EmployeesFilter.TotalRecords = data.Total;
                    scope.EmployeesFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Employees.push(result);
                        });
                    } else {
                        scope.Employees = data.Results;
                    }
                } else {
                    scope.Employees = [];
                    scope.EmployeesFilter.LoadMore = false;
                    scope.EmployeesFilter.TotalRecords = 0;
                }
            },
            errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        //reset the paging
        scope.EmployeesFilter.PageNumber = 1;
        scope.Employees = [];

        // sort
        if (scope.EmployeesFilter.SortExpression == sortBy)
            scope.EmployeesFilter.SortDir = (scope.EmployeesFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.EmployeesFilter.SortExpression = sortBy;
            scope.EmployeesFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.EmployeesFilter.TotalRecords / scope.EmployeesFilter.PageSize);
        if (totalPages > 1) {
            if (scope.EmployeesFilter.LoadMore && scope.EmployeesFilter.PageNumber < totalPages) {
                scope.EmployeesFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    return {
        Init: init,
        List: list,
        UpdateSort : updateSort,
        LoadMore : loadMore
    }
}])