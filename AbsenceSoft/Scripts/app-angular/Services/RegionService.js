﻿angular
.module('App.Services')
.factory('regionService', ['$resource', '$q', function ($resource, $q) {
    var regionResource = $resource('/Lookups/ListCountriesAndRegions', {}, {
        'list': {method: 'GET', isArray: true }
    });

    var countriesResource = $resource('/Lookups/ListCountries', {},  {
        'list': {method: 'GET', isArray: true}
    })

    var allCountriesAndRegions = [];
    var countriesPromise = undefined;

    var getCountryByCountryCode = function (countryCode) {
        for (var i = 0; i < allCountriesAndRegions.length; i++) {
            var country = allCountriesAndRegions[i];
            if (country.Code == countryCode)
                return country;
        }

        return undefined;
    }

    var getRegionsByCountryCode = function (countryCode) {
        var country = getCountryByCountryCode(countryCode);
        if(country && country.Regions)
            return country.Regions;
        else
            return [];
    }

    var getRegionNameByCountryCode = function (countryCode) {
        var country = getCountryByCountryCode(countryCode);
        if(country && country.RegionName)
            return country.RegionName;
        else
            return "Region";
    }

    var populateRegionsAndReturnValue = function () {
        if(!countriesPromise){
            countriesPromise = regionResource.list().$promise;
        }
        
        countriesPromise.then(function (countries) {
            allCountriesAndRegions = countries;
        });
    }

    return {
        GetCountries: function(){
            return countriesResource.list();
        },
        GetRegionsByCountryCode: function (countryCode) {
            if (allCountriesAndRegions.length == 0) {
                populateRegionsAndReturnValue();
                return countriesPromise.then(function () {
                    return $.when(getRegionsByCountryCode(countryCode));
                })
            } else {
                return $q.when(getRegionsByCountryCode(countryCode));
            }
        },
        GetRegionNameByCountryCode: function (countryCode) {
            if (allCountriesAndRegions.length == 0) {
                populateRegionsAndReturnValue();
                return countriesPromise.then(function () {
                    return $q.when(getRegionNameByCountryCode(countryCode));
                })
            } else {
                return $q.when(getRegionNameByCountryCode(countryCode));
            }
        }
    }


}]);