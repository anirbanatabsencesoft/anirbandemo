//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('supHrCaseListManager', [ '$resource', '$q', 'caseService', '$filter', function ($resource, $q, caseService, $filter) {
    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Cases = [];
        scope.CasesFilter = {
            Employer: '',
            EmployeeName: '',
            Reason: '',
            Updated: '',
            UpdatedAsDate: null,
            CaseNumber: '',
            Status: 0,
            AssignedToName: '',
            ListType: scope.ListType,
            DoPaging: false,
            PageNumber: 1,
            PageSize: 5,
            TotalRecords: 0,
            SortExpression: "StartDate",
            SortDir: "asc",
            LoadMore: true
        };

        list(scope, false);
    }

    var list = function (scope, doPaging) {
        scope.Cases.push({
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408cca9c417',
            EmployeeName: "Test Employee",
            CaseNumber: '0012345',
            StartDate: '2014/10/01',
            RTWDate: '2014/10/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408dda9c417',
            EmployeeName: "Test Employee",
            CaseNumber: '0012555',
            StartDate: '2014/11/01',
            RTWDate: '2014/11/01',
            Status: 'Pending'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa912169408dda9c417',
            EmployeeName: "Test Employee",
            CaseNumber: '0012445',
            StartDate: '2014/08//01',
            RTWDate: '2014/08/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408cca9c417',
            EmployeeName: "Test Employee",
            CaseNumber: '0012345',
            StartDate: '2014/10/01',
            RTWDate: '2014/10/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408dda9c417',
            EmployeeName: "TestAR Adopt",
            CaseNumber: '0012555',
            StartDate: '2014/11/01',
            RTWDate: '2014/11/01',
            Status: 'Pending'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa912169408dda9c417',
            EmployeeName: "TestAR Adopt",
            CaseNumber: '0012445',
            StartDate: '2014/08//01',
            RTWDate: '2014/08/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408cca9c417',
            EmployeeName: "TestAR Adopt",
            CaseNumber: '0012345',
            StartDate: '2014/10/01',
            RTWDate: '2014/10/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408dda9c417',
            EmployeeName: "TestAR Adopt",
            CaseNumber: '0012555',
            StartDate: '2014/11/01',
            RTWDate: '2014/11/01',
            Status: 'Pending'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa912169408dda9c417',
            EmployeeName: 'TestAR BoneMarrowHrsUsed528',
            CaseNumber: '0012445',
            StartDate: '2014/08//01',
            RTWDate: '2014/08/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408cca9c417',
            EmployeeName: 'TestAR BoneMarrowHrsUsed528',
            CaseNumber: '0012345',
            StartDate: '2014/10/01',
            RTWDate: '2014/10/01',
            Status: 'Approved'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408dda9c417',
            EmployeeName: 'TestAR BoneMarrowHrsUsed528',
            CaseNumber: '0012555',
            StartDate: '2014/11/01',
            RTWDate: '2014/11/01',
            Status: 'Pending'
        },
        {
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa912169408dda9c417',
            EmployeeName: 'TestAR BoneMarrowHrsUsed528',
            CaseNumber: '0012445',
            StartDate: '2014/08//01',
            RTWDate: '2014/08/01',
            Status: 'Approved'
        });
    }

    var filter = function (scope, doPaging) {
        var hasFilter = false;

        if (scope.CasesFilter.EmployeeName != null && scope.CasesFilter.EmployeeName != undefined && scope.CasesFilter.EmployeeName != '') {
            hasFilter = true;
            scope.Cases = $filter('filter')(scope.Cases, { EmployeeName: scope.CasesFilter.EmployeeName });
        }

        if (scope.CasesFilter.CaseNumber != null && scope.CasesFilter.CaseNumber != undefined && scope.CasesFilter.CaseNumber != '') {
            hasFilter = true;
            scope.Cases = $filter('filter')(scope.Cases, { CaseNumber: scope.CasesFilter.CaseNumber });
        }

        if (scope.CasesFilter.Status != null && scope.CasesFilter.Status != undefined && scope.CasesFilter.Status != '') {
            hasFilter = true;
            scope.Cases = $filter('filter')(scope.Cases, { Status: scope.CasesFilter.Status });
        }

        if (scope.CasesFilter.StartDate != null && scope.CasesFilter.StartDate != undefined && scope.CasesFilter.StartDate != '') {
            hasFilter = true;
            scope.Cases = $filter('filter')(scope.Cases, { StartDate: scope.CasesFilter.StartDate });
        }

        if (scope.CasesFilter.RTWDate != null && scope.CasesFilter.RTWDate != undefined && scope.CasesFilter.RTWDate != '') {
            hasFilter = true;
            scope.Cases = $filter('filter')(scope.Cases, { RTWDate: scope.CasesFilter.RTWDate });
        }

        if (!hasFilter) {
            scope.Cases = [];
            list(scope, false);
        }
    }

    var loadMore = function (scope, errorCallback) {
        scope.Cases.push({
            EmployeeId: '000000000000000000000014',
            CaseId: '54632aa914169408cca9c417',
            EmployeeName: 'TestAR BoneMarrowHrsUsed528',
            CaseNumber: '0012345',
            StartDate: '2014/10/01',
            RTWDate: '2014/10/01',
            Status: 'Approved'
        },
         {
             EmployeeId: '000000000000000000000014',
             CaseId: '54632aa914169408dda9c417',
             EmployeeName: 'TestAR Adopt',
             CaseNumber: '0012555',
             StartDate: '2014/11/01',
             RTWDate: '2014/11/01',
             Status: 'Pending'
         },
         {
             EmployeeId: '000000000000000000000014',
             CaseId: '54632aa912169408dda9c417',
             EmployeeName: 'Test Employee',
             CaseNumber: '0012445',
             StartDate: '2014/08//01',
             RTWDate: '2014/08/01',
             Status: 'Approved'
         });
    };

    var updateSort = function (scope, sortBy, errorCallback) {
        scope.CasesFilter.SortExpression = sortBy.toString();
        scope.CasesFilter.SortDir = (scope.CasesFilter.SortDir == 'desc' ? 'asc' : 'desc');
        var SortDir = (scope.CasesFilter.SortDir == 'desc' ? false : true);
        scope.Cases = $filter('orderBy')(scope.Cases, sortBy.toString(), SortDir);
    };

    return {
        Init: init,
        LoadMore: loadMore,
        UpdateSort: updateSort,
        List: list,
        Filter: filter
    }
}])