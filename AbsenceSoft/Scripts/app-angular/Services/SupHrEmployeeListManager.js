//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('supHrEmployeesListManager', [ '$resource', '$q', 'caseService', '$filter', 'lookupService', function ($resource, $q, caseService, $filter, lookupService) {
    var init = function (scope, infiniteScrollOnBody) {

        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Employees = [];
        scope.EmployeesFilter = {
            EmployeeName: '',
            EmployeeNumber: '',
            Gender:'',
            Email: '',
            HireDate: null,
            DoPaging: false,
            PageNumber: 1,
            PageSize: 5,
            TotalRecords: 0,
            SortExpression: "Title",
            SortDir: "asc",
            LoadMore: true
        };

        list(scope, false);
    }

    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }
    var i;
    var list = function (scope, doPaging) {
        for (i = 1 ; i < 10 ; i++) {
            scope.Employees.push({
                EmployeeName: 'Test Emp ' + i.toString(),
                EmployeeNumber: zeroPad(i, 9),
                Email: 'testemp' + i.toString() + '@absencesoft.com',
                HireDate: new Date().addMonths(50-i),
            })
        }
    }

    var filter = function (scope, doPaging) {
        var hasFilter = false;

        if (scope.EmployeesFilter.EmployeeName != null && scope.EmployeesFilter.EmployeeName != undefined && scope.EmployeesFilter.EmployeeName != '') {
            hasFilter = true;
            scope.Employees = $filter('filter')(scope.Employees, { EmployeeName: scope.EmployeesFilter.EmployeeName });
        }

        if (scope.EmployeesFilter.EmployeeNumber != null && scope.EmployeesFilter.EmployeeNumber != undefined && scope.EmployeesFilter.EmployeeNumber != '') {
            hasFilter = true;
            scope.Employees = $filter('filter')(scope.Employees, { EmployeeNumber: scope.EmployeesFilter.EmployeeNumber });
        }
       
        if (scope.EmployeesFilter.Email != null && scope.EmployeesFilter.Email != undefined && scope.EmployeesFilter.Email != '') {
            hasFilter = true;
            scope.Employees = $filter('filter')(scope.Employees, { Email: scope.EmployeesFilter.Email });
        }

        if (scope.EmployeesFilter.HireDate != null && scope.EmployeesFilter.HireDate != undefined && scope.EmployeesFilter.HireDate != '') {
            hasFilter = true;
            scope.Employees = $filter('filter')(scope.Employees, { HireDate: scope.EmployeesFilter.HireDate });
        }

        if (!hasFilter) {
            scope.Employees = [];
            list(scope, false);
        }
    }

    var loadMore = function (scope, errorCallback) {
        var nextlimit = i + 2;
        for (; i < nextlimit ; i++) {
            scope.Employees.push({
                EmployeeName: 'Test Emp ' + i.toString(),
                EmployeeNumber: zeroPad(i, 9),
                Email: 'testemp' + i.toString() + '@absencesoft.com',
                HireDate: new Date().addMonths(50 - i),
            })
        }
    };

    var updateSort = function (scope, sortBy, errorCallback) {
        scope.EmployeesFilter.SortExpression = sortBy.toString();
        scope.EmployeesFilter.SortDir = (scope.EmployeesFilter.SortDir == 'desc' ? 'asc' : 'desc');
        var SortDir = (scope.EmployeesFilter.SortDir == 'desc' ? false : true);
        scope.Employees = $filter('orderBy')(scope.Employees, sortBy.toString(), SortDir);
    };

    return {
        Init: init,
        LoadMore: loadMore,
        UpdateSort: updateSort,
        List: list,
        Filter: filter
    }
}])