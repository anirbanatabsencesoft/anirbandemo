//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('toDoListManager', ['$resource', '$q', 'todoService', 'lookupService', '$timeout', function ($resource, $q, todoService, lookupService, $timeout) {

    var init = function (scope, infiniteScrollOnBody) {
        scope.todoItemStatuses = lookupService.WorkflowItemStatuses().then(function (response)
        {
            scope.todoItemStatuses = response;
            //angular.forEach(scope.todoItemStatuses, function (item, index) {
            //    if (item.Text == "Overdue" || item.Text == "Pending") {
            //        var position = $.inArray(item, scope.todoItemStatuses);
            //        if (~position)
            //            scope.todoItemStatuses.splice(position, 1);
            //    }
            //});
        });
        
        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.ToDos = [];
        scope.ToDosFilter = {
            Text: '',
            Employer:'',
            Employee: '',
            Due: '',
            CaseId: null,
            Status: 4,
            DueAsDate: null,
            AssignedToName: '',
            ListType: scope.ListType,
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "DueDate",
            SortDir: "asc",
            LoadMore: true
        };
        var resetPagination = function () {
            resetPaging(scope);
        };
        scope.$watch('ToDosFilter.Text', resetPagination);
        scope.$watch('ToDosFilter.Employer', resetPagination);
        scope.$watch('ToDosFilter.Employee', resetPagination);
        scope.$watch('ToDosFilter.Due', resetPagination);
        scope.$watch('ToDosFilter.Status', resetPagination);
        scope.$watch('ToDosFilter.AssignedToName', resetPagination);

        list(scope, false);
    };
    var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
    var list = function (scope, doPaging, errorCallback) {
        // convert the date
        scope.ToDosFilter.DueAsDate = null;
        var isValidDateField = false;
        if (scope.ToDosFilter.Due != null && scope.ToDosFilter.Due != '' && scope.ToDosFilter.Due != undefined) {
            var dueDate = new Date(scope.ToDosFilter.Due);
            var strDueDate = dueDate.getMonth() + "/" + dueDate.getDate() + "/" + dueDate.getFullYear();
            if (dateInputRegex.test(strDueDate)) {
                try {
                    scope.ToDosFilter.DueAsDate = new Date(scope.ToDosFilter.Due).valueOf();
                    isValidDateField = true;
                }
                catch (e) {
                }
            }
        }

        if (scope.ToDosFilter.Due != null && scope.ToDosFilter.Due != "" && isValidDateField == false && scope.ToDosFilter.Due != undefined) {
            scope.ToDos = [];
            scope.ToDosFilter.LoadMore = false;
            scope.ToDosFilter.TotalRecords = 0;
            return;
        }

        // Get the numeric date representation of our updated filter if a valid date.
        scope.ToDosFilter.DoPaging = doPaging;
        todoService.List(
            {
                EmployerId: scope.ToDosFilter.Employer,
                EmployeeName: scope.ToDosFilter.Employee,
                CaseId: null,
                Title: scope.ToDosFilter.Text,
                DueDate: scope.ToDosFilter.DueAsDate,
                Status: parseInt(scope.ToDosFilter.Status),
                AssignedToName: scope.ToDosFilter.AssignedToName,
                ListType: scope.ToDosFilter.ListType,
                CaseId: scope.ToDosFilter.CaseId,
                PageNumber: scope.ToDosFilter.PageNumber,
                PageSize: scope.ToDosFilter.PageSize,
                SortBy: scope.ToDosFilter.SortExpression,
                SortDirection: parseInt(scope.ToDosFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.ToDosFilter.TotalRecords = data.Total;
                    scope.ToDosFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.ToDos.push(result);
                            InitReassignTodosViewModel(scope);
                        });
                    } else {
                        scope.ToDos = data.Results;
                        InitReassignTodosViewModel(scope);
                    }
                } else {
                    scope.ToDos = [];
                    scope.ToDosFilter.LoadMore = false;
                    scope.ToDosFilter.TotalRecords = 0;
                }
            },
            errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        resetPaging(scope);
        // sort
        if (scope.ToDosFilter.SortExpression == sortBy)
            scope.ToDosFilter.SortDir = (scope.ToDosFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.ToDosFilter.SortExpression = sortBy;
            scope.ToDosFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.ToDosFilter.TotalRecords / scope.ToDosFilter.PageSize);
        if (totalPages > 1) {
            if (scope.ToDosFilter.LoadMore && scope.ToDosFilter.PageNumber < totalPages) {
                scope.ToDosFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    var initToDosFilter = function (scope) {
        scope.ToDos = [];
        scope.ToDosFilter = {
            Text: '',
            Employee: '',
            Due: '',
            CaseId: null,
            Status: 4,
            DueAsDate: null,
            AssignedToName: '',
            ListType: '',
            DoPaging: false,
            PageNumber: 1,
            PageSize: 50,
            TotalRecords: 0,
            SortExpression: "DueDate",
            SortDir: "asc",
            LoadMore: true
        };
    };

    var resetPaging = function (scope) {
        scope.ToDosFilter.PageNumber = 1;
        scope.ToDos = [];
    };

    var getWorkFlowItemStatues = function (scope) {
        scope.todoItemStatuses = lookupService.WorkflowItemStatuses().then(function (response) {
            scope.todoItemStatuses = response;
            //angular.forEach(scope.todoItemStatuses, function (item, index) {
            //    if (item.Text == "Overdue" || item.Text == "Pending") {
            //        var position = $.inArray(item, scope.todoItemStatuses);
            //        if (~position)
            //            scope.todoItemStatuses.splice(position, 1);
            //    }
            //});
        });
    };

    var InitReassignTodosViewModel = function (scope) {
        scope.reassigntodos = { UserId: '', Todos: [] };
        angular.forEach(scope.ToDos, function (theTodo, index) {
            var todoDetail = new Object();
            todoDetail.TodoId = theTodo.Id;
            todoDetail.ApplyStatus = false;
            scope.reassigntodos.Todos.push(todoDetail);
        });
    };

    return {
        Init: init,
        InitToDosFilter: initToDosFilter,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore,
        GetNextToDo: todoService.NextToDo,
        GetWorkFlowItemStatues: getWorkFlowItemStatues
    };
}])