//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('employeeCaseListManager', ['$resource','$q','caseService','$filter',function ($resource, $q, caseService, $filter) {
    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Cases = [];
        //scope.CasesFilter = {
        //    Employer: '',
        //    EmployeeName: '',
        //    Reason: '',
        //    Updated: '',
        //    UpdatedAsDate: null,
        //    CaseNumber: '',
        //    Status: 0,
        //    AssignedToName: '',
        //    ListType: scope.ListType,
        //    DoPaging: false,
        //    PageNumber: 1,
        //    PageSize: 5,
        //    TotalRecords: 0,
        //    SortExpression: "StartDate",
        //    SortDir: "asc",
        //    LoadMore: true
        //};

        list(scope);
    }

    var list = function (scope) {
        caseService.GetEmployeeCases(scope.CurrentUserEmpId).then(function (data) {
            scope.Cases = data.Cases;
        })
       
    }

    //var filter = function (scope, doPaging) {
    //    var hasFilter = false;

    //    if (scope.CasesFilter.CaseNumber != null && scope.CasesFilter.CaseNumber != undefined && scope.CasesFilter.CaseNumber != '') {
    //        hasFilter = true;
    //        scope.Cases = $filter('filter')(scope.Cases, { CaseNumber: scope.CasesFilter.CaseNumber });
    //    }

    //    if (scope.CasesFilter.Status != null && scope.CasesFilter.Status != undefined && scope.CasesFilter.Status != '') {
    //        hasFilter = true;
    //        scope.Cases = $filter('filter')(scope.Cases, { Status: scope.CasesFilter.Status });
    //    }

    //    if (scope.CasesFilter.StartDate != null && scope.CasesFilter.StartDate != undefined && scope.CasesFilter.StartDate != '') {
    //        hasFilter = true;
    //        scope.Cases = $filter('filter')(scope.Cases, { StartDate: scope.CasesFilter.StartDate });
    //    }

    //    if (scope.CasesFilter.RTWDate != null && scope.CasesFilter.RTWDate != undefined && scope.CasesFilter.RTWDate != '') {
    //        hasFilter = true;
    //        scope.Cases = $filter('filter')(scope.Cases, { RTWDate: scope.CasesFilter.RTWDate });
    //    }

    //    if (!hasFilter) {
    //        scope.Cases = [];
    //        list(scope, false);
    //    }
    //}

    //var loadMore = function (scope, errorCallback) {
        
    //};

    //var updateSort = function (scope, sortBy, errorCallback) {
    //    scope.CasesFilter.SortExpression = sortBy.toString();
    //    scope.CasesFilter.SortDir = (scope.CasesFilter.SortDir == 'desc' ? 'asc' : 'desc');
    //    var SortDir = (scope.CasesFilter.SortDir == 'desc' ? false : true);
    //    scope.Cases = $filter('orderBy')(scope.Cases, sortBy.toString(), SortDir);
    //};

    return {
        Init: init,
        //LoadMore: loadMore,
        //UpdateSort: updateSort,
        List: list,
        //Filter: filter,
    }
}])