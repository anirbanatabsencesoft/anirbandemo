//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('contactService', ['$resource', '$modal', '$q', function ($resource, $modal, $q) {

    // Open Contacts Modal
    var openContactsModal = function (scope, employeeId) {
        var modalInstance = $modal.open({
            templateUrl: 'contacts.html', // id of the template created in view.
            controller: 'ContactModalCtrl', // name of the controller for modal
            resolve: {
                ParentScope: function () { // this is how data is resolved in modal controller
                    return scope; // passing data to modal
                },
                EmployeeId: function () {
                    return employeeId;
                }
            }
        });

        modalInstance.result.then(function () {
        });
    };

    

    return {
        OpenContactsModal: openContactsModal
    };

  

}]);
