﻿angular
.module('App.Services')
.factory('workflowService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var expressionsResource = $resource('/Workflow/Expressions/:EventType', {}, {
        'read': { method: 'GET', isArray: true },
        'post': { method: 'POST', isArray: true }
    });

    var rulesResource = $resource('/Workflow/Rules/:EventType', {}, {
        'post': { method: 'POST', isArray: true }
    });

    var workflowResource = $resource('/Workflow/Save', {}, {
        'save': { method: 'POST', isArray: false }
    })

    var copyWorkflowResource = $resource('/Workflow/Copy', {}, {
        'copy': { method: 'POST', isArray: false }
    });

    var workflowDeleteResource = $resource('/Workflow/:WorkflowId/Delete', {}, {
        'delete': { method: 'DELETE', isArray: false }
    });

    var workflowSuppressionResource = $resource('/Workflow/ToggleWorkflowSuppression', {}, {
        'toggle': { method: 'POST', isArray: false }
    });

    var activityResource = $resource('/Workflow/Activities', {}, {
        'read': { method: 'GET', isArray: true }
    });

    var activityCategoryResource = $resource('/Workflow/Designer/Toolbar/:EventType', {}, {
        'read': {method: 'GET', isArray: true}
    });

    var workflowInstanceResource = $resource('/WorkflowInstances/List', {}, {
        'read': { method: 'POST', isArray: false }
    });

    var pauseWorkflowInstanceResource = $resource('/WorkflowInstance/Pause', {}, {
        'save': { method: 'POST', isArray: false }
    });

    var resumeWorkflowInstanceResource = $resource('/WorkflowInstance/Resume', {}, {
        'save': { method: 'POST', isArray: false }
    });

    var cancelWorkflowInstanceResource = $resource('/WorkflowInstance/Cancel', {}, {
        'save': { method: 'POST', isArray: false }
    });

    var startWorkflowInstanceResource = $resource('/WorkflowInstance/Start', {}, {
        'criteria': { method: 'GET', isArray: true },
        'start': {method:'POST', isArray:false}
    })

    var workflowListResource = $resource('/Workflow/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var caseWorkflowResource = $resource('/Workflow/ListAll/:employerId', {}, {
        'get': { method: 'GET', isArray: true }
    });

    return {
        GetRuleGroupExpressions: function (expressionParameters) {
            return expressionsResource.read(expressionParameters);
        },
        GetRules: function (expressions) {
            return rulesResource.post(expressions)
        },
        GetRuleExpressionsForRules: function (rules) {
            return expressionsResource.post(rules)
        },
        SaveWorkflow: function (workflow) {
            return workflowResource.save(workflow);
        },
        CopyWorkflow:function(workflowCopy){
            return copyWorkflowResource.copy(workflowCopy);
        },
        DeleteWorkflow: function (workflowId) {
            var template = {
                WorkflowId: workflowId
            };
            return workflowDeleteResource.delete(template);
        },
        ToggleWorkflowSuppression: function(workflowId){
            var workflow = {
                Id: workflowId
            };
            return workflowSuppressionResource.toggle(workflow);
        },
        GetActivities: function () {
            return activityResource.read();
        },
        GetActivityCategories: function(toolbarParameters){
            return activityCategoryResource.read(toolbarParameters);
        },
        ListWorkflows: function (criteria) {
            criteria.SortDirection = criteria.SortDir === "asc" ? 1 : -1;
            return workflowListResource.list(criteria);
        },
        ListAllWorkflows: function (employerId) {
            return caseWorkflowResource.get({ employerId: employerId });
        },
        ListWorkflowInstances: function (workflowCriteria) {
            workflowCriteria.SortDirection = workflowCriteria.SortDir === 'asc' ? 1 : -1;
            return workflowInstanceResource.read(workflowCriteria);
        },
        PauseWorkflowInstance: function (workflowInstance) {
            return pauseWorkflowInstanceResource.save(workflowInstance);
        },
        ResumeWorkflowInstance: function (workflowInstance) {
            return resumeWorkflowInstanceResource.save(workflowInstance);
        },
        CancelWorkflowInstance: function (workflowInstance) {
            return cancelWorkflowInstanceResource.save(workflowInstance);
        },
        GetWorkflowCriteria: function(caseWorkflowInfo){
            return startWorkflowInstanceResource.criteria(caseWorkflowInfo);
        },
        StartCaseWorkflow: function (caseWorkflowInfo) {
            return startWorkflowInstanceResource.start(caseWorkflowInfo);
        }

    }
}]);