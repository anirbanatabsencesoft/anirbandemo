﻿(function (undefined) {
    'use strict';

    angular

        .module('App.Services')

        .factory('_', Factory);

    Factory.$inject = ['$window'];
    function Factory($window) {
        if (!$window._) {
            throw "Underscore.js is not loaded";
        }
        return $window._;
    }


})();