//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('templatesService', [ '$resource', '$q', '$http', function ($resource, $q, $http) {
    var paperworkListResource = $resource('/Templates/Paperwork/List', {}, {
        'list': {method: 'POST', isArray: false}
    });

    var paperworkResource = $resource('/Templates/Paperwork/:Id', {}, {
        'read': {method: 'GET', isArray: false},
        'save': { method: 'POST', isArray: false },
        'delete': { method: 'DELETE', isArray: false }
    });

    var communicationListResource = $resource('/Templates/Communications/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var communicationResource = $resource('/Templates/Communication/:Id', {}, {
        'read': { method: 'GET', isArray: false },
        'save': { method: 'POST', isArray: false },
        'delete': { method: 'DELETE', isArray: false }
    });

    return {
        Paperwork: function(paperworkData){
            return paperworkResource.read(paperworkData);
        },

        SavePaperworkTemplate: function (paperworkTemplate) {
            //if (paperworkTemplate.ReturnDateAdjustmentDays === null) {
            //    paperworkTemplate.ReturnDateAdjustment = null;
            //}
            return paperworkResource.save(paperworkTemplate);
        },

        DeletePaperworkTemplate: function(paperworkTemplate){
            return paperworkResource.delete(paperworkTemplate);
        },

        PaperworkList: function (filterData) {
            filterData.SortDirection = filterData.SortDir == 'asc' ? 1 : -1;
            return paperworkListResource.list(filterData);
        },

        Communication: function(communicationData){
            return communicationResource.read(communicationData);
        },

        SaveCommunicationTemplate: function(communicationTemplate){
            return communicationResource.save(communicationTemplate);
        },

        DeleteCommunicationTemplate: function(communicationTemplate){
            return communicationResource.delete(communicationTemplate);
        },

        CommunicationList: function (filterData) {
            filterData.SortDirection = filterData.SortDir == 'asc' ? 1 : -1;
            return communicationListResource.list(filterData);
        }


    }
}]);