//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('policyListManager', [ '$resource', '$q', 'policyService', function ($resource, $q, policyService) {

    var init = function (scope, policyFilter) {
            scope.YesNoList = [];
            scope.YesNoList.push('Yes');
            scope.YesNoList.push('No');

            var proposedPageSize = 10;
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;

            scope.Policies = [];
            if (policyFilter != null) {
                scope.PolicyFilter = policyFilter
                scope.OnlyPageRecords = false;
            }
            else
                scope.PolicyFilter = {
                    Code: '',
                    Name: '',
                    EmployerId: scope.EmployerId,
                    PolicyType: null,
                    WorkState: '',
                    DoPaging: false,
                    PageNumber: 1,
                    PageSize: proposedPageSize,
                    TotalRecords: 0,
                    SortExpression: "Name",
                    SortDir: "asc",
                    LoadMore: true,
                    OnlyPageRecords: scope.OnlyPageRecords
                };
            //var resetPaging = function () {
            //    scope.PolicyFilter.PageNumber = 1;
            //    scope.Policies = [];
            //};
            //scope.$watch('PolicyFilter.Name', resetPaging);
            //scope.$watch('PolicyFilter.Code', resetPaging);
            //scope.$watch('PolicyFilter.PolicyType', resetPaging);
            //scope.$watch('PolicyFilter.Disabled', resetPaging);

            list(scope, false);
        };
        //end Init()
        var resetPaging = function (scope) {
            scope.PolicyFilter.PageNumber = 1;
            scope.Policies = [];
        };
        var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
        var list = function (scope, doPaging, errorCallback) {

            // Get the numeric date representation of our updated filter if a valid date.
            scope.PolicyFilter.DoPaging = doPaging;
            if (scope.PolicyFilter.SortBy == undefined ||
                scope.PolicyFilter.SortBy == null ||
                scope.PolicyFilter.SortBy == '') {
                scope.PolicyFilter.SortBy = 'Name';
            }           
        
            policyService.List(
                {
                    EmployerId: scope.PolicyFilter.EmployerId,
                    Name: scope.PolicyFilter.Name,
                    Code: scope.PolicyFilter.Code,
                    PolicyType: scope.PolicyFilter.PolicyType,
                    WorkState: scope.PolicyFilter.WorkState,
                    PageNumber: scope.PolicyFilter.PageNumber,
                    PageSize: scope.PolicyFilter.PageSize,
                    SortBy: scope.PolicyFilter.SortBy,
                    SortDirection: parseInt(scope.PolicyFilter.SortDir == "asc" ? 1 : -1),
                    OnlyPageRecords: scope.OnlyPageRecords
                })
                .$promise.then(
                function (data, status) {
                    if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                        scope.PolicyFilter.TotalRecords = data.Total;
                        scope.PolicyFilter.LoadMore = true;
                        if (doPaging) {
                            angular.forEach(data.Results, function (result) {
                                scope.Policies.push(result);
                            });
                        } else {
                            scope.Policies = data.Results;
                        }
                    } else {
                        scope.Policies = [];
                        scope.PolicyFilter.LoadMore = false;
                        scope.PolicyFilter.TotalRecords = 0;
                    }
                },
                errorCallback);        
        };

        var updateSort = function (scope, sortBy, errorCallback) {

            //reset the paging
            scope.PolicyFilter.PageNumber = 1;
            scope.Policies = [];
            scope.PolicyFilter.SortBy = sortBy.toString();

            // sort
            if (scope.PolicyFilter.SortExpression == sortBy) {
                scope.PolicyFilter.SortDir = (scope.PolicyFilter.SortDir == 'desc' ? 'asc' : 'desc');
            }
            else {
                scope.PolicyFilter.SortExpression = sortBy;
                if (sortBy == "Updated")
                    scope.PolicyFilter.SortDir = 'desc';
                else
                    scope.PolicyFilter.SortDir = 'asc';
            }

            list(scope, false, errorCallback);
        };
        var loadMore = function (scope, errorCallback) {
            var totalPages = Math.ceil(scope.PolicyFilter.TotalRecords / scope.PolicyFilter.PageSize);
            if (totalPages > 1) {
                if (scope.PolicyFilter.LoadMore && scope.PolicyFilter.PageNumber < totalPages) {
                    scope.PolicyFilter.PageNumber++;
                    list(scope, true, errorCallback);
                }
            }
        };        
    
        return {
            Init: init,
            List: list,
            UpdateSort: updateSort,
            LoadMore: loadMore,            
            ResetPaging: resetPaging
        }
    }])