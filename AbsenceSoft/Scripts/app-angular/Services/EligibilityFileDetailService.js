//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('eligibilityFileDetailService', ['$resource', '$q', function ($resource, $q) {
    var eligibilityFileDetails = function (fileId) {
        var resource = $resource('/Files/Eligibility/' + fileId, {}, {
            'fileInfo': {
                method: 'GET', isArray: false
            }
        });
        return resource.fileInfo().$promise;
    }

    var listEligibilityLinkDataFiles = function (linkedEligibilityUploadId) {
        var resource = $resource('/Files/Eligibility/ListLinkDataFiles', {}, {
            'list': {
                method: 'GET', isArray: false
            }
        });
        return resource.list({ linkedEligibilityUploadId: linkedEligibilityUploadId }).$promise;
    }

    var listEligibilityDataFileErrors = function (fileId) {
        var resource = $resource('/Files/Eligibility/' + fileId + '/Errors', {}, {
            'fileErrors': {
                method: 'GET', isArray: false
            }
        });
        return resource.fileErrors().$promise;
    }

    var getEligibilityDataFileError = function (fileId, fileErrorId) {
        var resource = $resource('/Files/Eligibility/' + fileId + '/Errors/' + fileErrorId, {}, {
            'fileError': {
                method: 'GET', isArray: false
            }
        });
        return resource.fileError().$promise;
    }

    return {
        EligibilityFileDetails: eligibilityFileDetails,
        ListEligibilityDataFileErrors: listEligibilityDataFileErrors,
        GetEligibilityDataFileError: getEligibilityDataFileError,
        GetEligibilityDataFilesForEmployers: listEligibilityLinkDataFiles
    }
}]);