//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('toDoSuccessModalService', ['$modal', '$window', 'filterFilter', function ($modal, $window, filterFilter) {

    //#region to-do logic
    var openTodoModal = function (todoItems, isCommunication, communicationAttachments, caseId, caseStatus, showToDo, communicationId) {
        var modalInstance = $modal.open({
            templateUrl: 'TodoModalSuccess.html', // id of the template created in view.
            controller: 'TodoSuccessModalCtrl', // name of the controller for modal
            backdrop  : 'static', // Prevent modal Dismiss
            keyboard  : false, // Prevent Modal Dismiss using Keyborad egs. esc.
            resolve: {
                toDoItems: function () { // this is how data is resolved in modal controller
                    return todoItems; // passing data to modal
                },
                isCommunication: function () {
                    return isCommunication;
                },
                communicationAttachments: function () {
                    return communicationAttachments;
                },
                caseId: function () {
                    return caseId;
                },
                caseStatus: function () {
                    return caseStatus;
                },
                showToDo: function () {
                    return showToDo;
                },
                communicationId: function () {
                    return communicationId;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            //$scope.SelectedItemFromModel = selectedItem; // called when ok is pressed. passing data from model to page
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date()); // called when popup is dismissed.
        });
    };

    // this is the controller for data within modal - used in the template
    

    //#endregion to-do logic


    return {
        OpenTodoSuccessModal: openTodoModal
    };
}]);