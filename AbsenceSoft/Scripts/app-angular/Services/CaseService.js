﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('caseService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var caseResource = $resource('/cases/list', {}, {
        'list': { method: 'POST', isArray: false }
    });



    return {

        GetExportLink: function () {

        },
        List: function (filterData) {
            return caseResource.list(filterData);
        },
        CalculateEligibility: function (employeeId, data, successCallback, errorCallback) {
            $http({
                method: 'POST',
                url: '/Employees/' + employeeId + '/Cases/CalculateEligibility',
                data: data
            }).then(successCallback, errorCallback);
        },
        CalculateRelapseEligibility: function (employeeId, data, successCallback, errorCallback) {
            $http({
                method: 'POST',
                url: '/Employees/' + employeeId + '/Cases/CalculateRelapseEligibility',
                data: data
            }).then(successCallback, errorCallback);
        },
        GetPolicyData: function (caseId) {

            var resource = $resource('/Cases/' + caseId + '/PolicyData', {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },
        GetDenialReasons: function (reasonTarget, caseId) {
            var resource = $resource('/Cases/' + reasonTarget + '/DenialReasons', {}, {
                'list': {
                    method: 'GET', isArray: true
                }
            });

            return resource.list({ caseId: caseId }).$promise;
        },
        SavePolicyDataEdits: function (caseId, postData) {
            var resource = $resource('/Cases/' + caseId + '/Edit', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(postData).$promise;
        },
        AddManualPolicy: function (caseId, policyCode) {
            var resource = $resource('/Cases/' + caseId + '/AddManualPolicy', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save({ policyCode: policyCode }).$promise;
        },

        DeletePolicy: function (caseId, postData) {
            var resource = $resource('/Cases/' + caseId + '/DeletePolicy', postData, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save().$promise;
        },
        GetDefaultRangeForCalendarForCase: function () {
            var now = new Date();
            // default to the first day of 3 months ago
            // make the current month in the middle position, instead of last position
            var rangeStart = now.firstOfMonth().addMonths(-1).toFakeUTCDay();
            // end should be 3 months forward (exclusive)
            var rangeEnd = rangeStart.addMonths(3);         //.toFakeUTCDay();

            return {
                Start: rangeStart,
                End: rangeEnd
            };
        },
        GetDefaultRangeForCalendarForEmployee: function () {
            var now = new Date();       //.toFakeUTCDay();
            // default to the first day of 3 months ago
            var rangeStart = now.firstOfMonth().addMonths(-11).toFakeUTCDay();
            // end should be 3 months forward (exclusive)
            var rangeEnd = rangeStart.addMonths(12);        //.toFakeUTCDay();
            return {
                Start: rangeStart,
                End: rangeEnd
            };
        },
        GetMonthsForCalendar: function (dateRange) {
            var arrayOfMonths = [];
            var count = 0;
            while (dateRange.Start.addMonths(count) < dateRange.End) {
                arrayOfMonths.push(dateRange.Start.addMonths(count));
                count++;
            }
            return arrayOfMonths;
        },
        GetCalendarForCase: function (caseId, dateRange) {
            // normalize the date to start of the month and UTC
            var rangeStart = dateRange.Start;
            // normalize the date to start of the month and UTC
            var rangeEnd = dateRange.End;

            var postData =
            {
                CaseId: caseId,
                Start: rangeStart,
                End: rangeEnd
            }
            var resource = $resource('/Cases/' + caseId + '/Calendar', {}, {
                'list': {
                    method: 'POST', isArray: false
                }
            });
            return resource.list(postData).$promise.then(function (result) {
                $.each(result.Dates, function (index, element) {
                    var newDate = new Date(element.Date);
                    element.Date = newDate.toFakeUTCDay();
                });
                return result;
            });
        },
        GetCalendarForEmployee: function (employeeId, dateRange) {
            // normalize the date to start of the month and UTC
            var rangeStart = dateRange.Start;                //.firstOfMonth().toFakeUTCDay();
            // normalize the date to start of the month and UTC
            var rangeEnd = dateRange.End;                    // .firstOfMonth().toFakeUTCDay();

            var postData =
            {
                EmployeeId: employeeId,
                Start: rangeStart,
                End: rangeEnd
            }
            var resource = $resource('/Employees/' + employeeId + '/Calendar', {}, {
                'list': {
                    method: 'POST', isArray: false
                }
            });
            return resource.list(postData).$promise.then(function (result) {
                $.each(result.Dates, function (index, element) {
                    var newDate = new Date(element.Date);
                    element.Date = newDate.toFakeUTCDay();
                });
                return result;
            });
        },
        CancelCase: function (caseId, reason) {

            var resource = $resource('/Cases/' + caseId + '/Edit', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(postData).$promise;

            //var resource = $resource('/Cases/' + caseId + '/Cancel', { reason: reason }, {
            //    'save': { method: 'POST', isArray: false }
            //});
            //return resource.list;
        },
        GetCaseInfo: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/LeaveOfAbsence', {}, {
                'read': {
                    method: 'GET', isArray: false
                }
            });

            return resource.read().$promise;
        },
        GetCasePayInfo: function (caseId){
            var resource = $resource('/Cases/' + caseId + '/Pay', {}, {
                'read': {
                    method: 'GET', isArray: false
                }
            });

            return resource.read().$promise;
        },
        GetNextPrevRangeForCalendarForCase: function (date, nxtOrPrev) {
            var dt = new Date(date);
            var rangeStart = null;

            if (nxtOrPrev === "prev") {
                rangeStart = dt.firstOfMonth().addMonths(-1);
            }
            else {
                //var rangeStart = new Date(dt.getFullYear(), dt.getMonth(), 1);
                rangeStart = dt.firstOfMonth().addMonths(1);
            }

            // end should be 3 months forward (exclusive)
            var rangeEnd = rangeStart.addMonths(3);

            return {
                Start: rangeStart,
                End: rangeEnd
            };
        },
        GetTimeTrackingInfo: function (caseId, asOfDate, options) {
            var resource = $resource('/Cases/' + caseId + '/TimeTrackingInfo', options || {}, {
                'list': {
                    method: 'GET', isArray: true
                }
            });

            return resource.list({ CaseId: caseId, AsOfDate: asOfDate }).$promise;
        },
        CreateOrModifyCertificationDetail: function (caseId, certification) {

            var resource = $resource('/Cases/' + caseId + '/CreateOrModifyCertification', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(certification).$promise;
        },
        DeleteCertificationDetail: function (caseId, certificationId) {
            var resource = $resource('/Cases/' + caseId + '/DeleteCertification/' + certificationId, {}, {
                'delete': {
                    method: 'DELETE', isArray: false
                }
            });

            return resource.delete().$promise;
        },
        DeleteWorkRestriction: function (caseId, workRestrictionId) {
            var resource = $resource('/Demand/' + caseId + '/DeleteWorkRestriction/' + workRestrictionId, {}, {
                'delete': {
                    method: 'DELETE', isArray: false
                }
            });

            return resource.delete().$promise;
        },
        GetCertifications: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/GetCertifications', {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },
        CreateOrModifyAccommodationRequest: function (caseId, accommodation) {
            var resource = $resource('/Accommodations/' + caseId + '/CreateOrModifyAccommodationRequest', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(accommodation).$promise;
        },
        GetAccommodationRequests: function (caseId) {
            var resource = $resource('/Accommodations/' + caseId + '/GetCaseAccommodations', {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },
        EmployeeContactTypes: function (absenceReasonCode) {
            var resource = $resource('/Case/EmployeeContactTypes', {}, {
                'list': { method: 'GET', params: { absenceReasonCode: absenceReasonCode }, isArray: true }
            });
            return resource.list();
        },
        ApplyChangePolicyStatus: function (caseId, postData) {
            var resource = $resource('/Cases/' + caseId + '/Adjudicate', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        SetPolicyEndDate: function (caseId, policyCode, endDate) {
            var resource = $resource('/Cases/' + caseId + '/SetPolicyEndDate', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save({ PolicyCode: policyCode, EndDate: endDate, CaseId: caseId }).$promise;
        },
        ReassignCases: function (postData) {
            var resource = $resource('/Cases/Reassign', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        ReassignCaseAssignee: function (postData) {
            var resource = $resource('/Cases/ReassignCaseAssignee', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        ApplyChangeAccommodationStatus: function (caseId, postData) {
            var resource = $resource('/Accommodations/ApplyDetermination', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        PreValidateTimeOffRequest: function (IntermittentRequestViewModel) {
            var resource = $resource('/Cases/PreValidateTimeOffRequest', {}, {
                'PreValidateTimeOffRequest': {
                    method: 'POST', isArray: false
                }
            });
            return resource.PreValidateTimeOffRequest(IntermittentRequestViewModel).$promise;
        },
        GetSTDInfo: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/STDInfo', {}, {
                'stdinfo': {
                    method: 'GET', isArray: false
                }
            });

            return resource.stdinfo().$promise;
        },
        GuidelinesData: function (diagnosisCode,employerId) {
            var resource = $resource('/Cases/' + diagnosisCode + '/GuidelinesData?employerId=' + employerId, {}, {
                'guidelinesdata': {
                    method: 'POST', isArray: false
                }
            });

            return resource.guidelinesdata().$promise;
        },
        STDPolicyData: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/STDPolicyData', {}, {
                'STDPolicyData': {
                    method: 'GET', isArray: false
                }
            });

            return resource.STDPolicyData().$promise;
        },
        UpsertWorkRestrictions: function (postData) {
            var resource = $resource('/Cases/UpsertWorkRestrictions', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        UpdatePayPeriod: function (model) {
            var resource = $resource('/Cases/Pay/UpdatePayPeriodStatus', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        UpdateBasePay: function(model) {
            var resource = $resource('/Cases/Pay/UpdateBasePay', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        UpdatePolicyBenefitPercentage: function (model) {
            var resource = $resource('/Cases/Pay/ChangeBenefitPercentage', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        ChangeDetailIsOffset: function (model) {
            var resource = $resource('/Cases/Pay/ChangeDetailIsOffset', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        ChangeApplyOffsetsByDefault: function (model) {
            var resource = $resource('/Cases/Pay/ChangeApplyOffsetsByDefault', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        UpdatePaySchedule: function (model) {
            var resource = $resource('/Cases/Pay/UpdatePaySchedule', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        ChangeDetailBenefitAmount: function(model){
            var resource = $resource('/Cases/Pay/ChangeDetailBenefitAmount', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        ChangeDetailBenefitPercentage: function (model) {
            var resource = $resource('/Cases/Pay/ChangeDetailBenefitPercentage', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        ChangePayPeriod: function (model) {
            var resource = $resource('/Cases/Pay/UpdatePayPeriod', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        WaiveWaitingPeriod: function(model){
            var resource = $resource('/Cases/Pay/WaiveWaitingPeriod', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(model).$promise;
        },
        GetCaseHistory: function (employeeId, caseId) {
            var resource = $resource('/Case/Employee/' + employeeId + '/CaseHistory', {}, {
                'get': {
                    method: 'GET', isArray: false
                }
            });

            return resource.get({ caseId: caseId }).$promise;
        },
        BulkDownloadCases: function (model) {
            var resource = $resource('/BulkDownload/BulkDownloadCases', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(model).$promise;
        },
        GetCaseBulkDownloadStatus: function (employeeId) {
            var resource = $resource('/BulkDownload/' + employeeId + '/StatusRequest', {}, {
                'get': {
                    method: 'GET', isArray: true
                }
            });

            return resource.get().$promise;
        },
        HasFeatureAndPermissions: function () {
            var resource = $resource('/BulkDownload/HasFeatureAndPermissions', {}, {
                'get': {
                    method: 'GET', isArray: false
                }
            });

            return resource.get().$promise;
        },
        CancelAccommodationRequest: function (caseId, accomReqId, cancel, otherDesc) {
            var resource = $resource('/Accommodation/' + caseId + '/Request/' + accomReqId + '/Cancel', {}, {
                'get': {
                    method: 'GET', isArray: false
                }
            });

            return resource.get({ cancelReason: cancel, otherDesc: otherDesc }).$promise;
        },
        CreateEssCase: function (employeeId, caseModel) {
            var resource = $resource('/ESS/' + employeeId + '/Cases/Create', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(caseModel).$promise;
        },
        GetEmployeeCases: function (employeeId) {
            var resource = $resource('/Employee/' + employeeId + '/GetCases', {}, {
                'get': {
                    method: 'GET', isArray: false
                }
            });

            return resource.get().$promise;
        },
        GetESSCaseForReview: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/Review', {}, {
                'get': {
                    method: 'GET', isArray: false
                }
            });

            return resource.get().$promise;
        },
        CaseReviewed: function (reviewedCase) {
            var resource = $resource('/Case/Reviewed', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(reviewedCase).$promise;
        },
        GetRoles: function (toDoName) {
            var resource = $resource('/cases/GetRoles', {}, {
                'get': {
                    method: 'GET', isArray: true
                }
            });

            return resource.get({ type: toDoName }).$promise;
        },

        ToDoUserAssignmentSuggestionList: function (filterData) {
            var resource = $resource('/cases/ToDoUserAssignmentSuggestionList', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(filterData).$promise;
        },

        CreateManualTodo: function (createManualCaseTodo) {
            var resource = $resource('/cases/CreateManualTodo', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(createManualCaseTodo).$promise;
        },

        GetEECustomFields: function (employerId) {
            var resource = $resource('/Administration/' + employerId + '/GetEECustomFields', {}, {
                'list': { method: 'GET', isArray: true }
            });
            return resource.list({ page: 2 }).$promise;
        },

        UpdateCustomFields: function (caseId, customFields) {
            var resource = $resource('/Cases/' + caseId + '/UpdateCustomFields', {}, {
                'save': { method: 'POST', isArray: false }
            });
            return resource.save(customFields).$promise;
        },

        GetEmployeeFeatureInfo: function (emplyeeId) {
            var resource = $resource('/Employees/' + emplyeeId + '/GetEmployeeFeatureInfo', {}, {
                'list': { method: 'GET', isArray: false }
            });
            return resource.list().$promise;
        },

        UpdateAdditionalInfo: function (caseId, additionalInfo) {
            var resource = $resource('/Cases/' + caseId + '/UpdateAdditionalInfo', {}, {
                'save': { method: 'POST', isArray: false }
            });
            return resource.save(additionalInfo).$promise;
        },
        UpdateCaseInfo: function (caseId, caseInfo) {
            var resource = $resource('/Cases/' + caseId + '/UpdateCaseInfo', {}, {
                'save': { method: 'POST', isArray: false }
            });
            return resource.save(caseInfo).$promise;
        },
        UpdateCaseReporterInfo: function (caseId, caseReporterInfo) {
            var resource = $resource('/Cases/' + caseId + '/UpdateCaseReporterInfo', {}, {
                'save': { method: 'POST', isArray: false }
            });
            return resource.save(caseReporterInfo).$promise;
        },
        UpdateAdditionalCaseEvents: function (additionalCaseEvents) {
            var resource = $resource('/Cases/' + additionalCaseEvents.CaseId + '/UpdateAdditionalCaseEvents', {}, {
                'save': { method: 'POST', isArray: false }
            });
            return resource.save(additionalCaseEvents);
        },

        SaveCurrentProcessPath: function (postData) {
            var resource = $resource('/Cases/SaveCurrentProcessPath', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        SaveProcessPath: function (postData) {
            var resource = $resource('/Cases/SaveProcessPath', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        },
        LastViewedList: function (criteria) {
            var resource = $resource('/Cases/LastViewedList', {}, {
                'read': {
                    method: 'POST', isArray: false
                }
            });
            return resource.read(criteria).$promise;
        },
        CaseJobs: function (employeeId) {
            var resource = $resource('/Cases/' + employeeId + '/Jobs', {}, {
                'read': {
                    method: 'GET', isArray: true
                }
            });
            return resource.read().$promise;
        },
        GetCalculatedDaysOnJobTransferOrRestriction: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/GetCalculatedDaysOnJobTransferOrRestriction', {}, {
                'read': {
                    method: 'GET', isArray: false
                }
            });
            return resource.read().$promise;
        },
        GetCalculatedDaysAwayFromWork: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/GetDaysAwayFromWork', {}, {
                'read': {
                    method: 'GET', isArray: false
                }
            });
            return resource.read().$promise;
        },
        GetReducedVariableSchedule: function (caseId) {
            var resource = $resource('/Cases/' + caseId + '/ReducedVariableSchedule', {}, {
                'read': {
                    method: 'GET', isArray: true
                }
            });
            return resource.read().$promise;
        }
    };
}])