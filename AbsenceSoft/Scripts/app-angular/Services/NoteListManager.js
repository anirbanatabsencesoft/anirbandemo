//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('noteListManager', ['$resource', '$q', 'noteService', 'lookupService', '$filter', function ($resource, $q, noteService, lookupService, $filter) {

    var init = function (scope, infiniteScrollOnBody, ParentType, ParentTypeInstanceId) {
        scope.noteCategories = lookupService.GetNoteCategories().then(function (response) {
            scope.noteCategories = response;
        });

        var proposedPageSize = 5;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height() / 2;
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 99;
        }
     
        scope.Notes = [];
        scope.NotesFilter = {
            Id: ParentTypeInstanceId,
            NoteDate: '',
            NoteDateAsDate: '',
            Category: -1,
            Notes: '',
            CreatedBy: '',
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "CreatedDate",
            SortDir: "desc",
            LoadMore: true
        };

        

        var resetPaging = function () {
            scope.NotesFilter.PageNumber = 1;
            scope.Notes = [];
        };
        scope.$watch('NotesFilter.Date', resetPaging);
        scope.$watch('NotesFilter.Category', resetPaging);
        scope.$watch('NotesFilter.Notes', resetPaging);
        scope.$watch('NotesFilter.CreatedBy', resetPaging);
        list(scope, false);
    };
    var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
    var list = function (scope, doPaging, errorCallback) {       
        // convert the date        
        if (dateInputRegex.test(scope.NotesFilter.NoteDate)) {
            try {
                scope.NotesFilter.NoteDateAsDate = new Date(scope.NotesFilter.NoteDate).valueOf();
                isValidUpdteField = true;
            }
            catch (e) {

            }
        }

        scope.NotesFilter.NoteDateAsDate = null;
        var isValidDateField = false;
        if (scope.NotesFilter.NoteDate != null && scope.NotesFilter.NoteDate != '' && scope.NotesFilter.NoteDate != undefined) {
            var dueDate = new Date(scope.NotesFilter.NoteDate);
            var strDueDate = (dueDate.getMonth() + 1) + "/" + dueDate.getDate() + "/" + dueDate.getFullYear();
            if (dateInputRegex.test(strDueDate)) {
                try {
                    scope.NotesFilter.NoteDateAsDate = new Date(scope.NotesFilter.NoteDate).valueOf();
                    isValidDateField = true;
                }
                catch (e) {
                }
            }
        }

        if (scope.NotesFilter.NoteDate != null && scope.NotesFilter.NoteDate != '' && scope.NotesFilter.NoteDate != undefined && isValidDateField == false) {
            scope.Notes = [];
            scope.NotesFilter.LoadMore = false;
            scope.NotesFilter.TotalRecords = 0;
            return;
        }

        scope.NotesFilter.DoPaging = doPaging;

        if (scope.ParentType == "employee") {
            noteService.EmployeeNotesList(
            {
                Id: scope.NotesFilter.Id,
                NoteDate: scope.NotesFilter.NoteDateAsDate,
                Category: scope.NotesFilter.Category == -1 ? null : scope.NotesFilter.Category,
                Notes: scope.NotesFilter.Notes,
                CreatedBy: scope.NotesFilter.CreatedBy,
                PageNumber: scope.NotesFilter.PageNumber,
                PageSize: scope.NotesFilter.PageSize,
                SortBy: scope.NotesFilter.SortExpression,
                SortDirection: parseInt(scope.NotesFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.NotesFilter.TotalRecords = data.Total;
                    scope.NotesFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Notes.push(result);
                        });
                    } else {
                        scope.Notes = data.Results;
                    }
                } else {
                    scope.Notes = [];
                    scope.NotesFilter.LoadMore = false;
                    scope.NotesFilter.TotalRecords = 0;
                }
            },
            errorCallback);
        }
        else {
            noteService.CaseNotesList(
            {
                Id: scope.NotesFilter.Id,
                NoteDate: scope.NotesFilter.NoteDateAsDate,
                Category: scope.NotesFilter.Category == -1 ? null : scope.NotesFilter.Category,
                Notes: scope.NotesFilter.Notes,
                CreatedBy: scope.NotesFilter.CreatedBy,
                PageNumber: scope.NotesFilter.PageNumber,
                PageSize: scope.NotesFilter.PageSize,
                SortBy: scope.NotesFilter.SortExpression,
                SortDirection: parseInt(scope.NotesFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.NotesFilter.TotalRecords = data.Total;
                    scope.NotesFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Notes.push(result);
                        });
                    } else {
                        scope.Notes = data.Results;
                    }
                } else {
                    scope.Notes = [];
                    scope.NotesFilter.LoadMore = false;
                    scope.NotesFilter.TotalRecords = 0;
                }
            },
            errorCallback);
        }
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        //reset the paging
        scope.NotesFilter.PageNumber = 1;
        scope.Notes = [];

        // sort
        if (scope.NotesFilter.SortExpression == sortBy)
            scope.NotesFilter.SortDir = (scope.NotesFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.NotesFilter.SortExpression = sortBy;
            scope.NotesFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.NotesFilter.TotalRecords / scope.NotesFilter.PageSize);
        if (totalPages > 1) {
            if (scope.NotesFilter.LoadMore && scope.NotesFilter.PageNumber < totalPages) {
                scope.NotesFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore
    }
}]);