//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('caseListManager', ['$resource', '$q', 'caseService', function ($resource, $q, caseService) {

    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Cases = [];
        scope.CasesFilter = {
            Employer: '',
            EmployeeName: '',
            Reason: '',
            Updated: '',
            UpdatedAsDate: null,
            CaseNumber: '',
            CaseType: -2,
            Status: -2,
            AssignedToName: '',
            ListType: scope.ListType,
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "EmployeeName",
            SortDir: "asc",
            LoadMore: true
        };
        var resetPaging = function () {
            scope.CasesFilter.PageNumber = 1;
            scope.Cases = [];
        };
        scope.$watch('CasesFilter.EmployeeName', resetPaging);
        scope.$watch('CasesFilter.Employer', resetPaging);
        scope.$watch('CasesFilter.Reason', resetPaging);
        scope.$watch('CasesFilter.Updated', resetPaging);
        scope.$watch('CasesFilter.CaseNumber', resetPaging);
        scope.$watch('CasesFilter.Type', resetPaging);
        scope.$watch('CasesFilter.Status', resetPaging);
        scope.$watch('CasesFilter.AssignedToName', resetPaging);

        list(scope, false);
    };
    var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
    var list = function (scope, doPaging, errorCallback) {

        // convert the date
        scope.CasesFilter.UpdatedAsDate = null;
        var isValidUpdteField = false;
        if (scope.CasesFilter.Updated != null && scope.CasesFilter.Updated != '' && scope.CasesFilter.Updated != undefined) {
            var updatedDate = new Date(scope.CasesFilter.Updated);
            var strUpdatedDate = updatedDate.getMonth() + "/" + updatedDate.getDate() + "/" + updatedDate.getFullYear();
            if (dateInputRegex.test(strUpdatedDate)) {
                try {
                    scope.CasesFilter.UpdatedAsDate = new Date(scope.CasesFilter.Updated).valueOf();
                    isValidUpdteField = true;
                }
                catch (e) {

                }
            }
        }

        if (scope.CasesFilter.Updated != null && scope.CasesFilter.Updated != '' && scope.CasesFilter.Updated != undefined && isValidUpdteField == false) {
            scope.Cases = [];
            scope.CasesFilter.LoadMore = false;
            scope.CasesFilter.TotalRecords = 0;
            return;
        }


        // Get the numeric date representation of our updated filter if a valid date.
        scope.CasesFilter.DoPaging = doPaging;
        caseService.List(
            {
                EmployerId: scope.CasesFilter.Employer,
                EmployeeName: scope.CasesFilter.EmployeeName,
                Reason: scope.CasesFilter.Reason,
                Updated: scope.CasesFilter.UpdatedAsDate,
                CaseNumber: scope.CasesFilter.CaseNumber,
                Type: scope.CasesFilter.Type == -1 ? null : parseInt(scope.CasesFilter.Type),
                Status: scope.CasesFilter.Status == -1 ? null : parseInt(scope.CasesFilter.Status),
                AssignedToName: scope.CasesFilter.AssignedToName,
                ListType: scope.CasesFilter.ListType,
                PageNumber: scope.CasesFilter.PageNumber,
                PageSize: scope.CasesFilter.PageSize,
                SortBy: scope.CasesFilter.SortBy,
                SortDirection: parseInt(scope.CasesFilter.SortDir == "asc" ? 1 : -1)
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.CasesFilter.TotalRecords = data.Total;
                    scope.CasesFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Cases.push(result);
                            InitReassignCasesViewModel(scope);
                        });
                    } else {
                        scope.Cases = data.Results;
                        InitReassignCasesViewModel(scope);
                    }
                } else {
                    scope.Cases = [];
                    scope.CasesFilter.LoadMore = false;
                    scope.CasesFilter.TotalRecords = 0;
                }
            },
            errorCallback);        
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        //reset the paging
        scope.CasesFilter.PageNumber = 1;
        scope.Cases = [];
        scope.CasesFilter.SortBy = sortBy.toString();

        // sort
        if (scope.CasesFilter.SortExpression == sortBy) {
            scope.CasesFilter.SortDir = (scope.CasesFilter.SortDir == 'desc' ? 'asc' : 'desc');
        }
        else {
            scope.CasesFilter.SortExpression = sortBy;
            if (sortBy == "Updated")
                scope.CasesFilter.SortDir = 'desc';
            else
                scope.CasesFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.CasesFilter.TotalRecords / scope.CasesFilter.PageSize);
        if (totalPages > 1) {
            if (scope.CasesFilter.LoadMore && scope.CasesFilter.PageNumber < totalPages) {
                scope.CasesFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    var InitReassignCasesViewModel = function (scope) {
        scope.reassigncases = { UserId: '', Cases: [] };
        angular.forEach(scope.Cases, function (theCase, index) {
            var caseDetail = new Object();
            caseDetail.CaseId = theCase.Id;
            caseDetail.ApplyStatus = false;
            scope.reassigncases.Cases.push(caseDetail);
        });        
    };
    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore
    }
}])