//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('administrationService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var employerListResource = $resource('/administration/GetEmployersList', {}, {
        'list': { method: 'POST', isArray: false, cache: false }
    });

    var userListResource = $resource('/administration/GetUsers', {}, {
        'list': { method: 'POST', isArray: false, cache: false }
    });

    return {

        //#region "Administration Index"
        GetPermissions : function(){
            var resource = $resource('/Administration/GetPermissions', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get().$promise;
        },

        DeleteAccount: function (employerId) {
            var resource = $resource('/Administration/' + employerId + '/DeleteAccount', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save().$promise;
        },
        //#endregion "Administration Index"

        //#region "EditUserInfo"

        UpdateUserInfo: function (userInfo) {
            var resource = $resource('/Administration/UpdateUserInfo', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(userInfo).$promise;
        },

        UpdatePassword: function (ChangePasswordModel) {
            var resource = $resource('/Administration/UpdatePassword', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(ChangePasswordModel).$promise;
        },
        //#endregion "EditUserInfo"

        //#region "EditCompanyInfo"
        UpdateCompanyInfo: function (companyInfo) {
                var resource = $resource('/Administration/EditCompanyInfo', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(companyInfo).$promise;
        },
        //#endregion

        //#region "EditSecuritySettings"
        UpdateSecuritySettings: function (securitySettings) {
            var resource = $resource('/Administration/EditSecuritySettings', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(securitySettings).$promise;
        },
        //#endregion

        //#region "EditEmployerInfo"
        UpdateEmployer: function (employerData) {
            var resource = $resource('/Administration/UpdateEmployer', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(employerData).$promise;
        },
        //#endregion "EditEmployerInfo"


        //#region "AddUserInfo"
        AddUser: function (newUser) {
            var resource = $resource('/Administration/AddUserInfo', {}, {
                'AddUserInfo': {
                    method: 'POST', isArray: false
                }
            });
            return resource.AddUserInfo(newUser).$promise;
        },
        //#endregion "AddUserInfo"


        //#region CompanyInfo Popup
        GetCompanyInfo: function () {
            var resource = $resource('/Administration/GetCompanyInfo', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get().$promise;
        },

        GetSecuritySettings: function () {
            var resource = $resource('/Administration/GetSecuritySettings', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get().$promise;
        },

        GetBasicInfo: function () {
            var resource = $resource('/Administration/GetBasicInfo', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get().$promise;
        },

        GetEmployerInfo: function (employerId) {
            var resource = $resource('/Administration/GetEmployerInfo', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get({ employerId: employerId }).$promise;
        },

        GetUsersList: function () {
            var resource = $resource('/Administration/GetUsersList', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });

            return resource.get().$promise;
        },

        //#region "AddUserInfo"
        AddRole: function (newRole) {
            var resource = $resource('/Administration/AddRole', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(newRole).$promise;
        },

        EditRole: function (newRole) {
            var resource = $resource('/Administration/EditRole', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(newRole).$promise;
        },
        DeleteRole: function (roleId){
            return $http.delete('/Administration/DeleteRole/' + roleId);
        },

        GetRole: function (roleId) {
            var resource = $resource('/Administration/GetRole', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get({ roleId: roleId }).$promise;
        },

        GetRoleAndActivePermissions: function(roleId){
            var resource = $resource('/Administration/GetRoleAndActivePermissions/:roleId', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });
            return resource.get({ roleId: roleId }).$promise;
        },

        GetRoles: function () {
            var resource = $resource('/Administration/GetRoles', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });

            return resource.get().$promise;
        },
        //#endregion CompanyInfo Popup

        //#region "Delete User"
        DisableUser: function (userId) {
            var resource = $resource('/User/' + userId  + '/DisableUser', {}, {
                'DisableUser': {
                    method: 'POST', isArray: false
                }
            });
            return resource.DisableUser().$promise;
        },
        //#endregion "Delete User"


        UnLockUser: function (userId) {
            var resource = $resource('/User/' + userId + '/UnLockUser', {}, {
                'UnLockUser': {
                    method: 'POST', isArray: false
                }
            });
            return resource.UnLockUser().$promise;
        },
        ResetUserPassword: function (userId) {
            var resource = $resource('/User/' + userId + '/ResetUserPassword', {}, {
                'UnLockUser': {
                    method: 'POST', isArray: false
                }
            });
            return resource.UnLockUser().$promise;
        },
        EnableUser: function (data) {            
            var resource = $resource('/User/EnableUser', {}, {
                'EnableUser': {
                    method: 'POST', isArray: false
                }
            });
            return resource.EnableUser(data).$promise;
        },

        DoExport: function (employerId) {
            var resource = $resource("/ExportData/" + employerId + "/DoExport", {}, {
                DoExport: {
                    method: 'POST',
                }
            })

            return resource.DoExport();
        },

        EmployerList: function (filterData) {
            return employerListResource.list(filterData);
        },

        UserList: function (filterData) {
            return userListResource.list(filterData);
        },

        UpdateEmployerAccess: function (employerAccessList) {
            var resource = $resource('/Administration/UpdateEmployerAccess', {}, {
                'update': {
                    method: 'POST', isArray: false
                }
            });
            return resource.update(employerAccessList).$promise;
        },

        GetNotificationConfigurationsForUser: function (userId) {
            var resource = $resource('/Administration/' + userId + '/GetNotificationConfigurationsForUser', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });

            return resource.get().$promise;
        },

        SaveNotificationConfigurationsForUser: function (userId, userNotifications) {
            var resource = $resource('/Administration/' + userId + '/SaveNotificationConfigurationsForUser', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(userNotifications).$promise;
        },

        GetAssignedEmployeeForUser: function (userId) {
            var resource = $resource('/Administration/' + userId + '/GetAssignedEmployeeForUser', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });

            return resource.get().$promise;
        },
        GetCurrentOrganizationTreeForUser: function (userId) {
            var resource = $resource('/Organization/' + userId + '/GetCurrentOrganizationTreeForUser', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });

            return resource.get().$promise;
        },
        GetOrganizationTreeForUser: function (userId, orgType, searchTerm) {
            var resource = $resource('/Organization/' + userId + '/GetOrganizationTreeForUser', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });

            return resource.get({ organizationType: orgType, searchTerm: searchTerm }).$promise;
        },
        SaveOrganizationVisibilityForUser: function (userId, model) {
            var resource = $resource('/Organization/' + userId + '/SaveOrganizationVisibilityForUser', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(model).$promise;
        }
    }
}]);