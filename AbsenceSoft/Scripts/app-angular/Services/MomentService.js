﻿
(function () {
    'use strict';

    angular
      .module('App.Services')
      .factory('moment', MomentService);

    MomentService.$inject = ['$window'];
    function MomentService($window) {
        return $window.moment;
    }
})();
