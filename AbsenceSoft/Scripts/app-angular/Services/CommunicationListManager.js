//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('communicationListManager', ['$resource', '$q', 'communicationService', 'lookupService', function ($resource, $q, communicationService, lookupService) {

    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 5;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height() / 2;
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.Communications = [];
        scope.CommunicationsFilter = {
            CaseId: scope.CaseId,
            ModifiedDate: '',
            SentDate: '',
            CommunicationName: '',
            CommunicationSubject: '',
            Recipients: '',
            CreatedBy: '',
            IsDraft: false,
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "SentDate",
            SortDir: "desc",
            LoadMore: true,
            CommunicationType: ""

        };




        var resetPaging = function () {
            scope.CommunicationsFilter.PageNumber = 1;
            scope.Communications = [];
        };
        scope.$watch('CommunicationsFilter.SentDate', resetPaging);
        scope.$watch('CommunicationsFilter.CommunicationName', resetPaging);
        scope.$watch('CommunicationsFilter.CommunicationSubject', resetPaging);
        scope.$watch('CommunicationsFilter.Recipients', resetPaging);
        scope.$watch('CommunicationsFilter.CreatedBy', resetPaging);
        scope.$watch('CommunicationsFilter.CommunicationType', resetPaging);

        list(scope, false);
    };
    var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;

    var convertTheDate = function (dateField) {
        if (!dateField)
            return undefined;

        return new Date(dateField).valueOf();
    }

    var list = function (scope, doPaging, errorCallback) {
        scope.CommunicationsFilter.DoPaging = doPaging;

        communicationService.List(
           {
               CaseId: scope.CommunicationsFilter.CaseId,
               SentDate: convertTheDate(scope.CommunicationsFilter.SentDate),
               ModifiedDate: convertTheDate(scope.CommunicationsFilter.ModifiedDate),
               CommunicationName: scope.CommunicationsFilter.CommunicationName,
               CommunicationSubject: scope.CommunicationsFilter.CommunicationSubject,
               Recipients: scope.CommunicationsFilter.Recipients,
               CreatedBy: scope.CommunicationsFilter.CreatedBy,
               CommunicationType: scope.CommunicationsFilter.CommunicationType,
               PageNumber: scope.CommunicationsFilter.PageNumber,
               PageSize: scope.CommunicationsFilter.PageSize,
               SortBy: scope.CommunicationsFilter.SortExpression,
               IsDraft: scope.CommunicationsFilter.IsDraft,
               SortDirection: parseInt(scope.CommunicationsFilter.SortDir == "asc" ? 1 : -1)
           })
           .$promise.then(
           function (data, status) {
               if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                   scope.CommunicationsFilter.TotalRecords = data.Total;
                   scope.CommunicationsFilter.LoadMore = true;
                   if (doPaging) {
                       angular.forEach(data.Results, function (result) {
                           scope.Communications.push(result);
                       });
                   } else {
                       scope.Communications = data.Results;
                   }
               } else {
                   scope.Communications = [];
                   scope.CommunicationsFilter.LoadMore = false;
                   scope.CommunicationsFilter.TotalRecords = 0;
               }
           },
           errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        //reset the paging
        scope.CommunicationsFilter.PageNumber = 1;
        scope.Communications = [];

        // sort
        if (scope.CommunicationsFilter.SortExpression == sortBy)
            scope.CommunicationsFilter.SortDir = (scope.CommunicationsFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.CommunicationsFilter.SortExpression = sortBy;
            scope.CommunicationsFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.CommunicationsFilter.TotalRecords / scope.CommunicationsFilter.PageSize);
        if (totalPages > 1) {
            if (scope.CommunicationsFilter.LoadMore && scope.CommunicationsFilter.PageNumber < totalPages) {
                scope.CommunicationsFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };
    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore
    }
}]);