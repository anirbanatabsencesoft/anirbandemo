//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('documentService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var documentPolicyResource = $resource('/Documents/:employerId/GetDocumentUploadPolicy?fileName=:fileName', {}, {
        'read': { method: 'GET', isArray: false }
    });

    var documentResource = $resource('/Documents/Save', {}, {
        'save': { method: 'POST', isArray: false }
    });

    return {
        UploadPolicy: function (documentData) {
            return documentPolicyResource.read(documentData);
        },

        Save: function (documentData) {
            return documentResource.save(documentData);
        }
    }
}]);