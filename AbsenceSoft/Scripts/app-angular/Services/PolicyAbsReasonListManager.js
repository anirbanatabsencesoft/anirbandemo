//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('policyAbsReasonListManager', ['$resource', '$q', 'policyService', function ($resource, $q, policyService) {

    var init = function (scope) {

            scope.YesNoList = [];
            scope.YesNoList.push('Yes');
            scope.YesNoList.push('No');

            var proposedPageSize = 10;
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;

            scope.PolicyAbsReasons = [];
            scope.PolicyAbsReasonFilter = {
                PolicyId: '',                
                ReasonCode: '',
                AbsReasonName: '',
                EntitlementType: null,
                WorkState: '',
                DoPaging: false,
                PageNumber: 1,
                PageSize: proposedPageSize,
                TotalRecords: 0,
                SortExpression: "Name",
                SortDir: "asc",
                LoadMore: true
            };
            var resetPaging = function () {
                scope.PolicyAbsReasonFilter.PageNumber = 1;
                scope.Policies = [];
            };
            scope.$watch('PolicyAbsReasonFilter.AbsReasonName', resetPaging);
            scope.$watch('PolicyAbsReasonFilter.EntitlementType', resetPaging);
            scope.$watch('PolicyAbsReasonFilter.WorkState', resetPaging);
            //scope.$watch('PolicyAbsReasonFilter.Disabled', resetPaging);

            list(scope, false);
        };
        //end Init()

        var dateInputRegex = /^(?:[01]?)[0-9]([./-])(?:[0123]?)\d{1}\1\d{4}$/;
        var list = function (scope, doPaging, errorCallback) {

            // Get the numeric date representation of our updated filter if a valid date.
            scope.PolicyAbsReasonFilter.DoPaging = doPaging;
            if (scope.PolicyAbsReasonFilter.SortBy == undefined ||
                scope.PolicyAbsReasonFilter.SortBy == null ||
                scope.PolicyAbsReasonFilter.SortBy == '') {
                  scope.PolicyAbsReasonFilter.SortBy = 'Name';
            }           
        
            policyService.AbsReasonList(
                {
                    PolicyId: scope.Policy.Id || scope.PolicyId,
                    PolicyCode: scope.Policy.Code,
                    EmployerId: scope.Policy.EmployerId,
                    AbsReasonName: scope.PolicyAbsReasonFilter.AbsReasonName,
                    EntitlementType: scope.PolicyAbsReasonFilter.EntitlementType,
                    WorkState: scope.PolicyAbsReasonFilter.WorkState,
                    PageNumber: scope.PolicyAbsReasonFilter.PageNumber,
                    PageSize: scope.PolicyAbsReasonFilter.PageSize,
                    SortBy: scope.PolicyAbsReasonFilter.SortBy,
                    SortDirection: parseInt(scope.PolicyAbsReasonFilter.SortDir == "asc" ? 1 : -1)
                })
                .$promise.then(
                function (data, status) {
                    if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                        scope.PolicyAbsReasonFilter.TotalRecords = data.Total;
                        scope.PolicyAbsReasonFilter.LoadMore = true;
                        if (doPaging) {
                            angular.forEach(data.Results, function (result) {
                                scope.PolicyAbsReasons.push(result);
                            });
                        } else {
                            scope.PolicyAbsReasons = data.Results;
                        }
                    } else {
                        scope.PolicyAbsReasons = [];
                        scope.PolicyAbsReasonFilter.LoadMore = false;
                        scope.PolicyAbsReasonFilter.TotalRecords = 0;
                    }
                },
                errorCallback);        
        };

        var updateSort = function (scope, sortBy, errorCallback) {

            //reset the paging
            scope.PolicyAbsReasonFilter.PageNumber = 1;
            scope.Policies = [];
            scope.PolicyAbsReasonFilter.SortBy = sortBy.toString();

            // sort
            if (scope.PolicyAbsReasonFilter.SortExpression == sortBy) {
                scope.PolicyAbsReasonFilter.SortDir = (scope.PolicyAbsReasonFilter.SortDir == 'desc' ? 'asc' : 'desc');
            }
            else {
                scope.PolicyAbsReasonFilter.SortExpression = sortBy;
                if (sortBy == "Updated")
                    scope.PolicyAbsReasonFilter.SortDir = 'desc';
                else
                    scope.PolicyAbsReasonFilter.SortDir = 'asc';
            }

            list(scope, false, errorCallback);
        };
        var loadMore = function (scope, errorCallback) {
            var totalPages = Math.ceil(scope.PolicyAbsReasonFilter.TotalRecords / scope.PolicyAbsReasonFilter.PageSize);
            if (totalPages > 1) {
                if (scope.PolicyAbsReasonFilter.LoadMore && scope.PolicyAbsReasonFilter.PageNumber < totalPages) {
                    scope.PolicyAbsReasonFilter.PageNumber++;
                    list(scope, true, errorCallback);
                }
            }
        };
    
        return {
            Init: init,
            List: list,
            UpdateSort: updateSort,
            LoadMore: loadMore
        }
    }])