﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('absenceReasonService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var absReasonResource = $resource('/PolicyAbsenceReasons/List', {}, {
        'list': { method: 'POST', isArray: false }
    });   

    return {
        List: function (filterData) {
            return absReasonResource.list(filterData);
        },

        GetByCode: function (code, employerId) {

            var resource = $resource('/PolicyAbsenceReasons/' + code + '/GetAbsReasonByCode/' + employerId, {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },
        GetAbsenceReasonById: function (reasonId, employerId) {

            return $http.get('/PolicyAbsenceReasons/' + reasonId + '/GetAbsReasonById/' + employerId);
        },

        GetById: function (reasonId, employerId) {

            var resource = $resource('/PolicyAbsenceReasons/' + reasonId + '/GetAbsReasonById/' + employerId, {}, {
                'list': {
                    method: 'GET', isArray: false
                }
            });

            return resource.list().$promise;
        },

        Upsert: function (data) {

            var resource = $resource('/PolicyAbsenceReasons/' + data.Id + '/UpsertAbsReasonPost', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(data).$promise;
        },
        Delete:function(absenceReasonId)
        {
            return $http.delete('/PolicyAbsenceReasons/Delete/' + absenceReasonId);
        },
        ToggleEnabled: function (code, employerId) {
            var resource = $resource('/PolicyAbsenceReasons/' + code + '/ToggleEnabled/' + employerId, {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save().$promise;
        },
    };
}]);