//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('todoModalService', ['$timeout', '$modal', '$http', '$filter', '$resource', 'toDoListManager', 'lookupService', 'administrationService',
    function ($timeout, $modal, $http, $filter, $resource, toDoListManager, lookupService, administrationService) {

        //#region to-do logic
        var openTodoModal = function (scope, todoItem, ToDos, toDoFilter, IsCaseView) {

            // TODO: update this once more todo's are removed from modals, for now short circuit to the paperwork review page
            if (todoItem.ItemType == 'PaperworkReview') {
                window.location.href = '/Todos/' + todoItem.Id + '/Paperwork/Review';
                return;
            }
            if (todoItem.ItemType == 'EnterWorkRestrictions') {
                window.location.href = '/Todos/' + todoItem.Id + '/WorkRestrictions/Entry';
                return;
            }

            var modalInstance = $modal.open({
                templateUrl: 'TodoModal.html', // id of the template created in view.
                controller: 'TodoModalCtrl', // name of the controller for modal
                resolve: {
                    ToDoItem: function () { // this is how data is resolved in modal controller
                        return todoItem; // passing data to modal
                    },
                    ToDos: function () {
                        return ToDos;
                    },
                    toDoFilter: function () {
                        return toDoFilter;
                    },
                    IsCaseView: function () {
                        return IsCaseView;
                    },
                    CaseScope: function () {
                        return scope;
                    }
                }
            });

            modalInstance.result.then(function () {
                //$scope.SelectedItemFromModel = selectedItem; // called when ok is pressed. passing data from model to page
                toDoListManager.InitToDosFilter(scope);
                scope.ToDosFilter.CaseId = toDoFilter.CaseId;
                $timeout(function () { toDoListManager.List(scope, false, scope.ErrorHandler); }, 200);
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date()); // called when popup is dismissed.
            });
        };


        // this is the controller for data within modal - used in the template
        

        //#endregion to-do logic
        return {
            OpenTodoModal: openTodoModal
        }
    }]);
