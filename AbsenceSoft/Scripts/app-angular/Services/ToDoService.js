//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('todoService', ['$resource', '$q', function ($resource, $q) {
    var todoResource = $resource('/Todos/list', {}, {
        'list': { method: 'POST', isArray: false, cache: false }
    });
    return {
        List: function(filterData) {
            return todoResource.list(filterData);
        },
        NextToDo: function() {
            var resource = $resource('/Todos/GetNextToDo', {}, {
                'list': { method: 'GET', isArray: false, cache: false }
            });
            return resource.list();
        },
        ReassignTodos: function (postData) {
            var resource = $resource('/Todos/Reassign', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            return resource.save(postData).$promise;
        }
    };
}])