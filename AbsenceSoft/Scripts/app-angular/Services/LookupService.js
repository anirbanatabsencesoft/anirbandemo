﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
    .module('App.Services')
    .factory('lookupService', ['$resource', '$q',  function ($resource, $q) {
        return {
            EmployeeRelationships: function (includeSelf) {
                var resource = $resource('/lookups/employeeRelationships', {}, {
                    'list': { method: 'GET', params: { includeSelf: includeSelf }, isArray: true }
                });
                return resource.list();
            },
            MilitaryStatuses: function () {
                var resource = $resource('/lookups/militarystatuses', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            FMLPeriodTypes: function () {
                var resource = $resource('/lookups/fmlperiodtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyBehaviorTypes: function () {
                var resource = $resource('/lookups/PolicyBehaviorTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            WorkflowItemStatuses: function () {
                var resource = $resource('/lookups/WorkflowItemStatuses', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            CaseCancelReason: function () {
                var resource = $resource('/lookups/CaseCancelReason', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            CaseClosureCategory: function (CaseId) {
                var resource = $resource('/lookups/CaseClosureCategory', {}, {
                    'list': { method: 'GET', params: { CaseId: CaseId }, isArray: true }
                });
                return resource.list();
            },
            ContactTypes: function () {
                var resource = $resource('/lookups/ContactTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            AdminContactTypes: function () {
                var resource = $resource('/lookups/AdminContactTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            CaseAssigneeTypes: function () {
                var resource = $resource('/lookups/CaseAssigneeTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            EmployerContactTypes: function () {
                var resource = $resource('/lookups/EmployerContactTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            GetPacketTypes: function (employerId) {
                var resource = $resource('/lookups/GetPacketTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list({employerId: employerId }).$promise;
            },
            GetPacketTypesForTodo: function (employerId) {
                var resource = $resource('/lookups/GetPacketTypesForTodo', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                return resource.list({ employerId: employerId }).$promise;
            },
            GetNoteCategories: function () {
                var resource = $resource('/lookups/GetNoteCategories', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetAttachmentTypes: function () {
                var resource = $resource('/lookups/GetAttachmentTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetEmailRepliesTypes: function () {
                var resource = $resource('/lookups/GetEmailRepliesTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetEmployersForUserPlusUser: function () {
                var resource = $resource('/lookups/GetEmployersForUserPlusUser', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetRolesUser: function (userId) {
                var resource = $resource('/lookups/GetRolesUser', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list({ userId: userId }).$promise;
            },
            GetEmployersForUser: function () {
                var resource = $resource('/lookups/GetEmployersForUser', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetEmployersForCustomer: function () {
                var resource = $resource('/lookups/GetEmployersForCustomer', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list().$promise;
            },
            GetMultiEmployerAccessFeature: function () {
                var resource = $resource('/lookups/GetMultiEmployerAccessFeature', {}, {
                    'get': { method: 'GET', isArray: false }
                });
                return resource.get().$promise;
            },

            GetEmployeeSelfServiceFeature: function () {
                var resource = $resource('/lookups/GetEmployeeSelfServiceFeature', {}, {
                    'get': { method: 'GET', isArray: false }
                });
                return resource.get().$promise;
            },

            AbsenceReasons: function (employeeId, caseType) {
                var resource = $resource('/lookups/absencereasons/', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list({ employeeId: employeeId, caseType: caseType });
            },
            AbsenceReasonsPromise: function (employeeId, caseType) {
                var resource = $resource('/lookups/absencereasons/', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list({employeeId: employeeId, caseType: caseType }).$promise;
            },
            PolicyTypes: function () {
                var resource = $resource('/lookups/policytypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            RuleGroupSuccessTypes: function () {
                var resource = $resource('/lookups/rulegroupsuccesstypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyRuleGroupTypes: function () {
                var resource = $resource('/lookups/policyrulegrouptypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            EntitlementTypes: function () {
                var resource = $resource('/lookups/entitlementtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyEliminationTypes: function () {
                var resource = $resource('/lookups/policyeliminationtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            IntermittentRestrictionsTypes: function () {
                var resource = $resource('/lookups/intermittentrestrictionsTypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            CaseTypes: function () {
                var resource = $resource('/lookups/casetypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyShowTypes: function () {
                var resource = $resource('/lookups/policyshowtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyEvents: function () {
                var resource = $resource('/lookups/policyevents', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            GenderTypes: function () {
                var resource = $resource('/lookups/gendertypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            UnitTypes: function () {
                var resource = $resource('/lookups/unittypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            FMLPeriodTypes: function () {
                var resource = $resource('/lookups/fmlperiodtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            CaseEventTypes: function () {
                var resource = $resource('/lookups/caseeventtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            CaseEventDateTypes: function () {
                var resource = $resource('/lookups/caseeventdatetypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            EventDateTypes: function () {
                var resource = $resource('/lookups/eventdatetypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyShowTypes: function () {
                var resource = $resource('/lookups/policyshowtypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            PolicyRuleGroupTypes: function () {
                var resource = $resource('/lookups/policyrulegrouptypes', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            JobClassifications: function () {
                var resource = $resource('/lookups/JobClassifications', {}, {
                    'list': { method: 'GET', isArray: true }
                });
                return resource.list();
            },
            GetOfficeLocations: function (workState, startDate, endDate, isAccommodation) {
                var resource = $resource('/Lookups/OfficeLocation', {}, {
                    'list': { method: 'GET', params: { workState: workState, startDate: startDate, endDate: endDate, isAccommodation: isAccommodation }, isArray: true }
                });
                return resource.list().$promise;
            },
            GetWorkStates: function (startDate, endDate, isAccommodation) {
                var resource = $resource('/Lookups/WorkState', {}, {
                    'list': { method: 'GET', params: { startDate: startDate, endDate: endDate, isAccommodation: isAccommodation }, isArray: true }
                });
                return resource.list().$promise;
            },
            GetWorkStatesForStatusReport: function (filterData) {
                var resource = $resource('/Lookups/WorkStateForStatusReport', {}, {
                    'list': { method: 'POST', isArray: false }
                });
                return resource.list(filterData).$promise;
            },
            GetOfficeLocationForStatusReport: function (filterData) {
                var resource = $resource('/Lookups/OfficeLocationForStatusReport', {}, {
                    'list': { method: 'POST', isArray: false }
                });
                return resource.list(filterData).$promise;
            },

        PayPeriodTypes: function () {
            var resource = $resource('/lookups/payperiodtypes', {}, {
                'list': { method: 'GET', isArray: true }
            });
            return resource.list();
        },
        GetEmployerFeatures: function (employerId) {
            var resource = $resource('/Customers/GetEmployerFeatures', {}, {
                'get': {
                    method: 'Get', isArray: false
                }
            });
            return resource.get({ employerId: employerId }).$promise;
        },
        AccommodationTypes: function (employerId, caseType) {
            var resource = $resource('/Lookups/AccommodationTypes/' + employerId, {}, {
                'list': { method: 'GET', isArray: true }
            });
            return resource.list({ caseType: caseType });
        },
        GetAllCaseStatus: function () {
            var resource = $resource('/Lookups/GetAllCaseStatus', {}, {
                'list': {
                    method: 'GET', isArray: true
                }
            });
            return resource.list().$promise;
        },
        GetJobDenialReasons: function () {
            var resource = $resource('/Lookups/GetJobDenialReasons', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });
            return resource.get().$promise;
        },
        GetOrganizationTypes: function (userId) {
            var resource = $resource('/lookups/' + userId + '/GetOrganizationTypes', {}, {
                'list': { method: 'GET', isArray: true }
            });
            return resource.list().$promise;
        },
        GetCustomerServiceIndicatorOptions: function () {
            var resource = $resource('/Lookups/GetCustomerServiceIndicatorOptions', {}, {
                'get': {
                    method: 'Get', isArray: true
                }
            });
            return resource.get().$promise;
        },
        GetEmployerOfficeLocations: function (employerId) {
            var resource = $resource('/Lookups/' + employerId + '/GetEmployerOfficeLocations', {}, {
                'list': {
                    method: 'Get', isArray: true
                }
            });
            return resource.list().$promise;
        },
        GetOshaFormFieldOptions: function (employerId, oshaFormFieldCode) {
            var resource = $resource('/Lookups/' + employerId + '/GetOshaFormFieldOptions/' + oshaFormFieldCode , {}, {
                'list': {
                    method: 'Get', isArray: true
                }
            });
            return resource.list().$promise;
        }
    }
}])