//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('essService', [ '$resource', '$q', '$http', function ($resource, $q, $http) {
    
    return {
        Register: function (model) {
            var resource = $resource('/Home/EmployeeRegistrationPost', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(model).$promise;
        },

        Confirmation: function (model) {
            var resource = $resource('/Home/ConfirmRegistrationPost', {}, {
                'save': {
                    method: 'POST', isArray: false
                }
            });

            return resource.save(model).$promise;
        },
    };

}]);
