//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('userListManager', [
    '$resource', '$q', 'administrationService', 'lookupService', '$timeout', '$filter', '_', 'UserStatus',
    function ($resource, $q, administrationService, lookupService, $timeout, $filter, _, UserStatus) {

    var init = function (scope, infiniteScrollOnBody) {
        var proposedPageSize = 1;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight);
        }

        scope.Users = [];
        scope.UsersFilter = {
            UserName: '',
            Email: '',
            Role: '-1',
            Employer: '-1',
            DoPaging: false,
            PageNumber: 1,
            PageSize: proposedPageSize,
            TotalRecords: 0,
            SortExpression: "Name",
            SortDir: "asc",
            LoadMore: true,
            Status: UserStatus.hash.Active
        };
        var resetPaging = function () {
            scope.UsersFilter.PageNumber = 1;
            scope.Users = [];
        };
        scope.$watch('UsersFilter.UserName', resetPaging);
        scope.$watch('UsersFilter.Role', resetPaging);
        scope.$watch('UsersFilter.Email', resetPaging);
        scope.$watch('UsersFilter.Employer', resetPaging);

        list(scope, false);
    };

    var list = function (scope, doPaging, errorCallback) {
        scope.UsersFilter.DoPaging = doPaging;
        administrationService.UserList(
            {
                UserName: scope.UsersFilter.UserName,
                Email: scope.UsersFilter.Email,
                Role: scope.UsersFilter.Role == -1 ? null : scope.UsersFilter.Role,
                Employer: scope.UsersFilter.Employer == -1 ? null : scope.UsersFilter.Employer,
                PageNumber: scope.UsersFilter.PageNumber,
                PageSize: scope.UsersFilter.PageSize,
                SortBy: scope.UsersFilter.SortExpression,
                SortDirection: parseInt(scope.UsersFilter.SortDir == "asc" ? 1 : -1),
                Status: scope.UsersFilter.Status || 'Active'
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    scope.UsersFilter.TotalRecords = data.Total;
                    scope.UsersFilter.LoadMore = true;
                    if (doPaging) {
                        angular.forEach(data.Results, function (result) {
                            scope.Users.push(result);
                        });
                    } else {
                        scope.Users = data.Results;
                    }

                } else {
                    scope.Users = [];
                    scope.UsersFilter.LoadMore = false;
                    scope.UsersFilter.TotalRecords = 0;
                }
            },
            errorCallback);
    };
    var updateSort = function (scope, sortBy, errorCallback) {

        // sort
        if (scope.UsersFilter.SortExpression == sortBy)
            scope.UsersFilter.SortDir = (scope.UsersFilter.SortDir == 'asc' ? 'desc' : 'asc');
        else {
            scope.UsersFilter.SortExpression = sortBy;
            scope.UsersFilter.SortDir = 'asc';
        }

        list(scope, false, errorCallback);
    };
    var loadMore = function (scope, errorCallback) {
        var totalPages = Math.ceil(scope.UsersFilter.TotalRecords / scope.UsersFilter.PageSize);
        if (totalPages > 1) {
            if (scope.UsersFilter.LoadMore && scope.UsersFilter.PageNumber < totalPages) {
                scope.UsersFilter.PageNumber++;
                list(scope, true, errorCallback);
            }
        }
    };

    return {
        Init: init,
        List: list,
        UpdateSort: updateSort,
        LoadMore: loadMore,
    };
}])