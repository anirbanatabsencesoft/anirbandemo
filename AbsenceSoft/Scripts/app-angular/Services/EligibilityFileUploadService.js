//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('eligibilityFileUploadService', ['$resource', '$q', function ($resource, $q) {
    var listEligibilityDataFiles = function (employerId) {
        var resource = $resource('/Files/Eligibility/List/:employerId', {}, {
            'list': {
                method: 'GET', isArray: false
            }
        });
        return resource.list({ employerId: employerId }).$promise;
    }

  
    var getEligibilityDataFileUploadPolicy = function (fileName, employerId) {       
        var resource = $resource('/Files/Eligibility/Policy?fileName=' + fileName + '&employerId=' + employerId, {}, {
            'getPolicy': {
                method: 'GET', isArray: false
            }
        });
        return resource.getPolicy().$promise;
    }

    var createEligibilityDataFileUpload = function (eligibilityUploadObj) {
        var resource = $resource('/Files/Eligibility', {}, {
            'createUpload': {
                method: 'PUT', isArray: false
            }
        });
        return resource.createUpload(eligibilityUploadObj).$promise;
    }

    var deleteEligibilityDataFile = function (fileId) {
        var resource = $resource('/Files/Eligibility/' + fileId  + '/Delete', {}, {
            'deleteFile': {
                method: 'DELETE', isArray: false
            }
        });
        return resource.deleteFile().$promise;
    }


    var cancelEligibilityDataFile = function (fileId) {
        var resource = $resource('/Files/Eligibility/' + fileId + '/Cancel', {}, {
            'cancelFile': {
                method: 'DELETE', isArray: false
            }
        });
        return resource.cancelFile().$promise;
    }

    return {
        GetEligibilityDataFiles: listEligibilityDataFiles,
        GetEligibilityDataFileUploadPolicy: getEligibilityDataFileUploadPolicy,
        CreateEligibilityDataFileUpload: createEligibilityDataFileUpload,
        DeleteEligibilityDataFile: deleteEligibilityDataFile,
        CancelEligibilityDataFile: cancelEligibilityDataFile,
      
    }
}]);