﻿angular
.module('App.Services')
.factory('demandService', ['$resource', '$q', function ($resource, $q) {
    var listAllDemands = $resource('/Demand/ListAll/:EmployerId', {}, {
        'get': {
            method: 'GET', isArray: true
        }
    });

    var getAllDemands = function (employerId) {
        return listAllDemands.get({ EmployerId: employerId });
    }

    return {
        GetAllDemands: getAllDemands
    }
}]);