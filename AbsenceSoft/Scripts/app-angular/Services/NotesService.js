//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('noteService', ['$resource', '$q', function ($resource, $q) {
    var employeeResource = $resource('/Employees/Notes/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var caseResource = $resource('/Cases/Notes/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var noteCategories = $resource('/NoteCategory/List/:employerId', {}, {
        'list': { method: 'GET', isArray: true }
    })

    var noteTemplates = $resource('/NoteTemplatesByCategory/List/:noteCategoryCode', {}, {
        'list': { method: 'GET', isArray: true }
    })


    var renderNoteTemplates = $resource('/Notes/RenderTemplate', {}, {
        'get': {method:'GET', isArray:false}
    });
 

    return {
        EmployeeNotesList: function (filterData) {
            return employeeResource.list(filterData);
        },

        CaseNotesList: function (filterData) {
            return caseResource.list(filterData);
        },
        NoteCategoriesList: function (categoryData) {
            return noteCategories.list(categoryData);
        },
        NoteTemplatesList: function (categoryData) {
            return noteTemplates.list(categoryData);
        },
        RenderNoteTemplate: function (noteTemplateData) {
            return renderNoteTemplates.get(noteTemplateData);
        }
    };
}]);