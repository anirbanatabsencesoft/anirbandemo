//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
﻿angular
.module('App.Services')
.factory('reportService', ['$resource', function ($resource) {
    return {
        RunReport: function (reportId, SearchCriteria) {
            var resource = $resource('/Reports/' + reportId + '/RunReport/', {}, {
                'RunReport': { method: 'POST', isArray: false}
            });

            return resource.RunReport(SearchCriteria).$promise;
        },

        GetCriteria: function (reportId) {
            var resource = $resource('/reports/criteria', {}, {
                'GetCriteria': { method: 'GET', isArray: false }
            });

            return resource.GetCriteria({ reportId: reportId });
        },

        GetReportList: function (category) {
            var resource = $resource('/Reports/GetReportList/', {}, {
                'GetReportList': { method: 'GET', isArray: true }
            });

            return resource.GetReportList({ category: category }).$promise;
        },

        GetReportById: function (reportId) {
            var resource = $resource('/Reports/GetReportById', {}, {
                'GetReportById': { method: 'GET', isArray: false }
            });

            return resource.GetReportById({Id: reportId}).$promise;
        }
    }
}]);