//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('communicationService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    var communicationResource = $resource('/communications/list', {}, {
        'list': { method: 'POST', isArray: false }
    });

    var CreateCummunicationViewModel = function (ToDoItemId, CaseId, TemplateKey, CommunicationId) {
        var CreateCummunicationViewModelUrl = null;

        if (CommunicationId) {
            CreateCummunicationViewModelUrl = '/Communications/' + CommunicationId + '/CreateCummunicationViewModel/Resend';
        } else if (ToDoItemId) {
            CreateCummunicationViewModelUrl = '/Todos/' + ToDoItemId + '/Communications/CreateCummunicationViewModel/' + TemplateKey;
        } else {
            CreateCummunicationViewModelUrl = '/Cases/' + CaseId + '/Communications/CreateCummunicationViewModel/' + TemplateKey;
        }


        var resource = $resource(CreateCummunicationViewModelUrl, {}, {
            'list': {
                method: 'GET', isArray: false
            }
        });

        return resource.list().$promise;
    };

    var ViewCommunicationJson = function (communicationId) {
        var viewCommunicationUrl = '/Communications/' + communicationId + '/ViewCommunicationJson';

        var resource = $resource(viewCommunicationUrl, {}, {
            'list': {
                method: 'GET', isArray: false
            }
        });

        return resource.list().$promise;
    }

    var saveAsDraft = function (communicationModel) {
        var saveCommunicationAsDraft = '/Communications/SaveAsDraft';
        var resource = $resource(saveCommunicationAsDraft, {}, {
            'save': {
                method: 'POST', isArray: false
            }
        });

        return resource.save(communicationModel);
    }

    var deleteDrafts = function (drafts) {
        var deleteDrafts = '/Communications/DeleteDrafts';
        var theDrafts = {
            Communications: drafts
        };
        var resource = $resource(deleteDrafts, {}, {
            'delete': {
                method: 'POST', isArray: false
            }
        });

        return resource.delete(theDrafts);
    }
    return {
        List: function (filterData) {
            return communicationResource.list(filterData);
        },
        CreateCummunicationViewModel: CreateCummunicationViewModel,
        ViewCommunicationJson: ViewCommunicationJson,
        SaveAsDraft: saveAsDraft,
        DeleteDrafts: deleteDrafts
    };

}]);