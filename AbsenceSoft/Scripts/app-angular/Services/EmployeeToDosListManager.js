//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('employeeToDosListManager', ['$resource', '$q', 'caseService', '$filter', 'lookupService', function ($resource, $q, caseService, $filter, lookupService) {
    var init = function (scope, infiniteScrollOnBody) {

        scope.todoItemStatuses = lookupService.WorkflowItemStatuses().then(function (response) {
            scope.todoItemStatuses = response;
            //angular.forEach(scope.todoItemStatuses, function (item, index) {
            //    if (item.Text == "Overdue" || item.Text == "Pending") {
            //        var position = $.inArray(item, scope.todoItemStatuses);
            //        if (~position)
            //            scope.todoItemStatuses.splice(position, 1);
            //    }
            //});
        });

        var proposedPageSize = 10;
        if (infiniteScrollOnBody) {
            var topHeaderOffeset = 110;
            var rowHeight = 19;
            var documentHeight = $(window).height();
            proposedPageSize = parseInt((documentHeight - topHeaderOffeset) / rowHeight) + 5;
        }

        scope.ToDos = [];
        scope.ToDosFilter = {
            Text: '',
            Due: '',
            CaseId: null,
            CaseNumber: '',
            Status: 0,
            DueAsDate: null,
            DoPaging: false,
            PageNumber: 1,
            PageSize: 5,
            TotalRecords: 0,
            SortExpression: "Title",
            SortDir: "asc",
            LoadMore: true
        };

        list(scope, false);
    }

    var list = function (scope, doPaging) {
        scope.ToDos.push({
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012345',
            Title: 'Send Accommodation Acknowledgement Letter to Employee',
            DueDate: '2014/10/01',
            Status: 2
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012345',
            Title: 'Schedule Accommodation Ergonomic Assessment',
            DueDate: '2014/10/01',
            Status: 2
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012345',
            Title: 'HR Communication',
            DueDate: '2014/08/01',
            Status: 5
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012345',
            Title: 'Manager Communication',
            DueDate: '2014/08/05',
            Status: 1
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012555',
            Title: 'Send Accommodation Acknowledgement Letter to Employee',
            DueDate: '2014/10/01',
            Status: 2
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012555',
            Title: 'Schedule Accommodation Ergonomic Assessment',
            DueDate: '2014/10/01',
            Status: 3
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012555',
            Title: 'HR Communication',
            DueDate: '2014/08/01',
            Status: 3
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0012555',
            Title: 'Manager Communication',
            DueDate: '2014/08/05',
            Status: 4
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078555',
            Title: 'Send Accommodation Acknowledgement Letter to Employee',
            DueDate: '2014/10/01',
            Status: 2
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078555',
            Title: 'Schedule Accommodation Ergonomic Assessment',
            DueDate: '2014/10/01',
            Status: 5
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078555',
            Title: 'HR Communication',
            DueDate: '2014/08/01',
            Status: 5
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078555',
            Title: 'Manager Communication',
            DueDate: '2014/08/05',
            Status: 2
        });
    }

    var filter = function (scope, doPaging) {
        var hasFilter = false;

        if (scope.ToDosFilter.Text != null && scope.ToDosFilter.Text != undefined && scope.ToDosFilter.Text != '') {
            hasFilter = true;
            scope.ToDos = $filter('filter')(scope.ToDos, { Title: scope.ToDosFilter.Text });
        }
       
        if (scope.ToDosFilter.CaseNumber != null && scope.ToDosFilter.CaseNumber != undefined && scope.ToDosFilter.CaseNumber != '') {
            hasFilter = true;
            scope.ToDos = $filter('filter')(scope.ToDos, { CaseNumber: scope.ToDosFilter.CaseNumber });
        }

        
        if (scope.ToDosFilter.Status != null && scope.ToDosFilter.Status != undefined && scope.ToDosFilter.Status != '') {
            hasFilter = true;
            scope.ToDos = $filter('filter')(scope.ToDos, { Status: scope.ToDosFilter.Status });
        }

        if (scope.ToDosFilter.Due != null && scope.ToDosFilter.Due != undefined && scope.ToDosFilter.Due != '') {
            hasFilter = true;
            scope.ToDos = $filter('filter')(scope.ToDos, { DueDate: scope.ToDosFilter.Due });
        }

        if (!hasFilter) {
            scope.ToDos = [];
            list(scope, false);
        }
    }

    var loadMore = function (scope, errorCallback) {
        scope.ToDos.push({
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078515',
            Title: 'Send Accommodation Acknowledgement Letter to Employee',
            DueDate: '2014/10/01',
            Status: 2
        },
        {
            CaseId: '54632aa914169408cca9c417',
            CaseNumber: '0078515',
            Title: 'Schedule Accommodation Ergonomic Assessment',
            DueDate: '2014/10/01',
            Status: 3
        });
    };

    var updateSort = function (scope, sortBy, errorCallback) {
        scope.ToDosFilter.SortExpression = sortBy.toString();
        scope.ToDosFilter.SortDir = (scope.ToDosFilter.SortDir == 'desc' ? 'asc' : 'desc');
        var SortDir = (scope.ToDosFilter.SortDir == 'desc' ? false : true);
        scope.ToDos = $filter('orderBy')(scope.ToDos, sortBy.toString(), SortDir);
    };

    return {
        Init: init,
        LoadMore: loadMore,
        UpdateSort: updateSort,
        List: list,
        Filter: filter
    }
}])