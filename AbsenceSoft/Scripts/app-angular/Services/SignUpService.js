//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('signUpService', [ '$resource', '$q', '$http', function ($resource, $q, $http) {
    return {
        List: function (filterData) {
            return caseResource.list(filterData);
        },

        CreateAccount: function (signUpData) {
            var resource = $resource('/SignUp/Account', {}, {
                'CreateAccount': {
                    method: 'POST', isArray: false
                }
            });
            return resource.CreateAccount(signUpData).$promise;
        },

        CreateCustomer: function (customerData) {
            var resource = $resource('/SignUp/Customer', {}, {
                'CreateCustomer': {
                    method: 'POST', isArray: false
                }
            });
            return resource.CreateCustomer(customerData).$promise;
        },

        CreateEmployer: function (employerData) {
            var resource = $resource('/SignUp/Employer', {}, {
                'CreateEmployer': {
                    method: 'POST', isArray: false
                }
            });
            return resource.CreateEmployer(employerData).$promise;
        }
    }
}])