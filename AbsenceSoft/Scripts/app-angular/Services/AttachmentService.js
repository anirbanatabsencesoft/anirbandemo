//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('attachmentService', ['$resource', '$q', function ($resource, $q) {
    var resource = $resource('/Attachments/List', {}, {
        'list': { method: 'POST', isArray: false }
    });

    return {
        List: function (filterData) {
            return resource.list(filterData);
        }
    };
}]);