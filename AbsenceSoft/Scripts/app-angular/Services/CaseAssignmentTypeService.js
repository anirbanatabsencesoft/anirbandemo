﻿angular
.module('App.Services').factory('caseAssignmentTypeService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    return {
        Save: function (caseAssignmentType) {
            var resource = $resource('/CaseAssignment/Save', {}, {
                'save': { method: 'POST', isArray: false }
            });

            return resource.save(caseAssignmentType).$promise;
        },
        Delete: function (customerId, caseAssignmentTypeId) {
            var url = '/CaseAssignment/' + customerId + '/Delete/' + caseAssignmentTypeId;
            return $http.delete(url);
        },
        GetAssignableUsers: function (caseId) {
            var resource = $resource('/CaseAssignment/' + caseId + '/AssignableUsers', {}, {
                'get': { method: 'GET', isArray: true }
            });

            return resource.get().$promise;
        },
        GetAssignableTypes: function (customerId, employerId) {
            var resource = $resource('/CaseAssignment/' + customerId + '/Types/' + employerId, {}, {
                'get': { method: 'GET', isArray: true }
            });

            return resource.get().$promise;
        },
        UpdateCaseAssignment: function (assignment) {
            var resource = $resource('/CaseAssignment/UpdateCaseAssignment', {}, {
                'save': { method: 'POST', isArray: false }
            });

            return resource.save(assignment).$promise;
        }

    }
}]);