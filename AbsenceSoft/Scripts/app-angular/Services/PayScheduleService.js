//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
angular
.module('App.Services')
.factory('payScheduleService', ['$resource', '$q', '$http', function ($resource, $q, $http) {
    return {
        List: function (employerId) {
            var resource = $resource('/PaySchedule/' + employerId + '/List', {}, {
                'list': { method: 'GET', isArray: true }
            });

            return resource.list().$promise;
        },

        Get: function (employerId, payScheduleId) {
            var resource = $resource('/PaySchedule/' + employerId + '/Get/' + payScheduleId, {}, {
                'get': { method: 'GET', isArray: false }
            });

            return resource.get().$promise;
        },

        Save: function (paySchedule) {
            var resource = $resource('/PaySchedule/Save', {}, {
                'save': { method: 'POST', isArray: false }
            });

            return resource.save(paySchedule).$promise;
        },
        Delete: function (employerId, payScheduleId) {
            var url = '/PaySchedule/' + employerId + '/Delete/' + payScheduleId;
            return $http.delete(url);
        }

    }
}]);