﻿(function () {
    'use strict';

    angular
        .module('App.Admin.EmployerHoliday')
        .directive('employerHolidayListview', Directive);

    Directive.$inject = [];

    function Directive() {
        // Usage:
        // Creates:
        // 
        var directive = {
            replace: true,
            require: 'ngModel',
            scope: {
                vm: '=ngModel',
                itemSelected: '=itemSelected'
            },
            templateUrl: '/scripts/app-angular/admin/employerholiday/employer-holiday-listview.html'
        };
        return directive;
    }

})();