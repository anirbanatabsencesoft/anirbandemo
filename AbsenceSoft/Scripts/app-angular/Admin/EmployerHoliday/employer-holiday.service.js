﻿(function () {
    'use strict';
    angular

        .module('App.Admin.EmployerHoliday')

        .factory('EmployerHolidayService', Service);

    Service.$inject = ['$http', 'EmployerHolidayUtil'];
    function Service($http, EmployerHolidayUtil) {
        var resource = '/Administration/Employer/:employerId/Holiday/:holidayId';

        return {
            query : query,
            update : update,
            destroy: destroy
        };

        function query(options) {
            return $http.get(getUrl(options))
                .then(setScheduleTypes);
        }

        function destroy(options) {
            return $http.delete(getUrl(options), {
                    data: options.data
                })
                .then(function (res) {
                    setScheduleType(res.data);
                    return res.data;
                });
        }

        function update(options) {
            return $http.put(getUrl(options), options.data)
                .then(function (res) {
                    setScheduleType(res.data);
                    return res.data;
                });
        }

        function getUrl(options) {
            var url = resource;
            url = url.replace(":employerId", options.employerId);
            if (options.data) {
                url = url.replace(":holidayId", options.data.Id)
            } else {
                url = url.replace(":holidayId", "");
            }
            return url;
        }

        function setScheduleTypes(res) {
            res.data.Items.forEach(setScheduleType);
            return res.data;
        }

        function setScheduleType(h) {
            h.scheduleType = EmployerHolidayUtil.resolveScheduleType(h);            
            return h;
        }

    }

})();