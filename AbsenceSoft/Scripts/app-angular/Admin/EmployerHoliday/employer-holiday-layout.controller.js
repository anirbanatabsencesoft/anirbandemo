﻿(function () {
    'use strict';

    angular
        .module('App.Admin.EmployerHoliday')
        .controller('EmployerHolidayLayoutCtrl', EmployerHolidayController);

    EmployerHolidayController.$inject = ['$scope', '$log', '$location', '$routeParams', 'administrationService', 'EmployerHolidayService', '_'];

    function EmployerHolidayController($scope, $log, $location, $routeParams, adminSvc, EmployerHolidayService, _) {
        /* jshint validthis:true */
        var
            vm = $scope,
            state = {
                list: "list",
                edit: "edit",
                add: "add"
            };

        activate();

        function activate() {
            busy(true);
            angular.extend(vm, {
                employerId: $routeParams.eId,

                onItemSelected: beginItemEdit,
                itemEditCancelled: onEditCancelled,
                Items: [],
                editItem: null,
                state: state.list,
                itemSaved: onItemSaved,
                itemDeleted: onItemDeleted,

                addHoliday: addHoliday
            });

            loadData();
        }

        function loadData() {
            EmployerHolidayService.query({ employerId: vm.employerId })
                .then(function (data) {
                    vm.Items = data.Items;
                    vm.Name = data.EmployerName;
                })
                .finally(function () {
                    busy(false);
                });
        }

        function onEditCancelled() {
            angular.extend(vm, {
                editItem: null,
                state: state.list
            });
        }
        function addHoliday() {
            angular.extend(vm, {
                state: state.add,
                editItem: {
                    StartDate: new Date()
                }
            });
        }


        function beginItemEdit(item) {
            angular.extend(vm, {
                editItem: item,
                state: state.edit
            });
        }

        function onItemDeleted(deleted) {
            for (var i = 0; i < vm.Items.length; i++) {
                var item = vm.Items[i];
                if(item.Id === deleted.Id){
                    vm.Items.splice(i, 1);
                    break;
                }    
            }
            angular.extend(vm, {
                editItem: null,
                state: state.list
            });
        }

        function onItemSaved(item) {
            var found = vm.Items.filter(function (f) {
                return f.Id === item.Id;
            });
            if (found.length === 0) {
                vm.Items.push(item);
            }
            angular.extend(vm, {
                editItem: null,
                state: state.list
            });
        }

        function busy(isBusy) {
            vm.isBusy = isBusy;
        }
    }
})();
