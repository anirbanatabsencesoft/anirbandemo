﻿(function (undefined) {
    'use strict';
    angular
    .module('App.Admin.EmployerHoliday')

    .constant('ScheduleTypes', {
        all: scheduleTypes(),
        byId: scheduleTypes().reduce(function (prev, next) {
            prev[next.id] = next;
            return prev;
        }, {})
    });

    //
    // HACK: Don't particularly care for implementing the templates this way
    function scheduleTypes() {
        return [
                {
                    id: 'date-only',
                    label: 'Specific Date',
                    longdescription: 'A single, non-recurring date',
                    template: '<input datepicker ng-model="item.Date" />'
                },
                {
                    id: 'month-day',
                    label: 'Yearly, Same Month and Day',
                    longdescription: 'A holiday that occurs yearly on the same month and day, such as Independence Day',
                    template: '<month-day-picker month="item.Month" day-of-month="item.DayOfMonth" />'
                },
                {
                    id: 'month-week-weekday',
                    label: 'Yearly, Same Month, Week, and Weekday:',
                    longdescription: 'A holiday that occurs on the nth week of a month on a given weekday, such as Thanksgiving Day',
                    template: '<month-week-weekday-picker month="item.Month" week-of-month="item.WeekOfMonth" day-of-week="item.DayOfWeek" />'
                },
        ];
    }

})();