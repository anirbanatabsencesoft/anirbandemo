﻿(function () {
    'use strict';
    angular
        .module('App.Admin.EmployerHoliday')
        .factory('EmployerHolidayUtil', EmployerHolidayUtil);

    EmployerHolidayUtil.$inject = ['$interpolate', 'Calendar', '_'];
    function EmployerHolidayUtil($interpolate, Calendar, _) {
        return {
            resolveScheduleType: resolveScheduleType
        };

        function resolveScheduleType(holiday) {
            var
                displayTextTemplate = null,
                resolvedType = null;
            //HACK: Couldn't get custom filter to work with $interpolate
            holiday.monthname = _.chain(Calendar.Default.Months)
                                .filter(function (f) { return f.Value === holiday.Month; })
                                .map(function (f) { return f.Label; })
                                .first()
                                .value();
            holiday.dayname = _.chain(Calendar.Default.WeekDays)
                                .filter(function (f) { return f.Value === holiday.DayOfWeek; })
                                .map(function (f) { return f.Label; })
                                .first()
                                .value();
            holiday.weekname = _.chain(Calendar.Default.MonthWeeks)
                                .filter(function (f) { return f.Value === holiday.WeekOfMonth; })
                                .map(function (f) { return f.Label; })
                                .first()
                                .value();



            if (holiday.DayOfWeek >= 0 && holiday.Month && holiday.WeekOfMonth) {
                displayTextTemplate = "Yearly on the {{weekname}} {{dayname}} of {{monthname}}";
                resolvedType = "month-week-weekday";

            } else if (holiday.Month && holiday.DayOfMonth) {
                displayTextTemplate = "Yearly on {{Month}}/{{DayOfMonth}}";
                resolvedType = "month-day";

            } else if (holiday.Date) {
                displayTextTemplate = "One time on {{Date | date:'yyyy-MM-dd'}}";
                resolvedType = "date-only";

            }

            if (displayTextTemplate !== null) {
                holiday.DisplayText = $interpolate(displayTextTemplate)(holiday);
                if (holiday.Date) {
                    holiday.SpecificDate = $interpolate("{{Date | date:'MM/dd/yyyy'}}")(holiday);
                }
            } else {
                holiday.DisplayText = "Error: Unknown schedule type!";
            }
            return resolvedType;
        }
    }

})();