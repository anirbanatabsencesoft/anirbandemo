﻿(function () {
    'use strict';

    angular
        .module('App.Admin.EmployerHoliday')
        .directive('scheduleTypePicker', scheduleTypePicker);

    scheduleTypePicker.$inject = ['$window', '$compile', 'EmployerHolidayUtil'];

    function scheduleTypePicker($window, $compile, EmployerHolidayUtil) {
        // Usage:
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            require: 'ngModel',
            scope: {
                selected: '=scheduleTypeSelected',
                item: '=ngModel',
                isRequired: '=scheduleTypeRequired'
            }
        };
        return directive;

        //
        // HACK: This could have been done better.  Wasn't aware and didn't 
        // understand the ngModelController api at the time of implementation
        // https://docs.angularjs.org/api/ng/type/ngModel.NgModelController
        function link(scope, element, attrs, ctrl) {
            scope.$watch('selected', onSelectedChanged);
            scope.$watch('item', onItemChanged, true);

            function onItemChanged(newvalue, oldvalue) {
                var valid = EmployerHolidayUtil.resolveScheduleType(newvalue) !== null;
                ctrl.$setValidity('scheduleTypeInvalid', valid);
            }

            function onSelectedChanged(newvalue, oldvalue) {
                if (newvalue && oldvalue && newvalue.id !== oldvalue.id) {
                    resetDependantValues();
                }
                element.empty();
                if (newvalue) {
                    element
                        .empty()
                        .append(newvalue.template);
                    $compile(element.contents())(scope);
                }
            }

            function resetDependantValues() {
                angular.extend(scope.item, {
                    Date: null,
                    Month: null,
                    DayOfMonth: null,
                    WeekOfMonth: null,
                    DayOfWeek: null,
                    SpecificDate: null 
                });
            }

            function validate(value) {
                if (value) {
                    var valid = EmployerHolidayUtil.resolveScheduleType() !== null;
                    ctrl.$setValidity('invalidScheduleType', valid);
                }
                return valid ? value : undefined;
            }
        } // link
    }

})();