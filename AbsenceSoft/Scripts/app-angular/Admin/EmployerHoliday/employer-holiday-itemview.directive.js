﻿(function () {
    'use strict';

    angular
        .module('App.Admin.EmployerHoliday')
        .directive('employerHolidayItemview', EmployerHolidayItemViewDirective);

    EmployerHolidayItemViewDirective.$inject = ['$log', 'ScheduleTypes', 'EmployerHolidayService'];
    function EmployerHolidayItemViewDirective($log, ScheduleTypes, EmployerHolidayService) {
        // Usage:
        // Creates:
        // 
        var directive = {
            restrict: 'EA',
            replace: true,
            require: 'ngModel',
            templateUrl: '/scripts/app-angular/admin/employerholiday/employer-holiday-itemview.html',
            scope: {
                item: '=ngModel',
                saved: '=saved',
                cancelled: '=cancelled',
                employerId: '=employerId',
                deleted: '=deleted'
            },
            link: link
        };
        return directive;

        function link($scope) {
            var
                vm = $scope,
                scheduleTypeLookup = ScheduleTypes.byId;

            angular.extend(vm, {
                scheduleTypes: ScheduleTypes.all,
                activeScheduleType: null,
                submit: submit,
                cancel: cancel,
                destroy: destroy,
                isBusy: false,
                original: angular.copy(vm.item)
            });

            activate();

            function activate() {
                $scope.$watch('item.scheduleType', onScheduleTypeChanged);
            }

            function onScheduleTypeChanged(newvalue) {
                vm.activeScheduleType = scheduleTypeLookup[newvalue];
            }

            function cancel() {
                if (vm.original) {
                    angular.copy(vm.original, vm.item);
                }

                if (vm.cancelled) {
                    vm.cancelled(vm.item);
                }
            }

            function destroy() {
                if (confirm("Select OK to permanently remove this holiday. Cancel to abort.")) {
                    busy(true);
                    EmployerHolidayService.destroy({
                        employerId: vm.employerId, data: vm.item
                    })
                    .then(function () {
                        if (vm.deleted) {
                            vm.deleted(vm.item);
                        }
                    })
                    .finally(function () {
                        busy(false);
                    });

                }
            }

            function submit() {
                if (vm.employerHolidayForm.$invalid) {
                    $log.error('Form errors found, aborting:', vm.employerHolidayForm.$error);
                    return;
                }
                if (!vm.employerId) {
                    throw new 'employer-id must not bound';
                }

                $log.debug('Submitting: ', vm.item, vm.employerId);
                busy(true);

                EmployerHolidayService.update({
                    employerId: vm.employerId,
                    data: vm.item
                })
                .then(function (data) {
                    angular.copy(data, vm.item);
                    if (vm.saved) {
                        vm.saved(vm.item);
                    }
                })
                .finally(function () {
                    busy(false);
                });

            }

            function busy(busy) {
                vm.isBusy = busy;
            }
        }

    }

})();