﻿(function () {
    'use strict';

    angular
        .module('App.Admin.EmployerHoliday', [
            'App.Filters',
            'App.Services',
            'App.Directives',
            'ui.bootstrap.datepicker',
            'ngRoute',
        ])
        .config(Router)
    ;

    Router.$inject = ['$routeProvider', ];
    function Router($routeProvider) {
        $routeProvider.when('/admin/employer/:eId/holidays', {
            templateUrl: '/scripts/app-angular/admin/employerholiday/employer-holiday-layout.html',
            controller: 'EmployerHolidayLayoutCtrl'
        });
    }

})();