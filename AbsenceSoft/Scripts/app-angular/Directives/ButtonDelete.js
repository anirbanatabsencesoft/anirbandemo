﻿(function() {
    'use strict';

    angular
        .module('App.Directives')
        .directive('buttonDelete', ButtonDelete);

    ButtonDelete.$inject = ['$window'];
    
    function ButtonDelete ($window) {
        // Usage:
        //     <button button-delete />
        // Creates:
        // 
        var directive = {
            link: link,
            replace: true,
            template: 
                 '<button class="btn btn-inline btn-default" >' +
                '<i class="glyphicon glyphicon-remove"></i>' +
                '</button>'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();