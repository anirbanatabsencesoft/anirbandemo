﻿(function() {
    'use strict';

    angular
        .module('App.Directives')
        .directive('emptyToNull', EmptyToNull);

    EmptyToNull.$inject = ['$window'];
    
    function EmptyToNull($window) {
        // Usage:
        //     <el empty-to-null />
        // Creates:
        //      Resolves empty strings to null
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel'
        };
        return directive;

        function link(scope, elem, attrs, ctrl) {
            ctrl.$parsers.push(function (viewValue) {
                if (viewValue === "") {
                    return null;
                }
                return viewValue;
            });
        }
    }

})();