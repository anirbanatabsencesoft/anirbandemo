﻿(function() {
    'use strict';

    angular
        .module('App.Directives')
        .directive('iconHelp', iconHelp);

    iconHelp.$inject = ['$window'];
    
    function iconHelp($window) {
        // Usage:
        //     <icon-help></icon-help>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'E',
            template: '<i class="glyphicon glyphicon-question-sign" style="cursor:pointer"></i>'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();