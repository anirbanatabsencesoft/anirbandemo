﻿
(function () {
    'use strict';

    angular
      .module('App.Directives')
      .directive('dateInRange', DateInRange);

    DateInRange.$inject = ['$filter'];
    function DateInRange($filter) {
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel'
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var
                isoDateToDate = $filter('ISODateToDate'),
                input = {
                    rangeStart: null,
                    rangeEnd: null,
                    value: null
                };

            activate();

            function activate() {
                attrs.$observe('rangeStart', validate);
                attrs.$observe('rangeEnd', validate);
                ngModelCtrl.$viewChangeListeners.push(validate);
            }

            function validate() {
                angular.extend(input, {
                    rangeStart: isoDateToDate(scope.$eval(attrs.rangeStart)),
                    rangeEnd: isoDateToDate(scope.$eval(attrs.rangeEnd)),
                    value: toDate(ngModelCtrl.$viewValue || ngModelCtrl.$modelValue)
                });

                var valid = true;
                if (input.value) {
                    valid =
                        (input.rangeStart && input.value >= input.rangeStart)
                        && (input.rangeEnd && input.value <= input.rangeEnd);
                }

                ngModelCtrl.$setValidity('inrange', valid);
            }

            function toDate(value) {
                if (!value) {
                    return null;
                }

                if (typeof value === 'string') {
                    return new Date(value);
                }

                return value;
            }
        }

    }
})();

