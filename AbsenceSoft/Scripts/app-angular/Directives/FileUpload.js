angular.module('App.Directives').directive('fileUpload', ['documentService', function (documentService) {

    function uploadComplete(scope, document) {
        return function () {
            var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Document uploaded successfully" });
            documentService.Save(document).$promise.then(function (savedDoc) {
                if (scope.fileUploadComplete)
                    scope.fileUploadComplete(savedDoc);
            });
        }
        
    }

    function uploadFailed() {
        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "Document uploaded failed" });
    }

    function uploadCancelled() {
        var n = noty({ timeout: 10000, layout: 'topRight', type: 'warning', text: "Document upload cancelled" });
    }

    function link(scope, element, attrs) {

        $(element).fileupload({
            dataType: 'json',
            autoUpload: false,
            sequentialUploads: true
        });

        element.on('fileuploadadd', function (e, data) {
            if(data.files.length === 0)
                return;

            var file = data.files[0];
            var policyInfo = {
                employerId: scope.employerInfo.Id,
                fileName: file.name
            }
            var uploadPolicy = documentService.UploadPolicy(policyInfo).$promise.then(function (documentUploadPolicy) {
                var document = {
                    EmployerId: scope.employerInfo.Id,
                    CustomerId: scope.employerInfo.CustomerId,
                    DocumentType: 0,
                    Mime: file.type,
                    Size: file.size,
                    Name: file.name,
                    Loc: documentUploadPolicy.fileName
                };
                
                var fd = new FormData();
                fd.append('key', documentUploadPolicy.fileName);
                fd.append('acl', documentUploadPolicy.acl);
                fd.append('Content-Type', data.files[0].type);
                fd.append('AWSAccessKeyId', documentUploadPolicy.s3Key);
                fd.append('policy', documentUploadPolicy.s3PolicyBase64);
                fd.append('signature', documentUploadPolicy.s3Signature);
                fd.append("file", data.files[0]);
                var xhr = new XMLHttpRequest();
                xhr.addEventListener('load', uploadComplete(scope, document), false);
                xhr.addEventListener('error', uploadFailed, false);
                xhr.addEventListener('abort', uploadCancelled, false);
                xhr.open('POST', documentUploadPolicy.postUrl, true);
                xhr.send(fd);
                
            })
            
        });
    }
    return {
        restrict: 'A',
        scope: {
            employerInfo: '=employer',
            fileUploadComplete: '=uploadComplete'
        },
        link: link
    }
}]);