﻿angular
.module('App.Directives')
.directive('selectActivityType', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/select-activity-type.html'
    };
});