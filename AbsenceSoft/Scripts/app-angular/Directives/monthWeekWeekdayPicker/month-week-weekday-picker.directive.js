﻿(function () {
    'use strict';

    angular
        .module('App.Directives')
        .directive('monthWeekWeekdayPicker', MonthWeekWeekdayPicker);

    MonthWeekWeekdayPicker.$inject = ['Calendar']
    function MonthWeekWeekdayPicker(Calendar) {
        // Usage:
        // Creates:
        // 
        var directive = {
            restrict: 'EA',
            templateUrl: '/scripts/app-angular/directives/monthWeekWeekdayPicker/month-week-weekday-picker.html',
            scope: {
                month: '=month',
                weekOfMonth: '=weekOfMonth',
                dayOfWeek: '=dayOfWeek'
            },
            link: link
        };
        return directive;

        function link($scope) {
            var vm = $scope;
            activate();
            function activate() {
                angular.extend(vm, {
                    monthWeeks: Calendar.Default.MonthWeeks,
                    weekDays: Calendar.Default.WeekDays,
                    months: Calendar.Default.Months
                });
            }
        }
    }

})();