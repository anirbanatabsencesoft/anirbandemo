﻿/*
 * Directive wrapper for bootstrap-datepicker.
 * See https://github.com/eternicode/bootstrap-datepicker for documentation and demos.
 * Remarks:
 *  - Accepts no global options at current.  See code for options.
 *  - Format is hard-coded to MM/dd/yyyy below
 *  - A formatter is used to ensure backwards compatibilty with previous 'ToISODate' hack for now.
 * 
 */
(function () {
    'use strict';

    angular
      .module('App.Directives')
      .directive('atDatepickerPopup', DatePicker);

    DatePicker.$inject = ['$log', '$filter', 'moment'];
    function DatePicker($log, $filter, moment) {
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel',
            scope: {
                ngModel: '=ngModel',
                startDate: '=?startDate',
                endDate: '=?endDate'
            }
        };
        return directive;

        function link(scope, element, attrs, controller) {
            var toDate =
                $filter('ISODateToDate')

            activate();

            function formatDate(value) {
                if (!value) {
                    return null;
                }

                value = $filter('ISODateToDate')(value);
                return $filter('date')(value, 'MM/dd/yyyy');
            }

            function activate() {
                if (!element.datepicker) {
                    throw "datepicker not found. Ensure bootstrap-datepicker is included";
                }

                controller.$formatters.unshift(formatDate);
                controller.$parsers.push(formatDate);

                // HACK: wait for input element interaction to
                // attach the date picker controller and give model a chance
                // to be loaded.
                // otherwise, the datepicker controller does not bind to the 
                // the textbox properly.
                element.on('click', attachDatepicker);
                element.on('focus', attachDatepicker);
            }

            function attachDatepicker(evt) {
                evt.preventDefault();
                evt.stopPropagation();

                element.off('click', attachDatepicker);
                element.off('focus', attachDatepicker);


                element.datepicker({
                    autoclose: true,
                    todayBtn: 'linked',
                    todayHighlight: true,
                    assumeNearbyYear: true,
                    format: 'mm/dd/yyyy',
                    // HACK: unfortunately, Firefox is doing something weird and chopping off the top of the picker sometimes
                    orientation: 'top',
                    startDate: formatDate( scope.startDate ),
                    endDate: formatDate( scope.endDate )
                });
                element.datepicker('show');
            }

            var formats = [
                moment.ISO_8601
            ];

            function formatDate(value) {
                if (value) {
                    var date = new Date(value);
                    var parsed = moment(date);
                    if (parsed.isValid) {
                        var result = parsed.format('MM/DD/YYYY');
                        return result;
                    }
                    throw 'Unexpected date format: ' + value;
                } else {
                    return value;
                }
            }
        }

    }
})();

