﻿(function () {
    'use strict';
    angular
        .module('App.Directives')
        .directive('scrollToInvalid', ScrollToInvalid);

    ScrollToInvalid.$inject = ['$window', '$timeout'];
    function ScrollToInvalid($window, $timeout) {
        return {
            link: link
        };

        function link(scope, elem) {
            elem.on('submit', submitted);

            function submitted() {
                var firstInvalid = elem[0].querySelector('.ng-invalid, .has-error');

                // if we find one, set focus
                if (firstInvalid) {
                    var offset = $(firstInvalid).offset().top;

                    angular
                        .element('body')
                        .animate({ scrollTop: offset }, 'slow')
                        .focus()
                    ;
                    return false;
                }

                return true;
            }
        }
    }

})();