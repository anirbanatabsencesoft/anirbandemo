﻿(function () {
    'use strict';
    angular

        .module('App.Directives')


        /**
         * Adds 'form-submitted' class when form is submitted.
         * <form submitted>...</form>
         * //TODO: When form becomes valid again, class is reset to form-not-submitted
         */
        .directive('submitted', Directive);

    function Directive() {
        return {
            retrict: 'A',
            link: link
        };

        function link(scope, el) {
            var submitted = false;
            el
                .addClass('form-not-submitted');

            el.on('submit', function (evt) {
                el
                    .removeClass('form-not-submitted')
                    .addClass('form-submitted');
                return true;
            });
        }

    }

})();