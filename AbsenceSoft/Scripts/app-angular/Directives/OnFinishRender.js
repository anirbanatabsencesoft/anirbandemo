﻿(function () {
    'use strict';

    angular
        .module('App.Directives')
        .directive('onFinishRender', finishRender);

    finishRender.$inject = ['$timeout'];

    function finishRender($timeout) {
        var directive = {
            link: link,
            restrict: 'A',
        };
        return directive;

        function link(scope, element, attrs) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                })
            }
        }
    }

})();