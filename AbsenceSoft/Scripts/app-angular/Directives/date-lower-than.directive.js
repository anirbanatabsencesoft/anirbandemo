﻿(function () {
    'use strict';

    angular
      .module('App.Directives')
      .directive('dateLowerThan', DateLowerThanDirective);

    DateLowerThanDirective.$inject = [];
    function DateLowerThanDirective() {
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel'
        };

        return directive;

        function link(scope, element, attrs, ctrl) {
            attrs.$observe('dateLowerThan', onValueChanged);
            ctrl.$viewChangeListeners.push(onValueChanged);

            function onValueChanged() {
                ctrl.$setValidity('dateLowerThan', validate());
            }

            function validate() {
                var value = ctrl.$modelValue || ctrl.$viewValue;
                if (!value) {
                    return true;
                }
                var otherdate = attrs.dateLowerThan;
                if (!otherdate) {
                    return true;
                }

                //
                // HACK: Why are we binding to a string instead of a model value in the view??
                if (typeof value === "string") {
                    value = value.replace(/"/g, '');
                    value = new Date(value);
                }
                if (typeof otherdate === "string") {
                    otherdate = otherdate.replace(/"/g, '');
                    otherdate = new Date(otherdate);
                }

                return value <= otherdate;
            }
        }
    }
})();

