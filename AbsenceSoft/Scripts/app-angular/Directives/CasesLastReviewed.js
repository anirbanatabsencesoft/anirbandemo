﻿angular
.module('App.Directives')
.directive('casesLastReviewed', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/cases-last-reviewed.html'
    };
});