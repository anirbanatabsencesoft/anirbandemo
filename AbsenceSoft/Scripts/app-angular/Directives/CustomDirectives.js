﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one service per file!
//TODO: use templates instead of inline html in JS
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic numbers
//Hours and minutes input

angular
.module('App.Directives')
.directive('input', function () {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function (scope, element, attr, ngModel) {

            if (attr.type !== 'hoursMinutes') return;

            var logMatches = function (matches) {
                if (!matches) return;
                for (var i = 0; i <= matches.length - 1; i++) {
                    console.log("[" + i + "]" + " " + matches[i]);
                }
            };

            /// IMPORTANT : One must not update patterns in following function if do not have extensive knowledge of RegularExpressions.
            var getFormattedDuration = function (currentDuration) {
                var getHourMinutes = function (h, m) {
                    h = parseInt(h);
                    if (h == undefined || h.toString() == "NaN" || h == "") { h = 0; }
                    if (m != undefined && m.toString() != "NaN" && m != "") {
                        m = parseFloat(m);
                        if (m > 60) {
                            h = h + parseInt(m / 60);
                            m = m % 60;
                        }
                        m = parseInt(m);
                        return h + "h " + m + "m";
                    }
                    return h + "h 0m";
                };

                var getForDecimalHours = function (pattern) {
                    var matches = pattern.exec(currentDuration);
                    //logMatches(matches);
                    if (matches != null && matches.length == 3) {
                        var m = matches[2];
                        if (m != undefined && m.toString() != "NaN" && m != "") {
                            m = parseFloat(parseFloat("0." + m) * 60);
                        }
                        return getHourMinutes(matches[1], m);
                    }
                    return "";
                };

                var getForDecimalMinutes = function (pattern) {
                    var matches = pattern.exec(currentDuration);
                    //logMatches(matches);
                    if (matches != null && matches.length == 2) {
                        return getHourMinutes(0, matches[1]);
                    }
                    return "";
                };

                var result = "";

                // CASE1 : 3:30 = 3h 30m
                //var pattern = /^((?:\d){1,2})\:((?:\d){1,2})$/ig;
                var pattern = /^(\d+)\:?(\d*)$/ig;
                var matches = pattern.exec(currentDuration);
                if (matches != null && matches.length == 3) {
                    return getHourMinutes(matches[1], matches[2]);
                }

                // CASE2 : 3.5 = 3h 30m
                // CASE20 : .5 = 0h 30m
                pattern = /^(\d*)\.?(\d{0,2})$/ig;
                result = getForDecimalHours(pattern);
                if (result != "") return result;

                //CASE3 : 3.5h = 3h 30m
                //CASE4 : 3.5hrs = 3h 30m
                //CASE5 : 3.5hours = 3h 30m
                //CASE6 : 3.5 h = 3h 30m
                //CASE7 : 3.5 hrs = 3h 30m
                //CASE8 : 3.5 hours = 3h 30m
                pattern = /^(\d+)\.?(\d{0,2})(?:hours|hrs|h| hours| hrs| h)$/ig;
                result = getForDecimalHours(pattern);
                if (result != "") return result;

                //CASE9 : 210min = 3h 30m
                //CASE10 : 210mins = 3h 30m
                //CASE11 : 210minutes = 3h 30m
                //CASE12 : 210 min = 3h 30m
                //CASE13 : 210 mins = 3h 30m
                //CASE14 : 210 minutes = 3h 30m
                //CASE20 : 5 m = 0h 5m
                //CASE21 : 5m = 0h 5m                
                pattern = /^(\d+)(?:mins|min|minutes|m| mins| min| minutes| m)$/ig
                result = getForDecimalMinutes(pattern);
                if (result != "") return result;

                //CASE15 : 3hours 30minutes = 3h 30m
                //CASE16 : 3 hours 30 minutes = 3h 30m
                pattern = /^(\d+) {0,1}(?:hours|hrs|h) *(\d+) {0,1}(?:mins|min|minutes)$/ig;
                matches = pattern.exec(currentDuration);
                if (matches != null && matches.length == 3) {
                    //logMatches(matches);
                    return getHourMinutes(matches[1], matches[2]);
                }

                //CASE17 : 3h 0.5m = 3h 30m
                //CASE18 : 3hrs 0.5mins = 3h 30m
                pattern = /^(\d+) {0,1}(?:hours|hrs|hr|h) *(\d*\.*\d*) {0,1}(?:mins|min|minutes|m)$/ig;
                matches = pattern.exec(currentDuration);
                if (matches != null && matches.length == 3) {
                    // logMatches(matches);
                    var m = matches[2];
                    if (m != undefined && m.toString() != "NaN" && m != "") {
                        var pattern2 = /\./ig;
                        var matches2 = pattern2.exec(m);
                        if (matches2 != null && matches2.length > 0) {
                            // minutes contains decimal place, so convert to minutes.
                            m = parseFloat(parseFloat(m) * 60);
                        }
                        else {
                            m = parseFloat(m); // no need for converions
                        }
                    }
                    return getHourMinutes(matches[1], m);
                }

                return "";
            };

            element.unbind('change');
            element.bind('change', function (e) {
                var formattedDuration = "";
                if (this.value != undefined && this.value.toString() != "NaN" && this.value != "") {
                    formattedDuration = getFormattedDuration(this.value);
                }
                scope.$apply(function () {
                    $(element).val(formattedDuration);
                    if (ngModel) {
                        ngModel.$setViewValue(formattedDuration);
                    }
                });
                e.preventDefault();
            });
        }
    };
});
//viewTodoModal
angular
.module('App.Directives')
.directive('viewTodoModal', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/view-todo-modal.html'
    };
});
//viewToDoSuccessModal
angular
.module('App.Directives')
.directive('viewTodoSuccessModal', function () {

    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/view-todo-success-modal.html'
    };
});
//forgotPasswordModal
angular
.module('App.Directives')
.directive('forgotPasswordModal', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/forgot-password-modal.html'
    };
});

angular
.module('App.Directives')
.directive('changePasswordModal', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/change-password-modal.html'
    };
});

angular
.module('App.Directives')
.directive('downloadZipModal', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/download-zip-link.html'
    };
});

//contactsModal
angular
.module('App.Directives')
.directive('contactsModal', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/contacts.html'
    };
});
angular
.module('App.Directives')
.directive('whenScrolled', function () {
    return function (scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function () {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});
angular
.module('App.Directives')
.directive('onFinishRepeat', function () {
    return function (scope, element, attr) {
        scope.$emit('onFinishRepeat', element, attr);
    };
});
angular
.module('App.Directives')
.directive('caseMonth', ['$compile', '$timeout', 'userPermissions', function ($compile, $timeout, userPermissions) {

    function link(scope, element, attrs) {
        var monthDivId = 'month' + scope.$parent.$index;
        var monthDivQuerySelector = '#' + monthDivId;
        var monthStartDate = scope.$parent.month;
        var allowPopupRendering = false;

        scope.saveTimeLostForDay = function ($event, CaseNumber) {
            var date = $event.currentTarget.id.substring(4);
            var txtId = '#txt_' + date;
            var time = $(txtId).val();
            if (time) {
                date = date.split('_').join('/');
                scope.$parent.SaveTimeLostForDay(date, time, CaseNumber);
            }
            else {
                var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Please enter time.' });
            }            
        };

        scope.getTimeValue = function (missedTime, dateToMatch) {
            if (!missedTime) {
                return "";
            }
            var i = 0;
            for (i = 0; i < missedTime.length; i++) {
                if (new Date(new Date(missedTime[i].SampleDate).toDateString()).getTime() === dateToMatch.getTime()) {
                    break; 
                }
            }
            if (missedTime.length <= i) {
                return "";
            }
            let obj = missedTime[i];

            //let obj = missedTime.find(o => new Date(new Date(o.SampleDate).toDateString()).getTime() === dateToMatch.getTime());
            if (obj) {
                var m = obj.TotalMinutes;
                    var h = 0;
                if (m != undefined && m != "" && !isNaN(m.toString())) {
                    m = parseFloat(m);
                    if (m > 60) {
                        h = h + parseInt(m / 60);
                        m = m % 60;
                    }
                    m = parseInt(m);
                    return h + "h " + m + "m";
                }
            }
            return "";
        };
        
        var onDayRender = function (date) {

            if (!allowPopupRendering) return false;

            if (date.getMonth() !== monthStartDate.getMonth() || date.getFullYear() !== monthStartDate.getFullYear())
                return false;

            var popoverCustomClass = 'day-' + date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();

            var indexOfMatch = null;
            var matchingDays = scope.CaseCalendar.Dates.filter(function (caseCalendarDay) {
                var isMatch = caseCalendarDay.Date.isSameDay(date);
                return isMatch;
            });

            if (matchingDays.length > 0) {
                var innerScope = scope.$new();
                var matchDay = matchingDays[0];
                if (scope.IsESS)
                    matchDay.IsESSIntermittentEdit = true;
                else
                    matchDay.IsESSIntermittentEdit = false;

                $timeout(function () {

                    var template =
                        '<div class="popover-custom ' + popoverCustomClass + '">' +
                            '<div class="popover top">' +
                                '<div class="arrow"></div>' +
                                '<h3 class="popover-title">{{MatchingDay.Date.toDateString()}}</h3>' +
                                '<div class="popover-content popover-content-calendar">' +
                                    '<div class="case" ng-repeat="case in Cases">' +
                                    '<div><strong>{{case.ReasonName}} #{{case.CaseNumber}}</strong></div>' +
                                    '<div ng-if="case.CaseType == 2">' +
                                    '<strong>Intermittent Time Off:</strong>' +
                                    '<div ng-repeat="uses in case.UserRequests">' +
                                    '{{uses.IntermittentTypeText}}: {{uses.TotalTime}}' +
                                    '</div>' +
                                    '<br />' +
                                    '</div>' +
                                        '<div class="policy" ng-repeat="policy in case.Policies">' +
                                        'Policy: {{policy.PolicyName}}<br />' +
                                        'Status: {{policy.DeterminationText}}<br />' +
                                        'Absent Time: {{policy.HoursUsed}} {{policy.UsedTypeText}}' +
                                    '</div>' +
                                    '<div ng-if="case.CaseType != 2 && case.CaseType != 8 && AllowChange">' +
                                        'Absence Time: <input id=txt_{{MatchingDay.Date|date:"MM_dd_yyyy"}} value={{getTimeValue(case.MissedTimeOverride,MatchingDay.Date)}} type ="hoursMinutes" class = "absence-time-input"/>' +
                                        '<button type="button" id= btn_{{MatchingDay.Date|date:"MM_dd_yyyy"}} class="btn btn-primary" ng-click="saveTimeLostForDay($event,case.CaseId)">Save</button>' +
                                    '</div> ' +
                                    '<div>{{MatchingDay.HolidayName}}</div>' +                                    
                                '</div>' +                                
                            '</div>' +
                        '</div>';
                    var dayElementSelector = monthDivQuerySelector + " td.day-" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                    if ($(dayElementSelector).length > 0) {
                        var element = $(dayElementSelector);
                        if (userPermissions.indexOf("ViewITOR") != -1 || matchDay.IsHoliday)
                            element.append(template);
                        
                        if (matchDay.HasIntermittentSegment) {
                            $(dayElementSelector).unbind('click');
                            $(dayElementSelector).css("cursor", "pointer");
                            $(dayElementSelector).click(function (event) {
                                angular.element(dayElementSelector).scope().EditIntermittentRequest(matchDay, event);
                            });
                        }
                        else {
                            $(dayElementSelector).click(function () { return false; });
                        }

                        innerScope.MatchingDay = matchDay;
                        innerScope.Cases = matchDay.Cases;
                        innerScope.AllowChange = scope.AllowChange;
                        $compile($(".popover-custom." + popoverCustomClass).contents())(innerScope);
                    }
                    if ($("." + popoverCustomClass).length > 1) {
                        $("." + popoverCustomClass + ":gt(1)").remove();
                    }
                }, 2000);

                var classesToReturn = 'day-' + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                if (matchDay.HasApprovedUsage) {
                    classesToReturn += " approved";
                }
                if (matchDay.HasPendingUsage) {
                    classesToReturn += " pending";
                }
                if (matchDay.HasDeniedUsage) {
                    classesToReturn += " denied";
                }

                if (matchDay.HasApprovedUsage
                    && matchDay.HasDeniedUsage
                    && matchDay.HasPendingUsage) {
                    classesToReturn += " approved-pending-denied";
                }
                else if (matchDay.HasApprovedUsage
                    && matchDay.HasPendingUsage) {
                    classesToReturn += " approved-pending";
                }
                else if (matchDay.HasApprovedUsage
                    && matchDay.HasDeniedUsage) {
                    classesToReturn += " approved-denied";
                }
                else if (matchDay.HasPendingUsage
                    && matchDay.HasDeniedUsage) {
                    classesToReturn += " pending-denied";
                }

                if (matchDay.HasConsecutiveSegment) {
                    classesToReturn += " consecutive";
                }
                if (matchDay.HasIntermittentSegment) {
                    classesToReturn += " intermittent";
                }
                if (matchDay.HasConsecutiveSegment
                    && matchDay.HasIntermittentSegment) {
                    classesToReturn += " consecutive-intermittent";
                }
                if (matchDay.IsHoliday) {
                    classesToReturn += " holiday";
                }

                return {
                    //classes: 'timeoff-inter ' + classesToReturn
                    classes: classesToReturn
                };
            }
            return false;
        };

        attrs.$set('id', monthDivId);
        $(monthDivQuerySelector).datepicker({
            daysOfWeekDisabled: "0,1,2,3,4,5,6",
            beforeShowDay: onDayRender
        });

        allowPopupRendering = true;
        $(monthDivQuerySelector).datepicker('setDate', monthStartDate);

        // restyle appropriately
        $(monthDivQuerySelector + " td.day.disabled:not(.old .new)").each(function (index, element) {
            $(element).addClass("cal-cell");
        })
        $(monthDivQuerySelector + " th.prev").each(function (index, element) {
            $(element).addClass("disabled");
            this.style.visibility = "hidden";
        })
        $(monthDivQuerySelector + " th.next").each(function (index, element) {
            $(element).addClass("disabled");
            this.style.visibility = "hidden";
        })
        $(monthDivQuerySelector + " th.datepicker-switch").each(function (index, element) {
            $(element).addClass("disabled");
        })
        $(monthDivQuerySelector + " td.day.disabled.old").each(function (index, element) {
            this.style.visibility = "hidden";
        })
        $(monthDivQuerySelector + " td.day.disabled.new").each(function (index, element) {
            this.style.visibility = "hidden";
        })
        $(monthDivQuerySelector + " td.day.disabled.active").each(function (index, element) {
            $(element).removeClass("active");
        })
    };

    return {
        scope: {
            CaseCalendar: '=calendarData',
            IsESS: '@isEss',
            AllowChange: '=allowChange'
        },
        link: link
    };
}]);
angular
.module('App.Directives')
.directive('bootstrapModal', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            scope.HideBootstrapModal = function () {
                element.modal('hide');
            };
        }
    };
});
angular
.module('App.Directives')
.directive('bsTooltip', function () {
    return function (scope, element, attr) {
        $(element).tooltip();
    };
});
angular
.module('App.Directives')
.directive('absenceReasonClick', function () {
    return function (scope, element, attr) {
        if (scope.absenceReason.IsCategory) {
            // when you click on a category, clear all non-child items and child-items, hide all category divs, show that category div
            $(element).on('click', function () {
                $(".absence-reason-item").each(function (index, element) {
                    element.checked = false;
                });
                $(".absence-reason-child-item").each(function (index, element) {
                    element.checked = false;
                });
                $(".absence-reason-category-div").each(function (index, element) {
                    $(element).hide();
                });
                $("#" + scope.absenceReason.Category.replace(/ /g, '') + "AbsenceReasonCategoryDiv").show();
            });
        }
        else if (!scope.absenceReason.IsChild) {
            // when you click on an non-child item, clear all child items, clear all categories, hide all category divs
            $(element).on('click', function () {
                $(".absence-reason-child-item").each(function (index, element) {
                    element.checked = false;
                });
                $(".absence-reason-category").each(function (index, element) {
                    element.checked = false;
                });
                $(".absence-reason-category-div").each(function (index, element) {
                    $(element).hide();
                });
            });
        }
    };
});
angular
.module('App.Directives')
.directive('isValidDob', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            function validate(value) {
                angular.element('#' + elem[0].id).scope().DOBValidation('All');
            }

            scope.$watch(function () {
                return ngModel.$viewValue;
            }, validate);
        }
    };
});

angular
.module('App.Directives')
.directive('ngModelOnBlurEmployeeEmail', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function () {
                scope.$apply(function () {
                    if (elm.val()) {
                        $http({ method: 'GET', url: "/SignUp/CheckEmail", params: { email: elm.val() } }).
                         then(function (response) {
                             if (response.data.Exists != false) {
                                 $http({ method: 'GET', url: "/SignUp/CheckEmailCustomerId", params: { email: elm.val() } }).
                                         then(function (response) {
                                             if (response.data.Exists == true) {
                                                 $http({ method: 'GET', url: "/SignUp/CheckEmployeeEmail", params: { email: elm.val() } }).
                                                     then(function (response) {
                                                         if (response.data.Exists == true) {
                                                             ngModelCtrl.$setValidity('employeeEmail', true);
                                                         }
                                                         else {
                                                             ngModelCtrl.$setValidity('employeeEmail', false);
                                                         }
                                                     }, function (response) {
                                                         var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                                                     });
                                             }
                                             else {
                                                 ngModelCtrl.$setValidity('employeeEmail', false);
                                             }
                                         }, function (response) {
                                             var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                                         })
                             }
                             else {
                                 $http({ method: 'GET', url: "/SignUp/CheckEmployeeEmail", params: { email: elm.val() } }).
                                     then(function (response) {
                                         if (response.data.Exists == true) {
                                             ngModelCtrl.$setValidity('employeeEmail', true);
                                         }
                                         else {
                                             ngModelCtrl.$setValidity('employeeEmail', false);
                                         }
                                     }, function (response) {
                                         var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                                     })
                             }
                         }, function (response) {
                             var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                         });
                    }
                });

            }
        )
        }
    }
}])
angular
.module('App.Directives')
.directive('ngChangeContact', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;
            // elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('change', function () {
                scope.$apply(function () {
                    if (elm.val() == null || elm.val() == undefined || elm.val() == '') {
                        ngModelCtrl.$setValidity('duplicateContact', true);
                        return;
                    }

                    $http({
                        method: 'GET',
                        url: "/Case/Employee/" + attr.employeeid + "/IsContactTypeDuplicate/?contactType=" + elm.val()
                    }).
                     then(function (response) {
                         ngModelCtrl.$setValidity('duplicateContact', !response.data.IsDuplicate);
                         if (response.data.IsDuplicate) {
                             var element = angular.element("#ContactDuplication");
                             element.modal('show');
                         }
                     });

                });
            }
        )
        }
    }
}]);
angular
.module('App.Directives')
.directive('ngModelDomainOnblur', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function () {
                scope.$apply(function () {
                    if (elm.val() != '') {
                        $http({ method: 'GET', url: "/SignUp/CheckDomain", params: { subDomain: elm.val() } }).
                         then(function (response) {
                             if (response.data.Available == true) {
                                 ngModelCtrl.$setValidity('subDomain', true);
                                 return true;
                             }
                             else {
                                 ngModelCtrl.$setValidity('subDomain', false);
                                 return false;
                             }
                         }, function (response) {
                             var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                         });
                    }
                });
            }
        )
        }
    }
}]);
angular
.module('App.Directives')
.directive('ngModelEmployerDomainOnblur', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function () {
                scope.$apply(function () {
                    if (elm.val() != '') {
                        $http({ method: 'GET', url: "/SignUp/CheckEmployerDomain", params: { subDomain: elm.val(), employerId: attr.employerid } }).
                         then(function (response) {
                             if (response.data.Available == true) {
                                 ngModelCtrl.$setValidity('subDomain', true);
                                 return true;
                             }
                             else {
                                 ngModelCtrl.$setValidity('subDomain', false);
                                 return false;
                             }
                         }, function (response) {
                             var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.data.message });
                         });
                    }
                });
            }
        )
        }
    }
}]);
angular
.module('App.Directives')
.directive('isContactDuplicate', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            var validate = function (viewValue) {
                var lname = attr.lname;
                var contactType = attr.contacttype;
                var employeeid = attr.employeeid;

                if (viewValue != '' && lname != '' && contactType != '' && employeeid != '') {
                    $http({
                        method: 'GET',
                        url: "/Case/Employee/" + attr.employeeid + "/IsEmployeeContactDuplicate",
                        params: { contactType: attr.contacttype, firstName: viewValue, lastName: attr.lname }
                    }).then(function (response) {
                        if (response.data.IsDuplicate == true) {
                            ngModelCtrl.$setValidity('duplicateEmployeeContact', false);
                            return true;
                        }
                        else {
                            ngModelCtrl.$setValidity('duplicateEmployeeContact', true);
                            return false;
                        }
                    });
                }
                else {
                    ngModelCtrl.$setValidity('duplicateEmployeeContact', true);
                }
                return viewValue;
            };

            //ngModelCtrl.$parsers.unshift(validate);
            //ngModelCtrl.$formatters.push(validate);

            elm.bind('blur', function () {
                return validate(ngModelCtrl.$viewValue);
            });

            var elemLastname = angular.element("#" + attr.lnameid);
            elemLastname.bind('blur', function () {
                return validate(ngModelCtrl.$viewValue);
            });

            attr.$observe('contacttype', function (comparisonModel) {
                // Whenever the comparison model changes we'll re-validate
                return validate(ngModelCtrl.$viewValue);
            });
        }
    }
}]);
angular
.module('App.Directives')
.directive('ngBlurPasswordCheck', ['$http', function ($http) {
    var pattern = new RegExp(/^.*(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[\d\W\s]).*$/);

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            ngModelCtrl.$validators.password = validateComplexity;

            function validateComplexity(modelValue, viewValue) {
                var value = modelValue || viewValue;
                return value ?
                    pattern.test(value) :
                    true;
            }
        }
    }
}]);
angular
.module('App.Directives')
.directive('ngBlur', function () {
    return function (scope, elem, attrs) {
        elem.bind('blur', function () {
            scope.$apply(attrs.ngBlur);
        });
    };
});
angular
.module('App.Directives')
.directive('ngBlurTel', function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModelCtrl) {
            elem.bind('blur', function () {
                var tel = elem.val();
                if (!tel) {
                    scope.$apply(function () {
                        ngModelCtrl.$setValidity('tel', true);
                        return false;
                    })

                    return '';
                }
                var value = tel.toString().trim().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '').replace(/[^\d]/g, '');
                if (value.match(/[^0-9]/)) {
                    return tel;
                }

                if (value == "" || value.length < 10) {
                    scope.$apply(function () {
                        ngModelCtrl.$setValidity('tel', false);
                        return false;
                    })

                }
                else {
                    scope.$apply(function () {
                        ngModelCtrl.$setValidity('tel', true);
                        return false;
                    })
                }

                var country, city, number, ext;
                switch (value.length) {
                    case 10: // +1PPP####### -> C (PPP) ###-####
                        country = "+1";
                        city = value.slice(0, 3);
                        number = value.slice(3);
                        ext = '';
                        break;
                        //case 11: // +CPPP####### -> CCC (PP) ###-####
                        //    country = "+" + value[0];
                        //    city = value.slice(1, 4);
                        //    number = value.slice(4);
                        //    ext = '';
                        //    break;
                        //case 12: // +CCCPP####### -> CCC (PP) ###-####
                        //    country = "+" + value.slice(0, 3);
                        //    city = value.slice(3, 5);
                        //    number = value.slice(5);
                        //    ext = '';
                        //    break;
                    default:
                        if (value.length > 10) {
                            if (value[0] == 1) {
                                country = "+" + value[0];
                                city = value.slice(1, 4);
                                number = value.slice(4, 11);
                                ext = " x" + value.slice(11);
                            }
                            else {
                                country = "+1";
                                city = value.slice(0, 3);
                                number = value.slice(3, 10);
                                ext = " x" + value.slice(10);
                            }
                        }
                        else {
                            return tel;
                        }
                }
                //if (country == 1) {
                //    country = "";
                //}
                number = number.slice(0, 3) + '-' + number.slice(3);
                return elem.val((country + " (" + city + ") " + number + ext).trim());
            });
        }
    }
});
angular
.module('App.Directives')
.directive('slideToggle', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var target = document.querySelector(attrs.slideToggle);
            var slideToUp = document.querySelector(attrs.slideToUp);

            attrs.expanded = false;
            element.bind('click', function () {
                if (scope.frmreportcriteria.$valid) {
                    if ($(slideToUp).attr("id").indexOf("reportViewShow") < 0 && $(slideToUp).is(":visible")) {
                        $(slideToUp).slideUp('slow');
                    }
                    if ($(target).attr("id").indexOf("criteria") >= 0 || $(slideToUp).attr("id").indexOf("criteria") >= 0) {
                        scope.showCriteria = !scope.showCriteria;
                    }

                    if ($(target).is(":hidden")) {
                        $(target).slideDown('slow');

                    } else {
                        if ($(target).attr("id").indexOf("reportViewShow") < 0 && $(target).is(":visible")) {
                            $(target).slideUp('slow');
                        }
                    }
                }

            });
        }
    }
});

angular
.module('App.Directives')
.directive('employeeCustomField', ['$compile', '$filter', function ($compile, $filter) {
    function link(scope, element, attrs) {

        var fieldHTML = '<div class="col-sm-6 col-wrap">' +
                            '<div class="col-sm-4 field-name">{{TheField.Name}}</div>';



        var name = $filter('nospace')(scope.TheField.Name);

        //var frmRef = scope.TheForm + "['" + name + "']";
        var frmRef = '$parent.' + scope.TheForm + '.' + name;
        var fmfRefErr = frmRef + '.' + '$error.required';
        //var fmfRefPris = frmRef + '.' + '$prestine';
        //var fmfRefDty = frmRef + '.' + '$dirty';
        var req = scope.TheField.IsRequired ? 'required' : '';

        if (scope.TheField.ValueType == 2) // Select List
        {

            if (scope.TheField.DataType == 4 && scope.TheField.ListValues.length <= 4) {//radio button list

                fieldHTML = fieldHTML + '<div class="col-sm-7 col-sm-offset-1 field-input">';
                fieldHTML = fieldHTML + '<div class="btn-group group-' + scope.TheField.ListValues.length + '" data-toggle="buttons">';
                angular.forEach(scope.TheField.ListValues, function (item, index) {
                    fieldHTML = fieldHTML + '<label class="btn btn-info" ng-class="{ \'active\' : TheField.SelectedValue==\'' + item.Key + '\'}" ng-click="TheField.SelectedValue=\'' + item.Key + '\';">';
                    fieldHTML = fieldHTML + '<input type="radio" name="' + name + '" ng-model="TheField.SelectedValue" value="false" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + '/> ' + item.Key;
                    fieldHTML = fieldHTML + '</label>';
                });
                //fieldHTML = fieldHTML + '<label class="btn btn-info" ng-class="{ \'active\' : TheField.SelectedValue==true}" ng-click="TheField.SelectedValue=true;">';
                //fieldHTML = fieldHTML + '<input type="radio" name="' + name + '" ng-model="TheField.SelectedValue" value="true" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + '/> Yes';
                //fieldHTML = fieldHTML + '</label>';
                //fieldHTML = fieldHTML + '<label class="btn btn-info" ng-class="{ \'active\' : TheField.SelectedValue==false}" ng-click="TheField.SelectedValue=false;">';
                //fieldHTML = fieldHTML + '<input type="radio" name="' + name + '" ng-model="TheField.SelectedValue" value="false" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + '/> No';
                //fieldHTML = fieldHTML + '</label>';
                fieldHTML = fieldHTML + '</div>';
                if (scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && submitted ">Please enter ' + scope.TheField.Name + '</span>';
                }
                fieldHTML = fieldHTML + '</div>';
            }
            else {
                fieldHTML = fieldHTML + '<div class="col-sm-7 col-sm-offset-1 field-input">';
                fieldHTML = fieldHTML + '<select name="' + name + '" ng-model="TheField.SelectedValue" class="form-control" ng-options="lv.Key as lv.Value for lv in TheField.ListValues"' + (scope.TheField.IsRequired ? ' required="required"'
                        + ' ng-class="{\'has-error\':' + fmfRefErr + ' && submitted }"' : '') + ' >';
                fieldHTML = fieldHTML + '<option value="">Select One</option>';
                fieldHTML = fieldHTML + '</select>';
                if (scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && submitted">Please enter ' + scope.TheField.Name + '</span>';
                }
                fieldHTML = fieldHTML + '</div>';
            }
        }

        if (scope.TheField.ValueType == 1) // User Entered 
        {
            if (scope.TheField.DataType == 1 || scope.TheField.DataType == 2)//text field
            {
                fieldHTML = fieldHTML + '<div class="col-sm-7 col-sm-offset-1 field-input">';
                fieldHTML = fieldHTML + '<input type="' + (scope.TheField.DataType == 1?'text':'number') + '" name="' + name + '" class="form-control input-sm" ng-model="TheField.SelectedValue" ' + (scope.TheField.IsRequired ? 'required="required" ' + ' ng-class="{\'has-error\':' + fmfRefErr + ' && submitted}"' : '') + ' />';
                if (scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && submitted">Please enter ' + scope.TheField.Name + '</span>';
                    //fieldHTML = fieldHTML + '<span class="error" ng-cloak>Please enter ' + scope.TheField.Name + ' {{submitted}}</span>';
                    //fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && !' + fmfRefPris + ' && submitted || ' + fmfRefErr + ' && ' + fmfRefDty + '">Please enter ' + scope.TheForm.Name +'</span>';
                }
                fieldHTML = fieldHTML + '</div>';
            }

            if (scope.TheField.DataType == 4) {//radio button list
                fieldHTML = fieldHTML + '<div class="col-sm-7 col-sm-offset-1 field-input">';
                fieldHTML = fieldHTML + '<div class="btn-group group-2" data-toggle="buttons">';
                fieldHTML = fieldHTML + '<label class="btn btn-info" ng-class="{ \'active\' : TheField.SelectedValue==true}" ng-click="TheField.SelectedValue=true;">';
                fieldHTML = fieldHTML + '<input type="radio" name="' + name + '" ng-model="TheField.SelectedValue" value="true" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + '/> Yes';
                fieldHTML = fieldHTML + '</label>';
                fieldHTML = fieldHTML + '<label class="btn btn-info" ng-class="{ \'active\' : TheField.SelectedValue==false}" ng-click="TheField.SelectedValue=false;">';
                fieldHTML = fieldHTML + '<input type="radio" name="' + name + '" ng-model="TheField.SelectedValue" value="false" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + '/> No';
                fieldHTML = fieldHTML + '</label>';
                fieldHTML = fieldHTML + '</div>';
                if (scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && submitted ">Please enter ' + scope.TheField.Name + '</span>';
                }
                fieldHTML = fieldHTML + '</div>';
            }

            if (scope.TheField.DataType == 8) {//datepicker
                fieldHTML = fieldHTML + '<div class="col-sm-6 col-sm-offset-1 field-input">';
                fieldHTML = fieldHTML + '<div class="input-group">';
                if (!scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<input id="' + name + '" name="' + name + '" type="text" class="form-control" placeholder="mm/dd/yyyy" at-datepicker-popup="MM/dd/yyyy" datepicker-options="{{dateOptions}}" show-button-bar="true" ng-model="TheField.SelectedValue" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + ' />';
                }
                else {
                    fieldHTML = fieldHTML + '<input id="' + name + '" name="' + name + '" type="text" class="form-control" placeholder="mm/dd/yyyy" at-datepicker-popup="MM/dd/yyyy" datepicker-options="{{dateOptions}}" show-button-bar="true" ng-model="TheField.SelectedValue" ' + (scope.TheField.IsRequired ? 'required="required"' : '') + ' ng-class="{ \'has-error\' : ' + fmfRefErr + ' && submitted}"/>';
                }
                fieldHTML = fieldHTML + '<label for="' + name + '" class="input-group-addon">';
                fieldHTML = fieldHTML + '<i class="fa fa-calendar"></i>';
                fieldHTML = fieldHTML + '</label>';
                fieldHTML = fieldHTML + '</div>';

                if (scope.TheField.IsRequired) {
                    fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && submitted">Please select ' + scope.TheField.Name + '</span>';
                    //fieldHTML = fieldHTML + '<span class="error" ng-cloak ng-show="' + fmfRefErr + ' && !' + fmfRefPris + ' && submitted || ' + fmfRefErr + ' && ' + fmfRefDty + '">Please enter ' + scope.TheForm.Name +'</span>';
                }

                fieldHTML = fieldHTML + '</div>';

            }
        }



        fieldHTML = fieldHTML + '</div>';

        element.append(fieldHTML);
        //var innerScope = scope.$new();
        //innerScope.field = scope.TheField;
        //innerScope.form = scope.TheForm;
        $compile($(element).contents())(scope);
    }

    return {
        scope: {
            TheField: "=thefield",
            TheForm: "=theform",
            submitted: "=submitted",
        },
        link: link
    };
}]);
/// Edit company info modal
angular
.module('App.Directives')
.directive('editCompanyInfo', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/edit-company-info.html'
    };
});
/// Edit security settings modal
angular
.module('App.Directives')
.directive('editSecuritySettings', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/edit-security-settings.html'
    };
});
// Edit employer info modal
angular
.module('App.Directives')
.directive('editEmployerInfo', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/edit-employer-info.html'
    };
});
angular
.module('App.Directives')
.directive('hasPermission', ['userPermissions', function (userPermissions) {
    return {
        link: function (scope, element, attrs) {
            if (!attrs.hasPermission.substring)
                throw "hasPermission value must be a string";

            var value = attrs.hasPermission.trim();
            var notPermissionFlag = value[0] === '!';
            if (notPermissionFlag) {
                value = value.slice(1).trim();
            }

            function toggleVisibilityBasedOnPermission() {
                var hasPermission = isInArray(value, userPermissions);

                if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
                    if (!$(".controller-cases .hide-when")) {
                        element.show();
                    }
                }
                else
                    element.hide();
            }
            function isInArray(value, array) {
                return array.indexOf(value) > -1;
            }
            toggleVisibilityBasedOnPermission();
            //scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
        }
    };
}]);
// employer access modal
angular
.module('App.Directives')
.directive('employerAccess', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/employer-access.html'
    };
});
angular
.module('App.Directives')
.directive('popover', function () {
    return function (scope, elem) {
        elem.popover();
    }
});
angular
.module('App.Directives')
.directive('dashboardTodos', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/dashboard-todos.html'
    };
});

angular
.module('App.Directives')
.directive('dashboardEmployees', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/dashboard-employees.html'
    };
});

angular
.module('App.Directives')
.directive('dashboardCases', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/dashboard-cases.html'
    };
});

angular
.module('App.Directives')
.directive('employeeDashboardMyinfo', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/employee-dashboard-myinfo.html'
    };
});

angular
.module('App.Directives')
.directive('employeeDashboardCases', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/employee-dashboard-cases.html'
    };
});

angular
.module('App.Directives')
.directive('employeeDashboardTodos', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/employee-dashboard-todos.html'
    };
});

angular
.module('App.Directives')
.directive('supHrDashboardCases', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/suphr-dashboard-cases.html'
    };
});

angular
.module('App.Directives')
.directive('supHrDashboardTodos', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/suphr-dashboard-todos.html'
    };
});

angular
.module('App.Directives')
.directive('supHrDashboardEmployees', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/suphr-dashboard-employees.html'
    };
});

angular
.module('App.Directives')
.directive('employeeInfo', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/employee-info.html'
    };
});

angular
.module('App.Directives')
.directive('caseHistory', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/case-history.html'
    };
});

angular
.module('App.Directives')
.directive('employeeDashboard', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/directives/templates/dashboard/employee-dashboard.html'
    };
});

angular
.module('App.Directives')
.directive('employeeDash', function () {
    return {
        restrict: 'E',
        templateUrl: '/scripts/app-angular/dashboard/employee-dashboard.html'
    };
});
angular
.module('App.Directives')
.directive('scrollIf', function () {
    return function (scope, element, attributes) {
        setTimeout(function () {
            if (scope.$eval(attributes.scrollIf)) {
                $(element).parent().animate({ scrollTop: element[0].offsetTop - 100 }, 0);                
            }
        });
    }
 });

angular
.module('App.Directives').directive('converttonumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});