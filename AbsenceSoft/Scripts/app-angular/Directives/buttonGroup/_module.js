﻿(function () {
    'use strict';

    angular
        .module('App.Directives.ButtonGroup', [
            , 'ui.bootstrap'
            , 'ui.bootstrap.tpls'
        ]);

})();