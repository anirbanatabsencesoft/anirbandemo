﻿(function () {
    'use strict';
    angular

        .module('App.Directives.ButtonGroup')
        .directive('buttonGroup', ButtonGroupDirective);
    /*
     * Usage: 
     *      <div button-group 
     *          ng-model="myButtonValue" 
     *          button-items="myitemsarray" 
     *          help-text="myHelpText"
     *          caption="myOverallCaption"
     *          />
     * buttonItems expected properties:
     *  Id: Used for name of control
     *  Value: The value to set on the model when selected
     *  Caption: Label to use for the button
     */

    function ButtonGroupDirective() {
        var directive = {
            link: link,
            require: 'ngModel',
            scope: {
                buttonItems: '=',
                ngModel: '=',
                helpText: '=',
                caption: '=',
                ngChange: '='
            },
            templateUrl: '/scripts/app-angular/directives/buttongroup/button-group.html'
        };
        return directive;

        function link(scope, element, attrs, ctrl) {
            var vm = scope;
        }
    }

})();