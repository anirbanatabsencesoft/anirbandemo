﻿/*
 * Checks for unique email address (deleted or not).
 * Set validators: uniqueEmail, accountEnabled, accountNotDeleted
 */
(function () {
    'use strict';

    angular
        .module('App.Directives')
        .directive('atUniqueEmail', UniqueEmailDirective);

    UniqueEmailDirective.$inject = ['$http', '$q'];
    function UniqueEmailDirective($http, $q) {

        return {
            restrict: 'A',
            require: 'ngModel',
            link: link,
            scope: {
                signup: '=signup'
            }
        };

        function link(scope, elem, attr, ngCtrl) {
            ngCtrl.$asyncValidators.uniqueEmail = uniqueEmailValidator;

            function uniqueEmailValidator(modelValue, viewValue) {
                var value = modelValue || viewValue;

                if (!value) {
                    return $.when(true);
                }

                return $http({ method: 'GET', url: "/SignUp/CheckEmail", params: { email: value } })
                    .then(function (res) {
                        if (res.data.Exists) {
                            ngCtrl.$setValidity('accountEnabled', !res.data.IsDisabled);
                            ngCtrl.$setValidity('accountNotDeleted', !res.data.IsDeleted);
                            return $q.reject();
                        } else if (scope.signup) {
                            ngCtrl.$setValidity('matchesDomain', !res.data.MatchesDomain);
                            scope.$parent.matchesDomainContent = res.data.MatchesDomainContent;
                            if (res.data.MatchesDomain)
                                return $q.reject();

                            return true;
                        } else {
                            ngCtrl.$setValidity('accountEnabled', true);
                            ngCtrl.$setValidity('accountNotDeleted', true);
                            return true;
                        }
                    });
            }
        }

    }

})();