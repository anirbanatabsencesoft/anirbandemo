﻿(function() {
    'use strict';

    angular
        .module('App.Directives')
        .directive('employeeSearch', EmployeeSearch);

    EmployeeSearch.$inject = ['$window', '$q', 'employeeService'];
    
    function EmployeeSearch($window, $q, employeeService) {
        // Usage:
        //     <input employee-search />
        // Creates:
        // 
        var directive = {
            link: link,
            replace: true,
            template: '<input typeahead="item as item.label for item in searchEmployees($viewValue)" />'
        };     
        return directive;

        function link(scope, element, attrs) {
            var vm = scope;
            var appendEmployerInParentheses = false;
            if (attrs.appendEmployerInParentheses != undefined && attrs.appendEmployerInParentheses != null)
            {
                appendEmployerInParentheses = scope.$eval(attrs.appendEmployerInParentheses)
            }
            angular.extend(vm, {
                searchEmployees: searchEmployees,
                appendEmployerInParentheses: appendEmployerInParentheses
            });

            function searchEmployees(term) {
                var options = {
                    NameOrEmployeeNumber: term,
                    Include: ['Info', 'JobTitle']
                };
                return employeeService.List(options).$promise.then(function (data) {
                    data = data || {};
                    data.Results = data.Results || [];
                    return data.Results.map(function (item) {
                        item.Info = item.Info || {
                            Address: {}
                        };
                        return {
                            label: item.Name + ' - ' + item.EmployeeNumber + (scope.appendEmployerInParentheses? ' - (' + item.EmployerName + ')':''),
                            value: item
                        };
                    });
                });
            }
        }
    }

})();