﻿
(function () {
    'use strict';

    angular
      .module('App.Directives')
      .directive('customDirective', ValidateIntermittentDateDirective);

    ValidateIntermittentDateDirective.$inject = ['$window', '$filter', '$timeout'];
    function ValidateIntermittentDateDirective($window, $filter, $timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
            require: 'ngModel'
        };
        return directive;

        function link($scope, $element, $attrs, ctrl) {

            ctrl.$validators.validateIntermittentDate = validate;
            
            function validate(modelValue, viewValue) {
                var dateText = modelValue || viewValue;
                if (!dateText) {
                    return true;
                }

                var filterobj = { Status: "Approved" };
                var approvedPolicies = { PolicyCode: '', PolicyName: '', Adjudication: [] };
                var isDateInApprovedDateRange = false;
                $scope.IntermittentRequestViewModel.IsApprovedDisabled = true;

                angular.forEach($scope.IntermittentRequestViewModel.Details, function (detail, index) {
                    detail.IsApprovedDisabled = true;
                });

                //shows each policy in the UI
                $scope.SplitApprovalDecisionsToggle = false;
                //hide the group time inputs and Split button
                $scope.HideGroupTimeInputs = false;

                angular.forEach(policyData.AppliedPolicies, function (policy, index) {
                    var fmyData = $filter('filter')(policy.Adjudications, filterobj);
                    isDateInApprovedDateRange = false;
                    if (fmyData.length > 0) {
                        angular.forEach(fmyData, function (data, index) {

                            var startDate = new Date($filter('ISODateToDate')(data.StartDate));
                            var endDate = new Date($filter('ISODateToDate')(data.EndDate));

                            if (new Date(dateText) >= startDate && new Date(dateText) <= endDate) {
                                if (!isDateInApprovedDateRange) {
                                    isDateInApprovedDateRange = true
                                    $scope.IntermittentRequestViewModel.IsApprovedDisabled = false;

                                    angular.forEach($scope.IntermittentRequestViewModel.Details, function (detail, index) {
                                        if (detail.PolicyCode == policy.PolicyCode)
                                            detail.IsApprovedDisabled = false;
                                    });


                                    $timeout(function () {
                                        filterobj = { IsApprovedDisabled: false }
                                        var disabledFalse = $filter('filter')($scope.IntermittentRequestViewModel.Details, filterobj);

                                        filterobj = { IsApprovedDisabled: true }
                                        var disabledTrue = $filter('filter')($scope.IntermittentRequestViewModel.Details, filterobj);

                                        if (disabledFalse.length > 0 && disabledTrue.length > 0) {
                                            //shows each policy in the UI
                                            $scope.SplitApprovalDecisionsToggle = true;
                                            //hide the group time inputs and Split button
                                            $scope.HideGroupTimeInputs = true;
                                        }

                                    }, 200)


                                }
                            }
                        });
                    }
                });
            }
        }
    }
})();

