﻿(function () {
    'use strict';

    angular
        .module('App.Directives')
        .directive('monthDayPicker', MonthDayPicker);

    MonthDayPicker.$inject = ['Calendar'];

    function MonthDayPicker(Calendar) {
        // Usage:
        // Creates:
        // 
        var directive = {
            replace: true,
            restrict: 'EA',
            templateUrl: '/scripts/app-angular/directives/monthDayPicker/month-day-picker.html',
            scope: {
                month: '=month',
                dayOfMonth: '=dayOfMonth'
            },
            link: link
        };
        return directive;

        function link($scope) {
            var
                vm = $scope,
                parentvm = $scope.vm;

            activate();

            function activate() {
                angular.extend($scope, {
                    dateSelected: resolveBoundDate(),
                    months: Calendar.Default.Months
                });
                $scope.$watch('dateSelected', onDateSelected);
            }

            function resolveBoundDate() {
                if (vm.month && vm.dayOfMonth) {
                    return new Date(
                        new Date().getFullYear(),
                        vm.month - 1,
                        vm.dayOfMonth
                    );
                } else {
                    return null;
                }
            }

            function onDateSelected(newvalue, oldvalue) {
                if (newvalue) {
                    angular.extend(vm, {
                        month: vm.dateSelected.getMonth() + 1,
                        dayOfMonth: vm.dateSelected.getDate()
                    });

                } else if (oldvalue) {
                    angular.extend(parentvm, {
                        month: null,
                        dayOfMonth: null
                    });
                }
            }
        }
    }

})();