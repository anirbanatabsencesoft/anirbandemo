﻿(function () {
    'use strict';

    angular
        .module('App.Directives')
        .directive('monthPicker', MonthPicker);

    MonthPicker.$inject = ['$window'];

    function MonthPicker($window) {
        // Usage:
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            controller: MonthPickerController,
            replace: true,
            template:
                '<select ' +
                'ng-options="month.Value as month.Label for month in months"' +
                'title="Select a month"' +
                ' />'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

    MonthPickerController.$inject = ['$scope', 'Calendar'];
    function MonthPickerController($scope, Calendar) {
        angular.extend($scope, {
            months: Calendar.Default.Months
        });
    }

})();