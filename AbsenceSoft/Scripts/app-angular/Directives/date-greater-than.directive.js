﻿(function () {
    'use strict';

    angular
      .module('App.Directives')
      .directive('dateGreaterThan', DateGreaterThanDirective);

    DateGreaterThanDirective.$inject = [];
    function DateGreaterThanDirective() {
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel'
        };

        return directive;

        function link(scope, element, attrs, ctrl) {
            attrs.$observe('dateGreaterThan', onValueChanged);
            ctrl.$viewChangeListeners.push(onValueChanged);

            function onValueChanged() {
                ctrl.$setValidity('dateGreaterThan', validate());
            }

            function validate() {
                var value = ctrl.$modelValue || ctrl.$viewValue;
                if (!value) {
                    return true;
                }
                var otherdate = attrs.dateGreaterThan;
                if (!otherdate) {
                    return true;
                }

                //
                // HACK: Why are we binding to a string instead of a model value in the view??
                if (typeof value === "string") {
                    value = value.replace(/"/g, '');
                    value = new Date(value);
                }
                if (typeof otherdate === "string") {
                    otherdate = otherdate.replace(/"/g, '');
                    otherdate = new Date(otherdate);
                }

                console.trace({
                    validate: 'dateGreaterThan',
                    value: value,
                    otherdate: otherdate
                });

                return value >= otherdate;
            }
        }
    }
})();

