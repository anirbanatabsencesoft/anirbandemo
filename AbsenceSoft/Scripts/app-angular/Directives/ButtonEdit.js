﻿(function() {
    'use strict';

    angular
        .module('App.Directives')
        .directive('buttonEdit', ButtonEdit);

    ButtonEdit.$inject = ['$window'];
    
    function ButtonEdit ($window) {
        // Usage:
        //     <button-edit></button-edit>
        // Creates:
        // 
        var directive = {
            link: link,
            replace: true,
            template:
                '<button class="btn btn-inline btn-default" >' +
                '<i class="glyphicon glyphicon-pencil"></i>' +
                '</button>'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();