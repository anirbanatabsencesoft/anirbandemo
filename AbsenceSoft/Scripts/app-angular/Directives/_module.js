﻿(function () {
    'use strict';

    angular.module('App.Directives', [
        'App',
        'ui.bootstrap.typeahead',
        'ui.bootstrap.datepicker'
    ]);
})();
