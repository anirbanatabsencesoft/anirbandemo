﻿(function () {
    'use strict';

    angular
        .module('App')
        .directive('itemActions', ItemActions);

    ItemActions.$inject = ['$window'];

    function ItemActions($window) {
        // Usage:
        //     <div item-actions />
        // Creates:
        // 
        var directive = {
            link: link,
            replace: true,
            template:
                '<div>' +
                '<button button-edit ng-click="itemEditClick()"></button>' +
                '<button button-delete ng-click="itemDeleteClick()"></button>' +
                '</div>',
            controller: ItemActionsController,
            scope: {
                itemEditClick: '&',
                itemDeleteClick: '&'
            }
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

    ItemActionsController.$inject = ['$log', '$scope'];
    function ItemActionsController($log, $scope) {
        var vm = $scope;

        vm.itemEditClick = vm.itemEditClick ||
            function () {
                $log.warn('item-edit-click is not bound')
            };
        vm.itemDeleteClick = vm.itemDeleteclick ||
            function () {
                $log.warn('item-delete-click is not bound');
            }
    }

})();