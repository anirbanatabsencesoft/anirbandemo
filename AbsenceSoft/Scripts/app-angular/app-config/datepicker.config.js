﻿(function () {
    'use strict';

    angular
        .module('App')
        .config(datePickerConfig);

    datePickerConfig.$inject = ['datepickerConfig', 'datepickerPopupConfig'];
    function datePickerConfig(datepickerConfig, datepickerPopupConfig) {

        angular.extend(datepickerConfig, {
            showWeeks: false,
            startingDay: 0,
            showButtonBar: true,
        });

        angular.extend(datepickerPopupConfig, {
            showButtonBar: true,
            closeOnDateSelection: true,
            datepickerPopup: 'MM/dd/yyyy',
            datepickerAppendToBody: true
        });

    }

})();