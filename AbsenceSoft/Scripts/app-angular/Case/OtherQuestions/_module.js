﻿(function () {
    'use strict';
    angular
        .module('App.Case.OtherQuestions', [
            , 'ui.bootstrap'
            , 'ui.bootstrap.tpls'
            , 'App.Directives.ButtonGroup'
        ]);

})();