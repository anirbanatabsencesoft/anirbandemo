﻿(function () {
    'use strict';

    angular
        .module('App.Case.OtherQuestions')
        .directive('dynamicQuestions', DynamicQuestionsDirective);

    /*
     * Usage: 
     *  <div dynamic-questions 
     *      currentQuestionSet="currentQuestionSet" 
     * >
     *  </div>
     */


    DynamicQuestionsDirective.$inject = ['$log'];
    function DynamicQuestionsDirective($log) {
        return {
            link: link,
            templateUrl: '/scripts/app-angular/case/otherquestions/dynamic-questions.html',
            scope: {
                currentQuestionSet: '='
            }
        };

        function link(scope) {
            var vm = scope;

            activate();

            function activate() {
                angular.extend(vm, {
                    ui: {
                        answerChanged: answerChanged
                    }
                });

                evaulateRequired();
            }

            function evaulateRequired(thisQuestion) {
                forQuestionsAfter(thisQuestion, function (q) {
                    var isRequired = false;

                    if (typeof q.Required === 'function') {
                        var context = getQuestionContext(thisQuestion);
                        isRequired = q.Required(context);
                        $log.debug({ id: q.Id, fooresult: isRequired });
                    } else {
                        isRequired = q.Required;
                        $log.debug({ id: q.Id, propresult: isRequired });
                    }

                    angular.extend(q, {
                        ui: {
                            isRequired: isRequired,
                            isHidden: !isRequired
                        }
                    });
                });
            }


            function answerChanged(thisQuestion) {
                evaulateRequired(thisQuestion);
            }

            function getQuestionContext(thisQuestion) {
                var result = {
                    Questions: vm.currentQuestionSet.reduce(function (prev, curr) {
                        prev[curr.Id] = curr;
                        return prev;
                    }, {})
                };
                return result;
            }

            function forQuestionsAfter(thisQuestion, mutator) {
                var start = 0;
                if (thisQuestion) {
                    start = vm.currentQuestionSet.indexOf(thisQuestion) + 1;
                }
                for (var i = start; i < vm.currentQuestionSet.length; i++) {
                    var q = vm.currentQuestionSet[i];
                    mutator(q);
                }
            }
        }
    }


})();