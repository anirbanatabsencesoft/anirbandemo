﻿(function (undefined) {
    'use strict';

    angular
        .module('App.Case.OtherQuestions')
        .factory('DynamicQuestionsService', DynamicQuestionsService);

    DynamicQuestionsService.$inject = ['$interpolate'];
    function DynamicQuestionsService($interpolate) {
        var customAccommodationsReasons = ['INQ', 'SER', 'MED'];
        var customAccommodationTypes = ['TWA', 'LTA', 'COF', 'COFA', 'FFD', 'WPV', 'TA', 'DA', 'CONV', 'LEA'];
        var questions = [
            {
                Condition: function (context) {
                    return context.AbsenceReasonCode === 'SER';
                },
                QuestionSet: [
                    {
                        Id: 'MedicalDevices',
                        ReportLabel: 'Medical Devices',
                        Question: 'Describe medical devices and locations on body:',
                        HelpText: 'Examples: Right Knee Replacement, Steel Plate in Head',
                        Required: true
                    },
                    {
                        Id: 'Buildings',
                        ReportLabel: 'Buildings',
                        Question: 'Which buildings do you work in?',
                        Required: true
                    }
                ]
            },
            {
                Condition: function (context) {
                    return context.AbsenceReasonCode === 'MED';
                },
                Todos: [
                    {
                        Condition: function (context) {
                            var a = context.Questions.IsImpairingMedication.Answer;
                            return a === "No";
                        },
                        Template: 'New Case No Action Needed'
                    },
                    {
                        Condition: function (context) {
                            var a = context.Questions.IsImpairingMedication.Answer;
                            return a === "Yes" || a === "Sometimes";
                        },
                        Template: 'Work Risk Review Case'
                    }

                ],
                QuestionSet: [
                    {
                        Id: 'PrescribedMedication',
                        ReportLabel: 'Prescribed Medication',
                        Question: 'What is your prescribed medication?',
                        Required: true,
                        QuestionType: 'PickOneOfFew',
                        Answers: [
                            {
                                Caption: 'Medicinal Marijuana',
                                Value: 'MM'
                            },
                            {
                                Caption: 'Other',
                                Value: 'Other'
                            }
                        ]
                    },

                    {
                        Id: 'IsImpairingMedication',
                        Question: 'Are you using a prescription that causes impairment?',
                        ReportLabel: 'Impairing Medication',
                        Required: true,
                        QuestionType: 'PickOneOfFew',
                        Answers: [
                            {
                                Caption: 'Yes',
                                Value: 'Yes'
                            },
                            {
                                Caption: 'No',
                                Value: 'No'
                            },
                            {
                                Caption: "Don't know",
                                Value: "Don't know"
                            }
                        ]
                    },

                    {
                        Id: 'IsConsumedDuringWorkHours',
                        ReportLabel: 'Is Consumed During Work Hours?',
                        Question: 'Do you take the prescription during work hours?',
                        QuestionType: 'PickOneOfFew',
                        Answers: [
                            {
                                Caption: 'Yes',
                                Value: 'Yes'
                            },
                            {
                                Caption: 'No',
                                Value: 'No'
                            },
                            {
                                Caption: 'Sometimes',
                                Value: 'Sometimes'
                            }
                        ],
                        Required: function (context) {
                            var a = context.Questions.IsImpairingMedication.Answer;
                            return a === "Yes" || a === "Don't know";
                        }
                    },

                    {
                        Id: 'MedicationTerm',
                        ReportLabel: 'Medication Term',
                        Question: 'How long will you be using the prescription medication?',
                        QuestionType: 'PickOneOfFew',
                        Answers: [
                            {
                                Caption: 'Temp',
                                Value: 'Temp'
                            },
                            {
                                Caption: 'Long term',
                                Value: 'Long term'
                            },
                            {
                                Caption: 'As needed',
                                Value: 'As needed'
                            }
                        ],
                        Required: function (context) {
                            var a = context.Questions.IsConsumedDuringWorkHours.Answer;
                            return a === "Yes" || a === "Sometimes";
                        }
                    }
                ]
            }
        ];

        return {
            resolveQuestionSet: resolveQuestionSet,
            resolveTodos: resolveTodos,
            resolveUiFlags: resolveUiFlags
        };

        function resolveTodos(context) {
            context = context || {};
            var result = questions
                .filter(function (f) {
                    return f.Condition && f.Condition(context);
                })
                .reduce(function (curr, next) {
                    if (next.Todos) {
                        next.Todos
                            .filter(function (f) {
                                return f.Condition && f.Condition(context);
                            })
                            .forEach(function (item) {
                                curr.push({
                                    Title: item.Template
                                });
                            });

                        return curr;
                    }
                }, []);

            if ((!result || result.length === 0) && hasCustomReason(context)) {
                result = [
                    {
                        Title: "New {{AbsenceReasonName}} Case Created for Employee '{{EmployeeFirstName}} {{EmployeeLastName}}'"
                    }
                ];
            }
            result.forEach(function (item) {
                item.Title = $interpolate(item.Title)(context);
            });
            return result;
        }

        function hasCustomReason(context) {
            if (context.AbsenceReasonCode == 'ACCOMM' && context.AccommodationType != null && customAccommodationTypes.indexOf(context.AccommodationType.Code) > -1)
                return true;

            if (customAccommodationsReasons.indexOf(context.AbsenceReasonCode) > -1)
                return true;

            return false;
        }

        function resolveQuestionSet(context) {
            context = context || {};

            var result = questions.filter(function (f) {
                return f.Condition && f.Condition(context);
            });

            if (result.length === 0) {
                result = null;

            } else {

                result = result.filter(function (f) {
                    return !f.Condition || f.Condition(context);
                }).reduce(function (curr, next) {
                    next.QuestionSet.forEach(function (q) {
                        curr.push(q);
                    });
                    return curr;
                }, []);
            }

            if (result) {
                return angular.copy(result);
            }
            return result;
        }

        function resolveUiFlags(contextIn, contextOut) {

            switch (contextIn.AbsenceReasonCode) {
                case 'INQ':
                    contextOut.ShowSummarizeRequest = false;
                    break;
                case 'SER':
                case 'MED':
                    angular.extend(contextOut, {
                        IsAccommodation: true,
                        ShowIsWorkRelated: false,
                        AddHCP: false,
                        ShowAccommOptions: true,
                        ShowSummaryDurationOnly: true
                    });
                    break;
                default:
                    angular.extend(contextOut, {
                        ShowSummarizeRequest: true,
                        ShowSummaryDurationOnly: false,
                        AddHCP: true,
                        ShowAccommOptions: true
                    });
                    break;
            }

        }

    }

})();
