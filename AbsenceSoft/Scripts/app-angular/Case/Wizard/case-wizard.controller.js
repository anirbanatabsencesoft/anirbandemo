(function () {
    'use strict';

    angular
    .module('App.Case.Wizard')
    .controller("caseWizardCtrl", EmployeeCreateCaseController);

    EmployeeCreateCaseController.$inject = [
         '$log', '$scope', '$http', '$filter', '$resource', 'employeeService', 'caseService', 'lookupService', '$route', '$routeParams',
         'DynamicQuestionsService', 'userPermissions', '_'
    ];
    function EmployeeCreateCaseController($log, $scope, $http, $filter, $resource, employeeService, caseService, lookupService, $route, $routeParams,
            DynamicQuestionsService, userPermissions, _
        ) {
        var vm = $scope;

        activate();
        function activate() {
            angular.extend(vm, {
                //
                // properties
                Relation: null,
                StepsCompleted: true,
                ViewMockup: true,
                forms: {
                    ShowDetailsToEdit: false,
                    SelectedFamilyMember: {},
                    SelectedReason: null,
                    frmDynamicQuestions: null
                },
                ui: {
                    isDataLoading: true,
                    IsESSCaseReview: false,
                    NewHCP: false,
                    ShowAccomLeaveTypeMssg: false,
                    AddHCP: false,
                    ShowSummaryDurationOnly: false,
                    showCaseNumber: false,
                    showIsEmployeeOnLeave: false,
                    SelectedAbsenceReason: null,
                    SelectedAbsenceReasonCategory: null,
                    EmployeeCaseStartDate: null,
                    EmployeeCaseEndDate: null,

                    currentQuestionSet: null,
                    questionsById: null, // same as currentQuestionSet, except indexed by Id prop for easier UI checks
                    modalDismiss: modalDismiss,
                    print: print,
                    ShowOtherQuestions: false,
                    ShowSummarizeRequest: true,
                    IsCaseCreating: false
                }, 

                EmployeeRelationshipDOB: new Date().addMonths(-360),                
                ContactOptions: [
                { value: "Phone", label: "Phone" },
                {
                    value: "Email", label: "Email"
                }
                ],
                EmployeeCreateCaseModel: {},
                dateOptions: {
                    'starting-day': 0
                },
                PanelStatus: [],
                //
                // methods
                init: init,
                GetProviderContacts: GetProviderContacts,
                GoToStep: GoToStep,
                Step1Submit: Step1Submit,
                Step2Submit: Step2Submit,
                SetStartEndDate: SetStartEndDate,
                onClickAbsenceReason: onClickAbsenceReason,
                Step3Submit: Step3Submit,
                DenyNewContact: DenyNewContact,
                AllowNewContact: AllowNewContact,
                Step7Back: Step7Back,
                Step6Submit: Step6Submit,
                Step7Submit: Step7Submit,
                OnAccomTypeSelected: OnAccomTypeSelected,
                OnWorkRelatedClick: OnWorkRelatedClick,
                HCPContactChangeAccom: HCPContactChangeAccom,
                CreateCase: CreateCase,
                SetType: SetType,
                FilterReasons: FilterReasons,
                actions: {
                    Step8Submit: Step8Submit
                },
                GetRelativeStep: GetRelativeStep
            });

            activateWatches();
            initpanels();
        }

        function GetRelativeStep(step) {
            if (vm.ui.ShowSummarizeRequest) {
                return step;
            }
            else
                return step - 1;
        }

        function formatDate(date) {
            return $filter('date')(date, 'MM/dd/yyyy');
        }

        function initpanels() {
            angular.element(document).ready(function () {

                if (vm.IsAccommodation) {
                    for (var i = 0; i < 8; i++) {
                        if (i == 0) {
                            vm.PanelStatus.push(false);
                        }
                        else {
                            vm.PanelStatus.push(undefined);
                        }
                    }
                }
                else {
                    for (var i = 0; i < 7; i++) {
                        if (!vm.ui.IsESSCaseReview) {
                            if (i == 0) {
                                vm.PanelStatus.push(false);
                            }
                            else {
                                vm.PanelStatus.push(undefined);
                            }
                        }
                        else if (vm.ui.IsESSCaseReview) {
                            if (i == 6) {
                                vm.PanelStatus.push(false);
                            }
                            else {
                                vm.PanelStatus.push(undefined);
                            }
                            $("#Step" + 7).removeClass("hide");
                            $("#Step" + 1).addClass("hide");
                        }
                    }
                }
            });

        }


        function activateWatches() {
            //
            // Only show 'Is Employee on Leave' if 'Is the employee at work?' = "No"
            $scope.$watch('EmployeeCreateCaseModel.IsEmployeeAtWork', function (newvalue, oldvalue) {
                if (newvalue === false) {
                    vm.ui.showIsEmployeeOnLeave = true;
                } else {
                    vm.ui.showIsEmployeeOnLeave = false;
                    vm.EmployeeCreateCaseModel.IsEmployeeOnLeave = null;

                }
            });
            //
            // Show Case Number if EmployeeCreateCaseModel.IsEmployeeOnLeave = "Yes"
            $scope.$watch('EmployeeCreateCaseModel.IsEmployeeOnLeave', function (newvalue, oldvalue) {
                if (newvalue === true) {
                    vm.ui.showCaseNumber = true;
                } else {
                    vm.ui.showCaseNumber = false;
                    vm.EmployeeCreateCaseModel.EmpOtherLeaveCaseNumber = null;
                }
            });

            $scope.$watch('ui.SelectedAbsenceReason.Code', function (newvalue, oldvalue) {
                if (!newvalue) {
                    vm.ui.currentQuestionSet = null;
                    return;
                }

                DynamicQuestionsService.resolveUiFlags(getDynamicQuestionContext(), vm.ui);


                //switch (newvalue) {
                //    case 'INQ':
                //        vm.ShowSummarizeRequest = false;
                //        break;
                //    case 'SER':
                //    case 'MED':
                //        vm.ShowIsWorkRelated = false;
                //        vm.ui.ShowSummaryDurationOnly = true;
                //        vm.ui.AddHCP = false;
                //        break;
                //    default:
                //        vm.ShowSummarizeRequest = true;
                //        vm.ui.ShowSummaryDurationOnly = false;
                //        vm.ui.AddHCP = true;
                //        break;
                //}

                vm.ui.currentQuestionSet = DynamicQuestionsService.resolveQuestionSet(getDynamicQuestionContext());

                if (vm.ui.currentQuestionSet) {
                    vm.ui.questionsById = vm.ui.currentQuestionSet.reduce(function (prev, curr) {
                        prev[curr.Id] = curr;
                        return prev;
                    }, {});
                } else {
                    vm.ui.questionsById = null;
                }
            });
        }

        function getDynamicQuestionContext() {
            return {
                AbsenceReasonCode: vm.ui.SelectedAbsenceReason ? vm.ui.SelectedAbsenceReason.Code : null,
                AbsenceReasonName: vm.ui.SelectedAbsenceReason ? vm.ui.SelectedAbsenceReason.Name : null,
                AccommodationType: vm.ui.AccommodationRequest.Type ? vm.ui.AccommodationRequest.Type : null,
                EmployeeFirstName: vm.EmployeeInfo ? vm.EmployeeInfo.FirstName : null,
                EmployeeLastName: vm.EmployeeInfo ? vm.EmployeeInfo.LastName : null,
                Questions: vm.ui.currentQuestionSet ? 
                    vm.ui.currentQuestionSet.reduce(function (prev, curr) {
                        prev[curr.Id] = curr;
                        return prev;
                    }, {})  :
                    {}
                };
        }

        function init(employeeId, caseId, isESSCaseReview, toDoItemId, isAccommodation) {
            if (!$routeParams.eId) {
                vm.EmployeeId = employeeId;
            }
            else {
                vm.EmployeeId = $routeParams.eId;
            }

            if ($routeParams.isInquiry == 'true') {
                vm.EmployeeCreateCaseModel.Status = 'Inquiry';
            //    vm.SelectedAbsenceReason.Code = 'INQ';
            }
            
            vm.EmployeeCreateCaseModel.EmployeeId = vm.EmployeeId;
            vm.EmployeeCreateCaseModel.AccommodationRequest = {
                type: null
            };

            caseService.GetEmployeeFeatureInfo(vm.EmployeeId).then(function (data) {
                //Employer feature level flags that affect the UI
                // vm.NoFeaturesEnabled = data.NoFeaturesEnabled;
                vm.STDFeatureEnabled = data.STDFeatureEnabled;
                vm.ADAFeatureEnabled = data.ADAFeatureEnabled;
                vm.LOAFeatureEnabled = data.LOAFeatureEnabled;

                vm.EmployeeCreateCaseModel.STDFeatureEnabled = data.STDFeatureEnabled;
                vm.EmployeeCreateCaseModel.ADAFeatureEnabled = data.ADAFeatureEnabled;
                // vm.EmployeeCreateCaseModel.NoFeaturesEnabled = data.NoFeaturesEnabled;
                vm.EmployeeCreateCaseModel.LOAFeatureEnabled = data.LOAFeatureEnabled;

                // flag to show hide custom fields
                vm.ui.ShowIsWorkRelated = data.ShowIsWorkRelated;
                vm.ui.ShowContactSection = data.ShowContactSection;
                vm.ui.ShowOtherQuestions = data.ShowOtherQuestions;

                vm.EmployeeCreateCaseModel.ShowIsWorkRelated = data.ShowIsWorkRelated;
                vm.EmployeeCreateCaseModel.ShowContactSection = data.ShowContactSection;
                vm.EmployeeCreateCaseModel.ShowOtherQuestions = data.ShowOtherQuestions;
            });

            GetProviderContacts();

            vm.CaseId = caseId;
            vm.ui.IsESSCaseReview = isESSCaseReview == 'True' ? true : false;
            vm.IsAccommodation = isAccommodation == 'True' ? true : false;
            vm.ToDoItemId = toDoItemId;

            employeeService.GetEmployeeInfo(vm.EmployeeId, null).then(function (response) {
                vm.EmployeeInfo = response;

                vm.EmployeeCreateCaseModel.NewAddress = (response.Address.Address1 != null ? response.Address.Address1 : "") + " " + (response.Address.Address2 != null ? response.Address.Address2 : "");
                vm.EmployeeCreateCaseModel.NewCity = response.Address.City;
                vm.EmployeeCreateCaseModel.NewState = response.Address.State;
                vm.EmployeeCreateCaseModel.NewZipcode = response.Address.PostalCode;

                vm.EmployeeCreateCaseModel.NewAltAddress = (response.AltAddress.Address1 != null ? response.AltAddress.Address1 : "") + " " + (response.Address.Address2 != null ? response.Address.Address2 : "");
                vm.EmployeeCreateCaseModel.NewAltCity = response.AltAddress.City;
                vm.EmployeeCreateCaseModel.NewAltState = response.AltAddress.State;
                vm.EmployeeCreateCaseModel.NewAltZipcode = response.AltAddress.PostalCode;
                vm.EmployeeCreateCaseModel.NewAltEmail = response.AltEmail;

                vm.EmployeeCreateCaseModel.NewWorkPhone = response.WorkPhone != null ? $filter('tel')(response.WorkPhone) : "";
                vm.EmployeeCreateCaseModel.NewEmail = response.Email;
                vm.EmployeeCreateCaseModel.NewHomePhone = response.HomePhone != null ? $filter('tel')(response.HomePhone) : "";
                vm.EmployeeCreateCaseModel.NewCellPhone = response.CellPhone != null ? $filter('tel')(response.CellPhone) : "";
                vm.EmployeeCreateCaseModel.NewAltPhone = response.AltPhone != null ? $filter('tel')(response.AltPhone) : "";

                if (response.Address.Country == "US") {
                    vm.ShowAddressStateTextBox = false;
                    vm.ShowAddressStateDropDown = true;
                }
                else {
                    vm.ShowAddressStateTextBox = true;
                    vm.ShowAddressStateDropDown = false;
                }

                if (response.AltAddress.Country == "US") {
                    vm.ShowAltAddressStateTextBox = false;
                    vm.ShowAltAddressStateDropDown = true;
                }
                else {
                    vm.ShowAltAddressStateTextBox = true;
                    vm.ShowAltAddressStateDropDown = false;
                }

                vm.EmployeeRelationships = caseService.EmployeeContactTypes("FHC");
                vm.AbsenceReasons = null;
                lookupService.AbsenceReasonsPromise(vm.EmployeeId).then(function (reasons) {
                    vm.AbsenceReasons = reasons;
                    GetEmployeeCaseForReview();
                });

                $scope.$watch('EmployeeCreateCaseModel.NewZipCode', function (newValue, oldValue) {
                    if (newValue == oldValue)
                        return;

                    ZipcodeValidation();
                });

                $scope.$watch('EmployeeCreateCaseModel.NewAltZipCode', function (newValue, oldValue) {
                    if (newValue == oldValue)
                        return;

                    AltZipcodeValidation();
                });

                lookupService.AccommodationTypes(vm.EmployeeInfo.EmployerId, vm.EmployeeCreateCaseModel.CaseTypeId).$promise.then(function (data) {
                    vm.AccommodationTypes = data;
                })
                vm.EmployeeCreateCaseModel.IsContactInformationCorrect = true;
                vm.EmployeeCreateCaseModel.Job = response.Job;                
                vm.EmployeeCreateCaseModel.JobHistory = (response.JobHistory == undefined || response.JobHistory == null ? null : response.JobHistory);
                vm.EmployeeCreateCaseModel.ShowJobHistory = false;
                if (vm.EmployeeCreateCaseModel.Job == undefined || vm.EmployeeCreateCaseModel.Job == null) {
                    vm.EmployeeCreateCaseModel.Job = new Object();
                    vm.EmployeeCreateCaseModel.Job.Code = '';
                }
                if (vm.EmployeeCreateCaseModel.Job.StartDate != undefined && vm.EmployeeCreateCaseModel.Job.StartDate != null) {
                    vm.EmployeeCreateCaseModel.Job.StartDate = $filter('date')(new Date(vm.EmployeeCreateCaseModel.Job.StartDate), 'MM/dd/yyyy');
                }
                if (vm.EmployeeCreateCaseModel.Job.EndDate != undefined && vm.EmployeeCreateCaseModel.Job.EndDate != null) {
                    vm.EmployeeCreateCaseModel.Job.EndDate = $filter('date')(new Date(vm.EmployeeCreateCaseModel.Job.EndDate), 'MM/dd/yyyy');
                }
                if (vm.EmployeeCreateCaseModel.Job.Status == undefined || vm.EmployeeCreateCaseModel.Job.Status == null) {
                    vm.EmployeeCreateCaseModel.Job.Status = new Object();
                }
                if (vm.EmployeeCreateCaseModel.Job.Status.DenialReason == undefined || vm.EmployeeCreateCaseModel.Job.Status.DenialReason == null){
                    vm.EmployeeCreateCaseModel.Job.Status.DenialReason = '';                    
                }                
                // 
            }).finally(function () {
                vm.ui.isDataLoading = false;
            });

            employeeService.EmployeeExistingRelationships(vm.EmployeeId, "FHC").then(function (response) {
                vm.EmployeeExistingRelationships = response;
                vm.EmployeeExistingRelationships.push({
                    Id: 0,
                    Text: null,
                    FirstName: "",
                    LastName: "",
                    RelationShipId: null,
                    DOB: new Date().addMonths(-360)

                });

                if (vm.EmployeeExistingRelationships.length > 0) {
                    SelectRelative(vm.EmployeeExistingRelationships[0]);
                }
            });

            if (vm.ui.IsESSCaseReview) {
                vm.CaseCancelReason = lookupService.CaseCancelReason();
            }
            lookupService.GetJobDenialReasons().then(function (data) {
                vm.EmployeeCreateCaseModel.DenialReasons = data;
            });
            caseService.CaseJobs(vm.EmployeeId).then(function (data) {
                vm.EmployeeCreateCaseModel.Jobs = data;
            })
        }


        function GetEmployeeCaseForReview() {
            // if value is true then its ess case review mode
            if (Boolean(vm.ui.IsESSCaseReview)) {
                if (vm.CaseId != null && vm.CaseId != undefined && vm.CaseId != "") {
                    vm.ui.IsESSCaseReview = true;
                    caseService.GetESSCaseForReview(vm.CaseId).then(function (data) {
                        if (data.Success) {

                            if (data.CaseInfo.Status != 3) {
                                alert("This case is already been reviewed.");
                                window.location.href = "/";
                            }


                            vm.EmployeeCreateCaseModel = data.CaseInfo;

                            vm.ui.EmployeeCaseStartDate = vm.EmployeeCreateCaseModel.StartDate;
                            vm.ui.EmployeeCaseEndDate = vm.EmployeeCreateCaseModel.EndDate;
                            vm.IsAccommodation = vm.EmployeeCreateCaseModel.IsAccommodation;

                            if (vm.AbsenceReasons != null) {
                                angular.forEach(vm.AbsenceReasons, function (reason, index) {
                                    if (reason.IsCategory) {
                                        var selReason = $filter('filter')(reason.Children, { Id: vm.EmployeeCreateCaseModel.AbsenceReasonId });
                                        if (selReason.length > 0) {
                                            vm.ui.SelectedAbsenceReason = selReason[0];
                                            vm.selectedReasonChild = selReason[0].Id;
                                            vm.ui.SelectedAbsenceReasonCategory = reason;
                                            $("#" + reason.Category + "AbsenceReasonCategoryDiv").show();
                                            vm.IsRequired = true;
                                        }
                                    }
                                    else {
                                        if (reason.Id == vm.EmployeeCreateCaseModel.AbsenceReasonId) {
                                            vm.ui.SelectedAbsenceReason = reason;
                                            vm.ui.SelectedAbsenceReasonCategory = null;
                                            vm.IsRequired = false;
                                        }
                                    }
                                });

                                if (!vm.EmployeeCreateCaseModel.IsWorkScheduleCorrect) {
                                    vm.ShowScheduleWarning = true;
                                }

                                if (!vm.EmployeeCreateCaseModel.IsContactInformationCorrect) {
                                    vm.forms.ShowDetailsToEdit = true;
                                }

                            }


                            for (var i = 1; i < 6; i++) {

                                $("#Step" + (i + 1)).css("display", "block");

                                $("#collapse" + (i + 1)).collapse('show');
                            }
                        }
                    });
                }
            }
        }


        function ZipcodeValidation() {
            if (vm.EmployeeCreateCaseModel.NewZipCode == null || vm.EmployeeCreateCaseModel.NewZipCode == '' || vm.EmployeeCreateCaseModel.NewZipCode == undefined)
                return;

            //if (vm.Address.Country == null || vm.Address.Country == '' || vm.Address.Country == undefined)
            //    return;

            if (vm.EmployeeInfo.Address.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test(vm.EmployeeCreateCaseModel.NewZipCode)) {
                    vm.forms.frmStep1.PostalCode.$setValidity('valid', false);
                    return;
                }
            }

            vm.forms.frmStep1.PostalCode.$setValidity('valid', true);
        }

        function AltZipcodeValidation() {
            if (vm.EmployeeCreateCaseModel.NewAltZipCode == null || vm.EmployeeCreateCaseModel.NewAltZipCode == '' || vm.EmployeeCreateCaseModel.NewAltZipCode == undefined)
                return;

            if (vm.EmployeeInfo.AltAddress.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test(vm.EmployeeCreateCaseModel.NewAltZipCode)) {
                    vm.forms.frmStep1.AltPostalCode.$setValidity('valid', false);
                    return;
                }
            }

            vm.forms.frmStep1.AltPostalCode.$setValidity('valid', true);
        }

        function GoToNextStep(currStep, nextStep) {
            $log.debug('GoToNextStep', { currStep: currStep, nextStep: nextStep });
            $("#Step" + nextStep).removeClass("hide");
            $("#Step" + currStep).addClass("hide");
            vm.CurrentStep = nextStep;
            vm.PanelStatus[currStep - 1] = true;
            vm.PanelStatus[nextStep - 1] = false;
            vm.ui.IsCaseCreating = false;
        }

        function GoToStep(step) {
            $("#Step" + vm.CurrentStep).addClass('hide');
            $("#Step" + step).removeClass('hide');

            vm.CurrentStep = step;

            vm.PanelStatus[step - 1] = false;

            for (var i = 0; i < step - 1; i++) {
                vm.PanelStatus[i] = true;
            }

            HideNextSteps(step + 1);
        }

        function HideNextSteps(nextStepIndex) {

            if (vm.IsAccommodation) {
                for (var i = nextStepIndex; i <= 8; i++) {
                    vm.PanelStatus[i - 1] = undefined;
                }
            }
            else {
                for (var i = nextStepIndex; i <= 7; i++) {
                    vm.PanelStatus[i - 1] = undefined;
                }
            }

        }

        //#region "Steps submit and validation
        function Step1Submit() {
            // Check any option from "Yes" or "No" has been selected
            if (vm.EmployeeCreateCaseModel.IsContactInformationCorrect == null || vm.EmployeeCreateCaseModel.IsContactInformationCorrect == undefined) {
                vm.ValidateInfoError = true;
            }
            else {
                vm.ValidateInfoError = false;
            }

            if (!vm.ValidateInfoError) {
                vm.ValidateInfoError = vm.forms.frmStep1.$invalid || (vm.forms.frmContactMethod && vm.forms.frmContactMethod.$invalid)
            }

            if (!vm.ValidateInfoError) {
                vm.EmployeeCreateCaseModel.CaseTypeId = 8;
                GoToNextStep(1, 2);
            } else {
                vm.forms.ShowDetailsToEdit = true;
            }
            vm.Step1Submitted = true;
        }

        function SubmitNewContact(isValid) {
            if (isValid) {
                vm.forms.ShowDetailsToEdit = false;
                vm.forms.ShowNewContactWarning = true;
            }
        }

        function Step2Submit() {
            if (!vm.IsAccommodation)
                GoToNextStep(2, 3);

            if (vm.ui.SelectedAbsenceReason) {
                GoToNextStep(2, 3);
            }

        }

        function SetStartEndDate(duration) {
            $log.debug('SetStartEndDate', {
                EmployeeCaseStartDate: vm.ui.EmployeeCaseStartDate,
                EmployeeCaseEndDate: vm.ui.EmployeeCaseEndDate
            });
            if (duration == 1) {
                vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate = formatDate( vm.ui.EmployeeCaseStartDate );
                vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate = formatDate( vm.ui.EmployeeCaseEndDate );
            }
            else if (duration == 2) {
                vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate = formatDate(vm.ui.EmployeeCaseStartDate);
                vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate = null;
            }
        }

        function Step3Submit() {
            if (vm.forms.frmStep3.$valid) {
                if (vm.IsAccommodation) {
                    vm.EmployeeCreateCaseModel.LeaveFor = 'Self';
                    vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate = vm.ui.EmployeeCaseStartDate;
                    vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate = vm.ui.EmployeeCaseEndDate;
                    /// This is dumb.  We've already selected the reason, but they've got some bs about doing this, so we set it to administrative
                    /// then we go ahead and set it back to the actual reason
                    var actualReason = vm.ui.SelectedAbsenceReason;
                    var adminReason = vm.AbsenceReasons.filter(function (f) {
                        return f.Name === 'Administrative';
                    });
                    if (!adminReason) {
                        throw 'Administrative Absence Reason not found';
                    }
                    adminReason = adminReason[0];
                    onClickAbsenceReason(adminReason, false);
                    onClickAbsenceReason(actualReason);
                    GoToNextStep(3, 4);
                    GoToNextStep(5, 6);
                    if (vm.ui.SelectedAbsenceReason.Code == 'INQ')
                        GoToNextStep(6, 7);
                }
                else
                    GoToNextStep(3, 4);
            }
        }

        function Step4Submit() {
            if (vm.forms.frmStep4.$valid) {

                if (vm.EmployeeCreateCaseModel.LeaveFor == "Family") {
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipId = vm.forms.SelectedFamilyMember.RelationShipId;
                    vm.EmployeeCreateCaseModel.EmployeeContactId = vm.forms.SelectedFamilyMember.Id;
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = vm.forms.SelectedFamilyMember.FirstName;
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipLastName = vm.forms.SelectedFamilyMember.LastName;
                    if (vm.forms.SelectedFamilyMember.DOB != null && vm.forms.SelectedFamilyMember.DOB != undefined && vm.forms.SelectedFamilyMember.DOB != '')
                        vm.EmployeeCreateCaseModel.EmployeeRelationshipDOB = vm.forms.SelectedFamilyMember.DOB;
                }
                else {
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipId = null;
                    vm.EmployeeCreateCaseModel.EmployeeContactId = null;
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = null;
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipLastName = null;
                    vm.EmployeeCreateCaseModel.EmployeeRelationshipDOB = null;
                }

                GoToNextStep(4, 5);
            }
        }

        function SetRelationship(relationshipId) {
            var selectedRelationship = $filter('filter')(vm.EmployeeRelationships, { Id: relationshipId });
            if (selectedRelationship.length > 0) {
                vm.forms.SelectedFamilyMember.RelationShipId = relationshipId;
                vm.forms.SelectedFamilyMember.Text = selectedRelationship[0].Text;
            }
        }

        function SelectRelative(relative) {
            vm.forms.SelectedFamilyMember = relative;
            vm.forms.SelectedFamilyMemberId = relative.Id;
        }

        function Step5Submit() {
            if (vm.forms.frmStep5.$valid) {
                if (vm.ui.ShowSummarizeRequest) {
                    GoToNextStep(5, 6);
                } else {
                    GoToNextStep(5, 7);
                }
            }
        }

        function onClickAbsenceReason(absenceReason, scroll) {
            if (absenceReason.IsCategory) {
                vm.ui.SelectedAbsenceReason = null;
                vm.ui.SelectedAbsenceReasonCategory = absenceReason;
                vm.IsRequired = true;
            }
            else if (absenceReason.IsChild) {
                vm.ui.SelectedAbsenceReason = absenceReason;
                vm.selectedReasonChild = absenceReason.Id;
            }
            else {
                vm.ui.SelectedAbsenceReason = absenceReason;
                vm.ui.SelectedAbsenceReasonCategory = null;
            }

            if (scroll == undefined || scroll == true)
                $("html, body").animate({ scrollTop: $(document).height() - 500 }, 1000);

        };

        function EnableRel(Rel) {
            vm.Relation = Rel;
            if (Rel == "NewRel") {
                // vm.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', false);
                vm.showTextBox = true;
            }
            else if (Rel == "ExistingRel") {
                if (vm.DeniedNewDuplicateRelationship == true) {
                    vm.EmployeeNewRelationship = null;
                    vm.EmployeeRelationshipFirstName = null;
                    vm.EmployeeRelationshipLastName = null;
                    vm.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
                }
                vm.frmCreateCase.contFirstname.$setValidity('duplicateEmployeeContact', true);
                vm.showTextBox = false;
            }
            else {
                vm.showTextBox = false;
            }
        };

        function AllowNewContact() {
            vm.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
            vm.DeniedNewDuplicateRelationship = false;
            $("#ContactDuplication").modal('hide');
        }

        function DenyNewContact() {
            vm.DeniedNewDuplicateRelationship = true;
            $("#ContactDuplication").modal('hide');
        }

        function Step6Submit() {
            if (vm.forms.frmStep6.$valid) {
                GoToNextStep(5, 6);
            }
        }

        function Step7Back() {
            if (vm.ui.ShowSummarizeRequest) {
                GoToStep(6);
            } else {
                GoToStep(3);
            }
        }

        function Step7Submit() {

            if (vm.IsAccommodation) {
                vm.ValidateScheduleError = vm.forms.frmStep7.$invalid; vm.EmployeeCreateCaseModel.IsWorkScheduleCorrect = true; vm.ShowScheduleWarning = false;
            }
            else if (vm.EmployeeCreateCaseModel.IsWorkScheduleCorrect == null || vm.EmployeeCreateCaseModel.IsWorkScheduleCorrect == undefined) {
                vm.ValidateScheduleError = true;
            }
            else {
                vm.ValidateScheduleError = false;
            }

            if (!vm.ValidateScheduleError) {
                if (vm.ui.EmployeeCaseStartDate) {
                    vm.EmployeeCreateCaseModel.StartDate = vm.ui.EmployeeCaseStartDate;
                }
                else if (vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate) {
                    vm.EmployeeCreateCaseModel.StartDate = vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate;
                }
                if (vm.ui.EmployeeCaseEndDate) {
                    vm.EmployeeCreateCaseModel.EndDate = vm.ui.EmployeeCaseEndDate;
                }
                else if (vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate) {
                    vm.EmployeeCreateCaseModel.EndDate = vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate;
                }
                else if (!vm.ui.EmployeeCaseEndDate && vm.EmployeeCreateCaseModel.AccommodationRequest.Duration == 1) {
                    vm.EmployeeCreateCaseModel.EndDate = vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate;
                    vm.ui.EmployeeCaseEndDate = vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate;
                }
                else {
                    vm.EmployeeCreateCaseModel.EndDate = null;
                }

                if (vm.IsAccommodation) {
                    if (vm.ui.ShowOtherQuestions)
                        GoToNextStep(6, 7);
                    else
                        GoToNextStep(6, 8);
                }
                else {
                    GoToNextStep(6, 7);
                }

            }
            if ($('#Job_StartDate') != undefined && $('#Job_StartDate') != null) {
                $('#Job_StartDate').datepicker({ autoclose: true });
                $('#Job_StartDate').next().children().on('click', function () {
                    $('#Job_StartDate').datepicker('show');
                });
            }
            if ($('#Job_EndDate') != undefined && $('#Job_EndDate') != null) {
                $('#Job_EndDate').datepicker({ autoclose: true });
                $('#Job_EndDate').next().children().on('click', function () {
                    $('#Job_EndDate').datepicker('show');
                });
            };
        }

        function Step8Submit() {
            if (vm.forms.frmStep8.$valid) {
                GoToNextStep(7, 8);
            }
        }

        function SetAbsenceReasonParameters() {
            var selReasonCode = vm.ui.SelectedAbsenceReason.Code;
            switch (selReasonCode) {
                case "EHC":
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "FHC":
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "PREGMAT":
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.ShortDescription = "";
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "ADOPT":
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.ShortDescription = "";
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    break;
                case "MILITARY":
                case "EXIGENCY":
                case "RESERVETRAIN":
                case "CEREMONY":
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.ShortDescription = "";
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;

                    //vm.EmployeeCreateCaseModel.MilitaryStatusId = vm.MilitaryStatus != null ? vm.MilitaryStatus.Id : null;
                    break;

                case "PARENTAL":
                case "DOM":
                case "ORGAN":
                case "BONE":
                case "BLOOD":
                case "CRIME":
                case "BEREAVEMENT":
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.ShortDescription = "";
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;

                case "ACCOMM":
                    vm.EmployeeCreateCaseModel.IsAccommodation = true;
                    vm.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    vm.EmployeeCreateCaseModel.IsWorkRelated = null;
                    vm.EmployeeCreateCaseModel.ShortDescription = "";
                    vm.EmployeeCreateCaseModel.WillUseBonding = null;
                    vm.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    vm.EmployeeCreateCaseModel.BondingStartDate = null;
                    vm.EmployeeCreateCaseModel.BondingEndDate = null;
                    vm.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
            }
        }

        function OnAccomTypeSelected() {
            var IsWorkRelated = vm.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated;
            if (IsWorkRelated != null && IsWorkRelated != undefined && IsWorkRelated != '') {
                if (IsWorkRelated) {
                    if (vm.EmployeeCreateCaseModel.AccommodationRequest.Type.Code == "COF") {
                        vm.isDisabled = true;
                        $("#CaseWarning").modal('show');
                    }
                    else {
                        vm.isDisabled = false;
                    }
                }
                else {
                    vm.isDisabled = false;
                }
            }
        }

        function OnWorkRelatedClick(IsWorkRelated) {
            vm.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated = IsWorkRelated;
            // KRC: What is this? #CaseWarning doesn't exist and vm.EmployeeCreateCaseModel.AccommodationRequest.Type also
            // does not exist.  Removing for now
            //if (IsWorkRelated) {
            //    if (vm.EmployeeCreateCaseModel.AccommodationRequest.Type.Code == "COF") {
            //        vm.isDisabled = true;
            //        $("#CaseWarning").modal('show');
            //    }
            //    else {
            //        vm.isDisabled = false;
            //    }
            //}
            //else {
            //    vm.isDisabled = false;
            //}
        };

        function GetProviderContacts() {
            vm.ProviderContacts = [];
            $http({
                method: 'GET',
                url: "/Employees/" + vm.EmployeeId + "/Contacts/Medical" //provider
            }).then(function (response) {
                vm.ProviderContacts = response.data;
                var newContact = {};
                newContact.ContactId = '-1';
                newContact.Name = 'New Provider';
                vm.ProviderContacts.push(newContact);
            });
        }

        function HCPContactChangeAccom(contactId) {
            if (contactId == null || contactId == undefined || contactId == '') {
                vm.ui.NewHCP = false;
            }
            else {
                vm.ui.NewHCP = true;
                var contact = $filter('filter')(vm.ProviderContacts, { ContactId: contactId });
                if (contact.length > 0) {
                    vm.EmployeeCreateCaseModel.AccommodationRequest.HCPFirstName = contact[0].FirstName;
                    vm.EmployeeCreateCaseModel.AccommodationRequest.HCPLastName = contact[0].LastName;
                    vm.EmployeeCreateCaseModel.AccommodationRequest.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                    vm.EmployeeCreateCaseModel.AccommodationRequest.HCPFax = $filter('tel')(contact[0].Fax);
                }
                $("html, body").animate({ scrollTop: 600 }, 1000);
            }
        }


        function CreateCase() {
            if (vm.ui.IsCaseCreating) return;
            vm.ui.IsCaseCreating = true;
            // if leave is for "Self" then set relationship params to null
            if (vm.EmployeeCreateCaseModel.LeaveFor == "Self") {
                vm.EmployeeCreateCaseModel.EmployeeRelationshipId = null;
                vm.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = null;
                vm.EmployeeCreateCaseModel.EmployeeRelationshipLastName = null;
                vm.EmployeeCreateCaseModel.EmployeeContactId = null;
            }
            else {
                if (vm.EmployeeCreateCaseModel.EmployeeContactId == 0)
                    vm.EmployeeCreateCaseModel.EmployeeContactId = null;
            }

            // Set selected absencereason id
            vm.EmployeeCreateCaseModel.AbsenceReasonId = vm.ui.SelectedAbsenceReason != null ? vm.ui.SelectedAbsenceReason.Id : null;
            // Set absence reason property depending on the selected absence reason 
            SetAbsenceReasonParameters();

            // Case Start date / End Date
            vm.EmployeeCreateCaseModel.StartDate = vm.ui.EmployeeCaseStartDate;
            if (vm.ui.EmployeeCaseEndDate) {
                vm.EmployeeCreateCaseModel.EndDate = vm.ui.EmployeeCaseEndDate;
            }
            else if (!vm.EmpCaseEndDate && vm.EmployeeCreateCaseModel.AccommodationRequest.Duration == 1) {
                vm.EmployeeCreateCaseModel.EndDate = vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate;
            }
            else {
                vm.EmployeeCreateCaseModel.EndDate = null;
            }

            // remove formatting for telephone number
            vm.EmployeeCreateCaseModel.NewWorkPhone = vm.EmployeeCreateCaseModel.NewWorkPhone.replace(/[^0-9]/g, '');
            vm.EmployeeCreateCaseModel.NewHomePhone = vm.EmployeeCreateCaseModel.NewHomePhone.replace(/[^0-9]/g, '');
            vm.EmployeeCreateCaseModel.NewCellPhone = vm.EmployeeCreateCaseModel.NewCellPhone.replace(/[^0-9]/g, '');


            //accomm
            var accomm = {};

            var reqStartDt = null;
            if (vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate != undefined &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate != null &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate != '') {
                reqStartDt = new Date(vm.EmployeeCreateCaseModel.AccommodationRequest.StartDate);
                if (reqStartDt == "Invalid Date") {
                    reqStartDt = null;
                };
            }
            accomm.StartDate = reqStartDt;

            var reqEndDt = null;
            if (vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate != undefined &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate != null &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate != '') {
                reqEndDt = new Date(vm.EmployeeCreateCaseModel.AccommodationRequest.EndDate);
                if (reqEndDt == "Invalid Date") {
                    reqEndDt = null;
                };
            }
            accomm.EndDate = reqEndDt;


            if (vm.EmployeeCreateCaseModel.AccommodationRequest.Type != undefined &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.Type != null &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.Type != '') {
                accomm.Type = vm.EmployeeCreateCaseModel.AccommodationRequest.Type;
            }


            if (vm.EmployeeCreateCaseModel.AccommodationRequest.Duration != undefined &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.Duration != null &&
                vm.EmployeeCreateCaseModel.AccommodationRequest.Duration != '') {
                accomm.Duration = vm.EmployeeCreateCaseModel.AccommodationRequest.Duration;
            }

            if (vm.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated != undefined &&
                            vm.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated != null) {
                accomm.IsWorkRelated = vm.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated;
            }

            //accommodation description = case short description
            accomm.Description = vm.EmployeeCreateCaseModel.AccommodationRequest.OtherDescription;

            vm.EmployeeCreateCaseModel.AccommodationRequest.Accommodations = [];
            vm.EmployeeCreateCaseModel.AccommodationRequest.Accommodations.push(accomm);


            $log.debug('CurrentQuestionSet', vm.ui.currentQuestionSet);
            if (vm.ui.currentQuestionSet && vm.ui.currentQuestionSet.length > 0) {
                var todos = DynamicQuestionsService.resolveTodos(getDynamicQuestionContext());
                $log.debug('Submitting Todos: ', todos);

                _.merge(vm.EmployeeCreateCaseModel, {
                    IsAccommodation: true,
                    AccommodationRequest: {
                        AdditionalQuestions: vm.ui.currentQuestionSet.map(function (m) {
                            return {
                                Name: m.Id,
                                QuestionId: m.Id,
                                InteractiveProcessSteps: [{
                                    QuestionText: m.Question,
                                    ReportLabel: m.ReportLabel,
                                    sAnswer: m.Answer
                                }]
                            };
                        })
                    },
                    Todos: todos

                });

            }

            if (userPermissions != undefined && userPermissions != null)
                vm.EmployeeCreateCaseModel.JobAssignmentPermission = userPermissions.indexOf('JobAssignment') > -1;
            else
                vm.EmployeeCreateCaseModel.JobAssignmentPermission = false;
            if (vm.EmployeeCreateCaseModel.JobHistory != undefined && vm.EmployeeCreateCaseModel.JobHistory != null) {
                //Nullifying this as it is read only.
                vm.EmployeeCreateCaseModel.JobHistory = null;
            }

            caseService.CreateEssCase(vm.EmployeeId, vm.EmployeeCreateCaseModel).then(function (result) {
                vm.CaseCreatedModel = result;
                $("#CaseCreatedSuccessfully").modal('show');
                $('#CaseCreatedSuccessfully').on('hidden.bs.modal', modalDismiss);
            });
        }

        function modalDismiss() {
            window.location.href = '/employees/' + vm.EmployeeId + '/view';
        }

        function ReturnToHomePage() {
            if (vm.IsHRSupervisor) {
                //vm.SetMyCasesActive("MyTeam", true);
                vm.showSearch = true;
            }
            else {
                //vm.SetMyInfoActive("MyInfo", true);
                vm.showSearch = false;
            }
        }

        function ApproveCase() {
            vm.EmployeeCreateCaseModel.Status = 0;
            caseService.CaseReviewed(vm.EmployeeCreateCaseModel).then(function (response) {
                if (response != null) {
                    if (response.Success) {
                        CaseReviewCompleted();
                    }
                }
            });
        }


        function CancelCase() {
            vm.EmployeeCreateCaseModel.Status = 2;
            vm.EmployeeCreateCaseModel.CancelReason = vm.CaseCancelReasonSelect;
            caseService.CaseReviewed(vm.EmployeeCreateCaseModel).then(function (response) {
                if (response != null) {
                    if (response.Success) {
                        CaseReviewCompleted();
                    }
                }
            });
        }

        function CaseReviewCompleted() {
            var resource = $resource('/Todos/' + vm.ToDoItemId + '/EmployeeCaseReviewed', {
            }, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            resource.save().$promise.then(function (response, status) {
                window.location.href = "/Cases/" + vm.CaseId + "/View";
            });
        }

        function CancelReview() {
            window.location.href = "/Cases/" + vm.CaseId + "/View";
        }

        function FilterReasons(item) {
            if (vm.IsAccommodation) {
                var children = $filter('filter')(item.Children, { Code: 'ACCOMM' });
                if (children.length > 0)
                    return true;
                else
                    return false;
            }
            else {
                if (vm.EmployeeCreateCaseModel.LeaveFor == 'Family') {
                    var children = $filter('filter')(item.Children, { IsForFamilyMember: true });
                    return (item.IsForFamilyMember == true || (item.IsCategory == true && children.length > 0));
                }
                else
                    return item.IsForFamilyMember == false || item.IsCategory == true;
            }
        }

        //#endregion "Steps submit and validation
        function SetType(accom) {
            vm.EmployeeCreateCaseModel.AccommodationRequest.Type = accom;
        };

        function print() {
            var DocumentContainer = document.getElementById('printSection');
            var WindowObject = window.open("", "PrintWindow",
            "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
       
    }
})();
