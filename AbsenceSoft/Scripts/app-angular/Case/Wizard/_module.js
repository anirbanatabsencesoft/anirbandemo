﻿(function () {
    'use strict';
    angular
        .module('App.Case.Wizard', [
          , 'ui.bootstrap'
          , 'ui.bootstrap.tpls'
          , 'ngSanitize'
          , 'angular.filter'
          , 'App.Directives'
          , 'App.Case.OtherQuestions'
        ]);
})();