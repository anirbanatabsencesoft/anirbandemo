//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one filter per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic number
//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one filter per file!
//TODO: enable strict mode and fix any violations
//TODO: use constants here instead of magic number
//Policy Eligibility Status
angular
.module('App.Filters')
.filter('policyEligibilityStatus', function () {
    return function (status) {
        switch (status) {
            case -1:
            case 0:
                return "No";
            case 1:
                return "Yes";
        }
        return "No";
    };
});

//Policy Eligibility Status
angular
.module('App.Filters')
.filter('processingStatus', function () {
    return function (status) {
        switch (status) {
            case -1:
                return "Failed";
            case 0:
                return "Pending";
            case 1:
                return "Processing";
            case 2:
                return "Completed";
            case 3:
                return "Cancelled";
        }
        return "";
    };
});


//Denial Reason Filter for common select option
angular
.module('App.Filters')
.filter('denialReasonCommonFilter', function () {
    return function (denialReasons, policyReason, appliedPolicies, manuallyPolicies) {
        var array = [];
        angular.forEach(denialReasons, function (denialReason) {
            if (denialReason.CustomerId == null && denialReason.EmployerId == null) {
                array.push(denialReason);
            } else {
                var commonFlag = true;
                
                angular.forEach(appliedPolicies, function (appliedPolicy) {
                    if (!((jQuery.inArray(policyReason, denialReason.AbsenceReasons) >= 0 || denialReason.AbsenceReasons.length == 0) && (jQuery.inArray(appliedPolicy.PolicyType, denialReason.PolicyTypes) >= 0 || denialReason.PolicyTypes.length == 0) && (jQuery.inArray(appliedPolicy.PolicyCode, denialReason.PolicyCodes) >= 0 || denialReason.PolicyCodes.length == 0)))
                        commonFlag = false;
                });
                angular.forEach(manuallyPolicies, function (manuallyPolicy) {
                    if (!((jQuery.inArray(policyReason, denialReason.AbsenceReasons) >= 0 || denialReason.AbsenceReasons.length == 0) && (jQuery.inArray(manuallyPolicy.PolicyType, denialReason.PolicyTypes) >= 0 || denialReason.PolicyTypes.length == 0) && (jQuery.inArray(manuallyPolicy.PolicyCode, denialReason.PolicyCodes) >= 0 || denialReason.PolicyCodes.length == 0)))
                        commonFlag = false;
                });

                if (denialReason.AbsenceReasons.length == 0 && denialReason.PolicyTypes.length == 0 && denialReason.PolicyCodes.length == 0)
                    commonFlag = true;

                if (commonFlag)
                    array.push(denialReason);
            }
        });
        return array;
    };
});


//Denial Reason Filter by AbsebceReason, PolicyType, PolicyCode, AccomodationType + Add Core Reason for all
angular
.module('App.Filters')
.filter('denialReasonFilter', function () {
    return function (denialReasons, policyReason, policyType, policyCode, accommodationType) {
        var array = [];
        angular.forEach(denialReasons, function (denialReason) {
            if (denialReason.CustomerId == null && denialReason.EmployerId == null)
                array.push(denialReason);
            else if ((policyCode != "" && denialReason.AbsenceReasons.length == 0 && denialReason.PolicyTypes.length == 0 && denialReason.PolicyCodes.length == 0) || (accommodationType != "" && denialReason.AbsenceReasons.length == 0 && (denialReason.AccommodationTypes == null || denialReason.AccommodationTypes.length == 0)))
                array.push(denialReason);
            else if (policyCode != "" && (jQuery.inArray(policyReason, denialReason.AbsenceReasons) >= 0 || denialReason.AbsenceReasons.length == 0) && (jQuery.inArray(policyType, denialReason.PolicyTypes) >= 0 || denialReason.PolicyTypes.length == 0) && (jQuery.inArray(policyCode, denialReason.PolicyCodes) >= 0 || denialReason.PolicyCodes.length == 0))
                array.push(denialReason);
            else if (accommodationType != "" && (jQuery.inArray(policyReason, denialReason.AbsenceReasons) >= 0 || denialReason.AbsenceReasons.length == 0) && (jQuery.inArray(accommodationType, denialReason.AccommodationTypes) >= 0 || denialReason.AccommodationTypes.length == 0))
                array.push(denialReason);
        });
        return array;
    };
});


//Case Contact Type
angular
.module('App.Filters')
.filter('caseContactType', function () {
    return function (status) {
        switch (status) {
            case 0:
                return "Self: ";
            case 5:
                return "Spouse: ";
            case 6:
                return "Parent: ";
            case 7:
                return "Child: ";
            case 8:
                return "Next Of Kin: ";
            case 13:
                return "Step Parent: ";
            case 14:
                return "Foster Parent: ";
            case 17:
                return "Step Child: ";
            case 18:
                return "Foster Child: ";
            case 19:
                return "Adopted Child: ";
            case 20:
                return "Legal Ward: ";
            case 9:
                return "Domestic Partner: ";
            case 10:
                return "Civil Union Partner: ";
            case 11:
                return "Same Sex Spouse: ";
            case 12:
                return "Parent In Law: ";
            case 15:
                return "Grandparent: ";
            case 16:
                return "Grandparent In Law: ";
            case 21:
                return "Residence In House: ";
            case 22:
                return "Committed Relationship: ";
            case 23:
                return "Childs Significant Other: ";
            case 24:
                return "Domestic Partners Child: ";
            case 25:
                return "Civil Union Partners Child: ";
            case 26:
                return "Child Living With Employee: ";
            case 27:
                return "Grandchild: ";
            case 28:
                return "Niece: ";
            case 29:
                return "Nephew: ";
            case 30:
                return "Reciprocal Beneficiary: ";
            case 31:
                return "Elderly Relative: ";
            case 32:
                return "Sibling: ";
        }
        return;
    };
});

angular
.module('App.Filters')
.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});

angular
.module('App.Filters')
.filter('ToDate', function () {
    return function (date) {
        if (date == null || date == '' || date == undefined)
            return '';
        return new Date(date);
    };
});

angular
.module('App.Filters')
.filter('UtcToLocal', ['$filter', function ($filter) {
    return function (utcDateString, format) {
        if (utcDateString == null || utcDateString == '' || utcDateString == undefined)
        {
            return;
        }

        var date1 = new Date(utcDateString);
        var utcDateNum = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds());
        var utcDate = new Date(utcDateNum);

        return $filter('date')(new Date(utcDate.toLocaleString()), format);
    };
}]);

angular
.module('App.Filters')
.filter('ExtractDate', ['$filter', function ($filter) {
    return function (utcDateString, format) {
        if (utcDateString == null || utcDateString == '' || utcDateString == undefined) {
            return;
        }

        var date1 = new Date(utcDateString);
        var utcDate = new Date(date1.getUTCFullYear(), date1.getUTCMonth(), date1.getUTCDate());

        return $filter('date')(utcDate, format);
    };
}]);

/// This filter accepts ISO formatted string (YYYY-MM-DDThh:mm:ss.sTZD ) and converts it to javascript Date object. 
/// While converting from ISO to Date, it disregard the timezone
/// When you want date in string, you will have to use Watch and use 'date | MM-dd-yyyy' filter
angular
.module('App.Filters')
.filter('ISODateToDate', function () {
    return function (isoDate) {
        var resultDate = isoDate;

        // if not proper value, just send the value back.
        if (isoDate == undefined || isoDate == null || isoDate == '') {
            //console.log("ISODateToDate - Undefined or null or blank");
            return resultDate;
        }

        // if itself is a date then send the same date object back
        if (isoDate instanceof Date && !isNaN(isoDate.valueOf())) 
        {
            //console.log("ISODateToDate - itself is Date. input: " + isoDate.toString());
            return resultDate;
        }

        isoDate = isoDate.toString();
        if (isoDate.length < 10) {
            //console.log("ISODateToDate - invalid length of input - must be 10 Characters. input: " + isoDate);
        }
        else
        {
            var year = parseInt(isoDate.slice(0, 4));
            var month = parseInt(isoDate.slice(5, 7));
            var day = parseInt(isoDate.slice(8, 10));

            if (!isNaN(year) && !isNaN(month) && !isNaN(day)) {
                resultDate = new Date(year, month - 1, day, 0, 0, 0, 0);
                return resultDate;
            }
            else
            {
                //console.log("ISODateToDate - invalid format - must be ISO Formatted string - YYYY-MM-DDThh:mm:ss.sTZD. input: " + isoDate);
            }
        }
        
        // we are here, that means we couldn't find proper date conversion yet. let's try to convert to date now - the last option
        // now date is not iso string, but we are taking one step ahead and trying to see if its still a valid date string
        // if yes, go ahead and convert it to date & return it        
        var tryDate = new Date(isoDate);
        if (tryDate instanceof Date && !isNaN(tryDate.valueOf())) {
            resultDate = tryDate;
            return resultDate;
        }
        else
            console.log("ISODateToDate - invalid date format - not possible to convert string to date. input: " + isoDate);

        return resultDate;
    };
});

//Friendly format of telephone numbers
angular
.module('App.Filters')
.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number, ext;
        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = "+" + 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                ext = '';
                break;
            //case 11: // +CPPP####### -> CCC (PP) ###-####
            //    country = "+" + value[0];
            //    city = value.slice(1, 4);
            //    number = value.slice(4);
            //    ext = '';
            //    break;
            //case 12: // +CCCPP####### -> CCC (PP) ###-####
            //    country = "+" + value.slice(0, 3);
            //    city = value.slice(3, 5);
            //    number = value.slice(5);
            //    ext = '';
            //    break;
            default:
                if (value.length > 10) {
                    if (value[0] == 1) {
                        country = "+" + value[0];
                        city = value.slice(1, 4);
                        number = value.slice(4, 11);
                        ext = " x" + value.slice(11);
                    }
                    else {
                        country = "+1";
                        city = value.slice(0, 3);
                        number = value.slice(3, 10);
                        ext = " x" + value.slice(10);
                    }
                }
                else {
                    return tel;
                }
        }
        //if (country == 1) {
        //    country = "";
        //}

        number = number.slice(0, 3) + '-' + number.slice(3);
        return (" (" + city + ") " + number + ext).trim();
    };
});

//Split camel case
angular
.module('App.Filters')
.filter('splitCamelCaseString', function () {
    return function (input) {
        return input.charAt(0).toUpperCase() + input.substr(1).replace(/[A-Z]/g, ' $&');
    }
});


//Split camel case
angular
.module('App.Filters')
.filter('splitCamelCaseLargeString', function () {
    return function (input) {
        var newVal = input.charAt(0).toUpperCase() + input.substr(1).replace(/[A-Z]/g, ' $&');
        if (newVal.length < 10 || newVal.indexOf(' ') > 0) {
            return newVal;
        }
        else {
            var n = parseInt(newVal.length / 2);
            return newVal.match(/.{1,5}/g).join("\r");
        }
    }
});


//Split camel case
angular
.module('App.Filters')
.filter('splitLargeString', function () {
    return function (input) {
        
        if(new Date(input) !== "Invalid Date" && !isNaN(new Date(input)) ){
            return input;
        }
        else {
            if (input.length < 10 || input.indexOf(' ') > 0) {
                return input;
            }
            else {
                var n = parseInt(input.length / 2);
                return input.match(/.{1,5}/g).join("\r");
            }
        }
    }
});



//Units
angular
.module('App.Filters')
.filter('units', function () {
    return function (status) {
        switch (status) {
            case 0:
                return "minutes";
            case 1:
                return "hours";
            case 2:
                return "days";
            case 3:
                return "weeks"
            case 4:
                return "months";
            case 5:
                return "years";
        }
        return;
    };
});

angular
.module('App.Filters')
.filter('nospace', function () {
    return function (value) {
        if (!value)
            return '';
        else {
            var val = value.replace('/', '').replace('-', '').replace('(', '').replace(')', '').replace('?', '');

            return val.replace(/ /g, '');
        }

        //return (!value) ? '' : value.replace(/ /g, '');
    };
});

/**
* Filters out all duplicate elements from an array by checking the specified field
*/
angular
.module('App.Filters')
.filter('distinct', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});


// webFriendly, formats carriage returns and line endings to <br> tags, yay!
angular
.module('App.Filters')
.filter('webFriendly', function () {
    return function (value) {
        if (!value)
            return '';
        else {
            var val = value.replace('\r\n', '<br>').replace('\r', '<br>').replace('\n', '<br>');
            return val;
        }
    };
});


angular
.module('App.Filters')
.filter('jobFunction', function () {
    return function (job) {
        switch (job) {
            case 69:
                return "Essential";
            case 77:
                return "Marginal";
        }
        return "";
    };
});


angular
.module('App.Filters')
.filter('jobClassification', function () {
    return function (job) {
        switch (job) {
            case 1:
                return "Sedentary";
            case 2:
                return "Light";
            case 3:
                return "Medium";
            case 4:
                return "Heavy";
            case 5:
                return "Very Heavy";
        }
        return "";
    };
});


angular
.module('App.Filters')
.filter('nl2br', function () {
    return function (text) {
        return text ? text.replace(/(?:\\[rn]|[\r\n]+)+/g, "<br/>") : '';
    };
});

angular
.module('App.Filters')
.filter('todoStatus', function () {
    return function (status) {
        switch (status) {           
            case 1:
                return "Completed";
            case 2:
                return "Overdue";
            case 3:
                return "Cancelled";
            case 4:
                return "Open";
            case 5:
                return "Pending";
        }
        return "Overdue";
    };
});

//Military Status
angular
.module('App.Filters')
.filter('militaryStatus', function () {
    return function (status) {
        switch (status) {           
            case 0:
                return "Civilian";
            case 1:
                return "Active Duty";
            case 2:
                return "Veteran";
        }
        return "";
    };
});

angular
.module('App.Filters')
.filter('dayOfWeek', function () {
    return function (day) {
        switch (day) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return "";
    };
    });


angular
    .module('App.Filters').filter('PeriodUnit', [function () {
    return function (object) {
        var array = [];
        angular.forEach(object, function (unit) {
            if (unit.Text != 'Minutes' && unit.Text != 'Hours')
                array.push(unit);
        });
        return array;
    };
}]);