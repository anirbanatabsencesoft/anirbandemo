//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('FormsAuthTimeOutCtrl', ['$scope','$http',
    function ($scope, $http) {
        $scope.RenewSession = function () {
            $http({
                method: 'GET',
                url: '/Administration/RenewSession',
            }).then(function (data) {
                $("#FormsAuthTimeout").modal('hide');
            })
        }
    }
]);