//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('NotesCtrl', ['$scope', '$http', '$window', 'noteListManager', 'noteService', '$filter', '$sce', 'userPermissions',
    function ($scope, $http, $window, noteListManager, noteService, $filter, $sce, userPermissions) {

        $scope.ParentType = null; // employee or case
        $scope.ParentTypeInstanceId = null;
        $scope.Notes = [];
        $scope.DefaultNoteToConfidential = false;
        $scope.ViewPIIPortal = userPermissions.indexOf("ViewPIIPortal") > -1;
        $scope.ViewPHIPortal = userPermissions.indexOf("ViewPHIPortal") > -1;
        
        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };
        $scope.ParentSubTypeCode = null;
       

        $scope.init = function (parentType, parentTypeInstanceId, currDate, employerId, defaultNoteToConfidential, parentSubTypeCode) {           
            $scope.ParentType = parentType;
            $scope.ParentTypeInstanceId = parentTypeInstanceId;
            $scope.MaxDate = new Date(currDate);
            $scope.DefaultNoteToConfidential = defaultNoteToConfidential == 'True';
            $scope.CreateNoteViewModel.Public = !$scope.DefaultNoteToConfidential; 
            $scope.ParentSubTypeCode = parentSubTypeCode;
            //$scope.GetNotes()
          
            function isInArray(value, array) {
                return array.indexOf(value) > -1;
            }
            $scope.HasViewPermission = isInArray("ViewNotes", userPermissions);
            $scope.HasCreatePermission = isInArray("CreateNotes", userPermissions);
            if ($scope.HasCreatePermission)
                $scope.HasEditPermission = isInArray("EditNotes", userPermissions);

            $scope.HasDeletePermission = isInArray("DeleteNotes", userPermissions);

            noteListManager.Init($scope, true, $scope.ParentType, $scope.ParentTypeInstanceId);

            //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                    noteListManager.LoadMore($scope, $scope.ErrorHandler);
                }
            });


            //To do filter due date watch
            $scope.$watch('NotesFilter.NoteDate', function () {
                $scope.GetNotes(false);
            });

            var listData = {
                employerId: employerId,
                target: $scope.ParentType == 'case' ? 2 : 1
            }
            noteService.NoteCategoriesList(listData).$promise.then(function (data) {               
                var noteCategories = [];
                for (var i = 0; i < data.length; i++) {
                    $scope.PrependNoteCategory(noteCategories, data[i], '');
                }
                $scope.NoteCategories = noteCategories;
            });
        };

        $scope.PrependNoteCategory = function (categories, category, prefix) {           
            category.Name = prefix + category.Name;
            categories.push(category);
            if (category.ChildCategories && category.ChildCategories.length > 0) {
                var newPrefix = prefix + "-";
                for (var i = 0; i < category.ChildCategories.length; i++) {
                    $scope.PrependNoteCategory(categories, category.ChildCategories[i], newPrefix);
                }
            }
        }

        $scope.NoteCategoryChange = function () {           
            $scope.CreateNoteViewModel.TemplateId = '';
            var categoryData = {
                noteCategoryCode: $scope.CreateNoteViewModel.CategoryCode
            }
            if (categoryData.noteCategoryCode != null) {
                noteService.NoteTemplatesList(categoryData).$promise.then(function (data) {
                    $scope.NoteTemplates = data;
                });
            }            
        }

        $scope.EnableSave = function () {
            $('#btnSaveNotes').prop('disabled', false);
        }

        $scope.NoteTemplateChange = function () {           
            if (!$scope.CreateNoteViewModel.TemplateId)
                return;

            var noteTemplateData = {
                target: $scope.ParentType == 'case' ? 2 : 1,
                templateId: $scope.CreateNoteViewModel.TemplateId,
                entityId: $scope.ParentTypeInstanceId
            }

            noteService.RenderNoteTemplate(noteTemplateData).$promise.then(function (template) {
                if (template.error == null) {
                    if (template.Template) {
                        $scope.CreateNoteViewModel.Notes = template.Template;
                    }
                }
                else {
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: template.error.Message });
                }
            });
        }

        $scope.UpdateNoteSort = function (sortBy) {
            noteListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };

        //#region Get Notes
        $scope.GetNotes = function (doPaging) {
            noteListManager.List($scope, doPaging, $scope.ErrorHandler);
        };
        $scope.GetUrl = function () {
            if ($scope.ParentType == "employee") {
                return '/Employees/' + $scope.ParentTypeInstanceId + "/Notes/List";
            }
            else if ($scope.ParentType == "case") {
                return '/Cases/' + $scope.ParentTypeInstanceId + "/Notes/List";
            }
            return {};
        };
        //#endregion Get Notes

        //#region Create Note
        $scope.CreateNoteViewModel = {
            Id: "",
            EmployeeId: "",
            CaseId: "",
            Notes: "",
            Public: true,
            Category: "",
            TemplateId:""
        };

        $scope.hiImALink = function (data) {
            data = data.replace('[', '');
            data = data.replace(']', '');
            if (data.length > 55) {
                data = $filter('limitTo')(data, 50);
                data = data + " ...";
            }
            data = $filter('nl2br')(data);

            if (data && data.toString().indexOf('<br') >= 0) {
                return $sce.trustAsHtml(data);
            }

            return data;
        };

        $scope.hiImALinks = function (data, contact, answer, category, sAnswer) {
            data = data.replace('[', '');
            data = data.replace(']', '');
            data = data.replace(new RegExp('<','g'), '&lt;');
            data = data.replace(new RegExp('>', 'g'), '&gt;');
            if (answer != undefined || category == "InteractiveProcess" || sAnswer != undefined) {
                var ans;
                if (!answer) {
                    if (sAnswer) {
                        ans = sAnswer;
                    }
                    else {
                        ans = "N/A";
                    }
                }
                else {
                    ans = answer == true ? "Yes" : "No";
                }
                data = data + '<br />' + 'Answer - ' + ans;
            }

            if (contact != null && contact != undefined && contact != '') {
                data = data + '<br />' + 'Contact - ' + contact;
            }

            data = $filter('nl2br')(data);

            if (data && data.toString().indexOf('<br') >= 0) {
                return $sce.trustAsHtml(data);
            }

            return data;
        };
              

        $scope.submit = function () {              
            $('#btnSaveNotes').prop('disabled', true);
            var url = null;
            if ($scope.ParentType == "employee") {
                $scope.CreateNoteViewModel.EmployeeId = $scope.ParentTypeInstanceId;
                url = '/Employees/' + $scope.ParentTypeInstanceId + "/Notes/Create";
            }
            else if ($scope.ParentType == "case") {
                $scope.CreateNoteViewModel.CaseId = $scope.ParentTypeInstanceId;
                url = '/Cases/' + $scope.ParentTypeInstanceId + "/Notes/Create";
            }

            function isInArray(value, array) {
                return array.indexOf(value) > -1;
            }

            $scope.HasCreatePermission = isInArray("CreateNotes", userPermissions);
            if ($scope.HasCreatePermission)
                $scope.HasEditPermission = isInArray("EditNotes", userPermissions);
            else
                $scope.HasEditPermission = false;

            if(typeof($scope.ParentSubTypeCode) !== "undefined")
            {
                $scope.CreateNoteViewModel.CategoryCode = $scope.ParentSubTypeCode; 
            }                      
            $http({ method: 'POST', url: url, data: $scope.CreateNoteViewModel }).then(function (response) {
                if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                    //reset object
                    $scope.CreateNoteViewModel.Notes = "";
                    $scope.CreateNoteViewModel.Category = "";
                    $scope.CreateNoteViewModel.TemplateId = "";
                    $scope.GetNotes(false);
                    $scope.clearNoteViewModel();
                    $scope.NoteToDelete = undefined;                     
                    if (typeof ($scope.ParentSubTypeCode) !== "undefined") {
                        $scope.CreateNoteViewModel.CategoryCode = $scope.ParentSubTypeCode;
                    }
                }
            });
        };

        $scope.clearNoteViewModel = function () {
            $scope.CreateNoteViewModel = {
                Id: "",
                EmployeeId: "",
                CaseId: "",
                Notes: "",
                Public: !$scope.DefaultNoteToConfidential,
                Category: "",
                TemplateId: ""
            };
        };

        $scope.DeleteCaseNoteById = function (note) {
            if ($scope.HasDeletePermission) {
                var confirmText = "Are you sure you want to delete this note?";
                if (confirm(confirmText)) {
                    var url='';
                    if ($scope.ParentType == 'case') {
                        url = '/Cases/Note/Delete';
                    }
                    else {
                        url = '/Employee/Note/Delete';
                    }
                    $http({ method: 'POST', url: url, data: note }).then(function () {
                        $scope.GetNotes(false);
                        $scope.NoteToDelete = undefined;
                        if ($scope.CreateNoteViewModel.Id == note.Id) {
                            //reset the createNoteViewModel
                            $scope.clearNoteViewModel();
                        }
                    });
                }

            }
        }

        $scope.cancel = function ($event) {
            var url = '/';//     /Employees/{employeeId}/View  - or -  /Cases/{caseId}/View
            url += $scope.ParentType;
            url += 's/';
            url += $scope.ParentTypeInstanceId;
            url += '/view';
            $window.location.href = url;
        };
        //#endregion Create Note

        //#region "Edit note"
        $scope.GetNoteToEdit = function (note) {

            function isInArray(value, array) {
                return array.indexOf(value) > -1;
            }

            //userPermissions.splice($.inArray("EditNotes", userPermissions), 1);

            $scope.HasEditPermission = isInArray("EditNotes", userPermissions);

            var hasPermission = isInArray("EditNotes", userPermissions);
            if (hasPermission) {
                var noteId = note.Id || note;
                var url = '';
                if ($scope.ParentType == "employee") {
                    url = '/Employees/' + noteId + '/GetEmployeeNoteById';
                }
                else if ($scope.ParentType == "case") {
                    url = '/Cases/' + noteId + '/GetCaseNoteById';
                }

                $http({ method: 'GET', url: url }).then(function (d) {
                    var response = d.data;
                    $scope.NoteToDelete = note;
                    $scope.CreateNoteViewModel.Id = noteId;

                    if ($scope.ParentType == "employee") {
                        $scope.CreateNoteViewModel.EmployeeId = response.EmployeeId;
                        $scope.CreateNoteViewModel.CaseId = null;
                    }
                    else if ($scope.ParentType == "case") {
                        $scope.CreateNoteViewModel.EmployeeId = null;
                        $scope.CreateNoteViewModel.CaseId = response.CaseId;
                    }

                    $scope.CreateNoteViewModel.Notes = response.Notes;
                    $scope.CreateNoteViewModel.Public = response.Public;
                    $scope.CreateNoteViewModel.CategoryCode = response.CategoryCode;
                    if (response.TemplateId != null) {
                        $scope.NoteCategoryChange();
                        $scope.CreateNoteViewModel.TemplateId = response.TemplateId;
                    }
                    else {
                        $scope.CreateNoteViewModel.TemplateId = '';
                        $scope.NoteTemplates = null;
                    }
                });
            }
        }
        //#endregion

        $scope.GetViewCaseUrl = function () {
            if ($scope.ParentType == "employee") {
                window.location.href = "/Employees/" + $scope.ParentTypeInstanceId + "/View";

            }
            else if ($scope.ParentType == "case") {
                window.location.href = "/Cases/" + $scope.ParentTypeInstanceId + "/View";

            }
        };

        $scope.ToggleAllNotes = function (visible) {
            for (var i = 0; i < $scope.Notes.length; i++) {
                $scope.Notes[i].visible = visible;
            }
        }

        
    }]);