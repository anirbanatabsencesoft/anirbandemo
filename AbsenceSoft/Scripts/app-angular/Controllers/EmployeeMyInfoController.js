//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('employeeMyInfoCtrl', ['$scope', '$filter', '$sce', 'employeeService', function ($scope, $filter, $sce, employeeService) {
    $scope.EmployeeInfo = {};
    //$scope.IsTPA = false;

    $scope.init = function () {
        $scope.GetEmployeeInfo();
    }

    $scope.GetEmployeeInfo = function () {
        employeeService.GetEmployeeInfo($scope.CurrentUserEmpId, null).then(function (response) {
            $scope.EmployeeInfo = response;

            $scope.EmployeeFullName = response.FirstName + " " + response.LastName;
            $scope.EmployeeName = $filter('limitTo')($scope.EmployeeFullName, 25);
            $scope.EmployeeName = $scope.EmployeeName + "...";

            $scope.ShowDOB = false;
            $scope.DoBEyeClick = function () {
                $scope.ShowDOB = !$scope.ShowDOB;
            };
            $scope.ShowSalary = false;
            $scope.SalaryEyeClick = function () {
                $scope.ShowSalary = !$scope.ShowSalary;
            };

            if (response.Email != null) {
                $scope.EmailPieces = (response.Email).split('.');
                $scope.DomainExt = $scope.EmailPieces[$scope.EmailPieces.length - 1];

                $scope.EmployeeEmail = $filter('limitTo')(response.Email, 20 - $scope.DomainExt.length);
                $scope.EmployeeEmail = $scope.EmployeeEmail + '....' + $scope.DomainExt;
            }

        });
    }
}]);