﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('AdministrationCtrl', ['$log', '$scope', '$http', '$rootScope', '$modal', 'administrationService', 'employerListManager', 'userListManager', 'lookupService', 'UserStatus', '_',
function ($log, $scope, $http, $rootScope, $modal, administrationService, employerListManager, userListManager, lookupService, UserStatus, _) {
    var vm = $scope;

    activate();

    function activate() {
        _.merge(vm, {
            RestoreUser: RestoreUser,
            UserStatuses: UserStatus.all
        });
    }

        $scope.HasLinks = false;
        $scope.IsDeleteAccount = false;
        $scope.UserRoles = [{ Id: '-1', Text: "Filter" }];
        $scope.EmployersList = [{ Id: '-1', Text: "Filter" }];
        $scope.CustomerServiceIndicatorOptions = [];

        $scope.init = function (tt) {           
            $scope.IsESS = tt;
           administrationService.GetBasicInfo().then(function (data) {
                if (data != null) {
                    $rootScope.BasicInfo = data;
                    //if ($rootScope.BasicInfo.EmployersInfo.length == 1) {
                        lookupService.GetEmployerFeatures().then(function (data) {
                            if (data != null) {
                                $rootScope.EmployerFeatures = data;
                                $scope.IsTPA = data.MultiEmployerAccess;
                            }
                        })
                    //}


                    angular.forEach(data.EmployersInfo, function (employer, index) {
                        $scope.EmployersList.push({ Id: employer.EmployerId, Text: employer.EmployerName });
                    });

                    if ($rootScope.BasicInfo.EmployersInfo.length == 1) {
                        $http({
                            method: 'POST',
                            url: '/ExportData/' + $rootScope.BasicInfo.EmployersInfo[0].EmployerId + '/GetTotalLinks',
                        }).then(function (response) {
                            if (response.data.length > 0) {
                                $rootScope.BasicInfo.EmployersInfo[0].DoExportComplete = true;
                                $scope.HasLinks = true;
                            }
                        })
                    }

                }
            });

        LoadUsers();

            employerListManager.Init($scope, true);

            userListManager.Init($scope, true);

            administrationService.GetRoles().then(function (data) {
                angular.forEach(data, function (role, index) {
                    $scope.UserRoles.push({ Id: role.Id, Text: role.Name });
                });
            });

            lookupService.GetCustomerServiceIndicatorOptions()
                .then(function(data) {
                    if (data != null)
                        $scope.CustomerServiceIndicatorOptions = data;
                });
        };

        $scope.Error = null;

        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.DeleteAccount = function () {
            administrationService.DeleteAccount($scope.EmployerToDelete.EmployerId).then(function (result) {
                if (result.Success == true) {

                    if ($scope.IsDeleteAccount) {
                        window.location.href = "/login";
                    }
                    else {
                        $scope.GetEmployers(false);

                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Employer removed successfully.' });
                        $("#RemoveAccountWarning").modal('hide');
                    }
                }
                else {
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Account not deleted. Please try again after some time.' });
                    $("#RemoveAccountWarning").modal('hide');
                }

        }, $scope.ErrorHandler);
        }

        $scope.HasServiceOptions = function (basicInfo) {
            if (basicInfo
                && basicInfo.IsTpaCustomerWithEmployerServiceOptions
                && basicInfo.EmployersInfo
                && basicInfo.EmployersInfo.length > 0) {
                for (var i = 0; i < basicInfo.EmployersInfo.length; i++) {
                    var value = basicInfo.EmployersInfo[i].CusomerServiceOptionId;
                    if (value && value.length > 0) {
                        return true;
                    }
                }
            }

            return false;
        }

        $scope.ShowEmployerServiceType = function (basicInfo) {
            return $scope.HasServiceOptions(basicInfo)
                && basicInfo.EmployersInfo[0].CusomerServiceOptionId.length > 0;
        }

        $scope.HasReferenceCode = function (basicInfo) {
            if (basicInfo
                && basicInfo.EmployersInfo
                && basicInfo.EmployersInfo.length > 0) {
                for (var i = 0; i < basicInfo.EmployersInfo.length; i++) {
                    var value = basicInfo.EmployersInfo[i].ReferenceCode;
                    if (value && value.length > 0) {
                        return true;
                    }
                }
            }

            return false;
        }

        $scope.ShowEmployerReferenceCode = function (basicInfo) {
            return $scope.HasReferenceCode(basicInfo)
                && basicInfo.EmployersInfo[0].ReferenceCode.length > 0;
        }

        $scope.GetEmployerInfoModel = function(basicInfo, employer) {
            if (basicInfo
                && basicInfo.IsTpaCustomerWithEmployerServiceOptions
                && basicInfo.EmployersInfo
                && basicInfo.EmployersInfo.length > 0) {
                for (var i = 0; i < basicInfo.EmployersInfo.length; i++) {
                    if (basicInfo.EmployersInfo[i].EmployerId === employer.EmployerId) {
                        return basicInfo.EmployersInfo[i];
                    }
                }
            }

            return null;
        }

        $scope.GetServiceOptionTitle = function (basicInfo, employer) {
            if ($scope.HasServiceOptions(basicInfo)) {
                var employerInfo = $scope.GetEmployerInfoModel(basicInfo, employer);
                if (employerInfo
                    && employerInfo.CusomerServiceOptionId
                    && employerInfo.CusomerServiceOptionId.length > 0) {
                    var length = $scope.CustomerServiceIndicatorOptions.length;
                    for (var i = 0; i < length; i++) {
                        if ($scope.CustomerServiceIndicatorOptions[i].Id === employerInfo.CusomerServiceOptionId) {
                            return $scope.CustomerServiceIndicatorOptions[i].Value;
                        }
                    }
                }
            }

            return "";
        }

        $scope.OpenRemoveWarningModal = function (employer) {
            $("#RemoveAccountWarning").modal('show');
            $scope.EmployerToDelete = employer;
        }

        $scope.DeleteAccountWarning = function () {
            $("#DeleteAccount").modal('show');
            $scope.EmployerToDelete = $rootScope.BasicInfo.EmployersInfo[0];
            $scope.IsDeleteAccount = true;
        }

        $scope.DoExport = function () {
            var freezeDiv = $('#disabledAdminPage');
            freezeDiv.addClass("disablescreen");
            $("#pg").show();
            $("#RemoveAccountWarning").modal('hide');
            $("#DeleteAccount").modal('hide');
            administrationService.DoExport($scope.EmployerToDelete.EmployerId).$promise.then(function () {
                $("#pg").hide();
                var freezeDiv = $('#disabledAdminPage');
                freezeDiv.removeClass("disablescreen");
                $scope.EmployerToDelete.DoExportComplete = true;
                $scope.HasLinks = true;
            }, function (error) {
                $("#pg").hide();
                var freezeDiv = $('#disabledAdminPage');
                freezeDiv.removeClass("disablescreen");
            });
        }



        $scope.GetTotalLinks = function (employerId) {

            var downloadZipLink = $modal.open({
                templateUrl: 'DownloadZipLink.html',
                controller: downloadZipCtrlInstance,
                resolve: {
                    employerId: function () {
                        return employerId;
                    }
                }
            });
        }

        $scope.ShowRoles = function (user) {
            $scope.SelUserName = user.FirstName + (user.LastName == null ? "" : " " + user.LastName);
            $scope.RolesList = user.RolesName;
            $("#myModal").modal('show');
        }

        $scope.LoadMoreEmployers = function () {
            employerListManager.LoadMore($scope, $scope.ErrorHandler);
        };

        $scope.UpdateEmployerSort = function (sortBy) {
            employerListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };

        $scope.GetEmployers = function (doPaging) {
            employerListManager.List($scope, doPaging, $scope.ErrorHandler);

            // Refresh parent page data
            administrationService.GetBasicInfo().then(function (data) {
                if (data != null) {
                    $rootScope.BasicInfo = data;

                    $scope.EmployersList = [{ Id: '-1', Text: "Filter" }]
                    angular.forEach(data.EmployersInfo, function (employer, index) {
                        $scope.EmployersList.push({ Id: employer.EmployerId, Text: employer.EmployerName });
                    });

                    // if user has multiple employers or has multiple employer access is enable then he is TPA
                    if (data.EmployersInfo.length > 1 || $scope.HasMultiEmployerAccess) {
                        $scope.IsTPA = true;
                    }
                    else {
                        $scope.IsTPA = false;
                    }

                     if ($rootScope.BasicInfo.EmployersInfo.length == 1) {
                        $http({
                            method: 'POST',
                            url: '/ExportData/' + $rootScope.BasicInfo.EmployersInfo[0].EmployerId + '/GetTotalLinks',
                        }).then(function (response) {
                            if (response.data.length > 0) {
                                $rootScope.BasicInfo.EmployersInfo[0].DoExportComplete = true;
                                $scope.HasLinks = true;
                            }
                        })
                    }
                }
            });
        };

        $scope.LoadMoreUsers = function () {
            userListManager.LoadMore($scope, $scope.ErrorHandler);
        };

        $scope.UpdateUserSort = function (sortBy) {
            userListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };

        $scope.GetUsers = function (doPaging) {
            userListManager.List($scope, doPaging, $scope.ErrorHandler);
        };

        $scope.ViewPolicies = function (employerId) {
            window.location.href = '/policies/' + employerId;
        }

        $scope.GoToUploadEmployees = function (employerId) {
            window.location.href = "/files/" + employerId + "/eligibilityfileupload";
        }

        $scope.GoToManageEmployer = function (employerId) {
            window.location.href = "/Administration/" + employerId + "/ManageEmployer";
        }

    function LoadUsers() {
        $log.debug('Loading Users...');
        $scope.UsersList = [];
        administrationService.GetUsersList($scope).then(function (data) {
            if (data != null) {
                $scope.UsersList = data;
            }
        });
    }



    }]);


var downloadZipCtrlInstance = function ($scope, $modalInstance, $http, employerId) {

    $scope.EmployerId = employerId;

    $http({
        method: 'POST',
        url: '/ExportData/' + $scope.EmployerId + '/GetTotalLinks',
    }).then(function (response) {
        $scope.Links = response.data;
    })

    $scope.GetExportLink = function (key) {

        $http({
            method: 'POST',
            url: '/ExportData/GetExportLink/?key=' + key,
        }).then(function (response) {
            window.open(response.data.url, "_blank");
        })
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

angular
.module('App.Controllers')
.controller('ManageEmployerCtrl', ['$scope', '$http', 'administrationService', '$rootScope', '$modal', 'lookupService', 
    function ($scope, $http, administrationService, $rootScope, $modal, lookupService) {
        $scope.HasLinks = false;
        $scope.CustomerServiceIndicatorOptions = [];
        $scope.ServiceOptionTitle = "";

        $scope.HasServiceOptions = function (basicInfo) {
            if (basicInfo
                && basicInfo.IsTpaCustomerWithEmployerServiceOptions
                && basicInfo.EmployersInfo
                && basicInfo.EmployersInfo.length > 0) {
                for (var i = 0; i < basicInfo.EmployersInfo.length; i++) {
                    var value = basicInfo.EmployersInfo[i].CusomerServiceOptionId;
                    if (value && value.length > 0) {
                        return true;
                    }
                }
            }

            return false;
        }

        $scope.GetEmployerInfoModel = function (basicInfo, employer) {
            if (basicInfo
                && basicInfo.IsTpaCustomerWithEmployerServiceOptions
                && basicInfo.EmployersInfo
                && basicInfo.EmployersInfo.length > 0) {
                for (var i = 0; i < basicInfo.EmployersInfo.length; i++) {
                    if (basicInfo.EmployersInfo[i].EmployerId === employer.EmployerId) {
                        return basicInfo.EmployersInfo[i];
                    }
                }
            }

            return null;
        }

        $scope.GetServiceOptionTitle = function (basicInfo, employer) {
            if ($scope.HasServiceOptions(basicInfo)) {
                var employerInfo = $scope.GetEmployerInfoModel(basicInfo, employer);
                if (employerInfo
                    && employerInfo.CusomerServiceOptionId
                    && employerInfo.CusomerServiceOptionId.length > 0) {
                    var length = $scope.CustomerServiceIndicatorOptions.length;
                    for (var i = 0; i < length; i++) {
                        if ($scope.CustomerServiceIndicatorOptions[i].Id === employerInfo.CusomerServiceOptionId) {
                            return $scope.CustomerServiceIndicatorOptions[i].Value;
                        }
                    }
                }
            }

            return "";
        }

        $scope.initManageEmployer = function (employerId) {

            $scope.EmployerId = employerId;

            administrationService.GetEmployerInfo(employerId).then(function (response) {
                $scope.EmployerInfo = response;

                $http({
                    method: 'POST',
                    url: '/ExportData/' + employerId + '/GetTotalLinks',
                }).then(function (response) {
                    var data = response.data;
                    if (data.length > 0) {
                        $scope.EmployerInfo.DoExportComplete = true;
                        $scope.HasLinks = true;

                        
                    }
                    administrationService.GetBasicInfo().then(function (basicData) {
                        if (basicData != null) {
                            $scope.IsTpaCustomerWithEmployerServiceOptions = basicData.IsTpaCustomerWithEmployerServiceOptions;
                            if ($scope.IsTpaCustomerWithEmployerServiceOptions) {
                                lookupService.GetCustomerServiceIndicatorOptions()
                                   .then(function (data) {
                                       if (data != null)
                                           $scope.CustomerServiceIndicatorOptions = data;

                                       $scope.ServiceOptionTitle = $scope.GetServiceOptionTitle(basicData, $scope.EmployerInfo);
                                   });
                                
                            }
                            
                        }
                    });
                });
            });

            lookupService.GetEmployerFeatures(employerId).then(function (data) {
                if (data != null) {
                    $rootScope.EmployerFeatures = data;
                }
            });
        }

         $scope.GetEmployers = function (doPaging) {
                // Refresh parent page data
                administrationService.GetEmployerInfo($scope.EmployerId).then(function (response) {
                    $scope.EmployerInfo = response;
                });
         };

         $scope.OpenRemoveWarningModal = function () {
             $("#RemoveAccountWarning").modal('show');
         }

         $scope.DoExport = function () {
             var freezeDiv = $('#disabledAdminPage');
             freezeDiv.addClass("disablescreen");
             $("#pg").show();
             $("#RemoveAccountWarning").modal('hide');            
             administrationService.DoExport($scope.EmployerInfo.Id).$promise.then(function () {
                 $("#pg").hide();
                 var freezeDiv = $('#disabledAdminPage');
                 freezeDiv.removeClass("disablescreen");
                 $scope.EmployerInfo.DoExportComplete = true;
                 $scope.HasLinks = true;
             }, function (error) {
                 $("#pg").hide();
                 var freezeDiv = $('#disabledAdminPage');
                 freezeDiv.removeClass("disablescreen");
             });
         }

         $scope.GetTotalLinks = function (employerId) {

             var downloadZipLink = $modal.open({
                 templateUrl: 'DownloadZipLink.html',
                 controller: downloadZipCtrlInstance,
                 resolve: {
                     employerId: function () {
                         return employerId;
                     }
                 }
             });
         }

         $scope.GoToUploadEmployees = function (employerId) {
             window.location.href = "/files/" + employerId + "/eligibilityfileupload";
         }

         $scope.DeleteAccount = function () {
             administrationService.DeleteAccount($scope.EmployerInfo.Id).then(function (result) {
                 if (result.Success == true) {
                     var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Employer removed successfully.' });
                     
                     window.location.href = "/administration";
                 }
                 else {
                     var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Account not deleted. Please try again after some time.' });
                     $("#RemoveAccountWarning").modal('hide');
                 }

             }, $scope.ErrorHandler);
         }

        $scope.Back = function () {
             window.location.href = "/administration";
         }

    }]);
