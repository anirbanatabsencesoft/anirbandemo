﻿angular
.module('App.Controllers')
.controller('TeamAssignmentCtrl', ['$scope', '$window', 'administrationService', 'lookupService', 'teamAssignmentService',
function ($scope, $window, administrationService, lookupService, teamAssignmentService) {
    $scope.form = {
        submitted: false
    };
    $scope.TeamMembers = [];
    $scope.InitList = function (teams) {
        $scope.Teams = teams;
        init();
    }

    $scope.InitEdit = function (team) {
        $scope.Team = team;
        if ($scope.Team.Id) {
            teamAssignmentService.GetCurrentTeamMembers($scope.Team.CustomerId, $scope.Team.Id).then(function (teamMembers) {
                $scope.TeamMembers = teamMembers;
            });
        }
        init();
        getUsers();
    }

    $scope.InitMyTeam = function (myTeams) {
        $scope.MyTeams = myTeams;
        teamAssignmentService.GetUsersForAssignmentByTeamLead().then(function (users) {
            $scope.UsersForAssignmentByTeamLead = users;
        });
        init();
    }

    $scope.AddTeamMember = function () {
        $scope.TeamMembers.push($scope.NewTeamMember($scope.Team.Id));
    }

    $scope.AddMyTeamMember = function (team) {
        var newTeamMember = $scope.NewTeamMember(team.Team.Id);
        newTeamMember.UserId = team.NewUserId;
        teamAssignmentService.AddMyTeamMember(newTeamMember).then(function (myTeams) {
            for (var i = 0; i < myTeams.length; i++) {
                if (myTeams[i].Team.Id == team.Team.Id)
                    myTeams[i].ShowTeamMembers = true;
            }
            $scope.MyTeams = myTeams;
        });
    }

    $scope.NewTeamMember = function (teamId) {
        return teamMember = {
            TeamId: teamId,
            UserId: '',
            IsTeamLead: false
        }
    }

    $scope.RemoveTeamMember = function (teamMember) {
        var index = $scope.TeamMembers.indexOf(teamMember);
        if (index !== -1) {
            $scope.TeamMembers.splice(index, 1);
        }
    }

    $scope.RemoveMyTeamMember = function (team, teamMember) {
        teamAssignmentService.RemoveMyTeamMember(teamMember).then(function (myTeams) {
            for (var i = 0; i < myTeams.length; i++) {
                if (myTeams[i].Team.Id == team.Team.Id)
                    myTeams[i].ShowTeamMembers = true;
            }
            $scope.MyTeams = myTeams;
        });
    }



    $scope.SetTeamLead = function (teamMember) {
        for (var i = 0; i < $scope.TeamMembers.length; i++) {
            $scope.TeamMembers[i].IsTeamLead = false;
        }

        teamMember.IsTeamLead = true;
    }

    $scope.SaveTeam = function () {
        $scope.form.submitted = true;
        if ($scope.editTeam.$valid) {
            var teamSave = {
                Team: $scope.Team,
                TeamMembers: $scope.TeamMembers
            }

            teamAssignmentService.Save(teamSave).then($scope.GoToTeamIndex);
        }
    }

    $scope.DeleteTeam = function () {
        teamAssignmentService.Delete($scope.Team.CustomerId, $scope.Team.Id).then($scope.GoToTeamIndex);
    }

    getUsers = function () {
        administrationService.GetUsersList().then(function (data) {
            if (data != null) {
                $scope.UsersList = data;
            }
        });
    }


    init = function () {
        administrationService.GetBasicInfo().then(function (data) {
            $scope.BasicInfo = data;
        });
    }

    $scope.GoToTeamIndex = function () {
        window.location.href = '/Team/List';
    }

    $scope.GoToTeamEdit = function (team) {
        window.location.href = '/Team/' + team.CustomerId + '/Edit/' + team.Id;
    }

    $scope.Back = function () {
        $window.history.back();
    }
}]);
