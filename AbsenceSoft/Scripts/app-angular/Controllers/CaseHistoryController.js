//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
    .module('App.Controllers')
    .controller("caseHistoryCtrl", ['$scope', '$filter', 'caseService', function ($scope, $filter, caseService) {
        $scope.EmployeeInfo = {};
        $scope.IsTPA = false;
        $scope.HasFeatureAndPermissionsEnabled = false;
        $scope.AbsenceSoft = AbsenceSoft;

        $scope.initCaseHistory = function () {
            $scope.GetCaseHistory();

            if (!AbsenceSoft.hasStringValue($scope.CaseId))
                HasFeatureAndPermissions();
        };

        $scope.GetCaseHistory = function () {

            var caseId = null;

            if ($scope.CaseId != null && $scope.CaseId != undefined && $scope.CaseId != '')
                caseId = $scope.CaseId;

            caseService.GetCaseHistory($scope.EmployeeId, caseId).then(function (response) {
                $scope.CaseHistory = response;
            });
        };

        HasFeatureAndPermissions = function () {
            caseService.HasFeatureAndPermissions().then(function (data) {
                if (AbsenceSoft.hasValue(data))
                    $scope.HasFeatureAndPermissionsEnabled = data.HasPermission;
            }, function () {
                $scope.HasFeatureAndPermissionsEnabled = false;
            });
        };
    }]);