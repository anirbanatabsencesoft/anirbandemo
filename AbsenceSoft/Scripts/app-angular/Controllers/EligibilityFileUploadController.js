//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('eligibilityFileUploadCtrl', ['$scope', '$http', '$sce', 'eligibilityFileUploadService', '$window', 
    function ($scope, $http, $sce, eligibilityFileUploadService, $window) {

    //#region Variables

    //#endregion Variables

    //#region init
    $scope.init = function (employerId) {
        $scope.EmployerId = employerId;
        eligibilityFileUploadService.GetEligibilityDataFiles(employerId).then(function (result) {
            $scope.lstEligibilityFiles = result.Uploads;
        });

        $(function () {
            $('#file').fileupload({
                dataType: 'json',
                autoUpload: false,
                sequentialUploads: false,
                singleFileUploads: true,
                replaceFileInput: false
            });
        });

        $('#file').bind('fileuploadadd', function (e, data) {
            // Add the files to the list
            //console.log(data.files);

            if (data.files.length > 0) {
                $scope.file = data.files[0];

                eligibilityFileUploadService.GetEligibilityDataFileUploadPolicy($scope.file.name, $scope.EmployerId).then(function (fileUploadPolicy) {
                    // console.log(fileUploadPolicy);
                    $scope.fileUploadPolicy = fileUploadPolicy;

                    $scope.actionUrl = $sce.trustAsResourceUrl(fileUploadPolicy.postUrl)

                    var fd = new FormData();
                    //var key = $scope.file.name;
                    fd.append('key', fileUploadPolicy.fileName);
                    fd.append('acl', fileUploadPolicy.acl);
                    fd.append('Content-Type', $scope.file.type);
                    fd.append('AWSAccessKeyId', fileUploadPolicy.s3Key);
                    fd.append('policy', fileUploadPolicy.s3PolicyBase64);
                    fd.append('signature', fileUploadPolicy.s3Signature);
                    fd.append("file", data.files[0]);
                    var xhr = new XMLHttpRequest();
                    xhr.addEventListener("load", uploadComplete, false);
                    xhr.addEventListener("error", uploadFailed, false);
                    xhr.addEventListener("abort", uploadCanceled, false);
                    xhr.open('POST', fileUploadPolicy.postUrl, true);
                    xhr.send(fd);

                });
            }

        });


    }
    //#endregion init

    //#region Operations
    $scope.Back = function () {
        // var url = "/reports/#/absencereports/" + $scope.report;
        // window.location.href = url;
        $window.history.back();
    }

    $scope.Delete = function (fileId) {
        if (confirm("Are you sure you want to delete this file?")) {
            eligibilityFileUploadService.DeleteEligibilityDataFile(fileId).then(function (result) {
                if (result.Success) {
                    var n = noty({ timeout: 1000, layout: 'topRight', type: 'success', text: "File deleted successfully." });
                    // Re-bind the grid
                    eligibilityFileUploadService.GetEligibilityDataFiles($scope.EmployerId).then(function (result) {
                        $scope.lstEligibilityFiles = result.Uploads;
                    });
                }
            });
        }
    }

    $scope.Cancel = function (fileId) {
        if (confirm("Are you sure you want to cancel this file processing?")) {
            eligibilityFileUploadService.CancelEligibilityDataFile(fileId).then(function (result) {
                if (result.Success) {
                    var n = noty({ timeout: 1000, layout: 'topRight', type: 'success', text: "File processing cancelled successfully." });
                    // Re-bind the grid
                    eligibilityFileUploadService.GetEligibilityDataFiles($scope.EmployerId).then(function (result) {
                        $scope.lstEligibilityFiles = result.Uploads;
                    });
                }

            });
        }
    }

    $scope.showFileDetails = function (fileId) {
        window.location.href = "/Files/" + fileId + "/EligibilityFileDetails"
    }

    function uploadComplete(evt) {
        alert("Upload completed successfully...");

        // Check for an error condition from S3
        if (evt.target && (evt.target.status < 200 || evt.target.status > 206)) {
            var $err = $($.parseXML(evt.target.response || ''));
            return alert($err.find('Message').text() || evt.target.statusText || 'Error code ' + evt.target.status);
        }
        ///* This event is raised when the server send back a HTTP 200 response */
        //// Need to create the upload pointer in Mongo: curData
       
        var eligibilityUploadObj = {EmployerId: $scope.EmployerId, FileName: $scope.file.name, FileKey: $scope.fileUploadPolicy.fileName, MimeType: $scope.file.type, Size: $scope.file.size };
        eligibilityFileUploadService.CreateEligibilityDataFileUpload(eligibilityUploadObj).then(function (EligibilityUploadData) {
            //console.log(EligibilityUploadData);

            // Re-bind the grid
            eligibilityFileUploadService.GetEligibilityDataFiles($scope.EmployerId).then(function (result) {
                $scope.lstEligibilityFiles = result.Uploads;
            });
        });






        //$.ajax({
        //    url: "/Files/Eligibility",
        //    type: "PUT",
        //    data: JSON.stringify(curData),
        //    contentType: 'application/json',
        //    dataType: 'json',
        //    success: function (res) {
        //        /* TODO: Show success message, refresh grid thingy, etc. */
        //        alert('Transfer successful, TODO: Make AJAX request to save the file here.');
        //        // Clear out the file upload
        //        $('#upload-files').replaceWith($('#upload-files').clone());
        //    },
        //    error: function (res, status, error) {
        //        //alert('ERROR');
        //    }
        //});// Create file pointer
    } // uploadComplete

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.");
    } // uploadFailed

    function uploadCanceled(evt) {
        alert("The upload has been canceled by the user or the browser dropped the connection.");
    } // uploadCanceled
    //#endregion Operations

}]);