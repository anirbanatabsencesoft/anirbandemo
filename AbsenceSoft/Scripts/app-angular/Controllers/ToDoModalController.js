//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
    .module('App.Controllers')
    .controller('TodoModalCtrl', ['$scope', '$modalInstance', 'ToDoItem', 'ToDos', 'toDoFilter', 'IsCaseView', 'CaseScope', '$timeout', '$modal', '$http', '$filter', '$resource', 'toDoListManager', 'lookupService', 'administrationService', 'todoModalService',
        function ($scope, $modalInstance, ToDoItem, ToDos, toDoFilter, IsCaseView, CaseScope, $timeout, $modal, $http, $filter, $resource, toDoListManager, lookupService, administrationService, todoModalService) {
            $scope.toDo = ToDoItem;
            if ($scope.toDo.DueDateChangeReason && !$scope.toDo.OriginalDueDate)
                $scope.toDo.OriginalDueDate = $scope.toDo.CreatedDate;

            $scope.ToDos = ToDos;
            $scope.ToDosFilter = toDoFilter;
            $scope.IsCaseView = IsCaseView;
            $scope.forms = {};
            $scope.forms.RotationDays = null;
            $scope.forms.rotationArray = null;
            $scope.forms.FilteredDates = null;
            $scope.Times = [];
            $scope.ManualTimes = [];
            $scope.CurrentDisplayMonth = 4;
            $scope.AlreadyPushedMonths = [];
            $scope.IsOtherReasonVisible = false;

            administrationService.GetUsersList().then(function (data) {
                if (data != null) {
                    $scope.Users = data;
                }

                $.each($scope.Users, function (key, data) {
                    if (data.IsLoggedInUser) {
                        // Set the default re-assigned user to currently logged in user
                        $scope.reassigntodos.UserId = data.Id;
                    }
                });
            });

            $scope.submitToDo = function () {
                if (!$scope.toDo.reason || !($scope.toDo.reason || '').trim()) {
                    $scope.ToDoDueDateChangeReasonNotProvided = true;
                    return;
                }
                $scope.DueDate = !$scope.DueDate;
                $scope.ToDoDueDateChangeReasonNotProvided = false;
                $scope.toDo.NewDueDate = $filter('date')($scope.toDo.NewDueDate, "MM/dd/yyyy");

                $http({ method: 'POST', url: '/Todos/' + $scope.toDo.Id + '/ChangeDueDate?dueDate=' + $scope.toDo.NewDueDate + '&reason=' + $scope.toDo.reason })
                    .then(function (response, status) {
                        $scope.toDo.DueDate = $scope.toDo.NewDueDate;
                    }, function (response) {
                        alert(response.data.error.Message);
                    });
                $modalInstance.close();
            }

            $scope.Today = function () {
                $scope.toDo.NewDueDate = new Date();
            }

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.cancelToDo = function () {
                $scope.cancelClicked = true;
                if ($scope.forms.cancelToDoForm.$invalid)
                    return;

                $scope.toDo.Status = 3;
                $http({ method: 'POST', url: '/Todos/ChangeStatus', data: $scope.toDo})
                    .then(function (response, status) {
                        if (response) {
                            $scope.toDo.Status = "Cancelled";
                            toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                            //next to do
                            $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                                if (innerResponse.data) {
                                    $scope.toDo = innerResponse.data;
                                    $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                                } else {
                                    $scope.toDo.Status = "Cancelled";
                                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                                    $modalInstance.close();
                                }
                            }, function (innerResponse) {
                                alert(innerResponse.data.error.Message);
                            });
                        }
                    }, function (response) {
                        alert(response.data.error.Message);
                    });

                $modalInstance.close();
            };

            //ToDo service will create a copy of the todo and set the status to pending
            $scope.reopenToDo = function (workFlowItemId) {
                $http({ method: 'POST', url: '/Todos/' + workFlowItemId + '/ReOpen' }).then(function (response, status) {
                    if (response) {
                        toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                        //next to do
                        $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                            if (innerResponse.data) {
                                $scope.toDo = innerResponse.data;
                                $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                            } else {
                                $modalInstance.close();
                            }
                        }, function (innerResponse) {
                            alert(innerResponse.data.error.Message);
                        });
                    }
                }, function (response) {
                    alert(response.data.error.Message);
                });

                $modalInstance.close();
            };

            $scope.reassigntodos = { UserId: '', Todos: [] };

            $scope.ReassignTodo = function () {

                if ($scope.reassigntodos.UserId != undefined &&
                    $scope.reassigntodos.UserId != null &&
                    $scope.reassigntodos.UserId != '') {
                    var todoDetail = new Object();
                    todoDetail.TodoId = $scope.toDo.Id;
                    todoDetail.ApplyStatus = true;
                    $scope.reassigntodos.Todos.push(todoDetail);
                    $modalInstance.close();
                    var resource = $resource('/Todos/Reassign', {}, {
                        'save': {
                            method: 'POST', isArray: false
                        }
                    });
                    return resource.save($scope.reassigntodos).$promise;
                }
            };

            // display calendar in Sun..Sat sequence
            $scope.dateOptions = {
                'starting-day': 0
            };

            $scope.$watch('forms.RotationDays', function (newVal, oldVal) {
                if (newVal === oldVal)
                    return;

                if ($scope.forms.RotationDays != "" && $scope.forms.RotationDays != undefined && parseInt($scope.forms.RotationDays) > 0) {
                    for (var i = 0; i < parseInt($scope.forms.RotationDays); i++) {
                        $scope.Time = new Object();
                        $scope.Time.TotalMinutes = null;
                        $scope.Times.push($scope.Time);
                    }
                    $scope.forms.rotationArray = new Array(parseInt($scope.forms.RotationDays));
                }
                else {
                    $scope.forms.rotationArray = null;
                }
            });

            //#region PaperworkDue

            //PaperworkDueModel
            $scope.PaperworkDueModel = new Object();
            //PaperworkDueList
            $scope.PaperworkDue = null;
            //attachment
            $scope.CreationDate = null;
            $scope.CaseId = null;
            $scope.NewAttachments = [];
            $scope.NewAttachmentsSTD = [];
            $scope.Attachments = [];
            $scope.IsUploadInProgress = false;

            $scope.init = function (caseId, workFlowItemId) {
                $scope.WorkFlowItemId = workFlowItemId;
                $scope.CaseId = caseId;
                //if not already initialize
                if (!$scope.ToDosFilter) {
                    toDoListManager.InitToDosFilter($scope);
                    $scope.ToDosFilter.CaseId = caseId;
                }
                //$scope.CreationDate = creationDate;
                // Added code to intialize view model depending on the todo itemtype
                if ($scope.toDo.ItemType == 'PaperworkDue') {
                    $scope.InitPaperworkDueModel();
                }
                else if ($scope.toDo.ItemType == 'PaperworkReview') {
                    $scope.InitPaperworkReviewModel();
                }
                else if ($scope.toDo.ItemType == 'CaseReview') {
                    $scope.InitCaseReviewModel();
                }
                else if ($scope.toDo.ItemType == 'ReturnToWork') {
                    $scope.InitReturnToWorkModel();
                }
                else if ($scope.toDo.ItemType == 'CloseCase') {
                    $scope.InitCaseClosedModel();
                }
                else if ($scope.toDo.ItemType == 'VerifyReturnToWork') {
                    $scope.InitVerifyReturnToWorkModel();
                }
                else if ($scope.toDo.ItemType == 'ScheduleErgonomicAssessment') {
                    $scope.InitErgonomicAssessmentScheduledModel();
                }
                else if ($scope.toDo.ItemType == 'CompleteErgonomicAssessment') {
                    $scope.InitCompleteErgonomicAssessmentModel();
                }
                else if ($scope.toDo.ItemType == 'EquipmentSoftwareOrdered') {
                    $scope.InitEquipmentSoftwareOrderedModel();
                }
                else if ($scope.toDo.ItemType == 'EquipmentSoftwareInstalled') {
                    $scope.InitEquipmentSoftwareInstalledModel();
                }
                else if ($scope.toDo.ItemType == 'HRISScheduleChange') {
                    $scope.InitHRISScheduleChangeModel();
                }
                else if ($scope.toDo.ItemType == 'OtherAccommodation') {
                    $scope.InitOtherAccommodationModel();
                }
                else if ($scope.toDo.ItemType == 'STDCaseManagerContactsEE') {
                    $scope.InitSTDCaseManagerContactsEEModel();
                }
                else if ($scope.toDo.ItemType == 'STDContactHCP') {
                    $scope.InitSTDContactHCPModel();
                }
                else if ($scope.toDo.ItemType == 'STDDiagnosis') {
                    $scope.InitSTDDiagnosisModel();
                }
                else if ($scope.toDo.ItemType == 'STDDesignatePrimaryHCP') {
                    $scope.InitSTDDesignatePrimaryHCPModel();
                }
                else if ($scope.toDo.ItemType == 'STDGenHealthCond') {
                    $scope.InitSTDGenHealthCondModel();
                }
                else if ($scope.toDo.ItemType == 'STDFollowUp') {
                    $scope.InitSTDFollowUpModel();
                }
                else if ($scope.toDo.ItemType == 'ReviewCaseAttachment') {
                    $scope.InitCaseAttachmentReviewModel();
                }
                else if ($scope.toDo.ItemType == 'ReviewCaseNote') {
                    $scope.InitCaseNoteReviewModel();
                }
                else if ($scope.toDo.ItemType == 'ReviewEmployeeInformation') {
                    $scope.InitEmployeeInformationReviewModel();
                }
                else if ($scope.toDo.ItemType == 'ReviewCaseCreated') {
                    $scope.InitEmployeeCaseReviewModel();
                }
                else if ($scope.toDo.ItemType == 'ReviewWorkSchedule') {
                    $scope.InitEmployeeWorkScheduleReviewModel();
                }
                else if ($scope.toDo.ItemType == 'NotifyManager') {
                    $scope.InitWCManagerCaseReviewModel();
                }
                else if ($scope.toDo.ItemType == 'InitiateNewLeaveCase') {
                    $scope.InitInitiateNewLeaveCaseModel();
                }
                else if ($scope.toDo.ItemType == 'Manual') {
                    $scope.InitManualTodo();
                } else if ($scope.toDo.ItemType == 'DetermineRequest') {
                    $scope.InitDetermineRequest();
                }
                else if ($scope.toDo.ItemType == 'Question') {
                    $scope.InitQuestionModel();
                }


                $scope.CreationDate = new Date();
                $(function () {
                    $('#caseAttachments').fileupload({
                        dataType: 'json',
                        autoUpload: false,
                        sequentialUploads: true,
                        singleFileUploads: true,
                        replaceFileInput: false,
                        maxNumberOfFiles: 1
                    });

                    $('#caseAttachmentsSTD').fileupload({
                        dataType: 'json',
                        autoUpload: false,
                        sequentialUploads: true,
                        singleFileUploads: true,
                        replaceFileInput: false
                    });
                });

                $('#caseAttachments').bind('fileuploadadd', function (e, data) {
                    // Add the files to the list
                    for (var i = 0; i < data.files.length; ++i) {
                        var file = data.files[i];
                        var newAttachment = {
                            FileName: file.name,
                            FileData: data,
                            Description: "",
                            AttachmentType: 2,
                            AttachmentTypeName: "Paperwork",
                            IsUploading: false,
                            AttachmentId: $scope.NewAttachments.length + 1,
                            Message: "",
                            ShowCancel: false,
                            ShowMessage: false
                        };

                        // .$apply to update angular when something else makes changes
                        $scope.$apply($scope.NewAttachments[0] = newAttachment);
                    }
                });

                $('#caseAttachmentsSTD').bind('fileuploadadd', function (e, data) {
                    // Add the files to the list
                    for (var i = 0; i < data.files.length; ++i) {
                        var file = data.files[i];
                        // .$apply to update angular when something else makes changes
                        $scope.$apply(
                            $scope.NewAttachmentsSTD.push({
                                FileName: file.name,
                                FileData: data,
                                Description: "",
                                AttachmentType: 1,
                                AttachmentTypeName: "Communication",
                                IsUploading: false,
                                AttachmentId: $scope.NewAttachmentsSTD.length + 1,
                                Message: "",
                                ShowCancel: false,
                                ShowMessage: false
                            }));
                    }
                });
            };

            $scope.InitPaperworkDueModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/PaperworkDue', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.PaperworkDueModel = response;
                    $scope.PaperworkDueModel.NewDueDate = null;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.SavePaperworkDue = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/PaperworkDue', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.PaperworkDueModel).$promise;
            };

            $scope.CompletePaperworkDueToDo = function () {
                $scope.SavePaperworkDue().then(function (response, status) {
                    $scope.toDo.DueDate = $scope.PaperworkDueModel.NewDueDate;
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.dismiss('cancel');
            };

            $scope.CompletePaperworkNotReceived = function () {
                $scope.PaperworkDueModel.PaperworkReceived = false;
                $scope.CompletePaperworkDueToDo();
            };

            $scope.ExtendGracePeriod = function () {
                $scope.PaperworkDueModel.ExtendGracePeriod = true;
                $scope.PaperworkDueModel.CloseCase = false;
                $scope.PaperworkDueModel.NewDueDate = new Date($scope.PaperworkDueModel.NewDueDate).toDateString();
                if (!$scope.PaperworkDueModel.NewDueDate) {
                    $scope.GracePeriodNotProvided = true;
                    return;
                };

                $scope.CompletePaperworkNotReceived();
            };

            $scope.PaperworkNotReceivedCloseCase = function () {
                $scope.PaperworkDueModel.ExtendGracePeriod = false;
                $scope.PaperworkDueModel.CloseCase = true;
                $scope.CompletePaperworkNotReceived();
            };

            $scope.CompletePaperworkReceived = function () {
                $scope.PaperworkDueModel.PaperworkReceived = true;
                $scope.CompletePaperworkDueToDo();
            };

            var getNewAttachmentById = function (id) {
                for (var i = 0; i < $scope.NewAttachments.length; i++) {
                    if ($scope.NewAttachments[i].AttachmentId == id)
                        return $scope.NewAttachments[i];
                }
                return undefined;
            };

            var getNewAttachmentByIdSTD = function (id) {
                for (var i = 0; i < $scope.NewAttachmentsSTD.length; i++) {
                    if ($scope.NewAttachmentsSTD[i].AttachmentId == id)
                        return $scope.NewAttachmentsSTD[i];
                }
                return undefined;
            };

            var resetUploadInProgress = function () {
                var anyFileUploading = false;
                for (var i = 0; i < $scope.NewAttachments.length; i++) {
                    if ($scope.NewAttachments[i].IsUploading) {
                        anyFileUploading = true;
                        break;
                    }
                }
                $scope.IsUploadInProgress = anyFileUploading;
            };

            var resetUploadInProgressSTD = function () {
                var anyFileUploading = false;
                for (var i = 0; i < $scope.NewAttachmentsSTD.length; i++) {
                    if ($scope.NewAttachmentsSTD[i].IsUploading) {
                        anyFileUploading = true;
                        break;
                    }
                }
                $scope.IsUploadInProgress = anyFileUploading;
            };

            $scope.UploadPaperworkReceived = function () {
                var attachment = $scope.NewAttachments[0];
                attachment.FileData.formData = {
                    // AttachmentType: 1,
                    AttachmentType: attachment.AttachmentType,
                    Description: attachment.Description,
                    // Description: "",
                    Id: $scope.CaseId,
                    AttachmentId: attachment.AttachmentId
                };
                attachment.Message = "Uploading " + attachment.FileName;
                attachment.IsUploading = true;
                attachment.ShowMessage = true;
                attachment.FileData.submit({ async: false })
                    .then(function (result) {                        
                        if (result.Message == "Success") {
                            $scope.PaperworkDueModel.ReturnAttachmentId = result.Attachment.Id;
                            $scope.CompletePaperworkReceived();
                        }
                    }, function (jqXHR) {
                        var uploadedAttachment = getNewAttachmentById(this.formData.AttachmentId);
                        var message = "";
                        if (jqXHR.indexOf("Maximum request length exceeded") != -1)
                            message = uploadedAttachment.FileName + " reached the maximum upload file size limit & cannot be uploaded";
                        else
                            message = "Some error occured while uploading " + uploadedAttachment.FileName;
                        uploadedAttachment.Message = message;
                        uploadedAttachment.ShowCancel = true;
                        $scope.$apply(uploadedAttachment);
                        resetUploadInProgress();
                    })
            };

            $scope.UploadFilesSTD = function () {
                for (var i = 0; i < $scope.NewAttachmentsSTD.length; i++) {
                    var attachment = $scope.NewAttachmentsSTD[i];
                    attachment.FileData.formData = {
                        // AttachmentType: 1,
                        AttachmentType: attachment.AttachmentType,
                        Description: attachment.Description,
                        // Description: "",
                        Id: $scope.CaseId,
                        AttachmentId: attachment.AttachmentId
                    };
                    attachment.Message = "Uploading " + attachment.FileName;
                    attachment.IsUploading = true;
                    attachment.ShowMessage = true;
                    attachment.FileData.submit({ async: false })
                        .then(function (response) {
                            var result = response.data;
                            //console.log(result.Attachment.AttachmentId);
                            if (result.Message == "Success") {
                                var uploadedAttachment = getNewAttachmentByIdSTD(result.Attachment.AttachmentId);
                                uploadedAttachment.IsUploading = false;
                                $scope.$apply(
                                    $scope.Attachments.push({
                                        Id: result.Attachment.Id,
                                        FileName: result.Attachment.FileName,
                                        Description: result.Attachment.Description,
                                        AttachmentType: result.Attachment.AttachmentType,
                                        AttachmentTypeName: result.Attachment.AttachmentTypeName,
                                        CreatedDate: result.Attachment.CreatedDate,
                                        FileId: result.Attachment.FileId,
                                        ContentLength: result.Attachment.ContentLength,
                                        ContentType: result.Attachment.ContentType
                                    })
                                );
                                $scope.CancelFileUpload(uploadedAttachment);
                                resetUploadInProgressSTD();
                            }
                        },
                        function (jqXHR) {
                            var uploadedAttachment = getNewAttachmentById(this.formData.AttachmentId);
                            var message = "";
                            if (jqXHR.indexOf("Maximum request length exceeded") != -1)
                                message = uploadedAttachment.FileName + " reached the maximum upload file size limit & cannot be uploaded";
                            else
                                message = "Some error occured while uploading " + uploadedAttachment.FileName;
                            uploadedAttachment.Message = message;
                            uploadedAttachment.ShowCancel = true;
                            $scope.$apply(uploadedAttachment);
                            resetUploadInProgressSTD();
                        })
                }
            };

            $scope.CancelFileUpload = function (attachment) {
                var position = $.inArray(attachment, $scope.NewAttachments);
                if (~position) $scope.$apply($scope.NewAttachments.splice(position, 1));
            };

            $scope.CancelFileUploadSTD = function (attachment) {
                var position = $.inArray(attachment, $scope.NewAttachmentsSTD);
                if (~position) $scope.$apply($scope.NewAttachmentsSTD.splice(position, 1));
            };

            //#endregion PaperworkDue

            //#region PaperworkReview
            $scope.PaperWorkReviewModel = new Object();
            var certification = new Object();

            $scope.InitPaperworkReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/PaperworkReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.PaperWorkReviewModel = response;
                    // $scope.PaperworkDueModel.NewDueDate = null;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.SavePaperworkReviewComplete = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/PaperworkReview', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.PaperWorkReviewModel).$promise;
            };

            $scope.CreateCertification = function (caseId, certification) {
                var resource = $resource('/Cases/' + caseId + '/CreateOrModifyCertification', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save(certification).$promise;
            };

            $scope.PaperworkReviewIncomplete = function () {
                $scope.PaperWorkReviewModel.PaperworkIncomplete = true;
                $scope.SavePaperworkReviewComplete().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId }, params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.PaperworkReviewApproved = function () {
                var caseId = $scope.CaseId;
                $scope.PaperWorkReviewModel.PaperworkApproved = true;

                //Create Certification object if Intermittent case
                if ($scope.PaperWorkReviewModel.CaseType == "Intermittent") {
                    var certificationModel = {
                        CaseId: caseId, HasAnyResults: false,
                        Certifications: [{
                            StartDate: new Date($scope.PaperWorkReviewModel.CertificationStartDate).toDateString(),
                            EndDate: new Date($scope.PaperWorkReviewModel.CertificationEndDate).toDateString(),
                            Notes: $scope.PaperWorkReviewModel.Certification.Notes, Occurances: $scope.PaperWorkReviewModel.Certification.Occurances,
                            Frequency: $scope.PaperWorkReviewModel.Certification.Frequency, FrequencyType: $scope.PaperWorkReviewModel.Certification.FrequencyType,
                            Duration: $scope.PaperWorkReviewModel.Certification.Duration, DurationType: $scope.PaperWorkReviewModel.Certification.DurationType
                        }]
                    };
                    $scope.CreateCertification(caseId, certificationModel).then(function () {
                        CaseScope.reload()
                    });
                }
                $scope.SavePaperworkReviewComplete().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {

                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {

                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.PaperworkReviewDenied = function () {
                $scope.PaperWorkReviewModel.PaperworkDenied = true;
                $scope.SavePaperworkReviewComplete().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.ShowPaperworkReviewApprovedDetails = function () {

                if ($scope.PaperWorkReviewModel.CaseType == "Intermittent") {
                    //Show Certification Detail if Intermittent case
                    $scope.PaperworkReviewApprovedDetails = true;
                }
                else {
                    //Hide Certification Detail if NOT Intermittent case
                    $scope.PaperworkReviewApprovedDetails = false;
                    //Run method for Paperwork Approved
                    $scope.PaperworkReviewApproved();
                }
            }

            //#endregion PaperworkReview

            //#region CaseReview
            $scope.CaseReviewModel = new Object();

            $scope.InitCaseReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.CaseReviewModel = response;
                    // $scope.PaperworkDueModel.NewDueDate = null;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.SaveCaseReivewCompleted = function () {
                //todo:re-factor to call the correct back end code (controller action), after back end code is ready
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseReview', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.CaseReviewModel).$promise;
            };

            $scope.CaseReivewCompleted = function () {
                $scope.SaveCaseReivewCompleted().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.CloseToDoModal = function (caseId) {
                $modalInstance.close();
                window.location.href = "/Cases/" + caseId + "/View";
            };

            //#endregion CaseReview

            //#region ReturntoWork        

            $scope.InitReturnToWorkModel = function () {

                $scope.ReturnToWorkModel = {};


                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/ReturnToWork', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.ReturnToWorkModel = response;
                    $scope.ReturnToWorkModel.ReturnToWorkDate = $scope.ReturnToWorkModel.ReturnToWorkDateText || $scope.ReturnToWorkModel.DueDateText;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.SaveReturnToWork = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/ReturnToWork', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.ReturnToWorkModel).$promise;
            };

            $scope.ReturntoWorkRecertify = function () {
                $scope.ReturnToWorkModel.Recertify = true;
                $scope.SaveReturnToWork().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.ReturntoWorkNotReturn = function () {
                $scope.ReturnToWorkModel.WillNotReturnToWork = true;
                $scope.SaveReturnToWork().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };


            $scope.SaveWillReturnToWork = function () {
                $scope.ReturnToWorkModel.WillReturnToWork = true;
                var RTWdate = $filter('ISODateToDate')($scope.ReturnToWorkModel.ReturnToWorkDate);
                $scope.ReturnToWorkDate = new Date(RTWdate).toDateString();

                $scope.SaveReturnToWork().then(function (response, status) {
                    // Refreshing Case Information to update "Estimated Return To Work" for Jira Work Item-EZW-3 : RTW not reflecting properly
                    CaseScope.reload();
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId }, params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {

                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            //#endregion ReturntoWork

            //#region ReturntoWorkRelease

            $scope.ReturntoWorkReleaseSend = function () {

            };

            //#endregion ReturntoWorkRelease

            //#region VerifyReturntoWork

            $scope.VerifyReturnToWorkModel = new Object();

            $scope.InitVerifyReturnToWorkModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/VerifyReturnToWork', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.VerifyReturnToWorkModel = response;
                    // $scope.PaperworkDueModel.NewDueDate = null;
                    $scope.VerifyReturnToWorkModel.VerifyReturnToWorkDate = $scope.VerifyReturnToWorkModel.DueDateText;


                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.SaveVerifyReturntoWork = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/VerifyReturnToWork', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.VerifyReturnToWorkModel).$promise;
            };


            $scope.VerifyReturntoWorkYes = function () {
                $scope.VerifyReturnToWorkModel.EmployeeReturned = true;
                var verifyRTWdate = $filter('ISODateToDate')($scope.VerifyReturnToWorkModel.VerifyReturnToWorkDate);
                $scope.VerifyReturnToWorkDate = new Date(verifyRTWdate).toDateString();
                //console.log($scope.VerifyReturnToWorkDate);
                $scope.SaveVerifyReturntoWork().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;
                            CaseScope.GetCaseInfo();

                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            $scope.VerifyReturntoWorkNo = function () {
                $scope.VerifyReturnToWorkModel.EmployeeDidNotReturn = true;

                $scope.SaveVerifyReturntoWork().then(function (response, status) {
                    //set the status to reflect in grid
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    $http({ method: 'GET', url: '/Todos/GetNextToDo', params: { caseId: $scope.CaseId } }).then(function (innerResponse, innerStatus) {
                        if (innerResponse.data) {
                            $scope.toDo = innerResponse.data;
                            $scope.toDo.NewDueDate = innerResponse.data.DueDate;

                        } else {
                            $modalInstance.close();
                        }
                    }, function (innerResponse) {
                        alert(innerResponse.data.error.Message);
                    });

                }, function (response) {
                    alert(response.data.error.Message);
                });
                $modalInstance.close();
            };

            //#endregion VerifyReturntoWork

            //#region Ergo Assessment Scheduled

            $scope.ErgonomicAssessmentScheduledModel = new Object();

            $scope.InitErgonomicAssessmentScheduledModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/ErgonomicAssessmentScheduled', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.ErgonomicAssessmentScheduledModel = response;
                    // $scope.PaperworkDueModel.NewDueDate = null;
                    $scope.ErgonomicAssessmentScheduledModel.ErgonomicAssessmentScheduledDate = $scope.ErgonomicAssessmentScheduledModel.DateText;


                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.ErgonomicAssessmentScheduledSave = function () {

                if ($scope.ErgonomicAssessmentScheduledModel.ErgonomicAssessmentScheduledDate == null ||
                    $scope.ErgonomicAssessmentScheduledModel.ErgonomicAssessmentScheduledDate == '' ||
                    $scope.ErgonomicAssessmentScheduledModel.ErgonomicAssessmentScheduledDate == undefined) {
                    $scope.ShowErgoErrorMsg = true;
                }
                else {
                    $scope.ShowErgoErrorMsg = false;
                    $modalInstance.close();

                    var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/ErgonomicAssessmentScheduledCompleted', {}, {
                        'save': {
                            method: 'POST', isArray: false
                        }
                    });
                    return resource.save($scope.ErgonomicAssessmentScheduledModel).$promise;
                }
            };

            //#endregion Ergo Assessment Scheduled

            //#region Complete Ergo Assessment

            $scope.CompleteErgonomicAssessmentModel = new Object();

            $scope.InitCompleteErgonomicAssessmentModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteErgonomicAssessment', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.CompleteErgonomicAssessmentModel = response;
                    $scope.CompleteErgonomicAssessmentModel.AssessmentDate = '';
                    $scope.CompleteErgonomicAssessmentModel.ResponseText = '';

                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.CompleteErgonomicAssessmentSave = function () {
                $modalInstance.close();
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteErgonomicAssessmentCompleted', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.CompleteErgonomicAssessmentModel).$promise;
            };


            $scope.ErgoResponseYes = function () {
                $scope.CompleteErgonomicAssessmentModel.ResponseText = "Yes";
            };

            $scope.ErgoResponseNotYet = function () {
                $scope.CompleteErgonomicAssessmentModel.ResponseText = "NotYet";
            };

            $scope.ErgoResponseNo = function () {
                $scope.CompleteErgonomicAssessmentModel.ResponseText = "No";
            };

            //#endregion Complete Ergo Assessment

            //#region Equip Software Ordered

            $scope.EquipmentSoftwareOrderedModel = new Object();

            $scope.InitEquipmentSoftwareOrderedModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EquipmentSoftwareOrdered', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.EquipmentSoftwareOrderedModel = response;
                    $scope.EquipmentSoftwareOrderedModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.EquipmentSoftwareOrderedSave = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EquipmentSoftwareOrderedCompleted', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.EquipmentSoftwareOrderedModel).$promise;
                $modalInstance.close();
            };

            //#endregion Equip Software Ordered

            //#region Equip Software Installed

            $scope.EquipmentSoftwareInstalledModel = new Object();

            $scope.InitEquipmentSoftwareInstalledModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EquipmentSoftwareInstalled', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.EquipmentSoftwareInstalledModel = response;
                    $scope.EquipmentSoftwareInstalledModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.EquipmentSoftwareInstalledSave = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EquipmentSoftwareInstalledCompleted', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.EquipmentSoftwareInstalledModel).$promise;
                $modalInstance.close();
            };

            //#endregion

            //#region HRISScheduleChange

            $scope.HRISScheduleChangeModel = new Object();

            $scope.InitHRISScheduleChangeModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/HRISScheduleChange', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.HRISScheduleChangeModel = response;
                    $scope.HRISScheduleChangeModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.HRISScheduleChangeSave = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/HRISScheduleChangeCompleted', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.HRISScheduleChangeModel).$promise;
                $modalInstance.close();
            };

            //#endregion


            //#region OtherAccommodatin

            $scope.OtherAccommodationModel = new Object();

            $scope.InitOtherAccommodationModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/OtherAccommodation', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.OtherAccommodationModel = response;
                    $scope.OtherAccommodationModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.OtherAccommodationSave = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/OtherAccommodationCompleted', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.OtherAccommodationModel).$promise;
                $modalInstance.close();
            };

            //#endregoin

            //region STD

            $scope.STDCaseManagerContactsEEModel = new Object();

            $scope.InitSTDCaseManagerContactsEEModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDCaseManagerContactsEE', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.STDCaseManagerContactsEEModel = response;
                    $scope.STDCaseManagerContactsEEModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDCaseManagerContactsEESave = function (isComplete) {
                $scope.STDCaseManagerContactsEEModel.Completed = isComplete;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDCaseManagerContactsEEModel).$promise;
                $modalInstance.close();
            };

            //end region STD


            //STDContactHCP

            $scope.STDContactHCPModel = new Object();

            $scope.InitSTDContactHCPModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDContactHCP', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.STDContactHCPModel = response;
                    $scope.STDContactHCPModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDContactHCPSave = function (isComplete) {
                $scope.STDContactHCPModel.Completed = isComplete;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDContactHCPModel).$promise;
                $modalInstance.close();

            };
            //end STDContactHCP

            //STDDiagnosis

            $scope.STDDiagnosisModel = new Object();

            $scope.InitSTDDiagnosisModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDDiagnosis', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.STDDiagnosisModel = response;
                    $scope.STDDiagnosisModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDDiagnosisSave = function (isComplete) {
                $scope.STDDiagnosisModel.Completed = isComplete;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDDiagnosisComplete', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDDiagnosisModel).$promise;
                $modalInstance.close();

            };

            //End STDDiagnosis

            //STDDesignatePrimaryHCP

            $scope.STDDesignatePrimaryHCPModel = new Object();

            $scope.InitSTDDesignatePrimaryHCPModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDDesignatePrimaryHCP', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.STDDesignatePrimaryHCPModel = response;
                    $scope.STDDesignatePrimaryHCPModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDDesignatePrimaryHCPSave = function (isComplete) {
                $scope.STDDesignatePrimaryHCPModel.Completed = isComplete;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDDesignatePrimaryHCPModel).$promise;
                $modalInstance.close();

            };

            //End STDDesignatePrimaryHCP


            //STDGenHealthCond

            $scope.STDGenHealthCondModel = new Object();

            $scope.InitSTDGenHealthCondModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDGenHealthCond', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                $scope.submitted = false;
                resource.list().$promise.then(function (response, status) {
                    $scope.STDGenHealthCondModel = response;
                    $scope.STDGenHealthCondModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDGenHealthCondSave = function (closeIt) {
                $scope.STDGenHealthCondModel.Completed = closeIt;
                if ($scope.STDGenHealthCondModel.Completed == true)
                    $scope.submitted = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDGenHealthCondComplete', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDGenHealthCondModel).$promise;
                if ($scope.STDGenHealthCondModel.ConditionStartDate != null)
                    CaseScope.STD.ConditionStartDate = $scope.STDGenHealthCondModel.ConditionStartDate;

                CaseScope.STD.GeneralHealthCondition = $scope.STDGenHealthCondModel.GenHealthCond;
                $modalInstance.close();
                setTimeout(function () { location.reload(true); }, 1000);

            };

            //End STDGenHealthCond

            //STDFollowUp

            $scope.STDFollowUpModel = new Object();

            $scope.InitSTDFollowUpModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDFollowUp', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.STDFollowUpModel = response;
                    $scope.STDFollowUpModel.Completed = false;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.STDFollowUpSave = function (isComplete) {
                $scope.STDFollowUpModel.Completed = isComplete;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/STDFollowUpComplete', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.STDFollowUpModel).$promise;
                $modalInstance.close();
            };

            //End STDGenHealthCond

            //#region CloseCase
            $scope.CaseClosedModel = new Object();

            $scope.InitCaseClosedModel = function () {
                $scope.CaseClosureCategory = lookupService.CaseClosureCategory($scope.CaseId);

                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseClosed', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.CaseClosedModel = response;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.CloseCaseCall = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseClosed', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                return resource.save($scope.CaseClosedModel).$promise;
            };

            $scope.SelectReason = function (frmCloseCase) {
                if (($scope.CaseClosedModel.Reason).toUpperCase() == 'OTHER')
                { $scope.IsOtherReasonVisible = true; }
                else
                { $scope.IsOtherReasonVisible = false; }

            }

            $scope.CloseCase = function (frmCloseCase) {
                if (!frmCloseCase.$valid)
                    return;
                $scope.CaseClosedModel.CloseTheCase = true;
                $scope.CloseCaseCall().then(function (response, status) {
                    //set the status to reflect in grid               
                    $scope.toDo.Status = "Complete";

                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);

                    if ($scope.IsCaseView) {
                        CaseScope.RefreshDetails();
                        CaseScope.GetCaseInfo();
                    }

                    window.location.href = "/Cases/" + $scope.toDo.CaseId + "/View/";

                }, function (response) {
                    alert(response.data.error.Message);
                });

                $modalInstance.close();
            };
            $scope.CloseCaseConfirmation = function () {
                $modalInstance.close();
            };
            //#endregion CloseCase



            $scope.InitCaseAttachmentReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseAttachmentReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.CaseAttachmentReviewModel = response;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.ReviewCaseAttachmentSave = function (update) {
                $scope.CaseAttachmentReviewModel.Update = update;
                $scope.CaseAttachmentReviewModel.Completed = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.CaseAttachmentReviewModel).$promise.then(function (response, status) {
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });
            };



            $scope.InitCaseNoteReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CaseNoteReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.CaseNoteReviewModel = response;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.ReviewCaseNoteSave = function (update) {
                $scope.CaseNoteReviewModel.Update = update;
                $scope.CaseNoteReviewModel.Completed = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.CaseNoteReviewModel).$promise.then(function (response, status) {
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });
            };

            $scope.InitEmployeeInformationReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EmployeeInformationReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.EmployeeInfoReviewModel = response;

                    $scope.fields = Object.keys($scope.EmployeeInfoReviewModel.OldValues);

                }, function (response) {
                    alert(response.data.error.Message);
                });
            };

            $scope.ReviewEmployeeInformationSave = function (update) {
                $scope.EmployeeInfoReviewModel.Update = update;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EmployeeInformationReview', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.EmployeeInfoReviewModel).$promise.then(function (response, status) {
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });
            };

            $scope.InitEmployeeCaseReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/EmployeeCaseReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.EmployeeCaseReviewModel = response;
                }, function (response) {
                    alert(response.data.error.Message);
                });
            }

            $scope.InitEmployeeWorkScheduleReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/WorkScheduleReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.EmployeeWorkScheduleReviewModel = response;

                    $scope.WorkSchedule = response.EmployeeInfo.WorkSchedules[0];

                    $scope.forms.WorkScheduleStartDate = $scope.WorkSchedule.StartDate;
                    $scope.forms.WorkScheduleEndDate = $scope.WorkSchedule.EndDate;


                    $scope.$watch('forms.WorkScheduleStartDate', function () {
                        var strISODate = $filter("date")($scope.forms.WorkScheduleStartDate, "yyyy-MM-dd");
                        $scope.GetManualDays(strISODate);
                    });

                    //schedule view 
                    $scope.ScheduleStartEnd = true;
                    //weekly
                    if ($scope.WorkSchedule.ScheduleType == 0) {
                        $scope.ScheduleWeekly = true;
                        $scope.ScheduleRotating = false;
                        $scope.ScheduleRotatingPerDay = false;
                        $scope.ScheduleOneTime = false;
                        //populate day

                        angular.copy($scope.WorkSchedule.Times, $scope.Times);
                        //$scope.Times = $scope.WorkSchedule.Times;

                        if ($scope.Times.length <= 0) {
                            for (var i = 0; i < 7; i++) {
                                $scope.Time = new Object();
                                $scope.Time.TotalMinutes = null;
                                $scope.Times.push($scope.Time);
                            }
                        }

                        //for (var i = 0; i < 7; i++) {
                        //    $scope.Time = new Object();
                        //    //note: with current structure it is difficult to determine the time entered for exact day
                        //    ////$scope.Times = $scope.WorkSchedule.Times;
                        //    $scope.Time.TotalMinutes = null;
                        //    $scope.Times.push($scope.Time);
                        //}

                    }//rotating
                    else if ($scope.WorkSchedule.ScheduleType == 1) {
                        $scope.ScheduleWeekly = false;
                        $scope.ScheduleRotating = true;
                        $scope.ScheduleRotatingPerDay = false;
                        $scope.ScheduleOneTime = false;

                        angular.copy($scope.WorkSchedule.Times, $scope.Times);
                        //$scope.Times = $scope.WorkSchedule.Times;
                        $scope.forms.RotationDays = $scope.WorkSchedule.Times.length;


                    }//manual
                    else if ($scope.WorkSchedule.ScheduleType == 2) {
                        $scope.ScheduleWeekly = false;
                        $scope.ScheduleRotating = false;
                        $scope.ScheduleRotatingPerDay = false;
                        $scope.ScheduleOneTime = true;

                        var strISODate = $filter("date")($scope.forms.WorkScheduleStartDate, "yyyy-MM-dd");
                        $scope.GetManualDays(strISODate);
                    }

                }, function (response) {
                    alert(response.data.error.Message);
                });
            }

            $scope.ReviewWorkScheduleSave = function (isValid, isCloseTodo, update) {
                if (isCloseTodo) {
                    $scope.EmployeeWorkScheduleReviewModel.Update = false;
                    var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/WorkScheduleReviewed', {}, {
                        'save': {
                            method: 'POST', isArray: false
                        }
                    });
                    resource.save($scope.EmployeeWorkScheduleReviewModel).$promise.then(function (response, status) {
                        $scope.toDo.Status = "Complete";
                        toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                        $modalInstance.close();
                    });
                }
                else {
                    if (!isValid)
                        return;
                    else {
                        if ($scope.WorkSchedule.ScheduleType == 2) {
                            if ($scope.ManualTimes.length > 0) {
                                var isHourValueEntered = false;

                                angular.forEach($scope.ManualTimes, function (item, index) {
                                    if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                                        isHourValueEntered = true;
                                    }
                                });
                            }
                        }
                        else if ($scope.WorkSchedule.ScheduleType == 0 || $scope.WorkSchedule.ScheduleType == 1) {
                            if ($scope.Times.length > 0) {
                                var isHourValueEntered = false;
                                angular.forEach($scope.Times, function (item, index) {
                                    if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                                        isHourValueEntered = true;
                                    }
                                });
                            }
                        }
                        else {
                            isHourValueEntered = true;
                        }

                        if (!isHourValueEntered) {
                            isValid = false;

                            if ($scope.WorkSchedule.ScheduleType == 0) {
                                $scope.ShowWeeklyHourError = true;
                                $scope.ShowRotationHourError = false;
                                $scope.ShowManualHourError = false;
                            }
                            else if ($scope.WorkSchedule.ScheduleType == 1) {
                                $scope.ShowWeeklyHourError = false;
                                $scope.ShowRotationHourError = true;
                                $scope.ShowManualHourError = false;
                            }
                            else if ($scope.WorkSchedule.ScheduleType == 2) {
                                $scope.ShowWeeklyHourError = false;
                                $scope.ShowRotationHourError = false;
                                $scope.ShowManualHourError = true;
                            }
                            return;
                        }
                        else {
                            $scope.ShowWeeklyHourError = false;
                            $scope.ShowRotationHourError = false;
                            $scope.ShowManualHourError = false;
                        }
                    }

                    if (isValid) {
                        if ($scope.forms.WorkScheduleStartDate == null || $scope.forms.WorkScheduleStartDate == undefined || $scope.forms.WorkScheduleStartDate == '') {
                            $scope.WorkSchedule.StartDate = null;
                        }
                        else {
                            $scope.WorkSchedule.StartDate = new Date($scope.forms.WorkScheduleStartDate).toDateString();
                        }

                        if ($scope.forms.WorkScheduleEndDate == null || $scope.forms.WorkScheduleEndDate == undefined || $scope.forms.WorkScheduleEndDate == '') {
                            $scope.WorkSchedule.EndDate = null;
                        }
                        else {
                            $scope.WorkSchedule.EndDate = new Date($scope.forms.WorkScheduleEndDate).toDateString();
                        }

                        if ($scope.WorkSchedule.ScheduleType != 2) {// Other than "Manual"
                            $scope.WorkSchedule.Times = $scope.Times;
                        }
                        else {
                            $scope.WorkSchedule.Times = [];
                            angular.forEach($scope.ManualTimes, function (item, index) {
                                if (item.TotalMinutes != '') {
                                    $scope.WorkSchedule.Times.push(item);
                                }
                            });
                        }

                        $scope.EmployeeWorkScheduleReviewModel.Update = update;
                        $scope.EmployeeWorkScheduleReviewModel.EmployeeInfo.WorkSchedule = $scope.WorkSchedule;
                        var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/WorkScheduleReviewed', {}, {
                            'save': {
                                method: 'POST', isArray: false
                            }
                        });
                        resource.save($scope.EmployeeWorkScheduleReviewModel).$promise.then(function (response, status) {
                            $scope.toDo.Status = "Complete";
                            toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                            $modalInstance.close();
                        });
                    }
                }
            }

            $scope.UpdateScheduleType = function (scheduleType) {
                $scope.ScheduleType = scheduleType;
                $scope.WorkSchedule.ScheduleType = scheduleType;

                //weekly
                if (scheduleType == 0) {
                    //$scope.ChangeCase.WorkSchedule.ScheduleType = 0;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = true;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    //populate day
                    //$scope.Times = [];

                    if ($scope.Times.length <= 0) {
                        for (var i = 0; i < 7; i++) {
                            $scope.Time = new Object();
                            $scope.Time.TotalMinutes = null;
                            $scope.Times.push($scope.Time);
                        }
                    }


                    //
                    var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    var weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');

                    for (var i = 0; i < weeklyDays.length; i++) {
                        $(weeklyDays[i]).attr('name', 'WorkSchedule.Times[' + i + '].TotalMinutes');
                    }
                }
                //rotating
                else if (scheduleType == 1) {
                    //$scope.ChangeCase.WorkSchedule.ScheduleType = 1;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = true;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    //$scope.Times = [];

                    //
                    var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    var weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');
                }
                //manual
                else if (scheduleType == 2) {
                    //$scope.ChangeCase.WorkSchedule.ScheduleType = 2;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = true;

                    var strISODate = $filter("date")($scope.forms.WorkScheduleStartDate, "yyyy-MM-dd");
                    $scope.GetManualDays(strISODate);
                }
            };


            $scope.InitWCManagerCaseReviewModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/WCManagerCaseReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.WCManagerCaseReviewModel = response;
                });
            }

            $scope.CaseReviewCompleted = function () {
                $scope.WCManagerCaseReviewModel.update = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/WCManagerCaseReviewed', {}, {
                    'save': { method: 'POST', isArray: false }
                });
                resource.save($scope.WCManagerCaseReviewModel).$promise.then(function (response, status) {
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });
            }

            $scope.InitInitiateNewLeaveCaseModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/InitiateNewLeaveReview', {}, {
                    'list': { method: 'GET', isArray: false }
                });
                resource.list().$promise.then(function (response, status) {
                    $scope.InitiateNewLeaveTodo = response;
                });
            }

            $scope.InitiateNewLeaveTodoCompleted = function () {
                $scope.InitiateNewLeaveTodo.update = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/InitiateNewLeaveReviewComplete', {}, {
                    'save': { method: 'POST', isArray: false }
                });
                resource.save($scope.InitiateNewLeaveTodo).$promise.then(function (response, status) {
                    $scope.toDo.Status = "Complete";
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });
            }

            $scope.InitManualTodo = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/GetManualTodo', {}, {
                    'get': { method: 'GET', isArray: false }
                });
                resource.get().$promise.then(function (response, status) {
                    $scope.ManualTodo = response;
                });
            }

            $scope.InitDetermineRequest = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/DetermineRequest', {}, {
                    'get': { method: 'GET', isArray: false }
                });
                resource.get().$promise.then(function (response, status) {
                    $scope.IntermittentRequest = response;
                });
            }

            $scope.InitQuestionModel = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/QuestionToDo', {}, {
                    'get': { method: 'GET', isArray: false }
                });
                resource.get().$promise.then(function (response, status) {
                    $scope.QuestionConfiguration = response;
                });
            }

            $scope.SaveQuestionToDo = function () {
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/SaveQuestionToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                if ($("button[class*=selected]").length > 0) {
                    $scope.QuestionConfiguration.ResultText = $("button[class*=selected]")[0].innerHTML;
                    resource.save($scope.QuestionConfiguration).$promise.then(function (response, status) {
                        toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                        $modalInstance.close();
                    });
                }
                else {
                    return false;
                }
                
            };
            $scope.SelectAnswerButton = function (name) {
                $("button[id*=answerbtn_]").removeClass("btn-primary selected");
                $("button[id*=answerbtn_]").addClass("btn-info");
                $("#answerbtn_" + name).removeClass("btn-info");
                $("#answerbtn_" + name).addClass("btn-primary selected");
            }

            $scope.CompleteManualTodo = function () {
                $scope.ManualTodo.Completed = true;
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save($scope.ManualTodo).$promise.then(function (response, status) {
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });

            };

            $scope.ApproveDetermineRequestToDo = function () {
                $scope.toDo.ResultText = "Approved";
                $scope.CompleteToDo($scope.toDo);
            }

            $scope.DenyDetermineRequestToDo = function () {
                $scope.toDo.ResultText = "Denied";
                $scope.CompleteToDo($scope.toDo);
            }

            $scope.CompleteToDo = function (todo) {
                todo = todo || $scope.toDo;
                todo.Completed = true;
                todo.Status = '';
                var resource = $resource('/Todos/' + $scope.WorkFlowItemId + '/CompleteToDo', {}, {
                    'save': {
                        method: 'POST', isArray: false
                    }
                });
                resource.save(todo).$promise.then(function (response, status) {
                    toDoListManager.List(CaseScope, false, $scope.ErrorHandler);
                    $modalInstance.close();
                });

            }

            //#region "Manual Schedule"
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            var noofdays = function (mm, yyyy) {
                var daysofmonth;

                if ((mm == 4) || (mm == 6) || (mm == 9) || (mm == 11)) {
                    daysofmonth = 30;
                }
                else {
                    daysofmonth = 31
                    if (mm == 2) {
                        if (yyyy / 4 - parseInt(yyyy / 4) != 0) {
                            daysofmonth = 28
                        }
                        else {
                            if (yyyy / 100 - parseInt(yyyy / 100) != 0) {
                                daysofmonth = 29
                            }
                            else {
                                if ((yyyy / 400) - parseInt(yyyy / 400) != 0) {
                                    daysofmonth = 28
                                }
                                else {
                                    daysofmonth = 29
                                }
                            }
                        }
                    }
                }

                return daysofmonth;
            }

            $scope.GetManualDays = function (startDate) {

                if ($scope.WorkSchedule.ScheduleType != 2)
                    return;

                if (startDate == null) return;

                // var selectedDate = new Date(startDate);
                var selectedDate = $filter('ISODateToDate')(startDate);
                $scope.CurrentDisplayMonth = selectedDate.getMonth();
                $scope.CurrentDisplayYear = selectedDate.getFullYear();

                $scope.CurrentMonthName = monthNames[$scope.CurrentDisplayMonth] + " " + $scope.CurrentDisplayYear;

                if ($scope.CurrentDisplayMonth == 0) {
                    $scope.PrevMonthName = monthNames[11] + " " + ($scope.CurrentDisplayYear - 1);
                    $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
                }
                else if ($scope.CurrentDisplayMonth == 11) {
                    $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                    $scope.NextMonthName = monthNames[0] + " " + ($scope.CurrentDisplayYear + 1);
                }
                else {
                    $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                    $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
                }

                var numOfDaysInMonth = noofdays($scope.CurrentDisplayMonth + 1);

                var startDay = selectedDate.getDate();

                var monthPosition = $.inArray($scope.CurrentDisplayMonth, $scope.AlreadyPushedMonths);
                if (monthPosition == -1) {
                    for (var i = 1; i <= numOfDaysInMonth; i++) {
                        var time = { TotalMinutes: '', SampleDate: '' };


                        var tempDate = selectedDate.getFullYear() + "-" + ('0' + ($scope.CurrentDisplayMonth + 1)).slice(-2) + "-" + ('0' + (i)).slice(-2);
                        time.SampleDate = $filter('ISODateToDate')(tempDate);

                        // time.SampleDate = new Date(selectedDate.getFullYear(), $scope.CurrentDisplayMonth, i);

                        //if ($scope.EmployeeId) {
                        var found_time = $.grep($scope.WorkSchedule.Times, function (v) {
                            var vSampleDate = $filter('ISODateToDate')(v.SampleDate);
                            var timeSampleDate = $filter('ISODateToDate')(time.SampleDate);

                            var dt1 = new Date(vSampleDate.getFullYear(), vSampleDate.getMonth(), vSampleDate.getDate(), 0, 0, 0);
                            var dt2 = new Date(timeSampleDate.getFullYear(), timeSampleDate.getMonth(), timeSampleDate.getDate(), 0, 0, 0);

                            return dt1.isSameDay(dt2);
                        });

                        if (found_time.length > 0) {
                            $scope.ManualTimes.push(found_time[0]);
                        }
                        else {
                            $scope.ManualTimes.push(time);
                        }
                        //}
                        //else {
                        //    $scope.ManualTimes.push(time);
                        //}
                    }

                    $scope.AlreadyPushedMonths.push($scope.CurrentDisplayMonth)
                }
                // $scope.ManualTimes.push(monthData);
                $scope.FilterDates();
            };

            $scope.FilterDates = function () {
                $scope.forms.FilteredDates = [];
                angular.forEach($scope.ManualTimes, function (manualTime, index) {
                    var dt = $filter('ISODateToDate')(manualTime.SampleDate);;                  // new Date(manualTime.SampleDate);
                    if (dt.getMonth() == $scope.CurrentDisplayMonth) {
                        $scope.forms.FilteredDates.push(manualTime);
                    }
                });
            };

            $scope.GoToMonth = function (direction) {
                var year, month;

                if (direction == 'Prev') {
                    if ($scope.CurrentDisplayMonth == 0) {  // if January
                        month = 11;
                        year = $scope.CurrentDisplayYear - 1;
                    }
                    else {
                        month = $scope.CurrentDisplayMonth - 1;
                        year = $scope.CurrentDisplayYear;
                    }
                }
                else {
                    if ($scope.CurrentDisplayMonth == 11) {  // if December
                        month = 0;
                        year = $scope.CurrentDisplayYear + 1;
                    }
                    else {
                        month = $scope.CurrentDisplayMonth + 1;
                        year = $scope.CurrentDisplayYear;
                    }
                }

                var strDate = year + "-" + ('0' + (month + 1)).slice(-2) + "-" + ('0' + (1)).slice(-2);
                //time.SampleDate = $filter('ISODateToDate')(tempDate);

                //var newDate = new Date(year, month, 1)
                $scope.GetManualDays(strDate);
            };

            //#endregion "Manual Schedule"

        }]);