﻿angular
    .module('App.Controllers')
    .controller('ManageUserCtrl', ['$scope', '$http', '$filter', '$timeout', 'administrationService', 'lookupService',
        function ($scope, $http, $filter, $timeout, administrationService, lookupService) {
            $scope.submitted = false;
            $scope.tab2Submitted = false;
            $scope.forms = {};
            $scope.MissingRole = false;
            $scope.SelectedPacket = "";
            $scope.AllRoles = [];
            $scope.Saving = false;
            $scope.GlobalRoles = [];
            $scope.Invalid = false;
            $scope.IsESS = false;
            $scope.User = {};
            $scope.EmployerAccessModel = [];
            $scope.IsAddNew = false;
            $scope.OrganizationName = "";
            $scope.SelectedOrganizationType = "";
            $scope.OrgTypeNotSelected = false;
            $scope.ViewAll = false;
            $scope.DisplayNoResultsMessage = false;
            $scope.ShowOrganizationTab = false;
            $scope.EmployersList = [];
            $scope.EmployerColumns = [];
            $scope.ResetLink = '';
            $scope.ShowPasswordResetLink = false;

            $scope.DisableUser = function () {
                if (confirm("Disable user, " + $scope.User.Email + ".  Are you sure?\n\nClick OK to continue. Cancel to abort.")) {
                    administrationService.DisableUser($scope.User.Id).then(function (data) {
                        if (data.Success) {
                            alert("User " + $scope.User.Email + " disabled successfully");
                            window.location.href = '/user/list';
                        }
                    });
                }
            };

            $scope.EnableUser = function () {
                if (confirm("Restore user, " + $scope.User.Email + ".  Are you sure?\n\nClick OK to continue. Cancel to abort.")) {
                    administrationService.EnableUser($scope.SignUpData).then(function (data) {
                        window.location.reload(false);
                    });
                }
            };

            $scope.ResetUserPassword = function () {
                confirmDialog("Are you sure you want to reset the password for " + $scope.User.Email + "?", function () {
                    administrationService.ResetUserPassword($scope.User.Id).then(function (data) {
                        if (data.Success) {
                            $scope.ResetLink = data.link;
                            $scope.ShowPasswordResetLink = true;
                            $("html, body").animate({ scrollTop: 500 }, 1000);
                        }
                    });
                });
            };

            $scope.ClearPasswordResetLink = function () {
                $scope.ResetLink = '';
                $scope.ShowPasswordResetLink = false;
            };

            function confirmDialog(message, onConfirm) {
                var fClose = function () {
                    modal.modal("hide");
                };
                var modal = $("#confirmModal");
                modal.modal("show");
                $("#confirmMessage").empty().append(message);
                $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
                $("#confirmCancel").unbind().one("click", fClose);
            }

            $scope.UnLockUser = function () {
                administrationService.UnLockUser($scope.User.Id).then(function (data) {
                    if (data.Success) {                        
                        window.location.href = '/user/list';
                    }
                });
            };

           
            $scope.initManageUser = function (user, isESS, isAddNew, enableEmployersByDefault) {
                $scope.User = angular.fromJson(user);
                $scope.IsESS = isESS;
                $scope.IsAddNew = isAddNew;
                $scope.Invalid = false;
                if ($scope.IsAddNew) {
                    $scope.SignUpData = {
                        FirstName: "", LastName: "", Title: "", Password: "", Phone: "", Fax: "", ConfirmPassword: "", Email: "", JobTitle: "", Employers: [], AllRoles: [], ApplyRoles: [], AssignedEmployeeDisplayName: "", AssignedEmployeeId: "",
                        ToDoNotificationFrequency: null
                    };
                }
                else {
                    $scope.SignUpData = {
                        Id: $scope.User.Id, FirstName: $scope.User.FirstName, LastName: $scope.User.LastName, Title: $scope.User.Title, Password: "", ConfirmPassword: "", Email: $scope.User.Email, JobTitle: $scope.User.JobTitle, Employers: [], AllRoles: [], ApplyRoles: [], Phone: $scope.User.Phone, Fax: $scope.User.Fax, EndDate: $scope.User.EndDate,
                        ToDoNotificationFrequency: $scope.User.ToDoNotificationFrequency,
                        IsSelfServiceUser: $scope.User.IsSelfServiceUser
                    };
                    //Get Assigned Employee For User
                    administrationService.GetAssignedEmployeeForUser($scope.User.Id)
                        .then(function (response) {
                            if (response != null) {
                                $scope.SetAssignedEmployeeChanged(response);
                                if (typeof $scope.SignUpData.AssignedEmployeeId != "undefined"
                                    && $scope.SignUpData.AssignedEmployeeId != null
                                    && typeof $scope.SignUpData.AssignedEmployeeDisplayName != "undefined"
                                    && $scope.SignUpData.AssignedEmployeeDisplayName != null
                                    && $scope.SignUpData.AssignedEmployeeDisplayName.replace(/\s/g, "") !== "") {
                                    $scope.ShowOrganizationTab = true;
                                }
                                else
                                    $scope.ShowOrganizationTab = false;
                            }
                            else {
                                $scope.SignUpData.AssignedEmployeeDisplayName = "";
                                $scope.SignUpData.AssignedEmployeeId = "";
                            }
                        });
                }

                $scope.$watch("SignUpData.AssignedEmployeeDisplayName", $scope.OnAssignedEmployeeChanged);

                // Get All Employers list
                lookupService.GetEmployersForCustomer()
                    .then(function (response) {
                        for (var i = 0; i < response.length; i++) {
                            response[i].Show = true;
                        }
                        $scope.EmployersList = response;
                        $scope.EmployersList.sort(function (a, b) {
                            var nameA = a.Name.toLowerCase(), nameB = b.Name.toLowerCase();
                            if (nameA < nameB) //sort string ascending
                                return -1;
                            if (nameA > nameB)
                                return 1;
                            return 0; //default return value (no sorting)
                        });
                        $scope.SplitEmployersAcrossColumns();
                        if ($scope.IsAddNew) {
                            if ($scope.EmployersList.length === 1) {
                                $scope.EmployersList[0].checked = true;
                            } else if (enableEmployersByDefault) {
                                angular.forEach($scope.EmployersList, function (employer, index) {
                                    employer.checked = true;
                                });
                            }
                        }
                        else {
                            angular.forEach($scope.EmployersList, function (employer, index) {
                                employer.checked = $filter("filter")($scope.User.Employers, { Id: employer.Id }).length > 0;
                            });
                        }
                        $scope.GetRoles();
                    });


                // Get Employer Featuer
                lookupService.GetEmployerFeatures()
                    .then(function (result) {
                        if (result) {
                            if (result.MultiEmployerAccess === true || $scope.IsESS === "True") {
                                $scope.ShowEmployerAccess = true;
                            } else {
                                $scope.ShowEmployerAccess = false;
                            }
                        }
                    });

                //roles
                //Code block from here was moved into a scope function $scope.GetRoles to fix AT-2638.
                //This was because of asynchronous nature of service calls and undeterministic 
                //execution order of their respective responses. 
                //This has a dependency on lookupService.GetEmployersForCustomer() above, hence GetRoles is 
                //executed after returning from this lookup service call.
                //If revert is required, paste the content of $scope.GetRoles at this location.

                if (!$scope.IsAddNew) {
                    //Get Current Organization Tree For User
                    $scope.GetCurrentOrganizationTreeForUser();

                    //Get Organization Types
                    // For "Organization"  Tab
                    $scope.GetOrganizationTypeLookUps();
                    //});
                }
            }

            $scope.SetAllEmployers = function (checked) {
                for (var i = 0; i < $scope.EmployersList.length; i++) {
                    $scope.EmployersList[i].checked = checked;
                }
            }

            $scope.FilterEmployers = function () {
                var filterText = $scope.EmployerFilter.toLowerCase();
                var showAll = !filterText;
                for (var i = 0; i < $scope.EmployersList.length; i++) {
                    var employer = $scope.EmployersList[i];
                    employer.Show = showAll || employer.Name.toLowerCase().indexOf(filterText) > -1;
                }
                $scope.SplitEmployersAcrossColumns();
            }

            $scope.SplitEmployersAcrossColumns = function () {
                $scope.EmployerColumns = [];
                var numberOfColumns = 3;
                var employersToShow = [];
                for (var i = 0; i < $scope.EmployersList.length; i++) {
                    var employer = $scope.EmployersList[i];
                    if (employer.Show) {
                        employersToShow.push(employer);
                    }
                }
                var employersPerColumn = parseInt(employersToShow.length / numberOfColumns, 10);
                var extraEmployers = parseInt(employersToShow.length % numberOfColumns, 10);
                for (var i = 0; i < numberOfColumns; i++) {
                    var employersToGet = employersPerColumn;
                    var employersToSkip = i * employersPerColumn;
                    var employersTaken = 0;
                    if (i < extraEmployers) {
                        employersToGet++;
                        employersToSkip += i;
                    } else {
                        employersToSkip += extraEmployers;
                    }

                    var columnList = [];
                    for (var j = 0; j < employersToShow.length; j++) {
                        var employer = employersToShow[j];
                        if (employer.Show && employersTaken < employersToGet && j >= employersToSkip) {
                            columnList.push(employer);
                            employersTaken++;
                        }
                    }

                    $scope.EmployerColumns.push(columnList);
                }
            }

            $scope.GetRoles = function () {
                administrationService.GetRoles()
                    .then(function (data) {
                        if (data != null) {
                            angular.copy(data, $scope.SignUpData.AllRoles);
                            angular.copy(data, $scope.AllRoles);
                            angular.copy(data, $scope.GlobalRoles);

                            // Making model ready for employer access tab
                            angular.forEach($scope.EmployersList, function (employer, index) {
                                var allRoles = [];
                                angular.copy($scope.AllRoles, allRoles);
                                if ($scope.IsAddNew) {
                                    $scope.EmployerAccessModel.push({
                                        UserId: "",
                                        EmployerId: employer.Id,
                                        EmployerName: employer.Name,
                                        Roles: [],
                                        AllRoles: allRoles
                                    });
                                } else {
                                    var emp = $filter("filter")($scope.User.Employers, { Id: employer.Id });
                                    if (emp.length > 0) {
                                        $scope.EmployerAccessModel.push({
                                            UserId: $scope.User.Id,
                                            EmployerId: emp[0].Id,
                                            EmployerName: emp[0].Name,
                                            Roles: emp[0].Roles,
                                            AllRoles: allRoles
                                        });
                                    } else {
                                        $scope.EmployerAccessModel.push({
                                            UserId: $scope.User.Id,
                                            EmployerId: employer.Id,
                                            EmployerName: employer.Name,
                                            Roles: [],
                                            AllRoles: allRoles
                                        });
                                    }
                                }
                            });

                            if ($scope.IsAddNew) {
                                angular.forEach($scope.SignUpData.AllRoles, function (userRole, userIndex) {
                                    var roleDetail = {
                                        Id: userRole.Id,
                                        Apply: false,
                                        Type: userRole.Type
                                    };
                                    $scope.SignUpData.ApplyRoles.push(roleDetail);
                                });
                            } else {

                                $timeout(function () {
                                    // For "Employer Access" tab

                                    angular.forEach($scope.EmployerAccessModel, function (employerAccessModel, employerAccessModelIndex) {
                                        angular.forEach(employerAccessModel.AllRoles, function (employerAccessRole, employerAccessRoleIndex) {
                                            if (employerAccessModel.Roles.indexOf(employerAccessRole.Id) !== -1)
                                                employerAccessRole.Apply = true;
                                            else
                                                employerAccessRole.Apply = false;
                                        });
                                    });

                                    // For "Roles" Tab
                                    angular.forEach($scope.SignUpData.AllRoles, function (userRole, userIndex) {
                                        var roleDetail = {
                                            Id: userRole.Id,
                                            Apply: false,
                                            Type: userRole.Type
                                        };

                                        if ($scope.User.Roles.indexOf(userRole.Id) > -1) {
                                            roleDetail.Apply = true;
                                        } else {
                                            roleDetail.Apply = false;
                                        }

                                        $scope.SignUpData.ApplyRoles.push(roleDetail);
                                        $scope.$apply();
                                    });

                                    $scope.$watch("OrganizationName", $scope.OnOrganizationNameChanged);
                                    $scope.$watch("SelectedOrganizationType", $scope.OnOrganizationTypeChanged);
                                },
                                    500);
                            }
                        }
                    });
            }

            $scope.GetOrganizationTypeLookUps = function () {
                lookupService.GetOrganizationTypes($scope.User.Id)
                    .then(function (response) {
                        $scope.OrganizationTypes = response;
                        // For "Organization"  Tab
                        angular.forEach($scope.OrganizationTypes, function (type, index) {
                            var emp = $filter("filter")($scope.User.Employers, { Id: type.EmployerId });
                            if (emp.length > 0) {
                                type.EmployerName = emp[0].Name;
                            }
                            else {
                                type.EmployerName = "";
                            }
                        });
                    });
            }

            $scope.OnAssignedEmployeeChanged = function (changed) {
                if (changed && changed.value) {
                    $scope.SetAssignedEmployeeChanged(changed.value);
                }
            }

            $scope.SetAssignedEmployeeChanged = function (value) {
                if (typeof value.Id != "undefined") {
                    $scope.SignUpData.AssignedEmployeeDisplayName = value.Name + " - " + value.EmployeeNumber + " - " + "(" + value.EmployerName + ")";
                    $scope.SignUpData.AssignedEmployeeId = value.Id;
                    $scope.SignUpData.AssignedEmployeeEmployerId = value.EmployerId;
                    $scope.OrganizationTreeForUser = [];
                    $scope.DisplayNoResultsMessage = false;
                }
            }

            $scope.OnOrganizationTypeChanged = function (newValue, oldValue) {
                if (typeof newValue != "undefined"
                    && newValue != null
                    && newValue !== "") {
                    $scope.GetOrganizationTreeForUser($scope.SelectedOrganizationType, $scope.OrganizationName);
                }
            }

            $scope.OnOrganizationNameChanged = function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    $scope.GetOrganizationTreeForAssignment();
                    $scope.ViewAll = false;
                }
            }

            $scope.CreateUser = function (isValid) {
                if (isValid) {
                    $scope.Saving = true;
                    $scope.SignUpData.Employers = [];
                    $scope.Invalid = true;
                    angular.forEach($scope.EmployersList, function (employer, index) {
                        if (employer.checked) {
                            $scope.SignUpData.Employers.push(employer.Id);
                            $scope.Invalid = false;
                        }
                    });

                    if ($scope.Invalid) {
                        $scope.Saving = false;
                        return;
                    }

                    $scope.MissingRole = true;
                    angular.forEach($scope.SignUpData.ApplyRoles, function (role, index) {
                        if (role.Apply) {
                            $scope.MissingRole = false;
                        }
                    });

                    if (typeof $scope.SignUpData.AssignedEmployeeDisplayName == "undefined"
                        || $scope.SignUpData.AssignedEmployeeDisplayName == null
                        || $scope.SignUpData.AssignedEmployeeDisplayName.replace(/\s/g, "") === "") {
                        $scope.SignUpData.AssignedEmployeeEmployeeId = "";
                        $scope.SignUpData.AssignedEmployeeEmployerId = "";
                    }

                    // Save Employer Access                 
                    angular.forEach($scope.EmployerAccessModel, function (employer, index) {
                        if (employer.Roles.length > 0)
                            employer.Roles = [];

                        angular.forEach(employer.AllRoles, function (employerRole, employerIndex) {
                            if (employerRole.Apply === true)
                                employer.Roles.push(employerRole.Id);
                        });
                    });

                    if ($scope.MissingRole === false) {
                        if ($scope.IsAddNew) {
                            administrationService.AddUser($scope.SignUpData)
                                .then(function (data) {
                                    if (data.Error == null) {
                                        angular.forEach($scope.EmployerAccessModel, function (employer, index) {
                                            employer.UserId = data.Id;
                                        });

                                        // Sync the roles for a single employer to that of the current user roles.
                                        if ($scope.EmployerAccessModel
                                            && $scope.EmployerAccessModel.length === 1
                                            && $scope.ShowEmployerAccess === false) {
                                            $scope.EmployerAccessModel[0].Roles = [];
                                            angular.forEach($scope.SignUpData.ApplyRoles, function (applyRole, index) {
                                                if (applyRole.Apply === true) {
                                                    $scope.EmployerAccessModel[0].Roles.push(applyRole.Id);
                                                }
                                            });
                                        }

                                        var employerAccessList = $scope.CopyEmployerAccessModel();

                                        // Update employer access
                                        administrationService.UpdateEmployerAccess(employerAccessList)
                                            .then(function (response) {
                                                if (response.Error == null || response.Error === "") {
                                                    $scope.RefreshDetails(data);
                                                }
                                                else {
                                                    $scope.Saving = false;
                                                    $scope.submitted = false;
                                                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "Error while adding user" });
                                                }
                                            });
                                    }
                                    else {
                                        $scope.submitted = false;
                                        $scope.Saving = false;
                                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.Error });
                                    }

                                }, $scope.ErrorHandler);
                        }
                        else {
                            administrationService.UpdateUserInfo($scope.SignUpData)
                                .then(function (result) {
                                    if (result.Error == null) {
                                        $scope.RefreshDetails(result);
                                    }
                                    else {
                                        $scope.submitted = false;
                                        $scope.Saving = false;
                                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.Error });
                                    }
                                },
                                $scope.ErrorHandler);
                        }
                    }
                    else {
                        $scope.Saving = false;
                        $scope.submitted = false;
                        $('a[href=".user-roles"]').tab('show');
                    }
                }
            }

            $scope.RefreshDetails = function (data) {
                $scope.submitted = false;
                $scope.forms.frmAddUser.$setPristine();

                if ($scope.IsAddNew) {
                    msg = "User <strong>" + data.FirstName + ", " + data.LastName + "</strong> has been added to company <strong>" + data.CompanyName + "</strong> successfully";
                }
                else {
                    msg = "User <strong>" + data.FirstName + ", " + data.LastName + "</strong> has been updated successfully";
                }


                var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: msg });

                if ($scope.IsAddNew) {
                    $timeout(function () { window.location.href = "/user/list"; }, 1000);
                }

                if (window.location.href.indexOf('employerAccess') > -1) {
                    $timeout(function () { window.location.href = window.location.href.replace("#/employerAccess", ""); }, 1000);
                }
            }

            $scope.Back = function () {
                window.location.href = "/administration";
            }

            $scope.SuccessAction = function (action) {
                $("#SuccessMessage").modal('hide');
                if (action === "addUser")
                    window.location.href = "/administration/adduserinfo";
                else
                    window.location.href = "/administration";
            }

            $scope.cancel = function () {
                window.location.href = "/administration";
            };

            $scope.Save = function (isValid) {
                if (!isValid) {
                    $('a[href=".info"]').tab("show");
                    return;
                }

                if ($scope.IsAddNew) {
                    $scope.CreateUser(isValid);
                    if ($scope.Invalid)
                        return;
                }
                else {
                    // Save user general info and roles
                    $scope.MissingRole = true;

                    angular.forEach($scope.SignUpData.ApplyRoles, function (role, index) {
                        if (role.Apply) {
                            $scope.MissingRole = false;
                        }
                    });

                    $scope.SignUpData.Employers = [];
                    angular.forEach($scope.EmployersList, function (employer, index) {
                        if (employer.checked === true) {
                            $scope.SignUpData.Employers.push(employer.Id);
                        }
                    });

                    if (typeof $scope.SignUpData.AssignedEmployeeDisplayName == "undefined"
                        || $scope.SignUpData.AssignedEmployeeDisplayName == null
                        || $scope.SignUpData.AssignedEmployeeDisplayName.replace(/\s/g, "") === "") {
                        $scope.SignUpData.AssignedEmployeeId = "";
                        $scope.SignUpData.AssignedEmployeeEmployerId = "";
                    }

                    // Save Employer Access 
                    angular.forEach($scope.EmployerAccessModel, function (employer, index) {
                        employer.Roles = [];

                        angular.forEach(employer.AllRoles, function (employerRole, employerIndex) {
                            if (employerRole.Apply === true)
                                employer.Roles.push(employerRole.Id);
                        });
                    });

                    if ($scope.ShowEmployerAccess)
                        $scope.MissingRole = false;

                    if ($scope.MissingRole === false) {
                        // Sync the roles for a single employer to that of the current user roles.
                        if ($scope.EmployerAccessModel &&
                            $scope.EmployerAccessModel.length === 1 &&
                            $scope.ShowEmployerAccess === false) {
                            $scope.EmployerAccessModel[0].Roles = [];
                            angular.forEach($scope.SignUpData.ApplyRoles, function (applyRole, index) {
                                if (applyRole.Apply === true) {
                                    $scope.EmployerAccessModel[0].Roles.push(applyRole.Id);
                                }
                            });
                        } else {
                            angular.forEach($scope.SignUpData.ApplyRoles, function (applyRole, index) {
                                if (applyRole.Apply === true) {
                                    $scope.EmployerAccessModel[0].Roles.push(applyRole.Id);
                                }
                            });
                        }

                        // Update user general info and roles
                        administrationService.UpdateUserInfo($scope.SignUpData)
                            .then(function (result) {
                                if (!result.Error) {
                                    var employerAccessList = $scope.CopyEmployerAccessModel();

                                    // Update employer access
                                    administrationService.UpdateEmployerAccess(employerAccessList)
                                        .then(function (response) {
                                            if (!response.Error) {
                                                noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'User information successfully updated.'});
                                                if (typeof $scope.SignUpData.AssignedEmployeeId != "undefined"
                                                    && $scope.SignUpData.AssignedEmployeeId != null
                                                    && typeof $scope.SignUpData.AssignedEmployeeDisplayName != "undefined"
                                                    && $scope.SignUpData.AssignedEmployeeDisplayName != null
                                                    && $scope.SignUpData.AssignedEmployeeDisplayName.replace(/\s/g, "") !== "") {
                                                    $scope.GetCurrentOrganizationTreeForUser();
                                                    $scope.GetOrganizationTypeLookUps();
                                                    $scope.ShowOrganizationTab = true;
                                                }
                                                else { $scope.ShowOrganizationTab = false; }
                                            }
                                            else {
                                                // Update Employer Access Failed
                                                noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.Error });
                                            }
                                        });
                                }
                                else {
                                    // Update User Failed
                                    noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.Error });
                                }
                            },
                            $scope.ErrorHandler);
                    }
                }

            }

            $scope.CopyEmployerAccessModel = function () {
                var accessList = [];
                for (var i = 0; i < $scope.EmployerAccessModel.length; i++) {
                    var employerAccess = $scope.EmployerAccessModel[i];
                    accessList.push({
                        UserId: employerAccess.UserId,
                        EmployerId: employerAccess.EmployerId,
                        Error: employerAccess.Error,
                        AutoAssignCases: employerAccess.AutoAssignCases,
                        Roles: employerAccess.Roles
                    })
                }


                return {
                    EmployerAccessList: accessList
                };
            }

            $scope.GoToGeneralInfo = function () {
                $('a[href=".info"]').tab("show");
            };

            $scope.Range = function (min, max) {
                var input = [];
                for (var i = min; i < max; i++) {
                    input.push(i);
                }
                return input;
            };

            $scope.ViewAllOrganizationTrees = function () {
                $scope.ViewAll = true;
                $scope.OrgTypeNotSelected = false;
                $scope.GetOrganizationTreeForUser("", "");
            }

            $scope.GetOrganizationTreeForAssignment = function () {
                if (typeof $scope.SelectedOrganizationType === "undefined"
                    || $scope.SelectedOrganizationType == null
                    || $scope.SelectedOrganizationType === "") {
                    $scope.OrgTypeNotSelected = true;
                    return;
                }
                else
                    $scope.OrgTypeNotSelected = false;

                $scope.GetOrganizationTreeForUser($scope.SelectedOrganizationType, $scope.OrganizationName);
            }

            $scope.SaveOrganizationVisibilityForUser = function () {
                var model = {
                    OrganizationVisibilityTrees: $scope.OrganizationTreeForUser
                }
                administrationService.SaveOrganizationVisibilityForUser($scope.User.Id, model)
                    .then(function (response) {
                        if (response.Success) {
                            var msg = "Organization Visibility successfully assigned to User <strong>" + $scope.SignUpData.FirstName + ", " + $scope.SignUpData.LastName;
                            var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: msg });
                            //Get Current Organization Tree For User
                            $scope.GetCurrentOrganizationTreeForUser();
                        }
                    });
            }

            $scope.GetCurrentOrganizationTreeForUser = function () {
                administrationService.GetCurrentOrganizationTreeForUser($scope.User.Id)
                    .then(function (response) {
                        $scope.CurrentOrganizationTreeForUser = response;
                    });
            }

            $scope.GetOrganizationTreeForUser = function (selectedOrganizationType, organizationName) {
                administrationService.GetOrganizationTreeForUser($scope.User.Id, selectedOrganizationType, organizationName)
                    .then(function (response) {
                        $scope.OrganizationTreeForUser = response;
                        if ($scope.OrganizationTreeForUser.length > 0) {
                            $scope.DisplayNoResultsMessage = false;
                        }
                        else { $scope.DisplayNoResultsMessage = true; }
                    });
            }

            $scope.ExpandCollapseOrganization = function (org, expand) {
                if (org.Expanded !== expand) {
                    org.Expanded = expand;
                    if (typeof org.ChildOrganizationVisibilityTree != "undefined"
                        && org.ChildOrganizationVisibilityTree != null
                        && org.ChildOrganizationVisibilityTree.length > 0
                        && expand === false) {
                        angular.forEach(org.ChildOrganizationVisibilityTree, function (childOrg, index) {
                            $scope.ExpandCollapseOrganization(childOrg, expand);
                        });
                    }
                }
            }

            $scope.LoadOrganization = function (org) {
                $scope.GetOrganizationTreeForUser(org.OrganizationTypeCode, org.OrganizationName);
            }

            $scope.ResetAll = function (org) {
                if (typeof $scope.OrganizationTreeForUser != "undefined"
                    && $scope.OrganizationTreeForUser != null
                    && $scope.OrganizationTreeForUser.length > 0) {
                    angular.forEach($scope.OrganizationTreeForUser, function (orgTree, index) {
                        $scope.ResetOrgVisibility(orgTree);
                    });
                }
            }

            $scope.ResetOrgVisibility = function (org) {
                org.Affiliation = "-1";
                if (typeof org.ChildOrganizationVisibilityTree != "undefined"
                    && org.ChildOrganizationVisibilityTree != null
                    && org.ChildOrganizationVisibilityTree.length > 0) {
                    angular.forEach(org.ChildOrganizationVisibilityTree, function (childOrg, index) {
                        $scope.ResetOrgVisibility(childOrg);
                    });
                }
            }
        }]);
