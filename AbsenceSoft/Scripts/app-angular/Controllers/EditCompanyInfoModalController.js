//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: enable strict mode and fix any violations
(function (undefined) {
    'use strict';

    angular

    .module('App.Controllers')

    .controller('EditCompanyInfoModalCtrl', ['$scope', '$http', 'administrationService', '$filter', '$modalInstance', '$rootScope', function ($scope, $http, administrationService, $filter, $modalInstance, $rootScope) {
        $scope.SignUpData = {
            Customer: {
                Name: "", SubDomain: "", Address1: "", Address2: "", Country: "US", State: "", City: "", PostalCode: "", Phone: "", Fax: ""
            }
        }

        $scope.forms = {};


        $scope.init = function (model) {
            administrationService.GetCompanyInfo().then(function (data) {
                if (data != null) {
                    $scope.SignUpData.Customer = data;

                    $scope.SignUpData.Customer.Phone = $scope.SignUpData.Customer.Phone == "" ? "" : $filter("tel")($scope.SignUpData.Customer.Phone);
                    $scope.SignUpData.Customer.Fax = $scope.SignUpData.Customer.Fax == "" ? "" : $filter("tel")($scope.SignUpData.Customer.Fax);

                    if ($scope.SignUpData.Customer.LogoId != null && $scope.SignUpData.Customer.LogoId != undefined && $scope.SignUpData.Customer.LogoId != "") {
                        $scope.$broadcast('SetLogo');
                    }
                }
            })


            $scope.$watch('SignUpData.Customer.PostalCode', function () {
                $scope.PostalCodeValidationIn2();
            });

            $scope.$watch('SignUpData.Customer.Country', function () {
                $scope.PostalCodeValidationIn2();
            });
        };

        $scope.PostalCodeValidationIn2 = function () {
            if ($scope.SignUpData.Customer.PostalCode == null || $scope.SignUpData.Customer.PostalCode == '' || $scope.SignUpData.Customer.PostalCode == undefined)
                return;

            if ($scope.SignUpData.Customer.Country == null || $scope.SignUpData.Customer.Country == '' || $scope.SignUpData.Customer.Country == undefined)
                return;

            if ($scope.SignUpData.Customer.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.SignUpData.Customer.PostalCode)) {
                    $scope.forms.frmSignUpStep2.PostalCode.$setValidity('valid', false);
                    return;
                }
            }

            $scope.forms.frmSignUpStep2.PostalCode.$setValidity('valid', true);
        };

        $scope.PhoneRequirements = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        $scope.PostalCodeRquirements = /^[0-9]*$/;
        $scope.IsCompanyinfoEdit = true;

        $scope.Error = null;

        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.UpdateCompanyInfo = function (IsValid) {
            if (IsValid) {
                administrationService.UpdateCompanyInfo($scope.SignUpData.Customer).then(function (result) {
                    if (result.Error == null) {
                        //var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Company info updated successfully." });
                        $modalInstance.dismiss('cancel');

                        // Refresh parent page
                        administrationService.GetBasicInfo().then(function (data) {
                            if (data != null) {
                                $rootScope.BasicInfo = data;
                            }
                        });

                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.Error });
                    }

                },
                $scope.ErrorHandler);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);

})();
