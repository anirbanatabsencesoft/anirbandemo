(function () {
    'use strict';

    angular

        .module('App.Controllers')

        .controller('ResetPasswordCtrl', ['$window', '$scope', '$http', '$location', function ($window, $scope, $http, $location) {
            $scope.Resetkey = $location.absUrl().split('/')[4];
            $scope.PasswordRequirements = /^.*(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[\d\W\s]).*$/;
            $scope.ResetPassword = function (isValid) {
                if (isValid) {
                    $http({
                        method: 'POST', url: "/ResetPassword/" + $scope.Resetkey, data: { newPassword: $scope.NewPassword, confirmPassword: $scope.confirmPassword }})
                        .then(function (response) {
                            $window.location.href = "/successresetpassword/";
                        }, function (errorRresponse) {
                            console.log(errorRresponse);
                        });
                }
            }
        }]);

})();
