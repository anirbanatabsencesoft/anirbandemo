//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployerAccessCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.openEmployerAccessModal = function (user) {
        var AddUserModalInstance = $modal.open({
            templateUrl: 'EmployerAccess.html',
            controller: 'EmployerModalCtrl',
            resolve: {
                user: function () {
                    return user;
                }
            }
        });

        //AddUserModalInstance.result.then(function () {
        //}, function () {
        //    $("#disabledAdminPage").scope().GetUsers(false);
        //});
    };

}]);