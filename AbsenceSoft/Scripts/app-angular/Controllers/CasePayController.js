//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('CasePayCtrl', ['$scope', '$http', '$filter', 'lookupService', 'employeeService', 'caseService', 'payScheduleService',
    function ($scope, $http, $filter, lookupService, employeeService, caseService, payScheduleService) {

        $scope.CasePay = {};
        $scope.expand = false;

        $scope.forms = {};
        $scope.forms.submitted = false;
        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.forms.submitted = false;
            $scope.Error = response;
        };
        $scope.ShowEditBasePay = false;
        $scope.ShowEditOffset = false;

        $scope.OriginalBenefitPercentage = 0;
        $scope.EditPaymentPercentage = 0;
        $scope.EditPaymentAmount = 0;
        $scope.EditOffsetAmount = 0;
        $scope.EditPayPeriod = {};
        $scope.DetailRow = {};

        $scope.Init = function (model) {
            
            $scope.CaseId = model.CaseId;
            $scope.EmployeeId = model.EmployeeId;
            $scope.EmployerId = model.EmployerId;
            payScheduleService.List(model.EmployerId).then(function (paySchedules) {
                $scope.PaySchedules = paySchedules;
            });
            updateCaseDisplay(model);
            $scope.forms.submitted = false;
            
        };

        $scope.ConfirmationCallback;

        displayConfirmation = function (message, callback) {
            $scope.ConfirmationMessage = message;
            $scope.ConfirmationCallback = callback;
            $('#confirmationModal').modal('show');
        }

        $scope.Confirmation = function () {
            if ($scope.ConfirmationCallback) {
                $scope.ConfirmationCallback();
                $scope.ConfirmationCallback = null;
                $('#confirmationModal').modal('hide');
            }
        }

        $scope.SubmitToPayroll = function (payPeriod, $event) {
            var message = "Are you sure you want to submit to payroll?";
            $event.stopPropagation();
            displayConfirmation(message, function(){
                updatePayPeriod(payPeriod.PayPeriodId, 2);
            });
        };

        $scope.SubmitToException = function (payPeriod, $event) {
            var message = "Are you sure you want to exclude from payroll?";
            $event.stopPropagation();
            displayConfirmation(message, function () {
                updatePayPeriod(payPeriod.PayPeriodId, 1);
            });
        };



        updateCaseDisplay = function (casePay) {
            for (var i = 0; i < casePay.Policies.length; i++) {
                casePay.Policies[i].BenefitPercentage = parseInt(casePay.Policies[i].BenefitPercentage * 100, 10);
            }
            for (var i = 0; i < casePay.PayPeriods.length; i++) {
                var payPeriod = casePay.PayPeriods[i];
                payPeriod.PayThru = $filter('ISODateToDate')(payPeriod.PayThru);
                payPeriod.PayDate = $filter('ISODateToDate')(payPeriod.PayDate);
                payPeriod.Percentage *= 100;
                for (var j = 0; j < payPeriod.Detail.length; j++) {
                    payPeriod.Detail[j].BenefitPercentage = parseInt(payPeriod.Detail[j].BenefitPercentage * 100, 10);
                }
            }
            
            $scope.CasePay = casePay;
            $scope.EditPaySchedule = false;
        }

        updatePayPeriod = function (payPeriodId, newStatus) {
            var model = {
                CaseId: $scope.CaseId,
                PayPeriodId: payPeriodId,
                EmployerId: $scope.EmployerId,
                Status: newStatus
            };
            caseService.UpdatePayPeriod(model).then(updateCaseDisplay);
        }

        $scope.EditBenefitPercentage = function (policy) {
            var message = "By changing the current percentage value all payments which have not been submitted will reflect the percentage you entered. Further no system driven pay percentage calculation will occur on this case once you make this change and you will be responsible for providing manual updates to benefit payment percentage for the remainder of this case. AbsenceSoft recommends consultation with your designated HR leadership prior to making this change. Do you wish to proceed?";
            displayConfirmation(message, function () {
                $scope.OriginalBenefitPercentage = policy.BenefitPercentage;
                policy.EditBenefitPercentage = true;
            })
        }

        $scope.SaveEditPolicyBenefitPercentage = function (policy) {
            var model = {
                CaseId: $scope.CaseId,
                PolicyId: policy.PolicyCode,
                Amount: policy.BenefitPercentage/100
            }
            
            caseService.UpdatePolicyBenefitPercentage(model).then(updateCaseDisplay);
        }

        $scope.CancelEditPolicyBenefitPercentage = function (policy) {
            policy.BenefitPercentage = $scope.OriginalBenefitPercentage;
            policy.EditBenefitPercentage = false;
        }

        $scope.EditBasePay = function () {
            $scope.ShowEditBasePay = true;
        }

        $scope.CancelEditBasePay = function () {
            $scope.ShowEditBasePay = false;
        }

        $scope.SaveEditBasePay = function () {
            var model = {
                CaseId: $scope.CaseId,
                Amount: $scope.CasePay.BasePay
            };

            caseService.UpdateBasePay(model).then(updateCaseDisplay);
            $scope.ShowEditBasePay = false;
        }

        $scope.SaveEditPaySchedule = function () {
            var model = {
                CaseId: $scope.CaseId,
                PayScheduleId: $scope.CasePay.PayScheduleId
            }
            caseService.UpdatePaySchedule(model).then(updateCaseDisplay);
        }

        $scope.EditPayPeriodPayDate = function (payPeriod, $event) {
            prepDetailRowEdit(null, payPeriod);
            $scope.EditPayPeriod.ShowEditPayDate = true;
            $event.stopPropagation();
        }

        $scope.EditPayPeriodEndDate = function (payPeriod, $event) {
            prepDetailRowEdit(null, payPeriod);
            $scope.EditPayPeriod.ShowEditEndDate = true;
            $event.stopPropagation();
        }

        $scope.EditDetailPaymentAmount = function (detailRow, payPeriod) {
            prepDetailRowEdit(detailRow, payPeriod);
            $scope.EditPayPeriod.ShowEditPaymentAmount = true;
        }

        $scope.EditDetailPaymentPercentage = function (detailRow, payPeriod) {
            prepDetailRowEdit(detailRow, payPeriod);
            $scope.EditPayPeriod.ShowEditPaymentPercentage = true;
        }

        prepDetailRowEdit = function (detailRow, payPeriod) {
            $scope.EditPayPeriod = payPeriod;
            $scope.DetailRow = detailRow;
            $scope.HideLineItemEdit();
        }

        $scope.SaveEditPaymentPercentage = function () {
            $scope.DetailRow.CaseId = $scope.CaseId;
            $scope.DetailRow.BenefitPercentage /= 100;
            caseService.ChangeDetailBenefitPercentage($scope.DetailRow).then(updateCaseDisplay);
        }

        $scope.SaveEditPaymentAmount = function () {
            $scope.DetailRow.CaseId = $scope.CaseId;
            caseService.ChangeDetailBenefitAmount($scope.DetailRow).then(updateCaseDisplay);
        }

        $scope.SaveEditPayPeriodEndDate = function () {
            $scope.EditPayPeriod.CaseId = $scope.CaseId;
            caseService.ChangePayPeriod($scope.EditPayPeriod).then(updateCaseDisplay);
        }

        $scope.SaveEditPayPeriodPayDate = function () {
            $scope.EditPayPeriod.CaseId = $scope.CaseId;
            caseService.ChangePayPeriod($scope.EditPayPeriod).then(updateCaseDisplay);
        }

        $scope.ChangeDetailIsOffset = function (detailRow, isOffset) {
            if (isOffset != null && isOffset != undefined)
                detailRow.IsOffset = isOffset;

            detailRow.CaseId = $scope.CaseId;
            caseService.ChangeDetailIsOffset(detailRow).then(updateCaseDisplay);
        }

        $scope.ChangeApplyOffsetsByDefault = function(applyOffsetsByDefault)
        {
            if ($scope.CasePay.ApplyOffsetsByDefault == applyOffsetsByDefault)
                return;

            var model = {
                CaseId: $scope.CaseId,
                ApplyOffsetsByDefault: applyOffsetsByDefault
            }

            caseService.ChangeApplyOffsetsByDefault(model).then(updateCaseDisplay);
        }

        $scope.HideLineItemEdit = function () {
            $scope.EditPayPeriod.ShowEditPaymentAmount = false;
            $scope.EditPayPeriod.ShowEditPaymentPercentage = false;
            $scope.EditPayPeriod.ShowEditPayDate = false;
            $scope.EditPayPeriod.ShowEditEndDate = false;
        }

        $scope.SetWaiveWaitingPeriod = function (waiveWaitingPeriod) {
            if (waiveWaitingPeriod != $scope.CasePay.WaiveWaitingPeriod)
            {
                var model = {
                    CaseId: $scope.CaseId,
                    Waive: waiveWaitingPeriod
                }
                caseService.WaiveWaitingPeriod(model).then(updateCaseDisplay);
            }
            
        }


        $scope.UpdatePaySchedule = function () {

            var model = {
                EmployeeId: $scope.EmployeeId,
                EmployerId: $scope.EmployerId,
                PayScheduleId: $scope.PayScheduleId
            };

            employeeService.UpdatePaySchedule(model).then($scope.UpdatePayScheduleDisplay, $scope.ErrorHandler);

            var paySched = $filter('filter')($scope.STDPayEdit.PaySchedules, { Id: $scope.PayScheduleId });
            if (paySched.length > 0)
                $scope.STDPayEdit.CasePaymentSummary.CurrentPaySchedule = paySched[0].Name;

            $("#EditPaySchedule").modal('hide');
        };

        $scope.SetPayDates = function () {
            if ($scope.PayScheduleId == undefined)
                return;


        };

        $scope.GoToCase = function () {
            window.location.href = "/cases/" + $scope.CaseId + "/view";
        }

    }]);