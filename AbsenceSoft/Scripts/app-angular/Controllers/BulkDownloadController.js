//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('BulkDownloadCtrl', ['$scope', '$modalInstance', '$http', '$interval', 'noteService', 'caseId',
    function ($scope, $modalInstance, $http, $interval, noteService, caseId) {

        $scope.CaseExportId = '';
        $scope.DownloadType = 'PDF';
        $scope.CaseId = caseId;
        $scope.CaseNumber = '';
        $scope.ProcessingStatus = "Pending";
        $scope.TimeElapsed = "0s";
        $scope.FileKey = "";
        $scope.ShowProcessing = false;




        $scope.GetExportLink = function () {

            $http({
                method: 'POST',
                url: '/BulkDownload/GetExportLink/?key=' + $scope.FileKey,
            }).then(function (response) {
                window.open(response.data.url, "_blank");
            })
        }

        $scope.ChangeDownloadType = function (newValue) {
            $scope.DownloadType = newValue;


        }

        $scope.StartProcessing = function () {
            var model = {
                CaseId: caseId,
                DownloadType: $scope.DownloadType
            }

            noteService.CaseExportDownload(model).then(setProcessingStatus);


        }

        setProcessingStatus = function (model) {
            $scope.ShowProcessing = true;
            $scope.CaseExportId = model.caseExportId;

        }

        $scope.CheckDownloadStatus = function () {
            var model = {
                CaseExportRequestId: $scope.CaseExportId
            }
            $http({
                method: 'POST',
                url: '/BulkDownload/GetCaseBulkDownloadStatus',
                data: model
            }).then(function (response) {
                var data = response.data;
                $scope.TimeElapsed = data.Elapsed;
                $scope.FileKey = data.FileKey;

                if (data.Status == 0) {
                    $scope.ProcessingStatus = "Pending";
                }
                else if (data.Status == 1) {
                    $scope.ProcessingStatus = "Processing";
                }
                else if (data.Status == 2) {
                    $scope.ProcessingStatus = "Complete";
                    $scope.FileName = data.FileName;
                    $interval.cancel($scope.myCall);
                }
                else if (data.Status == -1) {
                    $scope.ProcessingStatus = "Failed";
                    $scope.Errors = data.Errors;
                }
            })

        }


        $scope.myCall = $interval($scope.CheckDownloadStatus, 1000);


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);
