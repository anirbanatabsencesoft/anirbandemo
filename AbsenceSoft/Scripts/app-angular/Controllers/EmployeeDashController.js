//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployeeDashCtrl', ['$scope', '$route', '$routeParams', '$location', 'employeeCaseListManager', function ($scope, $route, $routeParams, $location, employeeCaseListManager) {
    $scope.showSearch = false;

    $scope.Init = function () {
        if ($location.path().indexOf("/employee-new-accomm-create/") >= 0) {
            $route.reload();
            $scope.showSearch = true;
            $location.url($location.path());
        }
        else if ($location.path().indexOf("/employee-case-create/") >= 0) {
            $route.reload();
            $scope.showSearch = true;
            $location.url($location.path());
        }
        else {
            if ($scope.IsHRSupervisor) {
                $scope.SetMyCasesActive("MyTeam");
                $scope.showSearch = true;
            }
            else {
                $scope.SetMyCasesActive("MyInfo");
                $scope.showSearch = false;
            }
        }
    }

    $scope.SetViewMe = function () {
        $scope.SetMyCasesActive("MyInfo");
        $scope.showSearch = false;
    }

    $scope.SetViewMyTeam = function () {
        $scope.SetMyCasesActive("MyTeam",true);
        $scope.showSearch = true;
    }

    $scope.SetMyInfoActive = function (tab, isTabActive) {
        $route.reload();
        if (tab == "MyInfo") {
            $location.url("/employee-info");
            if (isTabActive) {
                $("#viewMyInfo").tab('show');
            }
        }
        else {
            $location.url("/employee-team")
        }
    }

    $scope.SetMyCasesActive = function (tab, isTabActive) {
        $route.reload();
        if (tab == "MyInfo") {
            $location.url("/employee-cases");
            if (isTabActive) {
                $("#viewMyCases").tab('show');
            }
        }
        else {
            $location.url("/employee-team-cases")
            if (isTabActive) {
                $("#viewMyTeamCases").tab('show');
            }
        }
    }

    $scope.SetReqNewLeaveActive = function (tab) {
        $route.reload();
        if (tab == "MyInfo") {
            //window.location.href = "/employees/" + $scope.CurrentUserEmpId + "/Cases/Create";
            $location.url("/employee-case-create")
        }
        else {
            $location.url("/employee-todos")
        }
    }
    $scope.SetReqNewAccomActive = function (tab) {
        $route.reload();
        if (tab == "MyInfo") {
            //  window.location.href = "/employees/" + $scope.CurrentUserEmpId + "/Cases/Accom";
            $location.url("/employee-new-accomm-create");
            angular.element(document).ready(function () {

                $("#requestAccomm").tab('show');
            })
        }
        else {
            window.location.href = "/reports#/home";
        }
    }
}]);