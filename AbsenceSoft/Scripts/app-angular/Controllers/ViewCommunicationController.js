//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('ViewCommunicationCtrl', ['$scope', '$http', 'communicationService', '$sanitize', '$sce',
    function ($scope, $http, communicationService, $sanitize, $sce) {
        $scope.CommunicationViewModel = null;

        $scope.init = function (caseId, communicationId) {
            $scope.caseId = caseId;
            $scope.communicationId = communicationId;

            $scope.GetCommunication(communicationId);
        };

        $scope.GetCommunication = function (communicationId) {
            $scope.viewCommunication = communicationService.ViewCommunicationJson(communicationId).then(function (response) {
                $scope.Communication = response;
            });

        };

        $scope.deliberatelyTrustDangerousSnippet = function (Msg) {
            return $sce.trustAsHtml(Msg);
        };

    }]);
