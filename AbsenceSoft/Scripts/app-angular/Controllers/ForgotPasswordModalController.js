//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('ForgotPasswordModalCtrl', [ '$scope', '$modalInstance', '$http', function ($scope, $modalInstance, $http) {
    $scope.ResetPassword = function (form) {
        $scope.submitted = true;
        var isValid = form.$valid;
        if (isValid) {
            $http({ method: 'POST', url: "/ForgotPassword/", params: { emailAddress: this.ResetEmail } }).
                then(function (response) {
                    if (response.data != undefined && response.data.success == true) {
                      var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Email has been sent with instructions on how to reset your password.' });
                  }
                  else {
                      var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "We're sorry but we can not find that user, please re-check your login or contact your system administrator." });
                  }
              //}).error(function (response) {
              //    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.error });
              //    $scope.submitted = false;
              //});
                },function (response) {
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.error });
                    $scope.submitted = false;
                });

            $scope.submitted = false;
            $modalInstance.close();
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);