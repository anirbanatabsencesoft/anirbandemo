//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('eligibilityFileDetailCtrl', ['$scope', '$http', 'eligibilityFileDetailService', function ($scope, $http, eligibilityFileDetailService) {

    $scope.init = function (fileId) {
        // File Detail
        eligibilityFileDetailService.EligibilityFileDetails(fileId).then(function (fileInfo) {
            $scope.FileInfo = fileInfo;
            $scope.EmployerId = fileInfo.EmployerId;
            $scope.LinkedEligibilityUploadId = fileInfo.Id;
            if (fileInfo.Status === 0 || fileInfo.Status === 1) {
                setTimeout(function (a) {
                    a.s.refreshFileInfo(a.f.Id);
                }, 5000, { s: $scope, f: fileInfo });
            }

            if ($scope.EmployerId  == null) {
                eligibilityFileDetailService.GetEligibilityDataFilesForEmployers($scope.LinkedEligibilityUploadId).then(function (result) {
                    $scope.lstEligibilityEmployerFiles = result.Uploads;

                });
            }
        });
      
        // File Error list
        eligibilityFileDetailService.ListEligibilityDataFileErrors(fileId).then(function (fileErrors) {           
            $scope.FileErros = fileErrors.Errors;
        });
    }

    $scope.refreshFileInfo = function (fileId) {
        eligibilityFileDetailService.EligibilityFileDetails(fileId).then(function (fileInfo) {
            $scope.FileInfo = fileInfo;
            if (fileInfo.Status === 0 || fileInfo.Status === 1) {
                setTimeout(function (a) {
                    a.s.refreshFileInfo(a.f.Id);
                }, 5000, { s: $scope, f: fileInfo });
            }
        });
        eligibilityFileDetailService.ListEligibilityDataFileErrors(fileId).then(function (fileErrors) {
            $scope.FileErros = fileErrors.Errors;
        });
    }

    
    $scope.showEmployerFileDetails = function (fileId) {
        window.location.href = "/Files/" + fileId + "/EligibilityFileDetails"
    }

    $scope.showErrorDetail = function (error) {
        $scope.ErrorDetail = error;
        $("#ShowErrorDetail").modal('show');
    }

    $scope.Back = function () {
        if ($scope.EmployerId == null) {
            window.location.href = "/Files/EligibilityFileUpload";
        }
        else {
            window.location.href = "/Files/EligibilityFileUpload/" + $scope.EmployerId ;
        }
    }

    $scope.ExportToCSV = function (fileId) {
        window.location.href = "/Files/Errors/" + fileId + "/ToCSV";
    }
}]);