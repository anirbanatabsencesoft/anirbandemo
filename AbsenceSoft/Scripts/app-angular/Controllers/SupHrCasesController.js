//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('SupHrCasesCtrl', ['$scope', '$timeout', 'supHrCaseListManager', function ($scope, $timeout, supHrCaseListManager) {

    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };
    $scope.IsTPA = false;

    $scope.init = function (callingPage) {
        var infiniteScrolling = callingPage == 'index';
        supHrCaseListManager.Init($scope, infiniteScrolling);
    }

    $scope.LoadMoreCases = function () {
        supHrCaseListManager.LoadMore($scope, $scope.ErrorHandler);
    };

    $scope.GetCases = function (doPaging) {
        supHrCaseListManager.Filter($scope, doPaging, $scope.ErrorHandler);
    };

    $scope.UpdateCaseSort = function (sortBy) {
        supHrCaseListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
    };


}]);