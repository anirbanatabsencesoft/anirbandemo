//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EditEmployerInfoModalCtrl', ['$scope', '$http', 'administrationService', 'lookupService', '$filter', '$modalInstance', '$rootScope', 'employerId', '$window', '$timeout', function ($scope, $http, administrationService, lookupService, $filter, $modalInstance, $rootScope, employerId, $window, $timeout) {

    $scope.SignUpData = {};
    $scope.NewAttachments = [];
    $scope.Attachments = [];
    $scope.IsUploadInProgress = false;
    $scope.forms = {};
    $scope.CompanySize = [{ Id: 999, Text: "1 to 999" }, { Id: 4999, Text: "1000 to 4999" }, { Id: null, Text: "5000 Above" }];
    $scope.EmployerIdToEdit = employerId;
    $scope.CustomerServiceIndicatorOptions = [];

    if (employerId != null && employerId != undefined) {
        $scope.IsAddNew = false;
    }
    else {
        $scope.IsAddNew = true;
    }

    $scope.init = function () {
        lookupService.FMLPeriodTypes()
            .$promise.then(function (results) {
                $scope.FMLPeriodTypes = results;
            }
            , $scope.ErrorHandler
            );

        //Get feature "EmployeeSelfService" either from employer (if exists) or customer
        lookupService.GetEmployerFeatures(employerId)
            .then(function(data) {
                if (data != null) {
                    $scope.EmployerFeatures = data;
                    $scope.HasEmployeeSelfService = data.EmployeeSelfService;
                }
            });

        lookupService.GetCustomerServiceIndicatorOptions()
            .then(function (data) {
                if (data != null)
                    $scope.CustomerServiceIndicatorOptions = data;
            });

        $scope.UploadEmployerLogo = function (IsValid, valid) {
            $scope.isDisabled = true;
            if ($scope.IsAddNew) {
                $scope.submitted = true;
                if (IsValid) {
                    $scope.UpdateEmployerInfo(IsValid, function () {
                        $scope.isDisabled = false;
                        $scope.$broadcast('UploadLogo', {
                            EmployerId: $scope.EmployerIdToEdit
                        });
                    }
                    );
                }
                else
                {
                    $scope.isDisabled = false;
                }
            }
            else {
                $scope.$broadcast('UploadLogo', { EmployerId: $scope.EmployerIdToEdit });
                $scope.isDisabled = false;
            }
        }

        //if (employerId != null) {

        //}
        //else
        //    lookupService.GetEmployeeSelfServiceFeature().then(function (response) {
        //        $scope.HasEmployeeSelfService = response.HasEmployeeSelfService;
        //    });

        administrationService.GetEmployerInfo($scope.EmployerIdToEdit).then(function (data) {
            if (data != null) {
                $scope.SignUpData.Employer = data;

                $scope.SignUpData.Employer.Phone = $scope.SignUpData.Employer.Phone == "" ? "" : $filter("tel")($scope.SignUpData.Employer.Phone)
                $scope.SignUpData.Employer.Fax = $scope.SignUpData.Employer.Fax == "" ? "" : $filter("tel")($scope.SignUpData.Employer.Fax)

                for (i = 0; i < $scope.SignUpData.Employer.Holidays.length; i++) {
                    $scope.SignUpData.Employer.Holidays[i]['Checked'] = true
                }

                $scope.$watch('SignUpData.Employer.PostalCode', function () {
                    $scope.PostalCodeValidationIn3();
                });

                $scope.$watch('SignUpData.Employer.Country', function () {
                    $scope.PostalCodeValidationIn3();
                });

                if ($scope.SignUpData.Employer.LogoId != null && $scope.SignUpData.Employer.LogoId != '' && $scope.SignUpData.Employer.LogoId != undefined) {
                    $scope.$broadcast('SetLogo', { EmployerId: data.Id });
                }
            }
        });

    };

    $scope.PostalCodeRquirements = /^[0-9]*$/;

    $scope.PostalCodeValidationIn3 = function () {
        if ($scope.SignUpData.Employer.PostalCode == null || $scope.SignUpData.Employer.PostalCode == '' || $scope.SignUpData.Employer.PostalCode == undefined)
            return;

        if ($scope.SignUpData.Employer.Country == null || $scope.SignUpData.Employer.Country == '' || $scope.SignUpData.Employer.Country == undefined)
            return;

        if ($scope.SignUpData.Employer.Country == 'US') {
            var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
            if (!regEx.test($scope.SignUpData.Employer.PostalCode)) {
                $scope.forms.frmSignUpStep3.PostalCode.$setValidity('valid', false);
                return;
            }
        }
        $scope.forms.frmSignUpStep3.PostalCode.$setValidity('valid', true);
    };

    $scope.Error = null;

    $scope.ErrorHandler = function (response) {
        $scope.submitted = false;
        $scope.Error = response;
    };

    $scope.UpdateEmployerInfo = function (IsValid, Callback) {

        if (IsValid) {
            $scope.isDisabled = true;

            var length = $scope.SignUpData.Employer.Holidays.length;
            var holidays = $scope.SignUpData.Employer.Holidays;

            var holidaysAfterFilter = [];
            arr = jQuery.grep(holidays, function (n, i) {
                if (n.Checked == true) {
                    holidaysAfterFilter.push(n)
                }
            });

            $scope.SignUpData.Employer.Holidays = holidaysAfterFilter;

            administrationService.UpdateEmployer($scope.SignUpData.Employer).then(function (data) {
                $scope.SignUpData.Employer.Holidays = holidays;
                if (data.Error == null || data.Error == "") {
                    if (data.Id != null || data.Id != "") {

                        if (Callback != null) {
                            $scope.EmployerIdToEdit = data.Id;
                            $scope.SignUpData.Employer.Id = data.Id;
                            $scope.IsAddNew = false;
                            Callback();
                        }
                        else {
                            //$modalInstance.dismiss('cancel');

                            //$scope.$broadcast('UploadLogo', { EmployerId: data.Id });
                            $scope.cancel();
                        }
                        //$scope.SignUpData.Employer.Holidays = holidays;
                        //if ($scope.EmployerIdToEdit != null && $scope.EmployerIdToEdit != undefined)
                        //    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Employer info updated successfully" });
                        //else {
                        //    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Employer info added successfully" });
                        //}
                    }
                }
                else {
                    $scope.SignUpData.Employer.Holidays = holidays;
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.Error });
                }
                $scope.submitted = false;
            }, function () {
                $scope.SignUpData.Employer.Holidays = holidays;
                $scope.ErrorHandler();
            });
        }
    };

    $scope.$on('AfterUploadLogo', function (event, args) {
        //$modalInstance.dismiss('cancel');

        //$scope.SignUpData.Employer.Holidays = holidays;
        if ($scope.EmployerIdToEdit != null && $scope.EmployerIdToEdit != undefined)
            var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Employer info updated successfully" });
        else {
            var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Employer info added successfully" });
        }

        $timeout(function () {
            $window.location.reload();
        }, 1000)
    });

    $scope.SetDayForSelectedMonth = function (monthNumber) {
        var year = (new Date).getFullYear();
        var month = parseInt(monthNumber);
        $scope.TotalDays = new Date(year, month, 0).getDate();
        var Days = [];
        for (i = 1; i <= $scope.TotalDays; i++) {
            Days.push({ "items": i });
        }
        $scope.Days = Days;
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);