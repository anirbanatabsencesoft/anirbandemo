//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EditUserInfoCtrl', ['$scope', '$modal', '$filter', function ($scope, $modal, $filter) {    
    $scope.GoToManageUser = function (user) {
        if (user != undefined && user != null)
            window.location.href = "/Administration/"  + user.Id + "/ManageUser";
        else
            window.location.href = "/Administration/000000000000000000000000/ManageUser/";
    };
}]);