//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('RuleGroupsCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.openRuleGroupsModal = function () {
        var RuleGroupsModalInstance = $modal.open({
            templateUrl: 'RuleGroups.html',
            controller: RuleGroupsCtrlInstance,
            //resolve: {
            //    userId: function () {
            //        return userId;
            //    }
            //}
        });
    };
}]);



var RuleGroupsCtrlInstance = function ($scope, $http, administrationService, $filter, $modalInstance, $rootScope) {
        
        

        $scope.forms = {};


        $scope.init = function (model) {
            administrationService.GetCompanyInfo().then(function (data) {
                if (data != null) {                    
                    $scope.SignUpData.Customer = data;

                    $scope.SignUpData.Customer.Phone = $scope.SignUpData.Customer.Phone == "" ? "" : $filter("tel")($scope.SignUpData.Customer.Phone);
                }
            })
           

            $scope.$watch('SignUpData.Customer.PostalCode', function () {
                $scope.PostalCodeValidationIn2();
            });

            $scope.$watch('SignUpData.Customer.Country', function () {
                $scope.PostalCodeValidationIn2();
            });
        };

        $scope.PostalCodeValidationIn2 = function () {
            if ($scope.SignUpData.Customer.PostalCode == null || $scope.SignUpData.Customer.PostalCode == '' || $scope.SignUpData.Customer.PostalCode == undefined)
                return;

            if ($scope.SignUpData.Customer.Country == null || $scope.SignUpData.Customer.Country == '' || $scope.SignUpData.Customer.Country == undefined)
                return;

            if ($scope.SignUpData.Customer.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.SignUpData.Customer.PostalCode)) {
                    $scope.forms.frmSignUpStep2.PostalCode.$setValidity('valid', false);
                    return;
                }
            }

            $scope.forms.frmSignUpStep2.PostalCode.$setValidity('valid', true);
        };

        $scope.PhoneRequirements = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        $scope.PostalCodeRquirements = /^[0-9]*$/;
        $scope.IsCompanyinfoEdit = true;

        $scope.Error = null;

        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.UpdateCompanyInfo = function (IsValid) {
            if (IsValid) {
                administrationService.UpdateCompanyInfo($scope.SignUpData.Customer).then(function (result) {
                    if (result.Error == null) {
                        $modalInstance.dismiss('cancel');

                        // Refresh parent page
                        administrationService.GetBasicInfo().then(function (data) {
                            if (data != null) {
                                $rootScope.BasicInfo = data;
                            }
                        });

                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Company info updated successfully." });
                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.Error });
                    }

                },
                $scope.ErrorHandler);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    };