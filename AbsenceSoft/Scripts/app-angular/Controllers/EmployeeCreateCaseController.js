//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations

    angular
    .module('App.Controllers')
.controller("employeeCreateCaseCtrl", ['$log', '$scope', '$http', '$filter', '$resource', 'employeeService', 'caseService', 'lookupService', '$route', '$routeParams',
    function ($log, $scope, $http, $filter, $resource, employeeService, caseService, lookupService, $route, $routeParams) {
        $scope.Relation = null;
        $scope.StepsCompleted = true;
        $scope.ViewMockup = true;
        $scope.forms = {};
        $scope.forms.ShowDetailsToEdit = false;
        $scope.forms.SelectedFamilyMember = {};
        $scope.EmployeeRelationshipDOB = new Date().addMonths(-360);
        $scope.SelectedAbsenceReason = null;
        $scope.SelectedAbsenceReasonCategory = null;
        $scope.forms.SelectedReason = null;
        $scope.ShowSummarizeRequest = true;
        $scope.ContactOptions = [
                    { value: "Phone", label: "Phone" },
                    { value: "Email", label: "Email" }
        ];
                //
            // Only show 'Is Employee on Leave' if 'Is the employee at work?' = "No"
            $scope.$watch('EmployeeCreateCaseModel.IsEmployeeAtWork', function (newvalue, oldvalue) {
                if (newvalue === false) {
                $scope.showIsEmployeeOnLeave = true;
                } else {
                $scope.showIsEmployeeOnLeave = false;
                $scope.EmployeeCreateCaseModel.IsEmployeeOnLeave = null;

                }
            });
            //
            // Show Case Number if EmployeeCreateCaseModel.IsEmployeeOnLeave = "Yes"
            $scope.$watch('EmployeeCreateCaseModel.IsEmployeeOnLeave', function (newvalue, oldvalue) {
            console.log({ onleave: newvalue });
                if (newvalue === true) {
                $scope.showCaseNumber = true;
                } else {
                $scope.showCaseNumber = false;
                $scope.EmployeeCreateCaseModel.EmpOtherLeaveCaseNumber = null;
                }
            });

        $scope.$watch('SelectedAbsenceReason.Code', function(newvalue, oldvalue) {
            if(newvalue && newvalue === 'INQ') {
                $scope.ShowSummarizeRequest = false;
                } else {
                $scope.ShowSummarizeRequest = true;
                }
            });

        $scope.EmployeeCreateCaseModel = { };
        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };

        $scope.init = function (employeeId, caseId, isESSCaseReview, toDoItemId, isAccommodation) {
            console.log({ init: arguments });
            if(!$routeParams.eId) {
                $scope.EmployeeId = employeeId;
            }
            else {
                $scope.EmployeeId = $routeParams.eId;
            }

            $scope.EmployeeCreateCaseModel.EmployeeId = $scope.EmployeeId;
            $scope.EmployeeCreateCaseModel.AccommodationRequest = {
                type: null
            }

            caseService.GetEmployeeFeatureInfo($scope.EmployeeId).then(function(data) {
                //Employer feature level flags that affect the UI
                // $scope.NoFeaturesEnabled = data.NoFeaturesEnabled;
                $scope.STDFeatureEnabled = data.STDFeatureEnabled;
                $scope.GuidelinesDataEnabled = data.GuidelinesDataEnabled;                
                $scope.ADAFeatureEnabled = data.ADAFeatureEnabled;
                $scope.LOAFeatureEnabled = data.LOAFeatureEnabled;

                $scope.EmployeeCreateCaseModel.STDFeatureEnabled = data.STDFeatureEnabled;
                $scope.EmployeeCreateCaseModel.GuidelinesDataEnabled = data.GuidelinesDataEnabled;
                $scope.EmployeeCreateCaseModel.ADAFeatureEnabled = data.ADAFeatureEnabled;
                // $scope.EmployeeCreateCaseModel.NoFeaturesEnabled = data.NoFeaturesEnabled;
                $scope.EmployeeCreateCaseModel.LOAFeatureEnabled = data.LOAFeatureEnabled;

                // flag to show hide custom fields
                $scope.ShowIsWorkRelated = data.ShowIsWorkRelated;
                $scope.ShowContactSection = data.ShowContactSection;
                $scope.ShowOtherQuestions = data.ShowOtherQuestions;

                $scope.EmployeeCreateCaseModel.ShowIsWorkRelated = data.ShowIsWorkRelated;
                $scope.EmployeeCreateCaseModel.ShowContactSection = data.ShowContactSection;
                $scope.EmployeeCreateCaseModel.ShowOtherQuestions = data.ShowOtherQuestions;
            })

            $scope.GetProviderContacts();

            $scope.CaseId = caseId;
            $scope.IsESSCaseReview = isESSCaseReview == 'True' ? true : false;
            $scope.IsAccommodation = isAccommodation == 'True' ? true : false;
            $scope.ToDoItemId = toDoItemId;

            employeeService.GetEmployeeInfo($scope.EmployeeId, null).then(function(response) {
                $scope.EmployeeInfo = response;

                $scope.EmployeeCreateCaseModel.NewAddress = (response.Address.Address1 != null ? response.Address.Address1: "") + " " + (response.Address.Address2 != null ? response.Address.Address2: "");
                $scope.EmployeeCreateCaseModel.NewCity = response.Address.City;
                $scope.EmployeeCreateCaseModel.NewState = response.Address.State;
                $scope.EmployeeCreateCaseModel.NewZipcode = response.Address.PostalCode;

                $scope.EmployeeCreateCaseModel.NewAltAddress = (response.AltAddress.Address1 != null ? response.AltAddress.Address1: "") + " " + (response.Address.Address2 != null ? response.Address.Address2: "");
                $scope.EmployeeCreateCaseModel.NewAltCity = response.AltAddress.City;
                $scope.EmployeeCreateCaseModel.NewAltState = response.AltAddress.State;
                $scope.EmployeeCreateCaseModel.NewAltZipcode = response.AltAddress.PostalCode;
                $scope.EmployeeCreateCaseModel.NewAltEmail = response.AltEmail;

                $scope.EmployeeCreateCaseModel.NewWorkPhone = response.WorkPhone != null ? $filter('tel') (response.WorkPhone): "";
                $scope.EmployeeCreateCaseModel.NewEmail = response.Email;
                $scope.EmployeeCreateCaseModel.NewHomePhone = response.HomePhone != null ? $filter('tel') (response.HomePhone): "";
                $scope.EmployeeCreateCaseModel.NewCellPhone = response.CellPhone != null ? $filter('tel')(response.CellPhone) : "";
                $scope.EmployeeCreateCaseModel.NewAltPhone = response.AltPhone != null ? $filter('tel')(response.AltPhone) : "";

                if (response.Address.Country == "US") {
                    $scope.ShowAddressStateTextBox = false;
                    $scope.ShowAddressStateDropDown = true;
                }
                else {
                    $scope.ShowAddressStateTextBox = true;
                    $scope.ShowAddressStateDropDown = false;
                }

                if (response.AltAddress.Country == "US") {
                    $scope.ShowAltAddressStateTextBox = false;
                    $scope.ShowAltAddressStateDropDown = true;
                }
                else {
                    $scope.ShowAltAddressStateTextBox = true;
                    $scope.ShowAltAddressStateDropDown = false;
                }

                $scope.EmployeeRelationships = caseService.EmployeeContactTypes("FHC");
                $scope.AbsenceReasons = null;
                lookupService.AbsenceReasonsPromise($scope.EmployeeId).then(function(reasons) {
                    $scope.AbsenceReasons = reasons;
                    $scope.GetEmployeeCaseForReview();
                });

                $scope.$watch('EmployeeCreateCaseModel.NewZipCode', function (newValue, oldValue) {
                    if(newValue == oldValue)
                        return;

                    $scope.ZipcodeValidation();
                });

                $scope.$watch('EmployeeCreateCaseModel.NewAltZipCode', function (newValue, oldValue) {
                    if(newValue == oldValue)
                        return;

                    $scope.AltZipcodeValidation();
                });

                lookupService.AccommodationTypes($scope.EmployeeInfo.EmployerId, $scope.EmployeeCreateCaseModel.CaseTypeId).$promise.then(function(data) {
                    $scope.AccommodationTypes = data;
                })
                $scope.EmployeeCreateCaseModel.IsContactInformationCorrect = true;

                $scope.$watch('forms.frmStep1.$valid', function (newValue, oldValue) {
                    if (!newValue) {
                        $scope.EmployeeCreateCaseModel.IsContactInformationCorrect = false;
                        $scope.forms.ShowDetailsToEdit = true;
                    }
                });

                //if ($scope.forms.frmStep1.$invalid) {
                //    console.trace({ invalid: $scope.forms.frmStep1.$invalid });
                //    $scope.EmployeeCreateCaseModel.IsContactInformationCorrect = false;
                //    $scope.forms.ShowDetailsToEdit = true;
                //}

            });

            employeeService.EmployeeExistingRelationships($scope.EmployeeId, "FHC").then(function(response) {
                $scope.EmployeeExistingRelationships = response;
                $scope.EmployeeExistingRelationships.push({
                    Id: 0,
                    Text: null,
                    FirstName: "",
                    LastName: "",
                    RelationShipId: null,
                    DOB: new Date().addMonths(-360)

                });

                if ($scope.EmployeeExistingRelationships.length > 0) {
                    $scope.SelectRelative($scope.EmployeeExistingRelationships[0]);
                }
            });

            if ($scope.IsESSCaseReview) {
                $scope.CaseCancelReason = lookupService.CaseCancelReason();
            }

        }


        $scope.GetEmployeeCaseForReview = function () {
            // if value is true then its ess case review mode
            if(Boolean($scope.IsESSCaseReview)) {
                if ($scope.CaseId != null && $scope.CaseId != undefined && $scope.CaseId != "") {
                    $scope.IsESSCaseReview = true;
                    caseService.GetESSCaseForReview($scope.CaseId).then(function(data) {
                        if(data.Success) {

                            vm.EmployeeCreateCaseModel = data.CaseInfo;

                            $scope.EmployeeCaseStartDate = $scope.EmployeeCreateCaseModel.StartDate;
                            $scope.EmployeeCaseEndDate = $scope.EmployeeCreateCaseModel.EndDate;
                            $scope.IsAccommodation = $scope.EmployeeCreateCaseModel.IsAccommodation;

                            if ($scope.AbsenceReasons != null) {
                                angular.forEach($scope.AbsenceReasons, function (reason, index) {
                                    if(reason.IsCategory) {
                                        var selReason = $filter('filter') (reason.Children, { Id: $scope.EmployeeCreateCaseModel.AbsenceReasonId });
                                        if (selReason.length > 0) {
                                            $scope.SelectedAbsenceReason = selReason[0];
                                            $scope.selectedReasonChild = selReason[0].Id;
                                            $scope.SelectedAbsenceReasonCategory = reason;
                                            $("#" + reason.Category + "AbsenceReasonCategoryDiv").show();
                                            $scope.IsRequired = true;
                                        }
                                    }
                                    else {
                                        if (reason.Id == $scope.EmployeeCreateCaseModel.AbsenceReasonId) {
                                            $scope.SelectedAbsenceReason = reason;
                                            $scope.SelectedAbsenceReasonCategory = null;
                                            $scope.IsRequired = false;
                                        }
                                    }
                                });

                                if (!$scope.EmployeeCreateCaseModel.IsWorkScheduleCorrect) {
                                    $scope.ShowScheduleWarning = true;
                                }

                                if (!$scope.EmployeeCreateCaseModel.IsContactInformationCorrect) {
                                    $scope.forms.ShowDetailsToEdit = true;
                                }

                            }


                            for (var i = 1; i < 6; i++) {

                                $("#Step" + (i + 1)).css("display", "block");

                                $("#collapse" + (i + 1)).collapse('show');
                            }
                        }
                    });
                }
            }
        }


        $scope.ZipcodeValidation = function () {
            if($scope.EmployeeCreateCaseModel.NewZipCode == null || $scope.EmployeeCreateCaseModel.NewZipCode == '' || $scope.EmployeeCreateCaseModel.NewZipCode == undefined)
                return;

            //if ($scope.Address.Country == null || $scope.Address.Country == '' || $scope.Address.Country == undefined)
            //    return;

            if ($scope.EmployeeInfo.Address.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.EmployeeCreateCaseModel.NewZipCode)) {
                    $scope.forms.frmStep1.PostalCode.$setValidity('valid', false);
                    return;
                }
            }

            $scope.forms.frmStep1.PostalCode.$setValidity('valid', true);
        };

        $scope.AltZipcodeValidation = function () {
            if($scope.EmployeeCreateCaseModel.NewAltZipCode == null || $scope.EmployeeCreateCaseModel.NewAltZipCode == '' || $scope.EmployeeCreateCaseModel.NewAltZipCode == undefined)
                return;

            //if ($scope.Address.Country == null || $scope.Address.Country == '' || $scope.Address.Country == undefined)
            //    return;

            if ($scope.EmployeeInfo.AltAddress.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.EmployeeCreateCaseModel.NewAltZipCode)) {
                    $scope.forms.frmStep1.AltPostalCode.$setValidity('valid', false);
                    return;
                }
            }

            $scope.forms.frmStep1.AltPostalCode.$setValidity('valid', true);
        };

        //$scope.ShowNextStep = function (currStepIndex, nextStepIndex) {       
        //    $("#Step" + nextStepIndex).slideDown('slow');
        //    if ((nextStepIndex-currStepIndex) > 1) {
        //        for (var i = currStepIndex + 1 ; i < 6 ; i++) {
        //            $("#Step" + i).slideUp('slow');
        //        }
        //    }
        //    if (nextStepIndex == 8)
        //        $scope.StepsCompleted = false;

        //    $scope.HideNextSteps(nextStepIndex + 1);

        //    $("html, body").animate({ scrollTop: $(document).height() - 500 }, 1000);
        //}


        $scope.PanelStatus = [];
        angular.element(document).ready(function() {

            if($scope.IsAccommodation) {
                for (var i = 0; i < 8; i++) {
                    if (i == 0) {
                        $scope.PanelStatus.push(false);
        }
                    else {
                        $scope.PanelStatus.push(undefined);
                    }
                }
            }
            else {
                for (var i = 0; i < 7; i++) {
                    if (!$scope.IsESSCaseReview) {
                        if (i == 0) {
                            $scope.PanelStatus.push(false);
                        }
                        else {
                            $scope.PanelStatus.push(undefined);
                        }
                    }
                    else if ($scope.IsESSCaseReview) {
                        if (i == 6) {
                            $scope.PanelStatus.push(false);
                        }
                        else {
                            $scope.PanelStatus.push(undefined);
                        }
                        $("#Step" + 7).removeClass("hide");
                        $("#Step" + 1).addClass("hide");
                    }
                }
            }
        });


        $scope.GoToNextStep = function (currStep, nextStep) {
            console.trace('GoToNextStep', { currStep: currStep, nextStep: nextStep });
            $("#Step" + nextStep).removeClass("hide");
            $("#Step" + currStep).addClass("hide");


            //$("#collapse" + currStep).collapse('hide');
            //$("#collapse" + nextStep).collapse('show');


            $scope.CurrentStep = nextStep;

            $scope.PanelStatus[currStep - 1]= true;
            $scope.PanelStatus[nextStep - 1]= false;

            //  $("html, body").animate({ scrollTop: $(document).height() - 500 }, 1000);
        }

        $scope.GoToStep = function (step) {
            $("#Step" + $scope.CurrentStep).addClass('hide');
            $("#Step" + step).removeClass('hide');

            $scope.CurrentStep = step;

            $scope.PanelStatus[step - 1]= false;

            for (var i = 0; i < step - 1; i++) {
                $scope.PanelStatus[i]= true;
            }

            $scope.HideNextSteps(step + 1);
        }

        $scope.HideNextSteps = function (nextStepIndex) {

            if($scope.IsAccommodation) {
                for (var i = nextStepIndex; i <= 8; i++) {
                    $scope.PanelStatus[i - 1]= undefined;
                }
            }
            else {
                for (var i = nextStepIndex; i <= 7; i++) {
                    $scope.PanelStatus[i - 1]= undefined;
                }
            }

        }


        //#region "Steps submit and validation
        $scope.Step1Submit = function () {
            // Check any option from "Yes" or "No" has been selected
            if($scope.EmployeeCreateCaseModel.IsContactInformationCorrect == null || $scope.EmployeeCreateCaseModel.IsContactInformationCorrect == undefined) {
                $scope.ValidateInfoError = true;
            }
            else {
                $scope.ValidateInfoError = false;
            }

            if (!$scope.ValidateInfoError) {
                $scope.ValidateInfoError = $scope.forms.frmStep1.$invalid || ($scope.forms.frmContactMethod && $scope.forms.frmContactMethod.$invalid)
            }

            if (!$scope.ValidateInfoError) {
                $scope.EmployeeCreateCaseModel.CaseTypeId = 8;
                $scope.GoToNextStep(1, 2);
            }
            $scope.Step1Submitted = true;
        }

        $scope.SubmitNewContact = function (isValid) {
            if(isValid) {
                $scope.forms.ShowDetailsToEdit = false;
                $scope.forms.ShowNewContactWarning = true;
            }
        }

        $scope.Step2Submit = function () {
            if(!$scope.IsAccommodation)
                $scope.GoToNextStep(2, 3);

            if ($scope.SelectedAbsenceReason) {
                $scope.GoToNextStep(2, 3);
            }

        }

        $scope.SetStratEndDate = function (duration) {
            if(duration == 1) {
                $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate = $scope.EmployeeCaseStartDate;
                $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate = $scope.EmployeeCaseEndDate;
            }
            else if (duration == 2) {
                $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate = $scope.EmployeeCaseStartDate;
                $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate = null;
            }
        }

        $scope.Step3Submit = function () {
            if($scope.forms.frmStep3.$valid) {
                if ($scope.IsAccommodation) {
                    $scope.EmployeeCreateCaseModel.LeaveFor = 'Self';
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate = $scope.EmployeeCaseStartDate;
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate = $scope.EmployeeCaseEndDate;
                    /// This is dumb.  We've already selected the reason, but they've got some bs about doing this, so we set it to administrative
                    /// then we go ahead and set it back to the actual reason
                    var actualReason = $scope.SelectedAbsenceReason;
                    var adminReason = $scope.AbsenceReasons.filter(function (f) {
                        return f.Name === 'Administrative';
                    });
                    if (!adminReason) {
                        throw 'Administrative Absence Reason not found';
                    }
                    adminReason = adminReason[0];
                    $scope.onClickAbsenceReason(adminReason, false);
                    $scope.onClickAbsenceReason(actualReason);
                    $scope.GoToNextStep(3, 4);
                    $scope.GoToNextStep(5, 6);
                    if ($scope.SelectedAbsenceReason.Code == 'INQ')
                        $scope.GoToNextStep(6, 7);
                }
                else
                    $scope.GoToNextStep(3, 4);
            }
        }

        $scope.Step4Submit = function () {
            if($scope.forms.frmStep4.$valid) {

                if ($scope.EmployeeCreateCaseModel.LeaveFor == "Family") {
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipId = $scope.forms.SelectedFamilyMember.RelationShipId;
                    $scope.EmployeeCreateCaseModel.EmployeeContactId = $scope.forms.SelectedFamilyMember.Id;
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = $scope.forms.SelectedFamilyMember.FirstName;
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipLastName = $scope.forms.SelectedFamilyMember.LastName;
                    if ($scope.forms.SelectedFamilyMember.DOB != null && $scope.forms.SelectedFamilyMember.DOB != undefined && $scope.forms.SelectedFamilyMember.DOB != '')
                        $scope.EmployeeCreateCaseModel.EmployeeRelationshipDOB = $scope.forms.SelectedFamilyMember.DOB.toDateString();
                }
                else {
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipId = null;
                    $scope.EmployeeCreateCaseModel.EmployeeContactId = null;
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.EmployeeCreateCaseModel.EmployeeRelationshipDOB = null;
                }

                $scope.GoToNextStep(4, 5);
            }
        }

        $scope.SetRelationship = function (relationshipId) {
            var selectedRelationship = $filter('filter') ($scope.EmployeeRelationships, { Id: relationshipId });
            if (selectedRelationship.length > 0) {
                $scope.forms.SelectedFamilyMember.RelationShipId = relationshipId;
                $scope.forms.SelectedFamilyMember.Text = selectedRelationship[0].Text;
            }
        }

        $scope.SelectRelative = function (relative) {
            $scope.forms.SelectedFamilyMember = relative;
            $scope.forms.SelectedFamilyMemberId = relative.Id;
        }

        $scope.Step5Submit = function () {
            if($scope.forms.frmStep5.$valid) {
                $scope.GoToNextStep(5, 6);
            }
        }

        $scope.onClickAbsenceReason = function (absenceReason, scroll) {
            if(absenceReason.IsCategory) {
                $scope.SelectedAbsenceReason = null;
                $scope.SelectedAbsenceReasonCategory = absenceReason;
                $scope.IsRequired = true;
            }
            else if (absenceReason.IsChild) {
                $scope.SelectedAbsenceReason = absenceReason;
                $scope.selectedReasonChild = absenceReason.Id;
            }
            else {
                $scope.SelectedAbsenceReason = absenceReason;
                $scope.SelectedAbsenceReasonCategory = null;
            }

            if (scroll == undefined || scroll == true)
                $("html, body").animate({ scrollTop: $(document).height() - 500 }, 1000);

        };

        $scope.EnableRel = function (Rel) {
            $scope.Relation = Rel;
            if (Rel == "NewRel") {
                // $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', false);
                $scope.showTextBox = true;
            }
            else if (Rel == "ExistingRel") {
                if ($scope.DeniedNewDuplicateRelationship == true) {
                    $scope.EmployeeNewRelationship = null;
                    $scope.EmployeeRelationshipFirstName = null;
                    $scope.EmployeeRelationshipLastName = null;
                    $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
                }
                $scope.frmCreateCase.contFirstname.$setValidity('duplicateEmployeeContact', true);
                $scope.showTextBox = false;
            }
            else {
                $scope.showTextBox = false;
            }
        };

        $scope.AllowNewContact = function () {
            $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
            $scope.DeniedNewDuplicateRelationship = false;
            $("#ContactDuplication").modal('hide');
        }

        $scope.DenyNewContact = function () {
            $scope.DeniedNewDuplicateRelationship = true;
            $("#ContactDuplication").modal('hide');
        }

        $scope.Step6Submit = function () {
            if($scope.forms.frmStep6.$valid) {
                $scope.GoToNextStep(5, 6);
            }
        }

        $scope.Step7Back = function () {
            if($scope.ShowSummarizeRequest) {
                $scope.GoToStep(6);
            } else {
                $scope.GoToStep(3);
        }
        };

        $scope.Step7Submit = function () {

            if($scope.IsAccommodation) {
                $scope.ValidateScheduleError = $scope.forms.frmStep7.$invalid; $scope.EmployeeCreateCaseModel.IsWorkScheduleCorrect = true; $scope.ShowScheduleWarning = false;
            }
            else if ($scope.EmployeeCreateCaseModel.IsWorkScheduleCorrect == null || $scope.EmployeeCreateCaseModel.IsWorkScheduleCorrect == undefined) {
                $scope.ValidateScheduleError = true;
            }
            else {
                $scope.ValidateScheduleError = false;
            }

            if (!$scope.ValidateScheduleError) {
                if ($scope.EmployeeCaseStartDate) {
                    $scope.EmployeeCreateCaseModel.StartDate = $scope.EmployeeCaseStartDate.toDateString();
                }
                else if ($scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate) {
                    $scope.EmployeeCreateCaseModel.StartDate = $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate.toDateString();
                }
                if ($scope.EmployeeCaseEndDate) {
                    $scope.EmployeeCreateCaseModel.EndDate = $scope.EmployeeCaseEndDate.toDateString();
                }
                else if ($scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate) {
                    $scope.EmployeeCreateCaseModel.EndDate = $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate.toDateString();
                }
                else if (!$scope.EmployeeCaseEndDate && $scope.EmployeeCreateCaseModel.AccommodationRequest.Duration == 1) {
                    $scope.EmployeeCreateCaseModel.EndDate = $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate.toDateString();
                    $scope.EmployeeCaseEndDate = $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate;
                }
                else {
                    $scope.EmployeeCreateCaseModel.EndDate = null;
                }

                if ($scope.IsAccommodation) {
                    if ($scope.ShowOtherQuestions)
                        $scope.GoToNextStep(6, 7);
                    else
                        $scope.GoToNextStep(6, 8);
                }
                else {
                    $scope.GoToNextStep(6, 7);
                }

            }
        }

        $scope.Step8Submit = function () {
            if($scope.forms.frmStep8.$valid) {
                $scope.GoToNextStep(7, 8);
            }
        }

        $scope.SetAbsenceReasonParameters = function () {
            var selReasonCode = $scope.SelectedAbsenceReason.Code;
            switch (selReasonCode) {
                case "EHC":
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "FHC":
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "PREGMAT":
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.ShortDescription = "";
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
                case "ADOPT":
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.ShortDescription = "";
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    break;
                case "MILITARY":
                case "EXIGENCY":
                case "RESERVETRAIN":
                case "CEREMONY":
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.ShortDescription = "";
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;

                    //$scope.EmployeeCreateCaseModel.MilitaryStatusId = $scope.MilitaryStatus != null ? $scope.MilitaryStatus.Id : null;
                    break;

                case "PARENTAL":
                case "DOM":
                case "ORGAN":
                case "BONE":
                case "BLOOD":
                case "CRIME":
                case "BEREAVEMENT":
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.ShortDescription = "";
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;

                case "ACCOMM":
                    $scope.EmployeeCreateCaseModel.IsAccommodation = true;
                    $scope.EmployeeCreateCaseModel.MilitaryStatusId = null;
                    $scope.EmployeeCreateCaseModel.IsWorkRelated = null;
                    $scope.EmployeeCreateCaseModel.ShortDescription = "";
                    $scope.EmployeeCreateCaseModel.WillUseBonding = null;
                    $scope.EmployeeCreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.ActualDeliveryDate = null;
                    $scope.EmployeeCreateCaseModel.BondingStartDate = null;
                    $scope.EmployeeCreateCaseModel.BondingEndDate = null;
                    $scope.EmployeeCreateCaseModel.AdoptionDate = null;
                    break;
            }
        }

        $scope.OnAccomTypeSelected = function () {
            var IsWorkRelated = $scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated;
            if (IsWorkRelated != null && IsWorkRelated != undefined && IsWorkRelated != '') {
                if (IsWorkRelated) {
                    if ($scope.EmployeeCreateCaseModel.AccommodationRequest.Type.Code == "COF") {
                        $scope.isDisabled = true;
                        $("#CaseWarning").modal('show');
                    }
                    else {
                        $scope.isDisabled = false;
                    }
                }
                else {
                    $scope.isDisabled = false;
                }
            }
        }

        $scope.OnWorkRelatedClick = function (IsWorkRelated) {
            $scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated = IsWorkRelated;
            if (IsWorkRelated) {
                if ($scope.EmployeeCreateCaseModel.AccommodationRequest.Type.Code == "COF") {
                    $scope.isDisabled = true;
                    $("#CaseWarning").modal('show');
                }
                else {
                    $scope.isDisabled = false;
                }
            }
            else {
                $scope.isDisabled = false;
            }
        };

        $scope.GetProviderContacts = function () {
            $scope.ProviderContacts = [];
            $http({
                method: 'GET',
                url: "/Employees/" + $scope.EmployeeId + "/Contacts/Medical" 
            }).then(function (response) {
                $scope.ProviderContacts = response.data;
                var newContact = { };
                newContact.ContactId = '-1';
                newContact.Name = 'New Provider';
                $scope.ProviderContacts.push(newContact);

            });
        };



        $scope.HCPContactChangeAccom = function (contactId) {
            if(contactId == null || contactId == undefined || contactId == '') {
                $scope.NewHCP = false;
            }
            else {
                $scope.NewHCP = true;
                var contact = $filter('filter') ($scope.ProviderContacts, { ContactId: contactId });
                if (contact.length > 0) {
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.HCPCompanyName = contact[0].CompanyName;
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.HCPFirstName = contact[0].FirstName;
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.HCPLastName = contact[0].LastName;
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.HCPPhone = $filter('tel') (contact[0].WorkPhone);
                    $scope.EmployeeCreateCaseModel.AccommodationRequest.HCPFax = $filter('tel') (contact[0].Fax);
                }
                $("html, body").animate({ scrollTop: 600 }, 1000);
            }
        }

        $scope.HCPContactTypeChangeAccom = function (contactType) {
            if (contactType != null && contactType != undefined && contactType != '') {
                var contactType = $filter('filter')($scope.ProviderContactTypes, { Code: contactType });
                if (contactType.length > 0) {
                    $scope.CreateCaseModel.AccommodationRequest.HCPContactTypeCode = contactType[0].Code;
                }
            }
        }

        $scope.CreateCase = function () {
            // if leave is for "Self" then set relationship params to null
            if($scope.EmployeeCreateCaseModel.LeaveFor == "Self") {
                $scope.EmployeeCreateCaseModel.EmployeeRelationshipId = null;
                $scope.EmployeeCreateCaseModel.EmployeeRelationshipFirstName = null;
                $scope.EmployeeCreateCaseModel.EmployeeRelationshipLastName = null;
                $scope.EmployeeCreateCaseModel.EmployeeContactId = null;
            }
            else {
                if ($scope.EmployeeCreateCaseModel.EmployeeContactId == 0)
                    $scope.EmployeeCreateCaseModel.EmployeeContactId = null;
            }

            // Set selected absencereason id
            $scope.EmployeeCreateCaseModel.AbsenceReasonId = $scope.SelectedAbsenceReason != null ? $scope.SelectedAbsenceReason.Id: null;
            // Set absence reason property depending on the selected absence reason 
            $scope.SetAbsenceReasonParameters();

            // Case Start date / End Date
            $scope.EmployeeCreateCaseModel.StartDate = $scope.EmployeeCaseStartDate.toDateString();
            if ($scope.EmployeeCaseEndDate) {
                $scope.EmployeeCreateCaseModel.EndDate = $scope.EmployeeCaseEndDate.toDateString();
            }
            else if (!$scope.EmpCaseEndDate && $scope.EmployeeCreateCaseModel.AccommodationRequest.Duration == 1) {
                $scope.EmployeeCreateCaseModel.EndDate = $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate.toDateString();
            }
            else {
                $scope.EmployeeCreateCaseModel.EndDate = null;
            }

            // remove formatting for telephone number
            $scope.EmployeeCreateCaseModel.NewWorkPhone = $scope.EmployeeCreateCaseModel.NewWorkPhone.replace(/[^0-9]/g, '');
            $scope.EmployeeCreateCaseModel.NewHomePhone = $scope.EmployeeCreateCaseModel.NewHomePhone.replace(/[^0-9]/g, '');
            $scope.EmployeeCreateCaseModel.NewCellPhone = $scope.EmployeeCreateCaseModel.NewCellPhone.replace(/[^0-9]/g, '');


            //accomm
            var accomm = { };

            var reqStartDt = null;
            if ($scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate != undefined &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate != null &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate != '') {
                reqStartDt = new Date($scope.EmployeeCreateCaseModel.AccommodationRequest.StartDate).toDateString();
                if (reqStartDt == "Invalid Date") {
                    reqStartDt = null;
                };
            }
            accomm.StartDate = reqStartDt;

            var reqEndDt = null;
            if ($scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate != undefined &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate != null &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate != '') {
                reqEndDt = new Date($scope.EmployeeCreateCaseModel.AccommodationRequest.EndDate).toDateString();
                if (reqEndDt == "Invalid Date") {
                    reqEndDt = null;
                };
            }
            accomm.EndDate = reqEndDt;


            if ($scope.EmployeeCreateCaseModel.AccommodationRequest.Type != undefined &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.Type != null &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.Type != '') {
                accomm.Type = $scope.EmployeeCreateCaseModel.AccommodationRequest.Type;
            }


            if ($scope.EmployeeCreateCaseModel.AccommodationRequest.Duration != undefined &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.Duration != null &&
                $scope.EmployeeCreateCaseModel.AccommodationRequest.Duration != '') {
                accomm.Duration = $scope.EmployeeCreateCaseModel.AccommodationRequest.Duration;
            }

            if ($scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated != undefined &&
                            $scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated != null &&
                            $scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated != '') {
                accomm.IsWorkRelated = $scope.EmployeeCreateCaseModel.AccommodationRequest.IsWorkRelated;
            }

            //accommodation description = case short description
            accomm.Description = $scope.EmployeeCreateCaseModel.AccommodationRequest.OtherDescription;

            $scope.EmployeeCreateCaseModel.AccommodationRequest.Accommodations = [];
            $scope.EmployeeCreateCaseModel.AccommodationRequest.Accommodations.push(accomm);


            caseService.CreateEssCase($scope.EmployeeId, $scope.EmployeeCreateCaseModel).then(function(result) {
                $scope.CaseCreatedModel = result;
                $("#CaseCreatedSuccessfully").modal('show');
            });

        }

        $scope.modalDismiss = function () {
            $("#CaseCreatedSuccessfully").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $route.reload();
            $scope.init($scope.EmployeeId);
            if ($scope.EmployeeId == $scope.CurrentUserEmpId)
                $scope.SetMyCasesActive("MyInfo", true);
            else
                $scope.SetMyCasesActive("MyTeam", true);
            //window.location.href = '/#/employee-cases';
        }

        $scope.ReturnToHomePage = function () {
            if($scope.IsHRSupervisor) {
                $scope.SetMyCasesActive("MyTeam", true);
                $scope.showSearch = true;
            }
            else {
                $scope.SetMyInfoActive("MyInfo", true);
                $scope.showSearch = false;
            }
        }

        $scope.ApproveCase = function () {
            $scope.EmployeeCreateCaseModel.Status = 0;
            caseService.CaseReviewed($scope.EmployeeCreateCaseModel).then(function(response) {
                if(response != null) {
                    if (response.Success) {
                        $scope.CaseReviewCompleted();
                    }
                }
            });
        }


        $scope.CancelCase = function () {
            $scope.EmployeeCreateCaseModel.Status = 2;
            $scope.EmployeeCreateCaseModel.CancelReason = $scope.CaseCancelReasonSelect;
            caseService.CaseReviewed($scope.EmployeeCreateCaseModel).then(function(response) {
                if(response != null) {
                    if (response.Success) {
                        $scope.CaseReviewCompleted();
                    }
                }
            });
        }

        $scope.CaseReviewCompleted = function () {
            var resource = $resource('/Todos/' + $scope.ToDoItemId + '/EmployeeCaseReviewed', {
            }, {
                'save': {
                    method: 'POST', isArray: false
                }
            });
            resource.save().$promise.then(function(response, status) {
                window.location.href = "/Cases/" + $scope.CaseId + "/View";
            });
        };

        $scope.CancelReview = function () {
            window.location.href = "/Cases/" + $scope.CaseId + "/View";
        }

        $scope.FilterReasons = function (item) {
            if($scope.IsAccommodation) {
                var children = $filter('filter') (item.Children, { Code: 'ACCOMM' });
                if (children.length > 0)
                    return true;
                else
                    return false;
            }
            else {
                if ($scope.EmployeeCreateCaseModel.LeaveFor == 'Family') {
                    var children = $filter('filter') (item.Children, { IsForFamilyMember: true });
                    return (item.IsForFamilyMember == true || (item.IsCategory == true && children.length > 0));
                }
                else
                    return item.IsForFamilyMember == false || item.IsCategory == true;
            }



        }

        //#endregion "Steps submit and validation
        $scope.SetType = function (accom) {
            $scope.EmployeeCreateCaseModel.AccommodationRequest.Type = accom;
        };

        $scope.print = function () {
            var DocumentContainer = document.getElementById('printSection');
            var WindowObject = window.open("", "PrintWindow",
            "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
    }]);
