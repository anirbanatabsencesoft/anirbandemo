﻿angular
.module('App.Controllers')
.controller('WorkflowListCtrl', ['$scope', '$http', '$window', '$filter', '$sce', '$q', 'userPermissions', 'administrationService', 'lookupService', 'workflowService',
    function ($scope, $http, $window, $filter, $sce, $q, userPermissions, administrationService, lookupService, workflowService) {
        $scope.Workflows = [];
        $scope.WorkflowCriteria = {
            Code: '',
            Name: '',
            CustomerId: '',
            DoPaging: false,
            PageNumber: 1,
            PageSize: 10,
            TotalRecords: 0,
            SortBy: "Name",
            SortDir: "asc",
            LoadMore: true
        };
        $scope.InitList = function (customerId) {
            $scope.CustomerId = customerId;
            $scope.InitCompanyInformation();
            $scope.WorkflowCriteria.CustomerId = customerId;
            $scope.LoadWorkflowList(false);
        };

        $scope.InitCompanyInformation = function () {
            administrationService.GetBasicInfo().then(function (data) {
                $scope.BasicInfo = data;
            });
        }

        $scope.LoadWorkflowList = function (doPaging) {
            $scope.WorkflowCriteria.DoPaging = doPaging;
            workflowService.ListWorkflows($scope.WorkflowCriteria).$promise.then(function (data, status) {
                $scope.WorkflowCriteria.TotalRecords = data.Total;
                $scope.WorkflowCriteria.LoadMore = true;
                if (doPaging) {
                    angular.forEach(data.Results, function (result) {
                        $scope.Workflows.push(result);
                    });
                } else {
                    $scope.Workflows = data.Results;
                }
            });
        }

        $scope.ResetWorkflows = function () {
            $scope.Workflows = [];
            $scope.WorkflowCriteria.PageNumber = 1;
            $scope.LoadMore = true;
        }

        $scope.LoadMoreWorkflows = function () {
            var totalPages = Math.ceil($scope.WorkflowCriteria.TotalRecords / $scope.WorkflowCriteria.PageSize);
            if (totalPages > 1 && $scope.WorkflowCriteria.LoadMore && $scope.WorkflowCriteria.PageNumber < totalPages) {
                $scope.WorkflowCriteria.PageNumber++;
                $scope.LoadWorkflowList(true);
            }
        }

        $scope.UpdateWorkflowSort = function (sortBy) {
            $scope.ResetWorkflows();
            if ($scope.WorkflowCriteria.SortBy == sortBy) {
                $scope.WorkflowCriteria.SortDir = $scope.WorkflowCriteria.SortDir == 'desc' ? 'asc' : 'desc';
            } else {
                $scope.WorkflowCriteria.SortBy = sortBy;
                $scope.WorkflowCriteria.SortDir = 'asc';
            }

            $scope.LoadWorkflowList(false);
        }

        $scope.FilterWorkflows = function () {
            $scope.ResetWorkflows();
            $scope.LoadWorkflowList(false);
        }

        $scope.GoToWorkflowEdit = function (workflow) {
            $window.location.href = '/Workflow/' + $scope.CustomerId + '/Edit/' + workflow.Id;
        }

        $scope.Back = function () {
            $window.history.back();
        }

    }]);