//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
(function () {
    'use strict';
    angular
    .module('App.Controllers')
    .controller('ChangePasswordCtrl', ChangePasswordCtrl);

    ChangePasswordCtrl.$inject = ['$scope', '$modal'];
    function ChangePasswordCtrl($scope, $modal) {
        var vm = $scope;
        angular.extend(vm, {
            openChangePasswordModal: openChangePasswordModal
        });

        function openChangePasswordModal(userId) {
            var $modalInstance = $modal.open({
                templateUrl: 'ChangePasswordModal.html',
                controller: ChangePasswordCtrlInstance,
                resolve: {
                    userId: function () {
                        return userId;
                    }
                }
            });
        }
    }

    ChangePasswordCtrlInstance.$inject = ['$scope', '$http', 'userId'];
    function ChangePasswordCtrlInstance($scope, $http, userId) {
        var vm = $scope;
        angular.extend(vm, {
            userId : userId,
            UpdatePassword : UpdatePassword,
            cancel : cancel
        });

        function UpdatePassword(IsfrmChangePasswordValid, ChangePasswordModel) {
            ChangePasswordModel.UserId = vm.userId;
            if (ChangePasswordModel.NewPassword != ChangePasswordModel.ConfirmNewPassword) {
                IsfrmChangePasswordValid = false;
            }
            if (IsfrmChangePasswordValid) {
                $http({ method: 'POST', url: "/Administration/UpdatePassword/", data: ChangePasswordModel })
                .then(function (result) {
                    if (result.data.Error == null) {

                        //$scope.frmChangePassword.$setPristine();

                        vm.submitted = false;
                        vm.$close();
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Password changed successfully" });
                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.data.Error });
                    }

                },
                vm.ErrorHandler);
            }

        }

        function cancel (){
            vm.$dismiss('cancel');
        }
    }

})();