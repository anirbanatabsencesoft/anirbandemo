//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
//#region CreateCommunicationController
angular
    .module('App.Controllers')
    .controller('CreateCommunicationCtrl', ['$scope', '$http', '$window', '$timeout', '$filter', '$interpolate', 'toDoListManager', 'toDoSuccessModalService', 'lookupService', 'communicationService', 'contactService',
        function ($scope, $http, $window, $timeout, $filter, $interpolate, toDoListManager, toDoSuccessModalService, lookupService, communicationService, contactService) {
            var select2Configuration = {
                multiple: true,
                ajax: {
                    dataType: 'json',
                    data: function (term) {
                        return {
                            searchTerm: term // search term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    }
                },
                //Allow manually entered text in drop down.
                createSearchChoice: function (term, data) {
                    if ($(data).filter(function () {
                        return this.text.localeCompare(term) === 0;
                    }).length === 0) {
                        return { id: term, text: term, Email: term, Fax: '', FirstName: term, LastName: '', Type: 1 };
                    }
                },
                tokenSeparators: [';', ','],
                formatResult: formatText,
                formatSelection: formatText,

    };

            // Format Select2 Options
            function formatText(optionElement) {
                if (optionElement != undefined) {
                    if (optionElement.text != undefined && optionElement.text.indexOf("Pers Email") != -1) {
                        return '<b>' + optionElement.text + '</b>';// format option text
                    }
                    else {
                        return optionElement.text
                    }
                }
                return optionElement;
            }

            // the model we will use for most of the Create Communication page
            $scope.CreateCommunicationModel = new Object();
            $scope.CaseId = null;
            $scope.TemplateKey = "";
            $scope.NewAttachments = [];
            $scope.NewAttachmentsCount = 0;
            $scope.UploadedAttachments = [];
            $scope.IsUploadInProgress = false;
            $scope.IsComposeBlankCommunication = false;
            $scope.isDisabled = false;
            $scope.IsResend = false;
            $scope.CommunicationId = null;
            $scope.isContentLoaded = false;

    $scope.init = function (caseId, employeeId, toDoItemId, templateKey, isResend, communicationId) {
        $scope.TemplateKey = templateKey;
        $scope.CaseId = caseId;
        $scope.EmployeeId = employeeId;
        $scope.ToDoItemId = toDoItemId;
        $scope.IsResend = isResend;
        $scope.CommunicationId = communicationId;

        if (!templateKey) {
            $scope.IsComposeBlankCommunication = true;
        }

        $scope.GetCommunicationList();

        lookupService.GetPacketTypes().then(function (response) {
            $scope.PacketTypes = response;
            $scope.PacketTypes.push({ Value: '', Text: 'Blank' });
        });

        lookupService.GetEmailRepliesTypes().then(function (response) {
            $scope.EmailRepliesTypes = response;
          
        });


        toDoListManager.InitToDosFilter($scope);
        var d = new Date();
        $scope.ToDosFilter.Due = (d.getUTCMonth() + 1).toString() + '/' + d.getUTCDate().toString() + '/' + d.getUTCFullYear().toString();
        $timeout(function () {//Simulate a request
            $scope.CreateCummunicationViewModel();
        }, 1500);

        $(function () {
            $('#caseAttachments').fileupload({
                dataType: 'json',
                autoUpload: false,
                sequentialUploads: true,
                singleFileUploads: true,
                replaceFileInput: false
            });
        });

        $('#caseAttachments').bind('fileuploadadd', function (e, data) {
            // Add the files to the list
            numFiles = $scope.NewAttachments.length

            if (data.files.length > 0)
                $scope.isDisabled = false;

            for (var i = 0; i < data.files.length; ++i) {
                var file = data.files[i];
                // .$apply to update angular when something else makes changes
                $scope.$apply(
                    $scope.NewAttachments.push({
                        FileName: file.name,
                        FileData: data,
                        Description: "",
                        AttachmentType: 2,
                        AttachmentTypeName: "Paperwork",    // (attachments are for the case, paperwork are for a communication, also known as enclosures; we can rename it later, for now it’s paperwork)
                        IsUploading: false,
                        Message: "",
                        ShowCancel: false,
                        ShowMessage: false
                    }));
            }
        });

        select2Configuration.ajax.url = "/employees/" + $scope.EmployeeId + "/case/" + $scope.CaseId + "/contacts/quickfind";
        $("input[name='To'].select2-multiple").select2(select2Configuration);
        $("input[name='CC'].select2-multiple").select2(select2Configuration);

        $scope.$watchCollection('UploadedAttachments', function (newAttachements, oldAttachements) {
            if ($scope.UploadedAttachments.length == $scope.NewAttachmentsCount && $scope.UploadedAttachments.length > 0) {
                $scope.AttachmentGroup = -1;
                $scope.AttachmentGroupNotSelected = false;
                $('#uploadModalDialog').modal('toggle');
                $scope.NewAttachmentsCount = 0;
            }
        });

        //Show modal after clicking print/send
        function showModel() {
            //Open Modal
            $('#SentSuccessfully').modal('show');
        }
    };

    $scope.CreateCommunication = function () {
        var communicationModel = $scope.PrepareCommunicationModel();
        $scope.CreateCummunicationUrl = null;
        if ($scope.ToDoItemId) {
            $scope.CreateCummunicationUrl = "/Todos/" + $scope.ToDoItemId + "/Communications/Create";
        } else {
            $scope.CreateCummunicationUrl = "/Cases/" + $scope.CaseId + "/Communications/Create";
        }

                $http({
                    method: 'POST',
                    url: $scope.CreateCummunicationUrl,
                    data: communicationModel
                }).then(function (d) {
                    var response = d.data;
                    if (response) {
                        // $scope.CommunicationId = response.CommunicationId;
                        var communicationAttachments = { caseId: response.CaseId, Paperwork: response.Paperwork, CommunicationType: response.CommunicationTypeValue };
                        communicationAttachments.Paperwork = communicationAttachments.Paperwork || [];
                        communicationAttachments.Paperwork.splice(0, 0, {
                            AttachmentId: response.AttachmentId,
                            Paperwork: {
                                Name: response.CommunicationName,
                                FileName: response.CommunicationName
                            }
                        });
                        communicationAttachments.CaseAttachments = communicationModel.CaseAttachments;
                        var showTodo = false;
                        if ($scope.ToDoItemId)
                            showTodo = true;

                        $scope.ToDosFilter.CaseId = response.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                        //TODO: it should be async/callback thing instead of timeout
                        $timeout(function () {//Simulate a request
                            if ($scope.ToDos) {
                                toDoSuccessModalService.OpenTodoSuccessModal($scope.ToDos, true, communicationAttachments, response.CaseId, null, showTodo, response.CommunicationId);
                            } else {
                                //error while fetching todo list
                            }
                        }, 1000);
                    }
                }, function (response) {
                    $scope.isDisableSend = false;
                });
            };

            $scope.PrepareCommunicationModel = function () {

                var recipients = [];
                var toAddresses = $("#to_emails").select2("data");
                var ccrecipients = [];
                var ccAddresses = $("#cc_emails").select2("data");

        for (var i = 0; i < toAddresses.length; i++) {
            if (toAddresses[i]) {
                var contact = { Email: '', FirstName: '', LastName: '', Fax: '', Type: 0 };
                contact.Email = toAddresses[i].Email;
                contact.FirstName = toAddresses[i].FirstName;
                contact.LastName = toAddresses[i].LastName;
                contact.Fax = toAddresses[i].Fax;
                contact.Type = toAddresses[i].Type;
                recipients.push(contact);
            }
        }

        for (var i = 0; i < ccAddresses.length; i++) {
            if (ccAddresses[i]) {
                var contact = { Email: '', FirstName: '', LastName: '', Fax: '', Type: 0 };
                contact.Email = ccAddresses[i].Email;
                contact.FirstName = ccAddresses[i].FirstName;
                contact.LastName = ccAddresses[i].LastName;
                contact.Fax = ccAddresses[i].Fax;
                contact.Type = ccAddresses[i].Type;
                ccrecipients.push(contact);
            }
        }
    
        if (!$scope.IsResend) {
            $scope.CreateCommunicationModel.CommunicationId = $scope.CommunicationId;
        }
        $scope.CreateCommunicationModel.Recipients = recipients;
        $scope.CreateCommunicationModel.CCRecipients = ccrecipients;
        $scope.CreateCommunicationModel.Message = GetActiveEditorContent();
        $scope.CreateCommunicationModel.TemplateKey = $scope.TemplateKey;

        var communicationModel = {};
        $scope.CreateCommunicationModel.ToDoItemId = $scope.ToDoItemId;
        angular.copy($scope.CreateCommunicationModel, communicationModel);

        // Discard unchecked attachment from the Paperwork array to be sent to server
        if (communicationModel.Paperwork != undefined && communicationModel.Paperwork != null) {
            for (var index = (communicationModel.Paperwork.length - 1) ; index >= 0; index--) {
                if (!communicationModel.Paperwork[index].Checked) {
                    communicationModel.Paperwork.splice(index, 1);
                }
            }
        }
        // Discard unchecked case attachment from the CaseAttachements array to be sent to server
        if (communicationModel.CaseAttachments != undefined && communicationModel.CaseAttachments != null) {
            for (var index = (communicationModel.CaseAttachments.length - 1) ; index >= 0; index--) {
                if (!communicationModel.CaseAttachments[index].Checked) {
                    communicationModel.CaseAttachments.splice(index, 1);
                }
            }
        }

        return communicationModel;
    }

            $scope.AddTextAndIdToRecipients = function (recipients) {
                for (var i = 0; i < recipients.length; i++) {
                    recipients[i].id = recipients[i].Id;
                    var text = recipients[i].FirstName + ' ' + recipients[i].LastName;
                    var email = recipients[i].Email;
                    var altEmail = recipients[i].AltEmail;
                    if (email) {
                        if (recipients[i].FirstName !== null || recipients[i].LastName !== null)
                            text += ' (Work Email - ' + email + ')';
                        else
                            text = email;
                    }

                    else if (altEmail)
                        text += ' (Alt Email - ' + email + ')';

            recipients[i].text = text;
        }
    };

    $scope.CreateCummunicationViewModel = function () {

        communicationService.CreateCummunicationViewModel($scope.ToDoItemId, $scope.CaseId, $scope.TemplateKey, $scope.CommunicationId)
            .then(function (result) {
                $scope.CreateCommunicationModel = result;
                result.CommunicationTypeValue = $("#hiddenCommType").val();
                if (result.Recipients) {
                    $scope.AddTextAndIdToRecipients(result.Recipients);
                    $("#to_emails").select2('data', result.Recipients);
                }

                if (result.CCRecipients) {
                    $scope.AddTextAndIdToRecipients(result.CCRecipients);
                    $("#cc_emails").select2('data', result.CCRecipients);
                }
                    

                $scope.LastSavedDate = result.ModifiedDate;
                /// set "Checked" to true for Preloaded paperwork
                angular.forEach($scope.CreateCommunicationModel.Paperwork, function (item, index) {
                    item.Checked = true;
                });
                /// set "Checked" to false for Case Attachments
                angular.forEach($scope.CreateCommunicationModel.CaseAttachments, function (item, index) {
                    item.Checked = false;
                    item.CreatedDate = item.cdt;
                    if ($scope.CreateCommunicationModel.SelectedCaseAttachmentIds != undefined && $scope.CreateCommunicationModel.SelectedCaseAttachmentIds != null &&
                        $scope.CreateCommunicationModel.SelectedCaseAttachmentIds.indexOf(item.Id) !== -1) {
                        item.Checked = true;
                    }
                });

                //get active content here
                if (result.Message) {
                    tinyMCE.activeEditor.setContent(result.Message);
                } else {
                    tinyMCE.activeEditor.setContent("");
                }

                $scope.isContentLoaded = true;
            });
    };

    var resetUploadInProgress = function () {
        var anyFileUploading = false;
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            if ($scope.NewAttachments[i].IsUploading) {
                anyFileUploading = true;
                break;
            }
        }
        $scope.IsUploadInProgress = anyFileUploading;
    };

    $scope.UploadFiles = function () {
        $scope.UploadedAttachments.length = 0;
        $scope.NewAttachmentsCount = $scope.NewAttachments.length;
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            var attachment = $scope.NewAttachments[i];

                    attachment.FileData.formData = {
                        AttachmentType: attachment.AttachmentType,
                        Description: attachment.Description,
                        Id: $scope.CaseId,
                        AttachmentId: attachment.AttachmentId
                    };
                    attachment.Message = "Uploading " + attachment.FileName;
                    attachment.IsUploading = true;
                    attachment.ShowMessage = true;
                    var scopeSubmit = function (att) {
                        att.FileData.submit({ async: false })
                            .then(function (response) {
                                var result = response;
                                if (result.Message == "Success") {
                                    $scope.UploadedAttachments.push(result.Attachment);
                                    att.IsUploading = false;

                                    $scope.CancelFileUpload(att);
                                    resetUploadInProgress();
                                }
                            }, function (jqXHR) {

                                var message = "";
                                if (jqXHR.indexOf("Maximum request length exceeded") != -1)
                                    message = att.FileName + " reached the maximum upload file size limit & cannot be uploaded";
                                else
                                    message = "Some error occured while uploading " + att.FileName;
                                att.Message = message;
                                att.ShowCancel = true;
                                $scope.$apply(att);
                                resetUploadInProgress();
                            });
                    };
                    scopeSubmit(attachment);
                }
            };

    $scope.CancelFileUpload = function (attachment) {
        var position = $.inArray(attachment, $scope.NewAttachments);
        if (~position) $scope.$apply($scope.NewAttachments.splice(position, 1));
    }

            $scope.SaveAtachment = function () {
                if ($scope.AttachmentGroup == 1 || $scope.AttachmentGroup == 2) { $('#uploadModalDialog').modal('toggle'); }
                else { $scope.AttachmentGroupNotSelected = true; return; }
                angular.forEach($scope.UploadedAttachments, function (value, key, obj) {
                    if ($scope.AttachmentGroup == 1) {
                        var paperwork = { Description: '', FileId: null, FileName: null, Name: '' };
                        paperwork.Description = value.Description;
                        paperwork.Name = value.FileName;
                        value.Checked = true;
                        $timeout(function () {
                            $scope.$apply(
                                $scope.CreateCommunicationModel.Paperwork.push({
                                    AttachmentId: value.Id,
                                    Checked: true,
                                    DueDate: null,
                                    AttachmentType: value.AttachmentType,
                                    AttachmentTypeName: value.AttachmentTypeName,
                                    Paperwork: paperwork
                                })
                            );
                        },10);
                    }
                    else if ($scope.AttachmentGroup == 2) {
                        value.Checked = true;
                        $timeout(function () {
                            $scope.$apply($scope.CreateCommunicationModel.CaseAttachments.push(value));
                        }, 10);
                    }
                });
                $scope.UploadedAttachments.length = 0;
                $scope.NewAttachmentsCount = 0;
                $scope.AttachmentGroup = -1;
                $scope.AttachmentGroupNotSelected = false;
            }
            $scope.setCommunicationType = function () {
                $scope.CreateCommunicationModel.CommunicationType = $scope.CreateCommunicationModel.CommunicationTypeValue;
            }

    $scope.setEmailRepliesType = function () {
        $scope.CreateCommunicationModel.EmailRepliesType = $scope.CreateCommunicationModel.EmailRepliesTypeValue;
    }

    $scope.GetCommunicationList = function () {
        $http.get('/Cases/Communications/GetCommunicationList', {
            params: { CaseId: $scope.CaseId }
            //config: {withCredentials: true}
        }).then(function (distribution) {
            $scope.GetCommunicationLists = distribution.data;
        });
    };

    $scope.PacketTypechange = function () {
        $scope.CreateCummunicationViewModel();
    };
    //#endregion


    //OpenContactsModal
    $scope.OpenContactsModal = function () {
        contactService.OpenContactsModal($scope, $scope.EmployeeId);
    };

    $scope.SelectAllAttachments = function (attachmentGroup) {
        if (attachmentGroup == 1) {
            $scope.CheckAllCheckboxes($scope.CreateCommunicationModel.Paperwork);
        }
        else if (attachmentGroup == 2) {
            $scope.CheckAllCheckboxes($scope.CreateCommunicationModel.CaseAttachments);
        }
    }

    $scope.CheckAllCheckboxes = function (attachmentList) {
        if (!attachmentList)
            return;

        angular.forEach(attachmentList, function (item, index) {
            item.Checked = true;
        });
    }

    $scope.SaveAsDraft = function () {
        var communicationModel = $scope.PrepareCommunicationModel();
        communicationService.SaveAsDraft(communicationModel).$promise.then(function (communication) {
            noty({ text: 'Communication Saved Successfully', layout: 'topRight', type: 'success', timeout: 10000 });
            $scope.LastSavedDate = new Date();
            $scope.CommunicationId = communication.Id;
            $scope.CreateCommunicationModel.Id = communication.Id;
            $scope.CreateCommunicationModel.IsDraft = true;
        });
    }

    $scope.DeleteDraft = function () {
        var draftsToDelete = [];
        var draft = {
            CommunicationId: $scope.CommunicationId
        };
        draftsToDelete.push(draft);
        communicationService.DeleteDrafts(draftsToDelete).$promise.then(function () {
            window.location.href = '/Cases/' + $scope.CaseId + '/Communications';
        });  
    };
}]);