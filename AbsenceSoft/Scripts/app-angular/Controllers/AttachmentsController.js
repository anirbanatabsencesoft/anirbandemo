//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('AttachmentsCtrl', ['$scope', '$http', 'attachmentListManager', function ($scope, $http, attachmentListManager) {
    $scope.CreationDate = null;
    $scope.CaseId = null;
    $scope.NewAttachments = [];
    $scope.Attachments = [];
    $scope.IsUploadInProgress = false;
    $scope.isDisabled = false;
    $scope.DefaultAttachmentToConfidential = false;

    $scope.CaseAttachmentRequest = null;
    $scope.HasCaseAttachmentRequest = false;
    $scope.submitted = false;
    $scope.WarningYesNo = "No";
    $scope.ErrorYesNo = "No";
    $scope.InfoYesNo = "No";
    $scope.forms = {};
    
    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };

    $scope.init = function (caseId, creationDate, defaultAttachmentToConfidential) {
        $scope.CaseId = caseId;
        $scope.DefaultAttachmentToConfidential = defaultAttachmentToConfidential == 'True';
        //$scope.GetAttachments();
        
        attachmentListManager.Init($scope, true);
        
        if (true) {
            //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                    attachmentListManager.LoadMore($scope, $scope.ErrorHandler);
                }
            });
        }

        //To do filter due date watch
        $scope.$watch('AttachmentsFilter.CreatedDate', function () {
            $scope.GetAttachments(false);
        });

        $scope.CreationDate = creationDate;
        $(function () {
            $('#caseAttachments').fileupload({
                dataType: 'json',
                autoUpload: false,
                sequentialUploads: true,
                singleFileUploads: true,
                replaceFileInput: false
            });
        });
        
        $('#caseAttachments').bind('fileuploadadd', function (e, data) {
            // Add the files to the list
            numFiles = $scope.NewAttachments.length
            if (data.files.length > 0)
                $scope.isDisabled = false;

            for (var i = 0; i < data.files.length; ++i) {
                var file = data.files[i];
                // .$apply to update angular when something else makes changes
                $scope.$apply(
                    $scope.NewAttachments.push({
                        FileName: file.name,
                        FileData: data,
                        Description: "",
                        AttachmentType: 2,
                        AttachmentTypeName: "Communication",
                        IsUploading: false,
                        AttachmentId: $scope.NewAttachments.length + 1,
                        Public: !$scope.DefaultAttachmentToConfidential,
                        Message: "",
                        ShowCancel: false,
                        ShowMessage:false
                    }));
            }
        });
    };

    //#region Edit Attachment
    $scope.initEditCARequest = function (IsRequest) {
        $scope.forms.submitted = false;
        
        if (IsRequest) {
            $scope.CaseAttachmentRequest = {
                Id: '',
                FileName: '',
                Description: '',
                CaseId: $scope.CaseId,
                HasAnyRequests: false,
            };
        }

        $scope.IsRequest = IsRequest;
        
        $scope.forms.ChangeStatusCaseAttachment = false;
        $scope.frmEditCaseAttachmentRequest.$setPristine();
        
    };

    $scope.SetCaseAttachmentRequestToEdit = function (caseAttachmentRequest) {
        $scope.forms.editSubmitted = false;
        $scope.EditCaseAttachmentRequest = caseAttachmentRequest;
        $scope.EditCaseAttachmentRequestCopy = angular.copy($scope.EditCaseAttachmentRequest);
        $scope.EnableCaseAttachments();
    };

    $scope.cancelCaseAttachmentRequests = function () {
        $scope.EditCaseAttachmentRequest = angular.copy($scope.EditCaseAttachmentRequestCopy);
        for (var i = 0; i < $scope.Attachments.length; i++) {
            if ($scope.Attachments[i].Id == $scope.EditCaseAttachmentRequest.Id)
            {
                $scope.Attachments[i].FileName = $scope.EditCaseAttachmentRequest.FileName;
                $scope.Attachments[i].Description = $scope.EditCaseAttachmentRequest.Description;
                return;
            }
        }
    };
    
    $scope.EnableCaseAttachments = function () {
        $scope.initEditCARequest(true);
        $scope.OpenCaseAttachmentsModal();
    };

    $scope.OpenCaseAttachmentsModal = function () {
        $("#EditCaseAttachmentRequest").modal('show');
    };

    $scope.ModifyCaseAttachment = function (IsValid, IsCreate, IsRequest) {
        if (IsRequest) {
            var attachmentId = $scope.EditCaseAttachmentRequest.Id;
            var fileName = $scope.EditCaseAttachmentRequest.FileName;
            var description = $scope.EditCaseAttachmentRequest.Description;

            var req = {
                method: 'POST',
                url: '/Cases/' + $scope.CaseId + '/ModifyCaseAttachmentRequest',
                data: { caseId: $scope.CaseId, attachmentId: attachmentId, fileName: fileName, description: description }
            }

            $http(req).then(function (data) {
                $("#EditCaseAttachmentRequest").modal('hide');
                $scope.submitted = false;
                $scope.forms.submitted = false;
                $scope.forms.editSubmitted = false;
                $scope.frmEditCaseAttachmentRequest.$setPristine();

                $scope.GetAttachments(false);

            }, function () { });
        }
    };

    //#endregion Edit Attachement

    $scope.UpdateAttachmentSort = function (sortBy) {
        attachmentListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
    };

    $scope.GetAttachments = function (doPaging) {
        attachmentListManager.List($scope, doPaging, $scope.ErrorHandler);
    };

    
    $scope.Delete = function (attachment) {
        if (confirm("Do you want to delete this attachment??")) {
            $http.delete('/Cases/' + $scope.CaseId + '/Attachments/' + attachment.Id).then(function (data) {
                for (var i = 0; i <= $scope.Attachments.length - 1; i++) {
                    if ($scope.Attachments[i].Id == attachment.Id) {
                        $scope.Attachments.splice(i, 1);
                    }
                }
            });
        }
    };

    var getNewAttachmentById = function (id) {
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            if ($scope.NewAttachments[i].AttachmentId == id)
                return $scope.NewAttachments[i];
        }
        return undefined;
    };

    var resetUploadInProgress = function () {
        var anyFileUploading = false;
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            if ($scope.NewAttachments[i].IsUploading)
            {
                anyFileUploading = true;
                break;
            }
        }        
        $scope.IsUploadInProgress = anyFileUploading;
    };

    $scope.UploadFiles = function () {        
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            var attachment = $scope.NewAttachments[i];            
            attachment.FileData.formData = {
                AttachmentType: attachment.AttachmentType,
                Description: attachment.Description,
                Id: $scope.CaseId,
                AttachmentId: attachment.AttachmentId,
                Public: attachment.Public
            };
            attachment.Message = "Uploading "+attachment.FileName;
            attachment.IsUploading = true;
            attachment.ShowMessage = true;
            attachment.FileData.submit({async:false})
                .then(function (response) {                    
                   var result = response;
                    if (result.Message == "Success") {
                    var uploadedAttachment = getNewAttachmentById(result.Attachment.AttachmentId);
                    uploadedAttachment.IsUploading = false;
                    $scope.$apply(
                        $scope.Attachments.unshift({
                            Id: result.Attachment.Id,
                            FileName: result.Attachment.FileName,
                            Description: result.Attachment.Description,
                            AttachmentType: result.Attachment.AttachmentType,
                            AttachmentTypeName: result.Attachment.AttachmentTypeName,
                            CreatedDate: result.Attachment.CreatedDate,
                            FileId: result.Attachment.FileId,
                            ContentLength: result.Attachment.ContentLength,
                            ContentType: result.Attachment.ContentType,
                            AttachedBy : result.Attachment.AttachedBy
                        })
                    );
                    $scope.CancelFileUpload(uploadedAttachment);
                    resetUploadInProgress();
                }
            }, 
            function (jqXHR) {
                var uploadedAttachment = getNewAttachmentById(this.formData.AttachmentId);
                var message = "";
                if (jqXHR.indexOf("Maximum request length exceeded") != -1) 
                    message = uploadedAttachment.FileName + " reached the maximum upload file size limit & cannot be uploaded";
                else
                    message = "Some error occured while uploading " + uploadedAttachment.FileName;
                uploadedAttachment.Message = message;
                uploadedAttachment.ShowCancel = true;
                $scope.$apply(uploadedAttachment);
                resetUploadInProgress();
            })            
        }
    };

    $scope.CancelFileUpload = function (attachment) {
        var position = $.inArray(attachment, $scope.NewAttachments);
        if (~position) $scope.$apply($scope.NewAttachments.splice(position, 1));
    };

    $scope.GetViewCaseUrl = function () {
            window.location.href = "/Cases/" + $scope.CaseId + "/View";
    };

}]);