//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployeeCasesIndexCtrl', ['$scope', '$timeout', 'employeeCaseListManager', function ($scope, $timeout, employeeCaseListManager) {

    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };
    $scope.IsTPA = false;

    $scope.init = function (callingPage) {
        var infiniteScrolling = callingPage == 'index';
        employeeCaseListManager.Init($scope, infiniteScrolling);
        $('.nav-pills li:first-child a').tab('show');
    }

    //$scope.LoadMoreCases = function () {
    //    employeeCaseListManager.LoadMore($scope, $scope.ErrorHandler);
    //};

    //$scope.GetCases = function (doPaging) {
    //    employeeCaseListManager.Filter($scope, doPaging, $scope.ErrorHandler);
    //};

    //$scope.UpdateCaseSort = function (sortBy) {
    //    employeeCaseListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
    //};

}]);