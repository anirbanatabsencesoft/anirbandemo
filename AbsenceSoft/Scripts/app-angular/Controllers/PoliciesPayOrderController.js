//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('PoliciesPayOrderCtrl', ['$scope', '$http', '$filter', '$rootScope', 'policyService', 'administrationService', '$window', '$q',
    function ($scope, $http, $filter, $rootScope, policyService, administrationService, $window, $q) {
        $scope.Init = function (employerId) {
            $scope.EmployerId = employerId;
            administrationService.GetEmployerInfo($scope.EmployerId).then(function (empData) {
                $scope.EmployerName = empData.SignatureName;
            });

            policyService.GetPoliciesPayOrder($scope.EmployerId).then(function (policies) {
                $scope.Policies = policies;
            })
        }

        $scope.Save = function () {
            var data = {
                EmployerId: $scope.EmployerId,
                PolicyPayOrder: $scope.Policies
            }
            policyService.SavePoliciesPayOrder(data).then(function (policies) {
                $scope.Policies = policies;
                var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Pay order successfully updated" });
            });
        }

        /// Moving a policy down means we need to increase the order
        /// so that the policy is paid later
        $scope.MovePolicyOrderDown = function (policy) {
            /// let's find the policy that has the order we're going to change to
            var policyToSwap = getPolicyByOrder(policy.Order + 1);
            policyToSwap.Order--;
            policy.Order++;
        }

        /// Moving a policy up means we need to decrease the order
        /// so the policy pays out sooner
        $scope.MovePolicyOrderUp = function (policy) {
            /// let's find the policy that has the order we're going to change to
            var policyToSwap = getPolicyByOrder(policy.Order -1);
            policyToSwap.Order++;
            policy.Order--;
        }

        $scope.Back = function () {
            $window.history.back();
        }

        getPolicyByOrder = function (order) {
            for (var i = 0; i < $scope.Policies.length; i++) {
                var policy = $scope.Policies[i];
                if (policy.Order == order)
                    return policy;
            }
        }
    }]);