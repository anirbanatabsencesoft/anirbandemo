﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: enable strict mode and fix any violations
(function (undefined) {
    'use strict';

    angular

    .module('App.Controllers')

    .controller('EditSecuritySettingsModalCtrl', ['$scope', '$http', 'administrationService', '$filter', '$modalInstance', '$rootScope', function ($scope, $http, administrationService, $filter, $modalInstance, $rootScope) {
        $scope.SignUpData = {
            Customer: {
                SecuritySettings: {
                    ShowInactiveLogoutPrompt: true,
                    InactiveLogoutPeriod: null,
                    ShowSelfServiceInactiveLogoutPrompt: true,
                    SelfServiceInactiveLogoutPeriod: null
                },
                PasswordPolicy: {
                    ExpirationDays: 90,
                    MinimumLength: 8,
                    ReuseLimitDays: 0,
                    ReuseLimitCount: 0
                }
            }
        }

        $scope.forms = {};

        $scope.init = function (model) {
            administrationService.GetSecuritySettings()
                .then(function(data) {
                    if (data != null) {
                        $scope.SignUpData.Customer = data;
                        if (!$scope.SignUpData.Customer.SecuritySettings) {
                            $scope.SignUpData.Customer.SecuritySettings = {
                                ShowInactiveLogoutPrompt: true,
                                InactiveLogoutPeriod: null,
                                ShowSelfServiceInactiveLogoutPrompt: true,
                                SelfServiceInactiveLogoutPeriod: null
                            };
                        }
                    }
                });
        };

        
        $scope.Error = null;

        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.UpdateSecuritySettings = function (IsValid) {
            if (IsValid) {
                administrationService.UpdateSecuritySettings($scope.SignUpData.Customer).then(function (result) {
                    if (result.Error == null) {
                        //var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Company info updated successfully." });
                        $modalInstance.dismiss('cancel');

                        // Refresh parent page
                        administrationService.GetBasicInfo().then(function (data) {
                            if (data != null) {
                                $rootScope.BasicInfo = data;
                            }
                        });

                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: result.Error });
                    }

                },
                $scope.ErrorHandler);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);

})();
