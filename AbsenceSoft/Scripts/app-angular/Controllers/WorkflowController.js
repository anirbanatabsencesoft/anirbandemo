﻿angular
.module('App.Controllers')
.controller('WorkflowCtrl', ['$scope', '$http', '$window', '$filter', '$sce', '$q', 'userPermissions', 'administrationService', 'lookupService', 'workflowService',
    function ($scope, $http, $window, $filter, $sce, $q, userPermissions, administrationService, lookupService, workflowService) {
        $scope.WorkflowCriteria = {
            CustomerId: '',
            PageNumber: 1,
            PageSize: 500,
            SortBy: "Name",
            SortDir: "asc",
        };
        $scope.DesignWorkflow = false;
        $scope.CustomerId = '';
        $scope.Workflows = [];
        $scope.RuleExpressions = [];
        $scope.Activities = [];
        $scope.Employers = [];
        $scope.Workflow = {};
        $scope.RuleGroupExpressions = [];
        $scope.ManualEventType = 0;
        $scope.TextControl = 1;
        $scope.SelectControl = 2;
        $scope.DateControl = 4;
        $scope.NumericControl = 6;
        $scope.CheckboxControl = 8;
        $scope.ToggleWorkflowText = "Design Workflow";
        $scope.ActivityFilter = '';
        $scope.Toolkit = {};
        $scope.Renderer = {};
        $scope.NodeData = {};
        $scope.NodeIdFunction = function (n) {
            return n.id;
        }

        $scope.NodeTypeFunction = function (n) {
            return n.type;
        }

        $scope.ToolkitConfiguration = {
            idFunction: $scope.NodeIdFunction,
            typeFunction: $scope.NodeTypeFunction,
            nodeFactory: function (type, data, callback) {
                callback(data);
            }
        };
        $scope.NodeConfiguration = {
            droppables: [],
            dragOptions: {
                zIndex: 50000,
                cursor: "move",
                clone: true
            },
            typeExtractor: function (el) {
                return el.getAttribute("jtk-node-type").toLowerCase();
            },
            dataGenerator: function (type, draggedElement) {
                /// every 15 characters should add another 120 pixels of width
                var width = (Math.floor(draggedElement.innerHTML.length / 15) + 1) * 120;
                return {
                    text: draggedElement.innerHTML,
                    activityId: draggedElement.getAttribute("jtk-activity-id"),
                    w: width,
                    h: 80
                };
            }
        }

        $scope.RendererConfiguration = {
            container: "workflow-designer",
            view: {
                nodes: {
                    "start": {
                        template: "tmplStart",
                        parameters: {
                            w: 50,
                            h: 50
                        }
                    },
                    "selectable": {
                        events: {
                            tap: function (params) {
                                $scope.Toolkit.toggleSelection(params.node);
                            }
                        }
                    },
                    "question": {
                        parent: "selectable",
                        template: "tmplQuestion",
                        parameters: {
                            w: 120,
                            h: 120
                        }
                    },
                    "action": {
                        parent: "selectable",
                        template: "tmplAction",
                        parameters: {
                            w: 120,
                            h: 70
                        }
                    },
                    "output": {
                        parent: "selectable",
                        template: "tmplOutput",
                        parameters: {
                            w: 120,
                            h: 70
                        }
                    }
                },
                // There are two edge types defined - 'yes' and 'no', sharing a common
                // parent.
                edges: {
                    "default": {
                        connector: ["Flowchart", { cornerRadius: 5 }],
                        paintStyle: { lineWidth: 2, strokeStyle: "#f76258", outlineWidth: 3, outlineColor: "transparent" },	//	paint style for this edge type.
                        hoverPaintStyle: { lineWidth: 2, strokeStyle: "rgb(67,67,67)" }, // hover paint style for this edge type.
                        events: {
                            "dblclick": function (params) {
                                jsPlumbToolkit.Dialogs.show({
                                    id: "dlgConfirm",
                                    data: {
                                        msg: "Delete Connection"
                                    },
                                    onOK: function () {
                                        $scope.Toolkit.removeEdge(params.edge);
                                    }
                                });
                            }
                        },
                        overlays: [
                            ["Arrow", { location: 1, width: 10, length: 10 }],
                            ["Arrow", { location: 0.3, width: 10, length: 10 }]
                        ]
                    },
                    "connection": {
                        parent: "default",
                        overlays: [
                            [
                                "Label", {
                                    label: "${outcome}",
                                    events: {
                                        click: function (params) {
                                            $scope.SelectOutcome(params.edge, params.edge.source.data.activityId);
                                        }
                                    }
                                }
                            ]
                        ]
                    }
                },

                ports: {
                    "start": {
                        endpoint: "Blank",
                        anchor: "Continuous",
                        uniqueEndpoint: true,
                        edgeType: "default"
                    },
                    "source": {
                        endpoint: "Blank",
                        paintStyle: { fillStyle: "#84acb3" },
                        anchor: "AutoDefault",
                        maxConnections: -1,
                        edgeType: "connection"
                    },
                    "target": {
                        maxConnections: -1,
                        endpoint: "Blank",
                        anchor: "AutoDefault",
                        paintStyle: { fillStyle: "#84acb3" },
                        isTarget: true
                    }
                }
            },
            // Layout the nodes using an absolute layout
            layout: {
                type: "Absolute"
            },
            events: {
                canvasClick: function (e) {
                    $scope.Toolkit.clearSelection();
                },
                edgeAdded: function (params) {
                    if (params.addedByMouse && params.source.data.type != "start") {
                        $scope.SelectOutcome(params.edge, params.source.data.activityId);
                    }
                }
            },
            miniview: {
                container: "miniview"
            },
            consumeRightClick: false,
            dragOptions: {
                filter: ".jtk-draw-handle, .node-action, .node-action i"
            }
        };


        $scope.SelectOutcome = function (edge, activityId) {
            var possibleOutcomes = $scope.GetPossibleOutcomes(activityId);
            jsPlumbToolkit.Dialogs.show({
                id: "dlgOutcome",
                data: {
                    text: edge.data.outcome || "",
                    list: $scope.GetPossibleOutcomes(activityId)
                },
                onOK: function (data) {
                    $scope.Toolkit.updateEdge(edge, { outcome: data.outcome });
                },
                onCancel: function () {
                    if (!edge.data.outcome)
                        $scope.Toolkit.removeEdge(edge);
                }
            });
        };

        $scope.GetPossibleOutcomes = function (activityId) {
            for (var i = 0; i < $scope.Activities.length; i++) {
                var activity = $scope.Activities[i];
                if (activity.Id == activityId)
                    return $scope.TransformOutcomes(activity.PossibleOutcomes);
            }

            return [];
        }

        $scope.TransformOutcomes = function (outcomes) {
            var transformedOutcomes = [];
            for (var i = 0; i < outcomes.length; i++) {
                transformedOutcomes.push({
                    id: outcomes[i],
                    value: outcomes[i]
                })
            }

            return transformedOutcomes;
        }





        $scope.InitEdit = function (workflow, customerId) {
            $scope.CustomerId = customerId;
            $scope.WorkflowCriteria.CustomerId = customerId
            $scope.Workflow = workflow;
            lookupService.GetEmployersForCustomer().then(function (employers) {
                $scope.Employers = employers;
            });
            lookupService.AdminContactTypes().$promise.then(function (contactTypes) {
                var transformedContactTypes = [{
                    id: "Self",
                    text: "Employee"
                }];
                for (var i = 0; i < contactTypes.length; i++) {
                    transformedContactTypes.push({
                        id: contactTypes[i].Code,
                        text: contactTypes[i].Name
                    });
                }
                $scope.ContactTypes = transformedContactTypes;
            });
            lookupService.CaseAssigneeTypes().$promise.then(function (caseAssigneeTypes) {
                //var transformedAssigneeTypes = [{
                //    id: "ALL",
                //    text: "All"
                //}];
                var transformedAssigneeTypes = [];
                for (var i = 0; i < caseAssigneeTypes.length; i++) {
                    transformedAssigneeTypes.push({
                        id: caseAssigneeTypes[i].Code,
                        text: caseAssigneeTypes[i].Name
                    });
                }
                $scope.CaseAssigneeTypes = transformedAssigneeTypes;
            });
            $scope.InitCompanyInformation();
            $scope.GetRuleExpressions();
            $scope.GetActivities();
            $scope.InitDesigner();
            lookupService.GetPacketTypes().then(function (communications) {
                $scope.Communications = communications;
            });
            workflowService.ListWorkflows($scope.WorkflowCriteria).$promise.then(function (data) {
                $scope.Workflows = data.Results;
            });
        }

        $scope.BindSelect2 = function (selector, data) {
            $(selector).select2({
                placeholder: "Contact Types",
                allowClear: true,
                minimumInputLength: 2,
                dropdownCssClass: "bigdrop",
                multiple: true,
                tags: true,
                tokenSeparators: [',', ' '],
                createSearchChoice: function (term) {
                    if (term.indexOf('@') > -1) {
                        var trimmedTerm = $.trim(term)
                        return {
                            id: trimmedTerm,
                            text: trimmedTerm
                        }
                    }
                },
                data: data
            });
        };
        $scope.BindCaseAssigneeTypeSelect2 = function (selector, data) {
            $(selector).select2({
                placeholder: "Case Assignee Types",
                allowClear: true,
                minimumInputLength: 2,
                dropdownCssClass: "bigdrop",                                                
                createSearchChoice: function (term) {
                    var filterData = [];
                    angular.forEach($scope.CaseAssigneeTypes, function (item) {
                        if (item.text.toLowerCase().indexOf(term.toLowerCase())) {                            
                            filterData.push({id: item.id, text: item.text});
                        }
                    });
                    return filterData;
                },
                data: data
            });
        };

        $scope.InitDesigner = function () {
            jsPlumbToolkit.ready(function () {
                var toolkit = jsPlumbToolkit.newInstance($scope.ToolkitConfiguration);
                var renderer = window.renderer = toolkit.render($scope.RendererConfiguration);

                jsPlumbToolkit.Dialogs.initialize({
                    selector: ".dlg"
                });

                renderer.bind("modeChanged", function (mode) {
                    jsPlumb.removeClass(jsPlumb.getSelector("[mode]"), "selected-mode");
                    jsPlumb.addClass(jsPlumb.getSelector("[mode='" + mode + "']"), "selected-mode");
                });

                // pan mode/select mode
                jsPlumb.on(".controls", "tap", "[mode]", function () {
                    renderer.setMode(this.getAttribute("mode"));
                });

                // on home button click, zoom content to fit.
                jsPlumb.on(".controls", "tap", "[reset]", function () {
                    toolkit.clearSelection();
                    renderer.zoomToFit();
                });

                $scope.Toolkit = toolkit;
                $scope.Renderer = renderer;
                jsPlumb.on("#workflow-designer", "tap", ".node-delete, .node-delete i", $scope.DeleteNode);
                jsPlumb.on("#workflow-designer", "tap", ".node-edit, .node-edit i", $scope.EditNodeConfiguration);
                setTimeout($scope.PopulateDesignerData, 1000);
                $scope.EventTypeChanged(false);
            });
        }

        $scope.PopulateDesignerData = function () {
            if ($scope.Workflow.DesignerData) {
                var ddJson = JSON.parse($scope.Workflow.DesignerData);

                if (ddJson.nodes != null && ddJson.nodes.length > 0) {
                    for (var j = 0; j < ddJson.nodes.length; j++) {
                        var node = ddJson.nodes[j];
                        if (node.activityId != "ConditionActivity")
                            continue;
                    if ((node.RuleExpressions == null || node.RuleExpressions == undefined || node.RuleExpressions == '')
                            && node.Rules != null && node.Rules != undefined && node.Rules != ''
                            && node.Rules.Rules != null && node.Rules.Rules != undefined && node.Rules.Rules != ''
                        ) {
                        node.RuleExpressions =[];
                        for (var i = 0; i < node.Rules.Rules.length; i++) {
                            var rule = node.Rules.Rules[i];
                            var selectedExpressionGroup = $scope.FindMatchingExpressionByRule(rule);

                             if(selectedExpressionGroup != null)
                             {
                                 node.RuleExpressions.push({
                                         RuleName: rule.Name,
                                         Name: rule.LeftExpression,
                                         RuleOperator: rule.Operator,
                                         Value: rule.RightExpression,
                                         StringValue: rule.RightExpression,
                                         SelectedGroup: selectedExpressionGroup.RuleGroup,
                                         SelectedExpression: selectedExpressionGroup.Expression,
                                         Operators: selectedExpressionGroup.Expression.Operators,
                                         OperatorsArray: transformOperators(selectedExpressionGroup.Expression.Operators),
                                        });
                            }
                            else
                            {
                                var kk = 0;
                                kk = 1;
                            }

                        };
                        };
                        };
                        };

                $scope.Toolkit.load({
                    data: ddJson
            });
            }
        }

        $scope.DeleteNode = function () {
            var info = $scope.Renderer.getObjectInfo(this);
            jsPlumbToolkit.Dialogs.show({
                id: "dlgConfirm",
                data: {
                    msg: "Delete '" + info.obj.data.text + "'"
                },
                onOK: function () {
                    $scope.Toolkit.removeNode(info.obj);
                }
            });
        }

        $scope.EditNodeConfiguration = function () {
            var info = $scope.Renderer.getObjectInfo(this);
            var id = "#dlg" + info.obj.data.activityId;
            $(id).modal('show');
            $scope.NodeData = info.obj.data;
            $scope.Node = info.obj
            if ($scope.Node.data.activityId == "SendCommunicationActivity") {
                $scope.AddSearchChoice('#to-contact-types', $scope.NodeData.To);
                $('#to-contact-types').select2('val', $scope.NodeData.To);
                $scope.AddSearchChoice('#cc-contact-types', $scope.NodeData.CC);
                $('#cc-contact-types').select2('val', $scope.NodeData.CC);
            }
            if ($scope.Node.data.activityId == "AutoCaseAssignmentActivity") {
                $scope.AddCaseAssigneeTypeChoices('#case-assignment-type-codes');
                $('#case-assignment-type-codes').select2('val', $scope.NodeData.CaseAssigneeTypeCode);
            }
            /// Technically outside of AngualrJS right now, because we're calling from jsPlumb
            /// So we have to let Angular know we made changes
            $scope.$apply(); 
        }

        $scope.AddSearchChoice = function (selector, searchChoices) {
            var currentElementSearchChoices = $scope.ContactTypes;
            if (searchChoices) {
                for (var i = 0; i < searchChoices.length; i++) {
                    var choice = searchChoices[i];
                    if (choice.indexOf('@') > -1) {
                        currentElementSearchChoices.push({
                            id: choice,
                            text: choice
                        })
                    }
                }
            }
            
            $scope.BindSelect2(selector, currentElementSearchChoices);
        }

        $scope.AddCaseAssigneeTypeChoices = function (selector) {
            var currentElementSearchChoices = $scope.CaseAssigneeTypes;
            $scope.BindCaseAssigneeTypeSelect2(selector, currentElementSearchChoices);
        }

        $scope.SaveNodeConfiguration = function () {
            if ($scope.Node.data.activityId == "SendCommunicationActivity") {
                $scope.NodeData.To = $('#to-contact-types').select2('val');
                $scope.NodeData.CC = $('#cc-contact-types').select2('val');
            }
            if ($scope.Node.data.activityId == "AutoCaseAssignmentActivity") {
                $scope.NodeData.CaseAssigneeTypeCode = $('#case-assignment-type-codes').select2('val');
            }
            $scope.Toolkit.updateNode($scope.Node, $scope.NodeData);
            var id = "#dlg" + $scope.NodeData.activityId;
            $(id).modal('hide')
            $scope.Node = {};
            $scope.NodeData = {};
            $('#to-contact-types').select2('destroy');
            $('#cc-contact-types').select2('destroy');
        }

        $scope.InitCompanyInformation = function () {
            administrationService.GetBasicInfo().then(function (data) {
                $scope.BasicInfo = data;
            });
        }

        $scope.TransformRules = function () {
            var rules = {
                Rules: $scope.Workflow.Criteria,
                EventType: $scope.Workflow.TargetEventType,
                EmployerId: $scope.Workflow.EmployerId
            };
            workflowService.GetRuleExpressionsForRules(rules).$promise.then(function (expressions) {
                for (var i = 0; i < expressions.length; i++) {
                    var expression = expressions[i];
                    var selectedExpressionGroup = $scope.FindMatchingExpression(expression);
                    expression.SelectedGroup = selectedExpressionGroup.RuleGroup;
                    expression.SelectedExpression = selectedExpressionGroup.Expression;
                    for (var j = 0; j < expression.Parameters.length; j++) {
                        expression.Parameters[j].StringValue = expression.Parameters[j].Value;
                    }
                    $scope.CopyCurrentRuleExpressionInformation(expression, expression.RuleOperator, expression.Value);
                }
                $scope.RuleExpressions = expressions;
            });
        }

        $scope.FindMatchingExpression = function (savedExpression) {
            for (var i = 0; i < $scope.RuleGroupExpressions.length; i++) {
                var ruleGroup = $scope.RuleGroupExpressions[i];
                for (var j = 0; j < ruleGroup.Expressions.length; j++) {
                    var expression = ruleGroup.Expressions[j];
                    if (expression.Name == savedExpression.Name)
                        return {
                            Expression: expression,
                            RuleGroup: ruleGroup
                        };
                }
            }
        }

        $scope.FindMatchingExpressionByRule = function (rule) {
            for (var i = 0; i < $scope.RuleGroupExpressions.length; i++) {
                var ruleGroup = $scope.RuleGroupExpressions[i];
                for (var j = 0; j < ruleGroup.Expressions.length; j++) {
                    var expression = ruleGroup.Expressions[j];
                    var s = rule.LeftExpression.replace(/[\(\)]+$/, '');
                    if(expression.Name == s)
                        return {
                            Expression: expression,
                            RuleGroup: ruleGroup
                        };
                        }
                        }
            return null;
        }

        $scope.EventTypeChanged = function (reset) {
            if ($scope.Workflow.TargetEventType) {
                $scope.GetActivityCategories();
                if (reset) {
                    $scope.GetRuleExpressions();
                    $scope.Toolkit.clear();
                }
            }
        }

        $scope.GetActivityCategories = function () {
            var toolbarParameters = {
                EventType: $scope.Workflow.TargetEventType
            }
            workflowService.GetActivityCategories(toolbarParameters).$promise.then(function (toolbarCategories) {
                $scope.ToolbarCategories = toolbarCategories;
                $scope.FilterActivityTypes();
            })
        }

        $scope.FilterActivityTypes = function () {
            var filter = $scope.ActivityFilter.toLowerCase();
            for (var i = 0; i < $scope.ToolbarCategories.length; i++) {
                var toolbarCategory = $scope.ToolbarCategories[i];
                toolbarCategory.ShowCategory = false;
                for (var j = 0; j < toolbarCategory.Activities.length; j++) {
                    var activity = toolbarCategory.Activities[j];
                    var activityName = activity.Name.toLowerCase();
                    activity.DisplayInWorkflowToolbar = activityName.indexOf(filter) > -1;
                    if (activity.DisplayInWorkflowToolbar) {
                        toolbarCategory.ShowCategory = true;
                        $('#' + toolbarCategory.CategoryName + 'Activities').addClass('in');
                    }
                }

            }
        }

        $scope.GetRuleExpressions = function () {
            var expressionParameters = {
                EventType: $scope.Workflow.TargetEventType,
                EmployerId: $scope.Workflow.EmployerId
            }

            workflowService.GetRuleGroupExpressions(expressionParameters).$promise.then(function (ruleGroupExpressions) {
                for (var i = 0; i < ruleGroupExpressions.length; i++) {
                    for (var j = 0; j < ruleGroupExpressions[i].Expressions.length; j++) {
                        ruleGroupExpressions[i].Expressions[j].OperatorsArray = transformOperators(ruleGroupExpressions[i].Expressions[j].Operators);
                    }
                }

                $scope.RuleGroupExpressions = ruleGroupExpressions;
                $scope.TransformRules();
            });
        }

        $scope.GetActivities = function () {
            workflowService.GetActivities().$promise.then(function (activities) {
                $scope.Activities = activities;
            })
        }

        transformOperators = function (operatorsObject) {
            var operatorsArray = [];
            for (var property in operatorsObject) {
                if (operatorsObject.hasOwnProperty(property)) {
                    var operator = {
                        Id: property,
                        Text: operatorsObject[property]
                    };
                    operatorsArray.push(operator);
                }
            }
            return operatorsArray;
        }

        $scope.AddRule = function () {
            $scope.RuleExpressions.push($scope.CreateEmptyRule());
        }

        $scope.AddNodeRule = function () {
            if (!$scope.NodeData.RuleExpressions)
                $scope.NodeData.RuleExpressions = [];

            $scope.NodeData.RuleExpressions.push($scope.CreateEmptyRule());
        }

        $scope.CreateEmptyRule = function () {
            return {
                RuleName: '',
                Name: '',
                RuleOperator: '',
                Value: ''
            };

        }

        $scope.RemoveRule = function (rule) {
            $scope.RemoveRuleFromList($scope.RuleExpressions, rule);
        }

        $scope.RemoveNodeRule = function (rule) {
            $scope.RemoveRuleFromList($scope.NodeData.RuleExpressions, rule);
        }

        $scope.RemoveRuleFromList = function (ruleList, rule) {
            var idx = ruleList.indexOf(rule);
            if (idx > -1)
                ruleList.splice(idx, 1);
        }

        $scope.CopyCurrentRuleExpressionInformation = function (rule, operator, value) {
            rule.Name = rule.SelectedExpression.Name;
            rule.IsFunction = rule.SelectedExpression.IsFunction;
            rule.Operators = rule.SelectedExpression.Operators;
            rule.OperatorsArray = rule.SelectedExpression.OperatorsArray;
            var savedParams = rule.Parameters;
            rule.Parameters = rule.SelectedExpression.Parameters;
            for (var i = 0; i < rule.Parameters.length; i++) {
                rule.Parameters[i].StringValue = savedParams[i].Value;
            }
            rule.RuleOperator = operator;
            rule.StringValue = value;
        }

        $scope.ResetRuleSelections = function (rule) {
            rule.SelectedExpression = undefined;
            rule.Operators = undefined;
            rule.OperatorsArray = undefined;
            rule.RuleOperator = '';
            rule.Value = '';
            rule.Parameters = [];
        }

        $scope.SaveWorkflow = function () {
            $scope.submitted = true;
            if (!$scope.editWorkflow.$invalid) {
                var expressions = {
                    EventType: $scope.Workflow.TargetEventType,
                    RuleExpressions: $scope.GetRuleExpressionsForEvaluation()
                }

                var workflowData = $scope.Toolkit.exportData();
                var allPromises = [];
                var criteriaPromise = $scope.TransformCriteriaExpressions(workflowData);
                var workflowPromise = workflowService.GetRules(expressions).$promise;
                workflowPromise.then(function (rules) {
                    $scope.Workflow.Criteria = rules;
                });
                allPromises.push(criteriaPromise);
                allPromises.push(workflowPromise);
                $q.all(allPromises).then(function () {
                    $scope.Workflow.DesignerData = JSON.stringify(workflowData);
                    var transformedWorkflowData = $scope.TransformWorkflowData(workflowData);
                    $scope.Workflow.Activities = transformedWorkflowData.Activities;
                    $scope.Workflow.Transitions = transformedWorkflowData.Transitions;
                    workflowService.SaveWorkflow($scope.Workflow).$promise.then(function (workflow) {
                        $scope.Workflow = workflow;
                        $scope.TransformRules();
                        noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Workflow saved successfully" });
                    });
                })
            }
        }

        $scope.GetRuleExpressionsForEvaluation = function () {
            var expressionData = [];
            for (var i = 0; i < $scope.RuleExpressions.length; i++) {
                var currentExpression = $scope.RuleExpressions[i];
                expressionData.push({
                    RuleId: currentExpression.RuleId,
                    RuleName: currentExpression.RuleName,
                    RuleDescription: currentExpression.RuleDescription,
                    RuleOperator: currentExpression.RuleOperator,
                    Operators: currentExpression.Operators,
                    Name: currentExpression.Name,
                    IsFunction: currentExpression.IsFunction,
                    Parameters: currentExpression.Parameters,
                    StringValue: currentExpression.StringValue
                })
            }
            return expressionData;
        }

        $scope.CreateNewCode = function(){
            $scope.NewCode = $scope.Workflow.Code + 'Copy';
            $scope.NewName = $scope.Workflow.Name + ' Copy';
            $scope.NewDescription = $scope.Workflow.Description;
        }

        $scope.CopyWorkflow = function () {
            if ($scope.NewName && $scope.NewCode && $scope.NewCode != $scope.Workflow.Code) {
                var workflowCopy = {
                    Code: $scope.Workflow.Code,
                    CustomerId: $scope.Workflow.CustomerId,
                    EmployerId: $scope.Workflow.EmployerId,
                    NewCode: $scope.NewCode,
                    NewDescription: $scope.NewDescription,
                    NewName: $scope.NewName
                };
                workflowService.CopyWorkflow(workflowCopy).$promise.then(function (copiedWorkflow) {
                        $window.location.href = '/Workflow/' + $scope.CustomerId + '/Edit/' + copiedWorkflow.Id;
                });
            }
        }

        $scope.TransformCriteriaExpressions = function (workflowData) {
            var criteriaPromises = [];
            for (var i = 0; i < workflowData.nodes.length; i++) {
                var node = workflowData.nodes[i];
                if (node.RuleExpressions && node.Rules && node.Rules.SuccessType) {
                    var rulesPromise = $scope.GetConditionNodeRules(node);
                    criteriaPromises.push(rulesPromise);
                }
            }

            return $q.all(criteriaPromises);
        }

        $scope.GetConditionNodeRules = function (node) {
            var expressions = {
                EventType: $scope.Workflow.TargetEventType,
                RuleExpressions: node.RuleExpressions
            }
            var rulesPromise = workflowService.GetRules(expressions).$promise;
            rulesPromise.then(function (rules) {
                node.Rules.Rules = rules;
            });
            return rulesPromise;
        }

        $scope.TransformWorkflowData = function (workflowData) {
            var nontransferredProperties = ["activityId", "text", "id", "h", "left", "top", "type", "w"];
            var transformedData = {
                Activities: [],
                Transitions: [],
            };
            for (var i = 0; i < workflowData.nodes.length; i++) {
                var node = workflowData.nodes[i];
                var activity = {
                    ActivityId: node.activityId,
                    Name: node.text,
                    Id: node.id
                };
                for (var property in node) {
                    if (node.hasOwnProperty(property) && nontransferredProperties.indexOf(property) == -1) {
                        var value = $scope.FilterInt(node[property]);
                        if (isNaN(value)) {
                            activity[property] = node[property];
                        } else {
                            activity[property] = value;
                        }
                    }
                }
                transformedData.Activities.push(activity);
            }
            for (var i = 0; i < workflowData.edges.length; i++) {
                var edge = workflowData.edges[i];
                transformedData.Transitions.push({
                    SourceActivityId: edge.source,
                    TargetActivityId: edge.target,
                    ForOutcome: edge.data.outcome || "Complete"
                })
            }
            return transformedData;
        }

        /// A stricter parse function, so leading white space and strings that start with numbers don't get parsed as integers
        $scope.FilterInt = function (value) {
            if (/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
                return Number(value);
            return NaN;
        }

        $scope.ToggleWorkflowSuppression = function () {
            workflowService.ToggleWorkflowSuppression($scope.Workflow.Id).$promise.then(function (workflow) {
                $scope.Workflow.IsDisabled = workflow.IsDisabled;
            });
        }

        $scope.DeleteWorkflow = function () {
            workflowService.DeleteWorkflow($scope.Workflow.Id).$promise.then(function () {
                $window.location.href = '/Workflow/' + $scope.Workflow.CustomerId + '/List';
            })
        }

        $scope.ToggleDesignWorkflow = function () {
            $scope.DesignWorkflow = !$scope.DesignWorkflow;
            if ($scope.DesignWorkflow) {
                $scope.ToggleWorkflowText = "Workflow Settings";
                $scope.NodeConfiguration.droppables = jsPlumb.getSelector(".activity:not(.jtk-surface-droppable-node)");
                $scope.Renderer.registerDroppableNodes($scope.NodeConfiguration);

                /// Even though we've already set it to visible, by the time we call repaint, the designer might not be visible
                /// cause angular has it's scope/apply cycle, so let's set a delay
                setTimeout($scope.Renderer.repaintEverything, 100);
            }
            else {
                $scope.ToggleWorkflowText = "Design Workflow";
            }

        }


        $scope.Back = function () {
            $window.history.back();
        }

    }]);