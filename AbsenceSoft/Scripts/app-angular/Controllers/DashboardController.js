﻿//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('DashboardCtrl', ['$scope', 'caseService', 'lookupService', 'userPermissions', 'administrationService',
    function ($scope, caseService, lookupService, userPermissions, administrationService) {
    $scope.ListType = "Individual";

    $scope.IsTeamApp = false;
    $scope.IsEmployee = false;
    $scope.IsHRSupervisor = false;
    
    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }

    $scope.IsTeamApp = isInArray("TeamDashboard", userPermissions);
    if ($scope.IsTeamApp == false) {
        $scope.IsHRSupervisor = isInArray("MyEmployeesDashboard", userPermissions);
        if ($scope.IsHRSupervisor == false)
            $scope.IsEmployee = isInArray("EmployeeDashboard", userPermissions);        
    }



    // Get list of employer for current user
    lookupService.GetEmployersForUser().then(function (response) {
        $scope.EmployerList = response;
    });

    //Get Feature 'MultiEmployerAccess'
    lookupService.GetEmployerFeatures().then(function (response) {
        $scope.IsTPA = response.MultiEmployerAccess;
        $scope.isLOA = response.LOA;
    });


    $scope.init = function (tt, currentUserEmpId) {
        $scope.IsESS = tt
        $scope.CurrentUserEmpId = currentUserEmpId;
        
    };



}]);
//#endregion DashboardCtrl