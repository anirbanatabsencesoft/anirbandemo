//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
//#region CreateCaseController
angular
    .module('App.Controllers')
    .controller('ViewCaseCtrl', ['$scope', '$http', '$location', '$window', '$anchorScroll', 'toDoListManager', 'caseService', 'todoModalService', 'lookupService', 'toDoSuccessModalService', 'contactService', '$timeout', '$filter', 'administrationService', '$window', 'userPermissions', 'employeeService', 'EligibilityStatus', 'absenceReasonService', 'demandService', 'regionService',
        function ($scope, $http, $location, $window, $anchorScroll, toDoListManager, caseService, todoModalService, lookupService, toDoSuccessModalService, contactService, $timeout, $filter, administrationService, $window, userPermissions, employeeService, EligibilityStatus, absenceReasonService, demandService, regionService) {
            var vm = $scope;
            activate();

            function activate() {
                angular.extend(vm, {
                    CaseInfo: {
                        Case: {
                            CaseType: ""
                        }
                    },
                    SelectedAbsenceReason: {
                        Code: ""
                    }
                });
            }

            $scope.Regions = [];
            $scope.CreateCertifications = [];
            $scope.CreateCertifications.Certifications = [];
            $scope.EditCertifications = [];
            $scope.EditCertifications.Certifications = [];
            $scope.RegionName = "Region";
            $scope.CaseId = null;
            // the model we will use for most of the Create Case page
            $scope.PolicyData = null;
            // data for the override status dropdowns
            $scope.OverrideResultOptions = [{ OverrideResult: 1, Text: 'Pass' }, { OverrideResult: -1, Text: 'Fail' }, { OverrideResult: 0, Text: 'Unknown' }];
            $scope.CaseCalendar = null;
            $scope.CaseCalendarDateRange = null;
            $scope.CaseCalendarMonths = [];
            $scope.ChangeStatus = false;
            $scope.HasEmployeeWorkSchedule = true;
            $scope.AppliedPoliciesList = [];
            $scope.ChangeCase = new Object();
            $scope.ChangeCase.WorkSchedule = new Object();
            $scope.CancelledOrClosedCase = false;
            $scope.NoPolicies = false;
            $scope.HidePolicies = false;
            $scope.submitted = false;
            $scope.WarningYesNo = "No";
            $scope.ErrorYesNo = "No";
            $scope.InfoYesNo = "No";
            $scope.ViewNotes1 = false;
            $scope.reassigncases = { UserId: '', Cases: [] };
            $scope.reassigncaseassignee = { CaseAssigneeTypeCode: '', CaseAssigneeTypeName: '', CaseAssigneeUserId: '', Cases: [] };
            $scope.ShowTimeTrackerInHours = false;
            $scope.ShowPayButton = false;
            $scope.AccommodationRequest = null;
            $scope.HasAccommodationRequest = false;
            $scope.showstepnote = false;
            $scope.showAccomm = false;
            $scope.showAccomIntProcNotes = false;
            $scope.showAllStepNotes = false;

            $scope.IsSTD = false;
            $scope.EditAccommType = null;
            $scope.isShowParenthesisText = true;

            $scope.STD = {};
            $scope.OriginalSTD = {};
            $scope.STD.PrimaryDiagnosis = {};
            $scope.STD.SecondaryDiagnosis = [];
            $scope.STD.DuplicateDiagnosis = false;
            $scope.STD.NoPrimaryDiagnosis = false;
            $scope.ComorbidityArgsRequired = false;

            $scope.forms = {};
            $scope.forms.ChangeDiagnosis = false;
            $scope.forms.Guidelines = {};
            $scope.RTWBestPracticeList = [];
            $scope.forms.PrimaryPath = null;
            $scope.forms.WorkPathRequired = false;
            $scope.forms.ShowReturnToWork = false;
            $scope.forms.ActivityModifications = false;
            $scope.forms.PhisicalTherapy = false;
            $scope.forms.Chiropractical = false;

            $scope.ManualTimes = [];
            $scope.CurrentDisplayMonth = 4;
            $scope.AlreadyPushedMonths = [];
            $scope.showDelete = false;
            $scope.showManualDelete = false;
            $scope.showManalDelete = false;

            $scope.SetPolicyEndDate = {};
            $scope.SetPolicyEndDate.PolicyCode = '';
            $scope.SetPolicyEndDate.PolicyName = '';
            $scope.SetPolicyEndDate.OriginalEndDate = '';
            $scope.SetPolicyEndDate.EndDate = '';
            $scope.SetPolicyEndDate.Show = false;
            $scope.PrimaryDiagnosisCopy = [];
            $scope.SecondaryDiagnosisCopy = [];
            $scope.DiagnosisRtwCopy = [];
            $scope.ReOpenCase = new Object();
            $scope.ReOpenCase.WorkSchedule = new Object();



            $scope.showDeleteFn = function (num) {
                if (num) {
                    $scope.showDelete = num;
                }
                else {
                    $scope.showDelete = undefined;
                }
            }

            $scope.showManualDeleteFn = function (num) {
                if (num) {
                    $scope.showManualDelete = num;
                }
                else {
                    $scope.showManualDelete = undefined;
                }
            }

            // display calendar in Sun..Sat sequence
            $scope.dateOptions = {
                'starting-day': 0
            };

            $scope.Error = null;
            $scope.ErrorHandler = function (response) {
                $scope.Error = response;
            };

            $scope.CaseCancelReason = lookupService.CaseCancelReason();

            $scope.ContactTypes = lookupService.ContactTypes();

            administrationService.GetUsersList().then(function (data) {
                if (data != null) {
                    $scope.UsersList = data;
                }
            })

            //#region controller initialization logic
            $scope.init = function (model, IsEmployeeSelfService) {
                $scope.CaseId = model.Id;
                $scope.CaseStartDate = model.StartDate;
                $scope.CaseEndDate = model.EndDate;

                $scope.EmployeeId = model.EmployeeId;
                $scope.EmployerId = model.EmployerId;
                $scope.IsEmployeeSelfService = IsEmployeeSelfService == "True" ? true : false;
                $scope.SetPermission();

                //Employer feature level flags that affect the UI
                $scope.NoFeaturesEnabled = model.NoFeaturesEnabled;
                $scope.ADAFeatureEnabled = model.ADAFeatureEnabled;
                $scope.LOAFeatureEnabled = model.LOAFeatureEnabled;
                $scope.STDPayFeatureEnabled = model.STDPayFeatureEnabled;
                $scope.RiskProfileFeatureEnabled = model.RiskProfileFeatureEnabled;
                $scope.ShowIsWorkRelated = model.ShowIsWorkRelated;
                $scope.ShowContactSection = model.ShowContactSection;
                $scope.ShowAccomCategory = model.ShowAccomCategory;
                $scope.ShowAccomAdditionalInfo = model.ShowAccomAdditionalInfo;
                $scope.AccommodationTypeCategoriesFeatureEnabled = model.AccommodationTypeCategoriesFeatureEnabled;

                $scope.FTWeeklyWorkHours = model.FTWeeklyWorkHours;

                $scope.WorkRestrictions = model.WorkRestrictions;

                angular.forEach($scope.WorkRestrictions, function (wr, index) {
                    if (wr.StartDate != undefined &&
                        wr.StartDate != null &&
                        wr.StartDate != '') {
                        wr.StartDate = $filter('ISODateToDate')(wr.StartDate);
                    };

                    if (wr.EndDate != undefined &&
                        wr.EndDate != null &&
                        wr.EndDate != '') {
                        wr.EndDate = $filter('ISODateToDate')(wr.EndDate);
                    };

                });

                $scope.GetCaseInfo();
                $scope.JobClassifications = lookupService.JobClassifications();
                toDoListManager.InitToDosFilter($scope);
                $scope.ToDosFilter.CaseId = model.Id;
                lookupService.WorkflowItemStatuses().then(function (response) {
                    $scope.todoItemStatuses = response;
                    toDoListManager.List($scope, false, $scope.ErrorHandler);
                });
                $scope.GetTimeTrackingData(model.Id);
                $scope.CaseCalendarDateRange = caseService.GetDefaultRangeForCalendarForCase();
                $scope.GetCalendarData();
                $scope.InitChangeCaseObject();
                $scope.GetCertifications();
                $scope.GetAdministrativeContacts();
                $scope.InitReOpenCaseObject();
                $scope.$watch('RotationDays', function () {
                    if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
                        for (var i = 0; i < parseInt($scope.RotationDays); i++) {
                            $scope.Time = new Object();
                            $scope.Time.TotalMinutes = null;
                            $scope.Times.push($scope.Time);
                        }
                        $scope.rotationArray = new Array(parseInt($scope.RotationDays));
                    }
                    else {
                        $scope.rotationArray = null;
                    }
                });

                $scope.$watch('ToDosFilter.Due', function (newvalue) {
                    if (newvalue !== "") {
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                    }
                });

                $scope.$watch('IntermittentRequestViewModel', function () {
                    if ($scope.WarningYesNo == "Yes") $scope.WarningYesNo = "No";
                    if ($scope.ErrorYesNo == "Yes") $scope.ErrorYesNo = "No";
                    if ($scope.InfoYesNo == "Yes") $scope.InfoYesNo = "No";
                }, true);


                // Get list of employer for current user
                lookupService.GetEmployersForUser().then(function (response) {
                    $scope.EmployerList = response;
                });

                //Get Feature 'MultiEmployerAccess'
                lookupService.GetEmployerFeatures().then(function (response) {
                    $scope.IsTPA = response.MultiEmployerAccess;
                });

                $scope.GetAllEmployeeContacts();
                $scope.EmployeeRelationshipsIncludingSelf = lookupService.EmployeeRelationships(true);
                $scope.GetAllEmployerContacts();

                // Re-initialize the default assignee of To Do
                $scope.ManualTodo.AssignToId = '';

                if (model.MessageOnViewLoad) {
                    var n = noty({
                        timeout: 10000, layout: 'topRight', type: 'error', text: model.MessageOnViewLoad
                    });
                }
                $scope.refreshWorkRelatedInfo($scope.CaseId);
            };

            $scope.InitializeDefaultAssignee = function () {
                $.each($scope.UsersList,
                    function (key, data) {
                        if (data.IsLoggedInUser) {
                            // Initialize the default assignee of To Do to currently logged-in user
                            $scope.ManualTodo.AssignToId = data.Id;
                        }
                    });
            };

            $scope.SetPermission = function () {
                $scope.AdjudicateCase = false;
                $scope.ApproveITOR = false
                $scope.ViewNotes = false;
                $scope.CreateNotes = false;
                $scope.EditNotes = false;

                $scope.AdjudicateCase = userPermissions.indexOf("AdjudicateCase") > -1;
                $scope.ApproveITOR = userPermissions.indexOf("ApproveITOR") > -1;
                $scope.ViewNotes = userPermissions.indexOf("ViewNotes") > -1;
                $scope.CreateNotes = userPermissions.indexOf("CreateNotes") > -1;
                $scope.EditNotes = userPermissions.indexOf("EditNotes") > -1;
            };

            $scope.GetCaseTypeString = function (RelapseCaseTypeInt) {
                switch (RelapseCaseTypeInt) {
                    case 2: return 'Intermittent';
                        break;
                    case 1: return 'Consecutive';
                        break;
                    case 4: return 'Reduced';
                        break;
                    case 8: return 'Administrative';
                        break;
                }
            }
            $scope.reload = function () {
                $scope.AlreadyPushedMonths = [];
                toDoListManager.InitToDosFilter($scope);
                $scope.ToDosFilter.CaseId = $scope.CaseId;
                lookupService.WorkflowItemStatuses().then(function (response) {
                    $scope.todoItemStatuses = response;
                    toDoListManager.List($scope, false, $scope.ErrorHandler);
                });
                $scope.GetTimeTrackingData($scope.CaseId);
                $scope.CaseCalendarDateRange = caseService.GetDefaultRangeForCalendarForCase();
                $scope.GetCalendarData();
                $scope.GetCaseInfo();
                $scope.InitChangeCaseObject();
                $scope.GetCertifications();
                $scope.GetAllEmployeeContacts();
                $scope.GetAllEmployerContacts();
                $scope.ShowCPP = false;
                $scope.EditCPP = false;
                $scope.ShowProcessPath = false;
                $scope.EditProcessPath = false;

                $scope.$watch('RotationDays', function () {
                    if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
                        for (var i = 0; i < parseInt($scope.RotationDays); i++) {
                            $scope.Time = new Object();
                            $scope.Time.TotalMinutes = null;
                            $scope.Times.push($scope.Time);
                        }
                        $scope.rotationArray = new Array(parseInt($scope.RotationDays));
                    }
                    else {
                        $scope.rotationArray = null;
                    }
                });

                $scope.$watch('ToDosFilter.Due', function () {
                    toDoListManager.List($scope, false, $scope.ErrorHandler);
                });

                $scope.$watch('IntermittentRequestViewModel', function () {
                    if ($scope.WarningYesNo == "Yes") $scope.WarningYesNo = "No";
                    if ($scope.ErrorYesNo == "Yes") $scope.ErrorYesNo = "No";
                    if ($scope.InfoYesNo == "Yes") $scope.InfoYesNo = "No";
                }, true);
                $scope.refreshWorkRelatedInfo($scope.CaseId);
            };
            $scope.GetCustomFieldValue = function (customField) {
                if (customField.DataType == 4 && customField.SelectedValue != undefined && customField.SelectedValue != null) {
                    if (customField.SelectedValue.toString().trim() == '') return '';
                    return (customField.SelectedValue == true ? 'Yes' : 'No');
                }
                else {
                    return customField.SelectedValue;
                }
            };
            $scope.GetCaseInfo = function () {
                $scope.CaseInfo = null;

                caseService.GetCaseInfo($scope.CaseId)
                    .then(function (result) {
                        $scope.HasEmployeeWorkSchedule = result.HasEmployeeWorkSchedule;
                        if (result.Case != undefined && result.Case != null) {
                            if (result.Case.CustomFields.length > 0) {
                                angular.forEach(result.Case.CustomFields, function (value, key) {
                                    var date = Date.parse(value.SelectedValue);
                                    if (value.DataType == 8 && !isNaN(date) && date != "Invalid Date") {
                                        value.SelectedValue = $filter('date')($filter('ISODateToDate')(value.SelectedValue), 'MM/dd/yyyy');
                                    }
                                    //else if (value.DataType == 4 && value.SelectedValue != undefined && value.SelectedValue != null && value.SelectedValue.toString().trim() != '') {
                                    //    value.SelectedValue = (value.SelectedValue == true ? 'Yes' : 'No');
                                    //}
                                })
                            }

                            // apply date filter for Event Date
                            if (result.Case.CaseEvents != null && result.Case.CaseEvents.length > 0) {
                                angular.forEach(result.Case.CaseEvents, function (value, key) {
                                    var date = Date.parse(value.EventDate);
                                    if (!isNaN(date) && date != "Invalid Date") {
                                        value.EventDate = $filter('ISODateToDate')(value.EventDate);
                                    }
                                })
                            }
                        }

                        $scope.CaseInfo = result;
                        if ($scope.CaseInfo.Case.CaseReporter!=null) {
                            if ($scope.CaseInfo.Case.CaseReporter.Contact.WorkPhone)
                                $scope.CaseReporterPhone = $scope.CaseInfo.Case.CaseReporter.Contact.WorkPhone;
                            else if ($scope.CaseInfo.Case.CaseReporter.Contact.CellPhone) {
                                $scope.CaseReporterPhone = $scope.CaseInfo.Case.CaseReporter.Contact.CellPhone;
                            }
                            else if ($scope.CaseInfo.Case.CaseReporter.Contact.HomePhone) {
                                $scope.CaseReporterPhone = $scope.CaseInfo.Case.CaseReporter.Contact.HomePhone;
                            }
                            else {
                                $scope.CaseReporterPhone = "";
                            }
                        }
                        $scope.GetDenialReasons("Policy", $scope.CaseId);
                        $scope.GetPolicyData($scope.CaseId);
                        $scope.EmployeeId = $scope.CaseInfo.Case.Employee.Id;
                        $scope.IsSTD = $scope.CaseInfo.Case.IsSTD;
                        $scope.EditAccommodationType = true;
                        if ($scope.CaseHasReason(result.Case)) {
                            absenceReasonService.GetAbsenceReasonById(result.Case.Reason.Id, $scope.EmployeeId).then(function (response) {
                                $scope.EditAccommodationType = ((response.data == undefined || response.data == null || response.data.EditAccommodationType == undefined || response.data.EditAccommodationType == null) ? true : response.data.EditAccommodationType);
                            });
                        }
                        $scope.AccommodationTypes = lookupService.AccommodationTypes($scope.CaseInfo.Case.Employee.EmployerId, $scope.CaseInfo.Case.CaseType);
                        //$scope.AccommodationTypes = lookupService.AccommodationTypes($scope.CaseInfo.Case.Employee.EmployerId);
                        $scope.GetAccommodationRequests();
                        // Accommodation option show / hide
                        if ($scope.CaseInfo.Case.CaseType == "Administrative")
                            $scope.ShowThisOption = true;
                        else if (($scope.CaseInfo.Case.CaseType == "Consecutive" || $scope.CaseInfo.Case.CaseType == "Intermittent") && ($scope.CaseInfo.Case.Reason != null && $scope.CaseInfo.Case.Reason.Code != "ACCOMM")) {
                            $scope.ShowThisOption = true;
                        }
                        else {
                            $scope.ShowThisOption = false;
                        }

                        if ($scope.CaseInfo.Case.Employee.Info.Email != undefined && $scope.CaseInfo.Case.Employee.Info.Email != null && $scope.CaseInfo.Case.Employee.Info.Email != '') {
                            $scope.EmailPieces = ($scope.CaseInfo.Case.Employee.Info.Email).split('.');
                            $scope.DomainExt = $scope.EmailPieces[$scope.EmailPieces.length - 1];

                            $scope.EmployeeEmail = $filter('limitTo')($scope.CaseInfo.Case.Employee.Info.Email, 20 - $scope.DomainExt.length);
                            $scope.EmployeeEmail = $scope.EmployeeEmail + '....' + $scope.DomainExt;
                        }
                        else {
                            $scope.EmployeeEmail = '';
                        }
                        if ($scope.CaseInfo.Case.CaseType) {
                            $scope.ChangeCase.CaseTypeString = $scope.CaseInfo.Case.CaseType;
                            switch ($scope.ChangeCase.CaseTypeString) {
                                case 'Intermittent': $scope.ChangeCase.CaseType = 2;
                                    break;
                                case 'Consecutive': $scope.ChangeCase.CaseType = 1;
                                    break;
                                case 'Reduced': $scope.ChangeCase.CaseType = 4;
                                    break;
                                case 'Administrative': $scope.ChangeCase.CaseType = 8;
                                    break;
                            }
                            $scope.ChangeCase.CaseTypeCopy = angular.copy($scope.ChangeCase.CaseType);


                            $scope.ReOpenCase.CaseTypeString = $scope.CaseInfo.Case.CaseType;
                            switch ($scope.ReOpenCase.CaseTypeString) {
                                case 'Intermittent': $scope.ReOpenCase.CaseType = 2;
                                    break;
                                case 'Consecutive': $scope.ReOpenCase.CaseType = 1;
                                    break;
                                case 'Reduced': $scope.ReOpenCase.CaseType = 4;
                                    break;
                                case 'Administrative': $scope.ReOpenCase.CaseType = 8;
                                    break;
                            }

                            $scope.ReOpenCase.CaseTypeCopy = angular.copy($scope.ReOpenCase.CaseType);
                        }
                        if ($scope.CaseInfo.Case.Employee.WorkSchedule) {
                            $scope.ChangeCase.WorkSchedule.ScheduleType = $scope.CaseInfo.Case.Employee.WorkSchedule.ScheduleType;
                            $scope.WorkScheduleStartDate = $scope.CaseInfo.Case.Employee.WorkSchedule.StartDate;
                            $scope.WorkScheduleEndDate = $scope.CaseInfo.Case.Employee.WorkSchedule.EndDate;
                            $scope.Times = $scope.CaseInfo.Case.Employee.WorkSchedule.Times;
                            if ($scope.CaseInfo.Case.Employee.WorkSchedule.ScheduleType == 0) {
                                $scope.ScheduleWeekly = true;
                                $scope.ScheduleStartEnd = false;
                                $scope.UpdateScheduleType(0);
                            }
                            if ($scope.CaseInfo.Case.Employee.WorkSchedule.ScheduleType == 1) {
                                $scope.ScheduleRotating = true;
                                $scope.ScheduleStartEnd = true;
                                $scope.UpdateScheduleType(1);
                            }
                            if ($scope.CaseInfo.Case.Employee.WorkSchedule.ScheduleType == 2) {
                                $scope.ScheduleRotatingPerDay = true;
                                $scope.ScheduleStartEnd = true;
                                $scope.UpdateScheduleType(2);
                            }
                            if ($scope.CaseInfo.Case.Employee.WorkSchedule.ScheduleType == 3) {
                                $scope.ScheduleRotatingPerDay = true;
                                $scope.ScheduleStartEnd = true;
                                $scope.UpdateScheduleType(3);
                            }
                        }

                        if ($scope.CaseInfo.Case.Employee.FullName != undefined && $scope.CaseInfo.Case.Employee.FullName != null && $scope.CaseInfo.Case.Employee.FullName != '') {
                            $scope.EmployeeFullName = $filter('limitTo')($scope.CaseInfo.Case.Employee.FullName, 25);
                            $scope.EmployeeFullName = $scope.EmployeeFullName + "...";
                        }
                        else {
                            $scope.EmployeeFullName = '';
                        }


                        //Check to see if case is cancelled or closed
                        if ($scope.CaseInfo.Case.Status != 0 && $scope.CaseInfo.Case.Status != 3) {
                            //Hide buttons and other stuff that sends data to backend
                            $scope.CancelledOrClosedCase = true;
                        };

                        $scope.ShowDOB = false;
                        $scope.DoBEyeClick = function () {
                            $scope.ShowDOB = !$scope.ShowDOB;
                        };
                        $scope.ShowSalary = false;
                        $scope.SalaryEyeClick = function () {
                            $scope.ShowSalary = !$scope.ShowSalary;
                        };
                        $scope.ShowSSN = false;
                        $scope.SSNEyeClick = function () {
                            $scope.ShowSSN = !$scope.ShowSSN;
                        };
                        if ($scope.CaseInfo.Case.Disability != undefined && $scope.CaseInfo.Case.Disability != null && $scope.CaseInfo.Case.Disability != '') {
                            $scope.STD = $scope.CaseInfo.Case.Disability;
                            $scope.OriginalSTD = $scope.CaseInfo.Case.Disability;
                            ///This is to bind dropdown with HcpContactName and New Provider Option

                            //$scope.SelectedContactId = $scope.HCPContactId;

                            if ($scope.STD.HCPPhone != null && $scope.STD.HCPPhone != undefined && $scope.STD.HCPPhone != '')
                                $scope.STD.HCPPhone = $filter('tel')($scope.STD.HCPPhone);

                            if ($scope.STD.HCPFax != null && $scope.STD.HCPFax != undefined && $scope.STD.HCPFax != '')
                                $scope.STD.HCPFax = $filter('tel')($scope.STD.HCPFax);

                            // Set diagnosis code
                            if ($scope.STD.PrimaryDiagnosis != null) {
                                //$scope.forms.ViewGuidelines = true;

                                caseService.GuidelinesData($scope.STD.PrimaryDiagnosis.Description.split('-')[0].trim(), $scope.CaseInfo.Case.Employee.EmployerId).then(function (response) {
                                    // Get Return-To-Work Path
                                    $scope.RTWBestPracticeList = (response.Guidelines || {}).RtwBestPractices || [];
                                    $scope.forms.WorkPathRequired = true;

                                    // Get already selected Return-To-Work path and set in dropdown
                                    var primaryPath = $filter('filter')($scope.RTWBestPracticeList, {
                                        Id: $scope.STD.PrimaryPathId
                                    });
                                    if (primaryPath.length > 0) {
                                        $scope.forms.PrimaryPath = primaryPath[0];
                                    }

                                });
                            }

                            if ($scope.STD.SecondaryDiagnosis == null || $scope.STD.SecondaryDiagnosis == undefined) {
                                $scope.STD.SecondaryDiagnosis = [];
                            }

                            $scope.InitDiagnosisSearch();

                        }
                    },
                        $scope.ErrorHandler);
            };

            // Validates that we have a reason and that we should get the updated absence reason info
            $scope.CaseHasReason = function (theCase) {
                // If for some reason no case, of course there won't be a reason
                if (!theCase)
                    return false;

                // If it's in inquiry status, it won't have a reason
                if (theCase.Status == 4)
                    return false;

                // If the reason isn't populated, duh
                if (!theCase.Reason)
                    return false;

                // If the reason is populated, but doesn't have an id, we don't want to throw any errors
                if (!theCase.Reason.Id)
                    return false;

                return true;
            };

            $scope.initOverrideResultOption = function (rule) {
                if (rule.Overridden) {
                    if (rule.OverrideResult == -1) {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[1];
                    }
                    else if (rule.OverrideResult == 1) {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[0];
                    }
                    else {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[2];
                    }
                }
                else {
                    if (rule.Result == -1) {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[1];
                    }
                    else if (rule.Result == 1) {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[0];
                    }
                    else {
                        rule.OverrideResultOption = $scope.OverrideResultOptions[2];
                    }
                }
            };
            $scope.onOverrideResultOptionChanged = function (rule, policy) {
                // make sure we move the result over to the property ASP.Net will use
                rule.OverrideResult = rule.OverrideResultOption.OverrideResult;

                // recalculate eligibility
                $scope.SavePolicyDataEdits();
            };
            $scope.GetCalendarData = function () {
                caseService.GetCalendarForCase($scope.CaseId, $scope.CaseCalendarDateRange)
                    .then(function (result) {
                        $scope.CaseCalendar = result;
                        // we do this AFTER getting the CaseCalendar because CaseCalendar is needed
                        // during the process of binding bootstrap's datepicker to CaseCalendarMonths            
                        $scope.CaseCalendarMonths = caseService.GetMonthsForCalendar($scope.CaseCalendarDateRange);
                    },
                        $scope.ErrorHandler);
            };

            $scope.SaveTimeLostForDay = function (date, time, caseId) {

                $http({ method: 'POST', url: '/Cases/' + caseId + '/SaveMissedTime', params: { caseId: caseId, time: time, date: new Date(date).toDateString() } }).
                    success(function (response) {
                        $scope.RefreshDetails();
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Time saved successfully" });
                    }).error(function (response) {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Error in saving missed time. Date: ' + date + ', Time: ' + time });
                    });
            };

            $scope.SavePolicyDataEdits = function () {
                return caseService.SavePolicyDataEdits($scope.CaseId, $scope.PolicyData)
                    .then(function (result) {
                        $scope.PolicyData = result;
                        // refresh policy related sections
                        $scope.RefreshDetails();
                    },
                        $scope.ErrorHandler);
            };

            $scope.AddManualPolicy = function (manualPolicyToAdd) {
                if (manualPolicyToAdd.PolicyCode != null) {
                    caseService.AddManualPolicy($scope.CaseId, manualPolicyToAdd.PolicyCode).then(function (result) {
                        if (result.success) {
                            $scope.RefreshDetails();
                            $scope.GetCaseInfo();
                            $scope.GetToDos(false);
                        }
                    })
                }
            };


            $scope.RemoveManualPolicy = function (manualPolicyToRemove) {
                if (manualPolicyToRemove.PolicyCode != null) {
                    var index = $scope.PolicyData.ManuallyAppliedPolicies.indexOf(manualPolicyToRemove);
                    $scope.PolicyData.ManuallyAppliedPolicies.splice(index, 1);
                    // recalculate eligibility
                    // $scope.SavePolicyDataEdits();
                    caseService.DeletePolicy($scope.CaseId, { Code: manualPolicyToRemove.PolicyCode }).then(function (result) {
                        $scope.GetPolicyData($scope.CaseId);
                        // Refresh the TimeTracker data
                        $scope.GetTimeTrackingData($scope.CaseId);
                    },
                        $scope.ErrorHandler);
                }
            };
            $scope.OverrideRule = function (rule) {
                rule.Overridden = true;
            };
            $scope.RevertRule = function (rule) {
                rule.Overridden = false;
                // recalculate eligibility
                $scope.SavePolicyDataEdits();
            };
            $scope.GetCommunicationsModalData = function () {
                $http({
                    method: 'POST',
                    url: '/Cases/' + $scope.CaseId + '/CommunicationsModalData/'//,
                    //data: $scope.CaseId
                }).then(function (response) {
                    // get the data back
                    $scope.CreatedLeaveOfAbsence = response.data;

                });
            };
            $scope.RefreshDetails = function () {
                // get denial reason data
                $scope.GetDenialReasons("Policy", $scope.CaseId);
                // refresh policy data, particular policy select for adjudications
                $scope.GetPolicyData($scope.CaseId);
                // refresh the calendar data
                $scope.GetCalendarData();
                // refresh the time tracking info
                $scope.GetTimeTrackingData();
                // Refresh certification
                $scope.GetCertifications($scope.CaseId);
                // Refresh accommodations
                $scope.GetAccommodationRequests();
                // Update short term disablility if applicable
                if ($scope.IsSTD) {
                    $scope.GetSTDInfo();
                    $scope.GetSTDPolicyData();
                }
                $scope.refreshWorkRelatedInfo($scope.CaseId);
            }

            //#region todo logic

            $scope.GetToDos = function (doPaging) {
                toDoListManager.List($scope, doPaging, $scope.ErrorHandler);
            };
            $scope.UpdateToDoSort = function (sortBy) {
                toDoListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
            };
            $scope.LoadMoreToDos = function () {
                toDoListManager.LoadMore($scope, $scope.ErrorHandler);
            };

            //#region Intermittent Request

            $scope.EndTimeChangeEvent = function (eventObj, indexVal) {
                $scope.IntermittentRequestViewModelList[indexVal].EndTimeOfEvent.Hour = eventObj.time.hours;
                $scope.IntermittentRequestViewModelList[indexVal].EndTimeOfEvent.Minutes = eventObj.time.minutes;
                $scope.IntermittentRequestViewModelList[indexVal].EndTimeOfEvent.PM = (eventObj.time.meridian == 'PM');
                $scope.calculatePendingValue($scope.IntermittentRequestViewModelList[indexVal].StartTimeOfEvent, $scope.IntermittentRequestViewModelList[indexVal].EndTimeOfEvent, indexVal);
                $('#IntermittentRequestViewModelPendingTime_' + indexVal).trigger("change");
                $('#IntermittentRequestViewModelDeniedTimeIsDenial_' + indexVal + '_0').trigger("change");
            }
            $scope.StartTimeChangeEvent = function (eventObj, indexVal) {
                $scope.IntermittentRequestViewModelList[indexVal].StartTimeOfEvent.Hour = eventObj.time.hours;
                $scope.IntermittentRequestViewModelList[indexVal].StartTimeOfEvent.Minutes = eventObj.time.minutes;
                $scope.IntermittentRequestViewModelList[indexVal].StartTimeOfEvent.PM = (eventObj.time.meridian == 'PM');
                $scope.calculatePendingValue($scope.IntermittentRequestViewModelList[indexVal].StartTimeOfEvent, $scope.IntermittentRequestViewModelList[indexVal].EndTimeOfEvent, indexVal);
                $('#IntermittentRequestViewModelPendingTime_' + indexVal).trigger("change");
                $('#IntermittentRequestViewModelDeniedTimeIsDenial_' + indexVal + '_0').trigger("change");
            }

            //Initialize Intermittent Request View Model
            $scope.initIntermittentRequest = function () {
                $scope.IntermittentRequestViewModelList = [{
                    Id: null,
                    IntermittentType: 0,
                    CaseId: $scope.CaseId,
                    RequestDate: '',
                    PendingTime: '',
                    ApprovedTime: '',
                    IsApprovedDisabled: false,
                    DeniedTime: '',
                    DenialReason: '',
                    DenialReasonOther: '',
                    Notes: '',
                    Details: [],
                    StartTimeOfEvent: {},
                    EndTimeOfEvent: {}
                }];

                $scope.SplitApprovalDecisionsToggle = [];
                $scope.IsEdit = false;
                $scope.submitted = false;
                $scope.SaveInProgress = false;

                //Create view model
                $scope.CreateSplitIntermittentViewModel();

                //When true this shows each policy in Intermittent Request for split approval decisions
                $scope.SplitApprovalDecisionsToggle.push(false);
                //Don't hide the group time inputs and Split button
                $scope.HideGroupTimeInputs = false;

                $timeout(function () {
                    $('#start-time-picker_0').timepicker({
                        showMeridian: true,
                        defaultTime: 'current',
                        showInputs: false
                    }).on('changeTime.timepicker', function (ev) {
                        $scope.StartTimeChangeEvent(ev, 0);
                    });

                    $('#end-time-picker_0').timepicker({
                        showMeridian: true,
                        defaultTime: 'current',
                        showInputs: false
                    }).on('changeTime.timepicker', function (ev) {
                        $scope.EndTimeChangeEvent(ev, 0);
                    });
                    $('#start-time-picker_0').timepicker('setTime', '');
                    $('#end-time-picker_0').timepicker('setTime', '');
                }, 10);

                $("#intermittentRequest").modal('show');
                $('#ddIntermittent_0').val($scope.IntermittentRequestViewModelList[0].IntermittentType);
                $scope.frmIntermittent.$setPristine();
            }

            //Update view model with split Intermittent policies
            $scope.CreateSplitIntermittentViewModel = function () {
                if ($scope.IntermittentRequestViewModelList != undefined && $scope.IntermittentRequestViewModelList.length > 0) {
                    for (var i = 0; i < $scope.IntermittentRequestViewModelList.length; i++) {
                        $scope.IntermittentRequestViewModelList[i].Details = [];

                        angular.forEach($scope.TimeTrackingInfo, function (policy, index) {
                            var policyDetail = new Object();
                            policyDetail.PolicyCode = policy.PolicyCode;
                            policyDetail.PolicyName = policy.PolicyName;
                            policyDetail.Available = policy.TimeRemainingText;
                            policyDetail.PendingTime = $scope.IntermittentRequestViewModelList[i].PendingTime;
                            policyDetail.ApprovedTime = $scope.IntermittentRequestViewModelList[i].ApprovedTime;
                            policyDetail.DeniedTime = $scope.IntermittentRequestViewModelList[i].DeniedTime;
                            policyDetail.DenialReasonCode = $scope.IntermittentRequestViewModelList[i].DenialReasonCode;
                            policyDetail.DenialReasonOther = $scope.IntermittentRequestViewModelList[i].DenialReasonOther;
                            policyDetail.IsApprovedDisabled = false;
                            $scope.IntermittentRequestViewModelList[i].Details.push(policyDetail);
                        });
                    }
                }
            }

            //Ran when "Split Approval Decisions" button is clicked
            $scope.SplitApprovalDecisions = function (itorIndex) {
                ////Disable the "Split Approval Decisions" button
                //$scope.SplitDisabled = true;

                //Clear any previously existing Details since new ones added in CreateSplitIntermittentViewModel method
                $scope.IntermittentRequestViewModelList[itorIndex].Details = [];
                //Create view model
                $scope.CreateSplitIntermittentViewModel();
                //If this is true, user wants to split approval decisions; This shows each policy in the UI

                // If create assign value in split portion
                if ($scope.IsEdit == false) {
                    angular.forEach($scope.IntermittentRequestViewModelList[itorIndex].Details, function (detail, index) {
                        if (detail.IsApprovedDisabled == false) {
                            detail.ApprovedTime = $scope.IntermittentRequestViewModelList[itorIndex].ApprovedTime;
                        }
                        detail.PendingTime = $scope.IntermittentRequestViewModelList[itorIndex].PendingTime;
                        detail.DeniedTime = $scope.IntermittentRequestViewModelList[itorIndex].DeniedTime;
                        detail.DenialReasonCode = $scope.IntermittentRequestViewModelList[itorIndex].DenialReasonCode;
                        detail.DenialReasonOther = $scope.IntermittentRequestViewModelList[itorIndex].DenialReasonOther;
                    });
                }


                if ($scope.SplitApprovalDecisionsToggle[itorIndex]) {
                    $scope.SplitApprovalDecisionsToggle[itorIndex] = false;
                } else {
                    $scope.SplitApprovalDecisionsToggle[itorIndex] = true;
                }

            }

            $scope.IntermittentValidation = function (itorIndex) {
                var hoursCheck = $('#IntermittentRequestViewModelPendingTime_' + itorIndex).val() != "" ? $('#IntermittentRequestViewModelPendingTime_' + itorIndex).val() : $('#IntermittentRequestViewModelDeniedTime_' + itorIndex).val() != "" ? $('#IntermittentRequestViewModelDeniedTime_' + itorIndex).val() : $('#IntermittentRequestViewModelApprovedTime_' + itorIndex).val() != "" ? $('#IntermittentRequestViewModelApprovedTime_' + itorIndex).val() : 0;
                if (!(hoursCheck == 0 || hoursCheck == "")) {
                    if ($('#start-time-picker_' + itorIndex).val() == "" || $('#end-time-picker_' + itorIndex).val() == "") {
                        $scope.IntermittentRequestViewModelList[itorIndex].StartTimeOfEvent = { Hour: "HH", Minutes: "MM" };
                        $scope.IntermittentRequestViewModelList[itorIndex].EndTimeOfEvent = { Hour: "HH", Minutes: "MM" };
                    }
                }

                return true;
            }

            // Pre-validate time off request
            $scope.PreValidateTimeOffRequest = function (isValid, itorIndex, event) {
                if (!isValid)
                    return;
                $scope.SaveInProgress = true;
                //If "Split Approval Decisions" button is NOT clicked
                if ($scope.SplitApprovalDecisionsToggle[itorIndex] == false) {
                    //Create view model now since "Split Approval Decision" button was not clicked
                    $scope.CreateSplitIntermittentViewModel();
                };
                if ($scope.IntermittentRequestViewModelList != undefined) {
                    $scope.IntermittentRequestViewModelToSave = null;
                    if (itorIndex < $scope.IntermittentRequestViewModelList.length) {
                        $scope.IntermittentRequestViewModelToSave = $scope.IntermittentRequestViewModelList[itorIndex];
                        $scope.IntermittentRequestViewModelToSave.RequestDate = new Date($scope.IntermittentRequestViewModelToSave.RequestDate).toDateString();
                        $scope.SplitApprovalDecisionsViewToggle = $scope.SplitApprovalDecisionsToggle[itorIndex];
                    }
                    if ($scope.IntermittentRequestViewModelToSave != null) {
                        $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Hour = $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Hour == "HH" ? "0" : $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Hour;
                        $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Hour = $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Hour == "HH" ? "0" : $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Hour;
                        $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Minutes = $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Minutes == "MM" ? "0" : $scope.IntermittentRequestViewModelToSave.StartTimeOfEvent.Minutes;
                        $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Minutes = $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Minutes == "MM" ? "0" : $scope.IntermittentRequestViewModelToSave.EndTimeOfEvent.Minutes;
                        var Intertype = $('#ddIntermittent_' + itorIndex).find(":selected").val();
                        if (Intertype != undefined) {
                            $scope.IntermittentRequestViewModelToSave.IntermittentType = Intertype;
                        }
                        caseService.PreValidateTimeOffRequest($scope.IntermittentRequestViewModelToSave).then(function (result) {
                            event.preventDefault();
                            if (result.HasWarnings) {
                                // Filter by each possible validation type
                                var filteredErrorMsg = $filter('filter')(result.Messages, {
                                    ValidationType: 3
                                });
                                var filteredWarningMsg = $filter('filter')(result.Messages, {
                                    ValidationType: 2
                                });
                                var filteredInfoMsg = $filter('filter')(result.Messages, {
                                    ValidationType: 1
                                });
                                var strErrorList = '', strWarningList = '', strInfoList = '';
                                $scope.WarningYesNo = "No";
                                $scope.ErrorYesNo = "No";
                                if (filteredErrorMsg.length > 0) {
                                    strErrorList = '<div style = "z-index: 1000;"><h4 class="noty-confirm-message"> Error : </h4><hr/><ul class="noty-message">';
                                    angular.forEach(filteredErrorMsg, function (err, index) {
                                        strErrorList = strErrorList + '<li class="noty-bulleted-content">' + err.Message + '</li>'

                                        if ((filteredErrorMsg.length - 1) == index) {
                                            strErrorList = strErrorList + '</ul></div><div class="noty-confirm-message">Are you sure you want to proceed further with error(s)?<div>';
                                        }
                                    });
                                }

                                if (filteredWarningMsg.length > 0) {
                                    strWarningList = '<div style = "z-index: 1000;"><h4 class="noty-confirm-message"> Warning : </h4><hr/><ul class="noty-message">';
                                    angular.forEach(filteredWarningMsg, function (err, index) {

                                        strWarningList = strWarningList + '<li class="noty-bulleted-content">' + err.Message + '</li>'

                                        if ((filteredWarningMsg.length - 1) == index) {
                                            strWarningList = strWarningList + '</ul></div>';
                                        }
                                    });
                                }

                                if (filteredInfoMsg.length > 0) {
                                    strInfoList = '<div style = "z-index: 1000;"><h4 class="noty-confirm-message"> Info : </h4><hr/><ul class="noty-message">';
                                    angular.forEach(filteredInfoMsg, function (err, index) {
                                        strInfoList = strInfoList + '<li class="noty-bulleted-content">' + err.Message + '</li>'

                                        if ((filteredInfoMsg.length - 1) == index) {
                                            strInfoList = strInfoList + '</ul></div>';
                                        }
                                    });
                                }

                                $timeout(function () {


                                    if (strErrorList != '' && $scope.ErrorYesNo == "No") {
                                        var n = noty({
                                            timeout: false,
                                            layout: 'customCenterFixedWidth',
                                            modal: true,
                                            type: 'error',
                                            template: strErrorList,
                                            animation: {
                                                open: {
                                                    height: 'toggle'
                                                },
                                                close: {
                                                    height: 'toggle'
                                                },
                                                easing: 'swing',
                                                speed: 500
                                            },
                                            buttons: [
                                                {
                                                    addClass: 'btn btn-primary', text: 'YES, PROCEED', onClick: function ($noty) {
                                                        $noty.close();
                                                        $scope.ErrorYesNo = "Yes";

                                                        if ($scope.ErrorYesNo == "Yes" && $scope.WarningYesNo == "Yes" && $scope.InfoYesNo == "Yes") {
                                                            $scope.CreateOrModifyTimeOffRequest($scope.frmIntermittent.$valid);
                                                        }
                                                        $scope.SaveInProgress = false;
                                                    }
                                                },
                                                {
                                                    addClass: 'btn btn-default', text: 'NO, CANCEL', onClick: function ($noty) {
                                                        $noty.close();
                                                        $scope.ErrorYesNo = "No";
                                                        $scope.SaveInProgress = false;
                                                    }
                                                }
                                            ]
                                        });
                                    }
                                    else {
                                        $scope.ErrorYesNo = "Yes";
                                        $scope.SaveInProgress = false;
                                    }


                                    if (strWarningList != '' && $scope.WarningYesNo == "No") {
                                        var n = noty({
                                            timeout: false,
                                            layout: 'customCenterFixedWidth',
                                            modal: true,
                                            type: 'warning',
                                            template: strWarningList,
                                            animation: {
                                                open: {
                                                    height: 'toggle'
                                                },
                                                close: {
                                                    height: 'toggle'
                                                },
                                                easing: 'swing',
                                                speed: 500
                                            },
                                            buttons: [
                                                {
                                                    addClass: 'btn btn-primary', text: 'YES, PROCEED', onClick: function ($noty) {

                                                        $noty.close();
                                                        $scope.WarningYesNo = "Yes";

                                                        if ($scope.ErrorYesNo == "Yes" && $scope.WarningYesNo == "Yes" && $scope.InfoYesNo == "Yes") {
                                                            $scope.CreateOrModifyTimeOffRequest($scope.frmIntermittent.$valid);
                                                        }
                                                        $scope.SaveInProgress = false;
                                                    }
                                                },
                                                {
                                                    addClass: 'btn btn-default', text: 'NO, CANCEL', onClick: function ($noty) {
                                                        $noty.close();
                                                        $scope.WarningYesNo = "No";
                                                        $scope.SaveInProgress = false;
                                                    }
                                                }
                                            ]
                                        });
                                    } else {
                                        $scope.WarningYesNo = "Yes";
                                        $scope.SaveInProgress = false;
                                    }

                                    if (strInfoList != '') {
                                        $scope.InfoYesNo = "No";
                                        var n = noty({
                                            timeout: false,
                                            layout: 'customCenterFixedWidth',
                                            modal: true,
                                            type: 'information',
                                            template: strInfoList,
                                            animation: {
                                                open: {
                                                    height: 'toggle'
                                                },
                                                close: {
                                                    height: 'toggle'
                                                },
                                                easing: 'swing',
                                                speed: 500
                                            },
                                            buttons: [
                                                {
                                                    addClass: 'btn btn-default', text: 'Close', onClick: function ($noty) {
                                                        $noty.close();
                                                        $scope.InfoYesNo = "Yes";

                                                        if ($scope.ErrorYesNo == "Yes" && $scope.WarningYesNo == "Yes" && $scope.InfoYesNo == "Yes") {
                                                            $scope.CreateOrModifyTimeOffRequest($scope.frmIntermittent.$valid);
                                                        }
                                                        $scope.InfoYesNo = "";
                                                        $scope.SaveInProgress = false;
                                                    }
                                                }
                                            ]
                                        });
                                    }
                                    else {
                                        $scope.InfoYesNo = "Yes";
                                        $scope.SaveInProgress = false;
                                    }

                                }, 1000)
                            }
                            else {
                                $scope.CreateOrModifyTimeOffRequest($scope.frmIntermittent.$valid);
                            }
                        });
                    }
                }
            }

            //POST final Intermittent Request view model
            $scope.CreateOrModifyTimeOffRequest = function (isValid) {
                if (!isValid)
                    return;

                //If "Split Approval Decisions" button is NOT clicked
                if ($scope.SplitApprovalDecisionsViewToggle == false) {
                    //Create view model now since "Split Approval Decision" button was not clicked
                    $scope.CreateSplitIntermittentViewModel();
                };

                $http({ method: 'POST', url: '/Cases/CreateOrModifyTimeOffRequest', data: $scope.IntermittentRequestViewModelToSave }).then(function (response) {
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        $scope.IntermittentRequestViewModelToSave.Id = response.data.Id;
                        $scope.submitted = false;
                        $scope.frmIntermittent.$setPristine();
                        $scope.IntermittentRequestViewModelToSave.Id = response.data.Id;
                        // refresh policy related sections
                        $scope.RefreshDetails();;
                        $scope.SaveInProgress = false;
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                    }
                }, function (response) {
                    $scope.WarningYesNo = "No";
                    $scope.ErrorYesNo = "No";
                    $scope.InfoYesNo = "No";
                    $scope.SaveInProgress = false;
                });;

                ////Re-enable the "Split Approval Decisions" button
                //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
                $scope.SplitApprovalDecisionsViewToggle = false;
            };

            //POST final Intermittent Request view model
            $scope.DeleteTimeOffRequest = function (isValid) {
                if (!isValid)
                    return;
                $scope.SaveInProgress = true;
                //If "Split Approval Decisions" button is NOT clicked
                if ($scope.SplitApprovalDecisionsViewToggle == false) {
                    //Create view model now since "Split Approval Decision" button was not clicked
                    $scope.CreateSplitIntermittentViewModel();
                };
                $scope.IntermittentRequestViewModelList[0].RequestDate = new Date($scope.IntermittentRequestViewModelList[0].RequestDate).toDateString();
                $http({ method: 'POST', url: '/Cases/DeleteTimeOffRequest', data: $scope.IntermittentRequestViewModelList[0] }).then(function (response, status) {
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        $scope.submitted = false;
                        $scope.frmIntermittent.$setPristine();

                        // refresh policy related sections
                        $scope.RefreshDetails();
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                        $scope.IntermittentRequestViewModelList = [];
                        $scope.SaveInProgress = false;
                        $("#intermittentRequest").modal('hide');
                        if ($("#deleteAllITOR").length > 0) {
                            $("#deleteAllITOR").modal('hide');
                        }
                    }
                }).error(function (response) {
                    $scope.WarningYesNo = "No";
                    $scope.ErrorYesNo = "No";
                    $scope.InfoYesNo = "No";
                    $scope.SaveInProgress = false;
                });;

                ////Re-enable the "Split Approval Decisions" button
                //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
                $scope.SplitApprovalDecisionsViewToggle = false;

                $('#deleteITOR').on('hidden.bs.modal', function () {
                    $(this).hide();
                })
                  
            };

            //POST final Intermittent Request view model
            $scope.RemoveTimeOffRequest = function (isValid, itorIndex) {
                if (!isValid)
                    return;
                $scope.SaveInProgress = true;
                //If "Split Approval Decisions" button is NOT clicked
                if ($scope.SplitApprovalDecisionsToggle[itorIndex] == false) {
                    //Create view model now since "Split Approval Decision" button was not clicked
                    $scope.CreateSplitIntermittentViewModel();
                };

                var IntermittentRequestViewModel = null;
                if ($scope.IntermittentRequestViewModelList != undefined) {
                    if (itorIndex < $scope.IntermittentRequestViewModelList.length) {
                        IntermittentRequestViewModel = $scope.IntermittentRequestViewModelList[itorIndex];
                    }
                }
                IntermittentRequestViewModel.RequestDate = new Date(IntermittentRequestViewModel.RequestDate).toDateString();
                $http({ method: 'POST', url: '/Cases/RemoveTimeOffRequest', data: IntermittentRequestViewModel }).then(function (response, status) {
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        $scope.submitted = false;
                        $scope.frmIntermittent.$setPristine();
                        // refresh policy related sections
                        $scope.RefreshDetails();
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                        $scope.IntermittentRequestViewModelList.splice(itorIndex, 1);
                        if ($scope.IntermittentRequestViewModelList.length <= 0) {
                            $('#intermittentRequest').modal('toggle');
                        }
                        $('#intermittentRequest').on('hidden.bs.modal', function () {
                            $(this).hide();
                        })
                        $scope.SaveInProgress = false;
                    }
                }).error(function (response) {
                    $scope.WarningYesNo = "No";
                    $scope.ErrorYesNo = "No";
                    $scope.InfoYesNo = "No";
                    $scope.SaveInProgress = false;
                });

                ////Re-enable the "Split Approval Decisions" button
                //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
                $scope.SplitApprovalDecisionsToggle[itorIndex] = false;

                $('#deleteITOR').on('hidden.bs.modal', function () {
                    $(this).hide();
                })

            };

            $scope.CancelDeletePopup = function () {
                $("#deleteAllITOR").modal('hide');
            }
            $scope.DeleteAllTORWarning = function () {
                $("#deleteAllITOR").modal('show');;
            }

        //POST final Intermittent Request view model
        $scope.DeleteTimeOffRequest = function (isValid) {
            if (!isValid)
                return;

            //If "Split Approval Decisions" button is NOT clicked
            if ($scope.SplitApprovalDecisionsToggle == false) {
                //Create view model now since "Split Approval Decision" button was not clicked
                $scope.CreateSplitIntermittentViewModel();
            };

            //$scope.IntermittentRequestViewModel.RequestDate = new Date($scope.IntermittentRequestViewModel.RequestDate).toDateString();
            $scope.IntermittentRequestViewModel.RequestDate = new Date($scope.IntermittentRequestDate).toDateString();

            //return;

            $http({ method: 'POST', url: '/Cases/DeleteTimeOffRequest', data: $scope.IntermittentRequestViewModel }).success(function (response, status) {
                if (response != null && response != '' && JSON.stringify(response) != '[]') {
                    //reset object
                    $scope.IntermittentRequestDate = '';
                    $scope.IntermittentRequestViewModel.IntermittentType = 0;
                    $scope.IntermittentRequestViewModel.RequestDate = '';
                    $scope.IntermittentRequestViewModel.PendingTime = '';
                    $scope.IntermittentRequestViewModel.ApprovedTime = '';
                    $scope.IntermittentRequestViewModel.DeniedTime = '';
                    $scope.IntermittentRequestViewModel.DenialReason = '';
                    $scope.IntermittentRequestViewModel.DenialReasonOther = '';
                    $scope.IntermittentRequestViewModel.Notes = '';
                    $scope.IntermittentRequestViewModel.Details = [];
                    $scope.submitted = false;
                    $scope.frmIntermittent.$setPristine();

                    // refresh policy related sections
                    $scope.RefreshDetails();

                   
                  
                    $scope.submitted = false;
                    $scope.ToDosFilter.CaseId = $scope.CaseId;
                    toDoListManager.List($scope, false, $scope.ErrorHandler);                    
                  
                    $('#intermittentRequest').on('hidden.bs.modal', function () {
                        $(this).hide();
                    })
                }
            }).error(function (response) {
                $scope.WarningYesNo = "No";
                $scope.ErrorYesNo = "No";
                $scope.InfoYesNo = "No";

            });;

            ////Re-enable the "Split Approval Decisions" button
            //$scope.SplitDisabled = false;
            //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
            $scope.SplitApprovalDecisionsToggle = false;         
           
            $('#deleteITOR').on('hidden.bs.modal', function () {
                $(this).hide();
            })
                  
        };
        $scope.CancelDeletePopup = function () { $("#deleteITOR").modal("hide"); }

        //POST final Intermittent Request view model
        $scope.DeleteTimeOffRequest = function (isValid) {
            if (!isValid)
                return;

            //If "Split Approval Decisions" button is NOT clicked
            if ($scope.SplitApprovalDecisionsToggle == false) {
                //Create view model now since "Split Approval Decision" button was not clicked
                $scope.CreateSplitIntermittentViewModel();
            };

            //$scope.IntermittentRequestViewModel.RequestDate = new Date($scope.IntermittentRequestViewModel.RequestDate).toDateString();
            $scope.IntermittentRequestViewModel.RequestDate = new Date($scope.IntermittentRequestDate).toDateString();

            //return;

            $http({ method: 'POST', url: '/Cases/DeleteTimeOffRequest', data: $scope.IntermittentRequestViewModel }).then(function (response, status) {
                if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                    //reset object
                    $scope.IntermittentRequestDate = '';
                    $scope.IntermittentRequestViewModel.IntermittentType = 0;
                    $scope.IntermittentRequestViewModel.RequestDate = '';
                    $scope.IntermittentRequestViewModel.PendingTime = '';
                    $scope.IntermittentRequestViewModel.ApprovedTime = '';
                    $scope.IntermittentRequestViewModel.DeniedTime = '';
                    $scope.IntermittentRequestViewModel.DenialReason = '';
                    $scope.IntermittentRequestViewModel.DenialReasonOther = '';
                    $scope.IntermittentRequestViewModel.Notes = '';
                    $scope.IntermittentRequestViewModel.Details = [];
                    $scope.submitted = false;
                    $scope.frmIntermittent.$setPristine();

                    // refresh policy related sections
                    $scope.RefreshDetails();

                   
                  
                    $scope.submitted = false;
                    $scope.ToDosFilter.CaseId = $scope.CaseId;
                    toDoListManager.List($scope, false, $scope.ErrorHandler);                    
                  
                    $('#intermittentRequest').on('hidden.bs.modal', function () {
                        $(this).hide();
                    })
                }
            }).error(function (response) {
                $scope.WarningYesNo = "No";
                $scope.ErrorYesNo = "No";
                $scope.InfoYesNo = "No";

            });;

            ////Re-enable the "Split Approval Decisions" button
            //$scope.SplitDisabled = false;
            //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
            $scope.SplitApprovalDecisionsToggle = false;         
           
            $('#deleteITOR').on('hidden.bs.modal', function () {
                $(this).hide();
            })
                  
        };
        $scope.CancelDeletePopup = function () { $("#deleteITOR").modal("hide"); }

        $('#ddIntermittent').on('change', function () {
            $scope.IntermittentRequestViewModel.IntermittentType = this.value;          
        });
            //Edit Intermittent Request

            $scope.CalendarClickTimestamp = 0;
            $scope.EditIntermittentRequest = function (intermittentRequest) {

                if ((event.timeStamp - $scope.CalendarClickTimestamp) < 3000) {
                    return;
                }
                $scope.CalendarClickTimestamp = event.timeStamp;

                if (userPermissions.indexOf("EditITOR") <= -1)
                    return;
                $scope.IntermittentRequestViewModelList = [];
                if (!$scope.IsEmployeeSelfService) {
                    $scope.IsEdit = true;
                    $scope.SplitApprovalDecisionsToggle = [];
                    $http({ method: 'GET', url: "/Cases/" + $scope.CaseId + "/GetTimeOffRequest", params: { caseId: $scope.CaseId, requestDate: intermittentRequest.Date.toDateString() } }).
                        then(function (response) {
                            $scope.IntermittentRequestViewModelList = response.data;
                            if ($scope.IntermittentRequestViewModelList.length > 0) {
                                for (var i = 0; i < $scope.IntermittentRequestViewModelList.length; i++) {

                                    $scope.IntermittentRequestViewModelList[i].RequestDate = $filter('ISODateToDate')($scope.IntermittentRequestViewModelList[i].RequestDate);

                                    $scope.SplitApprovalDecisionsToggle.push(false);
                                    if ($scope.IntermittentRequestViewModelList[i].StartTimeOfEvent == null) {
                                        $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent = { Hour: "HH", Minutes: "MM" };
                                    }

                                    if ($scope.IntermittentRequestViewModelList[i].EndTimeOfEvent == null) {
                                        $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent = { Hour: "HH", Minutes: "MM" };
                                    }
                                    var startHours = $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Hour == null ? 0 : $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Hour;
                                    var endHours = $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Hour == null ? 0 : $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Hour;
                                    var startMinutes = $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Minutes == null ? 0 : $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Minutes;
                                    var endMinutes = $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Minutes == null ? 0 : $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Minutes;

                                    $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Hour = startHours < 10 ? "0" + startHours : startHours;
                                    $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Hour = endHours < 10 ? "0" + endHours : endHours;

                                    $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Minutes = startMinutes < 10 ? "0" + startMinutes : startMinutes;
                                    $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Minutes = endMinutes < 10 ? "0" + endMinutes : endMinutes;

                                    if ($scope.IntermittentRequestViewModelList[i].ApprovedTime == "0h")
                                        $scope.IntermittentRequestViewModelList[i].ApprovedTime = "";

                                    if ($scope.IntermittentRequestViewModelList[i].PendingTime == "0h")
                                        $scope.IntermittentRequestViewModelList[i].PendingTime = "";

                                    if ($scope.IntermittentRequestViewModelList[i].DeniedTime == "0h")
                                        $scope.IntermittentRequestViewModelList[i].DeniedTime = "";

                                    angular.forEach($scope.IntermittentRequestViewModelList[i].Details, function (detail, index) {
                                        if (detail.ApprovedTime == "0h")
                                            detail.ApprovedTime = "";

                                        if (detail.PendingTime == "0h")
                                            detail.PendingTime = "";

                                        if (detail.DeniedTime == "0h")
                                            detail.DeniedTime = "";
                                    });

                                    //Check to see if time is equal for all policies
                                    if ($scope.IntermittentRequestViewModelList[i].IsTimeEqual == true) {
                                        // Set the group inputs equal to the time in the first policy
                                        $scope.IntermittentRequestViewModelList[i].ApprovedTime = $scope.IntermittentRequestViewModelList[i].Details[0].ApprovedTime;
                                        $scope.IntermittentRequestViewModelList[i].PendingTime = $scope.IntermittentRequestViewModelList[i].Details[0].PendingTime;
                                        $scope.IntermittentRequestViewModelList[i].DeniedTime = $scope.IntermittentRequestViewModelList[i].Details[0].DeniedTime;
                                        $scope.IntermittentRequestViewModelList[i].DenialReasonCode = $scope.IntermittentRequestViewModelList[i].Details[0].DenialReasonCode;
                                        $scope.IntermittentRequestViewModelList[i].DenialReasonOther = $scope.IntermittentRequestViewModelList[i].Details[0].DenialReasonOther;


                                        //shows each policy in the UI
                                        $scope.SplitApprovalDecisionsToggle[i] = false;
                                        //hide the group time inputs and Split button
                                        $scope.HideGroupTimeInputs = false;

                                    } else { // The time for these policies are NOT equal

                                        //shows each policy in the UI
                                        $scope.SplitApprovalDecisionsToggle[i] = true;
                                        //hide the group time inputs and Split button
                                        $scope.HideGroupTimeInputs = true;

                                    };
                                }
                            }

                        });
                    $timeout(function () {
                        $('#intermittentRequest').modal('show');
                        if ($scope.IntermittentRequestViewModelList != undefined) {
                            for (var i = 0; i < $scope.IntermittentRequestViewModelList.length; i++) {
                                var startHours = $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Hour;
                                var endHours = $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Hour;
                                var startMinutes = $scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.Minutes;
                                var endMinutes = $scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.Minutes;

                                var indexVal = angular.copy(i);
                                $('#start-time-picker_' + indexVal).timepicker({
                                    showMeridian: true,
                                    defaultTime: 'current',
                                    showInputs: false
                                }).on('changeTime.timepicker', function (ev) {
                                    $scope.StartTimeChangeEvent(ev, ev.currentTarget.id.split("_")[1]);
                                });
                                $('#start-time-picker_' + indexVal).val("");
                                $('#end-time-picker_' + indexVal).timepicker({
                                    showMeridian: true,
                                    defaultTime: 'current',
                                    showInputs: false
                                }).on('changeTime.timepicker', function (ev) {
                                    $scope.EndTimeChangeEvent(ev, angular.copy(ev.currentTarget.id.split("_")[1]));
                                });
                                $('#end-time-picker_' + indexVal).val("");
                                //-------------------------------------
                                var pendingTimeInEditMode = $scope.IntermittentRequestViewModelList[i].PendingTime;
                                if (startHours == 0 && startMinutes <= 0 && endHours <= 0 && endMinutes <= 0) {
                                    $('#start-time-picker_' + i).timepicker('setTime', "");
                                    $('#end-time-picker_' + i).timepicker('setTime', "");
                                }
                                else {
                                    var startTime = startHours + ':' + startMinutes + ' ';
                                    if ($scope.IntermittentRequestViewModelList[i].StartTimeOfEvent.PM) {
                                        startTime = startTime + 'PM';
                                    }
                                    else {
                                        startTime = (startTime == '00:00 ' ? '12:00 ' : startTime) + 'AM';
                                    }
                                    $('#start-time-picker_' + i).timepicker('setTime', startTime);

                                    var endTime = endHours + ':' + endMinutes + ' ';
                                    if ($scope.IntermittentRequestViewModelList[i].EndTimeOfEvent.PM) {
                                        endTime = endTime + 'PM';
                                    }
                                    else {
                                        endTime = (endTime == '00:00 ' ? '12:00 ' : endTime) + 'AM';
                                    }
                                    $('#end-time-picker_' + i).timepicker('setTime', endTime);
                                }
                                $scope.IntermittentRequestViewModelList[i].PendingTime = pendingTimeInEditMode;
                                $('#ddIntermittent_' + indexVal).val($scope.IntermittentRequestViewModelList[i].IntermittentType);
                                $('#IntermittentRequestViewModelPendingTime_' + indexVal).val(pendingTimeInEditMode);
                            }
                        }
                    }, 1000);
                }
                else {
                    $scope.$apply(function () {
                        $scope.ESSIntermittentRequestViewModel = {
                            CaseId: $scope.CaseId,
                            Date: intermittentRequest.Date,
                            RequestedTime: intermittentRequest.Cases[0].Policies[0].HoursUsed,
                            Notes: '',
                            IsESSIntermittentEdit: true,
                            Details: []
                        };
                        $scope.ESSIntermittentRequestViewModel.Details = [];

                        angular.forEach($scope.TimeTrackingInfo, function (policy, index) {

                            var reqPolicyData = $filter('filter')(intermittentRequest.Cases[0].Policies, {
                                PolicyCode: policy.PolicyCode
                            });


                            var policyDetail = new Object();
                            policyDetail.PolicyCode = policy.PolicyCode;
                            policyDetail.PolicyName = policy.PolicyName;
                            policyDetail.Available = policy.TimeRemainingText;
                            policyDetail.PendingTime = reqPolicyData[0].HoursUsed;
                            policyDetail.ApprovedTime = "";
                            policyDetail.DeniedTime = "";
                            policyDetail.DenialReason = null;
                            policyDetail.DenialReasonOther = "";
                            policyDetail.IsApprovedDisabled = false,
                                $scope.ESSIntermittentRequestViewModel.Details.push(policyDetail);
                        });


                        $scope.ESSIntermittentRequestDate = $filter('ISODateToDate')($scope.ESSIntermittentRequestViewModel.Date);
                        if (intermittentRequest.HasApprovedUsage)
                            $scope.ESSIntermittentRequestViewModel.ApprovedTime = intermittentRequest.Cases[0].Policies[0].HoursUsed;
                        else
                            $scope.ESSIntermittentRequestViewModel.ApprovedTime = 0;
                    });

                    $('#ESSIntermittentRequest').modal('show');
                }

            };

            $scope.AddAnotherIntermittentRequest = function () {
                IntermittentRequestView = angular.copy($scope.IntermittentRequestViewModelList[0]);
                IntermittentRequestView.Id = null;

                IntermittentRequestView.PendingTime = "";
                IntermittentRequestView.ApprovedTime = "";
                IntermittentRequestView.DeniedTime = "";
                IntermittentRequestView.IntermittentType = 0;
                $scope.IntermittentRequestViewModelList.push(IntermittentRequestView);
                $scope.SplitApprovalDecisionsToggle.push(false);
                $timeout(function () {
                    var totalItor = $scope.IntermittentRequestViewModelList.length - 1;
                    $('#start-time-picker_' + totalItor).timepicker({
                        showMeridian: true,
                        defaultTime: 'current',
                        showInputs: false
                    }).on('changeTime.timepicker', function (ev) {
                        $scope.StartTimeChangeEvent(ev, totalItor);
                    });
                    $('#start-time-picker_' + totalItor).val("");
                    $('#end-time-picker_' + totalItor).timepicker({
                        showMeridian: true,
                        defaultTime: 'current',
                        showInputs: false
                    }).on('changeTime.timepicker', function (ev) {
                        $scope.EndTimeChangeEvent(ev, totalItor);
                    });
                    $('#end-time-picker_' + totalItor).val("");
                }, 500);
            };

            $scope.calculatePendingValue = function (startTime, endTime, itorIndex) {
                if ($scope.IntermittentRequestViewModelList[itorIndex].RequestDate == "" || $scope.IntermittentRequestViewModelList[itorIndex].RequestDate == null)
                    return;

                var date = new Date($scope.IntermittentRequestViewModelList[itorIndex].RequestDate);

                var intermittentDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString().split("T")[0];

                var isPmStart = startTime.PM != false ? "PM" : "AM";
                var isPmEnd = endTime.PM != false ? "PM" : "AM";

                if ($scope.IntermittentRequestViewModelList[itorIndex].RequestDate == "") {
                    intermittentDate = new Date().formatMMDDYYYY();
                }

                if (startTime.Hour == "" || endTime.Hour == "" || startTime.Hour == "HH" || endTime.Hour == "HH" || !endTime.Hour || !startTime.Hour)
                    return;

                var minutesStart = startTime.Minutes;
                if (startTime.Minutes == "" || startTime.Minutes == "MM" || !startTime.Minutes) {
                    minutesStart = "00";
                }
                var minutesEnd = endTime.Minutes;
                if (endTime.Minutes == "" || endTime.Minutes == "MM" || !endTime.Minutes) {
                    minutesEnd = "00";
                }

                var startTime = intermittentDate.replace(/-/g, '/') + " " + startTime.Hour + ":" + minutesStart + " " + isPmStart;
                var endTime = intermittentDate.replace(/-/g, '/') + " " + endTime.Hour + ":" + minutesEnd + " " + isPmEnd;
                var timeStart = new Date(startTime);
                var timeEnd = new Date(endTime);

                var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds

                var minutes = diff % 60;
                var hours = (diff - minutes) / 60;
                if ($scope.IntermittentRequestViewModelList != undefined) {
                    $scope.IntermittentRequestViewModelList[itorIndex].PendingTime = hours + "h " + minutes + "m"; // return hours and minutes
                    if ($scope.IntermittentRequestViewModelList[itorIndex].Details != undefined && $scope.IntermittentRequestViewModelList[itorIndex].Details.length > 0) {
                        $scope.IntermittentRequestViewModelList[itorIndex].Details[0].DeniedTime = hours + "h " + minutes + "m"; // return hours and minutes
                    }
                }
            };
            //#endregion Intermittent Request   


            //#region "ESS Intermittent Request"
            //Initialize Intermittent Request View Model
            $scope.initESSIntermittentRequest = function () {
                $scope.ESSIntermittentRequestViewModel = {
                    CaseId: $scope.CaseId,
                    Date: '',
                    RequestedTime: '',
                    Notes: '',
                    IsESSIntermittentEdit: false,
                    Details: []
                };
                $scope.ESSIntermittentRequestDate = '';


                angular.forEach($scope.TimeTrackingInfo, function (policy, index) {
                    var policyDetail = new Object();
                    policyDetail.PolicyCode = policy.PolicyCode;
                    policyDetail.PolicyName = policy.PolicyName;
                    policyDetail.Available = policy.TimeRemainingText;
                    policyDetail.PendingTime = "";
                    policyDetail.ApprovedTime = "";
                    policyDetail.DeniedTime = "";
                    policyDetail.DenialReason = null;
                    policyDetail.DenialReasonOther = "";
                    policyDetail.IsApprovedDisabled = false,
                        $scope.ESSIntermittentRequestViewModel.Details.push(policyDetail);
                });


                $("#ESSIntermittentRequest").modal('show');

                $scope.frmESSIntermittent.$setPristine();
            }

            //POST final Intermittent Request view model
            $scope.CreateOrModifyESSTimeOffRequest = function (isValid) {
                if (!isValid)
                    return;

                $scope.ESSIntermittentRequestViewModel.RequestDate = new Date($scope.ESSIntermittentRequestDate).toDateString();
                $scope.ESSIntermittentRequestViewModel.PendingTime = $scope.ESSIntermittentRequestViewModel.RequestedTime;

                angular.forEach($scope.ESSIntermittentRequestViewModel.Details, function (detail, index) {
                    detail.PendingTime = $scope.ESSIntermittentRequestViewModel.RequestedTime;
                });

                $http({ method: 'POST', url: '/Cases/CreateOrModifyTimeOffRequest', data: $scope.ESSIntermittentRequestViewModel }).then(function (response) {
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        //reset object
                        $scope.ESSIntermittentRequestViewModel = {
                            CaseId: $scope.CaseId,
                            Date: '',
                            RequestedTime: '',
                            Notes: '',
                            IsESSIntermittentEdit: false,
                            Details: []
                        };
                        $scope.ESSIntermittentRequestDate = '';
                        $scope.EssIntermittentSubmitted = false;
                        $scope.frmESSIntermittent.$setPristine();

                        // refresh policy related sections
                        $scope.RefreshDetails();

                        $('#ESSIntermittentRequest').modal('hide');
                    }
                }, function (response) {

                    $scope.WarningYesNo = "No";
                    $scope.ErrorYesNo = "No";
                    $scope.InfoYesNo = "No";

                });;

                ////Re-enable the "Split Approval Decisions" button
                //$scope.SplitDisabled = false;
                //If this is true, user wants to split approval decisions; reset to false to hide each policy in the UI
                $scope.SplitApprovalDecisionsToggle = false;
            };
            //#endregion "ESS Intermittent Request"

            //OpenTodoModal
            $scope.OpenTodoModal = function (toDo) {
                todoModalService.OpenTodoModal($scope, toDo, $scope.ToDos, $scope.ToDosFilter, true, $scope.UsersList);
                //todoModalService.OpenTodoModal($scope.NextToDo);
            };

            //OpenContactsModal
            $scope.OpenContactsModal = function () {
                contactService.OpenContactsModal($scope, $scope.EmployeeId);
            };

            //case cancel Modal popup
            $scope.CaseCancelReason = lookupService.CaseCancelReason();

            $scope.CancelCaseChange = function () {

                if (!$scope.frmChangeCase.$valid)
                    return;

                $http({
                    method: 'POST',
                    url: '/Cases/' + $scope.CaseId + '/Cancel?reason=' + $("#ddlCaseCancel option:selected").text()
                }).then(function (response) {
                    $scope.CreatedLeaveOfAbsence = response.data;
                });
                window.location.href = "/Employees/" + $scope.EmployeeId + "/View/";
                //window.location.href = "/";
            }

            $scope.GoToPrev3Months = function (dtRangeStart) {
                $scope.CaseCalendarDateRange = caseService.GetNextPrevRangeForCalendarForCase(dtRangeStart, "prev");
                $scope.GetCalendarData();
            }

            $scope.GoToNext3Months = function (dtRangeStart) {
                $scope.CaseCalendarDateRange = caseService.GetNextPrevRangeForCalendarForCase(dtRangeStart, "next");
                $scope.GetCalendarData();
            }


            // when select main denial reason option then automaticlly change sub select option value
            $scope.ChangeAllDenialReasons = function (item) {
                var denialReasonName = null;
                
                for (i = 0; i < $scope.DenialReason.length; i++) {
                    if ($scope.DenialReason[i].Code == item) {
                        denialReasonName = $scope.DenialReason[i].Description;
                        break;
                    }
                }

                $scope.changepolicy.AdjudicationDenialReasonName = denialReasonName;

                for (var i = 0; i < $scope.changepolicy.Policies.length; i++) {
                    $scope.changepolicy.Policies[i].AdjudicationDenialReasonCode = item;
                    $scope.changepolicy.Policies[i].AdjudicationDenialReasonName = denialReasonName;
                }
            };

            //#region "Change Policy Status"

            $scope.ChangePolicyStatus = function () {

                $scope.cancel();
                $scope.ChangePolicyStatusViewModel = {
                    CaseId: $scope.CaseId,
                    ApprovedStartDate: '',
                    ApprovedEndDate: '',
                    DeniedStartDate: '',
                    DeniedEndDate: '',
                    PendingStartDate: '',
                    PendingEndDate: '',
                    AdjudicationDenialReasonCode: null,
                    DenialExplanation: '',
                    Policies: []
                };
                
                angular.forEach($scope.AppliedPoliciesList, function (policy, index) {
                    var policyDetail = new Object();
                    policyDetail.PolicyCode = policy.PolicyCode;
                    policyDetail.ApplyStatus = true;
                    policyDetail.AdjudicationDenialReasonCode = null;
                    policyDetail.DenialExplanation = "";
                    $scope.ChangePolicyStatusViewModel.Policies.push(policyDetail);
                });

                $scope.changepolicy = $scope.ChangePolicyStatusViewModel;

                //check all policy checkboxes
                $scope.CheckAll();

                //Show inputs for changing status
                $scope.ChangeStatus = true;
            };

            $scope.changepolicy = $scope.ChangePolicyStatusViewModel;

            $scope.CheckAll = function () {
                $('.policy-checkbox').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "policy-checkbox"
                });
                if ($scope.ChangePolicyStatusViewModel !== undefined && $scope.ChangePolicyStatusViewModel.Policies !== undefined)
                    angular.forEach($scope.ChangePolicyStatusViewModel.Policies, function (policy, index) {
                        policy.ApplyStatus = true;
                    });
            };

            $scope.UncheckAll = function () {
                $('.policy-checkbox').each(function () { //loop through each checkbox
                    this.checked = false;  //unselect all checkboxes with class "policy-checkbox"
                });
                if ($scope.ChangePolicyStatusViewModel !== undefined && $scope.ChangePolicyStatusViewModel.Policies !== undefined)
                    angular.forEach($scope.ChangePolicyStatusViewModel.Policies, function (policy, index) {
                        policy.ApplyStatus = false;
                    });
            };

            $scope.CheckForApprovalSuggestedPeriod = function () {
                if (!$scope.showWarning && !$scope.showOtherWarning) {
                    if ($scope.ApprovedStartDate != '' &&
                        $scope.ApprovedStartDate != undefined &&
                        $scope.ApprovedEndDate != '' &&
                        $scope.ApprovedEndDate != undefined) {
                        $scope.changepolicy.ApprovedStartDate = new Date($scope.ApprovedStartDate).toDateString();
                        $scope.changepolicy.ApprovedEndDate = new Date($scope.ApprovedEndDate).toDateString();

                        var diffDays = parseInt((new Date($scope.ApprovedEndDate) - new Date($scope.ApprovedStartDate)) / (1000 * 60 * 60 * 24));
                        if ($scope.IsSTD) {
                            if ($scope.CaseInfo.Case.Disability.PrimaryPathMaxDays != null && $scope.CaseInfo.Case.Disability.PrimaryPathMaxDays != undefined) {
                                if (diffDays > $scope.CaseInfo.Case.Disability.PrimaryPathMaxDays) {
                                    $("#ApprovalPeriodConfirmationPopup").modal('show');
                                    return;
                                }
                            }
                        }
                    }
                    else {
                        pSubmitted = false;
                    }

                    $scope.ApplyChangePolicyStatus();
                }
            }

            $scope.ApprovalOverride = function () {
                $("#ApprovalPeriodConfirmationPopup").modal('hide');
                $scope.ApplyChangePolicyStatus();
            }

            $scope.cancel = function () {
                // hide inputs for changing status
                $scope.ChangeStatus = !$scope.ChangeStatus;

                // clear date inputs and set form to pristine state
                $scope.ApprovedStartDate = null;
                $scope.ApprovedEndDate = null;
                $scope.DeniedStartDate = null;
                $scope.DeniedEndDate = null;
                $scope.PendingStartDate = null;
                $scope.PendingEndDate = null;
                $scope.frmChangeStatus.DeniedStartDate.$setPristine(true);
                $scope.frmChangeStatus.DeniedEndDate.$setValidity();
                $scope.frmChangeStatus.PendingStartDate.$setValidity();
                $scope.frmChangeStatus.PendingEndDate.$setValidity();
                $scope.frmChangeStatus.ApprovedStartDate.$setValidity();
                $scope.frmChangeStatus.ApprovedEndDate.$setValidity();

                //set submitted back to false to clear form errors
                $scope.pSubmitted = false;

                //set whole form to pristine
                $scope.frmChangeStatus.$setPristine();
            }

            $scope.cancelAR = function () {
                // hide inputs for changing status
                $scope.forms.ChangeStatusAccommodation = !$scope.forms.ChangeStatusAccommodation;

                // clear date inputs and set form to pristine state
                $scope.AccommodationRequest.ApprovedStartDate = null;
                $scope.AccommodationRequest.ApprovedEndDate = null;
                $scope.AccommodationRequest.DeniedStartDate = null;
                $scope.AccommodationRequest.DeniedEndDate = null;
                $scope.AccommodationRequest.PendingStartDate = null;
                $scope.AccommodationRequest.PendingEndDate = null;
                $scope.frmChangeStatusAccommodations.ApprovedStartDateAccomm.$setValidity();
                $scope.frmChangeStatusAccommodations.ApprovedEndDateAccomm.$setValidity();
                $scope.frmChangeStatusAccommodations.PendingStartDateAccomm.$setValidity();
                $scope.frmChangeStatusAccommodations.PendingEndDateAccomm.$setValidity();
                $scope.frmChangeStatusAccommodations.DeniedStartDateAccomm.$setValidity();
                $scope.frmChangeStatusAccommodations.DeniedEndDateAccomm.$setValidity();

                //set submitted back to false to clear form errors
                $scope.submittedAR = false;

                //set whole form to pristine
                $scope.frmChangeStatusAccommodations.$setPristine();
            }



            $scope.ApplyChangePolicyStatus = function () {
                var AnyDatesSet = false;

                if ($scope.ApprovedStartDate != '' &&
                    $scope.ApprovedStartDate != undefined &&
                    $scope.ApprovedEndDate != '' &&
                    $scope.ApprovedEndDate != undefined) {
                    AnyDatesSet = true;
                    $scope.changepolicy.ApprovedStartDate = new Date($scope.ApprovedStartDate).toDateString();
                    $scope.changepolicy.ApprovedEndDate = new Date($scope.ApprovedEndDate).toDateString();
                }


                if ($scope.DeniedStartDate != '' &&
                    $scope.DeniedStartDate != undefined &&
                    $scope.DeniedEndDate != '' &&
                    $scope.DeniedEndDate != undefined) {
                    AnyDatesSet = true;
                    $scope.changepolicy.DeniedStartDate = new Date($scope.DeniedStartDate).toDateString();
                    $scope.changepolicy.DeniedEndDate = new Date($scope.DeniedEndDate).toDateString();
                }

                if ($scope.PendingStartDate != '' &&
                    $scope.PendingStartDate != undefined &&
                    $scope.PendingEndDate != '' &&
                    $scope.PendingEndDate != undefined) {
                    AnyDatesSet = true;
                    $scope.changepolicy.PendingStartDate = new Date($scope.PendingStartDate).toDateString();
                    $scope.changepolicy.PendingEndDate = new Date($scope.PendingEndDate).toDateString();
                }

                if (AnyDatesSet) {
                    caseService.ApplyChangePolicyStatus($scope.CaseId, $scope.changepolicy).then(function (response) {
                        // refresh policy related sections
                        $scope.RefreshDetails();



                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);

                        //hide inputs for changing status
                        $scope.ChangeStatus = false;

                        // clear date inputs and set form to pristine state
                        $scope.ApprovedStartDate = null;
                        $scope.ApprovedEndDate = null;
                        $scope.DeniedStartDate = null;
                        $scope.DeniedEndDate = null;
                        $scope.PendingStartDate = null;
                        $scope.PendingEndDate = null;
                        $scope.frmChangeStatus.$setPristine();

                    });
                }
                else {
                    // keep form open 
                    $scope.ChangeStatus = true;
                }


            }



            //#endregion "Change Policy Status"

            $scope.ChangeAccommodationStatus = function () {


                $scope.ChangeAccommodationStatusViewModel = {
                    CaseId: $scope.CaseId,
                    ApprovedStartDate: '',
                    ApprovedEndDate: '',
                    DeniedStartDate: '',
                    DeniedEndDate: '',
                    PendingStartDate: '',
                    PendingEndDate: '',
                    AdjudicationDenialReason: '',
                    DenialExplanation: '',
                    Accommodations: []
                };

                angular.forEach($scope.AccommodationRequest.Accommodations, function (accommodation, index) {
                    var accommodationDetail = new Object();
                    accommodationDetail.AccommodationId = accommodation.Id;
                    accommodationDetail.Status = accommodation.Status;
                    if (accommodation.Status != 2)
                        accommodationDetail.ApplyStatus = true;
                    else
                        accommodationDetail.ApplyStatus = false;
                    $scope.ChangeAccommodationStatusViewModel.Accommodations.push(accommodationDetail);
                });

                $scope.changeaccommodation = $scope.ChangeAccommodationStatusViewModel;

                //check all policy checkboxes
                //$scope.CheckAllAccommodations();

                //Show inputs for changing status
                // $scope.forms.ChangeStatusAccommodation = true;
            };

            $scope.CheckAllAccommodations = function () {
                //$('.accommodation-checkbox').each(function () { //loop through each checkbox
                //    this.checked = true;  //select all checkboxes with class "accommodation-checkbox"
                //});

                angular.forEach($scope.changeaccommodation.Accommodations, function (accommodation, index) {
                    if (accommodation.Status != 2)
                        accommodation.ApplyStatus = true;
                    else
                        accommodation.ApplyStatus = false;
                });
            };

            $scope.UncheckAllAccommodations = function () {
                //$('.accommodation-checkbox').each(function () { //loop through each checkbox
                //    this.checked = false;  //unselect all checkboxes with class "accommodation-checkbox"
                //});

                angular.forEach($scope.changeaccommodation.Accommodations, function (accommodation, index) {
                    accommodation.ApplyStatus = false;
                });
            };

            $scope.NotesButtonClick = function () {
                if ($scope.ViewNotes1 == true) {
                    $scope.ViewNotes1 == false;
                }
                else {
                    $scope.ViewNotes1 == true;
                }
            }

            $scope.ApplyChangeAccommodationStatus = function () {

                var AnyOtherDatesSet = false;

                if ($scope.AccommodationRequest.ApprovedStartDate != '' &&
                    $scope.AccommodationRequest.ApprovedStartDate != undefined) {
                    AnyOtherDatesSet = true;
                }


                if ($scope.AccommodationRequest.DeniedStartDate != '' &&
                    $scope.AccommodationRequest.DeniedStartDate != undefined) {
                    AnyOtherDatesSet = true;
                }

                if ($scope.AccommodationRequest.PendingStartDate != '' &&
                    $scope.AccommodationRequest.PendingStartDate != undefined) {
                    AnyOtherDatesSet = true;
                }

                if (AnyOtherDatesSet) {

                    $scope.changeaccommodation.ApprovedStartDate = $scope.AccommodationRequest.ApprovedStartDate == null || $scope.AccommodationRequest.ApprovedStartDate == "" || $scope.AccommodationRequest.ApprovedStartDate == undefined ? null : new Date($scope.AccommodationRequest.ApprovedStartDate).toDateString();
                    $scope.changeaccommodation.ApprovedEndDate = $scope.AccommodationRequest.ApprovedEndDate == null || $scope.AccommodationRequest.ApprovedEndDate == "" || $scope.AccommodationRequest.ApprovedEndDate == undefined ? null : new Date($scope.AccommodationRequest.ApprovedEndDate).toDateString();

                    $scope.changeaccommodation.DeniedStartDate = $scope.AccommodationRequest.DeniedStartDate == null || $scope.AccommodationRequest.DeniedStartDate == "" || $scope.AccommodationRequest.DeniedStartDate == undefined ? null : new Date($scope.AccommodationRequest.DeniedStartDate).toDateString();
                    $scope.changeaccommodation.DeniedEndDate = $scope.AccommodationRequest.DeniedEndDate == null || $scope.AccommodationRequest.DeniedEndDate == "" || $scope.AccommodationRequest.DeniedEndDate == undefined ? null : new Date($scope.AccommodationRequest.DeniedEndDate).toDateString();

                    $scope.changeaccommodation.PendingStartDate = $scope.AccommodationRequest.PendingStartDate == null || $scope.AccommodationRequest.PendingStartDate == "" || $scope.AccommodationRequest.PendingStartDate == undefined ? null : new Date($scope.AccommodationRequest.PendingStartDate).toDateString();
                    $scope.changeaccommodation.PendingEndDate = $scope.AccommodationRequest.PendingEndDate == null || $scope.AccommodationRequest.PendingEndDate == "" || $scope.AccommodationRequest.PendingEndDate == undefined ? null : new Date($scope.AccommodationRequest.PendingEndDate).toDateString();

                    $scope.changeaccommodation.Resolved = $scope.AccommodationRequest.Resolved;
                    $scope.changeaccommodation.ResolvedDate = $scope.AccommodationRequest.ResolvedDate;
                    $scope.changeaccommodation.Resolution = $scope.AccommodationRequest.Resolution;

                    caseService.ApplyChangeAccommodationStatus($scope.CaseId, $scope.changeaccommodation).then(function (response) {
                        // refresh policy related sections
                        $scope.RefreshDetails();

                        // Workflow
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);

                        // hide inputs for changing status
                        $scope.forms.ChangeStatusAccommodation = false;

                        // clear date inputs and set form to pristine state
                        $scope.ApprovedStartDate = null;
                        $scope.ApprovedEndDate = null;
                        $scope.DeniedStartDate = null;
                        $scope.DeniedEndDate = null;
                        $scope.PendingStartDate = null;
                        $scope.PendingEndDate = null;

                        $scope.frmChangeStatusAccommodations.$setPristine();

                    });
                } else {
                    // keep form open 
                    $scope.forms.ChangeStatusAccommodation = true;
                }

                $scope.submittedAR = true;
            };

            $scope.AddProviderForm = function (isCreate) {
                if (isCreate) {
                    $scope.AddProvider = true;
                    $scope.GetProviderContacts();
                }
                else {
                    $scope.AddProviderEdit = true;
                }

            };

            $scope.GetProviderContacts = function () {
                $scope.AccommodationRequest.ProviderContacts = [];
                $http({
                    method: 'GET',
                    url: "/Employees/" + $scope.EmployeeId + "/Contacts/Medical" //provider
                }).then(function (response) {
                    $scope.AccommodationRequest.ProviderContacts = response.data;

                    var newContact = {
                    };
                    newContact.ContactId = '-1';
                    newContact.Name = 'New Provider';
                    $scope.AccommodationRequest.ProviderContacts.push(newContact);

                });
            };

            $scope.HCPContactChange = function (contactId) {
                if (contactId == null || contactId == undefined || contactId == '') {
                    $scope.NewHCP = false;
                }
                else {
                    $scope.NewHCP = true;
                    var contact = $filter('filter')($scope.AccommodationRequest.ProviderContacts, {
                        ContactId: contactId
                    });
                    if (contact.length > 0) {
                        $scope.AccommodationRequest.HCPCompanyName = contact[0].CompanyName;
                        $scope.AccommodationRequest.HCPFirstName = contact[0].FirstName;
                        $scope.AccommodationRequest.HCPLastName = contact[0].LastName;
                        $scope.AccommodationRequest.IsPrimary = contact[0].IsPrimary;
                        $scope.AccommodationRequest.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                        $scope.AccommodationRequest.HCPFax = $filter('tel')(contact[0].Fax);
                        $scope.AccommodationRequest.HCPCellPhone = $filter('tel')(contact[0].CellPhone);
                    }
                }
            }

            $scope.CancelAccommodationRequest = function (isValid, accomReqId) {

                if (!isValid)
                    return;

                $scope.EditAccommodation.Status = 2;
                $scope.EditAccommodation.CancelReason = $scope.AccomCancellation.Reason;
                $scope.EditAccommodation.OtherReasonDesc = $scope.AccomCancellation.OtherReasonDesc;


                $scope.CreateOrModifyAccommodation(true, false, false);

                //caseService.CancelAccommodationRequest($scope.CaseId, accomReqId, $scope.AccomCancellation.Reason, $scope.AccomCancellation.OtherReasonDesc).then(function (result) {
                //    if (result.Success) {

                //        $("#EditAccommodation").modal('hide');
                //        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Accommodation request cancelled successfully" });

                //        // Refresh accommodations
                //        $scope.GetAccommodationRequests();

                //        // refresh policy related sections
                //        //$scope.RefreshDetails();
                //        //$scope.GetCaseInfo();

                //        // Workflow
                //        //$scope.ToDosFilter.CaseId = $scope.CaseId;
                //        //toDoListManager.List($scope, false, $scope.ErrorHandler);
                //    }
                //});
            }

            $scope.UndoAccomRequestCancellation = function (accommodation, index) {
                $scope.EditAccommodationIndex = index;
                accommodation.StartDate = $filter('ISODateToDate')(accommodation.StartDate);
                accommodation.EndDate = $filter('ISODateToDate')(accommodation.EndDate);
                $scope.EditAccommodation = accommodation;

                // undo cancellation
                $scope.EditAccommodation.Status = 0;
                $scope.EditAccommodation.CancelReason = null;
                $scope.EditAccommodation.OtherReasonDesc = '';


                $scope.CreateOrModifyAccommodation(true, false, false);

                //caseService.CancelAccommodationRequest($scope.CaseId, accomReqId, $scope.AccomCancellation.Reason, $scope.AccomCancellation.OtherReasonDesc).then(function (result) {
                //    if (result.Success) {

                //        $("#EditAccommodation").modal('hide');
                //        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Accommodation request cancelled successfully" });

                //        // Refresh accommodations
                //        $scope.GetAccommodationRequests();

                //        // refresh policy related sections
                //        //$scope.RefreshDetails();
                //        //$scope.GetCaseInfo();

                //        // Workflow
                //        //$scope.ToDosFilter.CaseId = $scope.CaseId;
                //        //toDoListManager.List($scope, false, $scope.ErrorHandler);
                //    }
                //});
            }

            $scope.UndoAccommodationCancellation = function (accommodationRequest) {
                $scope.EditAccommodationRequest = accommodationRequest;

                // undo cancellation
                $scope.EditAccommodationRequest.Status = 0;
                $scope.EditAccommodationRequest.CancelReason = null;
                $scope.EditAccommodationRequest.OtherReasonDesc = '';


                $scope.CreateOrModifyAccommodation(true, false, true);

                //caseService.CancelAccommodationRequest($scope.CaseId, accomReqId, $scope.AccomCancellation.Reason, $scope.AccomCancellation.OtherReasonDesc).then(function (result) {
                //    if (result.Success) {

                //        $("#EditAccommodation").modal('hide');
                //        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Accommodation request cancelled successfully" });

                //        // Refresh accommodations
                //        $scope.GetAccommodationRequests();

                //        // refresh policy related sections
                //        //$scope.RefreshDetails();
                //        //$scope.GetCaseInfo();

                //        // Workflow
                //        //$scope.ToDosFilter.CaseId = $scope.CaseId;
                //        //toDoListManager.List($scope, false, $scope.ErrorHandler);
                //    }
                //});
            }

            $scope.cancelAccommodationRequests = function () {
                $scope.AccommodationRequest = angular.copy($scope.AccommodationRequestCopy);
                $scope.ShowAccomCancelReason = false;
            };

            //end accommodations
            $scope.compareDates = function (date1, date2, date3, date4) {
                var counter = 0;
                if (date1 && date4) {
                    if (date1.getTime() < new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                if (date2 && date4) {
                    if (date2.getTime() < new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                if (date3 && date4) {
                    if (date3.getTime() < new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                // don't know what this is supposed to be doing but right now but I can't apply any statuses (so if you are reading this comment and hardcoding then I forgot to remove it before I checked in sorry)
                $scope.showWarning = false; //(counter != 0);
                return $scope.showWarning;
            }
            $scope.compareDatesGreaterThan = function (date1, date2, date3, date4) {
                var counter = 0;
                if (date1 && date4) {
                    if (date1.getTime() > new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                if (date2 && date4) {
                    if (date2.getTime() > new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                if (date3 && date4) {
                    if (date3.getTime() > new Date(date4).getTime()) {
                        counter = counter + 1;
                    }
                }
                $scope.showOtherWarning = (counter != 0);
                return $scope.showOtherWarning;
            }

            $scope.GetDenialReasons = function (reasonTarget, caseId) {
                caseService.GetDenialReasons(reasonTarget, caseId)
                    .then(function (result) {
                        $scope.DenialReason = result;
                    },
                        $scope.ErrorHandler);
            }

            $scope.GetAccommodationDenialReasons = function (reasonTarget, caseId) {
                caseService.GetDenialReasons(reasonTarget, caseId)
                    .then(function (result) {
                        $scope.AccommodationDenialReason = result;
                    },
                        $scope.ErrorHandler);
            }

            $scope.GetPolicyData = function (caseId) {
                caseService.GetPolicyData(caseId)
                    .then(function (result) {
                        $scope.PolicyData = result;

                        // show suggested period message in policy section                       
                        $scope.GetSTDPolicyData();

                        $scope.ShowPayButton = false;
                        $scope.AppliedPoliciesList = [];
                        angular.forEach($scope.PolicyData.AppliedPolicies, function (item, index) {
                            var policy = {
                                PolicyCode: '', PolicyName: '', SeeSpouseCaseFlag: ''
                            };
                            policy.PolicyCode = item.PolicyCode;
                            policy.PolicyName = item.PolicyName;
                            policy.SeeSpouseCaseFlag = item.SeeSpouseCaseFlag;
                            $scope.AppliedPoliciesList.push(policy);
                            if (item.Paid && !$scope.ShowPayButton && $scope.STDPayFeatureEnabled)
                                $scope.ShowPayButton = true;
                        });

                        angular.forEach($scope.PolicyData.ManuallyAppliedPolicies, function (item, index) {
                            var policy = {
                                PolicyCode: '', PolicyName: '', SeeSpouseCaseFlag: ''
                            };
                            policy.PolicyCode = item.PolicyCode;
                            policy.PolicyName = item.PolicyName;
                            policy.SeeSpouseCaseFlag = item.SeeSpouseCaseFlag;
                            $scope.AppliedPoliciesList.push(policy);
                            if (item.Paid && !$scope.ShowPayButton && $scope.STDPayFeatureEnabled)
                                $scope.ShowPayButton = true;
                        });

                        if ($scope.AppliedPoliciesList.length == 0) {
                            $scope.NoPolicies = true;
                        }
                        else { $scope.NoPolicies = false; }

                        if ($scope.PolicyData.AppliedPolicies.length == 0 && $scope.PolicyData.ManuallyAppliedPolicies.length == 0
                            && ($scope.PolicyData.AvailableManualPolicies == null || $scope.PolicyData.AvailableManualPolicies.length == 0)) {
                            $scope.HidePolicies = true;
                        }
                    },
                    $scope.ErrorHandler);
            }

            $scope.GetTimeTrackingData = function () {
                // update Time Tracking Info
                var asOfDate = new Date();
                caseService.GetTimeTrackingInfo($scope.CaseId, asOfDate, {
                    includeStatus: [EligibilityStatus.byLabel.Eligible]
                })
                    .then(function (result) {
                        $scope.TimeTrackingInfo = result;
                        //// This is a hack for IE 8-11
                        //// Issue: When user clicks on "Apply Status", the data in the tables for "Policies," "Time Tracker" and "Absence History" should automatically refresh. This works in Chrome and FF. However in IE, only the "Absence History" table refreshes correctly. To see the correct data in the "Policies" and "Time Tracker" tables, the user has to manually refresh the page to see the data.
                        //// Solution: reload entire window in IE
                        //// Relates to GitHub Issue: https://github.com/absencesoftllc/tracker/issues/201
                        //// UPDATE: This may have fixed the issue: https://github.com/absencesoftllc/tracker/issues/216
                        //if (Object.hasOwnProperty.call(window, "ActiveXObject") && !window.ActiveXObject) {
                        //    location.reload(true);
                        //}
                    },
                    $scope.ErrorHandler);
            }

            $scope.GetSTDInfo = function () {
                caseService.GetSTDInfo($scope.CaseId)
                    .then(function (result) {
                        $scope.STD = result;
                    },
                    $scope.ErrorHandler);
            }

            //#region "Manual Schedule"
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            var noofdays = function (mm, yyyy) {
                var daysofmonth;

                if ((mm == 4) || (mm == 6) || (mm == 9) || (mm == 11)) {
                    daysofmonth = 30;
                }
                else {
                    daysofmonth = 31
                    if (mm == 2) {
                        if (yyyy / 4 - parseInt(yyyy / 4) != 0) {
                            daysofmonth = 28
                        }
                        else {
                            if (yyyy / 100 - parseInt(yyyy / 100) != 0) {
                                daysofmonth = 29
                            }
                            else {
                                if ((yyyy / 400) - parseInt(yyyy / 400) != 0) {
                                    daysofmonth = 28
                                }
                                else {
                                    daysofmonth = 29
                                }
                            }
                        }
                    }
                }

                return daysofmonth;
            }

            $scope.GetManualDays = function (startDate) {

                if ($scope.ChangeCase.WorkSchedule.ScheduleType != 2 && $scope.ReOpenCase.WorkSchedule.ScheduleType != 2)
                    return;

                if (startDate == null) return;

                // var selectedDate = new Date(startDate);
                var selectedDate = $filter('ISODateToDate')(startDate);
                $scope.CurrentDisplayMonth = selectedDate.getMonth();
                $scope.CurrentDisplayYear = selectedDate.getFullYear();

                $scope.CurrentMonthName = monthNames[$scope.CurrentDisplayMonth] + " " + $scope.CurrentDisplayYear;

                if ($scope.CurrentDisplayMonth == 0) {
                    $scope.PrevMonthName = monthNames[11] + " " + ($scope.CurrentDisplayYear - 1);
                    $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
                }
                else if ($scope.CurrentDisplayMonth == 11) {
                    $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                    $scope.NextMonthName = monthNames[0] + " " + ($scope.CurrentDisplayYear + 1);
                }
                else {
                    $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                    $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
                }

                var numOfDaysInMonth = noofdays($scope.CurrentDisplayMonth + 1);

                var startDay = selectedDate.getDate();

                var monthPosition = $.inArray($scope.CurrentDisplayMonth, $scope.AlreadyPushedMonths);
                if (monthPosition == -1) {
                    for (var i = 1; i <= numOfDaysInMonth; i++) {
                        var time = {
                            TotalMinutes: '', SampleDate: ''
                        };


                        var tempDate = selectedDate.getFullYear() + "-" + ('0' + ($scope.CurrentDisplayMonth + 1)).slice(-2) + "-" + ('0' + (i)).slice(-2);
                        time.SampleDate = $filter("date")(tempDate, "yyyy-MM-dd"); //$filter('ISODateToDate')(tempDate);
                        $scope.ManualTimes.push(time);
                    }

                    $scope.AlreadyPushedMonths.push($scope.CurrentDisplayMonth);
                    $scope.FilterDates();
                }
                else if ($scope.IsDateNotFiltered(selectedDate)) {
                    $scope.FilterDates();
                }
            };

            $scope.IsDateNotFiltered = function (dateToSearch) {
                if ($scope.FilteredDates.length <= 0) {
                    return true;
                }
                var filterIndex = 0;
                while (filterIndex < $scope.FilteredDates.length) {
                    var tempDate = $filter('ISODateToDate')($scope.FilteredDates[filterIndex].SampleDate);
                    if (tempDate != undefined && (dateToSearch.getMonth() == tempDate.getMonth())) {
                        return false;
                    }
                    filterIndex = filterIndex + 7;
                }
                return true;
            };

            $scope.currentMonthDates = function (dayToCheck) {
                if (dayToCheck.SampleDate == undefined) {
                    return false;
                }
                var selectedDate = $filter('ISODateToDate')(dayToCheck.SampleDate);
                if ($scope.CurrentDisplayMonth == selectedDate.getMonth()) {
                    return true;
                }
                return false;
            };

            $scope.FilterDates = function () {
                angular.forEach($scope.ManualTimes, function (manualTime, index) {
                    var dt = $filter('ISODateToDate')(manualTime.SampleDate);               
                    if (dt.getMonth() == $scope.CurrentDisplayMonth) {
                        $scope.FilteredDates.push(manualTime);
                        $scope.ChangeCase.WorkSchedule.ActualTimes.push(manualTime);
                        $scope.ReOpenCase.WorkSchedule.ActualTimes.push(manualTime);
                    }
                });
            };

            $scope.SetDayFromDate = function (val) {
                if (val != undefined && val != null) {
                    var blankDays = new Date(val).getDay();
                    for (var setDayCount = 0; setDayCount < blankDays; setDayCount++) {
                        $scope.FilteredDates.unshift({ "TotalMinutes": "Hide", "SampleData": "" });
                    }
                }
            };
            
            $scope.GoToMonth = function (direction) {
                var year, month;

                if (direction == 'Prev') {
                    if ($scope.CurrentDisplayMonth == 0) {  // if January
                        month = 11;
                        year = $scope.CurrentDisplayYear - 1;
                    }
                    else {
                        month = $scope.CurrentDisplayMonth - 1;
                        year = $scope.CurrentDisplayYear;
                    }
                }
                else {
                    if ($scope.CurrentDisplayMonth == 11) {  // if December
                        month = 0;
                        year = $scope.CurrentDisplayYear + 1;
                    }
                    else {
                        month = $scope.CurrentDisplayMonth + 1;
                        year = $scope.CurrentDisplayYear;
                    }
                }

                var strDate = year + "-" + ('0' + (month + 1)).slice(-2) + "-" + ('0' + (1)).slice(-2);
                //time.SampleDate = $filter('ISODateToDate')(tempDate);

                //var newDate = new Date(year, month, 1)
                $scope.GetManualDays(strDate);
            };

            //#endregion "Manual Schedule"


            //#region Certification Details
            $scope.GetCertifications = function (caseId) {
                caseService.GetCertifications($scope.CaseId).then(function (certification) {
                    $scope.Certifications = certification;
                });
            };

            $scope.SetCertificationToEdit = function (certification) {
                $scope.EditCertification = certification
                $scope.EditCertificationStartDate = $filter('ISODateToDate')(certification.StartDate);
                $scope.EditCertificationEndDate = $filter('ISODateToDate')(certification.EndDate);
                $scope.EditCertifications.Certifications = $filter('filter')($scope.Certifications.Certifications,
                    { StartDate: certification.StartDate, EndDate: certification.EndDate }, true);
            };

            $scope.AddCertificationOnCase = function () {
                var cert = {
                    StartDate: "", EndDate: "", Notes: "", Occurances: "", Frequency: "", FrequencyType: "", Duration: "", DurationType: "", FrequencyUnitType: "0", IsNew: true
                };
                $scope.CertificationStartDate = "";
                $scope.CertificationEndDate = "";
                $scope.Notes = "";
                $scope.CreateCertifications.Certifications = [];
                $scope.CreateCertifications.Certifications.push(cert);
                $scope.frmCreateCertification.$setDirty();
            }

            $scope.AddCertificationOnCreate = function () {
                $scope.frmCreateCertification.$setDirty();
                var cert = {
                    StartDate: "", EndDate: "", Notes: "", Occurances: "", Frequency: "", FrequencyType: "", Duration: "", DurationType: "", FrequencyUnitType: "0", IsNew: true
                };
                $scope.CreateCertifications.Certifications.push(cert);
            }

            $scope.AddCertificationOnEdit = function () {
                $scope.frmEditCertification.$setDirty();
                var cert = {
                    StartDate: "", EndDate: "", Notes: "", Occurances: "", Frequency: "", FrequencyType: "", Duration: "", DurationType: "", FrequencyUnitType: "0", IsNew: true
                };
                $scope.EditCertifications.Certifications.push(cert);
            }
            $scope.isCertificateInComplete = false;
            $scope.OnCertificateIncomplete = function () {
                if ($scope.isCertificateInComplete) {
                    submitted = true;
                }
                else {
                    submitted = false;
                }
            }

            $scope.SaveCreateCertification = function (IsValid, IsCreate) {
                if (!$scope.isCertificateInComplete) {

                    if (!IsValid)
                        return;
                }

                var certificationList = [];
                angular.forEach($scope.CreateCertifications.Certifications, function (certification, index) {
                    if ($scope.CertificationStartDate) { 
                        certification.StartDate = new Date($scope.CertificationStartDate).toDateString();
                    }
                    if ($scope.CertificationEndDate) {
                        certification.EndDate = new Date($scope.CertificationEndDate).toDateString();
                    }
                    certification.Notes = $scope.Notes;
                    certificationList.push(certification);
                });
                var certificationModel = { CaseId: $scope.CaseId, Certifications: certificationList, HasAnyResults: false, IsCertificateIncomplete: $scope.isCertificateInComplete };
                $scope.CreateOrModifyCertificationDetail(certificationModel);
                $("#AddCertification").modal('toggle');
                $scope.GetCertifications();
                $scope.submitted = false;
                $scope.frmCreateCertification.$setPristine();
            };

            $scope.SaveEditCertification = function (IsValid, IsCreate) {
                if (!$scope.isCertificateInComplete) {
                    if (!IsValid)
                        return;
                }

                var certificationList = [];
                angular.forEach($scope.EditCertifications.Certifications, function (certification, index) {
                    if ($scope.EditCertificationStartDate) {
                        certification.StartDate = new Date($scope.EditCertificationStartDate).toDateString();
                    }
                    if ($scope.EditCertificationEndDate) {
                        certification.EndDate = new Date($scope.EditCertificationEndDate).toDateString();
                    }
                    certification.Notes = $scope.EditCertification.Notes;
                    certificationList.push(certification);
                });
                var certificationModel = { CaseId: $scope.CaseId, Certifications: certificationList, HasAnyResults: false, IsCertificateIncomplete: $scope.isCertificateInComplete };
                $scope.CreateOrModifyCertificationDetail(certificationModel);
                $("#EditCertification").modal('toggle');
                $scope.submitted = false;
                $scope.frmEditCertification.$setPristine();
            };

            $scope.CreateOrModifyCertificationDetail = function (certificationModel) {
                caseService.CreateOrModifyCertificationDetail($scope.CaseId, certificationModel).then(function (response) {
                    $scope.GetCertifications();
                });
            };

            $scope.DeleteCertificationOnCreate = function (certification) {
                SetCertificationToDelete(certification);
                var position = $.inArray($scope.DeleteCertification, $scope.CreateCertifications.Certifications);
                if (position > -1)
                    $scope.$apply($scope.CreateCertifications.Certifications.splice(position, 1));
            };

            $scope.DeleteCertificationOnEdit = function (certification) {
                SetCertificationToDelete(certification);
                var position = $.inArray($scope.DeleteCertification, $scope.EditCertifications.Certifications);
                if (position > -1)
                    $scope.$apply($scope.EditCertifications.Certifications.splice(position, 1));
            };

            SetCertificationToDelete = function (certification) {
                $scope.DeleteCertification = certification;
                $scope.certificationId = $scope.DeleteCertification.Id;
                $scope.caseId = $scope.CaseId;
                if (angular.isDefined($scope.certificationId)) {
                    $scope.DeleteCertificationDetail($scope.caseId, $scope.certificationId);
                }
            };

            $scope.DeleteCertificationDetail = function (caseId, certificationId) {
                caseService.DeleteCertificationDetail(caseId, certificationId).then(function () {
                    $scope.GetCertifications();
                });
            };

            $scope.ShowCertificationUI = function () {
                if ($scope.CaseInfo != null && $scope.CaseInfo.Case != null) {
                    return $scope.CaseInfo.Case.HasIntermittent || $scope.CaseInfo.Case.HasCertification;
                }
                else {
                    return false;
                }
            }

            $scope.DeleteWorkRestriction = function (caseId, workRestrictionId) {
                caseService.DeleteWorkRestriction(caseId, workRestrictionId).then(function () {
                    location.reload(true);
                });
            }

            //#endregion Certification Details

            //#region Accommodations
            $scope.initAccommRequest = function () {
                window.location.href = "/Accommodations/" + $scope.CaseId + "/";
            };

            $scope.initAccommodationRequest = function (IsRequest) {
                $scope.forms.submitted = false;
                $scope.Accommodation = {
                    Id: '',
                    Type: null,
                    Description: '',
                    Duration: null,
                    StartDate: '',
                    EndDate: '',
                    Resolved: 0,
                    IsWorkRelated: false
                };

                $('#AccomodationIsWorkRelatedTrue').removeClass('active');

                if (IsRequest) {
                    $scope.AccommodationRequest = {
                        CaseId: $scope.CaseId,
                        GeneralHealthCondition: '',
                        Description: '',
                        Status: 'Open',
                        HCPContactId: null,
                        HCPCompanyName: '',
                        HCPFirstName: '',
                        HCPLastName: '',
                        HCPPhone: '',
                        HCPFax: '',
                        Accommodations: [],
                        HasAnyRequests: false,
                        ProviderContacts: [],
                    };
                }

                $scope.SetAccommodationData();

                $scope.IsRequest = IsRequest;
                $scope.AddProvider = false;
                $scope.AddStepNote = false;

                $scope.forms.ChangeStatusAccommodation = false;
                $scope.frmCreateAccommodationRequest.$setPristine();
            };

            $scope.GetAccommodationRequests = function () {
                caseService.GetAccommodationRequests($scope.CaseId).then(function (accommodationRequest) {
                    $scope.AccommodationRequest = accommodationRequest;
                    $scope.AccommodationRequestCopy = angular.copy($scope.AccommodationRequest);
                    //$scope.AccommodationRequest.AdditionalQuestions = $scope.AccomAdditionalQuestion;
                    $scope.forms.ChangeStatusAccommodation = false;
                    if ($scope.AccommodationRequest != null) {
                        $scope.HasAccommodationRequest = true;

                        // if has leave accommodation
                        var leaveAccom = $filter('filter')($scope.AccommodationRequest.Accommodations, {
                            Type: 5
                        });
                        if (leaveAccom.length > 0) {
                            $scope.ShowLeaveNoLongerNeeded = true;
                        }
                        else {
                            $scope.ShowLeaveNoLongerNeeded = false;
                        }
                    }
                    $scope.SetProvider();
                    $scope.OpenAccommodation();
                });
            };

            $scope.SetAccommodationRequestToEdit = function (accommodationRequest) {
                $scope.forms.editSubmitted = false;
                $scope.EditAccommodationRequest = accommodationRequest;
                if ($scope.EditAccommodationRequest.Status == 2)
                    $scope.ShowAccomRequestCancelReason = true;

                //$scope.EditAccommodationRequest.CancelReason = '';
                //$scope.EditAccommodationRequest.OtherReasonDesc = '';
            };

            $scope.SetAccommodationToEdit = function (accommodation, index) {
                $scope.forms.accomSubmitted = false;
                $scope.forms.cancelAccomSubmitted = false;
                $scope.AccomCancellation = {
                    IsCancel: false,
                    Reason: '',
                    OtherReasonDesc: ''
                };
                $scope.ShowAccomCancelReason = false;
                $scope.EditAccommodationIndex = index;
                accommodation.StartDate = $filter('ISODateToDate')(accommodation.StartDate);
                accommodation.EndDate = $filter('ISODateToDate')(accommodation.EndDate);
                accommodation.ResolvedDate = $filter('ISODateToDate')(accommodation.ResolvedDate);
                $scope.EditAccommodation = {};
                angular.copy(accommodation, $scope.EditAccommodation);

                $scope.NewHCP = false;
                $scope.ShowHCPSection = false;
                $scope.GetProviderContacts();
                $scope.GetAccommodationDenialReasons("Accommodation", $scope.CaseId);
                
                // Set Type
                var selAccom = $filter('filter')($scope.AccommodationTypes, {
                    Id: accommodation.Type.Id
                });
                if (selAccom.length > 0) {
                    $scope.EditAccommodation.Type = selAccom[0];
                    $scope.EditAccommType = $scope.EditAccommodation.Type;
                }


            };

            $scope.ToggleStepQuestion = function (step) {
                step.QuestionVisible = !step.QuestionVisible;
            };

            $scope.ToggleStepQuestions = function (step) {
                angular.forEach($scope.EditAccommodation.InteractiveProcessSteps, function (step, index) {
                    step.QuestionVisible = false;
                });

                step.QuestionVisible = true;
            };
            $scope.ToggleAdditionalInfoStepQuestions = function (step) {
                if ($scope.AccommodationRequest.AdditionalQuestions) {
                    angular.forEach($scope.AccommodationRequest.AdditionalQuestions, function (step, index) {
                        step.QuestionVisible = false;
                    });
                }

                step.QuestionVisible = true;
            }

            $scope.ExpandCollapseAccomIntProcQuestions = function (enable) {
                if ($scope.EditAccommodation != undefined && $scope.EditAccommodation != null
                    && $scope.EditAccommodation.InteractiveProcessGroups != undefined && $scope.EditAccommodation.InteractiveProcessGroups != null) {
                    angular.forEach($scope.EditAccommodation.InteractiveProcessGroups, function (group, index) {
                        if (group != undefined && group != null && group.InteractiveProcessSteps != undefined && group.InteractiveProcessSteps != null) {
                            angular.forEach(group.InteractiveProcessSteps, function (step, index) {
                                step.QuestionVisible = enable;
                            })
                        }
                    });
                }
            };

            $scope.ExpandCollapseAccomIntProcNotes = function (enable) {
                angular.forEach($scope.EditAccommodation.InteractiveProcessSteps, function (step, index) {
                    step.NoteVisible = enable;
                });
            };

            $scope.SetDecisionDate = function () {
                $scope.EditAccommodation.Resolved = true;
                $scope.EditAccommodation.ResolvedDate = new Date();
            }

            var jsonConvert = function (str) {
                try {
                    return JSON.parse(str);
                }
                catch (e) { }
                return str;
            }

            $scope.CreateOrModifyAccommodation = function (IsValid, IsCreate, IsRequest) {
                if (IsCreate && $scope.Accommodation != null && $scope.Accommodation.Duration == 3) {
                    return;
                }

                if (!IsValid)
                    return;

                if (IsCreate) {
                    var accommodationRequest = angular.copy($scope.AccommodationRequest);
                    accommodationRequest.CaseId = $scope.CaseId;

                    if ($scope.Accommodation) {
                        $scope.Accommodation.Type = $scope.EditAccommType;
                    }

                    //this is a new request
                    //clear the accommodations and add the new one
                    accommodationRequest.Accommodations = [];

                    if (IsRequest) {
                        accommodationRequest.Accommodations[0] = $scope.Accommodation;
                    }
                    //add the new accommodation to the list
                    else {
                        accommodationRequest.Accommodations.push($scope.Accommodation);
                    }

                    angular.forEach(accommodationRequest.Accommodations, function (value, key) {
                        value.StartDate = new Date(value.StartDate).toDateString();
                        if (value.Duration === 2) {
                            if (value.StartDate === "Invalid Date") {
                                value.StartDate = "";
                            }
                            value.EndDate = "";
                        }
                        else {
                            value.EndDate = new Date(value.EndDate).toDateString();
                        }
                    });


                    caseService.CreateOrModifyAccommodationRequest($scope.CaseId, accommodationRequest).then(function (response) {

                        if (IsRequest) {
                            $scope.CaseInfo.Case.IsAccommodation = true;
                        }
                        // refresh policy related sections
                        $scope.RefreshDetails();
                        $scope.GetCaseInfo();

                        // Workflow
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);

                        $("#AddAccommodationRequest").modal('hide');
                        $scope.AccommodationRequest = {
                            CaseId: $scope.CaseId,
                            GeneralHealthCondition: '',
                            Description: '',
                            Status: '',
                            Accommodations: [],
                        };

                        $scope.Accommodation = {
                            Id: '',
                            Type: '',
                            Description: '',
                            Duration: '',
                            StartDate: '',
                            EndDate: '',
                            Resolved: 0,
                            IsWorkRelated: false
                        };
                        $scope.GetProviderContacts();
                        $scope.submitted = false;
                        $scope.forms.submitted = false;
                        $scope.forms.editSubmitted = false;
                        $scope.forms.accomSubmitted = false;
                        $scope.forms.cancelAccomSubmitted = false;
                        $scope.frmCreateAccommodationRequest.$setPristine();
                    });
                }
                else {
                    if (IsRequest) {
                        var accommodationRequest = $scope.EditAccommodationRequest;
                        $scope.EditAccommodationRequest.Accommodations = [];
                        //angular.forEach($scope.EditAccommodationRequest.Accommodations, function (accom, index) {
                        //    accom.Status = $scope.EditAccommodationRequest.Status;
                        //    accom.CancelReason = $scope.EditAccommodationRequest.CancelReason;
                        //    accom.OtherReasonDesc = $scope.EditAccommodationRequest.OtherReasonDesc;
                        //});
                        accommodationRequest.OtherDescription = $scope.EditAccommodationRequest.OtherReasonDesc;
                    }
                    else {
                        var accommodationRequest = $scope.AccommodationRequest;
                        accommodationRequest.CaseId = $scope.CaseId;
                        if ($scope.EditAccommType != null) {
                            $scope.EditAccommodation.Type = $scope.EditAccommType;
                        }
                        var newAccommodation = $scope.EditAccommodation;

                        angular.forEach(newAccommodation.InteractiveProcessSteps, function (value, key) {
                            if (value.Order == 21) {
                                if (value.DecisionOverturned) {
                                    var decisionOverturned = new Date(value.DecisionOverturned).toDateString();
                                    if (decisionOverturned != "Invalid Date") {
                                        value.DecisionOverturned = decisionOverturned;
                                    }
                                }
                                if (value.DateOfInitialDecision) {
                                    var dateOfInitialDecision = new Date(value.DateOfInitialDecision).toDateString();
                                    if (dateOfInitialDecision != "Invalid Date") {
                                        value.DateOfInitialDecision = dateOfInitialDecision;
                                    }
                                }
                            }
                        })

                        if (newAccommodation.ResolvedDate) {
                            var resolvedDate = new Date(newAccommodation.ResolvedDate).toDateString();
                            if (resolvedDate != "Invalid Date")
                                newAccommodation.ResolvedDate = resolvedDate;
                            else
                                newAccommodation.ResolvedDate = null;
                        }
                        else
                            newAccommodation.ResolvedDate = null;

                        var date = new Date(newAccommodation.StartDate).toDateString();
                        newAccommodation.StartDate = (date == "Invalid Date" ? newAccommodation.StartDate : date);
                        if (newAccommodation.Duration == 2) {
                            //newAccommodation.StartDate = '';
                            newAccommodation.EndDate = '';
                        }
                        else {
                            newAccommodation.EndDate = (newAccommodation.EndDate == undefined || newAccommodation.EndDate == null ? null : new Date(newAccommodation.EndDate).toDateString());
                        }
                        accommodationRequest.Accommodations = [];
                        accommodationRequest.Accommodations[0] = newAccommodation;
                    }

                    caseService.CreateOrModifyAccommodationRequest($scope.CaseId, accommodationRequest).then(function (response) {
                        // refresh policy related sections
                        $scope.RefreshDetails();
                        $scope.GetCaseInfo();

                        // Workflow
                        $scope.ToDosFilter.CaseId = $scope.CaseId;
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                        $("#EditAccommodation").modal('hide');
                        $("#EditAccommodationRequest").modal('hide');
                        $scope.submitted = false;
                        $scope.forms.submitted = false;
                        $scope.forms.editSubmitted = false;
                        $scope.forms.accomSubmitted = false;
                        $scope.forms.cancelAccomSubmitted = false;
                        $scope.frmEditAccommodationRequest.$setPristine();
                    }, function () {
                        $scope.RefreshDetails();
                        $scope.GetCaseInfo();
                    });
                }                
            };

            $scope.UpdateAccommAdditionalInfo = function (isValidMainForm) {
                if (isValidMainForm) {
                    caseService.CreateOrModifyAccommodationRequest($scope.CaseId, $scope.AccommodationRequest).then(function (response) {

                        $scope.RefreshDetails();
                        $scope.GetCaseInfo();
                        $scope.GetAdministrativeContacts();
                        forms.AccomAddInfoSubmitted = false;

                        //$scope.forms.ShowAdditionalInfo = false;
                    });
                }
                else {
                    var firstInvalid = $("#frmAddInfo").find(".ng-invalid:first").find(".ng-invalid:first");
                    $('html, body').animate({
                        scrollTop: firstInvalid.offset().top
                    }, 1000);
                }

            };

            $scope.UpdateAccommodationDuration = function (duration) {
                $scope.Accommodation.Duration = duration;

                if ($scope.Accommodation.StartDate == "" || $scope.Accommodation.StartDate == null || $scope.Accommodation.StartDate == undefined) {
                    $scope.Accommodation.StartDate = $filter('ISODateToDate')($scope.CaseInfo.Case.StartDate);
                }

                if (duration == 1) {
                    $scope.Accommodation.EndDate = $filter('ISODateToDate')($scope.CaseInfo.Case.EndDate);
                }

            };

            $scope.UpdateAccommodationType = function (type) {
                $scope.Accommodation.Type = type;
            };

            $scope.UpdateAccommodationRequestStatus = function (status) {
                $scope.EditAccommodationRequest.Status = status;

                // Show hide cancel reason logic
                if (status == 2)
                    $scope.ShowAccomRequestCancelReason = true;
                else {
                    $scope.ShowAccomRequestCancelReason = false;
                    $scope.EditAccommodationRequest.CancelReason = null;
                    $scope.EditAccommodationRequest.OtherReasonDesc = '';
                }
            };

            $scope.UpdateAccommodationTypeEdit = function (type) {
                $scope.EditAccommodation.Type = type;
            };

            $scope.UpdateAccommodationDurationEdit = function (duration) {
                $scope.EditAccommodation.Duration = duration;

                if ($scope.Accommodation.StartDate == "" || $scope.Accommodation.StartDate == null || $scope.Accommodation.StartDate == undefined) {
                    $scope.Accommodation.StartDate = $filter('ISODateToDate')($scope.CaseInfo.Case.StartDate);
                }

                if (duration == 1) {
                    $scope.Accommodation.EndDate = $filter('ISODateToDate')($scope.CaseInfo.Case.EndDate);
                }
            };

            $scope.UpdateAccommodationResolvedEdit = function (yesNoFlag) {
                $scope.EditAccommodation.Resolved = yesNoFlag;
            };

            $scope.GetAdministrativeContacts = function () {
                $scope.AdministrativeContacts = [];
                $http({
                    method: 'GET',
                    url: "/Employees/" + $scope.EmployeeId + "/Contacts/List/"
                }).then(function (response) {
                    $scope.AdministrativeContacts = response.data;
                    var newContact = {
                    };
                    newContact.ContactId = -1;
                    newContact.Name = 'Add New Contact';
                    $scope.AdministrativeContacts.push(newContact);
                });
            };

            $scope.SetProvider = function () {
                if ($scope.AccommodationRequest.HCPContactId != undefined &&
                    $scope.AccommodationRequest.HCPContactId != null &&
                    $scope.AccommodationRequest.HCPContactId != '') {
                    $scope.AddProviderEdit = true;
                }
                else {
                    $scope.AddProviderEdit = false;
                }
            };

            $scope.EnableAccommodations = function () {
                $scope.initAccommodationRequest(true);
                $scope.OpenAccommodationsModal();
            };

            $scope.SetAccommodationData = function () {
                //if this is an accommodations only case
                //set the request description to be the case description
                //set the accommodation duration to be the case start / end dates
                if ($scope.CaseInfo.Case.Reason.Code == 'ACCOMM') {
                    $scope.AccommodationRequest.Description = $scope.CaseInfo.Case.Description;
                    $scope.Accommodation.StartDate = $filter('ISODateToDate')($scope.CaseInfo.Case.StartDate);
                    if ($scope.CaseInfo.Case.EndDate != '' &&
                        $scope.CaseInfo.Case.EndDate != undefined &&
                        $scope.CaseInfo.Case.EndDate != null) {
                        $scope.Accommodation.EndDate = $filter('ISODateToDate')($scope.CaseInfo.Case.EndDate);
                    }
                }
            };

            $scope.OpenAccommodation = function () {
                if ($scope.CaseInfo != null &&
                    $scope.AccommodationRequest != null) {
                    if ($scope.CaseInfo.Case.IsAccommodation &&
                        $scope.AccommodationRequest.HasAnyRequests == false) {
                        //$scope.EnableAccommodations();
                    }
                }
            };

            $scope.OpenAccommodationsModal = function () {
                $("#AddAccommodationRequest").modal('show');
            };

            $scope.ShowStepNote = function (index) {

            };

            //#endregion Accommodations

            //STD

            $scope.GoToPay = function () {
                window.location.href = "/cases/" + $scope.CaseId + "/pay";
            }

            $scope.UpdateDiagnosis = function (IsValid) {
                if (!IsValid)
                    return;


                $scope.STD.CaseId = $scope.CaseId;
                $scope.STD.PrimaryDiagnosisId = $("#primary-code").select2("val");
                $scope.STD.SecondaryDiagnosisIds = $("#secondary-code").select2("val");
                $scope.STD.DuplicateDiagnosis = $scope.STD.SecondaryDiagnosisIds.indexOf($scope.STD.PrimaryDiagnosisId) >= 0;
                $scope.STD.NoPrimaryDiagnosis = ($scope.STD.SecondaryDiagnosisIds != '' && $scope.STD.PrimaryDiagnosisId == '');

                if ($("#primary-code").select2("val") == undefined || $("#primary-code").select2("val") == null || $("#primary-code").select2("val") == '') {
                    $scope.STD.PrimaryPathId = null;
                    $scope.STD.PrimaryPathText = null;
                    $scope.STD.PrimaryPathMinDays = null;
                    $scope.STD.PrimaryPathMaxDays = null;
                    $scope.STD.PrimaryPathDaysUIText = null;
                }

                if ($scope.STD.DuplicateDiagnosis == false && $scope.STD.NoPrimaryDiagnosis == false) {
                    $http({
                        method: 'POST',
                        url: '/Cases/' + $scope.CaseId + '/UpdateDiagnosis',
                        data: $scope.STD
                    }).then(function (response) {
                        if (response.data) {

                            //Update button text
                            $scope.STD.PrimaryDiagnosis = response.data.PrimaryDiagnosis;
                            if (response.data.SecondaryDiagnosis == null || response.data.SecondaryDiagnosis == undefined || response.data.SecondaryDiagnosis == '') {
                                $scope.STD.SecondaryDiagnosis = [];
                            }

                            // Update policy section suggested period text
                            if ($scope.STD.PrimaryDiagnosis != null) {
                                if ($scope.CaseId && $scope.EmployeeId) {
                                    $scope.ComorbidityArgsRequired = false;
                                    $scope.Comorbidity.loadComorbidity($scope.CaseId, $scope.EmployeeId);
                                }
                            }
                            else {
                                $scope.Comorbidity.data = null;
                                $scope.Comorbidity.comorbidityArgs.Args = null;
                            }
                        }
                    });
                }
            };

            $scope.ResetHealthSection = function () {
                $("#disability-edit-condition").prop("disabled", false);
                $("#disability-edit-diagnosis").prop("disabled", false);
                $("#disability-comorbidity-guideline").prop("disabled", false);
                $("#health-section").show();
            }

            $scope.UpdateSTD = function (isValid) {
                if (isValid) {

                    $scope.STD.CaseId = $scope.CaseId;

                    var cDate = null;
                    if ($scope.STD.ConditionStartDate != undefined &&
                        $scope.STD.ConditionStartDate != null &&
                        $scope.STD.ConditionStartDate != '') {
                        cDate = new Date($scope.STD.ConditionStartDate).toDateString();
                        if (cDate == "Invalid Date") {
                            cDate = null;
                        };
                    }
                    $scope.STD.ConditionStartDate = cDate;

                    $http({
                        method: 'POST',
                        url: '/Cases/' + $scope.CaseId + '/UpdateDisability',
                        data: $scope.STD
                    }).then(function (response, status) {
                        $scope.ResetHealthSection();
                        $scope.frmEditSTD.$setPristine();
                        $scope.reload();
                        $("#EditSTD").modal('hide');
                    });
                }
            };

            $scope.CancelDiagnosisSearch = function () {
                $scope.forms.ChangeDiagnosis = false;

                $("#disability-edit-condition").prop("disabled", false);
                $("#disability-edit-diagnosis").prop("disabled", false);
                $("#disability-comorbidity-guideline").prop("disabled", false);
                $("#health-section").show();

                if ($scope.OriginalSTD != undefined && $scope.OriginalSTD != null && $scope.STD != undefined && $scope.STD != null) {
                    $scope.STD = $scope.OriginalSTD;
                    $scope.forms.PrimaryPathId = $scope.STD.PrimaryPathId;
                    if ($scope.OriginalRTWBestPracticeList != undefined && $scope.OriginalRTWBestPracticeList != null && $scope.OriginalRTWBestPracticeList != undefined && $scope.OriginalRTWBestPracticeList != null) {
                        $scope.RTWBestPracticeList = $scope.OriginalRTWBestPracticeList;
                        $scope.SetPrimaryReturnToWorkPath();
                    }
                }

            };


            $scope.$watch('$viewContentLoaded',
                function () {
                    $timeout(function () {
                        //do something
                    }, 0);
                });

            $scope.InitDiagnosisSearch = function () {
                $scope.forms.ChangeDiagnosis = true;
                $scope.OriginalSTD = angular.copy($scope.STD);
                $scope.OriginalRTWBestPracticeList = angular.copy($scope.RTWBestPracticeList);
                $scope.DuplicateDiagnosis = false;
                $scope.NoPrimaryDiagnosis = false;

                //ICD9 - Primary Code Lookup
                $("#primary-code").select2({
                    placeholder: "Search...",
                    allowClear: true,
                    minimumInputLength: 2,
                    dropdownCssClass: "bigdrop",
                    ajax: {
                        url: "/Cases/ICD9Search",
                        dataType: 'json',
                        data: function (term) {
                            return {
                                searchTerm: term // search term                            
                            };
                        },
                        results: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                }).on("select2-selecting", function (e) {
                    var selText = e.object.text;
                    // var data = { Id: (selText.split('-')[0]).trim() };
                    var data = {
                        employerId: $scope.CaseInfo.Case.Employee.EmployerId
                    };

                    $.ajax({
                        url: '/Cases/' + (selText.split('-')[0]).trim() + '/GuidelinesData/',
                        data: data,
                        dataType: "JSON",
                        type: "POST"
                    }).done(function (response) {
                        var guidelines = response.Guidelines;

                        // Set best practices guidelines dropdown
                        if (guidelines != null) {
                            //angular.element('#primary-code').scope().forms.ViewGuidelines = true;
                            angular.element('#primary-code').scope().STD.PrimaryDiagnosis = {
                                Id: e.object.id, Description: e.object.text
                            };
                            angular.element('#primary-code').scope().forms.WorkPathRequired = true;
                            angular.element('#primary-code').scope().RTWBestPracticeList = guidelines.RtwBestPractices;
                            angular.element('#primary-code').scope().$apply();
                        }
                        else {
                            //angular.element('#primary-code').scope().forms.ViewGuidelines = false;
                            angular.element('#primary-code').scope().STD.PrimaryDiagnosis = {
                                Id: e.object.id, Description: e.object.text
                            };
                            angular.element('#primary-code').scope().forms.WorkPathRequired = false;
                            angular.element('#primary-code').scope().RTWBestPracticeList = [];
                            angular.element('#primary-code').scope().forms.PrimaryPath = null;
                            angular.element('#primary-code').scope().$apply();
                        }

                        $scope.UpdateDiagnosis($scope.forms.frmSTDChangeDiagnosis.$valid);
                    });
                }).on("change", function (e) {
                    if ($("#primary-code").select2("val") == undefined || $("#primary-code").select2("val") == null || $("#primary-code").select2("val") == '') {
                        //angular.element('#primary-code').scope().forms.ViewGuidelines = false;
                        angular.element('#primary-code').scope().STD.PrimaryDiagnosis = null
                        angular.element('#primary-code').scope().forms.WorkPathRequired = false;
                        angular.element('#primary-code').scope().RTWBestPracticeList = [];
                        angular.element('#primary-code').scope().forms.PrimaryPath = null;
                        angular.element('#primary-code').scope().$apply();

                        if ($scope.STD.SecondaryDiagnosis == null || $scope.STD.SecondaryDiagnosis == '') {
                            $scope.UpdateDiagnosis($scope.forms.frmSTDChangeDiagnosis.$valid);
                        }
                    }
                    });

                //$scope.STD.PrimaryDiagnosisId = $("#primary-code").select2("val");
                //$scope.STD.SecondaryDiagnosisIds = $("#secondary-code").select2("val");
                //if ($scope.STD.SecondaryDiagnosisIds != null) {
                //    $scope.STD.DuplicateDiagnosis = $scope.STD.SecondaryDiagnosisIds.indexOf($scope.STD.PrimaryDiagnosisId) >= 0;
                //}                
                $("#secondary-code").select2({
                    placeholder: "Search...",
                    allowClear: true,
                    minimumInputLength: 2,
                    dropdownCssClass: "bigdrop",
                    multiple: true,
                    ajax: {
                        url: "/Cases/ICD9Search",
                        dataType: 'json',
                        data: function (term) {
                            return {
                                searchTerm: term // search term
                            };
                        },
                        results: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                }).on("select2-selecting", function (e) {
                    //var data = { Id: e.val };
                    var selText = e.object.text;
                    // var data = { Id: (selText.split('-')[0]).trim() };
                    var data = {
                    };

                    $.ajax({
                        url: '/Cases/' + e.val + '/GuidelinesDataSummary/',
                        data: data,
                        dataType: "JSON",
                        type: "POST"
                    }).done(function (response) {
                        var guidelines = response;

                        // Set best practices guidelines dropdown
                        var filtered = $filter('filter')(angular.element('#secondary-code').scope().STD.SecondaryDiagnosis, {
                            Id: e.object.id
                        });
                        if (filtered == undefined || filtered == null || filtered.length == undefined ||
                            filtered.length == null || filtered.length < 1) {
                            angular.element('#secondary-code').scope().STD.SecondaryDiagnosis.push({
                                Id: e.object.id, Description: e.object.text
                            });
                            angular.element('#secondary-code').scope().$apply();
                            $scope.UpdateDiagnosis($scope.forms.frmSTDChangeDiagnosis.$valid);
                        }
                    });
                }).on("change", function (e) {
                    if ($("#secondary-code").select2("val") == undefined || $("#secondary-code").select2("val") == null || $("#secondary-code").select2("val") == '') {
                        angular.element('#secondary-code').scope().STD.SecondaryDiagnosis = [];
                        angular.element('#secondary-code').scope().$apply();
                        if ($scope.STD.SecondaryDiagnosis == null || $scope.STD.SecondaryDiagnosis == undefined) {
                            $scope.STD.SecondaryDiagnosis = [];
                        }
                    }
                    else {
                        if (e.removed != null && e.removed != undefined) {
                            var removedObj = {
                                Id: e.removed.id, Description: e.removed.text
                            };
                            var secondaryDiagnosis = angular.element('#secondary-code').scope().STD.SecondaryDiagnosis;
                            var filtered = $filter('filter')(secondaryDiagnosis, {
                                Id: e.removed.id
                            });
                            if (filtered.length > 0) {
                                var position = $.inArray(filtered[0], secondaryDiagnosis);
                                if (~position) angular.element('#secondary-code').scope().$apply(secondaryDiagnosis.splice(position, 1));
                            }
                        }
                    }
                    $scope.UpdateDiagnosis($scope.forms.frmSTDChangeDiagnosis.$valid);
                    //$scope.STD.PrimaryDiagnosisId = $("#primary-code").select2("val");
                    //$scope.STD.SecondaryDiagnosisIds = $("#secondary-code").select2("val");
                    //if ($scope.STD.SecondaryDiagnosisIds != null) {
                    //    $scope.STD.DuplicateDiagnosis = $scope.STD.SecondaryDiagnosisIds.indexOf($scope.STD.PrimaryDiagnosisId) >= 0;
                    //}                
                    });

                if ($scope.STD.PrimaryDiagnosis != null) {
                    $("#primary-code").select2("data", {
                        id: $scope.STD.PrimaryDiagnosis.Id, text: $scope.STD.PrimaryDiagnosis.Description
                    });
                    if ($scope.CaseId && $scope.EmployeeId) {
                        $scope.ComorbidityArgsRequired = false;
                        $scope.Comorbidity.loadComorbidity($scope.CaseId, $scope.EmployeeId);
                    }
                }

                if ($scope.STD.SecondaryDiagnosis != null) {
                    var SecondaryDiagnosisDataListData = [];
                    angular.forEach($scope.STD.SecondaryDiagnosis, function (diagnosis, index) {
                        var data = new Object();
                        data.id = diagnosis.Id;
                        data.text = diagnosis.Description;
                        SecondaryDiagnosisDataListData.push(data);
                    });
                    $("#secondary-code").select2("data", SecondaryDiagnosisDataListData)
                }
            };

            $scope.ChangePath = function (primaryPath) {
                if (primaryPath) {
                    $scope.UpdateDiagnosis($scope.forms.frmSTDChangeDiagnosis.$valid);
                    $scope.STD.PrimaryPathId = primaryPath.Id;
                    $scope.forms.PrimaryPathId = primaryPath.Id;
                    $scope.STD.PrimaryPathText = primaryPath.RtwPath;
                    $scope.STD.PrimaryPathMinDays = primaryPath.MinDays;
                    $scope.STD.PrimaryPathMaxDays = primaryPath.MaxDays;
                    $scope.STD.PrimaryPathDaysUIText = primaryPath.RtwPath + ": " + primaryPath.Days;
                }
                else {
                    $scope.STD.PrimaryPathId = null;
                    $scope.forms.PrimaryPathId = null;
                    $scope.STD.PrimaryPathText = null;
                    $scope.STD.PrimaryPathMinDays = null;
                    $scope.STD.PrimaryPathMaxDays = null;
                    $scope.STD.PrimaryPathDaysUIText = null;
                }
            }

            $scope.GetPrimaryPathUIText = function (primaryPath) {
                return primaryPath.RtwPath + ": " + primaryPath.Days;
            }

            $scope.ViewSecondaryGuidelines = function (diagnosis) {
                $scope.forms.ShowReturnToWork = false;
                $scope.forms.ActivityModifications = false;
                $scope.forms.PhisicalTherapy = false;
                $scope.forms.Chiropractical = false;

                caseService.GuidelinesData(diagnosis.Description.split('-')[0].trim(), $scope.CaseInfo.Case.Employee.EmployerId).then(function (response) {
                    $scope.forms.Guidelines = response.Guidelines;
                    $("#ViewGuidelines").modal('show');
                });
            }

            $scope.ViewPrimaryGuidelines = function () {

                $scope.forms.ShowReturnToWork = false;
                $scope.forms.ActivityModifications = false;
                $scope.forms.PhisicalTherapy = false;
                $scope.forms.Chiropractical = false;

                caseService.GuidelinesData($scope.STD.PrimaryDiagnosis.Description.split('-')[0].trim(), $scope.CaseInfo.Case.Employee.EmployerId).then(function (response) {
                    $scope.forms.Guidelines = response.Guidelines;
                    $("#ViewGuidelines").modal('show');
                });
            }

            $scope.SetPrimaryReturnToWorkPath = function () {
                var path = undefined;
                var bestPracticeList = angular.element('#primary-code').scope().RTWBestPracticeList;
                for (var i = 0; i < bestPracticeList.length; i++) {
                    var bestPractice = bestPracticeList[i];
                    if (bestPractice.Id == $scope.forms.PrimaryPathId) {
                        path = bestPractice;
                        break;
                    }
                }

                if (path) {
                    $scope.forms.PrimaryPath = path;
                    $scope.ChangePath(path);
                }
            }

            $scope.GetSTDPolicyData = function () {
                // show suggested period message in policy section

                $scope.forms.ShowSuggestedPeriodMessage = false;

                var STDPolicy = [];

                if ($scope.PolicyData != null)
                    STDPolicy = $filter('filter')($scope.PolicyData.AppliedPolicies, {
                        PolicyCode: 'STD'
                    });

                if (STDPolicy.length > 0) {
                    var STDPolicyData = STDPolicy[0];

                    var pendingSTD = $filter('filter')(STDPolicyData.Adjudications, {
                        Status: "Pending"
                    });
                    if (pendingSTD.length > 0 && $scope.CaseInfo.Case.Disability.PrimaryPathId != null) {
                        $scope.forms.ShowSuggestedPeriodMessage = true;
                        $scope.forms.SuggestedStartDate = pendingSTD[0].StartDateText;

                        if ($scope.CaseInfo.Case.Disability.PrimaryPathMaxDays != null) {
                            // var dat = new Date(pendingSTD[0].StartDate);
                            //dat.setDate(dat.getDate() + parseInt($scope.CaseInfo.Case.Disability.PrimaryPathMaxDays))

                            var sDate = $filter('ISODateToDate')(pendingSTD[0].StartDate)

                            var interval = parseInt($scope.CaseInfo.Case.Disability.PrimaryPathMaxDays);
                            var dat = new Date(sDate.getFullYear(), sDate.getMonth(), sDate.getDate() + interval);
                            // $scope.forms.SuggestedEndDate = dat.toDateString();

                            // display whichever is lower
                            if (dat < new Date(pendingSTD[0].EndDate)) {
                                $scope.forms.SuggestedEndDate = dat;
                            }
                            else {
                                $scope.forms.SuggestedEndDate = pendingSTD[0].EndDateText;
                            }
                        }
                        else {
                            $scope.forms.SuggestedEndDate = pendingSTD[0].EndDateText;
                        }
                    }
                }
            }
            //End STD

            //#region "Change Case"
            $scope.InitChangeCaseObject = function () {
                $scope.ChangeCase.CaseId = $scope.CaseId;
                $scope.ChangeCaseStartDate = '';
                $scope.ChangeCase.StartDate = '';
                $scope.ChangeCaseEndDate = '';
                $scope.ChangeCase.EndDate = '';
                $scope.ChangeCase.IsStartDateNew = false;
                $scope.ChangeCase.IsEndDateNew = false;
                $scope.ChangeCase.ModifyDecisions = false;
                $scope.ChangeCase.ModifyAccommodations = false;
                $scope.ChangeCase.CaseType = 0;
                $scope.RotationDays = '';
                $scope.ChangeCase.WorkSchedule.DaysInRotation = '';
                $scope.ChangeCase.WorkSchedule = new Object();
                $scope.ChangeCase.WorkSchedule.ScheduleType = null;
                $scope.ChangeCase.WorkSchedule.Times = new Array();
                $scope.ChangeCase.WorkSchedule.ActualTimes = new Array();
                $scope.FilteredDates = new Array();
                $scope.WorkScheduleStartDate = '';
                $scope.WorkScheduleEndDate = '';
                $scope.Times = new Array();
                $scope.ManualTimes = new Array();
                $scope.ScheduleStartEnd = false;
                $scope.ScheduleWeekly = false;
                $scope.ScheduleRotating = false;
                $scope.ScheduleRotatingPerDay = false;
                $scope.ScheduleOneTime = false;
                $scope.ScheduleFteVariable = false;
                $scope.FteTimePercentage = 0;
                $scope.ChangeCase.WorkSchedule.FteWeeklyDuration = 0;
                $scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek = 0;
                $scope.ChangeCase.WorkSchedule.FteTimePercentage = 0;
                // Makes only one panel open at a time in accordion
                $scope.oneAtATime = true;

            };

            $scope.UpdateChangeCaseScheduleFteType = function (ScheduleFteType) {
                $scope.ChangeCase.WorkSchedule.FteWeeklyDuration = ScheduleFteType;
                $scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek = 0;
                $scope.FteTimePercentage = 0;
            };

            $scope.UpdateChangeCaseFteTimePercentage = function () {
                $scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek = (($scope.ChangeCase.WorkSchedule.FteTimePercentage / 100.0) * $scope.FTWeeklyWorkHours).toFixed(2);
                $('#fteHours').val($scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek.toFixed(2));
                $("#fteHours").blur();
            };

            $scope.UpdateChangeCaseScheduleFteType = function (ScheduleFteType) {
                $scope.ChangeCase.WorkSchedule.FteWeeklyDuration = ScheduleFteType;
                $scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek = 0;
                $scope.FteTimePercentage = 0;
            };

            $scope.UpdateChangeCaseFteTimePercentage = function () {
                $scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek = (($scope.ChangeCase.WorkSchedule.FteTimePercentage / 100.0) * $scope.FTWeeklyWorkHours).toFixed(2);
                $('#fteHours').val($scope.ChangeCase.WorkSchedule.FteAvgTimePerWeek);
                $("#fteHours").blur();
            };

            $scope.UpdateScheduleType = function (scheduleType) {
                $scope.ScheduleType = scheduleType;
                //populate day
                $scope.ChangeCase.WorkSchedule.Times = [];
                for (var i = 0; i < 7; i++) {
                    $scope.ChangeCase.WorkSchedule.Time = new Object();
                    $scope.ChangeCase.WorkSchedule.Time.TotalMinutes = '0h';
                    $scope.ChangeCase.WorkSchedule.Time.SampleDate = $scope.WorkScheduleStartDate;
                    $scope.ChangeCase.WorkSchedule.Times.push($scope.ChangeCase.WorkSchedule.Time);
                }
                //weekly
                if (scheduleType == 0) {
                    $scope.ChangeCase.WorkSchedule.ScheduleType = 0;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = true;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = false;

                    //
                    var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    var weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');

                    for (var i = 0; i < weeklyDays.length; i++) {
                        $(weeklyDays[i]).attr('name', 'ChangeCase.WorkSchedule.Times[' + i + '].TotalMinutes');
                    }
                }
                //rotating
                else if (scheduleType == 1) {
                    $scope.ChangeCase.WorkSchedule.ScheduleType = 1;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = true;
                    $scope.ScheduleRotatingPerDay = true;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = false;

                    var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    var weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');

                    for (var i = 0; i < weeklyDays.length; i++) {
                        $(weeklyDays[i]).attr('name', 'ChangeCase.WorkSchedule.Times[' + i + '].TotalMinutes');
                    }
                }
                //manual
                else if (scheduleType == 2) {
                    $scope.WorkScheduleStartDate = $scope.ChangeCaseStartDate;
                    $scope.ChangeCase.WorkSchedule.ScheduleType = 2;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = true;
                    $scope.ScheduleFteVariable = false;
                    $scope.$watch('WorkScheduleStartDate', function () {
                        var strISODate = $filter("date")($scope.WorkScheduleStartDate, "yyyy-MM-dd");
                        $scope.GetManualDays(strISODate);

                    });
                }
                //FTE Variable
                else if (scheduleType == 3) {
                    $scope.WorkScheduleStartDate = $scope.ChangeCaseStartDate;
                    $scope.ChangeCase.WorkSchedule.ScheduleType = 3;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = true;
                    $("#ftWeeklyWorkTime").blur();
                    $('#fteHours').val("");
                }
            };

            $scope.UpdateCaseType = function (caseType) {
                $scope.ChangeCase.CaseType = caseType;
            };

            $scope.SetOriginalCaseType = function () {
                $scope.ChangeCase.CaseType = $scope.ChangeCase.CaseTypeCopy;
            };

            $scope.UpdateCase = function (IsValid) {
                if (!IsValid)
                    return;

                if ($scope.ChangeCaseStartDate == null || $scope.ChangeCaseStartDate == undefined || $scope.ChangeCaseStartDate == '') {
                    $scope.ChangeCase.StartDate = null;
                }
                else {
                    $scope.ChangeCase.StartDate = new Date($scope.ChangeCaseStartDate).toDateString();
                }

                if ($scope.ChangeCaseEndDate == null || $scope.ChangeCaseEndDate == undefined || $scope.ChangeCaseEndDate == '') {
                    $scope.ChangeCase.EndDate = null;
                }
                else {
                    $scope.ChangeCase.EndDate = new Date($scope.ChangeCaseEndDate).toDateString();
                }
                if ($scope.RotationDays != null && $scope.RotationDays != undefined && $scope.RotationDays != '') {
                    $scope.ChangeCase.WorkSchedule.DaysInRotation = $scope.RotationDays;
                }

                var changeAdminToOtherType = false;
                var nonAdminSegmentExist = false;
                var newStartDate = $filter('ISODateToDate')($scope.ChangeCase.StartDate);
                var newEndDate = $filter('ISODateToDate')($scope.ChangeCase.EndDate);

                angular.forEach($scope.CaseInfo.Case.Segments, function (segment, index) {
                    if (segment.SegmentType == 8) {
                        var segmentStartDate = $filter('ISODateToDate')(segment.SegmentStartDate);
                        var segmentEndDate = $filter('ISODateToDate')(segment.SegmentEndDate);

                        if (newStartDate.getTime() <= segmentStartDate.getTime() && segmentEndDate.getTime() <= newEndDate.getTime()) {
                            changeAdminToOtherType = true;
                        }
                    }
                    else {
                        nonAdminSegmentExist = true;
                    }
                });

                if (changeAdminToOtherType && !nonAdminSegmentExist) {
                    frmChangeCase.IsStartDateNew.value = $scope.ChangeCase.IsStartDateNew;
                    frmChangeCase.IsEndDateNew.value = $scope.ChangeCase.IsEndDateNew;
                    frmChangeCase.ModifyAccommodations.value = $scope.ChangeCase.ModifyAccommodations;
                    frmChangeCase.ModifyDecisions.value = $scope.ChangeCase.ModifyDecisions;

                    $http.post('/Cases/' + $scope.CaseId + '/ValidateChangeCaseData', $scope.ChangeCase).then(function () {
                        $("#frmChangeCase").submit();
                    });


                    return;
                }

                $http({
                    method: 'POST',
                    url: '/Cases/' + $scope.CaseId + '/Change',
                    data: $scope.ChangeCase
                }).then(function (response, status) {
                    $scope.frmChangeCase.$setPristine();
                    $scope.submitted = false;
                    $scope.reload();

                    $("#changeCase").modal("hide");
                });
            }
            
            $scope.GetRotationDays = function () {
                if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
                    for (var i = 0; i < parseInt($scope.RotationDays); i++) {
                        $scope.Time = new Object();
                        $scope.Time.TotalMinutes = null;
                        $scope.Time.SampleDate = null;
                        $scope.Times.push($scope.Time);
                    }
                    return new Array(parseInt($scope.RotationDays));
                }
                else {
                    return null;
                }
            };

            $scope.SetCaseData = function () {
                    if ($scope.CaseInfo.Case.CaseRelapses.length > 0) {
                        var index = $scope.CaseInfo.Case.CaseRelapses.length - 1;
                        $scope.ChangeCaseStartDate = $filter('ISODateToDate')($scope.CaseInfo.Case.CaseRelapses[index].StartDate);
                        $scope.ChangeCase.CaseType = $scope.CaseInfo.Case.CaseRelapses[index].CaseType;
                    }
                    else {
                        $scope.ChangeCaseStartDate = $filter('ISODateToDate')($scope.CaseInfo.Case.StartDate);
                    }
                    $scope.ChangeCaseEndDate = $filter('ISODateToDate')($scope.CaseInfo.Case.EndDate);
                    $("#changeCase").modal('show');
                    $scope.PopulateVariableScheduleTime();
                }

            $scope.PopulateVariableScheduleTime = function () {

                caseService.GetReducedVariableSchedule($scope.ChangeCase.CaseId).then(function (varScheduleTime) {
                    if (varScheduleTime.length > 0) {
                        var start = $filter('ISODateToDate')($scope.CaseStartDate);
                        var end = $filter('ISODateToDate')($scope.CaseEndDate);
                        start = new Date(start.getFullYear(), start.getMonth(), 1);
                        end = new Date(end.getFullYear(), end.getMonth() + 1, 0);

                        $scope.ManualTimes = [];
                        $scope.AlreadyPushedMonths = [];
                        $scope.ChangeCase.WorkSchedule.ActualTimes = [];
                        $scope.ReOpenCase.WorkSchedule.ActualTimes = [];
                        $scope.FilteredDates = [];

                        var firstVarTimeDate = $filter('ISODateToDate')(varScheduleTime[0].SampleDate);
                        if (start.getTime() > firstVarTimeDate.getTime()) {
                            start = new Date(firstVarTimeDate.getFullYear(), firstVarTimeDate.getMonth(), 1);
                        }

                        $scope.AlreadyPushedMonths.push(start.getMonth());

                        var dateIndex = 0;
                        while (start <= end) {
                            var time = { TotalMinutes: '', SampleDate: '' };
                            
                            time.SampleDate = $filter('date')(new Date(start.getTime()), "yyyy-MM-dd");

                            if (dateIndex < varScheduleTime.length) {
                                var tempDate = $filter('ISODateToDate')(varScheduleTime[dateIndex].SampleDate);

                                if (start.getTime() == tempDate.getTime()) {                                    
                                    time.SampleDate = $filter('date')(tempDate, "yyyy-MM-dd");
                                    time.TotalMinutes = varScheduleTime[dateIndex].TotalMinutes;
                                    dateIndex = dateIndex + 1;
                                }
                            }

                            $scope.ManualTimes.push(time);  
                            
                            if ($.inArray(start.getMonth(), $scope.AlreadyPushedMonths) == -1) {
                                $scope.AlreadyPushedMonths.push(start.getMonth());
                            }

                            start.setDate(start.getDate() + 1);
                        }
                        $scope.UpdateScheduleType(2);
                    }
                });
            };

            //#endregion "Change Case"

            //#region "Edit Employee"
            $scope.EditEmployeeInfo = function () {
                $http({
                    method: 'POST',
                    url: '/Cases/' + $scope.CaseId + '/EmployeeInfo',
                    data: $scope.CaseInfo.Case.Employee.Info
                }).then(function (response) {

                    $scope.CaseInfo.Case.Employee.Info = response.data;

                    $("#EmployeeEdit").modal('toggle');

                    //When Submit button clicked, set to "true" and after completing the execution set to false. "submitted" is used for validation on button click.
                    $scope.submitted = false;
                    $scope.frmEditEmployee.$setPristine();

                }, function () {
                    var n = noty({
                        timeout: 10000, layout: 'topRight', type: 'error', text: response.message
                    });
                    $scope.submitted = false;
                });
            };

            //#endregion "Edit Employee"

           

            //#region "Reopen Case"

            $scope.SetOriginalReOpenCaseType = function () {
                $scope.ReOpenCase.CaseType = 1;
                $scope.ReOpenCase.IsRelapse = false;             
            };

            $scope.UpdateReOpenCaseType = function (caseType) {
                $scope.ReOpenCase.CaseType = caseType;
                if ($scope.ReOpenCase.CaseType == 4) {
                    $scope.UpdateReOpenScheduleType(0);
                }
            };

            $scope.InitReOpenCaseObject = function () {
                $scope.ReOpenCase.IsRelapse = false;               
                $scope.ReOpenCase.CaseType = 1;
                $scope.RotationDays = '';
                $scope.ReOpenCase.WorkSchedule.DaysInRotation = '';
                $scope.ReOpenCase.WorkSchedule = new Object();
                $scope.ReOpenCase.WorkSchedule.ScheduleType = 0;
                $scope.ReOpenCase.WorkSchedule.Times = new Array();
                $scope.ReOpenCase.WorkSchedule.ActualTimes = new Array();
                $scope.FilteredDates = new Array();
                $scope.WorkScheduleStartDate = '';
                $scope.WorkScheduleEndDate = '';
                $scope.Times = new Array();             
                $scope.ManualTimes = new Array();
                $scope.ScheduleStartEnd = false;
                $scope.ScheduleWeekly = false;
                $scope.ScheduleRotating = false;
                $scope.ScheduleRotatingPerDay = false;
                $scope.ScheduleFteVariable = false;
                $scope.FteTimePercentage = 0;
                $scope.ReOpenCase.WorkSchedule.FteWeeklyDuration = 0;
                $scope.ReOpenCase.WorkSchedule.FteAvgTimePerWeek = 0;
                $scope.ScheduleOneTime = false;

                $scope.oneAtATime = true;
            };

            $scope.UpdateReOpenScheduleFteType = function (ScheduleFteType) {
                $scope.ReOpenCase.WorkSchedule.FteWeeklyDuration = ScheduleFteType;
                $scope.ReOpenCase.WorkSchedule.FteAvgTimePerWeek = 0;
                $scope.FteTimePercentage = 0;
            };

            $scope.UpdateFteTimePercentage = function () {
                $scope.ReOpenCase.WorkSchedule.FteAvgTimePerWeek = (($scope.FteTimePercentage / 100.0) * $scope.FTWeeklyWorkHours).toFixed(2);
                $('#fteHoursRelapse').val($scope.ReOpenCase.WorkSchedule.FteAvgTimePerWeek.toFixed(2));
                $("#fteHoursRelapse").blur();
            };

            $scope.UpdateReOpenScheduleType = function (scheduleType) {
               
                $scope.ScheduleType = scheduleType;
                //populate day                
                $scope.ReOpenCase.WorkSchedule.Times = new Array();
                $scope.ReOpenCase.WorkSchedule.Times = [];
                for (var i = 0; i < 7; i++) {
                    $scope.ReOpenCase.WorkSchedule.Time = new Object();
                    $scope.ReOpenCase.WorkSchedule.Time.TotalMinutes = '0h';
                    $scope.ReOpenCase.WorkSchedule.Time.SampleDate = $scope.WorkScheduleStartDate;
                    $scope.ReOpenCase.WorkSchedule.Times.push($scope.ReOpenCase.WorkSchedule.Time);
                  
                }
                //weekly
                if (scheduleType == 0) {
                    $scope.ReOpenCase.WorkSchedule.ScheduleType = 0;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = true;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = false;

                    var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    var weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');

                    for (var i = 0; i < weeklyDays.length; i++) {
                        $(weeklyDays[i]).attr('name', 'ReOpenCase.WorkSchedule.Times[' + i + '].TotalMinutes');
                    }
                }
                //rotating
                else if (scheduleType == 1) {
                    $scope.ReOpenCase.WorkSchedule.ScheduleType = 1;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = true;
                    $scope.ScheduleRotatingPerDay = true;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = false;

                    rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                    weeklyDays = $('#schedule-weekly .schedule-days input');
                    weeklyDays.removeAttr('name');
                    rotatingDays.removeAttr('name');

                    for (var i = 0; i < weeklyDays.length; i++) {
                        $(weeklyDays[i]).attr('name', 'ReOpenCase.WorkSchedule.Times[' + i + '].TotalMinutes');
                    }
                }
                //manual
                else if (scheduleType == 2) {     
                    $scope.WorkScheduleStartDate = $scope.ReOpenCaseStartDate;
                    $scope.ReOpenCase.WorkSchedule.ScheduleType = 2;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = true;
                    $scope.ScheduleFteVariable = false;
                    $scope.$watch('WorkScheduleStartDate', function () {
                        var strISODate = $filter("date")($scope.WorkScheduleStartDate, "yyyy-MM-dd");
                        $scope.GetManualDays(strISODate);

                    });
                }
                //FTE Variable
                else if (scheduleType == 3) {
                    $scope.WorkScheduleStartDate = $scope.ReOpenCaseStartDate;
                    $scope.ReOpenCase.WorkSchedule.ScheduleType = 3;
                    $scope.ScheduleStartEnd = true;
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    $scope.ScheduleFteVariable = true;
                    $("#ftWeeklyWorkTime").blur();
                }
            };

            //Reopen claosed case
            $scope.ReopenCase = function () { 
                
                $("#reOpenCase").modal('show');
                $scope.PopulateVariableScheduleTime();
            }

            $scope.ReOpenClosedOrCancelledCase = function () {                
                var reopenInquiry = $scope.ReOpenCase.IsRelapse;
                $scope.CancelledOrClosedCase = false;
                if (!reopenInquiry) {
                    $http({
                        method: 'POST',
                        url: '/Cases/' + $scope.CaseId + '/Reopen',
                    }).then(function (response) {
                        $scope.CreatedLeaveOfAbsence = response.data;
                        $scope.reload();
                        $("#reOpenCase").modal("hide");
                    });
                }
            }
            

            //#endregion "Reopen Case"

            $scope.AcceptInquiryCase = function () {
                convertToCase = $window.confirm('Are you sure you want to convert this Inquiry into an Active Case?');
                if (convertToCase) {
                    $http({
                        method: 'POST',
                        url: '/Inquiries/' + $scope.CaseId + '/Accept',
                    }).then(function (response, status) {
                        window.location.href = "/Cases/" + $scope.CaseId + "/View";
                    });
                }
            }

            $scope.CloseInquiryCase = function () {
                //this code added for confirmation message!
                closeInquiry = $window.confirm('Are you sure you want to close this inquiry?');
                if (closeInquiry) {
                    $http({
                        method: 'POST',
                        url: '/Inquiries/' + $scope.CaseId + '/Close',
                    }).then(function (response, status) {
                        window.location.href = "/Employees/" + $scope.EmployeeId + "/View/";
                    });
                }
            }

            $scope.AddDays = function (date, days) {
                var result = new Date(date);
                result.setDate(date.getDate() + days);
                return result;
            };

            $scope.ReassignCase = function () {
                $scope.reassigncases.UserId = $scope.UserId;

                var caseDetail = new Object();
                caseDetail.CaseId = $scope.CaseId;
                caseDetail.ApplyStatus = true;
                $scope.reassigncases.Cases.push(caseDetail);

                caseService.ReassignCases($scope.reassigncases).then(function (response) {
                    $("#ReassignCase").modal('toggle');
                    $scope.GetCaseInfo();
                    toDoListManager.List($scope, false, $scope.ErrorHandler);
                });
            };
            $scope.ReassignCaseAssignment = function (obj) {
                $scope.reassigncaseassignee.CaseAssigneeTypeName = obj.CaseAssigneeTypeName;
                $scope.reassigncaseassignee.CaseAssigneeTypeCode = obj.CaseAssigneeTypeCode;
                $scope.reassigncaseassignee.UserId = obj.UserId;
                $scope.reassigncaseassignee.UserName = obj.UserName;
            };
            $scope.ReassignCaseAssignee = function () {
                var caseDetail = new Object();
                caseDetail.CaseId = $scope.CaseId;
                caseDetail.ApplyStatus = true;
                $scope.reassigncaseassignee.Cases = [];
                $scope.reassigncaseassignee.Cases.push(caseDetail);
                var userName = '';
                angular.forEach($scope.UsersList, function (item) {
                    if (item.Id == $scope.reassigncaseassignee.UserId) {
                        userName = item.DisplayName;
                    }
                })

                caseService.ReassignCaseAssignee($scope.reassigncaseassignee).then(function (response) {
                    if (response.Cases[0].SuppressThresholdValidation) {
                        $scope.RefreshCaseInfo();
                    }
                    else {
                        if (confirm('This will exceed the Threshold Case Assignment Count for user ' + userName)) {
                            $scope.reassigncaseassignee.Cases[0].SuppressThresholdValidation = true;
                            caseService.ReassignCaseAssignee($scope.reassigncaseassignee).then(function (response) {
                                $scope.RefreshCaseInfo();
                            });
                        }
                    }
                });
            };
            $scope.RefreshCaseInfo = function () {
                $("#ReassignCaseAssignee").modal('toggle');
                $scope.GetCaseInfo();
                toDoListManager.List($scope, false, $scope.ErrorHandler);
            }

            //#region Work Restrictions
            $scope.NewWorkRestriction = function () {
                $scope.submitted = false;
                $scope.WorkRestrictionsEdit = {
                };
                $scope.WorkRestrictionsEdit.Note = {
                };
                $scope.WorkRestrictionsEdit.Diagnosis = {
                };
                $scope.WorkRestrictions.IsEdit = false;
                $scope.WorkRestrictions.RestrictionEdit = false;
                $scope.AddProviderWorkRestriction = false;

                $scope.LimitationsList = [];
                $scope.AdditionalLimitationsList = [];
                $scope.SecondaryLimitationsList = [];

                $scope.InitDiagnosisSearchWorkRestriction();

            };

            $scope.UpdateWorkRestrictionSort = function (sortBy) {
                if ($scope.WorkRestrictionSort == sortBy) {
                    $scope.WorkRestrictionReverse = !$scope.WorkRestrictionReverse;
                } else {
                    $scope.WorkRestrictionSort = sortBy;
                    $scope.WorkRestrictionReverse = false;
                }
            };


            $scope.LimitationsChange = function () {
                var limList = $filter('filter')($scope.LimitationsList, {
                    Name: $scope.WorkRestrictionsEdit.Limitations
                });
                if (limList.length > 0)
                    var limitation = limList[0];

                if (limitation.Name == "Other")
                    $scope.WorkRestrictions.LimitationsEdit = true;
            };

            $scope.InitDiagnosisSearchWorkRestriction = function () {

                $("#wre-diagnosis-code").select2({
                    placeholder: "Search...",
                    allowClear: true,
                    minimumInputLength: 2,
                    dropdownCssClass: "bigdrop",
                    ajax: {
                        url: "/Cases/ICD9Search",
                        dataType: 'json',
                        data: function (term) {
                            return {
                                searchTerm: term // search term                            
                            };
                        },
                        results: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                })
                    .on("change", function (e) {
                        if ($("#wre-diagnosis-code").select2("val") == undefined || $("#wre-diagnosis-code").select2("val") == null || $("#wre-diagnosis-code").select2("val") == '') {
                            angular.element('#wre-diagnosis-code').scope().WorkRestrictionsEdit.Diagnosis = {
                            };
                            angular.element('#wre-diagnosis-code').scope().$apply();
                        }
                    });
            };

            $scope.AddProviderWorkRestrictionForm = function () {
                $scope.AddProviderWorkRestriction = true;
                $scope.GetProviderContactsWR();
            };

            $scope.GetProviderContactsWR = function () {
                $scope.WorkRestrictionsEdit.ProviderContacts = [];
                $http({
                    method: 'GET',
                    url: "/Employees/" + $scope.EmployeeId + "/Contacts/Medical"
                }).then(function (response) {

                    $scope.WorkRestrictionsEdit.ProviderContacts = response.data;
                    var newContact = {
                    };
                    newContact.ContactId = '-1';
                    newContact.Name = 'New Provider';
                    $scope.WorkRestrictionsEdit.ProviderContacts.push(newContact);

                });
            };

            $scope.HCPContactChangeWR = function (contactId) {
                if (contactId == null || contactId == undefined || contactId == '') {
                    $scope.NewHCPwr = false;
                }
                else {
                    $scope.NewHCPwr = true;
                    var contact = $filter('filter')($scope.WorkRestrictionsEdit.ProviderContacts, {
                        ContactId: contactId
                    });
                    if (contact.length > 0) {
                        $scope.WorkRestrictionsEdit.HCPCompanyName = contact[0].CompanyName;
                        $scope.WorkRestrictionsEdit.HCPFirstName = contact[0].FirstName;
                        $scope.WorkRestrictionsEdit.HCPLastName = contact[0].LastName;
                        $scope.WorkRestrictionsEdit.IsPrimary = contact[0].IsPrimary;
                        $scope.WorkRestrictionsEdit.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                        $scope.WorkRestrictionsEdit.HCPFax = $filter('tel')(contact[0].Fax);
                    }
                }
            }

            $scope.ShowHCPInfo = false;
            $scope.STDHCPContact = function (contactId) {
                $scope.ShowHCPInfo = true;
                var contact = $filter('filter')($scope.STD.ProviderContacts, {
                    ContactId: contactId
                });
                if (contact.length > 0) {
                    $scope.STD.HCPCompanyName = contact[0].CompanyName;
                    $scope.STD.HCPFirstName = contact[0].FirstName;
                    $scope.STD.HCPLastName = contact[0].LastName;
                    $scope.STD.IsPrimary = contact[0].IsPrimary;
                    $scope.STD.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                    $scope.STD.HCPFax = $filter('tel')(contact[0].Fax);
                }
            }

            //#endregion Work Restrictions

            //#region Edit Job Details
            $scope.EditJobDetails = function () {
                $scope.JobClassifications = lookupService.JobClassifications();

                caseService.GetJobDetails($scope.CaseId).then(function (response) {
                    $scope.EmployeeJobDetails = response;
                    if ($scope.EmployeeJobDetails.JobFunctions == null || $scope.EmployeeJobDetails.JobFunctions.length <= 0)
                        $scope.EmployeeJobDetails.JobFunctions.push({
                            Type: '', Description: ''
                        });
                    else {
                        angular.forEach($scope.EmployeeJobDetails.JobFunctions, function (jobFunc, index) {
                            if (jobFunc.Type == 69) {
                                jobFunc.Type = "Essential";
                            } else if (jobFunc.Type == 77) {
                                jobFunc.Type = "Marginal";
                            }
                        });
                    }
                });

                $("#editJobDetails").modal('show');
            }

            $scope.AddJobFunction = function () {
                $scope.EmployeeJobDetails.JobFunctions.push({
                    Type: '', Description: ''
                });
            }

            $scope.RemoveJobFunction = function (jobFunc) {
                $scope.EmployeeJobDetails.JobFunctions.splice($.inArray(jobFunc, $scope.EmployeeJobDetails.JobFunctions), 1);
            }

            $scope.UpdateJobDetails = function () {

                $("#editJobDetails").modal('hide');

                for (var i = $scope.EmployeeJobDetails.JobFunctions.length - 1; i >= 0; i--) {
                    var jobFunc = $scope.EmployeeJobDetails.JobFunctions[i];

                    if ((jobFunc.Type == null || jobFunc.Type == '' || jobFunc.Type == undefined) && (jobFunc.Description == null || jobFunc.Description == '' || jobFunc.Description == undefined)) {
                        $scope.EmployeeJobDetails.JobFunctions.splice($.inArray(jobFunc, $scope.EmployeeJobDetails.JobFunctions), 1);
                    }
                }

                $timeout(function () {
                    caseService.UpdateJobDetails($scope.CaseId, $scope.EmployeeJobDetails).then(function (response) {
                        $scope.$broadcast('ReloadEmployeeInfo');


                    }, $scope.ErrorHandler);
                }, 1000)
            }

            $scope.getJobFunctionDescription = function (val) {

                return $http.get('/Employees/' + $scope.CaseInfo.Case.Employee.EmployerId + '/GetJobFunctionDescription', {
                    params: {
                        desc: val
                    }
                }).then(function (response) {
                    return response.data;
                });
            }
            //#endregion Edit Job Details

            //mark end of ViewCaseCtrl


            //List of all TODO

            $scope.ManualTodo = {
            };

            $scope.GetTODOList = function () {
                lookupService.GetPacketTypesForTodo($scope.CaseInfo.Case.Employee.EmployerId).then(function (data) {
                    $scope.ManualTodo = {
                    };
                    $scope.forms.toDoSubmitted = false;
                    $scope.fields = Object.keys(data.Response);
                    $scope.ListOfAllTODO = data.Response;
                    $scope.showList = true;
                    $scope.showForm = false;
                    $scope.showManualTodoControls = false;
                    $("#modalToDo").modal('show');
                });
            }
            $scope.GetNotesList = function () {
                $("#modalNotes").modal('show');
                $("#ddCategoryCode option[value='']").remove();
                $("#ddCategoryCode ").prop("selectedIndex", 0);

            }
            $scope.openForm = function (TodoName) {
                $scope.ManualTodo = {
                };
                $scope.ManualTodo.ToDoItemType = TodoName;
                $scope.BindRole(TodoName);
            }

            $scope.openList = function () {
                $scope.showForm = false;
                $scope.showList = true;
                $scope.showManualTodoControls = false;
            }

            $scope.BindRole = function (TodoName) {
                caseService.GetRoles($scope.ManualTodo.ToDoItemType).then(function (data) {
                    $scope.Roles = data;
                    $scope.ManualTodo.Role = $scope.Roles[0].Key;
                    $scope.BindUser();
                    if (TodoName != "Manual") {
                        $scope.showForm = true;
                        $scope.showList = false;
                        $scope.showManualTodoControls = false;
                    }
                    else {
                        $scope.showForm = true;
                        $scope.showList = false;
                        $scope.showManualTodoControls = true;
                    }
                })
            }

            $scope.DeleteCaseNote = function (note) {
                var url = '/Cases/IntProc/Note/Delete';
                $http({ method: 'POST', url: url, data: { QuestionId: note.QuestionId, CaseId: $scope.CaseId } }).then(function (response, status) {
                    $("#InteractiveProcess").modal("hide");
                    $scope.reload();
                });
            }

        //$scope.DeleteITOR = function (note) {
        //    var url = '/Cases/IntProc/Note/Delete';
        //    $http({ method: 'POST', url: url, data: { QuestionId: note.QuestionId, CaseId: $scope.CaseId } }).success(function (response, status) {
        //        $("#InteractiveProcess").modal("hide");
        //        $scope.reload();
        //    });
        //}

            $scope.BindUser = function () {
                caseService.ToDoUserAssignmentSuggestionList({
                    CaseId: $scope.CaseId,
                    ToDoItemType: $scope.ManualTodo.ToDoItemType,
                    SortBy: "FirstName",
                    Role: $scope.ManualTodo.Role,
                }).then(function (data) {
                    $scope.Users = data.Results;
                })
                $scope.ManualTodo.AssignToId = '';
            }

            $scope.AddManualTodo = function (isValid) {
                if (isValid) {
                    $scope.ManualTodo.CaseId = $scope.CaseId;
                    if ($scope.ManualTodo.ToDoItemType == "Manual") {
                        var date = new Date($scope.ManualTodo.DueDate).toDateString();
                        $scope.ManualTodo.DueDate = date;
                        caseService.CreateManualTodo($scope.ManualTodo).then(function () {
                            $("#modalToDo").modal('hide');
                            $scope.forms.toDoSubmitted = false;
                            $scope.reload();
                        })
                    }
                    else {
                        $scope.ManualTodo.Title = $scope.ManualTodo.ToDoItemType;
                        $scope.ManualTodo.Description = $scope.ManualTodo.ToDoItemType;
                        var date = new Date($scope.ManualTodo.DueDate).toDateString();
                        $scope.ManualTodo.DueDate = date;
                        caseService.CreateManualTodo($scope.ManualTodo).then(function () {
                            $("#modalToDo").modal('hide');
                            $scope.forms.toDoSubmitted = false;
                            $scope.reload();
                        })
                    }
                }
            }

            //

            $scope.NotesWithoutQuestion = function (data) {
                if (data == null || data == undefined || data == '')
                    return '';

                var results = data.split(' - ');

                //data = data.replace(data.split(' - ')[0], '');
                //data = data.replace('- ', '');
                return "Note - " + results[results.length - 1];
            }

            $scope.SetCustomFieldsToEdit = function () {
                $scope.EditCustomFields = [];
                angular.copy($scope.CaseInfo.Case.CustomFields, $scope.EditCustomFields);
            }

            $scope.UpdateCustomFieldsValue = function () {
                if (!$scope.frmEditCustField.$invalid) {
                    var model = {
                        CustomFields: $scope.EditCustomFields
                    };
                    caseService.UpdateCustomFields($scope.CaseId, model).then(function (response) {
                        if (response.Success) {
                            $("#EditCustomFields").modal('hide');
                            $scope.GetCaseInfo();
                        }
                    });
                }
            }

            $scope.gotoTop = function () {
                $scope.top = 'top';
                // set the location.hash to the id of
                // the element you wish to scroll to.
                $location.hash('top');

                // call $anchorScroll()
                $anchorScroll();
            };

            $scope.EditAdditionalInformation = function () {
                var cPref, tPref;
                angular.forEach($scope.CaseInfo.Case.MetaData, function (value, key) {
                    if (value.Name != "EtlChangeTriggerScriptRun") {
                        if (value.Name == "ContactPreference")
                            cPref = value.Value;

                        if (value.Name == "TimePreference")
                            tPref = value.Value;
                    }
                });

                $scope.EditAdditionalInfo = {};
                if ($scope.EditAdditionalInfo) {

                    if (cPref)
                        $scope.EditAdditionalInfo.ContactPreference = cPref;
                    else
                        $scope.EditAdditionalInfo.ContactPreference = "";

                    if (tPref)
                        $scope.EditAdditionalInfo.TimePreference = tPref;
                    else
                        $scope.EditAdditionalInfo.TimePreference = "";

                    $scope.EditAdditionalInfo.SendECommunication = $scope.CaseInfo.Case.SendECommunication;
                    $scope.EditAdditionalInfo.PhoneNumber = $scope.CaseInfo.Case.PhoneNumber;
                    $scope.EditAdditionalInfo.Email = $scope.CaseInfo.Case.Email;
                    $scope.EditAdditionalInfo.Address1 = $scope.CaseInfo.Case.Address1;
                    $scope.EditAdditionalInfo.Address2 = $scope.CaseInfo.Case.Address2;
                    $scope.EditAdditionalInfo.City = $scope.CaseInfo.Case.City;
                    $scope.EditAdditionalInfo.State = $scope.CaseInfo.Case.State;
                    $scope.EditAdditionalInfo.Country = $scope.CaseInfo.Case.Country;
                    $scope.EditAdditionalInfo.PostalCode = $scope.CaseInfo.Case.PostalCode;
                    regionService.GetCountries().$promise.then(function (countries) {
                        $scope.Countries = countries;
                    });
                }
                $("#editAdditionalInfo").modal("show");
            }

            $scope.DeleteCaseEvent = function (caseEvent) {
                caseEvent.Delete = true;
            }

            $scope.AddCaseEvent = function () {
                var caseEvent = {
                    EventName: '',
                    EventDate: '',
                    Delete: false,
                    IsNew: true
                };
                $scope.CaseInfo.Case.CaseEvents.push(caseEvent);
            }

            $scope.UpdateAdditionalCaseEvents = function () {
                $scope.CaseEventsSubmitted = true;
                if ($scope.frmEditAdditionalCaseEvents.$invalid)
                    return;

                angular.forEach($scope.CaseInfo.Case.CaseEvents, function (value, key) {
                    value.EventDate = $filter('date')(value.EventDate, "MM/dd/yyyy");
                });

                var additionalCaseEvents = {
                    CaseId: $scope.CaseId,
                    Events: $scope.CaseInfo.Case.CaseEvents
                };


                caseService.UpdateAdditionalCaseEvents(additionalCaseEvents).$promise.then(function () {
                    // since the policies might have updated, we are going to go ahead and just reload the page
                    $('#editAdditionalCaseEvents').modal('hide');
                    $scope.reload();
                });
            }

            $scope.GetRegionsByCountry = function (resetState) {
                if (resetState)
                    $scope.EditAdditionalInfo.State = "";

                var country = $scope.EditAdditionalInfo.Country;;
                regionService.GetRegionNameByCountryCode(country).then(function (name) {
                    $scope.RegionName = name;
                });

                regionService.GetRegionsByCountryCode(country).then(function (regions) {
                    $scope.Regions = regions;
                });
            }

            $scope.UpdateAdditionalInfo = function () {
                if ($scope.frmEditAdditionalInfo.$invalid)
                    return;

                caseService.UpdateAdditionalInfo($scope.CaseId, $scope.EditAdditionalInfo).then(function (result) {
                    $scope.reload();
                    $('#editAdditionalInfo').modal('hide');
                })
            }

            $scope.EditCaseInfoInformation = function () {

                $scope.EditCaseInfo = {};
                if ($scope.EditCaseInfo) {
                    $scope.EditCaseInfo.Description = $scope.CaseInfo.Case.Description;
                    $scope.EditCaseInfo.ShortSummary = $scope.CaseInfo.Case.ShortSummary;
                }
                $("#editCaseInfo").modal("show");
            }

            $scope.UpdateCaseInfo = function () {
                if ($scope.frmEditCaseInfo.$invalid)
                    return;

                caseService.UpdateCaseInfo($scope.CaseId, $scope.EditCaseInfo).then(function (result) {
                    $scope.reload();
                    $('#editCaseInfo').modal('hide');
                })
            }

            $scope.SetType = function (accom) {
                if ($scope.ChangeCase && $scope.ChangeCase.AccommodationRequest) {
                    $scope.ChangeCase.AccommodationRequest.Type = accom;
                }
                if ($scope.CreateCaseModel && $scope.CreateCaseModel.AccommodationRequest) {
                    $scope.CreateCaseModel.AccommodationRequest.Type = accom;
                }
                if ($scope.Accommodation) {
                    $scope.Accommodation.Duration = accom.Duration;
                    $scope.Accommodation.Type = accom;
                    $scope.EditAccommType = accom;
                    if (accom.Duration == 0) {
                        $scope.Accommodation.Duration = 3;
                    }
                }
                if ($scope.EditAccommodation) {
                    $scope.EditAccommType = accom;
                    $scope.EditAccommodation.Type = accom;
                    if (accom.Duration == 0) {
                        $scope.EditAccommodation.Duration = 3;
                    }
                }
            };

            $scope.SetDurationBasedOnAccomType = function (accom) {
                if ($scope.Accommodation) {
                    $scope.Accommodation.Duration = accom.Duration;
                }
                if (accom.Duration == 0) {
                    if ($scope.Accommodation) {
                        $scope.Accommodation.Duration = 3;
                    }
                    if ($scope.EditAccommodation) {
                        $scope.EditAccommodation.Duration = 3;
                    }
                }
                $scope.EditAccommType = accom;
            }

            $scope.SetAccommodation = function (code) {
                for (var i = 0; i < $scope.AccommodationTypes.length; i++) {
                    var accomm = $scope.AccommodationTypes[i];
                    if (accomm.Code == code) {
                        $scope.SetType(accomm);
                        break;
                    }
                    else {
                        $scope.CheckAccommodationTypeChildren(code, accomm.Children);
                    }
                }
            };

            $scope.CheckAccommodationTypeChildren = function (accommodationCode, accommodationTypes) {
                for (var i = 0; i < accommodationTypes.length; i++) {
                    var accommType = accommodationTypes[i];
                    if (accommType.Code === accommodationCode) {
                        $scope.SetType(accommType);
                        break;
                    }
                    else {
                        $scope.CheckAccommodationTypeChildren(accommodationCode, accommType.Children);
                    }
                }
            };

            $scope.GetBlankDayFromDate = function (month) {
                var startDate = ($scope.CurrentDisplayMonth + 1).toString() + '/01/' + $scope.CurrentDisplayYear;
                var blankDays = new Date(startDate).getDay();
                return blankDays;
            };

            $scope.remove = function (array, index, policyToRemove) {
                if (array == 'MANUAL') {
                    $scope.PolicyData.ManuallyAppliedPolicies.splice(index, 1);
                }
                else if (array == 'APPLIED') {
                    $scope.PolicyData.AppliedPolicies.splice(index, 1);
                }
                //  $scope.SavePolicyDataEdits();

                caseService.DeletePolicy($scope.CaseId, { SegmentObjectId: policyToRemove.SegmentObjectId });
            }
            $scope.SetPolicyEndDate.showEdit = function (index) {
                var policy = $scope.PolicyData.AppliedPolicies[index];
                $scope.SetPolicyEndDate.PolicyCode = policy.PolicyCode;
                $scope.SetPolicyEndDate.PolicyName = policy.PolicyName;
                $scope.SetPolicyEndDate.EndDate = $filter('ISODateToDate')(policy.EndDate);
                $scope.SetPolicyEndDate.OriginalEndDate = policy.EndDate;
                $scope.SetPolicyEndDate.Show = true;
                $('#editPolicyEndDate').modal('show');
            };
            $scope.SetPolicyEndDate.setDate = function () {
                if (!$scope.SetPolicyEndDate.Show) {
                    $('#editPolicyEndDate').modal('hide');
                    return;
                }
                var dt = $scope.SetPolicyEndDate.EndDate ? new Date($scope.SetPolicyEndDate.EndDate).toDateString() : null;
                caseService.SetPolicyEndDate($scope.CaseId, $scope.SetPolicyEndDate.PolicyCode, dt).then(function () {
                    $scope.submitted = false;
                    // refresh policy related sections
                    $('#editPolicyEndDate').modal('hide');
                    $scope.SetPolicyEndDate.Show = false;
                    $scope.RefreshDetails();
                });
            };

            $scope.CalculateRiskProfile = function () {
                employeeService.CalculateRiskProfile($scope.EmployeeId, $scope.CaseId).$promise.then(function (riskProfile) {
                    var riskProfileReturned = !riskProfile.hasOwnProperty('Data') && riskProfile.Data !== false;

                    var successMessage = "Risk profile successfully calculated.";
                    if (!riskProfileReturned)
                        successMessage += " No risk profile applies to this case";

                    noty({ text: successMessage, layout: 'topRight', type: 'success', timeout: 10000 });

                    if (riskProfileReturned)
                        $scope.CaseInfo.Case.RiskProfile = riskProfile
                    else
                        $scope.CaseInfo.Case.RiskProfile = null;
                });
            }

            //Comorbidity section
            $scope.Comorbidity = {
                //set initial variable for not show co-morbidity data
                showComorbidity: false,
                comorbidityArgs: {
                    CaseId: null,
                    EmployeeId: null,
                    Args: null
                },

                data: null,

                //load comorbidity guideline for a case
                loadComorbidity: function (caseId, employeeId) {
                    //set variables
                    if ($scope.Comorbidity.comorbidityArgs.CaseId == null) {
                        $scope.Comorbidity.comorbidityArgs.CaseId = caseId;
                    }

                    if ($scope.Comorbidity.comorbidityArgs.EmployeeId == null) {
                        $scope.Comorbidity.comorbidityArgs.EmployeeId = employeeId;
                    }

                    var comorbidityData = '';
                    if ($scope.ComorbidityArgsRequired == false) {
                        comorbidityData = JSON.stringify({ "CaseId": caseId, "EmployeeId": employeeId });
                    }
                    else
                    {
                        comorbidityData = JSON.stringify($scope.Comorbidity.comorbidityArgs);
                    }

                    if ($scope.STD.PrimaryDiagnosis != null) {
                        $http.post("/Cases/CoMorbidityGuidelines", comorbidityData)
                            .then(function (response) {
                                var data = response.data;
                                //set the argument properly now
                                $scope.Comorbidity.comorbidityArgs.Args = data.Args;
                                //set the visibility
                                $scope.Comorbidity.showComorbidity = true;
                                //set data
                                $scope.Comorbidity.data = data;
                                $scope.Comorbidity.disableCF = false;
                            }), (function (data, status, headers, config) {
                                console.log(data);
                            });
                    }
                    else {
                        $scope.Comorbidity.comorbidityArgs.Args = null;
                        $scope.Comorbidity.data = null;
                    }
                },

                //Get the ConfoundingFactors class based on the selection
                getCFClass: function (key, value) {
                    if ($scope.Comorbidity.comorbidityArgs.Args != null) {
                        if ($scope.Comorbidity.comorbidityArgs.Args.CF[key] == value) {
                            return 'active';
                        }
                    }
                },

                getProgressbarClass: function () {
                    var cl = 'progress-bar progress-bar-success';
                    if ($scope.Comorbidity.data != null && $scope.Comorbidity.data.RiskAssessmentScore != null) {
                        var score = $scope.Comorbidity.data.RiskAssessmentScore;
                        if (score <= 20) {
                            cl += ' blue';
                        } else if (score > 20 && score <= 40) {
                            cl += ' green';
                        } else if (score > 40 && score <= 60) {
                            cl += ' yellow';
                        } else if (score > 60 && score <= 80) {
                            cl += ' orange';
                        } else if (score > 80) {
                            cl += ' red';
                        }
                    }

                    return cl;
                },

                //handle Opioids
                opioidsValue: 1,
                getOpioidsClass: function (value) {
                    if ($scope.Comorbidity.opioidsValue == value) {
                        return 'btn btn-xs btn-success active';
                    } else {
                        return 'btn btn-xs btn-default';
                    }
                },


                //set the ConfoundingFactors of the argument
                setCF: function (key, value) {
                    $scope.Comorbidity.comorbidityArgs.Args.CF[key] = value;
                    $scope.Comorbidity.disableCF = true;
                    $scope.Comorbidity.reCalculate();
                },

                //set the default value for show more
                showMoreOptions: false,
                showMoreButtonText: 'VIEW MORE DURATION GUIDELINES',

                //function to handle
                showMore: function () {
                    $scope.Comorbidity.showMoreOptions = !$scope.Comorbidity.showMoreOptions;
                    if ($scope.Comorbidity.showMoreOptions == true) {
                        $scope.Comorbidity.showMoreButtonText = "VIEW LESS DURATION GUIDELINES";
                        $("#odg-i-view-guidelines").removeClass("fa-chevron-down").addClass("fa-chevron-up");
                    } else {
                        $scope.Comorbidity.showMoreButtonText = "VIEW MORE DURATION GUIDELINES";
                        $("#odg-i-view-guidelines").addClass("fa-chevron-down").removeClass("fa-chevron-up");
                    }
                },

                //recalculate
                reCalculate: function () {
                    if ($scope.Comorbidity.comorbidityArgs.Args !== null) {
                        $scope.Comorbidity.comorbidityArgs.Args.ReCalculate = true;
                    }
                    $scope.ComorbidityArgsRequired = true;
                    $scope.Comorbidity.loadComorbidity();
                }
            }
            $scope.refreshWorkRelatedInfo = function (caseId) {
                caseService.GetCalculatedDaysOnJobTransferOrRestriction(caseId)
                    .then(function (result) {
                        $("#CalculatedDaysOnJobTransferOrRestriction").text(result.CalculatedDaysOnJobTransferOrRestriction);
                    },
                        $scope.ErrorHandler);

                caseService.GetCalculatedDaysAwayFromWork(caseId)
                    .then(function (result) {                        
                        $("#DaysAwayFromWorkCalculated").text(result.CalculatedGetDaysAwayFromWork);
                    },
                        $scope.ErrorHandler);
            }

            $scope.disableButton = function ($event) {
                $event.currentTarget.disabled = true;
            };

            $scope.EditCaseReporterInfoInformation = function () {

                $scope.EditCaseReporterInfo = {};
                $scope.EditCaseReporterInfo.CaseReporter = {};
                if ($scope.CaseInfo.Case.AuthorizedSubmitter !== null) {
                    $scope.EditCaseReporterInfo.AuthorizedSubmitter = {};
                    $scope.EditCaseReporterInfo.AuthorizedSubmitter.Id = $scope.CaseInfo.Case.AuthorizedSubmitter.Id;
                    $scope.AuthorizedSubmitterSelected();
                }
                else if ($scope.CaseInfo.Case.CaseReporter !== null) {
                    $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId = $scope.CaseInfo.Case.CaseReporter.Id;
                    $scope.ContactSelected();
                }
                else {
                    $scope.EditCaseReporterInfo.CaseReporter.FirstName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.LastName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.ContactTypeCode = "";
                    $scope.EditCaseReporterInfo.CaseReporter.Email = "";
                    $scope.EditCaseReporterInfo.CaseReporter.WorkPhone = "";
                }

                $("#editCaseReporterInfo").modal("show");
            }

            $scope.UpdateCaseReporterInfo = function () {
                if ($scope.frmEditCaseReporterInfo.$invalid)
                    return;

                caseService.UpdateCaseReporterInfo($scope.CaseId, $scope.EditCaseReporterInfo).then(function (result) {
                    $scope.reload();
                    $('#editCaseReporterInfo').modal('hide');
                })
            }

            $scope.GetEmployeeExistingRelationships = function () {
                if ($scope.SelectedAbsenceReason != null) {
                    if ($scope.SelectedAbsenceReason.IsForFamilyMember == true) {
                        employeeService.EmployeeExistingRelationships($scope.EmployeeId, $scope.SelectedAbsenceReason.Code).then(function (response) {
                            $scope.EmployeeExistingRelationships = response;
                            return;
                        });
                    }
                }

                $scope.EmployeeExistingRelationships = null;
            }

            $scope.GetAllEmployeeContacts = function () {
                employeeService.AllEmployeeContacts($scope.EmployeeId).then(function (response) {
                    $scope.AllEmployeeContacts = response;
                });
            }

            $scope.GetAllEmployerContacts = function () {
                employeeService.AllEmployerContacts($scope.EmployerId).then(function (response) {
                    $scope.AllEmployerContacts = response;
                });
            }

            $scope.ContactSelected = function () {
                if (!$scope.AllEmployeeContacts
                    || !$scope.EditCaseReporterInfo
                    || !$scope.EditCaseReporterInfo.CaseReporter)
                    return;

                var selectedContact = null;
                var currentCaseReporterId = $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId;
                for (var i = 0; i < $scope.AllEmployeeContacts.length; i++) {
                    var currentContact = $scope.AllEmployeeContacts[i];
                    if (currentContact.Id === currentCaseReporterId) {
                        selectedContact = currentContact;
                        break;
                    }
                }

                if (selectedContact) {
                    if ($scope.EditCaseReporterInfo.CaseReporter.AuthorizedSubmitter
                        && $scope.EditCaseReporterInfo.CaseReporter.AuthorizedSubmitter.Id
                        && $scope.EditCaseReporterInfo.CaseReporter.AuthorizedSubmitter.Id.length > 0) {
                        $scope.EditCaseReporterInfo.CaseReporter.AuthorizedSubmitter.Id = "";
                    }
                    $scope.EditCaseReporterInfo.CaseReporter.FirstName = selectedContact.FirstName;
                    $scope.EditCaseReporterInfo.CaseReporter.LastName = selectedContact.LastName;
                    $scope.EditCaseReporterInfo.CaseReporter.ContactTypeCode = selectedContact.ContactTypeCode;
                    $scope.EditCaseReporterInfo.CaseReporter.Email = selectedContact.Email;
                    $scope.EditCaseReporterInfo.CaseReporter.WorkPhone = selectedContact.WorkPhone;
                    $scope.EditCaseReporterInfo.CaseReporter.CellPhone = $filter('tel')(selectedContact.CellPhone);
                    $scope.EditCaseReporterInfo.CaseReporter.Fax = $filter('tel')(selectedContact.Fax);
                }
                else {
                    $scope.EditCaseReporterInfo.CaseReporter.FirstName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.LastName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.ContactTypeCode = "";
                    $scope.EditCaseReporterInfo.CaseReporter.Email = "";
                    $scope.EditCaseReporterInfo.CaseReporter.WorkPhone = "";
                    $scope.EditCaseReporterInfo.CaseReporter.CellPhone = "";
                    $scope.EditCaseReporterInfo.CaseReporter.Fax = "";
                }
            }

            $scope.AuthorizedSubmitterSelected = function () {
                if (!$scope.AllEmployerContacts
                    || !$scope.EditCaseReporterInfo
                    || !$scope.EditCaseReporterInfo.AuthorizedSubmitter)
                    return;

                var selectedAuthorizedSubmitter = null;
                var currentAuthorizedSubmitterId = $scope.EditCaseReporterInfo.AuthorizedSubmitter.Id;
                for (var i = 0; i < $scope.AllEmployerContacts.length; i++) {
                    var currentAuthorizedSubmitter = $scope.AllEmployerContacts[i];
                    if (currentAuthorizedSubmitter.Id === currentAuthorizedSubmitterId) {
                        selectedAuthorizedSubmitter = currentAuthorizedSubmitter;
                        break;
                    }
                }

                if (selectedAuthorizedSubmitter) {
                    if (!$scope.EditCaseReporterInfo.CaseReporter) {
                        $scope.EditCaseReporterInfo.CaseReporter = {};
                    }
                    $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId = "";
                    $scope.EditCaseReporterInfo.CaseReporter.FirstName = selectedAuthorizedSubmitter.FirstName;
                    $scope.EditCaseReporterInfo.CaseReporter.LastName = selectedAuthorizedSubmitter.LastName;
                    $scope.EditCaseReporterInfo.CaseReporter.ContactTypeCode = selectedAuthorizedSubmitter.ContactTypeCode;
                    $scope.EditCaseReporterInfo.CaseReporter.Email = selectedAuthorizedSubmitter.Email;
                    $scope.EditCaseReporterInfo.CaseReporter.WorkPhone = selectedAuthorizedSubmitter.WorkPhone;
                    $scope.EditCaseReporterInfo.CaseReporter.CellPhone = $filter('tel')(selectedAuthorizedSubmitter.CellPhone);
                    $scope.EditCaseReporterInfo.CaseReporter.Fax = $filter('tel')(selectedAuthorizedSubmitter.Fax);
                }
                else {
                    $scope.EditCaseReporterInfo.CaseReporter.FirstName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.LastName = "";
                    $scope.EditCaseReporterInfo.CaseReporter.ContactTypeCode = "";
                    $scope.EditCaseReporterInfo.CaseReporter.Email = "";
                    $scope.EditCaseReporterInfo.CaseReporter.WorkPhone = "";
                    $scope.EditCaseReporterInfo.CaseReporter.CellPhone = "";
                    $scope.EditCaseReporterInfo.CaseReporter.Fax = "";
                }
            }

            $scope.HasAuthorizedSubmitter = function () {
                return $scope.EditCaseReporterInfo
                    && $scope.EditCaseReporterInfo.AuthorizedSubmitter
                    && $scope.EditCaseReporterInfo.AuthorizedSubmitter.Id
                    && $scope.EditCaseReporterInfo.AuthorizedSubmitter.Id.length > 0;
            }

            $scope.HasExistingContactSubmitter = function () {
                var result = $scope.EditCaseReporterInfo !== undefined
                    && $scope.EditCaseReporterInfo.CaseReporter !== undefined
                    && $scope.EditCaseReporterInfo.CaseReporter != null
                    && $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId !== undefined
                    && $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId != null
                    && $scope.EditCaseReporterInfo.CaseReporter.EmployeeContactId.length > 0;
                return result;
            }

        }]);