//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('SupHrToDosCtrl', ['$scope', '$timeout', 'supHrToDosListManager', function ($scope, $timeout, supHrToDosListManager) {

    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };
    $scope.IsTPA = false;

    $scope.init = function (callingPage) {
        var infiniteScrolling = callingPage == 'index';
        supHrToDosListManager.Init($scope, infiniteScrolling);
    }

    $scope.GetToDos = function (doPaging) {
        supHrToDosListManager.Filter($scope, doPaging, $scope.ErrorHandler);
    };
    $scope.UpdateToDoSort = function (sortBy) {
        supHrToDosListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
    };
    $scope.LoadMoreToDos = function () {
        supHrToDosListManager.LoadMore($scope, $scope.ErrorHandler);
    };

}]);