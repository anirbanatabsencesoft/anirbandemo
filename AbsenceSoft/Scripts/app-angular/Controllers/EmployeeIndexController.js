//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployeeIndexCtrl', ['$scope', '$http', '$window', '$timeout', 'employeeListManager', 'employeeService', 'lookupService',
    function ($scope, $http, $window, $timeout, employeeListManager, employeeService, lookupService) {

        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.Error = response;
        };

        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };

        //#region controller initialization logic
        $scope.init = function (callingPage) {
            var infiniteScrollOnBody = callingPage == "index";
            employeeListManager.Init($scope, infiniteScrollOnBody);
            if (infiniteScrollOnBody) {
                //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                        employeeListManager.LoadMore($scope, $scope.ErrorHandler);
                    }
                });
            }

            if (callingPage != "dashboard") {
                // Get list of employer for current user
                lookupService.GetEmployersForUser().then(function (response) {
                    $scope.EmployerList = response;
                });

                //Get Feature 'MultiEmployerAccess'
                lookupService.GetEmployerFeatures().then(function (response) {
                    $scope.IsTPA = response.MultiEmployerAccess;
                });
            }

        };
        //#endregion controller initialization logic
        
        $scope.GetEmployees = function (doPaging) {
            $scope.EmployeesFilter.PageNumber = 1;
            employeeListManager.List($scope, doPaging, $scope.ErrorHandler);
        };

        var timeoutPromise;
        $scope.$watch('EmployeesFilter.Name', function (newVal, oldVal) {
            if (newVal === oldVal)
                return;

            $timeout.cancel(timeoutPromise);;
            timeoutPromise = $timeout(function () {
                employeeListManager.List($scope, false, $scope.ErrorHandler);
            }, 1000);
        });


        $scope.UpdateEmployeeSort = function (sortBy) {
            employeeListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };
        $scope.LoadMoreEmployees = function () {
            employeeListManager.LoadMore($scope, $scope.ErrorHandler);
        };

        $scope.CreateCaseValidation = function (employee, isESS) {
            $scope.currentEmployee = employee;
            employeeService.IsEmployeeInfoComplete(employee.Id).then(function (data) {
                if (data != null) {
                    if (data.IsEmployeeInfoComplete) {
                        if (isESS) {
                            window.location.href = '/#/employee-case-create/' + employee.Id;
                        }
                        else {
                            window.location.href = "/Employees/" + employee.Id + "/Cases/Create";
                        }
                        }
                    else {
                        $("#incompleteEmployeeInfoWarning").modal('show');
                    }

                }
            });
        }

        $scope.CompleteEmployeeInfo = function () {
            window.location.href = "/Employees/" + $scope.currentEmployee.Id + "/Edit";
        }

        $scope.GetViewEmployeeUrl = function (employeeId) {
            window.location.href = "/Employees/" + employeeId + "/View";
        };
        
    }]);
