//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('SelfServiceRegistrationCtrl', ['$scope', '$http', 'essService', '$window',
    function ($scope, $http, essService, $window) {

        $scope.Registration = {};
        $scope.RegistrationEdit = {};
        $scope.RegistrationComplete = false;

        $scope.forms = {};
        $scope.forms.submitted = false;
        $scope.forms.ShowSuccessMessage = false;
        $scope.IsDisabled = false;
        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.forms.submitted = false;
            $scope.Error = response;
        };
        $scope.ErrorMessages = [];

        $scope.Init = function (model) {
            $scope.RegistrationEdit = model;
            $scope.forms.submitted = false;
            if (model.Errors) {               
                $scope.ErrorMessages = model.Errors;
            }
        };
        
        $scope.SaveRegistration = function (IsValid) {
            if (IsValid) {

                //Registrtion
                $scope.IsDisabled = true;
                if ($scope.RegistrationEdit.ResetKey == null) {
                    $scope.Registration = essService.Register($scope.RegistrationEdit).then(function (response) {
                        $scope.Registration = response;
                        $scope.ErrorMessages = [];
                        if ($scope.Registration.Errors != undefined &&
                            $scope.Registration.Errors != null &&
                            $scope.Registration.Errors.length > 0)
                        {
                            angular.forEach($scope.Registration.Errors, function (error, index) {
                                //var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: error });
                                $scope.ErrorMessages.push(error);
                                $scope.IsDisabled = false;
                            });                            
                        }
                        else {
                            $scope.RegistrationComplete = true;
                        }                        
                    });
                }

                //Confirmation
                if ($scope.RegistrationEdit.ResetKey != null) {
                    if ($scope.RegistrationEdit.Password != $scope.RegistrationEdit.ConfirmNewPassword) {
                        return;
                    }

                    $scope.IsDisabled = true;
                    $scope.Registration = essService.Confirmation($scope.RegistrationEdit).then(function (response) {
                        $scope.Registration = response;
                        $scope.ErrorMessages = [];

                        if ($scope.Registration.Errors != undefined &&
                            $scope.Registration.Errors != null &&
                            $scope.Registration.Errors.length > 0) {
                            angular.forEach($scope.Registration.Errors, function (error, index) {
                                //var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: error });
                                $scope.ErrorMessages.push(error);
                                $scope.IsDisabled = false;
                            });
                        }
                        else if ($scope.Registration.error != undefined &&
                            $scope.Registration.error != null) {
                            $scope.ErrorMessages.push($scope.Registration.error.Message);
                            $scope.IsDisabled = false;
                        }
                        else {
                            //$window.location.href = "/Login/";
                            $scope.forms.ShowSuccessMessage = true;
                            $scope.IsDisabled = true;
                            $scope.RegistrationComplete = true;
                        }
                    });
                }
                
            }
        };

        $scope.reload = function () {
            window.location.reload();
        };

    }]);
