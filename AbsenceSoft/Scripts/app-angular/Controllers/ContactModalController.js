(function () {
    'use strict';
    //TODO: complete these TODO's when modifying this file
    //TODO: refactor to syntax as described in John Papa's style guide
    //TODO: encapsulate in IIFE
    //TODO: refactor to one controller per file!
    //TODO: enable strict mode and fix any violations
    angular
        .module('App.Controllers')
        .controller('ContactModalCtrl', ['$scope', '$modalInstance', 'lookupService', 'ParentScope', '$http', 'contactService', 'EmployeeId', '$filter', 'regionService', function ($scope, $modalInstance, lookupService, ParentScope, $http, contactService, EmployeeId, $filter, regionService) {
            var vm = $scope;

            angular.extend(vm, {
                EditContact: [],
                Contacts: [],
                Countries: [],
                Regions: [],
                init: init,
                form: {
                    submitted: false
                },
                resetContactViewModel: resetContactViewModel,
                FilterContacts: FilterContacts,
                GetContacts: GetContacts,
                SetContactToEdit: SetContactToEdit,
                SaveContact: SaveContact,
                GetRegionsByCountry: GetRegionsByCountry,
                SetContactToDelete: SetContactToDelete,
                DeleteContactDetail: DeleteContactDetail,
                cancel: cancel,
                ValidateContactDateOfBirth: ValidateContactDateOfBirth
            });

            activate();

            function activate() {
                vm.resetContactViewModel();
                $scope.$watch('ContactViewModel.EmployeeToCopy', onEmployeeToCopyChanged);
                $scope.$watch('ContactViewModel.ContactDateOfBirth', ValidateContactDateOfBirth);
            }



            function init() {
                vm.ContactTypes = lookupService.ContactTypes();
                vm.GetContacts();
                regionService.GetCountries().$promise.then(function (countries) {
                    $scope.Countries = countries;
                });

            }

            function ValidateContactDateOfBirth() {
                if (vm.ContactViewModel.ContactDateOfBirth == '' || vm.ContactViewModel.ContactDateOfBirth == undefined) {
                    return;
                }
                else {
                    var dateOfBirth = Date.parse(vm.ContactViewModel.ContactDateOfBirth);
                    if (isNaN(dateOfBirth)) {
                        vm.form.frmCreateContact.ContactDateOfBirth.$setValidity('valid', false);
                        vm.ContactDateOfBirthError = "Not valid date";
                    }
                    else if (dateOfBirth > new Date()) {
                        vm.form.frmCreateContact.ContactDateOfBirth.$setValidity('valid', false);
                        vm.ContactDateOfBirthError = "Date of Birth cannot be greater than today";
                    }
                    else {
                        vm.form.frmCreateContact.ContactDateOfBirth.$setValidity('valid', true);
                        vm.ContactDateOfBirthError = "";
                    }
                }
            }


            //#region "Get Contacts for current employee"

            function FilterContacts() {
                var url = '';
                if (vm.ContactViewModel.ContactTypeCode == null) {
                    vm.editContact = false;
                    url = "/Employees/" + EmployeeId + "/Contacts/List/";
                }
                else {
                    vm.editContact = true;
                    url = "/Employees/" + EmployeeId + "/Contacts/List/?contactTypeCode=" + vm.ContactViewModel.ContactTypeCode
                    vm.GetRegionsByCountry(false);
                }

                $http({
                    method: 'GET',
                    url: url
                }).then(function (response) {
                    vm.Contacts = response.data;
                });
            }

            function GetContacts() {
                $http({
                    method: 'GET',
                    url: '/Employees/' + EmployeeId + "/Contacts/CurrentUserList"
                }).then(function (response) {
                    vm.Contacts = response.data;
                });
            }

            //#endregion "Get Contacts for current employee"

            //#region Add Contact

            function onEmployeeToCopyChanged(changed) {
                if (changed && changed.value) {
                    var emp = changed.value;
                    angular.extend(vm.ContactViewModel, {
                        Id: '',
                        Title: emp.JobTitle,
                        RelatedEmployeeNumber: emp.EmployeeNumber,
                        FirstName: emp.FirstName,
                        MiddleName: emp.MiddleName,
                        LastName: emp.LastName,
                        Email: emp.Info.Email || emp.Info.AltEmail,
                        WorkPhone: emp.Info.WorkPhone,
                        HomePhone: emp.Info.HomePhone,
                        CellPhone: emp.Info.CellPhone,
                        Address: emp.Info.Address || emp.Info.AltAddress,
                        IsPrimary: false
                    });
                    vm.ContactViewModel.EmployeeToCopy = null;
                }
            }

            function resetContactViewModel() {
                vm.ContactViewModel = {
                    Id: '',
                    RelatedEmployeeNumber: '',
                    Title: '',
                    FirstName: '',
                    MiddleName: '',
                    LastName: '',
                    Email: '',
                    WorkPhone: '',
                    HomePhone: '',
                    CellPhone: '',
                    ContactTypeCode: '',
                    ContactTypeName: '',
                    Address: {
                        Address1: '',
                        City: '',
                        State: '',
                        PostalCode: '',
                        Country: 'US'
                    },
                    IsPrimary: false,
                    ContactPersonName: '',
                    EmployeeToCopy: null
                };
            }

            //#endregion Add Contact

            //#region Edit Contact
            function SetContactToEdit(contactId) {
                $http({
                    method: 'GET',
                    url: '/Employees/' + EmployeeId + '/Contacts/' + contactId
                }).then(function (response) {
                    vm.editContact = true;
                    vm.ContactViewModel = response.data;
                    if (vm.ContactViewModel.WorkPhone)
                        vm.ContactViewModel.WorkPhone = $filter("tel")(vm.ContactViewModel.WorkPhone);

                    if (vm.ContactViewModel.CellPhon)
                        vm.ContactViewModel.CellPhone = $filter("tel")(vm.ContactViewModel.CellPhone);

                    if (vm.ContactViewModel.HomePhone)
                        vm.ContactViewModel.HomePhone = $filter("tel")(vm.ContactViewModel.HomePhone);

                    GetRegionsByCountry(false);
                });
            }

            function GetRegionsByCountry(resetState) {
                if (resetState)
                    vm.ContactViewModel.Address.State = "";

                var country = vm.ContactViewModel.Address.Country;;
                regionService.GetRegionNameByCountryCode(country).then(function (name) {
                    vm.RegionName = name;
                });

                regionService.GetRegionsByCountryCode(country).then(function (regions) {
                    vm.Regions = regions;
                });
            }

            function SaveContact(isValid) {
                if (isValid) {
                    vm.form.submitted = false;
                    vm.editContact = false;
                    vm.ContactViewModel.Id = EmployeeId;
                    $http({
                        method: 'POST',
                        url: '/Employees/' + EmployeeId + '/Contact',
                        data: vm.ContactViewModel
                    }).then(function (response) {

                        $scope.$emit('EmployeeContactsChanged', true);
                        vm.resetContactViewModel();
                        vm.GetContacts();
                        vm.form.frmCreateContact.$setPristine();

                    });
                }
            }

            //#endregion Add Contact

            //#region Delete Contact

            function SetContactToDelete(contact) {

                if (confirm("Are you sure you want to delete this contact?")) {
                    vm.DeleteContact = contact
                    vm.DeleteContactDetail(contact.ContactId);
                }
            }

            function DeleteContactDetail(contactId) {
                $http({
                    method: 'POST',
                    url: '/Employees/' + contactId + '/DeleteContact'
                }).then(function (response) {
                    var position = $.inArray($scope.DeleteContact, $scope.Contacts);
                    if (~position) $scope.Contacts.splice(position, 1);
                });
            }

            //#endregion Delete Contact

            //mark end of ContactCtrl        
            function cancel() {
                $modalInstance.dismiss('cancel');
            }

        }]);
})();