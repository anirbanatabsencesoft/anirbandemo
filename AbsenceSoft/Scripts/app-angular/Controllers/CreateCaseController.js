//TODO: refactor to syntax as described in John Papa's style guide
//#region CreateCaseController

(function () {
    'use strict';

    angular
        .module('App.Controllers')
        .controller('CreateCaseCtrl', CreateCaseCtrl);

    CreateCaseCtrl.$inject = [
        '$log', '$scope', '$rootScope', '$filter', '$http', '$window', '$timeout', 'caseService', 'lookupService', 'toDoListManager',
        'toDoSuccessModalService', 'employeeService', 'contactService', 'DynamicQuestionsService', 'regionService', '$compile', '_'
    ];

    function CreateCaseCtrl(
        $log, $scope, $rootScope, $filter, $http, $window, $timeout, caseService, lookupService, toDoListManager, toDoSuccessModalService,
        employeeService, contactService, DynamicQuestionsService, regionService, $compile, _) {

        var vm = $scope;

        $rootScope.$on('EmployeeContactsChanged', function (event, data) {
            $scope.GetAllEmployeeContacts();
        });

        activate();

        function activate() {
            _.merge(vm, {
                ui: {
                    currentQuestionSet: null,
                    showContactPreferences: false,
                    ShowSummaryDurationOnly: true,
                    AddHCP: true,
                    ShowSummarizeRequest: true,
                    ShowAccommOptions: true,
                    IsAccommodation: false,
                    ShowIsWorkRelated: false
                }
            });

        }

        function FindShowAcommodationFlagInChildren(ChildObj, CodeName) {
            var x;
            var showAccoms = false;
            for (x in ChildObj) {
                if (ChildObj[x].Code == CodeName) {
                    if (ChildObj[x].IsToShowAccomodationAlways == true) {
                        showAccoms = true;
                        return showAccoms;
                    }
                }
            }
            return showAccoms;
        }

        $scope.militaryStatusChanged = function (dtVal) {
            $scope.MilitaryStatus = { Id: dtVal.Id };
        };

        // data for the override status dropdowns
        $scope.OverrideResultOptions = [{ OverrideResult: 1, Text: 'Pass' }, { OverrideResult: -1, Text: 'Fail' }, { OverrideResult: 0, Text: 'Unknown' }];

        // other lookup data
        $scope.AbsenceReasons = null;
        $scope.EmployeeRelationships = null;
        $scope.EmployeeExistingRelationships = null;
        $scope.EmployeeExistingRelationship = null;
        $scope.Relation = null;
        $scope.EmployeeRelationshipsIncludingSelf = null;
        $scope.MilitaryStatuses = null;
        $scope.SelectedAccommodationType = null;
        // the created leave of absence object after the case has been created        
        $scope.CreatedLeaveOfAbsence = null;
        $scope.SelectedAbsenceReason = null;
        $scope.CalculateEligibilityError = null;
        $scope.MilitaryStatus = null;
        $scope.EmployeeRelationship = null;
        $scope.HasCalculatedEligibility = false;
        $scope.IsWorkRelated = null;
        $scope.WillUseBonding = null;
        $scope.IsRecalculation = false;
        $scope.IsEligibilityCalculated = false;
        $scope.EmployerId = null;
        $scope.CustomFields = [];

        //disability
        $scope.IsSTD = false;
        $scope.ProviderContacts = [];
        $scope.AllEmployeeContacts = [];
        $scope.Countries = [];
        $scope.Regions = [];
        $scope.RegionName = "";

        //ada
        $scope.ShowAccomLeaveTypeMssg = false;
        $scope.AddHCP = false;
        $scope.AddHCPFunction = function () {
            $scope.AddHCP = true;
        }

        $scope.forms = {};
        $scope.forms.submitted = false;

        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };

        //for family member
        $scope.requiresRelationship = false;

        $scope.EnableRel = function (Rel) {
            $scope.forms.submitted = false;
            $scope.frmCreateCase.$setPristine();
            if ($scope.IsEligibilityCalculated) {
                $scope.resetEligibilityValues();
            }


            $scope.Relation = Rel;
            if (Rel == "NewRel") {
                // $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', false);
                $scope.showTextBox = true;
            }
            else if (Rel == "ExistingRel") {
                if ($scope.DeniedNewDuplicateRelationship == true) {
                    $scope.EmployeeNewRelationship = null;
                    $scope.EmployeeRelationshipFirstName = null;
                    $scope.EmployeeRelationshipLastName = null;
                    $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
                }
                $scope.frmCreateCase.contFirstname.$setValidity('duplicateEmployeeContact', true);
                $scope.showTextBox = false;
            }
            else {
                $scope.showTextBox = false;
            }
        };

        $scope.IsContactDuplicate = function (contact) {
            $http({
                method: 'GET',
                url: "/Case/Employee/" + $scope.CreateCaseModel.EmployeeId + "/IsContactTypeDuplicate/?contactType=" + contact.Text
            }).then(function (response) {
                if (response.data.IsDuplicate == true) {
                    $scope.EmployeeNewRelationship.$setValidity('duplicateContact', false);
                }
                else {
                    $scope.EmployeeNewRelationship.$setValidity('duplicateContact', true);
                }
            });
        };

        $scope.ScrollForm = function () {
            $("html, body").animate({ scrollTop: 500 }, 1000);
        }

        $scope.ScrollDown = function () {
            $("html, body").animate({ scrollTop: 100 }, 1000);
        }

        //#region controller initialization logic
        $scope.init = function (model, injuredBodyPartCode, whatHarmedTheEmployeeCode) {
            $scope.injuryMaxDate = Date.now();
            $scope.CreateCaseModel = model;
            $scope.InjuredBodyPartCode = injuredBodyPartCode;
            $scope.WhatHarmedTheEmployeeCode = whatHarmedTheEmployeeCode;
            $scope.CreateCaseModel.AccommodationRequest = {};
            $scope.CreateCaseModel.Disability = {};
            //Fixing AT-5627
            $scope.EditAccommodation = {};
            $scope.EditAccommodation.Type = {};

            $scope.RotationDays = '';
            $scope.CreateCase.WorkSchedule = new Object();
            $scope.CreateCase.WorkSchedule.DaysInRotation = '';
            $scope.CreateCase.WorkSchedule.ScheduleType = null;
            $scope.CreateCase.WorkSchedule.Times = new Array();
            $scope.CreateCase.WorkSchedule.ActualTimes = new Array();
            $scope.CreateCase.WorkSchedule.FTWeeklyWorkTime = model.FTWeeklyWorkTime;
            $scope.Times = new Array();
            $scope.ManualTimes = new Array();
            $scope.ScheduleStartEnd = false;
            $scope.ScheduleWeekly = false;
            $scope.ScheduleRotating = false;
            $scope.ScheduleRotatingPerDay = false;
            $scope.ScheduleOneTime = false;
            // Makes only one panel open at a time in accordion
            $scope.oneAtATime = true;
            $scope.CurrentDisplayMonth = 4;
            $scope.AlreadyPushedMonths = [];
            
            //Employer feature level flags that affect the UI
            $scope.NoFeaturesEnabled = model.NoFeaturesEnabled;
            $scope.STDFeatureEnabled = false; //model.STDFeatureEnabled;
            $scope.GuidelinesDataEnabled = true; //model.GuidelinesDataEnabled;
            $scope.ADAFeatureEnabled = model.ADAFeatureEnabled;
            $scope.LOAFeatureEnabled = model.LOAFeatureEnabled;
            $scope.STDPayFeatureEnabled = model.STDPayFeatureEnabled;
            $scope.AccommodationTypeCategoriesFeatureEnabled = model.AccommodationTypeCategoriesFeatureEnabled;
            $scope.CaseStartDate = model.StartDate;
            $scope.CaseEndDate = model.EndDate;

            $scope.CreateCaseModel.STDFeatureEnabled = false; //model.STDFeatureEnabled;
            $scope.CreateCaseModel.GuidelinesDataEnabled = true; //model.GuidelinesDataEnabled;

            // flag to show hide custom fields
            $scope.ui.ShowIsWorkRelated = model.ShowIsWorkRelated;
            $scope.ShowContactSection = model.ShowContactSection;
            $scope.ShowOtherQuestions = model.ShowOtherQuestions;

            // Set employee contact information
            $scope.CreateCaseModel.EmployeePrimaryPhone = model.EmployeePrimaryPhone != null ? $filter('tel')(model.EmployeePrimaryPhone) : null;
            $scope.CreateCaseModel.EmployeeSecondaryPhone = model.EmployeeSecondaryPhone != null ? $filter('tel')(model.EmployeeSecondaryPhone) : null;

            $scope.EmployeeId = model.EmployeeId;
            $scope.EmployerId = model.EmployerId;
            $scope.GetAllEmployeeContacts();

            lookupService.GetEmployerOfficeLocations(model.EmployerId).then(function (data) {
                $scope.EmployerOrganizations = data;
                $scope.CreateCaseModel.WorkRelatedInfo.InjuryLocation = model.EmployeeOfficeLocation;
            })

            lookupService.GetOshaFormFieldOptions(model.EmployerId, $scope.InjuredBodyPartCode).then(function (data) {
                $scope.InjuredBodyPart = data;
                $scope.CreateCaseModel.WorkRelatedInfo.InjuredBodyPart = model.InjuredBodyPart;
            })

            lookupService.GetOshaFormFieldOptions(model.EmployerId, $scope.WhatHarmedTheEmployeeCode).then(function (data) {
                $scope.WhatHarmedTheEmployee = data;
                $scope.CreateCaseModel.WorkRelatedInfo.WhatHarmedTheEmployee = model.WhatHarmedTheEmployee;
            })

            $scope.GetAllEmployerContacts();
            if ($scope.EmployerId) {
                caseService.GetEECustomFields($scope.EmployerId).then(function (response) {
                    $scope.CustomFields = response;
                });
            }

            employeeService.GetEmployeeInfo($scope.EmployeeId, null).then(function (response) {
                $scope.EmployeeInfo = response;
                if ($scope.EmployeeInfo.AltAddress.Address1 == null && $scope.EmployeeInfo.AltAddress.City == null && $scope.EmployeeInfo.AltAddress.State == null && $scope.EmployeeInfo.AltAddress.PostalCode == null) {
                    $scope.ShowAltAddress = false;
                }
                else {
                    $scope.ShowAltAddress = true;
                }

                if ($scope.EmployeeInfo.Address.Country == "US") {
                    $scope.ShowAddressStateTextBox = false;
                    $scope.ShowAddressStateDropDown = true;
                }
                else {
                    $scope.ShowAddressStateTextBox = true;
                    $scope.ShowAddressStateDropDown = false;
                }

                if ($scope.EmployeeInfo.AltAddress.Country == "US") {
                    $scope.ShowAltAddressStateTextBox = false;
                    $scope.ShowAltAddressStateDropDown = true;
                }
                else {
                    $scope.ShowAltAddressStateTextBox = true;
                    $scope.ShowAltAddressStateDropDown = false;
                }

                $scope.CreateCaseModel.NewAddress = {};
                angular.copy($scope.EmployeeInfo.Address, $scope.CreateCaseModel.NewAddress);

                $scope.CreateCaseModel.NewAltAddress = {};
                angular.copy($scope.EmployeeInfo.AltAddress, $scope.CreateCaseModel.NewAltAddress);

            });


            $scope.EmployeeRelationshipsIncludingSelf = lookupService.EmployeeRelationships(true);
            $scope.MilitaryStatuses = lookupService.MilitaryStatuses();
            toDoListManager.InitToDosFilter($scope);
            $scope.GetProviderContacts();

            /// So I'm not sure where the underlying C# method this calls went, so commenting it out for now
            /// $scope.GetProviderContactTypes(); 
            $scope.AccommodationTypes = [];

            if (!$scope.LOAFeatureEnabled) {
                $scope.CreateCaseModel.CaseTypeId = 8;
                $scope.DatesTargetConsecutive = false;
                $scope.DatesTargetIntermittent = false;
                $scope.DatesTargetReduced = false;
                $scope.DatesTargetAdministrative = true;
                $scope.CaseDatesInputGroup = true;
                $scope.AbsenceReasons = null;
                $scope.SelectedAbsenceReason = null;
                $scope.SelectedAbsenceReasonCategory = null;
                $scope.ui.IsAccommodation = false;
                $scope.IsSTD = false;
                $scope.AbsenceReasons = lookupService.AbsenceReasons($scope.EmployeeId, $scope.CreateCaseModel.CaseTypeId);
            }

            $scope.$watch('CaseEndDate', function () {
                if ($scope.IsEligibilityCalculated) {
                    $scope.resetEligibilityValues();
                }
                $scope.injuryMaxDate = Date.parse($scope.CaseEndDate) < Date.now() ? Date.parse($scope.CaseEndDate) : Date.now();         
            });

            $scope.$watch('CaseStartDate', function () {
                if ($scope.IsEligibilityCalculated) {
                    $scope.resetEligibilityValues();
                }               
            });

            $scope.$watch('CreateCaseModel.ExpectedDeliveryDate', function () {
                if ($scope.SelectedAbsenceReason != null) {
                    if ($scope.SelectedAbsenceReason.Code == 'PREGMAT') {
                        if ($scope.IsEligibilityCalculated) {
                            $scope.resetEligibilityValues();
                        }
                    }
                }
            });

            $scope.$watch('CreateCaseModel.BondingStartDate', function () {
                if ($scope.IsEligibilityCalculated) {
                    $scope.resetEligibilityValues();
                }
            });

            $scope.$watch('CreateCaseModel.BondingEndDate', function () {
                if ($scope.IsEligibilityCalculated) {
                    $scope.resetEligibilityValues();
                }
            });

            $scope.$watch('CreateCaseModel.ActualDeliveryDate', function () {
                if ($scope.SelectedAbsenceReason != null) {
                    if ($scope.SelectedAbsenceReason.Code == 'PREGMAT') {
                        if ($scope.IsEligibilityCalculated) {
                            $scope.resetEligibilityValues();
                        }
                    }
                }
            });

            $scope.$watch('CreateCaseModel.AdoptionDate', function () {
                if ($scope.SelectedAbsenceReason != null) {
                    if ($scope.SelectedAbsenceReason.Code == 'ADOPT') {
                        if ($scope.IsEligibilityCalculated) {
                            $scope.resetEligibilityValues();
                        }
                    }
                }
            });

            $scope.$watch('CustomFields', function () {
                if ($scope.IsEligibilityCalculated) {
                    $scope.resetEligibilityValues();
                }
            }, true);

            regionService.GetCountries().$promise.then(function (countries) {
                $scope.Countries = countries;
            })

            $scope.SetCaseStatus(model.Status);
        };

        $scope.getText = function (caseNumber, reason) {
            return caseNumber + ' (' + reason + ')';
        }

        $scope.SetSpouseCaseNumber = function () {
            $scope.CreateCaseModel.SpouseCaseNumber = $scope.SelectedSpouseCase.CaseNumber;
            $scope.CreateCaseModel.SpouseCaseId = $scope.SelectedSpouseCase.CaseId;
        }

        $scope.GetProviderContacts = function () {
            $scope.ProviderContacts = [];
            $http({
                method: 'GET',
                url: "/Employees/" + $scope.EmployeeId + "/Contacts/Medical"
            }).then(function (response) {
                $scope.ProviderContacts = response.data;
                var newContact = {};
                newContact.ContactId = '-1';
                newContact.Name = 'New Provider';
                $scope.ProviderContacts.push(newContact);

                // If already selected
                $scope.HCPContactChange($scope.CreateCaseModel.Disability.HCPContactId);
                $scope.HCPContactChangeAccom($scope.CreateCaseModel.Disability.HCPContactId);
            });
        };

        $scope.HCPContactChange = function (contactId) {
            if (contactId == null || contactId == undefined || contactId == '') {
                $scope.NewHCP = false;
            }
            else {
                $scope.NewHCP = true;
                var contact = $filter('filter')($scope.ProviderContacts, { ContactId: contactId });
                if (contact.length > 0) {
                    $scope.CreateCaseModel.Disability.HCPCompanyName = contact[0].CompanyName;
                    $scope.CreateCaseModel.Disability.HCPFirstName = contact[0].FirstName;
                    $scope.CreateCaseModel.Disability.HCPLastName = contact[0].LastName;
                    $scope.CreateCaseModel.Disability.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                    $scope.CreateCaseModel.Disability.HCPFax = $filter('tel')(contact[0].Fax);
                }
            }
        }

        $scope.HCPContactChangeAccom = function (contactId) {
            if (contactId == null || contactId == undefined || contactId == '') {
                $scope.NewHCP = false;
            }
            else {
                $scope.NewHCP = true;
                var contact = $filter('filter')($scope.ProviderContacts, { ContactId: contactId });
                if (contact.length > 0) {
                    $scope.CreateCaseModel.AccommodationRequest.HCPCompanyName = contact[0].CompanyName;
                    $scope.CreateCaseModel.AccommodationRequest.HCPFirstName = contact[0].FirstName;
                    $scope.CreateCaseModel.AccommodationRequest.HCPLastName = contact[0].LastName;
                    $scope.CreateCaseModel.AccommodationRequest.HCPPhone = $filter('tel')(contact[0].WorkPhone);
                    $scope.CreateCaseModel.AccommodationRequest.HCPFax = $filter('tel')(contact[0].Fax);
                }
                $("html, body").animate({ scrollTop: 1000 }, 1000);
            }
        }

        $scope.HCPContactTypeChangeAccom = function (contactType) {
            if (contactType != null && contactType != undefined && contactType != '') {
                var contactType = $filter('filter')($scope.ProviderContactTypes, { Code: contactType });
                if (contactType.length > 0) {
                    $scope.CreateCaseModel.AccommodationRequest.HCPContactTypeCode = contactType[0].Code;
                }
            }
        }

        $scope.GetProviderContactTypes = function () {
            $scope.ProviderContactTypes = [];
            $http({
                method: 'GET',
                url: "/Employees/" + $scope.EmployeeId + "/ContactTypes/Medical"
            }).then(function (response) {
                $scope.ProviderContactTypes = response.data;
                // If already selected
                $scope.HCPContactTypeChange($scope.CreateCaseModel.Disability.HCPContactTypeCode);
            });
        };

        $scope.HCPContactTypeChange = function (contactType) {
            if (contactType != null && contactType != undefined && contactType != '') {
                var contactType = $filter('filter')($scope.ProviderContactTypes, { Code: contactType });
                if (contactType.length > 0) {
                    $scope.CreateCaseModel.Disability.HCPContactTypeCode = contactType[0].Code;
                }
            }
        }

        $scope.filterOnlyAbsenceReasonsWithChildren = function (absenceReason) {
            // Do some tests

            if (absenceReason.Children != null
                && absenceReason.Children.length > 0) {
                return true; // this will be listed in the results
            }

            return false; // otherwise it won't be within the results
        };

        $scope.initOverrideResultOption = function (rule) {
            if (rule.Overridden) {
                if (rule.OverrideResult == -1) {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[1];
                }
                else if (rule.OverrideResult == 1) {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[0];
                }
                else {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[2];
                }
            }
            else {
                if (rule.Result == -1) {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[1];
                }
                else if (rule.Result == 1) {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[0];
                }
                else {
                    rule.OverrideResultOption = $scope.OverrideResultOptions[2];
                }
            }
        };

        $scope.IsRecal = function () {
            $scope.forms.submitted = false;
            if ($scope.frmCreateCase) {
                $scope.frmCreateCase.$setPristine();
                if ($scope.frmCreateCase.EmployeeNewRelationship) {
                    $scope.frmCreateCase.EmployeeNewRelationship.$dirty = true;
                    $scope.frmCreateCase.EmployeeNewRelationship.$pristine = false;
                    $scope.EmployeeNewRelationship = $scope.frmCreateCase.EmployeeNewRelationship.$modelValue;
                    $scope.EmployeeRelationshipFirstName = $scope.frmCreateCase.contFirstname.$modelValue;
                    $scope.EmployeeRelationshipLastName = $scope.frmCreateCase.contLastname.$modelValue;
                    $scope.EmployeeRelationshipDateOfBirth = $scope.frmCreateCase.NewCaseDOB.$modelValue;
                }
                if ($scope.frmCreateCase.EmployeeExistingRelationship) {
                    $scope.EmployeeExistingRelationship = $scope.frmCreateCase.EmployeeExistingRelationship.$modelValue;
                }
            }
            if ($scope.IsEligibilityCalculated) {
                $scope.resetEligibilityValues();
            }
        }

        $scope.onClickAbsenceReason = function (absenceReason) {

            //set the family flag first
            $scope.requiresRelationship = absenceReason.IsForFamilyMember;

            $scope.forms.submitted = false;
            $scope.frmCreateCase.$setPristine();

            var ShowAccoms = FindShowAcommodationFlagInChildren($scope.AbsenceReasons[0].Children, absenceReason.Code)
            if (ShowAccoms && $scope.ui.IsAccommodation)
                $("html, body").animate({ scrollTop: 800 }, 1000);
            else
                $("html, body").animate({ scrollTop: $(document).height() - 100 }, 1000);


            if ($scope.IsEligibilityCalculated) {
                $scope.resetEligibilityValues();
            }

            if ($scope.DeniedNewDuplicateRelationship == true) {
                $scope.EmployeeNewRelationship = null;
                $scope.EmployeeRelationshipFirstName = null;
                $scope.EmployeeRelationshipLastName = null;
                $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
            }

            if ($scope.frmCreateCase.contFirstname) {
                $scope.frmCreateCase.contFirstname.$setValidity('duplicateEmployeeContact', true);
            }

            $scope.IsRequired = false;
            if (absenceReason.IsCategory) {
                $scope.SelectedAbsenceReason = null;
                $scope.SelectedAbsenceReasonCategory = absenceReason;
                $scope.IsRequired = true;
            }
            else if (absenceReason.IsChild) {
                $scope.SelectedAbsenceReason = absenceReason;
                $scope.selectedReasonChild = absenceReason.Id;
            }
            else {
                $scope.SelectedAbsenceReason = absenceReason;
                $scope.SelectedAbsenceReasonCategory = null;
            }
            $scope.GetEmployeeContactTypes();
            $scope.GetEmployeeExistingRelationships();

            var ShowAccoms = FindShowAcommodationFlagInChildren($scope.AbsenceReasons[0].Children, absenceReason.Code)
            if (ShowAccoms) {
                //#862

                lookupService.AccommodationTypes($scope.EmployerId, $scope.CreateCaseModel.CaseTypeId).$promise.then(function (accommTypes) {
                    $scope.AccommodationTypes = accommTypes;
                });

                $scope.CreateCaseModel.AccommodationRequest.Type = null;
                $scope.CreateCaseModel.AccommodationRequest.Duration = null;
                if (($scope.CreateCaseModel.CaseTypeId == 1 || $scope.CreateCaseModel.CaseTypeId == 2) && ShowAccoms) {
                    $scope.ui.ShowAccommOptions = false;
                }
                else {
                    $scope.ui.ShowAccommOptions = true;
                }
                $scope.ui.IsAccommodation = true;
                $scope.CreateCaseModel.IsAccommodation = true;
                $scope.CreateCaseModel.AccommodationRequest.Description = $scope.CreateCaseModel.ShortDescription;
                $scope.CreateCaseModel.AccommodationRequest.StartDate = $filter('ISODateToDate')($scope.CreateCaseModel.StartDate);
                $scope.CreateCaseModel.AccommodationRequest.EndDate = $filter('ISODateToDate')($scope.CreateCaseModel.EndDate);
                $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated = false;
            }
            else {
                $scope.CreateCaseModel.AccommodationRequest.Type = null;
                $scope.CreateCaseModel.AccommodationRequest.Duration = null;
                $scope.ui.ShowAccommOptions = false;
                $scope.ui.IsAccommodation = false;
                $scope.CreateCaseModel.IsAccommodation = false;
                $scope.CreateCaseModel.AccommodationRequest.Description = $scope.CreateCaseModel.ShortDescription;
                $scope.CreateCaseModel.AccommodationRequest.StartDate = $filter('ISODateToDate')($scope.CreateCaseModel.StartDate);
                $scope.CreateCaseModel.AccommodationRequest.EndDate = $filter('ISODateToDate')($scope.CreateCaseModel.EndDate);
                $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated = false;
            }
            if ($scope.ShowOtherQuestions && ($scope.ui.IsAccommodation || $scope.HasCalculatedEligibility)) {
                $scope.CreateCaseModel.Job = new Object();
                $scope.CreateCaseModel.Job.Status = new Object();
                $scope.InitializeJob();
            }
            var questionContext = getDynamicQuestionContext();
            DynamicQuestionsService.resolveUiFlags(questionContext, vm.ui);
            vm.ui.currentQuestionSet = DynamicQuestionsService.resolveQuestionSet(questionContext);
            vm.ui.showContactPreferences = vm.ui.currentQuestionSet && vm.ui.currentQuestionSet.length > 0;
        };

        $scope.SetAccommodation = function (accommodationCode) {
            for (var i = 0; i < $scope.AccommodationTypes.length; i++) {
                var accommType = $scope.AccommodationTypes[i];
                if (accommType.Code === accommodationCode) {
                    $scope.SetType(accommType);
                    break;
                }
                else {
                    $scope.CheckAccommodationTypeChildren(accommodationCode, accommType.Children);
                }
            }
        };

        $scope.CheckAccommodationTypeChildren = function (accommodationCode, accommodationTypes) {
            for (var i = 0; i < accommodationTypes.length; i++) {
                var accommType = accommodationTypes[i];
                if (accommType.Code === accommodationCode) {
                    $scope.SetType(accommType);
                    break;
                }
                else {
                    $scope.CheckAccommodationTypeChildren(accommodationCode, accommType.Children);
                }
            }
        };

        $scope.GetBlankDayFromDate = function (month) {
                var startDate = ($scope.CurrentDisplayMonth + 1).toString() + '/01/' + $scope.CurrentDisplayYear;
                var blankDays = new Date(startDate).getDay();
                return blankDays;           
        };

        $scope.InitializeJob = function () {
            window.setTimeout(function () {
                var jobStartDate = $('#Job_StartDate');
                var jobEndDate = $('#Job_EndDate');
                $scope.InitializeElementNgModel($('.job-code'), 'CreateCaseModel.Job.Code');
                $scope.InitializeElementNgModel($('#Job_Title'), 'CreateCaseModel.Job.Title');
                $scope.InitializeElementNgModel(jobStartDate, 'CreateCaseModel.Job.StartDate');
                $scope.InitializeElementNgModel(jobEndDate, 'CreateCaseModel.Job.EndDate');
                angular.forEach($('input[id=Job_Status_Status]'), function (radio) {
                    radio.parentElement.setAttribute('ng-click', "CreateCaseModel.Job.Status.Status='" + radio.getAttribute('value') + "'");
                    $compile(radio.parentElement)($scope);
                });
                angular.forEach($('.job-activity-level'), function (radio) {
                    radio.parentElement.setAttribute('ng-click', "CreateCaseModel.Job.Activity='" + radio.getAttribute('value') + "'");
                    $compile(radio.parentElement)($scope);
                });
                $scope.InitializeElementNgModel($('#Job_Status_DenialReason'), 'CreateCaseModel.Job.Status.DenialReason');
                $scope.InitializeElementNgModel($('#Job_Status_DenialReasonOther'), 'CreateCaseModel.Job.Status.DenialReasonOther');
                if (AbsenceSoft.EmployeeJob.Init) {
                    AbsenceSoft.EmployeeJob.Init();
                }

                jobStartDate.datepicker({ autoclose: true });
                jobStartDate.next().children().on('click', function () {
                    jobStartDate.datepicker('show');
                });

                jobEndDate.datepicker({ autoclose: true });
                jobEndDate.next().children().on('click', function () {
                    jobEndDate.datepicker('show');
                });

            }, 100);
        }

        $scope.InitializeElementNgModel = function (element, model) {
            if (element != undefined && element != null && element.length > 0 && (element.attr('ng-model') == undefined
                || element.attr('ng-model') == null || element.attr('ng-model') == false)) {
                for (var i = 0; i < element.length; i++) {
                    var specificElement = $(element[i]);
                    specificElement.attr('ng-model', model);
                    $compile(specificElement)($scope);
                }
            }
        }

        $scope.GetEmployeeContactTypes = function () {
            if ($scope.SelectedAbsenceReason != null) {
                $scope.EmployeeRelationships = caseService.EmployeeContactTypes($scope.SelectedAbsenceReason.Code);
                return;
            }
            $scope.EmployeeRelationships = null;
        }

        $scope.SetCaseStatus = function (status) {
            if ($scope.CreateCaseModel.Status !== status) {
                $scope.CreateCaseModel.Status = status;
            }

            if ($scope.CreateCaseModel.CaseTypeId != "" && $scope.CreateCaseModel.CaseTypeId != null && $scope.CreateCaseModel.CaseTypeId != undefined) {
                switch (parseInt($scope.CreateCaseModel.CaseTypeId)) {
                    case 1:
                        $scope.OnConsecutiveClick();
                        break;
                    case 2:
                        $scope.OnIntermittentClick();
                        break;
                    case 4:
                        $scope.OnReducedClick();
                        break;
                    case 8:
                        $scope.OnAdministrativeClick();
                        break;
                    default:
                }
            }
        }

        $scope.GetEmployeeExistingRelationships = function () {
            if ($scope.SelectedAbsenceReason != null) {
                if ($scope.SelectedAbsenceReason.IsForFamilyMember == true) {
                    employeeService.EmployeeExistingRelationships($scope.EmployeeId, $scope.SelectedAbsenceReason.Code).then(function (response) {
                        $scope.EmployeeExistingRelationships = response;
                        return;
                    });
                }
            }

            $scope.EmployeeExistingRelationships = null;
        }

        $scope.GetAllEmployeeContacts = function () {
            employeeService.AllEmployeeContacts($scope.EmployeeId).then(function (response) {
                $scope.AllEmployeeContacts = response;
            });
        }

        $scope.GetAllEmployerContacts = function () {
            employeeService.AllEmployerContacts($scope.EmployerId).then(function (response) {
                $scope.AllEmployerContacts = response;
            });
        }

        $scope.ContactSelected = function () {
            if (!$scope.AllEmployeeContacts
                || !$scope.CreateCaseModel
                || !$scope.CreateCaseModel.CaseReporter)
                return;

            var selectedContact = null;
            var currentCaseReporterId = $scope.CreateCaseModel.CaseReporter.EmployeeContactId;
            for (var i = 0; i < $scope.AllEmployeeContacts.length; i++) {
                var currentContact = $scope.AllEmployeeContacts[i];
                if (currentContact.Id === currentCaseReporterId) {
                    selectedContact = currentContact;
                    break;
                }
            }

            if (selectedContact) {
                if ($scope.CreateCaseModel.CaseReporter.AuthorizedSubmitter
                    && $scope.CreateCaseModel.CaseReporter.AuthorizedSubmitter.Id
                    && $scope.CreateCaseModel.CaseReporter.AuthorizedSubmitter.Id.length > 0) {
                    $scope.CreateCaseModel.CaseReporter.AuthorizedSubmitter.Id = "";
                }
                $scope.CreateCaseModel.CaseReporter.FirstName = selectedContact.FirstName;
                $scope.CreateCaseModel.CaseReporter.LastName = selectedContact.LastName;
                $scope.CreateCaseModel.CaseReporter.ContactTypeCode = selectedContact.ContactTypeCode;
                $scope.CreateCaseModel.CaseReporter.Email = selectedContact.Email;
                $scope.CreateCaseModel.CaseReporter.WorkPhone = selectedContact.WorkPhone;
            }
            else {
                $scope.CreateCaseModel.CaseReporter.FirstName = "";
                $scope.CreateCaseModel.CaseReporter.LastName = "";
                $scope.CreateCaseModel.CaseReporter.ContactTypeCode = "";
                $scope.CreateCaseModel.CaseReporter.Email = "";
                $scope.CreateCaseModel.CaseReporter.WorkPhone = "";
            }
        }

        $scope.AuthorizedSubmitterSelected = function () {
            if (!$scope.AllEmployerContacts
                || !$scope.CreateCaseModel
                || !$scope.CreateCaseModel.AuthorizedSubmitter)
                return;

            var selectedAuthorizedSubmitter = null;
            var currentAuthorizedSubmitterId = $scope.CreateCaseModel.AuthorizedSubmitter.Id;
            for (var i = 0; i < $scope.AllEmployerContacts.length; i++) {
                var currentAuthorizedSubmitter = $scope.AllEmployerContacts[i];
                if (currentAuthorizedSubmitter.Id === currentAuthorizedSubmitterId) {
                    selectedAuthorizedSubmitter = currentAuthorizedSubmitter;
                    break;
                }
            }

            if (selectedAuthorizedSubmitter) {
                if (!$scope.CreateCaseModel.CaseReporter) {
                    $scope.CreateCaseModel.CaseReporter = {};
                }
                $scope.CreateCaseModel.CaseReporter.EmployeeContactId = "";
                $scope.CreateCaseModel.CaseReporter.FirstName = selectedAuthorizedSubmitter.FirstName;
                $scope.CreateCaseModel.CaseReporter.LastName = selectedAuthorizedSubmitter.LastName;
                $scope.CreateCaseModel.CaseReporter.ContactTypeCode = selectedAuthorizedSubmitter.ContactTypeCode;
                $scope.CreateCaseModel.CaseReporter.Email = selectedAuthorizedSubmitter.Email;
                $scope.CreateCaseModel.CaseReporter.WorkPhone = selectedAuthorizedSubmitter.WorkPhone;
            }
            else {
                $scope.CreateCaseModel.CaseReporter.FirstName = "";
                $scope.CreateCaseModel.CaseReporter.LastName = "";
                $scope.CreateCaseModel.CaseReporter.ContactTypeCode = "";
                $scope.CreateCaseModel.CaseReporter.Email = "";
                $scope.CreateCaseModel.CaseReporter.WorkPhone = "";
            }
        }

        $scope.HasAuthorizedSubmitter = function () {
            return $scope.CreateCaseModel
                && $scope.CreateCaseModel.AuthorizedSubmitter
                && $scope.CreateCaseModel.AuthorizedSubmitter.Id
                && $scope.CreateCaseModel.AuthorizedSubmitter.Id.length > 0;
        }

        $scope.HasExistingContactSubmitter = function () {
            var result = $scope.CreateCaseModel !== undefined
                && $scope.CreateCaseModel.CaseReporter !== undefined
                && $scope.CreateCaseModel.CaseReporter != null
                && $scope.CreateCaseModel.CaseReporter.EmployeeContactId !== undefined
                && $scope.CreateCaseModel.CaseReporter.EmployeeContactId != null
                && $scope.CreateCaseModel.CaseReporter.EmployeeContactId.length > 0;
            return result;
        }

        $scope.onOverrideResultOptionChanged = function (rule, policy) {
            // make sure we move the result over to the property ASP.Net will use
            rule.OverrideResult = rule.OverrideResultOption.OverrideResult;

            // recalculate eligibility
            $scope.CalculateEligibility($scope.frmCreateCase.$valid, true, true);
        };

        $scope.CalculateEligibility = function (IsValid, disableScrollOnValid) {  
            
            var data = $scope.GetPostData();

            if ($scope.CreateCaseModel.AbsenceReasonId == null || $scope.CreateCaseModel.AbsenceReasonId == '' || $scope.CreateCaseModel.AbsenceReasonId == undefined)
                return;

            if (IsValid) {                
                $scope.IsEligibilityCalculated = true;
                caseService.CalculateEligibility($scope.CreateCaseModel.EmployeeId, data, function (response, status, headers, config) {
                    var data = response.data;
                    $scope.CreateCaseModel.AppliedPolicies = [];
                    $scope.CreateCaseModel.AvailableManualPolicies = [];
                    $scope.CreateCaseModel.ManuallyAppliedPolicies = [];
                    $scope.CreateCaseModel.AppliedPolicies = data.AppliedPolicies;
                    $scope.CreateCaseModel.AvailableManualPolicies = data.AvailableManualPolicies;
                    $scope.CreateCaseModel.ManuallyAppliedPolicies = data.ManuallyAppliedPolicies;
                    $scope.IsSTD = data.IsSTD;
                    $scope.CreateCaseModel.IsSTD = data.IsSTD;

                    var ShowAccoms = FindShowAcommodationFlagInChildren($scope.AbsenceReasons[0].Children, $scope.SelectedAbsenceReason.Code)
                    if (($scope.CreateCaseModel.CaseTypeId == 1 || $scope.CreateCaseModel.CaseTypeId == 2) && ShowAccoms) {
                        $scope.ui.ShowAccommOptions = false;
                    }
                    else {
                        $scope.ui.ShowAccommOptions = true;
                    }
                    $scope.ui.IsAccommodation = data.IsAccommodation;
                    $scope.CreateCaseModel.IsAccommodation = data.IsAccommodation;
                    $scope.CreateCaseModel.AccommodationRequest.Description = $scope.CreateCaseModel.ShortDescription;
                    $scope.CreateCaseModel.AccommodationRequest.StartDate = $filter('ISODateToDate')($scope.CreateCaseModel.StartDate);
                    $scope.CreateCaseModel.AccommodationRequest.EndDate = $filter('ISODateToDate')($scope.CreateCaseModel.EndDate);
                    if (data.AccommodationRequest != null) {
                        $scope.CreateCaseModel.AccommodationRequest.Type = data.AccommodationRequest.Type;
                        $scope.CreateCaseModel.AccommodationRequest.Duration = data.AccommodationRequest.Duration;
                    }
                    $scope.HasCalculatedEligibility = true;
                    if (!disableScrollOnValid) {
                        $("html, body").animate({ scrollTop: $(document).height() - 300 }, 1000);
                    }                    
                }
                    , function (data, status, headers, config) {
                        $scope.CalculateEligibilityError = data;
                    });
            }
            else {
                $("html, body").animate({ scrollTop: $(document).height() - 1200 }, 1000);
            }
        };

        $scope.AddManualPolicy = function (manualPolicyToAdd) {
            if (manualPolicyToAdd.PolicyCode != null) {
                manualPolicyToAdd.UserIsHandlingExpandCollapse = true;
                manualPolicyToAdd.Expanded = true;
                $scope.CreateCaseModel.ManuallyAppliedPolicies.push(manualPolicyToAdd);
                var index = $scope.CreateCaseModel.AvailableManualPolicies.indexOf(manualPolicyToAdd);
                $scope.manualPolicyToAdd = null;
                $scope.CreateCaseModel.AvailableManualPolicies.splice(index, 1);

                // recalculate eligibility
                $scope.CalculateEligibility($scope.frmCreateCase.$valid, true);
            }
        };
        $scope.RemoveManualPolicy = function (manualPolicyToRemove) {
            if (manualPolicyToRemove.PolicyCode != null) {
                var index = $scope.CreateCaseModel.ManuallyAppliedPolicies.indexOf(manualPolicyToRemove);
                $scope.CreateCaseModel.ManuallyAppliedPolicies.splice(index, 1);

                // recalculate eligibility
                $scope.CalculateEligibility($scope.frmCreateCase.$valid, true);
            }
        };
        $scope.RemoveAppliedPolicy = function (appliedPolicyToRemove) {
            if (appliedPolicyToRemove.PolicyCode != null) {
                var index = $scope.CreateCaseModel.AppliedPolicies.indexOf(appliedPolicyToRemove);
                $scope.CreateCaseModel.AppliedPolicies.splice(index, 1);

                // recalculate eligibility
                $scope.CalculateEligibility($scope.frmCreateCase.$valid, false);
            }
        };

        function attachAdditionalQuestions() {
            if (vm.ui.currentQuestionSet && vm.ui.currentQuestionSet.length > 0) {

                _.merge(vm.CreateCaseModel, {
                    IsAccommodation: true,
                    AccommodationRequest: {
                        AdditionalQuestions: vm.ui.currentQuestionSet.map(function (m) {
                            return {
                                Name: m.Id,
                                QuestionId: m.Id,
                                InteractiveProcessSteps: [{
                                    QuestionText: m.Question,
                                    ReportLabel: m.ReportLabel,
                                    sAnswer: m.Answer
                                }]
                            };
                        })
                    }

                });

            }
        }

        function attachTodos() {
            var todos = DynamicQuestionsService.resolveTodos(getDynamicQuestionContext());
            $scope.CreateCaseModel.todos = todos;
        }

        $scope.GetPostData = function () {            
            if ($scope.CreateCaseModel.Status != 'Inquiry') {
                attachAdditionalQuestions();
                attachTodos();
            }

            $scope.CreateCaseModel.EmployeeId = $scope.EmployeeId;

            if ($scope.DatesTargetConsecutive) {
                $scope.CreateCaseModel.CaseTypeId = 1;
            }
            else if ($scope.DatesTargetIntermittent) {
                $scope.CreateCaseModel.CaseTypeId = 2;
            }
            else if ($scope.CaseDatesInputGroup && $scope.DatesTargetAdministrative == "false") {
                $scope.CreateCaseModel.CaseTypeId = 4;
            }
            else if ($scope.DatesTargetAdministrative) {
                $scope.CreateCaseModel.CaseTypeId = 8;
            }


            angular.forEach($scope.CustomFields, function (value, key) {
                var date = Date.parse(value.SelectedValue);
                if (value.DataType == 8 && !isNaN(date) && date != "Invalid Date") {
                    value.SelectedValue = new Date(value.SelectedValue).toDateString();
                }
            })
            $scope.CreateCaseModel.CustomFields = $scope.CustomFields;

            var sDate = !$scope.CaseStartDate ? null : new Date($scope.CaseStartDate).toDateString();
            var eDate = !$scope.CaseEndDate ? null : new Date($scope.CaseEndDate).toDateString();

            if (eDate == "Invalid Date") {
                eDate = null;
            };
            $scope.CreateCaseModel.StartDate = sDate;
            $scope.CreateCaseModel.EndDate = eDate;

            var cDate = null;

            if ($scope.CreateCaseModel.Disability.ConditionStartDate != undefined &&
                $scope.CreateCaseModel.Disability.ConditionStartDate != null &&
                $scope.CreateCaseModel.Disability.ConditionStartDate != '') {
                cDate = new Date($scope.CreateCaseModel.Disability.ConditionStartDate).toDateString();
                if (cDate == "Invalid Date") {
                    cDate = null;
                };
            }
            $scope.CreateCaseModel.Disability.ConditionStartDate = cDate;

            //accomm
            var accomm = {};

            var ShowAccoms = $scope.SelectedAbsenceReason == null ? false : FindShowAcommodationFlagInChildren($scope.AbsenceReasons[0].Children, $scope.SelectedAbsenceReason.Code)
            if (($scope.CreateCaseModel.CaseTypeId == 1 || $scope.CreateCaseModel.CaseTypeId == 2) && ShowAccoms) {
                accomm.StartDate = $scope.CreateCaseModel.StartDate;
                accomm.EndDate = $scope.CreateCaseModel.EndDate;
                if (accomm.EndDate != null) {
                    accomm.Duration = 1;
                }
                else {
                    accomm.Duration = 2;
                }

                accomm.Type = {
                    CaseTypeFlag: 7,
                    Code: "LEV",
                    CustomerId: null,
                    EmployerId: null,
                    Id: "000000000000000000000005",
                    Name: "Leave",
                    cby: "000000000000000000000000",
                    cdt: "2015-03-16T12:34:55.527Z",
                    mby: "000000000000000000000000",
                    mdt: "2015-03-17T07:29:19.546Z"
                }
            }
            else {
                var reqStartDt = null;
                if ($scope.CreateCaseModel.AccommodationRequest.StartDate != undefined &&
                    $scope.CreateCaseModel.AccommodationRequest.StartDate != null &&
                    $scope.CreateCaseModel.AccommodationRequest.StartDate != '') {
                    reqStartDt = new Date($scope.CreateCaseModel.AccommodationRequest.StartDate).toDateString();
                    if (reqStartDt == "Invalid Date") {
                        reqStartDt = null;
                    };
                }
                accomm.StartDate = reqStartDt;

                var reqEndDt = null;
                if ($scope.CreateCaseModel.AccommodationRequest.EndDate != undefined &&
                    $scope.CreateCaseModel.AccommodationRequest.EndDate != null &&
                    $scope.CreateCaseModel.AccommodationRequest.EndDate != '') {
                    reqEndDt = new Date($scope.CreateCaseModel.AccommodationRequest.EndDate).toDateString();
                    if (reqEndDt == "Invalid Date") {
                        reqEndDt = null;
                    };
                }
                accomm.EndDate = reqEndDt;


                if ($scope.CreateCaseModel.AccommodationRequest.Type != undefined &&
                    $scope.CreateCaseModel.AccommodationRequest.Type != null &&
                    $scope.CreateCaseModel.AccommodationRequest.Type != '') {
                    if ($scope.SelectedAccommodationType != null) {
                        $scope.CreateCaseModel.AccommodationRequest.Type = $scope.SelectedAccommodationType;
                    }
                    accomm.Type = $scope.CreateCaseModel.AccommodationRequest.Type;
                }


                if ($scope.CreateCaseModel.AccommodationRequest.Duration != undefined &&
                    $scope.CreateCaseModel.AccommodationRequest.Duration != null &&
                    $scope.CreateCaseModel.AccommodationRequest.Duration != '') {
                    accomm.Duration = $scope.CreateCaseModel.AccommodationRequest.Duration;
                }

                if ($scope.CreateCaseModel.AccommodationRequest.IsWorkRelated != undefined &&
                    $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated != null) {
                    accomm.IsWorkRelated = $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated;
                }
            }

            //accommodation description = case short description
            accomm.Description = $scope.CreateCaseModel.AccommodationRequest.OtherDescription;

            $scope.CreateCaseModel.AccommodationRequest.Accommodations = [];
            $scope.CreateCaseModel.AccommodationRequest.Accommodations.push(accomm);

            //Work Schedule
            if ($scope.CaseStartDate == null || $scope.CaseStartDate == undefined || $scope.CaseStartDate == '') {
                $scope.CreateCase.WorkSchedule.StartDate = null;
            }
            else {
                $scope.CreateCase.WorkSchedule.StartDate = new Date($scope.CaseStartDate).toDateString();
            }

            if ($scope.CaseEndDate == null || $scope.CaseEndDate == undefined || $scope.CaseEndDate == '') {
                $scope.CreateCase.WorkSchedule.EndDate = null;
            }
            else {
                $scope.CreateCase.WorkSchedule.EndDate = new Date($scope.CaseEndDate).toDateString();
            }
            $scope.CreateCase.WorkSchedule.DaysInRotation = $scope.RotationDays;
            $scope.CreateCaseModel.WorkSchedule = $scope.CreateCase.WorkSchedule;
          
            $scope.CreateCaseModel.AbsenceReasonId = $scope.SelectedAbsenceReason != null ? $scope.SelectedAbsenceReason.Id : null;
            var selReasonCode = $scope.SelectedAbsenceReason != null ? $scope.SelectedAbsenceReason.Code : "";          
            switch (selReasonCode) {
                case "EHC":
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    break;
                case "FHC":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    break;
                case "EDUCATIONAL":
                    $scope.CopyContactData();
                    break;
                case "PATERNITY":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CopyContactData();
                    break;
                case "PREGMAT":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    //$scope.CreateCaseModel.WillUseBonding = null;
                    //$scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    //$scope.CreateCaseModel.ActualDeliveryDate = null;
                    break;
                case "BONDING":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    $scope.CopyContactData();
                    break;
                case "ADOPT":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    break;
                case "MILITARY":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = $scope.MilitaryStatus != null ? $scope.MilitaryStatus.Id : null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    break;
                case "EXIGENCY":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = $scope.MilitaryStatus != null ? $scope.MilitaryStatus.Id : null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "RESERVETRAIN":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = $scope.MilitaryStatus != null ? $scope.MilitaryStatus.Id : null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "CEREMONY":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = $scope.MilitaryStatus != null ? $scope.MilitaryStatus.Id : null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "PARENTAL":
                case "DOM":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "ORGAN":
                case "BONE":
                case "BLOOD":
                case "CRIME":
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "BEREAVEMENT":
                    $scope.CreateCaseModel.IsAccommodation = false;
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CopyContactData();
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                case "ACCOMM":
                    $scope.CreateCaseModel.IsAccommodation = true;
                    $scope.CreateCaseModel.IsWorkRelated = null;
                    $scope.CreateCaseModel.MilitaryStatusId = null;
                    $scope.CreateCaseModel.EmployeeRelationshipCode = null;
                    $scope.CreateCaseModel.EmployeeRelationshipName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipFirstName = null;
                    $scope.CreateCaseModel.EmployeeRelationshipLastName = null;
                    $scope.CreateCaseModel.WillUseBonding = null;
                    $scope.CreateCaseModel.ExpectedDeliveryDate = null;
                    $scope.CreateCaseModel.ActualDeliveryDate = null;
                    $scope.CreateCaseModel.AdoptionDate = null;
                    break;
                default:                   
                    if ($scope.Relation !== null) {
                        $scope.CopyContactData();
                    }
            }

            return $scope.CreateCaseModel;
        };

        $scope.CopyContactData = function () {            
            if ($scope.Relation == "NewRel") {
                $scope.CreateCaseModel.EmployeeRelationshipCode = $scope.EmployeeNewRelationship != null ? $scope.EmployeeNewRelationship.Id : null;
                $scope.CreateCaseModel.EmployeeRelationshipName = $scope.EmployeeNewRelationship != null ? $scope.EmployeeNewRelationship.Text : null;
                $scope.CreateCaseModel.EmployeeRelationshipFirstName = $scope.EmployeeRelationshipFirstName;
                $scope.CreateCaseModel.EmployeeRelationshipLastName = $scope.EmployeeRelationshipLastName;
                $scope.CreateCaseModel.EmployeeRelationshipDateOfBirth = $scope.EmployeeRelationshipDateOfBirth;
                $scope.CreateCaseModel.EmployeeContactId = null;
            }
            else if ($scope.Relation == "ExistingRel") {
                $scope.CreateCaseModel.EmployeeRelationshipCode = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.RelationShipId : null;
                $scope.CreateCaseModel.EmployeeRelationshipName = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.RelationShipId : null;
                $scope.CreateCaseModel.EmployeeRelationshipFirstName = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.FirstName : null;
                $scope.CreateCaseModel.EmployeeRelationshipLastName = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.LastName : null;
                $scope.CreateCaseModel.EmployeeRelationshipDateOfBirth = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.DateOfBirth : null;
                $scope.CreateCaseModel.EmployeeContactId = $scope.EmployeeExistingRelationship != null ? $scope.EmployeeExistingRelationship.Id : null;
            }
        };

        $scope.SetType = function (accom) {
            $scope.SelectedAccommodationType = accom;
            if ($scope.CreateCaseModel) {
                $scope.CreateCaseModel.AccommodationRequest.Type = accom;
            }
            else if ($scope.Accommodation) {
                $scope.Accommodation.Type = accom;
            }
        };

        $scope.CreateCase = function (IsValid) {
            // verify there is a set status                        
            if ($scope.CreateCaseModel.Status == null || $scope.CreateCaseModel.Status == "") {
                noty({ timeout: 5000, layout: 'topRight', type: 'error', text: "No case status for case creation. Please contact support." });
                throw "no case status set for case creation";
            }

            if (IsValid) {
                var caseModel = $scope.GetPostData();

                //If we are Converting a Inquiry to Case , set this flag to  false.
                if (caseModel.IsInquiry) {
                    caseModel.IsInquiry = false;
                }

                $scope.isDisabled = true;
                $http({
                    method: 'POST',
                    url: '/Employees/' + $scope.EmployeeId + '/Cases/Create',
                    data: caseModel
                }).then(function (response) {
                    if (response) {
                        $scope.CreatedLeaveOfAbsence = response.data;
                        $scope.ToDosFilter.CaseId = $scope.CreatedLeaveOfAbsence.Case.Id;
                        var d = new Date();
                        $scope.ToDosFilter.Due = (d.getUTCMonth() + 1).toString() + '/' + d.getUTCDate().toString() + '/' + d.getUTCFullYear().toString();
                        toDoListManager.List($scope, false, $scope.ErrorHandler);
                        //TODO: it should be async/callback thing instead of timeout
                        $timeout(function () {//Simulate a request
                            if ($scope.ToDos.length > 0) {
                                toDoSuccessModalService.OpenTodoSuccessModal($scope.ToDos, false, null, $scope.CreatedLeaveOfAbsence.Case.Id, $scope.CreateCaseModel.Status, true, null);
                            } else if ($scope.CreateCaseModel.Status == "Inquiry") {
                                // inquiry/case view
                                $window.location.href = "/Inquiries/" + $scope.CreatedLeaveOfAbsence.Case.Id + "/View";
                            } else {
                                // go to case view
                                $window.location.href = "/Cases/" + $scope.CreatedLeaveOfAbsence.Case.Id + "/View";
                            }
                        }, 1000);
                    }
                });
            }
            else {
                var firstInvalid = $("#frmCreateCase").find(".ng-invalid:first");
                var scrollTo = firstInvalid.offset().top / 2;
                $('html, body').animate({ scrollTop: scrollTo }, 1000);
            }
        };
        $scope.OverrideRule = function (rule) {
            rule.Overridden = true;
        };
        $scope.RevertRule = function (rule) {
            rule.Overridden = false;
            // recalculate eligibility
            $scope.CalculateEligibility($scope.frmCreateCase.$valid, true);
        };

        //#region view specific logic
        $scope.DatesTargetConsecutive = false;
        $scope.DatesTargetIntermittent = false;
        $scope.DatesTargetReduced = false;
        $scope.DatesTargetAdministrative = false;
        $scope.CaseDatesInputGroup = false;

        $scope.CaseTypeSelected = function (caseTypeId) {
            // $("html, body").animate({ scrollTop: 500 }, 1000);
            $scope.IsRecal();
            $scope.CreateCaseModel.CaseTypeId = caseTypeId;
            $scope.DatesTargetConsecutive = false;
            $scope.DatesTargetIntermittent = false;
            $scope.DatesTargetReduced = false;
            $scope.DatesTargetAdministrative = false;
            $scope.CaseDatesInputGroup = true;
            $scope.ui.IsAccommodation = false;
            $scope.IsSTD = false;
            $scope.SelectedAbsenceReason = null;
            $scope.SelectedAbsenceReasonCategory = null;
            $scope.AbsenceReasons = null;

            if ($scope.CreateCaseModel.Status != 'Inquiry') {
                $scope.AbsenceReasons = lookupService.AbsenceReasons($scope.EmployeeId, $scope.CreateCaseModel.CaseTypeId);
            }
        }

        $scope.OnConsecutiveClick = function () {
            $scope.CaseTypeSelected(1);
            $scope.DatesTargetConsecutive = true;
        };

        $scope.OnIntermittentClick = function () {
            $scope.CaseTypeSelected(2);
            $scope.DatesTargetIntermittent = true;
        };

        $scope.OnReducedClick = function () {
            $scope.CaseTypeSelected(4);
            // Get Employee ID for Reduced Schedule
            if ($scope.EmployeeId) {
                employeeService.GetEmployeeWorkSchedule($scope.EmployeeId)
                    .then(function (result) {
                        // Add Current work schedule to CreateCaseModel to Postback to server on CalculateEligibility
                        $scope.CreateCaseModel.WorkSchedule = result;
                        $scope.UpdateScheduleType(0);
                        $scope.Times = $scope.CreateCaseModel.WorkSchedule.Times;
                    },
                    $scope.ErrorHandler);
            };
            if ($scope.IsEligibilityCalculated) {
                $scope.resetEligibilityValues();
            }
            $scope.DatesTargetReduced = true;      
        }; //End OnReducedClick()

        $scope.OnAdministrativeClick = function () {
            $scope.CaseTypeSelected(8);
            $scope.DatesTargetAdministrative = true;
        };//End OnAdministrativeClick()

        $scope.resetEligibilityValues = function () {
            $scope.IsRecalculation = true;
            $scope.HasCalculatedEligibility = false;
            $scope.IsSTD = false;
            $scope.ui.IsAccommodation = false;

            if ($scope.CreateCaseModel) {
                $scope.CreateCaseModel.AppliedPolicies = [];
                $scope.CreateCaseModel.AvailableManualPolicies = [];
                $scope.CreateCaseModel.ManuallyAppliedPolicies = [];
            }
        };

        $scope.AllowNewContact = function () {
            $scope.frmCreateCase.EmployeeNewRelationship.$setValidity('duplicateContact', true);
            $scope.DeniedNewDuplicateRelationship = false;
            $("#ContactDuplication").modal('hide');
        }

        $scope.DenyNewContact = function () {
            $scope.DeniedNewDuplicateRelationship = true;
            $("#ContactDuplication").modal('hide');
        }

        //OpenContactsModal
        $scope.OpenContactsModal = function () {
            contactService.OpenContactsModal($scope, $scope.EmployeeId);
        };

        $scope.AccommTypeSelected = function (isLeaveType) {
            if ($scope.CreateCaseModel.CaseTypeId == 8 && isLeaveType) {
                $scope.ShowAccomLeaveTypeMssg = true;
            } else {
                $scope.ShowAccomLeaveTypeMssg = false;
            }
        };

        //#endregion view specific logic

        $scope.OnWorkRelatedClick = function (IsWorkRelated) {
            $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated = IsWorkRelated;
            if (IsWorkRelated) {
                if ($scope.CreateCaseModel.AccommodationRequest.Type.Code == "COF") {
                    $scope.isDisabled = true;
                    $("#CaseWarning").modal('show');
                }
                else {
                    $scope.isDisabled = false;
                }
            }
            else {
                $scope.isDisabled = false;
            }
        };

        $scope.OnAccomTypeSelected = function () {
            var IsWorkRelated = $scope.CreateCaseModel.AccommodationRequest.IsWorkRelated;
            if (IsWorkRelated != null && IsWorkRelated != undefined && IsWorkRelated != '') {
                if (IsWorkRelated) {
                    if ($scope.CreateCaseModel.AccommodationRequest.Type.Code == "COF") {
                        $scope.isDisabled = true;
                        $("#CaseWarning").modal('show');
                    }
                    else {
                        $scope.isDisabled = false;
                    }
                }
                else {
                    $scope.isDisabled = false;
                }
            }
        }

        $scope.SetAddressToEdit = function (IsPrimaryAddr) {
            $scope.AddressToEdit = {};
            $scope.forms.addressSubmitted = false;
            if (IsPrimaryAddr)
                angular.copy($scope.EmployeeInfo.Address, $scope.AddressToEdit);
            else
                angular.copy($scope.EmployeeInfo.AltAddress, $scope.AddressToEdit);

            $scope.AddressToEdit.IsPrimary = IsPrimaryAddr;
            $scope.GetRegionsByCountry(false);
            $("#EditAddress").modal('show');
        }

        $scope.GetRegionsByCountry = function (resetState) {
            if (resetState)
                $scope.AddressToEdit.State = "";

            regionService.GetRegionNameByCountryCode($scope.AddressToEdit.Country).then(function (name) {
                $scope.RegionName = name;
            });
            regionService.GetRegionsByCountryCode($scope.AddressToEdit.Country).then(function (regions) {
                $scope.Regions = regions;
            });
        }

        $scope.SetNewAddress = function (isValid, isPrimaryAddr) {
            if (isValid) {
                if (isPrimaryAddr) {
                    $scope.EmployeeInfo.Address = $scope.AddressToEdit;
                    $scope.CreateCaseModel.NewAddress = $scope.AddressToEdit;
                    $scope.CreateCaseModel.IsContactInformationCorrect = false;
                }
                else {
                    $scope.EmployeeInfo.AltAddress = $scope.AddressToEdit;
                    $scope.CreateCaseModel.NewAltAddress = $scope.AddressToEdit;
                    $scope.CreateCaseModel.IsAltAddressCorrect = false;
                }

                $("#EditAddress").modal('hide');
            }

        }

        $scope.SetStartEndDate = function (duration) {
            if (duration == 1) {
                $scope.CreateCaseModel.AccommodationRequest.StartDate = $scope.CaseStartDate;
                $scope.CreateCaseModel.AccommodationRequest.EndDate = $scope.CaseEndDate;
            }
            else if (duration == 2) {
                $scope.CreateCaseModel.AccommodationRequest.StartDate = $scope.CaseStartDate;
                $scope.CreateCaseModel.AccommodationRequest.EndDate = null;
            }
        }

        $scope.SetContactPref = function () {
            if ($scope.CreateCaseModel.ContactPreference == 'Phone') {
                if ($scope.CreateCaseModel.EmployeePrimaryPhone == null || $scope.CreateCaseModel.EmployeePrimaryPhone == undefined || $scope.CreateCaseModel.EmployeePrimaryPhone == '') {
                    $scope.forms.EditPrimaryPhone = true;
                }
            }
            else {
                if ($scope.CreateCaseModel.EmployeePrimaryEmail == null || $scope.CreateCaseModel.EmployeePrimaryEmail == undefined || $scope.CreateCaseModel.EmployeePrimaryEmail == '') {
                    $scope.forms.EditPrimaryEmail = true;
                }
            }
        }

        $scope.savePrimaryPhone = function (PrimaryPhoneToEdit) {
            if (PrimaryPhoneToEdit) {
                $scope.CreateCaseModel.EmployeePrimaryPhone = $scope.forms.PrimaryPhoneToEdit; $scope.forms.EditPrimaryPhone = false;
            }
        }

        $scope.UpdateScheduleType = function (scheduleType) {
            $scope.ScheduleType = scheduleType;
            //populate day
            $scope.CreateCase.WorkSchedule.Times = [];
            for (var i = 0; i < 7; i++) {
                $scope.CreateCase.WorkSchedule.Time = new Object();
                $scope.CreateCase.WorkSchedule.Time.TotalMinutes = '0h';
                $scope.CreateCase.WorkSchedule.Time.SampleDate = $scope.CaseStartDate;
                $scope.CreateCase.WorkSchedule.Times.push($scope.CreateCase.WorkSchedule.Time);
            }
            //weekly
            if (scheduleType == 0) {
                $scope.CreateCase.WorkSchedule.ScheduleType = 0;
                $scope.ScheduleStartEnd = true;
                $scope.ScheduleWeekly = true;
                $scope.ScheduleRotating = false;
                $scope.ScheduleRotatingPerDay = false;
                $scope.ScheduleOneTime = false;
                $scope.ScheduleFteVariable = false;
            }
            //rotating
            else if (scheduleType == 1) {
                $scope.CreateCase.WorkSchedule.ScheduleType = 1;
                $scope.ScheduleStartEnd = true;
                $scope.ScheduleWeekly = false;
                $scope.ScheduleRotating = true;
                $scope.ScheduleRotatingPerDay = true;
                $scope.ScheduleOneTime = false;
                $scope.ScheduleFteVariable = false;
                var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
                var weeklyDays = $('#schedule-weekly .schedule-days input');
                weeklyDays.removeAttr('name');
                rotatingDays.removeAttr('name');

                for (var i = 0; i < weeklyDays.length; i++) {
                    $(weeklyDays[i]).attr('name', 'CreateCase.WorkSchedule.Times[' + i + '].TotalMinutes');
                }
            }
            //manual
            else if (scheduleType == 2) {
                $scope.WorkScheduleStartDate = $scope.CaseStartDate;
                $scope.CreateCase.WorkSchedule.ScheduleType = 2;
                $scope.ScheduleStartEnd = true;
                $scope.ScheduleWeekly = false;
                $scope.ScheduleRotating = false;
                $scope.ScheduleRotatingPerDay = false;
                $scope.ScheduleOneTime = true;
                $scope.ScheduleFteVariable = false;
                $scope.$watch('WorkScheduleStartDate', function () {
                    var strISODate = $filter("date")($scope.WorkScheduleStartDate, "yyyy-MM-dd");
                    $scope.GetManualDays(strISODate);

                });
            }
            //FTE Variable
            else if (scheduleType == 3) {
                $scope.WorkScheduleStartDate = $scope.ChangeCaseStartDate;
                $scope.CreateCase.WorkSchedule.ScheduleType = 3;
                $scope.ScheduleStartEnd = true;
                $scope.ScheduleWeekly = false;
                $scope.ScheduleRotating = false;
                $scope.ScheduleRotatingPerDay = false;
                $scope.ScheduleOneTime = false;
                $scope.ScheduleFteVariable = true;                
                $scope.UpdateWeeklyDurationType(0);
                $("#ftWeeklyWorkTime").blur();
            }
        };

        $scope.UpdateWeeklyDurationType = function (weeklyDurationType) {
            $scope.CreateCase.WorkSchedule.FteWeeklyDuration = weeklyDurationType;
            $scope.CreateCase.WorkSchedule.FteAvgTimePerWeek = 0;
            $scope.CreateCase.WorkSchedule.FteTimePercentage = 0;
        };

        $scope.UpdateFteTimePercentage = function () {
            $scope.CreateCase.WorkSchedule.FteAvgTimePerWeek = ($scope.CreateCase.WorkSchedule.FteTimePercentage / 100.0) * $scope.CreateCase.WorkSchedule.FTWeeklyWorkTime;
            $('#fteHours').val($scope.CreateCase.WorkSchedule.FteAvgTimePerWeek.toFixed(2));
            $("#fteHours").blur();
        };
        $scope.$watch('RotationDays', function () {
            if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
                for (var i = 0; i < parseInt($scope.RotationDays); i++) {
                    $scope.Time = new Object();
                    $scope.Time.TotalMinutes = null;
                    $scope.Times.push($scope.Time);
                }
                $scope.rotationArray = new Array(parseInt($scope.RotationDays));
            }
            else {
                $scope.rotationArray = null;
            }
        });

        //#region "Manual Schedule"
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var noofdays = function (mm, yyyy) {
            var daysofmonth;

            if ((mm == 4) || (mm == 6) || (mm == 9) || (mm == 11)) {
                daysofmonth = 30;
            }
            else {
                daysofmonth = 31;
                if (mm == 2) {
                    if (yyyy / 4 - parseInt(yyyy / 4) != 0) {
                        daysofmonth = 28;
                    }
                    else {
                        if (yyyy / 100 - parseInt(yyyy / 100) != 0) {
                            daysofmonth = 29;
                        }
                        else {
                            if ((yyyy / 400) - parseInt(yyyy / 400) != 0) {
                                daysofmonth = 28;
                            }
                            else {
                                daysofmonth = 29;
                            }
                        }
                    }
                }
            }

            return daysofmonth;
        }

        $scope.GetManualDays = function (startDate) {

            if ($scope.CreateCase.WorkSchedule.ScheduleType != 2)
                return;

            if (startDate == null) return;

            // var selectedDate = new Date(startDate);
            var selectedDate = $filter('ISODateToDate')(startDate);
            $scope.CurrentDisplayMonth = selectedDate.getMonth();
            $scope.CurrentDisplayYear = selectedDate.getFullYear();

            $scope.CurrentMonthName = monthNames[$scope.CurrentDisplayMonth] + " " + $scope.CurrentDisplayYear;

            if ($scope.CurrentDisplayMonth == 0) {
                $scope.PrevMonthName = monthNames[11] + " " + ($scope.CurrentDisplayYear - 1);
                $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
            }
            else if ($scope.CurrentDisplayMonth == 11) {
                $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                $scope.NextMonthName = monthNames[0] + " " + ($scope.CurrentDisplayYear + 1);
            }
            else {
                $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
                $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
            }

            var numOfDaysInMonth = noofdays($scope.CurrentDisplayMonth + 1);

            var startDay = selectedDate.getDate();

            var monthPosition = $.inArray($scope.CurrentDisplayMonth, $scope.AlreadyPushedMonths);
            if (monthPosition == -1) {
                for (var i = 1; i <= numOfDaysInMonth; i++) {
                    var time = {
                        TotalMinutes: '', SampleDate: ''
                    };

                    var tempDate = selectedDate.getFullYear() + "-" + ('0' + ($scope.CurrentDisplayMonth + 1)).slice(-2) + "-" + ('0' + (i)).slice(-2);
                    time.SampleDate = $filter("date")(tempDate, "yyyy-MM-dd"); //$filter('ISODateToDate')(tempDate);                   
                    $scope.ManualTimes.push(time);                  
                }

                $scope.AlreadyPushedMonths.push($scope.CurrentDisplayMonth);
                $scope.FilterDates();
            }

            $scope.GetBlankDayFromDate($scope.CurrentDisplayMonth + 1);
        };

        $scope.currentMonthDates = function (dayToCheck) {
            var selectedDate = $filter('ISODateToDate')(dayToCheck.SampleDate);
            if ($scope.CurrentDisplayMonth == selectedDate.getMonth()) {
                return true;
            }
            return false;
        };

        $scope.FilterDates = function () {
            angular.forEach($scope.ManualTimes, function (manualTime, index) {
                var dt = $filter('ISODateToDate')(manualTime.SampleDate);                  // new Date(manualTime.SampleDate);
                if (dt.getMonth() == $scope.CurrentDisplayMonth) {
                    $scope.CreateCase.WorkSchedule.ActualTimes.push(manualTime);
                }
            });
        };

        $scope.GoToMonth = function (direction) {
            var year, month;

            if (direction == 'Prev') {
                if ($scope.CurrentDisplayMonth == 0) {  // if January
                    month = 11;
                    year = $scope.CurrentDisplayYear - 1;
                }
                else {
                    month = $scope.CurrentDisplayMonth - 1;
                    year = $scope.CurrentDisplayYear;
                }
            }
            else {
                if ($scope.CurrentDisplayMonth == 11) {  // if December
                    month = 0;
                    year = $scope.CurrentDisplayYear + 1;
                }
                else {
                    month = $scope.CurrentDisplayMonth + 1;
                    year = $scope.CurrentDisplayYear;
                }
            }

            var strDate = year + "-" + ('0' + (month + 1)).slice(-2) + "-" + ('0' + (1)).slice(-2);
            //time.SampleDate = $filter('ISODateToDate')(tempDate);

            //var newDate = new Date(year, month, 1)
            $scope.GetManualDays(strDate);
        };

        function getDynamicQuestionContext() {
            return {
                AbsenceReasonCode: vm.SelectedAbsenceReason ? vm.SelectedAbsenceReason.Code : null,
                AbsenceReasonName: vm.SelectedAbsenceReason ? vm.SelectedAbsenceReason.Name : null,
                AccommodationType: vm.CreateCaseModel.AccommodationRequest.Type ? vm.CreateCaseModel.AccommodationRequest.Type : null,
                EmployeeFirstName: vm.EmployeeInfo ? vm.EmployeeInfo.FirstName : null,
                EmployeeLastName: vm.EmployeeInfo ? vm.EmployeeInfo.LastName : null,
                Questions: vm.ui.currentQuestionSet ?
                    vm.ui.currentQuestionSet.reduce(function (prev, curr) {
                        prev[curr.Id] = curr;
                        return prev;
                    }, {}) :
                    {}
            };
        }
    }
})();