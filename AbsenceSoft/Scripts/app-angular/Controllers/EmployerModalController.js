//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployerModalCtrl', ['$scope', '$http', '$filter', '$timeout', 'administrationService', 'lookupService', '$modalInstance', 'user',
    function ($scope, $http, $filter, $timeout, administrationService, lookupService, $modalInstance, user) {
    $scope.EmployersList = {};
    $scope.EmployerAccessModel = [];
    $scope.User = user;
    $scope.AllRoles = [];
    $scope.ApplyRoles = [];
    $scope.ShowEmployerAccess = false;

    $scope.SignUpData = {
        Id: $scope.User.Id, FirstName: $scope.User.FirstName, LastName: $scope.User.LastName, Password: "", ConfirmPassword: "", Email: $scope.User.Email, Employers: [], AllRoles: [], ApplyRoles: [], Phone: $scope.User.Phone, Fax: $scope.User.Fax, EndDate: $scope.User.EndDate
    };

    lookupService.GetRolesUser($scope.User.Id)
        .then(function (data) {
            if (data != null) {
                $scope.User.Roles = data;
                // Get All Employers list
                lookupService.GetEmployersForUserPlusUser()
                    .then(function (response) {
                        if (response != null) {
                            $scope.EmployersList = response;

                            lookupService.GetEmployerFeatures()
                                .then(function (result) {
                                    if (result) {
                                        if (result.MultiEmployerAccess === true || $scope.IsESS === "True") {
                                            $scope.ShowEmployerAccess = true;
                                        } else {
                                            $scope.ShowEmployerAccess = false;
                                        }

                                        // Get Roles
                                        $scope.GetRoles();
                                    }
                                });
                        }
                    });
            }
        });
    

    $scope.GetRoles = function () {
        administrationService.GetRoles()
            .then(function (data) {
                if (data != null) {
                    angular.copy(data, $scope.SignUpData.AllRoles);
                    angular.copy($filter("filter")(data, { Type: 0 }), $scope.AllRoles);
                    angular.copy($filter("filter")(data, { Type: 0 }), $scope.GlobalRoles);
                    angular.copy($filter("filter")(data, { Type: 1 }), $scope.ESSRoles);

                    angular.forEach($scope.EmployersList, function (employer, index) {
                        var allRoles = [];
                        if (employer.Id === "0") {
                            angular.copy($scope.SignUpData.AllRoles, allRoles);
                            $scope.EmployerAccessModel.push({
                                UserId: $scope.User.Id,
                                EmployerId: employer.Id,
                                EmployerName: employer.Name,
                                Roles: $scope.User.Roles,
                                AllRoles: allRoles
                            });
                        } else {
                            angular.copy($scope.AllRoles, allRoles);
                            var emp = $filter("filter")($scope.User.Employers, { Id: employer.Id });
                            if (emp.length > 0) {
                                $scope.EmployerAccessModel.push({
                                    UserId: $scope.User.Id,
                                    EmployerId: emp[0].Id,
                                    EmployerName: emp[0].Name,
                                    Roles: emp[0].Roles,
                                    AllRoles: allRoles
                                });
                            }
                        }
                    });

                    angular.forEach($scope.EmployerAccessModel, function (employer, employerIndex) {
                        if (employer.Roles.length > 0) {
                            angular.forEach(employer.Roles, function (employerRole, employerRoleIndex) {
                                var existingRole = $filter("filter")(employer.AllRoles, { Id: employerRole });
                                if (existingRole.length <= 0) {
                                    var roleToAdd = $filter("filter")($scope.ESSRoles, { Id: employerRole });
                                    if (roleToAdd) {
                                        employer.HasESSRole = true;
                                        employer.AllRoles.push(roleToAdd[0]);
                                    }
                                        
                                }
                            });
                        }
                    });

                    for (var i = $scope.EmployerAccessModel[0].AllRoles.length - 1; i >= 0; i--) {
                        var role = $scope.EmployerAccessModel[0].AllRoles[i];
                        if (role.Type === 1) {
                            if ($scope.EmployerAccessModel[0].Roles.indexOf(role.Id) === -1) {
                                $scope.EmployerAccessModel[0].AllRoles
                                    .splice($.inArray(role, $scope.EmployerAccessModel[0].AllRoles), 1);
                            }
                        }
                    }

                    $scope.EmployerAccessModel.SelectedEmployer = $scope.EmployerAccessModel[0].EmployerId;

                    $timeout(function () {
                        // For "Employer Access" tab

                        angular.forEach($scope.EmployerAccessModel, function (employerAccessModel, employerAccessModelIndex) {
                            angular.forEach(employerAccessModel.AllRoles, function (employerAccessRole, employerAccessRoleIndex) {
                                if (employerAccessModel.Roles.indexOf(employerAccessRole.Id) !== -1)
                                    employerAccessRole.Apply = true;
                                else
                                    employerAccessRole.Apply = false;
                            });
                        });

                        $scope.$apply();
                    },
                    500);
                }
            });
    }

    $scope.UpdateEmployerAccess = function (isValid) {
        if (!isValid)
            return;

        $scope.SignUpData.Employers = [];
        angular.forEach($scope.EmployersList, function (employer, index) {
            if (employer.checked === true) {
                $scope.SignUpData.Employers.push(employer.Id);
            }
        });

        if (typeof $scope.SignUpData.AssignedEmployeeDisplayName == "undefined"
            || $scope.SignUpData.AssignedEmployeeDisplayName == null
            || $scope.SignUpData.AssignedEmployeeDisplayName.replace(/\s/g, "") === "") {
            $scope.SignUpData.AssignedEmployeeId = "";
            $scope.SignUpData.AssignedEmployeeEmployerId = "";
        }

        // Save Employer Access 
        angular.forEach($scope.EmployerAccessModel, function (employer, index) {
            employer.Roles = [];

            angular.forEach(employer.AllRoles, function (employerRole, employerIndex) {
                if (employerRole.Apply === true)
                    employer.Roles.push(employerRole.Id);
            });
        });

        if (!$scope.ShowEmployerAccess
            && $scope.EmployerAccessModel.length > 1) {
            $scope.EmployerAccessModel.pop();
        }

        var employerAccessList = {
            EmployerAccessList: $scope.EmployerAccessModel
        }

        // Update employer access
        administrationService.UpdateEmployerAccess(employerAccessList)
            .then(function (response) {
                if (response.Error == null || response.Error === "") {
                    $scope.cancel();
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Employer access role assigned successfully." });
                    window.location.reload();
                }
                else {
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: response.Error });
                }
            });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);