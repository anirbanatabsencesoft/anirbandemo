//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EditEmployerInfoCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.openEditEmployerModal = function (employerId) {
        var EditEmployerInfoModalInstance = $modal.open({
            templateUrl: 'EditEmployerInfo.html',
            controller: 'EditEmployerInfoModalCtrl',
            windowClass: 'app-modal-window',
            resolve: {
                employerId: function () {
                    return employerId;
                }
            }
        });

        EditEmployerInfoModalInstance.result.then(function () {
        }, function () {
            $("#disabledAdminPage").scope().GetEmployers(false);
        });
    };
}]);