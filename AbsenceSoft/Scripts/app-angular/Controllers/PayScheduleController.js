//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('PayScheduleCtrl', ['$scope', '$http', '$filter', '$rootScope', 'payScheduleService', 'administrationService', '$window', '$q',
    function ($scope, $http, $filter, $rootScope, payScheduleService, administrationService, $window, $q) {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        $scope.ProcessingDay = '';
        $scope.form = { submitted: false };
        $scope.InitList = function (employerId) {
            init(employerId);
            payScheduleService.List($scope.EmployerId).then(function (paySchedules) {
                $scope.PaySchedules = paySchedules;
            })
        }

        $scope.Init = function (employerId, payScheduleId) {
            init(employerId);
            if (payScheduleId) {
                payScheduleService.Get(employerId, payScheduleId).then(function (paySchedule) {
                    $scope.PaySchedule = paySchedule;
                    $scope.UpdateProcessingDay();
                });
            } else {
                $scope.PaySchedule = {
                    EmployerId: $scope.EmployerId
                };
            }
        }

        init = function (employerId) {
            $scope.EmployerId = employerId;
            administrationService.GetEmployerInfo($scope.EmployerId).then(function (empData) {
                $scope.EmployerName = empData.SignatureName;
            });
        }

        $scope.GoToPayScheduleEdit = function (paySchedule) {
            window.location.href = '/PaySchedule/' + paySchedule.EmployerId + '/Edit/' + paySchedule.Id;
        }

        $scope.SavePaySchedule = function () {
            $scope.form.submitted = true;
            if ($scope.editPaySchedule.$valid) {
                $scope.PaySchedule.Default = $scope.PaySchedule.Default || false;
                payScheduleService.Save($scope.PaySchedule).then(function (paySchedule) {
                    $scope.PaySchedule = paySchedule;
                    $scope.form.submitted = false;
                    var goto = '/PaySchedule/' + paySchedule.EmployerId;
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Pay schedule successfully updated", callback: { afterClose: function () { window.location.href = goto; } } });
                })
            } else {
                var firstInvalid = $("#editPaySchedule").find(".ng-invalid:first");
                var scrollTo = firstInvalid.offset().top / 2;
                $('html, body').animate({ scrollTop: scrollTo }, 1000);
            }
        }

        $scope.DeletePaySchedule = function () {
            payScheduleService.Delete($scope.EmployerId, $scope.PaySchedule.Id).then(function (success) {
                if (success) {
                    var goto = '/PaySchedule/' + $scope.EmployerId;
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Pay schedule successfully deleted", callback: { afterClose: function () { window.location.href = goto; } } });
                } else {
                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "Unable to delete pay schedule" });
                }
            });

        }

        $scope.UpdateProcessingDay = function () {
            if ($scope.PaySchedule.PayPeriodType == 1 || $scope.PaySchedule.PayPeriodType == 2) {
                var dayOfWeek = (parseInt($scope.PaySchedule.DayEnd, 0) + $scope.PaySchedule.ProcessingDays) % 7;
                $scope.ProcessingDay = days[dayOfWeek];

            }
            
        }

        $scope.Back = function () {
            $window.history.back();
        }

    }]);