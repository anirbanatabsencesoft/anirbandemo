//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
//#region Employee Info Ctrl
angular
.module('App.Controllers')
.controller('ViewEmployeeCtrl', ['$scope', '$http', '$location', '$window', 'caseService', 'employeeService', 'contactService', 'lookupService', '$filter', '$timeout', '$rootScope', 
    function ($scope, $http, $location, $window, caseService, employeeService, contactService, lookupService, $filter, $timeout, $rootScope) {

        $scope.CaseCalendar = null;
        $scope.CaseCalendarDateRange = null;
        $scope.CaseCalendarMonths = [];
        $scope.EmployeeId = null;
        $scope.ShowTimeTrackerInHours = false;

        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.Error = response;
            $scope.isDisabled = false;
        };

        //#region controller initialization logic
        $scope.init = function (employeeId, IsESS) {
            $scope.EmployeeId = employeeId;
            $scope.IsEmployeeSelfService = IsESS == 'True' ? true : false;
            //if (IsESS == 'True')
            //    $rootScope.UserImage = "/Content/Images/user-blue.png";
            
            $scope.JobClassifications = lookupService.JobClassifications();

            employeeService.GetEmployerFeature(employeeId)
              .then(function (result) {
                  $scope.LOAFeatureEnabled = result.LOAFeatureEnabled;
              },
              $scope.ErrorHandler);

            employeeService.GetTimeTrackingInfo(employeeId)
              .then(function (result) {
                  $scope.TimeTrackingInfo = result;
              },
              $scope.ErrorHandler);


            $scope.CaseCalendarDateRange = caseService.GetDefaultRangeForCalendarForEmployee();
            $scope.GetCalendarData();


        };

        
        //#region delete employee
        $scope.DeleteEmployee = function () {
            employeeService.DeleteEmployee($scope.EmployeeId).then(function (result) {
                if (result) {
                    // $window.location.href = "/Home"; -- Modified by Mohan. /Home is invalid path.
                    $window.location.href = "/";
                }
            },
                $scope.ErrorHandler);
        };
        //#endregion delete employee

        // Calendar next previous sliding
        $scope.GoToPrev12Months = function (dtRangeStart) {
            $scope.CaseCalendarDateRange = employeeService.GetNextPrevRangeForCalendarForEmployee(dtRangeStart, "prev");
            $scope.GetCalendarData();
        }

        $scope.GoToNext12Months = function (dtRangeStart) {
            $scope.CaseCalendarDateRange = employeeService.GetNextPrevRangeForCalendarForEmployee(dtRangeStart, "next");
            $scope.GetCalendarData();
        }

        $scope.GetCalendarData = function () {
            caseService.GetCalendarForEmployee($scope.EmployeeId, $scope.CaseCalendarDateRange)
                .then(function (result) {
                    $scope.CaseCalendar = result;
                    // we do this AFTER getting the CaseCalendar because CaseCalendar is needed
                    // during the process of binding bootstrap's datepicker to CaseCalendarMonths            
                    $scope.CaseCalendarMonths = caseService.GetMonthsForCalendar($scope.CaseCalendarDateRange);
                });
        };

        //OpenContactsModal
        $scope.OpenContactsModal = function () {
            contactService.OpenContactsModal($scope, $scope.EmployeeId);
        };

        // check employee info is complete before opening case create page
        $scope.CreateCaseValidation = function () {
            employeeService.IsEmployeeInfoComplete($scope.EmployeeId).then(function (data) {
                if (data != null) {
                    if (data.IsEmployeeInfoComplete) {
                        if ($scope.LOAFeatureEnabled) {
                            if ($scope.IsEmployeeSelfService) {
                                window.location.href = '/#/employee-case-create/' + $scope.EmployeeId;
                            }
                            else {
                                window.location.href = "/Employees/" + $scope.EmployeeId + "/Cases/Create";
                            }
                        }
                        else if ($scope.IsEmployeeSelfService && !$scope.LOAFeatureEnabled) {
                            window.location.href = "/#/employee-new-accomm-create/" + $scope.EmployeeId;
                        }
                        else if (!$scope.IsEmployeeSelfService && !$scope.LOAFeatureEnabled) {
                            window.location.href = "/Employees/" + $scope.EmployeeId + "/Cases/Create";
                        }
                    }
                    else {
                        $("#incompleteEmployeeInfoWarning").modal('show');
                        $scope.incompleteFields = data.IncompleteFields;
                    }
                }
            })
        };

        $scope.CompleteEmployeeInfo = function () {
            window.location.href = "/Employees/" + $scope.EmployeeId + "/Edit";
        }

        //mark end of EmployeeInfoCtrl
    }]);
//#endregion Employee Info Ctrl
