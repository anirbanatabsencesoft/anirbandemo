//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('TodoSuccessModalCtrl', ['$scope', '$modalInstance', 'toDoItems', 'isCommunication', 'communicationAttachments', 'caseId', 'caseStatus', 'showToDo', 'communicationId', 'filterFilter', '$window',
    function ($scope, $modalInstance, toDoItems, isCommunication, communicationAttachments, caseId, caseStatus, showToDo, communicationId, filterFilter, $window) {
    $scope.toDos = filterFilter(toDoItems, { ItemType: 'Communication' });
    $scope.communicationAttachments = communicationAttachments;
    $scope.ShowBodyContents = false;
    $scope.ShowPDF = false;
    $scope.CaseId = caseId;
    $scope.showToDo = showToDo;
    $scope.CommunicationId = communicationId;

    angular.forEach($scope.toDos, function (toDo, key) {
        if (toDo.Status == "Pending" || toDo.Status == "Overdue") {
            $scope.ShowBodyContents = true;
        }
    });
    $scope.IsCommunication = isCommunication;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');

        if ($scope.toDos) {
            if ($scope.caseStatus != null && caseStatus != "" && caseStatus == "Inquiry")
                $window.location.href = "/Inquiries/" + $scope.CaseId + "/View";
            else
                $window.location.href = "/Cases/" + $scope.CaseId + "/View";  // $window.location.href = "/Cases/" + $scope.toDos[0].CaseId + "/View";
        }
    };

    if (communicationAttachments != null) {
        // If communication is not done thru email then only show PDF download
        
            if (communicationAttachments.Paperwork.length > 0 || communicationAttachments.CaseAttachments.length)
                $scope.ShowPDF = true;
       
    }

    $scope.OpenPDF = function (caseId, AttachmentId) {
        window.location.href = "/Cases/" + caseId + "/Attachments/" + AttachmentId;
    }
}]);