//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('CaseIndexCtrl', ['$scope', '$http', '$window', 'caseListManager', 'caseService', 'administrationService', '$modal', '$location', 'lookupService', 
function ($scope, $http, $window, caseListManager, caseService, administrationService, $modal, $location, lookupService) {

        //$scope.IsTPA = false;
        $scope.Error = null;

        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };

        $scope.ErrorHandler = function (response) {            
            $scope.Error = response;
        };       
    
        $scope.GetTotalLinks = function () {

            var downloadZipLink = $modal.open({
                templateUrl: 'DownloadZipLink.html',
                controller: downloadZipCtrlInstance,
            });           
        }

        administrationService.GetUsersList().then(function (data) {
            if (data != null) {
                $scope.UsersList = data;
            }
        })

        $scope.DoExport = function () {
            caseService.DoExport().$promise.then(function () {
                $scope.DoExportComplete = true;
            }, function (error) {
            });
        };

    //#re-assign cases
        $scope.reassigncases = { Cases: [] };

        $scope.ReAssignCases = function () {
            if ($scope.UserId) {
                $scope.showNoUserWarning = false;
                $scope.reassigncases.UserId = $scope.UserId;
                caseService.ReassignCases($scope.reassigncases).then(function (response) {
                    //provide status message to user on success / failure                
                    $scope.GetCases();
                    $scope.SetCheckboxes(false);
                });
            } else {
                $scope.showNoUserWarning = true;
            }
        };
        $scope.SetCheckboxes = function (checked) {
            for (var i = 0; i < $scope.reassigncases.Cases.length; i++) {
                $scope.reassigncases.Cases[i].ApplyStatus = checked;
            }
            $('.case-checkbox').each(function () {
                this.checked = checked;
            });
        }
        //#end re-assign cases

        $scope.init = function (callingPage) {

            $scope.DoExportComplete = false;

            lookupService.GetAllCaseStatus().then(function (response) {
                $scope.CaseStatusList = response;
            });

            var infiniteScrollOnBody = callingPage == "index";
            caseListManager.Init($scope, infiniteScrollOnBody);
            if (infiniteScrollOnBody) {
                //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                        caseListManager.LoadMore($scope, $scope.ErrorHandler);
                    }
                });
            }

            if (callingPage == "index") {
                $scope.ListType = "Individual";
            }

            $scope.$watch('ListType', function (newVal, oldVal) {
                if (newVal === oldVal) return;
                $scope.GetCases(false);
            });

            // Case filter updated date watch
            $scope.$watch('CasesFilter.Updated', function (newVal, oldVal) {
                if (newVal === oldVal) return;
                $scope.GetCases(false);
            });

            if (callingPage != "dashboard") {
                // Get list of employer for current user
                lookupService.GetEmployersForUser().then(function (response) {
                    $scope.EmployerList = response;
                });

                //Get Feature 'MultiEmployerAccess'
                lookupService.GetEmployerFeatures().then(function (response) {
                    $scope.IsTPA = response.MultiEmployerAccess;
                });
            }
        };
        
        $scope.GetCases = function (doPaging) {
            $scope.CasesFilter.PageNumber = 1;
            $scope.CasesFilter.ListType = $scope.ListType;
            caseListManager.List($scope, doPaging, $scope.ErrorHandler);            
        };
        $scope.UpdateCaseSort = function (sortBy) {
            caseListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };
        $scope.LoadMoreCases = function () {
            caseListManager.LoadMore($scope, $scope.ErrorHandler);
        };
        $scope.GetViewCaseUrl = function (caseId) {
            window.location.href = "/Cases/" + caseId + "/View";
        };

        $scope.CancelRedirection = function ($event) {
            $event.stopPropagation();                      
        }
}]);

var downloadZipCtrlInstance = function ($scope, $modalInstance, $http) {
    
        $http({
            method: 'POST',
            url: '/ExportData/GetTotalLinks',
        }).then(function (response) {
            $scope.Links = response.data;
        })

        $scope.GetExportLink = function (key) {

            $http({
                method: 'POST',
                url: '/ExportData/GetExportLink/?key=' + key,
            }).then(function (response) {
                window.open(response.data.url, "_blank");
            })
        }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };    
};