//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('ForgotPasswordCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.openForgotPasswordModal = function () {
        var forgotPasswordModalInstance = $modal.open({
            templateUrl: 'ForgotPasswordModal.html',
            controller: 'ForgotPasswordModalCtrl',
        });
    };
}]);

