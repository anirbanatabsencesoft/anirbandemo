angular
    .module('App.Controllers')
    .controller("BulkDownloadsForEmployeeCtrl", ['$scope', 'caseService', function ($scope, caseService) {
        $scope.employeeId = '';
        $scope.selectAllCases = false;     
        $scope.employeeCases = [];
        $scope.casesBulkDownloadStatus = [];
        $scope.GetCaseExportFormat = AbsenceSoft.Enums.CaseExportFormat.GetDescription;
        $scope.GetCaseExportProcessingStatus = AbsenceSoft.Enums.CaseExportProcessingStatus.GetDescription;
        $scope.AbsenceSoft = AbsenceSoft;
        $scope.timeoutId = undefined;

        $scope.InitBulkDownloadsForEmployee = function (employeeId) {
            $scope.employeeId = employeeId;

            if (AbsenceSoft.hasStringValue($scope.employeeId)) {
                getEmployeeCases();
                getCaseBulkDownloadStatus();                
            }

            $scope.caseExportProcessingStatus = AbsenceSoft.Enums.CaseExportProcessingStatus;
            $scope.caseExportFormat = AbsenceSoft.Enums.CaseExportFormat;
        };

        function getEmployeeCases () {
            caseService.GetEmployeeCases($scope.employeeId).then(function (response) {
                $scope.employeeCases = response.Cases;
                setAllCases(false);
            }, function () {
                $scope.employeeCases = [];
            });
        }

        $scope.SelectAllCases = function (event) {
            if (AbsenceSoft.hasValue(event) && AbsenceSoft.hasValue(event.target))
                setAllCases(event.target.checked ? true : false);
        };

        function setAllCases(shouldCheck) {
            for (var i = 0; i < $scope.employeeCases.length; i++) {
                $scope.employeeCases[i].IsChecked = shouldCheck;
            }
        }

        $scope.CheckAllSelected = function (index, event) {
            if (!(AbsenceSoft.hasValue(event) && AbsenceSoft.hasValue(event.target)))
                return;

            $scope.employeeCases[index].IsChecked = event.target.checked ? true : false;
            var isAllChecked = true;

            for (var i = 0; i < $scope.employeeCases.length; i++) {
                if (!$scope.employeeCases[i].IsChecked) {
                    isAllChecked = false;
                    break;
                }
            }

            $scope.selectAllCases = isAllChecked;
        };

        function getCaseBulkDownloadStatus() {
            caseService.GetCaseBulkDownloadStatus($scope.employeeId).then(function (data) {
                if (AbsenceSoft.hasValue(data) && data.length > 0) {
                    $scope.casesBulkDownloadStatus = data;
                    checkIfPendingOrProcessingRecord();
                }
            }, function () {
                $scope.casesBulkDownloadStatus = [];
            });
        }

        function checkIfPendingOrProcessingRecord() {
            var isPendingOrProcessingAvailable = false;

            for (var i = 0; i < $scope.casesBulkDownloadStatus.length; i++) {
                if ($scope.casesBulkDownloadStatus[i].Status === $scope.caseExportProcessingStatus.Pending || $scope.casesBulkDownloadStatus[i].Status === $scope.caseExportProcessingStatus.Processing) {
                    isPendingOrProcessingAvailable = true;
                    break;
                }
            }

            if (isPendingOrProcessingAvailable) {
                if (AbsenceSoft.hasValue($scope.timeoutId))
                    clearTimeout($scope.timeoutId);

                $scope.timeoutId = setTimeout(getCaseBulkDownloadStatus, 3000);
            }
        }

        $scope.BulkDownloadCases = function (downloadType) {
            var selectedCases = '';
            var downloadTypeValue = AbsenceSoft.Enums.CaseExportFormat.GetDescription(downloadType);

            if (!AbsenceSoft.hasStringValue(downloadTypeValue))
                return;

            for (var i = 0; i < $scope.employeeCases.length; i++) {
                if ($scope.employeeCases[i].IsChecked) {
                    selectedCases += $scope.employeeCases[i].CaseId + ',';
                }
            }

            if (!AbsenceSoft.hasStringValue(selectedCases)) {
                alert("Please select at least one case to export");
                return;
            }

            selectedCases = selectedCases.slice(0, selectedCases.length - 1);
            var model = { EmployeeId: $scope.employeeId, DownloadType: downloadTypeValue, CaseIds: selectedCases };

            caseService.BulkDownloadCases(model).then(function (data) {
                if (AbsenceSoft.hasValue(data)) {
                    getCaseBulkDownloadStatus();
                }
            });
        };
    }]);