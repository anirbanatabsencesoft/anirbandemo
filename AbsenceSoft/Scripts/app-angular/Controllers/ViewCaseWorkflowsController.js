﻿angular
.module('App.Controllers')
.controller('ViewCaseWorkflowsCtrl', ['$scope', 'lookupService', 'workflowService', 
function ($scope, lookupService, workflowService) {
    $scope.Workflows = [];
    $scope.CustomerWorkflows = [];
    $scope.CaseWorkflowCriteria = {
        SortDirection: 1,
        SortDir: 'asc',
        SortBy: 'CaseId',
        CaseId: $scope.CaseId,
        DoPaging: false,
        LoadMore: true,
        PageSize: 20,
        PageNumber: 1
    }
    $scope.WorkflowCriteria = [];
    $scope.InitViewCaseWorkflows = function (caseId, customerId, employerId) {
        $scope.CaseId = caseId;
        $scope.CaseWorkflowCriteria.CaseId = caseId;
        $scope.CustomerID = customerId;
        $scope.EmployerId = employerId;
        $scope.ListWorkflows(false);
        $scope.ListAllWorkflows();
    };

    $scope.ListAllWorkflows = function () {
        workflowService.ListAllWorkflows($scope.EmployerId).$promise.then(function (customerWorkflows) {
            $scope.CustomerWorkflows = customerWorkflows;
        });
    }

    $scope.ListWorkflows = function (doPaging) {
        $scope.CaseWorkflowCriteria.DoPaging = doPaging;
        /// Hack to the eventType and status are ints
        if ($scope.CaseWorkflowCriteria.EventType)
            $scope.CaseWorkflowCriteria.EventType = $scope.CaseWorkflowCriteria.EventType * 1;
        else
            $scope.CaseWorkflowCriteria.EventType = null;

        if ($scope.CaseWorkflowCriteria.Status)
            $scope.CaseWorkflowCriteria.Status = $scope.CaseWorkflowCriteria.Status * 1;
        else
            $scope.CaseWorkflowCriteria.Status = null;

        workflowService.ListWorkflowInstances($scope.CaseWorkflowCriteria).$promise.then(function (data, status) {
            if (data.Results !== null && data.Results !== '' && JSON.stringify(data.Results) !== '[]') {
                $scope.CaseWorkflowCriteria.TotalRecords = data.Total;
                $scope.CaseWorkflowCriteria.LoadMore = true;
                if (doPaging) {
                    angular.forEach(data.Results, function (result) {
                        $scope.Workflows.push(result);
                    });
                } else {
                    $scope.Workflows = data.Results;
                }
            } else {
                $scope.Workflows = [];
                $scope.CaseWorkflowCriteria.LoadMore = false;
                $scope.CaseWorkflowCriteria.TotalRecords = 0;
            }
        })
    }

    $scope.ResetWorkflowList = function () {
        $scope.Workflows = [];
        $scope.CaseWorkflowCriteria.PageNumber = 1;
        $scope.CaseWorkflowCriteria.LoadMore = true;
    }

    $scope.FilterWorkflows = function () {
        $scope.ResetWorkflowList();
        $scope.ListWorkflows(false);
    }

    $scope.LoadMoreCommunications = function () {
        var totalPages = Math.ceil($scope.CaseWorkflowCriteria.TotalRecords / $scope.CaseWorkflowCriteria.PageSize);
        if (totalPages > 1 && $scope.CaseWorkflowCriteria.LoadMore && $scope.CaseWorkflowCriteria.PageNumber < totalPages) {
            $scope.CaseWorkflowCriteria.PageNumber++;
            $scope.ListWorkflows(true);
        }
    }

    $scope.UpdateWorkflowSort = function (sortBy) {
        $scope.ResetWorkflowList();
        
        if ($scope.CaseWorkflowCriteria.SortBy === sortBy)
            $scope.CaseWorkflowCriteria.SortDir = $scope.CaseWorkflowCriteria.SortDir === 'asc' ? 'desc' : 'asc';
        else {
            $scope.CaseWorkflowCriteria.SortBy = sortBy;
            $scope.CaseWorkflowCriteria.SortDir = 'asc';
        }

        $scope.ListWorkflows(false);

    }

    $scope.PauseWorkflow = function (workflow) {
        var data = $scope.CreateWorkflowStatusData(workflow);
        workflowService.PauseWorkflowInstance(data).$promise.then($scope.UpdateCaseWorkflows(workflow));
    }

    $scope.ResumeWorkflow = function (workflow) {
        var data = $scope.CreateWorkflowStatusData(workflow);
        workflowService.ResumeWorkflowInstance(data).$promise.then($scope.UpdateCaseWorkflows(workflow));
    }

    $scope.CancelWorkflow = function (workflow) {
        var data = $scope.CreateWorkflowStatusData(workflow);
        workflowService.CancelWorkflowInstance(data).$promise.then($scope.UpdateCaseWorkflows(workflow));
    }

    $scope.UpdateCaseWorkflows = function (workflow) {
        $scope.ListAllWorkflows();
        $scope.SetWorkflowText(workflow)
    }

    $scope.CreateWorkflowStatusData = function (workflow) {
        return {
            Id: workflow.Id
        };
    }

    $scope.SetWorkflowText = function (workflow) {
        return function (updatedWorkflow) {
            switch (updatedWorkflow.Status) {
                case 0:
                    workflow.Status = 'Pending';
                    break;
                case 1:
                    workflow.Status = 'Running';
                    break;
                case 2:
                    workflow.Status = 'Paused';
                    break;
                case 3:
                    workflow.Status = 'Completed';
                    break;
                case 4:
                    workflow.Status = 'Canceled';
                    break;
                case 5:
                    workflow.Status = 'Failed';
                    break;
            }
        }
    }

    $scope.GetWorkflowCriteria = function () {
        var workflowInfo = {
            WorkflowId: $scope.NewWorkflow.Id,
            CaseId: $scope.CaseId
        }

        workflowService.GetWorkflowCriteria(workflowInfo).$promise.then(function (workflowCriteria) {
            $scope.WorkflowCriteria = workflowCriteria;
            $('#criteriaModal').modal('show');
        });
    }

    $scope.StartWorkflow = function () {
        var workflowInfo = {
            WorkflowId: $scope.NewWorkflow.Id,
            CaseId: $scope.CaseId,
            Criteria: $scope.WorkflowCriteria
        };
        workflowService.StartCaseWorkflow(workflowInfo).$promise.then(function (workflowInstance) {
            $('#criteriaModal').modal('hide');
            $scope.NewWorkflow = '';
            $scope.ListWorkflows(false);
        });
    }

}]);