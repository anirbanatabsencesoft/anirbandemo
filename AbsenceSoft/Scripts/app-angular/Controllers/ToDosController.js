//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('ToDoCtrl', ['$scope', '$http', '$window', 'toDoListManager', '$modal', '$log', 'todoModalService', 'todoService', 'administrationService', 'lookupService', 
    function ($scope, $http, $window, toDoListManager, $modal, $log, todoModalService, todoService, administrationService, lookupService) {
        //$scope.ListType = "Individual";
        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.Error = response;
        };

        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };

        //#region controller initialization logic
        $scope.init = function (callingPage) {

            administrationService.GetUsersList().then(function (data) {
                if (data != null) {
                    $scope.UsersList = data;
                }
            });

            // $scope.NextToDo = toDoListManager.GetNextToDo();
            var infiniteScrollOnBody = callingPage == "index";

            if (callingPage == "index") {
                $scope.ListType = "Individual";
            }

            toDoListManager.Init($scope, infiniteScrollOnBody);
            if (infiniteScrollOnBody) {
                //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                        toDoListManager.LoadMore($scope, $scope.ErrorHandler);
                    }
                });
            }

            if (callingPage != "dashboard") {
                // Get list of employer for current user
                lookupService.GetEmployersForUser().then(function (response) {
                    $scope.EmployerList = response;
                });

                //Get Feature 'MultiEmployerAccess'
                lookupService.GetEmployerFeatures().then(function (response) {
                    $scope.IsTPA = response.MultiEmployerAccess;
                });
            }
            
            $scope.$watch('ListType', function (newVal, oldVal) {
                if (newVal === oldVal) return;
                $scope.GetToDos(false);
            });
            $scope.$watch('ToDosFilter.Due', function (newVal, oldVal) {
                if (newVal === oldVal) return;
                toDoListManager.List($scope, false, $scope.ErrorHandler);
            });
            $scope.$watch('ToDosFilter.AssignedToName', function (newVal, oldVal) {
                if (newVal === oldVal) return;
                toDoListManager.List($scope, false, $scope.ErrorHandler);
            });            

        };
        //#endregion controller initialization logic

        //#re-assign todos
        $scope.reassigntodos = { UserId: '', Todos: [] };
        function ApplyTrue (todo) {
            todo.ApplyStatus = true;
        }
        function ApplyFalse(todo) {
            todo.ApplyStatus = false;
        }
        $scope.ReassignTodos = function () {
            if ($scope.UserId) {
                $scope.reassigntodos.UserId = $scope.UserId;
                todoService.ReassignTodos($scope.reassigntodos).then(function (response) {
                    //provide status message to user on success / failure                
                    $scope.GetToDos();
                    $scope.UncheckAll();
                });
            } else {
                $scope.showNoUserSelectedWarning = true;
            }
        };
        $scope.CheckAll = function () {
            $scope.reassigntodos.Todos.forEach(ApplyTrue);
            $('.todo-checkbox').each(function () {
                this.checked = true;
            });
        };
        $scope.UncheckAll = function () {
            $scope.reassigntodos.Todos.forEach(ApplyFalse);
            $('.todo-checkbox').each(function () {
                this.checked = false;
            });
        };
        //#end re-assign cases

        $scope.GetToDos = function (doPaging) {
            $scope.ToDosFilter.PageNumber = 1;
            $scope.ToDosFilter.ListType = $scope.ListType;
            toDoListManager.List($scope, doPaging, $scope.ErrorHandler);
        };
        $scope.UpdateToDoSort = function (sortBy) {
            toDoListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };
        $scope.LoadMoreToDos = function () {
            toDoListManager.LoadMore($scope, $scope.ErrorHandler);
        };
        $scope.OpenTodoModal = function (toDo) {
            todoModalService.OpenTodoModal($scope, toDo, $scope.ToDos, $scope.ToDosFilter, false);
            //todoModalService.OpenTodoModal($scope.NextToDo);
        };
    }]);