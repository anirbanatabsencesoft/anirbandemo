﻿angular
.module('App.Controllers').controller('CaseAssignmentCtrl', ['$scope', '$window', 'administrationService', 'lookupService', 'caseAssignmentTypeService',
function ($scope, $window, administrationService, lookupService, caseAssignmentTypeService) {
    $scope.form = {
        submitted: false
    };

    $scope.InitList = function (caseAssignmentTypes) {
        $scope.AssignmentTypes = caseAssignmentTypes;
        init();
    }

    $scope.InitEdit = function (caseAssignmentType) {
        $scope.AssignmentType = {
            Id: caseAssignmentType.Id,
            CustomerId: caseAssignmentType.CustomerId,
            EmployerId: caseAssignmentType.EmployerId,
            Name: caseAssignmentType.Name,
            MaxAllowedPerCase: caseAssignmentType.MaxAllowedPerCase
        };
        init();
    }

    init = function () {
        administrationService.GetBasicInfo().then(function (data) {
            $scope.BasicInfo = data;
        });
        lookupService.GetEmployersForUser().then(function (employers) {
            $scope.Employers = employers;
        });
    }

    $scope.GoToAssignmentTypeEdit = function (caseAssignmentRole) {
        window.location.href = '/CaseAssignment/' + caseAssignmentRole.CustomerId + '/TypeEdit/' + caseAssignmentRole.Id;
    }

    $scope.SaveCaseAssignmentType = function () {
        $scope.form.submitted = true;
        if ($scope.editCaseAssignmentType.$valid) {
            caseAssignmentTypeService.Save($scope.AssignmentType).then(function (caseAssignmentType) {
                $scope.form.submitted = false;
                window.location.href = '/CaseAssignment/' + caseAssignmentType.CustomerId + '/Types';
            })
        }
    }

    $scope.DeleteCaseAssignmentType = function () {
        caseAssignmentTypeService.Delete($scope.AssignmentType.CustomerId, $scope.AssignmentType.Id).then(function (success) {
            if (success) {
                window.location.href = '/CaseAssignment/' + $scope.AssignmentType.CustomerId + '/Types';
            } else {
                var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "Unable to delete case assignment type" });
            }
        })
    }

    $scope.Back = function () {
        $window.history.back();
    }
}]);