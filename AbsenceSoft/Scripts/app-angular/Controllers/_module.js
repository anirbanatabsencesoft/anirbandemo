﻿(function () {
    'use strict';

    angular.module('App.Controllers', [
        'App.Directives',
        'ui.bootstrap',
        'App.Case.OtherQuestions'
    ]);
})();
