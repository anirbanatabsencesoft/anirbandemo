//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployerAttachmentsCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.CreationDate = null;
    $scope.CustomerId = null;
    $scope.NewAttachments = [];
    $scope.Attachments = [];
    $scope.IsUploadInProgress = false;
    $scope.ShowUploadPanel = true;
    $scope.isDisabled = false;
    $scope.ShowCancel = true;

    $scope.$on('UploadLogo', function (event, args) {
        $scope.EmployerId = args.EmployerId;
        $scope.UploadFiles();
    });

    $scope.$on('SetLogo', function (event, args) {
        $scope.ShowUploadPanel = false;
        $scope.EmployerId = args.EmployerId;
        $scope.Attachments.push({
            Id: '/Employer/Logo?employerId=' + args.EmployerId,
            FileName: "Logo",
        })
    });

    $scope.init = function (customerId, creationDate) {
        $scope.CustomerId = customerId;
        $scope.ShowCancel = true;
        //$scope.GetAttachments();
        $scope.CreationDate = creationDate;
        $(function () {
            $('#customerLogo').fileupload({
                dataType: 'json',
                autoUpload: false,
                sequentialUploads: true,
                singleFileUploads: true,
                replaceFileInput: false
            });
        });


        $('#customerLogo').bind('fileuploadadd', function (e, data) {
            // Add the files to the list
            $scope.ShowUploadPanel = false;
            numFiles = $scope.NewAttachments.length

            if (data.files.length > 0)
                $scope.isDisabled = false;

            for (var i = 0; i < data.files.length; ++i) {
                var file = data.files[i];
                // .$apply to update angular when something else makes changes
                $scope.$apply(
                    $scope.NewAttachments.push({
                        FileName: file.name,
                        FileData: data,
                        Description: "",
                        AttachmentType: 2,
                        AttachmentTypeName: "Communication",
                        IsUploading: false,
                        AttachmentId: $scope.NewAttachments.length + 1,
                        Message: "",
                        ShowCancel: false,
                        ShowMessage: false
                    }));
            }
        });
    };

    $scope.GetAttachments = function () {
        $http({
            method: 'GET',
            url: '/SignUp/Logo'
        }).then(function (response) {
            $scope.Attachments = response.data;
        });
    };

    $scope.Delete = function (attachment) {
        $http.delete('/Employer/DeleteLogo?employerId=' + $scope.EmployerId).then(function (data) {
            if (data.data.IsSuccess) {
                for (var i = 0; i <= $scope.Attachments.length - 1; i++) {
                    if ($scope.Attachments[i].Id == attachment.Id) {
                        $scope.Attachments.splice(i, 1);
                    }
                }
                $scope.ShowUploadPanel = true;
                $scope.isDisabled = false;
            }
            else {
                var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.data.Error });
            }
        });
    };

    var getNewAttachmentById = function (id) {
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            if ($scope.NewAttachments[i].AttachmentId == id)
                return $scope.NewAttachments[i];
        }
        return undefined;
    };

    var resetUploadInProgress = function () {
        var anyFileUploading = false;
        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            if ($scope.NewAttachments[i].IsUploading) {
                anyFileUploading = true;
                break;
            }
        }
        $scope.IsUploadInProgress = anyFileUploading;
        $scope.ShowUploadPanel = anyFileUploading;
    };

    $scope.UploadFiles = function () {

        if ($scope.NewAttachments.length <= 0) {
            $scope.$emit('AfterUploadLogo', {});
            return;
        }

        $scope.ShowUploadPanel = false;
        $scope.ShowCancel = false;
        $scope.FileUploadInProgress = true;

        for (var i = 0; i < $scope.NewAttachments.length; i++) {
            var attachment = $scope.NewAttachments[i];

            attachment.FileData.formData = {
                AttachmentType: attachment.AttachmentType,
                Description: attachment.Description,
                Id: $scope.CustomerId,
                employerId: $scope.EmployerId,
                AttachmentId: attachment.AttachmentId
            };

            $scope.NewAttachments[i].Message = "Uploading " + attachment.FileName;
            $scope.NewAttachments[i].IsUploading = true;
            $scope.NewAttachments[i].ShowMessage = true;

            attachment.Message = "Uploading " + attachment.FileName;
            attachment.IsUploading = true;
            attachment.ShowMessage = true;
            attachment.FileData.submit({ async: false })
                .then(function (response) {
                    var result = response.data;
                    if (result.IsSuccess) {

                        $scope.FileUploadInProgress = false;
                        //$scope.$emit('AfterUploadLogo', {});

                        $scope.$apply(
                            $scope.Attachments.push({
                                Id: result.LogoURL + '?employerId=' + $scope.EmployerId,
                                FileName: "Logo",
                            })
                        );
                    }
                },
                function (jqXHR) {
                    if (jqXHR.indexOf("Maximum request length exceeded") != -1)
                        message = "uploaded file reached the maximum upload file size limit & cannot be uploaded";
                    else
                        message = "Some error occured while uploading file";

                    var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: message });
                })

            attachment.IsUploading = false;
            attachment.ShowMessage = false;
            resetUploadInProgress();

        }

        $scope.NewAttachments.length = 0;
    };

    $scope.CancelFileUpload = function (attachment) {
        $scope.ShowUploadPanel = true;
        $scope.isDisabled = false;
        var position = $.inArray(attachment, $scope.NewAttachments);
        if (~position) $scope.NewAttachments.splice(position, 1);
    }

}]);