﻿angular
.module('App.Controllers')
.controller('CasesLastReviewedCtrl', ['$scope', '$http', 'caseService',
    function ($scope, $http, caseService) {
        $scope.LastViewedCriteria = {
            PageSize: 4,
            PageNumber: 1
        }
        $scope.LastViewedTotal = 100;

        $scope.InitList = function () {
            $scope.LoadLastViewed(false);
        }

        $scope.LoadMoreLastViewed = function () {
         
            var totalPages = $scope.LastViewedTotal / $scope.LastViewedCriteria.PageSize;
            if (totalPages > 1 && $scope.LastViewedCriteria.PageNumber < totalPages) {
                $scope.LastViewedCriteria.PageNumber++;
                $scope.LoadLastViewed(true)
            }
        }

        $scope.LoadLastViewed = function (doPaging) {
            caseService.LastViewedList($scope.LastViewedCriteria).then(function (lastViewed) {
                if (doPaging) {
                    $scope.LastViewed = $scope.LastViewed.concat(lastViewed.Results);
                } else {
                    $scope.LastViewed = lastViewed.Results;
                }

            });
        }

        $scope.GoToCase = function (lastViewed) {            
                window.location.href = '/Cases/' + lastViewed.Id + '/View';            
        }
    }]);