//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('SignUpCtrl', ['$http','$filter', '$location', '$scope', 'signUpService', 'lookupService',
    function ($http,$filter, $location, $scope, signUpService, lookupService) {

        $scope.DomainLabel = $location.host();

        $scope.SignUpData = {
            FirstName: "", LastName: "", Password: "", ConfirmPassword: "", Email: "",
            Name: "", SubDomain: "", Address1: "", Address2: "", Country: "US", State: "", City: "", PostalCode: "", Phone: "", Fax: "",
            Employer: {
                SignatureName: "", MaxEmployees: null, Address1: "", Address2: "", City: "", State: "", PostalCode: "", Country: "US",
                Url: "", Phone: "", Fax: "",  EnableSpouseAtSameEmployerRule: "",
                Enable50In75MileRule: "", FMLPeriodType: "", ResetMonth: "", ResetDayOfMonth: "", FTWeeklyWorkHours: "",
                AllowIntermittentForBirthAdoptionOrFosterCare: "", IsPubliclyTradedCompany: "",
                Holidays:[]
            }
        }

        $scope.init = function (model) {
            lookupService.FMLPeriodTypes()
                .$promise.then(function (results) {
                    $scope.FMLPeriodTypes = results;
                }
                , $scope.ErrorHandler
                );

            lookupService.GetEmployerFeatures().then(function (response) {
                $scope.HasEmployeeSelfService = response.EmployeeSelfService;
            });

            $scope.SignUpData.Employer = model;
            var length = model.Holidays.length;
            for (i = 0; i < length; i++) {
                model.Holidays[i]['Checked'] = true
            }

            $scope.SignUpData.Employer.Phone = $scope.SignUpData.Employer.Phone == "" ? "" : $filter("tel")($scope.SignUpData.Employer.Phone)
            $scope.SignUpData.Employer.Fax = $scope.SignUpData.Employer.Fax == "" ? "" : $filter("tel")($scope.SignUpData.Employer.Fax)

            if ($scope.SignUpData.Employer.MaxEmployees == undefined || $scope.SignUpData.Employer.MaxEmployees == null)
                $scope.SignUpData.Employer.MaxEmployees = 4999;
        };

        $scope.CreationDate = null;
        $scope.CaseId = null;
        $scope.NewAttachments = [];
        $scope.Attachments = [];
        $scope.IsUploadInProgress = false;

        $scope.CompanySize = [{ Id: 999, Text: "1 to 999" }, { Id: 4999, Text: "1000 to 4999" }, { Id: null, Text: "5000 Above" }];
       
        $scope.SetDomainName = function () {
            $scope.SignUpData.SubDomain = $scope.SignUpData.Name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
        }

        $scope.SetDayForSelectedMonth = function (monthNumber) {
            var year = (new Date).getFullYear();
            var month = parseInt(monthNumber);
            $scope.TotalDays = new Date(year, month, 0).getDate();
            var Days = [];
            for (i = 1; i <= $scope.TotalDays; i++) {
                Days.push({ "items": i });
            }
            $scope.Days = Days;
        }

        $scope.$watch('SignUpData.PostalCode', function () {
            $scope.PostalCodeValidationIn2();
        });

        $scope.$watch('SignUpData.Country', function () {
            $scope.PostalCodeValidationIn2();
        });

        $scope.$watch('SignUpData.Employer.PostalCode', function () {
            $scope.PostalCodeValidationIn3();
        });

        $scope.$watch('SignUpData.Employer.Country', function () {
            $scope.PostalCodeValidationIn3();
        });

        $scope.PostalCodeValidationIn2 = function () {
            if ($scope.SignUpData.PostalCode == null || $scope.SignUpData.PostalCode == '' || $scope.SignUpData.PostalCode == undefined)
                return;

            if ($scope.SignUpData.Country == null || $scope.SignUpData.Country == '' || $scope.SignUpData.Country == undefined)
                return;

            if ($scope.SignUpData.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.SignUpData.PostalCode)) {
                    $scope.frmSignUpStep1.PostalCode.$setValidity('valid', false);
                    return;
                }
            }

            $scope.frmSignUpStep1.PostalCode.$setValidity('valid', true);
        };

        $scope.PostalCodeValidationIn3 = function () {
            if ($scope.SignUpData.Employer.PostalCode == null || $scope.SignUpData.Employer.PostalCode == '' || $scope.SignUpData.Employer.PostalCode == undefined)
                return;

            if ($scope.SignUpData.Employer.Country == null || $scope.SignUpData.Employer.Country == '' || $scope.SignUpData.Employer.Country == undefined)
                return;

            if ($scope.SignUpData.Employer.Country == 'US') {
                var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
                if (!regEx.test($scope.SignUpData.Employer.PostalCode)) {
                    $scope.frmSignUpStep3.PostalCode.$setValidity('valid', false);
                    return;
                }
            }
            $scope.frmSignUpStep3.PostalCode.$setValidity('valid', true);
        };

        $scope.Error = null;

        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.PasswordRequirements = /^.*(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[\d\W\s]).*$/;
        $scope.PhoneRequirements = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        $scope.PostalCodeRquirements = /^[0-9]*$/;
       
        $scope.SaveCustomerWithConfiguration = function (isValid) {

            if (isValid)
            {
                var checked = $("#chkTerms").is(":checked");
                if (!checked) {
                    isValid = false;
                    $("#hError").text("You must accept the terms and condition to proceed")
                }
            }
           
            if (isValid) {

                signUpService.CreateAccount($scope.SignUpData).then(function (data) {
                    if (data.Error == null || data.Error == "") {
                        if (data.Id != null || data.Id != "") {
                            $scope.submitted = false;
                            // window.location.href = "/signup/customer";
                            window.location.href = "/AccountSummary/" + data.Id;
                        }
                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.Error });
                        $scope.isDisabled = false;
                    }

                    $scope.submitted = false;

                }, $scope.ErrorHandler);
            }
            else {
                var firstInvalid = $("#frmSignUpStep1").find(".ng-invalid:first");
                var scrollTo = firstInvalid.offset().top / 2;
                $('html, body').animate({ scrollTop: scrollTo }, 1000);
                $scope.isDisabled = false;
            }
        }

        $scope.SaveCustomer = function (isValid) {
            if (isValid) {
                signUpService.CreateCustomer($scope.SignUpData.Customer).then(function (data) {
                    if (data.Error == null || data.Error == "") {
                        if (data.Id != null || data.Id != "") {
                            $scope.submitted = false;
                            sessionStorage.removeItem("subDomain");
                            window.location.href = "/signup/employer";
                        }
                    }
                    else {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.Error });
                    }
                    $scope.submitted = false;
                }, $scope.ErrorHandler);
            }
        }

        $scope.SaveEmployer = function (isValid) {
            if (isValid) {
                var length = $scope.SignUpData.Employer.Holidays.length;
                var holidays = $scope.SignUpData.Employer.Holidays;
                
                var holidaysAfterFilter = [];
                arr = jQuery.grep(holidays, function (n, i) {
                    if (n.Checked == true) {
                        holidaysAfterFilter.push(n)
                    }
                });

                $scope.SignUpData.Employer.Holidays = holidaysAfterFilter;
                
                signUpService.CreateEmployer($scope.SignUpData.Employer).then(function (data) {
                    $scope.SignUpData.Employer.Holidays = holidays;
                    if (data.Error == null || data.Error == "") {
                        if (data.Id != null || data.Id != "") {
                            $scope.submitted = false;
                            // window.location.href = "/employees/create";
                            // window.location.href = "/AccountSummary/" + data.UserId;
                            $("#RegCompleted").modal('show');
                        }
                    }
                    else {
                        $scope.SignUpData.Employer.Holidays = holidays;
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.Error });
                    }
                    $scope.submitted = false;
                    $scope.isDisabled = false;
                }, function () {
                    $scope.SignUpData.Employer.Holidays = holidays;
                });
            }

            $scope.isDisabled = false;
        }

        $scope.CancelRegistration = function () {
            sessionStorage.removeItem("Phone");
            sessionStorage.removeItem("Company");
            sessionStorage.removeItem("SubDomain");
            window.location.href = "/";
        }

        $scope.ApproveDenyUser = function (model) {
            var url;
            if (model.IsApprove) {
                $scope.Action = "Approve";
                url = "/signup/verify/" + model.CustomerId + "/" + model.ResetKey + "/approve";
            }
            else {
                $scope.Action = "Deny";
                url = "/signup/verify/" + model.CustomerId + "/" + model.ResetKey + "/deny";
            }

            if (model.IsSuccess) {
                $http({
                    method: 'POST',
                    url: url
                }).then(function (response) {
                    if (response.data.success) {
                        $scope.ErrorMessage = "";
                        if (model.IsApprove) {
                            $scope.SuccessMessage = "User approved successfully."
                        }
                        else {
                            $scope.SuccessMessage = "User account removed successfully."
                        }
                    }
                    else {
                        $scope.SuccessMessage = "";
                        if(response.data.error != undefined && response.data.error != null)
                            $scope.ErrorMessage = response.data.error
                        else
                            $scope.ErrorMessage = "Some error occured while approving user. Please try again later."
                    }
                });
            }
            else {
                $scope.Message = model.Message;
            }
        }

        $scope.AccountSummary = function (model) {
            $scope.AccountInfo = model;
        }

        $scope.GoTo = function (action) {
            if (action == "CreateEmployee") {
                window.location.href = "/Employees/Edit";
            }
            else if (action == "BatchUpload") {
                window.location.href = "/administration";
            }
            else {
                window.location.href = "/";
            }
        }

    }]);