//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EECustomFieldsCtrl', ['$scope', '$http', 'administrationService', '$window',
    function ($scope, $http, administrationService, $window) {

        $scope.Employers = {};
        $scope.Fields = {};

        $scope.EditField = {
            Id: '',
            EmployerId: '',
            Name: '',
            Description: '',
            Label: '',
            HelpText: '',
            DataType: '',
            ValueType: '',
            ListValues: [],
            ListValuesText: '',
            FieldOrder: '',
            IsRequired: false,
            IsCollectedAtIntake: true,
            Target: ''
        };

        $scope.NewField = {
            EmployerId: '',
            Name: '',
            Description: '',
            Label: '',
            HelpText: '',
            DataType: '',
            ValueType: 1,
            ListValues: [],
            ListValuesText: '',
            FieldOrder: '',
            IsRequired: false,
            IsCollectedAtIntake: true,
            Target: ''
        };

        $scope.IsNewField = false;
        $scope.BadListItem = false;

        $scope.form = $scope.form || {};
        $scope.form.submitted = false;
        $scope.form.IsAnyEmployerSelected = false;

        $scope.Init = function (model) {
            $scope.Employers = model.Employers;

            $scope.EmployersForSelect2 = [];
            angular.forEach($scope.Employers, function (value, key) {
                $scope.EmployersForSelect2.push({ id: value.Id, text: value.Name });
            });

            var firstTime = true;
            angular.forEach($scope.Employers, function (emp, index) {
                if (emp.Fields != null && emp.Fields.length > 0) {
                    firstTime = false;
                }
            });

            if (firstTime) {
                $scope.InitNewField();
            }
            else {
                if ($scope.Employers.length == 1) {
                    $scope.EditField.EmployerId = $scope.Employers[0].Id;
                    $scope.Fields = $scope.Employers[0].Fields;
                }
            }
        };

        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.submitted = false;
            $scope.Error = response;
        };

        $scope.DeleteField = function () {
            var url = '/Administration/' + $scope.EditField.EmployerId +'/DeleteEECustomField/' + $scope.EditField.Id;
            $http.delete(url).then(function () {
                $window.location.href = '/Administration'
            }, function () {
                noty({ timeout: 5000, layout: 'topRight', type: 'error', text: "Unable to delete field" });
            });
        }

        $scope.UpdateField = function (IsValid) {
            if (IsValid) {

                $scope.form.submitted = false;
                $scope.IsNewField = false;
                $scope.EditField.Label = $scope.EditField.Name;

                if ($scope.EditField.DataType == '8') {
                    $scope.EditField.ValueType = '1';
                }

                if ($scope.Employers.length == 1) {
                    $scope.EditField.EmployerId = $scope.Employers[0].Id;
                }

                $http({
                    method: 'POST',
                    url: '/Administration/UpdateEECustomField',
                    data: $scope.EditField
                }).then(function (response) {
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        $scope.Employers = response.data.Employers;
                        $scope.EditField = {
                            Id: '',
                            EmployerId: '',
                            Name: '',
                            Description: '',
                            Label: '',
                            HelpText: '',
                            DataType: '',
                            ValueType: '',
                            ListValues: [],
                            ListValuesText: '',
                            FieldOrder: '',
                            IsRequired: false,
                            IsCollectedAtIntake: true,
                            Target: ''
                        };

                        if ($scope.Employers.length == 1) {
                            $scope.EditField.EmployerId = $scope.Employers[0].Id;
                            $scope.Fields = $scope.Employers[0].Fields;
                        }

                        $scope.form.frmEditEECustomFields.$setPristine();
                        var n = noty({ timeout: 5000, layout: 'topRight', type: 'success', text: "Field successfully updated" });
                    }
                });
            }
        };

        $scope.CreateField = function (IsValid) {
            $scope.NewField.EmployerId = [];

            if ($scope.EmployersForSelect2.length > 1) {
                var employer = $("#NewEmployer").select2("data");;
                if (employer === undefined || employer.length == 0) {
                    $("#select2Error").show();
                    $("#NewEmployer").addClass("has-error")
                    IsValid = false;
                }
                else {
                    $("#select2Error").hide();
                    $("#NewEmployer").removeClass("has-error");
                }
            }
            else
            {
                $scope.NewField.EmployerId.push($scope.EmployersForSelect2[0].id);
            }
            if (IsValid) {

                $scope.form.submitted = false;
                $scope.NewField.Label = $scope.NewField.Name;


                angular.forEach(employer, function (value, key) {
                    $scope.NewField.EmployerId.push(value.id);
                })

                if ($scope.NewField.DataType == '8') {
                    $scope.NewField.ValueType = '1';
                }

                $http({
                    method: 'POST',
                    url: '/Administration/AddEECustomField',
                    data: $scope.NewField
                }).then(function (response) {
                    $scope.IsNewField = false;
                    if (response.data != null && response.data != '' && JSON.stringify(response.data) != '[]') {
                        $scope.Employers = response.data.Employers;
                        $scope.EditField = {
                            Id: '',
                            EmployerId: '',
                            Name: '',
                            Description: '',
                            Label: '',
                            HelpText: '',
                            DataType: '',
                            ValueType: '',
                            ListValues: [],
                            ListValuesText: '',
                            FieldOrder: '',
                            IsRequired: false,
                            IsCollectedAtIntake: true,
                            Target: 1
                        };

                        $scope.form.frmNewEECustomFields.$setPristine();
                        var n = noty({ timeout: 5000, layout: 'topRight', type: 'success', text: "Field successfully created" });
                    }
                });
            }
        };

        $scope.EmployerSelected = function () {
            if ($scope.IsNewField) {
                $scope.NewField.EmployerId = $scope.NewField.SelectedEmployer.Id;
            } else {
                $scope.EditField.EmployerId = $scope.EditField.SelectedEmployer.Id;
                $scope.Fields = $scope.EditField.SelectedEmployer.Fields;
            }
        };

        $scope.FieldSelected = function () {
            if ($scope.EditField.SelectedField == null) {
                $scope.EditField.Name = '';
                $scope.EditField.Description = '';
                $scope.EditField.Label = '';
                $scope.EditField.HelpText = '';
                $scope.EditField.DataType = '';
                $scope.EditField.ValueType = '';
                $scope.EditField.ListValues = [];
                $scope.EditField.Key = '';
                $scope.EditField.Value = '';
                $scope.EditField.ListValuesText = '';
                $scope.EditField.FieldOrder = null;
                $scope.EditField.IsRequired = false;
                $scope.EditField.IsCollectedAtIntake = true;
                $scope.EditField.Target = 1;
            }
            else {
                $scope.EditField.Id = $scope.EditField.SelectedField.Id;
                $scope.EditField.Name = $scope.EditField.SelectedField.Name;
                $scope.EditField.Page = $scope.EditField.SelectedField.Page;
                $scope.EditField.Description = $scope.EditField.SelectedField.Description;
                $scope.EditField.Label = $scope.EditField.SelectedField.Label;
                $scope.EditField.HelpText = $scope.EditField.SelectedField.HelpText;
                $scope.EditField.DataType = $scope.EditField.SelectedField.DataType;
                $scope.EditField.ValueType = $scope.EditField.SelectedField.ValueType;
                $scope.EditField.ListValues = $scope.EditField.SelectedField.ListValues;
                $scope.EditField.Key = '';
                $scope.EditField.Value = '';
                $scope.EditField.ListValuesText = '';
                $scope.EditField.FieldOrder = parseInt($scope.EditField.SelectedField.FieldOrder);
                $scope.EditField.IsRequired = $scope.EditField.SelectedField.IsRequired;
                $scope.EditField.IsCollectedAtIntake = $scope.EditField.SelectedField.IsCollectedAtIntake;
                $scope.EditField.Target = $scope.EditField.SelectedField.Target;

                var values = '';
                angular.forEach($scope.EditField.SelectedField.ListValues, function (val, index) {
                    values += val.Value + "\n";
                });
                $scope.EditField.ListValuesText = values;
            }
        };

        $scope.BindSelect2 = function () {
            $("#NewEmployer").select2({
                theme: "classic",
                placeholder: "Search & Add Multi-Employers",
                multiple: true,
                data: $scope.EmployersForSelect2,
            }).on("select2-selecting", function (e) {
                $("#select2Error").hide();
                $("#NewEmployer").removeClass("has-error")
            })
        }

        $scope.InitNewField = function () {
            $scope.IsNewField = true;
            $scope.NewField = {
                EmployerId: '',
                Name: '',
                Description: '',
                Label: '',
                HelpText: '',
                DataType: '',
                ValueType: 1,
                ListValues: [],
                ListValuesText: '',
                FieldOrder: '',
                IsRequired: false,
                IsCollectedAtIntake: true,
                Target: 1
            };

            $scope.form.submitted = false;
            if ($scope.form.frmNewEECustomFields) {
                $scope.form.frmNewEECustomFields.$setPristine();
            }
        };

        $scope.AddListItem = function () {
            if ($scope.IsNewField) {
                if ($scope.NewField.Value == undefined ||
                $scope.NewField.Value == null ||
                $scope.NewField.Value == '') {
                    $scope.BadListItem = true;
                }
                else {
                    $scope.BadListItem = false;
                    var index = $scope.NewField.ListValues.length + 1;
                    var ListItem = new Object();
                    ListItem.Key = $scope.NewField.Value;
                    ListItem.Value = $scope.NewField.Value;
                    $scope.NewField.ListValues.push(ListItem);

                    var values = '';
                    angular.forEach($scope.NewField.ListValues, function (val, index) {
                        values += val.Value + "\n";
                    });
                    $scope.NewField.ListValuesText = values;

                    $scope.NewField.Key = '';
                    $scope.NewField.Value = '';
                }
            }
            else//edit field
            {
                if ($scope.EditField.Value == undefined ||
                    $scope.EditField.Value == null ||
                    $scope.EditField.Value == '') {
                    $scope.BadListItem = true;
                }
                else {
                    $scope.BadListItem = false;
                    var ListItem = new Object();
                    ListItem.Key = $scope.EditField.Value;
                    ListItem.Value = $scope.EditField.Value;
                    $scope.EditField.ListValues.push(ListItem);

                    var values = '';
                    angular.forEach($scope.EditField.SelectedField.ListValues, function (val, index) {
                        values += val.Value + "\n";
                    });
                    $scope.EditField.ListValuesText = values;

                    $scope.EditField.Key = '';
                    $scope.EditField.Value = '';
                }
            }

        };

        $scope.ClearList = function () {
            if ($scope.IsNewField) {
                $scope.NewField.ListValues = [];
                $scope.NewField.ListValuesText = '';
            } else {
                $scope.EditField.ListValues = [];
                $scope.EditField.ListValuesText = '';
            }
        };

        $scope.CancelNew = function () {
            $scope.IsNewField = false;
            $scope.EditField = {
                Id: '',
                EmployerId: '',
                Name: '',
                Description: '',
                Label: '',
                HelpText: '',
                DataType: '',
                ValueType: '',
                ListValues: [],
                ListValuesText: '',
                FieldOrder: '',
                IsRequired: false,
                IsCollectedAtIntake: true,
                Target: 1
            };

            $scope.form.submitted = false;
            $scope.form.frmNewEECustomFields.$setPristine();
        };

        $scope.Back = function () {
            // var url = "/reports/#/absencereports/" + $scope.report;
            // window.location.href = url;
            $window.history.back();
        }
    }]);