//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EmployeeCtrl', ['$scope', '$filter', '$window', '$http', 'employeeService', 'payScheduleService', '$timeout', 'lookupService', 'userPermissions', '$rootScope', 'CustomFieldType', 'EmploymentStatus',
function ($scope, $filter, $window, $http, employeeService, payScheduleService, $timeout, lookupService, userPermissions, $rootScope, CustomFieldType, EmploymentStatus) {

    $scope.IsEdit = false;
    $scope.CreateEmployeeModel = new Object();
    $scope.Address = new Object();
    $scope.AltAddress = new Object();
    $scope.WorkSchedule = new Object();
    $scope.PaySchedules = [];
    $scope.EmploymentStatus = EmploymentStatus;
    $scope.Times = new Array();
    $scope.CustomFields = new Array();

    $scope.ManualTimes = [];
    $scope.CurrentDisplayMonth = 4;
    $scope.AlreadyPushedMonths = {};
    $scope.MaxDate = new Date();

    $scope.Error = null;
    $scope.ErrorHandler = function (response) {
        $scope.Error = response;
        $scope.isDisabled = false;
    };

    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };

    //#region controller initialization logic
    $scope.init = function (employeeId, IsESS) {
        $scope.EmployeeId = employeeId;
        $scope.IsESS = IsESS == 'True' ? true : false;
        $scope.SetPermission();
        $scope.showScheduleHistory = false;

        //if (IsESS == 'True')
        //    $rootScope.UserImage = "/Content/Images/user-blue.png";

        // Get list of employer for current user
        lookupService.GetEmployersForUser().then(function (data) {
            $scope.EmployerList = data;

            //Get Feature 'MultiEmployerAccess'
            lookupService.GetEmployerFeatures().then(function (response) {
                $scope.IsTPA = response.MultiEmployerAccess;

                if (!$scope.IsTPA || data.length == 1) {
                    $scope.CreateEmployeeModel.EmployerId = $scope.EmployerList[0].Id;
                }

                if (!$scope.EmployeeId) {
                    if ($scope.CreateEmployeeModel.EmployerId != null && $scope.CreateEmployeeModel.EmployerId != undefined && $scope.CreateEmployeeModel.EmployerId != '') {
                        employeeService.GetEECustomFields($scope.CreateEmployeeModel.EmployerId).then(function (response) {
                            $scope.CustomFields = response;
                        });
                    }
                }
            });
        });
        
        $scope.JobClassifications = lookupService.JobClassifications();

        //get the employee data from server side for edit, and load the client side model
        if ($scope.EmployeeId) {
            $scope.IsEdit = true;
            employeeService.GetEmployeeForEdit($scope.EmployeeId)
            .then(function (result) {

                //populate the employee object for edit
                $scope.CreateEmployeeModel = result;
                $scope.CreateEmployeeModel.ServiceDate = $filter('ISODateToDate')(result.ServiceDate);
                $scope.CreateEmployeeModel.HireDate = $filter('ISODateToDate')(result.HireDate);
                $scope.CreateEmployeeModel.RehireDate = $filter('ISODateToDate')(result.RehireDate);
                $scope.CreateEmployeeModel.DOB = $filter('ISODateToDate')(result.DOB);

                //$scope.CreateEmployeeModel.DOB = result.DOB;

                $scope.CreateEmployeeModel.WorkSchedule.StartDate = $filter('ISODateToDate')(result.WorkSchedule.StartDate);
                $scope.CreateEmployeeModel.WorkSchedule.EndDate = $filter('ISODateToDate')(result.WorkSchedule.EndDate);

                $scope.CreateEmployeeModel.HomePhone = $filter('tel')(result.HomePhone);
                $scope.CreateEmployeeModel.CellPhone = $filter('tel')(result.CellPhone);
                $scope.CreateEmployeeModel.WorkPhone = $filter('tel')(result.WorkPhone);

                if ($scope.CreateEmployeeModel.SpouseEmployeeNumber != null)
                    $("#related-employee").select2("data", { id: $scope.CreateEmployeeModel.SpouseEmployeeNumber, text: $scope.CreateEmployeeModel.SpouseFirstName });

                if (result.Gender == 77) {
                    $scope.CreateEmployeeModel.Gender = 'Male';
                } else {
                    $scope.CreateEmployeeModel.Gender = 'Female';
                }

                if ($scope.CreateEmployeeModel.JobFunctions == null || $scope.CreateEmployeeModel.JobFunctions.length == 0) {
                    $scope.CreateEmployeeModel.JobFunctions = [{ Type: '', Description: '' }];
                }
                else {
                    angular.forEach($scope.CreateEmployeeModel.JobFunctions, function (jobFunc, index) {
                        if (jobFunc.Type == 69) {
                            jobFunc.Type = "Essential";
                        } else if (jobFunc.Type == 77) {
                            jobFunc.Type = "Marginal";
                        }
                    });
                }

                $scope.CustomFields = result.CustomFields;

                onLoadCustomFields($scope.CustomFields);
                $scope.CreateEmployeeModel.CustomFields = $scope.CustomFields;
                $scope.Address = result.Address;
                $scope.CreateEmployeeModel.Address = $scope.Address;
                $scope.AltAddress = result.AltAddress;
                $scope.CreateEmployeeModel.AltAddress = $scope.AltAddress;
                $scope.WorkSchedule = result.WorkSchedule;
                $scope.CreateEmployeeModel.WorkSchedule = $scope.WorkSchedule;
                //schedule view 
                $scope.ScheduleStartEnd = true;
                //weekly
                if (result.WorkSchedule.ScheduleType == 0) {
                    $scope.ScheduleWeekly = true;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;
                    //populate day

                    angular.copy(result.WorkSchedule.Times, $scope.Times);
                    //$scope.Times = result.WorkSchedule.Times;

                    if ($scope.Times.length <= 0) {
                        for (var i = 0; i < 7; i++) {
                            $scope.Time = new Object();
                            $scope.Time.TotalMinutes = null;
                            $scope.Times.push($scope.Time);
                        }
                    }

                }//rotating
                else if (result.WorkSchedule.ScheduleType == 1) {
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = true;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = false;


                    //$scope.Times = result.WorkSchedule.Times;
                    angular.copy(result.WorkSchedule.Times, $scope.Times);
                    $scope.RotationDays = result.WorkSchedule.Times.length;
                }//manual
                else if (result.WorkSchedule.ScheduleType == 2) {
                    $scope.ScheduleWeekly = false;
                    $scope.ScheduleRotating = false;
                    $scope.ScheduleRotatingPerDay = false;
                    $scope.ScheduleOneTime = true;

                    angular.copy(result.WorkSchedule.Times, $scope.Times);
                    $scope.GetManualDays(new Date());
                    if ($scope.Times.length <= 0) {
                        for (var i = 0; i < 7; i++) {
                            $scope.Time = new Object();
                            $scope.Time.TotalMinutes = null;
                            $scope.Times.push($scope.Time);
                        }
                    }
                }
            },
           $scope.ErrorHandler);

        }


        // Adding watch for dates
        $scope.$watch('CreateEmployeeModel.ServiceDate', function () {
            $scope.DOBValidation('ServiceDate');
            $scope.CompareServiceAndHireDates();
        });

        $scope.$watch('CreateEmployeeModel.EmployerId', function () {
            if ($scope.CreateEmployeeModel.EmployerId) {
                payScheduleService.List($scope.CreateEmployeeModel.EmployerId).then(function (paySchedules) {
                    $scope.PaySchedules = paySchedules;
                })
            } else {
                $scope.PaySchedules = [];
            }
            
        })

        $scope.$watch('CreateEmployeeModel.HireDate', function () {
            $scope.DOBValidation('HireDate');
            $scope.CompareServiceAndHireDates();
            $scope.CompareRehireAndHireDates();
        });

        $scope.$watch('CreateEmployeeModel.RehireDate', function () {
            $scope.DOBValidation('RehireDate');
            $scope.CompareRehireAndHireDates();
        });

        $scope.$watch('CreateEmployeeModel.IsKeyEmployee', function () {
            if ($scope.CreateEmployeeModel.IsKeyEmployee)
                $scope.CreateEmployeeModel.IsExempt = true;
        });

        $scope.$watch('Address.PostalCode', function () {
            $scope.ZipcodeValidation();
        });

        $scope.$watch('Address.Country', function () {
            $scope.ZipcodeValidation();
        });

        $scope.$watch('AltAddress.PostalCode', function () {
            $scope.AltZipcodeValidation();
        });

        $scope.$watch('AltAddress.Country', function () {
            $scope.AltZipcodeValidation();
        });

        $scope.$watch('RotationDays', function (nVal, oVal) {
            if (nVal == oVal)
                return;

            if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
                if ($scope.Times.length < parseInt($scope.RotationDays)) {
                    for (var i = $scope.Times.length; i < parseInt($scope.RotationDays) ; i++) {
                        $scope.Time = new Object();
                        $scope.Time.TotalMinutes = null;
                        $scope.Times.push($scope.Time);
                    }
                }
                if ($scope.Times.length > parseInt($scope.RotationDays)) {
                    var diff = ($scope.Times.length - parseInt($scope.RotationDays));
                    $scope.Times.splice(parseInt($scope.RotationDays), diff);
                }

                $scope.rotationArray = new Array(parseInt($scope.RotationDays));
                for (var date in $scope.rotationArray) {
                    date.Formatted = ($scope.WorkSchedule.StartDate + $index).getDay();
                }
            }
            else {
                $scope.rotationArray = null;
            }
        });

    };

    $scope.SetPermission = function () {
        $scope.EditEmployeeInfo = false;
        $scope.EditEmployeeContactInfo = false;
        $scope.EditEmployeeJobInfo = false;
        $scope.EditEmployeeJobDetails = false;
        $scope.EditEmployeeWorkSchedule = false;
        $scope.EditEmployeeTimeTracker = false;
        $scope.EditEmployeeAbsenceHistory = false;

        $scope.EditEmployeeInfo = userPermissions.indexOf("EditEmployeeInfo") > -1;
        $scope.EditEmployeeContactInfo = userPermissions.indexOf("EditEmployeeContactInfo") > -1;
        $scope.EditEmployeeJobInfo = userPermissions.indexOf("EditEmployeeJobInfo") > -1;
        $scope.EditEmployeeJobDetails = userPermissions.indexOf("EditEmployeeJobDetails") > -1;
        $scope.EditEmployeeWorkSchedule = userPermissions.indexOf("EditEmployeeWorkSchedule") > -1;
    }
    //#endregion controller initialization logic

    //#region Validation
    $scope.DOBValidation = function (CheckWith) {
        if ($scope.CreateEmployeeModel.DOB == '' || $scope.CreateEmployeeModel.DOB == undefined)
            return;

        var DOB = Date.parse($scope.CreateEmployeeModel.DOB);
        if (isNaN(DOB)) { // d.valueOf() could also work
            $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
            $scope.DOBError = "Not valid date";
        }

        switch (CheckWith) {
            case 'All':
                if (DOB > new Date()) {
                    $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                    $scope.DOBError = "DOB cannot be greater than today";
                    break;
                }

                if ($scope.CreateEmployeeModel.ServiceDate != '' && $scope.CreateEmployeeModel.ServiceDate != null && $scope.CreateEmployeeModel.ServiceDate != undefined) {
                    if (DOB > new Date($scope.CreateEmployeeModel.ServiceDate)) {
                        $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                        $scope.DOBError = "DOB should be less than service date";
                        break;
                    }
                }

                if ($scope.CreateEmployeeModel.HireDate != '' && $scope.CreateEmployeeModel.HireDate != null && $scope.CreateEmployeeModel.HireDate != undefined) {
                    if (DOB > new Date($scope.CreateEmployeeModel.HireDate)) {
                        $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                        $scope.DOBError = "DOB should be less than hire date";
                        break;
                    }
                }
                if ($scope.CreateEmployeeModel.RehireDate != '' && $scope.CreateEmployeeModel.RehireDate != null && $scope.CreateEmployeeModel.RehireDate != undefined) {
                    if (DOB > new Date($scope.CreateEmployeeModel.RehireDate)) {
                        $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                        $scope.DOBError = "DOB should be less than re-hire date";
                        break;
                    }
                }

                $scope.frmCreateEmployee.DOB.$setValidity('valid', true);

                break;
            case 'ServiceDate':
                if ($scope.CreateEmployeeModel.ServiceDate == '' || $scope.CreateEmployeeModel.ServiceDate == undefined) {
                    $scope.DOBValidation('All');
                    return;
                }

                var serviceDate = Date.parse($scope.CreateEmployeeModel.ServiceDate);

                if (isNaN(serviceDate)) { // d.valueOf() could also work
                    $scope.frmCreateEmployee.ServiceDate.$setValidity('valid', false);
                    $scope.ServiceDateError = 'Not valid date';
                }

                if (DOB > serviceDate) {
                    $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                    $scope.DOBError = "DOB should be less than service date";
                }
                else {
                    $scope.DOBValidation('All');
                }

                break;
            case 'HireDate':
                if ($scope.CreateEmployeeModel.HireDate == '' || $scope.CreateEmployeeModel.HireDate == undefined) {
                    $scope.DOBValidation('All');
                    return;
                }

                var hireDate = Date.parse($scope.CreateEmployeeModel.HireDate);
                if (isNaN(hireDate)) { // d.valueOf() could also work
                    $scope.frmCreateEmployee.HireDate.$setValidity('valid', false);
                } else {
                    $scope.frmCreateEmployee.HireDate.$setValidity('valid', true);
                }

                if (DOB > hireDate) {
                    $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                    $scope.DOBError = "DOB should be less than hire date";
                }
                else {
                    $scope.DOBValidation('All');
                }

                break;
            case 'RehireDate':
                if ($scope.CreateEmployeeModel.RehireDate == '' || $scope.CreateEmployeeModel.RehireDate == undefined || $scope.CreateEmployeeModel.RehireDate == null) {
                    $scope.DOBValidation('All');
                    return;
                }

                var reHireDate = Date.parse($scope.CreateEmployeeModel.RehireDate);

                if (isNaN(reHireDate)) { // d.valueOf() could also work
                    $scope.frmCreateEmployee.RehireDate.$setValidity('valid', false);
                    $scope.RehireDateError = "Not valid date";
                }

                if (DOB > reHireDate) {
                    $scope.frmCreateEmployee.DOB.$setValidity('valid', false);
                    $scope.DOBError = "DOB should be less than re-hire date";
                }
                else {
                    $scope.DOBValidation('All');
                }

                break;
        }
    }

    $scope.CompareServiceAndHireDates = function () {
        var serviceDate = $scope.CreateEmployeeModel.ServiceDate;
        var hireDate = $scope.CreateEmployeeModel.HireDate;


        if (serviceDate == '' || serviceDate == null || serviceDate == undefined) {
            return;
        }

        if (hireDate == '' || hireDate == null || hireDate == undefined) {
            $scope.frmCreateEmployee.ServiceDate.$setValidity('valid', true);
            return;
        }
        var dt1 = Date.parse(serviceDate);
        var dt2 = Date.parse(hireDate);

        if (dt1 >= dt2) {
            $scope.frmCreateEmployee.ServiceDate.$setValidity('valid', true);
        }
        else {
            $scope.frmCreateEmployee.ServiceDate.$setValidity('valid', false);
            $scope.ServiceDateError = "Service date cannot be less than hire date ";
        }
    }

    $scope.CompareRehireAndHireDates = function () {
        var hireDate = $scope.CreateEmployeeModel.HireDate;
        var rehireDate = $scope.CreateEmployeeModel.RehireDate;

        if (hireDate == '' || hireDate == null || hireDate == undefined) {
            $scope.frmCreateEmployee.RehireDate.$setValidity('valid', true);
            return;
        }

        if (rehireDate == '' || rehireDate == null || rehireDate == undefined) {
            $scope.frmCreateEmployee.RehireDate.$setValidity('valid', true);
            return;
        }

        var dt1 = Date.parse(rehireDate);
        var dt2 = Date.parse(hireDate);

        if (dt1 >= dt2) {
            $scope.frmCreateEmployee.RehireDate.$setValidity('valid', true);
        }
        else {
            $scope.frmCreateEmployee.RehireDate.$setValidity('valid', false);
            $scope.RehireDateError = "Rehire date cannot be less than hire date ";
        }
    }

    $scope.ZipcodeValidation = function () {
        if ($scope.Address.PostalCode == null || $scope.Address.PostalCode == '' || $scope.Address.PostalCode == undefined)
            return;

        if ($scope.Address.Country == null || $scope.Address.Country == '' || $scope.Address.Country == undefined)
            return;

        if ($scope.Address.Country == 'US') {
            var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
            if (!regEx.test($scope.Address.PostalCode)) {
                $scope.frmCreateEmployee.PostalCode.$setValidity('valid', false);
                return;
            }
        }

        $scope.frmCreateEmployee.PostalCode.$setValidity('valid', true);
    };

    $scope.AltZipcodeValidation = function () {
        if ($scope.AltAddress.PostalCode == null || $scope.AltAddress.PostalCode == '' || $scope.AltAddress.PostalCode == undefined)
            return;

        if ($scope.AltAddress.Country == null || $scope.AltAddress.Country == '' || $scope.AltAddress.Country == undefined)
            return;

        if ($scope.AltAddress.Country == 'US') {
            var regEx = new RegExp("^[0-9]{5}([-]?[0-9]{4})?$");
            if (!regEx.test($scope.AltAddress.PostalCode)) {
                $scope.frmCreateEmployee.AltPostalCode.$setValidity('validalt', false);
                return;
            }
        }

        $scope.frmCreateEmployee.AltPostalCode.$setValidity('validalt', true);
    };
    //#endregion

    $scope.CancelEmployee = function () {
        if ($scope.EmployeeId) {
            if ($scope.IsESS) {
                $window.location.href = "/";
            }
            else {
                $window.location.href = "/Employees/" + $scope.EmployeeId + "/View";
            }

        }
        else {
            $window.location.href = "/";
        }
    };

    function validateJobFunctions() {
        var
            isValid = true,
            remove = [];

        angular.forEach($scope.CreateEmployeeModel.JobFunctions, function (jf) {
            if (jf.Type && !jf.Description) {
                remove.push(jf);                
                return;
            }

            if ((jf.Type || jf.Description) && !(jf.Type && jf.Description)) {
                alert('Job Function Type is required for Job Function: ' + jf.Description);
                isValid = false;
                return;
            }
        });
        remove.forEach(function (jf) {
            $scope.RemoveJobFunction(jf);
        });
        return isValid;
    }

    //#region Edit Employee
    $scope.EditEmployee = function (IsValid, ShowViewPage) {
        IsValid = IsValid && validateJobFunctions();

        if (!IsValid)
            return;
        else {
            if ($scope.WorkSchedule.ScheduleType == 2) {
                if ($scope.ManualTimes.length > 0) {
                    var isHourValueEntered = false;

                    angular.forEach($scope.ManualTimes, function (item, index) {
                        if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                            isHourValueEntered = true;
                        }
                    });
                }
            }
            else {
                if ($scope.Times.length > 0) {
                    var isHourValueEntered = false;
                    angular.forEach($scope.Times, function (item, index) {
                        if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                            isHourValueEntered = true;
                        }
                    });
                }
            }

            if (!isHourValueEntered) {
                IsValid = false;

                if ($scope.WorkSchedule.ScheduleType == 0) {
                    $scope.ShowWeeklyHourError = true;
                    $scope.ShowRotationHourError = false;
                    $scope.ShowManualHourError = false;
                }
                else if ($scope.WorkSchedule.ScheduleType == 1) {
                    $scope.ShowWeeklyHourError = false;
                    $scope.ShowRotationHourError = true;
                    $scope.ShowManualHourError = false;
                }
                else if ($scope.WorkSchedule.ScheduleType == 2) {
                    $scope.ShowWeeklyHourError = false;
                    $scope.ShowRotationHourError = false;
                    $scope.ShowManualHourError = true;
                }
                return;
            }
            else {
                $scope.ShowWeeklyHourError = false;
                $scope.ShowRotationHourError = false;
                $scope.ShowManualHourError = false;
            }
        }

        if (IsValid) {

            angular.forEach($scope.CreateEmployeeModel.JobFunctions, function (jobFunc, index) {
                if ((jobFunc.Type == null || jobFunc.Type == '' || jobFunc.Type == undefined) && (jobFunc.Description == null || jobFunc.Description == '' || jobFunc.Description == undefined)) {
                    $scope.RemoveJobFunction(jobFunc);
                }
            });

            $scope.isDisabled = true;
            $scope.CreateEmployeeModel.SpouseEmployeeNumber = $("#related-employee").select2("val");
            $scope.CreateEmployeeModel.Id = $scope.EmployeeId;
            $scope.CreateEmployeeModel.Address = $scope.Address;
            $scope.CreateEmployeeModel.AltAddress = $scope.AltAddress;
            
            //Text = 0x1,
            //Number = 0x2,
            //Flag = 0x4,
            //Date = 0x8
            onValidateCustomFields($scope.CustomFields);

            $scope.CreateEmployeeModel.CustomFields = $scope.CustomFields;
            $scope.CreateEmployeeModel.WorkSchedule = $scope.WorkSchedule;
            $scope.CreateEmployeeModel.WorkSchedule.Times = $scope.Times;

            /// variable schedule
            if ($scope.WorkSchedule.ScheduleType == 2) {
                $scope.CreateEmployeeModel.VariableTimes = $scope.ManualTimes;
            }

            // Date send as string
            $scope.CreateEmployeeModel.DOB = $scope.CreateEmployeeModel.DOB == null || $scope.CreateEmployeeModel.DOB == "" || $scope.CreateEmployeeModel.DOB == undefined ? null : new Date($scope.CreateEmployeeModel.DOB).toDateString();
            $scope.CreateEmployeeModel.HireDate = $scope.CreateEmployeeModel.HireDate == null || $scope.CreateEmployeeModel.HireDate == "" || $scope.CreateEmployeeModel.HireDate == undefined ? null : new Date($scope.CreateEmployeeModel.HireDate).toDateString();
            $scope.CreateEmployeeModel.RehireDate = $scope.CreateEmployeeModel.RehireDate == null || $scope.CreateEmployeeModel.RehireDate == "" || $scope.CreateEmployeeModel.RehireDate == undefined ? null : new Date($scope.CreateEmployeeModel.RehireDate).toDateString();
            $scope.CreateEmployeeModel.ServiceDate = new Date($scope.CreateEmployeeModel.ServiceDate).toDateString();

            $scope.CreateEmployeeModel.WorkSchedule.StartDate = $scope.WorkSchedule.StartDate == undefined || $scope.WorkSchedule.StartDate == null || $scope.WorkSchedule.StartDate == "" ? null : new Date($scope.WorkSchedule.StartDate).toDateString();
            $scope.CreateEmployeeModel.WorkSchedule.EndDate = $scope.WorkSchedule.EndDate == undefined || $scope.WorkSchedule.EndDate == null || $scope.WorkSchedule.EndDate == "" ? null : new Date($scope.WorkSchedule.EndDate).toDateString();

            $scope.CreateEmployeeModel.WorkPhone = $scope.CreateEmployeeModel.WorkPhone.replace(/[^0-9]/g, '');
            $scope.CreateEmployeeModel.CellPhone = $scope.CreateEmployeeModel.CellPhone.replace(/[^0-9]/g, '');
            $scope.CreateEmployeeModel.HomePhone = $scope.CreateEmployeeModel.HomePhone.replace(/[^0-9]/g, '');

            employeeService.EditEmployee($scope.EmployeeId, $scope.CreateEmployeeModel)
                .then(function (result) {
                    //window.history.back();
                    if (ShowViewPage)
                        $window.location.href = "/Employees/" + $scope.EmployeeId + "/View";
                    else
                        $window.location.href = "/";
                },
                $scope.ErrorHandler);
        }
    };
    //#endregion Edit Employee

    //#region "Manual Schedule"
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    var noofdays = function (mm, yyyy) {
        var daysofmonth;

        if ((mm == 4) || (mm == 6) || (mm == 9) || (mm == 11)) {
            daysofmonth = 30;
        }
        else {
            daysofmonth = 31
            if (mm == 2) {
                if (yyyy / 4 - parseInt(yyyy / 4) != 0) {
                    daysofmonth = 28
                }
                else {
                    if (yyyy / 100 - parseInt(yyyy / 100) != 0) {
                        daysofmonth = 29
                    }
                    else {
                        if ((yyyy / 400) - parseInt(yyyy / 400) != 0) {
                            daysofmonth = 28
                        }
                        else {
                            daysofmonth = 29
                        }
                    }
                }
            }
        }

        return daysofmonth;
    }

    $scope.GetManualDays = function (startDate) {
        if ($scope.WorkSchedule.ScheduleType != 2)
            return;

        if (startDate == null) return;

        // var selectedDate = new Date(startDate);
        var selectedDate = $filter('ISODateToDate')(startDate);
        $scope.CurrentDisplayMonth = selectedDate.getMonth();
        $scope.CurrentDisplayYear = selectedDate.getFullYear();

        $scope.CurrentMonthName = monthNames[$scope.CurrentDisplayMonth] + " " + $scope.CurrentDisplayYear;

        if ($scope.CurrentDisplayMonth == 0) {
            $scope.PrevMonthName = monthNames[11] + " " + ($scope.CurrentDisplayYear - 1);
            $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
        }
        else if ($scope.CurrentDisplayMonth == 11) {
            $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
            $scope.NextMonthName = monthNames[0] + " " + ($scope.CurrentDisplayYear + 1);
        }
        else {
            $scope.PrevMonthName = monthNames[$scope.CurrentDisplayMonth - 1] + " " + $scope.CurrentDisplayYear;
            $scope.NextMonthName = monthNames[$scope.CurrentDisplayMonth + 1] + " " + $scope.CurrentDisplayYear;
        }

        

        var startDay = selectedDate.getDate();
        var monthKey = $scope.CurrentDisplayYear + 'y' + $scope.CurrentDisplayMonth + 'm';
        var monthData = $scope.AlreadyPushedMonths[monthKey];
        if (monthData) {
            $scope.FilterDates();
        } else {
            var employeeScheduleData = {
                EmployeeId: $scope.EmployeeId,
                Month: $scope.CurrentDisplayMonth + 1,
                Year: $scope.CurrentDisplayYear
            };
            employeeService.GetEmployeeVariableSchedule(employeeScheduleData).then(function (result) {
                $scope.AlreadyPushedMonths[monthKey] = result;
                addMonthDataToManualTimes(result);
            });
        }
    };

    addMonthDataToManualTimes = function (monthData) {
        var numOfDaysInMonth = noofdays($scope.CurrentDisplayMonth + 1);
        for (var i = 1; i <= numOfDaysInMonth; i++) {
            var time = {
                TotalMinutes: '',
                SampleDate: ''
            };
            var tempDate = $scope.CurrentDisplayYear + "-" + ('0' + ($scope.CurrentDisplayMonth + 1)).slice(-2) + "-" + ('0' + (i)).slice(-2);
            time.SampleDate = $filter('ISODateToDate')(tempDate);
            if ($scope.EmployeeId && monthData.length > 0) {
                var savedTime = getTime(monthData, time.SampleDate);
                if (savedTime) {
                    $scope.ManualTimes.push(savedTime);
                }
                else {
                    $scope.ManualTimes.push(time);
                }
            }
            else {
                $scope.ManualTimes.push(time);
            }
        }

        $scope.FilterDates();
    }

    getTime = function (month, currentDate) {
        for (var i = 0; i < month.length; i++) {
            var savedDay = new Date(month[i].SampleDate).getUTCDate(); /// The get UTC Date is important because I found getDate() was returning 1 less than the date actually was
            var targetDay = currentDate.getDate();
            if (savedDay == targetDay) {
                return month[i];
            }
                
        }
    }

    $scope.FilterDates = function () {
        $scope.FilteredDates = [];
        angular.forEach($scope.ManualTimes, function (manualTime, index) {
            var dt = $filter('ISODateToDate')(manualTime.SampleDate);;                  // new Date(manualTime.SampleDate);
            if (dt.getMonth() == $scope.CurrentDisplayMonth) {
                $scope.FilteredDates.push(manualTime);
            }
        });
    };

    $scope.GoToMonth = function (direction) {
        var year, month;

        if (direction == 'Prev') {
            if ($scope.CurrentDisplayMonth == 0) {  // if January
                month = 11;
                year = $scope.CurrentDisplayYear - 1;
            }
            else {
                month = $scope.CurrentDisplayMonth - 1;
                year = $scope.CurrentDisplayYear;
            }
        }
        else {
            if ($scope.CurrentDisplayMonth == 11) {  // if December
                month = 0;
                year = $scope.CurrentDisplayYear + 1;
            }
            else {
                month = $scope.CurrentDisplayMonth + 1;
                year = $scope.CurrentDisplayYear;
            }
        }

        var strDate = year + "-" + ('0' + (month + 1)).slice(-2) + "-" + ('0' + (1)).slice(-2);
        $scope.GetManualDays(strDate);
    };

    //#endregion "Manual Schedule"

    //#region Create Employee Logic
    if (!$scope.EmployeeId) {
        //employee info
        $scope.CreateEmployeeModel.FirstName = '';
        $scope.CreateEmployeeModel.MiddleName = '';
        $scope.CreateEmployeeModel.LastName = '';
        $scope.CreateEmployeeModel.EmployeeNumber = '';
        $scope.CreateEmployeeModel.JobTitle = '';
        $scope.CreateEmployeeModel.DOB = '';
        $scope.CreateEmployeeModel.Gender = '';
        $scope.CreateEmployeeModel.Email = '';
        $scope.CreateEmployeeModel.MilitaryStatus = '';

        //Contact info
        $scope.Address.Country = 'US';
        $scope.Address.Address1 = '';
        $scope.Address.Address2 = '';
        $scope.Address.City = '';
        $scope.Address.State = '';
        $scope.Address.PostalCode = '';

        $scope.AltAddress.Country = 'US';
        $scope.AltAddress.Address1 = '';
        $scope.AltAddress.Address2 = '';
        $scope.AltAddress.City = '';
        $scope.AltAddress.State = '';
        $scope.AltAddress.PostalCode = '';

        $scope.CreateEmployeeModel.WorkPhone = '';
        $scope.CreateEmployeeModel.CellPhone = '';
        $scope.CreateEmployeeModel.HomePhone = '';

        //Job Info
        $scope.CreateEmployeeModel.WorkState = '';
        $scope.CreateEmployeeModel.OfficeLocation = '';
        $scope.CreateEmployeeModel.Meets50In75MileRule = null;
        $scope.CreateEmployeeModel.HireDate = '';
        $scope.CreateEmployeeModel.RehireDate = '';
        $scope.CreateEmployeeModel.ServiceDate = '';
        $scope.CreateEmployeeModel.PriorHoursWorked = '';
        $scope.CreateEmployeeModel.PriorHoursWorkedAsOf = '';
        $scope.CreateEmployeeModel.WorkedPerWeek = '';
        $scope.CreateEmployeeModel.WorkedPerWeekAsOf = '';
        $scope.CreateEmployeeModel.PayType = '';
        $scope.CreateEmployeeModel.IsExempt = null;
        $scope.CreateEmployeeModel.IsKeyEmployee = null;
        $scope.CreateEmployeeModel.Salary = '';
        $scope.CreateEmployeeModel.SpouseEmployeeNumber = null;
        //Employee Spouse
        //<input type="text" id="related-employee" class="form-control select2 bigdrop" />

        //Schedule
        $scope.WorkSchedule.ScheduleType = null;
        $scope.WorkSchedule.StartDate = null;
        $scope.WorkSchedule.EndDate = null;

        // Job Details
        $scope.CreateEmployeeModel.JobDescription = '';
        $scope.CreateEmployeeModel.Classification = '';
        $scope.CreateEmployeeModel.JobFunctions = [{ Type: '', Description: '' }];

        $scope.CustomFields = [];

    }
    $scope.CreateEmployee = function (IsValid, ShowViewPage) {
        IsValid = IsValid && validateJobFunctions();
        if(!IsValid) {
            return;
        }

        if (!IsValid) {
            //$scope.$apply();
            //var error = $('.has-error').first();
            //var scrollPos = error.offset().top;
            //$(window).scrollTop(scrollPos);
        }
        else {
            if ($scope.WorkSchedule.ScheduleType == 2) {
                if ($scope.ManualTimes.length > 0) {
                    var isHourValueEntered = false;

                    angular.forEach($scope.ManualTimes, function (item, index) {
                        if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                            isHourValueEntered = true;
                        }
                    });
                }
            }
            else {
                if ($scope.Times.length > 0) {
                    var isHourValueEntered = false;
                    angular.forEach($scope.Times, function (item, index) {
                        if (item.TotalMinutes != null && item.TotalMinutes != '' && item.TotalMinutes != undefined) {
                            isHourValueEntered = true;
                        }
                    });
                }
            }

            if (!isHourValueEntered) {
                IsValid = false;

                if ($scope.WorkSchedule.ScheduleType == 0) {
                    $scope.ShowWeeklyHourError = true;
                    $scope.ShowRotationHourError = false;
                    $scope.ShowManualHourError = false;
                }
                else if ($scope.WorkSchedule.ScheduleType == 1) {
                    $scope.ShowWeeklyHourError = false;
                    $scope.ShowRotationHourError = true;
                    $scope.ShowManualHourError = false;
                }
                else if ($scope.WorkSchedule.ScheduleType == 2) {
                    $scope.ShowWeeklyHourError = false;
                    $scope.ShowRotationHourError = false;
                    $scope.ShowManualHourError = true;
                }
                return;
            }
            else {
                $scope.ShowWeeklyHourError = false;
                $scope.ShowRotationHourError = false;
                $scope.ShowManualHourError = false;
            }
        }

        if (IsValid) {
            $scope.isDisabled = true;
            $scope.CreateEmployeeModel.SpouseEmployeeNumber = $("#related-employee").select2("val");
            $scope.CreateEmployeeModel.Address = $scope.Address;
            $scope.CreateEmployeeModel.AltAddress = $scope.AltAddress;
            $scope.CreateEmployeeModel.WorkSchedule = $scope.WorkSchedule;

            onValidateCustomFields($scope.CustomFields);
            $scope.CreateEmployeeModel.CustomFields = $scope.CustomFields;

            // Date send as string
            $scope.CreateEmployeeModel.DOB = $scope.CreateEmployeeModel.DOB == null || $scope.CreateEmployeeModel.DOB == "" || $scope.CreateEmployeeModel.DOB == undefined ? null : new Date($scope.CreateEmployeeModel.DOB).toDateString();
            $scope.CreateEmployeeModel.HireDate = $scope.CreateEmployeeModel.HireDate == null || $scope.CreateEmployeeModel.HireDate == "" || $scope.CreateEmployeeModel.HireDate == undefined ? null : new Date($scope.CreateEmployeeModel.HireDate).toDateString();
            $scope.CreateEmployeeModel.RehireDate = $scope.CreateEmployeeModel.RehireDate == null || $scope.CreateEmployeeModel.RehireDate == "" || $scope.CreateEmployeeModel.RehireDate == undefined ? null : new Date($scope.CreateEmployeeModel.RehireDate).toDateString();
            $scope.CreateEmployeeModel.ServiceDate = new Date($scope.CreateEmployeeModel.ServiceDate).toDateString();

            $scope.CreateEmployeeModel.WorkSchedule.StartDate = $scope.WorkSchedule.StartDate == undefined || $scope.WorkSchedule.StartDate == null || $scope.WorkSchedule.StartDate == "" ? null : new Date($scope.WorkSchedule.StartDate).toDateString();
            $scope.CreateEmployeeModel.WorkSchedule.EndDate = $scope.WorkSchedule.EndDate == undefined || $scope.WorkSchedule.EndDate == null || $scope.WorkSchedule.EndDate == "" ? null : new Date($scope.WorkSchedule.EndDate).toDateString();

            $scope.CreateEmployeeModel.WorkPhone = $scope.CreateEmployeeModel.WorkPhone.replace(/[^0-9]/g, '');
            $scope.CreateEmployeeModel.CellPhone = $scope.CreateEmployeeModel.CellPhone.replace(/[^0-9]/g, '');
            $scope.CreateEmployeeModel.HomePhone = $scope.CreateEmployeeModel.HomePhone.replace(/[^0-9]/g, '');

            angular.forEach($scope.CreateEmployeeModel.JobFunctions, function (jobFunc, index) {
                if ((jobFunc.Type == null || jobFunc.Type == '' || jobFunc.Type == undefined) && (jobFunc.Description == null || jobFunc.Description == '' || jobFunc.Description == undefined)) {
                    $scope.RemoveJobFunction(jobFunc);
                }
            });

            if ($scope.WorkSchedule.ScheduleType != 2) {// Other than "Manual"
                $scope.Times.forEach(function (time) {
                    if (time.TotalMinutes === null) {
                        time.TotalMinutes = '0h 0m';
                    }
                })
                $scope.CreateEmployeeModel.WorkSchedule.Times = $scope.Times;
            }
            else {
                $scope.CreateEmployeeModel.WorkSchedule.Times = [];
                angular.forEach($scope.ManualTimes, function (item, index) {
                    if (item.TotalMinutes != '') {
                        $scope.CreateEmployeeModel.WorkSchedule.Times.push(item);
                    }
                });
            }

            employeeService.CreateEmployee($scope.CreateEmployeeModel)
                .then(function (result) {
                    if (ShowViewPage)
                        $window.location.href = "/Employees/" + result.Id + "/View";
                    else
                        $window.location.href = "/";

                },
                $scope.ErrorHandler);
        }
        else {

        }
    };

    $scope.AddJobFunction = function () {
        $scope.CreateEmployeeModel.JobFunctions.push({ Type: '', Description: '' });
    }

    $scope.RemoveJobFunction = function (jobFunc) {
        $scope.CreateEmployeeModel.JobFunctions.splice($.inArray(jobFunc, $scope.CreateEmployeeModel.JobFunctions), 1);
    }

    $scope.SetEmployerFields = function () {
        if ($scope.CreateEmployeeModel.EmployerId != null && $scope.CreateEmployeeModel.EmployerId != undefined && $scope.CreateEmployeeModel.EmployerId != '') {
            employeeService.GetEECustomFields($scope.CreateEmployeeModel.EmployerId).then(function (response) {
                $scope.CustomFields = response;
            });
        }
        else {
            $scope.CustomFields = [];
        }
    }

    $scope.getJobFunctionDescription = function (val) {
        return $http.get('/Employees/' + $scope.CreateEmployeeModel.EmployerId + '/GetJobFunctionDescription', {
            params: {
                desc: val
            }
        }).then(function (response) {
            return response.data;
        });
    }
    //#endregion create employee logic

    //#region View specific logic
    if (!$scope.EmployeeId) {
        $scope.ShowAddressStateDropDown = true;
        $scope.ShowAddressStateTextBox = false;
        $scope.ShowAltAddressStateDropDown = true;
        $scope.ShowAltAddressStateTextBox = false;
        $scope.ShowPayTypeSalaryLabel = true;
        $scope.ShowPayTypeWageLabel = false;
        $scope.IsSsnInFocus = false;
        $scope.IsSalaryInFocus = false;
        //scheduletype
        $scope.ScheduleStartEnd = false;
        $scope.ScheduleWeekly = false;
        $scope.ScheduleRotating = false;
        $scope.ScheduleRotatingPerDay = false;
        $scope.ScheduleOneTime = false;
    }

    $scope.UpdatePayType = function (payType) {
        if (payType == 'Salary') {
            $scope.CreateEmployeeModel.PayType = 1;
            $scope.ShowPayTypeSalaryLabel = true;
            $scope.ShowPayTypeWageLabel = false;
        } else {
            $scope.CreateEmployeeModel.PayType = 2;
            $scope.ShowPayTypeSalaryLabel = false;
            $scope.ShowPayTypeWageLabel = true;
        }
    };

    $scope.OnCountrySelection = function () {
        if ($scope.Address.Country != "US") {
            $scope.ShowAddressStateDropDown = false;
            $scope.ShowAddressStateTextBox = true;
        } else {
            $scope.ShowAddressStateTextBox = false;
            $scope.ShowAddressStateDropDown = true;
        }
    };

    $scope.OnAltCountrySelection = function () {
        if ($scope.AltAddress.Country != "US") {
            $scope.ShowAltAddressStateDropDown = false;
            $scope.ShowAltAddressStateTextBox = true;
        } else {
            $scope.ShowAltAddressStateTextBox = false;
            $scope.ShowAltAddressStateDropDown = true;
        }
    };

    //Mask SSN and salary
    $scope.MaskSSNAndSalaryOnFocus = function (maskInput) {
        if (maskInput == 'Salary') {
            $scope.IsSalaryInFocus = true;
            $scope.IsSsnInFocus = false;
        } else if (maskInput == 'SSN') {
            $scope.IsSsnInFocus = true;
            $scope.IsSalaryInFocus = false;
        }
    };
    //Mask SSN and salary
    if ($scope.Salary != null && $scope.Salary != '') {
        angular.element(document.querySelector('#Salary')).type = "password";
    } else {
        angular.element(document.querySelector('#Salary')).type = "text";
    }
    $scope.OnSalaryChangeKeyUp = function () {
        if ($scope.IsSalaryInFocus)
            angular.element(document.querySelector('#Salary')).type = "text";
    };
    $scope.OnSalaryBlur = function () {
        $scope.IsSalaryInFocus = false;
        angular.element(document.querySelector('#Salary')).type = "password";
    };

    //#region schedule logic

    $scope.UpdateScheduleType = function (scheduleType) {
        $scope.ScheduleType = scheduleType;
        //weekly
        if (scheduleType == 0) {
            $scope.RotationDays = 7;
            $scope.WorkSchedule.ScheduleType = 0;
            $scope.ScheduleStartEnd = true;
            $scope.ScheduleWeekly = true;
            $scope.ScheduleRotating = false;
            $scope.ScheduleRotatingPerDay = false;
            $scope.ScheduleOneTime = false;
            //populate day
            if ($scope.Times.length <= 0) {
                for (var i = 0; i < 7; i++) {
                    $scope.Time = new Object();
                    $scope.Time.TotalMinutes = null;
                    $scope.Times.push($scope.Time);
                }
            }

            //
            var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
            var weeklyDays = $('#schedule-weekly .schedule-days input');
            weeklyDays.removeAttr('name');
            rotatingDays.removeAttr('name');

            for (var i = 0; i < weeklyDays.length; i++) {
                $(weeklyDays[i]).attr('name', 'WorkSchedule.Times[' + i + '].TotalMinutes');
            }
        }
            //rotating
        else if (scheduleType == 1) {
            $scope.WorkSchedule.ScheduleType = 1;
            $scope.ScheduleStartEnd = true;
            $scope.ScheduleWeekly = false;
            $scope.ScheduleRotating = true;
            $scope.ScheduleRotatingPerDay = false;
            $scope.ScheduleOneTime = false;
            //
            var rotatingDays = $('#schedule-rotating-per-day .schedule-days input');
            var weeklyDays = $('#schedule-weekly .schedule-days input');
            weeklyDays.removeAttr('name');
            rotatingDays.removeAttr('name');
        }
            //manual
        else if (scheduleType == 2) {
            $scope.WorkSchedule.ScheduleType = 2;
            $scope.ScheduleStartEnd = true;
            $scope.ScheduleWeekly = false;
            $scope.ScheduleRotating = false;
            $scope.ScheduleRotatingPerDay = false;
            $scope.ScheduleOneTime = true;

            $scope.$watch('WorkSchedule.StartDate', function () {
                $scope.GetManualDays($scope.WorkSchedule.StartDate);
            });
        }
    };

    $scope.fireWarningMessage = function () {
        $scope.showWarningMessage = true;
    }

    //return $scope.GetRotationDays = function () {
    //    if ($scope.RotationDays != "" && $scope.RotationDays != undefined && parseInt($scope.RotationDays) > 0) {
    //        for (var i = 0; i < parseInt($scope.RotationDays) ; i++) {
    //            $scope.Time = new Object();
    //            $scope.Time.TotalMinutes = null;
    //            $scope.Times.push($scope.Time);
    //        }
    //        return new Array(parseInt($scope.RotationDays));
    //    }
    //    else {
    //        return null;
    //    }
    //};

    //#endregion schedule logic

    //#endregion View specific logic

    //mark end of EmployeeCtrl

    $scope.dayOfWeek = function (num) {
        if ($scope.RotationDays % 7 == 0) {
            var limiter = function (num) {
                if (num >= 7) {
                    num = num - 7;
                    num = limiter(num);
                    return num;
                }
                else {
                    return num;
                }

            }
            num = limiter(num);
            switch (num) {
                case 0:
                    return 'Sunday';
                    break;
                case 1:
                    return 'Monday';
                    break;
                case 2:
                    return 'Tuesday';
                    break;
                case 3:
                    return 'Wednesday';
                    break;
                case 4:
                    return 'Thursday';
                    break;
                case 5:
                    return 'Friday';
                    break;
                case 6:
                    return 'Saturday'; break;

            };
        }
        else return 'Day ' + num;
    }

    function onLoadCustomFields(customFields) {
        var ISODateToDate = $filter('ISODateToDate');
        if (customFields && customFields.length) {
            customFields.forEach(function (customField) {
                switch (customField.SelectedValue) {
                    case CustomFieldType.Date:
                        var date = Date.parse(value.SelectedValue);
                        if (!isNaN(date) && date != "Invalid Date") {
                            value.SelectedValue = ISODateToDate(value.SelectedValue);
                        }
                    break;
                }
            });
        }
    }

    function onValidateCustomFields( customFields ) {
        if (customFields && customFields.length) {
            customFields.forEach(function (customField) {
                switch (customField.SelectedValue) {
                    case CustomFieldType.Date:
                        var date = Date.parse(value.SelectedValue);
                        if (!isNaN(date) && date != "Invalid Date") {
                            value.SelectedValue = new Date(value.SelectedValue).toDateString();
                        }
                        break;
                }
            });
        }
    }

}]);