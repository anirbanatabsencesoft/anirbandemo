//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('PoliciesCtrl', ['$log', '$scope', '$http', '$filter', '$rootScope', 'policyListManager', 'policyAbsReasonListManager', 'policyService', 'lookupService', 'absenceReasonService', 'administrationService', 'regionService', '$window', '$q',
    function ($log, $scope, $http, $filter, $rootScope, policyListManager, policyAbsReasonListManager, policyService, lookupService, absenceReasonService, administrationService, regionService, $window, $q) {

        $scope.Countries = [];
        $scope.AvailableWorkRegions = [];
        $scope.AvailableResidenceRegions = [];
        $scope.AvailableAbsenceReasonWorkRegions = [];
        $scope.AvailableAbsenceReasonResidenceRegions = [];
        $scope.CrosswalkTypes = [];


        //policy
        $scope.Policy = {};
        $scope.Policy.AbsenceReasons = [];
        $scope.PolicyTypesList = null;
        $scope.ConsecutiveToListData = [];
        $scope.ConflictingRules = [];

        $scope.NewPolicy = false;
        $scope.EditPolicy = false;

        //policy abs reason
        $scope.AbsReasonEdit = {};
        $scope.AbsReasonEdit.PolicyEvents = [];
        $scope.AbsReasonEdit.PaymentTiers = [];
        $scope.AbsReasonEdit.RuleGroups = [];
        $scope.AbsReasonEdit.Paid = false;
        $scope.AbsReasonEdit.UseWholeDollarAmountsOnly = false;
        $scope.AbsReasonEdit.AllowOffset = false;

        $scope.$watch('AbsReasonEdit.Paid', function (newvalue) {
            if (!$scope.AbsReasonEdit.Paid) {
                $scope.AbsReasonEdit.AllowOffset = false;
            }
        });

        $scope.NewPolicyReason = false;
        $scope.EditPolicyReason = false;
        $scope.PolicyAbsenceReason = false;

        $scope.AbsReasonEdit.CaseTypeConsecutive = false;
        $scope.AbsReasonEdit.CaseTypeIntermittent = false;
        $scope.AbsReasonEdit.CaseTypeReduced = false;
        $scope.AbsReasonEdit.CaseTypeAdministrative = false;

        //policy events
        $scope.NewPolicyEvent = false;
        $scope.EditPolicyEvent = false;

        //rule groups
        $scope.PolicyRules = false;
        $scope.RuleGroupType = '';

        //payment tier
        $scope.InvalidOrder = false;

        $scope.forms = {};
        $scope.forms.submitted = false;
        $scope.Error = null;
        $scope.ErrorHandler = function (response) {
            $scope.forms.submitted = false;
            $scope.Error = response;
        };

        $scope.MultiValueSelectInit = function () {
            $("select.multivalue").select2();
        };

        //policy
        $scope.Init = function (policyId, employerId) {
            $scope.PolicyId = policyId;
            $scope.EmployerId = employerId;        

            $scope.forms.submitted = false;


            //load ddls
            $scope.PolicyTypesList = lookupService.PolicyTypes();
            $scope.RuleGroupSuccessTypesList = lookupService.RuleGroupSuccessTypes();
            var ruleGroupPromise = lookupService.PolicyRuleGroupTypes().$promise.then(function (result) {
                $scope.PolicyRuleGroupTypes = result;
                $scope.SetRuleGroupTypes();
            });
            $scope.EntitlementTypesList = lookupService.EntitlementTypes();
            $scope.PolicyEliminationTypesList = lookupService.PolicyEliminationTypes();
            $scope.IntermittentRestrictionsTypesList = lookupService.IntermittentRestrictionsTypes();            
            $scope.CaseTypesList = lookupService.CaseTypes();
            $scope.PolicyShowTypesList = lookupService.PolicyShowTypes();
            $scope.UnitTypesList = lookupService.UnitTypes();
            $scope.FMLPeriodTypesList = lookupService.FMLPeriodTypes();
            $scope.CaseEventTypesList = lookupService.CaseEventTypes();
            $scope.CaseEventDateTypesList = lookupService.EventDateTypes(); 
            $scope.PolicyBehaviorTypesList = lookupService.PolicyBehaviorTypes();
           
            $scope.InitGenderTypeList();
            $scope.InitCoreAbsReasons();
            $scope.InitRuleExpressions();
            $scope.SetRuleGroupTypeText();
            if ($scope.PolicyId) {
                ruleGroupPromise.then(loadPolicy);
            }
            else {
                $scope.NewPolicy = true;
                $scope.NewPolicyReason = true;
                $scope.Policy.IsDuplicate = false;
                $scope.Policy.RuleGroups = [];
                $scope.Policy.EmployerId = $scope.EmployerId;
                $scope.PolicyReasonButtonText = "Save Policy";
                $scope.InitPoliciesSearch();
                administrationService.GetEmployerInfo($scope.EmployerId).then(function (empData) {
                    $scope.Policy.EmployerName = empData.SignatureName;
                });
            }

            regionService.GetCountries().$promise.then(function (countries) {
                $scope.Countries = countries;
            })

            policyService.GetPolicyCrosswalkTypes($scope.EmployerId).then(function (crosswalkTypes) {
                $scope.CrosswalkTypes = crosswalkTypes.data;
            });
        };

        $scope.WorkCountryChanged = function (resetState) {
            if (resetState)
                $scope.Policy.WorkState = "";

            regionService.GetRegionNameByCountryCode($scope.Policy.WorkCountry).then(function (name) {
                $scope.WorkRegionName = name;
            });
            regionService.GetRegionsByCountryCode($scope.Policy.WorkCountry).then(function (regions) {
                $scope.AvailableWorkRegions = regions;
            });
        };

        $scope.ResidenceCountryChanged = function (resetState) {
            if (resetState)
                $scope.Policy.ResidenceState = "";
            
            regionService.GetRegionNameByCountryCode($scope.Policy.ResidenceCountry).then(function(name){
                $scope.ResidenceRegionName = name;
            });
            regionService.GetRegionsByCountryCode($scope.Policy.ResidenceCountry).then(function(regions){
                $scope.AvailableResidenceRegions = regions;
            });
        };

        $scope.AbsenceReasonWorkCountryChanged = function (resetState) {
            if (resetState)
                $scope.AbsReasonEdit.WorkState = "";

            regionService.GetRegionNameByCountryCode($scope.AbsReasonEdit.WorkCountry).then(function (name) {
                $scope.AbsenceReasonWorkRegionName = name;
            });
            regionService.GetRegionsByCountryCode($scope.AbsReasonEdit.WorkCountry).then(function (regions) {
                $scope.AvailableAbsenceReasonWorkRegions = regions;
            });
        };

        $scope.AbsenceReasonResidenceCountryChanged = function (resetState) {
            if (resetState)
                $scope.AbsReasonEdit.ResidenceState = "";

            regionService.GetRegionNameByCountryCode($scope.AbsReasonEdit.ResidenceCountry).then(function (name) {
                $scope.AbsenceReasonResidenceRegionName = name;
            });
            regionService.GetRegionsByCountryCode($scope.AbsReasonEdit.ResidenceCountry).then(function (regions) {
                $scope.AbsenceReasonAvailableResidenceRegions = regions;
            });
        };

        reactivateTooltips = function () {
            window.setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
            }, 200);
        }

        setRuleGroupText = function (ruleGroup) {
            var typeText = $filter('filter')($scope.PolicyRuleGroupTypes, { Id: ruleGroup.RuleGroupType })[0];
            if (typeText)
                ruleGroup.TypeText = typeText.Text;

            var successText = $filter('filter')($scope.RuleGroupSuccessTypesList, { Id: ruleGroup.SuccessType })[0];
            if (successText)
                ruleGroup.SuccessText = successText.Text;
        }

        
        resetDisplay = function () {
            $scope.InitAbsReasonList();
            $scope.forms.submitted = false;
            $scope.NewPolicy = false;
            $scope.EditPolicy = false;
            $scope.NewPolicyReason = false;
            $scope.EditPolicyReason = false;
            $scope.NewRuleGroup = false;
            $scope.EditRuleGroup = false;
            $scope.PolicyReasonButtonText = "Save Absence Reason";
            reactivateTooltips();
        };

        loadPolicy = function () {
            policyService.GetById($scope.PolicyId, $scope.EmployerId).then(function (response) {
                $scope.Policy = response;
                $scope.Policy.IsDuplicate = false;
                transformPolicy();
                $scope.InitAbsReasonList();
                $scope.InitPoliciesSearch();
                $scope.PolicyReasonButtonText = "Save Absence Reason";
            });
        }

        transformPolicy = function () {
            if ($scope.Policy.EmployerId != null) {
                administrationService.GetEmployerInfo($scope.Policy.EmployerId).then(function (empData) {
                    $scope.Policy.EmployerName = empData.SignatureName;
                });
            } else {
                $scope.Policy.EmployerId = $scope.EmployerId;
            }

            $scope.IsCustomPolicy = $scope.Policy.IsCustom;

            $scope.Policy.EffectiveDate = $filter('ISODateToDate')($scope.Policy.EffectiveDate);

            angular.forEach($scope.Policy.AbsenceReasons, function (reason, index) {
            	reason.EffectiveDate = $filter('ISODateToDate')(reason.EffectiveDate);
                reason.CaseTypeConsecutive = (reason.CaseTypes & 1) == 1;
                reason.CaseTypeIntermittent = (reason.CaseTypes & 2) == 2;
                reason.CaseTypeReduced = (reason.CaseTypes & 4) == 4;
                reason.CaseTypeAdministrative = (reason.CaseTypes & 8) == 8;

                if (reason.EntitlementType != undefined ||
                       reason.EntitlementType != null ||
                       reason.EntitlementType != '') {

                    var list = $filter('filter')($scope.EntitlementTypesList, { Id: reason.EntitlementType });
                    if (list.length > 0) {
                        reason.EntitlementTypeText = list[0].Text;
                    }
                    else {
                        reason.EntitlementTypeText = '';
                    }
                }

                reason.IntermittentRestrictionsMinimumTimeUnitTypeText = getIntermittentRestrictionsCombosText($scope.EntitlementTypesList, reason.IntermittentRestrictionsMinimumTimeUnit);
                reason.IntermittentRestrictionsMaximumTimeUnitTypeText = getIntermittentRestrictionsCombosText($scope.EntitlementTypesList, reason.IntermittentRestrictionsMaximumTimeUnit);
                reason.IntermittentRestrictionsCalcTypeTypeText = getIntermittentRestrictionsCombosText($scope.IntermittentRestrictionsTypesList, reason.IntermittentRestrictionsCalcType);                               

                angular.forEach(reason.RuleGroups, getRuleExpressions);
            });

            angular.forEach($scope.PolicyTypesList, function (policyType, index) {
                if (policyType.Id == $scope.Policy.PolicyType) {
                    $scope.PolicyTypeText = policyType.Text;
                }
            });

            angular.forEach($scope.PolicyBehaviorTypesList, function (policyBehaviorType, index) {
                if (policyBehaviorType.Id == $scope.Policy.PolicyBehaviorType) {
                    $scope.PolicyBehaviorTypeText = policyBehaviorType.Text;
                }
            });

            angular.forEach($scope.GenderTypesList, function (genderType, index) {
                if (genderType.Id == $scope.Policy.Gender) {
                    $scope.GenderTypeText = genderType.Text;
                }
            });

            angular.forEach($scope.Policy.RuleGroups, getRuleExpressions);

            resetDisplay();
            $scope.WorkCountryChanged(false);
            $scope.ResidenceCountryChanged(false);
        }

        getIntermittentRestrictionsCombosText = function (typeOfLists, idType) {
            if (idType != undefined ||
                idType != null ||
                idType != '') {
                var list = $filter('filter')(typeOfLists, { Id: idType });
                if (list.length > 0) {
                    return  list[0].Text;
                }
                else {
                    return '';
                }
            }
        }
        
        getRuleExpressions = function (ruleGroup, index) {
            var rules = {
                EmployerId: $scope.EmployerId,
                Rules: ruleGroup.Rules
            };

            setRuleGroupText(ruleGroup);
            policyService.RuleToRuleExpression(rules).then(function (result) {
                ruleGroup.RuleExpressions = result;
                angular.forEach(ruleGroup.RuleExpressions, function (expression, index) {
                    $scope.LeftExpressionChange(expression, false);
                })
            });
        }

        getRules = function (ruleGroup, index) {
            angular.forEach(ruleGroup.RuleExpressions, function (rule, index) {
                rule.StringValue = null;
                if (rule.Parameters) {
                    angular.forEach(rule.Parameters, function (param, index) {
                        param.StringValue = null;
                    });
                }
            });
            var expressions = {
                EmployerId: $scope.EmployerId,
                Expressions: ruleGroup.RuleExpressions
            }
            return policyService.ValidateExpression(expressions).then(function (rules) {
                ruleGroup.Rules = rules;
            });

        }
        
        upsertPolicy = function () {
            $scope.Policy.ConsecutiveTo = $("#consecutive-to-policies").select2("val");
            $scope.Policy.SubstituteFor = $("#substitute-for-policies").select2("val");            

            var ruleGroupPromises = [];
            angular.forEach($scope.Policy.RuleGroups, function (ruleGroup, index) {
                ruleGroupPromises.push(getRules(ruleGroup, index));
            });

            angular.forEach($scope.Policy.AbsenceReasons, function (reason, index) {
                angular.forEach(reason.RuleGroups, function (ruleGroup, rIndex) {
                    ruleGroupPromises.push(getRules(ruleGroup, rIndex));
                });
            });
            $q.all(ruleGroupPromises).then(checkRulesForPotentialConflicts);
        };

        checkRulesForPotentialConflicts = function () {
            policyService.CheckPolicyForConflicts($scope.Policy).then(function (potentialConflicts) {
                if (potentialConflicts.data.length == 0) {
                    $scope.SavePolicyToServer();
                } else {
                    $scope.ConflictingRules = potentialConflicts.data;
                    $('#conflictingRules').modal('show');
                }
            });
        }

        $scope.SavePolicyToServer = function () {
            $('#conflictingRules').modal('hide');
            policyService.Upsert($scope.Policy).then(function (response) {
                $scope.EmployerFMLPeriodType = response.EmployerPeriodType;
                $scope.PolicyId = response.Policy.Id;
                loadPolicy();
              
                transformPolicy();
            });
        }

        $scope.SavePolicy = function (IsValid, error) {
            if (!$scope.frmCreatePolicy.$valid) {
                var firstInvalid = $("#frmCreatePolicy").find(".ng-invalid:first");
                var scrollTo = firstInvalid.offset().top / 2;
                $('html, body').animate({ scrollTop: scrollTo }, 1000);
                return;
            }
            
            if ($scope.NewPolicy) {
                $scope.NewPolicy = false;
                $scope.NewPolicyReason = true;
            } else {
                upsertPolicy();
            }

        };

        $scope.CancelPolicy = function ($event) {
            if ($scope.EditPolicy) {
                if ($scope.frmCreatePolicy.$dirty)
                    loadPolicy();
                $scope.EditPolicy = false;
                $event.preventDefault();
            }
        };

        $scope.CancelPolicyReason = function ($event) {
            if (!$scope.NewPolicy) {
                if ($scope.frmPolicyAbsReason.$dirty)
                    loadPolicy();

                resetDisplay();
                $event.preventDefault();
            }
        };

        $scope.EditPolicyClick = function (policyId) {            
            if ($scope.BrowserSupportsStorage) {
                try {
                    window.localStorage.setItem("PolicyFilter", JSON.stringify($scope.PolicyFilter));
                }
                catch (e) {
                    //Ignore
                }
            }
            window.location.href = "/Policies/" + policyId + "/UpsertPolicy/" + $scope.EmployerId;
        }

        $scope.BrowserSupportsStorage = function() {
            try {
                return 'localStorage' in window && window['localStorage'] !== null;
            } catch (e) {
                return false;
            }
        }

        $scope.TogglePolicy = function () {
            policyService.ToggleEnabled($scope.Policy.Id, $scope.EmployerId);
            $scope.Policy.IsDisabled = !$scope.Policy.IsDisabled;
        };

        $scope.DeletePolicy = function () {
            var policyData = {
                EmployerId: $scope.EmployerId,
                Code: $scope.Policy.Code
            }
            policyService.Delete(policyData);
            $window.location = '/Policies/' + $scope.EmployerId;
        }

        $scope.CheckExistingPolicy = function () {
            var policyData = {
                EmployerId: $scope.EmployerId,
                PolicyId: $scope.Policy.Id,
                Code: $scope.Policy.Code
            }
            policyService.CheckForDuplicatePolicy(policyData).then(function (result) {
                $scope.Policy.IsDuplicate = result.data;
            });
        }


        //end policy

        //policy abs reason
        $scope.InitAbsReason = function () {
            $scope.forms.submitted = false;
            $scope.AbsReasonEdit = {};
            $scope.AbsReasonEdit.PaymentTiers = [];
            $scope.AbsReasonEdit.PolicyEvents = [];
            $scope.AbsReasonEdit.RuleGroups = [];

            $scope.AbsReasonEdit.EffectiveDate = new Date().toFakeUTCDay();
            			
            $scope.AbsReasonEdit.CaseTypeConsecutive = false;
            $scope.AbsReasonEdit.CaseTypeIntermittent = false;
            $scope.AbsReasonEdit.CaseTypeReduced = false;
            $scope.AbsReasonEdit.CaseTypeAdministrative = false;

            $scope.SetInheritedPeriodType();
        };

        $scope.UpsertPolicyAbsReason = function () {
            var isValid = false;
            if ($scope.NewPolicy) {
                isValid = $scope.frmCreatePolicy.$valid && $scope.frmPolicyAbsReason.$valid;
            } else {
                isValid = $scope.frmPolicyAbsReason.$valid;
            }

            if (isValid)
                isValid = $scope.AbsReasonEdit.CaseTypeAdministrative || $scope.AbsReasonEdit.CaseTypeReduced || $scope.AbsReasonEdit.CaseTypeIntermittent
                    || $scope.AbsReasonEdit.CaseTypeConsecutive;
            if (!isValid) {
                var firstInvalid;
                if ($scope.NewPolicy) {
                    firstInvalid = $("#frmCreatePolicy").find(".ng-invalid:first")[0];
                }
                if (!firstInvalid) {
                    firstInvalid = $("#frmPolicyAbsReason").find(".ng-invalid:first");
                }
                var scrollTo = $(firstInvalid).offset().top / 2;
                $('html, body').animate({ scrollTop: scrollTo }, 1000);
                return;
            }

            if ($scope.AbsReasonEdit.PeriodType == "-1") {
                $scope.AbsReasonEdit.PeriodType = $scope.EmployerFMLPeriodType;
            };
           

            if (isValid) {

                var caseTypesInt = 0;
                if ($scope.AbsReasonEdit.CaseTypeConsecutive) {
                    caseTypesInt += 1;
                }
                if ($scope.AbsReasonEdit.CaseTypeIntermittent) {
                    caseTypesInt += 2;
                }
                if ($scope.AbsReasonEdit.CaseTypeReduced) {
                    caseTypesInt += 4;
                }
                if ($scope.AbsReasonEdit.CaseTypeAdministrative) {
                    caseTypesInt += 8;
                }

                $scope.AbsReasonEdit.CaseTypes = caseTypesInt;
                
                //new policy abs reason
                if ($scope.AbsReasonEdit.Id == undefined || $scope.AbsReasonEdit.Id == null || $scope.AbsReasonEdit.Id == '') {
                    $scope.Policy.AbsenceReasons.push($scope.AbsReasonEdit);
                }
                $scope.AbsReasonEdit.SubstituteFor = $("#ar-substitute-for-policies").select2("val");
                upsertPolicy();
            }
        }

        $scope.SetAbsReasonForEdit = function (reason) {
            var AbsReasonEditList = $filter('filter')($scope.Policy.AbsenceReasons, { ReasonCode: reason.ReasonCode });
            if (AbsReasonEditList.length > 0) {                
                $scope.AbsReasonEdit = AbsReasonEditList[0];
            }
            $scope.AbsReasonEdit.CaseTypeConsecutive = ($scope.AbsReasonEdit.CaseTypes & 1) == 1; // 1 is the C# Enum value for consecutive cases
            $scope.AbsReasonEdit.CaseTypeIntermittent = ($scope.AbsReasonEdit.CaseTypes & 2) == 2; // 2 is the C# enum value for Intermittent cases
            $scope.AbsReasonEdit.CaseTypeReduced = ($scope.AbsReasonEdit.CaseTypes & 4) == 4; // 4 is the C# enum value for Reduced cases 
            $scope.AbsReasonEdit.CaseTypeAdministrative = ($scope.AbsReasonEdit.CaseTypes & 8) == 8; // 8 is the C# enum value for Administrative cases

            bindSelect2('#ar-substitute-for-policies', $scope.AbsReasonEdit.SubstituteFor);

            //policy events text
            angular.forEach($scope.AbsReasonEdit.PolicyEvents, function (event, index) {

                var CaseEventTypeList = $filter('filter')($scope.CaseEventTypesList, { Id: event.EventType });
                if (CaseEventTypeList.length > 0) {
                    event.CaseEventTypeText = CaseEventTypeList[0].Text;
                }

                var CaseEventDateTypeList = $filter('filter')($scope.CaseEventDateTypesList, { Id: event.DateType });
                if (CaseEventDateTypeList.length > 0) {
                    event.CaseEventDateTypeText = CaseEventDateTypeList[0].Text;
                }
            });

            $scope.PolicyRuleGroupTypesList = $scope.PolicyRuleGroupTypes;
            $scope.SetInheritedPeriodType();
            $scope.NewPolicyReason = false;
            $scope.EditPolicyReason = true;
            reactivateTooltips();
            $scope.AbsenceReasonWorkCountryChanged(false);
            $scope.SetDayForSelectedMonth($scope.AbsReasonEdit.ResetMonth);
            $scope.AbsenceReasonResidenceCountryChanged(false);
        };

        $scope.SetInheritedPeriodType = function () {
            //set inherited employer period type
            if (($scope.AbsReasonEdit.PeriodType === null || $scope.AbsReasonEdit.PeriodType === undefined || $scope.AbsReasonEdit.PeriodType ==='') &&
                $scope.EmployerFMLPeriodType != undefined &&
                $scope.EmployerFMLPeriodType != null &&
                $scope.EmployerFMLPeriodType != '') {
                var listVal = '';
                var periodList = $filter('filter')($scope.FMLPeriodTypesList, { Id: $scope.EmployerFMLPeriodType });
                if (periodList.length > 0)
                    listVal = periodList[0].Text + " (inherited)";

                var inheritedPeriodType = {
                    Id: "-1",
                    Text: listVal
                };

                $scope.FMLPeriodTypesList.unshift(inheritedPeriodType);
               

                $scope.AbsReasonEdit.PeriodType = "-1";

               
            };
        }

        $scope.RemoveAbsReason = function (reason) {

            if (confirm("Are you sure you want to delete this policy absence reason?")) {

                var data = {};
                data.EmployerId = '';
                data.PolicyId = reason.PolicyId;
                data.AbsReasonCode = reason.ReasonCode;

                $scope.Policy = policyService.RemoveAbsReason(data).then(function (response) {
                    $scope.Policy = response;

                    if ($scope.Policy.EmployerId != null) {
                        administrationService.GetEmployerInfo($scope.Policy.EmployerId).then(function (empData) {
                            $scope.Policy.EmployerName = empData.SignatureName;
                        });
                    }

                    $scope.InitAbsReasonList();

                    //var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "Absence Reason updated successfully" });

                    //reload data
                });
            }
        };

        $scope.EntitlementChange = function () {
            if ($scope.AbsReasonEdit.EntitlementType != undefined ||
                $scope.AbsReasonEdit.EntitlementType != null ||
                $scope.AbsReasonEdit.EntitlementType != '') {

                var list = $filter('filter')($scope.EntitlementTypesList, { Id: $scope.AbsReasonEdit.EntitlementType });
                if (list.length > 0) {
                    $scope.AbsReasonEdit.EntitlementTypeText = list[0].Text;
                    $scope.AbsReasonEdit.Entitlement = null;
                }
                else {
                    $scope.AbsReasonEdit.EntitlementTypeText = '';
                }
                if ($scope.AbsReasonEdit.EntitlementType == 9) {
                    $scope.AbsReasonEdit.Entitlement = 1;
                }

                if ($scope.AbsReasonEdit.EntitlementType != 10) {
                    $scope.AbsReasonEdit.FixedWorkDaysPerWeek = null;
                }
            }
        };

        $scope.PerUseCapRequired = function () {
            return ($scope.AbsReasonEdit.PerUseCapType != undefined && $scope.AbsReasonEdit.PerUseCapType != null && $scope.AbsReasonEdit.PerUseCapType != ''
                && ($scope.AbsReasonEdit.PerUseCap === undefined || $scope.AbsReasonEdit.PerUseCap === null || $scope.AbsReasonEdit.PerUseCap === ''));
        };

        $scope.PerUseCapTypeRequired = function () {
            return ($scope.AbsReasonEdit.PerUseCap != undefined && $scope.AbsReasonEdit.PerUseCap != null && $scope.AbsReasonEdit.PerUseCap != ''
                && ($scope.AbsReasonEdit.PerUseCapType === undefined || $scope.AbsReasonEdit.PerUseCapType === null || $scope.AbsReasonEdit.PerUseCapType === ''));
        };

        //end policy abs reason

        //rule groups

        $scope.SetRuleGroupTypes = function () {
            $scope.PolicyRuleGroupTypesList = [];
            for (var i = 0; i < $scope.PolicyRuleGroupTypes.length; i++) {
                var ruleGroupType = $scope.PolicyRuleGroupTypes[i];
                if (ruleGroupType.Text == "Eligibility" || ruleGroupType.Text == "Selection" || ruleGroupType.Text == "Crosswalk")
                    $scope.PolicyRuleGroupTypesList.push(ruleGroupType);
            }

            $scope.AbsenceReasonsRulesGroupTypesList = $scope.PolicyRuleGroupTypes;
        }

        $scope.AddPolicyRuleGroup = function () {
            $scope.frmCreatePolicy.$setDirty();
            $scope.Policy.RuleGroups.unshift(newRuleGroup());
            reactivateTooltips();
        };

        $scope.AddAbsenceReasonRuleGroup = function () {
            $scope.frmPolicyAbsReason.$setDirty();
            $scope.AbsReasonEdit.RuleGroups.unshift(newRuleGroup());
            reactivateTooltips();
        };

        newRuleGroup = function () {
            return {
                Name: '',
                Description: '',
                RuleExpressions: [],
                SuccessType: null,
                RuleGroupType: '',
                Entitlement: null,
                ShowRuleGroupDelete: true,
                PaymentEntitlement: [],
                ShowRules: false,
                ShowPayments: false
            };
        }

        $scope.InitRuleExpressions = function () {
            //get the rule expression groups
            if ($scope.RuleExpressionGroups == undefined ||
                $scope.RuleExpressionGroups == null ||
                $scope.RuleExpressionGroups.length == 0) {

                policyService.GetRuleExpressions($scope.EmployerId).then(function (response) {
                    ruleExpressions = [];
                    /// make the results play nice with the UI
                    angular.forEach(response, function (ruleExpressionGroup, index) {
                        angular.forEach(ruleExpressionGroup.Expressions, function (expression, index) {
                            var ruleExpression = {
                                Name: expression.Name,
                                Prompt: expression.Prompt,
                                Title: ruleExpressionGroup.Title,
                                Operators: expression.Operators,
                                AvailableOperators: [],
                                Options: expression.Options,
                                ControlType: expression.ControlType,
                                Parameters: expression.Parameters,
                                IsFunction: expression.IsFunction,
                                IsCustom: expression.IsCustom,
                                TypeName: expression.TypeName,
                                RuleOperator: expression.RuleOperator,
                                Value: expression.Value
                            };
                            for (var property in expression.Operators) {
                                if (expression.Operators.hasOwnProperty(property)) {
                                    var operator = {
                                        Id: property,
                                        Text: expression.Operators[property]
                                    };
                                    ruleExpression.AvailableOperators.push(operator);
                                }
                            }
                            ruleExpressions.push(ruleExpression);
                        });
                    });

                    //Create the left side DDL list
                    $scope.RuleExpressionGroups = ruleExpressions;

                });
            }
        };

        function searchValue(nameKey, myArray) {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].Name === nameKey) {
                    return myArray[i];
                }
            }
        }
        
        //populate the operator based on left side choice
        $scope.LeftExpressionChange = function (expression, restart) {
            var expressionGroup = $scope.RuleExpressionGroups;
            var selectedExpression = searchValue(expression.Name, expressionGroup);
           
            if (selectedExpression) {
                selectedExpression = angular.copy(selectedExpression);
                expression.ControlType = selectedExpression.ControlType;
                expression.AvailableOperators = selectedExpression.AvailableOperators;
                expression.Options = selectedExpression.Options;
                if (!restart) {
                    angular.forEach(expression.Parameters, function (p, i) {
                        selectedExpression.Parameters[i].Value = p.Value;
                    });
                }
                expression.Parameters = selectedExpression.Parameters;                
                expression.Operators = selectedExpression.Operators;
                expression.IsFunction = selectedExpression.IsFunction;
                expression.IsCustom = selectedExpression.IsCustom;
                expression.TypeName = selectedExpression.TypeName;
            } else {
                expression.ControlType = null;
                expression.AvailableOperators = [];
                expression.Options = [];
                expression.Parameters = [];
                expression.Operators = [];
                expression.IsFunction = false;
                expression.IsCustom = false;
                expression.TypeName = null;
            }

            if (restart) {
                if (expression.Parameters.length > 0 || expression.ControlType == 8) {
                    expression.RuleOperator = selectedExpression.RuleOperator != null ? selectedExpression.RuleOperator : "==";
                    expression.Value = selectedExpression.Value != null ? selectedExpression.Value : true;
                } else {
                    expression.Value = null;
                    expression.RuleOperator = null;
                }
            }
            $scope.InitRelationSearch(expression);
        };

        $scope.DeletePolicyRuleGroup = function (ruleGroup) {
            $scope.frmCreatePolicy.$setDirty();
            var index = $scope.Policy.RuleGroups.indexOf(ruleGroup);
            if (index !== -1) {
                $scope.Policy.RuleGroups.splice(index, 1);
            }
        };

        $scope.DeleteAbsenceReasonRuleGroup = function (ruleGroup) {
            $scope.frmPolicyAbsReason.$setDirty();
            var index = $scope.AbsReasonEdit.RuleGroups.indexOf(ruleGroup);
            if (index !== -1) {
                $scope.AbsReasonEdit.RuleGroups.splice(index, 1);
            }
        }

        $scope.AddExpression = function (ruleGroup) {
            $scope.frmCreatePolicy.$setDirty();
            $scope.frmPolicyAbsReason.$setDirty();
            var expression = {
                RuleName: '',
                Operator: '',
                StringValue: ''
            };
            ruleGroup.RuleExpressions.unshift(expression);
            ruleGroup.ShowRules = true;
        };

        $scope.DeleteExpression = function (ruleGroup, expression) {
            $scope.frmCreatePolicy.$setDirty();
            $scope.frmPolicyAbsReason.$setDirty();
            var index = ruleGroup.RuleExpressions.indexOf(expression);
            if (index !== -1) {
                ruleGroup.RuleExpressions.splice(index, 1);
            };
        };

        $scope.SetRuleGroupClass = function () {
            if ($scope.forms.submitted == true && $scope.RuleGroupsEdit != null && $scope.RuleGroupsEdit.SuccessType == null) {
                return 'has-error';
            };
            return '';
        };

        $scope.SetRuleClass = function (rule, expression) {
            if ($scope.forms.submitted == true) {
                if (expression == 'left' && rule.LeftExpression == null) {
                    return 'has-error';
                };

                if (expression == 'operator' && rule.Operator == null) {
                    return 'has-error';
                };

                if (expression == 'right' && rule.RightExpression == null) {
                    return 'has-error';
                };
            };

            return '';
        };

        $scope.SetParamClass = function (param) {
            if ($scope.forms.submitted == true) {
                if (param.Value == null)
                    return 'has-error';
            };
        };
        //end policy abs reason rules

        //payment tiers
        $scope.AddPaymentTier = function () {
            $scope.frmPolicyAbsReason.$setDirty();
            $scope.AbsReasonEdit.PaymentTiers.push(newPaymentTier());
        };

        $scope.DeletePaymentTier = function (tier) {
            var index = $scope.AbsReasonEdit.PaymentTiers.indexOf(tier);
            if (index !== -1) {
                $scope.AbsReasonEdit.PaymentTiers.splice(index, 1);
            }
        };

        $scope.AddRuleGroupPaymentTier = function (ruleGroup) {
            ruleGroup.ShowPayments = true;
            ruleGroup.PaymentEntitlement.push(newPaymentTier());
        };

        $scope.DeleteRuleGroupPaymentTier = function (ruleGroup, tier) {
            var index = ruleGroup.PaymentEntitlement.indexOf(tier);
            if (index !== -1) {
                ruleGroup.PaymentEntitlement.splice(index, 1);
            }
        }

        newPaymentTier = function () {
            return {
                Id: '',
                Order: '',
                PaymentPercentage: '',
                MaxPaymentPercentage: '',
                Duration: ''
            };
        }
        //end payment tiers

        //policy events
        $scope.AddPolicyEvent = function () {
            $scope.PolicyEventEdit = {};
            $scope.NewPolicyEvent = true;
            $scope.EditPolicyEvent = false;
            $scope.forms.submitted = false;
        };

        $scope.CreatePolicyEvent = function (IsValid) {
            if (IsValid) {
                $scope.PolicyEventEdit.Id = null;
                $scope.AbsReasonEdit.PolicyEvents.push($scope.PolicyEventEdit);

                //policy events text
                angular.forEach($scope.AbsReasonEdit.PolicyEvents, function (event, index) {

                    var CaseEventTypeList = $filter('filter')($scope.CaseEventTypesList, { Id: event.EventType });
                    if (CaseEventTypeList.length > 0) {
                        event.CaseEventTypeText = CaseEventTypeList[0].Text;
                    }

                    var CaseEventDateTypeList = $filter('filter')($scope.CaseEventDateTypesList, { Id: event.DateType });
                    if (CaseEventDateTypeList.length > 0) {
                        event.CaseEventDateTypeText = CaseEventDateTypeList[0].Text;
                    }
                });


                $("#AddPolicyEvents").modal('hide');
            }
        };

        $scope.UpdatePolicyEvent = function (IsValid) {
            if (IsValid) {

                //policy events text
                angular.forEach($scope.AbsReasonEdit.PolicyEvents, function (event, index) {

                    var CaseEventTypeList = $filter('filter')($scope.CaseEventTypesList, { Id: event.EventType });
                    if (CaseEventTypeList.length > 0) {
                        event.CaseEventTypeText = CaseEventTypeList[0].Text;
                    }

                    var CaseEventDateTypeList = $filter('filter')($scope.CaseEventDateTypesList, { Id: event.DateType });
                    if (CaseEventDateTypeList.length > 0) {
                        event.CaseEventDateTypeText = CaseEventDateTypeList[0].Text;
                    }
                });

                $("#AddPolicyEvents").modal('hide');
            };
        };

        $scope.SetPolicyEventForEdit = function (event) {
            $scope.PolicyEventEdit = event;
            $scope.NewPolicyEvent = false;
            $scope.EditPolicyEvent = true;
            $scope.forms.submitted = false;
        };

        $scope.DeletePolicyEvent = function (event) {
            if (confirm("Are you sure you want to delete this policy event?")) {
                var index = $scope.AbsReasonEdit.PolicyEvents.indexOf(event);

                if (index !== -1) {
                    $scope.AbsReasonEdit.PolicyEvents.splice(index, 1);
                }
            };
        };
        //end policy events

        //helpers

        $scope.RuleGroupTypeChange = function (ruleGroup) {
            if (ruleGroup.RuleGroupType != 3) {
                ruleGroup.PaymentEntitlement = [];
            }
        }

        $scope.SetRuleGroupTypeText = function () {
            angular.forEach($scope.RuleGroupSuccessTypesList, function (item, index) {

                if (item.Text == 'And') {
                    item.Text = 'All';
                };

                if (item.Text == 'Or') {
                    item.Text = 'Any';
                };

                if (item.Text == 'Nor') {
                    item.Text = 'None';
                };
            });
        };

        $scope.InitGenderTypeList = function () {

            $scope.GenderTypesList = [];
            $scope.GenderObjNull = {
                Id: null,
                Text: 'N/A'
            };
            $scope.GenderObjMale = {
                Id: 77,
                Text: 'Male'
            };
            $scope.GenderObjFemale = {
                Id: 70,
                Text: 'Female'
            };
            $scope.GenderTypesList.push($scope.GenderObjNull);
            $scope.GenderTypesList.push($scope.GenderObjMale);
            $scope.GenderTypesList.push($scope.GenderObjFemale);
        };

        $scope.InitCoreAbsReasons = function () {

            absenceReasonService.List(
            {
                Name: '',
                Code: '',
                Category: '',
                PageSize: 0,
                EmployerId: $scope.EmployerId
            })
            .$promise.then(
            function (data, status) {
                if (data.Results != null && data.Results != '' && JSON.stringify(data.Results) != '[]') {
                    $scope.CoreAbsReasons = [];
                    /// We need to keep these reasons at the top
                    addAbsenceReasonByCode('EHC', data.Results);
                    addAbsenceReasonByCode('FHC', data.Results);
                    addAbsenceReasonByCode('PREGMAT', data.Results);
                    addAbsenceReasonByCode('MILITARY', data.Results);
                    addAbsenceReasonByCode('EXIGENCY', data.Results);
                    addAbsenceReasonByCode('ADOPT', data.Results);

                    /// everything else should be alphabetical
                    var orderedAbsenceReasons = $filter('orderBy')(data.Results, 'Name');
                    for (var i = 0; i < orderedAbsenceReasons.length; i++) {
                        $scope.CoreAbsReasons.push(orderedAbsenceReasons[i]);
                    }
                }
            });
        };

        addAbsenceReasonByCode = function (code, data) {
            var reason = $filter('filter')(data, { Code: code })[0];
            if (reason) {
                $scope.CoreAbsReasons.push(reason);
                var index = data.indexOf(reason);
                if (index != -1) {
                    data.splice(index, 1);
                }
            }
        }

        $scope.InitRelationSearch = function(expression) {
            if (expression != 'undefined' && expression.Options.length > 0 && expression.ControlType == 12) {
                var transformedData = [];
                $.each(expression.Options,
                    function(key, value) {
                        var option = {
                            id: value.Value,
                            text: value.Text
                        }
                        transformedData.push(option);
                    });

                $scope.Options = transformedData;
                $("input.multiplevalues").select2({
                    placeholder: "Search...",
                    allowClear: true,
                    minimumInputLength: 1,
                    dropdownCssClass: "bigdrop",
                    multiple: true,
                    data: $scope.Options
                });
                $("input.multiplevalues").val(expression.Value).trigger('change');
            }
        };

        $scope.InitPoliciesSearch = function () {
            /// get the list of policies for this employer
            var policyCriteria = {
                EmployerId: $scope.EmployerId,
                PageSize: 0
            };
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/Policies/List',
                contentType: 'application/json',
                data: JSON.stringify(policyCriteria),
                success: function (data) {
                    var transformedData = [];
                    for (var i = 0; i < data.Results.length; i++) {
                        var option = {
                            id: data.Results[i].Code,
                            text: data.Results[i].Name
                        }
                        transformedData.push(option);
                    }
                    $scope.PolicyList = transformedData;
                    bindSelect2('#consecutive-to-policies', $scope.Policy.ConsecutiveTo); 
                    bindSelect2('#substitute-for-policies', $scope.Policy.SubstituteFor);  
                    bindSelect2('#ar-substitute-for-policies', $scope.AbsReasonEdit.SubstituteFor);

                }
            });
        };

        bindSelect2 = function (selector, data) {
            $(selector).select2({
                placeholder: "Search Policies...",
                allowClear: true,
                minimumInputLength: 2,
                dropdownCssClass: "bigdrop",
                multiple: true,
                data: $scope.PolicyList
            });
            $(selector).val(data).trigger('change');
        };


        //policy List
        $scope.InitList = function (employerId, selectedPolicyId) {
            $scope.EmployerId = employerId;
            $scope.SelectedPolicyId = selectedPolicyId;
            $scope.OnlyPageRecords = (selectedPolicyId == null || selectedPolicyId == "" || selectedPolicyId == undefined ? true : false);

            administrationService.GetEmployerInfo(employerId).then(function (response) {
                $scope.EmployerInfo = response;
            });

            var policyFilter = ((selectedPolicyId == null || selectedPolicyId == "" || selectedPolicyId == undefined
            || $scope.BrowserSupportsStorage == false || window.localStorage.getItem("PolicyFilter") == null || window.localStorage.getItem("PolicyFilter") == undefined
            || window.localStorage.getItem("PolicyFilter") == '' ? null : jQuery.parseJSON(window.localStorage.getItem("PolicyFilter"))))
            policyListManager.Init($scope, policyFilter);
            if (policyFilter != null) {
                window.localStorage.removeItem("PolicyFilter");
            }
        };

        $scope.GetPolicies = function (doPaging) {
            $scope.OnlyPageRecords = true;
            policyListManager.ResetPaging($scope);
            policyListManager.List($scope, doPaging, $scope.ErrorHandler);
        };

        $scope.UpdatePolicySort = function (sortBy) {
            $scope.SelectedPolicyId = null;
            $scope.OnlyPageRecords = true;            
            policyListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);            
        };

        $scope.LoadMorePolicies = function () {
            $scope.OnlyPageRecords = true;
            $scope.SelectedPolicyId = null;            
            policyListManager.LoadMore($scope, $scope.ErrorHandler);
        };
        //end policy list

        //abs reason list
        $scope.InitAbsReasonList = function () {            
            policyAbsReasonListManager.Init($scope);
        };

        $scope.GetAbsReasons = function (doPaging) {
            policyAbsReasonListManager.List($scope, doPaging, $scope.ErrorHandler);
        };

        $scope.UpdateAbsReasonSort = function (sortBy) {
            policyAbsReasonListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };

        $scope.LoadMoreAbsReasons = function () {
            policyAbsReasonListManager.LoadMore($scope, $scope.ErrorHandler);
        };
        //end abs reason list

        $scope.Back = function () {

            $window.history.back();
        }

        $scope.SetDayForSelectedMonth = function (monthNumber) {
        	        	
        	if (monthNumber == null)
        	{
        		$scope.AbsReasonEdit.ResetDayOfMonth = null;
        	}
        	
        	var year = (new Date).getFullYear();
        	var month = parseInt(monthNumber);
        	
        	$scope.TotalDays = new Date(year, month, 0).getDate();
        	
        	var Days = [];
        	for (i = 1; i <= $scope.TotalDays; i++) {
        		Days.push({ "items": i });
        	}
        	$scope.Days = Days;
        }       

    }]);