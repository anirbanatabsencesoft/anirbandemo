//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('EditCompanyInfoCtrl', ['$scope', '$modal', '$window', function ($scope, $modal, $window) {
    $scope.openEditCompanyModal = function () {
        var EditCompanyInfoModalInstance = $modal.open({
            templateUrl: 'EditCompanyInfo.html',
            controller: 'EditCompanyInfoModalCtrl',
            //resolve: {
            //    userId: function () {
            //        return userId;
            //    }
            //}
        });

        EditCompanyInfoModalInstance.result.then(function () {
        }, function () {
            $window.location.reload();
        });
    };

    $scope.openEditSecuritySettingsModal = function () {
        var EditSecuritySettingsModalInstance = $modal.open({
            templateUrl: 'EditSecuritySettings.html',
            controller: 'EditSecuritySettingsModalCtrl'
        });

        EditSecuritySettingsModalInstance.result.then(function () {
        }, function () {
            $window.location.reload();
        });
    };
}]);



