//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller("employeeInfoCtrl", ['$scope', '$filter', '$sce', 'employeeService', 'userPermissions',
    function ($scope, $filter, $sce, employeeService, userPermissions) {
        $scope.EmployeeInfo = {
            HasRiskProfileFeature: false
        };
        
        $scope.IsTPA = false;
        $scope.ShowAllWorkSchedules = false;

        $scope.initEmpInfo = function () {
            $scope.ShowNotes = true;
            $scope.GetEmployeeInfo();

            $scope.ViewNotes = false;
            $scope.CreateNotes = false;
            $scope.EditNotes = false;

            $scope.ViewPIIPortal = userPermissions.indexOf("ViewPIIPortal") > -1;
            $scope.ViewPHIPortal = userPermissions.indexOf("ViewPHIPortal") > -1;
            $scope.ViewNotes = userPermissions.indexOf("ViewNotes") > -1;
            $scope.CreateNotes = userPermissions.indexOf("CreateNotes") > -1;
            $scope.EditNotes = userPermissions.indexOf("EditNotes") > -1;
        }

        $scope.GetEmployeeInfo = function () {
            var caseId = null;

            if ($scope.CaseId != null && $scope.CaseId != undefined && $scope.CaseId != '') {
                caseId = $scope.CaseId;
                $scope.ShowNotes = false;
            }

            employeeService.GetEmployeeInfo($scope.EmployeeId, caseId).then(function (response) {
                $scope.EmployeeInfo = response;
                employeeService.GetEmployeeLocationHistory(response.EmployeeNumber, response.EmployerId).then(function (locationHistory) {
                    $scope.EmployeeInfo.LocationHistory = locationHistory;
                });
                $scope.EmployeeFullName = response.FirstName + ($scope.isNullOrWhitespace(response.MiddleName) ? " " : " " + response.MiddleName + " ") + response.LastName;
                $scope.EmployeeName = $filter('limitTo')($scope.EmployeeFullName, 25);
                $scope.EmployeeName = $scope.EmployeeName + "...";

                if (response != undefined && response != null) {
                    if (response.CustomFields.length > 0) {
                        angular.forEach(response.CustomFields, function (value, key) {
                            if (value.DataType == 8 || value.DataType == 'Date') {
                                var date = Date.parse(value.SelectedValue);
                                if (!isNaN(date) && date != "Invalid Date") {
                                    value.SelectedValue = $filter('date')($filter('ISODateToDate')(value.SelectedValue), 'MM/dd/yyyy');
                                }
                            }
                        });
                    }
                }
                $scope.EmployeeInfo.WorkSchedules = $filter('orderBy')($scope.EmployeeInfo.WorkSchedules, 'StartDate', true);
                showFirstFourWorkSchedules();
                $scope.WorkScheduleText = 'more';

                $scope.ShowDOB = false;
                $scope.DoBEyeClick = function () {
                    $scope.ShowDOB = !$scope.ShowDOB;
                };
                $scope.ShowSalary = false;
                $scope.SalaryEyeClick = function () {
                    $scope.ShowSalary = !$scope.ShowSalary;
                };
                $scope.ShowSSN = false;
                $scope.SSNEyeClick = function () {
                    $scope.ShowSSN = !$scope.ShowSSN;
                };
                if (response.Email != null) {
                    $scope.EmailPieces = (response.Email).split('.');
                    $scope.DomainExt = $scope.EmailPieces[$scope.EmailPieces.length - 1];

                    $scope.EmployeeEmail = $filter('limitTo')(response.Email, 20 - $scope.DomainExt.length);
                    $scope.EmployeeEmail = $scope.EmployeeEmail + '....' + $scope.DomainExt;
                }

                if (response.AltEmail != null) {
                    $scope.EmailPieces = (response.AltEmail).split('.');
                    $scope.DomainExt = $scope.EmailPieces[$scope.EmailPieces.length - 1];
                    $scope.EmployeeAltEmail = $filter('limitTo')(response.AltEmail, 20 - ($scope.DomainExt.length + 3));
                    $scope.EmployeeAltEmail = $scope.EmployeeAltEmail + '....' + $scope.DomainExt;
                }

            });
        }

        $scope.ToggleWorkSchedules = function ($event) {
            if ($scope.ShowAllWorkSchedules) {
                $scope.WorkScheduleText = 'more';
                showFirstFourWorkSchedules();
            } else {
                $scope.WorkScheduleText = 'less';
                for (var i = 0; i < $scope.EmployeeInfo.WorkSchedules.length; i++) {
                    $scope.EmployeeInfo.WorkSchedules[i].ShowWorkSchedule = true;
                }
            }
            $scope.ShowAllWorkSchedules = !$scope.ShowAllWorkSchedules;
            $event.preventDefault();
        }

        showFirstFourWorkSchedules = function () {
            for (var i = 0; i < $scope.EmployeeInfo.WorkSchedules.length; i++) {
                $scope.EmployeeInfo.WorkSchedules[i].ShowWorkSchedule = i <= 3;
            }
        }

        $scope.CalculateRiskProfile = function () {
            employeeService.CalculateRiskProfile($scope.EmployeeId).$promise.then(function (riskProfile) {
                /// If it comes back with the Data property, and the Data property is false, we didn't actually calculate a new risk profile
                var riskProfileReturned = !riskProfile.hasOwnProperty('Data') && riskProfile.Data !== false;

                var successMessage = "Risk profile successfully calculated.";
                if (!riskProfileReturned)
                    successMessage += " No risk profile applies to this employee";

                noty({ text: successMessage, layout: 'topRight', type: 'success', timeout: 10000 });

                if (riskProfileReturned)
                    $scope.EmployeeInfo.RiskProfile = riskProfile
                else
                    $scope.EmployeeInfo.RiskProfile = null;
            })
        }

        $scope.$on('ReloadEmployeeInfo', function (event, args) {
            $scope.GetEmployeeInfo();
        });

        $scope.hiImALink = function (data) {
            data = $filter('nl2br')(data);

            if (data && data.toString().indexOf('<br') >= 0) {
                return $sce.trustAsHtml(data);
            }

            return data;
        };

        $scope.isNullOrWhitespace = function (str) {
            return !str || !str.trim();
        };

    }]);
