//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('CaseActivityCtrl', ['$scope', '$http', '$window', '$filter', '$sce', '$modal', 'userPermissions', 'noteService',
    function ($scope, $http, $window, $filter, $sce, $modal, userPermissions, noteService) {

        $scope.CaseActivity = [];
        $scope.PrintToDo = true;
        $scope.PrintAttachment = true;
        $scope.PrintCommunication = true;
        $scope.PrintNote = true;
        $scope.DateFilter = '';
        $scope.CategoryFilter = '';
        $scope.WrittenByFilter = '';
        $scope.DescriptionFilter = '';        
        $scope.SortExpression = 'CreatedDate';
        $scope.SortAsc = false,        
        $scope.init = function (caseActivity) {
            
            angular.forEach(caseActivity.Results, function (result) {
                if (result.StatusId == '1') {
                    result.CreatedDate = new Date(result.ModifiedDate);
                }
                else {
                    result.CreatedDate = new Date(result.CreatedDate);
                }
            })
            var sortedActivity = $filter('orderBy')(caseActivity.Results, 'CreatedDate', true);
            $scope.CaseActivity = sortedActivity;
            $scope.CaseActivityTotal = caseActivity.Total;
        };        
        
        $scope.GetCaseActivity = function () {
            /// do the thing
            
        };
        

        $scope.hiImALink = function (data) {
            data = data.replace('[', '');
            data = data.replace(']', '');
            if (data.length > 55) {
                data = $filter('limitTo')(data, 50);
                data = data + " ...";
            }
            data = $filter('nl2br')(data);

            if (data && data.toString().indexOf('<br') >= 0) {
                return $sce.trustAsHtml(data);
            }

            return data;
        };

        $scope.hiImALinks = function (data, contact, answer, category, sAnswer) {
            data = data.replace('[', '');
            data = data.replace(']', '');
            if (answer != undefined || category == "InteractiveProcess" || sAnswer != undefined) {
                var ans;
                if (!answer) {
                    if (sAnswer) {
                        ans = sAnswer;
                    }
                    else {
                        ans = "N/A";
                    }
                }
                else {
                    ans = answer == true ? "Yes" : "No";
                }
                data = data + '<br />' + 'Answer - ' + ans;
            }

            if (contact != null && contact != undefined && contact != '') {
                data = data + '<br />' + 'Contact - ' + contact;
            }

            data = $filter('nl2br')(data);

            if (data && data.toString().indexOf('<br') >= 0) {
                return $sce.trustAsHtml(data);
            }

            return data;
        };


       
        $scope.ToggleAllActivity = function (visible) {
            for (var i = 0; i < $scope.CaseActivity.length; i++) {
                $scope.CaseActivity[i].visible = visible;
            }
        };

        $scope.FilterCaseActivity = function (activity) {
            if ($scope.CategoryFilter != null && $scope.CategoryFilter != undefined && $scope.CategoryFilter != '' && activity.Category.toLowerCase().indexOf($scope.CategoryFilter.toLowerCase()) < 0) {
                return false;
            }
            if ($scope.WrittenByFilter != null && $scope.WrittenByFilter != undefined && $scope.WrittenByFilter != '' && activity.CreatedBy.toLowerCase().indexOf($scope.WrittenByFilter.toLowerCase()) < 0) {
                return false;
            }
            if ($scope.DateFilter != null && $scope.DateFilter != undefined && $scope.DateFilter != '' && $filter('date')(activity.CreatedDate, 'MM/dd/yy').indexOf($scope.DateFilter.toLowerCase()) < 0) {
                return false;
            }
            if ($scope.DescriptionFilter != null && $scope.DescriptionFilter != undefined && $scope.DescriptionFilter != '' && activity.Description.toLowerCase().indexOf($scope.DescriptionFilter.toLowerCase()) < 0) {
                return false;
            }
            return true;
        };
        $scope.UpdateCaseActivitySort = function (sortExpression) {
            if ($scope.SortExpression == sortExpression) {
                if ($scope.SortAsc == true) {
                    $scope.SortAsc = false
                }
                else {
                    $scope.SortAsc = true
                }
            }
            else {
                $scope.SortAsc = false
            }
            $scope.SortExpression = sortExpression;
        };
        
        $scope.SelectPrintTypes = function () {

            var selectPrintTypesModal = $modal.open({
                templateUrl: 'SelectActivityType.html',                
                controller: PrintTypesCtrlInstance,
                resolve: {
                    result: function () {
                        var result = {
                            toDo: $scope.PrintToDo,
                            communication: $scope.PrintCommunication,
                            attachment: $scope.PrintAttachment,
                            notes: $scope.PrintNote
                        }
                        return result;
                    }
                }
            });
            selectPrintTypesModal.result.then(function (result) {
                if (result != undefined){
                    $scope.PrintToDo = result.toDo;
                    $scope.PrintCommunication = result.communication;
                    $scope.PrintAttachment = result.attachment;
                    $scope.PrintNote = result.notes;            
                }
            });            
        }
    }]);

PrintTypesCtrlInstance = function SelectPrintTypesCtrlInstance($scope, $modalInstance, result) {
    $scope.result = result;

    $scope.cancel = function () {
        $scope.$close();
    };
    $scope.PrintTypes = function(){
        $scope.$close($scope.result);
    };
};
PrintTypesCtrlInstance.$inject = ['$scope', '$modalInstance', 'result'];

