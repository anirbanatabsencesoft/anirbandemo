(function () {
    'use strict';
    angular
        .module('App.Controllers')
        .controller('EditRolesCtrl', Controller);

    Controller.$inject = ['$log', '$scope', '$http', 'administrationService', '$filter', 'lookupService', '_'];
    function Controller($log, $scope, $http, administrationService, $filter, lookupService, _) {
        var vm = $scope;
        activate();

        function activate() {
            angular.extend(vm, {
                // props                
                SelectedRoleId: null,
                AllPermissions: [], // TODO: remove, not needed
                PermissionsViewModel: [],
                Error: null,
                DefaultEditRole: {},
                EditRole: {
                    Permissions: [],
                    IsNewRole: false
                },
               
                //
                // methods
                ErrorHandler: ErrorHandler,
                Init: Init,
                permissionsFilter: permissionsFilter,
                DeleteRole: DeleteRole,
                UpdateRole: UpdateRole,
                CreateRole: CreateRole,
                ExpandCollapse: ExpandCollapse,
                RoleInit: RoleInit,
                InitPermissions: InitPermissions,
                AddContactType: AddContactType,
                RemoveContactType: RemoveContactType,
                CopyFromRole: CopyFromRole,
                CheckUncheck: CheckUncheck,
                LoginBehaviorFeatures: null,
                LoginBehaviorOption: null
            });

            $scope.$watch('SelectedRoleId', function (newrole) {
                LoadRole({ roleId: newrole });
            });
        }

        function Init(model) {
            //Get Feature 'EmployeeSelfService'
            lookupService.GetEmployerFeatures().then(function (response) {
                if (response.EmployeeSelfService) {                   
                    $scope.AllRoles = model.AllRoles;                   
                }
                else {
                    $scope.AllRoles = $filter('filter')(model.AllRoles, { 'Type': 0 });
                }
            });
            $scope.ContactTypes = lookupService.AdminContactTypes() || [];
            var addContactType = lookupService.EmployerContactTypes() || [];
            // Add these to the other contact types if any employer contact types are found.
            for (var i = 0; i < addContactType.length; i++) {
                var ct = addContactType[i];
                if (ct && ct.Code) {
                    var found = false;
                    for (var x = 0; x < $scope.ContactTypes.length; x++) {
                        var ot = $scope.ContactTypes[x];
                        if (ot && ot.Code == ct.Code) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        $scope.ContactTypes.push(ct);
                    }
                }
            }
            $scope.ContactTypes.sort(function (a, b) {
                if (a.Name < b.Name)
                    return -1;
                if (a.Name > b.Name)
                    return 1;
                return 0;
            });

            //$scope.AllPermissions = model.AllPermissions;
            //$scope.AllCategories = model.AllCategories;

            $scope.SelectedCategories = [];
            angular.forEach($scope.AllCategories, function (category, index) {
                $scope.SelectedCategories.push(false);
            });
            vm.DefaultEditRole = {
                IsNewRole: false,
                CustomerId: model.CustomerId,
                RoleId: '',
                Permissions: [],
                ContactTypes: [],
                RevokeAccessIfNoContactsOfTypes: false,
                Type: 0,
                CopyFromRoleId: ''
            };
            RevertEditRole();
            //InitPermissions();
            $scope.ContactType = '';
            $scope.IsESSDataVisibilityInPortalFeatureEnabled = model.IsESSDataVisibilityInPortalFeatureEnabled;
        }

        function permissionsFilter(permission) {
            return permission.RoleTypes && permission.RoleTypes.indexOf($scope.EditRole.Type) >= 0;
        }

        function ErrorHandler(response) {
            $scope.submitted = false;
            $scope.Error = response;
        }

        function DeleteRole() {
            administrationService.DeleteRole($scope.EditRole.RoleId).then(function (success) {
                if (success) {
                    $('#confirmationModal').modal('hide');
                    var indexOf = -1;
                    for (var i = 0; i < $scope.AllRoles.length; i++) {
                        if ($scope.AllRoles[i].Id == $scope.EditRole.RoleId) {
                            indexOf = i;
                            break;
                        }
                    }
                    if (indexOf > -1) {
                        $scope.AllRoles.splice(indexOf, 1);
                        RoleInit(false);
                    }
                }
            });
        }

        function UpdateRole(isValid) {
            if ($scope.frmEditRoles.$invalid) {
                return;
            }
            vm.submitted = false;

            if (vm.EditRole.RoleId) {

                $scope.EditRole.Permissions = _.flatten((_($scope.PermissionsViewModel).pluck("items")).value());

                administrationService.EditRole($scope.EditRole).then(function (data) {
                    if (data != null) {
                        if (data.Success) {
                            noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Role successfully updated: ' + data.RoleName });
                            $scope.AllRoles = data.AllRoles || $scope.AllRoles;
                            RoleInit(false);
                        } else {
                            noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.error ? data.error.Message : 'Role update failed' });
                        }
                    }
                });
            } else {
                $log.error('UpdateRole:', 'Should not be updating without a RoleId');
            }
        }

        function CreateRole() {
            if (vm.frmEditRoles.$invalid) {
                return;
            }
            vm.submitted = false;

            administrationService.AddRole($scope.EditRole).then(function (data) {
                if (data != null) {
                    if (data.Success) {
                        noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Role successfully created' });
                        $scope.AllRoles = data.AllRoles;
                        RoleInit(false);
                    } else {
                        noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Role creation failed' });
                    }
                }
            })
        }

        function ExpandCollapse(expand) {
            vm.PermissionsViewModel.forEach(function (p) {
                p.isVisible = expand;
            });
        }
        function CheckUncheck(group, check) {
            if (group != undefined && group != null)
            {
                angular.forEach(group.items, function (permission) {
                    if (permissionsFilter(permission)) {
                        permission.Apply = check;
                    }
                });
            };
        }
        function RoleInit(isNew) {           
            vm.SelectedRoleId = null;
            vm.MissingRole = false;
            vm.EditRole.IsNewRole = isNew;
            vm.EditRole.RoleId = '';
            vm.EditRole.RoleName = '';
            vm.EditRole.Type = 0;
            vm.EditRole.ContactTypes = [];
            vm.EditRole.RevokeAccessIfNoContactsOfTypes = false;
            //vmpe.InitPermissions();
            vm.ExpandCollapse(false);
            vm.EditRole.CopyFromRoleId = '';
            if (isNew) {
                LoadRole({ isNew: true });
            }
        }

        function FormatPermissions() {
            if (!(vm.EditRole && vm.EditRole.Permissions)) {
                vm.PermissionsViewModel = [];
                return;
            }
            vm.PermissionsViewModel = _.chain(vm.EditRole.Permissions)
                .sortBy('Name')
                .groupBy('Category')
                .map(function (value, idx) {
                    return {
                        categoryName: idx,
                        items: _.filter(value, permissionsFilter),
                        isVisible: false
                    };
                })
                .sortBy('categoryName')
                .filter(function (f) {
                    return f.items && f.items.length > 0;
                })
                .value();
        }

        function RevertEditRole() {
            angular.copy(vm.DefaultEditRole, vm.EditRole);
        }

        function LoadRole(options) {
            options = options || {};
            if (!(options && (options.isNew || options.roleId))) {
                RevertEditRole();
                return;
            }
            administrationService.GetRoleAndActivePermissions(options.roleId).then(function (data) {
                if (!data) {
                    return;
                }
                angular.extend(vm.EditRole, {
                    CustomerId: data.CustomerId,
                    RoleId: data.Id,
                    RoleName: data.Name,
                    ContactTypes: data.ContactTypes || [],
                    RevokeAccessIfNoContactsOfTypes: data.RevokeAccessIfNoContactsOfTypes || false,
                    Type: data.Type || 0,
                    Permissions: data.Permissions,
                    IsNewRole: options.isNew,                    
                    DefaultRole : data.DefaultRole,                    
                    AlwaysAssign: data.AlwaysAssign,
                });
                FormatPermissions();
            });
        }

        function InitPermissions() {
            $scope.EditRole.Permissions = [];
            angular.forEach($scope.AllPermissions, function (perm, index) {
                var permDetail = new Object();
                permDetail.Id = perm.Id;
                permDetail.Apply = false;
                $scope.EditRole.Permissions.push(permDetail);
            });
        }

        function AddContactType() {
            if ($scope.EditRole.ContactTypes.indexOf($scope.ContactType) < 0) {
                $scope.EditRole.ContactTypes.push($scope.ContactType);
            }
            $scope.ContactType = '';
        }

        function RemoveContactType(idx) {
            $scope.EditRole.ContactTypes.splice(idx, 1);
        }

        function CopyFromRole() {
            if ($scope.EditRole.CopyFromRoleId) {
                administrationService.GetRoleAndActivePermissions($scope.EditRole.CopyFromRoleId)
                    .then(function (data) {
                        angular.extend(vm.EditRole, {
                            ContactTypes: data.ContactTypes || [],
                            RevokeAccessIfNoContactsOfTypes: data.RevokeAccessIfNoContactsOfTypes || false,
                            Type: data.Type || 0,
                            Permissions: data.Permissions
                        });
                    })
                    .then(FormatPermissions);
            }
        }
    } // Controller

})();
