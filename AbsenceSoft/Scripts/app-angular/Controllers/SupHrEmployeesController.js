//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
angular
.module('App.Controllers')
.controller('SupHrEmployeesCtrl', ['$scope', '$timeout', 'supHrEmployeesListManager', function ($scope, $timeout, supHrEmployeesListManager) {

    // display calendar in Sun..Sat sequence
    $scope.dateOptions = {
        'starting-day': 0
    };
    $scope.IsTPA = false;

    $scope.init = function (callingPage) {
        var infiniteScrolling = callingPage == 'index';
        supHrEmployeesListManager.Init($scope, infiniteScrolling);
    }

    $scope.GetEmployees = function (doPaging) {
        supHrEmployeesListManager.Filter($scope, doPaging, $scope.ErrorHandler);
    };
    $scope.UpdateEmployeeSort = function (sortBy) {
        supHrEmployeesListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
    };
    $scope.LoadMoreEmployees = function () {
        supHrEmployeesListManager.LoadMore($scope, $scope.ErrorHandler);
    };

}]);