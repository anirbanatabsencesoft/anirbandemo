//TODO: complete these TODO's when modifying this file
//TODO: refactor to syntax as described in John Papa's style guide
//TODO: encapsulate in IIFE
//TODO: refactor to one controller per file!
//TODO: enable strict mode and fix any violations
//#region CommunicationIndexCtrl
angular
.module('App.Controllers')
.controller('CommunicationIndexCtrl', ['$scope', '$http', 'lookupService', 'communicationListManager', 'communicationService',
    function ($scope, $http, lookupService, communicationListManager, communicationService) {

        // display calendar in Sun..Sat sequence
        $scope.dateOptions = {
            'starting-day': 0
        };
        $scope.IsDeleteDisabled = true;

        $scope.init = function (caseId, employeeId, employerId) {
            $scope.CaseId = caseId;
            $scope.EmployeeId = employeeId;
            $scope.EmployerId = employerId;
            //$scope.GetCommunicationList();

            lookupService.GetPacketTypes(employerId).then(function (response) {
                $scope.PacketTypes = response;               
            });

            communicationListManager.Init($scope, true);

            if (true) {
                //HACK: Used jQuery for infinite scrolling because for Angular it requires to be applied to complete html document, which is not part of any of our views
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                        communicationListManager.LoadMore($scope, $scope.ErrorHandler);
                    }
                });
            }

            //To do filter due date watch
            $scope.$watch('CommunicationsFilter.SentDate', function () {
                $scope.GetCommunications(false);
            });
            $scope.$watch('CommunicationsFilter.ModifiedDate', function () {
                $scope.GetCommunications(false);
            });
        };

        $scope.GetCommunications = function (doPaging) {
            communicationListManager.List($scope, doPaging, $scope.ErrorHandler);
        };

        $scope.UpdateCommunicationSort = function (sortBy) {
            communicationListManager.UpdateSort($scope, sortBy, $scope.ErrorHandler);
        };

        $scope.GetCommunicationList = function () {
            $http.get('/Cases/Communications/GetCommunicationList', {
                params: { CaseId: $scope.CaseId }
            }).then(function (distribution) {
                $scope.communications = distribution.data;
            });
        };

        $scope.GetRecipient = function (recipient, index) {
            var recipientVal = "";
            if (index > 0) {
                recipientVal = ", ";
            }
            if ((recipient.FirstName != null && recipient.FirstName != "") || (recipient.LastName != null && recipient.LastName != "")) {
                recipientVal = recipientVal + recipient.FirstName + " " + recipient.LastName;
            }
            else if (recipient.CellPhone != null && recipient.CellPhone != "") {
                recipientVal = recipientVal + recipient.CellPhone;
            }
            else if (recipient.Fax != null && recipient.Fax != "") {
                recipientVal = recipientVal + recipient.Fax;
            }
            else if (recipient.Email != null && recipient.Email != "") {
                recipientVal = recipientVal + recipient.Email;
            }
            else {
                recipientVal = "";
            }
            return recipientVal;            
        }

        $scope.GetViewCommunicationUrl = function (communication) {
            var url;
            if (!communication.IsDraft) {
                url = "/Communications/" + communication.Id + "/View/" + $scope.CaseId;
            } else {
                if(communication.ToDoItemId){
                    url = "/Todos/" + communication.ToDoItemId + "/Communications/Create/";
                } else {
                    url = "/Cases/" + communication.CaseId + "/Communications/Create/";
                    if(communication.Template)
                        url += communication.Template + "/";
                }
                url += "?draftId=" + communication.Id;
            }
            window.location.href = url;
        }

        $scope.DeleteSelectedDrafts = function () {
            var draftsToDelete = [];
            angular.forEach($scope.Communications, function (communication, i) {
                if (communication.Delete) {
                    communication.CommunicationId = communication.Id;
                    draftsToDelete.push(communication);
                }
            })
            if (draftsToDelete.length < 1)
                return;

            communicationService.DeleteDrafts(draftsToDelete).$promise.then(function () {
                $scope.GetCommunications(false);
                $scope.CheckDeleteStatus();
            });
        }

        $scope.CheckDeleteStatus = function () {
            
            var deleteShouldBeDisabled = true;
            angular.forEach($scope.Communications, function (communication, i) {
                if (communication.Delete && deleteShouldBeDisabled) {
                    deleteShouldBeDisabled = false;
                }
            });

            $scope.IsDeleteDisabled = deleteShouldBeDisabled;
        }

        $scope.GetViewCaseUrl = function () {
            window.location.href = "/Cases/" + $scope.CaseId + "/View";
        };

    }]);


