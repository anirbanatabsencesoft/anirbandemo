﻿angular
.module('App', [
  // NOTE: Ideally, dependencies should be declared on each module as appropriate
  // as apposed to attaching having the root module depend on everything
  // However, EmployeeCtrl doesn't appear to be in a module
  // 3rd party
  , 'ngResource'
  , 'ui.bootstrap'
  , 'ngSanitize'
  , 'ngRoute'
  , 'angular.filter'
  //
  // App
  , 'App.Directives'
  , 'App.Filters'
  , 'App.Controllers'
  //, 'App.Reports.AbsenceReports'
  , 'App.Services'
  , 'App.Admin.EmployerHoliday'
  , 'App.Case.Wizard'
])

.run(['$http', '$rootScope', function ($http, $rootScope) {
    $('[data-toggle="popover"]').popover({
        container: 'body'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.tooltip-link').tooltip();

    //add token for CSRF by default
    $http.defaults.headers.common['X-XSRF-Token'] = angular.element('input[name="__RequestVerificationToken"]').attr('value');
}])

.config(['$provide', '$routeProvider', '$rootScopeProvider', '$httpProvider', function ($provide, $routeProvider, $rootScopeProvider, $httpProvider) {
    ////this method skip error of $digest and $watch Error: $digest reaches maximum iteration
    $rootScopeProvider.digestTtl(25);

    var profile = angular.copy(window.permissionsList);
    if (profile != undefined) {
        $provide.constant('userPermissions', profile);
    } else {
        $provide.constant('userPermissions', '');
    }

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpResponseInterceptor');
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    $routeProvider.when('/customer', {
        templateUrl: 'customer.html',
        controller: 'SignUpCtrl',
    });

    $routeProvider.when('/account', {
        templateUrl: 'account.html',
        controller: 'SignUpCtrl',
    });

    $routeProvider.when('/employee', {
        templateUrl: 'employee.html',
        controller: 'SignUpCtrl',
    });

    $routeProvider.when('/employee-info', {
        templateUrl: '/scripts/app-angular/dashboard/employee-dashboard-myinfo.html',
        controller: 'employeeMyInfoCtrl'
    }).when('/employee-cases', {
        templateUrl: '/scripts/app-angular/dashboard/employee-dashboard-cases.html',
        controller: 'EmployeeCasesIndexCtrl'
    }).when('/employee-case-create/:eId', {
        templateUrl: '/scripts/app-angular/dashboard/employee-case-create.html',
        controller: 'caseWizardCtrl'
    }).when('/employee-team', {
        templateUrl: '/scripts/app-angular/dashboard/suphr-dashboard-employees.html',
        controller: 'EmployeeIndexCtrl'
    }).when('/employee-team-cases', {
        templateUrl: '/scripts/app-angular/dashboard/suphr-dashboard-cases.html',
        controller: 'CaseIndexCtrl'
    }).when('/employee-todos', {
        templateUrl: '/scripts/app-angular/dashboard/suphr-dashboard-todos.html',
        controller: 'ToDoCtrl'
    }).when('/employee-case-create', {
        templateUrl: '/scripts/app-angular/dashboard/employee-case-create.html',
        controller: 'caseWizardCtrl'
    }).when('/employee-new-accomm-create', {
        templateUrl: '/scripts/app-angular/dashboard/case-wizard-layout.html',
        controller: 'caseWizardCtrl'
    }).when('/employee-new-accomm-create/:eId/:isInquiry', {
        templateUrl: '/scripts/app-angular/case/wizard/case-wizard-layout.html',
        controller: 'caseWizardCtrl'
    }).otherwise({
        templateUrl: '/scripts/app-angular/dashboard/employee-dashboard-myinfo.html',
        controller: 'employeeMyInfoCtrl'
    });
}])

// Global error handling
.factory('httpResponseInterceptor', ['$q', '$rootScope', '$location', '$injector',
    function ($q, $rootScope, $location, $injector) {
        var $http;
        return {
            request: function (config) {
                if (config != null) {
                    if (config.url != "/Administration/GetUserIsAuthenticatedOrNot" &&
                        config.url != "/Administration/GetFormsAuthenticationTimeOut" &&
                        config.url != "/Administration/HandleCookieExpiration" &&
                        config.url.indexOf("DoExport") == -1) {
                    }
                }

                if ($rootScope.UserImage == null || $rootScope.UserImage == undefined || $rootScope.UserImage == "")
                    $rootScope.UserImage = "/Content/Images/user.png";
                return config || $q.when(config);
            },
            requestError: function (rejection) {
                return $q.reject(rejection);
            },
            response: function (response) {
                $http = $http || $injector.get('$http')
                if ($http.pendingRequests.length < 1) {
                }

                // do something on success
                return response || $q.when(response);
            },
            responseError: function (response) {
                // Show user friendly error message
                // console.log(response);
                if (response.status <= 0) {
                    // returns status 0 when either ajax timeout or abort - so not displaying any message here
                }
                else if (response.status == 400) {
                    var msg = '';
                    if (response.data != null) {
                        if (response.data.error != null) {
                            if (response.data.error.Message != null && response.data.error.Message != '') {
                                //msg = "Error: " + response.data.error.Message + "<br/><br/>Error occurs when accessing resource <br/>" + response.config.url;
                                msg = "Error: " + response.data.error.Message;
                            }
                        }

                        if (msg == '')
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong (code:' + response.status + ') when trying access resource <br/><br/>' + response.config.url + '.<br/><br/>We\'ve logged this, but it might work if you try it again.' });
                        else
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: msg });
                    }
                    else {
                        var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong (code:' + response.status + ') when trying access resource <br/><br/>' + response.config.url + '.<br/><br/>We\'ve logged this, but it might work if you try it again.' });
                    }

                }
                else if (response.status == 401) {
                    var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Unauthorized - Authentication required. Please sign-in to access resource<br/>' + response.config.url });
                }
                else if (response.status == 403) {
                    var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Forbidden - You don\'t have rights to access the resource<br/>' + response.config.url });
                }
                else if (response.status == 404) {
                    var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Page Not Found.<br/><br/>The resource <br/>' + response.config.url + '<br/>you are looking for has been removed, had its name changed, or is temporarily unavailable.' });
                }
                else if (response.status == 500) {
                    //var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Internal server error while trying access resource <br/><br/>' + response.config.url + '<br/><br/>Please try again.' });

                    var msg = '';
                    if (response.data != null && response.data != undefined) {
                        if (response.data.error != null && response.data.error != undefined) {
                            if (response.data.error.Message != null && response.data.error.Message != '') {
                                //msg = "Error: " + response.data.error.Message + "<br/><br/>Error occurs when accessing resource <br/>" + response.config.url;
                                msg = "Error: " + response.data.error.Message;
                            }
                        } else if (response.data.message) {
                            msg = "Error: " + response.data.message
                        }

                        if (msg == '')
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong. Please try again or contact manager for assistance.' });
                        else
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: msg });
                    }
                    else {
                        var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong. Please try again or contact manager for assistance.' });
                    }

                }
                else if (response.status == 600) {
                    var msg = '';
                    if (response.data != null) {
                        if (response.data.error != null) {
                            if (response.data.error.Message != null && response.data.error.Message != '') {
                                //msg = "Error: " + response.data.error.Message + "<br/><br/>Error occurs when accessing resource <br/>" + response.config.url;
                                msg = "Error: " + response.data.error.Message;
                            }
                        }

                        if (msg == '')
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong (code:' + response.status + ') when trying access resource <br/><br/>' + response.config.url + '.<br/><br/>We\'ve logged this, but it might work if you try it again.' });
                        else
                            var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: msg });
                    }
                    else {
                        var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong (code:' + response.status + ') when trying access resource <br/><br/>' + response.config.url + '.<br/><br/>We\'ve logged this, but it might work if you try it again.' });
                    }

                }
                else {
                    //var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Opps! Something went wrong (code:' + response.status + ') when trying access resource <br/><br/>' + response.config.url + '.<br/><br/>We\'ve logged this, but it might work if you try it again.' });
                    var n = noty({ timeout: 20000, layout: 'topRight', type: 'error', text: 'Error: Oops! Something went wrong (code:' + response.status + ') when trying access resource. We\'ve logged this, but it might work if you try it again.' });
                }

                return $q.reject(response);
            }
        }
    }])

//.constant('CustomFieldType', {
//    Text: 1,
//    Number: 2,
//    Flag: 4,
//    Date: 8
//})
;