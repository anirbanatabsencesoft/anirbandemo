﻿(function (AbsenceSoft, $) {
    AbsenceSoft.DemandTypes = AbsenceSoft.DemandTypes || {};
    AbsenceSoft.DemandTypes.Init = function (container, firstLoad) {
        $('.demand-type', container).change(function () {
            var demandType = $(this).val();
            if (demandType === 'Value') {
                $('.value-editor').removeClass('hide');
                $('#item-container').html('');
            } else {
                $('.value-editor').addClass('hide');
            }
        });


        if (!firstLoad) {
            AbsenceSoft.PageStart(container);
        }
        
    }

    $(document).ready(function () {
        AbsenceSoft.DemandTypes.Init('#editDemandTypeForm', true);
    })

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);