﻿(function (AbsenceSoft, $) {
    //
    //  Work Restrictions
    var gettingWorkRestrictionForEdit = false;
    var getEditWorkRestrictionForm = function (e) {
        if (gettingWorkRestrictionForEdit) {
            return (e.preventDefault || function () { return false; })();
        }
        cancelWorkRestriction();
        $.ajax({
            url: $(this).attr('href'),
            success: displayEditWorkRestrictionFormClosure($(this)),
            complete: function () {
                gettingWorkRestrictionForEdit = false;
            }
        });

        return false;
    };

    var displayEditWorkRestrictionFormClosure = function (that) {
        return function (html) {
            $('#addEditWorkRestriction').html(html);
            if (that.hasClass('edit-work-restriction')) {
                var form = $('#addEditWorkRestriction').remove();
                that.parents('.wr-container').after(form);
            }
            initWorkRestrictionsForm();
        };
    }

    var initWorkRestrictionsForm = function () {
        initialSelect2();
        $('#workRestrictionDemandId').change(getDemandTypes);
        $('#cancelWorkRestriction').click(cancelWorkRestriction);
        $('#editWorkRestrictionForm').submit(function () { $(this).find(':input[type=submit]').prop('disabled', true); });
        AbsenceSoft.PageStart('#addEditWorkRestriction');
    };
    
    var getDemandTypes = function (e) {
        var demandId = $(e).select2('val')[0]['val']; //$(this).val();
        $('#demand-values').html('');
        if (demandId) {
            $.ajax({
                url: '/Demand/' + demandId + '/Values',
                success: displayWorkRestrictionValues
            })
        }
    };
    var displayWorkRestrictionValues = function (html) {
        $('#demand-values').html(html);
    };
    var cancelWorkRestriction = function () {
        var html = $('#addEditWorkRestriction').html('').remove();
        $('#workRestrictionsDisplay').prepend(html);
        return false;
    };


    //
    // Employee Necessities
    var gettingEmployeeNecessityForEdit = false;
    var getEditEmployeeNecessityForm = function () {
        if (gettingEmployeeNecessityForEdit) {
            return (e.preventDefault || function () { return false; })();
        }
        $.ajax({
            url: $(this).attr('href'),
            success: displayEditEmployeeNecessityForm,
            complete: function () {
                gettingEmployeeNecessityForEdit = false;
            }
        });

        return false;
    };
    var displayEditEmployeeNecessityForm = function (html) {
        $('#addEditEmployeeNecessity').html(html);
        initEmployeeNecessityForm();
    };
    var initEmployeeNecessityForm = function () {
        $('#cancelEmployeeNecessity').click(cancelEmployeeNecessity);
        AbsenceSoft.PageStart('#addEditEmployeeNecessity');
    };
    var cancelEmployeeNecessity = function () {
        $('#addEditEmployeeNecessity').html('');
        return false;
    };

    var initialSelect2 = function () {
        $('#workRestrictionDemandId').select2();
    };

    //
    // Binding
    $(document).ready(function () {
        if ($('#addWorkRestriction').length == 0 && $('#addEmployeeNecessity').length == 0)
            return;
                
        $('#addWorkRestriction').click(getEditWorkRestrictionForm);
        $('.edit-work-restriction').click(getEditWorkRestrictionForm);
        $('#cancelWorkRestriction').click(cancelWorkRestriction);
        
        $('#addEmployeeNecessity').click(getEditEmployeeNecessityForm);
        $('.edit-employee-necessity').click(getEditEmployeeNecessityForm);
        $('#cancelEmployeeNecessity').click(cancelEmployeeNecessity);

    });

    AbsenceSoft.WorkRestrictions = AbsenceSoft.WorkRestriction || {};
    AbsenceSoft.WorkRestrictions.Init = initWorkRestrictionsForm;
    AbsenceSoft.WorkRestrictions.InitNecessities = initEmployeeNecessityForm;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);