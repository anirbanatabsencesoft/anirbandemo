﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditNecessity = AbsenceSoft.EditNecessity || {};

    var editNecessityStart = function () {
        $('#editNecessityForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        AbsenceSoft.PageStart('#editNecessityForm');
    }

    AbsenceSoft.EditNecessity.Start = editNecessityStart;

    $(document).ready(editNecessityStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);