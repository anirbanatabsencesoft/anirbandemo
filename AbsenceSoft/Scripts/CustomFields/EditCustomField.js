﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCustomField = AbsenceSoft.EditCustomField || {};

    var valueTypeChanged = function () {
        var selectedValueType = $('#valueType').val();
        if (selectedValueType == "2") {
            $('#selectListValues').removeClass('hide');
        }else{
            $('#selectListValues').addClass('hide');
        }
    }

    var targetsCaseChanged = function () {
        var checked = $('#targetsCase').is(':checked');
        if (checked) {
            $('#isCollectedAtIntake').removeClass('hide');
        } else {
            $('#isCollectedAtIntake').addClass('hide');
        }
    }

    var focusInNewValue = function () {
        /// Hack, let select2 finish unselecting
        /// Then set focus in newValue text box
        setTimeout(function () {
            $('#newValue').focus();
        }, 50);
    }
    
    var buildSelectList = function () {
        $('#selectListDropdown').select2({
            allowClear: true,
            minimumResultsForSearch: Infinity,
            multiple: true,
            tags: true,
            tokenSeparators: ["~", " "],
            separator: "~",
            createSearchChoice: function (term) {
                var trimmedTerm = $.trim(term)
                return {
                    id: trimmedTerm,
                    text: trimmedTerm
                }
            }
        }).change(focusInNewValue);
    }

    var addValueToList = function (e) {
        if (e.keyCode !== 13)
            return;

        /// When we add a new value to the select list, it will set the value to that value, and only that value
        /// so we store a reference and then tell it to set the value to all the values
        var newValue = $(this).val().trim();
        var val = $('#selectListDropdown').select2('val');
        $('#selectListDropdown').select2('data', { id: newValue, text: newValue });
        val.push(newValue);
        $('#selectListDropdown').select2('val', val);
        
        $(this).val('');
        e.preventDefault();
        return false;
    }

    var preventSubmit = function (e) {
        if (e.keyCode !== 13)
            return;

        e.preventDefault();
        return false;
    }

    var editCustomFieldStart = function () {
        if ($('#editCustomFieldForm').length < 1)
            return;

        buildSelectList();
        valueTypeChanged();

        $('#editCustomFieldForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
           .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#valueType').change(valueTypeChanged);
        $('#newValue').keydown(preventSubmit);
        $('#newValue').keyup(addValueToList);
        $('#newValue').blur(addValueToList);
        AbsenceSoft.PageStart('#editCustomFieldForm');
    }

    AbsenceSoft.EditCustomField.Start = editCustomFieldStart;

    $(document).ready(editCustomFieldStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);