﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {

        AbsenceSoft.TimeTracker = AbsenceSoft.TimeTracker || {};

        AbsenceSoft.TimeTracker.OnChangeTimeTrackerToggle = function (e) {
            var target = e.id;
            var chkhours = $('#chkHours');
            var chkdefaults = $('#chkDefaults');
            var showTimeTrackerInHours = $('#showTimeTrackerInHours');
            var showParenthesesText = $('#showParenthesesText');
            var chkPTpolicydefault = $('#chkPTpolicydefault');
            var chkPThours = $('#chkPThours');
            if (target == 'showHours') {
                $('#showHours').button('toggle');
                showTimeTrackerInHours.val(true);
                showParenthesesText.val(false);
                var showDefaultInParenthesesChecked = $('#includepolicydefaults').is(':checked');

                if (showDefaultInParenthesesChecked) {
                    showTimeTrackerInHours.val(true);
                    showParenthesesText.val(true);
                }
                $(".showinhours").removeClass("hidden");
                $(".showdefaults").addClass("hidden");
                if (chkPTpolicydefault.val() == 'true')
                    $("#includepolicydefaults").attr("checked", "checked");
                else
                    $("#includepolicydefaults").removeAttr('checked');
                chkhours.hide();
                chkdefaults.show();
            }
            else {
                $('#showPolicydefaults').button('toggle');
                showTimeTrackerInHours.val(false);
                showParenthesesText.val(true);
                var showHoursInParenthesesChecked = $('#includehours').is(':checked');

                if (showHoursInParenthesesChecked) {
                    showTimeTrackerInHours.val(false);
                    showParenthesesText.val(false);
                }
                $(".showdefaults").removeClass("hidden");
                $(".showinhours").addClass("hidden");
                if (chkPThours.val() == 'true')
                    $("#includehours").attr("checked", "checked");
                else
                    $("#includehours").removeAttr('checked');
                chkdefaults.hide();
                chkhours.show();
            }
        };

        AbsenceSoft.TimeTracker.OnClickParenthesesCheck = function (e) {
            var chkPTpolicydefault = $('#chkPTpolicydefault');
            var chkPThours = $('#chkPThours');
            var id = '#' + e.id;
            var isChecked = $(id).is(':checked');
            if (isChecked) {
                if (e.id == "includehours") {
                    $(".showptextdefault").removeClass("hidden");
                    chkPThours.val(true);
                }
                else {
                    $(".showptexthours").removeClass("hidden");
                    chkPTpolicydefault.val(true);
                }
            }
            else {
                if (e.id == "includehours") {
                    $(".showptextdefault").addClass("hidden");
                    chkPThours.val(false);
                }
                else {
                    $(".showptexthours").addClass("hidden");
                    chkPTpolicydefault.val(false);
                }
            }
        };

        $('body').on('click', function (e) {
            $('.timetracker-popover').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(".popovericon").removeClass("popovericonclick");
                    $(this).popover('hide');
                }
            });
        });
        
        $('.timetracker-popover').on('shown.bs.popover', function () {
            $(".popovericon").addClass("popovericonclick");
            var showTimeTrackerInHours = $('#showTimeTrackerInHours').val();
            var showParenthesesText = $('#showParenthesesText');

            if (showTimeTrackerInHours == 'true') {
                $('#showHours').click();
            }
            else if (showTimeTrackerInHours == 'false') {
                showParenthesesText.val(true);
                $('#showPolicydefaults').click();
            }
        });

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);