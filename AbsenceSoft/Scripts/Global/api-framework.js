﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {

        //defining the object
        AbsenceSoft.HttpUtils = AbsenceSoft.HttpUtils || { };

        AbsenceSoft.HttpUtils.logError = true;
        AbsenceSoft.HttpUtils.version = "v1";

        //get the token and on success call the API
        AbsenceSoft.HttpUtils.GetJsonP = function (component, endpoint, callback) {
            $.ajax({
                url: "/token/get", success: function (result) {
                    $.ajax({
                        url: "/host/" + component, success: function (host) {
                            var url = host + "/" + AbsenceSoft.HttpUtils.version + "/" + endpoint;
                            AbsenceSoft.HttpUtils.CallJsonP(url, callback, result);
                        }
                    });
                }
            });
        };

        //Call the API using JSONP
        AbsenceSoft.HttpUtils.CallJsonP = function (url, callback, token) {
            $.ajax({
                url: url,
                dataType: "jsonp",
                data: { "token": token },
                type: AbsenceSoft.HttpUtils.HttpVerbs.Get,
                contentType: 'text/javascript',
                headers: { 'X-AUTH-TOKEN': token },
                beforeSend: function (xhr) { xhr.setRequestHeader('X-AUTH-TOKEN', token); },
                success: callback,
                error: AbsenceSoft.HttpUtils.ReportError,
                jsonp: "callback"
            });
        };

        //Get
        AbsenceSoft.HttpUtils.Get = function (component, endpoint, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: '', Method: AbsenceSoft.HttpUtils.HttpVerbs.Get };
            $.post("/service/execute", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

        //Post operations
        AbsenceSoft.HttpUtils.Post = function (component, endpoint, data, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: JSON.stringify(data), Method: AbsenceSoft.HttpUtils.HttpVerbs.Post };
            $.post("/service/execute", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

        //Delete operations
        AbsenceSoft.HttpUtils.Delete = function (component, endpoint, data, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: JSON.stringify(data), Method: AbsenceSoft.HttpUtils.HttpVerbs.Delete };
            $.post("/service/execute", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

        //Put operations
        AbsenceSoft.HttpUtils.Put = function (component, endpoint, data, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: JSON.stringify(data), Method: AbsenceSoft.HttpUtils.HttpVerbs.Put };
            $.post("/service/execute", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

        //Post operations
        AbsenceSoft.HttpUtils.Patch = function (component, endpoint, data, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: JSON.stringify(data), Method: AbsenceSoft.HttpUtils.HttpVerbs.Patch };
            $.post("/service/execute", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

        //Post operations
        AbsenceSoft.HttpUtils.File = function (component, endpoint, data, callback) {
            var postedData = { Component: component, EndPoint: endpoint, PostedData: JSON.stringify(data), Method: AbsenceSoft.HttpUtils.HttpVerbs.Get };
            $.post("/service/download", postedData).done(callback).fail(AbsenceSoft.HttpUtils.ReportError);
        };

      

        ///handle the error
        AbsenceSoft.HttpUtils.ReportError = function (obj, status, err) {
            console.log("An error occurred while accessing the API. The data: ", obj, status, err);
        };


        //enums to handle
        AbsenceSoft.HttpUtils.HttpVerbs = {
            Get: "GET",
            Post: "POST",
            Delete: "DELETE",
            Put: "PUT",
            Patch: "PATCH"
        };

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);