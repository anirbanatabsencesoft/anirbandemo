﻿(function (AbsenceSoft, $) {
    AbsenceSoft.PageStart = function (element) {
        var datePickerConfiguration = {
            orientation: "top auto",
            assumeNearbyYear: true,
            autoclose: true
        };

        
        $('.input-group.date.editor', element).datepicker(datePickerConfiguration);
        // Not all datepickers are in an input-group
        $('.date-editor', element).datepicker(datePickerConfiguration);
        $('[data-toggle="tooltip"]', element).tooltip();

        AbsenceSoft.Repeater.Init(element);     
        AbsenceSoft.HoursMinutes.Init(element);
        AbsenceSoft.ScheduleEditor.Init(element);
        AbsenceSoft.LogoutWarning.Init(element);
        AbsenceSoft.EyeToggle.Init(element);
        AbsenceSoft.List.Init(element);
        $('[data-toggle="tooltip"]', element).tooltip();

        $('#search-body form, #searchForm').submit(function () {
            var search = $.trim($(this).find(':input[type=text]').val());
            if (search === "" || search === undefined || search === null) {
                return false;
            }
        });
    }

    $.validator.setDefaults({
        ignore: ''
    });

    

    AbsenceSoft.GenerateGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    $(document).ready(function () {
        AbsenceSoft.PageStart(document);
        
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
