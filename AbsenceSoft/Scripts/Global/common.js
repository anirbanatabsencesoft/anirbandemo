﻿/**
 * queryString - Gets the current query string parameters as an object (HashTable).
 * @returns {Object} An Object/HashTable representing all of the query string parameters.
 */
function queryString() {
    var search = arguments.length > 0 ? arguments[0] : window.location.search;
    var params = search.substr(search.indexOf("?") + 1);
    var retVal = {};
    params = params.split("&");
    // split param and value into individual pieces
    for (var i = 0, p; p = params[i++];) {
        var temp = p.split("=");
        var key = decodeURIComponent(temp[0]),
            val = decodeURIComponent((temp.length > 1 ? temp[1] : ""));
        retVal[key] = val;
    }
    return retVal;
}

/**
 * stopEvent - Prevents event propagation across browser platforms, including FF, Mobile, IE, etc.
 * @param [e] {Event} The event to cancel propagation of. If not provided window.event will be used instead.
 * @returns {boolean} Always returns false.
 */
function stopEvent(e) {
    if (!e) var e = window.event;

    //e.cancelBubble is supported by IE -
    // this will kill the bubbling process.
    if (e.cancelBubble !== undefined) {
        e.cancelBubble = true;
    }

    e.returnValue = false;

    //e.stopPropagation works only in FireFox.
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();

    return false;
} // stopEvent

/**
 * htmlEncode - HTML Encode the string value using a jQuery hack.
 * @param value {string} The string that needs to be HTML encoded.
 * @returns {string} An HTML encoded version of the original string provided.
 */
function htmlEncode(value) {
    return $('<div/>').text(value).html();

} // htmlEncode

/**
 * htmlDecode - Decode the HTML string value as raw HTML/markup.
 * @param value {string} The HTML encoded markup to decode.
 * @returns {string} The HTML string that has been decoded from the original HTML encoded string passed in.
 */
function htmlDecode(value) {
    return $('<div/>').html(value).text();

} // htmlDecode

Date.prototype.formatMMDDYYYY = function () {
    return this.getMonth() +
    "/" + this.getDate() +
    "/" + this.getFullYear();
}
Date.prototype.addMonths = function (months) {
    return new Date(this.getFullYear(), this.getMonth() + months, this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds());
}
Date.prototype.firstOfMonth = function()
{
    return new Date(this.getFullYear(),
                                        this.getMonth(),
                                        1,
                                        this.getHours(),
                                        this.getMinutes(),
                                        this.getSeconds(),
                                        this.getMilliseconds());
}
Date.prototype.toFakeUTCDay = function () {
    var newDate = new Date(this.getTime());
    newDate.syncToUTC();
    newDate.setMilliseconds(0);
    newDate.setSeconds(0);
    newDate.setMinutes(0);
    newDate.setHours(0);
    return newDate;
}
Date.prototype.syncToUTC = function () {
    this.setMinutes(this.getMinutes() + this.getTimezoneOffset());
    return this;
}
Date.prototype.isSameDay = function (otherDate) {
    if (otherDate.getFullYear() == this.getFullYear()
        && otherDate.getMonth() == this.getMonth()
        && otherDate.getDate() == this.getDate())
    {
        return true;
    }
    return false;
}