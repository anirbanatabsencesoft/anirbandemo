﻿$(function () {
    $.ajaxSetup({
        headers: {
            "X-XSRF-Token": $('input[name="__RequestVerificationToken"]').val()
        }
    });
});