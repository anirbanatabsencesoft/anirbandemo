﻿(function (AbsenceSoft, $) {
    AbsenceSoft.Admin = AbsenceSoft.Admin || {};

    var hideDirtyFields = function () {
        $('.hidden-on-clean').addClass('hide').attr('disabled', 'disabled');
    }

    var showDirtyFields = function () {
        $('.hidden-on-clean').removeClass('hide').removeAttr('disabled');
    }

    $(document).ready(function () {
        $('.admin-undo-btn').click(function () {
            window.location.reload(true);
        });
    });

    AbsenceSoft.Admin.HideDirtyFields = hideDirtyFields;
    AbsenceSoft.Admin.ShowDirtyFields = showDirtyFields;

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);