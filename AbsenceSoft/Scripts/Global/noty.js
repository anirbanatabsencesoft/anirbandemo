﻿// Customize layout of the noty (specifically setting height, width and position)

(function ($) {

    $.noty.layouts.customCenterFixedWidth = {
        name: 'customCenterFixedWidth',
        options: { // overrides options

        },
        container: {
            object: '<ul id="noty_customBottomCenter_layout_container" />',
            selector: 'ul#noty_customBottomCenter_layout_container',
            style: function () {
                $(this).css({
                    bottom: 30,
                    left: 0,
                    position: 'fixed',
                    width: '700px',
                    height: 'auto',
                    margin: 0,
                    padding: 0,
                    listStyleType: 'none',
                    zIndex: 10000000,
                });

                $(this).css({
                    left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px',
                    //bottom: ($(window).height() - $(this).outerHeight(false)) / 2 + 'px'
                });
            }
        },
        parent: {
            object: '<li />',
            selector: 'li',
            css: {}
        },
        css: {
            display: 'none',
            width: '700px'
        },
        addClass: ''
    };

})(jQuery);