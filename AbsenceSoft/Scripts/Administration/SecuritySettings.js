﻿(function (AbsenceSoft, $) {
    AbsenceSoft.SecuritySettings = AbsenceSoft.SecuritySettings || {};

    var securitySettingsStart = function () {
        $('#editSecuritySettingsForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        AbsenceSoft.PageStart('#editSecuritySettingsForm');
    }

    AbsenceSoft.SecuritySettings.Start = securitySettingsStart;

    $(document).ready(securitySettingsStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);