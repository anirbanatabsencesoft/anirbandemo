﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EmployerIndex = AbsenceSoft.EmployerIndex || {};

    var removeFileUpload = function () {
        $('#uploadedLogo').addClass('hide');
        $('#employerLogoName').html('');
        $('#employerLogoUploadFile').val('');
        $('#employerEditForm').trigger('checkform.areYouSure');

    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        if (uploadedFile.type.indexOf('image') === -1) {
            removeFileUpload();
            return;
        }

        $('#uploadedLogo').removeClass('hide');
        $('#employerLogoName').html(uploadedFile.name);
    }

    var handleCountryChanged = function () {
        AbsenceSoft.CountryEditor.GetRegionEditorByCountry($(this).val(), $('.region-editor'));
    }

    var employerIndexStart = function () {
        if($('#employerEditForm').length == 0)
            return;

        $('#employerEditForm').areYouSure({
            silent:true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#employerLogoUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelEmployerLogoUpload').click(removeFileUpload);
        $('.country-editor').change(handleCountryChanged);

        AbsenceSoft.PageStart('#employerEditForm');
    }

    AbsenceSoft.EmployerIndex.Start = employerIndexStart;

    $(document).ready(employerIndexStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);