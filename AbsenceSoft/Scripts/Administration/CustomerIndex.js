﻿(function (AbsenceSoft, $) {
    AbsenceSoft.CustomerIndex = AbsenceSoft.CustomerIndex || {};

    var removeFileUpload = function () {
        $('#uploadedLogo').addClass('hide');
        $('#customerLogoName').html('');
        $('#customerLogoUploadFile').val('');
        $('#customerEditForm').trigger('checkform.areYouSure');
    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        if (uploadedFile.type.indexOf('image') === -1) {
            removeFileUpload();
            return;
        }
            

        $('#uploadedLogo').removeClass('hide');
        $('#customerLogoName').html(uploadedFile.name);
    }

    var handleCountryChanged = function () {
        AbsenceSoft.CountryEditor.GetRegionEditorByCountry($(this).val(), $('.region-editor'));
    }

    var customerIndexStart = function () {
        if ($('#customerEditForm').length == 0)
            return;

        $('#customerEditForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#customerLogoUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelCustomerLogoUpload').click(removeFileUpload);
        $('.country-editor').change(handleCountryChanged);

        AbsenceSoft.PageStart('#customerEditForm');
    }

    AbsenceSoft.CustomerIndex.Start = customerIndexStart;

    $(document).ready(customerIndexStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);