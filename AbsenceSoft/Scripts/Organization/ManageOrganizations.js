﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        if ($('#hdnOrgEmployerId').length == 0)
            return;        
        findOrganizations();
        $('#orgSearch').keyup(filterList);        
        //triggered when modal is about to be shown
        $('button[data-toggle=modal]').click(function () {
            resetFormValidation();            
            if (addMode) {
                setLookUps();
                $('#hdnOrgId').val('');
                $('#orgCodeText').val('');
                $('#orgNameText').val('');
                $('#orgDescription').val('');
                
                $('#orgSicText').val('');
                $('#orgNaicsText').val('');
                $('#orgAddress1Text').val('');
                $('#orgAddress2Text').val('');
                $('#orgCityText').val('');
                $('#orgStateText').val('');
                $('#orgPostalText').val('');
                $('#orgCountryText').val('');
             

                $('#organizationType option[value=""]').prop('selected', true);
                $('#orgCountryText option[value=""]').prop('selected', true);
                $('#parentOrganization option[value=""]').prop('selected', true);
                $('#orgTitle').html('Add New Organization');
                $('#btnSaveOrganization').html('Add Organization');
            }            
            else if (addChildOrgMode == false) {
                $('#orgTitle').html('Edit ' + $('#orgNameText').val());
                $('#btnSaveOrganization').html('Save Organization');
                if ($('#parentOrganization').val()=='') $('#parentOrganization').val('-1');
            }
            addMode = true;
            addChildOrgMode = false;
        });
        $('#btnSaveOrganization').click(function () {
            $("#frmOrgEdit").validate();
            if ($("#frmOrgEdit").valid()) {
                resetFormValidation();
                $.ajax({
                    async: true,
                    url: '/Organization/Save',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify({
                        EmployerId: $('#hdnOrgEmployerId').val(), Id: $('#hdnOrgId').val(),
                        Name: $('#orgNameText').val(), Code: $('#orgCodeText').val(), Description: $('#orgDescription').val(),
                        TypeCode: $('#organizationType').val(), ParentCode: $('#parentOrganization').val(),
                        SicCode: $('#orgSicText').val(), NaicsCode: $('#orgNaicsText').val(),
                        Address1: $('#orgAddress1Text').val(), Address2: $('#orgAddress2Text').val(),
                        City: $('#orgCityText').val(), State: $('#orgStateText').val(),
                        PostalCode: $('#orgPostalText').val(), Country: $('#orgCountryText').val()
                    }),
                    success: saveSuccess,
                    error: function (message) {
                        alert(JSON.stringify(message));
                    }
                })
            }
        });
        $('.country-editor').change(handleCountryChanged);
        
        $('#btnDeleteOrganization').click(function () {
            $.ajax({
                async: true,
                url: '/Organization/' + $('#hdnOrgEmployerId').val() + '/Delete/' + $('#hdnDeleteOrgId').val(),
                contentType: 'application/json',
                type: 'DELETE',                
                success: deleteSuccess,
                error: function (message) {
                    alert(JSON.stringify(message));
                }
            })
        });                
        $.validator.addMethod('orgCodeUnique',
        function (value, element) {
            var response = false;
            $.ajax({
                async:false,
                type: 'GET',
                url: '/Organization/' + $('#hdnOrgEmployerId').val() + '/IsOrganizationCodeExisting/' + $('#orgCodeText').val(),
                contentType: 'application/json',
                data:{orgId:$('#hdnOrgId').val()},
                success: function (msg) {
                    //If code exists, set response to true
                    response = (msg.Result == true) ? false : true;
                }
            });
            return response;
            },
                "Code is already existing"
        );
        $('#frmOrgEdit').validate({
            rules: {
                "orgCodeText": {
                    required: true,
                    orgCodeUnique: true
                },
                "orgNameText": {
                    required: true
                },
                "orgDescription": {
                    required: true
                },
                "organizationType": {
                    required: true
                },
                "parentOrganization": {
                    required: true
                },
                "orgNaicsText": {
                    minlength: 6,
                    maxlength: 6
                },
                "orgSicText": {
                    minlength: 4,
                    maxlength: 4
                }
              
            },
            messages: {
                "orgCodeText": {
                    required: "The Code field is required",
                    orgCodeUnique: "The Code must be unique"
                },
                "orgNameText": {
                    required: "The Name field is required"
                },
                "orgDescription": {
                    required: "The Description field is required"
                },
                "organizationType": {
                    required: "The Organization Type field is required"
                },
                "parentOrganization": {
                    required: "The Parent Organization Type field is required"
                },
                "orgNaicsText": {
                    minlength: "NAICS Code should be exactly 6 characters",
                    maxlength: "NAICS Code should be exactly 6 characters"
                },
                "orgSicText": {
                    minlength: "SIC Code should be exactly 4 characters",
                    maxlength: "SIC Code should be exactly 4 characters"
                }

            }
        });


    });
    var levels = function (level) {
        var indents = [];
        for (var i = 1; i < level; i++) {
            indents.push(i);            
        }
        return indents;
    }

    var handleCountryChanged = function () {
        AbsenceSoft.CountryEditor.GetRegionEditorByCountry($(this).val(), $('.region-editor'));
    }
    var expand = function (id) {        
        $("#" + id).removeClass('manage-org-hide-child');
    }
    var lastSearchTerm = '';
    var addMode = true;
    var addChildOrgMode = false;
    var filterList = function () {
        if (lastSearchTerm == $('#orgSearch').val()) return;
        lastSearchTerm = $('#orgSearch').val();
        findOrganizations();
    }
    var findOrganizations = function () {
        $.ajax({
            async: true,
            url: '/Organization/' + $('#hdnOrgEmployerId').val() + '/FindOrganizations/' + $('#orgSearch').val(),
            contentType: 'application/json',
            method: 'GET',
            success: displayOrgListData,
            error: function (message) {
                alert(JSON.stringify(message));
            }
        })
    }    
    
    var displayOrgListData = function (result) {
        renderOrgTree(result.EmployerOrganizations);    
    }
    var renderOrgTree = function (data)
    {
        if (data.length > 0) {
            $('#results').html($("#organizationTemplate").tmpl(data));
            bindTreeExpandCollapseEvent();
        }
        else {
            $('#results').html("<div class='pull-center'>No Organizations found.</div>");
        }
    }
    var bindTreeExpandCollapseEvent = function () {        
        $(".glyphicon-org-expandable").each(function () {
            $(this).click(function () {
                $(this).parent('div').children('div.manage-org-hide-child').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).siblings('.glyphicon-org-collapsable').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).addClass('manage-org-hide-child');                
                $(this).parent('.org-collapse-border').each(function () {
                    $(this).children('.save-border-style').val($(this).css('border-left'));
                    $(this).children('.manage-org-arrow-left-1').removeClass('org-level-one-seperator-collapse');
                    $(this).children('.manage-org-arrow-left-1').css('border-bottom-color', $(this).css('border-left-color'));
                    $(this).removeClass('org-collapse-border');                    
                    $(this).addClass('org-expand-border');                                        
                    $(this).css('border-left-color', $(this).css('border-top-color'));
                })
                $(this).siblings('.org-level-one-seperator').each(function () {
                    $(this).removeClass('org-level-one-seperator-collapse');
                });
            })
        });        
        $(".glyphicon-org-collapsable").each(function () {
            $(this).click(function () {
                $(this).parent('div').children('div').each(function () {
                    $(this).addClass('manage-org-hide-child');
                });
                $(this).siblings('.glyphicon-org-expandable').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).addClass('manage-org-hide-child');
                $(this).parent('.org-expand-border').each(function () {
                    $(this).removeClass('org-expand-border');
                    $(this).addClass('org-collapse-border');
                    $(this).css('border-left',$(this).children('.save-border-style').val());
                });
                $(this).siblings('.org-level-one-seperator').each(function () {
                    $(this).addClass('org-level-one-seperator-collapse');
                });
            })
        });
        $(".glyphicon-plus-sign").each(function () {
            $(this).click(function () {
                $(this).parent('div').children('div.manage-org-hide-child').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).siblings('.glyphicon-minus-sign').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).addClass('manage-org-hide-child');
            })
        });
        $(".glyphicon-minus-sign").each(function () {
            $(this).click(function () {
                $(this).parent('div').children('div').each(function () {
                    $(this).addClass('manage-org-hide-child');
                });
                $(this).siblings('.glyphicon-plus-sign').each(function () {
                    $(this).removeClass('manage-org-hide-child');
                });
                $(this).addClass('manage-org-hide-child');
            })
        });
        $(".glyphicon-org-ring.glyphicon-org-edit").each(function () {
            $(this).click(function () {
                setLookUps();
                $(this).parent().siblings('.org-id').each(function () {
                    $('#hdnOrgId').val($(this).val());
                });
                $(this).parent().siblings('.org-code').each(function () {
                    $('#orgCodeText').val($(this).val());
                });
                $(this).parent().siblings('.org-name').each(function () {
                    $('#orgNameText').val($(this).val());
                });
                $(this).parent().siblings('.org-type').each(function () {
                    $('#organizationType option[value="' + $(this).val() + '"]').prop('selected', true);
                });
                $(this).parent().siblings('.org-description').each(function () {
                    $('#orgDescription').val($(this).val());
                });
                $(this).parent().siblings('.org-parent-code').each(function () {
                    $('#parentOrganization option[value="' + $(this).val() + '"]').prop('selected', true);
                });

                $(this).parent().siblings('.org-sic').each(function () {
                    $('#orgSicText').val($(this).val());
                });
                $(this).parent().siblings('.org-naics').each(function () {
                    $('#orgNaicsText').val($(this).val());
                });
                $(this).parent().siblings('.org-address-address1').each(function () {
                    $('#orgAddress1Text').val($(this).val());
                });
                $(this).parent().siblings('.org-address-address2').each(function () {
                    $('#orgAddress2Text').val($(this).val());
                });
                $(this).parent().siblings('.org-address-city').each(function () {
                    $('#orgCityText').val($(this).val());
                });
                $(this).parent().siblings('.org-address-state').each(function () {
                    $('#orgStateText').val($(this).val());
                });
                $(this).parent().siblings('.org-address-postal').each(function () {
                    $('#orgPostalText').val($(this).val());
                });
                $(this).parent().siblings('.org-address-country').each(function () {
                    $('#orgCountryText option[value="' + $(this).val() + '"]').prop('selected', true);
                
                });
                addMode = false;
                $('#addNewButton').trigger("click");
            })
        });
        $(".glyphicon-org-ring.glyphicon-org-add").each(function () {
            $(this).click(function () {
                setLookUps();
                $('#hdnOrgId').val('');
                $('#orgCodeText').val('');
                $('#orgNameText').val('');
                $('#orgDescription').val('');

                $('#orgSicText').val('');
                $('#orgNaicsText').val('');
                $('#orgAddress1Text').val('');
                $('#orgAddress2Text').val('');
                $('#orgCityText').val('');
                $('#orgStateText').val('');
                $('#orgPostalText').val('');
                $('#orgCountryText').val('');
                $('#organizationType option[value=""]').prop('selected', true);
                $('#orgCountryText option[value=""]').prop('selected', true);
             
                $(this).parent().siblings('.org-code').each(function () {
                    $('#parentOrganization option[value="' + $(this).val() + '"]').prop('selected', true);
                });
                $('#orgTitle').html('Add New Organization');
                $('#btnSaveOrganization').html('Add Organization');                
                addMode = false;
                addChildOrgMode = true;
                $('#addNewButton').trigger("click");                
            })
        });
        $(".glyphicon-org-ring.glyphicon-org-delete").each(function () {
            $(this).click(function () {
                $(this).parent().siblings('.org-id').each(function () {
                    $('#hdnDeleteOrgId').val($(this).val());
                });
                $(this).parent().siblings('.org-name').each(function () {
                    $('#orgDeleteTitle').html($(this).val());
                    $('#orgDeleteName').html($(this).val());
                    $('#orgDeleteName1').html($(this).val());
                });                
                $('#orgDeleteModalDialog').modal('toggle');
            })
        });
        $(".org-name-level-one").each(function () {
            $(this).click(function () {
                if ($(this).parents(':eq(1)').hasClass('org-collapse-border')) {
                    $(this).parent().siblings('.glyphicon-org-expandable').each(function () {
                        $(this).trigger('click');
                    });
                }
                else {
                    $(this).parent().siblings('.glyphicon-org-collapsable').each(function () {
                        $(this).trigger('click');
                    });
                }
            });
        });
        $(".org-name-level-two").each(function () {
            $(this).click(function () {
                if ($(this).parent().siblings('.glyphicon-minus-sign').hasClass('manage-org-hide-child')) {
                    $(this).parent().siblings('.glyphicon-plus-sign').each(function () {
                        $(this).trigger('click');
                    });
                }
                else {
                    $(this).parent().siblings('.glyphicon-minus-sign').each(function () {
                        $(this).trigger('click');
                    });
                }
            });
        });
        $(".org-name-level-normal").each(function () {
            $(this).click(function () {
                if ($(this).parent().siblings('.glyphicon-minus-sign').hasClass('manage-org-hide-child')) {
                    $(this).parent().siblings('.glyphicon-plus-sign').each(function () {
                        $(this).trigger('click');
                    });
                }
                else {
                    $(this).parent().siblings('.glyphicon-minus-sign').each(function () {
                        $(this).trigger('click');
                    });
                }
            });
        });
    }
    var buildParentOrganizationLookUp = function (organizations, lookUp) {        
        $.each(organizations, function (index, value) {
            lookUp.push({ 'Code': value.Code, 'Name': value.Name });            
        });
    }
    var sortResults = function (data ,prop, asc) {
        data = data.sort(function (a, b) {
            if (asc) {
                return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
            } else {
                return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
            }
        });        
    }
    var saveSuccess = function () {
        $('#orgModalDialog').modal('toggle');
        findOrganizations();
        resetFormValidation();
        if ($('#parentOrganization').val() == "-1") {
            parentOrgLookUp.push({ 'Code': $('#orgCodeText').val(), 'Name': $('#orgNameText').val() });
        }
    }
    var deleteSuccess = function () {
        $('#orgDeleteModalDialog').modal('toggle');
        findOrganizations();
    }
    var resetFormValidation = function () {
        $("#frmOrgEdit").data('validator').resetForm();
        $('#frmOrgEdit').find('input, label, select, textarea').removeClass('error');
    }
    var setLookUps = function () {
        $.ajax({
            async: false,
            url: '/Lookups/ListCountries',
            contentType: 'application/json',
            method: 'GET',
            success: setCountryLookUp,
            error: function (message) {
                alert(JSON.stringify(message));
            }
        });
        $.ajax({
            async: false,
            url: '/Lookups/' + $('#hdnOrgEmployerId').val() + '/GetEmployerOrganizationTypes',
            contentType: 'application/json',
            method: 'GET',
            success: setOrganizationTypeLookUp,
            error: function (message) {
                alert(JSON.stringify(message));
            }
        });
        $.ajax({
            async: false,
            url: '/Lookups/' + $('#hdnOrgEmployerId').val() + '/GetEmployerOrganizations',
            contentType: 'application/json',
            method: 'GET',
            success: setParentOrganizationLookUp,
            error: function (message) {
                alert(JSON.stringify(message));
            }
        });
    }
    var setOrganizationTypeLookUp = function (data) {
        data.splice(0, 0, { 'Code': '', 'Name': 'Choose One' });
        $('#organizationType').html($("#titleTemplate").tmpl(data));        
    }

    var setCountryLookUp = function (data) {
        data.splice(0, 0, { 'Code': '', 'Name': 'Choose One' });
        $('#orgCountryText').html($("#titleTemplate").tmpl(data));
    }
    
    var setParentOrganizationLookUp = function (data) {
        var parentOrgLookUp = [];
        buildParentOrganizationLookUp(data, parentOrgLookUp);
        sortResults(parentOrgLookUp, 'Name', true);
        parentOrgLookUp.splice(0, 0, { 'Code': '-1', 'Name': 'No Parent' });
        parentOrgLookUp.splice(0, 0, { 'Code': '', 'Name': 'Choose One' });
        $('#parentOrganization').html($("#titleTemplate").tmpl(parentOrgLookUp));        
    }    
    AbsenceSoft.ManageOrganizations = AbsenceSoft.ManageOrganizations || {};
    AbsenceSoft.ManageOrganizations.Levels = levels;
    AbsenceSoft.ManageOrganizations.Expand = expand;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);