﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdHocReports = AbsenceSoft.AdHocReports || {};
    var getFieldFilter = function () {
        AbsenceSoft.AdHocReports.MarkDirty();
        var that = $(this);
        $.ajax({
            url: '/Reports/FilterField',
            data: {
                reportTypeId: $('#adHocReportTypeId').val(),
                fieldName: that.val()
            },
            success: function (html) {
                var container = that.parents('.absencesoft-repeating').find('.filter-container');
                container.html(html);
                AbsenceSoft.PageStart(container);
            }
        })
    }

    var enhanceFilterFieldSelection = function (e, element) {
        $('.report-fields-for-filtering', element).select2()
            .change(getFieldFilter);
    }

    var hideAddFilterText = function () {
        $('#addFiltersText').addClass('hide');
        $('#runFiltersText').removeClass('hide');
    }

    var filterRemoved = function () {
        AbsenceSoft.AdHocReports.MarkDirty();
        if ($('#adHocReportFilters').html().trim() == '') {
            $('#addFiltersText').removeClass('hide');
            $('#runFiltersText').addClass('hide');
        }
    }

    $(document).ready(function () {
        if ($('#addFilter').length < 1)
            return;

        enhanceFilterFieldSelection()
        $('#addFilter').click(hideAddFilterText);
        $(document).on('as.repeater.add', enhanceFilterFieldSelection)
        $(document).on('as.repeater.remove', filterRemoved);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);