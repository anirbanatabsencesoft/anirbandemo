﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdHocReports = AbsenceSoft.AdHocReports || {};

    var removeActiveClassOnCategories = function () {
        $('.adhoc-category-button').removeClass('active');
    }

    var resetActive = function () {
        removeActiveClassOnCategories();
        $('.original-active').addClass('active');
        $($(this).attr('href')).addClass('hide');
    }
    
    AbsenceSoft.AdHocReports.ShowCategorySubMenu = function () {
        removeActiveClassOnCategories();
        var link = $(this);
        $('.adhoc-report-type').addClass('hide');
        var elementToShow = link.attr('href');
        $(".adhoc-category-button[href='" + elementToShow + "']").addClass('active');
        $(elementToShow).removeClass('hide');
        return false;
    }

    $(document).ready(function () {
        $('.adhoc-category-button').click(AbsenceSoft.AdHocReports.ShowCategorySubMenu);
        $('.ad-close-menu a').click(resetActive);
        $('.ad-category-current').click(resetActive);
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);