﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        if ($('#grpReport').length == 0)
            return;
        if ($('#reportsNav') != undefined && $('#reportsNav') != null) {
            $('#reportsNav').click(function () {
                if ($('#hdnSelectReport') != undefined && $('#hdnSelectReport') != null && $('#hdnSelectReport').val() != 'home')
                    location.href = "reports";
            });
        }
        if (queryString(location.href).report != undefined && (queryString(location.href).report == AbsenceSoft.SelectReports.BIReport || queryString(location.href).report == AbsenceSoft.SelectReports.OpReport))
            AbsenceSoft.SelectReports.ReportListView(queryString(location.href).report, false);
        else
            AbsenceSoft.SelectReports.Begin();
    });

    AbsenceSoft.SelectReports = AbsenceSoft.SelectReports || {};
    AbsenceSoft.SelectReports.Begin = function () {
        $('#operationsReports').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.OpReport, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.OpReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.OpReport);
        });
        $('#biReports').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.BIReport, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.BIReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.BIReport);
        });
        $('#adhocReports').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.AHReport, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHReport);
        });
        //$('#adhocReports').click(function () {
        //    //AbsenceSoft.SelectReports.ReportDetailListView(AbsenceSoft.SelectReports.AHFullScreenReport, false);
        //    AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.AHFullScreenReport, false);
        //    if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHFullScreenReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHFullScreenReport);
        //});

    };

    AbsenceSoft.SelectReports.ReportHomeView = function () {
        $('#btnReportList').click(function () {
            $.ajax({
                url: '/reports/home',
                data: {},
                success: function (html) {
                    $('#grpReport').html(html);
                    $('#hdnSelectReport').val('home');
                    AbsenceSoft.SelectReports.Begin();
                    if (window.history.pushState) window.history.pushState({ reportState: 'home' }, "Reports", "?report=home");
                }
            });
        });

        /* call Report builder screen */
        $('#adhocEmplReports').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.AHEmplReport, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport);
        });

        /* Filter button criteria refresh  */
        $('#filterButton').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.AHEmplReport2, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        $('#filterAccounts').click(function () {
            AbsenceSoft.SelectReports.ReportRefreshView("filterAccounts", false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        $('#filterProjects').click(function () {
            AbsenceSoft.SelectReports.ReportRefreshView("filterProjects", false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        $('#filterText').click(function () {
            AbsenceSoft.SelectReports.ReportRefreshView("filterText", false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        $('#filterDates').click(function () {
            AbsenceSoft.SelectReports.ReportRefreshView("filterDates", false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        $('#adhocNavTreeMenu').click(function () {
            AbsenceSoft.SelectReports.ReportRefreshView("adhocNavTreeMenu", false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHEmplReport2 }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHEmplReport2);
        });

        /* return to landing page from Empl page */
        $('#btnReturnMainPage').click(function () {
            AbsenceSoft.SelectReports.ReportListView(AbsenceSoft.SelectReports.AHReport, false);
            if (window.history.pushState) window.history.pushState({ reportState: AbsenceSoft.SelectReports.AHReport }, "Reports", "?report=" + AbsenceSoft.SelectReports.AHReport);
        });

        $('#customerCityAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });
        $('#customerNameAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });
        $('#customerStateAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });
        $('#customerEmailAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });
        $('#customerPhoneAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });
        $('#customerAddressAI').click(function () {
            $(this).toggleClass('ad-menu-x');
        });

        // I put this in a try catch block, as it was continually erroring out.
        try
        {
            $("#customerfields").live("click", function () {

                alert("customerfields click detected.");

                //Get selected option of the HTML SELECT
                var selectedItem = $("#customerfields option:selected").last();

                //Get the value of the selected option
                alert("SelectedItem Value: " + selectedItem.val());

                //Get html or text of the selected option
                // alert("SelectedItem Text: " + selectedItem.html());

                //Get index of selected option
                alert("SelectedItem Index: " + selectedItem.index());
            });
        }
        catch(err){
            console.log(err);
        }

    };

    /* change color of selected field */
    function selectField(fieldName) {
        alert('Customer field selection clicked (outside ReportHomeView function): ' + fieldName);
        document.getElementById(fieldName).style.color = "red";
        return false;
    }

    AbsenceSoft.SelectReports.ReportRefreshView = function (category, pushState) {
        $.ajax({
            url: '/reports/absencereports/' + category,
            data: {},
            success: function (html) {
                $('#grpReport').html(html);
                $('#hdnSelectReport').val('categoryPage');
                AbsenceSoft.SelectReports.ReportHomeView();
                if (pushState)
                    if (window.history.pushState) window.history.pushState({ reportState: category }, "Reports", "?report=" + category);
            }
        });
    };
    AbsenceSoft.SelectReports.ReportListView = function (category, pushState) {
        $.ajax({
            url: '/reports/absencereports/' + category,
            data: {},
            success: function (html) {
                $('#grpReport').html(html);
                $('#hdnSelectReport').val('categoryPage');
                AbsenceSoft.SelectReports.ReportHomeView();
                if (pushState)
                    if (window.history.pushState) window.history.pushState({ reportState: category }, "Reports", "?report=" + category);
            }
        });
    };
    AbsenceSoft.SelectReports.ReportDetailListView = function (category, pushState) {
        $.ajax({
            url: '/reports/absencereports',
            data: {}
        });
    };
    window.onpopstate = function (event) {
        if (event == undefined || event == null) return;
        if (event.state == null || event.state.reportState == 'home') {
            $.ajax({
                url: '/reports/home',
                data: {},
                success: function (html) {
                    $('#grpReport').html(html);
                    $('#hdnSelectReport').val('home');
                    AbsenceSoft.SelectReports.Begin();
                }
            });
        }
        else if (event.state != null) {
            AbsenceSoft.SelectReports.ReportListView(event.state.reportState, true);
        }
    };

    AbsenceSoft.SelectReports.BIReport = 'Business Intelligence Reports';
    AbsenceSoft.SelectReports.OpReport = 'Operations Report';
    AbsenceSoft.SelectReports.AHReport = 'Ad-Hoc Report';
    AbsenceSoft.SelectReports.AHEmplReport = 'Ad-Hoc Empl Report';
    AbsenceSoft.SelectReports.AHEmplReport2 = 'Ad-Hoc Empl Report2';
    AbsenceSoft.SelectReports.AHFullScreenReport = 'AdHocReportCreate';
    AbsenceSoft.SelectReports.CriteriaString = '';

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);