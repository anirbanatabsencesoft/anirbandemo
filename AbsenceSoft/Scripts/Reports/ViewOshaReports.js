﻿(function (AbsenceSoft, $) {

    $(document).ready(function () {
        AbsenceSoft.OshaReports = AbsenceSoft.OshaReports || {};

        AbsenceSoft.OshaReports.Submit = function () {
            // Display a spinner to the user.
            AbsenceSoft.OshaReports.Begin();
            setTimeout(AbsenceSoft.OshaReports.UpdateReportableYears, 3000);
            return true;
        };

        AbsenceSoft.OshaReports.Begin = function () {
            $('#btnOshaViewReport').prop('disabled', true);
            $('.fa-spinner.run-spinner').removeClass('hidden');
        };

        AbsenceSoft.OshaReports.Complete = function () {
            $('#btnOshaViewReport').prop('disabled', false);
            $('.fa-spinner.run-spinner').addClass('hidden');
        };

        AbsenceSoft.OshaReports.ValidateReportableYears = function () {
            var hasYears = $("#SelectedYear option").length > 1;

            if (!hasYears) {
                $('#SelectedYear').addClass('hidden');
                $('.osha-no-years-found').removeClass('hidden');
            }
            else {
                $('#SelectedYear').removeClass('hidden');
                $('.osha-no-years-found').addClass('hidden');
            }
        };

        AbsenceSoft.OshaReports.UpdateReportables = function () {
            AbsenceSoft.OshaReports.UpdateReportableYears();
            AbsenceSoft.OshaReports.UpdateReportableLocations();
        };

        AbsenceSoft.OshaReports.UpdateReportableLocations = function () {
            var selectedEmployerId = $('#EmployerId').val();
            if (selectedEmployerId !== undefined && selectedEmployerId !== null && selectedEmployerId != '') {            
                $.ajax({
                    url: '/Reports/GetOshaReportableLocations/' + selectedEmployerId,
                    success: function (result) {
                        //Notify the user the request is complete.                  
                        $('.fa-spinner.year-spinner').addClass('hidden');
                        $('#SelectedLocation').select2('destroy');
                        $('#SelectedLocation').empty();
                        $.each(result, function (index, item) {
                            $('#SelectedLocation').append('<option value="' + item.Code + '">' + item.Name + '</option>');
                        });
                        $('#SelectedLocation').select2();
                    }
                });
            }
        };      

        AbsenceSoft.OshaReports.UpdateReportableYears = function () {
            var selectedEmployerId = $('#EmployerId').val();
            if (selectedEmployerId !== undefined && selectedEmployerId !== null && selectedEmployerId != '') {
                // This call is triggered when the employer is changed.
                // We need to get the reportable Years for the selected employer.
                // If none are found, hide the drop down and display a message to the user.
                $('#btnOshaViewReport').prop('disabled', true);
                $('.fa-spinner.year-spinner').removeClass('hidden');
               
                $.ajax({
                    url: '/Reports/GetOshaReportableYears/' + selectedEmployerId,
                    success: function (result) {
                        //Notify the user the request is complete.
                        $('.fa-spinner.year-spinner').addClass('hidden');
                        $('#SelectedYear').select2('destroy');
                        //Empty and Update the year drop own.
                        $('#SelectedYear').empty().append('<option value="">Please select a year...</option>');

                        result.forEach(function (item) {
                            $('#SelectedYear').append('<option value="' + item + '">' + item + '</option>');
                        });
                        $('#SelectedYear').select2();
                        AbsenceSoft.OshaReports.ValidateReportableYears();
                        AbsenceSoft.OshaReports.ValidateOshaCriteria();
                    },
                    failure: function (err) {
                        $('#btnOshaViewReport').prop('disabled', false);
                        $('.fa-spinner.year-spinner').addClass('hidden');
                        AbsenceSoft.OshaReports.ValidateReportableYears();
                        AbsenceSoft.OshaReports.ValidateOshaCriteria();
                        $('#Results').val(err.message);
                    }
                });              

            }
            else {
                $("#Form").val($("#Form option:first").val());
                $("#SelectedYear").empty();
                AbsenceSoft.OshaReports.ValidateReportableYears();
            }
        };

        AbsenceSoft.OshaReports.ValidateOshaCriteria = function () {
            // Make sure nothing is spinning;
            AbsenceSoft.OshaReports.Complete();

            var selectedForm = $('#Form').val();
            var selectedYear = $('#SelectedYear').val();
            var selectedOfficeLocation = $('#SelectedLocation').val();
            var selectedEmployerId = $('#EmployerId').val();
            var isValid = selectedForm !== undefined &&
                          selectedForm !== null &&
                          selectedForm != '' &&
                          selectedEmployerId !== undefined &&
                          selectedEmployerId !== null &&
                          selectedEmployerId != '' &&
                          selectedYear !== undefined &&
                          selectedYear !== null &&
                          selectedYear.length > 0
                          selectedOfficeLocation !== undefined &&
                          selectedOfficeLocation !== null &&
                          selectedOfficeLocation.length > 0;

            // Enable or disable the run button.
            $('#btnOshaViewReport').prop('disabled', !isValid);

            if (isValid) {
                $('#btnOshaViewReport').removeClass('osha-run-report');
                $('#btnOshaViewReport').addClass('osha-run-report-valid');
            }
            else {
                $('#btnOshaViewReport').removeClass('osha-run-report-valid');
                $('#btnOshaViewReport').addClass('osha-run-report');
            }
        }

        $('#Form').change(AbsenceSoft.OshaReports.ValidateOshaCriteria);
        $('#SelectedYear').change(AbsenceSoft.OshaReports.ValidateOshaCriteria);
        $('#SelectedLocation').change(AbsenceSoft.OshaReports.ValidateOshaCriteria);
        $('#EmployerId').change(AbsenceSoft.OshaReports.UpdateReportables);
        $('#DownloadOshaForm').submit(AbsenceSoft.OshaReports.Submit);

        AbsenceSoft.OshaReports.ValidateOshaCriteria();
        setTimeout(AbsenceSoft.OshaReports.UpdateReportables, 1000);
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);