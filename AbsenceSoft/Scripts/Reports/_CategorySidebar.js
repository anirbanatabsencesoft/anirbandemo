﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AdHocReports = AbsenceSoft.AdHocReports || {};
    var sourceElement = null;

    var toggleCategories = function () {
        $(this).find('span').toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
        $('.adhoc-report-category').not(this).find('span').removeClass('fa-chevron-circle-up').addClass('fa-chevron-circle-down');
        $('.ad-menu-list.in').collapse('hide');
    }

    var toggleFieldSelection = function () {
        AbsenceSoft.AdHocReports.MarkDirty();
        var that = $(this);
        var key = that.find('input');
        var keyVal = key.val();
        var isCurrentlyIncluded = key.hasClass('include');
        that.find('span').toggleClass('ad-menu-x');
        key.toggleClass('include');

        if (isCurrentlyIncluded) {
            var header = $('#AdHocReportDataGrid thead tr')
                .find(':input[value="' + keyVal + '"]')
                .parents('.adhoc-field');

            var nthChild = getHeaderIndex(header) + 1;
            $('#AdHocReportDataGrid tbody tr td').remove(':nth-child(' + nthChild + ')');
            header.remove();
        } else {
            var newField = '<th class="adhoc-field" draggable="true"><h>' + that.text() + '</h><input type="hidden" value="' + keyVal + '" /></th>';
            $('#AdHocReportDataGrid thead tr').append(newField);
            $('#AdHocReportDataGrid tbody tr').append('<td class="data-border"></th>');
            bindDragEvents();
        }   
    }

    var getHeaderIndex = function (header) {
        return $('#AdHocReportDataGrid thead tr .adhoc-field').index(header);
    }

    var filterFields = function () {
        var searchTerm = $(this).val().trim().toLowerCase();

        $('.format-tree-primary-branch').each(function (i, element) {
            var branch = $(element);
            if (branch.find('p').text().toLowerCase().indexOf(searchTerm) == -1) {
                branch.addClass('hide');
            } else {
                branch.removeClass('hide');
            }
        });

        $('.adhoc-report-category-container').each(function (i, element) {
            var category = $(element);
            var notHiddenFields = category.find('.format-tree-primary-branch').not('.hide');
            if(notHiddenFields.length > 0)
            {
                category.removeClass('hide');
            } else {
                category.addClass('hide');
            }
        });
    }

    var dragStart = function (e) {
        $(this).addClass('adhoc-dragging');
        $('.adhoc-field').not(this).addClass('adhoc-drag-target');

        sourceElement = this;
        /// We're not using dataTransfer.setData or getData, because IE doesn't have proper support for it
        /// But we need to set something, or FF assumes we have nothing to transfer
        e.originalEvent.dataTransfer.setData('text/plain', $(this).html());
        e.originalEvent.dataTransfer.effectAllowed = 'move';
    }

    var dragEnd = function (e) {
        $(this).removeClass('adhoc-dragging');
        $('.adhoc-field').removeClass('adhoc-drag-target adhoc-dragging adhoc-drag-over');
        sourceElement = null;
    }

    var allowDrop = function (e) {
        if (e.preventDefault)
            e.preventDefault();

        e.originalEvent.dataTransfer.dropEffect = 'move';
        return false;
    }

    var dropped = function (e) {
        if (e.stopPropagation)
            e.stopPropagation();

        if (sourceElement != this) {
            var currentHTML = $(this).html();
            var sourceHTML = $(sourceElement).html();
            $(sourceElement).html(currentHTML);
            $(this).html(sourceHTML);

            var targetColumn = getHeaderIndex(this) + 1;
            var sourceColumn = getHeaderIndex(sourceElement) + 1;
            $('#AdHocReportDataGrid tbody tr td:nth-child(' + targetColumn + ')').each(function (i, targetCell) {
                /// get the target cell html
                var targetCellHTML = $(targetCell).html();

                /// get a reference to the source cell
                var sourceCell = $($('#AdHocReportDataGrid tbody tr td:nth-child(' + sourceColumn + ')')[i]);

                /// set the target cell html to the source cell html
                $(targetCell).html(sourceCell.html());

                /// set the source cell html to the target cell html
                sourceCell.html(targetCellHTML);
            });
        }

        return false;
    }

    var handleDragEnter = function (e) {
        $(this).addClass('adhoc-drag-over');
    }

    var handleDragLeave = function (e) {
        $(this).removeClass('adhoc-drag-over');
    }

    var bindDragEvents = function () {
        $('.adhoc-field').unbind('dragstart').on('dragstart', dragStart);
        $('.adhoc-field').unbind('dragend').on('dragend', dragEnd);
        $('.adhoc-field').unbind('dragover').on('dragover', allowDrop);
        $('.adhoc-field').unbind('drop').on('drop', dropped);
        $('.adhoc-field').unbind('dragenter').on('dragenter', handleDragEnter);
        $('.adhoc-field').unbind('dragleave').on('dragleave', handleDragLeave);
    }

    $(document).ready(function () {
        $('.adhoc-report-category').click(toggleCategories);
        $('.format-tree-primary-branch').click(toggleFieldSelection);
        $('#categorySearch').keyup(filterFields);
        $('.return-ad-btn').click(AbsenceSoft.AdHocReports.ShowCategorySubMenu);
        bindDragEvents();
    });
    
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);