﻿/// <reference path="../Global/api-framework.js" />

(function (AbsenceSoft, $) {
    $(document).ready(function () {

        AbsenceSoft.ReportOperations = AbsenceSoft.ReportOperations || {};

        //define the module
        AbsenceSoft.ReportOperations.Module = "reporting";

        //make sure the information box is hidden at the start
        $("#report-saved-alert").hide();

        //trigger change in hidden field
        $('#adHocReportId').change(function () {
            showHideDeleteButton();            
        });

        //show hide delete button
        var showHideDeleteButton = function () {
            var id = $('#adHocReportId').val();
            if (id) {
                $('#deleteImageButton').show();
                $('#exportImageButton').show();
            }
            else {
                $('#deleteImageButton').hide();
            }
        }

        showHideDeleteButton();

        //add click event
        $("#deleteAdHocReport").click(function () {
            AbsenceSoft.ReportOperations.deleteSavedAdhocReport();
        });
        $("#btnExportReport").click(function () {
            AbsenceSoft.ReportOperations.exportReportDef();
        });
        //export report definition
        AbsenceSoft.ReportOperations.exportReportDef = function () {
            var reportId = $('#adHocReportId').val();
            var endPoint = "/service/adhoc/download?adhocId=" + reportId;
            $("#exportReportForm").attr('action', endPoint);
            $("#exportReportForm").attr('method', 'post');
            $("#exportReportForm").submit();
        }
        //delete a saved report
        AbsenceSoft.ReportOperations.deleteSavedAdhocReport = function () {
            var reportId = $('#adHocReportId').val();
            var data = { AdhocReportIds: reportId };
            var endPoint = "reports/adhoc/delete";

            try {
                AbsenceSoft.HttpUtils.Delete(AbsenceSoft.ReportOperations.Module, endPoint, data, function (result) {
                    var data = JSON.parse(result);
                    $("#report-saved-alert").show();
                    if (data.success == true) {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("Report deleted successfully");
                        //close the window
                        window.setTimeout(function () {
                            $(location).attr("href", "/reports/adhocreports");
                        }, 1000);
                    }
                    else {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("No data deleted");
                    }
                });
            }
            catch (ex) {
                $("#report-saved-alert").show();
                $("#report-saved-alert span").text(ex.message);
            }
        }

        $("#exportReportToProd").click(function () {
            AbsenceSoft.ReportOperations.exportReportToProd();
        });

        AbsenceSoft.ReportOperations.exportReportToProd = function () {
            var id= $('#adHocReportId').val()
            var data = GetAdhocReport(id);
           
           // data = { ColumnDefinition : data };
          
            var endPoint = "reports/adhoc/save";

            try {
                AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, endPoint, data, function (result) {
                    var data = JSON.parse(result);
                    $("#report-saved-alert").show();
                    if (data.success == true) {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("Report exported to production successfully");
                        //close the window
                        window.setTimeout(function () {
                            $(location).attr("href", "/reports/adhocreports");
                        }, 1000);
                    }
                    else {
                        $("#report-saved-alert").show();
                        $("#report-saved-alert span").text("No data exported");
                    }
                });
            }
            catch (ex) {
                $("#report-saved-alert").show();
                $("#report-saved-alert span").text(ex.message);
            }

        }

        var GetAdhocReport = function (id) {
            endPoint = "reports/adhoc/user/savedreports/?id="&id;
            try {
                AbsenceSoft.HttpUtils.Get(AbsenceSoft.ReportOperations.Module, endPoint, function (result) {
                    var data = JSON.parse(result);                  
                    if (data.success == true) {
                        return data.data[0];
                    }
                    else {
                        return null;
                    }
                });
            }
            catch (ex) {
                return null;
            }
        }
        //make sure the information box is hidden at the start
        $("#report-schedule-alert").hide();

        //schedule report
        AbsenceSoft.ReportOperations.scheduleSavedReport = function () {           
            var endPoint = "reports/schedule";
            var frequency = $("#schedule-frequency").val();
            var scheduleday = $("#schedule-day").val();
            var scheduletime = $("#schedule-time");
            var data = scheduleReportReqObj;

            if (frequency == "0") { data.ScheduleInfo.WeeklyRecurEveryXWeeks = 0; }
            else { data.ScheduleInfo.WeeklyRecurEveryXWeeks = 1; }
            data.ScheduleInfo.WeeklyRecurType = 0;
            
            data.ScheduleInfo.WeeklySelectedDays = {
                "Sunday": scheduleday=="0"? true : false,
                "Monday": scheduleday == "1" ? true : false,
                "Tuesday": scheduleday == "2" ? true : false,
                "Wednesday": scheduleday == "3" ? true : false,
                "Thursday": scheduleday == "4" ? true : false,
                "Friday": scheduleday == "5" ? true : false,
                "Saturday": scheduleday == "6" ? true : false
            };
            
            try {               
                AbsenceSoft.HttpUtils.Post(AbsenceSoft.ReportOperations.Module, endPoint, data, function (result) {                   
                    var data = JSON.parse(result);
                    var data = result;                    
                    $("#report-schedule-alert").show();
                    if (JSON.parse(data).success == true) {
                        $("#report-schedule-alert").show();
                        $("#report-schedule-alert span").text("Report scheduled successfully.");                        
                    }
                    else {
                        $("#report-schedule-alert").show();
                        $("#report-schedule-alert span").text("Report scheduling failed.");
                    }
                });
            }
            catch (ex) {
                $("#report-schedule-alert").show();
                $("#report-schedule-alert span").text(ex.message);
            }
        }

        $('.alert .close').on('click', function (e) {
            $(this).parent().hide();
        });

    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);