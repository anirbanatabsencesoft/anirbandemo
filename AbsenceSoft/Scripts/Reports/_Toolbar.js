﻿(function (AbsenceSoft, $) {
    var isDirty = false;
    var isExport = false;
    var isLoading = false;
    var isSave = false;
    var getFilters = function () {
        var filters = [];
        var uiFilters = $('#filters').find('.absencesoft-repeating').each(function(i){
            var that = $(this);
            var fieldName = that.find('.report-fields-for-filtering').select2('val');
            var operator = that.find('.filter-operator').val();
            var value = that.find('.filter-value').val();
            /// Only include filters that they have made selections on all three values
            if (fieldName && operator && value) {
                filters.push({
                    FieldName: fieldName,
                    Operator: operator,
                    Value: value
                })
            }
        });

        return filters;
    }

    var getFields = function () {
        var fields = [];

        $('#AdHocReportDataGrid thead tr th').each(function (i, element) {
            var fieldName = $(this).find('input').val();
            fields.push({
                Name: fieldName,
                Order: i + 1
            });
        })
        return fields;
    }

    var dataIsValid = function (reportData) {
        return reportData.Fields.length > 0;
    }

    var buildReportData = function () {
        return {
            Id: $('#adHocReportId').val(),
            Name: $('#adHocReportTitle').text(),
            Type: {
                Id: $('#adHocReportTypeId').val()
            },
            Filters: getFilters(),
            Fields: getFields()
        };
    }

    var runReport = function () {
        var data = buildReportData();
        if (!dataIsValid(data)) {
            $('#noFieldsModal').modal('show');
            return;
        }

        if (!isLoading) {
            loadReport(data);
        }
    }

    var disableRunButton = function () {
        if (isLoading) {
            $('#runAdHocReport').addClass('disabled').attr('disabled', true);
        }
    }

    var loadReport = function (data) {
        $('#adHocLoading').removeClass('hide');
        setTimeout(disableRunButton, 500);
        isLoading = true;

        $.ajax({
            url: '/AdHocReport/Run',
            type: 'POST',
            data: data,
            success: updateReport,
            complete: finishedLoading
        });
    }

    var finishedLoading = function () {
        $('#adHocLoading').addClass('hide');
        $('#runAdHocReport').removeClass('disabled').attr('disabled', false);
        isLoading = false;
    }

    var updateReport = function (html) {
        var messageIndex = html.indexOf('<p id="adHocMessage">');
        var message = $(html.substring(messageIndex));
        var content = html.slice(0, messageIndex);
        $('#AdHocReportDataGrid tbody').html(content);
        $('#adHocMessage').html(message.html());
    }

    var saveReport = function () {
        var reportId = $('#adHocReportId').val();
        if (reportId) {
            $('#adHocReportName').val($('#adHocReportTitle').text());
            isSave = true;
            $('#adHocReportSaveText').text('Save Report:');
            $('#adHocSaveReportAsDialog').modal('show');            
        } else {            
            saveAsReport();         
        }        
    }

    var saveAsReport = function () {
        isSave = false;
        $('#adHocReportSaveText').text('Save Report As:');
        $('#adHocReportName').val('');
        $('#adHocSaveReportAsDialog').modal('show');
    }

    var resetReportName = function () {
        $('#reportName').val('');
        $('#adHocReportNameRequired').addClass('hide');
    }

    var reportSaved = function (reportDefinition) {
        /// Wasn't actually saved
        if (!reportDefinition.Id)
            return false;

        $('#adHocReportId').val(reportDefinition.Id);
        $('#adHocReportTitle').text(reportDefinition.Name);
        isDirty = false;
        $('#adHocSaveReportAsDialog').modal('hide');
        $('#adHocReportId').val(reportDefinition.Id);
        $('#deleteImageButton').show();
        //if (window.location.host.includes("stage")) {         // Existing ES6 - includes()
        if (window.location.host.indexOf("stage") >= 0) {        // Added equivalent ES6 line to above line
            $('#exportImageButton').show();
        } else {
            $('#exportImageButton').show();
        }
        var url = '/reports/adhocreport/' + $("#adHocReportTypeId").val() + '/' + reportDefinition.Id;
        var aTag = $("#adhocSavedReportList").has('a[href="' + url + '"]');
        if (aTag.length==0)
        {
            $("#adhocSavedReportList").append('<li><a href="/reports/adhocreport/' + $('#adHocReportTypeId').val() + '/' + reportDefinition.Id + '">' + reportDefinition.Name + '</a></li>');
        }
        
    }

    var saveReportAs = function () {
        var data = buildReportData();
        data.Name = $('#adHocReportName').val();
        if (data.Name.trim() == '')
        {
            $('#adHocReportNameRequired').removeClass('hide');
            return;
        }        
        if (isSave) {
            $.ajax({
                url: '/AdHocReport/Save',
                type: 'POST',
                data: data,
                success: reportSaved
            });
        }
        else {
            $.ajax({
                url: '/AdHocReport/SaveAs',
                type: 'POST',
                data: data,
                success: function (data) {
                    if (data.Name == null) {
                        data.success = false;
                        $('#duplicateadHocReportName').removeClass('hide');
                        return;
                    }
                    else {
                        $('#duplicateadHocReportName').addClass('hide');
                        reportSaved(data);
                        //if (reportDefinition.Id)            // reportDefinition used without declaration so, it will be always undefined
                        if (data.Id)                          // replced reportDefinition with data
                            $('#adHocSaveReportAsDialog').modal('hide');

                    }
                }
            });
        }
    }

    var exportToCSV = function () {
        var data = buildReportData();
        if (!dataIsValid(data)) {
            $('#noFieldsModal').modal('show');
            return false;
        }

        isExport = true;
        $('#adHocReportData').val(JSON.stringify(data));
        $('#exportAdHocReportForm').submit();
        setTimeout(function () {
            isExport = false;
        }, 5000);
    }

    var handleUnload = function (event) {
        if (isDirty && !isExport) {
            var confirmationMessage = "You have unsaved changes to this report.  Are you sure you would like to leave the page?";
            event.returnValue = confirmationMessage;
            return confirmationMessage;
        }   
    }
  
    var uploadFile=function(event) {
        //we can create form by passing the form to Constructor of formData object
        //or creating it manually using append function 
        //but please note file name should be same like the action Parameter
        //var dataString = new FormData();
        //dataString.append("UploadedFile", selectedFile);

        var form = $('#uploadAdHocReportForm')[0];
        var dataString = new FormData(form);
        $.ajax({
            url: '/service/adhoc/upload',  //Server script to process data
            type: 'POST',            
            //Ajax events
            success: successHandler,
            error: errorHandler,           
            // Form data
            data: dataString,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
    }
    var successHandler = function(result){
        noty({ type: 'success', text: 'Upload report successful !' });
        window.setTimeout(function () { window.location = "/reports/adhocreports"; }, 3000);
    }
    var errorHandler = function (result) {
        noty({ type: 'error', text: 'Upload report failed !' });
        window.setTimeout(function () { window.location = "/reports/adhocreports"; }, 3000);
    }

    AbsenceSoft.AdHocReports = AbsenceSoft.AdHocReports || {};
    AbsenceSoft.AdHocReports.MarkDirty = function () {
        isDirty = true;
        isExport = false;
    };
    AbsenceSoft.AdHocReports.buildReportData = function () {
        return buildReportData();
    }
    $(document).ready(function () {
        $('#btnUploadReport').click(uploadFile);
        if ($('#adHocReportResults').length < 1)
            return;

        $('#runAdHocReport').click(runReport);
        $('#saveAdHocReportAs').click(saveReportAs);
        $('#adHocSaveReportAsDialog').on('hidden.bs.modal', resetReportName);
        $('#saveAdHocReport').click(saveReport);
        $('#saveAsAdHocReport').click(saveAsReport);
        $('#exportAdHocReport').click(exportToCSV);
       
        window.onbeforeunload = handleUnload;
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);