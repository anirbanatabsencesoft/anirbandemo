﻿var scheduleReportReqObj = {
    "RequestInfo": {
        "Identifier": "string",
        "ReportId": "DC01E524-0B4D-4218-9656-3BF715C08397",
        "CustomerId": "000000000000000000000002",
        "ReportRequestType": 0,
        "ReportDefinition": {
            "Id": 0,
            "Name": "string",
            "Description": "string",
            "Criteria": {
                "StartDate": "2017-01-01T10:33:22.447Z",
                "EndDate": "2018-02-15T10:33:22.447Z",
                "AgeFrom": 0,
                "AgeTo": 0,
                "ShowAgeCriteria": true,
                "Filters": [
                    {
                        "FieldName": "EmployerId",
                        "Prompt": "Employer",
                        "ControlType": 9,
                        "Operators": {},
                        "Operator": null,
                        "Options": [
                            {
                                "Text": "2044 Test Employer Short",
                                "Value": "5a131ea5c5f3320878845ee6",
                                "GroupText": null
                            },
                            {
                                "Text": "AbsenceSoft Test",
                                "Value": "000000000000000000000002",
                                "GroupText": null
                            },
                            {
                                "Text": "AdHoc",
                                "Value": "591d9fe6c5f3311aa07956a7",
                                "GroupText": null
                            },
                            {
                                "Text": "ATEST11",
                                "Value": "59cbbbbbc5f331468c198259",
                                "GroupText": null
                            },
                            {
                                "Text": "Automation Company",
                                "Value": "5908f9a7c5f3382244601727",
                                "GroupText": null
                            },
                            {
                                "Text": "DeleteEMTest1",
                                "Value": "59e512d8c5f32f1f7889b8d9",
                                "GroupText": null
                            },
                            {
                                "Text": "ESS Test Employer",
                                "Value": "5984b025c5f3302484466bec",
                                "GroupText": null
                            },
                            {
                                "Text": "GopiTest2",
                                "Value": "59fad0c5c5f32f1ecce311f1",
                                "GroupText": null
                            },
                            {
                                "Text": "GopiTestUpdate",
                                "Value": "59dd06b6c5f3305f446f2e18",
                                "GroupText": null
                            },
                            {
                                "Text": "iBots Inc.",
                                "Value": "5967e130c5f332181c547e0e",
                                "GroupText": null
                            },
                            {
                                "Text": "Johns Company LLC",
                                "Value": "59a06100c5f32f55f87b56dc",
                                "GroupText": null
                            },
                            {
                                "Text": "Monsters, Inc",
                                "Value": "57697b6dc5f32f0f904b3476",
                                "GroupText": null
                            },
                            {
                                "Text": "Ops Test",
                                "Value": "59a072a8c5f32f55f87b5c86",
                                "GroupText": null
                            },
                            {
                                "Text": "Policy Manager",
                                "Value": "59b6bd7ac5f32f4140fd36f7",
                                "GroupText": null
                            },
                            {
                                "Text": "Regression company 2",
                                "Value": "5852e6a3c5f331084cf55e76",
                                "GroupText": null
                            },
                            {
                                "Text": "Regression company 3",
                                "Value": "5852e773c5f331084cf55ec5",
                                "GroupText": null
                            },
                            {
                                "Text": "We Are United",
                                "Value": "57a2d857c5f32f08583a6621",
                                "GroupText": null
                            }
                        ],
                        "TypeName": null,
                        "Value": null,
                        "Values": [],
                        "OnChangeMethodName": null,
                        "KeepOptionAll": true,
                        "Tweener": null,
                        "Required": false
                    },
                    {
                        "FieldName": "Duration",
                        "Prompt": "Duration",
                        "ControlType": 11,
                        "Operators": {},
                        "Operator": null,
                        "Options": [
                            {
                                "Text": "Monthly",
                                "Value": "Monthly",
                                "GroupText": null
                            },
                            {
                                "Text": "Quarterly",
                                "Value": "Quarterly",
                                "GroupText": null
                            },
                            {
                                "Text": "Yearly",
                                "Value": "Yearly",
                                "GroupText": null
                            },
                            {
                                "Text": "Date Range",
                                "Value": "DateRange",
                                "GroupText": null
                            },
                            {
                                "Text": "Rolling 12 Months",
                                "Value": "Rolling",
                                "GroupText": null
                            }
                        ],
                        "TypeName": null,
                        "Value": "Monthly",
                        "Values": ["Yearly"],
                        "OnChangeMethodName": "FilterChangeEvent",
                        "KeepOptionAll": false,
                        "Tweener": null,
                        "Required": false
                    },
                    {
                        "FieldName": "WorkState",
                        "Prompt": "Work State",
                        "ControlType": 2,
                        "Operators": {},
                        "Operator": null,
                        "Options": [
                            {
                                "Text": "CA",
                                "Value": "CA",
                                "GroupText": null
                            },
                            {
                                "Text": "ME",
                                "Value": "ME",
                                "GroupText": null
                            },
                            {
                                "Text": "PR",
                                "Value": "PR",
                                "GroupText": null
                            },
                            {
                                "Text": "WI",
                                "Value": "WI",
                                "GroupText": null
                            }
                        ],
                        "TypeName": null,
                        "Value": null,
                        "Values": [],
                        "OnChangeMethodName": "FilterChangeEvent",
                        "KeepOptionAll": true,
                        "Tweener": null,
                        "Required": false
                    },
                    {
                        "FieldName": "Location",
                        "Prompt": "Location",
                        "ControlType": 9,
                        "Operators": {},
                        "Operator": null,
                        "Options": [
                            {
                                "Text": "GT005SM",
                                "Value": "4333",
                                "GroupText": null
                            },
                            {
                                "Text": "Berkeley",
                                "Value": "BERKELEY",
                                "GroupText": null
                            },
                            {
                                "Text": "CA001",
                                "Value": "CA001",
                                "GroupText": null
                            },
                            {
                                "Text": "CA002",
                                "Value": "CA002",
                                "GroupText": null
                            },
                            {
                                "Text": "CA003",
                                "Value": "CA003",
                                "GroupText": null
                            },
                            {
                                "Text": "California4",
                                "Value": "CAL004",
                                "GroupText": null
                            },
                            {
                                "Text": "CO001",
                                "Value": "CO001",
                                "GroupText": null
                            },
                            {
                                "Text": "CO002",
                                "Value": "CO002",
                                "GroupText": null
                            },
                            {
                                "Text": "CO003",
                                "Value": "CO003",
                                "GroupText": null
                            },
                            {
                                "Text": "Den Centennial",
                                "Value": "Den Centennial",
                                "GroupText": null
                            },
                            {
                                "Text": "Denver",
                                "Value": "DENVER",
                                "GroupText": null
                            },
                            {
                                "Text": "Denver",
                                "Value": "DO",
                                "GroupText": null
                            },
                            {
                                "Text": "GTEST11",
                                "Value": "GT001",
                                "GroupText": null
                            },
                            {
                                "Text": "GTEST22",
                                "Value": "GT002",
                                "GroupText": null
                            },
                            {
                                "Text": "IL001",
                                "Value": "IL001",
                                "GroupText": null
                            },
                            {
                                "Text": "IL002",
                                "Value": "IL002",
                                "GroupText": null
                            },
                            {
                                "Text": "IL003",
                                "Value": "IL003",
                                "GroupText": null
                            },
                            {
                                "Text": "KF",
                                "Value": "KF",
                                "GroupText": null
                            },
                            {
                                "Text": "Los Angeles",
                                "Value": "LA",
                                "GroupText": null
                            },
                            {
                                "Text": "Los Angeles",
                                "Value": "LOS ANGELOS",
                                "GroupText": null
                            },
                            {
                                "Text": "NJ001",
                                "Value": "NJ001",
                                "GroupText": null
                            },
                            {
                                "Text": "NJ002",
                                "Value": "NJ002",
                                "GroupText": null
                            },
                            {
                                "Text": "NJ003",
                                "Value": "NJ003",
                                "GroupText": null
                            },
                            {
                                "Text": "NY001",
                                "Value": "NY001",
                                "GroupText": null
                            },
                            {
                                "Text": "NY002",
                                "Value": "NY002",
                                "GroupText": null
                            },
                            {
                                "Text": "NY003",
                                "Value": "NY003",
                                "GroupText": null
                            },
                            {
                                "Text": "POWERPLAY",
                                "Value": "PP",
                                "GroupText": null
                            },
                            {
                                "Text": "Q2Test",
                                "Value": "Q2",
                                "GroupText": null
                            },
                            {
                                "Text": "Regression2location007",
                                "Value": "R2L007",
                                "GroupText": null
                            },
                            {
                                "Text": "San Diego",
                                "Value": "SAN DIEGO",
                                "GroupText": null
                            },
                            {
                                "Text": "Seattle",
                                "Value": "SEATTLE",
                                "GroupText": null
                            }
                        ],
                        "TypeName": null,
                        "Value": null,
                        "Values": [],
                        "OnChangeMethodName": "FilterChangeEvent",
                        "KeepOptionAll": true,
                        "Tweener": null,
                        "Required": false
                    }
                ],
                "PlainEnglish": "From '01/16/2018' through '02/15/2018' where Duration is 'Monthly'",
                "HasCriterias": true
            }
        },
        "PageNumber": 1,
        "PageSize": 100,
        "orderByAsc": true,
        "Limit": 0,
        "Message": "string",
        "Offset": 0,
        "Rows": 0,
        "IsValid": true
    },
    "ScheduleInfo": {
        "Id": null,
        "AdjustmentValue": 0,
        "RecurrenceType": 1,
        "EndDate": null,
        "StartDate": "2018-02-19T21:48:10.8083146+05:30",
        "NumberOfOccurrences": 0,
        "EndDateType": null,
        "DailyRecurEveryXDays": 0,
        "DailyRecurType": null,
        "WeeklyRecurEveryXWeeks": 0,
        "WeeklyRecurType": null,
        "WeeklySelectedDays": {
            "Sunday": false,
            "Monday": false,
            "Tuesday": false,
            "Wednesday": false,
            "Thursday": false,
            "Friday": false,
            "Saturday": false
        },
        "MonthlyRecurEveryXMonths": 0,
        "MonthlyRecurOnSpecificDateDayValue": 0,
        "MonthlySpecificDatePartTwo": 0,
        "MonthlySpecificDatePartOne": 0,
        "MonthlyRecurType": 0,
        "YearlySpecificDatePartOne": 0,
        "YearlySpecificDatePartTwo": 0,
        "YearlySpecificDatePartThree": 0,
        "YearlyRecurType": null,
        "SpecificDateDayValue": 0,
        "SpecificDateMonthValue": 0
    }
};