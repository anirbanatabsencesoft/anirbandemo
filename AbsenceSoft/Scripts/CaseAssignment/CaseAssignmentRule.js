﻿//Added for AT-6540 to handle dynamically generated custom fields. 11 is the last index for fixed values in dropdown
var maxIndexForCustomFields = 12;
(function (AbsenceSoft, $) {
    $(document).ready(function () {

        $(".case-assignment-ruleset-reorder").on("click", function () {

            var token = $('input[name="__RequestVerificationToken"]').first().val();

            var caseAssignmentRuleSetOrderList = [];

            $('.case-assignment-ruleset-order').each(function (index, element) {
                if ($(this).siblings('.case-assignment-ruleset-changed').val()) {
                    var caseAssignmentRuleSetOrder = {
                        Code: $(this).siblings('.case-assignment-ruleset-code').val(),
                        CaseAssignmentRuleOrder: $(this).val()
                    };

                    caseAssignmentRuleSetOrderList.push(caseAssignmentRuleSetOrder);
                }                                
            });

            if (caseAssignmentRuleSetOrderList.length > 0) {
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/CaseAssignmentRuleSet/SaveRulesetOrder',
                    data: {
                        __RequestVerificationToken: token,
                        'model': caseAssignmentRuleSetOrderList
                    },
                    beforeSend: function (xhr) { xhr.setRequestHeader("X-XSRF-Token", token); },
                    success: function (response) {
                        $('.case-assignment-ruleset-changed').each(function (index, element) {
                            $(this).val(false);
                        });
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: 'Rule sets order updated.' });
                    },
                    error: function (message) {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: 'Rule sets order update failed.' });
                    }
                });

            }

            event.preventDefault();
            return false;                                      
        });

        $(".case-ruleset-move-up").on("click", function () {
            var elem = $(this).parent("div").parent("div").closest("div");
            elem.prev().before(elem);

            $(this).siblings('.case-assignment-ruleset-changed').val(true);
            var elemOrder = Number($(this).siblings('.case-assignment-ruleset-order').val()) - 1;
            

            $('.case-assignment-ruleset-order').each(function (index, element) {
                if (element.value == elemOrder) {
                    $(this).val(elemOrder + 1);                    
                    $(this).siblings('.case-assignment-ruleset-changed').val(true);
                }
            });

            $(this).siblings('.case-assignment-ruleset-order').val(elemOrder);
        });


        $(".case-ruleset-move-down").on("click", function () {
            var elem = $(this).parent("div").parent("div").closest("div");
            elem.next().after(elem);

            $(this).siblings('.case-assignment-ruleset-changed').val(true);
            var elemOrder = Number($(this).siblings('.case-assignment-ruleset-order').val()) + 1;

            $('.case-assignment-ruleset-order').each(function (index, element) {
                if (element.value == elemOrder) {
                    $(this).val(elemOrder - 1);                    
                    $(this).siblings('.case-assignment-ruleset-changed').val(true);
                }
            });

            $(this).siblings('.case-assignment-ruleset-order').val(elemOrder);
        });


        if ($('#hdnCaseAssignmentCustomer').length == 0)
            return;
        $('.btn-add-case-assignment-rule').click(function () {
            $.ajax({
                async: false,
                type: 'GET',
                url: '/CaseAssignmentRuleSet/CreateCaseAssignmentRule',
                contentType: 'application/json',
                success: function (response) {
                    var i = 0;
                    if ($('.case-assignment-rule').length <= 0) {
                        $('.case-assignment-rules').html('<div>' + response + '</div>');
                    }
                    else {
                        $('.case-assignment-rules').append('<div>' + response + '</div>');
                    }
                    UpdateElementsBehavior();
                }
            });
        });
        UpdateElementsBehavior();
    });
    var UpdateElementsBehavior = function () {
        UpdateCaseAssignmentRulesIndexes();
        UpdateCaseAssignmentRulesValidations();
        MakeSelect2();
        RuleChange();
        AssignmentTypeChange();
        ChangeOrderEvents();
        RemoveRuleEvents();
    }

    var UpdateRuleOrder = function () {
        $('.case-assignment-rule-order').each(function (index, elemant) {
            $(this).val(index + 1);
        });
    }
    var UpdateCaseAssignmentRulesIndexes = function () {
        $('.case-assignment-rule-name').each(function (index, element) {
            $(this).attr('name', 'CaseAssignmentRules[' + index + '].Name');
            $(this).attr('id', 'CaseAssignmentRules[' + index + '].Name');
            $(this).siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].Name');
            $(this).parents(':eq(2)').siblings('.case-assignment-rule-id').attr('name', 'CaseAssignmentRules[' + index + '].Id');
            $(this).parents(':eq(2)').siblings('.case-assignment-rule-id').attr('id', 'CaseAssignmentRules[' + index + '].Id');
            $(this).parents(':eq(2)').siblings('.case-assignment-rule-order').attr('name', 'CaseAssignmentRules[' + index + '].Order');
            $(this).parents(':eq(2)').siblings('.case-assignment-rule-order').attr('id', 'CaseAssignmentRules[' + index + '].Order');
            $(this).parents(':eq(2)').siblings('.case-assignment-rule-order').attr('value', index);
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-description').attr('name', 'CaseAssignmentRules[' + index + '].Description');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-description').attr('id', 'CaseAssignmentRules[' + index + '].Description');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-description').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].Description');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-behavior').attr('name', 'CaseAssignmentRules[' + index + '].Behavior');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-behavior').attr('id', 'CaseAssignmentRules[' + index + '].Behavior');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-behavior').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].Behavior');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type').attr('name', 'CaseAssignmentRules[' + index + '].RuleType');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type').attr('id', 'CaseAssignmentRules[' + index + '].RuleType');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].RuleType');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type-hidden').attr('name', 'CaseAssignmentRules[' + index + '].CustomFieldId');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type-hidden').attr('id', 'CaseAssignmentRules[' + index + '].CustomFieldId');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type-hidden-type').attr('name', 'CaseAssignmentRules[' + index + '].CustomFieldType');
            $(this).parents(':eq(1)').siblings().find('.case-assignment-rule-type-hidden-type').attr('id', 'CaseAssignmentRules[' + index + '].CustomFieldType');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-rule-values').attr('name', 'CaseAssignmentRules[' + index + '].RuleValues');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-rule-values').attr('id', 'CaseAssignmentRules[' + index + '].RuleValues');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-rule-values').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].RuleValues');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value').attr('name', 'CaseAssignmentRules[' + index + '].Value');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value').attr('id', 'CaseAssignmentRules[' + index + '].Value');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].Value');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value2').attr('name', 'CaseAssignmentRules[' + index + '].Value2');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value2').attr('id', 'CaseAssignmentRules[' + index + '].Value2');
            $(this).parents(':eq(1)').siblings().find('input.case-assignment-rule-value2').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].Value2');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type').attr('name', 'CaseAssignmentRules[' + index + '].CaseAssignmentType');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type').attr('id', 'CaseAssignmentRules[' + index + '].CaseAssignmentType');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].CaseAssignmentType');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type-value').attr('name', 'CaseAssignmentRules[' + index + '].CaseAssignmentId');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type-value').attr('id', 'CaseAssignmentRules[' + index + '].CaseAssignmentId');
            $(this).parents(':eq(1)').siblings().find('select.case-assignment-type-value').siblings('.field-validation-valid').attr('data-valmsg-for', 'CaseAssignmentRules[' + index + '].CaseAssignmentId');
        });
    }
    var UpdateCaseAssignmentRulesValidations = function () {
        $('#editCaseAssignmentRuleSetForm').removeData("validator")
        $('#editCaseAssignmentRuleSetForm').removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse($('#editCaseAssignmentRuleSetForm'));
    }


    var RuleChange = function () {
        $('.case-assignment-rule-type').each(function (index, element) {
            $(this).change(function () {
                $(this).parent().siblings('.case-assignment-rule-value-selection').html('');
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').html('');
                switch ($(this).val()) {
                    case '1':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#employerValues').html());
                        break;
                    case '2':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#workStateValues').html());
                        break;
                    case '3':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#caseTypesValues').html());
                        break;
                    case '4':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#policiesValues').html());
                        break;
                    case '5':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#policyTypesValues').html());
                        break;
                    case '6':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').
                            html("<div class='col-sm-6 case-assignment-padding-0R'>" +
                            "<input type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                            "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>" +
                            "</div>" +
                            "<div class='col-sm-6 case-assignment-padding-0R'>" +
                            "<input type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value2'/>" +
                            "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>" +
                            "</div>");
                        break;
                    case '10':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').
                            html("<input type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                            "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>");
                        break;
                    case '11':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#officeLocationsValues').html());
                        break;
                    case '12':
                        $(this).parent().siblings('.case-assignment-rule-value-selection').html($('#accommodationTypesValues').html());
                        break;
                    default:
                        //Added for AT-6540 to handle dynamically generated custom fields.
                        var customType = $('option:selected', this).attr('data-customType');
                        var valueType = $('option:selected', this).attr('data-valueType');
                        var customFieldId = $('option:selected', this).attr('customFieldId');
                        $(this).siblings('.case-assignment-rule-type-hidden').val(customFieldId);
                        var customFieldType = $('option:selected', this).attr('customFieldType');
                        $(this).siblings('.case-assignment-rule-type-hidden-type').val(customFieldType);
                        if (valueType == 'UserEntered') {
                            if (customType == 'Text') {
                                $(this).parent().siblings('.case-assignment-rule-value-selection').
                                    html("<input type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                                    "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>");
                            }
                            else if (customType == 'Number') {
                                $(this).parent().siblings('.case-assignment-rule-value-selection').
                                    html("<input type='number' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                                    "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>");
                            }
                            else if (customType == 'Date') {
                                $(this).parent().siblings('.case-assignment-rule-value-selection').
                                    html("<input id='customFieldDate' type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                                    "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>");
                                $('#customFieldDate').datepicker({ autoclose: true});
                            }
                            else {
                                $(this).parent().siblings('.case-assignment-rule-value-selection').
                                    html("<input type='text' data-val='true' data-val-required='Value is required.' class='form-control case-assignment-rule-value'/>" +
                                    "<span class='field-validation-valid help-block' data-valmsg-replace='true'></span>");
                            }
                        }
                        if (valueType == 'SelectList') {
                            var divId = "#CustomFieldValues_" + $(this).val();
                            $(this).parent().siblings('.case-assignment-rule-value-selection').html($(divId).html());
                        }
                        break;
                };

                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').html($('#assignmentTypesValues').html());
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').find('select.case-assignment-use-type').removeClass('case-assignment-use-type').addClass('case-assignment-type');
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').find('.case-assignment-type-value-use-selection').removeClass('case-assignment-type-value-use-selection').addClass('case-assignment-type-value-selection');
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').find('select.case-assignment-type').attr('data-val', true);
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').find('select.case-assignment-type').attr('data-val-required', 'This field is required.');
                $(this).parents(':eq(1)').siblings('.case-assignment-value-selection').find('.field-validation-valid').attr('data-valmsg-for', $(this).parent().siblings('.case-assignment-value-selection').find('select.case-assignment-type').name);

                AssignmentTypeChange();

                $(this).parent().siblings('.case-assignment-rule-value-selection').find('.case-assignment-rule-use-values').removeClass('case-assignment-rule-use-values').addClass('case-assignment-rule-values');
                $(this).parent().siblings('.case-assignment-rule-value-selection').find('select.case-assignment-rule-values').select2();
                $(this).parent().siblings('.case-assignment-rule-value-selection').find('select.case-assignment-rule-values').attr('data-val', true);
                $(this).parent().siblings('.case-assignment-rule-value-selection').find('select.case-assignment-rule-values').attr('data-val-required', 'This field is required.');
                $(this).parent().siblings('.case-assignment-rule-value-selection').find('.field-validation-valid').attr('data-valmsg-for', $(this).parent().siblings('.case-assignment-rule-value-selection').find('select.case-assignment-rule-values').name);

                UpdateCaseAssignmentRulesIndexes();
                UpdateCaseAssignmentRulesValidations();
            });
        });
    }
    var AssignmentTypeChange = function () {
        $('select.case-assignment-type').each(function (index, element) {
            $(this).unbind('change');
            $(this).change(function () {
                $(this).parent().siblings('.case-assignment-type-value-selection').html('');
                if ($(this).val() == 1) {
                    $(this).parent().siblings('.case-assignment-type-value-selection').html($('#assignmentTeamsValues').html());
                    $(this).parent().siblings('.case-assignment-type-value-selection').find('.case-assignment-use-type-value').removeClass('case-assignment-use-type-value').addClass('case-assignment-type-value');
                }
                else {
                    $(this).parent().siblings('.case-assignment-type-value-selection').html($('#assignmentUsersValues').html());
                    $(this).parent().siblings('.case-assignment-type-value-selection').find('.case-assignment-use-type-value2').removeClass('case-assignment-use-type-value2').addClass('case-assignment-type-value');
                }
                $(this).parent().siblings('.case-assignment-type-value-selection').find('select.case-assignment-type-value').attr('data-val', true);
                $(this).parent().siblings('.case-assignment-type-value-selection').find('select.case-assignment-type-value').attr('data-val-required', 'This field is required.');
                UpdateCaseAssignmentRulesIndexes();
                UpdateCaseAssignmentRulesValidations();
            });
        });
    }
    var MakeSelect2 = function () {
        $("select.case-assignment-rule-values").each(function () {
            $(this).select2();
        });
    }
    var ChangeOrderEvents = function () {
        $('.case-rule-move-up').each(function () {
            $(this).unbind('click');
            $(this).click(function () {
                var elm = ($(this).parents(':eq(3)'));
                if (elm != undefined && elm != null && elm.prev().length > 0) {
                    elm.insertBefore(elm.prev());
                    UpdateElementsBehavior();
                }
            });
        });
        $('.case-rule-move-down').each(function () {
            $(this).unbind('click');
            $(this).click(function () {
                var elm = ($(this).parents(':eq(3)'));
                if (elm != undefined && elm != null && elm.next().length > 0) {
                    elm.insertAfter(elm.next());
                    UpdateElementsBehavior();
                }
            });
        });
    }
    var RemoveRuleEvents = function () {
        $('.case-rule-delete').each(function (index, elemant) {
            $(this).unbind('click');
            $(this).click(function () {
                var elm = ($(this).parents(':eq(3)'));
                if (elm != undefined && elm != null && elm.length > 0) {
                    elm.remove();
                    UpdateElementsBehavior();
                }
            });
        });
    }
    AbsenceSoft.CaseAssignmentRule = AbsenceSoft.CaseAssignmentRule || {};
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);