﻿(function (AbsenceSoft, $) {
    AbsenceSoft.AccommodationType = AbsenceSoft.AccommodationType || {};

    var setTogglesToMatchAccommodationType = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    var toggleAbsenceReason = function () {
        var employerId = $('#employerId').val();
        $.ajax({
            type: 'POST',
            url: '/AccommodationTypes/ToggleAccommodationTypeSuppression/' + employerId,
            data: {
                Code: $('#accommodationTypeCode').val(),
                EmployerId: employerId
            },
            success: setTogglesToMatchAccommodationType
        });
    };

    var accommodationTypeStart = function () {
        if ($('#editAccommodationTypeForm').length < 1)
            return;

        $('#editAccommodationTypeForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('.ar-disabled-toggle').click(toggleAbsenceReason);
        AbsenceSoft.PageStart('#editAccommodationTypeForm');
    }

    AbsenceSoft.AccommodationType.Start = accommodationTypeStart;

    $(document).ready(accommodationTypeStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);