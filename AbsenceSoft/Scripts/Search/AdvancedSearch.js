﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        $("#as_load_screen").hide();
        $('#divSearchResult').ready(function () {
            setTimeout(function () {
                if ($("#searchList div.row").length >= 30) {
                    $('#loadMoreSearches').removeClass('hide');
                    $('#loadMoreSearches').addClass('show');
                }
            }, 1000);
        })
        $('#loadMoreSearches').click(loadNextSearches);
        $('.checkboxlst').change(AbsenceSoft.AdvancedSearch.FilterByCriteria);
        $("#txtLastName").change(AbsenceSoft.AdvancedSearch.FilterByCriteria);
        $("#txtDOB").change(AbsenceSoft.AdvancedSearch.FilterByCriteria);
        $("#as_unselect").on("click", function () {
            var as_unselect = $("#as_unselect").text();
            if (as_unselect.toLowerCase() === "unselect all") {
                $("#as_unselect").text("SELECT ALL");
                $('input:checkbox').removeAttr('checked');
                AbsenceSoft.AdvancedSearch.FilterByCriteria();
            }
            if (as_unselect.toLowerCase() === "select all") {
                $("#as_unselect").text("UNSELECT ALL");
                $('input:checkbox').prop("checked", true);
                AbsenceSoft.AdvancedSearch.FilterByCriteria();
            }
        });
        // This event is triggered when you select anything from Relevance drop down on Advanced Search page
        $("#as_sortddl").change(function () {
            AbsenceSoft.AdvancedSearch.ServerFilterByCriteria();
        });

        if ($("#asFilterBox").length > 0) {
            document.getElementById("asFilterBox").addEventListener("scroll", AbsenceSoft.AdvancedSearch.ScrollLeftDiv);

            $('#datepickerDOB').datepicker(
                {
                    orientation: "bottom auto",
                    autoclose: true
                }
            );
            $(window).scroll(function () {
                var offset = $("#datepickerDOB").offset();
                var height = $("#datepickerDOB").height();
                var datepickerHeight = $('.datepicker').height();
                $('.datepicker').css({ 'top': (offset.top - (datepickerHeight + 14)) + 'px', left: offset.left + 'px' });
                AbsenceSoft.AdvancedSearch.SetSearchBoxBottom();
            });
        }
    });

    var loadNextSearches = function (e) {
        var currentPage = parseInt($('#page').val(), 10);
        $('#page').val(currentPage += 1);
        loadSearches(e);
    }

    var loadSearches = function (e) {
        var employersArray = [];
        $(':checkbox').each(function () {
            if (this.checked) {
                employersArray.push($(this).val());
            }
        });
        if (employersArray !== null && employersArray.length >= 1) {
            $.ajax({
                url: '/search',
                data: {
                    sortBy: $('#as_sortddl :selected').val(),
                    searchText: $('#searchText').val(),
                    lastName: $('#lastname1').val(),
                    dateOfBirth: $('#dob1').val(),
                    page: $('#page').val(),
                    employers: employersArray,
                    pageSize: $('#pageSize').val(),
                    searchCount: $('#as_searchCount').val()
                },
                success: function (data) {
                    if (data != '' && data.indexOf('class="row"') != -1) {
                        $('#searchList').append(data);
                    }

                    if (data == '' || data.indexOf('class="row"') == -1) {
                        $('#loadMoreSearches').removeClass('show');
                        $('#loadMoreSearches').addClass('hide');
                        $('#noSearches').removeClass('hide');
                    }
                }
            });
        }
    };

    var AbsenceSoft = AbsenceSoft || {};
    AbsenceSoft.AdvancedSearch = AbsenceSoft.AdvancedSearch || {};

    AbsenceSoft.AdvancedSearch.ClearLastName = function () {
        $('#txtLastName').val('').focus();
        $(".as_clearable").html('');
        AbsenceSoft.AdvancedSearch.FilterByCriteria();
    }

    AbsenceSoft.AdvancedSearch.FilterByCriteria = function () {
        setTimeout(AbsenceSoft.AdvancedSearch.ServerFilterByCriteria, 500);
    }

    AbsenceSoft.AdvancedSearch.ApplyFilters = function () {
        var count = 0;
        $(".count").html(0);
        // Regular expression used in the replace function below will escape all special characters in Last Name
        var lastName = $("#txtLastName").val().trim().toLowerCase().replace(/[~`!@#$%^&*()-+=;:"',<>.?/]/g, "\\$&");
        if (lastName.length === 0) {
            $(".as_clearable").html('');
        }
        else {
            $(".as_clearable").html("<i id='iconChange' onclick='AbsenceSoft.AdvancedSearch.ClearLastName()' class='fa fa-times'></i>");
        }
        var dob = $("#txtDOB").val();
        var regExp = new RegExp('/', 'g');
        var dateOfBirth = dob.replace(regExp, '');
        var rows = $("#searchList div.row");

        if (count == 0) {
            $(".count").html(0);
        }
        AbsenceSoft.AdvancedSearch.SetSearchBoxBottom();
    }

    AbsenceSoft.AdvancedSearch.ServerFilterByCriteria = function () {
        AbsenceSoft.AdvancedSearch.ApplyFilters();

        $("#as_load_screen").show();
        $('#as_searchCount').val(0);
        $('#page').val(1);
        var employersArray = [];
        $(':checkbox').each(function () {
            if (this.checked) {
                employersArray.push($(this).val());
            }
        });
        if (employersArray !== null && employersArray.length >= 1) {
            var lastName = $("#txtLastName").val().trim().toLowerCase().replace(/[~`!@#$%^&*()-+=;:"',<>.?/]/g, "\\$&");
            $('#lastName').val(lastName);
            var dob = $("#txtDOB").val();
            var regExp = new RegExp('/', 'g');
            var dateOfBirth = dob.replace(regExp, '');
            $('#dob').val(dob);
            $.ajax({
                url: '/search',
                data: {
                    sortBy: $('#as_sortddl :selected').val(),
                    searchText: $('#searchText').val(),
                    lastName: $('#lastName').val(),
                    dateOfBirth: $('#dob').val(),
                    page: $('#page').val(),
                    employers: employersArray
                },
                success: function (data) {
                    $("#as_load_screen").hide();
                    $("#searchList").empty();
                    $('#searchList').append(data);
                    document.getElementById('searchcount').innerHTML = $('#searchcount1').val();
                    var as_unselect = $("#as_unselect").text();
                    if (as_unselect.toLowerCase() === "select all") {
                        $("#as_unselect").text("UNSELECT ALL");
                    }

                    setTimeout(function () {
                        if ($("#searchList div.row").length >= 30) {
                            $('#loadMoreSearches').removeClass('hide');
                            $('#loadMoreSearches').addClass('show');
                            $('#noSearches').removeClass('show');
                            $('#noSearches').addClass('hide');
                        }
                        else {
                            $('#loadMoreSearches').removeClass('show');
                            $('#loadMoreSearches').addClass('hide');
                            $('#noSearches').removeClass('show');
                            $('#noSearches').addClass('hide');
                        }
                    }, 300);
                },
                type: "POST",
            });
        }
        else {
            $("#as_load_screen").hide();
            var searchText = $('#searchText').val();
            var lastName = $('#lastName').val();
            var dob = $('#dob').val();
            $("#searchList").empty();
            $("#searchList").append('<input type="hidden" id="page" name="page" value="0" />');
            $("#searchList").append('<input type="hidden" id="searchText" name="searchText" value="" />');
            $("#searchList").append('<input type="hidden" id="lastName" name="lastName" value="" />');
            $("#searchList").append('<input type="hidden" id="dob" name="dob" value="" />');
            $('#page').val(0);
            $('#searchText').val(searchText);
            $('#lastName').val(lastName);
            $('#dob').val(dob);
            $('#searchList').append('<div id="as_no_data">No results match your criteria, please try searching for a new term or make your search broader to include more results.</div>');
            $('#noSearches').removeClass('show');
            $('#noSearches').addClass('hide');
            document.getElementById('searchcount').innerHTML = "0";
            var as_unselect = $("#as_unselect").text();
            if (as_unselect.toLowerCase() === "unselect all") {
                $("#as_unselect").text("SELECT ALL");
            }
       }
    }

    AbsenceSoft.AdvancedSearch.ServerSortByCriteria = function () {
        $("#as_load_screen").show();
        $(".checkboxlst").empty(); 
        $('#as_searchCount').val(0);
        $('#page').val(1)
        var lastName = $("#txtLastName").val().trim().toLowerCase().replace(/[~`!@#$%^&*()-+=;:"',<>.?/]/g, "\\$&");
        $('#lastName').val(lastName);
        var dob = $("#txtDOB").val();
        var regExp = new RegExp('/', 'g');
        var dateOfBirth = dob.replace(regExp, '');
        $('#dob').val(dob);
        $('#employers').val('');
        var employersArray = [];
        $(':checkbox').each(function () {
            if (this.checked) {
                employersArray.push($(this).val());
            }
        });
        if (employersArray !== null && employersArray.length >= 1) {
            $.ajax({
                url: '/search',
                data: {
                    sortBy: $('#as_sortddl :selected').val(),
                    searchText: $('#searchText').val(),
                    lastName: $('#lastName').val(),
                    dateOfBirth: $('#dob').val(),
                    employers: employersArray
                },
                success: function (data) {
                    $("#as_load_screen").hide();
                    $("#searchList").empty();
                    $('#searchList').append(data);
                    document.getElementById('searchcount').innerHTML = $('#searchcount1').val();

                    if (data != '' || data.indexOf('class="row"') != -1) {
                        $('#loadMoreSearches').removeClass('hide');
                        $('#loadMoreSearches').addClass('show');
                        $('#noSearches').removeClass('show');
                        $('#noSearches').addClass('hide');
                    }

                    AbsenceSoft.AdvancedSearch.FilterByCriteria();
                },
                type: "POST",
            });
        }
        else {
            $("#as_load_screen").hide();
            $("#searchList").empty();
        }
    }

    AbsenceSoft.AdvancedSearch.SortByCriteria = function () {
        var attribute = $('#as_sortddl :selected').val();
        var table, rows, switching, i, x, y, shouldSwitch;
        table = $("#searchList");
        switching = true;
        try {
            /*Make a loop that will continue until no switching has been done:*/
            while (switching) {
                //Start by saying: no switching is done:
                switching = false;
                rows = $("#searchList div.row");
                for (i = 0; i < (rows.length); i++) {
                    //Start by saying there should be no switching:
                    shouldSwitch = false;
                    /*Get the two elements you want to compare, one from current row and one from the next:*/
                    if (attribute == 'relevance' || attribute == "cn") {
                        x = $(rows[i]).find("A")[0].innerHTML.toLowerCase();
                        y = $(rows[i + 1]).find("A")[0].innerHTML.toLowerCase();
                        //Check if the two rows should switch place:
                        if (x > y) {
                            //If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                    else {
                        x = $(rows[i]).attr(attribute).toLowerCase();
                        y = $(rows[i + 1]).attr(attribute).toLowerCase();
                        //Check if the two rows should switch place:
                        if (x > y) {
                            //If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    /*If a switch has been marked, make the switch and mark that a switch has been done:*/
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        } catch (e) {

        }
    }

    AbsenceSoft.AdvancedSearch.SetSearchBoxBottom = function () {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            $("#searchBoxFilter").css({ 'bottom': '112px' }); 
        }
        else {
            $("#searchBoxFilter").css({ 'bottom': '0px' });
        }
    }

    AbsenceSoft.AdvancedSearch.ScrollLeftDiv = function () {
        var offset = $("#datepickerDOB").offset();
        var height = $("#datepickerDOB").height();
        var datepickerHeight = $('.datepicker').height();
        $('.datepicker').css({ 'top': (offset.top - (datepickerHeight + 14)) + 'px', left: offset.left + 'px' });
    }

    AbsenceSoft.AdvancedSearch.SetCheckBoxes = function () {
        var employers = $('#employers').val();
        if (parseInt($('#page').val()) == 2) {
            $('#as_searchCount').val(parseInt($('#searchcount' + $('#page').val()).val()) + parseInt($('#pageSize').val()));
        }
        else {
            $('#as_searchCount').val(parseInt($('#searchcount' + $('#page').val()).val()));
        }
        $('#lastName').val($('#lastname' + $('#page').val()).val());
        $('#dob').val($('#dob' + $('#page').val()).val());

        var temp = $('#employers' + $('#page').val()).val();
        if (temp) {
            $('#employers').val(temp);
            employers = temp;
        }

        if (employers != null) {
            var empArray = employers.split(';');
            var checkboxes = '';
            for (var i = 0; i < empArray.length; i++) {
                var employer = empArray[i];
                var emp = employer.replace(/[^a-zA-Z0-9_]/g, "").toLowerCase();
                var input = '<div class="checkbox as_chk"><input type="checkbox" id="'
                    + emp + '" value="' + employer + '" class="checkboxlst" checked /><label for="'
                    + emp + '"><span></span> ' + employer + ' </label></div>';
                var checkbox = checkboxes.concat(input);
                checkboxes = checkbox;
            }
            document.getElementById('employersList').innerHTML = checkboxes;
            $('.checkboxlst').change(AbsenceSoft.AdvancedSearch.FilterByCriteria);
        }
    }

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);
