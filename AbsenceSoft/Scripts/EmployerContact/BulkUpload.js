﻿(function (AbsenceSoft, $) {
    var hideUnableToParseFile = function () {
        $('#hideUnableToParseFile').addClass('hide');
    }

    var fileUploaded = function (e, data) {
        hideUnableToParseFile();
        $('#errorUploadingContacts').addClass('hide');
        if (data.files.length < 1)
            return;

        var fileToUpload = data.files[0];
        parseCSVFile(fileToUpload)
    };

    var unableToParseFile = function (error, file) {
        $('#hideUnableToParseFile').removeClass('hide').text(
            'Unable to parse the file. ' + error
            );
        setTimeout(hideUnableToParseFile, 30000)
    }

    var parseCSVFile = function (file) {
        $('#ecBulkUploadMessage')
            .removeClass('hide')
            .find('h4')
            .html('Parsing CSV File...');


        Papa.parse(file, {
            header: true,
            complete: bulkSaveContacts,
            error: unableToParseFile,
            skipEmptyLines: true
        });
    };

    var bulkSaveContacts = function (contacts) {
        $('#ecBulkUploadMessage')
            .find('h4')
            .html('Uploading Contacts...');

        
        if (contacts.errors.length > 10) {
            unableToParseFile();
            return;
        }

        var parsedContacts = contacts.data;
        $.ajax({
            url: '/EmployerContact/BulkUpload',
            data: JSON.stringify({
                Contacts: contacts.data
            }),
            method: 'POST',
            dataType:'json',
            contentType: 'application/json',
            success: contactsUploaded,
            error: failedToUploadContacts
        })
    };

    var contactsUploaded = function (data, textStatus, jqXHR) {
        if (data.Success) {
            $('#ecBulkUploadMessage').addClass('hide');
            noty({ type: 'success', layout: 'topRight', text: data.Message, timeout: 20000 });
            if (data.PartialSuccess) {
                $('#errorUploadingContacts').removeClass('hide');
            }
        } else {
            failedToUploadContacts(data, textStatus, jqXHR);
        }
        
    }

    var failedToUploadContacts = function (data, textStatus, jqXHR) {
        $('#ecBulkUploadMessage').addClass('hide');
        $('#errorUploadingContacts').removeClass('hide');
        noty({ type: 'error', layout: 'topRight', text: data.Message || 'Unable to upload contacts', timeout: 20000 });
    }

    $(document).ready(function () {
        $('#employerContactBulkUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);
    })

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);