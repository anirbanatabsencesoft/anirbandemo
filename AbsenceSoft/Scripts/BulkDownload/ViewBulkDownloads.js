﻿(function (AbsenceSoft, $) {
    AbsenceSoft.BulkDownloads = AbsenceSoft.BulkDownloads || {};

    var pendingDownloads = [];
    var queuePdfDownload = function () {
        queueBulkDownload(1);
    };

    var queueZipDownload = function () {
        queueBulkDownload(2);
    };

    var createNewDownloadRecord = function (downloadType) {
        var downloadTypeName = null;
        if (downloadType === 1) {
            downloadTypeName = "Pdf";
        } else {
            downloadTypeName = "Zip";
        }

        return function (caseExportRecord) {
            var downloadRecord =
                '<div class="col-sm-12">' +
                    '<div class="row bulk-download-row">' +
                        '<input class="bulk-download-id" type="hidden" value="' + caseExportRecord.Id + '"/>' +
                '<div class="col-sm-4">' + moment.utc(caseExportRecord.CreatedDate).local().format('MM/DD/YYYY HH:mm:ss') + '</div>' +
                        '<div class="col-sm-4">' + downloadTypeName + '</div>' +
                        '<div class="col-sm-3 bulk-download-status">Pending</div>' +
                    '</div>' +
                '</div>';
            $('#bulkDownloadList').prepend(downloadRecord);
            pendingDownloads.push(caseExportRecord.Id);
        };
    }

    var queueBulkDownload = function (downloadType) {
        var downloadRequest = {
            CaseId: $('#caseId').val(),
            DownloadType: downloadType
        };

        $.ajax({
            type: 'POST',
            url: '/BulkDownload/CaseBulkDownload',
            data: downloadRequest,
            success: createNewDownloadRecord(downloadType)
        });
    };

    var queuePendingDownloads = function () {
        $('.bulk-download-row').each(function (i, row) {
            var theRow = $(row);
            var status = theRow.find('.bulk-download-status').text().trim();
            if (status === "Pending" || status === "Processing") {
                pendingDownloads.push(theRow.find('.bulk-download-id').val());
            }
        });
    }

    var monitorPendingDownloads = function () {
        if (pendingDownloads.length === 0) {
            return;
        }

        var downloadModel = {
            CaseId: $('#caseId').val(),
            BulkDownloads: []
        };

        for (var i = 0; i < pendingDownloads.length; i++) {
            downloadModel.BulkDownloads.push({
                Id: pendingDownloads[i]
            });
        }

        $.ajax({
            type: 'POST',
            url: '/BulkDownload/StatusRequest',
            data: downloadModel,
            success: updatePendingDownloads
        })
    };

    var updatePendingDownloads = function (pendingDownloadsStatus) {
        for (var i = 0; i < pendingDownloadsStatus.length; i++) {
            var pendingDownload = pendingDownloadsStatus[i];
            switch (pendingDownload.Status) {
                case 0: // pending
                    continue;
                case 1: // Processing
                    setHtml(pendingDownload, 'Processing');
                    break;
                case 2: // Complete
                    var html = 'Complete <a href="/Documents/' + pendingDownload.DocumentId + '/Download"><span class="fa fa-download"></span></a>';
                    setHtml(pendingDownload, html);
                    removeFromPendingDownloads(pendingDownload.Id);
                    break;
                case 3: // Canceled
                    setHtml(pendingDownload, 'Canceled');
                    removeFromPendingDownloads(pendingDownload.Id);
                    break;
                case -1: // failed
                    setHtml(pendingDownload, 'Failed');
                    removeFromPendingDownloads(pendingDownload.Id);
                    break;
            }
        }
    }

    var setHtml = function (pendingDownload, html) {
        $(".bulk-download-id[value='" + pendingDownload.Id + "']").closest('.bulk-download-row').find('.bulk-download-status').html(html);
    }

    var removeFromPendingDownloads = function (id) {
        var index = pendingDownloads.findIndex(function (element) {
            return element === id;
        });

        if (index > -1) {
            pendingDownloads.splice(index, 1);
        }
    }

    $(document).ready(function () {
        $('#newZipDownload').click(queueZipDownload);
        $('#newPdfDownload').click(queuePdfDownload);
        queuePendingDownloads();
        setInterval(monitorPendingDownloads, 1000);

        $('.bulk-created-date').each(function () {
            try {
                var text = $(this).html();
                var serverDate = moment.utc(text).local().format('MM/DD/YYYY HH:mm:ss');

                $(this).html(serverDate);
            }
            catch (ex) {
                console.warn("Error converting date time", ex);
            }
        });

    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);