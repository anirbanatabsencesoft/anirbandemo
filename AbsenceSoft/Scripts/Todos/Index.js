﻿
(function (AbsenceSoft, $) {
    $(document).ready(function () {
        AbsenceSoft.Todos = AbsenceSoft.Todos || {};

        AbsenceSoft.Todos.OnSuccess = function(data)
        {
            window.location.href = data.RedirectUrl;
        }

        AbsenceSoft.Todos.OnFailure = function (data) {
            var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: data.statusText });
        }

        if ($('#paperwork-review-form').length == 0)
            return;

        AbsenceSoft.Todos.OnLoadPage = function () {

            $('.datepicker').datepicker({ autoclose: true });

            $('#paperwork-review-form-fields .required-field input[type=text]').prop('required', true);

            var showForm = false;

            var isError = $('#paperwork-error').html();

            var caseType = $('#paperwork-review-casetype').val();
            if(caseType == 'Intermittent' && ($('#paperwork-review-form-certification .field-validation-error').length > 0 || $('#paperwork-review-form-fields .field-validation-error').length > 0 || isError.length > 0)) {
                $("#paperwork-review-form-certification").show();
                showForm = true;
            }

            var isFields = $('#paperwork-review-fields').val();
            if(isFields == 'true' && ($('#paperwork-review-form-certification .field-validation-error').length > 0 || $('#paperwork-review-form-fields .field-validation-error').length > 0 || isError.length > 0)) {
                $("#paperwork-review-form-fields").show();
                showForm = true;
            }

            if(showForm) {
                $('#paperwork-review-actions').hide();
                $("#paperwork-review-form-section").show();
            }

            $( "#paperwork-review-actions-complete" ).click(function() {  AbsenceSoft.Todos.OnPaperworkAction('complete') });
            $( "#paperwork-review-actions-incomplete" ).click(function() {  AbsenceSoft.Todos.OnPaperworkAction('incomplete') });
            $("#paperwork-review-actions-denied").click(function () { AbsenceSoft.Todos.OnPaperworkAction('denied') });
            $("#paperwork-certificate-on-off-validation").click(function () { AbsenceSoft.Todos.OnCertificateIncomplete() });
            $("#paperwork-certificate-submit").click(function () { return AbsenceSoft.Todos.ValidateDateRange() });
        }

        AbsenceSoft.Todos.OnPaperworkAction = function (actionType) {

            $("#paperwork-action-type").val(actionType);
            $('#paperwork-review-form-actiontype').text(actionType);

            var showForm = false;

            var caseType = $('#paperwork-review-casetype').val();
            var hasCertifications = $('#paperwork-has-certifications').val();
            if (caseType == 'Intermittent' && hasCertifications == 'false') {
                $("#paperwork-review-form-certification").show();
                showForm = true;
            }

            var isFields = $('#paperwork-review-fields').val();
            if (isFields == 'true') {
                $("#paperwork-review-form-fields").show();
                showForm = true;
            }

            if(showForm) {
                $('#paperwork-review-actions').hide();
                $("#paperwork-review-form-section").show();
            } else {
                $( "#paperwork-review-form" ).submit();
            }
        }

        AbsenceSoft.Todos.OnCertificateIncomplete = function(){
            if ($("#paperwork-certificate-on-off-validation").is(':checked')) {
                $('#paperwork-certificate-on-off-validation').attr('value', true);
                $("#paperwork-certificate-submit").click(function (e) {
                    var validator = $(this).closest('form').validate();
                    validator.cancelSubmit = true;
                });
                 
                $("span.error").hide();
                $(".field-validation-error span").hide();
            }
            else {
                $('#paperwork-certificate-on-off-validation').attr('value', false);
                $("#paperwork-certificate-submit").click(function (e) {
                    var validator = $(this).closest('form').validate();
                    validator.cancelSubmit = false;
                });
                $("span.error").show();
                $(".field-validation-error span").show();
            }
        }

        AbsenceSoft.Todos.ValidateDateRange = function () {
            var caseStartDate = $('#CaseStartDate').val();
            var caseEndDate = $('#CaseEndDate').val();
            var certificationStartDate = Date.parse($('#paperwork-review-form-certification-startdate').val());
            var certificationEndDate = Date.parse($('#paperwork-review-form-certification-enddate').val());
            if ((!isNaN(certificationStartDate) && !isNaN(certificationEndDate)) &&
                (certificationStartDate < Date.parse(caseStartDate) || certificationEndDate > Date.parse(caseEndDate)))
                {
                    $('#paperwork-error').html("Error: Cert date range must fall within the date range of the case");
                    return false;
                }
                else
                {
                    $('#paperwork-error').html("");
                    return true;
                }
        }

        AbsenceSoft.Todos.OnLoadPage();
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);