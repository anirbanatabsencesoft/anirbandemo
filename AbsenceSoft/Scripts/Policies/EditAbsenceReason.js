﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditAbsenceReason = AbsenceSoft.EditAbsenceReason || {};

    var setTogglesToMatchAbsenceReason = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    var toggleAbsenceReason = function () {
        var code = $('#absenceReasonCode').val();
        var employerId = $('#EmployerId').val();
        
        $.ajax({
            type: 'POST',
            url: '/AbsenceReasons/ToggleAbsenceReasonSuppression/' + employerId,
            data: {
                Code: code
            },
            success: setTogglesToMatchAbsenceReason
        });
    };

    var toggleNewCategoryVisibility = function () {
        if ($(this).val() == "New Category") {
            $('#newAbsenceReasonCategory').removeClass('hide');
        } else {
            $('#newAbsenceReasonCategory')
                .addClass('hide')
                .find('.form-control')
                .val('');
        }
    }

    var absenceReasonStart = function () {
        if ($('#editAbsenceReasonForm').length < 1)
            return;

        $('#editAbsenceReasonForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('.ar-disabled-toggle').click(toggleAbsenceReason);

        $('#absenceReasonCategory').select2().change(toggleNewCategoryVisibility);
        
    }

    AbsenceSoft.EditAbsenceReason.Start = absenceReasonStart;

    $(document).ready(absenceReasonStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);