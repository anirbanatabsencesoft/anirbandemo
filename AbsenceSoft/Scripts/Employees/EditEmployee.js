﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        

        if ($('#editEmployeeForm').length == 0)
            return;
        
        $('.time-msg').hide();
        $("#editEmployeeForm").submit(function (event) {
            $('.time-msg').hide();
            var totalHours = "0";
            if ($("#fteHours") != undefined && $("#fteHours").val() != undefined) {
                totalHours = $('#fteHours').val();                
            }
            if ($("#Schedule_FteAvgTimePerWeek") != undefined && $("#Schedule_FteAvgTimePerWeek").val() != undefined) {
                totalHours = $('#Schedule_FteAvgTimePerWeek').val();
            }
            totalHours = Number(totalHours.substring(0, totalHours.indexOf('h')));
            if (totalHours > 168) {
                $('.time-msg').show();                
                event.preventDefault();
                return false;
            }
        });

        if ($('#EmployerId') != undefined && $('#EmployerId') != null) {
            $('#EmployerId').change(function () {
                $.ajax({
                    url: '/Employees/PaySchedule/' + $('#EmployerId').val(),
                    success: function (data) {
                        var options = '';
                        $.each(data, function (i) {
                            options = options + '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                        });
                        $('#PayScheduleId').html(options);
                    }
                });
                $.ajax({
                    url: '/Employees/Location/' + $('#EmployerId').val(),
                    success: function (data) {
                        var options = '';
                        $.each(data, function (i) {
                            options = options + '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                        });
                        $('#Organization_Code').html(options);
                    }
                });
                EmployeeJobs();
                EmployeeCustomFields();
                EmployerWeeklyWorkHours();
                EmployeeClasses();
            });
        }
        if ($('#Organization_Code') != undefined && $('#Organization_Code') != null) {
            $('#Organization_Code').change(function () {
                EmployeeJobs();
            });
        }
        if ($('#Id').length > 0 && $('#Id').val() != '' && $('#empCurrentOrganizations').length > 0) {
            $('#empCurrentOrganizations').parents(':eq(1)').removeClass('manage-org-hide-child');
        }
        AfterScheduleEditorRefreshed();
        $(document).on("ScheduleEditorRefreshed", null, AfterScheduleEditorRefreshed);
        jQuery.validator.addMethod("lessThan",
        function (value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value)) && (!/Invalid|NaN/.test(new Date($(params.elem).val())))) {
                return new Date(value) < new Date($(params.elem).val());
            }
            return true;
        }, function (params, element) {
            return params.sourceLabel + ' must be before ' + params.targetLabel;
            });

        bindCurrentOrganizationsExpandCollapseEvent();
        
        $('#empOrgNameSearch').keyup(function () {
            $('#assignEmpOrgMessage').addClass('manage-org-hide-child');
            if ($('#empOrgType').val() == '') {
                $('#orgTypeErrorMessage').removeClass('manage-org-hide-child');
            }
            else {
                $('#orgTypeErrorMessage').addClass('manage-org-hide-child');
                findOrganizations();
            }            
        });
        $('#empOrgType').change(function () {
            $('#assignEmpOrgMessage').addClass('manage-org-hide-child');
            $('#orgTypeErrorMessage').addClass('manage-org-hide-child');
            findOrganizations();
        });
        $('#btnViewAllEmpOrgs').click(function () {
            $('#assignEmpOrgMessage').addClass('manage-org-hide-child');
            orgType = '';
            orgSearchTerm = '';
            GetOrganizationTreeForEmployee();
        });
        $('#btnViewAllEmpOrgs').click(function () {
            orgType = '';
            orgSearchTerm = '';
            GetOrganizationTreeForEmployee();            

        });        
        setTimeout(function () {
            if ($("#DateOfBirth") != undefined && $("#DateOfBirth").val() != undefined && $("#ServiceDate") != undefined && $("#ServiceDate").val() != undefined) {
                $("#DateOfBirth").rules('add', { lessThan: { elem: "#ServiceDate", sourceLabel: 'Date Of Birth', targetLabel: 'Service Date' } });
            }
        }, 100);

        $('.country-editor').change(handleCountryChanged);      
        EmployeeCustomFields();
        //AT-6334-Implemented type-ahead search for job dropdown
        $('select[class*="form-control job-code"]').select2();
        if ($('#EmployeeClassCode') != undefined && $('#EmployeeClassCode') != null) {
            $("#EmployeeClassCode").change(function () {
                EmployeeClassCodeChange();
            });
        }
    });

    var handleCountryChanged = function () {
        var targetRegion = $(this).data('target-region');
        var targettedRegion = $('.' + targetRegion);
        var parentRow = targettedRegion.parents('.row')[0];
        var targettedLabel = $(parentRow).find('.control-label');
        console.log(targettedLabel);
        AbsenceSoft.CountryEditor.GetRegionEditorByCountry($(this).val(), targettedRegion);
        AbsenceSoft.CountryEditor.GetRegionLabelByCountry($(this).val(), targettedLabel);
    }

    function AfterScheduleEditorRefreshed() {
        $('#scheduleEditor div div div').first().attr("class", "col-sm-2 col-wrap");
        $('.fte-schedule-type').first().closest(".col-sm-7").siblings(".col-sm-4").attr("class", "col-sm-2 col-wrap");

        if ($('#Schedule_FTWeeklyWorkTime').length > 0) {
            EmployerWeeklyWorkHours();
        }
        $('#Schedule_FteTimePercentage').blur(function () {            
            HourPercentageCalculation();
        });
        if ($('#Schedule_FteTimePercentage').length > 0 && $('#Schedule_FteTimePercentage').val() > 0) {
            if ($('#fteHours').val() === null || $('#fteHours').val() === '' || $('#fteHours').val().length <= 0) {
                HourPercentageCalculation();                                
            }
        }
        $('.time-msg').hide();
    }
    function EmployeeClassCodeChange() {
        var selTypeText = $("#EmployeeClassCode option:selected").text();
        $("#EmployeeClassName").val(selTypeText);
    }
    function HourPercentageCalculation() {
        var totalHours = $('#Schedule_FTWeeklyWorkTime').val();
        totalHours = Number(totalHours.substring(0, totalHours.indexOf('h')));
        var percentage = $('#Schedule_FteTimePercentage').val();
        var timeValue = totalHours * (percentage / 100.0);
        $('#fteHours').val(timeValue.toFixed(2));
        $('#fteHours').blur();
    }

    function EmployerWeeklyWorkHours() {
        if ($('#Schedule_FTWeeklyWorkTime').length > 0) {
            $.ajax({
                url: '/Employees/EmployerWeeklyWorkHours/' + $('#EmployerId').val(),
                success: function (data) {
                    $('#Schedule_FTWeeklyWorkTime').val(data);
                    $('#Schedule_FTWeeklyWorkTime').blur();
                }
            });
        }
    }

    function EmployeeJobs() {
        $.ajax({
            url: '/Employees/Jobs/' + $('#EmployerId').val(),
            data: { officeLocation: $('#Organization_Code').val() },
            success: function (data) {
                var options = '';
                $.each(data, function (i) {
                    options = options + '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                });
                $('#Job_Code').html(options);
            }
        })
    }

    function EmployeeCustomFields() {        
        if ($('#EmployerId').val() == undefined || $('#EmployerId').val() == "")
            return;

        if ($('.validation-summary-errors').html())
            return;

        $.ajax({
            url: '/Employees/GetCustomFields/' + $('#EmployerId').val() + '/' + $('#Id').val(),
            success: function (data) {
                $('#EmployeeCustomFields').html(data);
                var datePickerConfiguration = {
                    orientation: "top auto",
                    autoclose: true
                };
                $('.input-group.date.editor').datepicker(datePickerConfiguration);
                $('#editEmployeeForm').removeData("validator");
                $('#editEmployeeForm').removeData("unobtrusiveValidation");
                $.validator.unobtrusive.parse($('#editEmployeeForm'));
            }
        });
    }

    function EmployeeClasses() {
        if ($('#EmployerId').val() == undefined || $('#EmployerId').val() == "")
            return;
        if ($('.validation-summary-errors').html())
            return;
        $.ajax({
            url: '/Employees/GetEmployeeClass/' + $('#EmployerId').val(),
            success: function (data) {
                $('#EmployeeClassInfo').html(data);
                $('#editEmployeeForm').removeData("validator");
                $('#editEmployeeForm').removeData("unobtrusiveValidation");
                $.validator.unobtrusive.parse($('#editEmployeeForm'));
                $("#EmployeeClassCode").change(function () {
                    EmployeeClassCodeChange();
                });
            }
        });
        
    }

    var employeeCurrentOrganizations = function () {
        $.ajax({
            async: true,
            url: '/Employees/' + $('#Id').val() + '/EmployeeCurrentOrganizations',
            contentType: 'application/json',
            method: 'GET',
            success: function (data) {
                $('#empCurrentOrganizations').html(data);
                bindCurrentOrganizationsExpandCollapseEvent();
            },
            error: function (message) {
                alert(JSON.stringify(message));
            }
        });
    }
    var orgType = '';
    var orgSearchTerm = '';
    var findOrganizations = function () {        
        if ($('#empOrgType').val() != orgType || $('#empOrgNameSearch').val() != orgSearchTerm)
        {
            orgType = $('#empOrgType').val();
            orgSearchTerm = $('#empOrgNameSearch').val();
            GetOrganizationTreeForEmployee();
        }
    }
    var GetOrganizationTreeForEmployee = function () {
        $.ajax({
            url: '/Organization/' + $('#Id').val() + '/GetOrganizationTreeForEmployee',
            data: { organizationType: orgType, searchTerm: orgSearchTerm },
            success: function (data) {
                $('#empOrgResults').html(data);
                bindAssignOrganizationsExpandCollapseEvent();
            }
        });
    }
    var bindCurrentOrganizationsExpandCollapseEvent = function () {
        $(".emp-org-expand").each(function () {
            $(this).click(function () {
                $(this).siblings('.emp-org-collapse').addClass('glyphicon');
                $(this).siblings('.emp-org-collapse').addClass('glyphicon-minus-sign');
                $(this).removeClass('glyphicon');
                $(this).removeClass('glyphicon-plus-sign');
                $(this).parent().siblings('.emp-org-child').removeClass('manage-org-hide-child');
            });
        });
        $(".emp-org-collapse").each(function () {
            $(this).click(function () {
                $(this).removeClass('glyphicon');
                $(this).removeClass('glyphicon-minus-sign');
                $(this).siblings('.emp-org-expand').addClass('glyphicon');
                $(this).siblings('.emp-org-expand').addClass('glyphicon-plus-sign');
                $(this).parent().siblings('.emp-org-child').addClass('manage-org-hide-child');
            });
        });
        $('.level1-org-load').click(function () {
            orgSearchTerm = $(this).parents(':eq(1)').siblings('.hdn-current-emp-org-name').val();
            orgType = $(this).parents(':eq(1)').siblings('.hdn-current-emp-org-type-code').val();
            GetOrganizationTreeForEmployee();
            $('#orgAssignment').removeClass('collapse');
            $('#orgAssignment').addClass('in');
        });
    }
    var bindAssignOrganizationsExpandCollapseEvent = function () {
        $(".assign-emp-org-expand").each(function () {
            $(this).click(function () {
                $(this).siblings('.assign-emp-org-collapse').addClass('glyphicon');
                $(this).siblings('.assign-emp-org-collapse').addClass('glyphicon-minus-sign');
                $(this).removeClass('glyphicon');
                $(this).removeClass('glyphicon-plus-sign');
                $(this).parents(':eq(1)').siblings('.assign-org-child-tree').removeClass('manage-org-hide-child');
            });
        });
        $(".assign-emp-org-collapse").each(function () {
            $(this).click(function () {
                $(this).removeClass('glyphicon');
                $(this).removeClass('glyphicon-minus-sign');
                $(this).siblings('.assign-emp-org-expand').addClass('glyphicon');
                $(this).siblings('.assign-emp-org-expand').addClass('glyphicon-plus-sign');
                $(this).parents(':eq(1)').siblings('.assign-org-child-tree').addClass('manage-org-hide-child');
            });
        });
        $("#btnResetEmpOrgVisibility").click(function () {
            $(".emp-org-affiliation[value='-1']").each(function () {
                $(this).prop("checked", true);
            })
        });
        $("#btnSaveEmpOrgVisibility").click(function () {
            var model = { OrganizationVisibilityTrees: [] };
            $('.emp-org-affiliation').each(function () {
                if ($(this).is(":checked")) {
                    var orgCode = $(this).parents(':eq(1)').siblings('.hdn-assign-emp-org-code').val();
                    var orgName = $(this).parents(':eq(1)').siblings('.hdn-assign-emp-org-name').val();
                    var orgPath = $(this).parents(':eq(1)').siblings('.hdn-assign-emp-org-path').val();
                    var orgTypeCode = $(this).parents(':eq(1)').siblings('.hdn-assign-emp-org-type-code').val();
                    var orgAffiliation = $(this).val();
                    model.OrganizationVisibilityTrees.push({
                        EmployerId: $('#EmployerId').val(),
                        OrganizationCode: orgCode,
                        OrganizationName: orgName,
                        OrganizationPath: orgPath,
                        OrganizationTypeCode: orgTypeCode,
                        Affiliation: orgAffiliation
                    });
                }
            });
            if (model.OrganizationVisibilityTrees.length > 0) {
                $.ajax({
                    async: true,
                    url: '/Organization/' + $('#Id').val() + '/SaveOrganizationVisibilityForEmployee',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify(model),
                    success: saveOrgAssignmentSuccess,
                    error: function (message) {
                        alert(JSON.stringify(message));
                    }
                });
            }
        });
    }
    var saveOrgAssignmentSuccess = function (data) {
        if (data.Success) {
            $('#assignEmpOrgMessage').removeClass('manage-org-hide-child');
            $('#assignEmpOrgMessage').html('Organization Visibility successfully assigned to Employee <strong>' + $('#FirstName').val() + ", " + $('#LastName').val());
            //AbsenceSoft.Noty({ type: 'success', text: 'Organization Visibility successfully assigned to Employee <strong>' + $('#FirstName') + ", " + $('#LastName') }, '#assignEmpOrgMessage');
            employeeCurrentOrganizations();
            setTimeout(function () {
                $('#assignEmpOrgMessage').html('');
                $('#assignEmpOrgMessage').addClass('manage-org-hide-child');
            }, 5000);
        }
    }

   

    var success = function (data) {
        AbsenceSoft.PageStart('#editEmployeeForm');
    };

    var complete = function (data) {
        AbsenceSoft.PageStart('#editEmployeeForm');
    };

    var error = function (data) {
        alert("Failed");
        AbsenceSoft.PageStart('#editEmployeeForm');
    };
    AbsenceSoft.Employee = AbsenceSoft.Employee || {};
    AbsenceSoft.Employee.Success = success;
    AbsenceSoft.Employee.Complete = complete;
    AbsenceSoft.Employee.Error = error;
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);