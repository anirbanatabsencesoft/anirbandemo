﻿google.load('visualization', '1.0', { 'packages': ['corechart'] });

$(function () {
    if ($('#dashboard-charts').length > 0) {
        $('#team-thingy-doodad').on('change', function () {
            var v = $(this).find('select').val() || 'Individual';
            loadDashboardCharts(v);
        });
        google.setOnLoadCallback(loadDashboardCharts('Individual'));
    }
});

function loadDashboardCharts(viewType) {
    loadChart(viewType, 'case-load-chart', 'Case Manager', 'Cases', 'Case Load', '/Chart/GetCaseLoadData');
    loadChart(viewType, 'open-todos-chart', 'Case Manager', 'ToDos', 'Open ToDos', '/Chart/GetOpenToDosData');
    loadChart(viewType, 'overdue-todos-chart', 'ToDo Type', 'Cases', 'Overdue ToDos', '/Chart/GetOverdueToDosData');
}

function loadChart(viewType, containerDivId, firstColumn, secondColumn, title, chartUrl) {
    var param = { 'viewType': viewType };
    $.ajax({
        type: 'GET',
        url: chartUrl,
        data: param,
        success: function (data) {
            var chartData = new google.visualization.DataTable();
            chartData.addColumn('string', firstColumn);
            chartData.addColumn('number', secondColumn);
            chartData.addRows(data);
            var chart = new google.visualization.PieChart(document.getElementById(containerDivId));
            chart.draw(chartData, { 'title': title });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var msg = "Unable to load case load chart: " + errorThrown;
            document.getElementById(containerDivId).html(msg).show();
        }
    });
}