﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCommunications = AbsenceSoft.EditCommunications || {};

    var removeFileUpload = function () {
        $('#uploadedTemplate').addClass('hide');
        $('#currentCommunicationTemplate').removeClass('hide');
        $('#templateName').html('');
        $('#communicationTemplateUploadFile').val('');
        $('#editCommunicationTemplateForm').trigger('checkform.areYouSure');
    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        $('#uploadedTemplate').removeClass('hide');
        $('#currentCommunicationTemplate').addClass('hide');
        $('#templateName').html(uploadedFile.name);
    }

    //set the styles for enable/disable buttons to make active
    var setToggleEnableDisable = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    //save the data
    var toggleCommunicationEnableDisable = function () {
        var id = $('#templateId').val();
        var employerId = $('#employerId').val();

        $.ajax({
            method: 'POST',
            url: '/Templates/ToggleEnablingCommunication/' + employerId,
            data: {
                Id: id,
                EmployerId: employerId
            },
            success: setToggleEnableDisable
        });
    };


    var communicationTemplateStart = function () {
        $('#editCommunicationTemplateForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#communicationTemplateUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelCommunicationTemplateUpload').click(removeFileUpload);

        $('#selectedPaperwork').select2();

        //set method for enable/disable toggle
        $('.comm-disabled-toggle').click(toggleCommunicationEnableDisable);
    }

    AbsenceSoft.EditCommunications.Start = communicationTemplateStart;
    if (location.href.indexOf('communication') > 0) {
        $(document).ready(communicationTemplateStart);
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);