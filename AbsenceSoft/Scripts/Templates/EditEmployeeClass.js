﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditEmployeeClasses = AbsenceSoft.EditEmployeeClasses || {};

    //set the styles for enable/disable buttons to make active
    var setToggleEnableDisable = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    //save the data
    var toggleEmployeeClassEnableDisable = function () {
        var code = $('#EmployeeClassCode').val();
        var employerId = $('#employerId').val();

        $.ajax({
            method: 'POST',
            url: '/EmployeeClass/ToggleEmployeeClassSuppression/' + employerId,
            data: {
                Code: code,
                EmployerId: employerId
            },
            success: setToggleEnableDisable
        });
    };

    var EmployeeClassTemplateStart = function () {

        $('#editEmployeeClassForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);
        
        //set method for enable/disable toggle
        $('.ar-disabled-toggle').click(toggleEmployeeClassEnableDisable);
    }

    var valueChanged = function (IsChecked, elementId) {
        if (IsChecked) {
            $('#' + elementId).removeClass('hide')
        }
        else {
            $('#' + elementId).addClass('hide');
        }
    }

    AbsenceSoft.EditEmployeeClasses.Start = EmployeeClassTemplateStart;
    if (location.href.indexOf('employeeclass') > 0) {
        $(document).ready(EmployeeClassTemplateStart);
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);