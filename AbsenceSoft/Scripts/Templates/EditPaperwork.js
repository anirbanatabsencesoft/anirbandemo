﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditPaperwork = AbsenceSoft.EditPaperwork || {};
    var areYouSureConfiguration = {
        silent: true,
        addRemoveFieldsMarksDirty:true
    };

    var updateAreYouSure = function () {
        $('#editPaperworkTemplateForm').trigger('rescan.areYouSure');
    }

    var removeFileUpload = function () {
        $('#uploadedPaperwork').addClass('hide');
        $('#currentPaperworkTemplate').removeClass('hide');
        $('#templateName').html('');
        $('#editPaperworkTemplateForm').trigger('checkform.areYouSure');
    }

    var fileUploaded = function (e, data) {
        if (data.files.length != 1)
            return;

        var uploadedFile = data.files[0];
        $('#uploadedPaperwork').removeClass('hide');
        $('#currentPaperworkTemplate').addClass('hide');
        $('#templateName').html(uploadedFile.name);
    }

    var addNewRule = function () {
        var clonedElement = $('#rule').html();
        var repeatingContainer = $(this).parents('.absencesoft-repeating');
        var ruleContainer = repeatingContainer.find('.rule-container');
        ruleContainer.find('.no-rules').hide();
        ruleContainer.append(clonedElement);
        repeatingContainer.find('.collapse').collapse('show');
        AbsenceSoft.PageStart(clonedElement);
        bindRemoveRule();
        updateAreYouSure();
        return false;
    }

    var removeRule = function () {
        $(this).parents('.rule-row').remove();
        updateAreYouSure();
        var isRuleRowExists = $('#rule').find('.rule-row');
        if (!$('.rule-row').length) {
            $('.rule-container p:first').show();
        }
        return false;
    }

    var ruleGroupAdded = function (e, newElement) {
        var newId = AbsenceSoft.GenerateGuid();
        $('.collapse', newElement).attr('id', newId);
        $('.rule-toggle', newElement).attr('href', '#' + newId);
        updateAreYouSure();
        bindAddRule();

        return false;
    }

    var bindAddRule = function () {
        $('.add-child-section').unbind('click').click(addNewRule);
    }

    var bindRemoveRule = function () {
        $('.remove-child-section').unbind('click').click(removeRule);
     }

    var renameCriteriaFields = function () {
        $('.admin-rule-group').each(function (i, ruleGroup) {
            $(ruleGroup).find('.rule-row').each(function (j, rule) {
                var jRule = $(rule);
                jRule.find('.criteria-type').attr('name', 'Criteria[' + i + '].Criteria[' + j + '].CriteriaType');
                jRule.find('.criteria-value').attr('name', 'Criteria[' + i + '].Criteria[' + j + '].Criteria');
            });
        });

    }

    var paperworkTemplateStart = function () {
        if ($('#editPaperworkTemplateForm').length < 1)
            return;

        $('#editPaperworkTemplateForm').areYouSure(areYouSureConfiguration)
            .on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields)
            .submit(renameCriteriaFields);

        $('#paperworkTemplateUploadFile').fileupload({
            autoUpload: false,
            singleFileUploads: true,
            replaceFileInput: false
        }).on('fileuploadadd', fileUploaded);

        $('#cancelPaperworkTemplateUpload').click(removeFileUpload);
        bindAddRule();
        bindRemoveRule();
        $(document).on('as.repeater.add', ruleGroupAdded);
        $(document).on('as.repeater.remove', updateAreYouSure);
    }

    AbsenceSoft.EditPaperwork.Start = paperworkTemplateStart;

    $(document).ready(paperworkTemplateStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);