﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditDenialReasons = AbsenceSoft.EditDenialReasons || {};

    //set the styles for enable/disable buttons to make active
    var setToggleEnableDisable = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the wf is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    //save the data
    var toggleDenialReasonEnableDisable = function () {
        var code = $('#DenialReasonCode').val();
        var employerId = $('#employerId').val();

        $.ajax({
            method: 'POST',
            url: '/DenialReason/Toggle/' + employerId,
            data: {
                Code: code,
                EmployerId: employerId
            },
            success: setToggleEnableDisable
        });
    };

    var DenialReasonTemplateStart = function () {

        $('#editDenialReasonForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('#selectedAbsenceReasons').select2();
        $('#selectedPolicyType').select2();
        $('#selectedPolicyCode').select2();
        $('#selectedAccommodationType').select2();

        if ($('#checkIsPolicy').is(":checked")) {
            valueChanged(true, 'divPolicyType');
            valueChanged(true, 'divPolicyCode');
        }
        if ($('#checkIsAccommodation').is(":checked")) {
            valueChanged(true, 'divAccommodationType')
        }

        $('#checkIsPolicy').change(function () {
            if ($(this).is(":checked")) {
                valueChanged(true, 'divPolicyType');
                valueChanged(true, 'divPolicyCode');
            }
            else {
                valueChanged(false, 'divPolicyType');
                valueChanged(false, 'divPolicyCode');
                $('#selectedPolicyType').each(function () { 
                    $(this).select2('val', '')
                });
                $('#selectedPolicyCode').each(function () { 
                    $(this).select2('val', '')
                });
            }
        });

        $('#checkIsAccommodation').change(function () {
            if ($(this).is(":checked")) {
                valueChanged(true, 'divAccommodationType');
            }
            else {
                valueChanged(false, 'divAccommodationType');
                $('#selectedAccommodationType').each(function () {
                    $(this).select2('val', '')
                });
            }
        });

        //set method for enable/disable toggle
        $('.ar-disabled-toggle').click(toggleDenialReasonEnableDisable);
    }

    var valueChanged = function (IsChecked, elementId) {
        if (IsChecked) {
            $('#' + elementId).removeClass('hide')
        }
        else {
            $('#' + elementId).addClass('hide');
        }
    }

    AbsenceSoft.EditDenialReasons.Start = DenialReasonTemplateStart;
    if (location.href.indexOf('denialreason') > 0) {
        $(document).ready(DenialReasonTemplateStart);
    }
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);