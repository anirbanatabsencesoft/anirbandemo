﻿(function (AbsenceSoft, $) {
    AbsenceSoft.ViewCaseCategories = AbsenceSoft.ViewCaseCategories || {};

    var searchCaseCategories = function () {
        var searchTerm = $(this).val().trim().toLowerCase();
        if (!searchTerm) {
            $('.note-category-name').removeClass('hide');
            } else {
            $('.note-category-name').each(filterCaseCategories(searchTerm));
        }
    };

    $(document).ready(function () {
       
        $('#searchCaseCategories').keyup(searchCaseCategories);
      
    });

})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);