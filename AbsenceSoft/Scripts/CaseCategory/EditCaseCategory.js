﻿(function (AbsenceSoft, $) {
    AbsenceSoft.EditCaseCategory = AbsenceSoft.EditCaseCategory || {};

    var setTogglesToMatchCaseCategory = function (data) {
        if (data.Disabled && !$('.disabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.disabled-button').addClass('active');
            $('.enabled-button').removeClass('active');
        } else if (!data.Disabled && !$('.enabled-button').hasClass('active')) {
            /// The confirmation says the accommodation type is disabled, but the UI does not reflect this
            $('.enabled-button').addClass('active');
            $('.disabled-button').removeClass('active');
        }
    };

    var toggleCaseCategory = function () {
        var id = $('#caseCategoryId').val();
        var employerId = $('#employerId').val();

        $.ajax({
            type: 'POST',
            url: '/CaseCategory/ToggleCaseCategorySuppression/' + employerId,
            data: {
                Id: id,
                EmployerId: employerId
            },
            success: setTogglesToMatchCaseCategory
        });
    };

    var editCaseCategoryStart = function () {
        var MakeSelect2 = function () {
            $("select.absence-reason-values").each(function () {
                $(this).select2();
            });
        }
        if ($('#editCaseCategoryForm').length < 1)
            return;

        $('#editCaseCategoryForm').areYouSure({
            silent: true
        }).on('dirty.areYouSure', AbsenceSoft.Admin.ShowDirtyFields)
            .on('clean.areYouSure', AbsenceSoft.Admin.HideDirtyFields);

        $('.ar-disabled-toggle').click(toggleCaseCategory);
        $('#editCaseCategoryRoles').select2();
        if (jQuery.type($('#editform').val()) === "undefined") {
            $(":input").prop("disabled", true);
        }

        AbsenceSoft.PageStart('#editCaseCategoryForm');
    }

    AbsenceSoft.EditCaseCategory.Start = editCaseCategoryStart;

    $(document).ready(editCaseCategoryStart);
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);