﻿(function (AbsenceSoft, $) {
    var checkHasConsult = function () {
        var consultationComments = $(this);
        if (consultationComments.val().trim() != '') {
            consultationComments.parents('.wre-returns').find('.has-consult').attr('checked', true);
        }
    }

    var checkConsultationComments = function () {
        var hasConsult = $(this);
        if (hasConsult.is(':checked')){
            hasConsult.parents('.wre-returns').find('.consultation-comments').attr('required', 'required');
        } else {
            hasConsult.parents('.wre-returns').find('.consultation-comments').prop('required', false);
        }
        hasConsult.parents('.wre-returns').find('.consultation-comments').attr('disabled', !hasConsult.is(':checked'));
    }

    var searchConsultations = function () {
        var searchTerm = $(this).val().toLowerCase();
        var consultations = $('.consultation-name');
        for (var i = 0; i < consultations.length; i++) {
            var restriction = $(consultations[i]);
            if (restriction.val().toLowerCase().indexOf(searchTerm) > -1) {
                restriction.parents('.wre-returns').removeClass('hide');
            } else {
                restriction.parents('.wre-returns').addClass('hide');
            }
        }
    }

    

    $(document).ready(function () {
        $('#consultationSearch').keyup(searchConsultations);
        $('.consultation-comments').change(checkHasConsult);
        $('.has-consult').change(checkConsultationComments);
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);