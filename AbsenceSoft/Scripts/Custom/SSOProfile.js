﻿(function (AbsenceSoft, $) {
    $(document).ready(function () {
        if ($('#btnSaveProfile') != undefined && $('#btnSaveProfile') != null && $('#btnSaveProfile').length > 0) {
            $('#btnSaveProfile').click(function () {
                $.ajax({
                    url: '/SSOProfile/Save',
                    type: "POST",
                    data: { CustomerId: $('#hdnCustomer').val(), Message: GetActiveEditorContent() },
                    success: function (data) {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'success', text: "SSO message saved successfully" });
                    },
                    error: function ()
                    {
                        var n = noty({ timeout: 10000, layout: 'topRight', type: 'error', text: "Unable to save SSO message" });
                    }
                });                
            });

            tinymce.init({
                height: 350,
                menubar: false,                
                selector: 'textarea', setup: function (ed) {
                    ed.on('init', function (args) {
                        GetActiveEditorContent = function () {
                            return tinyMCE.activeEditor.getContent();
                        };
                    });
                },                
                init_instance_callback: function (editor) {
                    //call load html after initialization
                    //CommunicationsLoadHtmlFile();
                },
                content_css: contentCss,
                // Bad, shameful, horribly behaved tinyMCE should not convert our fully qualified URLs to relative
                //  because it then breaks PDF and Email generation 'n' stuff.
                relative_urls: false,
                remove_script_host: false,
                convert_urls: true,
                theme_url: themeUrl,
                skin_url: skinUrl,                
                plugins: "textcolor",
                toolbar1: 'undo redo | styleselect | bold italic underline | ' +
                    'alignleft aligncenter alignright alignjustify | ' +
                    'bullist numlist | ' +
                    'outdent indent | ' +
                    'forecolor backcolor'                
            });
        }
    });
})(window.AbsenceSoft = window.AbsenceSoft || {}, jQuery);