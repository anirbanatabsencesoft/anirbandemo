﻿$(function () {    
        if ($('.js-focus-first-empty').length === 0)
            return;
        var toggleInput = function () {
            var container = $(this).parents('.eye-toggle-container');
            var input = container.find(':input');
            var newInputType = 'password'
            if (input.attr('type') == 'password')
                newInputType = 'text'

            container.find(':input').attr({
                type: newInputType,
                autocomplete: 'off'
            });
            container.find('.glyphicon-eye-close').toggle();
            container.find('.glyphicon-eye-open').toggle();
        };
        $('.glyphicon-eye-close').attr('tooltip', 'Click To Show Password');
        $('.glyphicon-eye-open').attr('tooltip', 'Click To Show Password');
        $('.glyphicon-eye-close').attr('style', 'cursor:pointer');
        $('.glyphicon-eye-open').attr('style', 'cursor:pointer');
        $('.glyphicon-eye-close').mouseup(toggleInput).hide();
        $('.glyphicon-eye-open').mousedown(toggleInput);    
});