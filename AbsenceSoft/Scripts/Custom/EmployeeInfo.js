﻿$(function () {

    //Toggle "more"/"less"
    $("#showMore").on("click", function () {
        if ($(this).html() == 'more') {
            $(this).html('less');
        }
        else {
            $(this).html('more');
        }
    });
});
