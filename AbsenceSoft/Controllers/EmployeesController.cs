﻿using System.Web.Helpers;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Common;
using AbsenceSoft.Data;
using AbsenceSoft.Data.Audit;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models;
using AbsenceSoft.Models.Employees;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Models.Cases;
using AbsenceSoft.Data.Cases;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Tasks;
using System.Reflection;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Data.Jobs;
using AbsenceSoft.Logic.Jobs;
using AbsenceSoft.Logic.Necessitys;
using AbsenceSoft.Models.RiskProfiles;
using AbsenceSoft.Models.CustomFields;
using Newtonsoft.Json;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure]
    public class EmployeesController : BaseController
    {
        //
        // GET: /Employee/
        [Title("Employees")]
        [Route("Employees", Name = "Employees")]
        [Secure("EditEmployee", "ViewEmployee")]
        public ViewResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public PartialViewResult ParentEmployeeInfo(string employeeId)
        {
            if (String.IsNullOrEmpty(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            var employee = Employee.GetById(employeeId);
            if (employee == null)
            {
                throw new ArgumentOutOfRangeException("employeeId", employeeId, "Employee not found.");
            }

            var model = new EmployeeParentedModel()
            {
                EmployeeId = employeeId,
                EmployeeNumber = employee.EmployeeNumber,
                EmployeeFirstName = employee.FirstName,
                EmployeeLastName = employee.LastName,
            };

            return PartialView("_ParentEmployeeInfo", model);
        }

        /// <summary>
        /// Gets a list of employees in JSON for display on the dashboard or All employees page.
        /// </summary>
        /// <param name="nameFilter">The employee's full name as a filter for either first or last (begins with query)</param>
        /// <param name="lastNameFilter">The employee's last name or the beginning part of (begins with query)</param>
        /// <param name="firstNameFilter">The employee's first name or the beginning part of (begins with query)</param>
        /// <param name="employeeNumberFilter">The employee id to seach by (contains query)</param>
        /// <param name="sortBy">The field name to sort by, plain text</param>
        /// <param name="sort">Either 1 (asc) or -1 (desc) for the sort order for the sort field</param>
        /// <param name="page">The page number that the user is on in the paging/scrolling of results</param>
        /// <param name="size">The size of each page</param>
        /// <returns></returns>
        [Secure("EditEmployee", "ViewMyPeerInfo", "ViewEmployee"), HttpPost]
        [Route("Employees/List", Name = "EmployeesList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult List(ListCriteria criteria)
        {
            try
            {
                using (var service = new EmployeeService())
                {
                    var results = service.EmployeeList(criteria);
                    return Json(results);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employee list");
            }
        }//End: Employees

        [Secure("EditEmployee", "ViewMyPeerInfo"), HttpPost]
        [Route("Employees/LastViewedList", Name = "EmployeesLastViewedList")]
        [ValidateApiAntiForgeryToken]
        public JsonResult LastViewedList(ListCriteria criteria)
        {
            try
            {
                using (var service = new EmployeeService())
                {
                    var results = service.EmployeeLastViewedList(criteria);
                    return Json(results);
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting latest employees viewed list");
            }
        }

        [Secure("EditEmployee"), HttpGet]
        public JsonResult EmployeeQuickFind(string searchTerm, string employerId, bool? eeNum)
        {
            ListCriteria criteria = new ListCriteria();
            criteria.Set("text", searchTerm);
            criteria.Set("EmployerId", employerId);
            if (eeNum.HasValue)
                criteria.Set("eeNum", eeNum.Value);

            try
            {
                using (var service = new EmployeeService())
                {
                    var data = service.EmployeeQuickFind(criteria);
                    var results = data.Results;
                    if (data.Results != null && data.Results.Any())
                    {
                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { id = "", text = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Error getting employee quick-find list");
            }
        }

        #region Refactored methods

        [Title("Employee"), Secure("EditEmployee", "ViewEmployee")]
        [HttpGet, Route("Employees/Edit/{employeeId?}", Name = "EditEmployee")]
        public ViewResult EditEmployee(string employeeId)
        {
            if (!string.IsNullOrEmpty(employeeId))
                return GetEmployeeInfo(employeeId, true);

            using (var employerService = new EmployerService())
            {
                EmployeeViewModel emp = new EmployeeViewModel()
                {
                    CustomerId = CurrentCustomer.Id,
                    EmployeeCustomFields = employerService.GetCustomFields(EntityTarget.Employee, true).Select(cf => new CustomFieldViewModel(cf)).ToList()
                };
                emp.Schedule.BuildTimesLists();
                List<Employer> employers = employerService.GetEmployersForCustomer(CurrentCustomer.Id);
                if (employers.Count == 1)
                {
                    emp.EmployerId = employers.First().Id;
                    emp.Job.EmployerId = emp.EmployerId;
                }

                using (var employeeService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    emp.EmployeeClasses = employeeService.GetAllEmployeeClasses();
                }

                return View(emp);
            }

        }

        [Title("Employee"), Secure("EditEmployee", "ViewEmployee")]
        [HttpGet, Route("Employees/{employeeId}/EmployeeCurrentOrganizations", Name = "EmployeeCurrentOrganizations")]
        public ActionResult EmployeeCurrentOrganizations(string employeeId)
        {
            using (var service = new OrganizationService(CurrentUser))
            {
                List<OrganizationVisibilityTree> tree = service.GetCurrentOrganizationTreeForEmployee(Employee.GetById(employeeId));
                return PartialView("~/Views/Shared/DisplayTemplates/EmployeeCurrentOrganizations.cshtml", tree);
            }
        }

        [Title("Employee"), Secure("EditEmployee", "ViewEmployee")]
        [HttpPost, Route("Employees/Edit/{employeeId?}")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EditEmployee(EmployeeViewModel employee, string backURL)
        {
            if (!string.IsNullOrWhiteSpace(employee.EmployerId) && string.IsNullOrWhiteSpace(employee.Job.EmployerId))
            {
                employee.Job.EmployerId = employee.EmployerId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var empService = new EmployeeService())
                    using (var orgService = new OrganizationService(CurrentUser))
                    using (var jobService = new JobService(employee.CustomerId, employee.EmployerId, CurrentUser))
                    {
                        Employee emp = new Employee();
                        if (!string.IsNullOrWhiteSpace(employee.Id))
                            emp = Employee.GetById(employee.Id);
                        emp = employee.ApplyToDataModel(emp);

                        var uiSchedule = employee.Schedule;
                        Schedule workSchedule = null;
                        var totalTimes = uiSchedule.Times.Sum(t => !string.IsNullOrEmpty(t.TotalMinutes) ? t.TotalMinutes.ParseFriendlyTime() : 0);
                        
                        if (totalTimes > 0 || uiSchedule.ScheduleType == ScheduleType.FteVariable)
                        {
                            workSchedule = emp.WorkSchedules.FirstOrDefault(t => t.StartDate == uiSchedule.StartDate);
                            if (workSchedule == null)
                                workSchedule = new Schedule();
                            workSchedule = uiSchedule.ApplyToDataModel(workSchedule);
                        }
                        List<VariableScheduleTime> appliedTimes = new List<VariableScheduleTime>();
                        if (uiSchedule.TemporaryActualScheduleValuesInString != null && uiSchedule.ScheduleType == ScheduleType.Variable)
                        {
                            uiSchedule.TemporaryActualTimes = JsonConvert.DeserializeObject<List<TimeViewModel>>(uiSchedule.TemporaryActualScheduleValuesInString);
                            foreach (var tempTime in uiSchedule.TemporaryActualTimes)
                            {
                                var matchingDate = uiSchedule.ActualTimes.FirstOrDefault(t => t.SampleDate == tempTime.SampleDate);
                                if (matchingDate == null)
                                {
                                    uiSchedule.ActualTimes.Add(new TimeViewModel()
                                    {
                                        SampleDate = tempTime.SampleDate,
                                        TotalMinutes = tempTime.TotalMinutes

                                    });
                                }
                                else
                                {
                                    matchingDate.TotalMinutes = tempTime.TotalMinutes;
                                }
                            }

                        }
                        if (uiSchedule.ActualTimes.Count > 0 && uiSchedule.ScheduleType == ScheduleType.Variable)
                        {
                            DateTime startDate = uiSchedule.ActualTimes.Min(t => t.SampleDate);
                            DateTime endDate = uiSchedule.ActualTimes.Max(t => t.SampleDate);
                            if (!string.IsNullOrWhiteSpace(emp.Id))
                            {
                                appliedTimes = empService.GetEmployeeVariableSchedule(emp.Id, startDate, endDate);
                            }

                            appliedTimes = uiSchedule.ApplyActualTimesToDataModel(appliedTimes);
                        }
                        if (uiSchedule.ScheduleType == ScheduleType.FteVariable && emp.StartDayOfWeek == null)
                        {
                            emp.StartDayOfWeek = DayOfWeek.Monday;
                        }
                        bool locationSaved = false;
                        if (!string.IsNullOrWhiteSpace(emp.Id) && string.Equals((employee.OriginalEmployeeNumber ?? string.Empty).Trim(),
                                (employee.EmployeeNumber.Trim() ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase))
                        {
                            UpdateEmployeeLocation(employee, orgService, emp);
                            locationSaved = true;
                        }

                        /* Update previously added jobs */
                        if (employee.JobHistory != null)
                        {
                            // iterate through job history
                            foreach (var job in employee.JobHistory)
                            {
                                if (!string.IsNullOrWhiteSpace(emp.Id) && string.Equals((employee.OriginalEmployeeNumber ?? string.Empty).Trim()
                                        , (employee.EmployeeNumber.Trim() ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase))
                                {
                                    // update employee job detail
                                    UpdateEmployeeJobHistory(employee, job, jobService, emp);
                                }
                            }
                        }

                        bool employeeJobSaved = false;
                        if (!string.IsNullOrWhiteSpace(emp.Id) && string.Equals((employee.OriginalEmployeeNumber ?? string.Empty).Trim()
                                , (employee.EmployeeNumber.Trim() ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase))
                        {
                            UpdateEmployeeJob(employee, jobService, emp);
                            employeeJobSaved = true;
                        }

                        emp = empService.Update(emp, workSchedule, appliedTimes);
                        if (!string.IsNullOrWhiteSpace(employee.Id) && (!string.Equals((employee.OriginalEmployeeNumber ?? string.Empty).Trim(),
                            (employee.EmployeeNumber.Trim() ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase)))
                        {
                            empService.UpdateEmployeeNumber(emp, employee.OriginalEmployeeNumber.Trim(), employee.EmployeeNumber.Trim());
                        }
                        if (!employeeJobSaved)
                        {
                            UpdateEmployeeJob(employee, jobService, emp);
                        }
                        if (!string.IsNullOrWhiteSpace(emp.Id) && uiSchedule.ActualTimes.Count > 0 && uiSchedule.ScheduleType == ScheduleType.Variable)
                        {
                            empService.SetWorkSchedule(emp, workSchedule, appliedTimes);
                        }
                        if (!locationSaved) { UpdateEmployeeLocation(employee, orgService, emp); emp.Save(); };

                        if (string.IsNullOrEmpty(backURL) || !backURL.ToLower().Contains("case"))
                            return RedirectViaJavaScript(Url.RouteUrl("ViewEmployee", new { employeeId = emp.Id }));
                        else
                            return RedirectViaJavaScript(backURL);
                    }
                }
                catch (AbsenceSoftException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    CopyEmployeeCustomFields(employee);
                    return View(employee);
                }
            }

            if (Request.IsAjaxRequest())
            {
                // What does this function do?
                // Ans: The client side validation for radio buttons (fields like Meets50In75Rule, IsExempt & IsKeyEmployee) does not fire in MVC (no resolution found)
                // and it is posted back to server for validation. If server side validation fails, the view is rendered again as a result the custom fields section disappears.
                // In case this scenario occures, the values enter by the user on the custom fields are retailed by copying them to the customfields 
                // and sent back to the view so that the view will render and values entered will be retained
                CopyEmployeeCustomFields(employee);

                // Populate the EmployeeCLass
                using (var employeeService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    employee.EmployeeClasses = employeeService.GetAllEmployeeClasses();
                }

                return PartialView(employee);
            }

            return View(employee);
        }

        private void CopyEmployeeCustomFields(EmployeeViewModel employee)
        {
            List<CustomFieldViewModel> employeeCustomFields = null;
            using (var employerService = new EmployerService())
            {
                employerService.CurrentEmployer = Employer.GetById(employee.EmployerId);
                if (string.IsNullOrEmpty(employee.Id))
                    //In Employee add mode only custom fields with collectedAtIntake true
                    employeeCustomFields = employerService.GetCustomFields(EntityTarget.Employee, true).Select(cf => new CustomFieldViewModel(cf)).ToList();
                else
                    employeeCustomFields = employerService.GetCustomFields(EntityTarget.Employee).Select(cf => new CustomFieldViewModel(cf)).ToList();

            }

            if (employeeCustomFields != null)
            {
                foreach (CustomFieldViewModel fld in employeeCustomFields)
                {
                    CustomFieldViewModel field = employee.EmployeeCustomFields.FirstOrDefault(x => x.Code == fld.Code);
                    if (field != null && field.SelectedValue != null)
                        fld.SelectedValue = ((string[])field.SelectedValue)[0];
                }
                employee.EmployeeCustomFields = employeeCustomFields;
            }
        }

        private void UpdateEmployeeJob(EmployeeViewModel employee, JobService jobService, Employee emp)
        {
            if (employee.Job != null && employee.Job.IHazJob)
            {
                if (employee.Job.EmployeeNumber == null)
                    employee.Job.EmployeeNumber = employee.EmployeeNumber.Trim();

                EmployeeJob job = emp.GetCurrentJob();
                job = employee.Job.ApplyToDataModel(job, emp.ServiceDate);

                if (employee.Job.isEndCurrentJob)
                    jobService.UpdateEmployeeJob(job, employee.Job.isEndCurrentJob);
                else
                    jobService.UpdateEmployeeJob(job);

                emp.JobTitle = job == null ? employee.Job == null ? null : employee.Job.Title : job.JobTitle;
            }
        }

        /// <summary>
        /// updates employee job(s) added in past.
        /// </summary>
        /// <param name="employee">employee view model object</param>
        /// <param name="employeeJob">employee job view model object</param>
        /// <param name="jobService">job service object</param>
        /// <param name="emp">employee object</param>
        private void UpdateEmployeeJobHistory(EmployeeViewModel employee, EmployeeJobViewModel employeeJob, JobService jobService, Employee emp)
        {
            if (employeeJob != null && employeeJob.IHazJob)
            {
                /* check for delete job option is checked */
                if (employeeJob.isDeleteEmployeeJob)
                {
                    // delete employee job
                    jobService.RemoveEmployeeJob(employeeJob.Id);
                }
                else
                {
                    /* Update the job detail */
                    if (employeeJob.EmployeeNumber == null)
                    {
                        employeeJob.EmployeeNumber = employee.EmployeeNumber.Trim();
                    }                       

                    EmployeeJob job = emp.GetJobById(employeeJob.Id);
                    job = employeeJob.ApplyToDataModel(job, emp.ServiceDate, true);
                    jobService.UpdateEmployeeJob(job);
                }
            }
        }

        private void UpdateEmployeeLocation(EmployeeViewModel employee, OrganizationService orgService, Employee emp)
        {
            EmployeeOrganization org = null;
            if (employee.Organization != null)
            {
                org = emp.GetOfficeLocation();
                employee.Organization.EmployerId = employee.EmployerId;
                org = employee.Organization.ApplyToDataModel(org, emp.EmployeeNumber.Trim(), emp.ServiceDate);
                orgService.UpdateOrganization(org);
            }

            if (emp.Info != null)
            {
                emp.Info.OfficeLocation = org == null ? employee.Organization == null ? null : employee.Organization.Code : org.Code;
            }
        }

        [HttpPost, Route("Employees/Schedule")]
        [ValidateApiAntiForgeryToken]
        public ActionResult EmployeeScheduleEditor(ScheduleViewModel schedule)
        {
            EmployeeViewModel emp = new EmployeeViewModel()
            {
                Schedule = schedule
            };
            schedule.Times.Clear();
            if (schedule.TemporaryActualScheduleValuesInString != null)
            {
                schedule.TemporaryActualTimes = JsonConvert.DeserializeObject<List<TimeViewModel>>(schedule.TemporaryActualScheduleValuesInString);
            }
            if (schedule.TemporaryActualScheduleWorkingHoursInString != null)
            {
                schedule.TemporaryVariableScheduleWeeklyTimes = JsonConvert.DeserializeObject<List<TimeViewModel>>(schedule.TemporaryActualScheduleWorkingHoursInString);
            }
            schedule.BuildTimesLists();
            return PartialView(emp);
        }
        [HttpGet, Route("Employees/PaySchedule/{employerId}")]
        public ActionResult EmployeePaySchedule(string employerId)
        {
            List<PaySchedule> employerPaySchedules = new PayScheduleService().PayScheduleForEmployer(employerId);
            List<SelectListItem> paySchedulesDropDown = employerPaySchedules.Select(p => new SelectListItem()
            {
                Value = p.Id,
                Text = p.Name
            }).ToList();
            return Json(paySchedulesDropDown, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, Route("Employees/{employerId}/ContactTypes/Medical")]
        public ActionResult EmployeeContactTypes(string employerId)
        {
            try
            {
                var contactTypeService = new ContactTypeService();
                var contactTypes = contactTypeService.GetMedicalContactTypes(employerId);
                return Json(contactTypes);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Route("Employees/Location/{employerId}")]
        public ActionResult EmployeeLocation(string employerId)
        {
            List<Organization> employerOrganizations = Organization.AsQueryable().Where(o => o.EmployerId == employerId && o.TypeCode == OrganizationType.OfficeLocationTypeCode).OrderBy(o => o.Name).ToList();
            List<SelectListItem> organizationDropDown = employerOrganizations.Select(p => new SelectListItem()
            {
                Value = p.Code,
                Text = p.Name
            }).ToList();
            organizationDropDown.Insert(0, new SelectListItem()
            {
                Value = string.Empty,
                Text = "Select a Location"
            });
            return Json(organizationDropDown, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, Route("Employees/Jobs/{employerId}")]
        public ActionResult EmployeeJobs(string employerId, string officeLocation)
        {
            var jobs = Job.AsQueryable()
                .Where(j => j.EmployerId == employerId)
                .Where(j => string.IsNullOrWhiteSpace(officeLocation) || j.OrganizationCode == officeLocation || j.OrganizationCode == null)
                .OrderBy(j => j.Name)
                .ToList();
            List<SelectListItem> jobDropDown = jobs.Select(p => new SelectListItem()
            {
                Value = p.Code,
                Text = p.Name+"("+p.Code+")"
            }).ToList();
            jobDropDown.Insert(0, new SelectListItem()
            {
                Value = string.Empty,
                Text = "Select a Job"
            });
            return Json(jobDropDown, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, Route("Employees/EmployerWeeklyWorkHours/{employerId}")]
        public ActionResult EmployerWeeklyWorkHours(string employerId)
        {
            var employerWeeklyWorkHours = Employer.AsQueryable()
                .Where(e => e.Id == employerId)
                .Select(e => e.FTWeeklyWorkHours);
            
            return Json(employerWeeklyWorkHours, JsonRequestBehavior.AllowGet);
        }
        private ViewResult GetEmployeeInfo(string employeeId, bool fetchEmpOrgs = false)
        {
            using (var empService = new EmployeeService())
            {
                var emp = empService.GetEmployee(employeeId);               
                if (!CurrentUser.HasEmployeeOrganizationAccess(emp))
                {
                    return View("~/Views/Home/Index.cshtml");
                }
                if (fetchEmpOrgs)
                {
                    using (var orgService = new OrganizationService(CurrentUser))
                    {
                        EmployeeViewModel employeeViewModel = new EmployeeViewModel(emp, orgService.GetCurrentOrganizationTreeForEmployee(emp));

                        // instantiate job - to create new job detail from edit employee form.
                        employeeViewModel.Job = new EmployeeJobViewModel();

                        // get previously added job(s).
                        employeeViewModel.JobHistory = GetSortedEmployeeJobList(emp.EmployerId, emp.EmployeeNumber);

                        // Check there is no job history and entered only job title
                        if (employeeViewModel.JobHistory.Count == 0 && !string.IsNullOrWhiteSpace(emp.JobTitle))
                        {
                            employeeViewModel.Job.Title = emp.JobTitle;
                        }

                        using (var employeeService = new EmployeeClassService(CurrentCustomer, CurrentEmployer, CurrentUser))
                        {
                            employeeViewModel.EmployeeClasses = employeeService.GetAllEmployeeClasses();
                        }
                        return View(employeeViewModel);
                    }
                }
                else
                    return View(new EmployeeViewModel(emp));
            }
        }

        [Route("Employees/{employeeId}/CaseHistory", Name = "CaseHistory")]
        public PartialViewResult CaseHistory(string employeeId)
        {
            return PartialView();
        }

        [Route("Employees/{employeeId}/TimeTracker", Name = "TimeTracker")]
        public PartialViewResult TimeTracker(string employeeId)
        {
            return PartialView();
        }

        [Route("Employees/{employerId}/EmployeeActionHistory/{employeeNumber}")]
        public ActionResult EmployeeLocationHistory(string employerId, string employeeNumber, bool fromUI= false)
        {
            var savedEmployeeOrgs = EmployeeOrganization.AsQueryable().Where(eo => eo.EmployeeNumber == employeeNumber && eo.EmployerId == employerId && eo.TypeCode == OrganizationType.OfficeLocationTypeCode).ToList();
            List<EmployeeOrganizationViewModel> employeeOrg = savedEmployeeOrgs.Select(eo => new EmployeeOrganizationViewModel(eo)).OrderByDescending(eo => eo.EndDate).ToList();
            //fromUI call should return View instead of Json
            if (Request.IsAjax() && !fromUI)
            {
                return Json(employeeOrg);
            }
            return PartialView(employeeOrg);
        }

        [Route("Employees/{employerId}/EmployeeJobHistory/{employeeNumber}")]
        public ActionResult EmployeeJobHistory(string employerId, string employeeNumber, bool fromUI = false)
        {
            List<EmployeeJobViewModel> employeeJobs = GetSortedEmployeeJobList(employerId, employeeNumber);
            //fromUI call should return View instead of Json
            if (Request.IsAjax() && !fromUI)
                return Json(employeeJobs);

            return PartialView(employeeJobs);
        }

        [Route("Employees/{employerId}/EmployeeJobHistoryGet/{employeeNumber}")]
        public ActionResult EmployeeJobHistoryGet(string employerId, string employeeNumber)
        {
            List<EmployeeJobViewModel> employeeJobs = GetSortedEmployeeJobList(employerId, employeeNumber);

            return PartialView("EmployeeJobHistory", employeeJobs);
        }

        private List<EmployeeJobViewModel> GetSortedEmployeeJobList(string employerId, string employeeNumber)
        {
            // Materialize a list of dates that are not nullable (for sorting)
            var employeeJobs = EmployeeJob.AsQueryable()
                .Where(j => j.EmployerId == employerId && j.EmployeeNumber == employeeNumber)
                .Select(j => new { StartDate = j.Dates.StartDate, EndDate = j.Dates.EndDate.GetValueOrDefault(DateTime.Parse("12/31/9999")), Jobs = new EmployeeJobViewModel(j) })
                .ToList();

            // Perform the sort (i tried to bake this sort into the above but kept getting serialization errors.
            var sortedJobs = employeeJobs
                                .OrderByDescending(o => o.EndDate)
                                .ThenByDescending(o => o.StartDate)
                                .Select(j => j)
                                .ToList();

            // Get the current job.
            var currentJob = sortedJobs.FirstOrDefault(j => DateTime.Now.IsInRange(j.StartDate, j.EndDate));

            // Attempt to move the current job to the top of the list.
            if (currentJob != null && sortedJobs.Remove(currentJob))
            {
                sortedJobs.Insert(0, currentJob);
            }

            return sortedJobs.Select(s => s.Jobs).ToList();
        }

        #endregion

        [Title("Employee"), Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/{employeeId}/View", Name = "ViewEmployee")]
        public ViewResult ViewEmployee(string employeeId)
        {
            ViewBag.EmployeeId = employeeId;
            using (var service = new EmployeeService())
            {
                var emp = service.GetEmployee(employeeId);
                if (emp != null)
                {
                    if (!CurrentUser.HasEmployeeOrganizationAccess(emp))
                    {
                        return View("~/Views/Home/Index.cshtml");
                    }
                    ViewBag.EmployeeFullName = emp.FirstName + " " + emp.LastName;
                }
                emp.GenerateEmployeeView(CurrentUser);
            }
            return View("View");
        }

        [ChildActionOnly]
        public PartialViewResult EmployeeInfo(string employeeId)
        {
            using (var service = new EmployeeService())
            {
                return PartialView(new ViewEmployeeModel(service.GetEmployee(employeeId)));
            }
        }

        [HttpGet, Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/Employee/{employeeId}")]
        public JsonResult GetEmployee(string employeeId)
        {
            try
            {
                Employee emp = Employee.GetById(employeeId);
                var data = new CreateEmployeeModel(emp);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/{employeeId}/GetEmployerFeature")]
        public JsonResult GetEmployerFeature(string employeeId)
        {
            try
            {
                Employee emp = Employee.GetById(employeeId);
                return Json(new { LOAFeatureEnabled = emp.Employer.HasFeature(Feature.LOA) });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/{employeeId}/GetInfo")]
        public JsonResult GetEmployeeInfo(string employeeId, string caseId)
        {
            try
            {
                var emp = Employee.GetById(employeeId);
                var riskProfile = emp.RiskProfile;
                if (!string.IsNullOrWhiteSpace(caseId))
                {
                    var caseEmployee = Case.GetById(caseId).Employee;
                    caseEmployee.WorkSchedules = emp.WorkSchedules;
                    caseEmployee.Info = emp.Info;
                    emp = caseEmployee;
                }

                if (emp.WorkSchedules != null)
                    emp.WorkSchedules = emp.WorkSchedules.OrderByDescending(s => s.StartDate).Take(52).ToList();

                var serviceOption = string.Empty;
                using (var employerServiceOptionService = new EmployerServiceOptionService(CurrentCustomer, CurrentEmployer, CurrentUser))
                {
                    var customerServiceOptionId = employerServiceOptionService.GetCustomerServiceOptionId(CurrentEmployer.Id, "ServiceOptionIndicator");
                    if (!string.IsNullOrWhiteSpace(customerServiceOptionId))
                    {
                        using (var customerServiceOptionService = new CustomerServiceOptionService(CurrentCustomer, CurrentEmployer, CurrentUser))
                        {
                            var customerServiceOption = customerServiceOptionService.GetCustomerServiceOptionById("ServiceOptionIndicator", customerServiceOptionId);
                            if (customerServiceOption != null)
                                serviceOption = customerServiceOption.Value ?? string.Empty;
                        }
                    }
                }

                var employer = Employer.GetById(emp.EmployerId);

                var createEmployeeModel = new CreateEmployeeModel(emp)
                {
                    HasConsultsFeature = CurrentCustomer.HasFeature(Feature.EmployeeConsults),
                    HasRiskProfileFeature = CurrentCustomer.HasFeature(Feature.RiskProfile),
                    HasFlightCrewFeature = CurrentCustomer.HasFeature(Feature.FlightCrew),
                    ServiceOptionType = serviceOption,
                    EmployerReferenceCode = employer.ReferenceCode,
                    JobHistory = GetSortedEmployeeJobList(emp.EmployerId, emp.EmployeeNumber),
                    RiskProfile = riskProfile == null ? null : new RiskProfileViewModel(riskProfile),
                    HasIgnoreScheduleForPriorHoursWorked = employer.IgnoreScheduleForPriorHoursWorked,
                    HasOrgDataVisibility = CurrentEmployer.HasFeature(Feature.OrgDataVisibility)
                };

                if (!string.IsNullOrEmpty(caseId))
                    createEmployeeModel.OfficeLocation = emp.Info.OfficeLocation;

                    using (var org = new OrganizationService(CurrentUser))
                    {
                        createEmployeeModel.EmployeeCurrentLevelOneOrganizations = org.GetCurrentLevelOneEmployeeOrganizations(emp);
                        createEmployeeModel.EmployeeOrganizationsList = GetEmployeeOrganizationsList(emp);
                        createEmployeeModel.EmployeeCurrentOrganizationsList = createEmployeeModel.EmployeeOrganizationsList.Count > 0 ? createEmployeeModel.EmployeeOrganizationsList.OrderByDescending(s => s.EndDate != null ? s.EndDate : DateTime.MaxValue).OrderByDescending(s => s.StartDate != null ? s.StartDate : DateTime.MaxValue).ToList() : null;
                    }

                return Json(createEmployeeModel);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }        

        [Secure("DeleteEmployee")]
        [Route("Employees/{employeeId}/Delete", Name = "DeleteEmployee")]
        public JsonResult Delete(string employeeId)
        {
            try
            {
                // There it goes, we're gunna delete this employee, say bye bye employee person no longer :-)
                new EmployeeService().Using(e => e.DeleteEmployee(employeeId));

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }

        }

        [HttpGet, Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/GetEmployeeQuickCaseInfo/{employeeId}")]
        public JsonResult GetEmployeeQuickCaseInfo(String employeeId)
        {
            var model = new List<EmployeeQuickCaseInfoModel>();

            if (!String.IsNullOrWhiteSpace(employeeId))
            {
                var query = Case.AsQueryable().Where(o => o.Employee.Id == employeeId).OrderBy(o => o.EndDate);
                foreach (var _case in query)
                {
                    model.Add(new EmployeeQuickCaseInfoModel()
                    {
                        CaseId = _case.Id,
                        CaseNumber = _case.CaseNumber,
                        Reason = _case.Reason.Name,
                        Description = _case.Description
                    });
                }
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, Secure("EditEmployee", "ViewEmployee")]
        [Route("Employees/GetEmployeeWorkSchedule/{employeeId}")]
        public JsonResult GetEmployeeWorkSchedule(string employeeId)
        {
            try
            {
                Employee emp = Employee.GetById(employeeId);
                var data = new CreateEmployeeModel(emp).WorkSchedule;

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [Secure("EditEmployee", "EditMyPeerInfo", "CreateCase"), HttpGet]
        [Route("Employees/IsEmployeeInfoComplete/{employeeId}")]
        public JsonResult IsEmployeeInfoComplete(string employeeId)
        {
            try
            {
                using (var employeeSvc = new EmployeeService())
                {
                    List<string> incompleteFields = employeeSvc.IsEmployeeInfoComplete(employeeId);
                    return Json(new { IsEmployeeInfoComplete = incompleteFields.Count > 0 ? false : true, IncompleteFields = incompleteFields });
                };
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost]
        [Route("Employees/UpdatePaySchedule")]
        [ValidateApiAntiForgeryToken]
        public JsonResult UpdatePaySchedule(PayScheduleModel model)
        {
            try
            {
                using (var employeeSvc = new EmployeeService())
                {
                    Employee emp = Employee.GetById(model.EmployeeId);
                    if (emp == null)
                    {
                        throw new AbsenceSoftException("Unable to find employee");
                    }


                    emp.PayScheduleId = model.PayScheduleId;
                    emp.Save();

                    return Json(new { Success = true });
                };
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Route("Employees/{employeeId}/VariableSchedule")]
        public JsonResult GetVariableWorkSchedule(string employeeId, int month, int year)
        {
            try
            {
                DateTime startDate = new DateTime(year, month, 1);
                DateTime endDate = startDate.GetLastDayOfMonth();
                List<TimeModel> times = new EmployeeService().Using(e => e.GetEmployeeVariableSchedule(employeeId, startDate, endDate))
                    .Select(t => new TimeModel()
                    {
                        TotalMinutes = ((int)t.Time.TotalMinutes).ToFriendlyTime(),
                        SampleDate = t.Time.SampleDate
                    }).ToList();
                return Json(times);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet, Route("Employees/GetEmployeeClass/{employerId}")]
        public PartialViewResult GetEmployeeClass(string employerId)
        {
            EmployeeViewModel emp = new EmployeeViewModel()
            {
                CustomerId = CurrentCustomer.Id,                
            };            

            using (var employeeService = new EmployeeClassService(CurrentCustomer.Id, employerId, CurrentUser.Id))
            {
                emp.EmployeeClasses = employeeService.GetAllEmployeeClasses();
            }
            return PartialView("_EmployeeClassInfo", emp);
        }

        [HttpGet, Route("Employees/GetCustomFields/{employerId}/{employeeId?}")]
        public PartialViewResult GetCustomFields(string employerId, string employeeId)
        {
            List<CustomFieldViewModel> employeeCustomFields = null;
            List<CustomField> employeeSavedFields = null;

            if (!string.IsNullOrEmpty(employeeId))
            {
                using (var employeeSvc = new EmployeeService())
                {
                    employeeSavedFields = employeeSvc.GetEmployee(employeeId)?.CustomFields;
                }
            }

            using (var employerService = new EmployerService())
            {
                employerService.CurrentEmployer = Employer.GetById(employerId);
                employeeCustomFields = employerService.GetCustomFields(EntityTarget.Employee).Select(cf => new CustomFieldViewModel(cf)).ToList();
            }

            if (employeeSavedFields != null)
            {
                foreach (CustomFieldViewModel fld in employeeCustomFields)
                {
                    CustomField field = employeeSavedFields.FirstOrDefault(x => x.Code == fld.Code && x.Id == fld.Id);
                    if (field != null)
                        fld.SelectedValue = field.SelectedValue;
                }
            }
            return PartialView("_CustomFields", employeeCustomFields);
        }

        /// <summary>
        /// Get Employee Organizations List
        /// </summary>
        /// <param name="emp">Employee</param>
        /// <returns>Employee Organizations</returns>
        private List<EmployeeOrganizationViewModel> GetEmployeeOrganizationsList(Employee emp)
        {
            string parentName = string.Empty, parentCode = string.Empty, parentTypeCode = string.Empty;

            List<EmployeeOrganizationViewModel> orgs = new List<EmployeeOrganizationViewModel>();
            List<EmployeeOrganization> empOrgs = new List<EmployeeOrganization>();
            List<Organization> parentOrgs = new List<Organization>();

            using (var org = new OrganizationService(CurrentUser))
            {
                empOrgs = org.GetEmployeeOrganizationsList(emp);

                if (empOrgs.Count > 0 && empOrgs.Any(e => e.Path.GetParentPath() != HierarchyPath.Null))
                {
                    List<HierarchyPath> parentPaths = empOrgs.Where(e => e.Path.GetParentPath() != HierarchyPath.Null).Select(p => p.Path.GetParentPath()).ToList();
                    parentOrgs = org.GetParentOrganizationList(emp, parentPaths);
                }
            }

            foreach (var empOrg in empOrgs)
            {
                if (empOrg.Path.GetParentPath() != HierarchyPath.Null)
                {
                    if (parentOrgs.Any(p => p.Path == empOrg.Path.GetParentPath()))
                    {
                        var parentOrg = parentOrgs.FirstOrDefault(o => o.Path == empOrg.Path.GetParentPath());

                        if (parentOrg != null)
                        {
                            parentName = parentOrg.Name;
                            parentCode = parentOrg.Code;
                            parentTypeCode = parentOrg.TypeCode;
                        }
                    }
                }

                EmployeeOrganizationViewModel model = new EmployeeOrganizationViewModel(empOrg, parentName, parentCode, parentTypeCode);
                orgs.Add(model);
            }

            return orgs;
        }
    }
}
