﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Models.Organization;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Logic.Security;
using AbsenceSoft.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    [Secure("EditOrganizations")]
    public class OrganizationController : BaseController
    {
        // GET: Organization
        [HttpGet, Route("Organization/{employerId}/List", Name = "ManageOrganizations")]
        public ActionResult ManageOrganizations(string employerId)
        {
            using (OrganizationService svc = new OrganizationService(CurrentUser))
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.EmployerId = employerId;
                model.EmployerName = Employer.GetById(employerId).Name;
                return View(model);
            }
        }

        // Find Organization
        [HttpGet, Route("Organization/{employerId}/FindOrganizations/{orgSearchTerm?}", Name = "FindOrganizations")]
        public JsonResult FindOrganizations(string employerId, string orgSearchTerm)
        {
            using (OrganizationService svc = new OrganizationService(CurrentUser))
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.EmployerOrganizations = svc.GetEmployerOrganizations(employerId, orgSearchTerm);                
                return Json(model);
            }
        }
        
        [HttpPost, Route("Organization/List", Name="ListOrganizations")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListOrganizations(ListCriteria criteria)
        {
            using (var orgService = new OrganizationService(CurrentUser))
            {
                var organizations = orgService.ListOrganizations(criteria);
                return PartialView(organizations);
            }
        }

        [HttpGet, Route("Organization/{employerId}/Edit/{organizationId?}", Name = "OrganizationEdit")]
        public ActionResult OrganizationEdit(string employerId, string organizationId = null)
        {
            OrganizationViewModel orgViewModel = null;
            if (organizationId != null)
            {
                Organization org = Organization.GetById(organizationId);
                orgViewModel = new OrganizationViewModel(org);
            }
            else
            {
                orgViewModel = new OrganizationViewModel();
            }
            
            return View(orgViewModel);
        }

        [HttpPost, Route("Organization/Save", Name = "SaveOrganization")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveOrganization(OrganizationViewModel orgViewModel)
        {
            Organization org = null;
            HierarchyPath originalPath = HierarchyPath.Null;
            if (!string.IsNullOrEmpty(orgViewModel.Id))
            {
                org = Organization.GetById(orgViewModel.Id);
                originalPath = org.Path;
            }
            else
            {
                org = new Organization();
            }            
            org = orgViewModel.ApplyToDataModel(org);
            org.Save();            
            if (!string.IsNullOrEmpty(orgViewModel.Id) && originalPath != org.Path)
            {
                using (var svc = new OrganizationService(CurrentUser))
                {
                    List<Organization> childOrgs = svc.GetChildOrganizations(orgViewModel.EmployerId, originalPath);
                    var bwo = Organization.Repository.Collection.InitializeUnorderedBulkOperation();                    
                    foreach (Organization childOrg in childOrgs)
                    {
                        childOrg.Path = HierarchyPath.Parse(childOrg.Path.Path.Replace(originalPath.Path, org.Path.Path));
                        bwo.Find(Organization.Query.EQ(og => og.Id, childOrg.Id)).Upsert().ReplaceOne(childOrg);
                        //childOrg.Save();
                    }
                    if (childOrgs.Count >0 ) bwo.Execute();
                    List<EmployeeOrganization> childEmpOrgs = svc.GetChildEmployeeOrganizations(orgViewModel.EmployerId, originalPath);
                    var bweo = EmployeeOrganization.Repository.Collection.InitializeUnorderedBulkOperation();
                    foreach (EmployeeOrganization childEmpOrg in childEmpOrgs)
                    {
                        childEmpOrg.Path = HierarchyPath.Parse(childEmpOrg.Path.Path.Replace(originalPath.Path, org.Path.Path));
                        bweo.Find(EmployeeOrganization.Query.EQ(og => og.Id, childEmpOrg.Id)).Upsert().ReplaceOne(childEmpOrg);
                        //childOrg.Save();
                    }
                    if (childEmpOrgs.Count > 0) bweo.Execute();
                }
            }                        
            return Json(new { Success = true });
        }
        
        [HttpDelete, Route("Organization/{employerId}/Delete/{organizationId}", Name = "DeleteOrganization")]
        public ActionResult DeleteOrganization(string employerId, string organizationId)
        {
            try
            {                
                using (var svc = new OrganizationService(CurrentUser))
                {
                    svc.DeleteOrganization(employerId, organizationId);
                    return Json(new { Success = true });
                }             
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Organization");
            }
        }

        [HttpGet, Route("Organization/{userId}/GetOrganizationTreeForUser", Name = "GetOrganizationTreeForUser")]
        public JsonResult GetOrganizationTreeForUser(string userId, string organizationType, string searchTerm)
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                User user = AbsenceSoft.Data.Security.User.GetById(userId);
                List<OrganizationVisibilityTree> tree = svc.GetOrganizationTreeForUser(user, organizationType, searchTerm);
                if (tree != null) { return Json(tree); } else { return Json(new List<OrganizationVisibilityTree>()); }
            }
        }
        [HttpGet, Route("Organization/{employeeId}/GetOrganizationTreeForEmployee", Name = "GetOrganizationTreeForEmployee")]
        public ActionResult GetOrganizationTreeForEmployee(string employeeId, string organizationType, string searchTerm)
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                Employee emp = Employee.GetById(employeeId);
                List<OrganizationVisibilityTree> tree = svc.GetOrganizationTreeForEmployee(emp, organizationType, searchTerm);
                return PartialView("~/Views/Shared/EditorTemplates/AssignOrganizations.cshtml", tree);
            }
        }

        [HttpGet, Route("Organization/{userId}/GetCurrentOrganizationTreeForUser", Name = "GetCurrentOrganizationTreeForUser")]
        public JsonResult GetCurrentOrganizationTreeForUser(string userId)
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                User user = AbsenceSoft.Data.Security.User.GetById(userId);
                List<OrganizationVisibilityTree> tree = svc.GetCurrentOrganizationTreeForUser(user);
                if (tree != null) { return Json(tree); } else { return Json(new List<OrganizationVisibilityTree>()); }
            }
        }

        [Secure, HttpPost]
        [Route("Organization/{userId}/SaveOrganizationVisibilityForUser", Name = "SaveOrganizationVisibilityForUser")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveOrganizationVisibilityForUser(OrganizationVisibilityViewModel viewModel)
        {
            string userId = RouteData.Values["userId"].ToString();
            if (string.IsNullOrWhiteSpace(userId))
                return JsonError(new Exception("user id is not provided"));

            User user = new AuthenticationService().GetUserById(userId);
            if (user == null)
                return JsonError(new Exception("user is not valid"));

            using (var svc = new OrganizationService(CurrentUser))
            {
                svc.SaveOrganizationVisibilityForUser(user, viewModel.OrganizationVisibilityTrees);
                return Json(new { Success = true });
            }
        }
        [Secure, HttpPost]
        [Route("Organization/{employeeId}/SaveOrganizationVisibilityForEmployee", Name = "SaveOrganizationVisibilityForEmployee")]
        [ValidateApiAntiForgeryToken]
        public ActionResult SaveOrganizationVisibilityForEmployee(OrganizationVisibilityViewModel viewModel)
        {
            string employeeId = RouteData.Values["employeeId"].ToString();
            if (string.IsNullOrWhiteSpace(employeeId))
                return JsonError(new Exception("employee id is not provided"));

            Employee emp = Employee.GetById(employeeId);
            if (emp == null)
                return JsonError(new Exception("user is not valid"));

            using (var svc = new OrganizationService(CurrentUser))
            {
                svc.SaveOrganizationVisibilityForEmployee(emp, viewModel.OrganizationVisibilityTrees);
                return Json(new { Success = true });
            }
        }
        [HttpGet, Route("Organization/{employerId}/IsOrganizationCodeExisting/{orgCode}", Name = "IsOrganizationCodeExisting")]
        public JsonResult IsOrganizationCodeExisting(string employerId, string orgCode, string orgId) 
        {
            using (var svc = new OrganizationService(CurrentUser))
            {
                return Json(new { Result = svc.IsOrganizationCodeExisting(employerId, orgId, orgCode) });
            }
        }
    }
}