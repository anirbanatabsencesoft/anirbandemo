﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Security;
using AbsenceSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Filters;
using AbsenceSoft.Data;
using AbsenceSoft.Web;
using AbsenceSoft.Web.Controllers;

namespace AbsenceSoft.Controllers
{
    public abstract class BaseController : SharedBaseController
    {
        private Messaging _m;
        protected virtual Messaging M { get { if (_m == null) _m = new Messaging(ControllerContext.HttpContext); return _m; } }

        private Array _userPermissions;
        private Array UserPermissions
        {
            get
            {
                if (_userPermissions == null && Employer.Current != null)
                {
                    _userPermissions = AbsenceSoft.Data.Security.User.Permissions.EmployerPermissions(Employer.Current.Id).ToArray();
                }
                else if (_userPermissions == null)
                {
                    _userPermissions = AbsenceSoft.Data.Security.User.Permissions.ProjectedPermissions.ToArray();
                }
                    
                return _userPermissions;
            }
        }

        protected virtual void AddError(string message, params string[] formatArgs)
        {
            M.AddError(message, formatArgs);
        }

        protected virtual void AddWarning(string message, params string[] formatArgs)
        {
            M.AddWarning(message, formatArgs);
        }

        protected virtual void AddInfo(string message, params string[] formatArgs)
        {
            M.AddInfo(message, formatArgs);
        }

        protected virtual void AddSuccess(string message, params string[] formatArgs)
        {
            M.AddSuccess(message, formatArgs);
        }

        protected virtual bool IsEmployeeSelfService
        {
            get
            {
                return AbsenceSoft.Data.Security.User.IsEmployeeSelfServicePortal;
            }
        }

        /// <summary>
        /// Creates a <see cref="T:System.Web.Mvc.JsonResult" /> object that serializes the specified object to 
        /// JavaScript Object Notation (JSON) format using the content type, content encoding, and the JSON 
        /// request behavior.
        /// </summary>
        /// <param name="data">The JavaScript object graph to serialize.</param>
        /// <param name="contentType">The content type (MIME type).</param>
        /// <param name="contentEncoding">The content encoding.</param>
        /// <param name="behavior">The JSON request behavior</param>
        /// <returns>The result object that serializes the specified object to JSON format.</returns>
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        protected virtual JsonResult JsonError(Exception error, string message = null, int statusCode = 500)
        {
            if (error != null)
                Log.Error(string.IsNullOrWhiteSpace(message) ? error.Message : message, error);
            else if (!string.IsNullOrWhiteSpace(message))
                Log.Error(message);

            JsonNetResult res = new JsonNetResult();
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            res.ContentType = "application/json";
            res.ContentEncoding = System.Text.Encoding.UTF8;
            res.Data = new JsonErrorModel() { error = error, message = message };

            int newStatus = statusCode;
            if (error != null)
            {
                if (error is ArgumentException || error is ArgumentNullException || error is AbsenceSoftException)
                    newStatus = 600;
                var aggErr = error as AbsenceSoftAggregateException;
                if (aggErr != null)
                {
                    newStatus = 600;
                    message = message ?? string.Join("; ", aggErr.Messages);
                }
            }

            if (Response != null)
            {
                string messageToDisplay = message ?? (error == null ? "An error has ocurred" : error.Message);
                if (messageToDisplay.Length >= 512) //StatusDescription has a limit of  512.
                {
                    messageToDisplay = messageToDisplay.Substring(0, 512);
                }
                Response.StatusCode = newStatus;
                Response.StatusDescription = messageToDisplay;
            }


            return res;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.ActionDescriptor.ActionName.Equals("Login"))
                ViewBag.UserPermissions = UserPermissions;

            ViewBag.IsEmployeeSelfService = IsEmployeeSelfService;

            base.OnActionExecuting(filterContext);
        }
    }
}