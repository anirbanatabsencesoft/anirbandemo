﻿using AbsenceSoft.Common;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Models.CustomFields;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    [Secure("EditCustomFields")]
    public class CustomFieldsController : BaseController
    {
        [Title("Custom Fields")]
        [HttpGet, Route("CustomFields/List/{employerId?}", Name="ViewCustomFields")]
        public ActionResult ViewCustomFields(string employerId)
        {
            ViewBag.EmployerId = employerId;
            return View();
        }

        [HttpPost, Route("CustomFields/List", Name="ListCustomFields")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListCustomFields(ListCriteria criteria)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                var results = administrationService.ListCustomFields(criteria);
                return PartialView(results);
            }
        }

        [Title("Custom Field")]
        [HttpGet, Route("CustomFields/EditCustomField/{employerId?}", Name="EditCustomField")]
        public ActionResult EditCustomField(string employerId, string id)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CustomFieldViewModel customField = new CustomFieldViewModel(administrationService.GetCustomField(id));
                ViewBag.EmployerId = employerId;
                return View(customField);
            }
        }

        [HttpPost, Route("CustomFields/SaveCustomField", Name="SaveCustomField")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCustomField(CustomFieldViewModel customField)
        {
            if (!ModelState.IsValid)
                return PartialView(customField);

            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CustomField field = customField.ApplyToDataModel(administrationService.GetCustomField(customField.Id));
                administrationService.SaveCustomField(field);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewCustomFields"));
            }
        }

        [HttpDelete, Route("CustomFields/DeleteCustomField/{employerId?}", Name="DeleteCustomField")]
        public ActionResult DeleteCustomField(string employerId, string id)
        {
            using (var administrationService = new AdministrationService(CurrentCustomer, CurrentEmployer, CurrentUser))
            {
                CustomField field = administrationService.GetCustomField(id);
                administrationService.DeleteCustomField(field);

                return RedirectViaJavaScript(Url.AdministrationRouteUrl("ViewCustomFields"));
            }
        }
    }
}