﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AbsenceSoft.Controllers
{
    public class ErrorController : BaseController
    {
        //
        // GET: /Error/
        [Route("Error", Name = "Error")]
        public ViewResult Error()
        {
            return View("Error");
        }

        [Route("Unauthorized", Name = "Unauthorized")]
        public void Unauthorized()
        {
            Response.Redirect("/Home/Unauthorized");
        }

        [Route("NotFound", Name = "NotFound")]
        public ViewResult NotFound()
        {
            return View("NotFound");
        }
    }
}