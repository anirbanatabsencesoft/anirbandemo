﻿using AbsenceSoft.Data.Customers;
using AbsenceSoft.Logic.Customers;
using AbsenceSoft.Filters;
using AbsenceSoft.Logic.Consultation;
using AbsenceSoft.Models.Consultations;
using AbsenceSoft.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsenceSoft.Web.Attributes;
using AbsenceSoft.Web.Filters;

namespace AbsenceSoft.Controllers
{
    public class ConsultationsController : BaseController
    {
        // Consultation Types:

        [Secure("EditConsultations")]
        [HttpGet, Route("Consultations/List", Name = "ViewConsultations")]
        public ActionResult ConsultationList()
        {
            return View();
        }

        [Secure("EditConsultations")]
        [HttpPost, Route("Consultations/List", Name = "ListConsultations")]
        [ValidateApiAntiForgeryToken]
        public ActionResult ListConsultations(string employerId, ListCriteria criteria)
        {
            using (var consultService = new ConsultationService(CurrentUser))
            {
                var consultations = consultService.GetConsultationTypesResults(criteria);
                return PartialView(consultations);
            }
        }

        [Secure("EditConsultations")]
        [HttpGet, Route("Consultations/Edit/{consultationTypeId?}", Name = "ConsultationEdit")]
        public ActionResult ConsultationEdit(string consultationTypeId = null)
        {
            ConsultationTypeViewModel model = null;
            if (consultationTypeId != null)
            {
                ConsultationType consultType = ConsultationType.GetById(consultationTypeId);
                model = new ConsultationTypeViewModel(consultType);
            }
            else
            {
                model = new ConsultationTypeViewModel();
            }

            return View(model);
        }

        [Secure("EditConsultations")]
        [HttpPost, Route("Consultations/Save", Name = "SaveConsultation")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveConsultation(ConsultationTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            ConsultationType consultationType = ConsultationType.GetById(model.Id);
            consultationType = model.ApplyToDataModel(consultationType);
            using (var consultationService = new ConsultationService(CurrentUser))
            {
                consultationService.SaveConsultation(consultationType);
            }

            return RedirectViaJavaScript(Url.Action("ConsultationList"));
        }

        [Secure("EditConsultations")]
        [HttpDelete, Route("Consultations/{customerId}/Delete/{consultationTypeId}", Name = "DeleteConsultation")]
        public ActionResult DeleteConsultation(string customerId, string consultationTypeId)
        {
            try
            {
                using (var consultationService = new ConsultationService(CurrentUser))
                {
                    ConsultationType consultationType = ConsultationType.GetById(consultationTypeId);
                    consultationService.DeleteConsultation(consultationType);
                }
                
                return RedirectViaJavaScript(Url.Action("ConsultationList"));
            }
            catch (Exception ex)
            {
                return JsonError(ex, "Unable to delete Consultation");
            }
        }

        // Employee Consultations:

        [Secure("EditEmployee", "ViewEmployee", "EmployeeConsults")]
        [HttpGet, Route("Consultations/{employerId}/Employees/{employeeId}/List/{caseId?}", Name = "EmployeeConsultationList")]
        public ActionResult EmployeeConsultationEntry(string employerId, string employeeId, string caseId)
        {
            Employee emp = Employee.GetById(employeeId);
            ConsultationEntryViewModel consultationEntry = new ConsultationEntryViewModel(employeeId, caseId);
            using (var consultationService = new ConsultationService(CurrentUser))
            {
                IEnumerable<ConsultationType> types = consultationService.GetConsultationTypes().OrderBy(c => c.Consult);
                IEnumerable<EmployeeConsultation> savedConsultations = consultationService.GetEmployeeConsultations(emp.EmployeeNumber);
                foreach (var consultation in types)
                {
                    EmployeeConsultation matchingSavedConsultation = savedConsultations.FirstOrDefault(c => c.ConsultationTypeId == consultation.Id);
                    if (matchingSavedConsultation != null)
                        consultationEntry.Consultations.Add(new EmployeeConsultationViewModel(matchingSavedConsultation));
                    else
                        consultationEntry.Consultations.Add(new EmployeeConsultationViewModel(consultation, employeeId));
                }
            }
            return View(consultationEntry);
        }

        [Secure("EditEmployee", "ViewEmployee")]
        [HttpPost, Route("Consultations/SaveEntry", Name="EmployeeConsultationSave")]
        [ValidateAntiForgeryToken]
        public ActionResult EmployeeConsultationSave(ConsultationEntryViewModel consultationEntry)
        {
            using (var consultationService = new ConsultationService(CurrentUser))
            {
                Employee emp = Employee.GetById(consultationEntry.EmployeeId);
                IEnumerable<ConsultationType> types = consultationService.GetConsultationTypes().OrderBy(c => c.Consult);
                IEnumerable<EmployeeConsultation> savedConsultations = consultationService.GetEmployeeConsultations(emp.EmployeeNumber);
                foreach (var consultation in consultationEntry.Consultations)
                {
                    EmployeeConsultation matchingSavedConsultation = savedConsultations.FirstOrDefault(c => c.ConsultationTypeId == consultation.ConsultationTypeId);
                    if (string.IsNullOrWhiteSpace(consultation.Comments) && matchingSavedConsultation != null)
                        consultationService.DeleteEmployeeConsultation(matchingSavedConsultation);
                    else if(!string.IsNullOrWhiteSpace(consultation.Comments))
                    {
                        consultation.EmployeeNumber = emp.EmployeeNumber;
                        EmployeeConsultation theConsult = consultation.ApplyToDataModel(matchingSavedConsultation);
                        theConsult.EmployerId = emp.EmployerId;
                        consultationService.SaveEmployeeConsultation(theConsult);
                    }
                }
            }

            string returnUrl = !string.IsNullOrWhiteSpace(consultationEntry.CaseId) 
                    ? Url.RouteUrl("ViewCase", new { caseId = consultationEntry.CaseId }) 
                    : Url.RouteUrl("ViewEmployee", new { employeeId = consultationEntry.EmployeeId });

            return RedirectViaJavaScript(returnUrl);
        }
    }
}